-- MySQL Workbench Synchronization
-- Generated: 2016-03-04 13:35
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP TABLE `biblioteca__suspensao`;

CREATE TABLE IF NOT EXISTS `biblioteca__suspensao` (
  `emprestimo_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  `suspensao_inicio` DATETIME NULL DEFAULT NULL,
  `suspensao_fim` DATETIME NULL DEFAULT NULL,
  `suspensao_liberacao_data` DATETIME NULL DEFAULT NULL,
  `usuario_id` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL,
  `suspensao_liberacao_motivo` VARCHAR(255) NULL DEFAULT NULL,
  INDEX `fk_biblioteca_suspensao_acesso_pessoas1_idx` (`usuario_id` ASC),
  INDEX `fk_biblioteca__suspensao_biblioteca__emprestimo1_idx` (`emprestimo_id` ASC),
  PRIMARY KEY (`emprestimo_id`),
  CONSTRAINT `fk_biblioteca_suspensao_acesso_pessoas1`
    FOREIGN KEY (`usuario_id`)
    REFERENCES `acesso_pessoas` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_biblioteca__suspensao_biblioteca__emprestimo1`
    FOREIGN KEY (`emprestimo_id`)
    REFERENCES `biblioteca__emprestimo` (`emprestimo_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

DROP TABLE `biblioteca__multa`;

CREATE TABLE IF NOT EXISTS `biblioteca__multa` (
  `emprestimo_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  `titulo_id` INT(11) ZEROFILL NOT NULL,
  INDEX `fk_biblioteca__suspensao_biblioteca__emprestimo1_idx` (`emprestimo_id` ASC),
  INDEX `fk_biblioteca__multa_financeiro__titulo1_idx` (`titulo_id` ASC),
  PRIMARY KEY (`emprestimo_id`),
  CONSTRAINT `fk_biblioteca__suspensao_biblioteca__emprestimo10`
    FOREIGN KEY (`emprestimo_id`)
    REFERENCES `biblioteca__emprestimo` (`emprestimo_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_biblioteca__multa_financeiro__titulo1`
    FOREIGN KEY (`titulo_id`)
    REFERENCES `financeiro__titulo` (`titulo_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
