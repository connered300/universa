ALTER TABLE `selecao_cursos` DROP FOREIGN KEY `fk_selecao_cursos_campus_curso1`;
ALTER TABLE `inscricao_cursos` DROP FOREIGN KEY fk_inscricao_cursos_selecao_cursos1;
ALTER TABLE `prova_edicao` DROP FOREIGN KEY fk_prova_edicao_selecao_cursos1;
ALTER TABLE `prova_edicao_caderno` DROP FOREIGN KEY fk_prova_edicao_caderno_prova_edicao1;
ALTER TABLE `selecao_cursos`
ADD CONSTRAINT `fk_selecao_cursos_campus_curso1` FOREIGN KEY (`cursocampus_id`) REFERENCES `campus_curso` (`cursocampus_id`) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE `inscricao_cursos`
ADD CONSTRAINT `fk_inscricao_cursos_selecao_cursos1` FOREIGN KEY (`selcursos_id`) REFERENCES `selecao_cursos` (`selcursos_id`) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE `prova_edicao`
ADD CONSTRAINT `fk_prova_edicao_selecao_cursos1` FOREIGN KEY (`selcursos_id`) REFERENCES `selecao_cursos` (`selcursos_id`) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE `prova_edicao_caderno`
ADD CONSTRAINT `fk_prova_edicao_caderno_prova_edicao1` FOREIGN KEY (`cad_id`) REFERENCES `prova_caderno` (`cad_id`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `selecao_cursos` DROP FOREIGN KEY `fk_selecao_cursos_campus_curso1`;
ALTER TABLE `inscricao_cursos` DROP FOREIGN KEY fk_inscricao_cursos_selecao_cursos1;
ALTER TABLE `prova_edicao` DROP FOREIGN KEY fk_prova_edicao_selecao_cursos1;
ALTER TABLE `prova_edicao_caderno` DROP FOREIGN KEY fk_prova_edicao_caderno_prova_edicao1;
ALTER TABLE `selecao_cursos` ADD CONSTRAINT `fk_selecao_cursos_campus_curso1` FOREIGN KEY (`cursocampus_id`) REFERENCES `campus_curso` (`cursocampus_id`) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE `inscricao_cursos` ADD CONSTRAINT `fk_inscricao_cursos_selecao_cursos1` FOREIGN KEY (`selcursos_id`) REFERENCES `selecao_cursos` (`selcursos_id`) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE `prova_edicao` ADD CONSTRAINT `fk_prova_edicao_selecao_cursos1` FOREIGN KEY (`selcursos_id`) REFERENCES `selecao_cursos` (`selcursos_id`) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE `prova_edicao_caderno` ADD CONSTRAINT `fk_prova_edicao_caderno_prova_edicao1` FOREIGN KEY (`cad_id`) REFERENCES `prova_caderno` (`cad_id`) ON UPDATE CASCADE ON DELETE CASCADE;