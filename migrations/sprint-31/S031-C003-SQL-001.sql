CREATE TABLE sis__integracao_disciplina (
  curso_disc_id INT(10) UNSIGNED ZEROFILL NOT NULL,
  integracao_id INT(10) UNSIGNED ZEROFILL NOT NULL,
  codigo        INT                       NOT NULL,
  PRIMARY KEY (curso_disc_id, integracao_id),
  CONSTRAINT fk_CursoDiscIdDiscId FOREIGN KEY (curso_disc_id) REFERENCES acadgeral__disciplina (disc_id),
  CONSTRAINT fk_IntegracaoDisciplinaIntegracao FOREIGN KEY (integracao_id) REFERENCES sis__integracao (integracao_id)
);


ALTER TABLE acadgeral__disciplina_curso
ADD COLUMN disc__nucleo_comum ENUM('Sim', 'Nao') NOT NULL DEFAULT 'Sim';

ALTER TABLE `sis__integracao_disciplina`
DROP FOREIGN KEY `fk_CursoDiscIdDiscId`,
DROP FOREIGN KEY `fk_IntegracaoDisciplinaIntegracao`;
ALTER TABLE `sis__integracao_disciplina`
ADD CONSTRAINT `fk_CursoDiscIdDiscId`
FOREIGN KEY (`curso_disc_id`)
REFERENCES `acadgeral__disciplina_curso` (`disc_curso_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_IntegracaoDisciplinaIntegracao`
FOREIGN KEY (`integracao_id`)
REFERENCES `sis__integracao` (`integracao_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `org__email_tipo`
ADD CONSTRAINT `fk_org__email_tipo__org__email_conta`
FOREIGN KEY (`conta_id`)
REFERENCES `org__email_conta`(`conta_id`)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `org__email_tipo`
ADD CONSTRAINT `fk_org__email_tipo__org__comunicacao_tipo`
FOREIGN KEY (`tipo_id`)
REFERENCES `org__comunicacao_tipo`(`tipo_id`)
  ON DELETE CASCADE ON UPDATE CASCADE;