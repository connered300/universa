SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `acadperiodo__letivo`
-- -----------------------------------------------------
ALTER TABLE `acadperiodo__letivo`
ADD `per_periodicidade` enum('semestral','anual','Quadrimestral','Trimestral','Nao Aplicado') not null,
ADD `per_descricao` VARCHAR(45) NULL,
ADD `per_data_finalizacao` DATETIME NULL,
ADD `nivel_id` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL,
ADD INDEX `fk_acadperiodo__letivo_acad_nivel1_idx` (`nivel_id` ASC),
ADD CONSTRAINT `fk_acadperiodo__letivo_acad_nivel1` FOREIGN KEY (`nivel_id`) REFERENCES `acad_nivel` (`nivel_id`)
ON DELETE CASCADE
ON UPDATE CASCADE;

-- -----------------------------------------------------
-- Table `acadperiodo__letivo_campus_curso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `acadperiodo__letivo_campus_curso` (
  `per_id` INT UNSIGNED ZEROFILL NOT NULL,
  `cursocampus_id` INT ZEROFILL UNSIGNED NOT NULL,
  PRIMARY KEY (`per_id`, `cursocampus_id`),
  INDEX `fk_acadperiodo__letivo_campus_curso_campus_curso1_idx` (`cursocampus_id` ASC),
  INDEX `fk_acadperiodo__letivo_campus_curso_acadperiodo__letivo1_idx` (`per_id` ASC),
  CONSTRAINT `fk_acadperiodo__letivo_campus_curso_acadperiodo__letivo1`
  FOREIGN KEY (`per_id`)
  REFERENCES `acadperiodo__letivo` (`per_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_acadperiodo__letivo_campus_curso_campus_curso1`
  FOREIGN KEY (`cursocampus_id`)
  REFERENCES `campus_curso` (`cursocampus_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `acadperiodo__letivo_selecao_edicao` (
  `per_id` INT UNSIGNED ZEROFILL NOT NULL,
  `edicao_id` INT UNSIGNED ZEROFILL NOT NULL,
  PRIMARY KEY (`per_id`, `edicao_id`),
  INDEX `fk_acadperiodo__letivo_selecao_edicao_selecao_edicao1_idx` (`edicao_id` ASC),
  INDEX `fk_acadperiodo__letivo_selecao_edicao_acadperiodo__letivo1_idx` (`per_id` ASC),
  CONSTRAINT `fk_acadperiodo__letivo_selecao_edicao_acadperiodo__letivo1`
    FOREIGN KEY (`per_id`)
    REFERENCES `acadperiodo__letivo` (`per_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_acadperiodo__letivo_selecao_edicao_selecao_edicao1`
    FOREIGN KEY (`edicao_id`)
    REFERENCES `selecao_edicao` (`edicao_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


-- Atualiza Registro de Semestralidade

UPDATE acadperiodo__letivo
SET
  per_periodicidade = 'semestral'
WHERE
  per_periodicidade IS NULL;

-- Atualiza Registros de Nivel de Ensino

UPDATE acadperiodo__letivo
SET
  nivel_id = (SELECT
                nivel_id
              FROM
                acad_nivel
              WHERE
                nivel_nome = 'Bacharelado')
WHERE
  nivel_id IS NULL OR nivel_id < 1;

-- Atualiza Registros de Finalização de Periodo

set sql_safe_updates=0;

UPDATE acadperiodo__letivo
SET
  per_data_finalizacao = per_data_fim
WHERE
  per_data_finalizacao IS NULL OR per_data_finalizacao < 1;

set sql_safe_updates=1;