--
-- Estrutura da tabela `sis__integracao_turma`
--

CREATE TABLE IF NOT EXISTS `sis__integracao_turma` (
  `integracao_id` INT(10) UNSIGNED ZEROFILL NOT NULL,
  `turma_id`      INT(10) UNSIGNED ZEROFILL NOT NULL,
  `codigo`        VARCHAR(45)               NOT NULL,
  PRIMARY KEY (`integracao_id`, `turma_id`),
  KEY `turma_id` (`turma_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `sis__integracao_turma`
--
ALTER TABLE `sis__integracao_turma`
ADD CONSTRAINT `sis__integracao_turma_ibfk_1` FOREIGN KEY (`integracao_id`) REFERENCES `sis__integracao` (`integracao_id`),
ADD CONSTRAINT `sis__integracao_turma_ibfk_2` FOREIGN KEY (`turma_id`) REFERENCES `acadperiodo__turma` (`turma_id`);


ALTER TABLE `acadperiodo__turma`
ADD COLUMN `unidade_id` INT(10) UNSIGNED ZEROFILL NOT NULL;


SET foreign_key_checks = 0;

ALTER TABLE `acadperiodo__turma`
ADD CONSTRAINT `fk_unidade__turma_123`
FOREIGN KEY (`unidade_id`)
REFERENCES `org__unidade_estudo` (`unidade_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

SET foreign_key_checks = 1;