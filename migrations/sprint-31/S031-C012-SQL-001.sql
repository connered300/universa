-- Alterações realizadas para concentrar regras de cálculo de desempenho do aluno  --

ALTER TABLE acad_curso_config
ADD COLUMN cursoconfig_metodo ENUM('Media', 'Soma') NOT NULL DEFAULT 'Soma',
ADD COLUMN cursoconfig_metodo_final ENUM('Media', 'Final') NOT NULL DEFAULT 'Media';

ALTER TABLE `acad_curso`
CHANGE COLUMN `curso_periodicidade` `curso_periodicidade` ENUM('Semestral', 'Anual', 'Quadrimestral', 'Trimestral') NOT NULL
COMMENT 'É o período em que o curso forma novas turmas';