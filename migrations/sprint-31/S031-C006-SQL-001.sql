CREATE TABLE IF NOT EXISTS `acadgeral__atividade_log` (
  `log_id` INT NOT NULL AUTO_INCREMENT,
  `log_data` DATETIME NOT NULL,
  `log_action` VARCHAR(200) NOT NULL,
  `log_user_agent` VARCHAR(200) NOT NULL,
  `log_ip` VARCHAR(45) NOT NULL,
  `log_info` TEXT(3000) NULL,
  `aluno_id` INT UNSIGNED ZEROFILL NOT NULL,
  `disc_id` INT UNSIGNED ZEROFILL NULL,
  `curso_id` INT ZEROFILL NULL,
  PRIMARY KEY (`log_id`),
  INDEX `fk_acadgeral__atividade_log_acadgeral__aluno1_idx` (`aluno_id` ASC),
  INDEX `fk_acadgeral__atividade_log_acadgeral__disciplina1_idx` (`disc_id` ASC),
  INDEX `fk_acadgeral__atividade_log_acad_curso1_idx` (`curso_id` ASC),
  CONSTRAINT `fk_acadgeral__atividade_log_acadgeral__aluno1`
    FOREIGN KEY (`aluno_id`)
    REFERENCES `acadgeral__aluno` (`aluno_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_acadgeral__atividade_log_acadgeral__disciplina1`
    FOREIGN KEY (`disc_id`)
    REFERENCES `acadgeral__disciplina` (`disc_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_acadgeral__atividade_log_acad_curso1`
    FOREIGN KEY (`curso_id`)
    REFERENCES `acad_curso` (`curso_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;