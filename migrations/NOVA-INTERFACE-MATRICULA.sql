ALTER TABLE `acad_curso`
CHANGE curso_integralizacao_medida curso_unidade_medida ENUM('Mensal', 'Bimestral', 'Trimestral', 'Quadrimestral', 'Semestral', 'Anual') NOT NULL;

ALTER TABLE acad_curso
CHANGE curso_periodicidade curso_possui_periodo_letivo enum('Semestral','Anual','Quadrimestral','Trimestral','Sim', 'Não', 'Não Aplicado') NOT NULL COMMENT 'É o período em que o curso forma novas turmas';

UPDATE acad_curso
SET curso_possui_periodo_letivo=IF(curso_possui_periodo_letivo='Não' OR curso_possui_periodo_letivo='Não Aplicado', 'Não', 'Sim') where curso_id>0;

ALTER TABLE acad_curso
CHANGE curso_possui_periodo_letivo curso_possui_periodo_letivo enum('Sim', 'Não') NOT NULL COMMENT 'É o período em que o curso forma novas turmas';

CREATE TABLE `acadperiodo__letivo_campus_curso` (
  `per_id` int(10) unsigned zerofill NOT NULL,
  `cursocampus_id` int(10) unsigned zerofill NOT NULL,
  PRIMARY KEY (`per_id`,`cursocampus_id`),
  KEY `fk_acadperiodo__letivo_campus_curso_campus_curso1_idx` (`cursocampus_id`),
  KEY `fk_acadperiodo__letivo_campus_curso_acadperiodo__letivo1_idx` (`per_id`),
  CONSTRAINT `fk_acadperiodo__letivo_campus_curso_acadperiodo__letivo1` FOREIGN KEY (`per_id`) REFERENCES `acadperiodo__letivo` (`per_id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_acadperiodo__letivo_campus_curso_campus_curso1` FOREIGN KEY (`cursocampus_id`) REFERENCES `campus_curso` (`cursocampus_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



## A SEGUINTE CONSULTA SÓ DEVE SER USADA PARA EFEITO DE TESTE

REPLACE INTO acadperiodo__letivo_campus_curso (per_id, cursocampus_id)
SELECT DISTINCT
  periodo_campus.periodo,
  periodo_campus.campus
FROM
  (
    SELECT per_id periodo, cursocampus_id campus FROM acadperiodo__letivo JOIN campus_curso
  ) as periodo_campus;

## É NECESSÁRIO EXECUTAR ESSA MIGRATION PARA QUE MODALIDADE E TIPO DE CURSO FUNCIONEM

REPLACE INTO `acesso_funcionalidades` (modulo, func_nome, func_controller, func_status, func_visivel)
  SELECT distinct acesso_modulos.id, func.descricao, func.controller,'Ativa','Não'  from acesso_modulos,
    (
      select 'Nível' as descricao, 'Matricula\\Controller\\AcadNivel' as controller UNION ALL
      select 'Modalidade' as descricao, 'Matricula\\Controller\\AcadModalidade' as controller from dual
    ) as func
  where mod_nome like 'Matricula';

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
    (
      select 'index' as act,'Index' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'search-for-json' as act,'Pesquisa' as descricao,'Leitura' as tipo,'N' as usr FROM dual
    ) as actions
  where func_controller IN('Matricula\\Controller\\AcadNivel', 'Matricula\\Controller\\AcadModalidade');

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
    (
      select 'search-for-json' as act,'Pesquisa' as descricao,'Leitura' as tipo,'N' as usr FROM dual
    ) as actions
  where func_controller IN('Vestibular\\Controller\\SelecaoEdicao');


REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
  select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
    inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
    acesso_grupo
  where func_controller IN(
    'Matricula\\Controller\\AcadNivel','Matricula\\Controller\\AcadModalidade', 'Vestibular\\Controller\\SelecaoEdicao'
  )
        and grup_nome LIKE 'admin%';

