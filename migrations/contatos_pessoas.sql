ALTER TABLE `pessoa`
ADD `con_contato_telefone` VARCHAR(255) NULL,
ADD `con_contato_celular` VARCHAR(255) NULL,
ADD `con_contato_email` VARCHAR(255) NULL,
ADD INDEX (`con_contato_telefone`, `con_contato_celular`, `con_contato_email`);


UPDATE pessoa SET
  pessoa.con_contato_email = if(
      pessoa.con_contato_email!="" AND pessoa.con_contato_email is not null,
      pessoa.con_contato_email,
      (
        SELECT con_contato
        FROM contato
        WHERE pessoa = pessoa.pes_id AND tipo_contato = 1 AND con_contato != ''
        ORDER BY con_contato DESC
        LIMIT 0, 1
      )
  ),
  pessoa.con_contato_telefone = if(
      pessoa.con_contato_telefone!="" AND pessoa.con_contato_telefone is not null,
      pessoa.con_contato_telefone,
      (
        SELECT con_contato
        FROM contato
        WHERE pessoa = pessoa.pes_id AND tipo_contato = 2 AND con_contato != ''
        ORDER BY con_contato DESC
        LIMIT 0, 1
      )
  ),
  pessoa.con_contato_celular = if(
      pessoa.con_contato_celular!="" AND pessoa.con_contato_celular is not null,
      pessoa.con_contato_celular,
      (
        SELECT con_contato
        FROM contato
        WHERE pessoa = pessoa.pes_id AND tipo_contato = 3 AND con_contato != ''
        ORDER BY con_contato DESC
        LIMIT 0, 1
      )
  )
where pes_id>=1;
