-- MySQL Workbench Synchronization
-- Generated: 2016-02-01 17:42
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `biblioteca__emprestimo` 
DROP FOREIGN KEY `fk_biblioteca__emprestimo_acesso_usuarios1`;

ALTER TABLE `biblioteca_suspensao` 
DROP FOREIGN KEY `fk_biblioteca_suspensao_acesso_usuarios1`;

ALTER TABLE `biblioteca__emprestimo` 
CHANGE COLUMN `emprestimo_multa_valor` `emprestimo_multa_valor` DECIMAL NULL DEFAULT NULL ,
CHANGE COLUMN `emprestimo_multa_valor_pago` `emprestimo_multa_valor_pago` DECIMAL NULL DEFAULT NULL ,
ADD INDEX `fk_biblioteca__emprestimo_acesso_pessoas1_idx` (`usuario_id` ASC),
DROP INDEX `fk_biblioteca__emprestimo_acesso_usuarios1_idx` ;

ALTER TABLE `biblioteca_suspensao` 
ADD INDEX `fk_biblioteca_suspensao_acesso_pessoas1_idx` (`usuario_id` ASC),
DROP INDEX `fk_biblioteca_suspensao_acesso_usuarios1_idx` ;

ALTER TABLE `biblioteca__emprestimo` 
ADD CONSTRAINT `fk_biblioteca__emprestimo_acesso_pessoas1`
  FOREIGN KEY (`usuario_id`)
  REFERENCES `acesso_pessoas` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `biblioteca_suspensao` 
ADD CONSTRAINT `fk_biblioteca_suspensao_acesso_pessoas1`
  FOREIGN KEY (`usuario_id`)
  REFERENCES `acesso_pessoas` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
