ALTER TABLE `biblioteca__titulo` DROP FOREIGN KEY `fk_biblioteca__titulo_biblioteca__area_cnpq1`;
ALTER TABLE `biblioteca__titulo` CHANGE COLUMN `area_cnpq_id` `area_cnpq_id` INT(10) UNSIGNED ZEROFILL NULL ;
ALTER TABLE `biblioteca__titulo` 
	ADD CONSTRAINT `fk_biblioteca__titulo_biblioteca__area_cnpq1` FOREIGN KEY (`area_cnpq_id`)
	REFERENCES `biblioteca__area_cnpq` (`area_cnpq_id`) ON UPDATE CASCADE;
