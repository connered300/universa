-- MySQL Workbench Synchronization
-- Generated: 2016-02-01 17:03
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE IF NOT EXISTS `biblioteca__emprestimo` (
  `emprestimo_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `exemplar_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  `pes_id` INT(11) UNSIGNED ZEROFILL NOT NULL,
  `usuario_id` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL,
  `modalidade_emprestimo_id` INT(10) UNSIGNED ZEROFILL NOT NULL,
  `emprestimo_data` DATETIME NOT NULL,
  `emprestimo_devolucao_data_previsao` DATETIME NOT NULL,
  `emprestimo_devolucao_data_efetuada` DATETIME NULL DEFAULT NULL,
  `emprestimo_multa_valor` DECIMAL NULL DEFAULT NULL,
  `emprestimo_multa_valor_pago` DECIMAL NULL DEFAULT NULL,
  `emprestimo_multa_observacao` TEXT NULL DEFAULT NULL,
  `emprestimo_cancelado` ENUM('N','S') NOT NULL,
  PRIMARY KEY (`emprestimo_id`),
  INDEX `fk_biblioteca__emprestimo_biblioteca__exemplar1_idx` (`exemplar_id` ASC),
  INDEX `fk_biblioteca__emprestimo_biblioteca__pessoa1_idx` (`pes_id` ASC),
  INDEX `fk_biblioteca__emprestimo_biblioteca__modalidade_emprestimo_idx` (`modalidade_emprestimo_id` ASC),
  INDEX `fk_biblioteca__emprestimo_acesso_usuarios1_idx` (`usuario_id` ASC),
  CONSTRAINT `fk_biblioteca__emprestimo_biblioteca__exemplar1`
    FOREIGN KEY (`exemplar_id`)
    REFERENCES `biblioteca__exemplar` (`exemplar_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_biblioteca__emprestimo_biblioteca__pessoa1`
    FOREIGN KEY (`pes_id`)
    REFERENCES `biblioteca__pessoa` (`pes_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_biblioteca__emprestimo_biblioteca__modalidade_emprestimo1`
    FOREIGN KEY (`modalidade_emprestimo_id`)
    REFERENCES `biblioteca__modalidade_emprestimo` (`modalidade_emprestimo_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_biblioteca__emprestimo_acesso_usuarios1`
    FOREIGN KEY (`usuario_id`)
    REFERENCES `acesso_usuarios` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `biblioteca_suspensao` (
  `suspensao_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `pes_id` INT(11) UNSIGNED ZEROFILL NOT NULL,
  `suspensao_inicio` DATETIME NULL DEFAULT NULL,
  `suspensao_fim` DATETIME NULL DEFAULT NULL,
  `suspensao_liberada` ENUM('N','S') NULL DEFAULT NULL,
  `usuario_id` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL,
  PRIMARY KEY (`suspensao_id`),
  INDEX `fk_biblioteca_suspensao_biblioteca__pessoa1_idx` (`pes_id` ASC),
  INDEX `fk_biblioteca_suspensao_acesso_usuarios1_idx` (`usuario_id` ASC),
  CONSTRAINT `fk_biblioteca_suspensao_biblioteca__pessoa1`
    FOREIGN KEY (`pes_id`)
    REFERENCES `biblioteca__pessoa` (`pes_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_biblioteca_suspensao_acesso_usuarios1`
    FOREIGN KEY (`usuario_id`)
    REFERENCES `acesso_usuarios` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
