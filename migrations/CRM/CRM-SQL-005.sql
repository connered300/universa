-- MySQL Workbench Synchronization
-- Generated: 2017-05-12 17:08
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `org__email_conta` 
DROP FOREIGN KEY `fk_email__conta_acesso_pessoas1`;

ALTER TABLE `org__email_conta` 
ADD COLUMN `conta_imap_host` TEXT NULL DEFAULT NULL AFTER `conta_ssl`,
ADD COLUMN `conta_imap_porta` INT(11) NULL DEFAULT NULL AFTER `conta_imap_host`,
ADD COLUMN `conta_imap_ssl` ENUM('ssl','tls') NULL DEFAULT NULL AFTER `conta_imap_porta`,
ADD COLUMN `conta_lead` ENUM('Sim','Não') NOT NULL DEFAULT 'Não' AFTER `conta_imap_ssl`,
ADD COLUMN `conta_ativada` ENUM('Sim','Não') NOT NULL DEFAULT 'Sim' AFTER `conta_lead`;

ALTER TABLE `org__email_conta` 
ADD CONSTRAINT `fk_email__conta_acesso_pessoas1`
  FOREIGN KEY (`usuario`)
  REFERENCES `acesso_pessoas` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
