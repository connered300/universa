-- MySQL Workbench Synchronization
-- Generated: 2017-04-15 14:42
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `crm__operador`
DROP COLUMN `operarador_ultimo_login`,
DROP COLUMN `operador_online`,
DROP COLUMN `operador_situacao`,
DROP COLUMN `operador_nome`;

ALTER TABLE `crm__operador`
ADD CONSTRAINT `fk_crm__operador_acesso_pessoas1`
  FOREIGN KEY (`operador_id`)
  REFERENCES `acesso_pessoas` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `crm__lead_atividade` DROP COLUMN `retorno_id`;

ALTER TABLE `crm__lead_atividade`
CHANGE COLUMN `interesse_data_cadastro` `atividade_data_cadastro` DATETIME NOT NULL ;


ALTER TABLE `crm__atividade_tipo`
ADD COLUMN `atividade_pai` INT(11) ZEROFILL NULL DEFAULT NULL AFTER `atividade_tipo_nome`,
ADD COLUMN `atividade_tipo` ENUM('Positiva','Negativa','Neutra') NOT NULL DEFAULT 'Positiva' AFTER `atividade_pai`,
ADD COLUMN `atividade_comentario_obrigatorio` ENUM('Sim','Não') NOT NULL DEFAULT 'Não' AFTER `atividade_tipo`,
ADD COLUMN `atividade_caracteriza_desistencia` ENUM('Sim','Não','Opcional') NOT NULL DEFAULT 'Não' AFTER `atividade_comentario_obrigatorio`,
ADD INDEX `fk_crm__atividade_tipo_crm__atividade_tipo1_idx` (`atividade_pai` ASC);

ALTER TABLE `crm__atividade_tipo`
ADD CONSTRAINT `fk_crm__atividade_tipo_crm__atividade_tipo1`
  FOREIGN KEY (`atividade_pai`)
  REFERENCES `crm__atividade_tipo` (`atividade_tipo_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `crm__lead` DROP FOREIGN KEY `fk_crm__lead_pessoa1`;
ALTER TABLE `crm__lead` DROP COLUMN `pes_id`;
ALTER TABLE `crm__lead` CHANGE COLUMN `lead_id` `pes_id` INT(10) ZEROFILL UNSIGNED NOT NULL ;
ALTER TABLE `crm__lead` DROP INDEX `fk_crm__lead_pessoa1_idx` ;

ALTER TABLE `crm__lead` 
ADD CONSTRAINT `fk_crm__lead_pessoa1`
  FOREIGN KEY (`pes_id`)
  REFERENCES `pessoa` (`pes_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;


ALTER TABLE `crm__lead_atividade` DROP FOREIGN KEY fk_crm__lead_atividade_crm__atividade_retorno1;
ALTER TABLE `crm__lead_atividade` DROP COLUMN `retorno_id`;
DROP TABLE IF EXISTS crm__atividade_retorno;


ALTER TABLE `crm__lead_interesse` 
DROP FOREIGN KEY `fk_crm__lead_interesse_crm__lead1`,
DROP FOREIGN KEY `fk_crm__lead_interesse_crm__interesse1`;

ALTER TABLE `crm__lead_atividade` 
DROP FOREIGN KEY `fk_crm__lead_interesse_crm__lead10`;

ALTER TABLE `crm__lead` 
CHANGE COLUMN `lead_id` `pes_id` INT(10) ZEROFILL UNSIGNED NOT NULL ;

ALTER TABLE `crm__lead_interesse` 
CHANGE COLUMN `lead_id` `pes_id` INT(10) ZEROFILL UNSIGNED NOT NULL ,
ADD INDEX `fk_crm_lead_interesse_crm__lead1_idx` (`pes_id` ASC),
ADD INDEX `fk_crm_lead_interesse_crm_interesse1_idx` (`interesse_id` ASC),
DROP INDEX `fk_crm__lead_interesse_crm__interesse1_idx` ,
DROP INDEX `fk_crm__lead_interesse_crm__lead1_idx` ;

ALTER TABLE `crm__lead_atividade` 
CHANGE COLUMN `lead_id` `pes_id` INT(10) UNSIGNED ZEROFILL NOT NULL ;

ALTER TABLE `crm__lead` 
ADD CONSTRAINT `fk_crm__lead_pessoa1`
  FOREIGN KEY (`pes_id`)
  REFERENCES `pessoa` (`pes_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `crm__lead_interesse` 
ADD CONSTRAINT `fk_crm_lead_interesse_crm__lead1`
  FOREIGN KEY (`pes_id`)
  REFERENCES `crm__lead` (`pes_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_crm_lead_interesse_crm_interesse1`
  FOREIGN KEY (`interesse_id`)
  REFERENCES `crm__interesse` (`interesse_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `crm__lead_atividade` 
ADD CONSTRAINT `fk_crm__lead_interesse_crm__lead10`
  FOREIGN KEY (`pes_id`)
  REFERENCES `crm__lead` (`pes_id`)
  ON UPDATE CASCADE;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
