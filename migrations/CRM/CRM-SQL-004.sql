--
-- módulo sistema
--
DELETE FROM `acesso_privilegios_grupo` WHERE acesso_actions_id IN(
	select acesso_actions.id from acesso_funcionalidades
	inner join acesso_actions on funcionalidade=acesso_funcionalidades.id
	where func_controller IN(
		'Crm\\Controller\\CrmAtividadeRetorno'
	)
) AND acesso_actions_id>=0 AND acesso_grupo_id>=0;

DELETE FROM `acesso_actions` WHERE funcionalidade in(
	SELECT acesso_funcionalidades.id from acesso_funcionalidades
	where func_controller IN(
		'Crm\\Controller\\CrmAtividadeRetorno'
	)
) and id>0;

DELETE FROM acesso_funcionalidades where func_controller IN(
		'Crm\\Controller\\CrmAtividadeRetorno'
) and id>0;