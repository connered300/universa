SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `crm__operador`
ADD COLUMN `operador_data_ultimo_recebimento` DATETIME NULL AFTER `time_id`;

ALTER TABLE `crm__operador`
ADD COLUMN `operador_situacao` ENUM('Ativo', 'Inativo') NOT NULL DEFAULT 'Ativo' AFTER `operador_data_ultimo_recebimento`;

ALTER TABLE `crm__operador`
ADD COLUMN `operador_percentual` FLOAT NULL DEFAULT 100 AFTER `operador_situacao`;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
(
	select 'distribuir' as act,'Distribuir/Redistribuir' as descricao,'Leitura' as tipo,'N' as usr from dual
) as actions
where func_controller IN('Crm\\Controller\\CrmLead');

REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
acesso_grupo
where func_controller IN(
		'Crm\\Controller\\CrmLead'
)
and grup_nome LIKE 'Admin%';

SET FOREIGN_KEY_CHECKS = 0;

REPLACE INTO crm__atividade_tipo
(atividade_tipo_id, atividade_tipo_nome, atividade_pai, atividade_tipo, atividade_comentario_obrigatorio, atividade_caracteriza_desistencia)
VALUES
  (1, 'Venda', NULL, 'Positiva', 'Sim', 'Não'),
  (2, 'Está em Negociação', NULL, 'Positiva', 'Sim', 'Não'),
  (3, 'Retornar Depois', NULL, 'Neutra', 'Sim', 'Não'),
  (4, 'Sem interesse', NULL, 'Negativa', 'Sim', 'Opcional'),
  (5, 'Só chama/Caixa Postal', NULL, 'Negativa', 'Sim', 'Opcional'),
  (6, 'Pessoa Errada', NULL, 'Negativa', 'Sim', 'Opcional'),
  (7, 'Telefone não Existe', NULL, 'Negativa', 'Não', 'Sim'),
  (8, 'Fora do perfil', NULL, 'Negativa', 'Sim', 'Sim'),
  (9, 'Preço', 4, 'Negativa', 'Sim', 'Opcional'),
  (10, 'Não se cadastrou', 4, 'Negativa', 'Sim', 'Opcional'),
  (11, 'Fechou com a concorrência', 4, 'Negativa', 'Sim', 'Opcional'),
  (12, 'Sem interesse', 4, 'Negativa', 'Sim', 'Opcional'),
  (13, 'Forma de pagamento', 4, 'Negativa', 'Sim', 'Opcional'),
  (14, 'Outro', 4, 'Negativa', 'Sim', 'Opcional');
SET FOREIGN_KEY_CHECKS = 1;

ALTER TABLE `crm__lead_atividade` DROP COLUMN `atividade_situacao`;
ALTER TABLE `crm__lead_atividade` DROP COLUMN `atividate_agendamento_titulo`;
ALTER TABLE `crm__lead_atividade` DROP COLUMN `atividade_descricao`;
ALTER TABLE `crm__lead_atividade` DROP COLUMN `atividade_forma_contato`;
ALTER TABLE `crm__lead_atividade` CHANGE COLUMN `atividate_comentario` `atividade_comentario` TEXT NULL DEFAULT NULL ;
ALTER TABLE `crm__lead_atividade` CHANGE COLUMN `atividate_obs` `atividade_comentario` TEXT NULL DEFAULT NULL ;
ALTER TABLE `crm__lead_atividade` ADD COLUMN `atividade_forma_contato` ENUM('Email', 'Sms', 'Telefone', 'Whatsapp') NULL DEFAULT NULL AFTER `atividade_agendamento`;
ALTER TABLE `crm__lead_atividade` ADD COLUMN `atividade_forma_contato_agendamento` ENUM('Email', 'Sms', 'Telefone', 'Whatsapp') NULL DEFAULT NULL AFTER `atividade_agendamento`;
ALTER TABLE `crm__lead_atividade` ADD COLUMN `atividade_desistencia` ENUM('Sim', 'Não') NULL DEFAULT 'Não' AFTER `atividade_comentario`;

ALTER TABLE `crm__atividade_tipo`
ADD COLUMN `atividade_agendamento` ENUM('Opcional', 'Obrigatório', 'Indisponível') NOT NULL DEFAULT 'Obrigatório' AFTER `atividade_caracteriza_desistencia`;

CREATE or REPLACE VIEW view__crm_atividade_tipo AS
  SELECT
    atividade.atividade_tipo_id,
    atividade.atividade_pai,
    atividade.atividade_tipo,
    atividade.atividade_comentario_obrigatorio,
    atividade.atividade_caracteriza_desistencia,
    atividade.atividade_agendamento,
    if(atividade_pai.atividade_tipo_id,
       concat(
           atividade_pai.atividade_tipo_nome,
           ' / ',
           atividade.atividade_tipo_nome
       ),
       atividade.atividade_tipo_nome
    ) AS atividade_tipo_nome,
    atividade_pai.atividade_tipo_nome as atividade_pai_nome,
    (
      SELECT count(*)
      FROM crm__atividade_tipo atividade_filha
      WHERE atividade_filha.atividade_pai = atividade.atividade_tipo_id
    ) AS numero_atividades_flhas
  FROM crm__atividade_tipo atividade
    LEFT JOIN crm__atividade_tipo atividade_pai ON atividade_pai.atividade_tipo_id = atividade.atividade_pai
  ORDER BY
    IF(
        atividade.atividade_tipo = 'Positiva',
        0,
        IF(atividade.atividade_tipo = 'Neutra', 1, 2)
    ) ASC,
    atividade_tipo_nome ASC
;

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
(
	select 'atualizar' as act,'Atualizar Dados' as descricao,'Escrita' as tipo,'N' as usr from dual
) as actions
where func_controller IN('Crm\\Controller\\CrmLead');

REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
acesso_grupo
where func_controller IN(
		'Crm\\Controller\\CrmLead'
)
and grup_nome LIKE 'Admin%';

CREATE TABLE view__crm_lead AS
SELECT
    DISTINCT(lead.pes_id),
    lead.lead_data_alteracao,
    lead.lead_data_cadastro,
    lead.lead_status,
    pes_nome, pes_cpf,
    endereco.end_id, tipo_endereco, end_pais, end_estado, end_tipo_end, end_bairro,
    end_cidade, end_cep, end_logradouro, end_tipo_logradouro, end_numero, end_complemento,
    pessoa.con_contato_email,
    pessoa.con_contato_telefone,
    pessoa.con_contato_celular,
    fonte_nome,
    mediador_nome,
    campanha_nome,
    login,
    time_nome,
    concat(login, ' / ', time_nome) as operador_nome,
    atividade.atividade_tipo_id,
    atividade.atividade_tipo_nome,
    atividade.atividade_data_cadastro,
    atividade.atividade_agendamento
  FROM crm__lead AS lead
    LEFT JOIN pessoa ON pessoa.pes_id=lead.pes_id
    LEFT JOIN pessoa_fisica ON pessoa_fisica.pes_id=pessoa.pes_id
    LEFT JOIN endereco ON endereco.pes_id=pessoa.pes_id
    LEFT JOIN crm__fonte ON crm__fonte.fonte_id=lead.fonte_id
    LEFT JOIN crm__mediador ON crm__mediador.mediador_id=lead.mediador_id
    LEFT JOIN crm__campanha ON crm__campanha.campanha_id=lead.campanha_id
    LEFT JOIN crm__operador ON crm__operador.operador_id=lead.operador_id
    LEFT JOIN acesso_pessoas ON acesso_pessoas.id=crm__operador.operador_id
    LEFT JOIN crm__time ON crm__time.time_id=crm__operador.time_id
    LEFT JOIN (
      SELECT * FROM (
                      SELECT
                        crm__lead_atividade.*,
                        atividade_tipo_nome,
                        @c := @c + 1 AS contador
                      FROM crm__lead_atividade
                        INNER JOIN view__crm_atividade_tipo
                          ON view__crm_atividade_tipo.atividade_tipo_id = crm__lead_atividade.atividade_tipo_id
                        JOIN (SELECT @c := 0) r
                      ORDER BY atividade_data_cadastro DESC
                    ) as ultima_atividade GROUP by pes_id
    ) as atividade  ON atividade.pes_id=lead.pes_id;



REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
(
	select 'converter' as act,'Conversao de Lead' as descricao,'Escrita' as tipo,'N' as usr from dual
) as actions
where func_controller IN('Crm\\Controller\\CrmLead');

REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
acesso_grupo
where func_controller IN(
		'Crm\\Controller\\CrmLead'
)
and grup_nome LIKE 'Admin%';


ALTER TABLE `crm__mediador`
ADD COLUMN `mediador_email` TEXT NULL DEFAULT NULL;