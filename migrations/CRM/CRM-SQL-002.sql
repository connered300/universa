--
-- módulo sistema
--
DELETE FROM `acesso_privilegios_grupo` WHERE acesso_actions_id IN(
	select acesso_actions.id from acesso_funcionalidades
	inner join acesso_actions on funcionalidade=acesso_funcionalidades.id
	where func_controller IN(
		'Crm\\Controller\\CrmMediador',
		'Crm\\Controller\\CrmCampanha',
		'Crm\\Controller\\CrmFonte',
		'Crm\\Controller\\CrmInteresse',
		'Crm\\Controller\\CrmAtividadeTipo',
		'Crm\\Controller\\CrmAtividadeRetorno',
		'Crm\\Controller\\CrmOperador',
		'Crm\\Controller\\CrmTime',
		'Crm\\Controller\\CrmLead',
		'Crm\\Controller\\CrmLeadAtividade'
	)
) AND acesso_actions_id>=0 AND acesso_grupo_id>=0;

DELETE FROM `acesso_actions` WHERE funcionalidade in(
	SELECT acesso_funcionalidades.id from acesso_funcionalidades
	where func_controller IN(
		'Crm\\Controller\\CrmMediador',
		'Crm\\Controller\\CrmCampanha',
		'Crm\\Controller\\CrmFonte',
		'Crm\\Controller\\CrmInteresse',
		'Crm\\Controller\\CrmAtividadeTipo',
		'Crm\\Controller\\CrmAtividadeRetorno',
		'Crm\\Controller\\CrmOperador',
		'Crm\\Controller\\CrmTime',
		'Crm\\Controller\\CrmLead',
		'Crm\\Controller\\CrmLeadAtividade'
	)
) and id>0;

DELETE FROM acesso_funcionalidades where func_controller IN(
		'Crm\\Controller\\CrmMediador',
		'Crm\\Controller\\CrmCampanha',
		'Crm\\Controller\\CrmFonte',
		'Crm\\Controller\\CrmInteresse',
		'Crm\\Controller\\CrmAtividadeTipo',
		'Crm\\Controller\\CrmAtividadeRetorno',
		'Crm\\Controller\\CrmOperador',
		'Crm\\Controller\\CrmTime',
		'Crm\\Controller\\CrmLead',
		'Crm\\Controller\\CrmLeadAtividade'
) and id>0;

DELETE FROM acesso_modulos where mod_route='crm/default' and id>0;

DELETE FROM acesso_sistemas where sis_nome='CRM' and id>0;

REPLACE INTO `acesso_sistemas` (`sis_nome`, `sis_status`, `sis_icon`, `sis_info`)
                       VALUES ('CRM', 'Ativo', 'fa-group', 'CRM');

REPLACE INTO `acesso_modulos` (`sistema`, `mod_nome`, `mod_route`, `mod_status`, `mod_icon`, `mod_info`, `mod_redireciona`, `mod_prioridade`)
SELECT acesso_sistemas.id, 'CRM', 'crm/default', 'Ativo', 'fa fa-group', 'Modúlo CRM', '/crm', round(acesso_sistemas.id/10) from acesso_sistemas
where sis_nome like 'CRM';

REPLACE INTO `acesso_funcionalidades` (modulo, func_nome, func_controller, func_status, func_visivel)
SELECT distinct acesso_modulos.id, func.descricao, func.controller,'Ativa','Sim'  from acesso_modulos,
(
	select 'Gestão de Mediadores' as descricao, 'Crm\\Controller\\CrmMediador' as controller from dual union all
	select 'Gestão de Campanhas' as descricao, 'Crm\\Controller\\CrmCampanha' as controller from dual union all
	select 'Gestão de Fontes' as descricao, 'Crm\\Controller\\CrmFonte' as controller from dual union all
	select 'Gestão de Interesses' as descricao, 'Crm\\Controller\\CrmInteresse' as controller from dual union all
	select 'Gestão de Tipos de Atividade' as descricao, 'Crm\\Controller\\CrmAtividadeTipo' as controller from dual union all
	select 'Gestão de Retornos de Atividade' as descricao, 'Crm\\Controller\\CrmAtividadeRetorno' as controller from dual union all
	select 'Gestão de Operadores' as descricao, 'Crm\\Controller\\CrmOperador' as controller from dual union all
	select 'Gestão de Times' as descricao, 'Crm\\Controller\\CrmTime' as controller from dual union all
	select 'Gestão de Leads' as descricao, 'Crm\\Controller\\CrmLead' as controller from dual union all
	select 'Gestão de Atividades de Leads' as descricao, 'Crm\\Controller\\CrmLeadAtividade' as controller from dual
) as func
where mod_nome like 'CRM';

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
(
	select 'index' as act,'Listagem' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'remove' as act,'Remover' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'add' as act,'Criar' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'edit' as act,'Editar' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'search-for-json' as act, 'Autocomplete' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'search' as act,'Datatables' as descricao,'Leitura' as tipo,'N' as usr from dual
) as actions
where func_controller IN(
		'Crm\\Controller\\CrmMediador',
		'Crm\\Controller\\CrmCampanha',
		'Crm\\Controller\\CrmFonte',
		'Crm\\Controller\\CrmInteresse',
		'Crm\\Controller\\CrmAtividadeTipo',
		'Crm\\Controller\\CrmAtividadeRetorno',
		'Crm\\Controller\\CrmOperador',
		'Crm\\Controller\\CrmTime',
		'Crm\\Controller\\CrmLead',
		'Crm\\Controller\\CrmLeadAtividade'
);
REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
acesso_grupo
where func_controller IN(
		'Crm\\Controller\\CrmMediador',
		'Crm\\Controller\\CrmCampanha',
		'Crm\\Controller\\CrmFonte',
		'Crm\\Controller\\CrmInteresse',
		'Crm\\Controller\\CrmAtividadeTipo',
		'Crm\\Controller\\CrmAtividadeRetorno',
		'Crm\\Controller\\CrmOperador',
		'Crm\\Controller\\CrmTime',
		'Crm\\Controller\\CrmLead',
		'Crm\\Controller\\CrmLeadAtividade'
)
and grup_nome LIKE 'Admin%';