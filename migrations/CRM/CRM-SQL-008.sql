ALTER TABLE `acesso_pessoas` ADD COLUMN `data_ultimo_login` DATETIME NULL
AFTER `email`;

ALTER TABLE `crm__incidencia_tipo`
DROP FOREIGN KEY `fk_crm__incidencia_tipo_crm__incidencia_tipo1`;
ALTER TABLE `crm__lead_atividade`
DROP FOREIGN KEY `fk_crm__lead_atividade_crm__atividade_tipo1`;


ALTER TABLE `crm__incidencia_tipo`
CHANGE COLUMN `incidencia_tipo_nome` `atividade_tipo_nome` VARCHAR(255) NULL,
CHANGE COLUMN `incidencia_pai` `atividade_pai` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL,
CHANGE COLUMN `incidencia_tipo` `atividade_tipo` ENUM('Positiva', 'Negativa', 'Neutra') NOT NULL DEFAULT 'Positiva',
CHANGE COLUMN `incidencia_comentario_obrigatorio` `atividade_comentario_obrigatorio` ENUM('Sim', 'N�o') NOT NULL DEFAULT 'N�o',
CHANGE COLUMN `incidencia_caracteriza_desistencia` `atividade_caracteriza_desistencia` ENUM('Sim', 'N�o', 'Opcional') NOT NULL DEFAULT 'N�o',
CHANGE COLUMN `incidencia_agendamento` `atividade_agendamento` ENUM('Opcional', 'Obrigat�rio', 'Indispon�vel') NOT NULL DEFAULT 'Obrigat�rio',
RENAME TO `crm__atividade_tipo`;

ALTER TABLE `crm__atividade_tipo` ADD COLUMN `atividade_tipo_id` INT(11) UNSIGNED ZEROFILL NOT NULL;

UPDATE crm__atividade_tipo
SET atividade_tipo_id = incidencia_tipo_id
WHERE incidencia_tipo_id >= 0;

ALTER TABLE `crm__atividade_tipo`
CHANGE COLUMN `atividade_tipo_id` `atividade_tipo_id` INT(11) UNSIGNED ZEROFILL NOT NULL
AFTER `incidencia_tipo_id`,
CHANGE COLUMN `incidencia_tipo_id` `incidencia_tipo_id` INT(11) UNSIGNED ZEROFILL NOT NULL,
DROP PRIMARY KEY;

ALTER TABLE `crm__atividade_tipo`
DROP COLUMN `incidencia_tipo_id`,
CHANGE COLUMN `atividade_tipo_id` `atividade_tipo_id` INT(11) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
ADD PRIMARY KEY (`atividade_tipo_id`);

ALTER TABLE `crm__lead_atividade`
CHANGE COLUMN `incidencia_tipo_id` `atividade_tipo_id` INT(11) UNSIGNED ZEROFILL NOT NULL;

ALTER TABLE `crm__atividade_tipo`
ADD CONSTRAINT `fk_crm__atividade_tipo_crm__atividade_tipo1`
FOREIGN KEY (`atividade_pai`)
REFERENCES `crm__atividade_tipo` (`atividade_tipo_id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `crm__lead_atividade`
ADD CONSTRAINT `fk_crm__lead_atividade_crm__atividade_tipo1`
FOREIGN KEY (`atividade_tipo_id`)
REFERENCES `crm__atividade_tipo` (`atividade_tipo_id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

DROP TABLE IF EXISTS view__crm_incidencia_tipo;
DROP VIEW IF EXISTS view__crm_incidencia_tipo;

CREATE OR REPLACE VIEW view__crm_atividade_tipo AS
  SELECT
    atividade.atividade_tipo_id,
    atividade.atividade_pai,
    atividade.atividade_tipo,
    atividade.atividade_comentario_obrigatorio,
    atividade.atividade_caracteriza_desistencia,
    atividade.atividade_agendamento,
    if(
        atividade_pai.atividade_tipo_id,
        concat(
            atividade_pai.atividade_tipo_nome,
            ' / ',
            atividade.atividade_tipo_nome
        ),
        atividade.atividade_tipo_nome
    )
      AS atividade_tipo_nome,
    atividade_pai.atividade_tipo_nome
      AS atividade_pai_nome,
    (
      SELECT count(*)
      FROM
        crm__atividade_tipo atividade_filha
      WHERE
        atividade_filha.atividade_pai
        =
        atividade.atividade_tipo_id
    )
      AS numero_atividades_flhas
  FROM crm__atividade_tipo atividade
    LEFT JOIN crm__atividade_tipo atividade_pai ON atividade_pai.atividade_tipo_id = atividade.atividade_pai
  ORDER BY
    IF(
        atividade.atividade_tipo = 'Positiva',
        0,
        IF(atividade.atividade_tipo = 'Neutra', 1, 2)
    ) ASC,
    atividade_tipo_nome ASC;


DELETE FROM `acesso_privilegios_grupo`
WHERE acesso_actions_id IN (
  SELECT acesso_actions.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
  WHERE func_controller LIKE 'Crm%Controller%'
) AND acesso_actions_id >= 0 AND acesso_grupo_id >= 0;

DELETE FROM `acesso_actions`
WHERE funcionalidade IN (
  SELECT acesso_funcionalidades.id
  FROM acesso_funcionalidades
  WHERE func_controller LIKE 'Crm%Controller%'
) AND id > 0;

DELETE FROM acesso_funcionalidades
WHERE func_controller LIKE 'Crm%Controller%' AND id > 0;

DELETE FROM acesso_modulos
WHERE mod_route = 'crm/default' AND id > 0;

DELETE FROM acesso_sistemas
WHERE sis_nome = 'CRM' AND id > 0;

REPLACE INTO `acesso_sistemas` (`sis_nome`, `sis_status`, `sis_icon`, `sis_info`)
VALUES ('CRM', 'Ativo', 'fa-group', 'CRM');

REPLACE INTO `acesso_modulos` (`sistema`, `mod_nome`, `mod_route`, `mod_status`, `mod_icon`, `mod_info`, `mod_redireciona`, `mod_prioridade`)
  SELECT
    acesso_sistemas.id,
    'CRM',
    'crm/default',
    'Ativo',
    'fa fa-group',
    'Mod�lo CRM',
    '/crm',
    round(acesso_sistemas.id / 10)
  FROM acesso_sistemas
  WHERE sis_nome LIKE 'CRM';

REPLACE INTO `acesso_funcionalidades` (modulo, func_nome, func_controller, func_status, func_visivel)
  SELECT DISTINCT
    acesso_modulos.id,
    func.descricao,
    func.controller,
    'Ativa',
    'Sim'
  FROM acesso_modulos,
    (
      SELECT
        'Gest�o de Mediadores'         AS descricao,
        'Crm\\Controller\\CrmMediador' AS controller
      FROM dual
      UNION ALL
      SELECT
        'Gest�o de Campanhas'          AS descricao,
        'Crm\\Controller\\CrmCampanha' AS controller
      FROM dual
      UNION ALL
      SELECT
        'Gest�o de Fontes'          AS descricao,
        'Crm\\Controller\\CrmFonte' AS controller
      FROM dual
      UNION ALL
      SELECT
        'Gest�o de Interesses'          AS descricao,
        'Crm\\Controller\\CrmInteresse' AS controller
      FROM dual
      UNION ALL
      SELECT
        'Gest�o de Tipos de Atividade'      AS descricao,
        'Crm\\Controller\\CrmAtividadeTipo' AS controller
      FROM dual
      UNION ALL
      SELECT
        'Gest�o de Operadores'         AS descricao,
        'Crm\\Controller\\CrmOperador' AS controller
      FROM dual
      UNION ALL
      SELECT
        'Gest�o de Times'          AS descricao,
        'Crm\\Controller\\CrmTime' AS controller
      FROM dual
      UNION ALL
      SELECT
        'Gest�o de Leads'          AS descricao,
        'Crm\\Controller\\CrmLead' AS controller
      UNION ALL
      SELECT
        'Atendimento de Leads'                AS descricao,
        'Crm\\Controller\\CrmLeadAtendimento' AS controller
      FROM dual
      UNION ALL
      SELECT
        'Gest�o de Atividades de Leads'     AS descricao,
        'Crm\\Controller\\CrmLeadAtividade' AS controller
      FROM dual
    ) AS func
  WHERE mod_nome LIKE 'CRM';

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT DISTINCT
    acesso_funcionalidades.id,
    actions.act,
    actions.descricao,
    actions.tipo,
    actions.usr
  FROM acesso_funcionalidades,
    (
      SELECT
        'index'    AS act,
        'Listagem' AS descricao,
        'Leitura'  AS tipo,
        'N'        AS usr
      FROM dual
      UNION ALL
      SELECT
        'remove'  AS act,
        'Remover' AS descricao,
        'Leitura' AS tipo,
        'N'       AS usr
      FROM dual
      UNION ALL
      SELECT
        'add'     AS act,
        'Criar'   AS descricao,
        'Leitura' AS tipo,
        'N'       AS usr
      FROM dual
      UNION ALL
      SELECT
        'edit'    AS act,
        'Editar'  AS descricao,
        'Leitura' AS tipo,
        'N'       AS usr
      FROM dual
      UNION ALL
      SELECT
        'search-for-json' AS act,
        'Autocomplete'    AS descricao,
        'Leitura'         AS tipo,
        'N'               AS usr
      FROM dual
      UNION ALL
      SELECT
        'search'     AS act,
        'Datatables' AS descricao,
        'Leitura'    AS tipo,
        'N'          AS usr
      FROM dual
    ) AS actions
  WHERE func_controller IN (
    'Crm\\Controller\\CrmMediador',
    'Crm\\Controller\\CrmCampanha',
    'Crm\\Controller\\CrmFonte',
    'Crm\\Controller\\CrmInteresse',
    'Crm\\Controller\\CrmAtividadeTipo',
    'Crm\\Controller\\CrmOperador',
    'Crm\\Controller\\CrmTime',
    'Crm\\Controller\\CrmLead',
    'Crm\\Controller\\CrmLeadAtendimento',
    'Crm\\Controller\\CrmLeadAtividade'
  );
REPLACE INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
    ,
    acesso_grupo
  WHERE func_controller IN (
    'Crm\\Controller\\CrmMediador',
    'Crm\\Controller\\CrmCampanha',
    'Crm\\Controller\\CrmFonte',
    'Crm\\Controller\\CrmInteresse',
    'Crm\\Controller\\CrmAtividadeTipo',
    'Crm\\Controller\\CrmOperador',
    'Crm\\Controller\\CrmTime',
    'Crm\\Controller\\CrmLead',
    'Crm\\Controller\\CrmLeadAtendimento',
    'Crm\\Controller\\CrmLeadAtividade'
  )
        AND grup_nome LIKE 'Admin%';

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT DISTINCT
    acesso_funcionalidades.id,
    actions.act,
    actions.descricao,
    actions.tipo,
    actions.usr
  FROM acesso_funcionalidades,
    (
      SELECT
        'distribuir'              AS act,
        'Distribuir/Redistribuir' AS descricao,
        'Leitura'                 AS tipo,
        'N'                       AS usr
      FROM dual
    ) AS actions
  WHERE func_controller IN ('Crm\\Controller\\CrmLead',
                            'Crm\\Controller\\CrmLeadAtendimento');

REPLACE INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
    ,
    acesso_grupo
  WHERE func_controller IN (
    'Crm\\Controller\\CrmLead',
    'Crm\\Controller\\CrmLeadAtendimento'
  )
        AND grup_nome LIKE 'Admin%';


REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT DISTINCT
    acesso_funcionalidades.id,
    actions.act,
    actions.descricao,
    actions.tipo,
    actions.usr
  FROM acesso_funcionalidades,
    (
      SELECT
        'atualizar'       AS act,
        'Atualizar Dados' AS descricao,
        'Escrita'         AS tipo,
        'N'               AS usr
      FROM dual
    ) AS actions
  WHERE func_controller IN ('Crm\\Controller\\CrmLead',
                            'Crm\\Controller\\CrmLeadAtendimento');

REPLACE INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
    ,
    acesso_grupo
  WHERE func_controller IN (
    'Crm\\Controller\\CrmLead',
    'Crm\\Controller\\CrmLeadAtendimento'
  )
        AND grup_nome LIKE 'Admin%';


REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT DISTINCT
    acesso_funcionalidades.id,
    actions.act,
    actions.descricao,
    actions.tipo,
    actions.usr
  FROM acesso_funcionalidades,
    (
      SELECT
        'converter'         AS act,
        'Conversao de Lead' AS descricao,
        'Escrita'           AS tipo,
        'N'                 AS usr
      FROM dual
    ) AS actions
  WHERE func_controller IN ('Crm\\Controller\\CrmLead',
                            'Crm\\Controller\\CrmLeadAtendimento');

REPLACE INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
    ,
    acesso_grupo
  WHERE func_controller IN (
    'Crm\\Controller\\CrmLead',
    'Crm\\Controller\\CrmLeadAtendimento'
  )
        AND grup_nome LIKE 'Admin%';

ALTER TABLE `crm__atividade_tipo`
ADD COLUMN `atividade_lead_status` ENUM('Novo', 'Atribu�do', 'Em processo', 'Convertido', 'Lixo') NULL

AFTER `atividade_agendamento`;
ALTER TABLE `crm__lead`
CHANGE COLUMN `lead_status` `lead_status` ENUM('Novo', 'Atribu�do', 'Em processo', 'Convertido', 'Lixo') NOT NULL DEFAULT 'Novo';

ALTER TABLE `crm__operador`
ADD COLUMN `operador_entrada1` TIME NULL
AFTER `operador_percentual`,
ADD COLUMN `operador_saida1` TIME NULL
AFTER `operador_entrada1`,
ADD COLUMN `operador_entrada2` TIME NULL
AFTER `operador_saida1`,
ADD COLUMN `operador_saida2` TIME NULL
AFTER `operador_entrada2`;



