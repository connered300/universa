-- MySQL Workbench Synchronization
-- Generated: 2017-04-09 16:33
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE IF NOT EXISTS `crm__fonte` (
  `fonte_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `fonte_nome` VARCHAR(70) NULL DEFAULT NULL,
  PRIMARY KEY (`fonte_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `crm__lead` (
  `lead_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `pes_id` INT(11) UNSIGNED ZEROFILL NOT NULL,
  `fonte_id` INT(10) ZEROFILL UNSIGNED NULL DEFAULT NULL,
  `mediador_id` INT(10) ZEROFILL UNSIGNED NULL DEFAULT NULL,
  `campanha_id` INT(10) ZEROFILL UNSIGNED NULL DEFAULT NULL,
  `operador_id` INT(10) ZEROFILL UNSIGNED NULL DEFAULT NULL,
  `lead_obs` TEXT NULL DEFAULT NULL,
  `lead_data_cadastro` DATETIME NOT NULL,
  `lead_data_alteracao` DATETIME NOT NULL,
  `lead_status` ENUM('Novo', 'Atribuído', 'Em processo', 'Convertido', 'Reciclado', 'Morto') NOT NULL DEFAULT 'Novo',
  PRIMARY KEY (`lead_id`),
  INDEX `fk_crm__lead_crm__fonte1_idx` (`fonte_id` ASC),
  INDEX `fk_crm__lead_crm__campanha1_idx` (`campanha_id` ASC),
  INDEX `fk_crm__lead_crm__operador1_idx` (`operador_id` ASC),
  INDEX `fk_crm__lead_crm__mediador1_idx` (`mediador_id` ASC),
  INDEX `fk_crm__lead_pessoa1_idx` (`pes_id` ASC),
  CONSTRAINT `fk_crm__lead_crm__fonte1`
    FOREIGN KEY (`fonte_id`)
    REFERENCES `crm__fonte` (`fonte_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_crm__lead_crm__campanha1`
    FOREIGN KEY (`campanha_id`)
    REFERENCES `crm__campanha` (`campanha_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_crm__lead_crm__operador1`
    FOREIGN KEY (`operador_id`)
    REFERENCES `crm__operador` (`operador_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_crm__lead_crm__mediador1`
    FOREIGN KEY (`mediador_id`)
    REFERENCES `crm__mediador` (`mediador_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_crm__lead_pessoa1`
    FOREIGN KEY (`pes_id`)
    REFERENCES `pessoa` (`pes_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `crm__campanha` (
  `campanha_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `campanha_nome` VARCHAR(70) NULL DEFAULT NULL,
  PRIMARY KEY (`campanha_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `crm__operador` (
  `operador_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `operador_nome` VARCHAR(70) NULL DEFAULT NULL,
  `operador_situacao` ENUM('Ativo', 'Inativo') NULL DEFAULT NULL,
  `operador_online` ENUM('Sim','Não') NULL DEFAULT NULL,
  `operarador_ultimo_login` DATETIME NULL DEFAULT NULL,
  `time_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  PRIMARY KEY (`operador_id`),
  INDEX `fk_crm__operador_crm__time1_idx` (`time_id` ASC),
  CONSTRAINT `fk_crm__operador_crm__time1`
    FOREIGN KEY (`time_id`)
    REFERENCES `crm__time` (`time_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `crm__time` (
  `time_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `time_nome` VARCHAR(70) NULL DEFAULT NULL,
  `time_localizacao` VARCHAR(200) NULL DEFAULT NULL,
  `time_situacao` ENUM('Ativo', 'Inativo') NULL DEFAULT NULL,
  PRIMARY KEY (`time_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `crm__mediador` (
  `mediador_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `mediador_nome` VARCHAR(70) NULL DEFAULT NULL,
  PRIMARY KEY (`mediador_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `crm__lead_interesse` (
  `lead_interesse_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `lead_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  `interesse_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  PRIMARY KEY (`lead_interesse_id`),
  INDEX `fk_crm__lead_interesse_crm__lead1_idx` (`lead_id` ASC),
  INDEX `fk_crm__lead_interesse_crm__interesse1_idx` (`interesse_id` ASC),
  CONSTRAINT `fk_crm__lead_interesse_crm__lead1`
    FOREIGN KEY (`lead_id`)
    REFERENCES `crm__lead` (`lead_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_crm__lead_interesse_crm__interesse1`
    FOREIGN KEY (`interesse_id`)
    REFERENCES `crm__interesse` (`interesse_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `crm__lead_atividade` (
  `atividade_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `lead_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  `interesse_data_cadastro` DATETIME NOT NULL,
  `operador_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  `atividade_tipo_id` INT(11) ZEROFILL NOT NULL,
  `retorno_id` INT(11) ZEROFILL UNSIGNED NULL DEFAULT NULL,
  `atividade_forma_contato` ENUM('Email','Sms','Telefone', 'Whatsapp','Outro') NULL DEFAULT NULL,
  `atividade_descricao` VARCHAR(200) NOT NULL,
  `atividade_agendamento` DATETIME NULL DEFAULT NULL,
  `atividate_agendamento_titulo` VARCHAR(200) NULL DEFAULT NULL,
  `atividate_obs` TEXT NULL DEFAULT NULL,
  `atividade_situacao` ENUM('Finalizada','Agendada','Cancelada') NOT NULL DEFAULT 'Finalizada',
  PRIMARY KEY (`atividade_id`),
  INDEX `fk_crm__lead_interesse_crm__lead1_idx` (`lead_id` ASC),
  INDEX `fk_crm__lead_atividade_crm__operador1_idx` (`operador_id` ASC),
  INDEX `fk_crm__lead_atividade_crm__atividade_tipo1_idx` (`atividade_tipo_id` ASC),
  INDEX `fk_crm__lead_atividade_crm__atividade_retorno1_idx` (`retorno_id` ASC),
  CONSTRAINT `fk_crm__lead_interesse_crm__lead10`
    FOREIGN KEY (`lead_id`)
    REFERENCES `crm__lead` (`lead_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_crm__lead_atividade_crm__operador1`
    FOREIGN KEY (`operador_id`)
    REFERENCES `crm__operador` (`operador_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_crm__lead_atividade_crm__atividade_tipo1`
    FOREIGN KEY (`atividade_tipo_id`)
    REFERENCES `crm__atividade_tipo` (`atividade_tipo_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_crm__lead_atividade_crm__atividade_retorno1`
    FOREIGN KEY (`retorno_id`)
    REFERENCES `crm__atividade_retorno` (`retorno_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `crm__atividade_tipo` (
  `atividade_tipo_id` INT(11) ZEROFILL NOT NULL AUTO_INCREMENT,
  `atividade_tipo_nome` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`atividade_tipo_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `crm__interesse` (
  `interesse_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `interesse_descricao` VARCHAR(200) NOT NULL,
  `interesse_data_cadastro` DATETIME NOT NULL,
  PRIMARY KEY (`interesse_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `crm__atividade_retorno` (
  `retorno_id` INT(11) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `atividade_tipo_id` INT(11) ZEROFILL NOT NULL,
  `retorno_descricao` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`retorno_id`),
  INDEX `fk_crm__atividade_tipo_extra_crm__atividade_tipo1_idx` (`atividade_tipo_id` ASC),
  CONSTRAINT `fk_crm__atividade_tipo_extra_crm__atividade_tipo1`
    FOREIGN KEY (`atividade_tipo_id`)
    REFERENCES `crm__atividade_tipo` (`atividade_tipo_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
