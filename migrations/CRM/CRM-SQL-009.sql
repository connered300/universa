ALTER TABLE `crm__lead_atividade`
CHANGE COLUMN `atividade_forma_contato` `atividade_forma_contato`
ENUM('Email', 'Sms', 'Telefone', 'Whatsapp', 'Outro')
NOT NULL DEFAULT 'Outro';
ALTER TABLE `crm__lead_atividade`
CHANGE COLUMN `atividade_forma_contato_agendamento` `atividade_forma_contato_agendamento`
ENUM('Email', 'Sms', 'Telefone', 'Whatsapp', 'Outro')
NOT NULL DEFAULT 'Outro';