-- MySQL Workbench Synchronization
-- Generated: 2017-07-23 14:20
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Leonardo Weslei Diniz

SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE IF NOT EXISTS `crm__lead_evento` (
    `evento_id`               INT(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
    `evento_data`             DATETIME                  NOT NULL,
    `pes_id`                  INT(10) UNSIGNED ZEROFILL NOT NULL,
    `mediador_id`             INT(10) UNSIGNED ZEROFILL NULL     DEFAULT NULL,
    `campanha_id`             INT(10) UNSIGNED ZEROFILL NULL     DEFAULT NULL,
    `fonte_id`                INT(10) UNSIGNED ZEROFILL NULL     DEFAULT NULL,
    `evento_outra_informacao` TEXT                      NULL     DEFAULT NULL,
    PRIMARY KEY (`evento_id`),
    INDEX `fk_crm__lead_evento_crm__lead1_idx` (`pes_id` ASC),
    INDEX `fk_crm__lead_evento_crm__mediador1_idx` (`mediador_id` ASC),
    INDEX `fk_crm__lead_evento_crm__campanha1_idx` (`campanha_id` ASC),
    INDEX `fk_crm__lead_evento_crm__fonte1_idx` (`fonte_id` ASC),
    CONSTRAINT `fk_crm__lead_evento_crm__lead1`
    FOREIGN KEY (`pes_id`)
    REFERENCES `crm__lead` (`pes_id`)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
    CONSTRAINT `fk_crm__lead_evento_crm__mediador1`
    FOREIGN KEY (`mediador_id`)
    REFERENCES `crm__mediador` (`mediador_id`)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
    CONSTRAINT `fk_crm__lead_evento_crm__campanha1`
    FOREIGN KEY (`campanha_id`)
    REFERENCES `crm__campanha` (`campanha_id`)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
    CONSTRAINT `fk_crm__lead_evento_crm__fonte1`
    FOREIGN KEY (`fonte_id`)
    REFERENCES `crm__fonte` (`fonte_id`)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = latin1;


SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;
