-- MySQL Workbench Synchronization
-- Generated: 2017-02-16 16:30
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `financeiro__cheque` 
DROP FOREIGN KEY `fk_financeiro__cheque_financeiro__pagamento1`;

ALTER TABLE `financeiro__aluno_config_pgto_curso` 
DROP FOREIGN KEY `fk_financeiro__aluno_config_pgto_curso_financeiro__meio_pagam1`;

ALTER TABLE `financeiro__cheque` 
ADD COLUMN `cheque_valor_disponivel` FLOAT(10,2) NOT NULL DEFAULT 0 AFTER `cheque_conta`;

ALTER TABLE `financeiro__aluno_config_pgto_curso` 
ADD COLUMN `valores_id` INT(11) NULL DEFAULT NULL AFTER `tipotitulo_id`,
ADD COLUMN `usuario_alteracao` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL AFTER `meio_pagamento_id`,
ADD COLUMN `usuario_supervisor` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL AFTER `usuario_alteracao`,
ADD COLUMN `config_pgto_curso_efetivado` ENUM('Sim','Não') NULL DEFAULT 'Não' AFTER `config_pgto_curso_vencimento`,
ADD COLUMN `config_pgto_curso_alterado` ENUM('Sim','Não') NULL DEFAULT 'Não' AFTER `config_pgto_curso_efetivado`,
ADD INDEX `fk_financeiro__aluno_config_pgto_curso_financeiro__valores1_idx` (`valores_id` ASC),
ADD INDEX `fk_financeiro__aluno_config_pgto_curso_acesso_pessoas1_idx` (`usuario_alteracao` ASC),
ADD INDEX `fk_financeiro__aluno_config_pgto_curso_acesso_pessoas2_idx` (`usuario_supervisor` ASC);

ALTER TABLE `financeiro__meio_pagamento` 
ADD COLUMN `meio_pagamento_uso_multiplo` ENUM('Sim','Não') NULL DEFAULT 'Não' AFTER `meio_pagamento_status`;

ALTER TABLE `financeiro__cheque` 
ADD CONSTRAINT `fk_financeiro__cheque_financeiro__pagamento1`
  FOREIGN KEY (`pagamento_id`)
  REFERENCES `financeiro__pagamento` (`pagamento_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `financeiro__aluno_config_pgto_curso` 
ADD CONSTRAINT `fk_financeiro__aluno_config_pgto_curso_financeiro__meio_pagam1`
  FOREIGN KEY (`meio_pagamento_id`)
  REFERENCES `financeiro__meio_pagamento` (`meio_pagamento_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_financeiro__aluno_config_pgto_curso_financeiro__valores1`
  FOREIGN KEY (`valores_id`)
  REFERENCES `financeiro__valores` (`valores_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_financeiro__aluno_config_pgto_curso_acesso_pessoas1`
  FOREIGN KEY (`usuario_alteracao`)
  REFERENCES `acesso_pessoas` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_financeiro__aluno_config_pgto_curso_acesso_pessoas2`
  FOREIGN KEY (`usuario_supervisor`)
  REFERENCES `acesso_pessoas` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `financeiro__cheque`
DROP FOREIGN KEY `fk_financeiro__cheque_financeiro__pagamento1`;

ALTER TABLE `financeiro__titulo_cheque`
DROP FOREIGN KEY `fk_financeiro__titulo_financeiro__cheque_financeiro__titulo1`;

ALTER TABLE `financeiro__cheque`
DROP COLUMN `cheque_valor_disponivel`,
DROP COLUMN `pagamento_id`,
ADD COLUMN `usuario_id` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL AFTER `cheque_id`,
ADD COLUMN `cheque_data_cadastro` DATETIME NULL DEFAULT NULL AFTER `usuario_id`,
ADD INDEX `fk_financeiro__cheque_acesso_pessoas1_idx` (`usuario_id` ASC),
DROP INDEX `fk_financeiro__cheque_financeiro__pagamento1_idx` ;

ALTER TABLE `financeiro__titulo_cheque`
CHANGE COLUMN `titulo_id` `titulo_id` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL ,
ADD COLUMN `pagamento_id` INT(11) NULL DEFAULT NULL AFTER `titulo_id`,
ADD INDEX `fk_financeiro__titulo_cheque_financeiro__pagamento1_idx` (`pagamento_id` ASC);

ALTER TABLE `financeiro__cheque`
ADD CONSTRAINT `fk_financeiro__cheque_acesso_pessoas1`
FOREIGN KEY (`usuario_id`)
REFERENCES `acesso_pessoas` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `financeiro__titulo_cheque`
ADD CONSTRAINT `fk_financeiro__titulo_financeiro__cheque_financeiro__titulo1`
FOREIGN KEY (`titulo_id`)
REFERENCES `financeiro__titulo` (`titulo_id`)
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_financeiro__titulo_cheque_financeiro__pagamento1`
FOREIGN KEY (`pagamento_id`)
REFERENCES `financeiro__pagamento` (`pagamento_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
