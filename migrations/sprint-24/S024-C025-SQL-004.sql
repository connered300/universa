-- MySQL Workbench Synchronization
-- Generated: 2017-01-17 09:18
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `acad_curso`
CHANGE COLUMN `curso_nome` `curso_nome` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NOT NULL COMMENT 'Nome do curso.' ,
ADD COLUMN `curso_carga_horaria_pratica` INT(11) NULL DEFAULT NULL AFTER `curso_carga_horaria`,
ADD COLUMN `curso_carga_horaria_teorica` INT(11) NULL DEFAULT NULL AFTER `curso_carga_horaria_pratica`;

ALTER TABLE `boleto_conf_conta` 
ADD COLUMN `confcont_instrucoes` TEXT NULL DEFAULT NULL AFTER `confcont_contrato`,
ADD COLUMN `confcont_nosso_numero_inicial` INT(11) NOT NULL DEFAULT 1 AFTER `confcont_instrucoes`;

ALTER TABLE `acad_curso_config` 
CHANGE COLUMN `per_ativ_id` `per_ativ_id` INT(10) UNSIGNED ZEROFILL NULL DEFAULT NULL COMMENT 'Periodo no qual a configuração passou a ser valida.' ,
ADD COLUMN `cursoconfig_data_inicio` DATE NULL DEFAULT NULL AFTER `per_desat_id`,
ADD COLUMN `cursoconfig_data_fim` DATE NULL DEFAULT NULL AFTER `cursoconfig_data_inicio`,
ADD COLUMN `cursoconfig_numero_maximo_pendencias` INT(11) NOT NULL DEFAULT 3 AFTER `cursoconfig_media_final_min`;

ALTER TABLE `financeiro__titulo` 
ADD COLUMN `alunocurso_id` INT(10) ZEROFILL UNSIGNED NULL DEFAULT NULL AFTER `usuario_autor`,
ADD INDEX `fk_financeiro__titulo_acadgeral__aluno_curso1_idx` (`alunocurso_id` ASC);

ALTER TABLE `sis__fila_integracao` 
ADD COLUMN `fila_chave` VARCHAR(32) NULL DEFAULT NULL AFTER `fila_data_finalizacao`;

ALTER TABLE `org__agente_educacional`
CHANGE COLUMN `agente_comissao` `agente_comissao_primeiro_nivel` FLOAT NOT NULL;
ALTER TABLE `org__agente_educacional`
ADD COLUMN `agente_comissao_demais_niveis` FLOAT NOT NULL AFTER `agente_comissao_primeiro_nivel`;

ALTER TABLE `org__agente_educacional`
ADD COLUMN `banc_id` INT(11) NULL DEFAULT NULL AFTER `agente_comissao_demais_niveis`,
ADD COLUMN `agente_conta_agencia` VARCHAR(20) NULL DEFAULT NULL AFTER `banc_id`,
ADD COLUMN `agente_conta_tipo` ENUM('Poupança','Corrente') NULL DEFAULT NULL AFTER `agente_conta_agencia`,
ADD COLUMN `agente_conta_numero` VARCHAR(20) NULL DEFAULT NULL AFTER `agente_conta_tipo`,
ADD COLUMN `agente_conta_titular` VARCHAR(100) NULL DEFAULT NULL AFTER `agente_conta_numero`,
ADD COLUMN `agente_observacao` TEXT NULL DEFAULT NULL AFTER `agente_conta_titular`,
ADD COLUMN `agente_data_cadastro` DATETIME NULL DEFAULT NULL AFTER `agente_situacao`,
ADD COLUMN `agente_data_atualizacao` DATETIME NULL DEFAULT NULL AFTER `agente_data_cadastro`,
ADD INDEX `fk_org__agente_educacional_boleto_banco1_idx` (`banc_id` ASC);

ALTER TABLE `financeiro__aluno_config_pgto_curso` 
ADD COLUMN `meio_pagamento_id` INT(10) ZEROFILL UNSIGNED NOT NULL AFTER `config_pgto_curso_vencimento`,
ADD INDEX `fk_financeiro__aluno_config_pgto_curso_financeiro__meio_pag_idx` (`meio_pagamento_id` ASC);

ALTER TABLE `financeiro__meio_pagamento` 
ADD COLUMN `meio_pagamento_uso_externo` ENUM('Sim', 'Não') NOT NULL DEFAULT 'Sim' AFTER `meio_pagamento_tipo`;

UPDATE `financeiro__meio_pagamento` SET meio_pagamento_uso_externo='Sim' WHERE meio_pagamento_id>0 and meio_pagamento_uso_externo IS  NULL;

ALTER TABLE `financeiro__meio_pagamento`
CHANGE COLUMN `meio_pagamento_uso_externo` `meio_pagamento_uso_externo` ENUM('Sim', 'Não') NOT NULL DEFAULT 'Sim' AFTER `meio_pagamento_tipo`;

CREATE TABLE IF NOT EXISTS `financeiro__configuracao_conta` (
  `configuracao_conta_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `tipotitulo_id` INT(11) NULL,
  `cursocampus_id` INT(10) ZEROFILL UNSIGNED NULL,
  `confcont_id` INT(11) NOT NULL,
  `configuracao_conta_data_inicial` DATE NULL DEFAULT NULL,
  `configuracao_conta_data_final` DATE NULL DEFAULT NULL,
  PRIMARY KEY (`configuracao_conta_id`),
  INDEX `fk_financeiro__configuracao_conta_financeiro__titulo_tipo1_idx` (`tipotitulo_id` ASC),
  INDEX `fk_financeiro__configuracao_conta_campus_curso1_idx` (`cursocampus_id` ASC),
  INDEX `fk_financeiro__configuracao_conta_boleto_conf_conta1_idx` (`confcont_id` ASC),
  CONSTRAINT `fk_financeiro__configuracao_conta_financeiro__titulo_tipo1`
    FOREIGN KEY (`tipotitulo_id`)
    REFERENCES `financeiro__titulo_tipo` (`tipotitulo_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_financeiro__configuracao_conta_campus_curso1`
    FOREIGN KEY (`cursocampus_id`)
    REFERENCES `campus_curso` (`cursocampus_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_financeiro__configuracao_conta_boleto_conf_conta1`
    FOREIGN KEY (`confcont_id`)
    REFERENCES `boleto_conf_conta` (`confcont_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

ALTER TABLE `financeiro__configuracao_conta`
CHANGE COLUMN `tipotitulo_id` `tipotitulo_id` INT(11) NULL,
CHANGE COLUMN `cursocampus_id` `cursocampus_id` INT(10) ZEROFILL UNSIGNED NULL;


ALTER TABLE `financeiro__titulo` ADD CONSTRAINT `fk_titulo_boleto_tipo1`
  FOREIGN KEY (`tipotitulo_id`)
  REFERENCES `financeiro__titulo_tipo` (`tipotitulo_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;
ALTER TABLE `financeiro__titulo` ADD CONSTRAINT `fk_financeiro__titulo_acesso_pessoas1`
  FOREIGN KEY (`usuario_autor`)
  REFERENCES `acesso_pessoas` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;
ALTER TABLE `financeiro__titulo` ADD CONSTRAINT `fk_financeiro__titulo_acesso_pessoas2`
  FOREIGN KEY (`usuario_baixa`)
  REFERENCES `acesso_pessoas` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;
ALTER TABLE `financeiro__titulo` ADD CONSTRAINT `fk_financeiro__titulo_acadgeral__aluno_curso1`
  FOREIGN KEY (`alunocurso_id`)
  REFERENCES `acadgeral__aluno_curso` (`alunocurso_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `org__agente_educacional` 
ADD CONSTRAINT `fk_org__agente_educacional_boleto_banco1`
  FOREIGN KEY (`banc_id`)
  REFERENCES `boleto_banco` (`banc_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `financeiro__aluno_config_pgto_curso` 
ADD CONSTRAINT `fk_financeiro__aluno_config_pgto_curso_financeiro__meio_pagam1`
  FOREIGN KEY (`meio_pagamento_id`)
  REFERENCES `financeiro__meio_pagamento` (`meio_pagamento_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;