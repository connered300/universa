SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `acadgeral__aluno`
ADD COLUMN `usuario_cadastro` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL,
ADD COLUMN `usuario_alteracao` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL,
ADD INDEX `fk_acadgeral__aluno_acesso_pessoas1_idx` (`usuario_cadastro` ASC),
ADD INDEX `fk_acadgeral__aluno_acesso_pessoas2_idx` (`usuario_alteracao` ASC);

ALTER TABLE `acadgeral__aluno`
ADD CONSTRAINT `fk_acadgeral__aluno_acesso_pessoas1`
  FOREIGN KEY (`usuario_cadastro`)2
  REFERENCES `acesso_pessoas` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_acadgeral__aluno_acesso_pessoas2`
  FOREIGN KEY (`usuario_alteracao`)
  REFERENCES `acesso_pessoas` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;


ALTER TABLE `acadgeral__aluno_curso`
ADD COLUMN `usuario_cadastro` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL,
ADD COLUMN `usuario_alteracao` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL,
ADD INDEX `fk_acadgeral__aluno_curso_acesso_pessoas1_idx` (`usuario_cadastro` ASC),
ADD INDEX `fk_acadgeral__aluno_curso_acesso_pessoas2_idx` (`usuario_alteracao` ASC);

ALTER TABLE `acadgeral__aluno_curso`
ADD CONSTRAINT `fk_acadgeral__aluno_curso_acesso_pessoas1`
  FOREIGN KEY (`usuario_cadastro`)
  REFERENCES `acesso_pessoas` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_acadgeral__aluno_curso_acesso_pessoas2`
  FOREIGN KEY (`usuario_alteracao`)
  REFERENCES `acesso_pessoas` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;


REPLACE INTO org__comunicacao_tipo(tipo_id, tipo_descricao) VALUES (0000000006, 'Envio de Títulos');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
