-- MySQL Workbench Synchronization
-- Generated: 2017-01-26 10:51
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia
-- **EXECUTAR COM MUITA ATENÇÃO AS QUERIES ABAIXO UMA A UMA**

SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE IF NOT EXISTS `selecao_edicao_fechamento` (
  `selecaofechamento_id`   INT(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  `edicao_id`              INT(10) UNSIGNED ZEROFILL NOT NULL,
  `usuariofechamento_id`   INT(11) UNSIGNED ZEROFILL NOT NULL,
  `selecaofechamento_data` DATETIME                  NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `classificacao_limite`   INT(11)                   NOT NULL,
  PRIMARY KEY (`selecaofechamento_id`),
  INDEX `fk_selecao_edicao_acesso_pessoas_acesso_pessoas1_idx` (`usuariofechamento_id` ASC),
  INDEX `fk_selecao_edicao_acesso_pessoas_selecao_edicao1_idx` (`edicao_id` ASC),
  CONSTRAINT `fk_selecao_edicao_acesso_pessoas_acesso_pessoas1`
  FOREIGN KEY (`usuariofechamento_id`)
  REFERENCES `acesso_pessoas` (`usuario`)
    ON UPDATE CASCADE,
  CONSTRAINT `fk_selecao_edicao_acesso_pessoas_selecao_edicao1`
  FOREIGN KEY (`edicao_id`)
  REFERENCES `selecao_edicao` (`edicao_id`)
    ON UPDATE CASCADE
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 45
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_general_ci;

ALTER TABLE `selecao_inscricao`
CHANGE COLUMN `inscricao_data` `inscricao_data` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
ADD COLUMN `inscricao_classificacao` INT(10) NULL DEFAULT NULL
AFTER `inscricao_ip`,
COMMENT = '';

ALTER TABLE `selecao_inscricao`
ADD CONSTRAINT `fk_selecao_inscricao_pessoa_fisica1`
FOREIGN KEY (`pes_id`)
REFERENCES `pessoa_fisica` (`pes_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

-- chave do pes_id da ies
ALTER TABLE `org_ies` DROP FOREIGN KEY `fk_ies_pessoa_juridica1`;

-- chave da configuração de boleto
ALTER TABLE `boleto` DROP FOREIGN KEY `fk_boleto_configuracao_boleto1`;

-- chave da configuração de boleto
ALTER TABLE `boleto_historico` DROP FOREIGN KEY `fk_boleto_configuracao_boleto10`;

-- deixando que possa ser null para que possa ser removido no futuro
ALTER TABLE `org_ies` CHANGE COLUMN `pes_id` `pes_id` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL
COMMENT 'Id da IES como pessoa jurídica';

ALTER TABLE `org_ies` ADD COLUMN `ies_nome` VARCHAR(255) NOT NULL DEFAULT '-'
AFTER `ies_id_sede`;
ALTER TABLE `org_ies` ADD COLUMN `ies_sigla` VARCHAR(50) NULL DEFAULT NULL
AFTER `ies_nome`;
ALTER TABLE `org_ies` ADD COLUMN `ies_end_numero` INT(11) NULL DEFAULT NULL
AFTER `ies_sigla`;
ALTER TABLE `org_ies` ADD COLUMN `ies_end_cep` VARCHAR(45) NULL DEFAULT NULL
AFTER `ies_end_numero`;
ALTER TABLE `org_ies` ADD COLUMN `ies_end_bairro` VARCHAR(45) NULL DEFAULT NULL
AFTER `ies_end_cep`;
ALTER TABLE `org_ies` ADD COLUMN `ies_end_cidade` VARCHAR(45) NULL DEFAULT NULL
AFTER `ies_end_bairro`;
ALTER TABLE `org_ies` ADD COLUMN `ies_end_estado` VARCHAR(45) NULL DEFAULT NULL
AFTER `ies_end_cidade`;
ALTER TABLE `org_ies` ADD COLUMN `ies_end_logradouro` VARCHAR(45) NULL DEFAULT NULL
AFTER `ies_end_estado`;
ALTER TABLE `org_ies` ADD COLUMN `ies_telefone` VARCHAR(15) NULL DEFAULT NULL
AFTER `ies_end_logradouro`;
ALTER TABLE `org_ies` ADD COLUMN `ies_email` VARCHAR(100) NULL DEFAULT NULL
AFTER `ies_telefone`;

-- deixando que possa ser null para que possa ser removido no futuro
ALTER TABLE `boleto` CHANGE COLUMN `confbol_id` `confbol_id` INT(11) NULL DEFAULT NULL;
-- configuração de conta diretamente no boleto
ALTER TABLE `boleto` ADD COLUMN `confcont_id` INT(11) NULL DEFAULT NULL
AFTER `confbol_id`;
ALTER TABLE `boleto` ADD INDEX `fk_boleto_boleto_conf_conta1_idx` (`confcont_id` ASC);
-- acrescimos multa+juros recebidos pelo banco
ALTER TABLE `boleto` ADD COLUMN `bol_acrescimos` FLOAT(10, 2) NULL DEFAULT NULL
AFTER `bol_juros`;
-- valor pago pelo sacado
ALTER TABLE `boleto` ADD COLUMN `bol_valor_pago` FLOAT(10, 2) NULL DEFAULT NULL
AFTER `bol_acrescimos`;

-- deixando que possa ser null para que possa ser removido no futuro
ALTER TABLE `boleto_historico` CHANGE COLUMN `confbol_id` `confbol_id` INT(11) NULL DEFAULT NULL;
-- configuração de conta diretamente no boleto
ALTER TABLE `boleto_historico` ADD COLUMN `confcont_id` INT(11) NULL DEFAULT NULL
AFTER `confbol_id`;
ALTER TABLE `boleto_historico` ADD INDEX `fk_boleto_historico_1_idx` (`confcont_id` ASC);

-- deixando que possa ser null para que possa ser removido no futuro
ALTER TABLE `selecao_edicao` CHANGE COLUMN `confbol_id` `confbol_id` INT(11) NULL DEFAULT NULL;
-- configuração de conta diretamente no boleto
ALTER TABLE `selecao_edicao` ADD COLUMN `confcont_id` INT(11) NULL DEFAULT NULL
AFTER `confbol_id`;
ALTER TABLE `selecao_edicao` ADD INDEX `fk_selecao_edicao_1_idx` (`confcont_id` ASC);

-- novas situações voltadas para a pós graduação
ALTER TABLE `acadgeral__aluno_curso`
CHANGE COLUMN `alunocurso_situacao` `alunocurso_situacao` ENUM('Deferido', 'Pendente', 'Trancado', 'Cancelado') NOT NULL DEFAULT 'Deferido';

-- chave da configuração de conta
ALTER TABLE `boleto`
ADD CONSTRAINT `fk_boleto_boleto_conf_conta1`
FOREIGN KEY (`confcont_id`)
REFERENCES `boleto_conf_conta` (`confcont_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

-- chave da configuração de conta
ALTER TABLE `boleto_historico`
ADD CONSTRAINT `fk_boleto_historico_1`
FOREIGN KEY (`confcont_id`)
REFERENCES `boleto_conf_conta` (`confcont_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

-- chave da configuração de conta
ALTER TABLE `selecao_edicao`
ADD CONSTRAINT `fk_selecao_edicao_conta`
FOREIGN KEY (`confcont_id`)
REFERENCES `boleto_conf_conta` (`confcont_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `financeiro__titulo_tipo`
CHANGE COLUMN `tipotitulo_matricula_emissao` `tipotitulo_matricula_emissao` ENUM('Sim', 'Não', 'Opcional') NULL DEFAULT 'Não';

-- atualiza configuração de conta do boleto
UPDATE boleto b
  INNER JOIN boleto_conf bc USING (confbol_id)
  INNER JOIN boleto_conf_conta bcc ON bcc.confcont_id = bc.confcont_id
SET b.confcont_id = bcc.confcont_id;

-- atualiza configuração de conta do boleto
UPDATE boleto_historico b
  INNER JOIN boleto_conf bc USING (confbol_id)
  INNER JOIN boleto_conf_conta bcc ON bcc.confcont_id = bc.confcont_id
SET b.confcont_id = bcc.confcont_id;

-- atualiza dados de ies
UPDATE
    org_ies ies
    INNER JOIN pessoa_juridica pji ON pji.pes_id = ies.pes_id
    INNER JOIN pessoa pi ON pi.pes_id = pji.pes_id
    LEFT JOIN endereco ON endereco.pes_id = pi.pes_id
SET
  ies.ies_nome           = coalesce(pji.pes_nome_fantasia, pi.pes_nome),
  ies.ies_sigla          = coalesce(pji.pes_sigla, pji.pes_nome_fantasia, pi.pes_nome),
  ies.ies_end_numero     = end_numero,
  ies.ies_end_cep        = end_cep,
  ies.ies_end_bairro     = end_bairro,
  ies.ies_end_cidade     = end_cidade,
  ies.ies_end_estado     = end_estado,
  ies.ies_end_logradouro = end_logradouro,
  ies.ies_telefone       = coalesce(
      (
        SELECT con_contato
        FROM contato
        WHERE pessoa = pi.pes_id AND tipo_contato = 2 AND con_contato != ''
        ORDER BY con_contato DESC
        LIMIT 0, 1
      ),
      (
        SELECT con_contato
        FROM contato
        WHERE pessoa = pi.pes_id AND tipo_contato = 1 AND con_contato != ''
        ORDER BY con_contato DESC
        LIMIT 0, 1
      )
  ),
  ies.ies_email          = (
    SELECT con_contato
    FROM contato
    WHERE pessoa = pi.pes_id AND tipo_contato = 1 AND con_contato != ''
    ORDER BY con_contato DESC
    LIMIT 0, 1
  );


-- chave da configuração de boleto
ALTER TABLE `selecao_edicao` DROP FOREIGN KEY `fk_selecao_edicao_boleto_conf1_idx`;
ALTER TABLE `selecao_edicao` DROP FOREIGN KEY `fk_selecao_edicao_boleto_conf1`;

-- atualiza configuração de conta do boleto
UPDATE selecao_edicao b
  INNER JOIN boleto_conf bc USING (confbol_id)
  INNER JOIN boleto_conf_conta bcc ON bcc.confcont_id = bc.confcont_id
SET b.confcont_id = bcc.confcont_id;

-- remove coluna de configuração de conta do historico de boleto
ALTER TABLE `selecao_edicao` DROP COLUMN `confbol_id`;

-- remove coluna de configuração de conta do historico de boleto
ALTER TABLE `boleto` DROP COLUMN `confbol_id`;

-- remove coluna de configuração de conta do historico de boleto
ALTER TABLE `boleto_historico` DROP COLUMN `confbol_id`;
-- remove tabela de configuração de boleto
DROP TABLE boleto_conf;

-- deixa que alunoperId possa ser nulo
ALTER TABLE `financeiro__desconto`
CHANGE COLUMN `alunoper_id` `alunoper_id` INT(10) UNSIGNED ZEROFILL NULL DEFAULT NULL;
-- deixa que alunoperId possa ser nulo
ALTER TABLE `financeiro__desconto`
ADD COLUMN `alunocurso_id` INT(10) UNSIGNED ZEROFILL NULL DEFAULT NULL;
-- chave estrangeira do curso do aluno
ALTER TABLE `financeiro__desconto`
ADD CONSTRAINT `fk_financeiro__desconto_acadgeral__aluno_curso1`
FOREIGN KEY (`alunocurso_id`)
REFERENCES `acadgeral__aluno_curso` (`alunocurso_id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

CREATE TABLE IF NOT EXISTS `financeiro__desconto_titulo` (
  `desconto_titulo_id`        INT(10) UNSIGNED ZEROFILL                  NOT NULL AUTO_INCREMENT,
  `desconto_id`               INT(11)                                    NOT NULL,
  `titulo_id`                 INT(11) ZEROFILL                           NOT NULL,
  `usuario_alteracao`         INT(11) UNSIGNED ZEROFILL                  NULL     DEFAULT NULL,
  `usuario_criacao`           INT(11) UNSIGNED ZEROFILL                  NULL     DEFAULT NULL,
  `desconto_titulo_criacao`   DATETIME                                   NOT NULL,
  `desconto_titulo_alteracao` DATETIME                                   NOT NULL,
  `desconto_titulo_aplicado`  ENUM('Sim', 'Não')                         NOT NULL DEFAULT 'Não',
  `desconto_titulo_situacao`  ENUM('Pendente', 'Deferido', 'Indeferido') NOT NULL DEFAULT 'Pendente',
  INDEX `fk_financeiro__desconto_financeiro__titulo_financeiro__titu_idx` (`titulo_id` ASC),
  INDEX `fk_financeiro__desconto_financeiro__titulo_financeiro__desc_idx` (`desconto_id` ASC),
  PRIMARY KEY (`desconto_titulo_id`),
  INDEX `fk_financeiro__desconto_titulo_acesso_pessoas1_idx` (`usuario_criacao` ASC),
  INDEX `fk_financeiro__desconto_titulo_acesso_pessoas2_idx` (`usuario_alteracao` ASC),
  CONSTRAINT `fk_financeiro__desconto_financeiro__titulo_financeiro__descon1`
  FOREIGN KEY (`desconto_id`)
  REFERENCES `financeiro__desconto` (`desconto_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_financeiro__desconto_financeiro__titulo_financeiro__titulo1`
  FOREIGN KEY (`titulo_id`)
  REFERENCES `financeiro__titulo` (`titulo_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_financeiro__desconto_titulo_acesso_pessoas1`
  FOREIGN KEY (`usuario_criacao`)
  REFERENCES `acesso_pessoas` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_financeiro__desconto_titulo_acesso_pessoas2`
  FOREIGN KEY (`usuario_alteracao`)
  REFERENCES `acesso_pessoas` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;

-- caso não exista a coluna desconto_titulo_aplicado na tabela, executar esta query
ALTER TABLE financeiro__desconto_titulo
ADD COLUMN `desconto_titulo_aplicado` ENUM('Sim', 'Não') NOT NULL DEFAULT 'Não'
AFTER desconto_titulo_alteracao;

-- Lança desconto para título em vez de mensalidade
INSERT INTO financeiro__desconto_titulo
(desconto_titulo_id, desconto_id, titulo_id, usuario_alteracao, usuario_criacao, desconto_titulo_criacao, desconto_titulo_alteracao, desconto_titulo_situacao)
  (SELECT
     NULL,
     desconto_id,
     titulo_id,
     if(d.usuario_id IS NULL OR d.usuario_id = 0, 1, d.usuario_id),
     if(d.usuario_id IS NULL OR d.usuario_id = 0, 1, d.usuario_id),
     titulo_data_processamento,
     titulo_data_processamento,
     if(md.descmensalidade_status = 'Ativo', 'Deferido', 'Indeferido')
   FROM financeiro__desconto d
     INNER JOIN financeiro__mensalidade_desconto md USING (desconto_id)
     INNER JOIN financeiro__titulo_mensalidade m USING (financeiro_titulo_mensalidade_id)
     INNER JOIN financeiro__titulo t USING (titulo_id));

-- atualiza alunocurso_id para alunos que possuem o alunoper_id
UPDATE financeiro__desconto d
  INNER JOIN acadperiodo__aluno ap USING (alunoper_id)
SET d.alunocurso_id = ap.alunocurso_id;

-- inclui aluno
ALTER TABLE `financeiro__desconto`
ADD COLUMN `aluno_id` INT(10) UNSIGNED ZEROFILL NULL DEFAULT NULL;
-- chave estrangeira do curso do aluno
ALTER TABLE `financeiro__desconto`
ADD CONSTRAINT `fk_financeiro__desconto_acadgeral__aluno1`
FOREIGN KEY (`aluno_id`)
REFERENCES `acadgeral__aluno` (`aluno_id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

-- atualiza alunocurso_id para alunos que possuem o alunoper_id
UPDATE financeiro__desconto d
  INNER JOIN acadgeral__aluno_curso ac USING (alunocurso_id)
SET d.aluno_id = ac.aluno_id;

-- Remove  alunocurso_id da tabela de desconto
ALTER TABLE `financeiro__desconto` DROP FOREIGN KEY `fk_financeiro__desconto_acadgeral__aluno_curso1`;
ALTER TABLE `financeiro__desconto` DROP COLUMN `alunocurso_id`;

SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;