--
-- módulo sistema
--
DELETE FROM `acesso_privilegios_grupo` WHERE acesso_actions_id IN(
	select acesso_actions.id from acesso_funcionalidades
	inner join acesso_actions on funcionalidade=acesso_funcionalidades.id
	where func_controller IN(
		'Financeiro\\Controller\\BoletoBanco',
		'Financeiro\\Controller\\BoletoConfConta',
		'Financeiro\\Controller\\Boleto',
		'Financeiro\\Controller\\BoletoLayout',
		'Financeiro\\Controller\\FinanceiroAlunoConfigPgtoCurso',
		'Financeiro\\Controller\\FinanceiroCheque',
		'Financeiro\\Controller\\FinanceiroDesconto',
		'Financeiro\\Controller\\FinanceiroDescontoTipo',
		'Financeiro\\Controller\\FinanceiroMeioPagamento',
		'Financeiro\\Controller\\FinanceiroMensalidadeDesconto',
		'Financeiro\\Controller\\FinanceiroPagamento',
		'Financeiro\\Controller\\FinanceiroRemessa',
		'Financeiro\\Controller\\FinanceiroRemessaLote',
		'Financeiro\\Controller\\FinanceiroRemessaLoteRegistro',
		'Financeiro\\Controller\\FinanceiroTituloAlteracao',
		'Financeiro\\Controller\\FinanceiroTituloCheque',
		'Financeiro\\Controller\\FinanceiroTituloConfig',
		'Financeiro\\Controller\\FinanceiroTitulo',
		'Financeiro\\Controller\\FinanceiroTituloMensalidade',
		'Financeiro\\Controller\\FinanceiroTituloTipo',
		'Financeiro\\Controller\\FinanceiroValores'
	)
) AND acesso_actions_id>=0 AND acesso_grupo_id>=0;

DELETE FROM `acesso_actions` WHERE funcionalidade in(
	SELECT acesso_funcionalidades.id from acesso_funcionalidades
	where func_controller IN(
		'Financeiro\\Controller\\BoletoBanco',
		'Financeiro\\Controller\\BoletoConfConta',
		'Financeiro\\Controller\\Boleto',
		'Financeiro\\Controller\\BoletoLayout',
		'Financeiro\\Controller\\FinanceiroAlunoConfigPgtoCurso',
		'Financeiro\\Controller\\FinanceiroCheque',
		'Financeiro\\Controller\\FinanceiroDesconto',
		'Financeiro\\Controller\\FinanceiroDescontoTipo',
		'Financeiro\\Controller\\FinanceiroMeioPagamento',
		'Financeiro\\Controller\\FinanceiroMensalidadeDesconto',
		'Financeiro\\Controller\\FinanceiroPagamento',
		'Financeiro\\Controller\\FinanceiroRemessa',
		'Financeiro\\Controller\\FinanceiroRemessaLote',
		'Financeiro\\Controller\\FinanceiroRemessaLoteRegistro',
		'Financeiro\\Controller\\FinanceiroTituloAlteracao',
		'Financeiro\\Controller\\FinanceiroTituloCheque',
		'Financeiro\\Controller\\FinanceiroTituloConfig',
		'Financeiro\\Controller\\FinanceiroTitulo',
		'Financeiro\\Controller\\FinanceiroTituloMensalidade',
		'Financeiro\\Controller\\FinanceiroTituloTipo',
		'Financeiro\\Controller\\FinanceiroValores'
	)
) and id>0;

DELETE FROM acesso_funcionalidades where func_controller IN(
		'Financeiro\\Controller\\BoletoBanco',
		'Financeiro\\Controller\\BoletoConfConta',
		'Financeiro\\Controller\\Boleto',
		'Financeiro\\Controller\\BoletoLayout',
		'Financeiro\\Controller\\FinanceiroAlunoConfigPgtoCurso',
		'Financeiro\\Controller\\FinanceiroCheque',
		'Financeiro\\Controller\\FinanceiroDesconto',
		'Financeiro\\Controller\\FinanceiroDescontoTipo',
		'Financeiro\\Controller\\FinanceiroMeioPagamento',
		'Financeiro\\Controller\\FinanceiroMensalidadeDesconto',
		'Financeiro\\Controller\\FinanceiroPagamento',
		'Financeiro\\Controller\\FinanceiroRemessa',
		'Financeiro\\Controller\\FinanceiroRemessaLote',
		'Financeiro\\Controller\\FinanceiroRemessaLoteRegistro',
		'Financeiro\\Controller\\FinanceiroTituloAlteracao',
		'Financeiro\\Controller\\FinanceiroTituloCheque',
		'Financeiro\\Controller\\FinanceiroTituloConfig',
		'Financeiro\\Controller\\FinanceiroTitulo',
		'Financeiro\\Controller\\FinanceiroTituloMensalidade',
		'Financeiro\\Controller\\FinanceiroTituloTipo',
		'Financeiro\\Controller\\FinanceiroValores'
) and id>0;

DELETE FROM acesso_modulos where mod_route='financeiro/default' and id>0;

REPLACE INTO `acesso_modulos` (`sistema`, `mod_nome`, `mod_route`, `mod_status`, `mod_icon`, `mod_info`, `mod_redireciona`, `mod_prioridade`)
SELECT acesso_sistemas.id, 'Financeiro', 'financeiro/default', 'Ativo', 'fa fa-money', 'Modúlo Financeiro', '/financeiro', round(acesso_sistemas.id/10) from acesso_sistemas
where sis_nome like 'Financeiro';

REPLACE INTO `acesso_funcionalidades` (modulo, func_nome, func_controller, func_status, func_visivel)
SELECT distinct acesso_modulos.id, func.descricao, func.controller,'Ativa','Sim'  from acesso_modulos,
(
	select 'Gestão de Banco' as descricao, 'Financeiro\\Controller\\BoletoBanco' as controller from dual union all
	select 'Gestão de Contas Bancárias' as descricao, 'Financeiro\\Controller\\BoletoConfConta' as controller from dual union all
	select 'Gestão de Boletos' as descricao, 'Financeiro\\Controller\\Boleto' as controller from dual union all
	select 'Gestão de Cheques' as descricao, 'Financeiro\\Controller\\FinanceiroCheque' as controller from dual union all
	select 'Gestão de Tipos de Desconto' as descricao, 'Financeiro\\Controller\\FinanceiroDescontoTipo' as controller from dual union all
	select 'Gestão de Meios de Pagamento' as descricao, 'Financeiro\\Controller\\FinanceiroMeioPagamento' as controller from dual union all
	select 'Gestão de Pagamentos' as descricao, 'Financeiro\\Controller\\FinanceiroPagamento' as controller from dual union all
	select 'Gestão de Remessas' as descricao, 'Financeiro\\Controller\\FinanceiroRemessa' as controller from dual union all
	select 'Gestão de Configurações de Título' as descricao, 'Financeiro\\Controller\\FinanceiroTituloConfig' as controller from dual union all
	select 'Gestão de Títulos' as descricao, 'Financeiro\\Controller\\FinanceiroTitulo' as controller from dual union all
	select 'Gestão de Tipos de Título' as descricao, 'Financeiro\\Controller\\FinanceiroTituloTipo' as controller from dual union all
	select 'Gestão de Configurações de Valores' as descricao, 'Financeiro\\Controller\\FinanceiroValores' as controller from dual
) as func
where mod_nome like 'Financeiro';

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
(
	select 'index' as act,'Listagem' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'remove' as act,'Remover' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'add' as act,'Criar' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'edit' as act,'Editar' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'search-for-json' as act, 'Autocomplete' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'search' as act,'Datatables' as descricao,'Leitura' as tipo,'N' as usr from dual
) as actions
where func_controller IN(
		'Financeiro\\Controller\\BoletoBanco',
		'Financeiro\\Controller\\BoletoConfConta',
		'Financeiro\\Controller\\Boleto',
		'Financeiro\\Controller\\BoletoLayout',
		'Financeiro\\Controller\\FinanceiroAlunoConfigPgtoCurso',
		'Financeiro\\Controller\\FinanceiroCheque',
		'Financeiro\\Controller\\FinanceiroDesconto',
		'Financeiro\\Controller\\FinanceiroDescontoTipo',
		'Financeiro\\Controller\\FinanceiroMeioPagamento',
		'Financeiro\\Controller\\FinanceiroMensalidadeDesconto',
		'Financeiro\\Controller\\FinanceiroPagamento',
		'Financeiro\\Controller\\FinanceiroRemessa',
		'Financeiro\\Controller\\FinanceiroRemessaLote',
		'Financeiro\\Controller\\FinanceiroRemessaLoteRegistro',
		'Financeiro\\Controller\\FinanceiroTituloAlteracao',
		'Financeiro\\Controller\\FinanceiroTituloCheque',
		'Financeiro\\Controller\\FinanceiroTituloConfig',
		'Financeiro\\Controller\\FinanceiroTitulo',
		'Financeiro\\Controller\\FinanceiroTituloMensalidade',
		'Financeiro\\Controller\\FinanceiroTituloTipo',
		'Financeiro\\Controller\\FinanceiroValores'
);
REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
acesso_grupo
where func_controller IN(
		'Financeiro\\Controller\\BoletoBanco',
		'Financeiro\\Controller\\BoletoConfConta',
		'Financeiro\\Controller\\Boleto',
		'Financeiro\\Controller\\BoletoLayout',
		'Financeiro\\Controller\\FinanceiroAlunoConfigPgtoCurso',
		'Financeiro\\Controller\\FinanceiroCheque',
		'Financeiro\\Controller\\FinanceiroDesconto',
		'Financeiro\\Controller\\FinanceiroDescontoTipo',
		'Financeiro\\Controller\\FinanceiroMeioPagamento',
		'Financeiro\\Controller\\FinanceiroMensalidadeDesconto',
		'Financeiro\\Controller\\FinanceiroPagamento',
		'Financeiro\\Controller\\FinanceiroRemessa',
		'Financeiro\\Controller\\FinanceiroRemessaLote',
		'Financeiro\\Controller\\FinanceiroRemessaLoteRegistro',
		'Financeiro\\Controller\\FinanceiroTituloAlteracao',
		'Financeiro\\Controller\\FinanceiroTituloCheque',
		'Financeiro\\Controller\\FinanceiroTituloConfig',
		'Financeiro\\Controller\\FinanceiroTitulo',
		'Financeiro\\Controller\\FinanceiroTituloMensalidade',
		'Financeiro\\Controller\\FinanceiroTituloTipo',
		'Financeiro\\Controller\\FinanceiroValores'
)
and grup_nome LIKE 'Admin%';


select * from arquivo_diretorios