-- View de configuração de títulos
DROP VIEW IF EXISTS `view__financeiro_configuracao_titulos`;
DROP TABLE IF EXISTS `view__financeiro_configuracao_titulos`;
CREATE OR REPLACE VIEW `view__financeiro_configuracao_titulos` AS
  SELECT
    tc.*,
    -- prioriza configuração de campus/curso
    if(cursocampus_id IS NOT NULL, 1, 2)              AS prioridade_por_curso,
    -- prioriza configuração de tipo de título
    if(tipotitulo_id IS NOT NULL, 1, 2)               AS prioridade_por_tipo,
    -- prioriza configuração de periodo letivo
    if(pl.per_id IS NOT NULL, 1, 2)                   AS prioridade_por_periodo,
    -- prioriza configuração que iniciou primeiro
    COALESCE(per_data_inicio, tituloconf_data_inicio) AS prioridade_por_inicio,
    -- prioriza configuração que termina antes
    COALESCE(per_data_fim, tituloconf_data_fim)       AS prioridade_por_fim
  FROM financeiro__titulo_config tc
    LEFT JOIN acadperiodo__letivo pl ON tc.per_id = pl.per_id
  WHERE (
    (date(now()) >= date(tc.tituloconf_data_inicio) AND date(now()) <= date(tc.tituloconf_data_fim)) OR
    (date(now()) >= date(pl.per_data_inicio) AND date(now()) <= date(pl.per_data_fim))
  )
  ORDER BY
    prioridade_por_curso ASC,
    prioridade_por_tipo ASC,
    prioridade_por_periodo ASC,
    prioridade_por_inicio ASC,
    prioridade_por_fim ASC,
    tituloconf_id ASC;


-- view com informações sobre os descontos de títulos
DROP VIEW IF EXISTS `view__financeiro_descontos_titulos`;
DROP TABLE IF EXISTS `view__financeiro_descontos_titulos`;
CREATE OR REPLACE VIEW `view__financeiro_descontos_titulos` AS
  SELECT
    fdt.*,
    u1.login      AS usuario_criacao_login,
    u2.login      AS usuario_alteracao_login,
    CONCAT(
        desconto_id, ' - ', desctipo_descricao, ' / ', pes_nome, ' / ',
        IF(desctipo_percmax IS NULL, desconto_valor, concat(desconto_percentual, '%'))
    )             AS desconto_dados,
    CONCAT(
        titulo_id, ' - ', titulo_descricao
    )             AS titulo_dados,
    fd.desconto_valor,
    fd.desconto_percentual,
    fd.desconto_status,
    fd.desconto_dia_limite,
    fdtp.desctipo_id,
    fdtp.desctipo_limita_vencimento,
    -- Verifica se o título está aberto
    if(titulo_estado = 'Aberto',
       -- verifica se o desconto está ativo e se o desconto do título foi deferido
       if(fd.desconto_status = 'Inativo' OR desconto_titulo_situacao <> 'Deferido',
          'Não',
          -- verifica se o desconto está limitado ao vencimento e se o título está vencido
          if(fdtp.desctipo_limita_vencimento = 'Sim' AND titulo_data_vencimento < now(),
             'Não',
             -- verifica se o dia limite do desconto relativo ao mês da data de vencimento já passou
             if(fd.desconto_dia_limite IS NOT NULL AND
                DATE(CONCAT(
                         year(titulo_data_vencimento), '-',
                         month(titulo_data_vencimento), '-',
                         fd.desconto_dia_limite
                     )) < curdate(),
                'Não',
                'Sim'
             )

          )
       ),
       -- verifica se o desconto foi aplicado
       if(fdt.desconto_titulo_aplicado = 'Sim', 'Sim', 'Não')
    )             AS desconto_aplicacao_valida,
    -- Verifica se o título está aberto
    if(titulo_estado = 'Aberto',
       -- verifica se o desconto é monetário ou percentual
       if(fd.desconto_valor IS NOT NULL,
          fd.desconto_valor,
          titulo_valor * (fd.desconto_percentual / 100)
       ),
       -- verifica se o desconto foi aplicado
       if(fdt.desconto_titulo_aplicado = 'Sim', desconto_titulo_valor, 0)
    )             AS desconto_aplicacao_valor,
    titulo_id        tituloId,
    titulo_valor,
    titulo_data_vencimento,
    titulo_estado,
    titulo_estado AS tituloEstado
  FROM financeiro__desconto_titulo fdt
    INNER JOIN financeiro__desconto fd USING (desconto_id)
    INNER JOIN financeiro__desconto_tipo fdtp USING (desctipo_id)
    LEFT JOIN acesso_pessoas u1 ON u1.id = fdt.usuario_criacao
    LEFT JOIN acesso_pessoas u2 ON u2.id = fdt.usuario_alteracao
    LEFT JOIN financeiro__titulo ft USING (titulo_id)
    LEFT JOIN acadgeral__aluno aa USING (aluno_id)
    LEFT JOIN pessoa p ON p.pes_id = aa.pes_id
  ORDER BY titulo_id DESC;


-- view com informações sobre os descontos de títulos
DROP VIEW IF EXISTS `view__financeiro_descontos_titulos_simplificado`;
DROP TABLE IF EXISTS `view__financeiro_descontos_titulos_simplificado`;
CREATE OR REPLACE VIEW `view__financeiro_descontos_titulos_simplificado` AS
  SELECT
    fdt.*,
    fd.desconto_valor,
    fd.desconto_percentual,
    fd.desconto_status,
    fd.desconto_dia_limite,
    fdtp.desctipo_id,
    fdtp.desctipo_limita_vencimento,
    -- Verifica se o título está aberto
    if(titulo_estado = 'Aberto',
       -- verifica se o desconto está ativo e se o desconto do título foi deferido
       if(fd.desconto_status = 'Inativo' OR desconto_titulo_situacao <> 'Deferido',
          'Não',
          -- verifica se o desconto está limitado ao vencimento e se o título está vencido
          if(fdtp.desctipo_limita_vencimento = 'Sim' AND titulo_data_vencimento < now(),
             'Não',
             -- verifica se o dia limite do desconto relativo ao mês da data de vencimento já passou
             if(fd.desconto_dia_limite IS NOT NULL AND
                DATE(CONCAT(
                         year(titulo_data_vencimento), '-',
                         month(titulo_data_vencimento), '-',
                         fd.desconto_dia_limite
                     )) < curdate(),
                'Não',
                'Sim'
             )

          )
       ),
       -- verifica se o desconto foi aplicado
       if(fdt.desconto_titulo_aplicado = 'Sim', 'Sim', 'Não')
    )             AS desconto_aplicacao_valida,
    -- Verifica se o título está aberto
    if(titulo_estado = 'Aberto',
       -- verifica se o desconto é monetário ou percentual
       if(fd.desconto_valor IS NOT NULL,
          fd.desconto_valor,
          titulo_valor * (fd.desconto_percentual / 100)
       ),
       -- verifica se o desconto foi aplicado
       if(fdt.desconto_titulo_aplicado = 'Sim', desconto_titulo_valor, 0)
    )             AS desconto_aplicacao_valor,
    titulo_id        tituloId,
    titulo_valor,
    titulo_data_vencimento,
    titulo_estado,
    titulo_estado AS tituloEstado
  FROM financeiro__desconto_titulo fdt
    INNER JOIN financeiro__desconto fd USING (desconto_id)
    INNER JOIN financeiro__desconto_tipo fdtp USING (desctipo_id)
    LEFT JOIN financeiro__titulo ft USING (titulo_id)
  ORDER BY titulo_id DESC;


-- view com informações sobre os títulos
DROP VIEW IF EXISTS `view__financeiro_titulos`;
DROP TABLE IF EXISTS `view__financeiro_titulos`;
CREATE OR REPLACE VIEW `view__financeiro_titulos` AS
  SELECT
    t.titulo_id,
    t.pes_id,
    p.pes_nome,
    t.tipotitulo_id,
    tt.tipotitulo_nome,
    t.usuario_baixa,
    ub.login       usuario_baixa_login,
    t.usuario_autor,
    ua.login       usuario_autor_login,
    t.titulo_novo,
    t.alunocurso_id,
    ac.aluno_id,
    a.pes_id as pes_id_aluno,
    c.curso_nome,
    c.curso_sigla,
    t.titulo_descricao,
    t.titulo_parcela,
    t.titulo_valor_pago,
    t.titulo_valor_pago_dinheiro,
    t.titulo_multa,
    t.titulo_juros,
    t.titulo_desconto,
    t.titulo_observacoes,
    t.titulo_desconto_manual,
    t.titulo_acrescimo_manual,
    t.titulo_data_processamento,
    t.titulo_data_vencimento,
    t.titulo_data_pagamento,
    t.titulo_data_baixa,
    t.titulo_tipo_pagamento,
    t.titulo_valor,
    t.titulo_estado,
    cc.cursocampus_id,
    greatest(
        if(
            titulo_data_pagamento IS NULL,
            datediff(date(now()), date(t.titulo_data_vencimento)),
            datediff(date(titulo_data_pagamento), date(t.titulo_data_vencimento))
        ),
        0
    )           AS dias_atraso,
    coalesce(
        if(titulo_data_pagamento IS NULL AND
           greatest(
               if(
                   titulo_data_pagamento IS NULL,
                   datediff(date(now()), date(t.titulo_data_vencimento)),
                   datediff(date(titulo_data_pagamento), date(t.titulo_data_vencimento))
               ),
               0
           ) > 0,
           -- configuração de juros e multa para título
             coalesce(tituloconf_multa, 0) * t.titulo_valor,
           titulo_multa
        ),
        0
    )           AS titulo_multa_calc,
    if(
        titulo_data_pagamento IS NULL,
        -- configuração de juros e multa para título
        coalesce(tituloconf_juros, 0) * greatest(
            if(
                titulo_data_pagamento IS NULL,
                datediff(date(now()), date(t.titulo_data_vencimento)),
                datediff(date(titulo_data_pagamento), date(t.titulo_data_vencimento))
            ),
            0
        ),
        coalesce(titulo_juros, 0)
    )           AS titulo_juros_calc,
    (
      SELECT coalesce(sum(desconto_aplicacao_valor), 0)
      FROM view__financeiro_descontos_titulos_simplificado fdt
      WHERE
        desconto_aplicacao_valida = 'Sim' AND
        fdt.titulo_id = t.titulo_id
    )           AS descontos,
    tc.tituloconf_id,
    tc.tituloconf_dia_venc,
    tc.tituloconf_dia_desc,
    tc.tituloconf_valor_desc,
    tc.tituloconf_percent_desc,
    tc.tituloconf_multa,
    tc.tituloconf_juros,
    tc.confcont_id,
    b.bol_id
  FROM financeiro__titulo t
    INNER JOIN financeiro__titulo_tipo tt ON t.tipotitulo_id = tt.tipotitulo_id
    -- dados aluno
    INNER JOIN pessoa p ON t.pes_id = p.pes_id
    -- curso aluno
    LEFT JOIN acadgeral__aluno_curso ac ON ac.alunocurso_id = t.alunocurso_id
    LEFT JOIN acadgeral__aluno a ON a.aluno_id = ac.aluno_id
    LEFT JOIN campus_curso cc ON cc.cursocampus_id = ac.cursocampus_id
    LEFT JOIN acad_curso c ON c.curso_id = cc.curso_id
    -- boleto
    LEFT JOIN boleto b ON t.titulo_id = b.titulo_id
    -- usuário baixa
    LEFT JOIN acesso_pessoas ua ON ua.id = t.usuario_autor
    LEFT JOIN acesso_pessoas ub ON ub.id = t.usuario_baixa
    LEFT JOIN view__financeiro_configuracao_titulos AS tc ON (
      (tc.cursocampus_id = ac.cursocampus_id OR
       tc.cursocampus_id IS NULL)
      AND
      (tc.tipotitulo_id = t.tipotitulo_id OR
       tc.tipotitulo_id IS NULL)
      )
  -- GROUP BY titulo_id
  ORDER BY
    titulo_data_vencimento DESC,
    titulo_data_processamento DESC,
    titulo_data_pagamento DESC,
    alunocurso_id ASC,
    prioridade_por_curso ASC,
    prioridade_por_tipo ASC,
    prioridade_por_periodo ASC,
    prioridade_por_inicio ASC,
    prioridade_por_fim ASC,
    tituloconf_id ASC;

-- view com informações sobre os títulos
DROP VIEW IF EXISTS `view__financeiro_titulos_simplificado`;
DROP TABLE IF EXISTS `view__financeiro_titulos_simplificado`;
CREATE OR REPLACE VIEW `view__financeiro_titulos_simplificado` AS
  SELECT
    t.titulo_id,
    t.pes_id,
    t.tipotitulo_id,
    t.usuario_baixa,
    t.usuario_autor,
    t.titulo_novo,
    t.alunocurso_id,
    ac.aluno_id,
    a.pes_id as pes_id_aluno,
    t.titulo_descricao,
    t.titulo_parcela,
    t.titulo_valor_pago,
    t.titulo_valor_pago_dinheiro,
    t.titulo_multa,
    t.titulo_juros,
    t.titulo_desconto,
    t.titulo_observacoes,
    t.titulo_desconto_manual,
    t.titulo_acrescimo_manual,
    t.titulo_data_processamento,
    t.titulo_data_vencimento,
    t.titulo_data_pagamento,
    t.titulo_data_baixa,
    t.titulo_tipo_pagamento,
    t.titulo_valor,
    t.titulo_estado,
    ac.cursocampus_id,
    greatest(
        if(
            titulo_data_pagamento IS NULL,
            datediff(date(now()), date(t.titulo_data_vencimento)),
            datediff(date(titulo_data_pagamento), date(t.titulo_data_vencimento))
        ),
        0
    )           AS dias_atraso,
    coalesce(
        if(titulo_data_pagamento IS NULL AND
           greatest(
               if(
                   titulo_data_pagamento IS NULL,
                   datediff(date(now()), date(t.titulo_data_vencimento)),
                   datediff(date(titulo_data_pagamento), date(t.titulo_data_vencimento))
               ),
               0
           ) > 0,
           -- configuração de juros e multa para título
             coalesce(tituloconf_multa, 0) * t.titulo_valor,
           titulo_multa
        ),
        0
    )           AS titulo_multa_calc,
    if(
        titulo_data_pagamento IS NULL,
        -- configuração de juros e multa para título
        coalesce(tituloconf_juros, 0) * greatest(
            if(
                titulo_data_pagamento IS NULL,
                datediff(date(now()), date(t.titulo_data_vencimento)),
                datediff(date(titulo_data_pagamento), date(t.titulo_data_vencimento))
            ),
            0
        ),
        coalesce(titulo_juros, 0)
    )           AS titulo_juros_calc,
    (
      SELECT coalesce(sum(desconto_aplicacao_valor), 0)
      FROM view__financeiro_descontos_titulos_simplificado fdt
      WHERE
        desconto_aplicacao_valida = 'Sim' AND
        fdt.titulo_id = t.titulo_id
    )           AS descontos,
    tc.tituloconf_id,
    tc.tituloconf_dia_venc,
    tc.tituloconf_dia_desc,
    tc.tituloconf_valor_desc,
    tc.tituloconf_percent_desc,
    tc.tituloconf_multa,
    tc.tituloconf_juros,
    tc.confcont_id,
    b.bol_id
  FROM financeiro__titulo t
    -- curso aluno
    LEFT JOIN acadgeral__aluno_curso ac ON ac.alunocurso_id = t.alunocurso_id
    LEFT JOIN acadgeral__aluno a ON a.aluno_id = ac.aluno_id
    -- boleto
    LEFT JOIN boleto b ON t.titulo_id = b.titulo_id
    -- usuário baixa
    LEFT JOIN view__financeiro_configuracao_titulos AS tc ON (
      (tc.cursocampus_id = ac.cursocampus_id OR
       tc.cursocampus_id IS NULL)
      AND
      (tc.tipotitulo_id = t.tipotitulo_id OR
       tc.tipotitulo_id IS NULL)
      )
  -- GROUP BY titulo_id
  ORDER BY
    titulo_id ASC,
    alunocurso_id ASC,
    prioridade_por_curso ASC,
    prioridade_por_tipo ASC,
    prioridade_por_periodo ASC,
    prioridade_por_inicio ASC,
    prioridade_por_fim ASC,
    tituloconf_id ASC;

DROP VIEW IF EXISTS `view__financeiro_titulos_totais`;
DROP TABLE IF EXISTS `view__financeiro_titulos_totais`;
CREATE OR REPLACE VIEW `view__financeiro_titulos_totais` AS
  SELECT
    t.*,
    (
      (t.titulo_valor + t.titulo_juros_calc + t.titulo_multa_calc + coalesce(t.titulo_acrescimo_manual, 0)) -
      (t.descontos + coalesce(t.titulo_desconto, 0) + coalesce(t.titulo_desconto_manual, 0))
    ) AS titulo_valor_calc
  FROM view__financeiro_titulos t
  GROUP BY titulo_id
  ORDER BY
    titulo_data_vencimento DESC,
    titulo_data_processamento DESC,
    titulo_data_pagamento DESC,
    alunocurso_id ASC;