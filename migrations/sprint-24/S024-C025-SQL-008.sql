-- MySQL Workbench Synchronization
-- Generated: 2017-02-03 18:07
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `financeiro__valores` 
ADD COLUMN `area_id` INT(10) UNSIGNED ZEROFILL NULL DEFAULT NULL AFTER `tipotitulo_id`,
ADD INDEX `fk_financeiro__valores_acadgeral__area_conhecimento1_idx` (`area_id` ASC);

ALTER TABLE `financeiro__titulo_config` 
ADD COLUMN `tipotitulo_id` INT(11) NULL DEFAULT NULL AFTER `per_id`,
ADD COLUMN `confcont_id` INT(11) NULL DEFAULT NULL AFTER `tipotitulo_id`,
ADD INDEX `fk_financeiro__titulo_config_financeiro__titulo_tipo1_idx` (`tipotitulo_id` ASC),
ADD INDEX `fk_financeiro__titulo_config_boleto_conf_conta1_idx` (`confcont_id` ASC);

ALTER TABLE `financeiro__desconto_titulo` 
ADD COLUMN `desconto_titulo_valor` FLOAT(11) NULL DEFAULT NULL AFTER `desconto_titulo_aplicado`;

CREATE TABLE IF NOT EXISTS `financeiro__permissao_desconto_tipo_titulo` (
  `tipotitulo_id` INT(11) NOT NULL,
  `desctipo_id` INT(11) NOT NULL,
  PRIMARY KEY (`tipotitulo_id`, `desctipo_id`),
  INDEX `fk_financeiro__titulo_tipo_financeiro__desconto_tipo_financ_idx` (`desctipo_id` ASC),
  INDEX `fk_financeiro__titulo_tipo_financeiro__desconto_tipo_financ_idx1` (`tipotitulo_id` ASC),
  CONSTRAINT `fk_financeiro__titulo_tipo_financeiro__desconto_tipo_financei1`
    FOREIGN KEY (`tipotitulo_id`)
    REFERENCES `financeiro__titulo_tipo` (`tipotitulo_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_financeiro__titulo_tipo_financeiro__desconto_tipo_financei2`
    FOREIGN KEY (`desctipo_id`)
    REFERENCES `financeiro__desconto_tipo` (`desctipo_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

ALTER TABLE `financeiro__valores` 
ADD CONSTRAINT `fk_financeiro__valores_acadgeral__area_conhecimento1`
  FOREIGN KEY (`area_id`)
  REFERENCES `acadgeral__area_conhecimento` (`area_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `financeiro__titulo_config` 
ADD CONSTRAINT `fk_financeiro__titulo_config_financeiro__titulo_tipo1`
  FOREIGN KEY (`tipotitulo_id`)
  REFERENCES `financeiro__titulo_tipo` (`tipotitulo_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_financeiro__titulo_config_boleto_conf_conta1`
  FOREIGN KEY (`confcont_id`)
  REFERENCES `boleto_conf_conta` (`confcont_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
