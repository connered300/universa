REPLACE INTO acadgeral__cadastro_origem(origem_nome) VALUES('Agente educacional');
REPLACE INTO acadgeral__cadastro_origem(origem_nome) VALUES('Telemarketing');
REPLACE INTO acadgeral__cadastro_origem(origem_nome) VALUES('Redes sociais');
REPLACE INTO acadgeral__cadastro_origem(origem_nome) VALUES('Indicação de aluno');
REPLACE INTO acadgeral__cadastro_origem(origem_nome) VALUES('E-mail');
REPLACE INTO acadgeral__cadastro_origem(origem_nome) VALUES('EducaEdu');
REPLACE INTO acadgeral__cadastro_origem(origem_nome) VALUES('Google');
REPLACE INTO acadgeral__cadastro_origem(origem_nome) VALUES('INFOJobs');
REPLACE INTO acadgeral__cadastro_origem(origem_nome) VALUES('SINPROPAR');
REPLACE INTO acadgeral__cadastro_origem(origem_nome) VALUES('SMS');
REPLACE INTO acadgeral__cadastro_origem(origem_nome) VALUES('Tutor Pós-Graduação EAD');
REPLACE INTO acadgeral__cadastro_origem(origem_nome) VALUES('TV/Rádio/Jornal');
REPLACE INTO acadgeral__cadastro_origem(origem_nome) VALUES('Outros');

REPLACE INTO org__comunicacao_tipo(tipo_id, tipo_descricao) VALUES (0000000001, 'Acesso Cadastro');
REPLACE INTO org__comunicacao_tipo(tipo_id, tipo_descricao) VALUES (0000000002, 'Acesso Recuperar Senha');
REPLACE INTO org__comunicacao_tipo(tipo_id, tipo_descricao) VALUES (0000000003, 'Vestibular Cadastro');
REPLACE INTO org__comunicacao_tipo(tipo_id, tipo_descricao) VALUES (0000000004, 'Boas Vindas Aluno');
REPLACE INTO org__comunicacao_tipo(tipo_id, tipo_descricao) VALUES (0000000005, 'Boas Vindas Agente');

REPLACE INTO sis__robo (robo_id, robo_nome, robo_url, robo_tempo_execucao, robo_ultima_execucao, robo_situacao)
VALUES (0000000001, 'Envio de Email', '/sistema/sis-fila-email/executar', '25', NULL, 'Ativo');
REPLACE INTO sis__robo (robo_id, robo_nome, robo_url, robo_tempo_execucao, robo_ultima_execucao, robo_situacao)
VALUES (0000000002, 'Envio de SMS', '/sistema/sis-fila-sms/executar', '25', NULL, 'Ativo');
REPLACE INTO sis__robo (robo_id, robo_nome, robo_url, robo_tempo_execucao, robo_ultima_execucao, robo_situacao)
VALUES (0000000003, 'Integração entre sistemas', '/sistema/sis-fila-integracao/executar', '25', NULL, 'Ativo');

-- Criar conta no locaSMS em http://locasms.com.br/#criarconta
REPLACE INTO sis__config(chave,valor) VALUES ('LOCASMS_USER','');
REPLACE INTO sis__config(chave,valor) VALUES ('LOCASMS_PASSWORD','');
REPLACE INTO sis__config(chave,valor) VALUES ('EFETUAR_INTEGRACAO_ALUNO','1');
REPLACE INTO sis__config(chave,valor) VALUES ('EFETUAR_INTEGRACAO_CURSO','1');
REPLACE INTO sis__config(chave,valor) VALUES ('ENVIAR_EMAIL_BOAS_VINDAS_ALUNO','1');
REPLACE INTO sis__config(chave,valor) VALUES ('ENVIAR_SMS_BOAS_VINDAS_ALUNO','1');
REPLACE INTO sis__config(chave,valor) VALUES ('ALUNO_POSSUI_AGENTE_EDUCACIONAL','1');
REPLACE INTO sis__config(chave,valor) VALUES ('ALUNO_POSSUI_UNIDADE_DE_ESTUDO','1');

REPLACE INTO org__email_conta (conta_id, usuario, conta_nome, conta_host, conta_usuario, conta_senha, conta_porta, conta_ssl)
VALUES (1, 1, 'Principal', 'mail.versatecnologia.com.br', 'universa@versatecnologia.com.br', 'uBHEfXV?', 587, null);
REPLACE INTO org__email_tipo(tipo_id,conta_id) VALUES(1,1);
REPLACE INTO org__email_tipo(tipo_id,conta_id) VALUES(2,1);
REPLACE INTO org__email_tipo(tipo_id,conta_id) VALUES(3,1);
REPLACE INTO org__email_tipo(tipo_id,conta_id) VALUES(4,1);
REPLACE INTO org__email_tipo(tipo_id,conta_id) VALUES(5,1);

--
-- módulo sistema
--
DELETE FROM `acesso_privilegios_grupo` WHERE acesso_actions_id IN(
	select acesso_actions.id from acesso_funcionalidades
	inner join acesso_actions on funcionalidade=acesso_funcionalidades.id
	where func_controller IN(
		'Pessoa\\Controller\\Pessoa',
		'Pessoa\\Controller\\PessoaFisica',
		'Pessoa\\Controller\\PessoaJuridica',
		'Matricula\\Controller\\AcadgeralAreaConhecimento',
		'Matricula\\Controller\\AcadgeralCadastroOrigem',
		'Organizacao\\Controller\\OrgAgenteEducacional',
		'Organizacao\\Controller\\OrgEmailConta',
		'Organizacao\\Controller\\OrgComunicacaoModelo',
		'Organizacao\\Controller\\OrgComunicacaoTipo',
		'Organizacao\\Controller\\OrgUnidadeEstudo',
		'Sistema\\Controller\\SisFilaEmail',
		'Sistema\\Controller\\SisFilaIntegracao',
		'Sistema\\Controller\\SisFilaSms',
		'Sistema\\Controller\\SisIntegracaoAluno',
		'Sistema\\Controller\\SisIntegracao',
		'Sistema\\Controller\\SisIntegracaoCurso',
		'Sistema\\Controller\\SisRobo',
		'Sistema\\Controller\\SisConfig'
	)
) AND acesso_actions_id>=0 AND acesso_grupo_id>=0;

DELETE FROM `acesso_actions` WHERE funcionalidade in(
	SELECT acesso_funcionalidades.id from acesso_funcionalidades
	where func_controller IN(
		'Pessoa\\Controller\\Pessoa',
		'Pessoa\\Controller\\PessoaFisica',
		'Pessoa\\Controller\\PessoaJuridica',
		'Matricula\\Controller\\AcadgeralAreaConhecimento',
		'Matricula\\Controller\\AcadgeralCadastroOrigem',
		'Organizacao\\Controller\\OrgAgenteEducacional',
		'Organizacao\\Controller\\OrgEmailConta',
		'Organizacao\\Controller\\OrgComunicacaoModelo',
		'Organizacao\\Controller\\OrgComunicacaoTipo',
		'Organizacao\\Controller\\OrgUnidadeEstudo',
		'Sistema\\Controller\\SisFilaEmail',
		'Sistema\\Controller\\SisFilaIntegracao',
		'Sistema\\Controller\\SisFilaSms',
		'Sistema\\Controller\\SisIntegracaoAluno',
		'Sistema\\Controller\\SisIntegracao',
		'Sistema\\Controller\\SisIntegracaoCurso',
		'Sistema\\Controller\\SisRobo',
		'Sistema\\Controller\\SisConfig'
	)
) and id>0;

DELETE FROM acesso_funcionalidades where func_controller IN(
		'Pessoa\\Controller\\Pessoa',
		'Pessoa\\Controller\\PessoaFisica',
		'Pessoa\\Controller\\PessoaJuridica',
		'Matricula\\Controller\\AcadgeralAreaConhecimento',
		'Matricula\\Controller\\AcadgeralCadastroOrigem',
		'Organizacao\\Controller\\OrgAgenteEducacional',
		'Organizacao\\Controller\\OrgEmailConta',
		'Organizacao\\Controller\\OrgComunicacaoModelo',
		'Organizacao\\Controller\\OrgComunicacaoTipo',
		'Organizacao\\Controller\\OrgUnidadeEstudo',
		'Sistema\\Controller\\SisFilaEmail',
		'Sistema\\Controller\\SisFilaIntegracao',
		'Sistema\\Controller\\SisFilaSms',
		'Sistema\\Controller\\SisIntegracaoAluno',
		'Sistema\\Controller\\SisIntegracao',
		'Sistema\\Controller\\SisIntegracaoCurso',
		'Sistema\\Controller\\SisRobo',
		'Sistema\\Controller\\SisConfig'
) and id>0;

DELETE FROM acesso_modulos where mod_route='sistema/default' and id>0;

DELETE FROM acesso_sistemas where sis_nome='Sistema' and id>0;

REPLACE INTO `acesso_sistemas` (`sis_nome`, `sis_status`, `sis_icon`, `sis_info`)
                       VALUES ('Sistema', 'Ativo', 'fa-gears', 'Sistema');

REPLACE INTO `acesso_modulos` (`sistema`, `mod_nome`, `mod_route`, `mod_status`, `mod_icon`, `mod_info`, `mod_redireciona`, `mod_prioridade`)
SELECT acesso_sistemas.id, 'Sistema', 'sistema/default', 'Ativo', 'fa fa-gears', 'Modúlo Sistema', '/sistema', round(acesso_sistemas.id/10) from acesso_sistemas
where sis_nome like 'Sistema';

REPLACE INTO `acesso_funcionalidades` (modulo, func_nome, func_controller, func_status, func_visivel)
SELECT distinct acesso_modulos.id, func.descricao, func.controller,'Ativa','Sim'  from acesso_modulos,
(
	select 'Gestão de Fila de Email' as descricao, 'Sistema\\Controller\\SisFilaEmail' as controller from dual union all
	select 'Gestão de Fila de Integração' as descricao, 'Sistema\\Controller\\SisFilaIntegracao' as controller from dual union all
	select 'Gestão de Fila de SMS' as descricao, 'Sistema\\Controller\\SisFilaSms' as controller from dual union all
--	select 'Gestão de Integração de Alunos' as descricao, 'Sistema\\Controller\\SisIntegracaoAluno' as controller from dual union all
	select 'Gestão de Integração' as descricao, 'Sistema\\Controller\\SisIntegracao' as controller from dual union all
--	select 'Gestão de Integração de Cursos' as descricao, 'Sistema\\Controller\\SisIntegracaoCurso' as controller from dual union all
	select 'Gestão de Robô' as descricao, 'Sistema\\Controller\\SisRobo' as controller from dual UNION ALL
	select 'Gestão de Configurações' as descricao, 'Sistema\\Controller\\SisConfig' as controller from dual
) as func
where mod_nome like 'Sistema';

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
(
	select 'index' as act,'Listagem' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'remove' as act,'Remover' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'add' as act,'Criar' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'edit' as act,'Editar' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'search-for-json' as act, 'Autocomplete' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'search' as act,'Datatables' as descricao,'Leitura' as tipo,'N' as usr from dual
) as actions
where func_controller IN(
		'Sistema\\Controller\\SisFilaEmail',
		'Sistema\\Controller\\SisFilaIntegracao',
		'Sistema\\Controller\\SisFilaSms',
		-- 'Sistema\\Controller\\SisIntegracaoAluno',
		'Sistema\\Controller\\SisIntegracao',
		-- 'Sistema\\Controller\\SisIntegracaoCurso',
		'Sistema\\Controller\\SisRobo',
		'Sistema\\Controller\\SisConfig'
);
REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
acesso_grupo
where func_controller IN(
		'Sistema\\Controller\\SisFilaEmail',
		'Sistema\\Controller\\SisFilaIntegracao',
		'Sistema\\Controller\\SisFilaSms',
		-- 'Sistema\\Controller\\SisIntegracaoAluno',
		'Sistema\\Controller\\SisIntegracao',
		-- 'Sistema\\Controller\\SisIntegracaoCurso',
		'Sistema\\Controller\\SisRobo',
		'Sistema\\Controller\\SisConfig'
)
and grup_nome LIKE 'Admin%';



--
-- Módulo matrícula
--
REPLACE INTO `acesso_funcionalidades` (modulo, func_nome, func_controller, func_status, func_visivel)
SELECT distinct acesso_modulos.id, func.descricao, func.controller,'Ativa','Sim'  from acesso_modulos,
(
	select 'Gestão de Áreas de conhecimento' as descricao, 'Matricula\\Controller\\AcadgeralAreaConhecimento' as controller from dual union all
	select 'Gestão de origem de cadastro' as descricao, 'Matricula\\Controller\\AcadgeralCadastroOrigem' as controller from dual
) as func
where mod_nome like 'Secretaria';

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
(
	select 'index' as act,'Listagem' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'remove' as act,'Remover' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'add' as act,'Criar' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'edit' as act,'Editar' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'search-for-json' as act, 'Autocomplete' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'search' as act,'Datatables' as descricao,'Leitura' as tipo,'N' as usr from dual
) as actions
where func_controller IN(
	'Matricula\\Controller\\AcadgeralAreaConhecimento',
	'Matricula\\Controller\\AcadgeralCadastroOrigem'
);
REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
acesso_grupo
where func_controller IN(
	'Matricula\\Controller\\AcadgeralAreaConhecimento',
	'Matricula\\Controller\\AcadgeralCadastroOrigem'
)
and grup_nome LIKE 'Admin%';

--
-- Módulo pessoa
--

DELETE FROM `acesso_privilegios_grupo` WHERE acesso_actions_id IN(
	select acesso_actions.id from acesso_funcionalidades
	inner join acesso_actions on funcionalidade=acesso_funcionalidades.id
	where func_controller IN(
	'Pessoa\\Controller\\Pessoa',
	'Pessoa\\Controller\\PessoaFisica',
	'Pessoa\\Controller\\PessoaJuridica',
	'Pessoa\\Controller\\Contato',
	'Pessoa\\Controller\\TipoContato'
	)
) AND acesso_actions_id>=0 AND acesso_grupo_id>=0;

DELETE FROM `acesso_actions` WHERE funcionalidade in(
	SELECT acesso_funcionalidades.id from acesso_funcionalidades
	where func_controller IN(
	'Pessoa\\Controller\\Pessoa',
	'Pessoa\\Controller\\PessoaFisica',
	'Pessoa\\Controller\\PessoaJuridica',
	'Pessoa\\Controller\\Contato',
	'Pessoa\\Controller\\TipoContato'
	)
) and id>0;

DELETE FROM acesso_funcionalidades where func_controller IN(
	'Pessoa\\Controller\\Pessoa',
	'Pessoa\\Controller\\PessoaFisica',
	'Pessoa\\Controller\\PessoaJuridica',
	'Pessoa\\Controller\\Contato',
	'Pessoa\\Controller\\TipoContato'
) and id>0;

DELETE FROM acesso_modulos where mod_route='pessoa/default' and id>0;

DELETE FROM acesso_sistemas where sis_nome='Pessoa' and id>0;

REPLACE INTO `acesso_sistemas` (`sis_nome`, `sis_status`, `sis_icon`, `sis_info`)
                       VALUES ('Sistema', 'Ativo', 'fa-gears', 'Sistema');

REPLACE INTO `acesso_sistemas` (`sis_nome`, `sis_status`, `sis_icon`, `sis_info`)
VALUES ('Pessoa', 'Ativo', 'fa-user', 'Pessoa');

REPLACE INTO `acesso_modulos` (`sistema`, `mod_nome`, `mod_route`, `mod_status`, `mod_icon`, `mod_info`, `mod_redireciona`, `mod_prioridade`)
SELECT acesso_sistemas.id, 'Pessoa', 'pessoa/default', 'Ativo', 'fa fa-group', 'Modúlo pessoa', '/sistema', round(acesso_sistemas.id/10) from acesso_sistemas
where sis_nome like 'Pessoa';

REPLACE INTO `acesso_funcionalidades` (modulo, func_nome, func_controller, func_status, func_visivel)
SELECT distinct acesso_modulos.id, func.descricao, func.controller,'Ativa','Sim'  from acesso_modulos,
(
	select 'Gestão de Pessoa' as descricao, 'Pessoa\\Controller\\Pessoa' as controller from dual union all
	select 'Gestão de Pessoas Físicas' as descricao, 'Pessoa\\Controller\\PessoaFisica' as controller from dual union all
	select 'Gestão de Pessoas Jurídicas' as descricao, 'Pessoa\\Controller\\PessoaJuridica' as controller from dual union all
	select 'Gestão de Tipos de contato' as descricao, 'Pessoa\\Controller\\TipoContato' as controller from dual union all
	select 'Gestão de Contatos de Pessoas' as descricao, 'Pessoa\\Controller\\Contato' as controller from dual
) as func
where mod_nome like 'Pessoa';

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
(
	select 'index' as act,'Listagem' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'remove' as act,'Remover' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'add' as act,'Criar' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'edit' as act,'Editar' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'search-for-json' as act, 'Autocomplete' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'search' as act,'Datatables' as descricao,'Leitura' as tipo,'N' as usr from dual
) as actions
where func_controller IN(
	'Pessoa\\Controller\\Pessoa',
	'Pessoa\\Controller\\PessoaFisica',
	'Pessoa\\Controller\\PessoaJuridica',
	'Pessoa\\Controller\\Contato',
	'Pessoa\\Controller\\TipoContato'
);
REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
acesso_grupo
where func_controller IN(
	'Pessoa\\Controller\\Pessoa',
	'Pessoa\\Controller\\PessoaFisica',
	'Pessoa\\Controller\\PessoaJuridica',
	'Pessoa\\Controller\\Contato',
	'Pessoa\\Controller\\TipoContato'
)
and grup_nome LIKE 'Admin%';

--
-- Módulo organização
--
REPLACE INTO `acesso_funcionalidades` (modulo, func_nome, func_controller, func_status, func_visivel)
SELECT distinct acesso_modulos.id, func.descricao, func.controller,'Ativa','Sim'  from acesso_modulos,
(
	select 'Gestão de Agentes Educacionais' as descricao, 'Organizacao\\Controller\\OrgAgenteEducacional' as controller from dual union all
	select 'Gestão de Contas de Email' as descricao, 'Organizacao\\Controller\\OrgEmailConta' as controller from dual union all
	select 'Gestão de Modelos de Comunicação' as descricao, 'Organizacao\\Controller\\OrgComunicacaoModelo' as controller from dual union all
	select 'Gestão de Tipo de Comunicação' as descricao, 'Organizacao\\Controller\\OrgComunicacaoTipo' as controller from dual union all
	select 'Gestão de Unidades de Estudo' as descricao, 'Organizacao\\Controller\\OrgUnidadeEstudo' as controller from dual
) as func
where mod_nome like 'Organização';

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
(
	select 'index' as act,'Listagem' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'remove' as act,'Remover' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'add' as act,'Criar' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'edit' as act,'Editar' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'search-for-json' as act, 'Autocomplete' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'search' as act,'Datatables' as descricao,'Leitura' as tipo,'N' as usr from dual
) as actions
where func_controller IN(
	'Organizacao\\Controller\\OrgAgenteEducacional',
	'Organizacao\\Controller\\OrgEmailConta',
	'Organizacao\\Controller\\OrgComunicacaoModelo',
	'Organizacao\\Controller\\OrgComunicacaoTipo',
	'Organizacao\\Controller\\OrgUnidadeEstudo'
);
REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
acesso_grupo
where func_controller IN(
	'Organizacao\\Controller\\OrgAgenteEducacional',
	'Organizacao\\Controller\\OrgEmailConta',
	'Organizacao\\Controller\\OrgComunicacaoModelo',
	'Organizacao\\Controller\\OrgComunicacaoTipo',
	'Organizacao\\Controller\\OrgUnidadeEstudo'
)
and grup_nome LIKE 'Admin%';