REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT DISTINCT
    acesso_funcionalidades.id,
    actions.act,
    actions.descricao,
    actions.tipo,
    actions.usr
  FROM acesso_funcionalidades,
    (
      SELECT
        'index'    AS act,
        'Listagem' AS descricao,
        'Leitura'  AS tipo,
        'N'        AS usr
      FROM dual
      UNION ALL
      SELECT
        'renegociacao' AS act,
        'Renegociação' AS descricao,
        'Escrita'      AS tipo,
        'N'            AS usr
      FROM dual
      UNION ALL
      SELECT
        'pagamento'  AS act,
        'Pagamento' AS descricao,
        'Escrita'    AS tipo,
        'N'          AS usr
      FROM dual
      UNION ALL
      SELECT
        'novo-titulo'  AS act,
        'Novo Título' AS descricao,
        'Escrita'    AS tipo,
        'N'          AS usr
      FROM dual
      UNION ALL
      SELECT
        'cancelar-titulo'  AS act,
        'Cancelar Título' AS descricao,
        'Escrita'    AS tipo,
        'N'          AS usr
      FROM dual
      UNION ALL
      SELECT
        'enviar-titulos'  AS act,
        'Enviar Títulos' AS descricao,
        'Escrita'    AS tipo,
        'N'          AS usr
      FROM dual
    ) AS actions
  WHERE func_controller IN (
    'Financeiro\\Controller\\Painel'
  );
REPLACE INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
    ,
    acesso_grupo
  WHERE func_controller IN (
    'Financeiro\\Controller\\Painel'
  )
        AND grup_nome LIKE 'Admin%';

ALTER TABLE `financeiro__titulo` CHANGE COLUMN `titulo_estado` `titulo_estado` ENUM('Pago', 'Aberto', 'Cancelado', 'Estorno', 'Alteracao', 'Pagamento Múltiplo') NULL DEFAULT NULL;
ALTER TABLE `financeiro__titulo_alteracao` CHANGE COLUMN `alteracao_acao` `alteracao_acao` ENUM('Atualizacao', 'Parcelamento', 'Renegociacao', 'Pagamento Múltiplo') NULL DEFAULT NULL;
