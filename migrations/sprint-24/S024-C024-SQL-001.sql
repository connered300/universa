-- MySQL Workbench Synchronization
-- Generated: 2016-12-14 09:48
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `org_campus` DROP FOREIGN KEY `fk_org_campus_endereco1`;

ALTER TABLE `pessoa_fisica` DROP FOREIGN KEY `fk_pessoa_fisica_pessoa1`;


ALTER TABLE `org_campus` DROP COLUMN `end_id`;
ALTER TABLE `org_campus` ADD COLUMN `arq_id` INT(10) UNSIGNED ZEROFILL NULL DEFAULT NULL AFTER `secretaria_pes_id`;
ALTER TABLE `org_campus` ADD INDEX `fk_org_campus_arquivo1_idx` (`arq_id` ASC);
ALTER TABLE `org_campus` DROP INDEX `fk_org_campus_endereco1_idx`;

ALTER TABLE `acad_curso` CHARACTER SET = utf8 , COLLATE = utf8_general_ci ;
ALTER TABLE `acad_curso` ADD COLUMN `area_id` INT(10) UNSIGNED ZEROFILL NULL DEFAULT NULL AFTER `nivel_id`;
ALTER TABLE `acad_curso` ADD COLUMN `curso_prazo_integralizacao_maxima` INT(11) NULL DEFAULT NULL AFTER `curso_prazo_integralizacao`;
ALTER TABLE `acad_curso` ADD INDEX `fk_acad_curso_acadgeral__area_conhecimento1_idx` (`area_id` ASC);

ALTER TABLE `pessoa_fisica` CHANGE COLUMN `pes_estado_civil` `pes_estado_civil` ENUM('Solteiro', 'Casado', 'União Estável', 'Viúvo', 'Divorciado') NULL DEFAULT NULL ;

ALTER TABLE `org_ies` ADD COLUMN `diretoria_pes_id` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL COMMENT 'Secretária Geral' AFTER `ies_organizacao_academica`;
ALTER TABLE `org_ies` ADD COLUMN `secretaria_pes_id` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL COMMENT 'Secretária Geral' AFTER `diretoria_pes_id`;
ALTER TABLE `org_ies` ADD COLUMN `arq_id` INT(10) UNSIGNED ZEROFILL NULL DEFAULT NULL AFTER `secretaria_pes_id`;
ALTER TABLE `org_ies` ADD INDEX `fk_org_ies_pessoa_fisica1_idx` (`diretoria_pes_id` ASC);
ALTER TABLE `org_ies` ADD INDEX `fk_org_ies_pessoa_fisica2_idx` (`secretaria_pes_id` ASC);
ALTER TABLE `org_ies` ADD INDEX `fk_org_ies_arquivo1_idx` (`arq_id` ASC);

ALTER TABLE `selecao_edicao` ADD COLUMN `edicao_filiacao` ENUM('Não','Sim') NOT NULL DEFAULT 'Não' AFTER `edicao_semestre`;
ALTER TABLE `selecao_edicao` ADD COLUMN `edicao_isencao` ENUM('Não','Sim') NOT NULL DEFAULT 'Não' AFTER `edicao_filiacao`;
ALTER TABLE `selecao_edicao` ADD COLUMN `edicao_num_cursos_opcionais` INT(11) NOT NULL DEFAULT '0' AFTER `edicao_isencao`;

ALTER TABLE `infra_predio` ADD COLUMN `predio_tipo` ENUM('principal','apoio') NOT NULL DEFAULT 'principal' AFTER `predio_logradouro`;

ALTER TABLE `acadgeral__aluno` ADD COLUMN `origem_id` INT(10) ZEROFILL UNSIGNED NULL DEFAULT NULL AFTER `aluno_cert_militar`;
ALTER TABLE `acadgeral__aluno` ADD COLUMN `pes_id_agenciador` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL AFTER `origem_id`;
ALTER TABLE `acadgeral__aluno` ADD COLUMN `unidade_id` INT(11) ZEROFILL NULL DEFAULT NULL AFTER `pes_id_agenciador`;
ALTER TABLE `acadgeral__aluno` ADD INDEX `fk_acadgeral__aluno_cadastro__origem1_idx` (`origem_id` ASC);
ALTER TABLE `acadgeral__aluno` ADD INDEX `fk_acadgeral__aluno_org__agente_educacional1_idx` (`pes_id_agenciador` ASC);
ALTER TABLE `acadgeral__aluno` ADD INDEX `fk_acadgeral__aluno_org__unidade_estudo1_idx` (`unidade_id` ASC);

ALTER TABLE `acadgeral__aluno_curso` ADD COLUMN `pes_id_agente` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL AFTER `tiposel_id`;
ALTER TABLE `acadgeral__aluno_curso` ADD COLUMN `alunocurso_situacao` ENUM('Deferido', 'Pendente') NOT NULL DEFAULT 'Deferido' AFTER `alunocurso_data_expedicao_diploma`;
ALTER TABLE `acadgeral__aluno_curso` ADD INDEX `fk_acadgeral__aluno_curso_org__agente_educacional1_idx` (`pes_id_agente` ASC);

ALTER TABLE `biblioteca__exemplar` ADD COLUMN `usuario` INT(11) UNSIGNED NOT NULL AFTER `exemplar_localizacao`;
ALTER TABLE `biblioteca__exemplar` ADD INDEX `fk_biblioteca__exemplar_usuario_idx` (`usuario` ASC);

ALTER TABLE `acadgeral__aluno_formacao` CHANGE COLUMN `formacao_ano` `formacao_ano` INT(11) NOT NULL ;

CREATE TABLE IF NOT EXISTS `sis__integracao` (
  `integracao_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `usuario` INT(11) UNSIGNED ZEROFILL NOT NULL,
  `integracao_descricao` VARCHAR(100) NOT NULL,
  `integracao_tipo` ENUM('Versa AVA') NOT NULL,
  `integracao_token` VARCHAR(50) NOT NULL,
  `integracao_url` TEXT NOT NULL,
  `integracao_cabecalho_envio` TEXT NULL DEFAULT NULL,
  `integracao_situacao` ENUM('Ativo', 'Inativo') NOT NULL,
  PRIMARY KEY (`integracao_id`),
  INDEX `fk_integracao_acesso_pessoas1_idx` (`usuario` ASC),
  CONSTRAINT `fk_integracao_acesso_pessoas1`
    FOREIGN KEY (`usuario`)
    REFERENCES `acesso_pessoas` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `sis__fila_integracao` (
  `fila_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `integracao_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  `fila_conteudo` TEXT NOT NULL,
  `fila_retorno` TEXT NULL DEFAULT NULL,
  `fila_data_cadastro` DATETIME NOT NULL,
  `fila_data_processamento` DATETIME NULL DEFAULT NULL,
  `fila_data_finalizacao` DATETIME NULL DEFAULT NULL,
  `fila_situacao` ENUM('Pendente', 'Enviado', 'Processando','Erro') NOT NULL DEFAULT 'Pendente',
  PRIMARY KEY (`fila_id`),
  INDEX `fk_integracao_fila_integracao1_idx` (`integracao_id` ASC),
  CONSTRAINT `fk_integracao_fila_integracao1`
    FOREIGN KEY (`integracao_id`)
    REFERENCES `sis__integracao` (`integracao_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `sis__fila_email` (
  `fila_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `conta_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  `fila_assunto` VARCHAR(250) NULL DEFAULT NULL,
  `fila_destinatario_nome` VARCHAR(100) NULL DEFAULT NULL,
  `fila_destinatario_email` VARCHAR(100) NOT NULL,
  `fila_conteudo` TEXT NOT NULL,
  `fila_retorno` TEXT NULL DEFAULT NULL,
  `fila_data_cadastro` DATETIME NOT NULL,
  `fila_data_processamento` DATETIME NULL DEFAULT NULL,
  `fila_data_finalizacao` DATETIME NULL DEFAULT NULL,
  `fila_situacao` ENUM('Pendente', 'Enviado', 'Processando','Erro') NOT NULL DEFAULT 'Pendente',
  PRIMARY KEY (`fila_id`),
  INDEX `fk_sis__fila_email_org__email__conta1_idx` (`conta_id` ASC),
  CONSTRAINT `fk_sis__fila_email_org__email__conta1`
    FOREIGN KEY (`conta_id`)
    REFERENCES `org__email_conta` (`conta_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `org__email_conta` (
  `conta_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `usuario` INT(11) UNSIGNED ZEROFILL NOT NULL,
  `conta_nome` VARCHAR(100) NOT NULL,
  `conta_host` TEXT NOT NULL,
  `conta_usuario` VARCHAR(100) NOT NULL,
  `conta_senha` VARCHAR(100) NOT NULL,
  `conta_porta` INT(11) NOT NULL,
  `conta_ssl` ENUM('ssl', 'tls') NULL DEFAULT NULL,
  PRIMARY KEY (`conta_id`),
  INDEX `fk_email__conta_acesso_pessoas1_idx` (`usuario` ASC),
  CONSTRAINT `fk_email__conta_acesso_pessoas1`
    FOREIGN KEY (`usuario`)
    REFERENCES `acesso_pessoas` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `org__comunicacao_modelo` (
  `modelo_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `tipo_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  `usuario` INT(11) UNSIGNED ZEROFILL NOT NULL,
  `modelo_nome` VARCHAR(45) NOT NULL,
  `modelo_tipo` ENUM('Email','SMS') NULL DEFAULT NULL,
  `modelo_assunto` VARCHAR(250) NOT NULL,
  `modelo_conteudo` TEXT NOT NULL,
  `modelo_data_inicio` DATETIME NOT NULL,
  `modelo_data_fim` DATETIME NOT NULL,
  PRIMARY KEY (`modelo_id`),
  INDEX `fk_email__modelo_acesso_pessoas1_idx` (`usuario` ASC),
  INDEX `fk_org__comunicacao_modelo_org__comunicacao_tipo1_idx` (`tipo_id` ASC),
  CONSTRAINT `fk_email__modelo_acesso_pessoas1`
    FOREIGN KEY (`usuario`)
    REFERENCES `acesso_pessoas` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_org__comunicacao_modelo_org__comunicacao_tipo1`
    FOREIGN KEY (`tipo_id`)
    REFERENCES `org__comunicacao_tipo` (`tipo_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `sis__integracao_curso` (
  `integracao_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  `curso_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  `codigo` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`integracao_id`, `curso_id`),
  INDEX `fk_acad_curso_fila__integracao_acad_curso1_idx` (`curso_id` ASC),
  INDEX `fk_sis__integracao_curso_sis__integracao1_idx` (`integracao_id` ASC),
  CONSTRAINT `fk_sis__integracao_curso_acad_curso1`
    FOREIGN KEY (`curso_id`)
    REFERENCES `acad_curso` (`curso_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_sis__integracao_curso_sis__integracao1`
    FOREIGN KEY (`integracao_id`)
    REFERENCES `sis__integracao` (`integracao_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = big5
COLLATE = big5_chinese_ci;

CREATE TABLE IF NOT EXISTS `sis__integracao_aluno` (
  `integracao_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  `aluno_id` INT(10) UNSIGNED ZEROFILL NOT NULL,
  `codigo` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`integracao_id`, `aluno_id`),
  INDEX `fk_fila__integracao_acadgeral__aluno_acadgeral__aluno1_idx` (`aluno_id` ASC),
  INDEX `fk_sis__integracao_aluno_sis__integracao1_idx` (`integracao_id` ASC),
  CONSTRAINT `fk_fila__integracao_acadgeral__aluno_acadgeral__aluno1`
    FOREIGN KEY (`aluno_id`)
    REFERENCES `acadgeral__aluno` (`aluno_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_sis__integracao_aluno_sis__integracao1`
    FOREIGN KEY (`integracao_id`)
    REFERENCES `sis__integracao` (`integracao_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `org__agente_educacional` (
  `pes_id` INT(11) UNSIGNED ZEROFILL NOT NULL,
  `pes_id_indicacao` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL,
  `origem_id` INT(10) ZEROFILL UNSIGNED NULL DEFAULT NULL,
  `agente_max_nivel` INT(1) NOT NULL DEFAULT 2,
  `agente_comissao` FLOAT(11) NOT NULL,
  `agente_situacao` ENUM('Ativo', 'Inativo') NOT NULL,
  PRIMARY KEY (`pes_id`),
  INDEX `fk_agente_educacional_agente_educacional1_idx` (`pes_id_indicacao` ASC),
  INDEX `fk_org__agente_educacional_org__cadastro_origem1_idx` (`origem_id` ASC),
  CONSTRAINT `fk_agente_educacional_pessoa_fisica1`
    FOREIGN KEY (`pes_id`)
    REFERENCES `pessoa_fisica` (`pes_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_agente_educacional_agente_educacional1`
    FOREIGN KEY (`pes_id_indicacao`)
    REFERENCES `org__agente_educacional` (`pes_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_org__agente_educacional_org__cadastro_origem1`
    FOREIGN KEY (`origem_id`)
    REFERENCES `acadgeral__cadastro_origem` (`origem_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `acadgeral__cadastro_origem` (
  `origem_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `origem_nome` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`origem_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `org__unidade_estudo` (
  `unidade_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `unidade_nome` VARCHAR(45) NOT NULL,
  `pes_id` INT(11) UNSIGNED ZEROFILL NOT NULL,
  `camp_id` INT(11) ZEROFILL NOT NULL,
  `unidade_end_numero` INT(11) NULL DEFAULT NULL,
  `unidade_end_cep` VARCHAR(45) NULL DEFAULT NULL,
  `unidade_end_bairro` VARCHAR(45) NULL DEFAULT NULL,
  `unidade_end_cidade` VARCHAR(45) NULL DEFAULT NULL,
  `unidade_end_estado` VARCHAR(45) NULL DEFAULT NULL,
  `unidade_end_logradouro` VARCHAR(45) NULL DEFAULT NULL,
  `unidade_telefone` VARCHAR(15) NULL DEFAULT NULL,
  `unidade_email` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`unidade_id`),
  INDEX `fk_infra_predio_org_campus11_idx` (`camp_id` ASC),
  INDEX `fk_org__unidade_estudo_agente_educacional1_idx` (`pes_id` ASC),
  CONSTRAINT `fk_infra_predio_org_campus10`
    FOREIGN KEY (`camp_id`)
    REFERENCES `org_campus` (`camp_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_org__unidade_estudo_agente_educacional1`
    FOREIGN KEY (`pes_id`)
    REFERENCES `org__agente_educacional` (`pes_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = 'Informações sobre o prédio\n';

CREATE TABLE IF NOT EXISTS `sis__fila_sms` (
  `fila_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `fila_destinatario` VARCHAR(45) NOT NULL,
  `fila_provedor` ENUM('LOCASMS') NULL DEFAULT NULL,
  `fila_conteudo` TEXT NOT NULL,
  `fila_retorno` TEXT NULL DEFAULT NULL,
  `fila_data_cadastro` DATETIME NOT NULL,
  `fila_data_processamento` DATETIME NULL DEFAULT NULL,
  `fila_data_finalizacao` DATETIME NULL DEFAULT NULL,
  `fila_situacao` ENUM('Pendente', 'Enviado', 'Processando','Erro') NOT NULL DEFAULT 'Pendente',
  PRIMARY KEY (`fila_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `sis__robo` (
  `robo_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `robo_nome` VARCHAR(45) NOT NULL,
  `robo_url` TEXT NOT NULL,
  `robo_tempo_execucao` INT(11) NOT NULL DEFAULT 120,
  `robo_ultima_execucao` DATETIME NULL DEFAULT NULL,
  `robo_situacao` ENUM('Ativo', 'Inativo', 'Processando') NOT NULL,
  PRIMARY KEY (`robo_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `org__comunicacao_tipo` (
  `tipo_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `tipo_descricao` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`tipo_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `sis__config` (
  `chave` VARCHAR(255) NOT NULL,
  `valor` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`chave`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `org__email_tipo` (
  `conta_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  `tipo_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  PRIMARY KEY (`conta_id`, `tipo_id`),
  INDEX `fk_org__email_conta_org__comunicacao_tipo_org__comunicacao__idx` (`tipo_id` ASC),
  INDEX `fk_org__email_conta_org__comunicacao_tipo_org__email_conta1_idx` (`conta_id` ASC),
  CONSTRAINT `fk_org__email_conta_org__comunicacao_tipo_org__email_conta1`
    FOREIGN KEY (`conta_id`)
    REFERENCES `org__email_conta` (`conta_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_org__email_conta_org__comunicacao_tipo_org__comunicacao_ti1`
    FOREIGN KEY (`tipo_id`)
    REFERENCES `org__comunicacao_tipo` (`tipo_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

ALTER TABLE `org_campus` 
ADD CONSTRAINT `fk_org_campus_arquivo1`
  FOREIGN KEY (`arq_id`)
  REFERENCES `arquivo` (`arq_id`)
  ON DELETE SET NULL
  ON UPDATE CASCADE;

ALTER TABLE `acad_curso` 
ADD CONSTRAINT `fk_acad_curso_acadgeral__area_conhecimento1`
  FOREIGN KEY (`area_id`)
  REFERENCES `acadgeral__area_conhecimento` (`area_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `pessoa_fisica` 
ADD CONSTRAINT `fk_pessoa_fisica_pessoa1`
  FOREIGN KEY (`pes_id`)
  REFERENCES `pessoa` (`pes_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `org_ies` 
ADD CONSTRAINT `fk_org_ies_arquivo1`
  FOREIGN KEY (`arq_id`)
  REFERENCES `arquivo` (`arq_id`)
  ON DELETE SET NULL
  ON UPDATE CASCADE;

ALTER TABLE `org_ies`
ADD CONSTRAINT `fk_org_ies_pessoa_fisica1`
  FOREIGN KEY (`diretoria_pes_id`)
  REFERENCES `pessoa_fisica` (`pes_id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `org_ies`
ADD CONSTRAINT `fk_org_ies_pessoa_fisica2`
  FOREIGN KEY (`secretaria_pes_id`)
  REFERENCES `pessoa_fisica` (`pes_id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `acadgeral__aluno` 
ADD CONSTRAINT `fk_acadgeral__aluno_cadastro__origem1`
  FOREIGN KEY (`origem_id`)
  REFERENCES `acadgeral__cadastro_origem` (`origem_id`)
  ON DELETE SET NULL
  ON UPDATE CASCADE;

ALTER TABLE `acadgeral__aluno`
ADD CONSTRAINT `fk_acadgeral__aluno_org__agente_educacional1`
  FOREIGN KEY (`pes_id_agenciador`)
  REFERENCES `org__agente_educacional` (`pes_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `acadgeral__aluno`
ADD CONSTRAINT `fk_acadgeral__aluno_org__unidade_estudo1`
  FOREIGN KEY (`unidade_id`)
  REFERENCES `org__unidade_estudo` (`unidade_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `acadgeral__aluno_curso` 
ADD CONSTRAINT `fk_acadgeral__aluno_curso_org__agente_educacional1`
  FOREIGN KEY (`pes_id_agente`)
  REFERENCES `org__agente_educacional` (`pes_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


-- MySQL Workbench Synchronization
-- Generated: 2016-12-22 10:37
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `acad_curso` 
DROP FOREIGN KEY `fk_acad_curso_acadgeral__area_conhecimento1`;

ALTER TABLE `acad_curso` 
CHANGE COLUMN `curso_nome` `curso_nome` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NOT NULL COMMENT 'Nome do curso.' ,
CHANGE COLUMN `curso_periodicidade` `curso_periodicidade` ENUM('semestral','anual','Quadrimestral','Trimestral') CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NOT NULL COMMENT 'É o período em que o curso forma novas turmas' ,
CHANGE COLUMN `curso_turno` `curso_turno` SET('Noturno','Vespertino','Matutino','Integral') NOT NULL COMMENT 'Turno de aplicação do curso.' ,
CHANGE COLUMN `curso_numero_ocde` `curso_numero_ocde` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NULL DEFAULT NULL COMMENT 'Identificacao do curso no INEP.' ,
CHANGE COLUMN `curso_sigla` `curso_sigla` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NULL DEFAULT NULL ,
CHANGE COLUMN `curso_numero_mec` `curso_numero_mec` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NULL DEFAULT NULL COMMENT 'Recebe a sigla do Curso.' ,
CHANGE COLUMN `curso_informacoes` `curso_informacoes` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NULL DEFAULT NULL COMMENT 'Informações sobre o curso. Como perfil do egresso e área de atuação.' ;

ALTER TABLE `acad_curso` 
ADD CONSTRAINT `fk_acad_curso_acadgeral__area_conhecimento1`
  FOREIGN KEY (`area_id`)
  REFERENCES `acadgeral__area_conhecimento` (`area_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- MySQL Workbench Synchronization
-- Generated: 2016-12-22 16:51
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DELETE from contato where pessoa not in(select pes_id from pessoa) AND con_id>=0;

ALTER TABLE `endereco`
DROP FOREIGN KEY `fk_endereco_pessoa1`;

ALTER TABLE `contato`
ENGINE = InnoDB ,
ADD INDEX `fk_contato_pessoa1_idx` (`pessoa` ASC),
DROP INDEX `fk_contato_pessoas1_idx`;

ALTER TABLE `contato`
ADD CONSTRAINT `fk_contato_tipo_contato1`
FOREIGN KEY (`tipo_contato`)
REFERENCES `tipo_contato` (`tip_contato_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
ALTER TABLE `contato`
ADD CONSTRAINT `fk_contato_pessoa1`
FOREIGN KEY (`pessoa`)
REFERENCES `pessoa` (`pes_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `endereco`
ADD CONSTRAINT `fk_endereco_pessoa1`
FOREIGN KEY (`pes_id`)
REFERENCES `pessoa` (`pes_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;


ALTER TABLE org__email_conta CHANGE COLUMN `conta_ssl` `conta_ssl` ENUM('ssl', 'tls') NULL DEFAULT NULL;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

ALTER TABLE `acad_curso`
CHANGE COLUMN `curso_nome` `curso_nome` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NOT NULL COMMENT 'Nome do curso.' ;