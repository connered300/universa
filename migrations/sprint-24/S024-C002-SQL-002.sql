-- MySQL Workbench Synchronization
-- Generated: 2016-11-22 12:08
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


-- --------------------------------------------------------

--
-- Estrutura da tabela `acadgeral__aluno_atividade`
--

CREATE TABLE IF NOT EXISTS `acadgeral__aluno_atividade` (
  `alunoatividade_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `alunocurso_id` int(10) unsigned zerofill NOT NULL,
  `alunonucleo_id` int(10) unsigned zerofill NOT NULL,
  `alunoatividade_total_simulado` int(11) DEFAULT NULL,
  `alunoatividade_total_pratico` int(11) DEFAULT NULL,
  `alunoatividade_portfolio_pontuacao` float DEFAULT NULL,
  `alunoatividade_serie` int(11) DEFAULT NULL,
  `alunoatividade_situacao` enum('Aprovado','Reprovado') DEFAULT NULL,
  PRIMARY KEY (`alunoatividade_id`),
  KEY `fk_acadgeral__aluno_atividade_acadgeral__aluno_curso1_idx` (`alunocurso_id`),
  KEY `fk_acadgeral__aluno_atividade_aluno_nucleo1_idx` (`alunonucleo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `atividadegeral__atividades`
--

CREATE TABLE IF NOT EXISTS `atividadegeral__atividades` (
  `atividadeatividade_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `atividadeatividade_descricao` varchar(45) NOT NULL,
  `atividadeatividade_observacoes` text NOT NULL,
  PRIMARY KEY (`atividadeatividade_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `atividadegeral__atividades_disciplina`
--

CREATE TABLE IF NOT EXISTS `atividadegeral__atividades_disciplina` (
  `atividadedisc_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `atividadeconfatividade_id` int(10) unsigned NOT NULL,
  `disc_id` int(10) unsigned zerofill NOT NULL,
  PRIMARY KEY (`atividadedisc_id`),
  KEY `fk_atividadegeral__atividades_disciplina_config_idx` (`atividadeconfatividade_id`),
  KEY `fk_atividadegeral__atividades_disciplina__disciplina_idx` (`disc_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `atividadegeral__configuracoes`
--

CREATE TABLE IF NOT EXISTS `atividadegeral__configuracoes` (
  `atividadeconf_portaria` varchar(45) NOT NULL,
  `atividadeconf_maximo_duracao` int(11) NOT NULL COMMENT 'Data duração das atividades prestadas em meses. ',
  `atividadeconf_numero_decreto` varchar(45) NOT NULL,
  `atividadeconf_data_decreto` datetime NOT NULL COMMENT 'Data que foi decretado as normas.',
  `atividadecarga_total_pontuacao` int(11) NOT NULL COMMENT 'Total minímo para aprovação do aluno na disciplina.',
  `atividadeconf_carga_total_horas` int(11) NOT NULL COMMENT 'Total de horas minímas para aprovação do aluno na disciplina. ',
  `atividadeconf_serie` set('1','2','3','4','5','6','7','8','9','10') NOT NULL COMMENT 'Séries que podem prestar disciplinas. ',
  PRIMARY KEY (`atividadeconf_portaria`),
  UNIQUE KEY `atividadeconf_portaria_UNIQUE` (`atividadeconf_portaria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `atividadegeral__configuracoes_atividades`
--

CREATE TABLE IF NOT EXISTS `atividadegeral__configuracoes_atividades` (
  `atividadeconfatividade_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `atividadeconf_portaria` varchar(45) NOT NULL,
  `atividadeatividade_id` int(11) unsigned zerofill NOT NULL,
  `atividadeconf_minimo_disciplina` int(11) DEFAULT NULL,
  `atividadegeral__horas_praticas_max` int(11) DEFAULT NULL COMMENT 'Horas praticas que podem ser aproveitadas',
  `atividadeconf_horas_praticas_min` int(11) DEFAULT NULL,
  `atividadeconf_pontuacao_max` int(11) DEFAULT NULL,
  `atividadeconf_pontuacao_min` int(11) DEFAULT NULL,
  PRIMARY KEY (`atividadeconfatividade_id`),
  KEY `fk_atividadegeral__configuracoes__atividades_est_idx` (`atividadeatividade_id`),
  KEY `fk_atividadegeral__configuracoes__atividades_est_idx1` (`atividadeconf_portaria`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `atividadegeral__nucleo`
--

CREATE TABLE IF NOT EXISTS `atividadegeral__nucleo` (
  `nucleo_id` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nucleo_descricao` varchar(45) DEFAULT NULL,
  `nucleo_capacidade_total` varchar(45) DEFAULT NULL,
  `nucleo_tipo` enum('interno','externo') DEFAULT NULL,
  `nucleo_efetivacao_inicio` datetime DEFAULT NULL,
  `nucleo_efetivacao_fim` datetime DEFAULT NULL,
  `nucleo_dias_funcionamento` set('Segunda','Terca','Quarta','Quinta','Sexta','Sabado','Domingo') DEFAULT NULL,
  `nucleo_funcionamento_abertura` time DEFAULT NULL,
  `nucleo_funcionamento_fechamento` time DEFAULT NULL,
  `nucleo_vagas` int(11) DEFAULT NULL,
  PRIMARY KEY (`nucleo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `atividadegeral__nucleo_processo`
--

CREATE TABLE IF NOT EXISTS `atividadegeral__nucleo_processo` (
  `nucleo_processo_id` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nucleo_id` int(10) unsigned zerofill NOT NULL,
  `processo_tipo_id` int(10) unsigned zerofill NOT NULL,
  `nucleo_processo_vinculacao` enum('ativo','inativo') NOT NULL,
  PRIMARY KEY (`nucleo_processo_id`),
  UNIQUE KEY `nucleo_processo_id_UNIQUE` (`nucleo_processo_id`),
  KEY `fk_nucleo_id_acadgeral__atividade_tprocesso_nucleo_id1_idx` (`nucleo_id`),
  KEY `fk_nucleo_processo_tip_idx` (`processo_tipo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `atividadegeral__nucleo_responsavel`
--

CREATE TABLE IF NOT EXISTS `atividadegeral__nucleo_responsavel` (
  `nucleoresponsavel_id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `pes_id` int(11) unsigned zerofill NOT NULL,
  `nucleo_id` int(10) unsigned zerofill NOT NULL,
  `nucleoresponsavel_inicio` datetime DEFAULT NULL,
  `nucleoresponsavel_fim` datetime DEFAULT NULL,
  `nucleoresponsavel_observacoes` varchar(45) DEFAULT NULL,
  `nucleo_responsavel_nivel` enum('auxiliar','supervisor') DEFAULT NULL,
  PRIMARY KEY (`nucleoresponsavel_id`),
  KEY `fk_pessoa_atividadegeral__nucleo_atividadegeral__nucleo1_idx` (`nucleo_id`),
  KEY `fk_pessoa_atividadegeral__nucleo_pessoa1_idx` (`pes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `atividadegeral__nucleo__sala`
--

CREATE TABLE IF NOT EXISTS `atividadegeral__nucleo__sala` (
  `atividadegeral_sala_nucleo_id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nucleo_id` int(10) unsigned zerofill NOT NULL,
  `sala_id` int(11) unsigned zerofill NOT NULL,
  PRIMARY KEY (`atividadegeral_sala_nucleo_id`),
  UNIQUE KEY `atividadegeral_sala_id_UNIQUE` (`atividadegeral_sala_nucleo_id`),
  KEY `fk_atividadegeral__nucleo__sala_atividadegeral__nucleo1_idx` (`nucleo_id`),
  KEY `fk_atividadegeral__nucleo__sala_infra_sala1_idx` (`sala_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `atividadegeral__processo`
--

CREATE TABLE IF NOT EXISTS `atividadegeral__processo` (
  `atividade_processo_id` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nucleo_processo_id` int(10) unsigned zerofill NOT NULL,
  `atividade_processo_descricao` varchar(45) NOT NULL,
  `atividade_processo_data_inicio` datetime NOT NULL,
  `atividade_processo_data_fim` datetime DEFAULT NULL,
  PRIMARY KEY (`atividade_processo_id`),
  KEY `fk_atividadegeral__processo_atividadegeral__nucleo_processo1_idx` (`nucleo_processo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `atividadegeral__processo_tipo`
--

CREATE TABLE IF NOT EXISTS `atividadegeral__processo_tipo` (
  `processo_tipo_id` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `processo_tipo_descricao` varchar(45) NOT NULL,
  `processo_tipo_observacao` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`processo_tipo_id`),
  UNIQUE KEY `tprocesso_descricao_UNIQUE` (`processo_tipo_descricao`),
  UNIQUE KEY `tprocesso_id_UNIQUE` (`processo_tipo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `atividadegeral__processo__pessoas`
--

CREATE TABLE IF NOT EXISTS `atividadegeral__processo__pessoas` (
  `pes_id` int(11) unsigned zerofill NOT NULL,
  `atividadeprocesso_id` int(11) unsigned zerofill NOT NULL,
  PRIMARY KEY (`pes_id`,`atividadeprocesso_id`),
  KEY `fk_pessoa_acadgeral__atividade_processo_pr_idx` (`atividadeprocesso_id`),
  KEY `fk_pessoa_acadgeral__atividade_processo_pessoa1_idx` (`pes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `atividadeperiodo__aluno_atividades`
--

CREATE TABLE IF NOT EXISTS `atividadeperiodo__aluno_atividades` (
  `atividadealuno_atividade_id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `alunoatividade_descricao` text,
  `alunonucleo_id` int(10) unsigned zerofill NOT NULL,
  `atividadeatividade_id` int(11) unsigned zerofill NOT NULL,
  `usuario_lancamento` int(11) unsigned zerofill NOT NULL,
  `alunoatividade_data` datetime NOT NULL,
  `atividadeprocesso_id` int(11) unsigned zerofill DEFAULT NULL,
  `alunoatividade_pontos` int(11) DEFAULT NULL,
  `alunoatividade_horas` int(11) DEFAULT NULL,
  `alunoatividades_observacao` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`atividadealuno_atividade_id`),
  KEY `fk_atividadeperiodo__aluno_atividades_acesso_pessoas1_idx` (`usuario_lancamento`),
  KEY `fk_atividadeperiodo__aluno_atividades_processo1_idx` (`atividadeprocesso_id`),
  KEY `fk_atividadeperiodo__aluno_atividades_atividade_idx` (`atividadeatividade_id`),
  KEY `fk_atividadeperiodo__aluno_atividadeperiodo__aluno_n_idx` (`alunonucleo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `atividadeperiodo__aluno_nucleo`
--

CREATE TABLE IF NOT EXISTS `atividadeperiodo__aluno_nucleo` (
  `alunonucleo_id` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `per_id` int(10) unsigned zerofill NOT NULL,
  `nucleo_id` int(10) unsigned zerofill NOT NULL,
  `alunoper_id` int(10) unsigned zerofill NOT NULL,
  `alunoperiodo_data_inscricao` datetime NOT NULL,
  `alunoperiodo_data_fechamento` datetime DEFAULT NULL,
  `alunoperiodo_total_horas` int(11) DEFAULT NULL,
  `alunoperiodo_observacoes` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`alunonucleo_id`),
  KEY `fk_atividadeperiodo__aluno_nucleo_atividadegeral__nucleo1_idx` (`nucleo_id`),
  KEY `fk_atividadeperiodo__aluno_nucleo_periodo__letivo1_idx` (`per_id`),
  KEY `fk_atividadeperiodo__aluno_nucleo_acadperiodo__aluno1_idx` (`alunoper_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `atividadeperiodo__aluno_nucleo_processo`
--

CREATE TABLE IF NOT EXISTS `atividadeperiodo__aluno_nucleo_processo` (
  `alunoprocesso_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `alunonucleo_id` int(10) unsigned zerofill NOT NULL,
  `atividade_processo_id` int(11) unsigned zerofill NOT NULL,
  `alunoprocesso_data_inicio` datetime NOT NULL,
  `alunoprocesso_data_fim` datetime DEFAULT NULL,
  `alunoprocesso_observacoes` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`alunoprocesso_id`),
  KEY `fk_atividadeperiodo__aluno_nucleo_processo__al_idx` (`alunonucleo_id`),
  KEY `fk_atividadeperiodo__aluno_nucleo_processo__proc_idx` (`atividade_processo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `atividadeperiodo__aluno_portfolio`
--

CREATE TABLE IF NOT EXISTS `atividadeperiodo__aluno_portfolio` (
  `portfolio_id` int(11) NOT NULL,
  `alunonucleo_id` int(10) unsigned zerofill NOT NULL,
  `portfolioaluno_data_entrega` datetime NOT NULL,
  `arq_id` int(10) unsigned zerofill DEFAULT NULL,
  `portfolioaluno_pontuacao` int(11) DEFAULT NULL,
  `portfolioaluno_data_avaliacao` datetime DEFAULT NULL,
  `portfolio_aluno_retorno` text,
  `portfolioaluno_situacao` enum('Aprovado','Reprovado') DEFAULT NULL,
  PRIMARY KEY (`portfolio_id`),
  KEY `fk_atividadeperiodo__aluno_portfolio_arquivo1_idx` (`arq_id`),
  KEY `fk_atividadeperiodo__aluno_portfolio_aluno_nu_idx` (`alunonucleo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `atividadeperiodo__letivo`
--

CREATE TABLE IF NOT EXISTS `atividadeperiodo__letivo` (
  `per_id` int(10) unsigned zerofill NOT NULL,
  `atividadeconf_portaria` varchar(45) NOT NULL,
  `atividadeperiodo_vagas` int(11) DEFAULT NULL,
  `atividadeperiodo_data_inicio` datetime DEFAULT NULL,
  `atividadeperiodo_data_fechamento` datetime DEFAULT NULL,
  `atividadeperiodo_funcionamento_inicio` time DEFAULT NULL,
  `atividadeperiodo_funcionamento_fim` time DEFAULT NULL,
  PRIMARY KEY (`per_id`),
  KEY `fk_atividade__periodo__letivo_acadperiodo__letivo1_idx` (`per_id`),
  KEY `fk_atividade__periodo__letivo_atividadegeral__configuracoes1_idx` (`atividadeconf_portaria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `acadgeral__aluno_atividade`
--
ALTER TABLE `acadgeral__aluno_atividade`
  ADD CONSTRAINT `fk_acadgeral__aluno_atividade_acadgeral__aluno_curso1` FOREIGN KEY (`alunocurso_id`) REFERENCES `acadgeral__aluno_curso` (`alunocurso_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_acadgeral__aluno_atividade_aluno_nucleo1` FOREIGN KEY (`alunonucleo_id`) REFERENCES `atividadeperiodo__aluno_nucleo` (`alunonucleo_id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `atividadegeral__atividades_disciplina`
--
ALTER TABLE `atividadegeral__atividades_disciplina`
  ADD CONSTRAINT `fk_atividadegeral__atividades_disciplina__configur1` FOREIGN KEY (`atividadeconfatividade_id`) REFERENCES `atividadegeral__configuracoes_atividades` (`atividadeconfatividade_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_atividadegeral__atividades_disciplina_acadgeral__disciplina1` FOREIGN KEY (`disc_id`) REFERENCES `acadgeral__disciplina` (`disc_id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `atividadegeral__configuracoes_atividades`
--
ALTER TABLE `atividadegeral__configuracoes_atividades`
  ADD CONSTRAINT `fk_atividadegeral__configuracoes_atividades_estag1` FOREIGN KEY (`atividadeconf_portaria`) REFERENCES `atividadegeral__configuracoes` (`atividadeconf_portaria`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_atividadegeral__configuracoes_atividades_estag2` FOREIGN KEY (`atividadeatividade_id`) REFERENCES `atividadegeral__atividades` (`atividadeatividade_id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `atividadegeral__nucleo_processo`
--
ALTER TABLE `atividadegeral__nucleo_processo`
  ADD CONSTRAINT `fk_atividadegeral__nucleo_processo_tipo1` FOREIGN KEY (`processo_tipo_id`) REFERENCES `atividadegeral__processo_tipo` (`processo_tipo_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_nucleo_id_acadgeral__atividade_tprocesso_nucleo_id1` FOREIGN KEY (`nucleo_id`) REFERENCES `atividadegeral__nucleo` (`nucleo_id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `atividadegeral__nucleo_responsavel`
--
ALTER TABLE `atividadegeral__nucleo_responsavel`
  ADD CONSTRAINT `fk_pessoa_atividadegeral__nucleo_atividadegeral__nucleo1` FOREIGN KEY (`nucleo_id`) REFERENCES `atividadegeral__nucleo` (`nucleo_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_pessoa_atividadegeral__nucleo_pessoa1` FOREIGN KEY (`pes_id`) REFERENCES `pessoa` (`pes_id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `atividadegeral__nucleo__sala`
--
ALTER TABLE `atividadegeral__nucleo__sala`
  ADD CONSTRAINT `fk_atividadegeral__nucleo__sala_atividadegeral__nucleo1` FOREIGN KEY (`nucleo_id`) REFERENCES `atividadegeral__nucleo` (`nucleo_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_atividadegeral__nucleo__sala_infra_sala1` FOREIGN KEY (`sala_id`) REFERENCES `infra_sala` (`sala_id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `atividadegeral__processo`
--
ALTER TABLE `atividadegeral__processo`
  ADD CONSTRAINT `fk_atividadegeral__processo_atividadegeral__nucleo_processo1` FOREIGN KEY (`nucleo_processo_id`) REFERENCES `atividadegeral__nucleo_processo` (`nucleo_processo_id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `atividadegeral__processo__pessoas`
--
ALTER TABLE `atividadegeral__processo__pessoas`
  ADD CONSTRAINT `fk_pessoa_acadgeral__atividade_processo_pessoa1` FOREIGN KEY (`pes_id`) REFERENCES `pessoa` (`pes_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_pessoa_acadgeral__atividade_processo_proc1` FOREIGN KEY (`atividadeprocesso_id`) REFERENCES `atividadegeral__processo` (`atividade_processo_id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `atividadeperiodo__aluno_atividades`
--
ALTER TABLE `atividadeperiodo__aluno_atividades`
  ADD CONSTRAINT `fk_atividadeperiodo__aluno_atividades_acesso_pessoas1` FOREIGN KEY (`usuario_lancamento`) REFERENCES `acesso_pessoas` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_atividadeperiodo__aluno_atividades_aluno_nuc1` FOREIGN KEY (`alunonucleo_id`) REFERENCES `atividadeperiodo__aluno_nucleo` (`alunonucleo_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_atividadeperiodo__aluno_atividades_atividadegeral__processo1` FOREIGN KEY (`atividadeprocesso_id`) REFERENCES `atividadegeral__processo` (`atividade_processo_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_atividadeperiodo__aluno_atividades_atividades1` FOREIGN KEY (`atividadeatividade_id`) REFERENCES `atividadegeral__atividades` (`atividadeatividade_id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `atividadeperiodo__aluno_nucleo`
--
ALTER TABLE `atividadeperiodo__aluno_nucleo`
  ADD CONSTRAINT `fk_atividadeperiodo__aluno_nucleo_acadperiodo__aluno1` FOREIGN KEY (`alunoper_id`) REFERENCES `acadperiodo__aluno` (`alunoper_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_atividadeperiodo__aluno_nucleo_atividade__periodo__letivo1` FOREIGN KEY (`per_id`) REFERENCES `atividadeperiodo__letivo` (`per_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_atividadeperiodo__aluno_nucleo_atividadegeral__nucleo1` FOREIGN KEY (`nucleo_id`) REFERENCES `atividadegeral__nucleo` (`nucleo_id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `atividadeperiodo__aluno_nucleo_processo`
--
ALTER TABLE `atividadeperiodo__aluno_nucleo_processo`
  ADD CONSTRAINT `fk_atividadeperiodo__aluno_nucleo_processo__alun1` FOREIGN KEY (`alunonucleo_id`) REFERENCES `atividadeperiodo__aluno_nucleo` (`alunonucleo_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_atividadeperiodo__aluno_nucleo_processo__proces1` FOREIGN KEY (`atividade_processo_id`) REFERENCES `atividadegeral__processo` (`atividade_processo_id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `atividadeperiodo__aluno_portfolio`
--
ALTER TABLE `atividadeperiodo__aluno_portfolio`
  ADD CONSTRAINT `fk_atividadeperiodo__aluno_portfolio_aluno_nucl1` FOREIGN KEY (`alunonucleo_id`) REFERENCES `atividadeperiodo__aluno_nucleo` (`alunonucleo_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_atividadeperiodo__aluno_portfolio_arquivo1` FOREIGN KEY (`arq_id`) REFERENCES `arquivo` (`arq_id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `atividadeperiodo__letivo`
--
ALTER TABLE `atividadeperiodo__letivo`
  ADD CONSTRAINT `fk_atividade__periodo__letivo_acadperiodo__letivo1` FOREIGN KEY (`per_id`) REFERENCES `acadperiodo__letivo` (`per_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_atividade__periodo__letivo_atividadegeral__configuracoes1` FOREIGN KEY (`atividadeconf_portaria`) REFERENCES `atividadegeral__configuracoes` (`atividadeconf_portaria`) ON UPDATE CASCADE;


-- registra as funcionalidades e atribui as permissoes

set @mod=0;
set @func=0;

INSERT INTO `acesso_modulos` (`id`, `sistema`, `mod_nome`, `mod_route`, `mod_status`, `mod_icon`, `mod_info`, `mod_redireciona`, `mod_prioridade`) 
VALUES (Null, 00000000050, 'Atividades Práticas', 'atividades/default', 'Ativo', 'fa fa-gavel', 'Módulo resposável por administrar atividades extracurriculares de alunos', '/atividades', 12);

select @mod:=id from acesso_modulos where mod_nome='Atividades Práticas';
select @func:=(max(id)+1) from acesso_funcionalidades;

INSERT INTO acesso_funcionalidades VALUES (@func, @mod, 'Aluno Atividades', 'Atividades\\Controller\\AlunoAtividades', '', null, 'Ativa', 'Sim');

INSERT INTO `acesso_actions` VALUES (null,@func,'index','Listagem',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';

INSERT INTO `acesso_actions` VALUES (null,@func,'add','Adicionar',NULL,NULL,'Escrita','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';

INSERT INTO `acesso_actions` VALUES (null,@func,'edit','Editar',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';

INSERT INTO `acesso_actions` VALUES (null,@func,'search-for-json','Autocomplete',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';

INSERT INTO `acesso_actions` VALUES (null,@func,'search','Datatables',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';

select @func:=(max(id)+1) from acesso_funcionalidades;

INSERT INTO acesso_funcionalidades VALUES (@func, @mod, 'Secretaria Estágio', 'Atividades\\Controller\\AlunoNucleo', '', null, 'Ativa', 'Sim');

INSERT INTO `acesso_actions` VALUES (null,@func,'index','Listagem',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';

INSERT INTO `acesso_actions` VALUES (null,@func,'add','Adicionar',NULL,NULL,'Escrita','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';

INSERT INTO `acesso_actions` VALUES (null,@func,'edit','Editar',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';

INSERT INTO `acesso_actions` VALUES (null,@func,'search-for-json','Autocomplete',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';

INSERT INTO `acesso_actions` VALUES (null,@func,'search','Datatables',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';
