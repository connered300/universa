-- MySQL Workbench Synchronization
-- Generated: 2017-03-14 15:03
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `boleto` 
ADD COLUMN `baixa_id` INT(11) NULL DEFAULT NULL AFTER `bol_valor_pago`,
ADD INDEX `fk_boleto_baixas_efetuadas1_idx` (`baixa_id` ASC);

CREATE TABLE IF NOT EXISTS `boleto_baixa` (
  `baixa_id` INT(11) NOT NULL AUTO_INCREMENT,
  `arq_id` INT(10) UNSIGNED ZEROFILL NOT NULL,
  `usuario_baixa` INT(11) UNSIGNED ZEROFILL NOT NULL,
  `baixa_data_submissao` DATETIME NOT NULL,
  PRIMARY KEY (`baixa_id`),
  INDEX `fk_baixas_efetuadas_arquivo1_idx` (`arq_id` ASC),
  INDEX `fk_boleto_baixas_acesso_pessoas1_idx` (`usuario_baixa` ASC),
  CONSTRAINT `fk_baixas_efetuadas_arquivo1`
    FOREIGN KEY (`arq_id`)
    REFERENCES `arquivo` (`arq_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_boleto_baixas_acesso_pessoas1`
    FOREIGN KEY (`usuario_baixa`)
    REFERENCES `acesso_pessoas` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

ALTER TABLE `boleto` 
ADD CONSTRAINT `fk_boleto_baixas_efetuadas1`
  FOREIGN KEY (`baixa_id`)
  REFERENCES `boleto_baixa` (`baixa_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
