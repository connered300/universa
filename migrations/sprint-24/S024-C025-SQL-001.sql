-- MySQL Workbench Synchronization
-- Generated: 2016-12-19 16:47
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `boleto` 
DROP FOREIGN KEY `fk_boleto_titulo1`;

ALTER TABLE `financeiro__titulo` DROP FOREIGN KEY `fk_financeiro__titulo_acesso_pessoas1`;
ALTER TABLE `financeiro__titulo` DROP FOREIGN KEY `fk_financeiro__titulo_acesso_pessoas2`;
ALTER TABLE `financeiro__titulo` DROP FOREIGN KEY `fk_titulo_pessoa1`;

ALTER TABLE `financeiro__valores` DROP FOREIGN KEY `fk_financeiro__mensalidade_financeiro__titulo_tipo1`;
ALTER TABLE `financeiro__valores` DROP FOREIGN KEY `fk_financeiro__mensalidade_acadperiodo__letivo1`;

ALTER TABLE `financeiro__titulo_config` DROP FOREIGN KEY `fk_financeiro__titulo_config_acadperiodo__letivo1`;

ALTER TABLE `boleto` CHANGE COLUMN `titulo_id` `titulo_id` INT(11) ZEROFILL NULL DEFAULT NULL;
ALTER TABLE `boleto` ADD COLUMN `pagamento_id` INT(11) NULL DEFAULT NULL AFTER `titulo_id`;
ALTER TABLE `boleto` ADD COLUMN `bol_desconto_data` DATETIME NULL DEFAULT NULL AFTER `bol_via`;
ALTER TABLE `boleto` ADD COLUMN `bol_desconto_valor` FLOAT(11) NULL DEFAULT NULL AFTER `bol_desconto_data`;
ALTER TABLE `boleto` ADD COLUMN `bol_multa` FLOAT(10,2) NULL DEFAULT NULL AFTER `bol_desconto_valor`;
ALTER TABLE `boleto` ADD COLUMN `bol_juros` FLOAT(10,3) NULL DEFAULT NULL AFTER `bol_multa`;
ALTER TABLE `boleto` ADD INDEX `fk_boleto_financeiro__pagamento1_idx` (`pagamento_id` ASC);

ALTER TABLE `financeiro__titulo_tipo` ADD COLUMN `tipotitulo_tempo_vencimento` INT(11) NULL DEFAULT NULL AFTER `tipotitulo_descricao`;
ALTER TABLE `financeiro__titulo_tipo` ADD COLUMN `tipotitulo_matricula_emissao` ENUM('Sim', 'Não') NULL DEFAULT 'Não' AFTER `tipotitulo_tempo_vencimento`;

ALTER TABLE `financeiro__titulo` CHANGE COLUMN `titulo_estado` `titulo_estado` ENUM('Pago', 'Aberto', 'Cancelado', 'Estorno', 'Alteracao') NULL DEFAULT NULL;
ALTER TABLE `financeiro__titulo` ADD COLUMN `titulo_descricao` VARCHAR(255) NOT NULL DEFAULT '-' AFTER `titulo_novo`;
ALTER TABLE `financeiro__titulo` ADD COLUMN `titulo_parcela` TINYINT(4) NOT NULL DEFAULT 1 AFTER `titulo_descricao`;

ALTER TABLE `financeiro__cheque` ADD COLUMN `pagamento_id` INT(11) NULL DEFAULT NULL AFTER `cheque_id`;
ALTER TABLE `financeiro__cheque` ADD INDEX `fk_financeiro__cheque_financeiro__pagamento1_idx` (`pagamento_id` ASC);

ALTER TABLE `financeiro__valores` CHANGE COLUMN `per_id` `per_id` INT(10) ZEROFILL UNSIGNED NULL DEFAULT NULL;
ALTER TABLE `financeiro__valores` CHANGE COLUMN `tipotitulo_id` `tipotitulo_id` INT(11) NOT NULL;
ALTER TABLE `financeiro__valores` ADD COLUMN `valores_data_inicio` DATE NULL DEFAULT NULL AFTER `valores_preco`;
ALTER TABLE `financeiro__valores` ADD COLUMN `valores_data_fim` DATE NULL DEFAULT NULL AFTER `valores_data_inicio`;
ALTER TABLE `financeiro__valores` ADD COLUMN `valores_modelo` ENUM('integral', 'parcela') NULL DEFAULT NULL AFTER `valores_data_fim`;
ALTER TABLE `financeiro__valores` ADD COLUMN `valores_parcela` INT(11) NOT NULL DEFAULT 1 AFTER `valores_modelo`;

ALTER TABLE `financeiro__titulo_config` CHANGE COLUMN `cursocampus_id` `cursocampus_id` INT(10) ZEROFILL UNSIGNED NULL DEFAULT NULL ;
ALTER TABLE `financeiro__titulo_config` CHANGE COLUMN `per_id` `per_id` INT(10) UNSIGNED ZEROFILL NULL DEFAULT NULL ;
ALTER TABLE `financeiro__titulo_config` ADD COLUMN `tituloconf_data_inicio` DATE NULL DEFAULT NULL AFTER `tituloconf_juros`;
ALTER TABLE `financeiro__titulo_config` ADD COLUMN `tituloconf_data_fim` DATE NULL DEFAULT NULL AFTER `tituloconf_data_inicio`;

CREATE TABLE IF NOT EXISTS `financeiro__remessa` (
  `remessa_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `confcont_id` INT(11) NOT NULL,
  `remessa_codigo` INT(11) NOT NULL,
  `remessa_data` DATETIME NOT NULL,
  `arq_id` INT(10) UNSIGNED ZEROFILL NOT NULL,
  PRIMARY KEY (`remessa_id`),
  UNIQUE INDEX `remessa_codigo_UNIQUE` (`remessa_codigo` ASC, `confcont_id` ASC),
  INDEX `fk_financeiro__remessa_arquivo1_idx` (`arq_id` ASC),
  CONSTRAINT `fk_financeiro_remessa_boleto_conf_conta1`
    FOREIGN KEY (`confcont_id`)
    REFERENCES `boleto_conf_conta` (`confcont_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `financeiro__remessa_lote` (
  `lote_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `remessa_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  `lote_codigo` INT(11) NOT NULL,
  PRIMARY KEY (`lote_id`),
  INDEX `fk_financeiro_remessa_lote_financeiro_remessa1_idx` (`remessa_id` ASC),
  UNIQUE INDEX `lote_codigo_UNIQUE` (`lote_codigo` ASC, `remessa_id` ASC),
  CONSTRAINT `fk_financeiro_remessa_lote_financeiro_remessa1`
    FOREIGN KEY (`remessa_id`)
    REFERENCES `financeiro__remessa` (`remessa_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `financeiro__remessa_lote_registro` (
  `registro_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `lote_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  `bol_id` INT(11) NOT NULL,
  `registro_codigo` INT(11) NOT NULL,
  `registro_acao` ENUM('Registro', 'Cancelamento', 'Alteracao') NULL DEFAULT NULL,
  PRIMARY KEY (`registro_id`),
  INDEX `fk_financeiro__remessa_lote_boleto_financeiro__remessa_lote_idx` (`lote_id` ASC),
  INDEX `fk_financeiro__remessa_lote_boleto_boleto1_idx` (`bol_id` ASC),
  UNIQUE INDEX `registro_codigo_UNIQUE` (`registro_codigo` ASC, `registro_id` ASC),
  CONSTRAINT `fk_financeiro__remessa_lote_boleto_financeiro__remessa_lote1`
    FOREIGN KEY (`lote_id`)
    REFERENCES `financeiro__remessa_lote` (`lote_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_financeiro__remessa_lote_boleto_boleto1`
    FOREIGN KEY (`bol_id`)
    REFERENCES `boleto` (`bol_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `financeiro__aluno_config_pgto_curso` (
  `config_pgto_curso_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `alunocurso_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  `tipotitulo_id` INT(11) NOT NULL,
  `config_pgto_curso_parcela` INT(11) NOT NULL,
  `config_pgto_curso_valor` FLOAT(11) NOT NULL,
  `config_pgto_curso_vencimento` INT(11) NOT NULL,
  INDEX `fk_financeiro__aluno_config_pgto_curso_acadgeral__aluno_cur_idx` (`alunocurso_id` ASC),
  PRIMARY KEY (`config_pgto_curso_id`),
  INDEX `fk_financeiro__aluno_config_pgto_curso_financeiro__titulo_t_idx` (`tipotitulo_id` ASC),
  CONSTRAINT `fk_financeiro__aluno_config_pgto_curso_acadgeral__aluno_curso1`
    FOREIGN KEY (`alunocurso_id`)
    REFERENCES `acadgeral__aluno_curso` (`alunocurso_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_financeiro__aluno_config_pgto_curso_financeiro__titulo_tipo1`
    FOREIGN KEY (`tipotitulo_id`)
    REFERENCES `financeiro__titulo_tipo` (`tipotitulo_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `financeiro__pagamento` (
  `pagamento_id` INT(11) NOT NULL AUTO_INCREMENT,
  `titulo_id` INT(11) ZEROFILL NOT NULL,
  `forma_pagamento_id` INT(11) NOT NULL,
  `pagamento_usuario` INT(11) UNSIGNED ZEROFILL NOT NULL,
  `pes_id` INT(11) UNSIGNED ZEROFILL NOT NULL,
  `pagamento_valor_bruto` FLOAT(10,2) NOT NULL DEFAULT 0,
  `pagamento_valor_juros` FLOAT(10,2) NOT NULL DEFAULT 0,
  `pagamento_valor_multa` FLOAT(10,2) NOT NULL DEFAULT 0,
  `pagamento_acrescimos` FLOAT(10,2) NOT NULL DEFAULT 0,
  `pagamento_valor_descontos` FLOAT(10,2) NOT NULL DEFAULT 0,
  `pagamento_valor_final` FLOAT(10,2) NOT NULL DEFAULT 0,
  `pagamento_desconto_manual` FLOAT(10,2) NOT NULL DEFAULT 0,
  `pagamento_data_baixa` DATETIME NULL DEFAULT NULL,
  `pagamento_observacoes` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`pagamento_id`),
  INDEX `fk_financeiro__pagamento_acesso_pessoas1_idx` (`pagamento_usuario` ASC),
  INDEX `fk_financeiro__pagamento_financeiro__meio_pagamento1_idx` (`forma_pagamento_id` ASC),
  INDEX `fk_financeiro__pagamento_financeiro__titulo1_idx` (`titulo_id` ASC),
  INDEX `fk_financeiro__pagamento_pessoa_fisica1_idx` (`pes_id` ASC),
  CONSTRAINT `fk_financeiro__pagamento_acesso_pessoas1`
    FOREIGN KEY (`pagamento_usuario`)
    REFERENCES `acesso_pessoas` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_financeiro__pagamento_financeiro__meio_pagamento1`
    FOREIGN KEY (`forma_pagamento_id`)
    REFERENCES `financeiro__meio_pagamento` (`meio_pagamento_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_financeiro__pagamento_financeiro__titulo1`
    FOREIGN KEY (`titulo_id`)
    REFERENCES `financeiro__titulo` (`titulo_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_financeiro__pagamento_pessoa_fisica1`
    FOREIGN KEY (`pes_id`)
    REFERENCES `pessoa` (`pes_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = 'Tabela Responsavel por armazenar informação de multiplos t';

CREATE TABLE IF NOT EXISTS `financeiro__meio_pagamento` (
  `meio_pagamento_id` INT(11) NOT NULL,
  `forma_pagamento_descricao` VARCHAR(45) NOT NULL,
  `forma_pagamento_tipo` ENUM('avista', 'aprazo') NULL DEFAULT NULL,
  `forma_pagamento_status` ENUM('ativa', 'inativa') NULL DEFAULT NULL,
  PRIMARY KEY (`meio_pagamento_id`),
  UNIQUE INDEX `financeiro_forma_pagamento_UNIQUE` (`meio_pagamento_id` ASC),
  UNIQUE INDEX `forma_pagamento_descricao_UNIQUE` (`forma_pagamento_descricao` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = 'Forma de pagamento';

CREATE TABLE IF NOT EXISTS `financeiro__titulo_alteracao` (
  `alteracao_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `titulo_id_origem` INT(11) ZEROFILL NOT NULL,
  `titulo_id_destino` INT(11) ZEROFILL NOT NULL,
  `alteracao_acao` ENUM('Atualizacao', 'Parcelamento', 'Renegociacao') NULL DEFAULT NULL,
  INDEX `fk_financeiro__titulo_financeiro__titulo_financeiro__titulo_idx` (`titulo_id_destino` ASC),
  INDEX `fk_financeiro__titulo_financeiro__titulo_financeiro__titulo_idx1` (`titulo_id_origem` ASC),
  PRIMARY KEY (`alteracao_id`),
  CONSTRAINT `fk_financeiro__titulo_financeiro__titulo_financeiro__titulo1`
    FOREIGN KEY (`titulo_id_origem`)
    REFERENCES `financeiro__titulo` (`titulo_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_financeiro__titulo_financeiro__titulo_financeiro__titulo2`
    FOREIGN KEY (`titulo_id_destino`)
    REFERENCES `financeiro__titulo` (`titulo_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

ALTER TABLE `boleto` 
ADD CONSTRAINT `fk_boleto_titulo1`
  FOREIGN KEY (`titulo_id`)
  REFERENCES `financeiro__titulo` (`titulo_id`)
  ON UPDATE CASCADE;

ALTER TABLE `boleto` 
ADD CONSTRAINT `fk_boleto_financeiro__pagamento1`
  FOREIGN KEY (`pagamento_id`)
  REFERENCES `financeiro__pagamento` (`pagamento_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `boleto_banco` 
ADD CONSTRAINT `boleto_banco_ibfk_2`
  FOREIGN KEY (`layout_id`)
  REFERENCES `boleto_layout` (`layout_id`)
  ON UPDATE CASCADE;

ALTER TABLE `financeiro__titulo` 
DROP FOREIGN KEY `fk_titulo_boleto_tipo1`;

ALTER TABLE `financeiro__titulo`
ADD CONSTRAINT `fk_titulo_boleto_tipo1`
  FOREIGN KEY (`tipotitulo_id`)
  REFERENCES `financeiro__titulo_tipo` (`tipotitulo_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `financeiro__titulo`
ADD CONSTRAINT `fk_financeiro__titulo_acesso_pessoas1`
  FOREIGN KEY (`usuario_autor`)
  REFERENCES `acesso_pessoas` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `financeiro__titulo`
ADD CONSTRAINT `fk_financeiro__titulo_acesso_pessoas2`
  FOREIGN KEY (`usuario_baixa`)
  REFERENCES `acesso_pessoas` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `financeiro__titulo`
ADD CONSTRAINT `fk_titulo_pessoa1`
  FOREIGN KEY (`pes_id`)
  REFERENCES `pessoa` (`pes_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `financeiro__cheque` 
ADD CONSTRAINT `fk_financeiro__cheque_financeiro__pagamento1`
  FOREIGN KEY (`pagamento_id`)
  REFERENCES `financeiro__pagamento` (`pagamento_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `financeiro__valores` 
ADD CONSTRAINT `fk_financeiro__mensalidade_financeiro__titulo_tipo1`
  FOREIGN KEY (`tipotitulo_id`)
  REFERENCES `financeiro__titulo_tipo` (`tipotitulo_id`)
  ON UPDATE CASCADE;

ALTER TABLE `financeiro__valores` 
ADD CONSTRAINT `fk_financeiro__valores_acadperiodo__letivo1`
  FOREIGN KEY (`per_id`)
  REFERENCES `acadperiodo__letivo` (`per_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `financeiro__titulo_config` 
DROP FOREIGN KEY `fk_financeiro__titulo_config_campus_curso1`;

ALTER TABLE `financeiro__titulo_config`
ADD CONSTRAINT `fk_financeiro__titulo_config_campus_curso1`
  FOREIGN KEY (`cursocampus_id`)
  REFERENCES `campus_curso` (`cursocampus_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `financeiro__titulo_config`
ADD CONSTRAINT `fk_financeiro__titulo_config_acadperiodo__letivo1`
  FOREIGN KEY (`per_id`)
  REFERENCES `acadperiodo__letivo` (`per_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `financeiro__remessa` 
ADD CONSTRAINT `fk_financeiro__remessa_arquivo1`
  FOREIGN KEY (`arq_id`)
  REFERENCES `arquivo` (`arq_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- MySQL Workbench Synchronization
-- Generated: 2016-12-20 11:15
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `financeiro__pagamento` CHANGE COLUMN `forma_pagamento_id` `meio_pagamento_id` INT(11) NOT NULL ;

ALTER TABLE `financeiro__meio_pagamento` CHANGE COLUMN `forma_pagamento_descricao` `meio_pagamento_descricao` VARCHAR(45) NOT NULL;
ALTER TABLE `financeiro__meio_pagamento` CHANGE COLUMN `forma_pagamento_tipo` `meio_pagamento_tipo` ENUM('avista', 'aprazo') NULL DEFAULT NULL;
ALTER TABLE `financeiro__meio_pagamento` CHANGE COLUMN `forma_pagamento_status` `meio_pagamento_status` ENUM('ativa', 'inativa') NULL DEFAULT NULL;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- MySQL Workbench Synchronization
-- Generated: 2016-12-20 16:21
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `financeiro__pagamento` DROP FOREIGN KEY `fk_financeiro__pagamento_financeiro__titulo1`;
ALTER TABLE `financeiro__pagamento` DROP FOREIGN KEY `fk_financeiro__pagamento_financeiro__meio_pagamento1`;

ALTER TABLE `financeiro__pagamento` CHANGE COLUMN `titulo_id` `titulo_id` INT(10) ZEROFILL UNSIGNED NOT NULL;
ALTER TABLE `financeiro__pagamento` 
ADD CONSTRAINT `fk_financeiro__pagamento_financeiro__titulo1`
  FOREIGN KEY (`titulo_id`)
  REFERENCES `financeiro__titulo` (`titulo_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;


ALTER TABLE `financeiro__pagamento` CHANGE COLUMN `meio_pagamento_id` `meio_pagamento_id`  INT(11) ZEROFILL UNSIGNED NOT NULL;

ALTER TABLE `financeiro__meio_pagamento` CHANGE COLUMN `meio_pagamento_id` `meio_pagamento_id` INT(11) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `financeiro__meio_pagamento` CHANGE COLUMN `meio_pagamento_tipo` `meio_pagamento_tipo` ENUM('a vista', 'a prazo') NULL DEFAULT NULL;

ALTER TABLE `financeiro__pagamento`
ADD CONSTRAINT `fk_financeiro__pagamento_financeiro__meio_pagamento1`
    FOREIGN KEY (`meio_pagamento_id`)
    REFERENCES `financeiro__meio_pagamento` (`meio_pagamento_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- MySQL Workbench Synchronization
-- Generated: 2016-12-20 16:36
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `financeiro__pagamento` DROP FOREIGN KEY `fk_financeiro__pagamento_financeiro__meio_pagamento1`;
ALTER TABLE `financeiro__pagamento` DROP FOREIGN KEY `fk_financeiro__pagamento_financeiro__titulo1`;

ALTER TABLE `financeiro__pagamento` CHANGE COLUMN `titulo_id` `titulo_id` INT(11) ZEROFILL NOT NULL;
ALTER TABLE `financeiro__pagamento` CHANGE COLUMN `meio_pagamento_id` `meio_pagamento_id` INT(10) UNSIGNED ZEROFILL NOT NULL ;

ALTER TABLE `financeiro__meio_pagamento`
CHANGE COLUMN `meio_pagamento_id` `meio_pagamento_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT ;

ALTER TABLE `financeiro__pagamento` ADD CONSTRAINT `fk_financeiro__pagamento_financeiro__meio_pagamento1`
  FOREIGN KEY (`meio_pagamento_id`)
  REFERENCES `financeiro__meio_pagamento` (`meio_pagamento_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `financeiro__pagamento`
ADD CONSTRAINT `fk_financeiro__pagamento_financeiro__titulo1`
  FOREIGN KEY (`titulo_id`)
  REFERENCES `financeiro__titulo` (`titulo_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
