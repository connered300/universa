-- MySQL Workbench Synchronization
-- Generated: 2016-11-22 11:48
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


-- -----------------------------------------------------
-- Placeholder table for view `view__alunoperiodo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `view__alunoperiodo` (`per_id` INT, `Ano` INT, `per_nome` INT, `Matricula` INT, `pes_id` INT, `Nome` INT, `alunoper_id` INT, `Situacao` INT, `turma_id` INT, `tturma_descricao` INT, `Turma` INT, `Periodo` INT, `Turno` INT, `Carteira` INT);

-- -----------------------------------------------------
-- Placeholder table for view `view__classificacao_vestibular`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `view__classificacao_vestibular` (`candidato` INT, `edicao_id` INT, `inscricao_id` INT, `pes_id` INT, `inscricao_lingua_estrangeira` INT, `linguas` INT, `redacao` INT, `portugues` INT, `conhecimento` INT, `total` INT, `RESULTADO` INT);

-- -----------------------------------------------------
-- Placeholder table for view `view__descontos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `view__descontos` (`per_id` INT, `per_ano` INT, `per_nome` INT, `turma_id` INT, `turma_nome` INT, `turma_serie` INT, `turma_turno` INT, `desctipo_id` INT, `desctipo_descricao` INT, `desctipo_modalidade` INT, `desconto_percentual` INT, `desconto_id` INT, `desconto_valor` INT, `desconto_mensalidades` INT, `desconto_status` INT, `alunocurso_id` INT, `pes_id` INT, `pes_nome` INT, `matsit_descricao` INT);

-- -----------------------------------------------------
-- Placeholder table for view `view__integradora`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `view__integradora` (`integradora_id` INT, `alunocurso_id` INT, `alunoper_id` INT, `alunodisc_id` INT, `pes_id` INT, `turma_id` INT, `disc_id` INT, `situacao_id` INT, `integradora_status` INT, `Aluno` INT, `Turma` INT, `Periodo` INT, `Turno` INT, `SituacaoMatricula` INT, `Disciplina` INT, `PeriodoMaximo` INT, `PontosDistribuidos` INT, `Nquestoes` INT, `AcertosRespostas` INT, `respostas_id` INT, `AcertosDisciplina` INT, `NDI` INT, `QuantDisciplinas` INT, `PontosPorQuestao` INT, `TotalAcertos` INT, `NGC` INT, `NFD` INT);

-- -----------------------------------------------------
-- Placeholder table for view `view__mensalidades`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `view__mensalidades` (`Ano` INT, `Semestre` INT, `Pessoa` INT, `Matricula` INT, `alunoper_id` INT, `Nome` INT, `Titulo` INT, `Turma` INT, `Periodo` INT, `Turno` INT, `Parcela` INT, `Processamento` INT, `Vencimento` INT, `Valor` INT, `TipoPagamento` INT, `Pagamento` INT, `Multa` INT, `Juros` INT, `ValorPago` INT, `Estado` INT, `ValorDinheiro` INT, `Desconto` INT, `DescontoManual` INT, `AcrescimoManual` INT, `Situacao` INT);

-- -----------------------------------------------------
-- Placeholder table for view `view__titulos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `view__titulos` (`Inscricao` INT, `Vestibular` INT, `Nome` INT, `Numero` INT, `Tipo` INT, `FormaPagamento` INT, `Emissao` INT, `Valor` INT, `Pagamento` INT, `Valor Pago` INT);

-- -----------------------------------------------------
-- Placeholder table for view `view__vestibular_classificacao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `view__vestibular_classificacao` (`pes_nome` INT, `edicao_id` INT, `inscricao_id` INT, `pes_id` INT, `inscricao_lingua_estrangeira` INT, `ingresso` INT, `conhecimento` INT, `redacao` INT, `portugues` INT, `inscricao_nota` INT, `resultado` INT);

-- -----------------------------------------------------
-- Placeholder table for view `view__notas_etapas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `view__notas_etapas` (`professor_id` INT, `professor_nome` INT, `turma_id` INT, `turma_nome` INT, `docdisc_id` INT, `disc_id` INT, `disc_nome` INT, `alunocurso_id` INT, `aluno_id` INT, `aluno_nome` INT, `situacao_aluno_id` INT, `situacao_aluno_descricao` INT, `situacao_disciplina_id` INT, `situacao_disciplina_descricao` INT, `alunodisc_id` INT, `nota_etapa_01` INT, `nota_etapa_02` INT, `nota_etapa_03` INT);

-- -----------------------------------------------------
-- Placeholder table for view `view__faltas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `view__faltas` (`per_id` INT, `per_ano` INT, `per_semestre` INT, `turma_nome` INT, `turma_serie` INT, `turma_turno` INT, `turma_ordem` INT, `Matricula` INT, `alunodisc_id` INT, `Aluno` INT, `Professor` INT, `SituacaoGeral` INT, `Disciplina` INT, `SituacaoDisciplina` INT, `CH` INT, `Faltas` INT, `perc` INT);

-- -----------------------------------------------------
-- Placeholder table for view `view__notas_etapas_2`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `view__notas_etapas_2` (`per_id` INT, `PeriodoLetivo` INT, `Ano` INT, `Semestre` INT, `professor_id` INT, `professor_nome` INT, `turma_id` INT, `turma_nome` INT, `Periodo` INT, `Turno` INT, `Ordem` INT, `docdisc_id` INT, `disc_id` INT, `disc_nome` INT, `Matricula` INT, `aluno_id` INT, `aluno_nome` INT, `situacao_aluno_id` INT, `situacao_aluno_descricao` INT, `situacao_disciplina_id` INT, `situacao_disciplina_descricao` INT, `alunodisc_id` INT, `nota_etapa_01` INT, `nota_etapa_02` INT, `nota_etapa_03` INT);

-- -----------------------------------------------------
-- Placeholder table for view `view__vestibular_classificacao_2`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `view__vestibular_classificacao_2` (`pes_nome` INT, `edicao_id` INT, `inscricao_id` INT, `pes_id` INT, `inscricao_lingua_estrangeira` INT, `ingresso` INT, `conhecimento` INT, `redacao` INT, `portugues` INT, `inscricao_nota` INT, `resultado` INT);

-- -----------------------------------------------------
-- Placeholder table for view `view__vestibular_inscricoes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `view__vestibular_inscricoes` (`Edicao` INT, `Ano` INT, `Semestre` INT, `PeriodoEdicao` INT, `Realizacao` INT, `Turno` INT, `Curso` INT, `Modalidade` INT, `pes_id` INT, `Matricula` INT, `PeriodoMatricula` INT, `Candidato` INT, `Genero` INT, `Nascimento` INT, `Cidade` INT, `Bairro` INT, `Estado` INT, `LinguaEstrangeira` INT, `Nota` INT, `DataInscricao` INT, `Situacao` INT, `Participacao` INT, `Resultado` INT, `Matriculado` INT);



-- -----------------------------------------------------
-- View `view__alunoperiodo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `view__alunoperiodo`;
CREATE
     OR REPLACE ALGORITHM = UNDEFINED 
    SQL SECURITY DEFINER
VIEW `view__alunoperiodo` AS
    SELECT 
        `acadperiodo__letivo`.`per_id` AS `per_id`,
        `acadperiodo__letivo`.`per_ano` AS `Ano`,
        `acadperiodo__letivo`.`per_nome` AS `per_nome`,
        `alunocurso`.`alunocurso_id` AS `Matricula`,
        `aluno`.`pes_id` AS `pes_id`,
        `pessoa`.`pes_nome` AS `Nome`,
        `acadperiodo__aluno`.`alunoper_id` AS `alunoper_id`,
        `situacao`.`matsit_descricao` AS `Situacao`,
        `turma`.`turma_id` AS `turma_id`,
        `tipoturma`.`tturma_descricao` AS `tturma_descricao`,
        `turma`.`turma_nome` AS `Turma`,
        `turma`.`turma_serie` AS `Periodo`,
        `turma`.`turma_turno` AS `Turno`,
        `alunocurso`.`alunocurso_carteira` AS `Carteira`
    FROM
        (((((((`acadgeral__aluno` `aluno`
        JOIN `acadgeral__aluno_curso` `alunocurso` ON ((`aluno`.`aluno_id` = `alunocurso`.`aluno_id`)))
        JOIN `acadperiodo__aluno` ON ((`alunocurso`.`alunocurso_id` = `acadperiodo__aluno`.`alunocurso_id`)))
        JOIN `pessoa` ON ((`aluno`.`pes_id` = `pessoa`.`pes_id`)))
        JOIN `acadperiodo__letivo`)
        JOIN `acadperiodo__turma` `turma` ON (((`alunocurso`.`cursocampus_id` = `turma`.`cursocampus_id`)
            AND (`acadperiodo__aluno`.`turma_id` = `turma`.`turma_id`)
            AND (`acadperiodo__letivo`.`per_id` = `turma`.`per_id`))))
        JOIN `acadgeral__turma_tipo` `tipoturma` ON ((`turma`.`tturma_id` = `tipoturma`.`tturma_id`)))
        JOIN `acadgeral__situacao` `situacao` ON ((`situacao`.`situacao_id` = `acadperiodo__aluno`.`matsituacao_id`)));




-- -----------------------------------------------------
-- View `view__classificacao_vestibular`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `view__classificacao_vestibular`;

CREATE OR REPLACE
VIEW `view__classificacao_vestibular` AS
    SELECT 
        `pessoa`.`pes_nome` AS `candidato`,
        `selecao_inscricao`.`edicao_id` AS `edicao_id`,
        `selecao_inscricao`.`inscricao_id` AS `inscricao_id`,
        `selecao_inscricao`.`pes_id` AS `pes_id`,
        `selecao_inscricao`.`inscricao_lingua_estrangeira` AS `inscricao_lingua_estrangeira`,
        `p`.`provainsc_pontuacao` AS `linguas`,
        `pr`.`provainsc_pontuacao` AS `redacao`,
        `pp`.`provainsc_pontuacao` AS `portugues`,
        `pc`.`provainsc_pontuacao` AS `conhecimento`,
        (((`pp`.`provainsc_pontuacao` + `p`.`provainsc_pontuacao`) + `pc`.`provainsc_pontuacao`) + `pr`.`provainsc_pontuacao`) AS `total`,
        IF((`pr`.`provainsc_pontuacao` > 4),
            'CLASSIFICADO',
            'DESCLASSIFICADO') AS `RESULTADO`
    FROM
        ((((((`selecao_inscricao`
        JOIN `selecao_edicao` ON ((`selecao_inscricao`.`edicao_id` = `selecao_edicao`.`edicao_id`)))
        JOIN `pessoa` ON (((`selecao_inscricao`.`pes_id` = `pessoa`.`pes_id`)
            AND (`selecao_edicao`.`edicao_id` = `selecao_inscricao`.`edicao_id`))))
        JOIN `prova_inscrito` `p` ON (((`selecao_inscricao`.`inscricao_id` = `p`.`inscricao_id`)
            AND (`p`.`proedicao_id` = (SELECT 
                `prova_edicao`.`proedicao_id`
            FROM
                (`prova_edicao`
            JOIN `prova` ON ((`prova_edicao`.`prova_id` = `prova`.`prova_id`)))
            WHERE
                ((`prova`.`prova_descricao` = `selecao_inscricao`.`inscricao_lingua_estrangeira`)
                    AND (`prova_edicao`.`edicao_id` = `selecao_inscricao`.`edicao_id`))
            LIMIT 1)))))
        JOIN `prova_inscrito` `pr` ON (((`selecao_inscricao`.`inscricao_id` = `pr`.`inscricao_id`)
            AND (`pr`.`proedicao_id` = (SELECT 
                `prova_edicao`.`proedicao_id`
            FROM
                (`prova_edicao`
            JOIN `prova` ON ((`prova_edicao`.`prova_id` = `prova`.`prova_id`)))
            WHERE
                ((`prova`.`prova_descricao` = 'Redação')
                    AND (`prova_edicao`.`edicao_id` = `selecao_inscricao`.`edicao_id`)))))))
        JOIN `prova_inscrito` `pp` ON (((`selecao_inscricao`.`inscricao_id` = `pp`.`inscricao_id`)
            AND (`pp`.`proedicao_id` = (SELECT 
                `prova_edicao`.`proedicao_id`
            FROM
                (`prova_edicao`
            JOIN `prova` ON ((`prova_edicao`.`prova_id` = `prova`.`prova_id`)))
            WHERE
                ((`prova`.`prova_descricao` = 'Lingua Portuguesa e Literatura Brasileira')
                    AND (`prova_edicao`.`edicao_id` = `selecao_inscricao`.`edicao_id`)))))))
        JOIN `prova_inscrito` `pc` ON (((`selecao_inscricao`.`inscricao_id` = `pc`.`inscricao_id`)
            AND (`pc`.`proedicao_id` = (SELECT 
                `prova_edicao`.`proedicao_id`
            FROM
                (`prova_edicao`
            JOIN `prova` ON ((`prova_edicao`.`prova_id` = `prova`.`prova_id`)))
            WHERE
                ((`prova`.`prova_descricao` = 'Conhecimentos Gerais')
                    AND (`prova_edicao`.`edicao_id` = `selecao_inscricao`.`edicao_id`)))))))
    ORDER BY `selecao_inscricao`.`edicao_id` , IF((`pr`.`provainsc_pontuacao` > 4),
        'CLASSIFICADO',
        'DESCLASSIFICADO') , (((`pp`.`provainsc_pontuacao` + `p`.`provainsc_pontuacao`) + `pc`.`provainsc_pontuacao`) + `pr`.`provainsc_pontuacao`) DESC , `pr`.`provainsc_pontuacao` DESC , `pp`.`provainsc_pontuacao` DESC , `pc`.`provainsc_pontuacao` DESC , `p`.`provainsc_pontuacao` DESC;



-- -----------------------------------------------------
-- View `view__descontos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `view__descontos`;

CREATE 
     OR REPLACE ALGORITHM = UNDEFINED 
    SQL SECURITY DEFINER
VIEW `view__descontos` AS
    SELECT 
        `acadperiodo__turma`.`per_id` AS `per_id`,
        `acadperiodo__letivo`.`per_ano` AS `per_ano`,
        `acadperiodo__letivo`.`per_nome` AS `per_nome`,
        `acadperiodo__aluno`.`turma_id` AS `turma_id`,
        `acadperiodo__turma`.`turma_nome` AS `turma_nome`,
        `acadperiodo__turma`.`turma_serie` AS `turma_serie`,
        `acadperiodo__turma`.`turma_turno` AS `turma_turno`,
        `financeiro__desconto`.`desctipo_id` AS `desctipo_id`,
        `financeiro__desconto_tipo`.`desctipo_descricao` AS `desctipo_descricao`,
        `financeiro__desconto_tipo`.`desctipo_modalidade` AS `desctipo_modalidade`,
        `financeiro__desconto`.`desconto_percentual` AS `desconto_percentual`,
        `financeiro__desconto`.`desconto_id` AS `desconto_id`,
        `financeiro__desconto`.`desconto_valor` AS `desconto_valor`,
        `financeiro__desconto`.`desconto_mensalidades` AS `desconto_mensalidades`,
        `financeiro__desconto`.`desconto_status` AS `desconto_status`,
        `acadperiodo__aluno`.`alunocurso_id` AS `alunocurso_id`,
        `acadgeral__aluno`.`pes_id` AS `pes_id`,
        `pessoa`.`pes_nome` AS `pes_nome`,
        `acadgeral__situacao`.`matsit_descricao` AS `matsit_descricao`
    FROM
        ((((((((`financeiro__desconto`
        JOIN `financeiro__desconto_tipo` ON ((`financeiro__desconto`.`desctipo_id` = `financeiro__desconto_tipo`.`desctipo_id`)))
        JOIN `acadperiodo__aluno` ON ((`financeiro__desconto`.`alunoper_id` = `acadperiodo__aluno`.`alunoper_id`)))
        JOIN `acadperiodo__turma` ON ((`acadperiodo__aluno`.`turma_id` = `acadperiodo__turma`.`turma_id`)))
        JOIN `acadperiodo__letivo` ON ((`acadperiodo__turma`.`per_id` = `acadperiodo__letivo`.`per_id`)))
        JOIN `acadgeral__aluno_curso` ON (((`acadperiodo__aluno`.`alunocurso_id` = `acadgeral__aluno_curso`.`alunocurso_id`)
            AND (`acadperiodo__turma`.`cursocampus_id` = `acadgeral__aluno_curso`.`cursocampus_id`))))
        JOIN `acadgeral__aluno` ON ((`acadgeral__aluno_curso`.`aluno_id` = `acadgeral__aluno`.`aluno_id`)))
        JOIN `pessoa` ON ((`acadgeral__aluno`.`pes_id` = `pessoa`.`pes_id`)))
        JOIN `acadgeral__situacao` ON ((`acadperiodo__aluno`.`matsituacao_id` = `acadgeral__situacao`.`situacao_id`)));





-- -----------------------------------------------------
-- View `view__integradora`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `view__integradora`;

CREATE 
     OR REPLACE ALGORITHM = UNDEFINED 
    SQL SECURITY DEFINER
VIEW `view__integradora` AS
    SELECT 
        `integradora`.`integradora_id` AS `integradora_id`,
        `alunoPeriodo`.`alunocurso_id` AS `alunocurso_id`,
        `alunoDisciplina`.`alunoper_id` AS `alunoper_id`,
        `alunoDisciplina`.`alunodisc_id` AS `alunodisc_id`,
        `alunoGeral`.`pes_id` AS `pes_id`,
        `turma`.`turma_id` AS `turma_id`,
        `disciplina`.`disc_id` AS `disc_id`,
        `situacao`.`situacao_id` AS `situacao_id`,
        `integradora`.`integradora_status` AS `integradora_status`,
        `pessoa`.`pes_nome` AS `Aluno`,
        `turma`.`turma_nome` AS `Turma`,
        `turma`.`turma_serie` AS `Periodo`,
        `turma`.`turma_turno` AS `Turno`,
        `situacao`.`matsit_descricao` AS `SituacaoMatricula`,
        `disciplina`.`disc_nome` AS `Disciplina`,
        `integradoraConf`.`integconf_serie_max` AS `PeriodoMaximo`,
        `integradoraConf`.`integconf_pontos` AS `PontosDistribuidos`,
        `integradoraConf`.`integconf_questoes` AS `Nquestoes`,
        `alunoIntegradora`.`alunointeg_respostas` AS `AcertosRespostas`,
        `alunoIntegradora`.`alunointeg_id` AS `respostas_id`,
        `alunoIntegradora`.`alunointeg_acertos` AS `AcertosDisciplina`,
        `alunoIntegradora`.`alunointeg_nota_isolada` AS `NDI`,
        (SELECT 
                COUNT(DISTINCT `acadperiodo__aluno_disciplina`.`disc_id`)
            FROM
                (`acadperiodo__aluno_disciplina`
                JOIN `acadperiodo__integradora_aluno` ON ((`acadperiodo__aluno_disciplina`.`alunodisc_id` = `acadperiodo__integradora_aluno`.`alunodisc_id`)))
            WHERE
                (`alunoDisciplina`.`alunoper_id` = `acadperiodo__aluno_disciplina`.`alunoper_id`)
            GROUP BY `alunoDisciplina`.`alunoper_id`) AS `QuantDisciplinas`,
        ROUND((`integradoraConf`.`integconf_pontos` / (`integradoraConf`.`integconf_questoes` * (SELECT 
                        COUNT(DISTINCT `acadperiodo__aluno_disciplina`.`disc_id`)
                    FROM
                        (`acadperiodo__aluno_disciplina`
                        JOIN `acadperiodo__integradora_aluno` ON ((`acadperiodo__aluno_disciplina`.`alunodisc_id` = `acadperiodo__integradora_aluno`.`alunodisc_id`)))
                    WHERE
                        (`alunoDisciplina`.`alunoper_id` = `acadperiodo__aluno_disciplina`.`alunoper_id`)
                    GROUP BY `alunoDisciplina`.`alunoper_id`))),
                2) AS `PontosPorQuestao`,
        (SELECT 
                SUM(`acadperiodo__integradora_aluno`.`alunointeg_acertos`)
            FROM
                (`acadperiodo__integradora_aluno`
                JOIN `acadperiodo__aluno_disciplina` ON ((`acadperiodo__integradora_aluno`.`alunodisc_id` = `acadperiodo__aluno_disciplina`.`alunodisc_id`)))
            WHERE
                (`acadperiodo__aluno_disciplina`.`alunoper_id` = `alunoDisciplina`.`alunoper_id`)
            GROUP BY `acadperiodo__aluno_disciplina`.`alunoper_id`) AS `TotalAcertos`,
        CEILING((ROUND((`integradoraConf`.`integconf_pontos` / (`integradoraConf`.`integconf_questoes` * (SELECT 
                                COUNT(0)
                            FROM
                                (`acadperiodo__aluno_disciplina`
                                JOIN `acadperiodo__integradora_aluno` ON ((`acadperiodo__aluno_disciplina`.`alunodisc_id` = `acadperiodo__integradora_aluno`.`alunodisc_id`)))
                            WHERE
                                (`alunoDisciplina`.`alunoper_id` = `acadperiodo__aluno_disciplina`.`alunoper_id`)
                            GROUP BY `alunoDisciplina`.`alunoper_id`))),
                        2) * (SELECT 
                        SUM(`acadperiodo__integradora_aluno`.`alunointeg_acertos`)
                    FROM
                        (`acadperiodo__integradora_aluno`
                        JOIN `acadperiodo__aluno_disciplina` ON ((`acadperiodo__integradora_aluno`.`alunodisc_id` = `acadperiodo__aluno_disciplina`.`alunodisc_id`)))
                    WHERE
                        (`acadperiodo__aluno_disciplina`.`alunoper_id` = `alunoDisciplina`.`alunoper_id`)
                    GROUP BY `acadperiodo__aluno_disciplina`.`alunoper_id`))) AS `NGC`,
        CEILING(((`alunoIntegradora`.`alunointeg_nota_isolada` + (ROUND((`integradoraConf`.`integconf_pontos` / (`integradoraConf`.`integconf_questoes` * (SELECT 
                                COUNT(0)
                            FROM
                                (`acadperiodo__aluno_disciplina`
                                JOIN `acadperiodo__integradora_aluno` ON ((`acadperiodo__aluno_disciplina`.`alunodisc_id` = `acadperiodo__integradora_aluno`.`alunodisc_id`)))
                            WHERE
                                (`alunoDisciplina`.`alunoper_id` = `acadperiodo__aluno_disciplina`.`alunoper_id`)
                            GROUP BY `alunoDisciplina`.`alunoper_id`))),
                        2) * (SELECT 
                        SUM(`acadperiodo__integradora_aluno`.`alunointeg_acertos`)
                    FROM
                        (`acadperiodo__integradora_aluno`
                        JOIN `acadperiodo__aluno_disciplina` ON ((`acadperiodo__integradora_aluno`.`alunodisc_id` = `acadperiodo__aluno_disciplina`.`alunodisc_id`)))
                    WHERE
                        (`acadperiodo__aluno_disciplina`.`alunoper_id` = `alunoDisciplina`.`alunoper_id`)
                    GROUP BY `acadperiodo__aluno_disciplina`.`alunoper_id`))) / 2)) AS `NFD`
    FROM
        ((((`acadperiodo__letivo` `periodoLetivo`
        LEFT JOIN (`acadgeral__situacao` `situacao`
        LEFT JOIN (`acadperiodo__turma` `turma`
        LEFT JOIN (`pessoa`
        LEFT JOIN (`acadgeral__aluno` `alunoGeral`
        LEFT JOIN (`acadgeral__aluno_curso` `alunoCurso`
        LEFT JOIN (`acadperiodo__aluno` `alunoPeriodo`
        LEFT JOIN (`acadperiodo__aluno_disciplina` `alunoDisciplina`
        LEFT JOIN `acadperiodo__integradora_aluno` `alunoIntegradora` ON ((`alunoIntegradora`.`alunodisc_id` = `alunoDisciplina`.`alunodisc_id`))) ON (((`alunoDisciplina`.`turma_id` = `alunoPeriodo`.`turma_id`)
            AND (`alunoDisciplina`.`alunoper_id` = `alunoPeriodo`.`alunoper_id`)))) ON ((`alunoPeriodo`.`alunocurso_id` = `alunoCurso`.`alunocurso_id`))) ON ((`alunoCurso`.`aluno_id` = `alunoGeral`.`aluno_id`))) ON ((`alunoGeral`.`pes_id` = `pessoa`.`pes_id`))) ON ((`turma`.`turma_id` = `alunoPeriodo`.`turma_id`))) ON ((`situacao`.`situacao_id` = `alunoPeriodo`.`matsituacao_id`))) ON ((`periodoLetivo`.`per_id` = `turma`.`per_id`)))
        LEFT JOIN `acadperiodo__integradora_conf` `integradoraConf` ON (((`integradoraConf`.`per_id_final` <= `periodoLetivo`.`per_id`)
            OR ((`integradoraConf`.`per_id_inicial` <= `periodoLetivo`.`per_id`)
            AND ISNULL(`integradoraConf`.`per_id_final`)))))
        LEFT JOIN `acadgeral__disciplina` `disciplina` ON ((`disciplina`.`disc_id` = `alunoDisciplina`.`disc_id`)))
        LEFT JOIN `acadperiodo__integradora` `integradora` ON ((`integradora`.`integradora_id` = `alunoIntegradora`.`integradora_id`)))
    WHERE
        ((`situacao`.`matsit_descricao` = 'Matriculado')
            AND (`turma`.`tturma_id` = (SELECT 
                `turmaTipo`.`tturma_id`
            FROM
                `acadgeral__turma_tipo` `turmaTipo`
            WHERE
                (`turmaTipo`.`tturma_descricao` = 'Convencional')))
            AND (`turma`.`turma_serie` <= `integradoraConf`.`integconf_serie_max`));





-- -----------------------------------------------------
-- View `view__mensalidades`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `view__mensalidades`;

CREATE 
     OR REPLACE ALGORITHM = UNDEFINED 
    SQL SECURITY DEFINER
VIEW `view__mensalidades` AS
    SELECT 
        `acadperiodo__letivo`.`per_ano` AS `Ano`,
        `acadperiodo__letivo`.`per_nome` AS `Semestre`,
        `financeiro__titulo`.`pes_id` AS `Pessoa`,
        `acadgeral__aluno_curso`.`alunocurso_id` AS `Matricula`,
        `acadperiodo__aluno`.`alunoper_id` AS `alunoper_id`,
        `pessoa`.`pes_nome` AS `Nome`,
        `financeiro__titulo`.`titulo_id` AS `Titulo`,
        `acadperiodo__turma`.`turma_nome` AS `Turma`,
        `acadperiodo__turma`.`turma_serie` AS `Periodo`,
        `acadperiodo__turma`.`turma_turno` AS `Turno`,
        `financeiro__titulo_mensalidade`.`mensalidade_parcela` AS `Parcela`,
        CAST(`financeiro__titulo`.`titulo_data_processamento`
            AS DATE) AS `Processamento`,
        CAST(`financeiro__titulo`.`titulo_data_vencimento`
            AS DATE) AS `Vencimento`,
        `financeiro__titulo`.`titulo_valor` AS `Valor`,
        `financeiro__titulo`.`titulo_tipo_pagamento` AS `TipoPagamento`,
        CAST(`financeiro__titulo`.`titulo_data_pagamento`
            AS DATE) AS `Pagamento`,
        `financeiro__titulo`.`titulo_multa` AS `Multa`,
        `financeiro__titulo`.`titulo_juros` AS `Juros`,
        `financeiro__titulo`.`titulo_valor_pago` AS `ValorPago`,
        `financeiro__titulo`.`titulo_estado` AS `Estado`,
        `financeiro__titulo`.`titulo_valor_pago_dinheiro` AS `ValorDinheiro`,
        `financeiro__titulo`.`titulo_desconto` AS `Desconto`,
        `financeiro__titulo`.`titulo_desconto_manual` AS `DescontoManual`,
        `financeiro__titulo`.`titulo_acrescimo_manual` AS `AcrescimoManual`,
        `acadgeral__situacao`.`matsit_descricao` AS `Situacao`
    FROM
        (((((((((`financeiro__titulo_tipo`
        JOIN `financeiro__titulo` ON ((`financeiro__titulo_tipo`.`tipotitulo_id` = `financeiro__titulo`.`tipotitulo_id`)))
        JOIN `pessoa` ON ((`financeiro__titulo`.`pes_id` = `pessoa`.`pes_id`)))
        JOIN `acadgeral__aluno` ON ((`financeiro__titulo`.`pes_id` = `acadgeral__aluno`.`pes_id`)))
        JOIN `acadgeral__aluno_curso` ON ((`acadgeral__aluno`.`aluno_id` = `acadgeral__aluno_curso`.`aluno_id`)))
        JOIN `financeiro__titulo_mensalidade` ON ((`financeiro__titulo`.`titulo_id` = `financeiro__titulo_mensalidade`.`titulo_id`)))
        JOIN `acadperiodo__aluno` ON (((`acadgeral__aluno_curso`.`alunocurso_id` = `acadperiodo__aluno`.`alunocurso_id`)
            AND (`financeiro__titulo_mensalidade`.`alunoper_id` = `acadperiodo__aluno`.`alunoper_id`))))
        JOIN `acadperiodo__turma` ON (((`acadgeral__aluno_curso`.`cursocampus_id` = `acadperiodo__turma`.`cursocampus_id`)
            AND (`acadperiodo__aluno`.`turma_id` = `acadperiodo__turma`.`turma_id`))))
        JOIN `acadperiodo__letivo` ON ((`acadperiodo__turma`.`per_id` = `acadperiodo__letivo`.`per_id`)))
        JOIN `acadgeral__situacao` ON ((`acadgeral__situacao`.`situacao_id` = `acadperiodo__aluno`.`matsituacao_id`)))
    WHERE
        (`financeiro__titulo_tipo`.`tipotitulo_nome` = 'Mensalidade');





-- -----------------------------------------------------
-- View `view__titulos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `view__titulos`;

CREATE 
     OR REPLACE ALGORITHM = UNDEFINED 
    SQL SECURITY DEFINER
VIEW `view__titulos` AS
    SELECT 
        `selecao_inscricao`.`inscricao_id` AS `Inscricao`,
        `selecao_inscricao`.`edicao_id` AS `Vestibular`,
        `pessoa`.`pes_nome` AS `Nome`,
        `financeiro__titulo`.`titulo_id` AS `Numero`,
        `financeiro__titulo_tipo`.`tipotitulo_nome` AS `Tipo`,
        `financeiro__titulo`.`titulo_tipo_pagamento` AS `FormaPagamento`,
        `financeiro__titulo`.`titulo_data_processamento` AS `Emissao`,
        `financeiro__titulo`.`titulo_valor` AS `Valor`,
        IF((`financeiro__titulo`.`titulo_data_pagamento` IS NOT NULL),
            `financeiro__titulo`.`titulo_data_pagamento`,
            NULL) AS `Pagamento`,
        `financeiro__titulo`.`titulo_valor_pago` AS `Valor Pago`
    FROM
        ((((`financeiro__titulo`
        JOIN `pessoa_fisica` ON ((`financeiro__titulo`.`pes_id` = `pessoa_fisica`.`pes_id`)))
        JOIN `selecao_inscricao` ON ((`financeiro__titulo`.`pes_id` = `selecao_inscricao`.`pes_id`)))
        JOIN `pessoa` ON ((`financeiro__titulo`.`pes_id` = `pessoa`.`pes_id`)))
        JOIN `financeiro__titulo_tipo` ON ((`financeiro__titulo`.`tipotitulo_id` = `financeiro__titulo_tipo`.`tipotitulo_id`)));





-- -----------------------------------------------------
-- View `view__vestibular_classificacao`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `view__vestibular_classificacao`;

CREATE 
     OR REPLACE ALGORITHM = UNDEFINED 
    SQL SECURITY DEFINER
VIEW `view__vestibular_classificacao` AS
    SELECT 
        `pessoa`.`pes_nome` AS `pes_nome`,
        `selecao_inscricao`.`edicao_id` AS `edicao_id`,
        `selecao_inscricao`.`inscricao_id` AS `inscricao_id`,
        `selecao_inscricao`.`pes_id` AS `pes_id`,
        `selecao_inscricao`.`inscricao_lingua_estrangeira` AS `inscricao_lingua_estrangeira`,
        (IF((`selecao_tipo`.`tiposel_nome` = 'Vestibular'),
            'Vestibular',
            IF((`selecao_tipo`.`tiposel_nome` = 'Notas do Enem'),
                'Enem',
                IF(((((`pp`.`provainsc_pontuacao` + `p`.`provainsc_pontuacao`) + `pc`.`provainsc_pontuacao`) + `pr`.`provainsc_pontuacao`) >= ((CEILING(ROUND(((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000),
                                    0)) + CEILING(ROUND(((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000),
                                    0))) + CEILING(ROUND(((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000),
                                    0)))),
                    'Vestibular',
                    'Notas do Enem'))) COLLATE utf8_general_ci) AS `ingresso`,
        IF((`selecao_tipo`.`tiposel_nome` = 'Vestibular'),
            (`p`.`provainsc_pontuacao` + `pc`.`provainsc_pontuacao`),
            IF((`selecao_tipo`.`tiposel_nome` = 'Notas do Enem'),
                CEILING(ROUND(((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000),
                                0)),
                IF(((((`pp`.`provainsc_pontuacao` + `p`.`provainsc_pontuacao`) + `pc`.`provainsc_pontuacao`) + `pr`.`provainsc_pontuacao`) >= ((CEILING(ROUND(((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000),
                                    0)) + CEILING(ROUND(((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000),
                                    0))) + CEILING(ROUND(((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000),
                                    0)))),
                    (`p`.`provainsc_pontuacao` + `pc`.`provainsc_pontuacao`),
                    CEILING(ROUND(((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000),
                                    0))))) AS `conhecimento`,
        IF((`selecao_tipo`.`tiposel_nome` = 'Vestibular'),
            `pr`.`provainsc_pontuacao`,
            IF((`selecao_tipo`.`tiposel_nome` = 'Notas do Enem'),
                CEILING(ROUND(((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000),
                                0)),
                IF(((((`pp`.`provainsc_pontuacao` + `p`.`provainsc_pontuacao`) + `pc`.`provainsc_pontuacao`) + `pr`.`provainsc_pontuacao`) >= ((CEILING(ROUND(((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000),
                                    0)) + CEILING(ROUND(((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000),
                                    0))) + CEILING(ROUND(((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000),
                                    0)))),
                    `pr`.`provainsc_pontuacao`,
                    CEILING(ROUND(((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000),
                                    0))))) AS `redacao`,
        IF((`selecao_tipo`.`tiposel_nome` = 'Vestibular'),
            `pp`.`provainsc_pontuacao`,
            IF((`selecao_tipo`.`tiposel_nome` = 'Notas do Enem'),
                CEILING(ROUND(((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000),
                                0)),
                IF(((((`pp`.`provainsc_pontuacao` + `p`.`provainsc_pontuacao`) + `pc`.`provainsc_pontuacao`) + `pr`.`provainsc_pontuacao`) >= ((CEILING(ROUND(((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000),
                                    0)) + CEILING(ROUND(((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000),
                                    0))) + CEILING(ROUND(((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000),
                                    0)))),
                    `pp`.`provainsc_pontuacao`,
                    CEILING(ROUND(((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000),
                                    0))))) AS `portugues`,
        IF((`selecao_tipo`.`tiposel_nome` = 'Vestibular'),
            (((`pp`.`provainsc_pontuacao` + `p`.`provainsc_pontuacao`) + `pc`.`provainsc_pontuacao`) + `pr`.`provainsc_pontuacao`),
            IF((`selecao_tipo`.`tiposel_nome` = 'Notas do Enem'),
                ((CEILING(ROUND(((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000),
                                0)) + CEILING(ROUND(((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000),
                                0))) + CEILING(ROUND(((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000),
                                0))),
                IF(((((`pp`.`provainsc_pontuacao` + `p`.`provainsc_pontuacao`) + `pc`.`provainsc_pontuacao`) + `pr`.`provainsc_pontuacao`) >= ((CEILING(ROUND(((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000),
                                    0)) + CEILING(ROUND(((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000),
                                    0))) + CEILING(ROUND(((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000),
                                    0)))),
                    (((`pp`.`provainsc_pontuacao` + `p`.`provainsc_pontuacao`) + `pc`.`provainsc_pontuacao`) + `pr`.`provainsc_pontuacao`),
                    ((CEILING(ROUND(((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000),
                                    0)) + CEILING(ROUND(((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000),
                                    0))) + CEILING(ROUND(((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000),
                                    0)))))) AS `inscricao_nota`,
        (IF(((`pr`.`provainsc_pontuacao` > 4)
                OR (`selecao_inscricao`.`inscricao_nota_enem_redacao` > 4)),
            'CLASSIFICADO',
            'DESCLASSIFICADO') COLLATE utf8_general_ci) AS `resultado`
    FROM
        ((((((((`selecao_inscricao`
        JOIN `selecao_edicao` ON ((`selecao_inscricao`.`edicao_id` = `selecao_edicao`.`edicao_id`)))
        JOIN `pessoa` ON (((`selecao_inscricao`.`pes_id` = `pessoa`.`pes_id`)
            AND (`selecao_edicao`.`edicao_id` = `selecao_inscricao`.`edicao_id`))))
        JOIN `selecao_tipo_edicao` ON (((`selecao_inscricao`.`edicao_id` = `selecao_tipo_edicao`.`edicao_id`)
            AND (`selecao_inscricao`.`seletipoedicao_id` = `selecao_tipo_edicao`.`seletipoedicao_id`))))
        JOIN `selecao_tipo` ON ((`selecao_tipo_edicao`.`tiposel_id` = `selecao_tipo`.`tiposel_id`)))
        LEFT JOIN `prova_inscrito` `p` ON (((`selecao_inscricao`.`inscricao_id` = `p`.`inscricao_id`)
            AND (`p`.`proedicao_id` = (SELECT 
                `prova_edicao`.`proedicao_id`
            FROM
                (`prova_edicao`
            JOIN `prova` ON ((`prova_edicao`.`prova_id` = `prova`.`prova_id`)))
            WHERE
                ((`prova`.`prova_descricao` = `selecao_inscricao`.`inscricao_lingua_estrangeira`)
                    AND (`prova_edicao`.`edicao_id` = `selecao_inscricao`.`edicao_id`))
            LIMIT 1)))))
        LEFT JOIN `prova_inscrito` `pr` ON (((`selecao_inscricao`.`inscricao_id` = `pr`.`inscricao_id`)
            AND (`pr`.`proedicao_id` = (SELECT 
                `prova_edicao`.`proedicao_id`
            FROM
                (`prova_edicao`
            JOIN `prova` ON ((`prova_edicao`.`prova_id` = `prova`.`prova_id`)))
            WHERE
                ((`prova`.`prova_descricao` = 'Redação')
                    AND (`prova_edicao`.`edicao_id` = `selecao_inscricao`.`edicao_id`)))))))
        LEFT JOIN `prova_inscrito` `pp` ON (((`selecao_inscricao`.`inscricao_id` = `pp`.`inscricao_id`)
            AND (`pp`.`proedicao_id` = (SELECT 
                `prova_edicao`.`proedicao_id`
            FROM
                (`prova_edicao`
            JOIN `prova` ON ((`prova_edicao`.`prova_id` = `prova`.`prova_id`)))
            WHERE
                ((`prova`.`prova_descricao` = 'Lingua Portuguesa e Literatura Brasileira')
                    AND (`prova_edicao`.`edicao_id` = `selecao_inscricao`.`edicao_id`)))))))
        LEFT JOIN `prova_inscrito` `pc` ON (((`selecao_inscricao`.`inscricao_id` = `pc`.`inscricao_id`)
            AND (`pc`.`proedicao_id` = (SELECT 
                `prova_edicao`.`proedicao_id`
            FROM
                (`prova_edicao`
            JOIN `prova` ON ((`prova_edicao`.`prova_id` = `prova`.`prova_id`)))
            WHERE
                ((`prova`.`prova_descricao` = 'Conhecimentos Gerais')
                    AND (`prova_edicao`.`edicao_id` = `selecao_inscricao`.`edicao_id`)))))))
    WHERE
        ((`selecao_inscricao`.`inscricao_status` = 'aceita')
            AND ((`p`.`provainsc_id` IS NOT NULL)
            OR (`selecao_tipo`.`tiposel_nome` = 'Notas do Enem')
            OR (`selecao_tipo`.`tiposel_nome` = 'Enem e Vestibular')))
    ORDER BY `selecao_inscricao`.`edicao_id` , IF(((`pr`.`provainsc_pontuacao` > 4)
            OR (`selecao_inscricao`.`inscricao_nota_enem_redacao` > 4)),
        'CLASSIFICADO',
        'DESCLASSIFICADO') , IF((`selecao_tipo`.`tiposel_nome` = 'Vestibular'),
        (((`pp`.`provainsc_pontuacao` + `p`.`provainsc_pontuacao`) + `pc`.`provainsc_pontuacao`) + `pr`.`provainsc_pontuacao`),
        IF((`selecao_tipo`.`tiposel_nome` = 'Notas do Enem'),
            CEILING(ROUND(((((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000) + ((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000)) + ((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000)),
                            2)),
            IF(((((`pp`.`provainsc_pontuacao` + `p`.`provainsc_pontuacao`) + `pc`.`provainsc_pontuacao`) + `pr`.`provainsc_pontuacao`) >= CEILING(ROUND(((((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000) + ((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000)) + ((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000)),
                                0))),
                (((`pp`.`provainsc_pontuacao` + `p`.`provainsc_pontuacao`) + `pc`.`provainsc_pontuacao`) + `pr`.`provainsc_pontuacao`),
                CEILING(ROUND(((((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000) + ((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000)) + ((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000)),
                                0))))) DESC , IF((`selecao_tipo`.`tiposel_nome` = 'Vestibular'),
        `pr`.`provainsc_pontuacao`,
        IF((`selecao_tipo`.`tiposel_nome` = 'Notas do Enem'),
            CEILING(ROUND(((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000),
                            2)),
            IF(((((`pp`.`provainsc_pontuacao` + `p`.`provainsc_pontuacao`) + `pc`.`provainsc_pontuacao`) + `pr`.`provainsc_pontuacao`) >= CEILING(ROUND(((((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000) + ((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000)) + ((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000)),
                                0))),
                `pr`.`provainsc_pontuacao`,
                CEILING(ROUND(((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000),
                                0))))) DESC , IF((`selecao_tipo`.`tiposel_nome` = 'Vestibular'),
        `pp`.`provainsc_pontuacao`,
        IF((`selecao_tipo`.`tiposel_nome` = 'Notas do Enem'),
            CEILING(ROUND(((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000),
                            2)),
            IF(((((`pp`.`provainsc_pontuacao` + `p`.`provainsc_pontuacao`) + `pc`.`provainsc_pontuacao`) + `pr`.`provainsc_pontuacao`) >= CEILING(ROUND(((((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000) + ((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000)) + ((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000)),
                                0))),
                `pp`.`provainsc_pontuacao`,
                CEILING(ROUND(((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000),
                                0))))) DESC , IF((`selecao_tipo`.`tiposel_nome` = 'Vestibular'),
        (`p`.`provainsc_pontuacao` + `pc`.`provainsc_pontuacao`),
        IF((`selecao_tipo`.`tiposel_nome` = 'Notas do Enem'),
            CEILING(ROUND(((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000),
                            2)),
            IF(((((`pp`.`provainsc_pontuacao` + `p`.`provainsc_pontuacao`) + `pc`.`provainsc_pontuacao`) + `pr`.`provainsc_pontuacao`) >= CEILING(ROUND(((((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000) + ((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000)) + ((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000)),
                                0))),
                (`p`.`provainsc_pontuacao` + `pc`.`provainsc_pontuacao`),
                CEILING(ROUND(((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000),
                                0))))) DESC;





-- -----------------------------------------------------
-- View `view__notas_etapas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `view__notas_etapas`;

CREATE 
     OR REPLACE ALGORITHM = UNDEFINED 
    SQL SECURITY DEFINER
VIEW `view__notas_etapas` AS
    SELECT 
        `prof`.`pes_id` AS `professor_id`,
        `prof`.`pes_nome` AS `professor_nome`,
        `turma`.`turma_id` AS `turma_id`,
        `turma`.`turma_nome` AS `turma_nome`,
        `docdisc`.`docdisc_id` AS `docdisc_id`,
        `disc`.`disc_id` AS `disc_id`,
        `disc`.`disc_nome` AS `disc_nome`,
        `alunocur`.`alunocurso_id` AS `alunocurso_id`,
        `pesaluno`.`pes_id` AS `aluno_id`,
        `pesaluno`.`pes_nome` AS `aluno_nome`,
        `situacaoGeral`.`situacao_id` AS `situacao_aluno_id`,
        `situacaoGeral`.`matsit_descricao` AS `situacao_aluno_descricao`,
        `situacaoDisc`.`situacao_id` AS `situacao_disciplina_id`,
        `situacaoDisc`.`matsit_descricao` AS `situacao_disciplina_descricao`,
        `alunodisc`.`alunodisc_id` AS `alunodisc_id`,
        (SELECT 
                `acadperiodo__etapa_aluno`.`alunoetapa_nota`
            FROM
                ((`acadperiodo__etapa_aluno`
                JOIN `acadperiodo__etapa_diario` ON ((`acadperiodo__etapa_aluno`.`diario_id` = `acadperiodo__etapa_diario`.`diario_id`)))
                JOIN `acadperiodo__etapas` ON ((`acadperiodo__etapa_diario`.`etapa_id` = `acadperiodo__etapas`.`etapa_id`)))
            WHERE
                ((`acadperiodo__etapas`.`etapa_ordem` = 1)
                    AND (`alunodisc`.`alunodisc_id` = `acadperiodo__etapa_aluno`.`alunodisc_id`)
                    AND (`acadperiodo__etapa_aluno`.`alunoetapa_nota` IS NOT NULL))
            ORDER BY `alunodisc`.`alunodisc_sit_data` DESC
            LIMIT 1) AS `nota_etapa_01`,
        (SELECT 
                `acadperiodo__etapa_aluno`.`alunoetapa_nota`
            FROM
                ((`acadperiodo__etapa_aluno`
                JOIN `acadperiodo__etapa_diario` ON ((`acadperiodo__etapa_aluno`.`diario_id` = `acadperiodo__etapa_diario`.`diario_id`)))
                JOIN `acadperiodo__etapas` ON ((`acadperiodo__etapa_diario`.`etapa_id` = `acadperiodo__etapas`.`etapa_id`)))
            WHERE
                ((`acadperiodo__etapas`.`etapa_ordem` = 2)
                    AND (`alunodisc`.`alunodisc_id` = `acadperiodo__etapa_aluno`.`alunodisc_id`)
                    AND (`acadperiodo__etapa_aluno`.`alunoetapa_nota` IS NOT NULL))
            ORDER BY `alunodisc`.`alunodisc_sit_data` DESC
            LIMIT 1) AS `nota_etapa_02`,
        (SELECT 
                `acadperiodo__etapa_aluno`.`alunoetapa_nota`
            FROM
                ((`acadperiodo__etapa_aluno`
                JOIN `acadperiodo__etapa_diario` ON ((`acadperiodo__etapa_aluno`.`diario_id` = `acadperiodo__etapa_diario`.`diario_id`)))
                JOIN `acadperiodo__etapas` ON ((`acadperiodo__etapa_diario`.`etapa_id` = `acadperiodo__etapas`.`etapa_id`)))
            WHERE
                ((`acadperiodo__etapas`.`etapa_ordem` = 3)
                    AND (`alunodisc`.`alunodisc_id` = `acadperiodo__etapa_aluno`.`alunodisc_id`)
                    AND (`acadperiodo__etapa_aluno`.`alunoetapa_nota` IS NOT NULL))
            ORDER BY `alunodisc`.`alunodisc_sit_data` DESC
            LIMIT 1) AS `nota_etapa_03`
    FROM
        ((((((((((((((`acadperiodo__etapa_aluno`
        JOIN `acadperiodo__etapa_diario` ON ((`acadperiodo__etapa_aluno`.`diario_id` = `acadperiodo__etapa_diario`.`diario_id`)))
        JOIN `acadperiodo__docente_disciplina` `docdisc` ON ((`acadperiodo__etapa_diario`.`docdisc_id` = `docdisc`.`docdisc_id`)))
        JOIN `acadgeral__docente` ON ((`docdisc`.`docente_id` = `acadgeral__docente`.`docente_id`)))
        JOIN `pessoa` `prof` ON ((`acadgeral__docente`.`pes_id` = `prof`.`pes_id`)))
        JOIN `acadperiodo__turma` `turma` ON ((`turma`.`turma_id` = `docdisc`.`turma_id`)))
        JOIN `acadperiodo__aluno_disciplina` `alunodisc` ON ((`alunodisc`.`alunodisc_id` = `acadperiodo__etapa_aluno`.`alunodisc_id`)))
        JOIN `acadperiodo__aluno` `alunoper` ON ((`alunoper`.`alunoper_id` = `alunodisc`.`alunoper_id`)))
        JOIN `acadgeral__aluno_curso` `alunocur` ON ((`alunocur`.`alunocurso_id` = `alunoper`.`alunocurso_id`)))
        JOIN `acadgeral__aluno` `aluno` ON ((`aluno`.`aluno_id` = `alunocur`.`aluno_id`)))
        JOIN `pessoa` `pesaluno` ON ((`pesaluno`.`pes_id` = `aluno`.`pes_id`)))
        JOIN `acadgeral__situacao` `situacaoDisc` ON ((`situacaoDisc`.`situacao_id` = `alunodisc`.`situacao_id`)))
        JOIN `acadgeral__disciplina` `disc` ON ((`disc`.`disc_id` = `docdisc`.`disc_id`)))
        JOIN `acadgeral__situacao` `situacaoGeral` ON ((`situacaoGeral`.`situacao_id` = `alunoper`.`matsituacao_id`)))
        JOIN `acadperiodo__letivo` ON ((`acadperiodo__letivo`.`per_id` = `turma`.`per_id`)))
    GROUP BY `alunodisc`.`alunodisc_id`;





-- -----------------------------------------------------
-- View `view__faltas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `view__faltas`;

CREATE 
     OR REPLACE ALGORITHM = UNDEFINED 
    SQL SECURITY DEFINER
VIEW `view__faltas` AS
    SELECT 
        `acadperiodo__turma`.`per_id` AS `per_id`,
        `acadperiodo__letivo`.`per_ano` AS `per_ano`,
        `acadperiodo__letivo`.`per_semestre` AS `per_semestre`,
        `acadperiodo__turma`.`turma_nome` AS `turma_nome`,
        `acadperiodo__turma`.`turma_serie` AS `turma_serie`,
        `acadperiodo__turma`.`turma_turno` AS `turma_turno`,
        `acadperiodo__turma`.`turma_ordem` AS `turma_ordem`,
        `acadgeral__aluno_curso`.`alunocurso_id` AS `Matricula`,
        `acadperiodo__frequencia`.`alunodisc_id` AS `alunodisc_id`,
        `pessoa`.`pes_nome` AS `Aluno`,
        `Professor`.`pes_nome` AS `Professor`,
        `situacaoGeral`.`matsit_descricao` AS `SituacaoGeral`,
        `acadgeral__disciplina`.`disc_nome` AS `Disciplina`,
        `acadgeral__situacao`.`matsit_descricao` AS `SituacaoDisciplina`,
        (IFNULL(`acadperiodo__matriz_disciplina`.`per_disc_chpratica`,
                0) + IFNULL(`acadperiodo__matriz_disciplina`.`per_disc_chteorica`,
                0)) AS `CH`,
        COUNT(0) AS `Faltas`,
        ROUND(((COUNT(0) * 100) / (IFNULL(`acadperiodo__matriz_disciplina`.`per_disc_chpratica`,
                        0) + IFNULL(`acadperiodo__matriz_disciplina`.`per_disc_chteorica`,
                        0))),
                0) AS `perc`
    FROM
        (((((((((((((((`acadperiodo__frequencia`
        JOIN `acadperiodo__aluno_disciplina` ON ((`acadperiodo__frequencia`.`alunodisc_id` = `acadperiodo__aluno_disciplina`.`alunodisc_id`)))
        JOIN `acadperiodo__aluno` ON (((`acadperiodo__aluno_disciplina`.`turma_id` = `acadperiodo__aluno`.`turma_id`)
            AND (`acadperiodo__aluno_disciplina`.`alunoper_id` = `acadperiodo__aluno`.`alunoper_id`))))
        JOIN `acadgeral__aluno_curso` ON ((`acadperiodo__aluno`.`alunocurso_id` = `acadgeral__aluno_curso`.`alunocurso_id`)))
        JOIN `acadgeral__aluno` ON ((`acadgeral__aluno_curso`.`aluno_id` = `acadgeral__aluno`.`aluno_id`)))
        JOIN `pessoa` ON ((`acadgeral__aluno`.`pes_id` = `pessoa`.`pes_id`)))
        JOIN `acadgeral__disciplina` ON ((`acadperiodo__aluno_disciplina`.`disc_id` = `acadgeral__disciplina`.`disc_id`)))
        JOIN `acadperiodo__turma` ON ((`acadperiodo__turma`.`turma_id` = `acadperiodo__aluno_disciplina`.`turma_id`)))
        JOIN `acadperiodo__letivo` ON ((`acadperiodo__turma`.`per_id` = `acadperiodo__letivo`.`per_id`)))
        JOIN `acadperiodo__matriz_curricular` ON ((`acadperiodo__turma`.`mat_cur_id` = `acadperiodo__matriz_curricular`.`mat_cur_id`)))
        JOIN `acadperiodo__matriz_disciplina` ON (((`acadperiodo__matriz_disciplina`.`mat_cur_id` = `acadperiodo__matriz_curricular`.`mat_cur_id`)
            AND (`acadperiodo__matriz_disciplina`.`disc_id` = `acadgeral__disciplina`.`disc_id`)
            AND (`acadperiodo__matriz_disciplina`.`per_disc_periodo` = `acadperiodo__turma`.`turma_serie`))))
        JOIN `acadgeral__situacao` ON ((`acadgeral__situacao`.`situacao_id` = `acadperiodo__aluno_disciplina`.`situacao_id`)))
        JOIN `acadgeral__situacao` `situacaoGeral` ON ((`situacaoGeral`.`situacao_id` = `acadperiodo__aluno`.`matsituacao_id`)))
        JOIN `acadperiodo__docente_disciplina` ON (((`acadperiodo__docente_disciplina`.`disc_id` = `acadgeral__disciplina`.`disc_id`)
            AND (`acadperiodo__docente_disciplina`.`turma_id` = `acadperiodo__turma`.`turma_id`))))
        JOIN `acadgeral__docente` ON ((`acadgeral__docente`.`docente_id` = `acadperiodo__docente_disciplina`.`docente_id`)))
        JOIN `pessoa` `Professor` ON ((`Professor`.`pes_id` = `acadgeral__docente`.`pes_id`)))
    GROUP BY `acadperiodo__frequencia`.`alunodisc_id`;





-- -----------------------------------------------------
-- View `view__notas_etapas_2`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `view__notas_etapas_2`;

CREATE 
     OR REPLACE ALGORITHM = UNDEFINED 
    SQL SECURITY DEFINER
VIEW `view__notas_etapas_2` AS
    SELECT 
        `PerLetivo`.`per_id` AS `per_id`,
        `PerLetivo`.`per_nome` AS `PeriodoLetivo`,
        `PerLetivo`.`per_ano` AS `Ano`,
        `PerLetivo`.`per_semestre` AS `Semestre`,
        `Docente`.`docente_id` AS `professor_id`,
        `PessoaProfessor`.`pes_nome` AS `professor_nome`,
        `Turma`.`turma_id` AS `turma_id`,
        `Turma`.`turma_nome` AS `turma_nome`,
        `Turma`.`turma_serie` AS `Periodo`,
        `Turma`.`turma_turno` AS `Turno`,
        `Turma`.`turma_ordem` AS `Ordem`,
        `DocenteDisc`.`docdisc_id` AS `docdisc_id`,
        `Disciplina`.`disc_id` AS `disc_id`,
        `Disciplina`.`disc_nome` AS `disc_nome`,
        `AlunoCurso`.`alunocurso_id` AS `Matricula`,
        `PessoaAluno`.`pes_id` AS `aluno_id`,
        `PessoaAluno`.`pes_nome` AS `aluno_nome`,
        `situacaoGeral`.`situacao_id` AS `situacao_aluno_id`,
        `situacaoGeral`.`matsit_descricao` AS `situacao_aluno_descricao`,
        `situacaoDisc`.`situacao_id` AS `situacao_disciplina_id`,
        `situacaoDisc`.`matsit_descricao` AS `situacao_disciplina_descricao`,
        `AlunoDisciplina`.`alunodisc_id` AS `alunodisc_id`,
        (SELECT 
                `EtapaAluno`.`alunoetapa_nota`
            FROM
                ((`acadperiodo__etapa_aluno` `EtapaAluno`
                JOIN `acadperiodo__etapa_diario` `EtapaDiario` ON ((`EtapaDiario`.`diario_id` = `EtapaAluno`.`diario_id`)))
                JOIN `acadperiodo__etapas` `Etapas` ON ((`Etapas`.`etapa_id` = `EtapaDiario`.`etapa_id`)))
            WHERE
                ((`Etapas`.`etapa_ordem` = 1)
                    AND (`EtapaAluno`.`alunodisc_id` = `AlunoDisciplina`.`alunodisc_id`)
                    AND (`EtapaDiario`.`docdisc_id` = `DocenteDisc`.`docdisc_id`))
            ORDER BY `AlunoDisciplina`.`alunodisc_sit_data` DESC
            LIMIT 1) AS `nota_etapa_01`,
        (SELECT 
                `EtapaAluno`.`alunoetapa_nota`
            FROM
                ((`acadperiodo__etapa_aluno` `EtapaAluno`
                JOIN `acadperiodo__etapa_diario` `EtapaDiario` ON ((`EtapaDiario`.`diario_id` = `EtapaAluno`.`diario_id`)))
                JOIN `acadperiodo__etapas` `Etapas` ON ((`Etapas`.`etapa_id` = `EtapaDiario`.`etapa_id`)))
            WHERE
                ((`Etapas`.`etapa_ordem` = 2)
                    AND (`EtapaAluno`.`alunodisc_id` = `AlunoDisciplina`.`alunodisc_id`)
                    AND (`EtapaDiario`.`docdisc_id` = `DocenteDisc`.`docdisc_id`))
            ORDER BY `AlunoDisciplina`.`alunodisc_sit_data` DESC
            LIMIT 1) AS `nota_etapa_02`,
        (SELECT 
                `EtapaAluno`.`alunoetapa_nota`
            FROM
                ((`acadperiodo__etapa_aluno` `EtapaAluno`
                JOIN `acadperiodo__etapa_diario` `EtapaDiario` ON ((`EtapaDiario`.`diario_id` = `EtapaAluno`.`diario_id`)))
                JOIN `acadperiodo__etapas` `Etapas` ON ((`Etapas`.`etapa_id` = `EtapaDiario`.`etapa_id`)))
            WHERE
                ((`Etapas`.`etapa_ordem` = 3)
                    AND (`EtapaAluno`.`alunodisc_id` = `AlunoDisciplina`.`alunodisc_id`)
                    AND (`EtapaDiario`.`docdisc_id` = `DocenteDisc`.`docdisc_id`))
            ORDER BY `AlunoDisciplina`.`alunodisc_sit_data` DESC
            LIMIT 1) AS `nota_etapa_03`
    FROM
        ((((((((((((`pessoa` `PessoaProfessor`
        JOIN `acadgeral__docente` `Docente` ON ((`PessoaProfessor`.`pes_id` = `Docente`.`pes_id`)))
        JOIN `acadperiodo__docente_disciplina` `DocenteDisc` ON ((`Docente`.`docente_id` = `DocenteDisc`.`docente_id`)))
        JOIN `acadgeral__disciplina` `Disciplina` ON ((`DocenteDisc`.`disc_id` = `Disciplina`.`disc_id`)))
        JOIN `acadperiodo__turma` `Turma` ON ((`Turma`.`turma_id` = `DocenteDisc`.`turma_id`)))
        JOIN `acadperiodo__letivo` `PerLetivo` ON ((`PerLetivo`.`per_id` = `Turma`.`per_id`)))
        JOIN `acadperiodo__aluno_disciplina` `AlunoDisciplina` ON (((`AlunoDisciplina`.`turma_id` = `Turma`.`turma_id`)
            AND (`AlunoDisciplina`.`disc_id` = `Disciplina`.`disc_id`))))
        JOIN `acadperiodo__aluno` `AlunoPeriodo` ON ((`AlunoPeriodo`.`alunoper_id` = `AlunoDisciplina`.`alunoper_id`)))
        JOIN `acadgeral__aluno_curso` `AlunoCurso` ON ((`AlunoCurso`.`alunocurso_id` = `AlunoPeriodo`.`alunocurso_id`)))
        JOIN `acadgeral__aluno` `Aluno` ON ((`Aluno`.`aluno_id` = `AlunoCurso`.`aluno_id`)))
        JOIN `pessoa` `PessoaAluno` ON ((`PessoaAluno`.`pes_id` = `Aluno`.`pes_id`)))
        JOIN `acadgeral__situacao` `situacaoDisc` ON ((`situacaoDisc`.`situacao_id` = `AlunoDisciplina`.`situacao_id`)))
        JOIN `acadgeral__situacao` `situacaoGeral` ON ((`situacaoGeral`.`situacao_id` = `AlunoPeriodo`.`matsituacao_id`)));





-- -----------------------------------------------------
-- View `view__vestibular_classificacao_2`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `view__vestibular_classificacao_2`;

CREATE 
     OR REPLACE ALGORITHM = UNDEFINED 
    SQL SECURITY DEFINER
VIEW `view__vestibular_classificacao_2` AS
    SELECT 
        `pessoa`.`pes_nome` AS `pes_nome`,
        `selecao_inscricao`.`edicao_id` AS `edicao_id`,
        `selecao_inscricao`.`inscricao_id` AS `inscricao_id`,
        `selecao_inscricao`.`pes_id` AS `pes_id`,
        `selecao_inscricao`.`inscricao_lingua_estrangeira` AS `inscricao_lingua_estrangeira`,
        IF((`selecao_tipo`.`tiposel_nome` = 'Vestibular'),
            'Vestibular',
            IF((`selecao_tipo`.`tiposel_nome` = 'Notas do Enem'),
                'Enem',
                IF(((((`pp`.`provainsc_pontuacao` + `p`.`provainsc_pontuacao`) + `pc`.`provainsc_pontuacao`) + `pr`.`provainsc_pontuacao`) >= ((CEILING(ROUND(((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000),
                                    0)) + CEILING(ROUND(((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000),
                                    0))) + CEILING(ROUND(((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000),
                                    0)))),
                    'Vestibular',
                    'Notas do Enem'))) AS `ingresso`,
        IF((`selecao_tipo`.`tiposel_nome` = 'Vestibular'),
            (`p`.`provainsc_pontuacao` + `pc`.`provainsc_pontuacao`),
            IF((`selecao_tipo`.`tiposel_nome` = 'Notas do Enem'),
                CEILING(ROUND(((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000),
                                0)),
                IF(((((`pp`.`provainsc_pontuacao` + `p`.`provainsc_pontuacao`) + `pc`.`provainsc_pontuacao`) + `pr`.`provainsc_pontuacao`) >= ((CEILING(ROUND(((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000),
                                    0)) + CEILING(ROUND(((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000),
                                    0))) + CEILING(ROUND(((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000),
                                    0)))),
                    (`p`.`provainsc_pontuacao` + `pc`.`provainsc_pontuacao`),
                    CEILING(ROUND(((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000),
                                    0))))) AS `conhecimento`,
        IF((`selecao_tipo`.`tiposel_nome` = 'Vestibular'),
            `pr`.`provainsc_pontuacao`,
            IF((`selecao_tipo`.`tiposel_nome` = 'Notas do Enem'),
                CEILING(ROUND(((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000),
                                0)),
                IF(((((`pp`.`provainsc_pontuacao` + `p`.`provainsc_pontuacao`) + `pc`.`provainsc_pontuacao`) + `pr`.`provainsc_pontuacao`) >= ((CEILING(ROUND(((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000),
                                    0)) + CEILING(ROUND(((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000),
                                    0))) + CEILING(ROUND(((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000),
                                    0)))),
                    `pr`.`provainsc_pontuacao`,
                    CEILING(ROUND(((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000),
                                    0))))) AS `redacao`,
        IF((`selecao_tipo`.`tiposel_nome` = 'Vestibular'),
            `pp`.`provainsc_pontuacao`,
            IF((`selecao_tipo`.`tiposel_nome` = 'Notas do Enem'),
                CEILING(ROUND(((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000),
                                0)),
                IF(((((`pp`.`provainsc_pontuacao` + `p`.`provainsc_pontuacao`) + `pc`.`provainsc_pontuacao`) + `pr`.`provainsc_pontuacao`) >= CEILING(ROUND(((((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000) + ((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000)) + ((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000)),
                                    0))),
                    `pp`.`provainsc_pontuacao`,
                    CEILING(ROUND(((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000),
                                    0))))) AS `portugues`,
        IF((`selecao_tipo`.`tiposel_nome` = 'Vestibular'),
            (((`pp`.`provainsc_pontuacao` + `p`.`provainsc_pontuacao`) + `pc`.`provainsc_pontuacao`) + `pr`.`provainsc_pontuacao`),
            IF((`selecao_tipo`.`tiposel_nome` = 'Notas do Enem'),
                ((CEILING(ROUND(((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000),
                                0)) + CEILING(ROUND(((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000),
                                0))) + CEILING(ROUND(((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000),
                                0))),
                IF(((((`pp`.`provainsc_pontuacao` + `p`.`provainsc_pontuacao`) + `pc`.`provainsc_pontuacao`) + `pr`.`provainsc_pontuacao`) >= ((CEILING(ROUND(((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000),
                                    0)) + CEILING(ROUND(((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000),
                                    0))) + CEILING(ROUND(((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000),
                                    0)))),
                    (((`pp`.`provainsc_pontuacao` + `p`.`provainsc_pontuacao`) + `pc`.`provainsc_pontuacao`) + `pr`.`provainsc_pontuacao`),
                    ((CEILING(ROUND(((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000),
                                    0)) + CEILING(ROUND(((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000),
                                    0))) + CEILING(ROUND(((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000),
                                    0)))))) AS `inscricao_nota`,
        IF(((`pr`.`provainsc_pontuacao` > 4)
                OR (`selecao_inscricao`.`inscricao_nota_enem_redacao` > 4)),
            'CLASSIFICADO',
            'DESCLASSIFICADO') AS `resultado`
    FROM
        ((((((((`selecao_inscricao`
        JOIN `selecao_edicao` ON ((`selecao_inscricao`.`edicao_id` = `selecao_edicao`.`edicao_id`)))
        JOIN `pessoa` ON (((`selecao_inscricao`.`pes_id` = `pessoa`.`pes_id`)
            AND (`selecao_edicao`.`edicao_id` = `selecao_inscricao`.`edicao_id`))))
        JOIN `selecao_tipo_edicao` ON (((`selecao_inscricao`.`edicao_id` = `selecao_tipo_edicao`.`edicao_id`)
            AND (`selecao_inscricao`.`seletipoedicao_id` = `selecao_tipo_edicao`.`seletipoedicao_id`))))
        JOIN `selecao_tipo` ON ((`selecao_tipo_edicao`.`tiposel_id` = `selecao_tipo`.`tiposel_id`)))
        LEFT JOIN `prova_inscrito` `p` ON (((`selecao_inscricao`.`inscricao_id` = `p`.`inscricao_id`)
            AND (`p`.`proedicao_id` = (SELECT 
                `prova_edicao`.`proedicao_id`
            FROM
                (`prova_edicao`
            JOIN `prova` ON ((`prova_edicao`.`prova_id` = `prova`.`prova_id`)))
            WHERE
                ((`prova`.`prova_descricao` = `selecao_inscricao`.`inscricao_lingua_estrangeira`)
                    AND (`prova_edicao`.`edicao_id` = `selecao_inscricao`.`edicao_id`))
            LIMIT 1)))))
        LEFT JOIN `prova_inscrito` `pr` ON (((`selecao_inscricao`.`inscricao_id` = `pr`.`inscricao_id`)
            AND (`pr`.`proedicao_id` = (SELECT 
                `prova_edicao`.`proedicao_id`
            FROM
                (`prova_edicao`
            JOIN `prova` ON ((`prova_edicao`.`prova_id` = `prova`.`prova_id`)))
            WHERE
                ((`prova`.`prova_descricao` = 'Redação')
                    AND (`prova_edicao`.`edicao_id` = `selecao_inscricao`.`edicao_id`)))))))
        LEFT JOIN `prova_inscrito` `pp` ON (((`selecao_inscricao`.`inscricao_id` = `pp`.`inscricao_id`)
            AND (`pp`.`proedicao_id` = (SELECT 
                `prova_edicao`.`proedicao_id`
            FROM
                (`prova_edicao`
            JOIN `prova` ON ((`prova_edicao`.`prova_id` = `prova`.`prova_id`)))
            WHERE
                ((`prova`.`prova_descricao` = 'Lingua Portuguesa e Literatura Brasileira')
                    AND (`prova_edicao`.`edicao_id` = `selecao_inscricao`.`edicao_id`)))))))
        LEFT JOIN `prova_inscrito` `pc` ON (((`selecao_inscricao`.`inscricao_id` = `pc`.`inscricao_id`)
            AND (`pc`.`proedicao_id` = (SELECT 
                `prova_edicao`.`proedicao_id`
            FROM
                (`prova_edicao`
            JOIN `prova` ON ((`prova_edicao`.`prova_id` = `prova`.`prova_id`)))
            WHERE
                ((`prova`.`prova_descricao` = 'Conhecimentos Gerais')
                    AND (`prova_edicao`.`edicao_id` = `selecao_inscricao`.`edicao_id`)))))))
    WHERE
        ((`selecao_inscricao`.`inscricao_status` = 'aceita')
            AND ((`p`.`provainsc_id` IS NOT NULL)
            OR (`selecao_tipo`.`tiposel_nome` = 'Notas do Enem')
            OR (`selecao_tipo`.`tiposel_nome` = 'Enem e Vestibular')))
    ORDER BY `selecao_inscricao`.`edicao_id` , IF(((`pr`.`provainsc_pontuacao` > 4)
            OR (`selecao_inscricao`.`inscricao_nota_enem_redacao` > 4)),
        'CLASSIFICADO',
        'DESCLASSIFICADO') , IF((`selecao_tipo`.`tiposel_nome` = 'Vestibular'),
        (((`pp`.`provainsc_pontuacao` + `p`.`provainsc_pontuacao`) + `pc`.`provainsc_pontuacao`) + `pr`.`provainsc_pontuacao`),
        IF((`selecao_tipo`.`tiposel_nome` = 'Notas do Enem'),
            CEILING(ROUND(((((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000) + ((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000)) + ((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000)),
                            2)),
            IF(((((`pp`.`provainsc_pontuacao` + `p`.`provainsc_pontuacao`) + `pc`.`provainsc_pontuacao`) + `pr`.`provainsc_pontuacao`) >= CEILING(ROUND(((((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000) + ((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000)) + ((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000)),
                                0))),
                (((`pp`.`provainsc_pontuacao` + `p`.`provainsc_pontuacao`) + `pc`.`provainsc_pontuacao`) + `pr`.`provainsc_pontuacao`),
                CEILING(ROUND(((((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000) + ((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000)) + ((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000)),
                                0))))) DESC , IF((`selecao_tipo`.`tiposel_nome` = 'Vestibular'),
        `pr`.`provainsc_pontuacao`,
        IF((`selecao_tipo`.`tiposel_nome` = 'Notas do Enem'),
            CEILING(ROUND(((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000),
                            2)),
            IF(((((`pp`.`provainsc_pontuacao` + `p`.`provainsc_pontuacao`) + `pc`.`provainsc_pontuacao`) + `pr`.`provainsc_pontuacao`) >= CEILING(ROUND(((((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000) + ((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000)) + ((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000)),
                                0))),
                `pr`.`provainsc_pontuacao`,
                CEILING(ROUND(((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000),
                                0))))) DESC , IF((`selecao_tipo`.`tiposel_nome` = 'Vestibular'),
        `pp`.`provainsc_pontuacao`,
        IF((`selecao_tipo`.`tiposel_nome` = 'Notas do Enem'),
            CEILING(ROUND(((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000),
                            2)),
            IF(((((`pp`.`provainsc_pontuacao` + `p`.`provainsc_pontuacao`) + `pc`.`provainsc_pontuacao`) + `pr`.`provainsc_pontuacao`) >= CEILING(ROUND(((((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000) + ((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000)) + ((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000)),
                                0))),
                `pp`.`provainsc_pontuacao`,
                CEILING(ROUND(((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000),
                                0))))) DESC , IF((`selecao_tipo`.`tiposel_nome` = 'Vestibular'),
        (`p`.`provainsc_pontuacao` + `pc`.`provainsc_pontuacao`),
        IF((`selecao_tipo`.`tiposel_nome` = 'Notas do Enem'),
            CEILING(ROUND(((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000),
                            2)),
            IF(((((`pp`.`provainsc_pontuacao` + `p`.`provainsc_pontuacao`) + `pc`.`provainsc_pontuacao`) + `pr`.`provainsc_pontuacao`) >= CEILING(ROUND(((((25 * `selecao_inscricao`.`inscricao_nota_enem_redacao`) / 1000) + ((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000)) + ((50 * `selecao_inscricao`.`inscricao_nota_enem_portugues`) / 1000)),
                                0))),
                (`p`.`provainsc_pontuacao` + `pc`.`provainsc_pontuacao`),
                CEILING(ROUND(((25 * (((`selecao_inscricao`.`inscricao_nota_enem_matematica` + `selecao_inscricao`.`inscricao_nota_enem_cienc_natureza`) + `selecao_inscricao`.`inscricao_nota_enem_cienc_humanas`) / 3)) / 1000),
                                0))))) DESC;





-- -----------------------------------------------------
-- View `view__vestibular_inscricoes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `view__vestibular_inscricoes`;

CREATE 
     OR REPLACE ALGORITHM = UNDEFINED 
    SQL SECURITY DEFINER
VIEW `view__vestibular_inscricoes` AS
    SELECT 
        CONCAT(CAST(`selecao_edicao`.`edicao_ano` AS CHAR CHARSET UTF8),
                '/',
                CAST(IF((`selecao_edicao`.`edicao_semestre` = 'Primeiro'),
                        1,
                        2)
                    AS CHAR CHARSET UTF8)) AS `Edicao`,
        `selecao_edicao`.`edicao_ano` AS `Ano`,
        IF((`selecao_edicao`.`edicao_semestre` = 'Primeiro'),
            1,
            2) AS `Semestre`,
        `acadperiodo__letivo`.`per_id` AS `PeriodoEdicao`,
        CAST(`selecao_data_realizacao`.`realizacao_data`
            AS DATE) AS `Realizacao`,
        `selecao_cursos`.`selcursos_periodos` AS `Turno`,
        `acad_curso`.`curso_nome` AS `Curso`,
        `selecao_tipo`.`tiposel_nome` AS `Modalidade`,
        `pessoa`.`pes_id` AS `pes_id`,
        `acadgeral__aluno_curso`.`alunocurso_id` AS `Matricula`,
        `acadperiodo__turma`.`per_id` AS `PeriodoMatricula`,
        `pessoa`.`pes_nome` AS `Candidato`,
        `pessoa_fisica`.`pes_sexo` AS `Genero`,
        `pessoa_fisica`.`pes_data_nascimento` AS `Nascimento`,
        `endereco`.`end_cidade` AS `Cidade`,
        `endereco`.`end_bairro` AS `Bairro`,
        `endereco`.`end_estado` AS `Estado`,
        `classificacao`.`inscricao_lingua_estrangeira` AS `LinguaEstrangeira`,
        IFNULL(`classificacao`.`inscricao_nota`, 0) AS `Nota`,
        `inscricao`.`inscricao_data` AS `DataInscricao`,
        `inscricao`.`inscricao_status` AS `Situacao`,
        IF((`prova_inscrito`.`inscricao_id` IS NOT NULL),
            'Presente',
            IF((`selecao_tipo`.`tiposel_prova` = 'Sim'),
                'Ausente',
                'Dispensado')) AS `Participacao`,
        `classificacao`.`resultado` AS `Resultado`,
        IF(((`acadperiodo__aluno`.`alunocurso_id` IS NOT NULL)
                AND (`acadperiodo__turma`.`per_id` IS NOT NULL)),
            'Sim',
            'Nao') AS `Matriculado`
    FROM
        ((((((((((((((((((`selecao_edicao`
        JOIN `selecao_data_realizacao` ON ((`selecao_edicao`.`edicao_id` = `selecao_data_realizacao`.`edicao_id`)))
        JOIN `selecao_cursos` ON ((`selecao_edicao`.`edicao_id` = `selecao_cursos`.`edicao_id`)))
        JOIN `campus_curso` ON ((`selecao_cursos`.`cursocampus_id` = `campus_curso`.`cursocampus_id`)))
        JOIN `acad_curso` ON ((`campus_curso`.`curso_id` = `acad_curso`.`curso_id`)))
        JOIN `selecao_inscricao` `inscricao` ON ((`inscricao`.`edicao_id` = `selecao_edicao`.`edicao_id`)))
        JOIN `inscricao_cursos` ON (((`inscricao_cursos`.`inscricao_id` = `inscricao`.`inscricao_id`)
            AND (`inscricao_cursos`.`selcursos_id` = `selecao_cursos`.`selcursos_id`))))
        JOIN `pessoa` ON ((`pessoa`.`pes_id` = `inscricao`.`pes_id`)))
        JOIN `pessoa_fisica` ON ((`pessoa_fisica`.`pes_id` = `pessoa`.`pes_id`)))
        JOIN `selecao_tipo_edicao` ON ((`inscricao`.`seletipoedicao_id` = `selecao_tipo_edicao`.`seletipoedicao_id`)))
        JOIN `selecao_tipo` ON ((`selecao_tipo`.`tiposel_id` = `selecao_tipo_edicao`.`tiposel_id`)))
        LEFT JOIN `view__vestibular_classificacao` `classificacao` ON ((`classificacao`.`inscricao_id` = `inscricao`.`inscricao_id`)))
        LEFT JOIN `prova_inscrito` ON ((`prova_inscrito`.`inscricao_id` = `inscricao`.`inscricao_id`)))
        LEFT JOIN `acadgeral__aluno` ON ((`acadgeral__aluno`.`pes_id` = `inscricao`.`pes_id`)))
        LEFT JOIN `acadgeral__aluno_curso` ON ((`acadgeral__aluno`.`aluno_id` = `acadgeral__aluno_curso`.`aluno_id`)))
        LEFT JOIN `acadperiodo__aluno` ON ((`acadperiodo__aluno`.`alunocurso_id` = `acadgeral__aluno_curso`.`alunocurso_id`)))
        LEFT JOIN `endereco` ON ((`endereco`.`pes_id` = `pessoa`.`pes_id`)))
        LEFT JOIN `acadperiodo__letivo` ON (((`acadperiodo__letivo`.`per_ano` = `selecao_edicao`.`edicao_ano`)
            AND (`acadperiodo__letivo`.`per_semestre` = IF((`selecao_edicao`.`edicao_semestre` = 'Primeiro'), 1, 2)))))
        LEFT JOIN `acadperiodo__turma` ON (((`acadperiodo__turma`.`turma_id` = `acadperiodo__aluno`.`turma_id`)
            AND (`acadperiodo__turma`.`per_id` = `acadperiodo__letivo`.`per_id`))))
    GROUP BY `inscricao`.`inscricao_id`;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
