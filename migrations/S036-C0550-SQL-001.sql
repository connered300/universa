INSERT INTO `org__comunicacao_tipo` (`tipo_id`, `tipo_descricao`, `tipo_contexto`)
VALUES (7, 'Comunicação Para Aluno Após Deferimento', 'Aluno');

INSERT INTO `org__comunicacao_modelo` (`tipo_id`, `usuario`, `modelo_nome`, `modelo_tipo`, `modelo_assunto`, `modelo_conteudo`, `modelo_data_inicio`, `modelo_data_fim`)
VALUES (7, 1, 'Email de Comunicação Para Aluno Após Deferimento', 'Email', 'Confirmação de Matrícula', '\n<strong>Prezado(a) {{ pessoa.pesNome }}, obrigado por escolher a {ies.iesSigla}}!</strong><br/>\nEste e-mail tem por objetivo informar que sua matrícula foi confirmada com sucesso no curso {{ curso.cursoNome }} !\n\n</br>Atenciosamente</br>\n\n{{ies.iesNome}} - {{ies.iesEndLogradouro}}&nbsp;Nº:&nbsp;{{ies.iesEndNumero}},{{ies.iesEndBairro}} - {{ies.iesEndCidade}}&nbsp;{{ies.iesEndEstado}}.</br>\n{% if ies.iesTelefone OR ies.iesEmail %} Contato:</br> {% if ies.iesTelefone %} Telefone: {{ies.iesTelefone }} </br>  {% endif %} {% ies.iesEmail %}E-mail:{{ies.iesEmail}}  {% endif %} {% endif %}', '2018-01-01', '2019-01-01');

INSERT INTO `org__comunicacao_modelo` (`tipo_id`, `usuario`, `modelo_nome`, `modelo_tipo`, `modelo_assunto`, `modelo_conteudo`, `modelo_data_inicio`, `modelo_data_fim`)
VALUES (7, 1, 'Sms de Comunicação Para Aluno Após Deferimento', 'Sms', 'Prezado(a) {{ pessoa.pesNome }}, sua matricula foi confirmada com sucesso no curso {{ curso.cursoNome }} !', '2018-01-01', '2019-01-01');

REPLACE INTO `org__comunicacao_modelo_grupo_permissao` (`grupo_id`, `modelo_id`)
SELECT 1, modelo_id FROM org__comunicacao_modelo ORDER BY modelo_id DESC LIMIT 0,2;