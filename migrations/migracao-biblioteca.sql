SET FOREIGN_KEY_CHECKS=0;
SET SQL_SAFE_UPDATES=0;
-- LIMPA TODAS AS TABELAS. Não limpar: biblioteca__cutter;

-- TRUNCATE biblioteca__grupo_bibliografico;
-- TRUNCATE biblioteca__area;
-- TRUNCATE biblioteca__area_cnpq;
-- TRUNCATE biblioteca__assunto;
-- TRUNCATE biblioteca__autor;
-- TRUNCATE biblioteca__colecao;
-- TRUNCATE biblioteca__grupo_leitor;
-- TRUNCATE biblioteca__setor;
-- TRUNCATE biblioteca__modalidade_emprestimo;
-- TRUNCATE biblioteca__modalidade_emprestimo_grupo;
-- TRUNCATE biblioteca__empresa;
-- SET @ultimaPF:=0;
-- SELECT max(pes_id) INTO @ultimaPF FROM pessoa WHERE pes_tipo='Fisica';
-- DELETE FROM biblioteca__empresa WHERE pes_id > @ultimaPF;
-- DELETE FROM pessoa_juridica WHERE pes_id > @ultimaPF;
-- DELETE FROM pessoa WHERE pes_id > @ultimaPF;
-- TRUNCATE biblioteca__pessoa;
-- TRUNCATE biblioteca__idioma;
-- TRUNCATE biblioteca__assunto_relacionado; -- TODO: Falta Fazer
-- TRUNCATE biblioteca__titulo;
-- TRUNCATE biblioteca__titulo_assunto;
-- TRUNCATE biblioteca__titulo_autor;
-- TRUNCATE biblioteca__titulo_idioma;
-- TRUNCATE biblioteca__exemplar;
-- TRUNCATE biblioteca__aquisicao;
-- TRUNCATE biblioteca__aquisicao_exemplar;
-- TRUNCATE biblioteca__emprestimo;
-- TRUNCATE biblioteca__suspensao;
-- DELETE FROM biblioteca__migracao WHERE migracao_tipo IN('titulo','tituloX','exemplar');
-- DROP TABLE IF EXISTS biblioteca__migracao;

CREATE TABLE IF NOT EXISTS `biblioteca__migracao` (
  `migracao_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `migracao_tipo` VARCHAR(100) NULL DEFAULT NULL,
  `migracao_de` VARCHAR(100) NULL DEFAULT NULL,
  `migracao_para` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`migracao_id`)
  )
ENGINE = InnoDB DEFAULT CHARACTER SET = utf8 COLLATE = utf8_general_ci;

DELIMITER ;;

DROP PROCEDURE IF EXISTS migraAreaCnpq;;
CREATE PROCEDURE migraAreaCnpq()
BEGIN
	DECLARE done INT DEFAULT FALSE;
	DECLARE _Codigo, _Descricao VARCHAR(255);
	DECLARE _IDCNPq, area_cnpq_id INT;
	DECLARE Consulta CURSOR FOR
		SELECT * FROM acervo.tbcnpq WHERE IDCNPq NOT IN
		(
			SELECT CAST(migracao_de AS UNSIGNED) FROM biblioteca__migracao WHERE migracao_tipo='area_cnpq'
		);
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN Consulta;

	read_loop: LOOP
		FETCH Consulta INTO _IDCNPq, _Codigo, _Descricao;

		IF done THEN
			LEAVE read_loop;
		END IF;

--		IF _Descricao <> "" THEN
			INSERT INTO biblioteca__area_cnpq (`area_cnpq_id`, `area_cnpq_codigo`, `area_cnpq_descricao`)
			VALUES (null, _Codigo, _Descricao);
			SELECT LAST_INSERT_ID() into area_cnpq_id from dual;
		    INSERT INTO biblioteca__migracao (`migracao_tipo`, `migracao_de`, `migracao_para`)
		    VALUES ('area_cnpq', _IDCNPq, area_cnpq_id);
--		END IF;
	END LOOP read_loop;

	CLOSE Consulta;
END;;

DROP PROCEDURE IF EXISTS migraArea;;
CREATE PROCEDURE migraArea()
BEGIN
	DECLARE done INT DEFAULT FALSE;
	DECLARE _IDCodigoArea, _DescArea VARCHAR(255);
	DECLARE _IDArea, area_id INT;
	DECLARE Consulta CURSOR FOR
		SELECT * FROM acervo.tbarea WHERE IDArea NOT IN(
			SELECT CAST(migracao_de AS UNSIGNED) FROM biblioteca__migracao WHERE migracao_tipo='area'
		);
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN Consulta;

	read_loop: LOOP
		FETCH Consulta INTO _IDArea, _IDCodigoArea, _DescArea;

		IF done THEN
			LEAVE read_loop;
		END IF;

--		IF _DescArea <> "" THEN
			INSERT INTO biblioteca__area (`area_id`, `area_descricao`)
			VALUES (null, _DescArea);
			SELECT LAST_INSERT_ID() into area_id from dual;
		    INSERT INTO biblioteca__migracao (`migracao_tipo`, `migracao_de`, `migracao_para`)
		    VALUES ('area', _IDArea, area_id);
--		END IF;
	END LOOP read_loop;

	CLOSE Consulta;
END;;

DROP PROCEDURE IF EXISTS migraAssunto;;
CREATE PROCEDURE migraAssunto()
BEGIN
	DECLARE done INT DEFAULT FALSE;
	DECLARE _DescAssunto VARCHAR(255);
	DECLARE _IDAssunto, assunto_id INT;
	DECLARE Consulta CURSOR FOR
		SELECT t.* FROM acervo.tbassunto t WHERE t.IDAssunto NOT IN(
			SELECT CAST(migracao_de AS UNSIGNED) FROM biblioteca__migracao WHERE migracao_tipo='assunto'
		);
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN Consulta;

	read_loop: LOOP
		FETCH Consulta INTO _IDAssunto, _DescAssunto;

		IF done THEN
			LEAVE read_loop;
		END IF;

--		IF _DescAssunto <> "" THEN
			INSERT INTO biblioteca__assunto (`assunto_id`, `assunto_descricao`)
			VALUES (null, _DescAssunto);
			SELECT LAST_INSERT_ID() into assunto_id from dual;
		    INSERT INTO biblioteca__migracao (`migracao_tipo`, `migracao_de`, `migracao_para`)
		    VALUES ('assunto', _IDAssunto, assunto_id);
--		END IF;
	END LOOP read_loop;

	CLOSE Consulta;
END;;

DROP PROCEDURE IF EXISTS migraAutor;;
CREATE PROCEDURE migraAutor()
BEGIN
	DECLARE done INT DEFAULT FALSE;
	DECLARE _DescAutor VARCHAR(255);
	DECLARE _IDAutor, autor_id INT;
	DECLARE Consulta CURSOR FOR
		SELECT * FROM acervo.tbautor WHERE IDAutor NOT IN(
			SELECT CAST(migracao_de AS UNSIGNED) FROM biblioteca__migracao WHERE migracao_tipo='autor'
		);
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN Consulta;

	read_loop: LOOP
		FETCH Consulta INTO _IDAutor, _DescAutor;

		IF done THEN
			LEAVE read_loop;
		END IF;

--		IF _DescAutor <> "" THEN
			INSERT INTO biblioteca__autor (`autor_id`, `autor_referencia`)
			VALUES (null, _DescAutor);
			SELECT LAST_INSERT_ID() into autor_id from dual;
		    INSERT INTO biblioteca__migracao (`migracao_tipo`, `migracao_de`, `migracao_para`)
		    VALUES ('autor', _IDAutor, autor_id);
--		END IF;
	END LOOP read_loop;

	CLOSE Consulta;
END;;

DROP PROCEDURE IF EXISTS migraGrupoBibliografico;;
CREATE PROCEDURE migraGrupoBibliografico()
BEGIN
	DECLARE done INT DEFAULT FALSE;
	DECLARE _Descricao VARCHAR(255);
	DECLARE _IdGrupoBibliografico, grupo_bibliografico_id INT;
	DECLARE Consulta CURSOR FOR
		SELECT * FROM acervo.tbgrupobibliografico WHERE IdGrupoBibliografico NOT IN(
			SELECT CAST(migracao_de AS UNSIGNED) FROM biblioteca__migracao WHERE migracao_tipo='grupo_bibliografico'
		);
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN Consulta;

	read_loop: LOOP
		FETCH Consulta INTO _IdGrupoBibliografico, _Descricao;

		IF done THEN
			LEAVE read_loop;
		END IF;

--		IF _Descricao <> "" THEN
			INSERT INTO biblioteca__grupo_bibliografico (`grupo_bibliografico_id`, `grupo_bibliografico_nome`)
			VALUES (null, _Descricao);
			SELECT LAST_INSERT_ID() into grupo_bibliografico_id from dual;
		    INSERT INTO biblioteca__migracao (`migracao_tipo`, `migracao_de`, `migracao_para`)
		    VALUES ('grupo_bibliografico', _IdGrupoBibliografico, grupo_bibliografico_id);
--		END IF;
	END LOOP read_loop;

	CLOSE Consulta;
END;;

DROP PROCEDURE IF EXISTS migraColecao;;
CREATE PROCEDURE migraColecao()
BEGIN
	DECLARE done INT DEFAULT FALSE;
	DECLARE _DescSerie VARCHAR(255);
	DECLARE _IDSerie, colecao_id INT;
	DECLARE Consulta CURSOR FOR
		SELECT * FROM acervo.tbserie WHERE IDSerie NOT IN(
			SELECT CAST(migracao_de AS UNSIGNED) FROM biblioteca__migracao WHERE migracao_tipo='colecao'
		);
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN Consulta;

	read_loop: LOOP
		FETCH Consulta INTO _IDSerie, _DescSerie;

		IF done THEN
			LEAVE read_loop;
		END IF;

--		IF _DescSerie <> "" THEN
			INSERT INTO biblioteca__colecao (`colecao_id`, `colecao_descricao`)
			VALUES (null, _DescSerie);
			SELECT LAST_INSERT_ID() into colecao_id from dual;
		    INSERT INTO biblioteca__migracao (`migracao_tipo`, `migracao_de`, `migracao_para`)
		    VALUES ('colecao', _IDSerie, colecao_id);
--		END IF;
	END LOOP read_loop;

	CLOSE Consulta;
END;;

DROP PROCEDURE IF EXISTS migraGrupoLeitor;;
CREATE PROCEDURE migraGrupoLeitor()
BEGIN
	DECLARE done INT DEFAULT FALSE;
	DECLARE _DescTipoLeitor VARCHAR(255);
	DECLARE _IDTipoLeitor, grupo_leitor_id INT;
	DECLARE Consulta CURSOR FOR
		SELECT * FROM acervo.tbtipoleitor WHERE IDTipoLeitor NOT IN(
			SELECT CAST(migracao_de AS UNSIGNED) FROM biblioteca__migracao WHERE migracao_tipo='grupo_leitor'
		);
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN Consulta;

	read_loop: LOOP
		FETCH Consulta INTO _IDTipoLeitor, _DescTipoLeitor;

		IF done THEN
			LEAVE read_loop;
		END IF;

--		IF _DescTipoLeitor <> "" THEN
			INSERT INTO biblioteca__grupo_leitor (`grupo_leitor_id`, `grupo_leitor_nome`)
			VALUES (null, _DescTipoLeitor);
			SELECT LAST_INSERT_ID() into grupo_leitor_id from dual;
		    INSERT INTO biblioteca__migracao (`migracao_tipo`, `migracao_de`, `migracao_para`)
		    VALUES ('grupo_leitor', _IDTipoLeitor, grupo_leitor_id);
--		END IF;
	END LOOP read_loop;

	CLOSE Consulta;
END;;

DROP PROCEDURE IF EXISTS migraSetor;;
CREATE PROCEDURE migraSetor()
BEGIN
	DECLARE done INT DEFAULT FALSE;
	DECLARE _IDSetor, _SiglaSetor, _DescSetor VARCHAR(255);
	DECLARE setor_id INT;
	DECLARE Consulta CURSOR FOR
		SELECT * FROM acervo.tbsetor WHERE IDSetor NOT IN(
			SELECT CAST(migracao_de AS UNSIGNED) FROM biblioteca__migracao WHERE migracao_tipo='setor'
		);
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN Consulta;

	read_loop: LOOP
		FETCH Consulta INTO _IDSetor, _SiglaSetor, _DescSetor;

		IF done THEN
			LEAVE read_loop;
		END IF;

--		IF _DescSetor <> "" THEN
			INSERT INTO biblioteca__setor (`setor_id`, `setor_sigla`, `setor_descricao`)
			VALUES (null, _SiglaSetor, _DescSetor);
			SELECT LAST_INSERT_ID() into setor_id from dual;
		    INSERT INTO biblioteca__migracao (`migracao_tipo`, `migracao_de`, `migracao_para`)
		    VALUES ('setor', _IDSetor, setor_id);
--		END IF;
	END LOOP read_loop;

	CLOSE Consulta;
END;;

DROP PROCEDURE IF EXISTS migraModalidades;;
CREATE PROCEDURE migraModalidades()
BEGIN
	DECLARE done INT DEFAULT FALSE;
	DECLARE 
			_IdPermissaoLeitor, _IdTipoLeitor, _DiasSuspensao, _ValorMulta,
			_LReserva, _EspReserva,
			_IncluiSabado, _IncluiDomingo, _IncluiFeriado,
			_LVolume, _LPrazo,
			_PVolume, _PPrazo,
			_MVolume, _MPrazo,
			_TVolume, _TPrazo,
			_migracao_para, _grupo_leitor_nome, _dias_nao_uteis, _grupo_bibliografico_id VARCHAR(255);
	DECLARE modalidade_emprestimo_id INT;
	DECLARE Consulta CURSOR FOR
		SELECT tl.*,CAST(gl.migracao_para AS UNSIGNED),bgl.grupo_leitor_nome
		FROM acervo.tbpermissaotipoleitor tl
		INNER JOIN biblioteca__migracao gl on gl.migracao_tipo='grupo_leitor' and CAST(gl.migracao_de AS UNSIGNED)=tl.IdTipoLeitor
		INNER JOIN biblioteca__grupo_leitor bgl on bgl.grupo_leitor_id=CAST(gl.migracao_para AS UNSIGNED)
		WHERE IdPermissaoLeitor NOT IN(
			SELECT CAST(migracao_de AS UNSIGNED) FROM biblioteca__migracao WHERE migracao_tipo='modalidade_emprestimo'
		);
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN Consulta;

	read_loop: LOOP
		FETCH Consulta INTO
			_IdPermissaoLeitor, _IdTipoLeitor, _DiasSuspensao, _ValorMulta,
			_LReserva, _EspReserva,
			_IncluiSabado, _IncluiDomingo, _IncluiFeriado,
			_LVolume, _LPrazo,
			_PVolume, _PPrazo,
			_MVolume, _MPrazo,
			_TVolume, _TPrazo,
			_migracao_para, _grupo_leitor_nome;

		IF done THEN
			LEAVE read_loop;
		END IF;

		INSERT INTO biblioteca__modalidade_emprestimo
		(modalidade_emprestimo_id, modalidade_emprestimo_descricao, grupo_leitor_id, modalidade_emprestimo_dias_suspensao)
		VALUES
		(null, concat(_grupo_leitor_nome, ' - Modalidade Padrão'), _migracao_para, _DiasSuspensao);
		SELECT LAST_INSERT_ID() into modalidade_emprestimo_id from dual;
	    INSERT INTO biblioteca__migracao (`migracao_tipo`, `migracao_de`, `migracao_para`)
	    VALUES ('modalidade_emprestimo', _IdPermissaoLeitor, modalidade_emprestimo_id);

	    SET _dias_nao_uteis=TRIM(
	    	TRAILING ',' FROM CONCAT(
		    	IF(_IncluiSabado=1,'Sabado,',''),
		    	IF(_IncluiDomingo=1,'Domingo,',''),
		    	IF(_IncluiFeriado=1,'Feriado,','')
	    	)
    	);

	    -- Criar regra para Livro
	    SELECT b.grupo_bibliografico_id INTO _grupo_bibliografico_id FROM biblioteca__grupo_bibliografico b WHERE trim(grupo_bibliografico_nome) LIKE 'Livro';
	    INSERT INTO biblioteca__modalidade_emprestimo_grupo
		(
			`modalidade_emprestimo_grupo_id`, `modalidade_emprestimo_id`, `grupo_bibliografico_id`,
			`modalidade_emprestimo_numero_volumes`, `modalidade_emprestimo_prazo_maximo`, `modalidade_emprestimo_valor_multa`, `modalidade_emprestimo_dias_nao_uteis`,
			`modalidade_emprestimo_tempo_reserva`
		) VALUES (
			null, modalidade_emprestimo_id, _grupo_bibliografico_id,
			_LVolume, _LPrazo, _ValorMulta, _dias_nao_uteis, _EspReserva
		);

	    -- Criar regra para Periodico
	    SELECT b.grupo_bibliografico_id INTO _grupo_bibliografico_id FROM biblioteca__grupo_bibliografico b WHERE trim(grupo_bibliografico_nome) LIKE 'Periodico';
	    INSERT INTO biblioteca__modalidade_emprestimo_grupo
		(
			`modalidade_emprestimo_grupo_id`, `modalidade_emprestimo_id`, `grupo_bibliografico_id`,
			`modalidade_emprestimo_numero_volumes`, `modalidade_emprestimo_prazo_maximo`, `modalidade_emprestimo_valor_multa`, `modalidade_emprestimo_dias_nao_uteis`,
			`modalidade_emprestimo_tempo_reserva`
		) VALUES (
			null, modalidade_emprestimo_id, _grupo_bibliografico_id,
			_PVolume, _PPrazo, _ValorMulta, _dias_nao_uteis, _EspReserva
		);

	    -- Criar regra para Multimeio
	    SELECT b.grupo_bibliografico_id INTO _grupo_bibliografico_id FROM biblioteca__grupo_bibliografico b WHERE trim(grupo_bibliografico_nome) LIKE 'Multimeio';
	    INSERT INTO biblioteca__modalidade_emprestimo_grupo
		(
			`modalidade_emprestimo_grupo_id`, `modalidade_emprestimo_id`, `grupo_bibliografico_id`,
			`modalidade_emprestimo_numero_volumes`, `modalidade_emprestimo_prazo_maximo`, `modalidade_emprestimo_valor_multa`, `modalidade_emprestimo_dias_nao_uteis`,
			`modalidade_emprestimo_tempo_reserva`
		) VALUES (
			null, modalidade_emprestimo_id, _grupo_bibliografico_id,
			_MVolume, _MPrazo, _ValorMulta, _dias_nao_uteis, _EspReserva
		);

	    -- Criar regra para Tese e Dissertação
	    SELECT b.grupo_bibliografico_id INTO _grupo_bibliografico_id FROM biblioteca__grupo_bibliografico b WHERE trim(grupo_bibliografico_nome) LIKE 'Tese e Dissertação';
	    INSERT INTO biblioteca__modalidade_emprestimo_grupo
		(
			`modalidade_emprestimo_grupo_id`, `modalidade_emprestimo_id`, `grupo_bibliografico_id`,
			`modalidade_emprestimo_numero_volumes`, `modalidade_emprestimo_prazo_maximo`, `modalidade_emprestimo_valor_multa`, `modalidade_emprestimo_dias_nao_uteis`,
			`modalidade_emprestimo_tempo_reserva`
		) VALUES (
			null, modalidade_emprestimo_id, _grupo_bibliografico_id,
			_TVolume, _TPrazo, _ValorMulta, _dias_nao_uteis, _EspReserva
		);

	END LOOP read_loop;

	CLOSE Consulta;
END;;

DROP PROCEDURE IF EXISTS migraEditora;;
CREATE PROCEDURE migraEditora()
BEGIN
	DECLARE done INT DEFAULT FALSE;
	DECLARE
		_Razaosocial, _CGCCPF, _Inscricao,
		_Endereco, _Bairro, _Cidade, _UF, _CEP,
		_Telefone, _Fax,
		_Contato, _DescEditora, _Email, _Obs,
		_IDEditora, _pes_id, _pes_id2, _empresa_tipo2
	VARCHAR(255);
	-- DECLARE IDEditora, pes_id INT;
	DECLARE Consulta CURSOR FOR
		SELECT t.* FROM acervo.tbeditora t WHERE t.IDEditora NOT IN
		(
			SELECT CAST(migracao_de AS UNSIGNED) FROM biblioteca__migracao WHERE migracao_tipo='editora'
		);
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN Consulta;

	read_loop: LOOP
		FETCH Consulta INTO
			_Razaosocial, _CGCCPF, _Inscricao,
			_Endereco, _Bairro, _Cidade, _UF, _CEP,
			_Telefone, _Fax,
			_Contato, _DescEditora, _Email, _Obs, _IDEditora;

		IF done THEN
			LEAVE read_loop;
		END IF;

		SET _DescEditora=TRIM(SUBSTRING(COALESCE(TRIM(_DescEditora), ''), 1, 45));
		SET _Razaosocial=TRIM(SUBSTRING(COALESCE(TRIM(_Razaosocial), ''), 1, 45));

		IF _DescEditora = "" THEN
			SET _DescEditora=_Razaosocial;
		END IF;

		IF _Razaosocial = "" THEN
			SET _Razaosocial=_DescEditora;
		END IF;

		IF _DescEditora <> "" THEN
			SET _pes_id=(SELECT p.pes_id FROM pessoa p WHERE p.pes_nome LIKE _Razaosocial AND pes_tipo='Juridica');

			IF _pes_id IS NULL THEN
			 	SET _pes_id=(SELECT p.pes_id FROM pessoa_juridica p WHERE p.pes_nome_fantasia LIKE _DescEditora);
			END IF;

			IF _pes_id IS NULL THEN
				INSERT INTO `pessoa` (`pes_id`, `pes_nome`, `pes_tipo`, `pes_nacionalidade`)
				VALUES (null, _Razaosocial, 'Juridica', 'Brasileiro');
				SELECT LAST_INSERT_ID() INTO _pes_id FROM DUAL;
				INSERT INTO `pessoa_juridica`
				(
					`pes_id`, `pes_nome_fantasia`, `pes_cnpj`, `pes_insc_municipal`, `pes_insc_estadual`,
					`pes_data_inicio`, `pes_data_fim`, `pes_sigla`, `pes_natureza_juridica`
				)
				VALUES ( _pes_id, _DescEditora, COALESCE(_CGCCPF,''), _Inscricao, null, now(), null, null, null);

				IF _Email like "%@%" THEN
					INSERT INTO `contato` (`con_id`, `pessoa`, `tipo_contato`, `con_contato`)
					VALUES (null, _pes_id, 1, _Email);
				END IF;

				set _Telefone=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(_Telefone,'(',''),')',''),'-',''),' ',''),'x',''),'X',''),'.','');

				IF LENGTH(_Telefone)>6 THEN
					INSERT INTO `contato` (`con_id`, `pessoa`, `tipo_contato`, `con_contato`)
					VALUES (null, _pes_id, 2, _Telefone);
				END IF;
				set _Fax=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(_Fax,'(',''),')',''),'-',''),' ',''),'x',''),'X',''),'.','');

				IF LENGTH(_Fax)>6 THEN
					INSERT INTO `contato` (`con_id`, `pessoa`, `tipo_contato`, `con_contato`)
					VALUES (null, _pes_id, 2, _Fax);
				END IF;

				IF LENGTH(_Cidade)>0 AND LENGTH(_UF)>0 THEN
					INSERT INTO `endereco`
					(
						`end_id`, `pes_id`, `tipo_endereco`, `end_pais`, `end_estado`, `end_tipo_end`,
						`end_bairro`, `end_cidade`, `end_cep`, `end_logradouro`, `end_tipo_logradouro`,
						`end_numero`, `end_complemento`, `end_data`
					) VALUES (
						null, _pes_id, 1, null, _UF, null, _Bairro, _Cidade, _CEP, _Endereco, null, null, null, now()
					);
				END IF;
			END IF;

			SET _empresa_tipo2=(SELECT p.empresa_tipo FROM biblioteca__empresa p WHERE p.pes_id=_pes_id);
			SET _pes_id2=(SELECT p.pes_id FROM biblioteca__empresa p WHERE p.pes_id=_pes_id);

			IF _pes_id2 IS NULL THEN
				INSERT INTO `biblioteca__empresa` (`pes_id`, `empresa_observacao`, `empresa_tipo`, `empresa_website`)
				VALUES (_pes_id, _Obs, 'Editora', IF(_Email like "%@%", null, _Email));
			ELSE
				IF _empresa_tipo2 NOT LIKE '%Editora%' THEN
					UPDATE `biblioteca__empresa` SET empresa_tipo=TRIM(TRAILING ',' FROM CONCAT(empresa_tipo,',Empresa')) WHERE pes_id LIKE _pes_id;
				END IF;
			END IF;

		    INSERT INTO biblioteca__migracao (`migracao_tipo`, `migracao_de`, `migracao_para`)
		    VALUES ('editora', _IDEditora, _pes_id);
		END IF;
	END LOOP read_loop;

	CLOSE Consulta;
END;;

DROP PROCEDURE IF EXISTS migraEditoraMista;;
CREATE PROCEDURE migraEditoraMista()
BEGIN
	DECLARE done INT DEFAULT FALSE;
	DECLARE
		_Razaosocial, _CGCCPF, _Inscricao,
		_Endereco, _Bairro, _Cidade, _UF, _CEP,
		_Telefone, _Fax,
		_Contato, _DescEditora, _Email, _Obs,
		_IDEditora, _pes_id
	VARCHAR(255);
	-- DECLARE IDEditora, pes_id INT;
	DECLARE Consulta CURSOR FOR
		SELECT t.* FROM (
			SELECT
				COALESCE(IF(TRIM(f.RazaoSocial)="" OR f.RazaoSocial IS NULL, NULL,f.RazaoSocial), IF(TRIM(e.Razaosocial)="" OR e.Razaosocial IS NULL, NULL,e.Razaosocial)) AS RazaoSocial,
				COALESCE(IF(TRIM(f.CGCCPF)="" OR f.CGCCPF IS NULL, NULL,f.CGCCPF), IF(TRIM(e.CGCCPF)="" OR e.CGCCPF IS NULL, NULL,e.CGCCPF)) AS CGCCPF,
				COALESCE(IF(TRIM(f.Inscricao)="" OR f.Inscricao IS NULL, NULL,f.Inscricao), IF(TRIM(e.Inscricao)="" OR e.Inscricao IS NULL, NULL,e.Inscricao)) AS Inscricao,
				COALESCE(IF(TRIM(f.Endereco)="" OR f.Endereco IS NULL, NULL,f.Endereco), IF(TRIM(e.Endereco)="" OR e.Endereco IS NULL, NULL,e.Endereco)) AS Endereco,
				COALESCE(IF(TRIM(f.Bairro)="" OR f.Bairro IS NULL, NULL,f.Bairro), IF(TRIM(e.Bairro)="" OR e.Bairro IS NULL, NULL,e.Bairro)) AS Bairro,
				COALESCE(IF(TRIM(f.Cidade)="" OR f.Cidade IS NULL, NULL,f.Cidade), IF(TRIM(e.Cidade)="" OR e.Cidade IS NULL, NULL,e.Cidade)) AS Cidade,
				COALESCE(IF(TRIM(f.UF)="" OR f.UF IS NULL, NULL,f.UF), IF(TRIM(e.UF)="" OR e.UF IS NULL, NULL,e.UF)) AS UF,
				COALESCE(IF(TRIM(f.CEP)="" OR f.CEP IS NULL, NULL,f.CEP), IF(TRIM(e.CEP)="" OR e.CEP IS NULL, NULL,e.CEP)) AS CEP,
				COALESCE(IF(TRIM(f.Telefone)="" OR f.Telefone IS NULL, NULL,f.Telefone), IF(TRIM(e.Telefone)="" OR e.Telefone IS NULL, NULL,e.Telefone)) AS Telefone,
				COALESCE(IF(TRIM(f.Fax)="" OR f.Fax IS NULL, NULL,f.Fax), IF(TRIM(e.Fax)="" OR e.Fax IS NULL, NULL,e.Fax)) AS Fax,
				COALESCE(IF(TRIM(f.Contato)="" OR f.Contato IS NULL, NULL,f.Contato), IF(TRIM(e.Contato)="" OR e.Contato IS NULL, NULL,e.Contato)) AS Contato,
				COALESCE(IF(TRIM(f.DescFornecedor)="" OR f.DescFornecedor IS NULL, NULL,f.DescFornecedor), IF(TRIM(e.DescEditora)="" OR e.DescEditora IS NULL, NULL,e.DescEditora)) AS DescEditora,
				COALESCE(IF(TRIM(f.Email)="" OR f.Email IS NULL, NULL,f.Email), IF(TRIM(e.Email)="" OR e.Email IS NULL, NULL,e.Email)) AS Email,
				COALESCE(IF(TRIM(f.Obs)="" OR f.Obs IS NULL, NULL,f.Obs), IF(TRIM(e.Obs)="" OR e.Obs IS NULL, NULL,e.Obs)) AS Obs,
				CONCAT(COALESCE(f.IDFornecedor,''),"-", COALESCE(e.IDEditora,'')) AS IDEditora
				FROM acervo.tbeditora e
				LEFT JOIN acervo.tbfornecedor f ON TRIM(e.RazaoSocial)= TRIM(f.Razaosocial) AND TRIM(e.DescEditora)= TRIM(f.DescFornecedor)
			) t WHERE t.IDEditora NOT IN
		(
			SELECT migracao_de FROM biblioteca__migracao WHERE migracao_tipo='editora_mista'
		);
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN Consulta;

	read_loop: LOOP
		FETCH Consulta INTO
			_Razaosocial, _CGCCPF, _Inscricao,
			_Endereco, _Bairro, _Cidade, _UF, _CEP,
			_Telefone, _Fax,
			_Contato, _DescEditora, _Email, _Obs, _IDEditora;

		IF done THEN
			LEAVE read_loop;
		END IF;

		SET _DescEditora=TRIM(SUBSTRING(COALESCE(TRIM(_DescEditora), ''), 1, 45));
		SET _Razaosocial=TRIM(SUBSTRING(COALESCE(TRIM(_Razaosocial), ''), 1, 45));

		IF _DescEditora = "" THEN
			SET _DescEditora=_Razaosocial;
		END IF;

		IF _Razaosocial = "" THEN
			SET _Razaosocial=_DescEditora;
		END IF;

		IF _DescEditora <> "" THEN
			SET _pes_id=(SELECT p.pes_id FROM pessoa p WHERE p.pes_nome LIKE _Razaosocial AND pes_tipo='Juridica');

			IF _pes_id IS NULL THEN
			 	SET _pes_id=(SELECT p.pes_id FROM pessoa_juridica p WHERE p.pes_nome_fantasia LIKE _DescEditora);
			END IF;

			IF _pes_id IS NULL THEN
				INSERT INTO `pessoa` (`pes_id`, `pes_nome`, `pes_tipo`, `pes_nacionalidade`)
				VALUES (null, _Razaosocial, 'Juridica', 'Brasileiro');
				SELECT LAST_INSERT_ID() INTO _pes_id FROM DUAL;
				INSERT INTO `pessoa_juridica`
				(
					`pes_id`, `pes_nome_fantasia`, `pes_cnpj`, `pes_insc_municipal`, `pes_insc_estadual`,
					`pes_data_inicio`, `pes_data_fim`, `pes_sigla`, `pes_natureza_juridica`
				)
				VALUES ( _pes_id, _DescEditora, COALESCE(_CGCCPF,''), _Inscricao, null, now(), null, null, null);

				IF _Email like "%@%" THEN
					INSERT INTO `contato` (`con_id`, `pessoa`, `tipo_contato`, `con_contato`)
					VALUES (null, _pes_id, 1, _Email);
				END IF;

				set _Telefone=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(_Telefone,'(',''),')',''),'-',''),' ',''),'x',''),'X',''),'.','');

				IF LENGTH(_Telefone)>6 THEN
					INSERT INTO `contato` (`con_id`, `pessoa`, `tipo_contato`, `con_contato`)
					VALUES (null, _pes_id, 2, _Telefone);
				END IF;
				set _Fax=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(_Fax,'(',''),')',''),'-',''),' ',''),'x',''),'X',''),'.','');

				IF LENGTH(_Fax)>6 THEN
					INSERT INTO `contato` (`con_id`, `pessoa`, `tipo_contato`, `con_contato`)
					VALUES (null, _pes_id, 2, _Fax);
				END IF;

				IF LENGTH(_Cidade)>0 AND LENGTH(_UF)>0 THEN
					INSERT INTO `endereco`
					(
						`end_id`, `pes_id`, `tipo_endereco`, `end_pais`, `end_estado`, `end_tipo_end`,
						`end_bairro`, `end_cidade`, `end_cep`, `end_logradouro`, `end_tipo_logradouro`,
						`end_numero`, `end_complemento`, `end_data`
					) VALUES (
						null, _pes_id, 1, null, _UF, null, _Bairro, _Cidade, _CEP, _Endereco, null, null, null, now()
					);
				END IF;
			END IF;

			REPLACE INTO `biblioteca__empresa` (`pes_id`, `empresa_observacao`, `empresa_tipo`, `empresa_website`)
			VALUES (_pes_id, _Obs, 'Editora,Fornecedor', IF(_Email like "%@%", null, _Email));

		    INSERT INTO biblioteca__migracao (`migracao_tipo`, `migracao_de`, `migracao_para`)
		    VALUES ('editora_mista', _IDEditora, _pes_id);
		END IF;
	END LOOP read_loop;

	CLOSE Consulta;
END;;

DROP PROCEDURE IF EXISTS migraFornecedor;;
CREATE PROCEDURE migraFornecedor()
BEGIN
	DECLARE done INT DEFAULT FALSE;
	DECLARE
		Razaosocial, CGCCPF, Inscricao,
		Endereco, Bairro, Cidade, UF, CEP,
		Telefone, Fax,
		Contato, DescFornecedor, Email, Obs,
		IDFornecedor, pes_id, pes_id2, empresa_tipo2
	VARCHAR(255);
	-- DECLARE IDFornecedor, pes_id INT;
	DECLARE Consulta CURSOR FOR
		SELECT t.* FROM acervo.tbfornecedor t WHERE t.IDFornecedor NOT IN
		(
			SELECT CAST(migracao_de AS UNSIGNED) FROM biblioteca__migracao WHERE migracao_tipo='fornecedor'
		);
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN Consulta;

	read_loop: LOOP
		FETCH Consulta INTO
			Razaosocial, CGCCPF, Inscricao,
			Endereco, Bairro, Cidade, UF, CEP,
			Telefone, Fax,
			Contato, DescFornecedor, Email, Obs, IDFornecedor;

		IF done THEN
			LEAVE read_loop;
		END IF;

		SET DescFornecedor=TRIM(SUBSTRING(COALESCE(TRIM(DescFornecedor), ''), 1, 45));
		SET Razaosocial=TRIM(SUBSTRING(COALESCE(TRIM(Razaosocial), ''), 1, 45));

		IF DescFornecedor = "" THEN
			SET DescFornecedor=Razaosocial;
		END IF;

		IF Razaosocial = "" THEN
			SET Razaosocial=DescFornecedor;
		END IF;

		IF DescFornecedor <> "" THEN
			SET pes_id=(SELECT p.pes_id FROM pessoa p WHERE p.pes_nome LIKE Razaosocial AND pes_tipo='Juridica');

			IF pes_id IS NULL THEN
			 	SET pes_id=(SELECT p.pes_id FROM pessoa_juridica p WHERE p.pes_nome_fantasia LIKE DescFornecedor);
			END IF;

			IF pes_id IS NULL THEN
				INSERT INTO `pessoa` (`pes_id`, `pes_nome`, `pes_tipo`, `pes_nacionalidade`)
				VALUES (null, Razaosocial, 'Juridica', 'Brasileiro');
				SELECT LAST_INSERT_ID() INTO pes_id FROM DUAL;
				INSERT INTO `pessoa_juridica`
				(
					`pes_id`, `pes_nome_fantasia`, `pes_cnpj`, `pes_insc_municipal`, `pes_insc_estadual`,
					`pes_data_inicio`, `pes_data_fim`, `pes_sigla`, `pes_natureza_juridica`
				)
				VALUES ( pes_id, DescFornecedor, COALESCE(CGCCPF,''), Inscricao, null, now(), null, null, null);

				IF Email like "%@%" THEN
					INSERT INTO `contato` (`con_id`, `pessoa`, `tipo_contato`, `con_contato`)
					VALUES (null, pes_id, 1, Email);
				END IF;

				set Telefone=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(Telefone,'(',''),')',''),'-',''),' ',''),'x',''),'X',''),'.','');

				IF LENGTH(Telefone)>6 THEN
					INSERT INTO `contato` (`con_id`, `pessoa`, `tipo_contato`, `con_contato`)
					VALUES (null, pes_id, 2, Telefone);
				END IF;
				set Fax=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(Fax,'(',''),')',''),'-',''),' ',''),'x',''),'X',''),'.','');

				IF LENGTH(Fax)>6 THEN
					INSERT INTO `contato` (`con_id`, `pessoa`, `tipo_contato`, `con_contato`)
					VALUES (null, pes_id, 2, Fax);
				END IF;

				IF LENGTH(Cidade)>0 AND LENGTH(UF)>0 THEN
					INSERT INTO `endereco`
					(
						`end_id`, `pes_id`, `tipo_endereco`, `end_pais`, `end_estado`, `end_tipo_end`,
						`end_bairro`, `end_cidade`, `end_cep`, `end_logradouro`, `end_tipo_logradouro`,
						`end_numero`, `end_complemento`, `end_data`
					) VALUES (
						null, pes_id, 1, null, UF, null, Bairro, Cidade, CEP, Endereco, null, null, null, now()
					);
				END IF;
			END IF;

			SET pes_id2=pes_id;
			SET empresa_tipo2=(SELECT p.empresa_tipo FROM biblioteca__empresa p WHERE p.pes_id=pes_id2);
			SET pes_id2=(SELECT p.pes_id FROM biblioteca__empresa p WHERE p.pes_id=pes_id2);

			IF pes_id2 IS NULL THEN
				INSERT INTO `biblioteca__empresa` (`pes_id`, `empresa_observacao`, `empresa_tipo`, `empresa_website`)
				VALUES (pes_id, Obs, 'Fornecedor', IF(Email like "%@%", null, Email));
			ELSE
				IF empresa_tipo2 NOT LIKE '%Fornecedor%' THEN
					UPDATE `biblioteca__empresa` SET empresa_tipo=TRIM(TRAILING ',' FROM CONCAT(empresa_tipo,',Fornecedor')) WHERE pes_id LIKE pes_id;
				END IF;
			END IF;

		    INSERT INTO biblioteca__migracao (`migracao_tipo`, `migracao_de`, `migracao_para`)
		    VALUES ('fornecedor', IDFornecedor, pes_id);
		END IF;
	END LOOP read_loop;

	CLOSE Consulta;
END;;

DROP PROCEDURE IF EXISTS migraLeitor;;
CREATE PROCEDURE migraLeitor()
BEGIN
	DECLARE done INT DEFAULT FALSE;
	DECLARE Imagem BLOB;
	DECLARE
		TipoLeitor, Codigo, Matricula, Controle, Nome, Sexo,
		Nacionalid, Dtnasc, Naturali, UF, Ident, Orgao, UForg,
		Estciv, Ender, Bairro, Cidade, UFE, Telefone, Pai, Mae,
		DTCad, ARLZ, Class, Escor, Cidor, CEP, E2Grau, E2Cida,
		E2UF, E2AC, ARV, Pontos, UFOR, Titulo, Zona, Secao, UFTit,
		Certmil, Historico, CPF, DDD, SituacaoLeitor,
		DataSituacao, Obs, ddd1, Celular, Email, Senha,
		Funcao, pes_id, grupo_leitor_id, pes_id2
	VARCHAR(255);
	-- DECLARE IDFornecedor, pes_id INT;
	DECLARE Consulta CURSOR FOR
		SELECT t.* FROM acervo.tbleitor t WHERE t.Codigo NOT IN
		(
			SELECT CAST(migracao_de AS UNSIGNED) FROM biblioteca__migracao WHERE migracao_tipo='leitor'
		);
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN Consulta;

	read_loop: LOOP
		FETCH Consulta INTO
			TipoLeitor, Codigo, Matricula, Controle, Nome, Sexo,
			Nacionalid, Dtnasc, Naturali, UF, Ident, Orgao, UForg,
			Estciv, Ender, Bairro, Cidade, UFE, Telefone, Pai, Mae,
			DTCad, ARLZ, Class, Escor, Cidor, CEP, E2Grau, E2Cida,
			E2UF, E2AC, ARV, Pontos, UFOR, Titulo, Zona, Secao, UFTit,
			Certmil, Historico, Imagem, CPF, DDD, SituacaoLeitor,
			DataSituacao, Obs, ddd1, Celular, Email, Senha,
			Funcao;

		IF done THEN
			LEAVE read_loop;
		END IF;

		SET Nome=TRIM(SUBSTRING(COALESCE(TRIM(Nome), ''), 1, 45));


		IF Nome <> "" and Nome IS NOT NULL THEN
			SET pes_id=(SELECT p.pes_id FROM pessoa p WHERE p.pes_nome LIKE Nome AND pes_tipo='Fisica' limit 1);
		END IF;

		IF pes_id IS NULL AND trim(CPF) <> "" THEN
		 	SET pes_id=(SELECT p.pes_id FROM pessoa_fisica p WHERE p.pes_cpf LIKE CPF limit 1);
		END IF;

		IF pes_id IS NULL THEN
			INSERT INTO `pessoa` (`pes_id`, `pes_nome`, `pes_tipo`, `pes_nacionalidade`)
			VALUES (null, Nome, 'Fisica', 'Brasileiro');
			SELECT LAST_INSERT_ID() INTO pes_id FROM DUAL;

			SET Sexo=UPPER(SUBSTRING(TRIM(COALESCE(Sexo,'M')), 1,1));
			SET Sexo=IF(Sexo NOT IN('M','F'), NULL, Sexo);
			SET Sexo=IF(Sexo = 'M','Masculino',IF(Sexo='F', 'Feminino', NULL));

			INSERT INTO `pessoa_fisica`
			(
				pes_id, pes_cpf, pes_cpf_emissao, pes_rg, pes_rg_emissao, pes_sobrenome, pes_sexo,
				pes_data_nascimento, pes_estado_civil, pes_filhos, pes_naturalidade, pes_doc_estrangeiro,
				pes_emissor_rg, pes_nasc_uf
			)
			VALUES (
				pes_id, COALESCE(CPF,''), null, COALESCE(Ident,''), null, null, Sexo,
				Dtnasc, if(Estciv='S','Solteiro',if(Estciv='C','Casado', null)), null, Naturali, null,
				Orgao, UF
			);

			IF Email like "%@%" THEN
				INSERT INTO `contato` (`con_id`, `pessoa`, `tipo_contato`, `con_contato`)
				VALUES (null, pes_id, 1, Email);
			END IF;

			SET Telefone=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(Telefone,'(',''),')',''),'-',''),' ',''),'x',''),'X',''),'.','');

			IF LENGTH(Telefone)>6 THEN
				INSERT INTO `contato` (`con_id`, `pessoa`, `tipo_contato`, `con_contato`)
				VALUES (null, pes_id, 2, CONCAT(COALESCE(DDD, ''),Telefone));
			END IF;

			SET Celular=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(Celular,'(',''),')',''),'-',''),' ',''),'x',''),'X',''),'.','');

			IF LENGTH(Celular)>6 THEN
				INSERT INTO `contato` (`con_id`, `pessoa`, `tipo_contato`, `con_contato`)
				VALUES (null, pes_id, 3, CONCAT(COALESCE(ddd1,DDD, ''), Celular));
			END IF;

			IF LENGTH(Cidade)>0 AND LENGTH(UFE)>0 THEN
				INSERT INTO `endereco`
				(
					`end_id`, `pes_id`, `tipo_endereco`, `end_pais`, `end_estado`, `end_tipo_end`,
					`end_bairro`, `end_cidade`, `end_cep`, `end_logradouro`, `end_tipo_logradouro`,
					`end_numero`, `end_complemento`, `end_data`
				) VALUES (
					null, pes_id, 1, null, UFE, null, Bairro, Cidade, CEP, Ender, null, null, null, now()
				);
			END IF;
		END IF;

		SET grupo_leitor_id=(SELECT migracao_para FROM biblioteca__migracao WHERE migracao_tipo='grupo_leitor' and migracao_de=TipoLeitor);
		SET Senha=TRIM(COALESCE(Senha, ''));
		SET Senha=IF(Senha="0", null, Senha);
		SET Funcao=TRIM(COALESCE(Funcao, ''));
		SET Obs=TRIM(COALESCE(Obs, ''));
		SET Obs=CONCAT(Obs, IF(Funcao <> '', CONCAT('\nFunção: ', Funcao), ''));

		SET pes_id2=pes_id;
		SET pes_id2=(SELECT p.pes_id FROM biblioteca__pessoa p WHERE p.pes_id=pes_id2);

		IF pes_id2 IS NULL THEN
			INSERT INTO `biblioteca__pessoa` (`pes_id`, `grupo_leitor_id`, `pessoa_senha`, `pessoa_observacao`, `pessoa_situacao`)
			VALUES (pes_id, grupo_leitor_id, if(Senha <> "", sha1(Senha), null), Obs, IF(SituacaoLeitor=1, 'Inativo', 'Ativo'));
		END IF;

	    INSERT INTO biblioteca__migracao (`migracao_tipo`, `migracao_de`, `migracao_para`)
	    VALUES ('leitor', Codigo, pes_id);
	END LOOP read_loop;

	CLOSE Consulta;
END;;

DROP PROCEDURE IF EXISTS migraIdioma;;
CREATE PROCEDURE migraIdioma()
BEGIN
	DECLARE done INT DEFAULT FALSE;
	DECLARE IDIdioma VARCHAR(255);
	DECLARE idioma_id INT;
	DECLARE Consulta CURSOR FOR
		SELECT DISTINCT idioma
		FROM(
			SELECT Idioma1 AS idioma
			FROM acervo.tbacervo UNION
			SELECT Idioma2 AS idioma
			FROM acervo.tbacervo UNION
			SELECT Idioma3 AS idioma
			FROM acervo.tbacervo
		) t
		WHERE idioma <> "" AND idioma NOT IN(
			SELECT migracao_de FROM biblioteca__migracao WHERE migracao_tipo='idioma'
		);
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN Consulta;

	read_loop: LOOP
		FETCH Consulta INTO IDIdioma;

		IF done THEN
			LEAVE read_loop;
		END IF;

		INSERT INTO biblioteca__idioma (`idioma_id`, `idioma_descricao`)
		VALUES (null, IDIdioma);
		SELECT LAST_INSERT_ID() into idioma_id from dual;
	    INSERT INTO biblioteca__migracao (`migracao_tipo`, `migracao_de`, `migracao_para`)
	    VALUES ('idioma', IDIdioma, idioma_id);
	END LOOP read_loop;

	CLOSE Consulta;
END;;

DROP FUNCTION IF EXISTS extrairDigitosDaString;;
CREATE FUNCTION extrairDigitosDaString( str CHAR(32) ) RETURNS CHAR(32)
BEGIN
  DECLARE i, len SMALLINT DEFAULT 1;
  DECLARE ret CHAR(32) DEFAULT '';
  DECLARE c CHAR(1);

  IF str IS NULL
  THEN
    RETURN "";
  END IF;

  SET len = CHAR_LENGTH( str );
  REPEAT
    BEGIN
      SET c = MID( str, i, 1 );
      IF c BETWEEN '0' AND '9' THEN
        SET ret=CONCAT(ret,c);
      END IF;
      SET i = i + 1;
    END;
  UNTIL i > len END REPEAT;
  RETURN ret;
END;;

DROP PROCEDURE IF EXISTS migraTitulo;;
CREATE PROCEDURE migraTitulo()
BEGIN
	DECLARE done INT DEFAULT FALSE;
	DECLARE
			chave, qtd, Registros, Idiomas, _Registro, _idGrupo, _GrupoBibliografico, _DataRegistro, _Titulo, _Subtitulo, _AnoPublicacao,
			_LocalEdicao, _TipoEntrada, _Volume, _Exemplar, _Edicao, _ISBN, _idCNPQ, _IDSubArea, _IDArea,
			_Emprestado, _Reservado, _DataCompraDoacao, _NotaFiscal, _DataCatalogacao, _IDDoador, _CDU,
			_IDFornecedor, _ValordaNota, _Tomo, _Paginas, _Idioma1, _Idioma2, _Idioma3, _IDSerie, _Databaixa,
			_Motivobaixa, _IDEditora, _Editor, _ISSN, _IdSetor, _TipoAcesso, _Cutter, _Localizacao, _Permuta,
			_Periodicidade, _DataInicioPeriodico, _DataFinalPeriodico, _EditorPeriodico, _Circulacao,
			_Status, _Nota, _RequisitosUso, _Bibliografia, _idGrade
		TEXT;
	DECLARE _titulo_id INT;
	DECLARE iteracao_loop, tamanho_loop INT;
	DECLARE
		__grupo_bibliografico_id, __area_cnpq_id, __area_id, __colecao_id, __pes_id_editora
		VARCHAR(255);
	DECLARE Consulta CURSOR FOR
		SELECT * FROM acervo.tbacervo t
		WHERE t.Registro
		-- NOT IN(
		--  	SELECT CAST(migracao_de AS UNSIGNED) FROM biblioteca__migracao WHERE migracao_tipo='tituloX'
		-- )
		ORDER BY t.Registro ASC;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN Consulta;

	set tamanho_loop=(select FOUND_ROWS() from dual);
	set iteracao_loop=0;
	-- select iteracao_loop, tamanho_loop;

	read_loop: LOOP
		FETCH Consulta INTO
			-- chave, qtd, Registros, Idiomas,
			_Registro, _idGrupo, _GrupoBibliografico, _DataRegistro, _Titulo, _Subtitulo, _AnoPublicacao,
			_LocalEdicao, _TipoEntrada, _Volume, _Exemplar, _Edicao, _ISBN, _idCNPQ, _IDSubArea, _IDArea,
			_Emprestado, _Reservado, _DataCompraDoacao, _NotaFiscal, _DataCatalogacao, _IDDoador, _CDU,
			_IDFornecedor, _ValordaNota, _Tomo, _Paginas, _Idioma1, _Idioma2, _Idioma3, _IDSerie, _Databaixa,
			_Motivobaixa, _IDEditora, _Editor, _ISSN, _IdSetor, _TipoAcesso, _Cutter, _Localizacao, _Permuta,
			_Periodicidade, _DataInicioPeriodico, _DataFinalPeriodico, _EditorPeriodico, _Circulacao,
			_Status, _Nota, _RequisitosUso, _Bibliografia, _idGrade;

		IF done THEN
			LEAVE read_loop;
		END IF;

		SET chave=MD5(
			CONCAT(
				_Titulo,
				'-', COALESCE(_AnoPublicacao,''),
				'-', COALESCE(_LocalEdicao,''),
				'-', COALESCE(_Volume,''),
				'-', COALESCE(_Tomo,''),
				'-', COALESCE(_Edicao,'')
			)
		);

		SET _titulo_id=(SELECT CAST(migracao_para AS UNSIGNED) FROM biblioteca__migracao WHERE migracao_tipo='titulo' and migracao_de=chave limit 1);

		IF _titulo_id IS NULL THEN
			SET __grupo_bibliografico_id=(SELECT CAST(migracao_para AS UNSIGNED) FROM biblioteca__migracao WHERE migracao_tipo='grupo_bibliografico' and CAST(migracao_de AS UNSIGNED)=_idGrupo);
			SET __area_cnpq_id=(SELECT CAST(migracao_para AS UNSIGNED) FROM biblioteca__migracao WHERE migracao_tipo='area_cnpq' and CAST(migracao_de AS UNSIGNED)=_idCNPQ);
			SET __area_id=(SELECT CAST(migracao_para AS UNSIGNED) FROM biblioteca__migracao WHERE migracao_tipo='area' and CAST(migracao_de AS UNSIGNED)=_IDArea);
			SET __pes_id_editora=(SELECT CAST(migracao_para AS UNSIGNED) FROM biblioteca__migracao WHERE migracao_tipo='editora' and CAST(migracao_de AS UNSIGNED)=_IDEditora);
			SET __colecao_id=(SELECT CAST(migracao_para AS UNSIGNED) FROM biblioteca__migracao WHERE migracao_tipo='colecao' and CAST(migracao_de AS UNSIGNED)=_IDSerie);

			SET _AnoPublicacao=extrairDigitosDaString(_AnoPublicacao);
			SET _AnoPublicacao=IF(_AnoPublicacao="", NULL, _AnoPublicacao);
			SET _Volume=extrairDigitosDaString(_Volume);
			SET _Volume=IF(_Volume="", NULL, _Volume);
			SET _Edicao=extrairDigitosDaString(_Edicao);
			SET _Edicao=IF(_Edicao="", NULL, _Edicao);
			SET _Tomo=extrairDigitosDaString(_Tomo);
			SET _Tomo=IF(_Tomo="", NULL, _Tomo);
			SET _Paginas=extrairDigitosDaString(_Paginas);
			SET _Paginas=IF(_Paginas="", NULL, _Paginas);

			INSERT INTO `biblioteca__titulo` (
				`titulo_id`, `grupo_bibliografico_id`, `area_cnpq_id`, `area_id`, `titulo_data_cadastro`,
				`titulo_data_alteracao`, `titulo_titulo`, `titulo_descricao`, `pes_id_editora`,
				`titulo_edicao_ano`, `titulo_edicao_local`, `titulo_edicao_volume`, `titulo_edicao_numero`,
				`titulo_edicao_isbn`, `titulo_edicao_cdu`, `titulo_edicao_cutter`, `titulo_edicao_tomo`,
				`titulo_paginas`, `colecao_id`, `titulo_nota`, `titulo_requisitos_uso`, `titulo_situacao`
			) VALUES (
				null, __grupo_bibliografico_id, __area_cnpq_id, __area_id, _DataRegistro,
				_DataCatalogacao, _Titulo, _Subtitulo, __pes_id_editora,
				_AnoPublicacao, _LocalEdicao, _Volume, _Edicao,
				SUBSTRING(COALESCE(_ISBN,_ISSN), 1, 18), SUBSTRING(_CDU, 1, 20), SUBSTRING(_Cutter, 1, 20), _Tomo,
				_Paginas, __colecao_id, _Nota, _RequisitosUso, 'Ativo'
			);
			SELECT LAST_INSERT_ID() into _titulo_id from dual;
		    INSERT INTO biblioteca__migracao (`migracao_tipo`, `migracao_de`, `migracao_para`)
		    VALUES ('titulo', chave, _titulo_id), ('tituloX', _Registro, _titulo_id);
	    END IF;

		SET _Exemplar=extrairDigitosDaString(COALESCE(TRIM(_Exemplar),''));
		SET _Exemplar=IF(_Exemplar="", 1, _Exemplar);

		call migraTituloIdiomas(_titulo_id, CONCAT(_Idioma1,',',_Idioma2,',',_Idioma3));
		call migraExemplares(
		 	_titulo_id, _Registro, _Exemplar, _Emprestado, _Reservado, _DataRegistro,
			_DataCatalogacao, _Databaixa, _Motivobaixa, _IdSetor, _TipoAcesso, _Localizacao
		);
	END LOOP read_loop;
	CLOSE Consulta;
END;;

DROP PROCEDURE IF EXISTS migraExemplaresLoop;;
CREATE PROCEDURE migraExemplaresLoop()
BEGIN
	DECLARE done INT DEFAULT FALSE;
	DECLARE
			chave, qtd, Registros, Idiomas,
			_Registro, _idGrupo, _GrupoBibliografico, _DataRegistro, _Titulo, _Subtitulo, _AnoPublicacao,
			_LocalEdicao, _TipoEntrada, _Volume, _Exemplar, _Edicao, _ISBN, _idCNPQ, _IDSubArea, _IDArea,
			_Emprestado, _Reservado, _DataCompraDoacao, _NotaFiscal, _DataCatalogacao, _IDDoador, _CDU,
			_IDFornecedor, _ValordaNota, _Tomo, _Paginas, _Idioma1, _Idioma2, _Idioma3, _IDSerie, _Databaixa,
			_Motivobaixa, _IDEditora, _Editor, _ISSN, _IdSetor, _TipoAcesso, _Cutter, _Localizacao, _Permuta,
			_Periodicidade, _DataInicioPeriodico, _DataFinalPeriodico, _EditorPeriodico, _Circulacao,
			_Status, _Nota, _RequisitosUso, _Bibliografia, _idGrade
		TEXT;
	DECLARE _titulo_id INT;
	DECLARE _exemplar_id, __setor_id VARCHAR(255);
	DECLARE iteracao_loop, tamanho_loop INT;
	DECLARE
		__grupo_bibliografico_id, __area_cnpq_id, __area_id, __colecao_id, __pes_id_editora
		VARCHAR(255);
	DECLARE Consulta CURSOR FOR
		SELECT CAST(gl.migracao_para AS UNSIGNED) as migracao_para,  t.* FROM acervo.tbacervo t
		INNER JOIN biblioteca__migracao gl on gl.migracao_tipo='tituloX' and CAST(gl.migracao_de AS UNSIGNED)=t.Registro
		ORDER BY t.Registro ASC;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN Consulta;

	set tamanho_loop=(select FOUND_ROWS() from dual);
	set iteracao_loop=0;
	-- select iteracao_loop, tamanho_loop;

	read_loop: LOOP
		FETCH Consulta INTO
			-- chave, qtd, Registros, Idiomas,
			_titulo_id, _Registro, _idGrupo, _GrupoBibliografico, _DataRegistro, _Titulo, _Subtitulo, _AnoPublicacao,
			_LocalEdicao, _TipoEntrada, _Volume, _Exemplar, _Edicao, _ISBN, _idCNPQ, _IDSubArea, _IDArea,
			_Emprestado, _Reservado, _DataCompraDoacao, _NotaFiscal, _DataCatalogacao, _IDDoador, _CDU,
			_IDFornecedor, _ValordaNota, _Tomo, _Paginas, _Idioma1, _Idioma2, _Idioma3, _IDSerie, _Databaixa,
			_Motivobaixa, _IDEditora, _Editor, _ISSN, _IdSetor, _TipoAcesso, _Cutter, _Localizacao, _Permuta,
			_Periodicidade, _DataInicioPeriodico, _DataFinalPeriodico, _EditorPeriodico, _Circulacao,
			_Status, _Nota, _RequisitosUso, _Bibliografia, _idGrade;

		IF done THEN
			LEAVE read_loop;
		END IF;

		SET __grupo_bibliografico_id=(SELECT CAST(migracao_para AS UNSIGNED) FROM biblioteca__migracao WHERE migracao_tipo='grupo_bibliografico' and CAST(migracao_de AS UNSIGNED)=_idGrupo);
		SET __area_cnpq_id=(SELECT CAST(migracao_para AS UNSIGNED) FROM biblioteca__migracao WHERE migracao_tipo='area_cnpq' and CAST(migracao_de AS UNSIGNED)=_idCNPQ);
		SET __area_id=(SELECT CAST(migracao_para AS UNSIGNED) FROM biblioteca__migracao WHERE migracao_tipo='area' and CAST(migracao_de AS UNSIGNED)=_IDArea);
		SET __pes_id_editora=(SELECT CAST(migracao_para AS UNSIGNED) FROM biblioteca__migracao WHERE migracao_tipo='editora' and CAST(migracao_de AS UNSIGNED)=_IDEditora);
		SET __colecao_id=(SELECT CAST(migracao_para AS UNSIGNED) FROM biblioteca__migracao WHERE migracao_tipo='colecao' and CAST(migracao_de AS UNSIGNED)=_IDSerie);

		SET _AnoPublicacao=extrairDigitosDaString(_AnoPublicacao);
		SET _AnoPublicacao=IF(_AnoPublicacao="", NULL, _AnoPublicacao);
		SET _Volume=extrairDigitosDaString(_Volume);
		SET _Volume=IF(_Volume="", NULL, _Volume);
		SET _Edicao=extrairDigitosDaString(_Edicao);
		SET _Edicao=IF(_Edicao="", NULL, _Edicao);
		SET _Tomo=IF(_Tomo="", NULL, _Tomo);
		SET _Paginas=extrairDigitosDaString(_Paginas);
		SET _Paginas=IF(_Paginas="", NULL, _Paginas);
		SET _Exemplar=extrairDigitosDaString(COALESCE(TRIM(_Exemplar),''));
		SET _Exemplar=IF(_Exemplar="", 1, _Exemplar);
		SET _exemplar_id=(SELECT CAST(migracao_para AS UNSIGNED) FROM biblioteca__migracao WHERE migracao_tipo='exemplar' and CAST(migracao_de AS UNSIGNED)=_Registro limit 1);

		IF _exemplar_id IS NULL THEN
			SET __setor_id=(SELECT CAST(migracao_para AS UNSIGNED) FROM biblioteca__migracao WHERE migracao_tipo='setor' and CAST(migracao_de AS UNSIGNED)=_setor_id*1);
			INSERT INTO `biblioteca__exemplar`
			(
				`exemplar_id`, `titulo_id`, `exemplar_codigo`, `exemplar_emprestado`,
				`exemplar_reservado`, `exemplar_data_cadastro`, `exemplar_data_alteracao`,
				`exemplar_baixa_data`, `exemplar_baixa_motivo`, `setor_id`, `exemplar_acesso`,
				`exemplar_localizacao`
			)
			VALUES
			(
				null, _titulo_id, _Exemplar, IF(_Emprestado = 1,'S','N'),
				IF(_Reservado = 1,'S','N'), _DataRegistro, _DataCatalogacao,
				IF(_Databaixa = '1900-01-01 00:00:00', NULL, _Databaixa), _Motivobaixa,
				__setor_id, IF(_TipoAcesso = 0,'Restrito','Livre'),
				IF(COALESCE(_Localizacao,"") = "", NULL, _Localizacao)
			);

			SELECT LAST_INSERT_ID() into _exemplar_id from dual;

		    INSERT INTO biblioteca__migracao (`migracao_tipo`, `migracao_de`, `migracao_para`)
		    VALUES ('exemplar', _Registro, _exemplar_id);
	    END IF;
	END LOOP read_loop;
	CLOSE Consulta;
END;;

DROP PROCEDURE IF EXISTS migraExemplares;;
CREATE PROCEDURE migraExemplares(
	IN _titulo_id VARCHAR(255),
	IN _Registro VARCHAR(255),
	IN _exemplar_codigo VARCHAR(255),
	IN _exemplar_emprestado VARCHAR(255),
	IN _exemplar_reservado VARCHAR(255),
	IN _exemplar_data_cadastro VARCHAR(255),
	IN _exemplar_data_alteracao VARCHAR(255),
	IN _exemplar_baixa_data VARCHAR(255),
	IN _exemplar_baixa_motivo VARCHAR(255),
	IN _setor_id VARCHAR(255),
	IN _exemplar_acesso VARCHAR(255),
	IN _exemplar_localizacao VARCHAR(255)
)
BEGIN
	DECLARE _exemplar_id, __setor_id VARCHAR(255);
	SET _exemplar_id=(SELECT CAST(migracao_para AS UNSIGNED) FROM biblioteca__migracao WHERE migracao_tipo='exemplar' and CAST(migracao_de AS UNSIGNED)=_Registro limit 1);

	IF _exemplar_id IS NULL THEN
		SET __setor_id=(SELECT CAST(migracao_para AS UNSIGNED) FROM biblioteca__migracao WHERE migracao_tipo='setor' and CAST(migracao_de AS UNSIGNED)=_setor_id*1);
		INSERT INTO `biblioteca__exemplar`
		(
			`exemplar_id`, `titulo_id`, `exemplar_codigo`, `exemplar_emprestado`,
			`exemplar_reservado`, `exemplar_data_cadastro`, `exemplar_data_alteracao`,
			`exemplar_baixa_data`, `exemplar_baixa_motivo`, `setor_id`, `exemplar_acesso`,
			`exemplar_localizacao`
		)
		VALUES
		(
			null, _titulo_id, _exemplar_codigo, IF(_exemplar_emprestado=1,'S','N'),
			IF(_exemplar_reservado=1,'S','N'), _exemplar_data_cadastro, _exemplar_data_alteracao,
			IF(_exemplar_baixa_data='1900-01-01 00:00:00', NULL, _exemplar_baixa_data), _exemplar_baixa_motivo,
			__setor_id, IF(_exemplar_acesso=0,'Restrito','Livre'),
			COALESCE(IF(_exemplar_localizacao="", NULL, _exemplar_localizacao), NULL)
		);

		SELECT LAST_INSERT_ID() into _exemplar_id from dual;

	    INSERT INTO biblioteca__migracao (`migracao_tipo`, `migracao_de`, `migracao_para`)
	    VALUES ('exemplar', _Registro, _exemplar_id), ('tituloX', _Registro, _titulo_id);
	    call migraTituloAssuntos(_titulo_id, _Registro);
		call migraTituloAutores(_titulo_id, _Registro);
    END IF;
END;;

DROP PROCEDURE IF EXISTS migraTituloAssuntos;;
CREATE PROCEDURE migraTituloAssuntos(
	IN _titulo_id VARCHAR(255),
	IN _Registro VARCHAR(255)
)
BEGIN
	DECLARE done INT DEFAULT FALSE;
	DECLARE _IDAssunto VARCHAR(255);
	DECLARE Consulta CURSOR FOR
		SELECT CAST(gl.migracao_para AS UNSIGNED)
		FROM acervo.tbassuntoacervo t
		INNER JOIN biblioteca__migracao gl on gl.migracao_tipo='assunto' and CAST(gl.migracao_de AS UNSIGNED)=t.IDAssunto
		LEFT JOIN biblioteca__titulo_assunto ta on ta.titulo_id=_titulo_id and ta.assunto_id=CAST(gl.migracao_para AS UNSIGNED)
		WHERE t.Registro=_Registro and ta.assunto_id IS NULL
		GROUP BY t.IDAssunto;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN Consulta;

	read_loop: LOOP
		FETCH Consulta INTO _IDAssunto;

		IF done THEN
			LEAVE read_loop;
		END IF;

		REPLACE INTO biblioteca__titulo_assunto (`titulo_id`, `assunto_id`)
		VALUES (_titulo_id, _IDAssunto);
	END LOOP read_loop;

	CLOSE Consulta;
END;;

DROP PROCEDURE IF EXISTS migraTituloAutores;;
CREATE PROCEDURE migraTituloAutores(
	IN _titulo_id VARCHAR(255),
	IN _Registro VARCHAR(255)
)
BEGIN
	DECLARE done INT DEFAULT FALSE;
	DECLARE _IdAutorAcervo,_Tipo VARCHAR(255);
	DECLARE Consulta CURSOR FOR
		SELECT CAST(gl.migracao_para AS UNSIGNED), t.Tipo
		FROM acervo.tbautoracervo t
		INNER JOIN biblioteca__migracao gl on gl.migracao_tipo='autor' and CAST(gl.migracao_de AS UNSIGNED)=t.IdAutorAcervo
		LEFT JOIN biblioteca__titulo_autor ta on ta.titulo_id=_titulo_id and ta.autor_id=CAST(gl.migracao_para AS UNSIGNED)
		WHERE t.Registro=_Registro and ta.autor_id IS NULL
		GROUP BY t.IdAutorAcervo;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN Consulta;

	read_loop: LOOP
		FETCH Consulta INTO _IdAutorAcervo,_Tipo;

		IF done THEN
			LEAVE read_loop;
		END IF;

		REPLACE INTO biblioteca__titulo_autor (`titulo_id`, `autor_id`, `titulo_autor_tipo`)
		VALUES (
			_titulo_id, _IdAutorAcervo,
			IF(_Tipo='A','Autor principal',
				IF(_Tipo='C','Colaborador',
					IF(_Tipo='T','Tradutor',
						IF(_Tipo='O','Orientador','Autor principal')
					)
				)
			)
		);
	END LOOP read_loop;

	CLOSE Consulta;
END;;

DROP PROCEDURE IF EXISTS migraTituloIdiomas;;
CREATE PROCEDURE migraTituloIdiomas(
	IN _titulo_id VARCHAR(255),
	IN _Idiomas VARCHAR(255)
)
BEGIN
	DECLARE done INT DEFAULT FALSE;
	DECLARE _idioma_id VARCHAR(255);
	DECLARE Consulta CURSOR FOR
		SELECT CAST(gl.migracao_para AS UNSIGNED)
		FROM biblioteca__migracao gl
		LEFT JOIN biblioteca__titulo_idioma ta on ta.titulo_id=_titulo_id and ta.idioma_id=CAST(gl.migracao_para AS UNSIGNED)
		WHERE ta.idioma_id IS NULL AND gl.migracao_tipo='idioma' AND FIND_IN_SET(gl.migracao_de, _Idiomas)
		GROUP BY gl.migracao_de;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN Consulta;

	read_loop: LOOP
		FETCH Consulta INTO _idioma_id;

		IF done THEN
			LEAVE read_loop;
		END IF;

		REPLACE INTO biblioteca__titulo_idioma (`titulo_id`, `idioma_id`)
		VALUES (_titulo_id, _idioma_id);
	END LOOP read_loop;

	CLOSE Consulta;
END;;

DROP PROCEDURE IF EXISTS migraTituloEditora;;
CREATE PROCEDURE migraTituloEditora()
BEGIN
	DECLARE done INT DEFAULT FALSE;
	DECLARE
			chave, qtd, Registros, Idiomas, _Registro, _idGrupo, _GrupoBibliografico, _DataRegistro, _Titulo, _Subtitulo, _AnoPublicacao,
			_LocalEdicao, _TipoEntrada, _Volume, _Exemplar, _Edicao, _ISBN, _idCNPQ, _IDSubArea, _IDArea,
			_Emprestado, _Reservado, _DataCompraDoacao, _NotaFiscal, _DataCatalogacao, _IDDoador, _CDU,
			_IDFornecedor, _ValordaNota, _Tomo, _Paginas, _Idioma1, _Idioma2, _Idioma3, _IDSerie, _Databaixa,
			_Motivobaixa, _IDEditora, _Editor, _ISSN, _IdSetor, _TipoAcesso, _Cutter, _Localizacao, _Permuta,
			_Periodicidade, _DataInicioPeriodico, _DataFinalPeriodico, _EditorPeriodico, _Circulacao,
			_Status, _Nota, _RequisitosUso, _Bibliografia, _idGrade
		TEXT;
	DECLARE _titulo_id INT;
	DECLARE iteracao_loop, tamanho_loop INT;
	DECLARE
		__grupo_bibliografico_id, __area_cnpq_id, __area_id, __colecao_id, __pes_id_editora
		VARCHAR(255);
	DECLARE Consulta CURSOR FOR
		SELECT * FROM acervo.tbacervo t
		WHERE t.Registro
		-- NOT IN(
		--  	SELECT CAST(migracao_de AS UNSIGNED) FROM biblioteca__migracao WHERE migracao_tipo='tituloX'
		-- )
		ORDER BY t.Registro ASC;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN Consulta;

	set tamanho_loop=(select FOUND_ROWS() from dual);
	set iteracao_loop=0;
	-- select iteracao_loop, tamanho_loop;

	read_loop: LOOP
		FETCH Consulta INTO
			-- chave, qtd, Registros, Idiomas,
			_Registro, _idGrupo, _GrupoBibliografico, _DataRegistro, _Titulo, _Subtitulo, _AnoPublicacao,
			_LocalEdicao, _TipoEntrada, _Volume, _Exemplar, _Edicao, _ISBN, _idCNPQ, _IDSubArea, _IDArea,
			_Emprestado, _Reservado, _DataCompraDoacao, _NotaFiscal, _DataCatalogacao, _IDDoador, _CDU,
			_IDFornecedor, _ValordaNota, _Tomo, _Paginas, _Idioma1, _Idioma2, _Idioma3, _IDSerie, _Databaixa,
			_Motivobaixa, _IDEditora, _Editor, _ISSN, _IdSetor, _TipoAcesso, _Cutter, _Localizacao, _Permuta,
			_Periodicidade, _DataInicioPeriodico, _DataFinalPeriodico, _EditorPeriodico, _Circulacao,
			_Status, _Nota, _RequisitosUso, _Bibliografia, _idGrade;

		IF done THEN
			LEAVE read_loop;
		END IF;

		SET chave=MD5(
			CONCAT(
				_Titulo,
				'-', COALESCE(_AnoPublicacao,''),
				'-', COALESCE(_LocalEdicao,''),
				'-', COALESCE(_Volume,''),
				'-', COALESCE(_Tomo,''),
				'-', COALESCE(_Edicao,'')
			)
		);

		SET _titulo_id=(SELECT CAST(migracao_para AS UNSIGNED) FROM biblioteca__migracao WHERE migracao_tipo='titulo' and migracao_de=chave limit 1);

		IF _titulo_id IS NOT NULL THEN
			SET __pes_id_editora=(SELECT CAST(migracao_para AS UNSIGNED) FROM biblioteca__migracao WHERE migracao_tipo='editora' and CAST(migracao_de AS UNSIGNED)=_IDEditora);
			UPDATE biblioteca__titulo SET pes_id_editora=__pes_id_editora WHERE titulo_id=_titulo_id;
	    END IF;
	END LOOP read_loop;
	CLOSE Consulta;
END;;

DELIMITER ;

-- call migraEditoraMista();
-- call migraEditora();
-- call migraFornecedor();
-- call migraGrupoBibliografico();
-- call migraGrupoLeitor();
-- call migraSetor();
-- call migraModalidades();
-- call migraLeitor();
-- call migraIdioma();
-- call migraArea();
-- call migraAreaCnpq();
-- call migraAssunto();
-- call migraAutor();
-- call migraColecao();

-- call migraTitulo();
-- call migraExemplaresLoop();
call migraTituloEditora();

SET SQL_SAFE_UPDATES=1;
SET FOREIGN_KEY_CHECKS=1;