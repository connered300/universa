ALTER TABLE `acadgeral__aluno_curso`
ADD `origem_id` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL,
ADD `alunocurso_pessoa_indicacao` VARCHAR(255) NULL,
ADD `alunocurso_mediador` VARCHAR(255) NULL,
ADD INDEX `fk_acadgeral__aluno_curso_acadgeral__cadastro_origem1_idx` (`origem_id` ASC);

ALTER TABLE `acadgeral__aluno_curso`
ADD CONSTRAINT `fk_acadgeral__aluno_curso_acadgeral__cadastro_origem1`
FOREIGN KEY (`origem_id`) REFERENCES `acadgeral__cadastro_origem` (`origem_id`);