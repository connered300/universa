-- MySQL Workbench Synchronization
-- Generated: 2016-01-07 14:56
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Leonardo

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `biblioteca__autor` CHANGE COLUMN `autor_referencia` `autor_referencia` VARCHAR(100) NOT NULL ;

CREATE TABLE IF NOT EXISTS `biblioteca__cutter` (
  `cutter_id` INT(10) NOT NULL DEFAULT 0,
  `cutter_nome` VARCHAR(200) NOT NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

ALTER TABLE `biblioteca__autor` ADD COLUMN `autor_cutter` VARCHAR(10) NULL DEFAULT NULL AFTER `autor_referencia`;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
