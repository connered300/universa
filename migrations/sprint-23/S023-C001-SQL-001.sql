
SELECT @mod:=id from acesso_modulos where mod_nome= 'Secretaria';

-- registrando funcionaliade de Curso
SELECT @func:=(max(id)+1) from acesso_funcionalidades;

INSERT INTO
`acesso_funcionalidades`(`id`, `modulo`, `func_nome`, `func_controller`, `func_icon`, `func_info`, `func_status`, `func_visivel`)
VALUES (@func, @mod, 'Cursos', 'Matricula\\Controller\\AcadCurso', null, null, 'Ativa', 'Sim');

INSERT INTO `acesso_actions` VALUES (null,@func,'index','Listagem',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';

INSERT INTO `acesso_actions` VALUES (null,@func, 'search-for-json', 'Pesquisa Opções Cursos',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';


-- registrando funcionaliade de Disciplinas
SELECT @func:=(max(id)+1) from acesso_funcionalidades;

INSERT INTO
`acesso_funcionalidades`(`id`, `modulo`, `func_nome`, `func_controller`, `func_icon`, `func_info`, `func_status`, `func_visivel`)
VALUES (@func, @mod, 'Gerência de Disciplinas', 'Matricula\\Controller\\AcadgeralDisciplina', null, null, 'Ativa', 'Sim');


INSERT INTO `acesso_actions` VALUES (null,@func,'index','Listagem',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';

INSERT INTO `acesso_actions` VALUES (null,@func,'add','Criar',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';

INSERT INTO `acesso_actions` VALUES (null,@func,'edit','Editar',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';

INSERT INTO `acesso_actions` VALUES (null,@func,'search-for-json','Autocomplete',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';

INSERT INTO `acesso_actions` VALUES (null,@func,'search','Datatables',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';

INSERT INTO `acesso_actions` VALUES (null,@func,'remove','Remover',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';