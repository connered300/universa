set @mod=0;
set @func=0;

select @mod:=id from acesso_modulos where mod_nome='Organizacao';
select @func:=(max(id)+1) from acesso_funcionalidades;

INSERT INTO acesso_funcionalidades VALUES (@func, @mod, 'Instituição', 'Organizacao\\Controller\\OrgIes', '', null, 'Ativa', 'Sim');

INSERT INTO `acesso_actions` VALUES (null,@func,'index','Listagem',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';

INSERT INTO `acesso_actions` VALUES (null,@func,'add','Criar',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';

INSERT INTO `acesso_actions` VALUES (null,@func,'edit','Editar',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';

INSERT INTO `acesso_actions` VALUES (null,@func,'search-for-json','Autocomplete',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';

INSERT INTO `acesso_actions` VALUES (null,@func,'search','Datatables',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';

INSERT INTO `arquivo_diretorios` (`arq_diretorio_id`, `arq_diretorio_nome`, `arq_diretorio_endereco`, `arq_espaco_disponivel`, `arq_espaco_ocupado`, `arq_ativo`) VALUES ('8', 'Instituicao', './data/uploads/instituicao', '10000000', '0', 'Sim');

ALTER TABLE `org_ies`
DROP FOREIGN KEY `fk_org_ies_arquivo1`;
ALTER TABLE `org_ies`
ADD CONSTRAINT `fk_org_ies_arquivo1`
  FOREIGN KEY (`arq_id`)
  REFERENCES `arquivo` (`arq_id`)
  ON DELETE SET NULL
  ON UPDATE CASCADE;

ALTER TABLE `org_campus`
DROP FOREIGN KEY `fk_org_campus_arquivo1`;
ALTER TABLE `org_campus`
ADD CONSTRAINT `fk_org_campus_arquivo1`
  FOREIGN KEY (`arq_id`)
  REFERENCES `arquivo` (`arq_id`)
  ON DELETE SET NULL
  ON UPDATE CASCADE;

