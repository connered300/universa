-- MySQL Workbench Synchronization
-- Generated: 2016-10-18 17:57
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `org_campus` 
ADD COLUMN `arq_id` INT(10) UNSIGNED ZEROFILL NULL DEFAULT NULL AFTER `secretaria_pes_id`,
ADD INDEX `fk_org_campus_arquivo1_idx` (`arq_id` ASC);

ALTER TABLE `org_ies` 
ADD COLUMN `diretoria_pes_id` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL COMMENT 'Secretária Geral' AFTER `ies_organizacao_academica`,
ADD COLUMN `secretaria_pes_id` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL COMMENT 'Secretária Geral' AFTER `diretoria_pes_id`,
ADD COLUMN `arq_id` INT(10) UNSIGNED ZEROFILL NULL DEFAULT NULL AFTER `secretaria_pes_id`,
ADD INDEX `fk_org_ies_pessoa_fisica1_idx` (`diretoria_pes_id` ASC),
ADD INDEX `fk_org_ies_pessoa_fisica2_idx` (`secretaria_pes_id` ASC),
ADD INDEX `fk_org_ies_arquivo1_idx` (`arq_id` ASC);

ALTER TABLE `org_campus` 
ADD CONSTRAINT `fk_org_campus_arquivo1`
  FOREIGN KEY (`arq_id`)
  REFERENCES `arquivo` (`arq_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `org_ies` 
ADD CONSTRAINT `fk_org_ies_pessoa_fisica1`
  FOREIGN KEY (`diretoria_pes_id`)
  REFERENCES `pessoa_fisica` (`pes_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_org_ies_pessoa_fisica2`
  FOREIGN KEY (`secretaria_pes_id`)
  REFERENCES `pessoa_fisica` (`pes_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_org_ies_arquivo1`
  FOREIGN KEY (`arq_id`)
  REFERENCES `arquivo` (`arq_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
