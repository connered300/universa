set @func=0;

select @func:=id from acesso_funcionalidades where func_controller='Organizacao\\Controller\\OrgCampus';

INSERT INTO `acesso_actions` VALUES (null,@func,'index','Listagem',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';

INSERT INTO `acesso_actions` VALUES (null,@func,'add','Criar',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';

INSERT INTO `acesso_actions` VALUES (null,@func,'edit','Editar',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';

INSERT INTO `acesso_actions` VALUES (null,@func,'search-for-json','Autocomplete',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';

INSERT INTO `acesso_actions` VALUES (null,@func,'search','Datatables',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';

ALTER TABLE `infra_predio`
ADD COLUMN `predio_tipo` ENUM('principal', 'apoio') NOT NULL DEFAULT 'principal' COMMENT '' AFTER `predio_logradouro`;

ALTER TABLE `org_campus` DROP FOREIGN KEY `fk_org_campus_endereco1`;
ALTER TABLE `org_campus` DROP COLUMN `end_id`, DROP INDEX `fk_org_campus_endereco1_idx` ;
