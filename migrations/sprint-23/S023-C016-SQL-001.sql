set @mod=0;
set @func=0;

select @mod:=id from acesso_modulos where mod_nome='Secretaria';
select @func:=(max(id)+1) from acesso_funcionalidades;

INSERT INTO acesso_funcionalidades VALUES (@func, @mod, 'Gestão de Cursos', 'Matricula\\Controller\\AcadCurso', '', null, 'Ativa', 'Sim');
-- select @func:=id from acesso_funcionalidades where func_controller='Matricula\\Controller\\AcadCurso';

INSERT INTO `acesso_actions` VALUES (null,@func,'index','Listagem',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';

INSERT INTO `acesso_actions` VALUES (null,@func,'add','Criar',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';

INSERT INTO `acesso_actions` VALUES (null,@func,'edit','Editar',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';

INSERT INTO `acesso_actions` VALUES (null,@func,'search-for-json','Autocomplete',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';

INSERT INTO `acesso_actions` VALUES (null,@func,'search','Datatables',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Admin%';

ALTER TABLE `acad_nivel`
CHANGE COLUMN `mod_obs` `nivel_obs` TEXT NULL DEFAULT NULL COMMENT 'Observações sobre o nivel do curso.' ;


REPLACE INTO `acad_nivel` (`nivel_id`, `nivel_nome`) VALUES
('00000000001', 'Ensino Fundamental'),
('00000000002', 'Ensino Medio'),
('00000000003', 'Graduacao'),
('00000000004', 'Pos Graduacao'),
('00000000005', 'Latu Senso'),
('00000000006', 'Stricto Senso'),
('00000000026', 'Licenciatura'),
('00000000027', 'Tecnólogo');

REPLACE INTO `acad_modalidade` (`mod_id`, `mod_nome`)
VALUES ('2', 'Semipresencial'), ('3', 'EAD');

