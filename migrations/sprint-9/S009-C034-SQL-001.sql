-- MySQL Workbench Synchronization
-- Generated: 2015-09-17 16:42
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: breno

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `acadperiodo__matriz_curricular` CHANGE `mat_cur_data` `mat_cur_data` DATETIME NULL COMMENT 'Data em que a matriz se torna ativa.';
ALTER TABLE `acadperiodo__matriz_curricular` CHANGE `mat_cur_data` `mat_cur_data` TEXT NULL DEFAULT NULL COMMENT 'Data em que a matriz se torna ativa.';
UPDATE `acadperiodo__matriz_curricular` SET `mat_cur_data` = 2015;

ALTER TABLE `acadperiodo__matriz_curricular`
CHANGE COLUMN `mat_cur_data` `mat_cur_ano` YEAR NOT NULL COMMENT 'Data em que a matriz se torna ativa.' ,
ADD COLUMN `mat_cur_semestre` INT(11) NULL DEFAULT NULL AFTER `mat_cur_descricao`;

ALTER TABLE `acadperiodo__matriz_curricular`
ADD CONSTRAINT `fk_acadgeral__matriz_curricular_acad_curso1`
  FOREIGN KEY (`curso_id`)
  REFERENCES `acad_curso` (`curso_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

UPDATE `acadperiodo__matriz_curricular` SET `mat_cur_semestre` = '1';

ALTER TABLE `acadperiodo__matriz_curricular` CHANGE `mat_cur_semestre` `mat_cur_semestre` INT(11) NOT NULL;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
