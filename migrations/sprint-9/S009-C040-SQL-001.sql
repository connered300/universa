-- MySQL Workbench Synchronization
-- Generated: 2015-09-16 14:58
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: breno

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `acadperiodo__matriz_disciplina`
CHANGE COLUMN `per_disc_creditos` `per_disc_creditos` INT(11) NULL DEFAULT NULL ;

ALTER TABLE `acadperiodo__matriz_curricular`
ADD CONSTRAINT `fk_acadgeral__matriz_curricular_acad_curso1`
  FOREIGN KEY (`curso_id`)
  REFERENCES `acad_curso` (`curso_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
