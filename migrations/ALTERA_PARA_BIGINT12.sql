SET foreign_key_checks = 0;
ALTER TABLE `acadperiodo__aluno_resumo`
CHANGE COLUMN `alunocurso_id` `alunocurso_id` BIGINT(12) UNSIGNED ZEROFILL NOT NULL;

ALTER TABLE `acadperiodo__aluno`
CHANGE COLUMN `alunocurso_id` `alunocurso_id` BIGINT(12) UNSIGNED ZEROFILL NOT NULL;

ALTER TABLE `atividadeperiodo__aluno_sumarizador`
CHANGE COLUMN `alunocurso_id` `alunocurso_id` BIGINT(12) UNSIGNED ZEROFILL NOT NULL;

ALTER TABLE `financeiro__titulo`
CHANGE COLUMN `alunocurso_id` `alunocurso_id` BIGINT(12) UNSIGNED ZEROFILL NULL;

ALTER TABLE `acadgeral__aluno_atividade`
CHANGE COLUMN `alunocurso_id` `alunocurso_id` BIGINT(12) UNSIGNED ZEROFILL NOT NULL;

ALTER TABLE `financeiro__aluno_config_pgto_curso`
CHANGE COLUMN `alunocurso_id` `alunocurso_id` BIGINT(12) UNSIGNED ZEROFILL NOT NULL;

ALTER TABLE `acadgeral__aluno_curso`
CHANGE COLUMN `alunocurso_id` `alunocurso_id` BIGINT(12) UNSIGNED ZEROFILL NOT NULL;


SET foreign_key_checks = 1;