ALTER TABLE `acadgeral__aluno_formacao`
ADD COLUMN `formacao_concluido` ENUM('Sim', 'Não') NULL DEFAULT 'Não' AFTER `formacao_ano`;
ALTER TABLE `acadgeral__aluno_formacao`
CHANGE COLUMN `formacao_concluido` `formacao_concluido` ENUM('Sim', 'Não') NOT NULL DEFAULT 'Sim';

ALTER TABLE `acadgeral__aluno` 
DROP FOREIGN KEY `acadgeral__aluno_ibfk_1`;
ALTER TABLE `acadgeral__aluno` 
ADD CONSTRAINT `acadgeral__aluno_ibfk_1`
  FOREIGN KEY (`arq_id`)
  REFERENCES `arquivo` (`arq_id`)
  ON DELETE SET NULL
  ON UPDATE CASCADE;

ALTER TABLE `acadperiodo__resumo_equivalencia`
DROP FOREIGN KEY `fk_acadperiodo__resumo_equivalencia_acadgeral__aluno_formacao1`,
DROP FOREIGN KEY `fk_acadperiodo__resumo_equivalencia_acadgeral__disciplina1`;
ALTER TABLE `acadperiodo__resumo_equivalencia`
DROP FOREIGN KEY `fk_acadperiodo__resumo_equivalencia_acadperiodo__aluno_resumo1`;
ALTER TABLE `acadperiodo__resumo_equivalencia` ADD CONSTRAINT `fk_acadperiodo__resumo_equivalencia_acadgeral__aluno_formacao1`
  FOREIGN KEY (`formacao_id`)
  REFERENCES `acadgeral__aluno_formacao` (`formacao_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_acadperiodo__resumo_equivalencia_acadperiodo__aluno_resumo1`
  FOREIGN KEY (`res_id`)
  REFERENCES `acadperiodo__aluno_resumo` (`res_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_acadperiodo__resumo_equivalencia_acadgeral__disciplina1`
  FOREIGN KEY (`disc_id`)
  REFERENCES `acadgeral__disciplina` (`disc_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
