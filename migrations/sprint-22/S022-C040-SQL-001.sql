-- MySQL Workbench Synchronization
-- Generated: 2016-09-19 14:20
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `org_campus` 
ADD COLUMN `arq_id` INT(10) UNSIGNED ZEROFILL NULL DEFAULT NULL AFTER `secretaria_pes_id`,
ADD INDEX `fk_org_campus_arquivo1_idx` (`arq_id` ASC);

ALTER TABLE `org_campus` 
ADD CONSTRAINT `fk_org_campus_arquivo1`
  FOREIGN KEY (`arq_id`)
  REFERENCES `arquivo` (`arq_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;


ALTER TABLE `selecao_inscricao` 
DROP FOREIGN KEY `fk_selecao_inscricao_infra_sala1`,
DROP FOREIGN KEY `fk_selecao_inscricao_selecao_tipo_edicao1`;

ALTER TABLE `selecao_inscricao` 
ADD COLUMN `selelocais_id` INT(11) UNSIGNED ZEROFILL NOT NULL AFTER `sala_id`,
ADD COLUMN `inscricao_data_horario_prova` DATETIME NULL DEFAULT NULL AFTER `inscricao_entrega_doc`,
ADD COLUMN `mae_cpf` VARCHAR(45) NULL DEFAULT NULL AFTER `inscricao_data_horario_prova`,
ADD COLUMN `mae_nome` VARCHAR(45) NULL DEFAULT NULL AFTER `mae_cpf`,
ADD COLUMN `pai_cpf` VARCHAR(45) NULL DEFAULT NULL AFTER `mae_nome`,
ADD COLUMN `pai_nome` VARCHAR(45) NULL DEFAULT NULL AFTER `pai_cpf`,
ADD COLUMN `inscricao_ip` VARCHAR(45) NULL DEFAULT NULL AFTER `pai_nome`,
ADD INDEX `fk_selecao_inscricao_selecao_locais1_idx` (`selelocais_id` ASC);

ALTER TABLE `selecao_inscricao` 
ADD CONSTRAINT `fk_selecao_inscricao_infra_sala1`
  FOREIGN KEY (`sala_id`)
  REFERENCES `infra_sala` (`sala_id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_selecao_inscricao_selecao_locais1`
  FOREIGN KEY (`selelocais_id`)
  REFERENCES `selecao_locais` (`selelocais_id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_selecao_inscricao_selecao_tipo_edicao1`
  FOREIGN KEY (`seletipoedicao_id`)
  REFERENCES `selecao_tipo_edicao` (`seletipoedicao_id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;