ALTER TABLE financeiro__titulo_alteracao
  MODIFY COLUMN alteracao_acao
  ENUM('Atualizacao','Parcelamento','Renegociacao','Pagamento Múltiplo','Estorno');

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT DISTINCT
    acesso_funcionalidades.id,
    actions.act,
    actions.descricao,
    actions.tipo,
    actions.usr
  FROM acesso_funcionalidades,
    (
      SELECT
        'estornar-titulo'    AS act,
        'Estorno de título' AS descricao,
        'Leitura'  AS tipo,
        'N'        AS usr
      FROM dual
    ) AS actions
  WHERE func_controller IN (
    'Financeiro\\Controller\\Painel'
  );
REPLACE INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
    ,
    acesso_grupo
  WHERE func_controller IN (
    'Financeiro\\Controller\\Painel'
  )
        AND grup_nome LIKE 'Admin%';