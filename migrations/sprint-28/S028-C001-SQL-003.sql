alter table financeiro__aluno_config_pgto_curso add column config_pgto_curso_vencimento_temp int;

update financeiro__aluno_config_pgto_curso set config_pgto_curso_vencimento_temp=config_pgto_curso_vencimento where config_pgto_curso_id>=0;

update financeiro__aluno_config_pgto_curso set config_pgto_curso_vencimento=null where config_pgto_curso_id>=0;

alter table financeiro__aluno_config_pgto_curso change column config_pgto_curso_vencimento config_pgto_curso_vencimento DATETIME DEFAULT NULL;

SET SQL_SAFE_UPDATES = 0;

UPDATE financeiro__aluno_config_pgto_curso as fa
inner join acadgeral__aluno_curso as a using(alunocurso_id)
SET fa.config_pgto_curso_vencimento=date_add( date(date_format(a.alunocurso_data_cadastro,'%Y-%m-01')), interval  greatest(1*fa.config_pgto_curso_vencimento_temp,1)-1 day )
where fa.config_pgto_curso_id>=0 and a.alunocurso_id>=0;


select * from financeiro__aluno_config_pgto_curso;


SET SQL_SAFE_UPDATES = 1;


-- alter table financeiro__aluno_config_pgto_curso drop column config_pgto_curso_vencimento_temp;