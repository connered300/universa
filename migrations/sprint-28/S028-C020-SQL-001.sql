/*
    * Este script é responsável por realizar a alteração da estrutura e dos dados das tabelas relativas ao armazenamento das informações do resultado da prova do vestibular.
 */

/*
 * Altera a estrutura da tabela inscrição_curso, migrando as colunas relativas ao resultado da prova do candidato para ela.
*/

ALTER TABLE `inscricao_cursos`
ADD COLUMN `inscricao_resultado` ENUM('aprovado', 'desclassificado', 'ausente', 'excedente') NULL DEFAULT NULL AFTER `inscricao_turno`,
ADD COLUMN `inscricao_classificacao` INT(10) NULL DEFAULT NULL AFTER `inscricao_resultado`,
ADD COLUMN `inscricao_nota` FLOAT NULL DEFAULT NULL AFTER `inscricao_classificacao`,
ADD COLUMN `inscricao_opcao` INT(10) NULL DEFAULT 1 AFTER `inscricao_nota`,
ADD COLUMN `inscricao_observacao` VARCHAR(245) NULL DEFAULT NULL AFTER `inscricao_opcao`,
ADD COLUMN `inscricao_data_alteracao` DATETIME NULL DEFAULT CURRENT_TIMESTAMP AFTER `inscricao_observacao`;

ALTER TABLE `inscricao_cursos`
CHANGE COLUMN `inscricao_curso_opcao` `inscricao_curso_opcao` INT(11) NULL DEFAULT 1 COMMENT 'Armazena  opção dos cursos selecionados pelo inscrito.\nEx: Op 1: Ciencia computação';

/*
 * Altera os dados relativos a prova de processos seletivos já concluídos, migrando os registros relativos ao resultado da prova do candidato para a tabela inscricao_cursos.
*/
UPDATE inscricao_cursos
        INNER JOIN
    selecao_inscricao ON inscricao_cursos.inscricao_id = selecao_inscricao.inscricao_id 
SET 
    inscricao_cursos.inscricao_resultado = selecao_inscricao.inscricao_resultado,
    inscricao_cursos.inscricao_classificacao = selecao_inscricao.inscricao_classificacao,
    inscricao_cursos.inscricao_nota = selecao_inscricao.inscricao_nota
WHERE
    inscricao_cursos.inscricao_id = selecao_inscricao.inscricao_id;

/*
 * Remove da tabela selecao_inscricao as colunas que foram migradas para tabela inscricao_cursos
*/
ALTER TABLE selecao_inscricao DROP COLUMN inscricao_resultado, DROP COLUMN inscricao_classificacao, DROP COLUMN inscricao_nota;