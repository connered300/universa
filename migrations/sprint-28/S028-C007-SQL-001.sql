# ADICIONANDO A COLUNA ALUNOCURSO_DATA_MATRICULA
ALTER TABLE `acadgeral__aluno_curso`
ADD COLUMN `alunocurso_data_matricula` DATETIME NULL DEFAULT NULL COMMENT 'data no qual o aluno foi identificado como aluno pela instuição';

# COLOCANDO TODOS OS VALORES DA DATA DE CADASTRO NA DATA DE MATRÍCULA
UPDATE acadgeral__aluno_curso SET alunocurso_data_matricula = alunocurso_data_cadastro WHERE alunocurso_id>=1;

# ADICIONANDO A COLUNA ALUNOCURSO_DATA_MATRICULA
ALTER TABLE `acadgeral__aluno_curso`
CHANGE `alunocurso_data_matricula` `alunocurso_data_matricula` DATETIME NOT NULL COMMENT 'data no qual o aluno foi identificado como aluno pela instuição';