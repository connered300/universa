  UPDATE inscricao_cursos
        INNER JOIN
    selecao_inscricao USING (inscricao_id) 
SET 
    inscricao_resultado = 'Ausente'
WHERE
    inscricao_status = 'Aceita'
        AND (inscricao_resultado = ''
        OR inscricao_resultado IS NULL)