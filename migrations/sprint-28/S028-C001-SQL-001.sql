
DROP VIEW IF EXISTS view__financeiro_titulos_pagos_agente;
DROP VIEW IF EXISTS view__financeiro_apuracao_comissao_nivel1_agente;
DROP VIEW IF EXISTS view__financeiro_apuracao_comissao_agente;
DROP VIEW IF EXISTS view__financeiro_titulos_pagos_agente_dia;


# view de titulos pagos pelos alunos do agente
CREATE OR  REPLACE VIEW view__financeiro_titulos_pagos_agente_dia AS
SELECT
	agente.pes_id,
	agente.pes_id_indicacao,
	agente_situacao,
	(agente_comissao_primeiro_nivel/100) AS agente_comissao_primeiro_nivel,
	(agente_comissao_demais_niveis/100) AS agente_comissao_demais_niveis,
	tipotitulo_id,
	COALESCE(titulo_valor_pago,0) valor_pago,
	titulo_id,
	date(titulo_data_pagamento) as titulo_data_pagamento
FROM financeiro__titulo t
INNER JOIN acadgeral__aluno_curso alunoCurso ON alunoCurso.alunocurso_id = t.alunocurso_id
INNER JOIN acadgeral__aluno aluno ON aluno.aluno_id = alunoCurso.aluno_id
INNER JOIN org__agente_educacional agente ON agente.pes_id =coalesce(alunoCurso.pes_id_agente, aluno.pes_id_agenciador)
WHERE
	t.titulo_estado in('Pago')
GROUP BY t.titulo_id, agente.pes_id, t.tipotitulo_id, date(titulo_data_pagamento);


# view de titulos pagos pelos alunos do agente
CREATE OR  REPLACE VIEW view__financeiro_titulos_pagos_agente AS
SELECT
	pes_id,
	pes_id_indicacao,
	tipotitulo_id,
	agente_comissao_primeiro_nivel,
	agente_comissao_demais_niveis,
	agente_situacao,
	(SUM(valor_pago) * agente_comissao_primeiro_nivel) as comissao,
	count(titulo_id) as quantidade,
	SUM(valor_pago) valor_pago,
	titulo_data_pagamento
FROM /*(
	SELECT
		agente.pes_id,
		agente.pes_id_indicacao,
		agente_situacao,
		(agente_comissao_primeiro_nivel/100) AS agente_comissao_primeiro_nivel,
		(agente_comissao_demais_niveis/100) AS agente_comissao_demais_niveis,
		tipotitulo_id,
		COALESCE(titulo_valor_pago,0) valor_pago,
		titulo_id,
		date(titulo_data_pagamento) as titulo_data_pagamento
	FROM financeiro__titulo t
	INNER JOIN acadgeral__aluno_curso alunoCurso ON alunoCurso.alunocurso_id = t.alunocurso_id
	INNER JOIN acadgeral__aluno aluno ON aluno.aluno_id = alunoCurso.aluno_id
	INNER JOIN org__agente_educacional agente ON agente.pes_id =coalesce(alunoCurso.pes_id_agente, aluno.pes_id_agenciador)
	WHERE
		t.titulo_estado in('Pago', 'Pagamento Múltiplo')
	GROUP BY t.titulo_id, agente.pes_id, t.tipotitulo_id, date(titulo_data_pagamento)
)*/
view__financeiro_titulos_pagos_agente_dia
as apuracao

GROUP BY apuracao.pes_id, tipotitulo_id, titulo_data_pagamento;


ALTER TABLE `pessoa` CHANGE COLUMN `pes_nome` `pes_nome` VARCHAR(255) NOT NULL ;
ALTER TABLE `pessoa_juridica` CHANGE COLUMN `pes_nome_fantasia` `pes_nome_fantasia` VARCHAR(255) NULL DEFAULT NULL;


ALTER TABLE `acadgeral__aluno_curso` CHANGE COLUMN `alunocurso_situacao` `alunocurso_situacao` ENUM('Deferido', 'Pendente', 'Trancado', 'Cancelado', 'Indeferido') NOT NULL DEFAULT 'Deferido' ;
