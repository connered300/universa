ALTER TABLE `documento__modelo`
  CHANGE COLUMN `modelo_contexto` `modelo_contexto`
    ENUM('Aluno', 'Aluno Curso', 'Pessoa', 'Agente Educacional');

DROP TABLE IF EXISTS `documento__modelo_grupo_permissao`;
CREATE TABLE IF NOT EXISTS `documento__modelo_grupo_permissao` (
  `modelo_id` INT(10) UNSIGNED ZEROFILL NOT NULL,
  `grupo_id` INT(11) UNSIGNED ZEROFILL NOT NULL,
  PRIMARY KEY (`modelo_id`, `grupo_id`),
  INDEX `fk_acesso_grupo_has_documento__modelo_documento__modelo1_idx` (`modelo_id` ASC),
  INDEX `fk_acesso_grupo_has_documento__modelo_acesso_grupo1_idx` (`grupo_id` ASC),
  CONSTRAINT `fk_acesso_grupo_has_documento__modelo_acesso_grupo1`
  FOREIGN KEY (`grupo_id`)
  REFERENCES `acesso_grupo` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_acesso_grupo_has_documento__modelo_documento__modelo1`
  FOREIGN KEY (`modelo_id`)
  REFERENCES `documento__modelo` (`modelo_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;