
ALTER TABLE `acad_curso`
ADD COLUMN `curso_situacao` ENUM('Ativo', 'Inativo') NOT NULL DEFAULT 'Ativo' AFTER `curso_prazo_integralizacao_maxima`;

UPDATE acad_curso SET curso_situacao='Ativo' WHERE curso_id>=0;