REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT DISTINCT
    acesso_funcionalidades.id,
    actions.act,
    actions.descricao,
    actions.tipo,
    actions.usr
  FROM acesso_funcionalidades,
    (
      SELECT
        'enviar-comunicacao'    AS act,
        'Envio de comunicação ao aluno' AS descricao,
        'Leitura'  AS tipo,
        'N'        AS usr
      FROM dual
    ) AS actions
  WHERE func_controller IN (
    'Organizacao\\Controller\\OrgComunicacaoModelo'
  );
REPLACE INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
    ,
    acesso_grupo
  WHERE func_controller IN (
    'Organizacao\\Controller\\OrgComunicacaoModelo'
  )
        AND grup_nome LIKE 'Admin%';

SET FOREIGN_KEY_CHECKS = 0;

ALTER TABLE `org__comunicacao_tipo`
ADD COLUMN `tipo_contexto` ENUM('Aluno', 'Aluno Curso', 'Pessoa', 'Agente Educacional') NOT NULL DEFAULT 'Aluno';

ALTER TABLE `sis__fila_email` DROP FOREIGN KEY fk_sis__fila_email_org__comunicacao_modelo1;
ALTER TABLE `sis__fila_email` DROP COLUMN modelo_id;
ALTER TABLE `sis__fila_sms` DROP FOREIGN KEY fk_sis__fila_sms_org__comunicacao_modelo1;
ALTER TABLE `sis__fila_sms` DROP COLUMN modelo_id;

ALTER TABLE `sis__fila_email`
ADD COLUMN `tipo_id` INT ZEROFILL UNSIGNED NOT NULL,
ADD INDEX `fk_sis__fila_email_org__comunicacao_tipo1_idx` (`tipo_id` ASC),
ADD CONSTRAINT `fk_sis__fila_email_org__comunicacao_tipo1`
FOREIGN KEY (`tipo_id`)
REFERENCES `org__comunicacao_tipo` (`tipo_id`);

ALTER TABLE `sis__fila_sms`
ADD COLUMN `tipo_id` INT ZEROFILL UNSIGNED NOT NULL,
ADD INDEX `fk_sis__fila_sms_org__comunicacao_tipo1_idx` (`tipo_id` ASC),
ADD CONSTRAINT `fk_sis__fila_sms_org__comunicacao_tipo1`
FOREIGN KEY (`tipo_id`)
REFERENCES `org__comunicacao_tipo` (`tipo_id`);

SET FOREIGN_KEY_CHECKS = 1;