ALTER TABLE `acadgeral__aluno_curso` CHANGE `alunocurso_situacao` `alunocurso_situacao` ENUM('Deferido', 'Pendente', 'Trancado', 'Cancelado', 'Indeferido', 'Transferencia')
CHARACTER SET utf8
COLLATE utf8_general_ci NOT NULL DEFAULT 'Deferido';
ALTER TABLE `acadgeral__aluno_curso_historico` CHANGE `alunocurso_situacao` `alunocurso_situacao` ENUM('Deferido', 'Pendente', 'Trancado', 'Cancelado', 'Indeferido', 'Transferencia')
CHARACTER SET utf8
COLLATE utf8_general_ci NOT NULL DEFAULT 'Deferido';