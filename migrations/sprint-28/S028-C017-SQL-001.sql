--
-- módulo sistema
--
REPLACE INTO `acesso_funcionalidades` (modulo, func_nome, func_controller, func_status, func_visivel)
SELECT distinct acesso_modulos.id, func.descricao, func.controller,'Ativa','Sim'  from acesso_modulos,
(
	select 'Cursos de alunos' as descricao, 'Matricula\\Controller\\AcadgeralAlunoCurso' as controller from dual
) as func
where mod_nome like 'Secretaria';

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
(
	select 'index' as act,'Listagem' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'add' AS act, 'Adição' AS descricao, 'Escrita' as tipo,'N' as usr from dual union all
	select 'edit' AS act, 'Edição' AS descricao, 'Escrita' as tipo,'N' as usr from dual union all
	select 'search' AS act, 'Dados para o datatables' AS descricao, 'Escrita' as tipo,'N' as usr from dual union all
	select 'search-for-json' AS act, 'Pesquisa Ajax' AS descricao, 'Escrita' as tipo,'N' as usr from dual union all
	select 'aluno-curso-informacoes' as act,'Informações de curso de aluno' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'deferir-aluno-curso' as act,'Deferir curso de aluno' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'alterar-situacao' as act,'Alteração de Situação' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'alterar-datas' as act,'Alteração de Datas' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'relatorio-alunos' as act,'Relatório de alunos' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'remove' AS act, 'Remoção' AS descricao, 'Escrita' as tipo,'N' as usr from dual
) as actions
where func_controller IN('Matricula\\Controller\\AcadgeralAlunoCurso');

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
(
	select 'ficha-inscricao' AS act, 'Ficha Inscrição' AS descricao, 'Escrita' as tipo,'N' as usr from dual
) as actions
where func_controller IN('Matricula\\Controller\\Relatorios');

REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
acesso_grupo
where func_controller IN(
		'Matricula\\Controller\\AcadgeralAlunoCurso',
		'Matricula\\Controller\\Relatorios'
)
and grup_nome LIKE 'admin%';




REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
(
	select 'relatorio-alunos' as act,'Relatório de alunos' as descricao,'Leitura' as tipo,'N' as usr from dual
) as actions
where func_controller IN('Matricula\\Controller\\AcadgeralAluno');

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
(
	select 'ficha-inscricao' AS act, 'Ficha Inscrição' AS descricao, 'Escrita' as tipo,'N' as usr from dual
) as actions
where func_controller IN('Matricula\\Controller\\Relatorios');

REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
acesso_grupo
where func_controller IN(
		'Matricula\\Controller\\AcadgeralAluno',
		'Matricula\\Controller\\AcadgeralAlunoCurso',
		'Matricula\\Controller\\Relatorios'
)
and grup_nome LIKE 'admin%';