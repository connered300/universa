CREATE TABLE IF NOT EXISTS `org__comunicacao_modelo_grupo_permissao` (
  `grupo_id` INT(11) UNSIGNED ZEROFILL NOT NULL,
  `modelo_id` INT ZEROFILL UNSIGNED NOT NULL,
  PRIMARY KEY (`grupo_id`, `modelo_id`),
  INDEX `fk_acesso_grupo_org__comunicacao_modelo_org__comunicacao_mo_idx` (`modelo_id` ASC),
  INDEX `fk_acesso_grupo_org__comunicacao_modelo_acesso_grupo1_idx` (`grupo_id` ASC),
  CONSTRAINT `fk_acesso_grupo_org__comunicacao_modelo_acesso_grupo12` -- JA EXISTE UMA CHAVE SEM O 2, PORTANTO NÃO O REMOVAM
    FOREIGN KEY (`grupo_id`)
    REFERENCES `acesso_grupo` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_acesso_grupo_org__comunicacao_modelo_org__comunicacao_mode12` -- CASO SUPRACITADO
    FOREIGN KEY (`modelo_id`)
    REFERENCES `org__comunicacao_modelo` (`modelo_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

ALTER TABLE sis__fila_email ADD COLUMN  `tipo_id` INT ZEROFILL UNSIGNED NULL DEFAULT NULL;
ALTER TABLE sis__fila_email ADD INDEX `fk_sis__fila_email_org__comunicacao_tipo1_idx` (`tipo_id` ASC);
ALTER TABLE `sis__fila_email` ADD CONSTRAINT `fk_sis__fila_email_org__comunicacao_tipo1`
FOREIGN KEY (`tipo_id`) REFERENCES `org__comunicacao_tipo` (`tipo_id`);

ALTER TABLE sis__fila_sms ADD COLUMN  `tipo_id` INT ZEROFILL UNSIGNED NULL DEFAULT NULL;
ALTER TABLE sis__fila_sms ADD INDEX `fk_sis__fila_sms_org__comunicacao_tipo1_idx` (`tipo_id` ASC);
ALTER TABLE `sis__fila_sms` ADD CONSTRAINT `fk_sis__fila_sms_org__comunicacao_tipo1`
FOREIGN KEY (`tipo_id`) REFERENCES `org__comunicacao_tipo` (`tipo_id`);
