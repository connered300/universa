ALTER TABLE `financeiro__titulo_config`
CHANGE COLUMN `tituloconf_percent_desc` `tituloconf_percent_desc` FLOAT(5, 5) NULL DEFAULT NULL,
CHANGE COLUMN `tituloconf_multa` `tituloconf_multa` FLOAT(5, 5) NULL DEFAULT NULL,
CHANGE COLUMN `tituloconf_juros` `tituloconf_juros` FLOAT(5, 5) NULL DEFAULT NULL;
