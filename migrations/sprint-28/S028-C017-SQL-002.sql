ALTER TABLE `acadgeral__aluno_curso`
ADD COLUMN `motivo_id` int(10) NULL DEFAULT NULL AFTER `alunocurso_data_matricula`,
ADD COLUMN `alunocurso_alteracao_observacao` VARCHAR(255) NULL DEFAULT NULL AFTER `motivo_id`;

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `acadgeral__aluno_curso`
ADD CONSTRAINT `fk_alteracao_motivo1`
FOREIGN KEY (motivo_id)
REFERENCES `acadgeral__motivo_alteracao` (motivo_id)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

CREATE TABLE IF NOT EXISTS `acadgeral__aluno_curso_historico` (
  `alunocurso_hist_id` int(11) NOT NULL auto_increment,
  `alunocurso_id` bigint(12) unsigned zerofill NOT NULL,
  `cursocampus_id` int(10) unsigned zerofill NOT NULL,
  `aluno_id` int(10) unsigned zerofill NOT NULL,
  `tiposel_id` int(10) unsigned zerofill DEFAULT NULL,
  `pes_id_agente` int(11) unsigned zerofill DEFAULT NULL,
  `alunocurso_data_cadastro` datetime NOT NULL COMMENT 'Cadastro do aluno no sistema.',
  `alunocurso_data_alteracao` datetime DEFAULT CURRENT_TIMESTAMP,
  `alunocurso_carteira` varchar(45) DEFAULT NULL,
  `alunocurso_observacoes` text,
  `alunocurso_data_colacao` date DEFAULT NULL,
  `alunocurso_data_expedicao_diploma` date DEFAULT NULL,
  `alunocurso_situacao` enum('Deferido','Pendente','Trancado','Cancelado','Indeferido') NOT NULL DEFAULT 'Deferido',
  `usuario_cadastro` int(11) unsigned zerofill DEFAULT NULL,
  `usuario_alteracao` int(11) unsigned zerofill DEFAULT NULL,
  `alunocurso_data_matricula` datetime NOT NULL COMMENT 'data no qual o aluno foi identificado como aluno pela instuição',
  `motivo_id` int(10) unsigned zerofill DEFAULT NULL,
  `alunocurso_alteracao_observacao` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`alunocurso_hist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Registro da alteracao no cadastro do aluno';
