CREATE TABLE `acadgeral__motivo_alteracao` (
  `motivo_id`        INT(10) ZEROFILL NOT NULL AUTO_INCREMENT,
  `motivo_descricao` VARCHAR(245)     NOT NULL,
  PRIMARY KEY (`motivo_id`)
);
