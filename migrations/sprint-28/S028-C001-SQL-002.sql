
-- view com informações sobre os descontos de títulos
DROP VIEW IF EXISTS `view__financeiro_descontos_titulos_simplificado`;
DROP TABLE IF EXISTS `view__financeiro_descontos_titulos_simplificado`;
CREATE OR REPLACE VIEW `view__financeiro_descontos_titulos_simplificado` AS
  SELECT
    fdt.*,
    fd.aluno_id,
    fd.desconto_valor,
    fd.desconto_percentual,
    fd.desconto_status,
    fd.desconto_dia_limite,
    fdtp.desctipo_id,
    fdtp.desctipo_limita_vencimento,
    -- Verifica se o título está aberto
    if(titulo_estado = 'Aberto',
       -- verifica se o desconto está ativo e se o desconto do título foi deferido
       if(fd.desconto_status = 'Inativo' OR desconto_titulo_situacao <> 'Deferido',
          'Não',
          -- verifica se o desconto está limitado ao vencimento e se o título está vencido
          if(fdtp.desctipo_limita_vencimento = 'Sim' AND titulo_data_vencimento < now(),
             'Não',
             -- verifica se o dia limite do desconto relativo ao mês da data de vencimento já passou
             if(fd.desconto_dia_limite IS NOT NULL AND
                DATE(CONCAT(
                         year(titulo_data_vencimento), '-',
                         month(titulo_data_vencimento), '-',
                         fd.desconto_dia_limite
                     )) < curdate(),
                'Não',
                'Sim'
             )

          )
       ),
       -- verifica se o desconto foi aplicado
       if(fdt.desconto_titulo_aplicado = 'Sim', 'Sim', 'Não')
    )             AS desconto_aplicacao_valida,
    -- Verifica se o título está aberto
    if(titulo_estado = 'Aberto',
       -- verifica se o desconto é monetário ou percentual
       if(fd.desconto_valor IS NOT NULL,
          fd.desconto_valor,
          titulo_valor * (fd.desconto_percentual / 100)
       ),
       -- verifica se o desconto foi aplicado
       if(fdt.desconto_titulo_aplicado = 'Sim', desconto_titulo_valor, 0)
    )             AS desconto_aplicacao_valor,
    titulo_id        tituloId,
    titulo_valor,
    titulo_data_vencimento,
    titulo_estado,
    titulo_estado AS tituloEstado
  FROM financeiro__desconto_titulo fdt
    INNER JOIN financeiro__desconto fd USING (desconto_id)
    INNER JOIN financeiro__desconto_tipo fdtp USING (desctipo_id)
    LEFT JOIN financeiro__titulo ft USING (titulo_id)
  ORDER BY titulo_id DESC;