SELECT
    org__agente_educacional.pes_id,
    (
        pessoa.pes_nome
    ) agenteNome,
    count(
        DISTINCT alunocurso_id
    ) quantidadeAlunoCurso,
    count(
        DISTINCT ac.aluno_id
    ) quantidadeAlunoCurso,
    (
        ac.alunocurso_data_cadastro
    ) dataCadastro,
    ac.alunocurso_data_matricula,
    (
        ac.alunocurso_situacao
    )
FROM acadgeral__aluno_curso ac
    INNER JOIN acadgeral__aluno a ON a.aluno_id = ac.aluno_id
    INNER JOIN org__agente_educacional
        ON org__agente_educacional.pes_id = coalesce(ac.pes_id_agente, a.pes_id_agenciador)
    INNER JOIN pessoa ON pessoa.pes_id=org__agente_educacional.pes_id;