-- MySQL Workbench Synchronization
-- Generated: 2016-01-25 11:22
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE IF NOT EXISTS `biblioteca__titulo` (
  `titulo_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `grupo_bibliografico_id` INT(10) UNSIGNED ZEROFILL NOT NULL,
  `area_cnpq_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  `area_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  `titulo_data_cadastro` DATETIME NOT NULL,
  `titulo_data_alteracao` DATETIME NULL DEFAULT NULL,
  `titulo_titulo` TINYTEXT NOT NULL,
  `titulo_descricao` TEXT NULL DEFAULT NULL,
  `pes_id_editora` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL,
  `titulo_edicao_ano` INT(4) ZEROFILL NULL DEFAULT NULL,
  `titulo_edicao_local` VARCHAR(150) NULL DEFAULT NULL,
  `titulo_edicao_volume` INT(11) NULL DEFAULT NULL,
  `titulo_edicao_numero` INT(11) NULL DEFAULT NULL,
  `titulo_edicao_isbn` VARCHAR(18) NULL DEFAULT NULL,
  `titulo_edicao_cdu` VARCHAR(20) NULL DEFAULT NULL,
  `titulo_edicao_cutter` VARCHAR(20) NULL DEFAULT NULL,
  `titulo_edicao_tomo` INT(11) NULL DEFAULT NULL,
  `titulo_paginas` INT(11) NULL DEFAULT NULL,
  `colecao_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  `titulo_nota` TEXT NULL DEFAULT NULL,
  `titulo_requisitos_uso` TEXT NULL DEFAULT NULL,
  `titulo_situacao` ENUM('Ativo','Baixado') NOT NULL,
  PRIMARY KEY (`titulo_id`),
  INDEX `fk_biblioteca__titulo_biblioteca__grupo_bibliografico1_idx` (`grupo_bibliografico_id` ASC),
  INDEX `fk_biblioteca__titulo_biblioteca__area_cnpq1_idx` (`area_cnpq_id` ASC),
  INDEX `fk_biblioteca__titulo_biblioteca__area1_idx` (`area_id` ASC),
  INDEX `fk_biblioteca__titulo_biblioteca__empresa1_idx` (`pes_id_editora` ASC),
  INDEX `fk_biblioteca__titulo_biblioteca__colecao1_idx` (`colecao_id` ASC),
  CONSTRAINT `fk_biblioteca__titulo_biblioteca__grupo_bibliografico1`
    FOREIGN KEY (`grupo_bibliografico_id`)
    REFERENCES `biblioteca__grupo_bibliografico` (`grupo_bibliografico_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_biblioteca__titulo_biblioteca__area_cnpq1`
    FOREIGN KEY (`area_cnpq_id`)
    REFERENCES `biblioteca__area_cnpq` (`area_cnpq_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_biblioteca__titulo_biblioteca__area1`
    FOREIGN KEY (`area_id`)
    REFERENCES `biblioteca__area` (`area_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_biblioteca__titulo_biblioteca__empresa1`
    FOREIGN KEY (`pes_id_editora`)
    REFERENCES `biblioteca__empresa` (`pes_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_biblioteca__titulo_biblioteca__colecao1`
    FOREIGN KEY (`colecao_id`)
    REFERENCES `biblioteca__colecao` (`colecao_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `biblioteca__idioma` (
  `idioma_id` INT(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  `idioma_descricao` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`idioma_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `biblioteca__acervo_idioma` (
  `acervo_idioma_id` INT(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  `titulo_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  `idioma_id` INT(10) UNSIGNED ZEROFILL NOT NULL,
  INDEX `fk_biblioteca__titulo_biblioteca__idioma_biblioteca__idioma_idx` (`idioma_id` ASC),
  INDEX `fk_biblioteca__titulo_biblioteca__idioma_biblioteca__titulo_idx` (`titulo_id` ASC),
  PRIMARY KEY (`acervo_idioma_id`),
  CONSTRAINT `fk_biblioteca__titulo_biblioteca__idioma_biblioteca__titulo1`
    FOREIGN KEY (`titulo_id`)
    REFERENCES `biblioteca__titulo` (`titulo_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_biblioteca__titulo_biblioteca__idioma_biblioteca__idioma1`
    FOREIGN KEY (`idioma_id`)
    REFERENCES `biblioteca__idioma` (`idioma_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `biblioteca__colecao` (
  `colecao_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `pes_id` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL,
  `colecao_descricao` VARCHAR(200) NULL DEFAULT NULL,
  PRIMARY KEY (`colecao_id`),
  INDEX `fk_biblioteca__colecao_biblioteca__empresa1_idx` (`pes_id` ASC),
  CONSTRAINT `fk_biblioteca__colecao_biblioteca__empresa1`
    FOREIGN KEY (`pes_id`)
    REFERENCES `biblioteca__empresa` (`pes_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `biblioteca__acervo_autor` (
  `acervo_autor_id` INT(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  `titulo_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  `autor_id` INT(10) UNSIGNED ZEROFILL NOT NULL,
  `acervo_autor_tipo` ENUM('Autor principal', 'Colaborador', 'Tradutor', 'Orientador') NOT NULL,
  INDEX `fk_biblioteca__titulo_biblioteca__autor_biblioteca__autor1_idx` (`autor_id` ASC),
  INDEX `fk_biblioteca__titulo_biblioteca__autor_biblioteca__titulo1_idx` (`titulo_id` ASC),
  PRIMARY KEY (`acervo_autor_id`),
  CONSTRAINT `fk_biblioteca__titulo_biblioteca__autor_biblioteca__titulo1`
    FOREIGN KEY (`titulo_id`)
    REFERENCES `biblioteca__titulo` (`titulo_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_biblioteca__titulo_biblioteca__autor_biblioteca__autor1`
    FOREIGN KEY (`autor_id`)
    REFERENCES `biblioteca__autor` (`autor_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `biblioteca__titulo_assunto` (
  `titulo_assunto_id` INT(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  `assunto_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  `titulo_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  INDEX `fk_biblioteca__assunto_biblioteca__titulo_biblioteca__titul_idx` (`titulo_id` ASC),
  INDEX `fk_biblioteca__assunto_biblioteca__titulo_biblioteca__assun_idx` (`assunto_id` ASC),
  PRIMARY KEY (`titulo_assunto_id`),
  CONSTRAINT `fk_biblioteca__assunto_biblioteca__titulo_biblioteca__assunto1`
    FOREIGN KEY (`assunto_id`)
    REFERENCES `biblioteca__assunto` (`assunto_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_biblioteca__assunto_biblioteca__titulo_biblioteca__titulo1`
    FOREIGN KEY (`titulo_id`)
    REFERENCES `biblioteca__titulo` (`titulo_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `biblioteca__exemplar` (
  `exemplar_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `titulo_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  `exemplar_codigo` INT(11) NOT NULL,
  `exemplar_emprestado` ENUM('S','N') NOT NULL DEFAULT 'N',
  `exemplar_reservado` ENUM('S','N') NOT NULL DEFAULT 'N',
  `exemplar_data_cadastro` INT(11) NOT NULL,
  `exemplar_data_alteracao` DATETIME NOT NULL,
  `exemplar_baixa_data` DATETIME NULL DEFAULT NULL,
  `exemplar_baixa_motivo` VARCHAR(20) NULL DEFAULT NULL,
  `setor_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  `exemplar_acesso` ENUM('Livre','Restrito') NOT NULL,
  `exemplar_localizacao` VARCHAR(200) NULL DEFAULT NULL,
  PRIMARY KEY (`exemplar_id`),
  INDEX `fk_biblioteca__exemplar_biblioteca__titulo1_idx` (`titulo_id` ASC),
  INDEX `fk_biblioteca__exemplar_biblioteca__setor1_idx` (`setor_id` ASC),
  CONSTRAINT `fk_biblioteca__exemplar_biblioteca__titulo1`
    FOREIGN KEY (`titulo_id`)
    REFERENCES `biblioteca__titulo` (`titulo_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_biblioteca__exemplar_biblioteca__setor1`
    FOREIGN KEY (`setor_id`)
    REFERENCES `biblioteca__setor` (`setor_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `biblioteca__aquisicao` (
  `aquisicao_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `titulo_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  `pes_id_doador` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL COMMENT 'Doador',
  `pes_id_fornecedor` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL,
  `aquisicao_tipo` ENUM('Compra','Doacao','Permuta','Outro') NULL DEFAULT NULL,
  `aquisicao_data` DATE NOT NULL,
  `aquisicao_nota_fiscal` VARCHAR(50) NULL DEFAULT NULL,
  `aquisicao_permuta_descricao` TEXT NULL DEFAULT NULL,
  `aquisicao_nota_valor` DOUBLE NULL DEFAULT NULL,
  `aquisicao_observacao` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`aquisicao_id`),
  INDEX `fk_biblioteca__aquisicao_biblioteca__pessoa1_idx` (`pes_id_doador` ASC),
  INDEX `fk_biblioteca__aquisicao_biblioteca__empresa1_idx` (`pes_id_fornecedor` ASC),
  INDEX `fk_biblioteca__aquisicao_biblioteca__titulo1_idx` (`titulo_id` ASC),
  CONSTRAINT `fk_biblioteca__aquisicao_biblioteca__pessoa1`
    FOREIGN KEY (`pes_id_doador`)
    REFERENCES `biblioteca__pessoa` (`pes_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_biblioteca__aquisicao_biblioteca__empresa1`
    FOREIGN KEY (`pes_id_fornecedor`)
    REFERENCES `biblioteca__empresa` (`pes_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_biblioteca__aquisicao_biblioteca__titulo1`
    FOREIGN KEY (`titulo_id`)
    REFERENCES `biblioteca__titulo` (`titulo_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `biblioteca__setor` (
  `setor_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `setor_descricao` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`setor_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `biblioteca__aquisicao_exemplar` (
  `aquisicao_exemplar_id` INT(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  `aquisicao_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  `exemplar_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  PRIMARY KEY (`aquisicao_exemplar_id`),
  INDEX `fk_biblioteca__aquisicao_biblioteca__exemplar_biblioteca__e_idx` (`exemplar_id` ASC),
  INDEX `fk_biblioteca__aquisicao_biblioteca__exemplar_biblioteca__a_idx` (`aquisicao_id` ASC),
  CONSTRAINT `fk_biblioteca__aquisicao_biblioteca__exemplar_biblioteca__aqu1`
    FOREIGN KEY (`aquisicao_id`)
    REFERENCES `biblioteca__aquisicao` (`aquisicao_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_biblioteca__aquisicao_biblioteca__exemplar_biblioteca__exe1`
    FOREIGN KEY (`exemplar_id`)
    REFERENCES `biblioteca__exemplar` (`exemplar_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
