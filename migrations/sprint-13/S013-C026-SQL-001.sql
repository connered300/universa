-- MySQL Workbench Synchronization
-- Generated: 2016-01-22 11:12
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE IF NOT EXISTS `biblioteca__assunto` (
  `assunto_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `assunto_descricao` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`assunto_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `biblioteca__assunto_relacionado` (
  `assunto_relacionado_id` INT(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  `assunto_id_origem` INT(10) ZEROFILL UNSIGNED NOT NULL,
  `assunto_id_destino` INT(10) ZEROFILL UNSIGNED NOT NULL,
  INDEX `fk_biblioteca__assunto_biblioteca__assunto_biblioteca__assu_idx` (`assunto_id_destino` ASC),
  INDEX `fk_biblioteca__assunto_biblioteca__assunto_biblioteca__assu_idx1` (`assunto_id_origem` ASC),
  PRIMARY KEY (`assunto_relacionado_id`),
  CONSTRAINT `fk_biblioteca__assunto_biblioteca__assunto_biblioteca__assunto1`
    FOREIGN KEY (`assunto_id_origem`)
    REFERENCES `biblioteca__assunto` (`assunto_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_biblioteca__assunto_biblioteca__assunto_biblioteca__assunto2`
    FOREIGN KEY (`assunto_id_destino`)
    REFERENCES `biblioteca__assunto` (`assunto_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
