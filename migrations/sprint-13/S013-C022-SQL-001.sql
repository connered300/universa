-- MySQL Workbench Synchronization
-- Generated: 2016-01-19 09:21
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `biblioteca__area_cnpq` 
CHANGE COLUMN `area_cnpq_codigo` `area_cnpq_codigo` VARCHAR(15) NOT NULL AFTER `area_cnpq_id`;

CREATE TABLE IF NOT EXISTS `biblioteca__pessoa` (
  `pes_id` INT(11) UNSIGNED ZEROFILL NOT NULL,
  `grupo_leitor_id` INT(10) UNSIGNED ZEROFILL NULL DEFAULT NULL,
  `pessoa_senha` VARCHAR(35) NULL DEFAULT NULL,
  `pessoa_observacao` TEXT NULL DEFAULT NULL,
  `pessoa_situacao` ENUM('Ativo', 'Inativo') NULL DEFAULT NULL,
  INDEX `fk_biblioteca__pessoa_pessoa_fisica1_idx` (`pes_id` ASC),
  INDEX `fk_biblioteca__pessoa_biblioteca__grupo_leitor1_idx` (`grupo_leitor_id` ASC),
  CONSTRAINT `fk_biblioteca__pessoa_pessoa_fisica1`
    FOREIGN KEY (`pes_id`)
    REFERENCES `pessoa_fisica` (`pes_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_biblioteca__pessoa_biblioteca__grupo_leitor1`
    FOREIGN KEY (`grupo_leitor_id`)
    REFERENCES `biblioteca__grupo_leitor` (`grupo_leitor_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
