-- MySQL Workbench Synchronization
-- Generated: 2016-01-25 11:30
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `biblioteca__acervo_idioma` 
CHANGE COLUMN `acervo_idioma_id` `titulo_idioma_id` INT(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT , RENAME TO  `biblioteca__titulo_idioma` ;

ALTER TABLE `biblioteca__acervo_autor` 
CHANGE COLUMN `acervo_autor_tipo` `titulo_autor_tipo` ENUM('Autor principal', 'Colaborador', 'Tradutor', 'Orientador') NOT NULL AFTER `titulo_autor_id`,
CHANGE COLUMN `acervo_autor_id` `titulo_autor_id` INT(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT , RENAME TO  `biblioteca__titulo_autor` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
