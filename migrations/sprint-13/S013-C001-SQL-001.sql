-- MySQL Workbench Synchronization
-- Generated: 2016-01-15 18:06
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: breno

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `acesso_actions_filhas`
DROP FOREIGN KEY `fk_acesso_actions_has_acesso_actions_acesso_actions1`,
DROP FOREIGN KEY `fk_acesso_actions_has_acesso_actions_acesso_actions2`;

ALTER TABLE `acesso_privilegios_grupo`
DROP FOREIGN KEY `fk_acesso_actions_has_acesso_grupo_acesso_actions1`,
DROP FOREIGN KEY `fk_acesso_actions_has_acesso_grupo_acesso_grupo1`;

ALTER TABLE `acesso_usuarios_privilegios`
DROP FOREIGN KEY `fk_acesso_usuarios_has_acesso_actions_acesso_actions1`,
DROP FOREIGN KEY `fk_acesso_usuarios_has_acesso_actions_acesso_usuarios1`;

ALTER TABLE `acesso_modulos`
CHANGE COLUMN `mod_prioridade` `mod_prioridade` INT(11) NULL DEFAULT NULL AFTER `mod_info`,
CHANGE COLUMN `mod_redireciona` `mod_redireciona` VARCHAR(45) NULL DEFAULT NULL ;

ALTER TABLE `acesso_actions_filhas`
ADD CONSTRAINT `fk_acesso_actions_has_acesso_actions_acesso_actions1`
  FOREIGN KEY (`pai`)
  REFERENCES `acesso_actions` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_acesso_actions_has_acesso_actions_acesso_actions2`
  FOREIGN KEY (`filha`)
  REFERENCES `acesso_actions` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `acesso_privilegios_grupo`
ADD CONSTRAINT `fk_acesso_actions_has_acesso_grupo_acesso_actions1`
  FOREIGN KEY (`acesso_actions_id`)
  REFERENCES `acesso_actions` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_acesso_actions_has_acesso_grupo_acesso_grupo1`
  FOREIGN KEY (`acesso_grupo_id`)
  REFERENCES `acesso_grupo` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `acesso_usuarios_privilegios`
ADD CONSTRAINT `fk_acesso_usuarios_has_acesso_actions_acesso_actions1`
  FOREIGN KEY (`acesso_actions_id`)
  REFERENCES `acesso_actions` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_acesso_usuarios_has_acesso_actions_acesso_usuarios1`
  FOREIGN KEY (`acesso_usuarios_id`)
  REFERENCES `acesso_usuarios` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
