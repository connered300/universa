-- MySQL Workbench Synchronization
-- Generated: 2016-01-25 17:38
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `biblioteca__titulo` DROP FOREIGN KEY `fk_biblioteca__titulo_biblioteca__colecao1`;
ALTER TABLE `biblioteca__titulo` CHANGE COLUMN `colecao_id` `colecao_id` INT(10) UNSIGNED ZEROFILL NULL ;
ALTER TABLE `biblioteca__titulo` 
ADD CONSTRAINT `fk_biblioteca__titulo_biblioteca__colecao1`
  FOREIGN KEY (`colecao_id`)
  REFERENCES `biblioteca__colecao` (`colecao_id`)
  ON UPDATE CASCADE;

ALTER TABLE `biblioteca__exemplar` CHANGE COLUMN `exemplar_data_cadastro` `exemplar_data_cadastro` DATETIME NOT NULL ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
