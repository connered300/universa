SET FOREIGN_KEY_CHECKS = 0;
SET SQL_SAFE_UPDATES=0;

ALTER TABLE `financeiro__titulo`
ADD COLUMN `tituloconf_id` INT NOT NULL,
ADD FOREIGN KEY (`tituloconf_id`)
REFERENCES `financeiro__titulo_config` (`tituloconf_id`);

UPDATE
    financeiro__titulo AS ft,
    (
      SELECT
        t.tituloconf_id AS tituloconfId,
        t.titulo_id     AS tituloId
      FROM (
             SELECT
               t.titulo_id,
               tc.tituloconf_id
             FROM financeiro__titulo t
               LEFT JOIN acadgeral__aluno_curso ac ON ac.alunocurso_id = t.alunocurso_id
               LEFT JOIN view__financeiro_configuracao_titulos AS tc ON (
                 (tc.cursocampus_id = ac.cursocampus_id OR
                  tc.cursocampus_id IS NULL)
                 AND
                 (tc.tipotitulo_id = t.tipotitulo_id OR
                  tc.tipotitulo_id IS NULL)
                 )
             ORDER BY
               titulo_data_vencimento DESC,
               titulo_data_processamento DESC,
               titulo_data_pagamento DESC,
               t.alunocurso_id ASC,
               prioridade_por_curso ASC,
               prioridade_por_tipo ASC,
               prioridade_por_periodo ASC,
               prioridade_por_inicio ASC,
               prioridade_por_fim ASC,
               tituloconf_id ASC
           ) t
      GROUP BY titulo_id
    ) AS t
SET ft.tituloconf_id = t.tituloconfId
WHERE ft.titulo_id = t.tituloId;

SET FOREIGN_KEY_CHECKS = 1;
SET SQL_SAFE_UPDATES=1;