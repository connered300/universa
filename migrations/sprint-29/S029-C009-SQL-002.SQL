ALTER TABLE `acad_curso`
CHANGE COLUMN `curso_periodicidade` `curso_periodicidade` ENUM('semestral', 'anual', 'Quadrimestral', 'Trimestral', 'Nao Aplicado') NOT NULL COMMENT 'É o período em que o curso forma novas turmas' ,
ADD COLUMN `curso_integralizacao_medida` ENUM('Mensal', 'Bimestral', 'Trimestral', 'Quadrimestral', 'Semestral', 'Anual') NOT NULL AFTER `curso_situacao`;
