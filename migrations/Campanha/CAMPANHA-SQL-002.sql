--
-- módulo Comunicacao
--
DELETE FROM `acesso_privilegios_grupo`
WHERE acesso_actions_id IN (
  SELECT acesso_actions.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
  WHERE func_controller IN (
    'Comunicacao\Controller\ComunicacaoCampanha',
    'Comunicacao\Controller\ComunicacaoCampanhaEstatistica',
    'Comunicacao\Controller\ComunicacaoCampanhaRegra',
    'Comunicacao\\Controller\\ComunicacaoCampanha',
    'Comunicacao\\Controller\\ComunicacaoCampanhaEstatistica',
    'Comunicacao\\Controller\\ComunicacaoCampanhaRegra'
  )
) AND acesso_actions_id >= 0 AND acesso_grupo_id >= 0;

DELETE FROM `acesso_actions`
WHERE funcionalidade IN (
  SELECT acesso_funcionalidades.id
  FROM acesso_funcionalidades
  WHERE func_controller IN (
    'Comunicacao\Controller\ComunicacaoCampanha',
    'Comunicacao\Controller\ComunicacaoCampanhaEstatistica',
    'Comunicacao\Controller\ComunicacaoCampanhaRegra',
    'Comunicacao\\Controller\\ComunicacaoCampanha',
    'Comunicacao\\Controller\\ComunicacaoCampanhaEstatistica',
    'Comunicacao\\Controller\\ComunicacaoCampanhaRegra'
  )
) AND id > 0;

DELETE FROM acesso_funcionalidades
WHERE func_controller IN (
  'Comunicacao\Controller\ComunicacaoCampanha',
  'Comunicacao\Controller\ComunicacaoCampanhaEstatistica',
  'Comunicacao\Controller\ComunicacaoCampanhaRegra',
  'Comunicacao\\Controller\\ComunicacaoCampanha',
  'Comunicacao\\Controller\\ComunicacaoCampanhaEstatistica',
  'Comunicacao\\Controller\\ComunicacaoCampanhaRegra'
) AND id > 0;

DELETE FROM acesso_modulos
WHERE mod_route = 'comunicacao/default' AND id > 0;

DELETE FROM acesso_sistemas
WHERE sis_nome = 'Comunicacao' AND id > 0;

REPLACE INTO `acesso_sistemas` (`sis_nome`, `sis_status`, `sis_icon`, `sis_info`)
VALUES ('Comunicacao', 'Ativo', 'fa-group', 'Comunicacao');

REPLACE INTO `acesso_modulos` (`sistema`, `mod_nome`, `mod_route`, `mod_status`, `mod_icon`, `mod_info`, `mod_redireciona`, `mod_prioridade`)
  SELECT
    acesso_sistemas.id,
    'Comunicacao',
    'comunicacao/default',
    'Ativo',
    'fa fa-group',
    'Módulo Comunicacao',
    '/comunicacao',
    round(acesso_sistemas.id / 10)
  FROM acesso_sistemas
  WHERE sis_nome LIKE 'Comunicacao';

REPLACE INTO `acesso_funcionalidades` (modulo, func_nome, func_controller, func_status, func_visivel)
  SELECT DISTINCT
    acesso_modulos.id,
    func.descricao,
    func.controller,
    'Ativa',
    'Sim'
  FROM acesso_modulos,
    (
      SELECT
        'Gestão de campanha'                           AS descricao,
        'Comunicacao\\Controller\\ComunicacaoCampanha' AS controller
      FROM dual
      UNION ALL
      SELECT
        'Gestão de campanha estatistica'                          AS descricao,
        'Comunicacao\\Controller\\ComunicacaoCampanhaEstatistica' AS controller
      FROM dual
      UNION ALL
      SELECT
        'Gestão de campanha regra'                          AS descricao,
        'Comunicacao\\Controller\\ComunicacaoCampanhaRegra' AS controller
      FROM dual
    ) AS func
  WHERE mod_nome LIKE 'Comunicacao';

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT DISTINCT
    acesso_funcionalidades.id,
    actions.act,
    actions.descricao,
    actions.tipo,
    actions.usr
  FROM acesso_funcionalidades,
    (
      SELECT
        'index'    AS act,
        'Listagem' AS descricao,
        'Leitura'  AS tipo,
        'N'        AS usr
      FROM dual
      UNION ALL
      SELECT
        'remove'  AS act,
        'Remover' AS descricao,
        'Leitura' AS tipo,
        'N'       AS usr
      FROM dual
      UNION ALL
      SELECT
        'add'     AS act,
        'Criar'   AS descricao,
        'Leitura' AS tipo,
        'N'       AS usr
      FROM dual
      UNION ALL
      SELECT
        'edit'    AS act,
        'Editar'  AS descricao,
        'Leitura' AS tipo,
        'N'       AS usr
      FROM dual
      UNION ALL
      SELECT
        'search-for-json' AS act,
        'Autocomplete'    AS descricao,
        'Leitura'         AS tipo,
        'N'               AS usr
      FROM dual
      UNION ALL
      SELECT
        'search'     AS act,
        'Datatables' AS descricao,
        'Leitura'    AS tipo,
        'N'          AS usr
      FROM dual
    ) AS actions
  WHERE func_controller IN (
    'Comunicacao\\Controller\\ComunicacaoCampanha',
    'Comunicacao\\Controller\\ComunicacaoCampanhaEstatistica',
    'Comunicacao\\Controller\\ComunicacaoCampanhaRegra'
  );
REPLACE INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
    ,
    acesso_grupo
  WHERE func_controller IN (
    'Comunicacao\\Controller\\ComunicacaoCampanha',
    'Comunicacao\\Controller\\ComunicacaoCampanhaEstatistica',
    'Comunicacao\\Controller\\ComunicacaoCampanhaRegra'
  )
        AND grup_nome LIKE 'admin';