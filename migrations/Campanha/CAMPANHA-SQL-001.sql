#DROP TABLE IF EXISTS `campanha__estatistica`;
#DROP TABLE IF EXISTS `campanha__regra`;
#DROP TABLE IF EXISTS `campanha`;

DROP TABLE IF EXISTS `comunicacao__campanha_estatistica`;
DROP TABLE IF EXISTS `comunicacao__campanha_regra`;
DROP TABLE IF EXISTS `comunicacao__campanha`;

CREATE TABLE IF NOT EXISTS `comunicacao__campanha` (
  `campanha_id`                INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `campanha_usuario_cadastro`  INT(11) UNSIGNED ZEROFILL             NOT NULL,
  `campanha_usuario_alteracao` INT(11) UNSIGNED ZEROFILL             NOT NULL,
  `campanha_arquivo`           INT(10) UNSIGNED ZEROFILL             NULL DEFAULT NULL,
  `campanha_nome`              VARCHAR(255)                          NOT NULL,
  `campanha_descricao`         TEXT                                  NULL DEFAULT NULL,
  `campanha_link`              TEXT                                  NULL DEFAULT NULL,
  `campanha_alvo`              SET('Agente', 'Alunos', 'Operadores') NULL DEFAULT NULL,
  `campanha_data_alteracao`    DATETIME                              NOT NULL,
  `campanha_data_cadastro`     DATETIME                              NOT NULL,
  `campanha_situacao`          ENUM('Ativa', 'Inativa')              NULL DEFAULT NULL,
  PRIMARY KEY (`campanha_id`),
  INDEX `fk_campanha__acesso_pessoas1_idx` (`campanha_usuario_alteracao` ASC),
  CONSTRAINT `fk_campanha__acesso_pessoas1`
  FOREIGN KEY (`campanha_usuario_alteracao`)
  REFERENCES `acesso_pessoas` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  INDEX `fk_campanha__acesso_pessoas2_idx` (`campanha_usuario_cadastro` ASC),
  CONSTRAINT `fk_campanha__acesso_pessoas2`
  FOREIGN KEY (`campanha_usuario_cadastro`)
  REFERENCES `acesso_pessoas` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  INDEX `fk_campanha__arquivo_idx` (`campanha_arquivo` ASC),
  CONSTRAINT `fk_campanha__arquivo`
  FOREIGN KEY (`campanha_arquivo`)
  REFERENCES `arquivo` (`arq_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `comunicacao__campanha_regra` (
  `regra_id`    INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `campanha_id` INT(11) UNSIGNED                                         NOT NULL,
  `regra_chave` ENUM('Nível', 'Modalidade', 'Curso', 'Cidade', 'Estado') NULL DEFAULT NULL,
  `regra_valor` TEXT                                                     NOT NULL,
  PRIMARY KEY (`regra_id`),
  INDEX `fk_comunicacao__campanha_regra_campanha_idx` (`campanha_id` ASC),
  CONSTRAINT `fk_comunicacao__campanha_regra_campanha`
  FOREIGN KEY (`campanha_id`)
  REFERENCES `comunicacao__campanha` (`campanha_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `comunicacao__campanha_estatistica` (
  `estatistica_id`   INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `campanha_id`      INT(11) UNSIGNED                            NOT NULL,
  `usuario_id`       INT(11) UNSIGNED                            NULL,
  `aluno_id`         INT(11) UNSIGNED                            NULL,
  `curso_id`         INT(11) UNSIGNED                            NULL,
  `estatistica_data` DATETIME                                    NOT NULL,
  `estatistica_alvo` ENUM('Agente', 'Alunos', 'Operadores')      NULL,
  `estatistica_acao` ENUM('Visualização', 'Clique', 'Conversão') NULL,
  PRIMARY KEY (`estatistica_id`),

  INDEX `fk_comunicacao__campanha_estatistica_campanha_idx` (`campanha_id` ASC),
  CONSTRAINT `fk_comunicacao__campanha_estatistica_campanha`
  FOREIGN KEY (`campanha_id`)
  REFERENCES `comunicacao__campanha` (`campanha_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  INDEX `fk_comunicacao__campanha_estatistica_acad_curso_idx` (`curso_id` ASC),
  CONSTRAINT `fk_comunicacao__campanha_estatistica_acad_curso`
  FOREIGN KEY (`curso_id`)
  REFERENCES `acad_curso` (`curso_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  INDEX `fk_comunicacao__campanha_estatistica_acesso_pessoas_idx` (`usuario_id` ASC),
  CONSTRAINT `fk_comunicacao__campanha_estatistica_acesso_pessoas`
  FOREIGN KEY (`usuario_id`)
  REFERENCES `acesso_pessoas` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  INDEX `fk_comunicacao__campanha_estatistica_acadgeral__aluno_idx` (`usuario_id` ASC),
  CONSTRAINT `fk_comunicacao__campanha_estatistica_acadgeral__aluno`
  FOREIGN KEY (`aluno_id`)
  REFERENCES `acadgeral__aluno` (`aluno_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_general_ci;