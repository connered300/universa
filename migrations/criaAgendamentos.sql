SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


CREATE TABLE IF NOT EXISTS acadgeral__agendamento_tipo (
  agenda_nome VARCHAR(45) NOT NULL,
  agenda_descricao VARCHAR(255) NOT NULL,
  PRIMARY KEY (agenda_nome))
  ENGINE = InnoDB
  COMMENT = 'Informações sobre o tipo de agendamento da atividade.';

CREATE TABLE IF NOT EXISTS acadperiodo__agendamento (
  agenda_id INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  agenda_nome VARCHAR(45) NOT NULL,
  agenda_titulo VARCHAR(45) NOT NULL COMMENT 'Titulo do agendamento da atividade.\nEx: Prova, Traballho.',
  agenda_descricao TEXT NULL COMMENT 'Descrição detalhada do agendamento da atividade.',
  agenda_nota FLOAT NULL COMMENT 'Valor total do agendamento da atividade.',
  agenda_data_inicio DATETIME NOT NULL COMMENT 'Publicação do agendamento da atividade.',
  agenda_data_fim DATETIME NOT NULL COMMENT 'Data máxima de aceitabilidade do agendamento da atividade.',
  agenda_avaliativa ENUM('Sim', 'Não') NOT NULL,
  agenda_data_cadastro DATETIME NULL,
  agenda_entrega_tipo ENUM('Presencial', 'Online') NOT NULL,
  agenda_anexos ENUM('Sim', 'Não') NOT NULL,
  PRIMARY KEY (agenda_id),
  INDEX fk_acadperiodo__agendamento_acadgeral__agendamento_tipo1_idx (agenda_nome ASC),
  CONSTRAINT fk_acadperiodo__agendamento_acadgeral__agendamento_tipo1
  FOREIGN KEY (agenda_nome)
  REFERENCES acadgeral__agendamento_tipo (agenda_nome)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
  ENGINE = InnoDB
  COMMENT = 'Informações de agendamento de atividades acadêmicas.';

CREATE TABLE IF NOT EXISTS acadperiodo__etapa_diario (
  diario_id INT ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  docdisc_id INT ZEROFILL NOT NULL,
  etapa_id INT ZEROFILL UNSIGNED NOT NULL,
  diario_data_envio DATETIME NULL COMMENT 'Envio da primeira remessa notas do periodo com listagem dos aprovados.',
  diario_data_envio_final DATETIME NULL COMMENT 'Envio da ultima remessa de nota com listagem do aprovados que ficaram de final.',
  PRIMARY KEY (diario_id),
  INDEX fk_acadperiodo__encerramento_diario_acadperiodo__etapas1_idx (etapa_id ASC),
  INDEX fk_acadperiodo__encerramento_diario_acadperiodo__docente_di_idx (docdisc_id ASC),
  CONSTRAINT acadperiodo__etapa_diario_ibfk_1
  FOREIGN KEY (docdisc_id)
  REFERENCES acadperiodo__docente_disciplina (docdisc_id)
    ON UPDATE CASCADE,
  CONSTRAINT acadperiodo__etapa_diario_ibfk_2
  FOREIGN KEY (etapa_id)
  REFERENCES acadperiodo__etapas (etapa_id)
    ON UPDATE CASCADE)
  ENGINE = InnoDB
  COMMENT = 'Informações do diario academico de um docente em uma deter' /* comment truncated */ /*mi*/

CREATE TABLE IF NOT EXISTS acadperiodo__agendamento_diario (
  agendadiario_id INT NOT NULL AUTO_INCREMENT,
  agenda_id INT UNSIGNED ZEROFILL NOT NULL,
  diario_id INT ZEROFILL UNSIGNED NOT NULL,
  PRIMARY KEY (agendadiario_id),
  INDEX fk_acadperiodo__docente_disciplina_acadperiodo__agendamento__idx (agenda_id ASC),
  UNIQUE INDEX agenda_id_UNIQUE (agenda_id ASC),
  UNIQUE INDEX agendadocente_id_UNIQUE (agendadiario_id ASC),
  INDEX fk_acadperiodo__agend_docente_etapa_diario_idx (diario_id ASC),
  CONSTRAINT fk_acadperiodo__docente_disciplina_acadperiodo__agendamento_ac2
  FOREIGN KEY (agenda_id)
  REFERENCES acadperiodo__agendamento (agenda_id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT fk_acadperiodo__agendamento_docente_acadperiodo__etapa_diario1
  FOREIGN KEY (diario_id)
  REFERENCES acadperiodo__etapa_diario (diario_id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
  ENGINE = InnoDB

CREATE TABLE IF NOT EXISTS acadperiodo__agendamento_aluno (
  agendaaluno_id INT UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  agendaaluno_nota FLOAT NULL DEFAULT NULL,
  agendaaluno_conceito VARCHAR(45) NULL,
  agendaaluno_carga_horaria INT NULL,
  agendaaluno_data_entrega DATETIME NULL,
  agendaaluno_origem_registro ENUM('WebService', 'Sistema') NULL DEFAULT 'Sistema',
  agendaaluno_estado ENUM('Pendente', 'Enviado', 'Avaliado') NULL DEFAULT 'Pendente',
  usuario_avaliador INT(11) UNSIGNED ZEROFILL NOT NULL,
  agendaaluno_data_avaliacao DATETIME NULL,
  agendaaluno_observacao VARCHAR(250) NULL,
  PRIMARY KEY (agendaaluno_id),
  INDEX fk_acadperiodo__agendamento_aluno_acesso_pessoas1_idx (usuario_avaliador ASC),
  CONSTRAINT fk_acadperiodo__agendamento_aluno_acesso_pessoas1
  FOREIGN KEY (usuario_avaliador)
  REFERENCES acesso_pessoas (id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
  ENGINE = InnoDB
  COMMENT = 'Informações de nota do aluno em determinada agendamento.'

CREATE TABLE IF NOT EXISTS acadperiodo__agendamento_diario_aluno (
  agendadiario_id INT NOT NULL,
  agendaaluno_id INT UNSIGNED ZEROFILL NOT NULL,
  alunodisc_id INT UNSIGNED ZEROFILL NULL,
  PRIMARY KEY (agendadiario_id, agendaaluno_id),
  INDEX fk_acadperiodo__agendamento_diario_acadperiodo__agen_alu_idx (agendaaluno_id ASC),
  INDEX fk_acadperiodo__agendamento_diario_acadperiodo__agen_alu_idx1 (agendadiario_id ASC),
  INDEX fk_acadperiodo__agendamento_diario_aluno_acadper__aluno_d_idx (alunodisc_id ASC),
  CONSTRAINT fk_acadperiodo__agendamento_diario_acadper__agen_aluno1
  FOREIGN KEY (agendadiario_id)
  REFERENCES acadperiodo__agendamento_diario (agendadiario_id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT fk_acadperiodo__agendamento_diario_acadper__agen_aluno2
  FOREIGN KEY (agendaaluno_id)
  REFERENCES acadperiodo__agendamento_aluno (agendaaluno_id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT fk_acadperiodo__agendamento_diario_aluno_acadperiodo__aluno_dis1
  FOREIGN KEY (alunodisc_id)
  REFERENCES acadperiodo__aluno_disciplina (alunodisc_id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
  ENGINE = InnoDB
  
REPLACE INTO acadgeral__agendamento_tipo(agenda_nome, agenda_descricao) VALUES ("Atividade em Sala", "Atividade em Sala");
REPLACE INTO acadgeral__agendamento_tipo(agenda_nome, agenda_descricao) VALUES ("Avaliação", "Avaliação");

SELECT @sis:=id from acesso_sistemas where sis_nome like 'Academico%';
REPLACE INTO acesso_modulos VALUES (null, @sis, "Diario", "diario/default", "Ativo", null, null, null, null);
SELECT @mod:=id from acesso_modulos where mod_nome like 'Diario%';

REPLACE INTO acesso_funcionalidades VALUES (null, @mod, 'Agendamento', 'Diario\\Controller\\Agendamento', '', null, 'Ativa', 'Sim');
REPLACE INTO acesso_actions (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
    (
      select 'index' as act,'Listagem' as descricao,'Leitura' as tipo,'N' as usr from dual union all
      select 'searchForJson' as act,'Listagem' as descricao,'Leitura' as tipo,'N' as usr from dual union all
      select 'add' as act,'Adicionar Agendamento' as descricao,'Leitura' as tipo,'N' as usr from dual union all
      select 'edit' as act,'Editar Agendamento' as descricao,'Leitura' as tipo,'N' as usr from dual union all
      select 'remove' as act,'Remover Agendamento' as descricao,'Leitura' as tipo,'N' as usr from dual union all
      select 'search' as act,'Datatables' as descricao,'Leitura' as tipo,'N' as usr from dual
    ) as actions
  where func_controller IN(
    'Diario\\Controller\\Agendamento'
  );
REPLACE INTO acesso_privilegios_grupo (acesso_actions_id, acesso_grupo_id)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id,
    acesso_grupo
  WHERE func_controller IN (
    'Diario\\Controller\\Agendamento'
  ) AND grup_nome LIKE 'Admin%';


REPLACE INTO acesso_funcionalidades VALUES (null, @mod, 'Agendamento', 'Diario\\Controller\\AgendamentoDiarioAluno', '', null, 'Ativa', 'Sim');
REPLACE INTO acesso_actions (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
    (
      select 'add' as act,'Gestão de Notas do Aluno no Agendamento' as descricao,'Leitura' as tipo,'N' as usr from dual union all
      select 'search' as act,'Datatables' as descricao,'Leitura' as tipo,'N' as usr from dual
    ) as actions
  where func_controller IN(
    'Diario\\Controller\\AgendamentoDiarioAluno'
  );
REPLACE INTO acesso_privilegios_grupo (acesso_actions_id, acesso_grupo_id)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id,
    acesso_grupo
  WHERE func_controller IN (
    'Diario\\Controller\\AgendamentoDiarioAluno'
  ) AND grup_nome LIKE 'Admin%';

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
