SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Criando um bkp para a tabela para executar o script de atualização, pois as relações foram mudadas
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS acadgeral__disciplina_requisito_bkp (
  discreq_id      INT(11) ZEROFILL                      NOT NULL,
  discreq_origem  INT(10) UNSIGNED ZEROFILL             NOT NULL,
  discreq_destino INT(10) UNSIGNED ZEROFILL             NOT NULL,
  discreq_tipo    ENUM('Pre-Requisito', 'Co-Requisito') NOT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS acadgeral__disciplina_ementa_bkp (
  ementa_id     INT(10) UNSIGNED ZEROFILL NOT NULL,
  arq_id        INT(10) UNSIGNED ZEROFILL NOT NULL,
  disc_curso_id INT(10) UNSIGNED ZEROFILL NOT NULL,
  ementa_hist   INT(10) UNSIGNED ZEROFILL NULL DEFAULT NULL,
  ementa_data   DATE                      NOT NULL
  COMMENT 'Data de publicacao da ementa.'
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS acadgeral__disciplina_curso_bkp (
  disc_curso_id         INT(10) UNSIGNED ZEROFILL NOT NULL,
  curso_id              INT(11) ZEROFILL          NOT NULL
  COMMENT 'Identificador do curso.',
  disc_id               INT(10) UNSIGNED ZEROFILL NOT NULL
  COMMENT 'Identificador da disciplina',
  tdisc_id              INT(10) UNSIGNED ZEROFILL NOT NULL,
  disc_ativo            ENUM('Sim', 'Não')        NOT NULL DEFAULT 'Sim'
  COMMENT 'Informações sobre a disciplina, que podera ser usados quando a disciplina no poder ser vinculado a alguma curso.',
  disc_curs_sug_periodo INT(11)                   NULL     DEFAULT NULL
  COMMENT 'Sugere ao curso o periodo que esta disciplina sera aplicada.',
  disc__nucleo_comum    ENUM('Sim', 'Nao')        NOT NULL DEFAULT 'Sim'
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS acadgeral__disciplina_equivalencia_bkp (
  discequiv_id      INT(10) UNSIGNED ZEROFILL NOT NULL,
  discequiv_origem  INT(10) UNSIGNED ZEROFILL NOT NULL
  COMMENT 'Disciplina origem da equivalencia.',
  discequiv_destino INT(10) UNSIGNED ZEROFILL NOT NULL
  COMMENT 'Disciplina destino da equivalencia.'
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;


-- ---------
-- migração dos dados para a tabela de bkp
-- ---------

REPLACE INTO acadgeral__disciplina_curso_bkp (disc_id, disc_curso_id, curso_id, tdisc_id, disc_curs_sug_periodo, disc__nucleo_comum) (
  SELECT
    d.disc_id,
    dc.disc_curso_id,
    dc.curso_id,
    d.tdisc_id,
    dc.disc_curs_sug_periodo,
    dc.disc__nucleo_comum
  FROM acadgeral__disciplina_curso dc
    INNER JOIN acadgeral__disciplina d USING (disc_id)
);

REPLACE INTO acadgeral__disciplina_ementa_bkp (ementa_id, disc_curso_id, arq_id, ementa_hist, ementa_data) (
  SELECT
    ementa_id,
    disc_curso_id,
    arq_id,
    ementa_hist,
    ementa_data
  FROM acadgeral__disciplina_ementa
    INNER JOIN acadgeral__disciplina_curso USING (disc_id)
);

REPLACE INTO acadgeral__disciplina_requisito_bkp (discreq_id, discreq_origem, discreq_destino) (
  SELECT
    requisito.discreq_id,
    origem.disc_curso_id  AS discreq_origem,
    destino.disc_curso_id AS discreq_destino
  FROM acadgeral__disciplina_requisito requisito
    INNER JOIN acadgeral__disciplina_curso origem ON (requisito.discreq_origem = origem.disc_id)
    INNER JOIN acadgeral__disciplina_curso destino ON (requisito.discreq_destino = destino.disc_id)
);

REPLACE INTO acadgeral__disciplina_equivalencia (discequiv_id, discequiv_origem, discequiv_destino) (
  SELECT
    equi.discequiv_id,
    origem.disc_curso_id  AS discreq_origem,
    destino.disc_curso_id AS discreq_destino
  FROM acadgeral__disciplina_equivalencia equi
    INNER JOIN acadgeral__disciplina_curso origem ON (equi.discequiv_origem = origem.disc_id)
    INNER JOIN acadgeral__disciplina_curso destino ON (equi.discequiv_destino = destino.disc_id)
);

-- ---------
-- Alterando as chaves estrangeiras para não der problema ao fazer a migração dos dados
-- ---------


ALTER TABLE acadgeral__disciplina DROP FOREIGN KEY fk_acadgeral__disciplina_acadgeral__disciplina_tipo;
ALTER TABLE acadgeral__disciplina DROP INDEX fk_acadgeral__disciplina_acadgeral__disciplina_tipo1_idx;


ALTER TABLE acadgeral__disciplina ADD COLUMN docente_id INT(10) UNSIGNED ZEROFILL NULL
AFTER disc_sigla;
ALTER TABLE acadgeral__disciplina ADD INDEX fk_acadgeral__disciplina_acadgeral__docente1_idx (docente_id ASC);
ALTER TABLE acadgeral__disciplina ADD CONSTRAINT fk_acadgeral__disciplina_acadgeral__docente1
FOREIGN KEY (docente_id)
REFERENCES acadgeral__docente (docente_id)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;


ALTER TABLE acadgeral__disciplina_curso DROP INDEX index4;
ALTER TABLE acadgeral__disciplina_curso DROP INDEX fk_disciplina_sugestao_curso_acad_curso1_idx;


ALTER TABLE acadgeral__disciplina_curso ADD COLUMN tdisc_id INT(10) UNSIGNED ZEROFILL NULL
AFTER curso_id;
ALTER TABLE acadgeral__disciplina_curso ADD COLUMN disc_ativo ENUM('Sim', 'Nao') NOT NULL DEFAULT 'Sim'
AFTER disc__nucleo_comum;
ALTER TABLE acadgeral__disciplina_curso ADD UNIQUE INDEX index4 (disc_id ASC);
ALTER TABLE acadgeral__disciplina_curso ADD INDEX fk_acadgeral__disciplina_curso_acadgeral__disciplina_tipo1_idx (tdisc_id ASC);
ALTER TABLE acadgeral__disciplina_curso ADD INDEX fk_acadgeral__disciplina_curso_acad_curso1_idx (curso_id ASC);
ALTER TABLE acadgeral__disciplina_curso ADD CONSTRAINT fk_disciplina_sugestao_curso_disciplina1
FOREIGN KEY (disc_id)
REFERENCES acadgeral__disciplina (disc_id)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;
ALTER TABLE acadgeral__disciplina_curso ADD CONSTRAINT fk_acadgeral__disciplina_curso_acadgeral__disciplina_tipo1
FOREIGN KEY (tdisc_id)
REFERENCES acadgeral__disciplina_tipo (tdisc_id)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;
ALTER TABLE acadgeral__disciplina_curso ADD CONSTRAINT fk_acadgeral__disciplina_curso_acad_curso1
FOREIGN KEY (curso_id)
REFERENCES acad_curso (curso_id)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;


ALTER TABLE acadgeral__disciplina_requisito DROP INDEX fk_acadgeral__disciplina_requisito_acadgeral__disciplina1_idx;
ALTER TABLE acadgeral__disciplina_requisito DROP INDEX fk_acadgeral__disciplina_requisito_acadgeral__disciplina2_idx;


ALTER TABLE acadgeral__disciplina_requisito CHANGE COLUMN discreq_origem discreq_origem_old INT(10) UNSIGNED ZEROFILL NULL;
ALTER TABLE acadgeral__disciplina_requisito CHANGE COLUMN discreq_destino discreq_destino_old INT(10) UNSIGNED ZEROFILL NULL;
ALTER TABLE acadgeral__disciplina_requisito ADD COLUMN discreq_origem INT(10) UNSIGNED ZEROFILL NULL
AFTER discreq_tipo;
ALTER TABLE acadgeral__disciplina_requisito ADD COLUMN discreq_destino INT(10) UNSIGNED ZEROFILL NULL
AFTER discreq_origem;
ALTER TABLE acadgeral__disciplina_requisito ADD INDEX fk_acadgeral__disciplina_requisito_acadgeral__disc_curso_idx (discreq_origem ASC);
ALTER TABLE acadgeral__disciplina_requisito ADD INDEX fk_acadgeral__disciplina_requisito_acadgeral__disc_curso_idx1 (discreq_destino ASC);
ALTER TABLE acadgeral__disciplina_requisito ADD CONSTRAINT fk_acadgeral__disciplina_requisito_acadgeral__disciplina_curso1
FOREIGN KEY (discreq_origem)
REFERENCES acadgeral__disciplina_curso (disc_curso_id)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;
ALTER TABLE acadgeral__disciplina_requisito ADD CONSTRAINT fk_acadgeral__disciplina_requisito_acadgeral__disciplina_curso2
FOREIGN KEY (discreq_destino)
REFERENCES acadgeral__disciplina_curso (disc_curso_id)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

CREATE TABLE IF NOT EXISTS acadgeral__disciplina_equivalencia (
  discequiv_id      INT(10) UNSIGNED ZEROFILL NULL,
  discequiv_origem  INT(10) UNSIGNED ZEROFILL NULL
  COMMENT 'Disciplina origem da equivalencia.',
  discequiv_destino INT(10) UNSIGNED ZEROFILL NULL
  COMMENT 'Disciplina destino da equivalencia.'
);


ALTER TABLE acadgeral__disciplina_equivalencia DROP INDEX fk_disciplina_disciplina_disciplina3_idx;
ALTER TABLE acadgeral__disciplina_equivalencia DROP INDEX fk_disciplina_disciplina_disciplina4_idx;
ALTER TABLE acadgeral__disciplina_equivalencia DROP INDEX index4;


ALTER TABLE acadgeral__disciplina_equivalencia CHANGE COLUMN discequiv_origem discequiv_origem_old INT(10) UNSIGNED ZEROFILL NULL;

ALTER TABLE acadgeral__disciplina_equivalencia CHANGE COLUMN discequiv_destino discequiv_destino_old INT(10) UNSIGNED ZEROFILL NULL;

ALTER TABLE acadgeral__disciplina_equivalencia ADD COLUMN discequiv_origem INT(10) UNSIGNED ZEROFILL NULL;

ALTER TABLE acadgeral__disciplina_equivalencia ADD COLUMN discequiv_destino INT(10) UNSIGNED ZEROFILL NULL
AFTER discequiv_origem;

ALTER TABLE acadgeral__disciplina_equivalencia ADD INDEX fk_acadgeral__disciplina_curso_acadgeral__disciplina_curso__idx (discequiv_destino ASC);

ALTER TABLE acadgeral__disciplina_equivalencia ADD INDEX fk_acadgeral__disciplina_curso_acadgeral__disciplina_curso__idx1 (discequiv_origem ASC);

ALTER TABLE acadgeral__disciplina_equivalencia ADD CONSTRAINT fk_acadgeral__disciplina_curso_acadgeral__disciplina_curso_ac1
FOREIGN KEY (discequiv_origem) REFERENCES acadgeral__disciplina_curso (disc_curso_id)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE acadgeral__disciplina_equivalencia ADD CONSTRAINT fk_acadgeral__disciplina_curso_acadgeral__disciplina_curso_ac2
FOREIGN KEY (discequiv_destino) REFERENCES acadgeral__disciplina_curso (disc_curso_id)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;


ALTER TABLE acadgeral__disciplina_ementa DROP INDEX fk_disciplina_ementa_disciplina1_idx;
ALTER TABLE acadgeral__disciplina_ementa DROP INDEX fk_disciplina_arquivo_arquivo1_idx;
ALTER TABLE acadgeral__disciplina_ementa DROP INDEX index4;


ALTER TABLE acadgeral__disciplina_ementa ADD COLUMN disc_curso_id INT(10) UNSIGNED ZEROFILL NULL
AFTER ementa_data;
ALTER TABLE acadgeral__disciplina_ementa CHANGE COLUMN arq_id arq_id INT(10) UNSIGNED ZEROFILL NULL;
ALTER TABLE acadgeral__disciplina_ementa ADD INDEX fk_acadgeral__disciplina_ementa_acadgeral__disciplina_curso_idx (disc_curso_id ASC);
ALTER TABLE acadgeral__disciplina_ementa ADD INDEX fk_acadgeral__disciplina_ementa_arquivo_idx (arq_id ASC);
ALTER TABLE acadgeral__disciplina_ementa ADD CONSTRAINT fk_disciplina_ementa_arquivo1
FOREIGN KEY (arq_id)
REFERENCES arquivo (arq_id)
  ON DELETE SET NULL
  ON UPDATE CASCADE;
ALTER TABLE acadgeral__disciplina_ementa ADD CONSTRAINT fk_disciplina_ementa_disciplina_ementa1
FOREIGN KEY (disc_curso_id)
REFERENCES acadgeral__disciplina_curso (disc_curso_id)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

-- -----------------------------------------------------
-- Script de migração das tabelas de bkp para as tabelas alteradas
-- -----------------------------------------------------
REPLACE INTO acadgeral__disciplina_curso (disc_id, disc_curso_id, curso_id, tdisc_id, disc_curs_sug_periodo, disc__nucleo_comum) (
  SELECT
    disc_id,
    disc_curso_id,
    curso_id,
    tdisc_id,
    disc_curs_sug_periodo,
    disc__nucleo_comum
  FROM acadgeral__disciplina_curso_bkp
);

REPLACE INTO acadgeral__disciplina_ementa (disc_curso_id, arq_id, ementa_hist, ementa_data) (
  SELECT
    disc_curso_id,
    arq_id,
    ementa_hist,
    ementa_data
  FROM acadgeral__disciplina_ementa_bkp);

REPLACE INTO acadgeral__disciplina_requisito (discreq_id, discreq_origem, discreq_destino, discreq_tipo) (
  SELECT *
  FROM acadgeral__disciplina_requisito_bkp
);

REPLACE INTO acadgeral__disciplina_equivalencia (discequiv_id, discequiv_origem, discequiv_destino) (
  SELECT *
  FROM acadgeral__disciplina_equivalencia_bkp
);

REPLACE INTO acadgeral__disciplina_tipo (tdisc_id, tdisc_descricao, tdisc_avaliacao)
VALUES (1, "Regular", "Notas e Horas");
REPLACE INTO acadgeral__disciplina_tipo (tdisc_id, tdisc_descricao, tdisc_avaliacao)
VALUES (2, "Optativa", "Notas e Horas");
REPLACE INTO acadgeral__disciplina_tipo (tdisc_id, tdisc_descricao, tdisc_avaliacao) VALUES (3, "Estágio", "Horas");
REPLACE INTO acadgeral__disciplina_tipo (tdisc_id, tdisc_descricao, tdisc_avaliacao)
VALUES (4, "Equivalência", "Notas e Horas");
REPLACE INTO acadgeral__disciplina_tipo (tdisc_id, tdisc_descricao, tdisc_avaliacao)
VALUES (5, "Base Comum", "Notas e Horas");

-- -----------
-- Removendo as tabelas de bkp
-- -----------

-- DROP TABLE IF EXISTS acadgeral__disciplina_requisito_bkp;
-- DROP TABLE IF EXISTS acadgeral__disciplina_ementa_bkp;
-- DROP TABLE IF EXISTS acadgeral__disciplina_equivalencia_bkp;
-- DROP TABLE IF EXISTS acadgeral__disciplina_curso_bkp;

-- -----------
-- Criando as chaves estrangeiras e primárias para as tabelas corretas
-- -----------

ALTER TABLE acadgeral__disciplina_curso
CHANGE COLUMN tdisc_id tdisc_id INT(10) UNSIGNED ZEROFILL NOT NULL;


ALTER TABLE acadgeral__disciplina_requisito  CHANGE COLUMN discreq_origem discreq_origem INT(10) UNSIGNED ZEROFILL NOT NULL
AFTER discreq_tipo;
ALTER TABLE acadgeral__disciplina_requisito CHANGE COLUMN discreq_destino discreq_destino INT(10) UNSIGNED ZEROFILL NOT NULL
AFTER discreq_origem;

ALTER TABLE acadgeral__disciplina_equivalencia
DROP PRIMARY KEY;


ALTER TABLE acadgeral__disciplina_equivalencia CHANGE COLUMN discequiv_origem discequiv_origem INT(10) UNSIGNED ZEROFILL NOT NULL;
ALTER TABLE acadgeral__disciplina_equivalencia CHANGE COLUMN discequiv_destino discequiv_destino INT(10) UNSIGNED ZEROFILL NOT NULL
AFTER discequiv_origem;
ALTER TABLE acadgeral__disciplina_equivalencia CHANGE COLUMN discequiv_id discequiv_id INT(10) UNSIGNED ZEROFILL NULL;
ALTER TABLE acadgeral__disciplina_equivalencia ADD PRIMARY KEY (discequiv_origem, discequiv_destino);


ALTER TABLE acadgeral__disciplina_ementa ADD COLUMN disc_curso_id INT(10) UNSIGNED ZEROFILL NOT NULL
AFTER ementa_data;
ALTER TABLE acadgeral__disciplina_ementa CHANGE COLUMN arq_id arq_id INT(10) UNSIGNED ZEROFILL NOT NULL;

SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Novas permissões
-- -----------------------------------------------------
SELECT @mod := id
FROM acesso_modulos
WHERE mod_nome LIKE 'Sistema%';

REPLACE INTO acesso_funcionalidades
VALUES (NULL, @mod, 'Disciplina Curso', 'Matricula\\Controller\\AcadgeralDisciplinaCurso', '', NULL, 'Ativa', 'Sim');
REPLACE INTO acesso_actions (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT DISTINCT
    acesso_funcionalidades.id,
    actions.act,
    actions.descricao,
    actions.tipo,
    actions.usr
  FROM acesso_funcionalidades,
    (
      SELECT
        'add'       AS act,
        'Adicionar' AS descricao,
        'Leitura'   AS tipo,
        'N'         AS usr
      FROM dual
      UNION ALL
      SELECT
        'edit'    AS act,
        'Editar'  AS descricao,
        'Leitura' AS tipo,
        'N'       AS usr
      FROM dual
      UNION ALL
      SELECT
        'remove'  AS act,
        'Remover' AS descricao,
        'Leitura' AS tipo,
        'N'       AS usr
      FROM dual
    ) AS actions
  WHERE func_controller IN (
    'Matricula\\Controller\\AcadgeralDisciplinaCurso'
  );
REPLACE INTO acesso_privilegios_grupo (acesso_actions_id, acesso_grupo_id)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
    ,
    acesso_grupo
  WHERE func_controller IN (
    'Matricula\\Controller\\AcadgeralDisciplinaCurso'
  )
        AND grup_nome LIKE 'admin%';

REPLACE INTO acesso_funcionalidades
VALUES (NULL, @mod, 'Docente', 'Matricula\\Controller\\AcadgeralDocente', '', NULL, 'Ativa', 'Sim');
REPLACE INTO acesso_actions (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT DISTINCT
    acesso_funcionalidades.id,
    actions.act,
    actions.descricao,
    actions.tipo,
    actions.usr
  FROM acesso_funcionalidades,
    (
      SELECT
        'searchForJson'   AS act,
        'Search For Json' AS descricao,
        'Leitura'         AS tipo,
        'N'               AS usr
      FROM dual
    ) AS actions
  WHERE func_controller IN (
    'Matricula\\Controller\\AcadgeralDocente'
  );
REPLACE INTO acesso_privilegios_grupo (acesso_actions_id, acesso_grupo_id)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
    ,
    acesso_grupo
  WHERE func_controller IN (
    'Matricula\\Controller\\AcadgeralDocente'
  )
        AND grup_nome LIKE 'admin%';