-- -----------------------------------------
-- Nova view que cálcula apenas os descontos
-- válidos e que podem ser aplicados aos
-- títulos
-- -----------------------------------------

DROP VIEW IF EXISTS `view__financeiro_descontos_titulos_atualizada`;
CREATE VIEW `view__financeiro_descontos_titulos_atualizada` AS
SELECT
`fdt`.`desconto_titulo_id` AS `desconto_titulo_id`,
`fdt`.`desconto_id` AS `desconto_id`,
`fdt`.`titulo_id` AS `titulo_id`,
`fdtp`.`desctipo_desconto_acumulativo` AS `desctipo_desconto_acumulativo`,
`fdtp`.`desctipo_limita_vencimento`,
`ft`.`titulo_data_vencimento` AS `titulo_data_vencimento`,
`fd`.`desconto_valor` AS `desconto_valor`,
`fd`.`desconto_percentual` AS `desconto_percentual`,
`fd`.`aluno_id` AS `aluno_id`,
`fdtp`.`desctipo_id` AS `desctipo_id`,
`ft`.`titulo_valor` AS `titulo_valor`,
ROUND(
	IF (
		(`ft`.`titulo_estado` != 'Aberto'),
		-- Se o t´titulo está fechado, pega o valor aplicado ou 0
		IF((`fdt`.`desconto_titulo_aplicado` = 'Sim'), `fdt`.`desconto_titulo_valor`, 0 ),
		IF(
			-- Verifica se o desconto é limitado ao vencimento e se o título já venceu
			(`fdtp`.`desctipo_limita_vencimento` = 'Sim' AND `ft`.`titulo_data_vencimento` < curdate()),
			0,
			-- Joga o valor do desconto ou calcula o desconto sobre o valor bruto do título
			IF((`fd`.`desconto_valor`>0), `fd`.`desconto_valor`, (`ft`.`titulo_valor` * (`fd`.`desconto_percentual` / 100)))
		)
	),
2
) AS `desconto_aplicacao_valor`
FROM `financeiro__desconto_titulo` `fdt`
JOIN `financeiro__desconto` `fd` ON `fdt`.`desconto_id` = `fd`.`desconto_id`
JOIN `financeiro__desconto_tipo` `fdtp` ON `fd`.`desctipo_id` = `fdtp`.`desctipo_id`
LEFT JOIN `financeiro__titulo` `ft` ON `fdt`.`titulo_id` = `ft`.`titulo_id`
WHERE `fd`.`desconto_status` != 'Inativo' AND `fdt`.`desconto_titulo_situacao` = 'Deferido';

-- -----------------------------------------
-- Consulta que traz os títulos com
-- desconto prefixado condicionado e não
-- condicionado ao vencimento
-- -----------------------------------------

SELECT
  t.titulo_id,
  t.pes_id,
  p.pes_nome,
  t.tipotitulo_id,
  t.usuario_baixa,
  ub.login       usuario_baixa_login,
  t.usuario_autor,
  ua.login       usuario_autor_login,
  t.titulo_novo,
  t.alunocurso_id,
  ac.aluno_id,
  a.pes_id AS pes_id_aluno,
  t.titulo_descricao,
  t.titulo_parcela,
  t.titulo_valor_pago,
  t.titulo_valor_pago_dinheiro,
  t.titulo_multa,
  t.titulo_juros,
  t.titulo_desconto,
  t.titulo_observacoes,
  t.titulo_desconto_manual,
  t.titulo_acrescimo_manual,
  t.titulo_data_processamento,
  t.titulo_data_vencimento,
  t.titulo_data_pagamento,
  t.titulo_data_baixa,
  t.titulo_tipo_pagamento,
  t.titulo_valor,
  t.titulo_estado,
  ac.cursocampus_id,
  GREATEST(
    IF(
    titulo_data_pagamento IS NULL,
    DATEDIFF(DATE(NOW()), DATE_ADD(DATE(t.titulo_data_vencimento),INTERVAL coalesce(tituloconf_dia_juros,0) DAY)),
    DATEDIFF(DATE(t.titulo_data_pagamento), DATE(t.titulo_data_vencimento))
    ),
    0
    ) AS dias_atraso,
    
  total_descontos_nao_limitados.desconto AS descontos_nao_limitados_total,
  total_descontos_nao_limitados.desctipo_desconto_acumulativo descontos_nao_limitados,
    
  total_descontos_limitados.desconto AS descontos_limitados_total,
  total_descontos_limitados.desctipo_desconto_acumulativo descontos_limitados_acumulativo,
  
  tc.tituloconf_id,
  tc.tituloconf_dia_venc,
  tc.tituloconf_dia_juros,
  tc.tituloconf_dia_desc,
  tc.tituloconf_valor_desc,
  tc.tituloconf_percent_desc,
  tc.tituloconf_multa,
  tc.tituloconf_juros,
  tc.confcont_id,
  b.bol_id,
  tt.tipotitulo_nome,
  tc.tituloconf_desconto_incentivo_acumulativo,
  coalesce(pes.pes_nome ,'-') as aluno_nome
FROM financeiro__titulo t
INNER JOIN financeiro__titulo_tipo AS tt on tt.tipotitulo_id=t.tipotitulo_id

-- dados aluno
INNER JOIN pessoa p ON t.pes_id = p.pes_id

-- curso aluno
LEFT JOIN acadgeral__aluno_curso ac ON ac.alunocurso_id = t.alunocurso_id
LEFT JOIN acadgeral__aluno a ON a.aluno_id = ac.aluno_id

-- nome para alunos
LEFT JOIN pessoa pes ON a.pes_id=pes.pes_id

-- boleto
LEFT JOIN boleto b ON t.titulo_id = b.titulo_id

-- usuário baixa
LEFT JOIN acesso_pessoas ua ON ua.id = t.usuario_autor
LEFT JOIN acesso_pessoas ub ON ub.id = t.usuario_baixa
LEFT JOIN financeiro__titulo_config  AS tc USING(tituloconf_id)

-- Descontos que podem ser abatidos independente da data
LEFT JOIN
(
    SELECT coalesce(sum(desconto_aplicacao_valor), 0) AS desconto, desctipo_desconto_acumulativo, fdt.titulo_id
    FROM view__financeiro_descontos_titulos_atualizada fdt
    WHERE desctipo_limita_vencimento = 'Não'
    GROUP BY titulo_id
) AS total_descontos_nao_limitados ON total_descontos_nao_limitados.titulo_id=t.titulo_id

-- Descontos que podem ser abatidos somente até o vencimento da data
LEFT JOIN
(
    SELECT coalesce(sum(desconto_aplicacao_valor), 0) AS desconto, desctipo_desconto_acumulativo,fdt.titulo_id
    FROM view__financeiro_descontos_titulos_atualizada fdt
    WHERE desctipo_limita_vencimento = 'Sim'
    GROUP BY titulo_id
) AS total_descontos_limitados ON total_descontos_limitados.titulo_id=t.titulo_id

-- CONDITIONS --

;

-- -----------------------------------------
-- Nova consulta de informações de títulos
-- -----------------------------------------

SELECT
      t.*,
      format(t.titulo_valor,2,'de_DE') as titulo_valor_Format,
      c.curso_nome,
      c.curso_sigla
FROM (
  SELECT
    t.*,     
    @CalculoDescontoIncentivo:='$formaCalculoIncentivo' AS calculoDescontoIncentivo,
    @DiaCobrancaJuros:=
    DATE_ADD(
      t.titulo_data_vencimento, INTERVAL COALESCE(t.tituloconf_dia_juros,0) DAY
    ) AS data_cobranca_juros,
    @descontos_vinculados_validos:= format(
      (
        t.descontos_nao_limitados_total +
        if(t.dias_atraso = 0, t.descontos_limitados_total, 0) +
        coalesce(t.titulo_desconto, 0) +
        coalesce(t.titulo_desconto_manual,0)
      ),
      2,
      'de_DE'
    ) as titulo_desconto_calc_formatado,
    @ValorLiquido:=(
      t.titulo_valor +
      COALESCE (t.titulo_acrescimo_manual,0) -
      (
        t.descontos_nao_limitados_total +
        if(t.dias_atraso = 0, t.descontos_limitados_total, 0) +
        COALESCE (t.titulo_desconto,0)
      )
    ) AS titulo_valor_liquido,
    @ValorLiquidoSemAcrescimo:=(
      t.titulo_valor - (
        t.descontos_nao_limitados_total +
        if(t.dias_atraso = 0, t.descontos_limitados_total, 0) +
        COALESCE (t.titulo_desconto,0) +
        COALESCE(t.titulo_desconto_manual,0))

    ) AS titulo_valor_liquido_sem_acrescimo,

      @ValorMulta:=round(
        coalesce(
           if(titulo_data_pagamento IS NULL AND t.dias_atraso > 0 AND date(now()) > @DiaCobrancaJuros ,
              -- configuração de juros e multa para título
                 @ValorLiquido * coalesce(tituloconf_multa, 0),
                 titulo_multa
           ),
           0
        ),
        2
    ) AS titulo_multa_calc,
    @ValorJuros:=round(
        coalesce(
           if(titulo_data_pagamento IS NULL AND t.dias_atraso > 0 AND date(now()) > @DiaCobrancaJuros,
             ((@ValorLiquido + @ValorMulta) * coalesce(tituloconf_juros, 0) * t.dias_atraso),
             coalesce(titulo_juros, 0)
           )
        ),
        2
     ) AS titulo_juros_calc,
     
     @ValorDescontoAntecipado:=if(
      (
        -- Desconto de incentivo é acumulativo
        t.tituloconf_desconto_incentivo_acumulativo='Sim' AND
        -- Desconto prefixado não condicionado ao vencimento é acumulativo ou não existe
        (t.descontos_nao_limitados_total IS NULL OR t.descontos_nao_limitados_acumulativo='Sim') AND
        -- Desconto prefixado condicionado ao vencimento é acumulativo ou não existe
        (t.descontos_limitados_total IS NULL OR t.descontos_limitados_acumulativo='Sim') AND
        -- Data do desconto ainda não passou
        DATE(now()) < DATE(t.data_desconto_antecipado)
      ),
      if(
        coalesce(t.tituloconf_valor_desc,0) != 0,
        t.tituloconf_valor_desc,
        (if('$testaSeFormaDeCalculoValorLiquido', @ValorLiquidoSemAcrescimo, t.titulo_valor) * t.tituloconf_percent_desc
        ))
        ,
        0
    ) AS valor_desconto_antecipado,
  
    (@ValorLiquido + @ValorMulta + @ValorJuros) AS titulo_valor_calc,
    (
       t.descontos_nao_limitados_total +
       if(t.dias_atraso = 0, t.descontos_limitados_total, 0) +
       coalesce(t.titulo_desconto, 0) +
       coalesce(t.titulo_desconto_manual, 0)
    ) AS titulo_desconto_calc,
    format((@ValorLiquido + @ValorMulta + @ValorJuros),2,'de_DE') as titulo_valor_calc_formatado
  FROM (
    SELECT
      t.titulo_id,
      t.pes_id,
      p.pes_nome,
      t.tipotitulo_id,
      t.usuario_baixa,
      ub.login       usuario_baixa_login,
      t.usuario_autor,
      ua.login       usuario_autor_login,
      t.titulo_novo,
      t.alunocurso_id,
      ac.aluno_id,
      a.pes_id AS pes_id_aluno,
      t.titulo_descricao,
      t.titulo_parcela,
      t.titulo_valor_pago,
      t.titulo_valor_pago_dinheiro,
      t.titulo_multa,
      t.titulo_juros,
      t.titulo_desconto,
      t.titulo_observacoes,
      t.titulo_desconto_manual,
      t.titulo_acrescimo_manual,
      t.titulo_data_processamento,
      t.titulo_data_vencimento,
      t.titulo_data_pagamento,
      t.titulo_data_baixa,
      t.titulo_tipo_pagamento,
      t.titulo_valor,
      t.titulo_estado,
      ac.cursocampus_id,
      GREATEST(
        IF(
          titulo_data_pagamento IS NULL,
          DATEDIFF(DATE(NOW()), DATE_ADD(DATE(t.titulo_data_vencimento),INTERVAL coalesce(tituloconf_dia_juros,0) DAY)),
          DATEDIFF(DATE(t.titulo_data_pagamento), DATE(t.titulo_data_vencimento))
        ),
        0
      ) AS dias_atraso,
        
      COALESCE(total_descontos_nao_limitados.desconto, 0) AS descontos_nao_limitados_total,
      total_descontos_nao_limitados.desctipo_desconto_acumulativo descontos_nao_limitados_acumulativo,
        
      COALESCE(total_descontos_limitados.desconto, 0) AS descontos_limitados_total,
      total_descontos_limitados.desctipo_desconto_acumulativo descontos_limitados_acumulativo,
      
      tc.tituloconf_id,
      tc.tituloconf_dia_venc,
      tc.tituloconf_dia_juros,
      tc.tituloconf_dia_desc,
      tc.tituloconf_valor_desc,
      tc.tituloconf_percent_desc,
      tc.tituloconf_multa,
      tc.tituloconf_juros,
      tc.confcont_id,
      b.bol_id,
      tt.tipotitulo_nome,
      tc.tituloconf_desconto_incentivo_acumulativo,
      coalesce(pes.pes_nome ,'-') as aluno_nome,

      IF(
        tc.tituloconf_dia_desc,
        DATE(
          CONCAT(
            DATE_FORMAT(t.titulo_data_vencimento,'%Y-%m-'),
            -- Verifica qual o menor dia possível
            LPAD(LEAST(tc.tituloconf_dia_desc, DAY(t.titulo_data_vencimento)),2,0)
          )
        ),
        NULL
      ) AS data_desconto_antecipado
    FROM financeiro__titulo t
    INNER JOIN financeiro__titulo_tipo AS tt on tt.tipotitulo_id=t.tipotitulo_id

    -- dados aluno
    INNER JOIN pessoa p ON t.pes_id = p.pes_id

    -- curso aluno
    LEFT JOIN acadgeral__aluno_curso ac ON ac.alunocurso_id = t.alunocurso_id
    LEFT JOIN acadgeral__aluno a ON a.aluno_id = ac.aluno_id

    -- nome para alunos
    LEFT JOIN pessoa pes ON a.pes_id=pes.pes_id

    -- boleto
    LEFT JOIN boleto b ON t.titulo_id = b.titulo_id

    -- usuário baixa
    LEFT JOIN acesso_pessoas ua ON ua.id = t.usuario_autor
    LEFT JOIN acesso_pessoas ub ON ub.id = t.usuario_baixa
    LEFT JOIN financeiro__titulo_config  AS tc USING(tituloconf_id)

    -- Descontos que podem ser abatidos independente da data
    LEFT JOIN
    (
        SELECT coalesce(sum(desconto_aplicacao_valor), 0) AS desconto, desctipo_desconto_acumulativo, fdt.titulo_id
        FROM view__financeiro_descontos_titulos_atualizada fdt
        WHERE desctipo_limita_vencimento = 'Não'
        GROUP BY titulo_id
        ORDER BY desctipo_desconto_acumulativo ASC
    ) AS total_descontos_nao_limitados ON total_descontos_nao_limitados.titulo_id=t.titulo_id

    -- Descontos que podem ser abatidos somente até o vencimento da data
    LEFT JOIN
    (
        SELECT coalesce(sum(desconto_aplicacao_valor), 0) AS desconto, desctipo_desconto_acumulativo,fdt.titulo_id
        FROM view__financeiro_descontos_titulos_atualizada fdt
        WHERE desctipo_limita_vencimento = 'Sim'
        GROUP BY titulo_id
        ORDER BY desctipo_desconto_acumulativo ASC
    ) AS total_descontos_limitados ON total_descontos_limitados.titulo_id=t.titulo_id

    -- CONDITIONS --
  ) t
    -- CONDITIONS 2 --
  GROUP BY titulo_id
) t
LEFT JOIN campus_curso cc ON cc.cursocampus_id = t.cursocampus_id
LEFT JOIN acad_curso c ON c.curso_id = cc.curso_id
;

-- -----------------------------------------
-- Nova consulta de informações de títulos
-- -----------------------------------------

SELECT
      t.*,format(t.titulo_valor,2,'de_DE') as titulo_valor_Format,
      c.curso_nome,
      c.curso_sigla
FROM (
  SELECT
    t.*,

    @CalculoDescontoIncentivo:='$formaCalculoIncentivo' AS calculoDescontoIncentivo,
    @DiaCobrancaJuros:=
    DATE_ADD(
      t.titulo_data_vencimento, INTERVAL COALESCE(t.tituloconf_dia_juros,0) DAY
    ) AS data_cobranca_juros,
    @DiaDescontoAntecipado:=IF(t.tituloconf_dia_desc,
      DATE(
        CONCAT(
          date_format(t.titulo_data_vencimento,'%Y-%m-'),
          LPAD(LEAST(t.tituloconf_dia_desc, DAY(t.titulo_data_vencimento)),2,0)
        )
      ),
      NULL
    ) AS data_desconto_antecipado,
    @descontos_vinculados_validos:= format((t.descontos + coalesce(t.titulo_desconto, 0) + coalesce(t.titulo_desconto_manual,0)),2,'de_DE') as titulo_desconto_calc_formatado,
    @ValorLiquido:=(
      t.titulo_valor +
      COALESCE (t.titulo_acrescimo_manual,0) -
      (
        COALESCE (t.descontos, 0) +
        COALESCE (t.titulo_desconto,0)
      )
    ) AS titulo_valor_liquido,
    @ValorLiquidoSemAcrescimo:=(
      t.titulo_valor - (

        COALESCE (t.descontos, 0) +
        COALESCE (t.titulo_desconto,0) +
        COALESCE(t.titulo_desconto_manual,0))

    ) AS titulo_valor_liquido_sem_acrescimo,

      @ValorMulta:=round(
        coalesce(
           if(titulo_data_pagamento IS NULL AND t.dias_atraso > 0 AND date(now()) > @DiaCobrancaJuros ,
              -- configuração de juros e multa para título
                 @ValorLiquido * coalesce(tituloconf_multa, 0),
                 titulo_multa
           ),
           0
        ),
        2
    ) AS titulo_multa_calc,
    @ValorJuros:=round(
        coalesce(
           if(titulo_data_pagamento IS NULL AND t.dias_atraso > 0 AND date(now()) > @DiaCobrancaJuros ,
             ((@ValorLiquido + @ValorMulta) * coalesce(tituloconf_juros, 0) * t.dias_atraso),
             coalesce(titulo_juros, 0)
           )
        ),
        2
     ) AS titulo_juros_calc,
     
     @ValorDescontoAntecipado:=if(
      (
		(((t.desctipo_desconto_acumulativo='Sim' AND t.tituloconf_desconto_incentivo_acumulativo='Sim' )OR t.descontos IS NULL) AND DATE(now()) < DATE(@DiaDescontoAntecipado))
      ),
      if(
        coalesce(t.tituloconf_valor_desc,0) != 0,
        t.tituloconf_valor_desc,
        (if('$testaSeFormaDeCalculoValorLiquido',@ValorLiquidoSemAcrescimo,t.titulo_valor)* t.tituloconf_percent_desc
        ))
        ,
        0
    ) AS valor_desconto_antecipado,
  
    (@ValorLiquido + @ValorMulta + @ValorJuros) AS titulo_valor_calc,
    (
       t.descontos + coalesce(t.titulo_desconto, 0) + coalesce(t.titulo_desconto_manual, 0)
    ) AS titulo_desconto_calc,
    format((@ValorLiquido + @ValorMulta + @ValorJuros),2,'de_DE') as titulo_valor_calc_formatado
            
  FROM (
    SELECT
      t.titulo_id,
      t.pes_id,
      p.pes_nome,
      t.tipotitulo_id,
      t.usuario_baixa,
      ub.login       usuario_baixa_login,
      t.usuario_autor,
      ua.login       usuario_autor_login,
      t.titulo_novo,
      t.alunocurso_id,
      ac.aluno_id,
      a.pes_id AS pes_id_aluno,
      t.titulo_descricao,
      t.titulo_parcela,
      t.titulo_valor_pago,
      t.titulo_valor_pago_dinheiro,
      t.titulo_multa,
      t.titulo_juros,
      t.titulo_desconto,
      t.titulo_observacoes,
      t.titulo_desconto_manual,
      t.titulo_acrescimo_manual,
      t.titulo_data_processamento,
      t.titulo_data_vencimento,
      t.titulo_data_pagamento,
      t.titulo_data_baixa,
      t.titulo_tipo_pagamento,
      t.titulo_valor,
      t.titulo_estado,
      ac.cursocampus_id,
      GREATEST(
        IF(
        titulo_data_pagamento IS NULL,
        DATEDIFF(DATE(NOW()), DATE_ADD(DATE(t.titulo_data_vencimento),INTERVAL coalesce(tituloconf_dia_juros,0) DAY)),
        DATEDIFF(DATE(t.titulo_data_pagamento), DATE(t.titulo_data_vencimento))
        ),
        0
        ) AS dias_atraso,
      total_descontos.desconto           AS descontos,
      total_descontos.desctipo_desconto_acumulativo ,
      tc.tituloconf_id,
      tc.tituloconf_dia_venc,
      tc.tituloconf_dia_juros,
      tc.tituloconf_dia_desc,
      tc.tituloconf_valor_desc,
      tc.tituloconf_percent_desc,
      tc.tituloconf_multa,
      tc.tituloconf_juros,
      tc.confcont_id,
      b.bol_id,
      tt.tipotitulo_nome,
      tc.tituloconf_desconto_incentivo_acumulativo,
      coalesce(pes.pes_nome ,'-') as aluno_nome
    FROM financeiro__titulo t
      -- dados aluno
      INNER JOIN pessoa p ON t.pes_id = p.pes_id
      -- curso aluno
      LEFT JOIN acadgeral__aluno_curso ac ON ac.alunocurso_id = t.alunocurso_id
      LEFT JOIN acadgeral__aluno a ON a.aluno_id = ac.aluno_id

      -- nome para alunos
      LEFT JOIN pessoa pes ON a.pes_id=pes.pes_id

      -- boleto
      LEFT JOIN boleto b ON t.titulo_id = b.titulo_id
      -- usuário baixa
      LEFT JOIN acesso_pessoas ua ON ua.id = t.usuario_autor
      LEFT JOIN acesso_pessoas ub ON ub.id = t.usuario_baixa
      LEFT JOIN financeiro__titulo_config  AS tc USING(tituloconf_id)
       INNER JOIN financeiro__titulo_tipo AS tt on tt.tipotitulo_id=t.tipotitulo_id
      LEFT JOIN
      (
        SELECT coalesce(sum(desconto_aplicacao_valor), 0) AS desconto, desctipo_desconto_acumulativo,fdt.titulo_id
        FROM view__financeiro_descontos_titulos_atualizada fdt
        WHERE desconto_aplicacao_valor > 0
        GROUP BY titulo_id
        ORDER BY desctipo_desconto_acumulativo ASC
      ) AS total_descontos ON total_descontos.titulo_id=t.titulo_id
	-- CONDITIONS --
  ) t
    -- CONDITIONS 2 --
  GROUP BY titulo_id
) t
LEFT JOIN campus_curso cc ON cc.cursocampus_id = t.cursocampus_id
LEFT JOIN acad_curso c ON c.curso_id = cc.curso_id;