ALTER TABLE `financeiro__titulo_tipo`
CHANGE COLUMN `tipotitulo_matricula_emissao` `tipotitulo_matricula_emissao` ENUM('Sim', 'Não', 'Opcional', 'Opcional Internamente') NULL DEFAULT 'Não';

ALTER TABLE `financeiro__aluno_config_pgto_curso`
ADD COLUMN `config_pgto_curso_selecionado` ENUM('Sim','Não') NULL DEFAULT 'Sim';

ALTER TABLE `financeiro__aluno_config_pgto_curso`
CHANGE `config_pgto_curso_parcela` `config_pgto_curso_parcela` INT(11) NULL,
CHANGE `config_pgto_curso_valor` `config_pgto_curso_valor` FLOAT NULL,
CHANGE `config_pgto_curso_vencimento` `config_pgto_curso_vencimento` INT(11) NULL;