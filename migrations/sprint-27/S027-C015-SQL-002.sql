-- MySQL Workbench Synchronization
-- Generated: 2017-05-18 08:57
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'TRADITIONAL,ALLOW_INVALID_DATES';

DROP TABLE IF EXISTS `protocolo__departamento`;
CREATE TABLE IF NOT EXISTS `protocolo__setor` (
  `setor_id`           INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `setor_descricao`    VARCHAR(200)                       NOT NULL,
  `setor_visibilidade` SET('Aluno', 'Externo', 'Interno') NOT NULL DEFAULT 'Interno',
  `setor_email`        VARCHAR(100)                       NULL     DEFAULT NULL,
  `pes_id`             INT(11) UNSIGNED ZEROFILL          NOT NULL,
  PRIMARY KEY (`setor_id`),
  INDEX `fk_protocolo__departamento_pessoa1_idx` (`pes_id` ASC),
  CONSTRAINT `fk_protocolo__departamento_pessoa1`
  FOREIGN KEY (`pes_id`)
  REFERENCES `pessoa` (`pes_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_general_ci;

DROP TABLE IF EXISTS `protocolo__solicitacao`;
CREATE TABLE IF NOT EXISTS `protocolo__solicitacao` (
  `solicitacao_id`           INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `setor_id`                 INT(10) ZEROFILL UNSIGNED NOT NULL,
  `solicitacao_descricao`    VARCHAR(200)                       NOT NULL,
  `solicitacao_visibilidade` SET('Aluno', 'Externo', 'Interno') NOT NULL DEFAULT 'Interno',
  PRIMARY KEY (`solicitacao_id`),
  INDEX `fk_protocolo__categoria_protocolo__departamento1_idx` (`setor_id` ASC),
  CONSTRAINT `fk_protocolo__categoria_protocolo__departamento1`
  FOREIGN KEY (`setor_id`)
  REFERENCES `protocolo__setor` (`setor_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_general_ci;


DROP TABLE IF EXISTS `protocolo`;
CREATE TABLE IF NOT EXISTS `protocolo` (
  `protocolo_id`                        INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `solicitacao_id`                      INT(10) ZEROFILL UNSIGNED NOT NULL,
  `usuario_id_criacao`                  INT(11) UNSIGNED ZEROFILL                   NULL     DEFAULT NULL,
  `usuario_id_responsavel`              INT(11) UNSIGNED ZEROFILL                   NULL     DEFAULT NULL,
  `protocolo_solicitante_pes_id`        INT(11) UNSIGNED ZEROFILL                   NULL     DEFAULT NULL,
  `protocolo_solicitante_alunocurso_id` BIGINT(12) ZEROFILL UNSIGNED NULL DEFAULT NULL,
  `protocolo_solicitante_nome`          VARCHAR(100)                                NULL     DEFAULT NULL,
  `protocolo_solicitante_email`         VARCHAR(100)                                NULL     DEFAULT NULL,
  `protocolo_solicitante_telefone`      VARCHAR(45)                                 NULL     DEFAULT NULL,
  `protocolo_solicitante_visivel`       ENUM('Sim', 'Não')                          NOT NULL DEFAULT 'Sim',
  `protocolo_assunto`                   VARCHAR(200)                                NOT NULL,
  `protocolo_data_cadastro`             DATETIME                                    NOT NULL,
  `protocolo_data_alteracao`            DATETIME                                    NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `protocolo_avaliacao`                 INT(5)                                      NULL     DEFAULT 3,
  `protocolo_situacao`                  ENUM('Aberto', 'Em Andamento', 'Concluído') NULL     DEFAULT NULL,
  PRIMARY KEY (`protocolo_id`),
  INDEX `fk_protocolo_protocolo__categoria1_idx` (`solicitacao_id` ASC),
  INDEX `fk_protocolo_acadgeral__aluno_curso1_idx` (`protocolo_solicitante_alunocurso_id` ASC),
  INDEX `fk_protocolo_pessoa1_idx` (`protocolo_solicitante_pes_id` ASC),
  INDEX `fk_protocolo_acesso_pessoas1_idx` (`usuario_id_responsavel` ASC),
  INDEX `fk_protocolo_acesso_pessoas2_idx` (`usuario_id_criacao` ASC),
  CONSTRAINT `fk_protocolo_protocolo__categoria1`
  FOREIGN KEY (`solicitacao_id`)
  REFERENCES `protocolo__solicitacao` (`solicitacao_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_protocolo_acadgeral__aluno_curso1`
  FOREIGN KEY (`protocolo_solicitante_alunocurso_id`)
  REFERENCES `acadgeral__aluno_curso` (`alunocurso_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_protocolo_pessoa1`
  FOREIGN KEY (`protocolo_solicitante_pes_id`)
  REFERENCES `pessoa` (`pes_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_protocolo_acesso_pessoas1`
  FOREIGN KEY (`usuario_id_responsavel`)
  REFERENCES `acesso_pessoas` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_protocolo_acesso_pessoas2`
  FOREIGN KEY (`usuario_id_criacao`)
  REFERENCES `acesso_pessoas` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_general_ci;

DROP TABLE IF EXISTS `protocolo__mensagem`;
CREATE TABLE IF NOT EXISTS `protocolo__mensagem` (
  `mensagem_id`                  INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `protocolo_id`                 INT(10) ZEROFILL UNSIGNED NOT NULL,
  `mensagem_usuario_id`          INT(11) UNSIGNED ZEROFILL       NULL     DEFAULT NULL,
  `mensagem_data`                DATETIME                        NOT NULL,
  `mensagem_origem`              ENUM('Solicitante', 'Operador') NOT NULL DEFAULT 'Operador',
  `mensagem_solicitante_visivel` ENUM('Sim', 'Não')              NOT NULL DEFAULT 'Sim',
  `mensagem_conteudo`            TEXT                            NOT NULL,
  PRIMARY KEY (`mensagem_id`),
  INDEX `fk_protocolo__mensagem_protocolo1_idx` (`protocolo_id` ASC),
  INDEX `fk_protocolo__mensagem_acesso_pessoas1_idx` (`mensagem_usuario_id` ASC),
  CONSTRAINT `fk_protocolo__mensagem_protocolo1`
  FOREIGN KEY (`protocolo_id`)
  REFERENCES `protocolo` (`protocolo_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_protocolo__mensagem_acesso_pessoas1`
  FOREIGN KEY (`mensagem_usuario_id`)
  REFERENCES `acesso_pessoas` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_general_ci;


DROP TABLE IF EXISTS `protocolo__mensagem_padrao`;
CREATE TABLE IF NOT EXISTS `protocolo__mensagem_padrao` (
  `mensagem_id`        INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `setor_id`           INT(10) ZEROFILL UNSIGNED NULL DEFAULT NULL,
  `solicitacao_id`     INT(10) ZEROFILL UNSIGNED NULL DEFAULT NULL,
  `mensagem_descricao` VARCHAR(150) NULL DEFAULT NULL,
  `mensagem_conteudo`  TEXT         NULL DEFAULT NULL,
  PRIMARY KEY (`mensagem_id`),
  INDEX `fk_protocolo_mensagem_padrao_protocolo__solicitacao1_idx` (`solicitacao_id` ASC),
  INDEX `fk_protocolo_mensagem_padrao_protocolo__setor1_idx` (`setor_id` ASC),
  CONSTRAINT `fk_protocolo_mensagem_padrao_protocolo__solicitacao1`
  FOREIGN KEY (`solicitacao_id`)
  REFERENCES `protocolo__solicitacao` (`solicitacao_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_protocolo_mensagem_padrao_protocolo__setor1`
  FOREIGN KEY (`setor_id`)
  REFERENCES `protocolo__setor` (`setor_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_general_ci;


DROP TABLE IF EXISTS `protocolo__mensagem_arquivo`;
CREATE TABLE IF NOT EXISTS `protocolo__mensagem_arquivo` (
  `mensagem_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  `arq_id`      INT(10) UNSIGNED ZEROFILL NOT NULL,
  PRIMARY KEY (`mensagem_id`, `arq_id`),
  INDEX `fk_protocolo__mensagem_arquivo_arquivo1_idx` (`arq_id` ASC),
  INDEX `fk_protocolo__mensagem_arquivo_protocolo__mensagem1_idx` (`mensagem_id` ASC),
  CONSTRAINT `fk_protocolo__mensagem_arquivo_protocolo__mensagem1`
  FOREIGN KEY (`mensagem_id`)
  REFERENCES `protocolo__mensagem` (`mensagem_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_protocolo__mensagem_arquivo_arquivo1`
  FOREIGN KEY (`arq_id`)
  REFERENCES `arquivo` (`arq_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_general_ci;


DROP TABLE IF EXISTS `protocolo__setor_grupo`;
CREATE TABLE IF NOT EXISTS `protocolo__setor_grupo` (
  `setor_grupo_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `setor_id`       INT(10) ZEROFILL UNSIGNED NOT NULL,
  `grupo_id`       INT(11) UNSIGNED ZEROFILL NOT NULL,
  PRIMARY KEY (`setor_grupo_id`),
  INDEX `fk_protocolo__setor_acesso_pessoas_protocolo__setor1_idx` (`setor_id` ASC),
  UNIQUE INDEX `unq_protocolo__setor_acesso_pessoas_protocolo__operador` (`setor_id` ASC, `grupo_id` ASC),
  INDEX `fk_protocolo__setor_operador_acesso_grupo1_idx` (`grupo_id` ASC),
  CONSTRAINT `fk_protocolo__setor_acesso_pessoas_protocolo__setor1`
  FOREIGN KEY (`setor_id`)
  REFERENCES `protocolo__setor` (`setor_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_protocolo__setor_operador_acesso_grupo1`
  FOREIGN KEY (`grupo_id`)
  REFERENCES `acesso_grupo` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_general_ci;


SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;
