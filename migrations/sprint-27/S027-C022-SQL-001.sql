ALTER TABLE `acadgeral__aluno_formacao`
CHANGE COLUMN `formacao_tipo_ensino` `formacao_tipo_ensino` ENUM('Publico','Privado') NULL DEFAULT NULL,
CHANGE COLUMN `formacao_ano` `formacao_ano` INT(4) NULL DEFAULT NULL,
CHANGE COLUMN `formacao_regime` `formacao_regime` ENUM('Anual', 'Semestral', 'Trimestral', 'Bimestral', 'Mensal') NULL DEFAULT NULL,
CHANGE COLUMN `formacao_duracao` `formacao_duracao` INT(11) NULL DEFAULT NULL;