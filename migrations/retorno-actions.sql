set @module    = (SELECT id FROM acesso_modulos WHERE mod_nome = 'Financeiro');
set @func = (SELECT (max(id))+1 FROM acesso_funcionalidades);

REPLACE INTO `acesso_funcionalidades` (`id`, `modulo`, `func_nome`, `func_controller`, `func_icon`, `func_info`, `func_status`, `func_visivel`) VALUES
(@func, @module, 'Baixa Bancária', 'Financeiro\\Controller\\FinanceiroRetorno', '', '', 'Ativa', 'Sim');

REPLACE INTO `acesso_actions` (`id`, `funcionalidade`, `act_nome`, `act_label`, `act_icon`, `act_info`, `act_tipo`, `act_visivel_usuario_final`) VALUES
(null, @func, 'index', 'Listagem', 'null', 'null', 'Leitura', 'N'),
(null, @func, 'search', 'Datatables', 'null', 'null', 'Leitura', 'N'),
(null, @func, 'ler-arquivo-retorno', 'Lê Arquivo de Retorno', '', '', 'Escrita', 'N');