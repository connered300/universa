--  ADICIONANDO A COLUNA NA TABELA
ALTER TABLE crm__lead ADD COLUMN `lead_data_atribuicao` DATETIME NULL DEFAULT NULL;
ALTER TABLE crm__lead CHANGE COLUMN `lead_data_atribuicao` `lead_data_atribuicao` DATETIME NULL DEFAULT NULL;

--  SCRIPT QUE ATUALIZA AS DATAS
UPDATE crm__lead
  LEFT JOIN
  (
    SELECT
      least(
          lead_data_atribuicao,
          lead_data_alteracao,
          coalesce(min(atividade_data_cadastro), lead_data_alteracao)
      ) AS lead_data_atribuicao,
      pes_id
    FROM
      crm__lead
      LEFT JOIN crm__lead_atividade USING (pes_id)
    GROUP BY pes_id
  ) AS ultimaAtividadeLead USING (pes_id)
SET crm__lead.lead_data_atribuicao = ultimaAtividadeLead.lead_data_atribuicao
WHERE pes_id > 0;