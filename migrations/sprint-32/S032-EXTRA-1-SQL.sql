REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
(
	select 'index' AS act, 'Matrícula' AS descricao, 'Escrita' as tipo,'N' as usr from dual UNION
	select 'matricula' AS act, 'Acadêmico' AS descricao, 'Escrita' as tipo,'N' as usr from dual
) as actions
where func_controller IN('Matricula\\Controller\\AcadgeralAlunoCurso');

REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
acesso_grupo
where func_controller IN(
		'Matricula\\Controller\\AcadgeralAlunoCurso'
)
and grup_nome LIKE 'admin%';

ALTER TABLE `acadperiodo__letivo`
  ADD `per_data_vencimento_inicial` DATETIME NOT NULL,
  ADD `per_data_vencimento_inicial_editavel` ENUM('Sim', 'Não') NOT NULL;