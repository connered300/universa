ALTER TABLE financeiro__titulo_config
ADD COLUMN tituloconf_desconto_incentivo_acumulativo ENUM('Sim', 'Não') NULL DEFAULT 'Não';

ALTER TABLE financeiro__desconto_tipo
ADD COLUMN desctipo_desconto_acumulativo ENUM('Sim', 'Não') NOT NULL DEFAULT 'Não';

/* view__financeiro_descontos_titulos_simplificado*/
DROP VIEW IF EXISTS `view__financeiro_descontos_titulos_simplificado`;

CREATE VIEW `view__financeiro_descontos_titulos_simplificado` AS
    SELECT
        `fdt`.`desconto_titulo_id` AS `desconto_titulo_id`,
        `fdt`.`desconto_id` AS `desconto_id`,
        `fdt`.`titulo_id` AS `titulo_id`,
        `fdt`.`usuario_alteracao` AS `usuario_alteracao`,
        `fdt`.`usuario_criacao` AS `usuario_criacao`,
        `fdt`.`desconto_titulo_criacao` AS `desconto_titulo_criacao`,
        `fdt`.`desconto_titulo_alteracao` AS `desconto_titulo_alteracao`,
        `fdt`.`desconto_titulo_aplicado` AS `desconto_titulo_aplicado`,
        `fdt`.`desconto_titulo_valor` AS `desconto_titulo_valor`,
        `fdt`.`desconto_titulo_situacao` AS `desconto_titulo_situacao`,
        `fd`.`aluno_id` AS `aluno_id`,
        `fd`.`desconto_valor` AS `desconto_valor`,
        `fd`.`desconto_percentual` AS `desconto_percentual`,
        `fd`.`desconto_status` AS `desconto_status`,
        `fd`.`desconto_dia_limite` AS `desconto_dia_limite`,
        `fdtp`.`desctipo_id` AS `desctipo_id`,
        fdtp.desctipo_desconto_acumulativo AS desctipo_desconto_acumulativo,
        `fdtp`.`desctipo_limita_vencimento` AS `desctipo_limita_vencimento`,
        IF((`ft`.`titulo_estado` = 'Aberto'),
            IF(((`fd`.`desconto_status` = 'Inativo')
                    OR (`fdt`.`desconto_titulo_situacao` <> 'Deferido')),
                'Não',
                IF(((`fdtp`.`desctipo_limita_vencimento` = 'Sim')
                        AND (`ft`.`titulo_data_vencimento` < CAST(NOW() AS DATE))),
                    'Não',
                    IF(((`fd`.`desconto_dia_limite` IS NOT NULL)
                            AND (CAST(CONCAT(YEAR(`ft`.`titulo_data_vencimento`),
                                    '-',
                                    MONTH(`ft`.`titulo_data_vencimento`),
                                    '-',
                                    `fd`.`desconto_dia_limite`)
                            AS DATE) < CURDATE())),
                        'Não',
                        'Sim'))),
            IF((`fdt`.`desconto_titulo_aplicado` = 'Sim'),
                'Sim',
                'Não')) AS `desconto_aplicacao_valida`,
        IF((`ft`.`titulo_estado` = 'Aberto'),
            IF((`fd`.`desconto_valor` IS NOT NULL),
                `fd`.`desconto_valor`,
                (`ft`.`titulo_valor` * (`fd`.`desconto_percentual` / 100))),
            IF((`fdt`.`desconto_titulo_aplicado` = 'Sim'),
                `fdt`.`desconto_titulo_valor`,
                0)) AS `desconto_aplicacao_valor`,
        `fdt`.`titulo_id` AS `tituloId`,
        `ft`.`titulo_valor` AS `titulo_valor`,
        `ft`.`titulo_data_vencimento` AS `titulo_data_vencimento`,
        `ft`.`titulo_estado` AS `titulo_estado`,
        `ft`.`titulo_estado` AS `tituloEstado`
    FROM
        (((`financeiro__desconto_titulo` `fdt`
        JOIN `financeiro__desconto` `fd` ON ((`fdt`.`desconto_id` = `fd`.`desconto_id`)))
        JOIN `financeiro__desconto_tipo` `fdtp` ON ((`fd`.`desctipo_id` = `fdtp`.`desctipo_id`)))
        LEFT JOIN `financeiro__titulo` `ft` ON ((`fdt`.`titulo_id` = `ft`.`titulo_id`)))
    ORDER BY `fdt`.`titulo_id` DESC;




DROP VIEW IF EXISTS `view__financeiro_descontos_titulos`;

CREATE
     OR REPLACE ALGORITHM = UNDEFINED
VIEW `view__financeiro_descontos_titulos` AS
    SELECT
        `fdt`.`desconto_titulo_id` AS `desconto_titulo_id`,
        `fdt`.`desconto_id` AS `desconto_id`,
        `fdt`.`titulo_id` AS `titulo_id`,
        `fdt`.`usuario_alteracao` AS `usuario_alteracao`,
        `fdt`.`usuario_criacao` AS `usuario_criacao`,
        `fdt`.`desconto_titulo_criacao` AS `desconto_titulo_criacao`,
        `fdt`.`desconto_titulo_alteracao` AS `desconto_titulo_alteracao`,
        `fdt`.`desconto_titulo_aplicado` AS `desconto_titulo_aplicado`,
        `fdt`.`desconto_titulo_valor` AS `desconto_titulo_valor`,
        `fdt`.`desconto_titulo_situacao` AS `desconto_titulo_situacao`,
        `u1`.`login` AS `usuario_criacao_login`,
        `u2`.`login` AS `usuario_alteracao_login`,
        CONCAT(`fdt`.`desconto_id`,
                ' - ',
                `fdtp`.`desctipo_descricao`,
                ' / ',
                `p`.`pes_nome`,
                ' / ',
                IF(ISNULL(`fdtp`.`desctipo_percmax`),
                    `fd`.`desconto_valor`,
                    CONCAT(`fd`.`desconto_percentual`, '%'))) AS `desconto_dados`,
        CONCAT(`fdt`.`titulo_id`,
                ' - ',
                `ft`.`titulo_descricao`) AS `titulo_dados`,
        `fd`.`desconto_valor` AS `desconto_valor`,
        `fd`.`desconto_percentual` AS `desconto_percentual`,
        `fd`.`desconto_status` AS `desconto_status`,
        `fd`.`desconto_dia_limite` AS `desconto_dia_limite`,
        `fdtp`.`desctipo_id` AS `desctipo_id`,
        `fdtp`.`desctipo_limita_vencimento` AS `desctipo_limita_vencimento`,
        fdtp.desctipo_desconto_acumulativo AS desctipo_desconto_acumulativo,
        IF((`ft`.`titulo_estado` = 'Aberto'),
            IF(((`fd`.`desconto_status` = 'Inativo')
                    OR (`fdt`.`desconto_titulo_situacao` <> 'Deferido')),
                'Não',
                IF(((`fdtp`.`desctipo_limita_vencimento` = 'Sim')
                        AND (`ft`.`titulo_data_vencimento` < CAST(NOW() AS DATE))),
                    'Não',
                    IF(((`fd`.`desconto_dia_limite` IS NOT NULL)
                            AND (CAST(CONCAT(YEAR(`ft`.`titulo_data_vencimento`),
                                    '-',
                                    MONTH(`ft`.`titulo_data_vencimento`),
                                    '-',
                                    `fd`.`desconto_dia_limite`)
                            AS DATE) < CURDATE())),
                        'Não',
                        'Sim'))),
            IF((`fdt`.`desconto_titulo_aplicado` = 'Sim'),
                'Sim',
                'Não')) AS `desconto_aplicacao_valida`,
        IF((`ft`.`titulo_estado` = 'Aberto'),
            IF((`fd`.`desconto_valor` IS NOT NULL),
                `fd`.`desconto_valor`,
                (`ft`.`titulo_valor` * (`fd`.`desconto_percentual` / 100))),
            IF((`fdt`.`desconto_titulo_aplicado` = 'Sim'),
                `fdt`.`desconto_titulo_valor`,
                0)) AS `desconto_aplicacao_valor`,
        `fdt`.`titulo_id` AS `tituloId`,
        `ft`.`titulo_valor` AS `titulo_valor`,
        `ft`.`titulo_data_vencimento` AS `titulo_data_vencimento`,
        `ft`.`titulo_estado` AS `titulo_estado`,
        `ft`.`titulo_estado` AS `tituloEstado`
    FROM
        (((((((`financeiro__desconto_titulo` `fdt`
        JOIN `financeiro__desconto` `fd` ON ((`fdt`.`desconto_id` = `fd`.`desconto_id`)))
        JOIN `financeiro__desconto_tipo` `fdtp` ON ((`fd`.`desctipo_id` = `fdtp`.`desctipo_id`)))
        LEFT JOIN `acesso_pessoas` `u1` ON ((`u1`.`id` = `fdt`.`usuario_criacao`)))
        LEFT JOIN `acesso_pessoas` `u2` ON ((`u2`.`id` = `fdt`.`usuario_alteracao`)))
        LEFT JOIN `financeiro__titulo` `ft` ON ((`fdt`.`titulo_id` = `ft`.`titulo_id`)))
        LEFT JOIN `acadgeral__aluno` `aa` ON ((`fd`.`aluno_id` = `aa`.`aluno_id`)))
        LEFT JOIN `pessoa` `p` ON ((`p`.`pes_id` = `aa`.`pes_id`)))
    ORDER BY `fdt`.`titulo_id` DESC;
