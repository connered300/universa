ALTER TABLE `acad_curso`
CHANGE curso_periodicidade `curso_possui_periodo_letivo` ENUM('Sim', 'Não') CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NOT NULL COMMENT 'É o período em que o curso forma novas turmas';
ALTER TABLE `acad_curso`
ADD `curso_unidade_medida` ENUM('Mensal', 'Bimestral', 'Trimestral', 'Quadrimestral', 'Semestral', 'Anual') NOT NULL;