REPLACE INTO `acesso_funcionalidades` (modulo, func_nome, func_controller, func_status, func_visivel)
  SELECT distinct acesso_modulos.id, func.descricao, func.controller,'Ativa','Sim'  from acesso_modulos,
    (
      select 'Meus Protocolos' as descricao, 'Protocolo\\Controller\\ProtocoloVinculado' as controller from dual
    ) as func
  where mod_nome like 'Protocolo';

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
    (
      select 'index' as act,'Index' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'search' as act,'Busca' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'edit' as act,'Busca' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'add' as act,'Adicionar' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'search-mensagem' as act,'Busca' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'add-mensagem' as act,'Adicionar Mensagem' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'search-protocolo-arquivo' as act,'Buscar arquivos' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'atualiza-situacao' as act,'Listagem' as descricao,'Leitura' as tipo,'N' as usr FROM dual
    ) as actions
  where func_controller IN('Protocolo\\Controller\\ProtocoloVinculado');

REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
  select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
    inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
    acesso_grupo
  where func_controller IN(
    'Protocolo\\Controller\\ProtocoloVinculado'
  )
  and grup_nome LIKE 'admin%';