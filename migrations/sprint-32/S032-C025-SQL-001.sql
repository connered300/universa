ALTER TABLE `acadgeral__motivo_alteracao`
ADD COLUMN `motivo_situacao` ENUM('Deferido', 'Pendente', 'Trancado', 'Cancelado', 'Indeferido', 'Transferencia', 'Concluido') NOT NULL DEFAULT 'Deferido'
AFTER `motivo_descricao`;

ALTER TABLE `acadgeral__aluno_curso`
CHANGE COLUMN `alunocurso_situacao` `alunocurso_situacao` ENUM('Deferido', 'Pendente', 'Trancado', 'Cancelado', 'Indeferido', 'Transferencia', 'Concluido') NULL DEFAULT NULL;

ALTER TABLE `acadgeral__motivo_alteracao`
CHANGE COLUMN `motivo_situacao` `motivo_situacao` SET('Deferido', 'Pendente', 'Trancado', 'Cancelado', 'Indeferido', 'Transferencia', 'Concluido') NOT NULL DEFAULT 'Deferido,Pendente,Trancado,Cancelado,Indeferido,Transferencia,Concluido';

UPDATE `acadgeral__motivo_alteracao`
SET `motivo_situacao` = 'Deferido,Pendente,Trancado,Cancelado,Indeferido,Transferencia,Concluido'
WHERE motivo_id > 0;


/*
Script para alterar a situação dos alunos, na tabela acadgeral__aluno_curso
*/
UPDATE `acadgeral__aluno_curso` SET `alunocurso_situacao`='Concluido'
WHERE (alunocurso_data_expedicao_diploma is not null OR alunocurso_data_colacao is not null) and alunocurso_id>0;