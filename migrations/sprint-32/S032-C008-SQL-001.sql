ALTER TABLE `acadperiodo__aluno_resumo`
  MODIFY `resaluno_origem` ENUM('Legado','Transferência','Novo título','Manual','Sistema', 'WebService') NOT NULL DEFAULT 'Sistema',
  MODIFY `resaluno_serie` int(11) DEFAULT NULL;


ALTER TABLE `acadperiodo__aluno_resumo`
ADD COLUMN `resaluno_detalhe` VARCHAR(100) NULL ;


ALTER TABLE `acadperiodo__aluno_resumo`
CHANGE COLUMN `resaluno_origem` `resaluno_origem` ENUM('Legado', 'Transferência', 'Novo título', 'Manual', 'Sistema', 'WebService') NOT NULL DEFAULT 'WebService' ;
