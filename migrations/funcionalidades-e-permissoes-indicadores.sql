REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT DISTINCT
    acesso_funcionalidades.id,
    actions.act,
    actions.descricao,
    actions.tipo,
    actions.usr
  FROM acesso_funcionalidades,
    (
      SELECT
        'busca-historico-escolar'       AS act,
        'Listar registros do histórico' AS descricao,
        'Escrita'                       AS tipo,
        'N'                             AS usr
      FROM dual
      UNION ALL
      SELECT
        'historico-remover'         AS act,
        'Remover item do histórico' AS descricao,
        'Escrita'                   AS tipo,
        'N'                         AS usr
      FROM dual
      UNION ALL
      SELECT
        'salva-historico-escolar'  AS act,
        'Salvar Item no histórico' AS descricao,
        'Escrita'                  AS tipo,
        'N'                        AS usr
      FROM dual
    ) AS actions
  WHERE func_controller IN (
    'Matricula\\Controller\\Aluno'
  );

REPLACE INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
    ,
    acesso_grupo
  WHERE func_controller IN (
    'Matricula\\Controller\\Aluno'
  )
        AND grup_nome LIKE 'Admin';