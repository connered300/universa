ALTER TABLE financeiro__retorno ADD COLUMN retorno_status ENUM('Pendente', 'Processando', 'Finalizado', 'Rejeitado') NOT NULL DEFAULT 'Finalizado';
ALTER TABLE financeiro__retorno ADD COLUMN retorno_log TEXT NULL;

ALTER TABLE financeiro__retorno_detalhe ADD COLUMN detalhe_status ENUM('Pendente', 'Processando', 'Finalizado', 'Rejeitado') NOT NULL DEFAULT 'Finalizado';
ALTER TABLE financeiro__retorno_detalhe ADD COLUMN detalhe_log TEXT NULL;
ALTER TABLE financeiro__retorno_detalhe ADD COLUMN detalhe_codigo_movimento INTEGER;
ALTER TABLE financeiro__retorno_detalhe ADD COLUMN titulo_id INTEGER;

SET @control = 'Financeiro\\Controller\\FinanceiroRetorno';

REPLACE INTO acesso_actions (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT DISTINCT
    acesso_funcionalidades.id,
    actions.act,
    actions.descricao,
    actions.tipo,
    actions.usr
  FROM acesso_funcionalidades,
    (
      SELECT
        'searchDetalhes'  AS act,
        'Search Detalhes' AS descricao,
        'Leitura'         AS tipo,
        'N'               AS usr
      FROM dual
    ) AS actions
  WHERE func_controller IN (
    @control
  );
REPLACE INTO acesso_privilegios_grupo (acesso_actions_id, acesso_grupo_id)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
    ,
    acesso_grupo
  WHERE func_controller IN (
    @control
  )
        AND grup_nome LIKE 'admin%';
