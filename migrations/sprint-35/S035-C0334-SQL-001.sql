	REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT DISTINCT
    acesso_funcionalidades.id,
    actions.act,
    actions.descricao,
    actions.tipo,
    actions.usr
  FROM acesso_funcionalidades,
    (
      SELECT
        'consultar-acervo'       AS act,
        'Consulta acervo para alunos' AS descricao,
        'Escrita'                       AS tipo,
        'N'                             AS usr
      FROM dual
    ) AS actions
  WHERE func_controller IN (
    'Aluno\\Controller\\Biblioteca'
  );

REPLACE INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id,acesso_grupo
  WHERE func_controller IN (
    'Aluno\\Controller\\Biblioteca'  ) AND grup_nome LIKE '%Aluno';


select @mod:=id from acesso_modulos where mod_nome like 'Professor%';

REPLACE INTO acesso_funcionalidades VALUES (null, @mod, 'Biblioteca', 'Professor\\Controller\\Biblioteca', '', null, 'Ativa', 'Sim');

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT DISTINCT
    acesso_funcionalidades.id,
    actions.act,
    actions.descricao,
    actions.tipo,
    actions.usr
  FROM acesso_funcionalidades,
    (
      SELECT
        'consultar-acervo'       AS act,
        'Consulta acervo para professor' AS descricao,
        'Escrita'                       AS tipo,
        'N'                             AS usr
      FROM dual
    ) AS actions
  WHERE func_controller IN (
    'Professor\\Controller\\Biblioteca'
  );

REPLACE INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id,acesso_grupo
  WHERE func_controller IN (
    'Professor\\Controller\\Biblioteca'  ) AND grup_nome LIKE 'Professores';




