ALTER TABLE acadperiodo__letivo ADD COLUMN per_rematricula_online_inicio DATETIME;
ALTER TABLE acadperiodo__letivo ADD COLUMN per_rematricula_online_fim DATETIME;
UPDATE acadperiodo__letivo
SET
  per_rematricula_online_inicio = per_matricula_inicio,
  per_rematricula_online_fim    = per_matricula_fim
WHERE per_id >= 1;