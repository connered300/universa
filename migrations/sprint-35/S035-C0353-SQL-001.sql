ALTER TABLE `financeiro__pagamento` DROP FOREIGN KEY `fk_financeiro__pagamento_financeiro_meio_pagamento1`;

ALTER TABLE `financeiro__pagamento`
ADD CONSTRAINT `fk_financeiro__pagamento_financeiro_meio_pagamento1`
FOREIGN KEY (`meio_pagamento_id`)
REFERENCES `financeiro__meio_pagamento` (`meio_pagamento_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

SET @MaxMpgid := (
  SELECT max(meio_pagamento_id) + 1
  FROM financeiro__meio_pagamento
);

UPDATE financeiro__meio_pagamento
SET meio_pagamento_id = @MaxMpgid
WHERE meio_pagamento_id = 0;