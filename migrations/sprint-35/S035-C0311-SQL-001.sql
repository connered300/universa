REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT DISTINCT
    acesso_funcionalidades.id,
    actions.act,
    actions.descricao,
    actions.tipo,
    actions.usr
  FROM acesso_funcionalidades,
    (
      SELECT
        'boletos-aluno'                    AS act,
        'Impressão de Boletos para Alunos' AS descricao,
        'Leitura'                          AS tipo,
        'N'                                AS usr
    ) AS actions
  WHERE func_controller IN ('Financeiro\\Controller\\Relatorio');

REPLACE INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
    ,
    acesso_grupo
  WHERE func_controller IN (
    'Financeiro\\Controller\\Relatorio'
  ) AND grup_nome LIKE 'admin%';

REPLACE INTO `acesso_funcionalidades` (modulo, func_nome, func_controller, func_status, func_visivel)
  SELECT DISTINCT
    acesso_modulos.id,
    func.descricao,
    func.controller,
    'Ativa',
    'Sim'
  FROM acesso_modulos,
    (
      SELECT
        'Financeiro Pessoa'                        AS descricao,
        'Financeiro\\Controller\\FinanceiroPessoa' AS controller
      FROM dual
    ) AS func
  WHERE mod_nome LIKE 'Financeiro';

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT DISTINCT
    acesso_funcionalidades.id,
    actions.act,
    actions.descricao,
    actions.tipo,
    actions.usr
  FROM acesso_funcionalidades,
    (
      SELECT
        'boletos-aluno-table' AS act,
        'Boletos para Aluno'  AS descricao,
        'Leitura'             AS tipo,
        'N'                   AS usr
    ) AS actions
  WHERE func_controller IN ('Financeiro\\Controller\\FinanceiroPessoa');

REPLACE INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
    ,
    acesso_grupo
  WHERE func_controller IN (
    'Financeiro\\Controller\\FinanceiroPessoa'
  ) AND grup_nome LIKE '%Admin%';