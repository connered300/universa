ALTER TABLE `atividadeperiodo__evento`
DROP FOREIGN KEY `fk_atividadeperiodo__evento_periodo_letivo_idx`;

ALTER TABLE `atividadeperiodo__evento`
DROP COLUMN `evento_periodo_letivo`,
DROP INDEX `fk_atividadeperiodo__evento_periodo_letivo_idx` ;