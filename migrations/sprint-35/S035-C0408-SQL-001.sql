ALTER TABLE `selecao_locais` DROP FOREIGN KEY `fk_selecao_locais_infra_predio1`;
ALTER TABLE `selecao_locais` CHANGE COLUMN `predio_id` `predio_id` INT(11) UNSIGNED ZEROFILL NULL;
ALTER TABLE `selecao_locais` ADD COLUMN `unidade_id` INT(10) ZEROFILL NULL
AFTER `selecao_locaiscol`;

ALTER TABLE `selecao_locais`
ADD CONSTRAINT `fk_selecao_locais_infra_predio1`
FOREIGN KEY (`predio_id`)
REFERENCES `infra_predio` (`predio_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `selecao_locais`
ADD INDEX `fk_selecao_locais_unidade_estudo_idx` (`unidade_id` ASC);

ALTER TABLE `selecao_locais`
ADD CONSTRAINT `fk_selecao_locais_unidade_estudo`
FOREIGN KEY (`unidade_id`)
REFERENCES `org__unidade_estudo` (`unidade_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

