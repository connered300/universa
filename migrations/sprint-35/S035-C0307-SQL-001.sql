REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT DISTINCT
    acesso_funcionalidades.id,
    actions.act,
    actions.descricao,
    actions.tipo,
    actions.usr
  FROM acesso_funcionalidades,
    (
      SELECT
        'imprimir'                AS act,
        'Impressão de Protocolos' AS descricao,
        'Leitura'                 AS tipo,
        'N'                       AS usr
      FROM dual
    ) AS actions
  WHERE func_controller IN ('Protocolo\\Controller\\Protocolo');

REPLACE INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
  INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id, acesso_grupo
  WHERE func_controller IN ('Protocolo\\Controller\\Protocolo') AND grup_nome LIKE 'admin%';