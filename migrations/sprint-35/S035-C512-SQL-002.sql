ALTER TABLE `financeiro__cheque` ADD COLUMN `pes_id` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL;
ALTER TABLE `financeiro__cheque` ADD COLUMN `cheque_observacao` TEXT NULL DEFAULT NULL;
ALTER TABLE `financeiro__cheque` ADD INDEX `fk_financeiro__cheque_pessoa_idx` (`pes_id` ASC);
ALTER TABLE `financeiro__cheque`
ADD CONSTRAINT `fk_financeiro__cheque_pessoa` FOREIGN KEY (`pes_id`) REFERENCES `pessoa` (`pes_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;
