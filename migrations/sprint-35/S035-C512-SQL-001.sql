ALTER TABLE `financeiro__meio_pagamento`
ADD COLUMN `meio_pagamento_campos_cheque` ENUM('Sim', 'Não') NULL DEFAULT 'Não'
AFTER `meio_pagamento_status`;
UPDATE financeiro__meio_pagamento
SET meio_pagamento_campos_cheque = 'Sim'
WHERE meio_pagamento_descricao LIKE '%cheque%';
