SET FOREIGN_KEY_CHECKS = 0;

TRUNCATE protocolo__mensagem_arquivo;
TRUNCATE protocolo__mensagem;
TRUNCATE protocolo;
TRUNCATE protocolo__solicitacao;
TRUNCATE protocolo__setor;

-- Migra setores
INSERT INTO protocolo__setor (setor_id, setor_descricao, setor_visibilidade, setor_email, pes_id)
  SELECT
    ID_DEPARTAMENTO,
    NOME,
    'Interno,Externo,Aluno',
    EMAIL,
    1
  FROM mv_ead_instituicao.wp_vz3fcq_ticketplus_departamento;

-- Migra solicitações
INSERT INTO protocolo__solicitacao (solicitacao_id, setor_id, solicitacao_descricao, solicitacao_visibilidade)
  SELECT
    ID,
    ID_DEP,
    categoria,
    'Interno,Externo,Aluno'
  FROM mv_ead_instituicao.wp_vz3fcq_ticketplus_cat_departamento;

-- Migra protocolos
INSERT INTO protocolo
(protocolo_id, solicitacao_id, usuario_id_criacao, usuario_id_alteracao, usuario_id_responsavel,
 protocolo_solicitante_pes_id, protocolo_solicitante_alunocurso_id, protocolo_solicitante_nome, protocolo_solicitante_email, protocolo_solicitante_telefone,
 protocolo_solicitante_visivel, protocolo_assunto, protocolo_data_cadastro, protocolo_data_alteracao, protocolo_avaliacao,
 protocolo_situacao)
  SELECT
    ID_TICKET,
    ID_CATEGORIA,
    NULL,
    NULL,
    NULL,
    a.pes_id,
    NULL,
    NOME,
    EMAIL,
    TELEFONE,
    'SIm',
    CONCAT(COALESCE(TITULO, ''), IF(CURSO, ' - ', ''), coalesce(CURSO, '')),
    DATA,
    DATA,
    NOTA,
    IF(ID_STATUS = 1, 'Em Aberto', IF(ID_STATUS = 3, 'Em Andamento', 'Concluído'))
  FROM mv_ead_instituicao.wp_vz3fcq_ticketplus_ticket
    LEFT JOIN sis__integracao_aluno sia ON sia.codigo = ID_USER
    LEFT JOIN acadgeral__aluno a ON a.aluno_id = MATRICULA OR a.aluno_id = sia.aluno_id
  GROUP BY ID_TICKET;

-- Migra mensagens de ticket
INSERT INTO protocolo__mensagem
(mensagem_id, protocolo_id, mensagem_usuario_id, mensagem_data, mensagem_origem, mensagem_solicitante_visivel, mensagem_conteudo)
  SELECT
    ID_TICKET_TEXT,
    ID_TICKET,
    NULL,
    DATA,
    IF(CLIENTE = 0, 'Operador', 'Solicitante'),
    'Sim',
    CONCAT(COALESCE(MENSAGEM, ''), IF(responsavel, '<br> - <br>', ''), coalesce(responsavel, ''))
  FROM mv_ead_instituicao.wp_vz3fcq_ticketplus_ticket_text;

SET FOREIGN_KEY_CHECKS = 1;