--
-- Estrutura da tabela `financeiro__remessa`
--

DROP TABLE IF EXISTS financeiro__remessa;
CREATE TABLE IF NOT EXISTS `financeiro__remessa` (
  `remessa_id` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `confcont_id` int(11) NOT NULL,
  `remessa_codigo` int(11) NOT NULL,
  `remessa_data` datetime NOT NULL,
  `arq_id` int(10) unsigned zerofill NOT NULL,
  PRIMARY KEY (`remessa_id`),
  UNIQUE KEY `remessa_codigo_UNIQUE` (`remessa_codigo`,`confcont_id`),
  KEY `fk_financeiro__remessa_arquivo1_idx` (`arq_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `financeiro__remessa_lote`
--
DROP TABLE IF EXISTS financeiro__remessa_lote;
CREATE TABLE IF NOT EXISTS `financeiro__remessa_lote` (
  `remessa_lote_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bol_id` int(11) NOT NULL,
  `remessa_id` int(10) unsigned zerofill NOT NULL,
  `lote_codigo` int(11) NOT NULL,
  PRIMARY KEY (`remessa_lote_id`),
  UNIQUE KEY `uniq` (`bol_id`,`remessa_id`,`lote_codigo`),
  KEY `fk_boleto_financeiro__remessa_financeiro__remessa1_idx` (`remessa_id`),
  KEY `fk_boleto_financeiro__remessa_boleto1_idx` (`bol_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `financeiro__remessa`
--
ALTER TABLE `financeiro__remessa`
  ADD CONSTRAINT `fk_financeiro__remessa_arquivo1` FOREIGN KEY (`arq_id`) REFERENCES `arquivo` (`arq_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_financeiro_remessa_boleto_conf_conta1` FOREIGN KEY (`confcont_id`) REFERENCES `boleto_conf_conta` (`confcont_id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `financeiro__remessa_lote`
--
ALTER TABLE `financeiro__remessa_lote`
  ADD CONSTRAINT `fk_boleto_financeiro__remessa_boleto1` FOREIGN KEY (`bol_id`) REFERENCES `boleto` (`bol_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_boleto_financeiro__remessa_financeiro__remessa1` FOREIGN KEY (`remessa_id`) REFERENCES `financeiro__remessa` (`remessa_id`) ON UPDATE CASCADE;
