SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `financeiro__titulo_tipo`
ADD COLUMN `tipotitulo_desconto_percentual_maximo` FLOAT(11) NOT NULL AFTER `tipotitulo_descricao`,
ADD COLUMN `tipotitulo_numero_maximo_parcelas` INT(11) NOT NULL AFTER `tipotitulo_desconto_percentual_maximo`;


ALTER TABLE `financeiro__titulo_tipo`
CHANGE `tipotitulo_desconto_percentual_maximo` `tipotitulo_desconto_percentual_maximo` FLOAT NULL;
ALTER TABLE `financeiro__titulo_tipo`
CHANGE `tipotitulo_numero_maximo_parcelas` `tipotitulo_numero_maximo_parcelas` INT(11) NULL;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
