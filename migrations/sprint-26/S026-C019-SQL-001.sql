-- MySQL Workbench Synchronization
-- Generated: 2017-05-09 14:07
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


DROP TABLE IF EXISTS `financeiro__retorno_detalhe`;
DROP TABLE IF EXISTS `financeiro__retorno`;

CREATE TABLE IF NOT EXISTS `financeiro__retorno` (
  `retorno_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `arq_id` INT(10) UNSIGNED ZEROFILL NULL DEFAULT NULL,
  `usuario_id` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL,
  `retorno_banco` INT(11) NOT NULL,
  `retorno_conta` INT(11) NULL DEFAULT NULL,
  `retorno_convenio` INT(11) NULL DEFAULT NULL,
  `retorno_data` DATETIME NOT NULL,
  PRIMARY KEY (`retorno_id`),
  INDEX `fk_financeiro__remessa_arquivo1_idx` (`arq_id` ASC),
  INDEX `fk_financeiro__retorno_acesso_pessoas1_idx` (`usuario_id` ASC),
  CONSTRAINT `fk_financeiro__remessa_arquivo10`
    FOREIGN KEY (`arq_id`)
    REFERENCES `arquivo` (`arq_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_financeiro__retorno_acesso_pessoas1`
    FOREIGN KEY (`usuario_id`)
    REFERENCES `acesso_pessoas` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `financeiro__retorno_detalhe` (
  `detalhe_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `retorno_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  `detalhe_data` DATETIME NULL DEFAULT NULL,
  `detalhe_data_vencimento` DATETIME NULL DEFAULT NULL,
  `detalhe_nosso_numero` VARCHAR(40) NULL DEFAULT NULL,
  `detalhe_numero_documento` VARCHAR(40) NULL DEFAULT NULL,
  `detalhe_valor_titulo` FLOAT(11) NULL DEFAULT NULL,
  `detalhe_valor_pago` FLOAT(11) NULL DEFAULT NULL,
  `detalhe_valor_mora` FLOAT(11) NULL DEFAULT NULL,
  `detalhe_valor_desconto` FLOAT(11) NULL DEFAULT NULL,
  `detalhe_carteira` VARCHAR(5) NULL DEFAULT NULL,
  `detalhe_alegacao` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`detalhe_id`),
  INDEX `fk_financeiro__retorno_detalhe_financeiro__retorno1_idx` (`retorno_id` ASC),
  CONSTRAINT `fk_financeiro__retorno_detalhe_financeiro__retorno1`
    FOREIGN KEY (`retorno_id`)
    REFERENCES `financeiro__retorno` (`retorno_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
