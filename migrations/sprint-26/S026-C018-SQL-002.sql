-- MySQL Workbench Synchronization
-- Generated: 2017-05-08 13:43
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `financeiro__aluno_config_pgto_curso`
ADD COLUMN `config_pgto_curso_obs` TEXT NULL DEFAULT NULL AFTER `config_pgto_curso_alterado`,
ADD COLUMN `config_pgto_curso_justificativa` TEXT NULL DEFAULT NULL AFTER `config_pgto_curso_obs`;

ALTER TABLE `financeiro__aluno_config_pgto_curso`
ADD CONSTRAINT `fk_financeiro__aluno_config_pgto_curso_financeiro__meio_pagam1`
FOREIGN KEY (`meio_pagamento_id`)
REFERENCES `financeiro__meio_pagamento` (`meio_pagamento_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `financeiro__aluno_config_pgto_curso`
ADD CONSTRAINT `fk_financeiro__aluno_config_pgto_curso_financeiro__valores1`
FOREIGN KEY (`valores_id`)
REFERENCES `financeiro__valores` (`valores_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `financeiro__aluno_config_pgto_curso`
ADD CONSTRAINT `fk_financeiro__aluno_config_pgto_curso_acesso_pessoas1`
FOREIGN KEY (`usuario_alteracao`)
REFERENCES `acesso_pessoas` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `financeiro__aluno_config_pgto_curso`
ADD CONSTRAINT `fk_financeiro__aluno_config_pgto_curso_acesso_pessoas2`
FOREIGN KEY (`usuario_supervisor`)
REFERENCES `acesso_pessoas` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;