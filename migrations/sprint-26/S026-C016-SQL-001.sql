SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'TRADITIONAL,ALLOW_INVALID_DATES';

-- Adiciona vinculo de qual matricula de aluno em periodo letivo gerou o título

ALTER TABLE `financeiro__titulo`
ADD COLUMN `alunoper_id` INT(10) UNSIGNED ZEROFILL NULL DEFAULT NULL,
ADD INDEX `fk_financeiro__titulo_acadperiodo__aluno1_idx` (`alunoper_id` ASC);

SET foreign_key_checks = 0;
SET SQL_SAFE_UPDATES = 0;

-- Efetua migração das matriculas de aluno em periodo letivo para os títulos relacionados
UPDATE financeiro__titulo t
  INNER JOIN financeiro__titulo_mensalidade fm USING (titulo_id)
SET t.alunoper_id = fm.alunoper_id;

SET SQL_SAFE_UPDATES = 1;

SET foreign_key_checks = 1;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;