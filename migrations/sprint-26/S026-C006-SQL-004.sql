-- MySQL Workbench Synchronization
-- Generated: 2017-05-01 16:50
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `documento__modelo`
ADD COLUMN `tipotitulo_id` INT(11) NULL DEFAULT NULL AFTER `area_id`,
ADD INDEX `fk_documento__modelo_financeiro__titulo_tipo1_idx` (`tipotitulo_id` ASC);

CREATE TABLE IF NOT EXISTS `documento__emissao` (
  `emissao_id` INT(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  `modelo_id` INT(10) ZEROFILL UNSIGNED NOT NULL,
  `pes_id` INT(11) UNSIGNED ZEROFILL NOT NULL,
  `emisaso_data` DATETIME NULL DEFAULT NULL,
  `usuario_id_emissao` INT(11) UNSIGNED ZEROFILL NOT NULL,
  INDEX `fk_documento__modelo_pessoa_pessoa1_idx` (`pes_id` ASC),
  INDEX `fk_documento__modelo_pessoa_documento__modelo1_idx` (`modelo_id` ASC),
  PRIMARY KEY (`emissao_id`),
  INDEX `fk_documento__emissao_acesso_pessoas1_idx` (`usuario_id_emissao` ASC),
  CONSTRAINT `fk_documento__modelo_pessoa_documento__modelo1`
    FOREIGN KEY (`modelo_id`)
    REFERENCES `documento__modelo` (`modelo_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_documento__modelo_pessoa_pessoa1`
    FOREIGN KEY (`pes_id`)
    REFERENCES `pessoa` (`pes_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_documento__emissao_acesso_pessoas1`
    FOREIGN KEY (`usuario_id_emissao`)
    REFERENCES `acesso_pessoas` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

ALTER TABLE `documento__modelo`
ADD CONSTRAINT `fk_documento__modelo_financeiro__titulo_tipo1`
  FOREIGN KEY (`tipotitulo_id`)
  REFERENCES `financeiro__titulo_tipo` (`tipotitulo_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE documento__emissao
CHANGE COLUMN `emisaso_data` `emissao_data` DATETIME NULL;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
