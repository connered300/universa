-- MySQL Workbench Synchronization
-- Generated: 2017-05-02 11:04
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `financeiro__titulo_tipo` 
CHANGE COLUMN `tipotitulo_tempo_vencimento` `tipotitulo_tempo_vencimento` INT(11) NULL DEFAULT 0 ,
ADD COLUMN `tipotitulo_tempo_vencimento_tipo` ENUM('DIA','MES') NULL DEFAULT 'DIA' AFTER `tipotitulo_descricao`;

ALTER TABLE `acadgeral__aluno`
ADD COLUMN `aluno_pessoa_indicacao` VARCHAR(255) NULL DEFAULT NULL AFTER `usuario_alteracao`,
ADD COLUMN `aluno_mediador` VARCHAR(255) NULL DEFAULT NULL AFTER `aluno_pessoa_indicacao`;

ALTER TABLE `acadgeral__aluno`
ADD CONSTRAINT `fk_acadgeral__aluno_arquivo1`
  FOREIGN KEY (`arq_id`)
  REFERENCES `arquivo` (`arq_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
