-- MySQL Workbench Synchronization
-- Generated: 2017-04-25 10:57
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE IF NOT EXISTS `documento__modelo` (
  `modelo_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `tdoc_id` INT(10) UNSIGNED ZEROFILL NOT NULL,
  `camp_id` INT(11) ZEROFILL NULL DEFAULT NULL,
  `curso_id` INT(11) ZEROFILL NULL DEFAULT NULL,
  `area_id` INT(10) UNSIGNED ZEROFILL NULL DEFAULT NULL,
  `modelo_data_inicio` DATE NOT NULL,
  `modelo_data_fim` DATE NOT NULL,
  `modelo_descricao` VARCHAR(150) NULL DEFAULT NULL,
  `modelo_conteudo` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`modelo_id`),
  INDEX `fk_documento__modelo_documento__tipo1_idx` (`tdoc_id` ASC),
  INDEX `fk_documento__modelo_org_campus1_idx` (`camp_id` ASC),
  INDEX `fk_documento__modelo_acad_curso1_idx` (`curso_id` ASC),
  INDEX `fk_documento__modelo_acadgeral__area_conhecimento1_idx` (`area_id` ASC),
  CONSTRAINT `fk_documento__modelo_documento__tipo1`
    FOREIGN KEY (`tdoc_id`)
    REFERENCES `documento__tipo` (`tdoc_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_documento__modelo_org_campus1`
    FOREIGN KEY (`camp_id`)
    REFERENCES `org_campus` (`camp_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_documento__modelo_acad_curso1`
    FOREIGN KEY (`curso_id`)
    REFERENCES `acad_curso` (`curso_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_documento__modelo_acadgeral__area_conhecimento1`
    FOREIGN KEY (`area_id`)
    REFERENCES `acadgeral__area_conhecimento` (`area_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

ALTER TABLE `documento__modelo` ADD COLUMN `modelo_contexto` ENUM('Aluno', 'Aluno Curso', 'Pessoa') NULL DEFAULT NULL AFTER `modelo_conteudo`;
UPDATE `documento__modelo` SET `modelo_contexto`='Aluno Curso' WHERE modelo_contexto IS NULL AND modelo_id>0;
ALTER TABLE `documento__modelo` CHANGE COLUMN `modelo_contexto` `modelo_contexto` ENUM('Aluno', 'Aluno Curso', 'Pessoa') NOT NULL DEFAULT 'Aluno Curso' AFTER `modelo_conteudo`;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
