SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE IF NOT EXISTS `sis__integracao_campus` (
  `integracao_campus_id` INT(11) NOT NULL AUTO_INCREMENT,
  `integracao_id` INT(11) ZEROFILL NOT NULL,
  `camp_id` INT(11) ZEROFILL NOT NULL,
  INDEX `fk_sis__integracao_org_campus_org_campus1_idx` (`camp_id` ASC),
  INDEX `fk_sis__integracao_org_campus_sis__integracao1_idx` (`integracao_id` ASC),
  PRIMARY KEY (`integracao_campus_id`),
  CONSTRAINT `fk_sis__integracao_org_campus_sis__integracao1`
    FOREIGN KEY (`integracao_id`)
    REFERENCES `sis__integracao` (`integracao_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_sis__integracao_org_campus_org_campus1`
    FOREIGN KEY (`camp_id`)
    REFERENCES `org_campus` (`camp_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
    )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;