--
-- módulo sistema
--
DELETE FROM `acesso_privilegios_grupo` WHERE acesso_actions_id IN(
	select acesso_actions.id from acesso_funcionalidades
	inner join acesso_actions on funcionalidade=acesso_funcionalidades.id
	where func_controller IN(
		'Documentos\\Controller\\DocumentoEmissao',
		'Documentos\\Controller\\DocumentoGrupo',
		'Documentos\\Controller\\DocumentoModelo',
		'Documentos\\Controller\\DocumentoTipo'
	)
) AND acesso_actions_id>=0 AND acesso_grupo_id>=0;

DELETE FROM `acesso_actions` WHERE funcionalidade in(
	SELECT acesso_funcionalidades.id from acesso_funcionalidades
	where func_controller IN(
		'Documentos\\Controller\\DocumentoEmissao',
		'Documentos\\Controller\\DocumentoGrupo',
		'Documentos\\Controller\\DocumentoModelo',
		'Documentos\\Controller\\DocumentoTipo'
	)
) and id>0;

DELETE FROM acesso_funcionalidades where func_controller IN(
		'Documentos\\Controller\\DocumentoEmissao',
		'Documentos\\Controller\\DocumentoGrupo',
		'Documentos\\Controller\\DocumentoModelo',
		'Documentos\\Controller\\DocumentoTipo'
) and id>0;

DELETE FROM acesso_modulos where mod_route='documentos/default' and id>0;

DELETE FROM acesso_sistemas where sis_nome='Documentos' and id>0;

REPLACE INTO `acesso_sistemas` (`sis_nome`, `sis_status`, `sis_icon`, `sis_info`)
                       VALUES ('Documentos', 'Ativo', 'fa-file', 'Sistema');


REPLACE INTO `acesso_modulos` (`sistema`, `mod_nome`, `mod_route`, `mod_status`, `mod_icon`, `mod_info`, `mod_redireciona`, `mod_prioridade`)
SELECT acesso_sistemas.id, 'Documentos', 'documentos/default', 'Ativo', 'fa fa-file', 'Modúlo Documentos', '/documentos', round(acesso_sistemas.id/10) from acesso_sistemas
where sis_nome like 'Documentos';

REPLACE INTO `acesso_funcionalidades` (modulo, func_nome, func_controller, func_status, func_visivel)
SELECT distinct acesso_modulos.id, func.descricao, func.controller,'Ativa','Sim'  from acesso_modulos,
(

	select 'Histórico de Documentos Emitidos' as descricao, 'Documentos\\Controller\\DocumentoEmissao' as controller from dual union all
	select 'Gestão de Documentos de Grupos' as descricao, 'Documentos\\Controller\\DocumentoGrupo' as controller from dual union all
	select 'Gestão de Modelos de Documentos' as descricao, 'Documentos\\Controller\\DocumentoModelo' as controller from dual union all
	select 'Gestão de Tipos de Documentos' as descricao, 'Documentos\\Controller\\DocumentoTipo' as controller from dual
) as func
where mod_nome like 'Documentos';

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
(
	select 'index' as act,'Listagem' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'remove' as act,'Remover' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'add' as act,'Criar' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'edit' as act,'Editar' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'search-for-json' as act, 'Autocomplete' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'search' as act,'Datatables' as descricao,'Leitura' as tipo,'N' as usr from dual
) as actions
where func_controller IN(
		'Documentos\\Controller\\DocumentoEmissao',
		'Documentos\\Controller\\DocumentoGrupo',
		'Documentos\\Controller\\DocumentoModelo',
		'Documentos\\Controller\\DocumentoTipo'
);
REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
acesso_grupo
where func_controller IN(
		'Documentos\\Controller\\DocumentoEmissao',
		'Documentos\\Controller\\DocumentoGrupo',
		'Documentos\\Controller\\DocumentoModelo',
		'Documentos\\Controller\\DocumentoTipo'
)
and grup_nome LIKE 'Admin%';
