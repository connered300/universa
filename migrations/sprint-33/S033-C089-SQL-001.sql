-- SQL PARA ADICIONAR A NOVA SITUAÇÃO

ALTER TABLE financeiro__titulo
CHANGE COLUMN `titulo_estado` `titulo_estado` ENUM('Pago', 'Aberto', 'Cancelado', 'Estorno', 'Alteracao', 'Pagamento Múltiplo', 'Estorno De Pagamento Múltiplo') NULL DEFAULT NULL;

-- SQL PARA MIGRAÇÃO DA NOVA SITUAÇÃO

/* Sql para mudar a situação dos filhos*/
SET SQL_SAFE_UPDATES = 0;

UPDATE financeiro__titulo AS tt
SET tt.titulo_estado = 'Estorno De Pagamento Múltiplo'
WHERE
  tt.titulo_id IN (
    SELECT *
    FROM (
           -- Buscando os títulos filhos
           SELECT titulo_id_origem
           FROM financeiro__titulo_alteracao filhos
           WHERE filhos.titulo_id_origem IN (
             SELECT filhos.titulo_id_origem
             FROM financeiro__titulo filhos_t
             WHERE filhos_t.titulo_estado = 'Pagamento Múltiplo'
           ) AND filhos.titulo_id_destino IN (
             -- Buscando os títulos pais
             SELECT titulo.titulo_id
             FROM financeiro__titulo titulo
             WHERE
               titulo.titulo_estado = 'Estorno' AND
               titulo.titulo_id IN (
                 SELECT alteracao.titulo_id_destino
                 FROM financeiro__titulo_alteracao alteracao
               )
           )
         ) AS t
  );

SET SQL_SAFE_UPDATES = 1;