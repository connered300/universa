ALTER TABLE financeiro__titulo
ADD COLUMN `titulo_incluir_remessa` ENUM('Sim', 'Não') NOT NULL DEFAULT 'Sim';
ALTER TABLE financeiro__remessa_lote
ADD COLUMN `lote_movimento` INT(2) NOT NULL DEFAULT '01';

UPDATE financeiro__titulo
  INNER JOIN boleto USING (titulo_id)
  LEFT JOIN financeiro__desconto_titulo USING (titulo_id)
  LEFT JOIN financeiro__remessa_lote USING (bol_id)
SET titulo_incluir_remessa = 'Não'
WHERE financeiro__remessa_lote.bol_id IS NOT NULL AND financeiro__desconto_titulo.desconto_id IS NULL AND titulo_id > 0;