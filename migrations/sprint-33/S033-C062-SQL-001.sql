-- view com informações sobre os títulos
DROP VIEW IF EXISTS `view__financeiro_titulos`;
CREATE OR REPLACE VIEW `view__financeiro_titulos` AS
  SELECT
    t.titulo_id,
    t.pes_id,
    p.pes_nome,
    t.tipotitulo_id,
    tt.tipotitulo_nome,
    t.usuario_baixa,
    ub.login       usuario_baixa_login,
    t.usuario_autor,
    ua.login       usuario_autor_login,
    t.titulo_novo,
    t.alunocurso_id,
    ac.aluno_id,
    a.pes_id as pes_id_aluno,
    c.curso_nome,
    c.curso_sigla,
    t.titulo_descricao,
    t.titulo_parcela,
    t.titulo_valor_pago,
    t.titulo_valor_pago_dinheiro,
    t.titulo_multa,
    t.titulo_juros,
    t.titulo_desconto,
    t.titulo_observacoes,
    t.titulo_desconto_manual,
    t.titulo_acrescimo_manual,
    t.titulo_data_processamento,
    t.titulo_data_vencimento,
    t.titulo_data_pagamento,
    t.titulo_data_baixa,
    t.titulo_tipo_pagamento,
    t.titulo_valor,
    t.titulo_estado,
    t.financeiro_desconto_incentivo,
    cc.cursocampus_id,
    greatest(
        if(
            titulo_data_pagamento IS NULL,
            datediff(date(now()), date(t.titulo_data_vencimento)),
            datediff(date(titulo_data_pagamento), date(t.titulo_data_vencimento))
        ),
        0
    )           AS dias_atraso,
    coalesce(
        if(titulo_data_pagamento IS NULL AND
           greatest(
               if(
                   titulo_data_pagamento IS NULL,
                   datediff(date(now()), date(t.titulo_data_vencimento)),
                   datediff(date(titulo_data_pagamento), date(t.titulo_data_vencimento))
               ),
               0
           ) > 0,
           -- configuração de juros e multa para título
             coalesce(tituloconf_multa, 0) * t.titulo_valor,
           titulo_multa
        ),
        0
    )           AS titulo_multa_calc,
    if(
        titulo_data_pagamento IS NULL,
        -- configuração de juros e multa para título
        coalesce(tituloconf_juros, 0) * greatest(
            if(
                titulo_data_pagamento IS NULL,
                datediff(date(now()), date(t.titulo_data_vencimento)),
                datediff(date(titulo_data_pagamento), date(t.titulo_data_vencimento))
            ),
            0
        ),
        coalesce(titulo_juros, 0)
    )AS titulo_juros_calc,

    (
      SELECT
      coalesce(
      ROUND(SUM(IF(desconto_aplicacao_valor>0,desconto_aplicacao_valor,coalesce((titulo_valor*(desconto__percentual_calc/100)),0)))+ coalesce(t.financeiro_desconto_incentivo,0),2)
      ,0)

      FROM view__financeiro_descontos_titulos_atualizada fdt
      WHERE fdt.titulo_id = t.titulo_id
    )AS descontos,

    tc.tituloconf_id,
    tc.tituloconf_dia_venc,
    tc.tituloconf_dia_desc,
    tc.tituloconf_valor_desc,
    tc.tituloconf_percent_desc,
    tc.tituloconf_multa,
    tc.tituloconf_juros,
    tc.confcont_id,
    b.bol_id
  FROM financeiro__titulo t
    INNER JOIN financeiro__titulo_tipo tt ON t.tipotitulo_id = tt.tipotitulo_id
    -- dados aluno
    INNER JOIN pessoa p ON t.pes_id = p.pes_id
    -- curso aluno
    LEFT JOIN acadgeral__aluno_curso ac ON ac.alunocurso_id = t.alunocurso_id
    LEFT JOIN acadgeral__aluno a ON a.aluno_id = ac.aluno_id
    LEFT JOIN campus_curso cc ON cc.cursocampus_id = ac.cursocampus_id
    LEFT JOIN acad_curso c ON c.curso_id = cc.curso_id
    -- boleto
    LEFT JOIN boleto b ON t.titulo_id = b.titulo_id
    -- usuário baixa
    LEFT JOIN acesso_pessoas ua ON ua.id = t.usuario_autor
    LEFT JOIN acesso_pessoas ub ON ub.id = t.usuario_baixa
    LEFT JOIN financeiro__titulo_config AS tc ON tc.tituloconf_id=t.tituloconf_id
  -- GROUP BY titulo_id
  ORDER BY
    titulo_data_vencimento DESC,
    titulo_data_processamento DESC,
    titulo_data_pagamento DESC,
    alunocurso_id ASC,
    tituloconf_id ASC;