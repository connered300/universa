DELETE  from `acesso_funcionalidades` where func_controller= 'Matricula\\Controller\\AcadgeralDocente';
REPLACE INTO `acesso_funcionalidades` (modulo, func_nome, func_controller, func_status, func_visivel)
  SELECT distinct acesso_modulos.id, func.descricao, func.controller,'Ativa','Sim'  from acesso_modulos,
    (
      select 'Gestão de Docentes' as descricao, 'Matricula\\Controller\\AcadgeralDocente' as controller from dual
    ) as func
  where mod_nome like 'Secretaria';

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
    (
      select 'index' as act,'Listagem' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'search' as act,'Dados da Listagem' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'edit' as act,'Edição' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'add' as act,'Novo Registro' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'remove' as act,'Novo Registro' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'search-fo-json' as act,'Pesquisa' as descricao,'Leitura' as tipo,'N' as usr
    ) as actions
  where func_controller IN('Matricula\\Controller\\AcadgeralDocente');

REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
  select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
    inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
    acesso_grupo
  where func_controller IN(
    'Matricula\\Controller\\AcadgeralDocente'
  )
        and grup_nome LIKE 'admin%';