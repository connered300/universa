-- -----------------------------------------
-- Nova view que cálcula apenas os descontos
-- válidos e que podem ser aplicados aos
-- títulos
-- -----------------------------------------

DROP VIEW IF EXISTS `view__financeiro_descontos_titulos_atualizada`;
CREATE VIEW `view__financeiro_descontos_titulos_atualizada` AS
  SELECT
    `fdt`.`desconto_titulo_id`             AS `desconto_titulo_id`,
    `fdt`.`desconto_id`                    AS `desconto_id`,
    `fdt`.`titulo_id`                      AS `titulo_id`,
    `fdtp`.`desctipo_desconto_acumulativo` AS `desctipo_desconto_acumulativo`,
    `fdtp`.`desctipo_limita_vencimento`,
    `ft`.`titulo_data_vencimento`          AS `titulo_data_vencimento`,
    `fd`.`desconto_valor`                  AS `desconto_valor`,
    `fd`.`desconto_percentual`             AS `desconto_percentual`,
    `fd`.`aluno_id`                        AS `aluno_id`,
    `fdtp`.`desctipo_id`                   AS `desctipo_id`,
    `ft`.`titulo_valor`                    AS `titulo_valor`,
    ft.alunoper_id                         AS alunoper_id,
    ft.alunocurso_id                       AS alunocurso_id,
    ft.tipotitulo_id                       AS tipotitulo_id,
    ROUND(
        IF(fd.desconto_valor, fd.desconto_valor,
           ((`ft`.`titulo_valor` * (COALESCE(`fd`.`desconto_percentual`, 0) / 100))))
        , 2)                                  desconto__valor_calc,

    ROUND(
        IF(
            (`ft`.`titulo_estado` != 'Aberto'),
            -- Se o t´titulo está fechado, pega o valor aplicado ou 0
            IF((`fdt`.`desconto_titulo_aplicado` = 'Sim'), `fdt`.`desconto_titulo_valor`, 0),
            IF(
            -- Verifica se o desconto é limitado ao vencimento e se o título já venceu
                (`fdtp`.`desctipo_limita_vencimento` = 'Sim' AND `ft`.`titulo_data_vencimento` < curdate()),
                0,
                -- Joga o valor do desconto ou calcula o desconto sobre o valor bruto do título
                IF((`fd`.`desconto_valor` > 0), `fd`.`desconto_valor`,
                   (`ft`.`titulo_valor` * (`fd`.`desconto_percentual` / 100)))
            )
        ),
        2
    )                                      AS `desconto_aplicacao_valor`,
    ROUND(
        coalesce(
            if(`fd`.`desconto_percentual` > 0, `fd`.`desconto_percentual`,
               ((fd.desconto_valor * 100) / ft.titulo_valor)), 0
        ), 2
    )                                      AS desconto__percentual_calc

  FROM `financeiro__desconto_titulo` `fdt`
    JOIN `financeiro__desconto` `fd` ON `fdt`.`desconto_id` = `fd`.`desconto_id`
    JOIN `financeiro__desconto_tipo` `fdtp` ON `fd`.`desctipo_id` = `fdtp`.`desctipo_id`
    LEFT JOIN `financeiro__titulo` `ft` ON `fdt`.`titulo_id` = `ft`.`titulo_id`
  WHERE `fd`.`desconto_status` != 'Inativo' AND `fdt`.`desconto_titulo_situacao` = 'Deferido';
