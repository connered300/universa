REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
    (
      select 'remove' as act,'Remover' as descricao,'Escrita' as tipo,'N' as usr
    ) as actions
  where func_controller IN('Atividades\\Controller\\AlunoAtividades');

  REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
  select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
    inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
    acesso_grupo
  where func_controller IN(
    'Atividades\\Controller\\AlunoAtividades'

  )
  and grup_nome LIKE 'admin%';