SET SQL_SAFE_UPDATES = 0;

ALTER TABLE `boleto`
ADD COLUMN `bol_carteira` ENUM('14', '24') NOT NULL DEFAULT '14';

UPDATE
boleto
INNER JOIN financeiro__titulo USING (titulo_id)
SET bol_carteira = '24'
WHERE
YEAR(titulo_data_vencimento) <= 2018 AND
MONTH(titulo_data_vencimento) <= 6 AND
bol_estado IN ('Aberto');

ALTER TABLE `boleto`
CHANGE COLUMN `bol_carteira` `bol_carteira` ENUM('14', '24') NOT NULL DEFAULT '14' ;

SET SQL_SAFE_UPDATES = 1;