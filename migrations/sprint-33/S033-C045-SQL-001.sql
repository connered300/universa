select @mod:=id from acesso_modulos where mod_nome like 'Aluno%';

REPLACE INTO acesso_funcionalidades VALUES (null, @mod, 'Biblioteca', 'Aluno\\Controller\\Biblioteca', '', null, 'Ativa', 'Sim');

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
    (
      select 'index' as act,'Listagem' as descricao,'Leitura' as tipo,'N' as usr from dual union all
      select 'search' as act,'Datatables' as descricao,'Leitura' as tipo,'N' as usr from dual
    ) as actions
  where func_controller IN(
    'Aluno\\Controller\\Biblioteca'
  );


REPLACE INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id,
    acesso_grupo
  WHERE func_controller IN (
    'Aluno\\Controller\\Biblioteca'
  )
        AND grup_nome LIKE 'Aluno%';