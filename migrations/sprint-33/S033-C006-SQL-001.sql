SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS  atividadegeral__tipo;
SET FOREIGN_KEY_CHECKS=1;

 CREATE TABLE IF NOT EXISTS `atividadeperiodo__evento` (
  `evento_id` INT  primary KEY AUTO_INCREMENT ,
  `evento_secretaria` int(11) NOT NULL,
  `evento_nome` VARCHAR(45) NOT NULL,
  `evento_horas_validas` INT NOT NULL,
  `evento_lotacao` INT NULL,
  `evento_responsavel` VARCHAR(45) NULL,
  `evento_tipo_atividade` INT(10) UNSIGNED NOT NULL,
  `evento_data` DATETIME NULL,
   evento_periodo_letivo INT(10) UNSIGNED NOT NULL,
  `evento_tipo` ENUM('Interno', 'Externo') NOT NULL,
  `evento_descricao` TEXT NULL,
  `usuario_cadastro` INT(11) UNSIGNED ZEROFILL NOT NULL,
  INDEX `fk_atividadeperiodo__evento_atividadegeral__atividades1_idx` (`evento_tipo_atividade` ASC),
  INDEX `fk_atividadeperiodo__evento_acesso_pessoas1_idx` (`usuario_cadastro` ASC),
  INDEX `fk_atividadeperiodo__evento_secretaria_secretaria1_idx` (`evento_secretaria` ASC),
  INDEX `fk_atividadeperiodo__evento_periodo_letivo_idx` (`evento_periodo_letivo` ASC),

 CONSTRAINT `fk_atividadeperiodo__evento_secretaria_secretaria1_idx`
    FOREIGN KEY (`evento_secretaria`)
    REFERENCES `atividadegeral__secretaria` (`secretaria_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
 CONSTRAINT `fk_atividadeperiodo__evento_atividadegeral__atividades1_idx`
    FOREIGN KEY (`evento_tipo_atividade`)
    REFERENCES `atividadegeral__atividades` (`atividadeatividade_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
 CONSTRAINT `fk_atividadeperiodo__evento_acesso_pessoas1`
    FOREIGN KEY (`usuario_cadastro`)
    REFERENCES `acesso_pessoas` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
CONSTRAINT `fk_atividadeperiodo__evento_periodo_letivo_idx`
    FOREIGN KEY (`evento_periodo_letivo`)
    REFERENCES `acadperiodo__letivo` (`per_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
    )ENGINE = InnoDB;

/*
  SE JÁ ESTIVER SIDO CRIADA APENAS SQL DE CORREÇÃO PARA RESTRIÇÃO DA CHAVE REFERENTE AO EVENTO_TIPO_ATIVIDADE
*/

ALTER TABLE `atividadeperiodo__evento` DROP FOREIGN KEY `fk_atividadeperiodo__evento_atividadegeral__tipo1`;
ALTER TABLE `atividadeperiodo__evento` ADD INDEX `fk_atividadeperiodo__evento_atividadegeral__atividades1_idx_idx` (`evento_tipo_atividade` ASC);
ALTER TABLE `atividadeperiodo__evento` DROP INDEX `fk_atividadeperiodo__evento_atividadegeral__tipo1_idx` ;
ALTER TABLE `atividadeperiodo__evento`
ADD CONSTRAINT `fk_atividadeperiodo__evento_atividadegeral__atividades1_idx`
FOREIGN KEY (`evento_tipo_atividade`)
REFERENCES `atividadegeral__atividades` (`atividadeatividade_id`)
ON DELETE RESTRICT
ON UPDATE CASCADE;


CREATE TABLE IF NOT EXISTS `atividadeperiodo__aluno` (
  `aluno_atividade_id` INT PRIMARY KEY AUTO_INCREMENT,
  `usuario_cadastro` INT(11) UNSIGNED ZEROFILL NOT NULL,
  `aluno_atividade_data_lancamento` DATETIME NOT NULL,
  `alunoper_id` INT(10) UNSIGNED ZEROFILL NULL DEFAULT NULL,
  `evento_id` INT NOT NULL ,
  `aluno_atividade_data_realizacao` DATETIME NOT NULL,
  `aluno_atividade_horas_reais` INT NOT NULL,
  `aluno_atividade_horas_validas` INT NOT NULL,
  `aluno_atividade_observacao` VARCHAR(45) NULL,
  INDEX `fk_atividadeperiodo__aluno_acadperiodo__aluno1_idx` (`alunoper_id` ASC),
  INDEX `fk_atividadeperiodo__aluno_atividadeperiodo__evento1_idx` (`evento_id` ASC),
  INDEX `fk_atividadeperiodo__aluno_acesso_pessoas1_idx` (`usuario_cadastro` ASC),

  CONSTRAINT `fk_atividadeperiodo__aluno_acadperiodo__aluno1`
    FOREIGN KEY (`alunoper_id`)
    REFERENCES `acadperiodo__aluno` (`alunoper_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,

  CONSTRAINT `fk_atividadeperiodo__aluno_atividadeperiodo__evento1`
    FOREIGN KEY (`evento_id`)
    REFERENCES `atividadeperiodo__evento` (`evento_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,

  CONSTRAINT `fk_atividadeperiodo__aluno_acesso_pessoas1`
    FOREIGN KEY (`usuario_cadastro`)
    REFERENCES `acesso_pessoas` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)

ENGINE = InnoDB;


/*
Se a tabela já estiver sido criada
*/

ALTER TABLE `atividadeperiodo__aluno`
CHANGE COLUMN `evento_data_lancamento` `aluno_atividade_data_lancamento` DATETIME NOT NULL;
ALTER TABLE `atividadeperiodo__aluno`
CHANGE COLUMN `evento_data_realizacao` `aluno_atividade_data_realizacao` DATETIME NOT NULL;
ALTER TABLE `atividadeperiodo__aluno`
CHANGE COLUMN `evento_horas_reais` `aluno_atividade_horas_reais` INT(11) NOT NULL;
ALTER TABLE `atividadeperiodo__aluno`
CHANGE COLUMN `evento_horas_validas` `aluno_atividade_horas_validas` INT(11) NOT NULL ;


/*
Se o modulo de Atividades Praticas não estiver visivel executar o script S024-C002-SQL-002.sql
*/



REPLACE INTO `acesso_funcionalidades` (modulo, func_nome, func_controller, func_status, func_visivel)
  SELECT distinct acesso_modulos.id, func.descricao, func.controller,'Ativa','Sim'  from acesso_modulos,
    (
      select 'Atividades' as descricao, 'Atividades\\Controller\\AtividadePeriodoAluno' as controller from dual
    ) as func
  where mod_nome like 'Atividades Práticas';

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
    (
      select 'index' as act,'Listagem' as descricao,'Leitura' as tipo,'N' as usr  union all
      select 'add' as act,'Criar' as descricao,'Escrita' as tipo,'N' as usr  union all
      select 'edit' as act,'Editar' as descricao,'Escrita' as tipo,'N' as usr  union all
      select 'remove' as act,'Remover' as descricao,'Escrita' as tipo,'N' as usr  from dual
    ) as actions
  where func_controller IN('Atividades\\Controller\\AtividadePeriodoAluno');

REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
  select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
    inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
    acesso_grupo
  where func_controller IN(
    'Atividades\\Controller\\AtividadePeriodoAluno'
  )
  and grup_nome LIKE 'admin%';


  REPLACE INTO `acesso_funcionalidades` (modulo, func_nome, func_controller, func_status, func_visivel)
  SELECT distinct acesso_modulos.id, func.descricao, func.controller,'Ativa','Sim'  from acesso_modulos,
    (
      select 'Eventos' as descricao, 'Atividades\\Controller\\AtividadePeriodoEvento' as controller from dual
    ) as func
  where mod_nome like 'Atividades Práticas';

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
    (
      select 'index' as act,'Listagem' as descricao,'Leitura' as tipo,'N' as usr  union all
      select 'add' as act,'Criar' as descricao,'Escrita' as tipo,'N' as usr  union all
      select 'edit' as act,'Editar' as descricao,'Escrita' as tipo,'N' as usr  union all
      select 'remove' as act,'Remover' as descricao,'Escrita' as tipo,'N' as usr  from dual
    ) as actions
  where func_controller IN('Atividades\\Controller\\AtividadePeriodoEvento');

REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
  select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
    inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
    acesso_grupo
  where func_controller IN(
    'Atividades\\Controller\\AtividadePeriodoEvento'
  )
  and grup_nome LIKE 'admin%';



/* ATENÇÃO FALTOU ESSA COLUNA E NÃO HÁ LOCALIZEI NAS MIGRATIONS, NÃO SEI SE AINDA ESTA EM USO OU VOU ADICIONADA RECENTEMENTE*/

ALTER TABLE `atividadeperiodo__aluno_nucleo`
ADD COLUMN `alunoperiodo_status` ENUM('Ativo', 'Inativo') NOT NULL DEFAULT 'Ativo' ;
