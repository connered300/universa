/* Corrige títulos que foram alterados para Estorno De pagam... quando era Estorno*/
SET SQL_SAFE_UPDATES = 0;
UPDATE financeiro__titulo AS tt
SET tt.titulo_estado = 'Estorno'
WHERE tt.titulo_id IN (
  SELECT titulo_id_origem
  FROM
    (
      SELECT
        financeiro__titulo_alteracao.*,
        count(titulo_id_origem) AS contTitulo,
        financeiro__titulo.alunocurso_id,
        financeiro__titulo.pes_id
      FROM financeiro__titulo_alteracao
        INNER JOIN financeiro__titulo ON titulo_id_origem = titulo_id
      -- Nesssa data a demanda foi finalizada
      WHERE titulo_estado = 'Estorno De Pagamento Múltiplo' AND alteracao_acao='Estorno'
      GROUP BY financeiro__titulo_alteracao.titulo_id_origem
      -- Onde o título só pode ter 1 alteração ou seja nao tem filhos
      HAVING contTitulo < 2
    ) AS t
);
SET SQL_SAFE_UPDATES = 1;

SET SQL_SAFE_UPDATES = 0;
UPDATE financeiro__titulo AS tt
SET tt.titulo_estado = 'Alteracao'
WHERE tt.titulo_id IN (
  SELECT titulo_id_origem
  FROM
    (
      SELECT
        financeiro__titulo_alteracao.*,
        count(titulo_id_origem) AS contTitulo,
        financeiro__titulo.alunocurso_id,
        financeiro__titulo.pes_id
      FROM financeiro__titulo_alteracao
        INNER JOIN financeiro__titulo ON titulo_id_origem = titulo_id
      -- Nesssa data a demanda foi finalizada
      WHERE titulo_estado = 'Estorno De Pagamento Múltiplo' AND alteracao_acao='Renegociacao'
      GROUP BY financeiro__titulo_alteracao.titulo_id_origem
      -- Onde o título só pode ter 1 alteração ou seja nao tem filhos
      HAVING contTitulo < 2
    ) AS t
);

SET SQL_SAFE_UPDATES = 1;