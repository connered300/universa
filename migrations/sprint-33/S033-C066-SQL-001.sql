REPLACE INTO `acesso_funcionalidades` (modulo, func_nome, func_controller, func_status, func_visivel)
  SELECT distinct acesso_modulos.id, func.descricao, func.controller,'Ativa','Sim'  from acesso_modulos,
    (
      select 'Informações cheque' as descricao, 'Financeiro\\Controller\\FinanceiroCheque' as controller from dual
    ) as func
  where mod_nome like 'Financeiro Cheque';

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
    (
      select 'retorna-dados-cheque' as act,'retorna-dados-cheque' as descricao,'Leitura' as tipo,'N' as usr
    ) as actions
  where func_controller IN('Financeiro\\Controller\\FinanceiroCheque');

REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
  select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
    inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
    acesso_grupo
  where func_controller IN(
    'Financeiro\\Controller\\FinanceiroCheque'
  )
  and grup_nome LIKE 'admin%';