ALTER TABLE org_ies
  ADD COLUMN `ies_credenciamento` TEXT DEFAULT NULL;

ALTER TABLE campus_curso
  ADD COLUMN cursocampus_autorizacao TEXT DEFAULT NULL,
  ADD COLUMN cursocampus_reconhecimento TEXT DEFAULT NULL;

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT DISTINCT
    acesso_funcionalidades.id,
    actions.act,
    actions.descricao,
    actions.tipo,
    actions.usr
  FROM acesso_funcionalidades,
    (
      SELECT
        'emissao-certificados'    AS act,
        'Emissão de certificados' AS descricao,
        'Leitura'  AS tipo,
        'N'        AS usr
      FROM dual
    ) AS actions
  WHERE func_controller IN (
    'Matricula\\Controller\\Relatorios'
  );

REPLACE INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
    ,
    acesso_grupo
  WHERE func_controller IN (
    'Matricula\\Controller\\Relatorios'
  )
        AND grup_nome LIKE 'Admin%';