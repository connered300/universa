CREATE TABLE IF NOT EXISTS `acesso_pessoas_campus_curso` (
  `acesso_pessoas_id` INT(11) UNSIGNED ZEROFILL NOT NULL,
  `cursocampus_id` INT ZEROFILL UNSIGNED NOT NULL,
  PRIMARY KEY (`acesso_pessoas_id`, `cursocampus_id`),
  INDEX `fk_acesso_pessoas_campus_curso_campus_curso1_idx` (`cursocampus_id` ASC),
  INDEX `fk_acesso_pessoas_campus_curso_acesso_pessoas1_idx` (`acesso_pessoas_id` ASC),
  CONSTRAINT `fk_acesso_pessoas_campus_curso_acesso_pessoas1`
    FOREIGN KEY (`acesso_pessoas_id`)
    REFERENCES `acesso_pessoas` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_acesso_pessoas_campus_curso_campus_curso1`
    FOREIGN KEY (`cursocampus_id`)
    REFERENCES `campus_curso` (`cursocampus_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;