REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT DISTINCT
    acesso_funcionalidades.id,
    actions.act,
    actions.descricao,
    actions.tipo,
    actions.usr
  FROM acesso_funcionalidades,
    (
      SELECT
        'baixa'    AS act,
        'Realizar baixa de multas' AS descricao,
        'Leitura'  AS tipo,
        'N'        AS usr
      FROM dual
    ) AS actions
  WHERE func_controller IN (
    'Biblioteca\\Controller\\Multa'
  );

  REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
acesso_grupo
where func_controller IN(
		'Biblioteca\\Controller\\Multa'
)
and grup_nome LIKE 'admin%';