ALTER TABLE `financeiro__titulo_config`
ADD COLUMN `tituloconf_dia_desc2` INT(11) NULL DEFAULT NULL AFTER `tituloconf_nome`,
ADD COLUMN `tituloconf_valor_desc2` FLOAT NULL DEFAULT NULL AFTER `tituloconf_dia_desc2`,
ADD COLUMN `tituloconf_percent_desc2` FLOAT NULL DEFAULT NULL AFTER `tituloconf_valor_desc2`;

ALTER TABLE `financeiro__titulo`
ADD COLUMN `financeiro_desconto_incentivo` FLOAT(10,2) NULL DEFAULT '0.00';