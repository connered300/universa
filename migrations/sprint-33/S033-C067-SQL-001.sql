ALTER TABLE `acad_curso_config`
ADD COLUMN `cursoconfig_matriz_curricular` INT(10) ZEROFILL NULL;

ALTER TABLE `acad_curso_config`
ADD INDEX `fk_acad_curso_config_1_idx` (`cursoconfig_matriz_curricular` ASC);
ALTER TABLE `acad_curso_config`
ADD CONSTRAINT `fk_acad_curso_config_1`
FOREIGN KEY (`cursoconfig_matriz_curricular`)
REFERENCES `acadperiodo__matriz_curricular` (`mat_cur_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
