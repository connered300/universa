/*biblioteca__grupo_bibliografico*/
ALTER TABLE `biblioteca__grupo_bibliografico` ADD COLUMN `grupo_bibliografico_local_edicao` ENUM('Sim', 'Não') NOT NULL DEFAULT 'Sim'
AFTER `grupo_bibliografico_situacao`;
ALTER TABLE `biblioteca__grupo_bibliografico` ADD COLUMN `grupo_bibliografico_exemplares` ENUM('Sim', 'Não') NOT NULL DEFAULT 'Sim'
AFTER `grupo_bibliografico_local_edicao`;
ALTER TABLE `biblioteca__grupo_bibliografico` ADD COLUMN `grupo_bibliografico_autor` ENUM('Sim', 'Não') NOT NULL DEFAULT 'Sim'
AFTER `grupo_bibliografico_exemplares`;
ALTER TABLE `biblioteca__grupo_bibliografico` ADD COLUMN `grupo_bibliografico_acesso_portal` ENUM('Sim', 'Não') NOT NULL DEFAULT 'Sim'
AFTER `grupo_bibliografico_autor`;

/*biblioteca__titulo*/
ALTER TABLE `biblioteca__titulo` ADD COLUMN `titulo_acesso_portal` ENUM('Sim', 'Não') NOT NULL DEFAULT 'Sim';
ALTER TABLE `biblioteca__titulo` ADD COLUMN `arq_id_arquivo` INT(10) ZEROFILL UNIQUE;
ALTER TABLE `biblioteca__titulo` ADD COLUMN `arq_id_capa` INT(10) ZEROFILL UNIQUE;


ALTER TABLE `biblioteca__titulo`
ADD CONSTRAINT `fk_biblioteca__titulo_imagem`
FOREIGN KEY (`arq_id_capa`)
REFERENCES `arquivo` (`arq_id`)
  ON DELETE SET NULL
  ON UPDATE CASCADE;

ALTER TABLE `biblioteca__titulo`
ADD CONSTRAINT `fk_biblioteca__arq_id_arquivo`
FOREIGN KEY (`arq_id_arquivo`)
REFERENCES `arquivo` (`arq_id`)
  ON DELETE SET NULL
  ON UPDATE CASCADE;


/*SQL Permissão*/

REPLACE INTO `acesso_funcionalidades` (modulo, func_nome, func_controller, func_status, func_visivel)
  SELECT DISTINCT
    acesso_modulos.id,
    func.descricao,
    func.controller,
    'Ativa',
    'Sim'
  FROM acesso_modulos,
    (
      SELECT
        'Grupo Bibliográfico'                        AS descricao,
        'Biblioteca\\Controller\\GrupoBibliografico' AS controller
      FROM dual
    ) AS func
  WHERE mod_nome LIKE 'Biblioteca';

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT DISTINCT
    acesso_funcionalidades.id,
    actions.act,
    actions.descricao,
    actions.tipo,
    actions.usr
  FROM acesso_funcionalidades,
    (
      SELECT
        'index'    AS act,
        'Listagem' AS descricao,
        'Leitura'  AS tipo,
        'N'        AS usr
      UNION ALL
      SELECT
        'add'     AS act,
        'Criar'   AS descricao,
        'Escrita' AS tipo,
        'N'       AS usr
      UNION ALL
      SELECT
        'edit'    AS act,
        'Editar'  AS descricao,
        'Escrita' AS tipo,
        'N'       AS usr
      UNION ALL
      SELECT
        'remove'  AS act,
        'Remover' AS descricao,
        'Escrita' AS tipo,
        'N'       AS usr
      FROM dual
    ) AS actions
  WHERE func_controller IN ('Biblioteca\\Controller\\GrupoBibliografico');

REPLACE INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
    ,
    acesso_grupo
  WHERE func_controller IN (
    'Biblioteca\\Controller\\GrupoBibliografico'
  )
        AND grup_nome LIKE 'admin%';


REPLACE INTO `acesso_funcionalidades` (modulo, func_nome, func_controller, func_status, func_visivel)
  SELECT DISTINCT
    acesso_modulos.id,
    func.descricao,
    func.controller,
    'Ativa',
    'Sim'
  FROM acesso_modulos,
    (
      SELECT
        'Grupo Leitor'                        AS descricao,
        'Biblioteca\\Controller\\GrupoLeitor' AS controller
      FROM dual
    ) AS func
  WHERE mod_nome LIKE 'Biblioteca';

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT DISTINCT
    acesso_funcionalidades.id,
    actions.act,
    actions.descricao,
    actions.tipo,
    actions.usr
  FROM acesso_funcionalidades,
    (
      SELECT
        'index'    AS act,
        'Listagem' AS descricao,
        'Leitura'  AS tipo,
        'N'        AS usr
      UNION ALL
      SELECT
        'add'     AS act,
        'Criar'   AS descricao,
        'Escrita' AS tipo,
        'N'       AS usr
      UNION ALL
      SELECT
        'edit'    AS act,
        'Editar'  AS descricao,
        'Escrita' AS tipo,
        'N'       AS usr
      UNION ALL
      SELECT
        'remove'  AS act,
        'Remover' AS descricao,
        'Escrita' AS tipo,
        'N'       AS usr
      FROM dual
    ) AS actions
  WHERE func_controller IN ('Biblioteca\\Controller\\GrupoLeitor');

REPLACE INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
    ,
    acesso_grupo
  WHERE func_controller IN (
    'Biblioteca\\Controller\\GrupoLeitor'
  )
        AND grup_nome LIKE 'admin%';


