SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE `avaliacao__fim` (
  `fim_id`        VARCHAR(20) NOT NULL,
  `fim_nome`      VARCHAR(45)  DEFAULT NULL,
  `fim_descricao` VARCHAR(250) DEFAULT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Extraindo dados da tabela `avaliacao__fim`
--

INSERT INTO `avaliacao__fim` (`fim_id`, `fim_nome`, `fim_descricao`) VALUES
  ('CPAAluno', 'CPA Aluno', ''),
  ('CPADocente', 'CPA Docente', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `avaliacao__questao`
--

CREATE TABLE `avaliacao__questao` (
  `questao_id`        INT(11)     NOT NULL,
  `questaotipo_id`    VARCHAR(20) NOT NULL,
  `aplicacao_id`      VARCHAR(20) NOT NULL,
  `questao_descricao` VARCHAR(255) DEFAULT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `avaliacao__questao_aplicacao`
--

CREATE TABLE `avaliacao__questao_aplicacao` (
  `aplicacao_id`        VARCHAR(20) NOT NULL,
  `aplicacao_descricao` VARCHAR(45) DEFAULT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Extraindo dados da tabela `avaliacao__questao_aplicacao`
--

INSERT INTO `avaliacao__questao_aplicacao` (`aplicacao_id`, `aplicacao_descricao`) VALUES
  ('alunoDocencia', 'Aluno avalia corpo Docente por Docência'),
  ('autoAvaliacao', 'Auto Avaliação'),
  ('docenteTurma', 'Docente Avalia Turma');

-- --------------------------------------------------------

--
-- Estrutura da tabela `avaliacao__questao_resposta`
--

CREATE TABLE `avaliacao__questao_resposta` (
  `questaoresposta_id`        INT(11)                             NOT NULL,
  `questao_id`                INT(11)                             NOT NULL,
  `questaoresposta_descricao` VARCHAR(255)                        NOT NULL,
  `questaoresposta_posicao`   INT(11)                                      DEFAULT NULL,
  `questaoresposta_gabarito`  ENUM('Sim', 'Não', 'Não Aplicável') NOT NULL DEFAULT 'Não Aplicável'
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `avaliacao__questao_tipo`
--

CREATE TABLE `avaliacao__questao_tipo` (
  `questaotipo_id`        VARCHAR(20) NOT NULL,
  `questaotipo_descricao` VARCHAR(45) DEFAULT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Extraindo dados da tabela `avaliacao__questao_tipo`
--

INSERT INTO `avaliacao__questao_tipo` (`questaotipo_id`, `questaotipo_descricao`) VALUES
  ('multiplaEscolha', 'Multipla Escolha');

-- --------------------------------------------------------

--
-- Estrutura da tabela `avaliacao__questionario`
--

CREATE TABLE `avaliacao__questionario` (
  `questionario_id`                     INT(11)     NOT NULL,
  `fim_id`                              VARCHAR(20) NOT NULL,
  `questionario_titulo`                 VARCHAR(45) NOT NULL,
  `questionario_descricao`              TEXT,
  `questionario_mensagem_inicial`       TEXT,
  `questionario_mensagem_agradecimento` TEXT,
  `questionario_inicio`                 DATETIME    NOT NULL,
  `questionario_fim`                    DATETIME                                        DEFAULT NULL,
  `questionario_alteracao`              DATETIME                                        DEFAULT NULL,
  `questionario_criacao`                DATETIME                                        DEFAULT NULL,
  `questionario_estado`                 ENUM('Rascunho', 'Publicado', 'Fechado')        DEFAULT NULL,
  `questionario_secoes`                 INT(11)                                         DEFAULT '1',
  `questionario_obrigatoriedade`        ENUM('Obrigatorio', 'Desejavel', 'Facultativo') DEFAULT NULL,
  `questionario_anonimo`                ENUM('Sim', 'Não')                              DEFAULT 'Não'
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `avaliacao__questionario_grupo`
--

CREATE TABLE `avaliacao__questionario_grupo` (
  `grupo_id`        INT(11) UNSIGNED ZEROFILL NOT NULL,
  `questionario_id` INT(11)                   NOT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `avaliacao__questionario_questao`
--

CREATE TABLE `avaliacao__questionario_questao` (
  `questionarioquestao_id`          INT(11) NOT NULL,
  `questionario_id`                 INT(11) NOT NULL,
  `secao_id`                        INT(11) NOT NULL,
  `questao_id`                      INT(11) NOT NULL,
  `questionarioquestao_posicao`     INT(11) NOT NULL,
  `questionarioquestao_anulada`     ENUM('Sim', 'Não') DEFAULT 'Não',
  `questionarioquestao_obrigatoria` ENUM('Sim', 'Não') DEFAULT 'Sim',
  `questionarioquestao_peso`        VARCHAR(45)        DEFAULT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `avaliacao__questionario_respondente`
--

CREATE TABLE `avaliacao__questionario_respondente` (
  `respondente_id`     INT(11)                      NOT NULL,
  `secao_id`           INT(11)                      NOT NULL,
  `respondente_status` ENUM('Concluído', 'Parcial') NOT NULL,
  `respondente_data`   DATETIME DEFAULT NULL,
  `pes_id`             INT(11) UNSIGNED ZEROFILL    NOT NULL,
  `questionario_id`    INT(11)                      NOT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `avaliacao__respondente_resposta`
--

CREATE TABLE `avaliacao__respondente_resposta` (
  `respondenteresposta_id`          INT(11) NOT NULL,
  `questionarioquestao_id`          INT(11) NOT NULL,
  `questaoresposta_id`              INT(11)      DEFAULT NULL,
  `respondente_id`                  INT(11)      DEFAULT NULL,
  `respondenteresposta_peso`        VARCHAR(45)  DEFAULT NULL,
  `respondenteresposta_complemento` VARCHAR(128) DEFAULT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `avaliacao__respondente_resposta_concluida`
--

CREATE TABLE `avaliacao__respondente_resposta_concluida` (
  `respondenteresposta_id`        INT(11)  NOT NULL,
  `questionarioquestao_id`        INT(11)  NOT NULL,
  `respondente_id`                INT(11)  NOT NULL,
  `respondenteresposta_submissao` DATETIME NOT NULL,
  respondenteresposta_resposta_aberta VARCHAR(255) NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `avaliacao__secao`
--

CREATE TABLE `avaliacao__secao` (
  `secao_id`        INT(11)     NOT NULL,
  `secao_nome`      VARCHAR(45) NOT NULL,
  `secao_descricao` VARCHAR(100) DEFAULT NULL,
  `secao_ordem`     INT(11)     NOT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `avaliacao__fim`
--
ALTER TABLE `avaliacao__fim`
ADD PRIMARY KEY (`fim_id`);

--
-- Indexes for table `avaliacao__questao`
--
ALTER TABLE `avaliacao__questao`
ADD PRIMARY KEY (`questao_id`),
ADD KEY `fk_perguntas_pergunta_tipo_idx` (`questaotipo_id`),
ADD KEY `fk_questionario_questoes_questionario_questao_aplicacao1_idx` (`aplicacao_id`);

--
-- Indexes for table `avaliacao__questao_aplicacao`
--
ALTER TABLE `avaliacao__questao_aplicacao`
ADD PRIMARY KEY (`aplicacao_id`);

--
-- Indexes for table `avaliacao__questao_resposta`
--
ALTER TABLE `avaliacao__questao_resposta`
ADD PRIMARY KEY (`questaoresposta_id`),
ADD KEY `fk_questionario_respostas_questionario_questao1_idx` (`questao_id`);

--
-- Indexes for table `avaliacao__questao_tipo`
--
ALTER TABLE `avaliacao__questao_tipo`
ADD PRIMARY KEY (`questaotipo_id`),
ADD UNIQUE KEY `questaotipo_id_UNIQUE` (`questaotipo_id`);

--
-- Indexes for table `avaliacao__questionario`
--
ALTER TABLE `avaliacao__questionario`
ADD PRIMARY KEY (`questionario_id`),
ADD KEY `fk_avaliacao__questionario_avaliacao_fim1_idx` (`fim_id`);

--
-- Indexes for table `avaliacao__questionario_grupo`
--
ALTER TABLE `avaliacao__questionario_grupo`
ADD PRIMARY KEY (`grupo_id`, `questionario_id`),
ADD KEY `fk_acesso_grupo_questionario_questionario1_idx` (`questionario_id`),
ADD KEY `fk_acesso_grupo_questionario_acesso_grupo1_idx` (`grupo_id`);

--
-- Indexes for table `avaliacao__questionario_questao`
--
ALTER TABLE `avaliacao__questionario_questao`
ADD PRIMARY KEY (`questionarioquestao_id`),
ADD KEY `fk_Questionario_has_perguntas_Questionario1_idx` (`questionario_id`),
ADD KEY `fk_questionario_pergunta_questionario_questoes1_idx` (`questao_id`),
ADD KEY `fk_avaliacao___questionario_questao_avaliacao_secao1_idx` (`secao_id`);

--
-- Indexes for table `avaliacao__questionario_respondente`
--
ALTER TABLE `avaliacao__questionario_respondente`
ADD PRIMARY KEY (`respondente_id`),
ADD KEY `fk_avaliacao__questionario_respondente_avaliacao_secao1_idx` (`secao_id`),
ADD KEY `fk_avaliacao__questionario_respondente_pessoa1_idx` (`pes_id`),
ADD KEY `fk_avaliacao__questionario_respondente_questionario1_idx` (`questionario_id`);

--
-- Indexes for table `avaliacao__respondente_resposta`
--
ALTER TABLE `avaliacao__respondente_resposta`
ADD PRIMARY KEY (`respondenteresposta_id`),
ADD KEY `fk_respostas_usuarios_questionario_pergunta1_idx` (`questionarioquestao_id`),
ADD KEY `fk_questionario_respostas_usuarios_copy1_questionario_respo_idx` (`respondente_id`),
ADD KEY `fk_avaliacao__respondente_resposta_obtida_avaliacao__questa_idx` (`questaoresposta_id`);

--
-- Indexes for table `avaliacao__respondente_resposta_concluida`
--
ALTER TABLE `avaliacao__respondente_resposta_concluida`
ADD PRIMARY KEY (`respondenteresposta_id`),
ADD KEY `fk_respostas_usuarios_questionario_pergunta1_idx` (`questionarioquestao_id`),
ADD KEY `fk_avaliacao__questionario_resposta_anonima_avaliacao__ques_idx` (`respondente_id`);

--
-- Indexes for table `avaliacao__secao`
--
ALTER TABLE `avaliacao__secao`
ADD PRIMARY KEY (`secao_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `avaliacao__questao`
--
ALTER TABLE `avaliacao__questao`
MODIFY `questao_id` INT(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT = 1;

--
-- AUTO_INCREMENT for table `avaliacao__questao_resposta`
--
ALTER TABLE `avaliacao__questao_resposta`
MODIFY `questaoresposta_id` INT(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT = 1;

--
-- AUTO_INCREMENT for table `avaliacao__questionario`
--
ALTER TABLE `avaliacao__questionario`
MODIFY `questionario_id` INT(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT = 1;

--
-- AUTO_INCREMENT for table `avaliacao__questionario_questao`
--
ALTER TABLE `avaliacao__questionario_questao`
MODIFY `questionarioquestao_id` INT(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT = 1;

--
-- AUTO_INCREMENT for table `avaliacao__questionario_respondente`
--
ALTER TABLE `avaliacao__questionario_respondente`
MODIFY `respondente_id` INT(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT = 1;

--
-- AUTO_INCREMENT for table `avaliacao__respondente_resposta`
--
ALTER TABLE `avaliacao__respondente_resposta`
MODIFY `respondenteresposta_id` INT(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT = 1;

--
-- AUTO_INCREMENT for table `avaliacao__respondente_resposta_concluida`
--
ALTER TABLE `avaliacao__respondente_resposta_concluida`
MODIFY `respondenteresposta_id` INT(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT = 1;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `avaliacao__questao`
--
ALTER TABLE `avaliacao__questao`
ADD CONSTRAINT `fk_perguntas_pergunta_tipo` FOREIGN KEY (`questaotipo_id`) REFERENCES `avaliacao__questao_tipo` (`questaotipo_id`)
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_questionario_questoes_questionario_questao_aplicacao1` FOREIGN KEY (`aplicacao_id`) REFERENCES `avaliacao__questao_aplicacao` (`aplicacao_id`)
  ON UPDATE CASCADE;

--
-- Limitadores para a tabela `avaliacao__questao_resposta`
--
ALTER TABLE `avaliacao__questao_resposta`
ADD CONSTRAINT `fk_questionario_respostas_questionario_questao1` FOREIGN KEY (`questao_id`) REFERENCES `avaliacao__questao` (`questao_id`)
  ON UPDATE CASCADE;

--
-- Limitadores para a tabela `avaliacao__questionario`
--
ALTER TABLE `avaliacao__questionario`
ADD CONSTRAINT `fk_avaliacao__questionario_avaliacao_fim1` FOREIGN KEY (`fim_id`) REFERENCES `avaliacao__fim` (`fim_id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

--
-- Limitadores para a tabela `avaliacao__questionario_grupo`
--
ALTER TABLE `avaliacao__questionario_grupo`
ADD CONSTRAINT `fk_acesso_grupo_questionario_acesso_grupo1` FOREIGN KEY (`grupo_id`) REFERENCES `acesso_grupo` (`id`)
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_acesso_grupo_questionario_questionario1` FOREIGN KEY (`questionario_id`) REFERENCES `avaliacao__questionario` (`questionario_id`)
  ON UPDATE CASCADE;

--
-- Limitadores para a tabela `avaliacao__questionario_questao`
--
ALTER TABLE `avaliacao__questionario_questao`
ADD CONSTRAINT `fk_Questionario_has_perguntas_Questionario1` FOREIGN KEY (`questionario_id`) REFERENCES `avaliacao__questionario` (`questionario_id`)
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_avaliacao___questionario_questao_avaliacao_secao1` FOREIGN KEY (`secao_id`) REFERENCES `avaliacao__secao` (`secao_id`)
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_questionario_pergunta_questionario_questoes1` FOREIGN KEY (`questao_id`) REFERENCES `avaliacao__questao` (`questao_id`)
  ON UPDATE CASCADE;

--
-- Limitadores para a tabela `avaliacao__questionario_respondente`
--
ALTER TABLE `avaliacao__questionario_respondente`

ADD CONSTRAINT `fk_avaliacao__questionario_respondente_pessoa1` FOREIGN KEY (`pes_id`) REFERENCES `pessoa` (`pes_id`)
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_avaliacao__questionario_respondente_questionario1` FOREIGN KEY (`questionario_id`) REFERENCES `avaliacao__questionario` (`questionario_id`)
  ON UPDATE CASCADE;

--
-- Limitadores para a tabela `avaliacao__respondente_resposta`
--
ALTER TABLE `avaliacao__respondente_resposta`
ADD CONSTRAINT `fk_avaliacao__respondente_resposta_obtida_avaliacao__questao_1` FOREIGN KEY (`questaoresposta_id`) REFERENCES `avaliacao__questao_resposta` (`questaoresposta_id`)
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_questionario_respostas_usuarios_copy1_questionario_respond1` FOREIGN KEY (`respondente_id`) REFERENCES `avaliacao__questionario_respondente` (`respondente_id`)
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_respostas_usuarios_questionario_pergunta10` FOREIGN KEY (`questionarioquestao_id`) REFERENCES `avaliacao__questionario_questao` (`questionarioquestao_id`)
  ON UPDATE CASCADE;

--
-- Limitadores para a tabela `avaliacao__respondente_resposta_concluida`
--
ALTER TABLE `avaliacao__respondente_resposta_concluida`
ADD CONSTRAINT `fk_avaliacao__questionario_resposta_anonima_avaliacao__questi1` FOREIGN KEY (`respondente_id`) REFERENCES `avaliacao__questionario_respondente` (`respondente_id`)
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_respostas_usuarios_questionario_pergunta1` FOREIGN KEY (`questionarioquestao_id`) REFERENCES `avaliacao__questionario_questao` (`questionarioquestao_id`)
  ON UPDATE CASCADE;


--
-- módulo Avaliacao
--
DELETE FROM `acesso_privilegios_grupo`
WHERE acesso_actions_id IN (
  SELECT acesso_actions.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
  WHERE func_controller IN (
    'Avaliacao\\Controller\\AvaliacaoFim',
    'Avaliacao\\Controller\\AvaliacaoQuestao',
    'Avaliacao\\Controller\\AvaliacaoQuestaoAplicacao',
    'Avaliacao\\Controller\\AvaliacaoQuestaoResposta',
    'Avaliacao\\Controller\\AvaliacaoQuestaoTipo',
    'Avaliacao\\Controller\\AvaliacaoQuestionario',
    'Avaliacao\\Controller\\AvaliacaoQuestionarioGrupo',
    'Avaliacao\\Controller\\AvaliacaoQuestionarioQuestao',
    'Avaliacao\\Controller\\AvaliacaoQuestionarioRespondente',
    'Avaliacao\\Controller\\AvaliacaoRespondenteResposta',
    'Avaliacao\\Controller\\AvaliacaoRespondenteRespostaConcluida',
    'Avaliacao\\Controller\\AvaliacaoSecao'
  )
) AND acesso_actions_id >= 0 AND acesso_grupo_id >= 0;

DELETE FROM `acesso_actions`
WHERE funcionalidade IN (
  SELECT acesso_funcionalidades.id
  FROM acesso_funcionalidades
  WHERE func_controller IN (
    'Avaliacao\\Controller\\AvaliacaoFim',
    'Avaliacao\\Controller\\AvaliacaoQuestao',
    'Avaliacao\\Controller\\AvaliacaoQuestaoAplicacao',
    'Avaliacao\\Controller\\AvaliacaoQuestaoResposta',
    'Avaliacao\\Controller\\AvaliacaoQuestaoTipo',
    'Avaliacao\\Controller\\AvaliacaoQuestionario',
    'Avaliacao\\Controller\\AvaliacaoQuestionarioGrupo',
    'Avaliacao\\Controller\\AvaliacaoQuestionarioQuestao',
    'Avaliacao\\Controller\\AvaliacaoQuestionarioRespondente',
    'Avaliacao\\Controller\\AvaliacaoRespondenteResposta',
    'Avaliacao\\Controller\\AvaliacaoRespondenteRespostaConcluida',
    'Avaliacao\\Controller\\AvaliacaoSecao'
  )
) AND id > 0;

DELETE FROM acesso_funcionalidades
WHERE func_controller IN (
  'Avaliacao\\Controller\\AvaliacaoFim',
  'Avaliacao\\Controller\\AvaliacaoQuestao',
  'Avaliacao\\Controller\\AvaliacaoQuestaoAplicacao',
  'Avaliacao\\Controller\\AvaliacaoQuestaoResposta',
  'Avaliacao\\Controller\\AvaliacaoQuestaoTipo',
  'Avaliacao\\Controller\\AvaliacaoQuestionario',
  'Avaliacao\\Controller\\AvaliacaoQuestionarioGrupo',
  'Avaliacao\\Controller\\AvaliacaoQuestionarioQuestao',
  'Avaliacao\\Controller\\AvaliacaoQuestionarioRespondente',
  'Avaliacao\\Controller\\AvaliacaoRespondenteResposta',
  'Avaliacao\\Controller\\AvaliacaoRespondenteRespostaConcluida',
  'Avaliacao\\Controller\\AvaliacaoSecao'
) AND id > 0;

DELETE FROM acesso_modulos
WHERE mod_route = 'avaliacao/default' AND id > 0;

DELETE FROM acesso_sistemas
WHERE sis_nome = 'Avaliacao' AND id > 0;

REPLACE INTO `acesso_sistemas` (`sis_nome`, `sis_status`, `sis_icon`, `sis_info`)
VALUES ('Avaliacao', 'Ativo', 'fa-group', 'Avaliacao');

REPLACE INTO `acesso_modulos` (`sistema`, `mod_nome`, `mod_route`, `mod_status`, `mod_icon`, `mod_info`, `mod_redireciona`, `mod_prioridade`)
  SELECT
    acesso_sistemas.id,
    'Avaliacao',
    'avaliacao/default',
    'Ativo',
    'fa fa-group',
    'Módulo Avaliacao',
    '/avaliacao',
    round(acesso_sistemas.id / 10)
  FROM acesso_sistemas
  WHERE sis_nome LIKE 'Avaliacao';

REPLACE INTO `acesso_funcionalidades` (modulo, func_nome, func_controller, func_status, func_visivel)
  SELECT DISTINCT
    acesso_modulos.id,
    func.descricao,
    func.controller,
    'Ativa',
    'Sim'
  FROM acesso_modulos,
    (
      SELECT
        'Gestão de fim'                       AS descricao,
        'Avaliacao\\Controller\\AvaliacaoFim' AS controller
      FROM dual
      UNION ALL
      SELECT
        'Gestão de questão'                       AS descricao,
        'Avaliacao\\Controller\\AvaliacaoQuestao' AS controller
      FROM dual
      UNION ALL
      SELECT
        'Gestão de questão aplicação'                      AS descricao,
        'Avaliacao\\Controller\\AvaliacaoQuestaoAplicacao' AS controller
      FROM dual
      UNION ALL
      SELECT
        'Gestão de questão resposta'                      AS descricao,
        'Avaliacao\\Controller\\AvaliacaoQuestaoResposta' AS controller
      FROM dual
      UNION ALL
      SELECT
        'Gestão de questão tipo'                      AS descricao,
        'Avaliacao\\Controller\\AvaliacaoQuestaoTipo' AS controller
      FROM dual
      UNION ALL
      SELECT
        'Gestão de questionario'                       AS descricao,
        'Avaliacao\\Controller\\AvaliacaoQuestionario' AS controller
      FROM dual
      UNION ALL
      SELECT
        'Gestão de questionario grupo'                      AS descricao,
        'Avaliacao\\Controller\\AvaliacaoQuestionarioGrupo' AS controller
      FROM dual
      UNION ALL
      SELECT
        'Gestão de questionario questão'                      AS descricao,
        'Avaliacao\\Controller\\AvaliacaoQuestionarioQuestao' AS controller
      FROM dual
      UNION ALL
      SELECT
        'Gestão de questionario respondente'                      AS descricao,
        'Avaliacao\\Controller\\AvaliacaoQuestionarioRespondente' AS controller
      FROM dual
      UNION ALL
      SELECT
        'Gestão de respondente resposta'                      AS descricao,
        'Avaliacao\\Controller\\AvaliacaoRespondenteResposta' AS controller
      FROM dual
      UNION ALL
      SELECT
        'Gestão de respondente resposta concluida'                     AS descricao,
        'Avaliacao\\Controller\\AvaliacaoRespondenteRespostaConcluida' AS controller
      FROM dual
      UNION ALL
      SELECT
        'Gestão de seção'                       AS descricao,
        'Avaliacao\\Controller\\AvaliacaoSecao' AS controller
      FROM dual
    ) AS func
  WHERE mod_nome LIKE 'Avaliacao';

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT DISTINCT
    acesso_funcionalidades.id,
    actions.act,
    actions.descricao,
    actions.tipo,
    actions.usr
  FROM acesso_funcionalidades,
    (
      SELECT
        'index'    AS act,
        'Listagem' AS descricao,
        'Leitura'  AS tipo,
        'N'        AS usr
      FROM dual
      UNION ALL
      SELECT
        'remove'  AS act,
        'Remover' AS descricao,
        'Leitura' AS tipo,
        'N'       AS usr
      FROM dual
      UNION ALL
      SELECT
        'add'     AS act,
        'Criar'   AS descricao,
        'Leitura' AS tipo,
        'N'       AS usr
      FROM dual
      UNION ALL
      SELECT
        'edit'    AS act,
        'Editar'  AS descricao,
        'Leitura' AS tipo,
        'N'       AS usr
      FROM dual
      UNION ALL
      SELECT
        'search-for-json' AS act,
        'Autocomplete'    AS descricao,
        'Leitura'         AS tipo,
        'N'               AS usr
      FROM dual
      UNION ALL
      SELECT
        'search'     AS act,
        'Datatables' AS descricao,
        'Leitura'    AS tipo,
        'N'          AS usr
      FROM dual
    ) AS actions
  WHERE func_controller IN (
    'Avaliacao\\Controller\\AvaliacaoFim',
    'Avaliacao\\Controller\\AvaliacaoQuestao',
    'Avaliacao\\Controller\\AvaliacaoQuestaoAplicacao',
    'Avaliacao\\Controller\\AvaliacaoQuestaoResposta',
    'Avaliacao\\Controller\\AvaliacaoQuestaoTipo',
    'Avaliacao\\Controller\\AvaliacaoQuestionario',
    'Avaliacao\\Controller\\AvaliacaoQuestionarioGrupo',
    'Avaliacao\\Controller\\AvaliacaoQuestionarioQuestao',
    'Avaliacao\\Controller\\AvaliacaoQuestionarioRespondente',
    'Avaliacao\\Controller\\AvaliacaoRespondenteResposta',
    'Avaliacao\\Controller\\AvaliacaoRespondenteRespostaConcluida',
    'Avaliacao\\Controller\\AvaliacaoSecao'
  );
REPLACE INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
    ,
    acesso_grupo
  WHERE func_controller IN (
    'Avaliacao\\Controller\\AvaliacaoFim',
    'Avaliacao\\Controller\\AvaliacaoQuestao',
    'Avaliacao\\Controller\\AvaliacaoQuestaoAplicacao',
    'Avaliacao\\Controller\\AvaliacaoQuestaoResposta',
    'Avaliacao\\Controller\\AvaliacaoQuestaoTipo',
    'Avaliacao\\Controller\\AvaliacaoQuestionario',
    'Avaliacao\\Controller\\AvaliacaoQuestionarioGrupo',
    'Avaliacao\\Controller\\AvaliacaoQuestionarioQuestao',
    'Avaliacao\\Controller\\AvaliacaoQuestionarioRespondente',
    'Avaliacao\\Controller\\AvaliacaoRespondenteResposta',
    'Avaliacao\\Controller\\AvaliacaoRespondenteRespostaConcluida',
    'Avaliacao\\Controller\\AvaliacaoSecao'
  )
        AND grup_nome LIKE 'Admin%';

/* Usar somente se as instruções acima ja estiverem sido executadas


ALTER TABLE `avaliacao__questionario_respondente`
DROP FOREIGN KEY `fk_avaliacao__questionario_respondente_avaliacao_secao1`;
ALTER TABLE `avaliacao__questionario_respondente` 
CHANGE COLUMN `secao_id` `secao_id` INT(11) NULL ;
ALTER TABLE `avaliacao__questionario_respondente` 
ADD CONSTRAINT `fk_avaliacao__questionario_respondente_avaliacao_secao1`
  FOREIGN KEY (`secao_id`)
  REFERENCES `avaliacao__secao` (`secao_id`)
  ON UPDATE CASCADE;

  */

INSERT INTO avaliacao__questao_tipo VALUES('respostaCurta','respostaCurta');
INSERT INTO avaliacao__questao_tipo VALUES('respostaLonga','respostaLonga');


set foreign_key_checks=0;
ALTER TABLE `avaliacao__secao` 
CHANGE COLUMN `secao_id` `secao_id` INT(11) NOT NULL AUTO_INCREMENT ;
set foreign_key_checks=1;


/* Usar somente se as instruções acima ja estiverem sido executadas


ALTER TABLE `avaliacao__respondente_resposta`
  ADD COLUMN `respondenteresposta_resposta_aberta` TEXT NULL ;
*/

SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;
