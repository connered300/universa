ALTER TABLE `avaliacao__questionario_respondente`
ADD COLUMN `questionario_id` INT(11) NOT NULL ;

ALTER TABLE `avaliacao__questionario_respondente`
ADD INDEX `fk_avaliacao__questionario_respondente_questionario1_idx` (`questionario_id` ASC);
ALTER TABLE `avaliacao__questionario_respondente`
ADD CONSTRAINT `fk_avaliacao__questionario_respondente_questionario1`
  FOREIGN KEY (`questionario_id`)
  REFERENCES `avaliacao__questionario` (`questionario_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;
