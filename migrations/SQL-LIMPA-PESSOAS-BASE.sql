SET SQL_SAFE_UPDATES=0;
# Apagar alunos
delete from acadperiodo__aluno_disciplina_final where 1;
delete from acadperiodo__aluno_disciplina where 1;
delete from acadperiodo__aluno where 1;
delete from acadgeral__responsavel where 1;
delete from acadgeral__aluno_curso where 1;
delete from acadgeral__aluno_formacao where 1;
delete from financeiro__desconto_titulo where 1;
delete from financeiro__desconto where 1;
delete from financeiro__aluno_config_pgto_curso where 1;
delete from acadgeral__aluno where 1;
#  Apagar pessoas

delete from acadperiodo__docente_disciplina;
delete from financeiro__titulo_alteracao;
delete from financeiro__pagamento;
delete from financeiro__titulo;
delete from boleto;
delete from financeiro__retorno;
delete from financeiro__retorno_detalhe;
delete from financeiro__remessa;
delete from financeiro__remessa_lote;


delete from contato where pessoa not in (
    select pes_fisica from acesso_pessoas where pes_fisica is not null union all
    select pes_juridica from acesso_pessoas where pes_juridica is not null union all
    select pes_id from acad_curso where pes_id is not null union all
    select pes_id from org_ies where pes_id is not null union all
    select diretoria_pes_id from org_ies where diretoria_pes_id is not null union all
    select secretaria_pes_id from org_ies where secretaria_pes_id is not null union all
    select diretoria_pes_id from org_campus where diretoria_pes_id is not null union all
    select secretaria_pes_id from org_campus where secretaria_pes_id is not null union all
    select pes_id from boleto_conf_conta where pes_id is not null union all
    select pes_id from org__agente_educacional where pes_id is not null union all
    select 1
);

delete from endereco where pes_id not in (
    select pes_fisica from acesso_pessoas where pes_fisica is not null union all
    select pes_juridica from acesso_pessoas where pes_juridica is not null union all
    select pes_id from acad_curso where pes_id is not null union all
    select pes_id from org_ies where pes_id is not null union all
    select diretoria_pes_id from org_ies where diretoria_pes_id is not null union all
    select secretaria_pes_id from org_ies where secretaria_pes_id is not null union all
    select diretoria_pes_id from org_campus where diretoria_pes_id is not null union all
    select secretaria_pes_id from org_campus where secretaria_pes_id is not null union all
    select pes_id from boleto_conf_conta where pes_id is not null union all
    select pes_id from org__agente_educacional where pes_id is not null union all
    select 1
);

delete from acadgeral__docente where pes_id not in (
    select pes_fisica from acesso_pessoas where pes_fisica is not null union all
    select pes_juridica from acesso_pessoas where pes_juridica is not null union all
    select pes_id from acad_curso where pes_id is not null union all
    select pes_id from org_ies where pes_id is not null union all
    select diretoria_pes_id from org_ies where diretoria_pes_id is not null union all
    select secretaria_pes_id from org_ies where secretaria_pes_id is not null union all
    select diretoria_pes_id from org_campus where diretoria_pes_id is not null union all
    select secretaria_pes_id from org_campus where secretaria_pes_id is not null union all
    select pes_id from boleto_conf_conta where pes_id is not null union all
    select pes_id from org__agente_educacional where pes_id is not null union all
    select 1
);

delete from documento__pessoa where pes_id not in (
    select pes_fisica from acesso_pessoas where pes_fisica is not null union all
    select pes_juridica from acesso_pessoas where pes_juridica is not null union all
    select pes_id from acad_curso where pes_id is not null union all
    select pes_id from org_ies where pes_id is not null union all
    select diretoria_pes_id from org_ies where diretoria_pes_id is not null union all
    select secretaria_pes_id from org_ies where secretaria_pes_id is not null union all
    select diretoria_pes_id from org_campus where diretoria_pes_id is not null union all
    select secretaria_pes_id from org_campus where secretaria_pes_id is not null union all
    select pes_id from boleto_conf_conta where pes_id is not null union all
    select pes_id from org__agente_educacional where pes_id is not null union all
    select 1
);

delete from pessoa_necessidades where pes_id not in (
    select pes_fisica from acesso_pessoas where pes_fisica is not null union all
    select pes_juridica from acesso_pessoas where pes_juridica is not null union all
    select pes_id from acad_curso where pes_id is not null union all
    select pes_id from org_ies where pes_id is not null union all
    select diretoria_pes_id from org_ies where diretoria_pes_id is not null union all
    select secretaria_pes_id from org_ies where secretaria_pes_id is not null union all
    select diretoria_pes_id from org_campus where diretoria_pes_id is not null union all
    select secretaria_pes_id from org_campus where secretaria_pes_id is not null union all
    select pes_id from boleto_conf_conta where pes_id is not null union all
    select pes_id from org__agente_educacional where pes_id is not null union all
    select 1
);

delete from pessoa_fisica where pes_id not in (
    select pes_fisica from acesso_pessoas where pes_fisica is not null union all
    select pes_juridica from acesso_pessoas where pes_juridica is not null union all
    select pes_id from acad_curso where pes_id is not null union all
    select pes_id from org_ies where pes_id is not null union all
    select diretoria_pes_id from org_ies where diretoria_pes_id is not null union all
    select secretaria_pes_id from org_ies where secretaria_pes_id is not null union all
    select diretoria_pes_id from org_campus where diretoria_pes_id is not null union all
    select secretaria_pes_id from org_campus where secretaria_pes_id is not null union all
    select pes_id from boleto_conf_conta where pes_id is not null union all
    select pes_id from org__agente_educacional where pes_id is not null union all
    select 1
);

delete from pessoa_juridica where pes_id not in (
    select pes_fisica from acesso_pessoas where pes_fisica is not null union all
    select pes_juridica from acesso_pessoas where pes_juridica is not null union all
    select pes_id from acad_curso where pes_id is not null union all
    select pes_id from org_ies where pes_id is not null union all
    select diretoria_pes_id from org_ies where diretoria_pes_id is not null union all
    select secretaria_pes_id from org_ies where secretaria_pes_id is not null union all
    select diretoria_pes_id from org_campus where diretoria_pes_id is not null union all
    select secretaria_pes_id from org_campus where secretaria_pes_id is not null union all
    select pes_id from boleto_conf_conta where pes_id is not null union all
    select pes_id from org__agente_educacional where pes_id is not null union all
    select 1
);

delete from pessoa where pes_id not in (
    select pes_fisica from acesso_pessoas where pes_fisica is not null union all
    select pes_juridica from acesso_pessoas where pes_juridica is not null union all
    select pes_id from acad_curso where pes_id is not null union all
    select pes_id from org_ies where pes_id is not null union all
    select diretoria_pes_id from org_ies where diretoria_pes_id is not null union all
    select secretaria_pes_id from org_ies where secretaria_pes_id is not null union all
    select diretoria_pes_id from org_campus where diretoria_pes_id is not null union all
    select secretaria_pes_id from org_campus where secretaria_pes_id is not null union all
    select pes_id from boleto_conf_conta where pes_id is not null union all
    select pes_id from org__agente_educacional where pes_id is not null union all
    select 1
);

# Apaga filas
delete from sis__fila_email;
delete from sis__fila_sms;

#Full delete
delete from campus_curso;
delete from acad_curso;
delete from org__agente_educacional;
delete from org__unidade_estudo;
delete from org__comunicacao_modelo;
delete from prova_caderno;
delete from prova_edicao;
delete from prova;
delete from selecao_cursos;
delete from selecao_data_realizacao;
delete from selecao_locais;
delete from selecao_edicao;
delete from sis__integracao_aluno;
delete from sis__integracao_campus;
delete from sis__integracao_curso;
delete from tipo_logradouro;
set @campId:=(select camp_id from org_campus limit 0,1);
delete from org_campus where camp_id > @campId;
delete from org_ies where ies_id not in (select ies_id from org_campus);


SET SQL_SAFE_UPDATES=1;

SET FOREIGN_KEY_CHECKS=0;

TRUNCATE `acadgeral__aluno`;
TRUNCATE `acadgeral__aluno_curso`;
TRUNCATE `acadgeral__aluno_curso_historico`;
TRUNCATE `acadgeral__aluno_formacao`;
TRUNCATE `acadgeral__area_conhecimento`;
TRUNCATE `acadgeral__ativ_tipo`;
TRUNCATE `acadgeral__cadastro_origem`;
TRUNCATE `acadgeral__disciplina`;
TRUNCATE `acadgeral__disciplina_curso`;
TRUNCATE `acadgeral__disciplina_ementa`;
TRUNCATE `acadgeral__disciplina_equivalencia`;
TRUNCATE `acadgeral__disciplina_requisito`;
TRUNCATE `acadgeral__docente`;
TRUNCATE `acadgeral__docente_formacao`;
TRUNCATE `acadgeral__horario_grade__padrao`;
TRUNCATE `acadgeral__motivo_alteracao`;
TRUNCATE `acadgeral__responsavel`;
TRUNCATE `acadperiodo__aluno`;
TRUNCATE `acadperiodo__aluno_disciplina`;
TRUNCATE `acadperiodo__aluno_disciplina_final`;
TRUNCATE `acadperiodo__aluno_disciplina_historico`;
TRUNCATE `acadperiodo__aluno_historico`;
TRUNCATE `acadperiodo__aluno_resumo`;
TRUNCATE `acadperiodo__anotacao`;
TRUNCATE `acadperiodo__anotacao_arquivo`;
TRUNCATE `acadperiodo__atividades`;
TRUNCATE `acadperiodo__atividades_arquivo`;
TRUNCATE `acadperiodo__atividade_nota`;
TRUNCATE `acadperiodo__atividade_nota_arquivo`;
TRUNCATE `acadperiodo__calendario`;
TRUNCATE `acadperiodo__calendario_data`;
TRUNCATE `acadperiodo__calendario_data_evento`;
TRUNCATE `acadperiodo__calendario_tipo_evento`;
TRUNCATE `acadperiodo__diario_entrega`;
TRUNCATE `acadperiodo__disciplina_grade`;
TRUNCATE `acadperiodo__docente_disciplina`;
TRUNCATE `acadperiodo__etapas`;
TRUNCATE `acadperiodo__etapas_excecao`;
TRUNCATE `acadperiodo__etapa_aluno`;
TRUNCATE `acadperiodo__etapa_diario`;
TRUNCATE `acadperiodo__etapa_diario_extensao`;
TRUNCATE `acadperiodo__frequencia`;
TRUNCATE `acadperiodo__grade`;
TRUNCATE `acadperiodo__integradora`;
TRUNCATE `acadperiodo__integradora_aluno`;
TRUNCATE `acadperiodo__integradora_aluno_historico`;
TRUNCATE `acadperiodo__integradora_avaliacao`;
TRUNCATE `acadperiodo__integradora_caderno`;
TRUNCATE `acadperiodo__integradora_conf`;
TRUNCATE `acadperiodo__integradora_gabarito`;
TRUNCATE `acadperiodo__integradora_turma`;
TRUNCATE `acadperiodo__letivo`;
TRUNCATE `acadperiodo__matriz_curricular`;
TRUNCATE `acadperiodo__matriz_disciplina`;
TRUNCATE `acadperiodo__notas`;
TRUNCATE `acadperiodo__resumo_equivalencia`;
TRUNCATE `acadperiodo__turma`;
TRUNCATE `acadperiodo__turma_excecao`;
TRUNCATE `acad_curso`;
TRUNCATE `acad_curso_conceito`;
TRUNCATE `acad_curso_config`;
TRUNCATE `acesso_log`;
TRUNCATE `arquivo`;
TRUNCATE `atividadegeral__atividades`;
TRUNCATE `atividadegeral__atividades_disciplina`;
TRUNCATE `atividadegeral__configuracoes`;
TRUNCATE `atividadegeral__configuracoes_atividades`;
TRUNCATE `atividadegeral__configuracoes_curso`;
TRUNCATE `atividadegeral__nucleo`;
TRUNCATE `atividadegeral__nucleo_processo`;
TRUNCATE `atividadegeral__nucleo_responsavel`;
TRUNCATE `atividadegeral__nucleo__sala`;
TRUNCATE `atividadegeral__processo`;
TRUNCATE `atividadegeral__processo_tipo`;
TRUNCATE `atividadegeral__processo__pessoas`;
TRUNCATE `atividadegeral__secretaria`;
TRUNCATE `atividadegeral__tipo`;
TRUNCATE `atividadeperiodo__aluno_atividades`;
TRUNCATE `atividadeperiodo__aluno_nucleo`;
TRUNCATE `atividadeperiodo__aluno_nucleo_processo`;
TRUNCATE `atividadeperiodo__aluno_portfolio`;
TRUNCATE `atividadeperiodo__aluno_sumarizador`;
TRUNCATE `atividadeperiodo__letivo`;
TRUNCATE `bairro`;
TRUNCATE `biblioteca__aquisicao`;
TRUNCATE `biblioteca__aquisicao_exemplar`;
TRUNCATE `biblioteca__area`;
TRUNCATE `biblioteca__assunto`;
TRUNCATE `biblioteca__assunto_relacionado`;
TRUNCATE `biblioteca__autor`;
TRUNCATE `biblioteca__colecao`;
TRUNCATE `biblioteca__cutter`;
TRUNCATE `biblioteca__empresa`;
TRUNCATE `biblioteca__emprestimo`;
TRUNCATE `biblioteca__exemplar`;
TRUNCATE `biblioteca__grupo_bibliografico`;
TRUNCATE `biblioteca__grupo_leitor`;
TRUNCATE `biblioteca__idioma`;
TRUNCATE `biblioteca__modalidade_emprestimo`;
TRUNCATE `biblioteca__modalidade_emprestimo_grupo`;
TRUNCATE `biblioteca__multa`;
TRUNCATE `biblioteca__pessoa`;
TRUNCATE `biblioteca__setor`;
TRUNCATE `biblioteca__suspensao`;
TRUNCATE `biblioteca__titulo`;
TRUNCATE `biblioteca__titulo_assunto`;
TRUNCATE `biblioteca__titulo_autor`;
TRUNCATE `biblioteca__titulo_idioma`;
TRUNCATE `boleto`;
TRUNCATE `boleto_baixa`;
TRUNCATE `boleto_historico`;
TRUNCATE `contato`;
TRUNCATE `crm__interesse`;
TRUNCATE `crm__lead`;
TRUNCATE `crm__lead_atividade`;
TRUNCATE `crm__lead_evento`;
TRUNCATE `crm__lead_interesse`;
TRUNCATE `crm__operador`;
TRUNCATE `crm__time`;
TRUNCATE `documento__emissao`;
TRUNCATE `documento__grupo`;
TRUNCATE `documento__modelo`;
TRUNCATE `documento__modelo_grupo_permissao`;
TRUNCATE `documento__pessoa`;
TRUNCATE `endereco`;
TRUNCATE `financeiro__aluno_config_pgto_curso`;
TRUNCATE `financeiro__cheque`;
TRUNCATE `financeiro__desconto`;
TRUNCATE `financeiro__desconto_titulo`;
TRUNCATE `financeiro__mensalidade_desconto`;
TRUNCATE `financeiro__pagamento`;
TRUNCATE `financeiro__permissao_desconto_tipo_titulo`;
TRUNCATE `financeiro__remessa`;
TRUNCATE `financeiro__remessa_lote`;
TRUNCATE `financeiro__retorno`;
TRUNCATE `financeiro__retorno_detalhe`;
TRUNCATE `financeiro__titulo`;
TRUNCATE `financeiro__titulo_alteracao`;
TRUNCATE `financeiro__titulo_cheque`;
TRUNCATE `financeiro__titulo_config`;
TRUNCATE `financeiro__titulo_mensalidade`;
TRUNCATE `financeiro__valores`;
TRUNCATE `infra_classificacao`;
TRUNCATE `infra_predio`;
TRUNCATE `infra_sala`;
TRUNCATE `inscricao_cursos`;
TRUNCATE `logradouro`;
TRUNCATE `necessidades_especiais`;
TRUNCATE `org__agente_educacional`;
TRUNCATE `org__comunicacao_modelo`;
TRUNCATE `org__comunicacao_modelo_grupo_permissao`;
TRUNCATE `org__unidade_estudo`;
TRUNCATE `pessoa_necessidades`;
TRUNCATE `protocolo`;
TRUNCATE `protocolo__mensagem`;
TRUNCATE `protocolo__mensagem_arquivo`;
TRUNCATE `protocolo__mensagem_padrao`;
TRUNCATE `protocolo__setor`;
TRUNCATE `protocolo__setor_grupo`;
TRUNCATE `protocolo__solicitacao`;
TRUNCATE `prova`;
TRUNCATE `prova_caderno`;
TRUNCATE `prova_edicao`;
TRUNCATE `prova_edicao_caderno`;
TRUNCATE `prova_gabarito`;
TRUNCATE `prova_inscrito`;
TRUNCATE `selecao_cursos`;
TRUNCATE `selecao_data_realizacao`;
TRUNCATE `selecao_edicao`;
TRUNCATE `selecao_edicao_fechamento`;
TRUNCATE `selecao_inscricao`;
TRUNCATE `selecao_locais`;
TRUNCATE `selecao_tipo_edicao`;
TRUNCATE `sis__fila_email`;
TRUNCATE `sis__fila_integracao`;
TRUNCATE `sis__fila_sms`;
TRUNCATE `sis__integracao_aluno`;
TRUNCATE `sis__integracao_campus`;
TRUNCATE `sis__integracao_curso`;

SET FOREIGN_KEY_CHECKS=1;