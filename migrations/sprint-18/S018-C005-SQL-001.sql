SET FOREIGN_KEY_CHECKS=0;
SET SQL_SAFE_UPDATES=0;

DELIMITER ;;

DROP PROCEDURE IF EXISTS migraEgressos;;
CREATE PROCEDURE migraEgressos()
BEGIN
	DECLARE done INT DEFAULT FALSE;
	DECLARE
		_pes_id, _pes_nome, _pes_tipo, _pes_nacionalidade,
		_pes_cpf, _pes_cpf_emissao, _pes_rg, _pes_rg_emissao,
		_pes_sobrenome, _pes_sexo, _pes_data_nascimento,
		_pes_estado_civil, _pes_filhos, _pes_naturalidade,
		_pes_doc_estrangeiro, _pes_emissor_rg, _pes_nasc_uf
		VARCHAR(255);
	DECLARE __pes_id, __aluno_id  INT;
	DECLARE ConsultaPessoa CURSOR FOR
		SELECT
			p.pes_id, pes_nome, pes_tipo, pes_nacionalidade,
			pes_cpf, pes_cpf_emissao, pes_rg, pes_rg_emissao,
			pes_sobrenome, pes_sexo, pes_data_nascimento,
			pes_estado_civil, pes_filhos, pes_naturalidade,
			pes_doc_estrangeiro, pes_emissor_rg, pes_nasc_uf
		FROM pessoa AS p
			INNER JOIN pessoa_fisica AS pf
				ON pf.pes_id = p.pes_id
		WHERE REPLACE(REPLACE(CONVERT(UPPER(pes_nome) USING ascii),'?',''),' ','') NOT IN(
				SELECT REPLACE(REPLACE(CONVERT(UPPER(pes_nome) USING ascii),'?',''),' ','') FROM pessoa
			)
			AND REPLACE(UPPER(pes_nome),' ','') NOT IN(
				SELECT REPLACE(UPPER(pes_nome),' ','') FROM pessoa
			)
			-- AND IF(
			-- 	(pes_cpf IS NOT NULL AND TRIM(pes_cpf)<>''),
			-- 	(
			-- 		REPLACE(REPLACE(REPLACE(pes_cpf, '-', ''), '.', ''), ' ', '') NOT IN(
			-- 			SELECT REPLACE(REPLACE(REPLACE(pes_cpf, '-', ''), '.', ''), ' ', '') FROM pessoa_fisica
			-- 		)
			-- 	),
			-- 	1
			-- )
			;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN ConsultaPessoa;

	read_loop: LOOP
		FETCH ConsultaPessoa INTO
		_pes_id, _pes_nome, _pes_tipo, _pes_nacionalidade,
		_pes_cpf, _pes_cpf_emissao, _pes_rg, _pes_rg_emissao,
		_pes_sobrenome, _pes_sexo, _pes_data_nascimento,
		_pes_estado_civil, _pes_filhos, _pes_naturalidade,
		_pes_doc_estrangeiro, _pes_emissor_rg, _pes_nasc_uf;

		IF done THEN
			LEAVE read_loop;
		END IF;

			SET __aluno_id:=(
				SELECT aluno_id FROM acadgeral__aluno WHERE aluno_id IN(
					SELECT aluno_id FROM acadgeral__aluno WHERE pes_id=_pes_id
				)
			);

			IF __aluno_id IS NULL THEN
				INSERT INTO `pessoa` (`pes_id`, `pes_nome`, `pes_tipo`, `pes_nacionalidade`)
				VALUES (null, _pes_nome, _pes_tipo, _pes_nacionalidade);

				SELECT LAST_INSERT_ID() INTO __pes_id FROM DUAL;

				INSERT INTO `pessoa_fisica`
				(
					pes_id, pes_cpf, pes_cpf_emissao, pes_rg, pes_rg_emissao, pes_sobrenome, pes_sexo,
					pes_data_nascimento, pes_estado_civil, pes_filhos, pes_naturalidade, pes_doc_estrangeiro,
					pes_emissor_rg, pes_nasc_uf
				)
				VALUES (
					__pes_id, _pes_cpf, _pes_cpf_emissao, _pes_rg, _pes_rg_emissao, _pes_sobrenome, _pes_sexo,
					_pes_data_nascimento, _pes_estado_civil, _pes_filhos, _pes_naturalidade, _pes_doc_estrangeiro,
					_pes_emissor_rg, _pes_nasc_uf
				);

				INSERT INTO endereco(end_id, pes_id, tipo_endereco, end_pais, end_estado, end_tipo_end, end_bairro, end_cidade, end_cep, end_logradouro, end_tipo_logradouro, end_numero, end_complemento, end_data)
				SELECT null, __pes_id, tipo_endereco, end_pais, end_estado, end_tipo_end, end_bairro, end_cidade, end_cep, end_logradouro, end_tipo_logradouro, end_numero, end_complemento, end_data
				FROM endereco
				WHERE pes_id=_pes_id;

				INSERT INTO acadgeral__aluno(aluno_id, pes_id, arq_id, aluno_mae, aluno_pai, aluno_etnia, aluno_data_cadastro, aluno_titulo_eleitoral, aluno_zona_eleitoral, aluno_secao_eleitoral, aluno_cert_militar)
				SELECT aluno_id, __pes_id, arq_id, aluno_mae, aluno_pai, aluno_etnia, aluno_data_cadastro, aluno_titulo_eleitoral, aluno_zona_eleitoral, aluno_secao_eleitoral, aluno_cert_militar
				FROM acadgeral__aluno
				WHERE pes_id=_pes_id;

				INSERT INTO acadgeral__aluno_curso(alunocurso_id, cursocampus_id, aluno_id, alunocurso_data_cadastro, alunocurso_ensino_anter, alunocurso_formacao_ano, alunocurso_carteira)
				SELECT alunocurso_id, cursocampus_id, aa.aluno_id, alunocurso_data_cadastro, alunocurso_ensino_anter, alunocurso_formacao_ano, alunocurso_carteira
				FROM acadgeral__aluno_curso AS ac
				INNER JOIN acadgeral__aluno AS aa on ac.aluno_id=aa.aluno_id
				WHERE pes_id=_pes_id;
			END IF;
	END LOOP read_loop;

	CLOSE ConsultaPessoa;
END;;

DELIMITER ;

call migraEgressos();

SET SQL_SAFE_UPDATES=1;
SET FOREIGN_KEY_CHECKS=1;