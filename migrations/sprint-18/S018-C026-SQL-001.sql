set @func=0;
select @func:=(max(id)+1) from acesso_funcionalidades;
INSERT INTO acesso_funcionalidades VALUES (@func, '40', 'Exemplares de títulos', 'Biblioteca\\Controller\\Exemplar', 'fa-book', null, 'Ativa', 'Sim');
INSERT INTO `acesso_actions` VALUES
(null,@func,'index','Listagem',NULL,NULL,'Leitura','N'),
(null,@func,'search','JSON datatables',NULL,NULL,'Leitura','N'),
(null,@func,'baixa','Baixa de exemplares',NULL,NULL,'Escrita','N');