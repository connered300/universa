# view de titulos pagos pelos alunos do agente
CREATE OR  REPLACE VIEW view__financeiro_titulos_pagos_agente AS
SELECT
	pessoa.pes_id,
	agente.pes_id_indicacao,
	pessoa.pes_nome,
	titulos.tipotitulo_id,
	titulos.tipotitulo_nome,
	(agente_comissao_primeiro_nivel/100) AS agente_comissao_primeiro_nivel,
	(agente_comissao_demais_niveis/100) AS agente_comissao_demais_niveis,
	titulos.pes_id AS pes_id_reposnsavel_pagmento,
	titulos.alunocurso_id,
	titulos.titulo_id,
	COALESCE(titulo_valor_pago,0) titulo_valor_pago,
	date(titulos.titulo_data_pagamento) as titulo_data_pagamento
FROM acadgeral__aluno aluno
INNER JOIN acadgeral__aluno_curso alunoCurso USING(aluno_id)
INNER JOIN org__agente_educacional agente
		ON (agente.pes_id = alunoCurso.pes_id_agente OR agente.pes_id = aluno.pes_id_agenciador)
INNER JOIN pessoa ON pessoa.pes_id = agente.pes_id
JOIN financeiro__titulo_tipo tt
LEFT JOIN view__financeiro_titulos_totais  titulos
		ON titulos.alunocurso_id=alunoCurso.alunocurso_id AND
		   titulos.titulo_estado in('Pago', 'Pagamento Múltiplo') AND
		   titulos.tipotitulo_id=tt.tipotitulo_id;

# view de comissão de agente no nível 1
CREATE OR  REPLACE VIEW view__financeiro_apuracao_comissao_nivel1_agente AS
SELECT
	pes_id,
	pes_id_indicacao,
	pes_nome,
	tipotitulo_id,
	tipotitulo_nome,
	agente_comissao_primeiro_nivel,
	agente_comissao_demais_niveis,
	count(titulo_id) as quantidade,
	round(sum(titulo_valor_pago) * agente_comissao_primeiro_nivel, 2) as comissao,
	titulo_data_pagamento
FROM view__financeiro_titulos_pagos_agente
GROUP BY
	pes_id, tipotitulo_id, titulo_data_pagamento;



DROP VIEW IF EXISTS view__financeiro_apuracao_comissao_agente;
# CONSULTA de apuração de comissão de  agentes para tipos de título
SELECT
	pes_id,
	pes_id_indicacao,
	pes_nome,
	tipotitulo_id,
	tipotitulo_nome,
	agente_comissao_primeiro_nivel,
	agente_comissao_demais_niveis,
	quantidade,
	comissao,
	titulo_data_pagamento,
	comissao_n2,
	quantidade_n2,
	comissao_n3,
	quantidade_n3,
	(quantidade+quantidade_n2+quantidade_n3) quantidade_total,
	(comissao+comissao_n2+comissao_n3) as comissao_total
FROM (
SELECT
	n1.*,
	(n2.comissao * n1.agente_comissao_demais_niveis) as comissao_n2,
	n2.quantidade as quantidade_n2,
	(n3.comissao * n1.agente_comissao_demais_niveis) as comissao_n3,
	n3.quantidade as quantidade_n3
FROM view__financeiro_apuracao_comissao_nivel1_agente n1
LEFT JOIN view__financeiro_apuracao_comissao_nivel1_agente n2
	ON n2.pes_id_indicacao=n1.pes_id AND n2.tipotitulo_id=n1.tipotitulo_id
LEFT JOIN view__financeiro_apuracao_comissao_nivel1_agente n3
	ON n3.pes_id_indicacao=n2.pes_id AND n3.tipotitulo_id=n2.tipotitulo_id
WHERE n1.tipotitulo_id IS NOT NULL
GROUP BY
	pes_id , tipotitulo_id
) AS apuracao;