-- MySQL Workbench Synchronization
-- Generated: 2017-03-10 12:23
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Descrição: Script para que a consulta obedeça a ordenação por tipo de situação do aluno, assim permitindo que as dispensas sobreponham as reprovações em um GROUP BY.

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';



ALTER TABLE `acadperiodo__aluno_resumo`
CHANGE COLUMN `resaluno_situacao` `resaluno_situacao` ENUM('Aprovado', 'Equivalencia','DISPENSADO', 'E.FINAL', 'PR', 'Cancelado','Reprovado') NOT NULL COMMENT '' ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
