--
-- módulo sistema
--
DELETE FROM `acesso_privilegios_grupo` WHERE acesso_actions_id IN(
	select acesso_actions.id from acesso_funcionalidades
	inner join acesso_actions on funcionalidade=acesso_funcionalidades.id
	where func_controller IN(
		'Financeiro\\Controller\\Relatorio'
	)
) AND acesso_actions_id>=0 AND acesso_grupo_id>=0;

DELETE FROM `acesso_actions` WHERE funcionalidade in(
	SELECT acesso_funcionalidades.id from acesso_funcionalidades
	where func_controller IN(
		'Financeiro\\Controller\\Relatorio'
	)
) and id>0;

DELETE FROM acesso_funcionalidades where func_controller IN(
		'Financeiro\\Controller\\Relatorio'
) and id>0;

REPLACE INTO `acesso_funcionalidades` (modulo, func_nome, func_controller, func_status, func_visivel)
SELECT distinct acesso_modulos.id, func.descricao, func.controller,'Ativa','Sim'  from acesso_modulos,
(
	select 'Relatórios' as descricao, 'Financeiro\\Controller\\Relatorio' as controller from dual
) as func
where mod_nome like 'Financeiro';

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
(
	select 'index' as act,'Listagem de Relatórios' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'sintetico' as act,'Relatório sintético' as descricao,'Leitura' as tipo,'N' as usr from dual union all
	select 'comissao-agentes' as act,'Comissão de agentes' as descricao,'Leitura' as tipo,'N' as usr from dual
) as actions
where func_controller IN(
		'Financeiro\\Controller\\Relatorio'
);
REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
acesso_grupo
where func_controller IN(
		'Financeiro\\Controller\\Relatorio'
)
and grup_nome LIKE 'admin%';