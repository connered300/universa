-- MySQL Workbench Synchronization
-- Generated: 2017-04-03 11:12
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE IF NOT EXISTS `financeiro__filtro` (
  `filtro_id`       INT(10) UNSIGNED ZEROFILL                                  NOT NULL AUTO_INCREMENT,
  `filtro_nome`     VARCHAR(60)                                                NOT NULL,
  `filtro_estado`   SET('Pago', 'Aberto', 'Cancelado', 'Estorno', 'Alteracao') NULL     DEFAULT NULL,
  `filtro_situacao` ENUM('Ativo', 'Inativo')                                   NOT NULL,
  PRIMARY KEY (`filtro_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `financeiro__filtro_tipo` (
  `filtro_id`     INT(10) UNSIGNED ZEROFILL NOT NULL,
  `tipotitulo_id` INT(11)                   NOT NULL,
  PRIMARY KEY (`filtro_id`, `tipotitulo_id`),
  INDEX `fk_financeiro__filtro_financeiro__titulo_tipo_financeiro__t_idx` (`tipotitulo_id` ASC),
  INDEX `fk_financeiro__filtro_financeiro__titulo_tipo_financeiro__f_idx` (`filtro_id` ASC),
  CONSTRAINT `fk_financeiro__filtro_financeiro__titulo_tipo_financeiro__fil1`
  FOREIGN KEY (`filtro_id`)
  REFERENCES `financeiro__filtro` (`filtro_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_financeiro__filtro_financeiro__titulo_tipo_financeiro__tit1`
  FOREIGN KEY (`tipotitulo_id`)
  REFERENCES `financeiro__titulo_tipo` (`tipotitulo_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_general_ci;


ALTER TABLE `financeiro__filtro_tipo`
DROP FOREIGN KEY `fk_financeiro__filtro_financeiro__titulo_tipo_financeiro__fil1`,
DROP FOREIGN KEY `fk_financeiro__filtro_financeiro__titulo_tipo_financeiro__tit1`;
ALTER TABLE `financeiro__filtro_tipo`
ADD CONSTRAINT `fk_financeiro__filtro_financeiro__titulo_tipo_financeiro__fil1`
FOREIGN KEY (`filtro_id`)
REFERENCES `financeiro__filtro` (`filtro_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_financeiro__filtro_financeiro__titulo_tipo_financeiro__tit1`
FOREIGN KEY (`tipotitulo_id`)
REFERENCES `financeiro__titulo_tipo` (`tipotitulo_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;


SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;
