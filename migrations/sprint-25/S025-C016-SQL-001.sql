-- MySQL Workbench Synchronization
-- Generated: 2017-04-04 16:35
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `cidade`
CHANGE COLUMN `cid_cep` `cid_cep` VARCHAR(10) NULL DEFAULT NULL,
ADD COLUMN `cid_codigo_ibge` VARCHAR(10) NULL DEFAULT NULL
AFTER `cid_cep`,
ADD COLUMN `cid_ddd` VARCHAR(3) NULL DEFAULT NULL
AFTER `cid_codigo_ibge`,
ADD COLUMN `cid_lat` VARCHAR(180) NULL DEFAULT NULL
AFTER `cid_ddd`,
ADD COLUMN `cid_lng` VARCHAR(180) NULL DEFAULT NULL
AFTER `cid_lat`;

ALTER TABLE `estado`
ADD COLUMN `regiao_id` INT(10) ZEROFILL UNSIGNED NULL DEFAULT NULL AFTER `pais`,
ADD COLUMN `est_codigo_ibge` VARCHAR(10) NULL DEFAULT NULL AFTER `est_uf`,
ADD INDEX `fk_estado_regiao1_idx` (`regiao_id` ASC);

CREATE TABLE IF NOT EXISTS `zona` (
  `zona_id`   INT(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  `est_id`    INT(10) UNSIGNED ZEROFILL NOT NULL,
  `zona_nome` VARCHAR(80)               NULL     DEFAULT NULL,
  `zona_ddd`  VARCHAR(3)                NULL     DEFAULT NULL,
  PRIMARY KEY (`zona_id`),
  INDEX `fk_zona_estado1_idx` (`est_id` ASC),
  CONSTRAINT `fk_zona_estado1`
  FOREIGN KEY (`est_id`)
  REFERENCES `estado` (`est_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_general_ci;

ALTER TABLE `estado`
ADD CONSTRAINT `fk_estado_regiao1`
FOREIGN KEY (`regiao_id`)
REFERENCES `regiao` (`regiao_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;


SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;

-- MySQL Workbench Synchronization
-- Generated: 2017-04-04 16:56
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `estado`
DROP FOREIGN KEY `fk_estado_regiao1`;

ALTER TABLE `regiao`
DROP FOREIGN KEY `fk_regiao_regiao1`,
DROP FOREIGN KEY `fk_regiao_estado1`;

ALTER TABLE `estado`
ADD INDEX `fk_estado_regiao1_idx` (`regiao_id` ASC),
DROP INDEX `fk_estado_regiao1_idx`;

ALTER TABLE `regiao`
DROP COLUMN `regiao_pertence`,
DROP COLUMN `est_id`,
DROP INDEX `fk_regiao_estado1_idx`,
DROP INDEX `fk_regiao_regiao1_idx`;

ALTER TABLE `estado`
ADD CONSTRAINT `fk_estado_regiao1`
FOREIGN KEY (`regiao_id`)
REFERENCES `regiao` (`regiao_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;


SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'TRADITIONAL,ALLOW_INVALID_DATES';

TRUNCATE `logradouro`;
TRUNCATE `bairro`;
TRUNCATE `pais`;
TRUNCATE `estado`;
TRUNCATE `cidade`;
TRUNCATE `zona`;
TRUNCATE `regiao`;

SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;
