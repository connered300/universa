-- MySQL Workbench Synchronization
-- Generated: 2017-02-03 11:32
-- Model: New Model
-- Version: 1.0
-- Author: Versatile Tecnologia

/**

DATA: 03/02/2017
DESCRIÇÃO: ESTE SQL SERÁ UTILIZADO PRA ALTERAR O TIPO DO CAMPO QUE ARMAZENA CARGA HORÁRIA NA MATRIZ CURRICULAR.

OBS: O CAMPO CARGA HORÁRIA DE ESTÁGIO ESTÁ SENDO USADO PARA ÁREA DE TRANFERENCIA DOS DADOS EXISTENTES NOS CAMPO DE CARGA HORÁRIA PRÁTICA E TEÓRICA.

*/

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


-- BACKUP HORAS TÉORICAS
UPDATE acadperiodo__matriz_disciplina
SET
    per_disc_chestagio = per_disc_chteorica
WHERE
    per_disc_chestagio IS NULL
        OR per_disc_chestagio < 1;

-- LIMPANDO TABELA CARGA HORÁRIA TEÓRICA
UPDATE acadperiodo__matriz_disciplina
SET
    per_disc_chteorica = NULL
WHERE
    1;

-- ALTERANDO O TIPO DO CAMPO CARGA TEÓRICA
ALTER TABLE `acadperiodo__matriz_disciplina`
CHANGE COLUMN `per_disc_chteorica` `per_disc_chteorica` INT(11) NULL DEFAULT 0 COMMENT '' ;

-- RECUPERANDO A CARGA HORÁRIA TÉORICA
UPDATE acadperiodo__matriz_disciplina
SET
      per_disc_chteorica = HOUR(per_disc_chestagio)
WHERE
        1;

-- BACKUP HORAS PRÁTICAS
UPDATE acadperiodo__matriz_disciplina
SET
    per_disc_chestagio = per_disc_chpratica
WHERE
     1;

-- LIMPANDO TABELA CARGA HORÁRIA PRÁTICAS
UPDATE acadperiodo__matriz_disciplina
SET
    per_disc_chpratica = NULL
WHERE
    1;

-- ALTERANDO O TIPO DO CAMPO CARGA PRÁTICAS
ALTER TABLE `acadperiodo__matriz_disciplina`
CHANGE COLUMN `per_disc_chpratica` `per_disc_chpratica` INT(11) NULL DEFAULT 0 COMMENT '' ;

-- RECUPERANDO A CARGA HORÁRIA PRÁTICAS
UPDATE acadperiodo__matriz_disciplina
SET
      per_disc_chpratica = hour(per_disc_chestagio)
WHERE
    1;

-- ALTERANDO O TIPO DO CAMPO CARGA ESTÁGIO
ALTER TABLE `acadperiodo__matriz_disciplina`
CHANGE COLUMN `per_disc_chestagio` `per_disc_chestagio` INT(11) NULL DEFAULT 0 COMMENT '';

-- LIMPANDO A TABELA DE ESTÁGIO

UPDATE acadperiodo__matriz_disciplina
SET
      per_disc_chestagio = NULL
WHERE
    1;





