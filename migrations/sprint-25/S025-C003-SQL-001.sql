-- MySQL Workbench Synchronization
-- Generated: 2017-02-10 10:59
-- Model: New Model
-- Version: 1.0
-- Author: Versatile Tecnologia

/**

DESCRIÇÃO: ALTERAÇÃO NA TABELA DE DISCIPLINAS.


*/

ALTER TABLE `acadgeral__disciplina` CHANGE `disc_nome` `disc_nome` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Nome da disciplina.';
