-- MySQL Workbench Synchronization
-- Generated: 2017-03-24 16:28
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `acadgeral__aluno`
CHANGE COLUMN `aluno_cert_militar` `aluno_cert_militar` VARCHAR(45) NULL DEFAULT NULL COMMENT '' ;

ALTER TABLE `acadgeral__aluno`
CHANGE COLUMN `aluno_zona_eleitoral` `aluno_zona_eleitoral` VARCHAR(10) NULL DEFAULT NULL COMMENT '';


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
