-- Identifica id da funcionalidade
set @func=(SELECT id FROM acesso_funcionalidades WHERE func_controller LIKE 'Coordenacao%Controller%Docentes');
-- Insere nova ação
INSERT INTO `acesso_actions` VALUES
(null,@func,'docencia-anotacoes','Buscar anotações para listagem',NULL,NULL,'Leitura','N');
-- Pega id da ação criada
set @act=(select last_insert_id());
-- Adiciona permissao para os grupos da condicao
INSERT INTO `acesso_privilegios_grupo`
(`acesso_actions_id`, `acesso_grupo_id`)
SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'admin' OR grup_nome LIKE 'Coordena%';

-- Identifica id da funcionalidade
set @func=(SELECT id FROM acesso_funcionalidades WHERE func_controller LIKE 'Coordenacao%Controller%Docentes');
-- Insere nova ação
INSERT INTO `acesso_actions` VALUES
(null,@func,'docencia-anotacao-anexos','Buscar anexos de anotações',NULL,NULL,'Leitura','N');
-- Pega id da ação criada
set @act=(select last_insert_id());
-- Adiciona permissao para os grupos da condicao
INSERT INTO `acesso_privilegios_grupo`
(`acesso_actions_id`, `acesso_grupo_id`)
SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'admin' OR grup_nome LIKE 'Coordena%';