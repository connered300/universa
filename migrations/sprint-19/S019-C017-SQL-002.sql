
ALTER TABLE `acad_curso_config` ADD CONSTRAINT `fk_acad_curso_config_acad_curso_acad_curso`
  FOREIGN KEY (`curso_id`) REFERENCES `acad_curso` (`curso_id`)
  ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `acad_curso_config`
ADD COLUMN `cursoconfig_media_final_min` FLOAT NOT NULL DEFAULT 70 AFTER `cursoconfig_freq_min`;


ALTER TABLE `acadperiodo__docente_disciplina`
ADD COLUMN `docdisc_data_fechamento` DATETIME NULL AFTER `docdisc_data_fim`,
ADD COLUMN `docdisc_data_fechamento_final` DATETIME NULL AFTER `docdisc_data_fechamento`;



-- Identifica id da funcionalidade
set @func=(SELECT id FROM acesso_funcionalidades WHERE func_controller LIKE 'Professor%Controller%Disciplinas');

INSERT INTO `acesso_actions` VALUES (null,@func,'informacao-pendencia','Informaçãoo de pendências da disciplina',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Professor%';

INSERT INTO `acesso_actions` VALUES (null,@func,'alunos-pendencia-notas','Listagem de alunos sem notas de etapa',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Professor%';

INSERT INTO `acesso_actions` VALUES (null,@func,'finalizar-lancamentos-parte1','Efetiva lançamento de resumo e final',NULL,NULL,'Escrita','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Professor%';

INSERT INTO `acesso_actions` VALUES (null,@func,'alunos-final','Listagem de alunos que ficaram de final',NULL,NULL,'Leitura','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Professor%';

INSERT INTO `acesso_actions` VALUES (null,@func,'finalizar-lancamentos-parte2','Salva notas de alunos de final',NULL,NULL,'Escrita','N');
set @act=(select last_insert_id());
INSERT INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`) SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'Professor%';