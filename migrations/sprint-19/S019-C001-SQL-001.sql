SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE IF NOT EXISTS acadperiodo__diario_entrega (
diario_entrega_id INT(11) ZEROFILL NOT NULL AUTO_INCREMENT,
docdisc_id INT(11) ZEROFILL NOT NULL,
diario_frequencia_entrega_data DATETIME NULL DEFAULT NULL,
diario_anotacoes_entrega_data DATETIME NULL DEFAULT NULL,
diario_encerramento_entrega_data DATETIME NULL DEFAULT NULL,
diario_entrega_mes ENUM('janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho', 'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro', 'encerramento') NOT NULL,
PRIMARY KEY (diario_entrega_id),
INDEX fk_acadperiodo__diario_entrega_acadperiodo__docente_discipl_idx (docdisc_id ASC),
CONSTRAINT fk_acadperiodo__diario_entrega_acadperiodo__docente_disciplina1
FOREIGN KEY (docdisc_id)
REFERENCES acadperiodo__docente_disciplina (docdisc_id)
ON DELETE RESTRICT
ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
