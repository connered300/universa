-- Identifica id da funcionalidade
set @func=(SELECT id FROM acesso_funcionalidades WHERE func_controller LIKE 'Coordenacao%Controller%Docentes');
-- Insere nova ação
INSERT INTO `acesso_actions` VALUES
(null,@func,'painel','Painel do Docente',NULL,NULL,'Leitura','N');
-- Pega id da ação criada
set @act=(select last_insert_id());
-- Adiciona permissao para os grupos da condicao
INSERT INTO `acesso_privilegios_grupo`
(`acesso_actions_id`, `acesso_grupo_id`)
SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'admin' OR grup_nome LIKE 'Coordena%';

-- Identifica id da funcionalidade
set @func=(SELECT id FROM acesso_funcionalidades WHERE func_controller LIKE 'Coordenacao%Controller%Docentes');
-- Insere nova ação
INSERT INTO `acesso_actions` VALUES
(null,@func,'docencia-informacao','Informações da docencia',NULL,NULL,'Leitura','N');
-- Pega id da ação criada
set @act=(select last_insert_id());
-- Adiciona permissao para os grupos da condicao
INSERT INTO `acesso_privilegios_grupo`
(`acesso_actions_id`, `acesso_grupo_id`)
SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'admin' OR grup_nome LIKE 'Coordena%';

SET SQL_SAFE_UPDATES=0;
DELETE FROM acesso_privilegios_grupo WHERE acesso_actions_id in (
	SELECT id FROM acesso_actions where act_nome like 'get-info-docente'
);
DELETE FROM acesso_actions where act_nome like 'get-info-docente';
SET SQL_SAFE_UPDATES=1;

-- Identifica id da funcionalidade
set @func=(SELECT id FROM acesso_funcionalidades WHERE func_controller LIKE 'Coordenacao%Controller%Docentes');
-- Insere nova ação
INSERT INTO `acesso_actions` VALUES
(null,@func,'docencia-extensao','Datas limite de fechamento de etapa',NULL,NULL,'Leitura','N');
-- Pega id da ação criada
set @act=(select last_insert_id());
-- Adiciona permissao para os grupos da condicao
INSERT INTO `acesso_privilegios_grupo`
(`acesso_actions_id`, `acesso_grupo_id`)
SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'admin' OR grup_nome LIKE 'Coordena%';

SET SQL_SAFE_UPDATES=0;
DELETE FROM acesso_privilegios_grupo WHERE acesso_actions_id in (
	SELECT id FROM acesso_actions where act_nome like 'save-info-docente'
);
DELETE FROM acesso_actions where act_nome like 'save-info-docente';
SET SQL_SAFE_UPDATES=1;

-- Identifica id da funcionalidade
set @func=(SELECT id FROM acesso_funcionalidades WHERE func_controller LIKE 'Coordenacao%Controller%Docentes');
-- Insere nova ação
INSERT INTO `acesso_actions` VALUES
(null,@func,'docencia-extensao-gravar','Gravar extesão de etapa',NULL,NULL,'Leitura','N');
-- Pega id da ação criada
set @act=(select last_insert_id());
-- Adiciona permissao para os grupos da condicao
INSERT INTO `acesso_privilegios_grupo`
(`acesso_actions_id`, `acesso_grupo_id`)
SELECT @act,id FROM acesso_grupo WHERE grup_nome LIKE 'admin' OR grup_nome LIKE 'Coordena%';