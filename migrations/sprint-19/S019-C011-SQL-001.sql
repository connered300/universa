CREATE OR REPLACE VIEW view__notas_etapas AS
SELECT
    prof.pes_id professor_id,
    prof.pes_nome professor_nome,
    turma.turma_id,
    turma.turma_nome,
    docdisc.docdisc_id,
    disc.disc_id,
    disc.disc_nome,
    alunocur.alunocurso_id,
    pesaluno.pes_id aluno_id,
    pesaluno.pes_nome aluno_nome,
    situacaoGeral.situacao_id situacao_aluno_id,
    situacaoGeral.matsit_descricao situacao_aluno_descricao,
    situacaoDisc.situacao_id situacao_disciplina_id,
    situacaoDisc.matsit_descricao situacao_disciplina_descricao,
    alunodisc.alunodisc_id,
    (SELECT
            alunoetapa_nota
        FROM
            acadperiodo__etapa_aluno
                NATURAL JOIN
            acadperiodo__etapa_diario
                NATURAL JOIN
            acadperiodo__etapas
        WHERE
            etapa_ordem = 1
                AND alunodisc.alunodisc_id = alunodisc_id
                AND alunoetapa_nota IS NOT NULL
        ORDER BY alunodisc_sit_data DESC
        LIMIT 1) nota_etapa_01,
    (SELECT
            alunoetapa_nota
        FROM
            acadperiodo__etapa_aluno
                NATURAL JOIN
            acadperiodo__etapa_diario
                NATURAL JOIN
            acadperiodo__etapas
        WHERE
            etapa_ordem = 2
                AND alunodisc.alunodisc_id = alunodisc_id
                AND alunoetapa_nota IS NOT NULL
        ORDER BY alunodisc_sit_data DESC
        LIMIT 1) nota_etapa_02,
    (SELECT
            alunoetapa_nota
        FROM
            acadperiodo__etapa_aluno
                NATURAL JOIN
            acadperiodo__etapa_diario
                NATURAL JOIN
            acadperiodo__etapas
        WHERE
            etapa_ordem = 3
                AND alunodisc.alunodisc_id = alunodisc_id
                AND alunoetapa_nota IS NOT NULL
        ORDER BY alunodisc_sit_data DESC
        LIMIT 1) nota_etapa_03
FROM
    acadperiodo__etapa_aluno
        NATURAL JOIN
    acadperiodo__etapa_diario
        NATURAL JOIN
    acadperiodo__docente_disciplina docdisc
        NATURAL JOIN
    acadgeral__docente
        NATURAL JOIN
    pessoa prof
        INNER JOIN
    acadperiodo__turma turma ON turma.turma_id = docdisc.turma_id
        INNER JOIN
    acadperiodo__aluno_disciplina alunodisc ON alunodisc.alunodisc_id = acadperiodo__etapa_aluno.alunodisc_id
        INNER JOIN
    acadperiodo__aluno alunoper ON alunoper.alunoper_id = alunodisc.alunoper_id
        INNER JOIN
    acadgeral__aluno_curso alunocur ON alunocur.alunocurso_id = alunoper.alunocurso_id
        INNER JOIN
    acadgeral__aluno aluno ON aluno.aluno_id = alunocur.aluno_id
        INNER JOIN
    pessoa pesaluno ON pesaluno.pes_id = aluno.pes_id
        INNER JOIN
    acadgeral__situacao situacaoDisc ON situacaoDisc.situacao_id = alunodisc.situacao_id
        INNER JOIN
    acadgeral__disciplina disc ON disc.disc_id = docdisc.disc_id
        INNER JOIN
    acadgeral__situacao situacaoGeral ON situacaoGeral.situacao_id = alunoper.matsituacao_id
        INNER JOIN
    acadperiodo__letivo ON acadperiodo__letivo.per_id = turma.per_id
    GROUP BY alunodisc.alunodisc_id