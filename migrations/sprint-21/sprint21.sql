SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP TABLE IF EXISTS `acadgeral__aluno_curso_formacao`;
DROP TABLE IF EXISTS `acadgeral__aluno_formacao`;
DROP TABLE IF EXISTS `acadgeral__aluno_curso_formacao`;
DROP TABLE IF EXISTS `acadgeral__aluno_formacao`;
CREATE TABLE IF NOT EXISTS `acadgeral__aluno_formacao` (
 `formacao_id` INT UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
 `aluno_id` INT UNSIGNED ZEROFILL NOT NULL,
 `formacao_instituicao` VARCHAR(250) NOT NULL,
 `formacao_curso` VARCHAR(150) NOT NULL,
 `formacao_tipo_ensino` ENUM('Publico','Privado') NOT NULL,
 `formacao_tipo` ENUM('Ensino Médio', 'Graduação', 'Pós Graduação', 'Mestrado', 'Doutorado') NOT NULL,
 `formacao_ano` YEAR NOT NULL,
 `formacao_regime` ENUM('Anual', 'Semestral', 'Trimestral', 'Bimestral', 'Mensal') NOT NULL,
 `formacao_duracao` INT(11) NOT NULL,
 PRIMARY KEY (`formacao_id`),
 INDEX `fk_acadgeral__aluno_formacao_acadgeral__aluno1_idx` (`aluno_id` ASC),
 CONSTRAINT `fk_acadgeral__aluno_formacao_acadgeral__aluno1`
   FOREIGN KEY (`aluno_id`)
   REFERENCES `acadgeral__aluno` (`aluno_id`)
   ON DELETE RESTRICT
   ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
-- MySQL Workbench Synchronization
-- Generated: 2016-08-01 16:45
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `acadperiodo__aluno_historico` 
DROP FOREIGN KEY `fk_acadperiodo__aluno_historico_acadperiodo__turma1`;

ALTER TABLE `acadperiodo__aluno_disciplina_historico` 
DROP FOREIGN KEY `fk_acadperiodo__aluno_disciplina_historico_acadperiodo__turma1`;

ALTER TABLE `pessoa_fisica` 
ADD COLUMN `pes_falecido` ENUM('Sim','Não') NOT NULL DEFAULT 'Não' COMMENT '' AFTER `pes_nasc_uf`, 
COMMENT = 'Informação de pessoa fisica.' ;

ALTER TABLE `acadperiodo__aluno` ADD COLUMN `usuario_criador` INT(11) UNSIGNED ZEROFILL NOT NULL COMMENT '' AFTER `matsituacao_id`;
ALTER TABLE `acadperiodo__aluno` ADD COLUMN `alunoper_observacoes` TEXT NULL DEFAULT NULL COMMENT '' AFTER `alunoper_sit_data`;
ALTER TABLE `acadperiodo__aluno` ADD INDEX `fk_acadperiodo__aluno_acesso_pessoas1_idx` (`usuario_criador` ASC)  COMMENT '';
ALTER TABLE `acadperiodo__aluno` COMMENT = 'Informações do aluno em uma disciplina' ;

ALTER TABLE `acadperiodo__aluno_disciplina` DROP COLUMN `alunodisc_resultado`;
ALTER TABLE `acadperiodo__aluno_disciplina` DROP COLUMN `alunodisc_cargahoraria`;
ALTER TABLE `acadperiodo__aluno_disciplina` DROP COLUMN `alunodisc_nota`;
ALTER TABLE `acadperiodo__aluno_disciplina` DROP COLUMN `alunodisc_faltas`;
ALTER TABLE `acadperiodo__aluno_disciplina` ADD COLUMN `usuario_criador` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL COMMENT '' AFTER `situacao_id`;
ALTER TABLE `acadperiodo__aluno_disciplina` ADD INDEX `fk_acadperiodo__aluno_disciplina_acesso_pessoas1_idx` (`usuario_criador` ASC)  COMMENT '';

ALTER TABLE `acadperiodo__aluno` 
ADD CONSTRAINT `fk_acadperiodo__aluno_acesso_pessoas1`
  FOREIGN KEY (`usuario_criador`)
  REFERENCES `acesso_pessoas` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `acadperiodo__aluno_disciplina` 
ADD CONSTRAINT `fk_acadperiodo__aluno_disciplina_acesso_pessoas1`
  FOREIGN KEY (`usuario_criador`)
  REFERENCES `acesso_pessoas` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `acadperiodo__aluno_historico` 
ADD CONSTRAINT `fk_acadperiodo__aluno_historico_acadperiodo__turma1`
  FOREIGN KEY (`turma_id`)
  REFERENCES `acadperiodo__turma` (`turma_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `acadperiodo__aluno_disciplina_historico` 
ADD CONSTRAINT `fk_acadperiodo__aluno_disciplina_historico_acadperiodo__turma1`
  FOREIGN KEY (`turma_id`)
  REFERENCES `acadperiodo__turma` (`turma_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

INSERT INTO `acesso_actions` (`id`, `funcionalidade`, `act_nome`, `act_label`, `act_icon`, `act_info`, `act_tipo`, `act_visivel_usuario_final`) VALUES
(null, 00000000084, 'desligamento-aluno-pendencias', 'Pendencias do aluno', '', '', 'Escrita', 'S');

INSERT INTO `acadgeral__situacao` (`situacao_id`, `matsit_descricao`) VALUES (0000000015, 'Obito');


INSERT INTO `acadgeral__disciplina_tipo` (`tdisc_id`, `tdisc_descricao`) VALUES ('4', 'Equivalência');

INSERT INTO acadgeral__aluno_formacao
(aluno_id, formacao_instituicao, formacao_curso, formacao_tipo_ensino, formacao_tipo, formacao_ano, formacao_regime, formacao_duracao)
SELECT
    aluno_id,
    alunocurso_ensino_anter,
    'Ensino Médio',
    IF(ISNULL(alunocurso_ensino_anter_tipo), 'Publico', alunocurso_ensino_anter_tipo),
    'Ensino Médio',
    coalesce(alunocurso_formacao_ano,0),
    'Anual',
    3
FROM acadgeral__aluno
NATURAL JOIN acadgeral__aluno_curso;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- MySQL Workbench Synchronization
-- Generated: 2016-08-29 14:42
-- Model: New Model
-- Version: 1.0
-- Project: Universa
-- Author: Versatile Tecnologia

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER SCHEMA  DEFAULT CHARACTER SET utf8  DEFAULT COLLATE utf8_general_ci ;
ALTER TABLE `acad_curso` CHARACTER SET = utf8 , COLLATE = utf8_general_ci ;


ALTER TABLE `org_campus` 
ADD COLUMN `diretoria_pes_id` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL COMMENT 'Secretária Geral' AFTER `camp_site`,
ADD COLUMN `secretaria_pes_id` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL COMMENT 'Secretária Geral' AFTER `diretoria_pes_id`,
ADD INDEX `fk_org_campus_pessoa_fisica1_idx` (`secretaria_pes_id` ASC),
ADD INDEX `fk_org_campus_pessoa_fisica2_idx` (`diretoria_pes_id` ASC);

ALTER TABLE `boleto_conf_conta` 
ADD COLUMN `confcont_variacao` VARCHAR(45) NULL DEFAULT NULL AFTER `confcont_carteira`,
ADD COLUMN `confcont_convenio` VARCHAR(45) NULL DEFAULT NULL AFTER `confcont_variacao`,
ADD COLUMN `confcont_contrato` VARCHAR(45) NULL DEFAULT NULL AFTER `confcont_convenio`;

ALTER TABLE `acadgeral__aluno_curso` 
DROP COLUMN `alunocurso_formacao_ano`,
DROP COLUMN `alunocurso_ensino_anter_tipo`,
DROP COLUMN `alunocurso_ensino_anter`,
ADD COLUMN `alunocurso_data_colacao` DATE NULL DEFAULT NULL AFTER `alunocurso_observacoes`,
ADD COLUMN `alunocurso_data_expedicao_diploma` DATE NULL DEFAULT NULL AFTER `alunocurso_data_colacao`;

update acadperiodo__aluno_resumo set resaluno_situacao=trim(replace(replace(resaluno_situacao,'\r',''), '\t','')) where res_id>0;
update acadperiodo__aluno_resumo set resaluno_situacao='E.FINAL' where res_id>0 and resaluno_situacao='E. FINAL';


ALTER TABLE `acadperiodo__aluno_resumo` CHANGE COLUMN `resaluno_situacao` `resaluno_situacao` ENUM('Aprovado','Reprovado','Equivalencia','Cancelado','DISPENSADO','E.FINAL','PR') NOT NULL;
ALTER TABLE `acadperiodo__aluno_resumo` CHANGE COLUMN `resaluno_nota` `resaluno_nota` FLOAT(11) NULL DEFAULT NULL COMMENT 'Remover itens ,\'DISPENSADO\',\'E.FINAL\',\'PR\'' ;
ALTER TABLE `acadperiodo__aluno_resumo` ADD COLUMN `usuario` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL AFTER `res_id`;
ALTER TABLE `acadperiodo__aluno_resumo` ADD COLUMN `resaluno_origem` ENUM('Legado', 'Transferência', 'Novo título', 'Manual', 'Sistema') NOT NULL DEFAULT 'Sistema' AFTER `alunoper_id`;
ALTER TABLE `acadperiodo__aluno_resumo` ADD INDEX `fk_acadperiodo__aluno_resumo_acesso_pessoas1_idx` (`usuario` ASC);



UPDATE acadperiodo__aluno_resumo SET resaluno_origem = 'Legado'
WHERE alunoper_id IS NULL AND res_id >= 0;

ALTER TABLE `acadgeral__aluno_formacao` 
CHARACTER SET = utf8 , COLLATE = utf8_general_ci ,
ADD COLUMN `formacao_cidade` VARCHAR(100) NULL DEFAULT NULL AFTER `formacao_instituicao`,
ADD COLUMN `formacao_estado` VARCHAR(50) NULL DEFAULT NULL AFTER `formacao_cidade`;

CREATE TABLE IF NOT EXISTS `acadperiodo__resumo_equivalencia` (
  `equivalencia_id` INT(10) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `formacao_id` INT(10) UNSIGNED ZEROFILL NOT NULL,
  `res_id` INT(11) NOT NULL,
  `disc_id` INT(10) UNSIGNED ZEROFILL NOT NULL,
  PRIMARY KEY (`equivalencia_id`),
  INDEX `fk_acadperiodo__resumo_equivalencia_acadgeral__aluno_formac_idx` (`formacao_id` ASC),
  INDEX `fk_acadperiodo__resumo_equivalencia_acadperiodo__aluno_resu_idx` (`res_id` ASC),
  INDEX `fk_acadperiodo__resumo_equivalencia_acadgeral__disciplina1_idx` (`disc_id` ASC),
  INDEX `acadperiodo__resumo_equivalencia_unq` (`formacao_id` ASC, `res_id` ASC, `disc_id` ASC),
  CONSTRAINT `fk_acadperiodo__resumo_equivalencia_acadgeral__aluno_formacao1`
    FOREIGN KEY (`formacao_id`)
    REFERENCES `acadgeral__aluno_formacao` (`formacao_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_acadperiodo__resumo_equivalencia_acadperiodo__aluno_resumo1`
    FOREIGN KEY (`res_id`)
    REFERENCES `acadperiodo__aluno_resumo` (`res_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_acadperiodo__resumo_equivalencia_acadgeral__disciplina1`
    FOREIGN KEY (`disc_id`)
    REFERENCES `acadgeral__disciplina` (`disc_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

ALTER TABLE `org_campus` 
ADD CONSTRAINT `fk_org_campus_pessoa_fisica1`
  FOREIGN KEY (`secretaria_pes_id`)
  REFERENCES `pessoa_fisica` (`pes_id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_org_campus_pessoa_fisica2`
  FOREIGN KEY (`diretoria_pes_id`)
  REFERENCES `pessoa_fisica` (`pes_id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `boleto_conf_conta` 
ADD CONSTRAINT `boleto_conf_conta_ibfk_1`
  FOREIGN KEY (`arq_id`)
  REFERENCES `arquivo` (`arq_id`)
  ON UPDATE CASCADE;

ALTER TABLE `acadperiodo__aluno_resumo` 
ADD CONSTRAINT `fk_acadperiodo__aluno_resumo_acesso_pessoas1`
  FOREIGN KEY (`usuario`)
  REFERENCES `acesso_pessoas` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

  ALTER TABLE `acadgeral__aluno_formacao` 
CHANGE COLUMN `formacao_instituicao` `formacao_instituicao` VARCHAR(250) NOT NULL ,
CHANGE COLUMN `formacao_curso` `formacao_curso` VARCHAR(150) NOT NULL ,
CHANGE COLUMN `formacao_tipo_ensino` `formacao_tipo_ensino` ENUM('Publico','Privado') NOT NULL ,
CHANGE COLUMN `formacao_tipo` `formacao_tipo` ENUM('Ensino Médio','Graduação','Pós Graduação','Mestrado','Doutorado') NOT NULL ,
CHANGE COLUMN `formacao_regime` `formacao_regime` ENUM('Anual','Semestral','Trimestral','Bimestral','Mensal') NOT NULL ;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
