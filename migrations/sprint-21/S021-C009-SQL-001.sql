INSERT INTO `acadgeral__disciplina_tipo` (`tdisc_id`, `tdisc_descricao`) VALUES ('4', 'Equivalência');

UPDATE acadperiodo__aluno_resumo SET resaluno_origem = 'Legado'
WHERE alunoper_id IS NULL AND res_id >= 0;

INSERT INTO acadgeral__aluno_formacao
(aluno_id, formacao_instituicao, formacao_curso, formacao_tipo_ensino, formacao_tipo, formacao_ano, formacao_regime, formacao_duracao)
SELECT
    aluno_id,
    alunocurso_ensino_anter,
    'Ensino Médio',
    IF(ISNULL(alunocurso_ensino_anter_tipo), 'Publico', alunocurso_ensino_anter_tipo),
    'Ensino Médio',
    alunocurso_formacao_ano,
    'Anual',
    3
FROM acadgeral__aluno
NATURAL JOIN acadgeral__aluno_curso;