SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP TABLE IF EXISTS `acadgeral__aluno_curso_formacao`;
DROP TABLE IF EXISTS `acadgeral__aluno_formacao`;
DROP TABLE IF EXISTS `acadgeral__aluno_curso_formacao`;
DROP TABLE IF EXISTS `acadgeral__aluno_formacao`;
CREATE TABLE IF NOT EXISTS `acadgeral__aluno_formacao` (
 `formacao_id` INT UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
 `aluno_id` INT UNSIGNED ZEROFILL NOT NULL,
 `formacao_instituicao` VARCHAR(250) NOT NULL,
 `formacao_curso` VARCHAR(150) NOT NULL,
 `formacao_tipo_ensino` ENUM('Publico','Privado') NOT NULL,
 `formacao_tipo` ENUM('Ensino Médio', 'Graduação', 'Pós Graduação', 'Mestrado', 'Doutorado') NOT NULL,
 `formacao_ano` YEAR NOT NULL,
 `formacao_regime` ENUM('Anual', 'Semestral', 'Trimestral', 'Bimestral', 'Mensal') NOT NULL,
 `formacao_duracao` INT(11) NOT NULL,
 PRIMARY KEY (`formacao_id`),
 INDEX `fk_acadgeral__aluno_formacao_acadgeral__aluno1_idx` (`aluno_id` ASC),
 CONSTRAINT `fk_acadgeral__aluno_formacao_acadgeral__aluno1`
   FOREIGN KEY (`aluno_id`)
   REFERENCES `acadgeral__aluno` (`aluno_id`)
   ON DELETE RESTRICT
   ON UPDATE CASCADE)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
