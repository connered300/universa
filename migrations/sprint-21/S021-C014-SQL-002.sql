ALTER TABLE `acadgeral__aluno_formacao`
ADD COLUMN `formacao_cidade` VARCHAR(100) NULL AFTER `formacao_instituicao`,
ADD COLUMN `formacao_estado` VARCHAR(50) NULL AFTER `formacao_cidade`;
