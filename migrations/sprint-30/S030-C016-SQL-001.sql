ALTER TABLE `org__agente_educacional`
CHANGE COLUMN `agente_comissao_primeiro_nivel` `agente_comissao_primeiro_nivel` FLOAT(5,2) NULL DEFAULT 0 ;

ALTER TABLE `org__agente_educacional`
CHANGE COLUMN `agente_comissao_demais_niveis` `agente_comissao_demais_niveis`  FLOAT(5,2) NULL DEFAULT 0  ;