SET FOREIGN_KEY_CHECKS = 0;
ALTER TABLE `acad_curso`
ADD COLUMN `arq_id_carteirinha` INT(10) UNSIGNED ZEROFILL,
ADD INDEX `fk_acad_curso_carteirinha_idx1` (`arq_id_carteirinha` ASC),
ADD CONSTRAINT `fk_acad_curso_carteirinha`
FOREIGN KEY (`arq_id_carteirinha`)
REFERENCES `arquivo` (`arq_id`)
  ON DELETE SET NULL
  ON UPDATE CASCADE;

ALTER TABLE `org_ies`
ADD `arq_id_carteirinha` INT(10) UNSIGNED ZEROFILL,
ADD INDEX `fk_org_ies_carteirinha_idx2` (`arq_id_carteirinha` ASC),
ADD CONSTRAINT `fk_org_ies_carteirinha`
FOREIGN KEY (`arq_id_carteirinha`)
REFERENCES `arquivo` (`arq_id`)
  ON DELETE SET NULL
  ON UPDATE CASCADE;

ALTER TABLE `org_campus`
ADD `arq_id_carteirinha` INT(10) UNSIGNED ZEROFILL,
ADD INDEX `fk_org_campus_carteirinha_idx3` (`arq_id_carteirinha` ASC),
ADD CONSTRAINT `fk_org_campus_carteirinha`
FOREIGN KEY (`arq_id_carteirinha`)
REFERENCES `arquivo` (`arq_id`)
  ON DELETE SET NULL
  ON UPDATE CASCADE;

ALTER TABLE acadgeral__aluno DROP FOREIGN KEY fk_acadgeral__aluno_arquivo1;
ALTER TABLE acadgeral__aluno ADD CONSTRAINT `fk_acadgeral__aluno_arquivo1`
FOREIGN KEY (`arq_id`) REFERENCES `arquivo` (`arq_id`) ON DELETE SET NULL ON UPDATE CASCADE;

SET FOREIGN_KEY_CHECKS = 1;