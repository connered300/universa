SET SQL_SAFE_UPDATES = 0;

UPDATE acesso_funcionalidades SET func_nome = 'Gestão de Protocolos'
  WHERE func_controller in ('Protocolo\\Controller\\Protocolo');

UPDATE acesso_funcionalidades SET func_nome = 'Gestão de Setores'
  WHERE func_controller in ('Protocolo\\Controller\\ProtocoloSetor');

SET SQL_SAFE_UPDATES = 1;