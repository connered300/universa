DROP TABLE IF EXISTS `sis__campo_valor`;
DROP TABLE IF EXISTS `sis__campo_personalizado`;
DROP TABLE IF EXISTS `sis__campo_entidade`;
DROP TABLE IF EXISTS `sis__campo_tipo`;

CREATE TABLE IF NOT EXISTS `sis__campo_entidade` (
  `entidade_id`        VARCHAR(255) NOT NULL,
  `entidade_descricao` VARCHAR(255) NOT NULL,
  `entidade_tabela`    VARCHAR(255) NOT NULL,
  `entidade_chave`     VARCHAR(255) NOT NULL,
  PRIMARY KEY (`entidade_id`),
  KEY `entidade_id` (`entidade_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1;

CREATE TABLE IF NOT EXISTS `sis__campo_personalizado` (
  `campo_id`                INT(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  `campo_nome`              VARCHAR(64)               NOT NULL,
  `campo_descricao`         VARCHAR(255)                       DEFAULT NULL,
  `campo_tamanho_min`       INT(2)                             DEFAULT NULL,
  `campo_tamanho_max`       INT(10)                            DEFAULT NULL,
  `campo_expressao_regular` VARCHAR(255)                       DEFAULT NULL,
  `campo_valor_padrao`      TEXT,
  `campo_obrigatorio`       ENUM('Sim', 'Não')                 DEFAULT 'Não',
  `campo_visivel_painel`    ENUM('Sim', 'Não')                 DEFAULT 'Não',
  `campo_exibicao_externa`  ENUM('Sim', 'Não')                 DEFAULT 'Não',
  `campo_ativo`             ENUM('Sim', 'Não')                 DEFAULT 'Sim',
  `campo_multiplo`          ENUM('Sim', 'Não')                 DEFAULT 'Não',
  `campo_valores`           TEXT,
  `tipo_campo`              VARCHAR(255)              NOT NULL,
  `entidade_id`             VARCHAR(255)              NOT NULL,

  PRIMARY KEY (`campo_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1
  AUTO_INCREMENT = 1;

CREATE TABLE IF NOT EXISTS `sis__campo_tipo` (
  `tipo_campo`     VARCHAR(255) NOT NULL,
  `tipo_descricao` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`tipo_campo`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1;

CREATE TABLE IF NOT EXISTS `sis__campo_valor` (
  `campo_id`    INT(10) UNSIGNED ZEROFILL NOT NULL,
  `campo_valor` TEXT,
  `campo_chave` VARCHAR(255)              NOT NULL DEFAULT '',
  PRIMARY KEY (`campo_id`, `campo_chave`),
  KEY `fk_sis__campo_valor_campo_idx` (`campo_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1;

ALTER TABLE `sis__campo_personalizado`
ADD INDEX `fk_sis__campo_personalizado_entidade_idx` (`entidade_id` ASC),
ADD INDEX `fk_sis__campo_personalizado_tipo_idx` (`tipo_campo` ASC);
ALTER TABLE `sis__campo_personalizado`
ADD CONSTRAINT `fk_sis__campo_personalizado_entidade`
FOREIGN KEY (`entidade_id`)
REFERENCES `sis__campo_entidade` (`entidade_id`)
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_sis__campo_personalizado_tipo`
FOREIGN KEY (`tipo_campo`)
REFERENCES `sis__campo_tipo` (`tipo_campo`)
  ON UPDATE CASCADE;

ALTER TABLE `sis__campo_valor`
ADD CONSTRAINT `fk_sis__campo_valor_campo` FOREIGN KEY (`campo_id`) REFERENCES `sis__campo_personalizado` (`campo_id`)
  ON UPDATE CASCADE;

INSERT INTO `sis__campo_tipo` (`tipo_campo`, `tipo_descricao`) VALUES ('text', 'Texto curto');
INSERT INTO `sis__campo_tipo` (`tipo_campo`, `tipo_descricao`) VALUES ('number', 'Numero');
INSERT INTO `sis__campo_tipo` (`tipo_campo`, `tipo_descricao`) VALUES ('textarea', 'Texto Longo');
INSERT INTO `sis__campo_tipo` (`tipo_campo`, `tipo_descricao`) VALUES ('hidden', 'Campo Oculto');
INSERT INTO `sis__campo_tipo` (`tipo_campo`, `tipo_descricao`) VALUES ('url', 'Endereço eletronico(URL)');
INSERT INTO `sis__campo_tipo` (`tipo_campo`, `tipo_descricao`) VALUES ('file', 'Arquivo');
INSERT INTO `sis__campo_tipo` (`tipo_campo`, `tipo_descricao`) VALUES ('password', 'Senha');

INSERT INTO `sis__campo_entidade` (`entidade_id`, `entidade_descricao`, `entidade_tabela`, `entidade_chave`) VALUES
  ('Aluno', 'Informações do aluno', 'acadgeral__aluno', 'aluno_id'),
  ('Curso do aluno', 'Informações sobre o curso do aluno', 'acadgeral__aluno_curso', 'alunocurso_id');

/*Permissão*/
SET @mod = (SELECT id
            FROM acesso_modulos
            WHERE mod_route = 'sistema/default'
            LIMIT 0, 1);

REPLACE INTO `acesso_funcionalidades` (`modulo`, `func_nome`, `func_controller`, `func_icon`, `func_info`, `func_status`, `func_visivel`)
VALUES
  (@mod, 'Gestão de Entidades de Campos', 'Sistema\\Controller\\SisCampoEntidade', '', '', 'Ativa', 'Sim');
REPLACE INTO `acesso_funcionalidades` (`modulo`, `func_nome`, `func_controller`, `func_icon`, `func_info`, `func_status`, `func_visivel`)
VALUES
  (@mod, 'Gestão de Campo Personalizado', 'Sistema\\Controller\\SisCampoPersonalizado', '', '', 'Ativa', 'Sim');
REPLACE INTO `acesso_funcionalidades` (`modulo`, `func_nome`, `func_controller`, `func_icon`, `func_info`, `func_status`, `func_visivel`)
VALUES
  (@mod, 'Gestão de Tipos de Campo Personalizado', 'Sistema\\Controller\\SisCampoTipo', '', '', 'Ativa', 'Sim');
/*REPLACE INTO `acesso_funcionalidades` (`modulo`, `func_nome`, `func_controller`, `func_icon`, `func_info`, `func_status`, `func_visivel`) VALUES
(@mod, 'Gestão de Valores de Personalizado', 'Sistema\\Controller\\SisCampoValor', '', '', 'Ativa', 'Sim');*/


REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT DISTINCT
    acesso_funcionalidades.id,
    actions.act,
    actions.descricao,
    actions.tipo,
    actions.usr
  FROM acesso_funcionalidades,
    (
      SELECT
        'index'    AS act,
        'Listagem' AS descricao,
        'Leitura'  AS tipo,
        'N'        AS usr
      FROM dual
      UNION ALL
      SELECT
        'remove'  AS act,
        'Remover' AS descricao,
        'Leitura' AS tipo,
        'N'       AS usr
      FROM dual
      UNION ALL
      SELECT
        'add'     AS act,
        'Criar'   AS descricao,
        'Leitura' AS tipo,
        'N'       AS usr
      FROM dual
      UNION ALL
      SELECT
        'edit'    AS act,
        'Editar'  AS descricao,
        'Leitura' AS tipo,
        'N'       AS usr
      FROM dual
      UNION ALL
      SELECT
        'search-for-json' AS act,
        'Autocomplete'    AS descricao,
        'Leitura'         AS tipo,
        'N'               AS usr
      FROM dual
      UNION ALL
      SELECT
        'search'     AS act,
        'Datatables' AS descricao,
        'Leitura'    AS tipo,
        'N'          AS usr
      FROM dual
    ) AS actions
  WHERE func_controller IN (
    'Sistema\\Controller\\SisCampoEntidade',
    'Sistema\\Controller\\SisCampoPersonalizado',
    'Sistema\\Controller\\SisCampoTipo',
    'Sistema\\Controller\\SisCampoValor'
  );
REPLACE INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
    ,
    acesso_grupo
  WHERE func_controller IN (
    'Sistema\\Controller\\SisCampoEntidade',
    'Sistema\\Controller\\SisCampoPersonalizado',
    'Sistema\\Controller\\SisCampoTipo',
    'Sistema\\Controller\\SisCampoValor'
  )
        AND grup_nome LIKE 'Admin%';


REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT DISTINCT
    acesso_funcionalidades.id,
    actions.act,
    actions.descricao,
    actions.tipo,
    actions.usr
  FROM acesso_funcionalidades,
    (
      SELECT
        'busca-registros-internos' AS act,
        'Dados internos'           AS descricao,
        'Leitura'                  AS tipo,
        'N'                        AS usr
    ) AS actions
  WHERE func_controller IN ('Sistema\\Controller\\SisCampoEntidade');

REPLACE INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
    ,
    acesso_grupo
  WHERE func_controller IN (
    'Sistema\\Controller\\SisCampoEntidade'
  )
        AND grup_nome LIKE 'admin%';