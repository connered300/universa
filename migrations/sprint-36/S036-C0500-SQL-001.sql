ALTER TABLE `acad_curso_config`
CHANGE COLUMN `cursoconfig_nota_fracionada` `cursoconfig_nota_fracionada` TEXT NOT NULL ;

UPDATE `acad_curso_config` SET `cursoconfig_nota_fracionada`='2' WHERE cursoconfig_nota_fracionada = 'Sim' and curso_id>0;
UPDATE `acad_curso_config` SET `cursoconfig_nota_fracionada`='0' WHERE cursoconfig_nota_fracionada = 'Não' and curso_id>0;


ALTER TABLE `acad_curso_config`
CHANGE COLUMN `cursoconfig_nota_fracionada` `cursoconfig_nota_fracionada` INT(2) NOT NULL DEFAULT 0 ;