
/*Caso houver algum registro com Cartao na descrição será sobrescrito, por esse dados abaixo*/
INSERT INTO `financeiro__meio_pagamento` VALUES (null,'Cartão', 'a prazo', 'Não', 'ativa', 'Não', 'Sim')
ON DUPLICATE KEY UPDATE meio_pagamento_descricao = 'Cartão';



ALTER TABLE `financeiro__titulo_tipo` ADD COLUMN `tipotitulo_prioridade` INT DEFAULT 1;
UPDATE financeiro__titulo_tipo set tipotitulo_prioridade=tipotitulo_id where tipotitulo_id>0;

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT DISTINCT
    acesso_funcionalidades.id,
    actions.act,
    actions.descricao,
    actions.tipo,
    actions.usr
  FROM acesso_funcionalidades,
    (
      SELECT
        'alteraPrioridade'      AS act,
        'Prioridade tipo título' AS descricao,
        'Leitura'            AS tipo,
        'N'                  AS usr
    ) AS actions
  WHERE func_controller IN ('Financeiro\\Controller\\FinanceiroTituloTipo');

REPLACE INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
    ,
    acesso_grupo
  WHERE func_controller IN (
    'Financeiro\\Controller\\FinanceiroTituloTipo'
  )
        AND grup_nome LIKE 'admin%';
