ALTER TABLE `biblioteca__titulo_autor`
CHANGE COLUMN `titulo_autor_tipo` `titulo_autor_tipo` ENUM('Autor principal', 'Colaborador', 'Organizador', 'Tradutor', 'Orientador', 'Coordenador') NOT NULL;
UPDATE pessoa
SET
  pessoa.con_contato_email    = if(
      pessoa.con_contato_email != "" AND pessoa.con_contato_email IS NOT NULL,
      pessoa.con_contato_email,
      (
        SELECT con_contato
        FROM contato
        WHERE pessoa = pessoa.pes_id AND tipo_contato = 1 AND con_contato != ''
        ORDER BY con_contato DESC
        LIMIT 0, 1
      )
  ),
  pessoa.con_contato_telefone = if(
      pessoa.con_contato_telefone != "" AND pessoa.con_contato_telefone IS NOT NULL,
      pessoa.con_contato_telefone,
      (
        SELECT con_contato
        FROM contato
        WHERE pessoa = pessoa.pes_id AND tipo_contato = 2 AND con_contato != ''
        ORDER BY con_contato DESC
        LIMIT 0, 1
      )
  ),
  pessoa.con_contato_celular  = if(
      pessoa.con_contato_celular != "" AND pessoa.con_contato_celular IS NOT NULL,
      pessoa.con_contato_celular,
      (
        SELECT con_contato
        FROM contato
        WHERE pessoa = pessoa.pes_id AND tipo_contato = 3 AND con_contato != ''
        ORDER BY con_contato DESC
        LIMIT 0, 1
      )
  )
WHERE pes_id >= 1;

ALTER TABLE `financeiro__filtro` CHANGE `filtro_estado`  `filtro_estado` SET('Pago', 'Aberto', 'Cancelado', 'Estorno', 'Alteracao', 'Pagamento Múltiplo', 'Estorno De Pagamento Múltiplo') NULL DEFAULT NULL;
