CREATE TABLE IF NOT EXISTS `biblioteca__virtual` (
  `pes_id` INT(10) UNSIGNED ZEROFILL NOT NULL,
  `usuario_categoria` INT(1) NOT NULL,
  PRIMARY KEY (`pes_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

ALTER TABLE `biblioteca__virtual`
	ADD CONSTRAINT `biblioteca__virtual_pessoa`
  FOREIGN KEY (`pes_id`) REFERENCES `pessoa` (`pes_id`);

REPLACE INTO `acesso_funcionalidades` (modulo, func_nome, func_controller, func_status, func_visivel)
  SELECT distinct acesso_modulos.id, func.descricao, func.controller,'Ativa','Sim'  from acesso_modulos,
    (
      select 'Biblioteca Virtual' as descricao, 'Biblioteca\\Controller\\BibliotecaVirtual' as controller from dual
    ) as func
  where mod_nome like 'Biblioteca';

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
    (
      select 'index' as act,'Index' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'add' as act,'Index' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'search' as act,'Busca' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'remove' as act,'Busca' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'criar-url' as act,'Adicionar' as descricao,'Leitura' as tipo,'N' as usr FROM dual
    ) as actions
  where func_controller IN('Biblioteca\\Controller\\BibliotecaVirtual');

REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
  select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
    inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
    acesso_grupo
  where func_controller IN(
    'Biblioteca\\Controller\\BibliotecaVirtual'
  )
  and grup_nome LIKE 'admin%';

REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
  select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
    inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
    acesso_grupo
  where
    func_controller IN('Biblioteca\\Controller\\BibliotecaVirtual')
  AND
    act_nome IN('criar-url')
  AND
    grup_nome LIKE 'aluno%';