ALTER TABLE ``sis__fila_maxipago`
CHANGE COLUMN `fila_situacao` `fila_situacao` ENUM('Pendente', 'Enviado', 'Processando', 'Erro', 'Falha no Pagamento', 'Cancelado', 'Estorno') NULL DEFAULT 'Pendente' ;
