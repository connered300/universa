CREATE TABLE IF NOT EXISTS financeiro__cartao (
  cartao_id         INT(11) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  cartao_titular    VARCHAR(70)        NOT NULL,
  cartao_numero     VARCHAR(50)        NOT NULL,
  cartao_vencimento VARCHAR(50)        NOT NULL,
  cartao_cvv        VARCHAR(50)        NOT NULL,
  cartao_excluido   ENUM('Sim', 'Não') NOT NULL DEFAULT 'Não',
  -- pes_id é a chave da criptografia
  pes_id            INT(11) ZEROFILL UNSIGNED NOT NULL,
  PRIMARY KEY (cartao_id)
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_general_ci;

ALTER TABLE `financeiro__cartao`
ADD INDEX `fk_financeiro__cartao_pessoa_idx` (`pes_id` ASC);
ALTER TABLE `financeiro__cartao`
ADD CONSTRAINT `fk_financeiro__cartao_pessoa`
FOREIGN KEY (`pes_id`)
REFERENCES `pessoa` (`pes_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;


/*Armazena o retorno do Maxi Pago, segundo a página 52 pode retornar códigos para as possiveis respostas*/
ALTER TABLE `financeiro__pagamento`
ADD COLUMN `pagamento_retorno_api` TEXT NULL DEFAULT NULL;

/*Armazena com qual cartão foi pago*/
ALTER TABLE `financeiro__pagamento`
ADD COLUMN `cartao_id` INT(11) ZEROFILL UNSIGNED NULL DEFAULT NULL;


/* Ligação do financeiro__pagamento com financeiro__cartao*/
ALTER TABLE `financeiro__pagamento`
ADD INDEX `fk_financeiro__pagamento_cartao_idx` (`cartao_id` ASC);
ALTER TABLE `financeiro__pagamento`
ADD CONSTRAINT `fk_financeiro__pagamento_cartao`
FOREIGN KEY (`cartao_id`)
REFERENCES `financeiro__cartao` (`cartao_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;


/*Tabela que ira armazenar as integracoes do maxi pago*/
CREATE TABLE IF NOT EXISTS sis__fila_maxipago (
  `fila_id`                 INT(11) ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `titulo_id`               INT(11) ZEROFILL UNSIGNED NOT NULL,
  `fila_conteudo`           TEXT     NOT NULL,
  `fila_retorno`            TEXT     NULL                                      DEFAULT NULL,
  `fila_data_cadastro`      DATETIME NOT NULL,
  `fila_data_processamento` DATETIME NULL                                      DEFAULT NULL,
  `fila_data_finalizacao`   DATETIME NULL                                      DEFAULT NULL,
  `fila_situacao`           ENUM('Pendente', 'Enviado', 'Processando', 'Erro') DEFAULT 'Pendente',
  PRIMARY KEY (fila_id)
)
  ENGINE = InnoDB;


ALTER TABLE `sis__fila_maxipago`
ADD INDEX `fk_sis__fila_maxipago_titulo_idx` (`titulo_id` ASC);
ALTER TABLE `sis__fila_maxipago`
ADD CONSTRAINT `fk_sis__fila_maxipago_titulo`
FOREIGN KEY (`titulo_id`)
REFERENCES `financeiro__titulo` (`titulo_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;


/* Adicionar chave do cartão ao plano de pagamento*/
ALTER TABLE `financeiro__aluno_config_pgto_curso`
ADD COLUMN `cartao_id` INT(11) ZEROFILL NULL DEFAULT NULL;

ALTER TABLE `financeiro__aluno_config_pgto_curso`
ADD INDEX `fk_financeiro__aluno_config_pgto_curso_cartao_idx` (`cartao_id` ASC);
ALTER TABLE `financeiro__aluno_config_pgto_curso`
ADD CONSTRAINT `fk_financeiro__aluno_config_pgto_curso_cartao`
FOREIGN KEY (`cartao_id`)
REFERENCES `financeiro__cartao` (`cartao_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;


/* Adicionar na tabela de titulos , referência da tabela de plano de pagamento */

ALTER TABLE `financeiro__titulo`
ADD COLUMN `config_pgto_curso_id` INT(11) ZEROFILL UNSIGNED NULL DEFAULT NULL;

ALTER TABLE `financeiro__titulo`
ADD INDEX `fk_financeiro__titulo_config_curso_pgt_idx` (`config_pgto_curso_id` ASC);
ALTER TABLE `financeiro__titulo`
ADD CONSTRAINT `fk_financeiro__titulo_config_curso_pgt`
FOREIGN KEY (`config_pgto_curso_id`)
REFERENCES `financeiro__aluno_config_pgto_curso` (`config_pgto_curso_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

