UPDATE financeiro__titulo SET alunoper_id = NULL where alunoper_id not in (SELECT alunoper_id FROM acadperiodo__aluno);
ALTER TABLE  `financeiro__titulo` ADD FOREIGN KEY (  `alunoper_id` ) REFERENCES  `acadperiodo__aluno` (`alunoper_id`) ON DELETE RESTRICT ON UPDATE CASCADE ;
ALTER TABLE  `financeiro__titulo` DROP FOREIGN KEY  `financeiro__titulo_ibfk_1`;
ALTER TABLE  `financeiro__titulo` ADD FOREIGN KEY (  `tituloconf_id` ) REFERENCES  `financeiro__titulo_config` (`tituloconf_id`) ON DELETE RESTRICT ON UPDATE CASCADE ;