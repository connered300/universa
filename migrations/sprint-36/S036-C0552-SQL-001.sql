INSERT INTO `sis__robo` (`robo_nome`, `robo_url`, `robo_tempo_execucao`, `robo_ultima_execucao`, `robo_situacao`)
VALUES ('Integração MaxiPago', '/sistema/sis-fila-maxipago/executar', '25', '2017-06-29 17:00:00', 'Inativo');

INSERT INTO `org__comunicacao_tipo` (`tipo_id`, `tipo_descricao`, `tipo_contexto`)
VALUES (8, 'Cobrança Cartão', 'Aluno');

INSERT INTO `org__comunicacao_modelo` (`tipo_id`, `usuario`, `modelo_nome`, `modelo_tipo`, `modelo_assunto`, `modelo_conteudo`, `modelo_data_inicio`, `modelo_data_fim`)
VALUES (8, '1', 'Email de Recorrência Maxipago', 'Email', 'Financeiro {{ies.iesSigla}}', 'Prezado(a) {{ pessoa.pesNome }}, obrigado por escolher a {{ies.iesSigla}} !<br/> &nbsp; &nbsp; &nbsp;N&atilde;o foi possivel realizar o pagamento de uma ou mais parcelas do seu curso com o cart&atilde;o informado , por favor acesse o link <a href="{{urlRecorrencia}}">{{urlRecorrencia}}</a> <br/> Atenciosamente,<br /> {{ies.iesNome}}<br /> &nbsp;', '2018-01-01 00:00:00', '2019-01-01 00:00:00');


INSERT INTO `org__comunicacao_modelo` (`tipo_id`, `usuario`, `modelo_nome`, `modelo_tipo`, `modelo_assunto`, `modelo_conteudo`, `modelo_data_inicio`, `modelo_data_fim`)
VALUES (8, '1', 'Sms de recorrencia maxipago', 'SMS', 'Financeiro {{ies.iesSigla}}', 'Olá! Nós da {{ies.iesSigla}} te enviamos um e-mail importante sobre seus dados financeiros!', '2018-01-01 00:00:00', '2019-01-01 00:00:00');

ALTER TABLE `sis__fila_maxipago`
CHANGE COLUMN `fila_situacao` `fila_situacao` ENUM('Pendente', 'Enviado', 'Processando', 'Erro', 'Falha no Pagamento') NULL DEFAULT 'Pendente' ;

ALTER TABLE `sis__fila_maxipago` ADD COLUMN `fila_agendamento` DATETIME NOT NULL;

ALTER TABLE `sis__fila_maxipago` ADD COLUMN `cartao_id` INT(11) ZEROFILL NULL DEFAULT NULL;

ALTER TABLE `sis__fila_maxipago` ADD INDEX `fk_sis__fila_maxipago_cartao1_idx` (`cartao_id` ASC);
ALTER TABLE `sis__fila_maxipago`
ADD CONSTRAINT `fk_sis__fila_maxipago_cartao`
FOREIGN KEY (`cartao_id`)
REFERENCES `financeiro__cartao` (`cartao_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE .`sis__fila_maxipago`
CHANGE COLUMN `fila_situacao` `fila_situacao` ENUM('Pendente', 'Enviado', 'Processando', 'Erro', 'Falha no Pagamento', 'Cancelado') NULL DEFAULT 'Pendente' ;

ALTER TABLE `sis__fila_maxipago` ADD COLUMN `fila_recorrencia_id` INT(11) ZEROFILL NULL DEFAULT NULL ;

ALTER TABLE `sis__fila_maxipago` ADD INDEX `fk_sis__fila_maxipago_recorrencia_idx` (`fila_recorrencia_id` ASC);
ALTER TABLE `sis__fila_maxipago`
ADD CONSTRAINT `fk_sis__fila_maxipago_recorrencia`
FOREIGN KEY (`fila_recorrencia_id`)
REFERENCES `sis__fila_maxipago` (`fila_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;