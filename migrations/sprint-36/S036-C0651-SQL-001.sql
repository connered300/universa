CREATE TABLE IF NOT EXISTS `org__unidade_agente` (
  `pes_id`     INT(11) UNSIGNED ZEROFILL NOT NULL,
  `unidade_id` INT(11) UNSIGNED ZEROFILL NOT NULL,
  PRIMARY KEY (`pes_id`, `unidade_id`),
  KEY `unidade_id` (`unidade_id`),
  CONSTRAINT `org__unidade_agente_ibfk_1` FOREIGN KEY (`pes_id`) REFERENCES `org__agente_educacional` (`pes_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `org__unidade_agente_ibfk_2` FOREIGN KEY (`unidade_id`) REFERENCES `org__unidade_estudo` (`unidade_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
  ENGINE = InnoDB;

ALTER TABLE `org__unidade_estudo` CHANGE `pes_id`  `pes_id` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL;

REPLACE INTO org__unidade_agente
SELECT pes_id,unidade_id FROM org__unidade_estudo
INNER JOIN org__agente_educacional USING(pes_id);