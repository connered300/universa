set @module    = (SELECT MAX(id) FROM acesso_modulos WHERE mod_nome = 'Pessoa');
set @func = (SELECT (max(id))+1 FROM acesso_funcionalidades);

REPLACE INTO `acesso_funcionalidades` (`id`, `modulo`, `func_nome`, `func_controller`, `func_icon`, `func_info`, `func_status`, `func_visivel`) VALUES
  (@func, @module, 'Pessoa Física', 'Pessoa\\Controller\\PessoaFisica', '', '', 'Ativa', 'Sim');

REPLACE INTO `acesso_actions` (`id`, `funcionalidade`, `act_nome`, `act_label`, `act_icon`, `act_info`, `act_tipo`, `act_visivel_usuario_final`) VALUES
  (null, @func, 'pessoas-duplicadas', 'Pessoas Duplicadas', 'null', 'null', 'Leitura', 'N'),
  (null, @func, 'remover-duplicados', 'Script para remover todos duplicados sem interface', 'null', 'null', 'Leitura', 'N');

REPLACE INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
    ,
    acesso_grupo
  WHERE func_controller IN (
    'Pessoa\\Controller\\PessoaFisica'
  )
        AND grup_nome LIKE 'Admin';