<?php

namespace Acesso\Controller;

use Sistema\ViewHelper\SisConfig;
use VersaSpine\Controller\AbstractCoreController;
use Zend\View\Model\JsonModel;

class AcessoPessoasController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request              = $this->getRequest();
        $paramsGet            = $request->getQuery()->toArray();
        $paramsPost           = $request->getPost()->toArray();
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEntityManager());

        $result = $serviceAcessoPessoas->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEntityManager());
        $serviceAcessoPessoas->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceAcessoPessoas->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function addAction($id = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados             = array();
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEntityManager());

        if ($id) {
            $arrDados = $serviceAcessoPessoas->getArray($id);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceAcessoPessoas->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de pessoas salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceAcessoPessoas->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceAcessoPessoas->getLastError());
                }
            }
        }

        $serviceAcessoPessoas->formataDadosPost($arrDados);
        $serviceAcessoPessoas->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $id = $this->params()->fromRoute("id", 0);

        if (!$id) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($id);
    }

    public function ifLoginIsUsedAction()
    {
        $request              = $this->getRequest();
        $paramsGet            = $request->getQuery()->toArray();
        $paramsPost           = $request->getPost()->toArray();
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEntityManager());

        $result = $serviceAcessoPessoas->ifLoginIsUsed(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceAcessoPessoas->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceAcessoPessoas->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }

    public function autocompleteJsonAction()
    {
        $result = $this->services()->getService($this->getService())->autocomplete(
            $this->getRequest()->getPost('query')
        );
        $this->json->setVariable("array", $result);

        return $this->json;
    }

    public function pesquisaUsuarioJsonAction()
    {
        $pesquisa     = $this->getRequest()->getPost()->toArray()['query'];
        $acessoPessoa = new \Acesso\Service\AcessoPessoas($this->services()->getEm());

        $result = $acessoPessoa->pesquisaUsuarioJson($pesquisa);

        $this->json->setVariable("array", $result);

        return $this->json;
    }

    public function paginationAction()
    {
        $service = $this->services()->getService($this->getService());
        $post    = $this->getRequest()->getPost()->toArray();
        if (count($post) > 0) {
            $dados = $service->pagination($post['page'], true);
        } else {
            $dados = $service->pagination(array(), true);
        }

        return $this->json->setVariable("aData", $dados['dados']);
    }

    public function checkLoginAction()
    {
        $login  = $this->getRequest()->getPost("loginUser");
        $acesso = new \Acesso\Service\AcessoPessoas($this->services()->getEm());
        $result = $acesso->checkLogin($login);

        return $this->json->setVariable("aData", $result);
    }

    public function buscaEmailAction()
    {
        $pesId  = $this->getRequest()->getPost("pesId");
        $acesso = new \Acesso\Service\AcessoPessoas($this->services()->getEm());
        $restul = $acesso->buscaEmail($pesId);

        return $this->json->setVariable("email", $restul);
    }

    public function buscaUsuarioAction()
    {
        $pesId  = $this->getRequest()->getPost("pesId");
        $acesso = new \Acesso\Service\AcessoPessoas($this->services()->getEm());
        $restul = $acesso->buscaUsuario($pesId);

        return $this->json->setVariable("dados", $restul);
    }

    /**
     * Função para recuperação de senha, envia senha para o e-mail cadastrado.
     * Note que se você quiser redirecionar para uma rota especifica após a troca da senha é só criar
     * os campos que já existem no formulário RecuperarSenha chamados redirectRoute, redirectController e
     * redirectAction.
     **/
    public function recoveryPasswordAction()
    {
        $service = new \Acesso\Service\AcessoPessoas($this->getEntityManager());
        $request = $this->getRequest();

        if ($request->isPost()) {
            $dados = $this->getRequest()->getPost()->toArray();

            try {
                $config           = $this->getServiceManager()->get('Config');
                $serviceSisConfig = new SisConfig($this->getEntityManager(), $config);

                $configEmail = $serviceSisConfig->localizarChave('emailRecovery');

                if (is_string($configEmail)) {
                    $configEmail = json_decode($configEmail, true);
                }

                $configEmail['projectName']  = $config['projectName'];
                $configEmail['linkRedirect'] = $this
                    ->getServiceManager()
                    ->get('ViewHelperManager')->get('ServerUrl')
                    ->__invoke();

                if ($dados['redirectRoute'] == 'vestibular/default') {
                    $configEmail['linkRedirect'] .= $this->url()->fromRoute('vestibular-login');
                } else {
                    $configEmail['linkRedirect'] .= $this->url()->fromRoute('acesso');
                }

                if ($dados['novaSenha']) {
                    $retorno = $service->trocaSenha($dados, $configEmail);
                } else {
                    $retorno = $service->recuperarSenha($dados, $configEmail);
                }

                if ($dados['redirectRoute']) {
                    $func = "add" . ucfirst($retorno['type']) . "Message";

                    $this->flashMessenger()->$func($retorno['message']);

                    return $this->redirect()->toRoute(
                        $dados['redirectRoute'],
                        ['controller' => $dados['redirectController'], 'action' => $dados['redirectAction']]
                    );
                } else {
                    return $this->getJson()->setVariable('response', $retorno);
                }
            } catch (\Exception $ex) {
                $message = 'Houve um erro ao redefinir sua senha! Tente novamente, caso persista contate o suporte.';

                if ($dados['redirectRoute']) {
                    $this->flashMessenger()->addErrorMessage($message);

                    return $this->redirect()->toRoute(
                        $dados['redirectRoute'],
                        ['controller' => $dados['redirectController'], 'action' => $dados['redirectAction']]
                    );
                } else {
                    $message = array('type' => 'error', 'message' => $message);

                    return $this->getJson()->setVariable('response', $message);
                }
            }
        } else {
            return $this->redirect()->toRoute(
                'acesso/default',
                ['controller' => 'autenticacao', 'action' => 'login']
            );
        }
    }

    public function alteraSenhaAction()
    {
        $request    = $this->getRequest();
        $paramsGet  = $request->getQuery()->toArray();
        $paramsPost = $request->getPost()->toArray();
        $arrDados   = array_merge($paramsGet, $paramsPost);

        if ($arrDados) {
            $service = New \Acesso\Service\AcessoPessoas($this->getEntityManager());

            if ($arrDados['novaSenha'] !== $arrDados['confirmNovaSenha']) {
                $this->getJson()->setVariable('type', 'error');
                $this->getJson()->setVariable('message', 'As senhas informadas não conferem!');

                return $this->getJson();
            }

            /** @var \Acesso\Entity\AcessoPessoas $objAcessoPessoas */
            $objAcessoPessoas = $service->retornaUsuarioLogado();

            if (!$objAcessoPessoas) {
                $this->getJson()->setVariable('type', 'error');
                $this->getJson()->setVariable('message', 'Não existe usuário logado!');

                return $this->getJson();
            }

            $senhaCriptografada = sha1($arrDados['senha']);

            if ($objAcessoPessoas->getSenha() != $senhaCriptografada) {
                $this->getJson()->setVariable('type', 'error');
                $this->getJson()->setVariable('message', 'A senha atual digitada não confere!');

                return $this->getJson();
            }

            try {
                $objAcessoPessoas->setSenha(sha1($arrDados['novaSenha']));
                $this->getEntityManager()->persist($objAcessoPessoas);
                $this->getEntityManager()->flush($objAcessoPessoas);
                $this->getJson()->setVariable('type', 'success');
                $this->getJson()->setVariable('message', 'Senha alterada com sucesso!');
            } catch (\Exception $e) {
                $this->getJson()->setVariable('type', 'error');
                $this->getJson()->setVariable('message', 'Falha ao alterar senha!');
            }
        }

        return $this->getJson();
    }

    public function trocaSenhaJsonAction()
    {
        $request = $this->getRequest();
        $service = New \Acesso\Service\AcessoPessoas($this->getEntityManager());

        if ($request->isPost()) {
            $usuId = $_SESSION['Zend_Auth']['storage']['usuario'];
            $login = $_SESSION['Zend_Auth']['storage']['login'];

            $dados = $request->getPost()->toArray();

            $repository = $service->getEm()->getRepository('Acesso\Entity\AcessoPessoas');
            $user       = $repository->validaCredenciais($login, $dados['senhaAtual']);

            if ($user) {
                $reference = $user->toArray();

                if ($dados['novaSenha'] === $dados['senhaConf']) {
                    $reference['senha'] = sha1($dados['novaSenha']);

                    $edita = $service->editaSenha($reference);

                    $this->json->setVariable('success', true);
                } else {
                    $this->json->setVariables(['erro' => "As senhas informadas não conferem."]);
                }
            } else {
                $this->json->setVariables(['erro' => "A senha atual está incorreta."]);
            }
        }

        return $this->json;
    }
}
