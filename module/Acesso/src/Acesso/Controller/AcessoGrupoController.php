<?php

namespace Acesso\Controller;

use VersaSpine\Controller\AbstractCoreController;

/**
 * Description of AcessoModulos
 *
 * @author joao
 */
class AcessoGrupoController extends AbstractCoreController
{
    public function searchForJsonAction()
    {
        $request              = $this->getRequest();
        $paramsGet            = $request->getQuery()->toArray();
        $paramsPost           = $request->getPost()->toArray();
        $serviceAcessoGrupo = new \Acesso\Service\AcessoGrupo($this->getEntityManager());

        $result = $serviceAcessoGrupo->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function buscaActionsAction()
    {
        $this->json->setVariable(
            "actions",
            $this->services()->getService($this->getService())->buscaActions($this->getRequest()->getPost('grupo'))
        );

        return $this->json;
    }

}
