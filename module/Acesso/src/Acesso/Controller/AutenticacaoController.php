<?php

namespace Acesso\Controller;

use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session as SessionStorage;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class AutenticacaoController extends AbstractActionController
{
    public function verifyCredencialsAction()
    {
        $em   = $this->getServiceLocator()->get("Doctrine\\ORM\\EntityManager");
        $view = new JsonModel();
        /** @var \Zend\Http\Request $request */
        $request = $this->getRequest();
        $config  = $this->getServiceLocator()->get('Config');
        $arrData = $request->getQuery();

        $message   = '';
        $success   = false;
        $url       = '';
        $arrGroups = array();

        if (!$config['secretTokenAuth']) {
            $message = 'Secret token not defined!';
        } elseif (!$arrData['secretToken'] || $config['secretTokenAuth'] != $arrData['secretToken']) {
            $message = 'Invalid secret token!';
        } elseif (!$arrData["password"] || !$arrData["username"]) {
            $message = 'Invalid credentials!';
        } else {
            $authService         = new AuthenticationService();
            $adapter             = new \Acesso\Adapter\Adapter($em);
            $serviceAcessoPessoa = new \Acesso\Service\AcessoPessoas($em);

            $adapter->setSenha($arrData["password"]);
            $adapter->setLogin($arrData["username"]);

            $result = $authService->authenticate($adapter);
            $ret    = $result->isValid();

            if ($ret) {
                $url = $this
                    ->getServiceLocator()
                    ->get('ViewHelperManager')->get('ServerUrl')
                    ->__invoke();

                $objEvent    = $this->getEvent();
                $objBasePath = $this->basePath();
                $objBasePath->setEvent($objEvent);
                $url .= $objBasePath->basePath('/');

                $arrUsuario = $authService->getIdentity()['user'];

                /** @var \Acesso\Entity\AcessoPessoas $objUsuario */
                $objUsuario = $serviceAcessoPessoa->retornaUsuarioLogado($arrUsuario['id']);

                /** @var \Acesso\Entity\AcessoGrupo $objGrupo */
                foreach ($objUsuario->getUsuario()->getGrupo() as $objGrupo) {
                    $arrGroups[] = $objGrupo->getGrupNome();
                }

                $message = 'Welcome ' . $arrData['username'] . '!';

                $success = true;
            } else {
                $message = 'Invalid credentials!';
            }
        }

        $view->setVariable('message', $message);
        $view->setVariable('groups', $arrGroups);
        $view->setVariable('success', $success);
        $view->setVariable('redirect', $url);

        return $view;
    }

    public function loginAction()
    {
        $em   = $this->getServiceLocator()->get("Doctrine\\ORM\\EntityManager");
        $view = new ViewModel();
        /** @var \Zend\Http\Request $request */
        $request = $this->getRequest();

        if ($this->flashMessenger()->hasErrorMessages()) {
            $view->setVariable("erro", $this->flashMessenger()->getErrorMessages());
        } else {
            if ($this->flashMessenger()->hasInfoMessages()) {
                $view->setVariable("info", $this->flashMessenger()->getInfoMessages());
            }
        }

        if (isset($_SESSION['errorLogin']) && $_SESSION['errorLogin'] !== "") {
            $this->flashMessenger()->addErrorMessage($_SESSION['errorLogin']);
            $view->setVariable('errorLogin', $_SESSION['errorLogin']);
        }

        if ($request->isPost()) {
            $session     = new SessionStorage();
            $authService = new AuthenticationService();
            $adapter     = new \Acesso\Adapter\Adapter($em);

            $adapter->setSenha($request->getPost("password"));
            $adapter->setLogin($request->getPost("username"));
            $arrDados = $request->getPost()->toArray();

            $authService->setStorage($session);
            $result = $authService->authenticate($adapter);
            $ret    = $result->isValid();

            if ($ret && !$arrDados['dados']) {
                $user = $authService->getIdentity()['user'];
                // 4 horas
                $user['cache_expire'] = 60 * 4;
                $user['datetime_login'] = new \DateTime('now');

                if (is_array($session_config_app = $this->getServiceLocator()->get("config")['session']['config'])) {
                    $user['cache_expire'] = $session_config_app['options']['cache_expire'];
                }

                $session->write($user);

                $srvModulos = new \Acesso\Service\AcessoFuncionalidades($em);
                $session    = new \Zend\Session\Container("AcessoVersaSkeleton");
                $modulos    = $srvModulos->buscaFuncionalidadesUsuario($user['usuario'], true);

                $session->sistemaSelected = $modulos['sis_id'];

                if ($modulos['mod_redireciona'] == "" || $modulos['mod_redireciona'] == null) {
                    $modulos['mod_redireciona'] = "/";
                }

                $objEvent    = $this->getEvent();
                $objBasePath = $this->basePath();
                $objBasePath->setEvent($objEvent);
                $url = $objBasePath->basePath($modulos['mod_redireciona']);

                return $this->redirect()->toUrl($url);
            } else {
                if ($arrDados['link-login-only'] || $arrDados['dados']) {
                    $view->setTemplate('/organizacao/autenticacao/login');

                    if ($ret) {
                        $arrDados = base64_decode($arrDados['dados']);
                        parse_str($arrDados, $arrDados);

                        $rotaRetorno = $arrDados['link-login-only'] . '?dst=' . $arrDados['link-orig-esc'] . '&username=T-' . $arrDados['mac-esc'];

                        if (!$arrDados['link-login-only'] || !$arrDados['link-orig-esc'] || !$arrDados['mac-esc']) {
                            $this->flashMessenger()->addErrorMessage(
                                "Erro de endereçamento, favor entrar em contato com suporte!"
                            );
                        } else {
                            return $this->redirect()->toUrl($rotaRetorno);
                        }
                    }

                    if (!$arrDados['dados']) {
                        $arrDados['dados'] = http_build_query($arrDados);
                        $arrDados['dados'] = base64_encode($arrDados['dados']);
                    }

                    if ($result->getCode() == -3 && ($arrDados['username'] && $arrDados['password'])) {
                        $view->setVariable('erro', "Credenciais inválidas!");
                    }

                    $view->setVariable('dados', $arrDados['dados']);
                    $view->setVariable('titulo', 'Digite seus dados de login para acessar o WiFi.');
                    $view->setTerminal(true);

                    return $view;
                } elseif ($result->getCode() == -3) {
                    $this->flashMessenger()->addErrorMessage("Credenciais inválidas!");
                } else {
                    if ($result->getCode() == -4) {
                        $this->flashMessenger()->addInfoMessage($result->getMessages()[0]);
                    }
                }
            }

            return $this->redirect()->toRoute('acesso-autenticacao-login');
        }

        $config = $this->getServiceLocator()->get('Config');

        if (isset($config['viewLogin'])) {
            $viewLogin = $config['viewLogin'];
            $view->setTemplate($viewLogin);
        }

        $view->setTerminal(true);

        return $view;
    }

    public function logoutAction()
    {
        $session = $_SESSION['Zend_Auth']['storage'];

        if (isset($session['erro']) && $session['erro'] !== "") {
            $error = $session['erro'];
            session_destroy();
            session_start();
            $_SESSION['errorLogin'] = $error;
        } else {
            session_destroy();
        }

        return $this->redirect()->toRoute("acesso-autenticacao-login");
    }
}
