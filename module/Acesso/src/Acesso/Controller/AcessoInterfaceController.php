<?php

namespace Acesso\Controller;

use VersaSpine\Controller\AbstractCoreController;


class AcessoInterfaceController extends AbstractCoreController
{

    /**
     * AcessoInterfaceController constructor.
     */
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAcadCurso = new \Matricula\Service\AcadCurso($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceAcadCurso->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function indexAction()
    {
        return $this->getView();
    }

    public function alteraCredencialAction()
    {
        $request = $this->getRequest();
        $service = $this->getServiceLocator()->get('service');

        if($request->isPost())
        {
            $dados = $request->getPost()->toArray();
        }
    }
}
