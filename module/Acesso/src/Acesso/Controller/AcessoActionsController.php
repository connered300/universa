<?php

namespace Acesso\Controller;

use VersaSpine\Controller\AbstractCoreController;

/**
 * Description of AcessoActionsController
 *
 * @author joao
 */
class AcessoActionsController extends AbstractCoreController
{
    public function __construct($class = null)
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $criterios = $this->getRequest()->getPost()->toArray();
        $this->json->setVariable(
            "data",
            $this->services()->getService($this->getService())->pesquisaForJson($criterios)
        );

        return $this->json;
    }

    public function removivelAction()
    {
        $post = $this->getRequest()->getPost();

        return $this->json->setVariable("data", $this->services()->getService($this->getService())->removivel($post));
    }

    public function buscaActionsFilhasAction()
    {
        return $this->json->setVariable(
            "data",
            $this->services()->getService($this->getService())->buscaActionsFilhas(
                $this->getRequest()->getPost('action')
            )
        );
    }

}
