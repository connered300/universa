<?php

namespace Acesso\Controller;

use Zend\Mvc\Controller\AbstractActionController;

/**
 * Description of ConfigSessionController
 *
 * @author jp
 */
class ConfigSessionController extends AbstractActionController
{
    public function selectSistemAction()
    {
        $id = $this->params()->fromRoute("sistema");
        if (!$id) {
            throw new \Exception("Para mudar de sistema selecione um sistema");
        }
        $session                  = new \Zend\Session\Container("AcessoVersaSkeleton");
        $session->sistemaSelected = $id;

        return $this->redirect()->toRoute("versa-skleton-home");
    }

}
