<?php

namespace Acesso\Controller;

use VersaSpine\Controller\AbstractCoreController;

/**
 * Description of AcessoSistemasController
 *
 * @author joao
 */
class AcessoSistemasController extends AbstractCoreController
{
    public function searchForJsonAction()
    {
    }

    public function __construct($class = null)
    {
        parent::__construct(__CLASS__);
    }

}
