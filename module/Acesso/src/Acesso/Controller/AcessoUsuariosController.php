<?php

namespace Acesso\Controller;

use VersaSpine\Controller\AbstractCoreController;

/**
 * Description of AcessoUsuariosController
 *
 * @author jp
 */
class AcessoUsuariosController extends AbstractCoreController
{
    public function __construct($class = null)
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
    }

    public function buscaGruposUsrAction()
    {
        return $this->json->setVariable(
            "grupos",
            $this->services()->getService($this->getService())->buscaGruposUsr($this->getRequest()->getPost("usuario"))
        );
    }

    public function buscaPrivilegiosUsrAction()
    {
        return $this->json->setVariable(
            "actions",
            $this->services()->getService($this->getService())->buscaPrivilegiosUsr(
                $this->getRequest()->getPost("usuario")
            )
        );
    }

    public function criaUsuariosPorTipoAutomaticamenteAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $service = $this->services()->getService($this->getService());
            $data    = $request->getPost()->toArray();
            foreach ($data['tipoUsuario'] as $tipoUsuario) {
                $pessoas = $service->buscaPessoaSemUsuarioPorTipo($tipoUsuario);
                if(! empty($pessoas)){
                    $service->addUsuarios($pessoas, $tipoUsuario);
                }
            }

            return $this->redirect()->toRoute('acesso/default', array('controller' => $data['controller']));
        }
    }

}
