<?php

namespace Acesso\Controller;

use VersaSpine\Controller\AbstractCoreController;

/**
 * Description of AcessoModulos
 *
 * @author joao
 */
class AcessoFuncionalidadesController extends AbstractCoreController
{
    public function __construct($class = null)
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
    }

    public function pesquisaFuncionalidadesAction()
    {
        $layout  = true;
        $post    = $this->getRequest()->getPost()->toArray();
        $get     = $this->getRequest()->getQuery()->toArray();
        $request = array_merge($get, $post);
        $grupo   = isset($request['grupo']) ? true : false;
        $dados   = $this->services()->getService($this->getService())->listaFuncionalidades(null, null, false, $grupo);

        if ($request['layout'] == "false") {
            $layout = false;
            $this->view->setTerminal(true);
        }

        $this->view->setVariable("layout", $layout);
        $this->view->setVariable("dados", $dados);

        return $this->view;
    }

    public function editAction()
    {
        $view = parent::editAction();

        if ($view == $this->getView()) {
            $view->setTemplate($this->getTemplateToRoute('add'));
        }

        return $view;
    }
}
