<?php

namespace Acesso\Controller;

use Zend\Mvc\Controller\AbstractActionController,
    Zend\View\Model\ViewModel;

/**
 * Description of AcessoAutorizacaoController
 *
 * @author jp
 */
class AutorizacaoController extends AbstractActionController
{
    public function acessoNegadoAction()
    {
        $view = new ViewModel();

        return $view;
    }

}
