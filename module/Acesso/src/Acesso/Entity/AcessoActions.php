<?php

namespace Acesso\Entity;

use Doctrine\ORM\Mapping as ORM;

use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AcessoActions
 *
 * @ORM\Table(name="acesso_actions", indexes={@ORM\Index(name="fk_acesso_actions_acesso_funcionalidades1_idx", columns={"funcionalidade"})})
 * @ORM\Entity
 * @LG\LG(id="id",label="funcNome")
 * @Jarvis\Jarvis(title="Lista de ações",icon="fa fa-table")
 */
class AcessoActions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="id")
     * @LG\Labels\Attributes(text="Índice",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="act_nome", type="string", length=45, nullable=false)
     * @LG\Labels\Property(name="act_nome")
     * @LG\Labels\Attributes(text="Nome",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $actNome;

    /**
     * @var string
     *
     * @ORM\Column(name="act_label", type="string", length=45, nullable=false)
     * @LG\Labels\Property(name="act_label")
     * @LG\Labels\Attributes(text="Label da action",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $actLabel;

    /**
     * @var string
     *
     * @ORM\Column(name="act_icon", type="string", length=45, nullable=true)
     * @LG\Labels\Property(name="act_icon")
     * @LG\Labels\Attributes(text="Ícone",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $actIcon;

    /**
     * @var string
     *
     * @ORM\Column(name="act_info", type="string", length=45, nullable=true)
     * @LG\Labels\Property(name="act_info")
     * @LG\Labels\Attributes(text="Informações",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $actInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="act_tipo", type="string", length=7, nullable=false)
     * @LG\Labels\Property(name="act_tipo")
     * @LG\Labels\Attributes(text="Tipo de ação",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $actTipo;

    /**
     * @var string
     *
     * @ORM\Column(name="act_visivel_usuario_final", type="string", length=1, nullable=false)
     * @LG\Labels\Property(name="act_visivel_usuario_final")
     * @LG\Labels\Attributes(text="ação visível à usuários finais",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $actVisivelUsuarioFinal;

    /**
     * @var AcessoFuncionalidades
     *
     * @ORM\ManyToOne(targetEntity="AcessoFuncionalidades")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="funcionalidade", referencedColumnName="id")
     * })
     * @LG\Labels\Property(name="funcionalidade")
     * @LG\Labels\Attributes(text="Funcionalidade Pai",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE")
     *
     */
    private $funcionalidade;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AcessoActions", inversedBy="pai")
     * @ORM\JoinTable(name="acesso_actions_filhas",
     *   joinColumns={
     *     @ORM\JoinColumn(name="pai", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="filha", referencedColumnName="id")
     *   }
     * )
     */
    private $filha;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AcessoUsuarios", mappedBy="acessoActions")
     */
    private $acessoUsuarios;

    /**
     * Constructor
     */
    public function __construct($array)
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($array, $this);
        //        $this->filha       = new \Doctrine\Common\Collections\ArrayCollection();
        //        $this->acessoUsuarios = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getActNome()
    {
        return $this->actNome;
    }

    /**
     * @return string
     */
    public function getActLabel()
    {
        return $this->actLabel;
    }

    /**
     * @return string
     */
    public function getActIcon()
    {
        return $this->actIcon;
    }

    /**
     * @return string
     */
    public function getActInfo()
    {
        return $this->actInfo;
    }

    /**
     * @return AcessoFuncionalidades
     */
    public function getFuncionalidade()
    {
        return $this->funcionalidade;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFilha()
    {
        return $this->filha;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAcessoUsuarios()
    {
        return $this->acessoUsuarios;
    }

    /**
     * @param $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param $actNome
     */
    public function setActNome($actNome)
    {
        $this->actNome = $actNome;
    }

    /**
     * @param $actLabel
     */
    public function setActLabel($actLabel)
    {
        $this->actLabel = $actLabel;
    }

    /**
     * @param $actIcon
     */
    public function setActIcon($actIcon)
    {
        $this->actIcon = $actIcon;
    }

    /**
     * @param $actInfo
     */
    public function setActInfo($actInfo)
    {
        $this->actInfo = $actInfo;
    }

    /**
     * @param $funcionalidade
     */
    public function setFuncionalidade($funcionalidade)
    {
        $this->funcionalidade = $funcionalidade;
    }

    /**
     * @param $filha
     */
    public function setFilha($filha)
    {
        $this->filha = $filha;
    }

    /**
     * @param $acessoGrupo
     */
    public function setAcessoGrupo($acessoGrupo)
    {
        $this->acessoGrupo = $acessoGrupo;
    }

    /**
     * @param $acessoUsuarios
     */
    public function setAcessoUsuarios($acessoUsuarios)
    {
        $this->acessoUsuarios = $acessoUsuarios;
    }

    /**
     * @return string
     */
    public function getActTipo()
    {
        return $this->actTipo;
    }

    /**
     * @param string $actTipo
     */
    public function setActTipo($actTipo)
    {
        $this->actTipo = $actTipo;
    }

    /**
     * @return string
     */
    public function getActVisivelUsuarioFinal()
    {
        return $this->actVisivelUsuarioFinal;
    }

    /**
     * @param string $actVisivelUsuarioFinal
     */
    public function setActVisivelUsuarioFinal($actVisivelUsuarioFinal)
    {
        $this->actVisivelUsuarioFinal = $actVisivelUsuarioFinal;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \Zend\Stdlib\Hydrator\ClassMethods())->extract($this);
    }

}