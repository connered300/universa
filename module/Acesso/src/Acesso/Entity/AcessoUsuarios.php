<?php

namespace Acesso\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AcessoUsuarios
 *
 * @ORM\Table(name="acesso_usuarios")
 * @ORM\Entity
 * @LG\LG(id="id",label="usr_status")
 */
class AcessoUsuarios
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="acesso_data_registro", type="datetime", nullable=false)
     */
    private $acessoDataRegistro;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="acesso_data_senha_salva", type="datetime", nullable=false)
     */
    private $acessoDataSenhaSalva;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_status", type="string", nullable=false)
     */
    private $usrStatus;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AcessoGrupo", inversedBy="usuario")
     * @ORM\JoinTable(name="acesso_usuarios_grupos",
     *   joinColumns={
     *     @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="grupo_id", referencedColumnName="id")
     *   }
     * )
     */
    private $grupo;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AcessoActions", inversedBy="acessoUsuarios")
     * @ORM\JoinTable(name="acesso_usuarios_privilegios",
     *   joinColumns={
     *     @ORM\JoinColumn(name="acesso_usuarios_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="acesso_actions_id", referencedColumnName="id")
     *   }
     * )
     */
    private $acessoActions;

    /**
     * Constructor
     */
    public function __construct(array $data)
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
        $this->setAcessoDataRegistro($data['acessoDataRegistro']);
        $this->setAcessoDataSenhaSalva($data['acessoDataSenhaSalva']);
        $this->setUsrStatus($data['usrStatus']);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAcessoDataRegistro()
    {
        return $this->acessoDataRegistro;
    }

    public function getAcessoDataSenhaSalva()
    {
        return $this->acessoDataSenhaSalva;
    }

    public function getUsrStatus()
    {
        return $this->usrStatus;
    }

    public function getGrupo()
    {
        return $this->grupo;
    }

    public function getAcessoActions()
    {
        return $this->acessoActions;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setAcessoDataRegistro($acessoDataRegistro)
    {
        if (empty($acessoDataRegistro)) {
            $acessoDataRegistro = new \DateTime("now");
        }

        $this->acessoDataRegistro = $acessoDataRegistro;
    }

    public function setAcessoDataSenhaSalva($acessoDataSenhaSalva)
    {
        if (empty($acessoDataSenhaSalva)) {
            $acessoDataSenhaSalva = new \DateTime("now");
        }

        $this->acessoDataSenhaSalva = $acessoDataSenhaSalva;
    }

    public function setUsrStatus($usrStatus)
    {
        if (!$usrStatus) {
            $usrStatus = "Ativo";
        }

        $this->usrStatus = $usrStatus;
    }

    public function setGrupo(\Doctrine\Common\Collections\Collection $grupo)
    {
        $this->grupo = $grupo;
    }

    public function setAcessoActions(\Doctrine\Common\Collections\Collection $acessoActions)
    {
        $this->acessoActions = $acessoActions;
    }
}
