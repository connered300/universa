<?php
namespace Acesso\Entity;

use Doctrine\ORM\Mapping as ORM;

use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AcessoSistemas
 *
 * @ORM\Table(name="acesso_sistemas", indexes={@ORM\Index(name="fk_acesso_sistemas_acesso_usuarios1_idx", columns={"usuario"}), @ORM\Index(name="fk_acesso_sistemas_pessoa_juridica1_idx", columns={"pessoa_juridica_id"})})
 * @ORM\Entity
 * @LG\LG(id="id",label="sis_nome",tableId="sistemas")
 * @Jarvis\Jarvis(title="Lista de sistemas",icon="fa fa-table",id="listaDeSistemas")
 */
class AcessoSistemas
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="id")
     * @LG\Labels\Attributes(text="Índice",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="sis_nome", type="string", length=45, nullable=false)
     * @LG\Labels\Property(name="sis_nome")
     * @LG\Labels\Attributes(text="Nome",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE%")
     */
    private $sisNome;

    /**
     * @var string
     *
     * @ORM\Column(name="sis_status", type="string", nullable=false)
     * @LG\Labels\Property(name="sis_status")
     * @LG\Labels\Attributes(text="Status",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $sisStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="sis_icon", type="string", length=45, nullable=true)
     * @LG\Labels\Property(name="sis_icon")
     * @LG\Labels\Attributes(text="Ícone",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $sisIcon;

    /**
     * @var string
     *
     * @ORM\Column(name="sis_info", type="string", length=255, nullable=true)
     * @LG\Labels\Property(name="sis_info")
     * @LG\Labels\Attributes(text="sisInfo",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE%")
     */
    private $sisInfo;

    /**
     * @Labels\Methods(labelMethod="Construtor da classe")
     */
    public function __construct(array $data)
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getSisNome()
    {
        return $this->sisNome;
    }

    public function getSisStatus()
    {
        return $this->sisStatus;
    }

    public function getSisIcon()
    {
        return $this->sisIcon;
    }

    public function getSisInfo()
    {
        return $this->sisInfo;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setSisNome($sisNome)
    {
        $this->sisNome = $sisNome;
    }

    public function setSisStatus($sisStatus)
    {
        $this->sisStatus = $sisStatus;
    }

    public function setSisIcon($sisIcon)
    {
        $this->sisIcon = $sisIcon;
    }

    public function setSisInfo($sisInfo)
    {
        $this->sisInfo = $sisInfo;
    }

    public function toArray()
    {
        return array(
            'id'        => $this->id,
            'sisNome'   => $this->sisNome,
            'sisIcon'   => $this->sisIcon,
            'sisInfo'   => $this->sisInfo,
            'sisStatus' => $this->sisStatus
        );
    }

}
