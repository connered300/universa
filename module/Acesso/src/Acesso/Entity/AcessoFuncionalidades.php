<?php

namespace Acesso\Entity;

use Doctrine\ORM\Mapping as ORM;

use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AcessoFuncionalidades
 *
 * @ORM\Table(name="acesso_funcionalidades", indexes={@ORM\Index(name="fk_acesso_funcionalidades_acesso_modulos1_idx", columns={"modulo"})})
 * @ORM\Entity
 * @LG\LG(id="_listaFunc",label="func_nome",limit="100")
 * @Jarvis\Jarvis(title="Lista de funcionalidades",icon="fa fa-table")
 * @LG\Querys\OrderBy(params={"func_nome"="ASC","func_controller"="ASC"})
 * @LG\Querys\GroupBy()
 */
class AcessoFuncionalidades
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="id")
     * @LG\Labels\Attributes(text="Índice",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="func_nome", type="string", length=45, nullable=false)
     * @LG\Labels\Property(name="func_nome")
     * @LG\Labels\Attributes(text="Nome da funcionalidade",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE%")
     */
    private $funcNome;

    /**
     * @var string
     *
     * @ORM\Column(name="func_controller", type="string", length=45, nullable=false)
     * @LG\Labels\Property(name="func_controller")
     * @LG\Labels\Attributes(text="Controlador",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE%")
     */
    private $funcController;

    /**
     * @var string
     *
     * @ORM\Column(name="func_icon", type="string", length=45, nullable=true)
     * @LG\Labels\Property(name="func_icon")
     * @LG\Labels\Attributes(text="Ícone",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE%")
     */
    private $funcIcon;

    /**
     * @var string
     *
     * @ORM\Column(name="func_info", type="string", length=45, nullable=true)
     * @LG\Labels\Property(name="func_info")
     * @LG\Labels\Attributes(text="Informação",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE%")
     */
    private $funcInfo;

    /**
     * @var AcessoModulos
     *
     * @ORM\ManyToOne(targetEntity="AcessoModulos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="modulo", referencedColumnName="id")
     * })
     * @LG\Labels\Property(name="modulo")
     * @LG\Labels\Attributes(text="Módulo",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE")
     * @LG\Querys\Joins(table="acesso_modulos",joinType="inner",joinCampDest="id",joinCampDestLabel="mod_nome")
     */
    private $modulo;

    /**
     * @var string
     *
     * @ORM\Column(name="func_status", type="string", nullable=false)
     * @LG\Labels\Property(name="func_status")
     * @LG\Labels\Attributes(text="Status",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $funcStatus;

    /**
     * @var boolean
     * @LG\Labels\Property(name="func_visivel")
     * @LG\Labels\Attributes(text="Visível",icon="fa fa-eye")
     * @LG\Querys\Conditions(type="=")
     * @ORM\Column(name="func_visivel", type="string", nullable=false)
     */
    private $funcVisivel;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFuncNome()
    {
        return $this->funcNome;
    }

    /**
     * @return string
     */
    public function getFuncController()
    {
        return $this->funcController;
    }

    /**
     * @return string
     */
    public function getFuncIcon()
    {
        return $this->funcIcon;
    }

    /**
     * @return string
     */
    public function getFuncInfo()
    {
        return $this->funcInfo;
    }

    /**
     * @return AcessoModulos
     */
    public function getModulo()
    {
        return $this->modulo;
    }

    /**
     * @param $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param $funcNome
     */
    public function setFuncNome($funcNome)
    {
        $this->funcNome = $funcNome;
    }

    /**
     * @param $funcController
     */
    public function setFuncController($funcController)
    {
        $this->funcController = $funcController;
    }

    /**
     * @param $funcIcon
     */
    public function setFuncIcon($funcIcon)
    {
        $this->funcIcon = $funcIcon;
    }

    /**
     * @param $funcInfo
     */
    public function setFuncInfo($funcInfo)
    {
        $this->funcInfo = $funcInfo;
    }

    /**
     * @param $modulo
     */
    public function setModulo($modulo)
    {
        $this->modulo = $modulo;
    }

    /**
     * @return string
     */
    public function getFuncStatus()
    {
        return $this->funcStatus;
    }

    /**
     * @param $funcStatus
     */
    public function setFuncStatus($funcStatus)
    {
        $this->funcStatus = $funcStatus;
    }

    /**
     * @return bool
     */
    public function getFuncVisivel()
    {
        return $this->funcVisivel;
    }

    /**
     * @param $funcVisivel
     */
    public function setFuncVisivel($funcVisivel)
    {
        $this->funcVisivel = $funcVisivel;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array(
            'id'             => $this->id,
            'funcNome'       => $this->funcNome,
            'funcController' => $this->funcController,
            'funcInfo'       => $this->funcInfo,
            'funcIcon'       => $this->funcIcon,
            'funcStatus'     => $this->funcStatus,
            'funcVisivel'    => $this->funcVisivel,
            'modulo'         => $this->modulo->getId()
        );
    }

}
