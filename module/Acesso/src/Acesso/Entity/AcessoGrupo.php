<?php

namespace Acesso\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AcessoGrupo
 *
 * @ORM\Table(name="acesso_grupo")
 * @ORM\Entity
 * @LG\LG(id="id",label="grup_nome")
 * @Jarvis\Jarvis(title="Lista de grupos",icon="fa fa-table")
 */
class AcessoGrupo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false, length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="id")
     * @LG\Labels\Attributes(text="código")
     * @LG\Querys\Conditions(type="=")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="grup_nome", type="string", nullable=false, length=45)
     * @LG\Labels\Property(name="grup_nome")
     * @LG\Labels\Attributes(text="nome")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $grupNome;

    /**
     * @var string
     *
     * @ORM\Column(name="grup_info", type="string", nullable=true, length=255)
     * @LG\Labels\Property(name="grup_info")
     * @LG\Labels\Attributes(text="informação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $grupInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="grup_icon", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="grup_icon")
     * @LG\Labels\Attributes(text="ícone")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $grupIcon;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AcessoActions", inversedBy="acessoActions")
     * @ORM\JoinTable(name="acesso_privilegios_grupo",
     *   joinColumns={
     *     @ORM\JoinColumn(name="acesso_grupo_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="acesso_actions_id", referencedColumnName="id")
     *   }
     * )
     */
    private $acessoActions;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AcessoUsuarios", mappedBy="grupo")
     */
    private $usuario;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $id
     * @return AcessoGrupo
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getGrupNome()
    {
        return $this->grupNome;
    }

    /**
     * @param string $grupNome
     * @return AcessoGrupo
     */
    public function setGrupNome($grupNome)
    {
        $this->grupNome = $grupNome;

        return $this;
    }

    /**
     * @return string
     */
    public function getGrupInfo()
    {
        return $this->grupInfo;
    }

    /**
     * @param string $grupInfo
     * @return AcessoGrupo
     */
    public function setGrupInfo($grupInfo)
    {
        $this->grupInfo = $grupInfo;

        return $this;
    }

    /**
     * @return string
     */
    public function getGrupIcon()
    {
        return $this->grupIcon;
    }

    /**
     * @param string $grupIcon
     * @return AcessoGrupo
     */
    public function setGrupIcon($grupIcon)
    {
        $this->grupIcon = $grupIcon;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'id'       => $this->getId(),
            'grupNome' => $this->getGrupNome(),
            'grupInfo' => $this->getGrupInfo(),
            'grupIcon' => $this->getGrupIcon(),
        );

        return $array;
    }

    public function setAcessoActions(\Doctrine\Common\Collections\Collection $acessoActions)
    {
        $this->acessoActions = $acessoActions;
    }

    public function setUsuario(\Doctrine\Common\Collections\Collection $usuario)
    {
        $this->usuario = $usuario;
    }
}
