<?php

namespace Acesso\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AcessoLog
 *
 * @ORM\Table(name="acesso_log", indexes={@ORM\Index(name="fk_acesso_log_acesso_usuarios1_idx", columns={"usuario"})})
 * @ORM\Entity
 * @LG\LG(id="id",label="log_msg")
 */
class AcessoLog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="log_controller", type="string", length=45, nullable=false)
     */
    private $logController;

    /**
     * @var string
     *
     * @ORM\Column(name="log_action", type="string", length=45, nullable=false)
     */
    private $logAction;

    /**
     * @var integer
     *
     * @ORM\Column(name="log_priority", type="integer", nullable=false)
     */
    private $logPriority;

    /**
     * @var string
     *
     * @ORM\Column(name="log_msg", type="string", length=45, nullable=true)
     */
    private $logMsg;

    /**
     * @var \AcessoUsuarios
     *
     * @ORM\ManyToOne(targetEntity="AcessoUsuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario", referencedColumnName="id")
     * })
     */
    private $usuario;

    public function __construct($data)
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getLogController()
    {
        return $this->logController;
    }

    public function getLogAction()
    {
        return $this->logAction;
    }

    public function getLogPriority()
    {
        return $this->logPriority;
    }

    public function getLogMsg()
    {
        return $this->logMsg;
    }

    public function getUsuario()
    {
        return $this->usuario;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setLogController($logController)
    {
        $this->logController = $logController;
    }

    public function setLogAction($logAction)
    {
        $this->logAction = $logAction;
    }

    public function setLogPriority($logPriority)
    {
        $this->logPriority = $logPriority;
    }

    public function setLogMsg($logMsg)
    {
        $this->logMsg = $logMsg;
    }

    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }

}
