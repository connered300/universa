<?php

namespace Acesso\Entity;

use Doctrine\ORM\Mapping as ORM;

use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AcessoModulos
 *
 * @ORM\Table(name="acesso_modulos", indexes={@ORM\Index(name="fk_acesso_modulos_acesso_sistemas_idx", columns={"sistema"})})
 * @ORM\Entity
 * @LG\LG(id="id",label="mod_nome")
 * @Jarvis\Jarvis(title="Lista de módulos",icon="fa fa-table")
 */
class AcessoModulos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="id")
     * @LG\Labels\Attributes(text="Índice",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="mod_nome", type="string", length=45, nullable=false)
     * @LG\Labels\Property(name="mod_nome")
     * @LG\Labels\Attributes(text="Nome do módulo",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE%")
     */
    private $modNome;

    /**
     * @var string
     *
     * @ORM\Column(name="mod_route", type="string", length=45, nullable=false)
     * @LG\Labels\Property(name="mod_route")
     * @LG\Labels\Attributes(text="Nome da rota",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE%")
     */
    private $modRoute;

    /**
     * @var string
     *
     * @ORM\Column(name="mod_status", type="string", nullable=false)
     * @LG\Labels\Property(name="mod_status")
     * @LG\Labels\Attributes(text="Status",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $modStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="mod_icon", type="string", length=45, nullable=true)
     * @LG\Labels\Property(name="mod_icon")
     * @LG\Labels\Attributes(text="Ícone",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $modIcon;

    /**
     * @var string
     *
     * @ORM\Column(name="mod_info", type="string", length=45, nullable=true)
     * @LG\Labels\Property(name="mod_info")
     * @LG\Labels\Attributes(text="Informação",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE%")
     */
    private $modInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="mod_prioridade", type="integer", length=45, nullable=true)
     * @LG\Labels\Property(name="mod_prioridade")
     * @LG\Labels\Attributes(text="Prioridade do módulo",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $modPrioridade;

    /**
     * @var string
     *
     * @ORM\Column(name="mod_redireciona", type="string", length=45, nullable=true)
     * @LG\Labels\Property(name="mod_redireciona")
     * @LG\Labels\Attributes(text="Redirecionar",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $modRedireciona;

    /**
     * @var \AcessoSistemas
     *
     * @ORM\ManyToOne(targetEntity="AcessoSistemas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sistema", referencedColumnName="id")
     * })
     * @LG\Labels\Property(name="sistema")
     * @LG\Labels\Attributes(text="Sistema",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE%")
     * @LG\Querys\Joins(table="acesso_sistemas",joinType="inner",joinCampDest="id",joinCampDestLabel="sis_nome")
     */
    private $sistema;

    public function __construct(array $data)
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    public function getModRoute()
    {
        return $this->modRoute;
    }

    public function setModRoute($modRoute)
    {
        $this->modRoute = $modRoute;
    }

    function getModPrioridade()
    {
        return $this->modPrioridade;
    }

    function getModRedireciona()
    {
        return $this->modRedireciona;
    }

    function setModPrioridade($modPrioridade)
    {
        $this->modPrioridade = $modPrioridade;
    }

    function setModRedireciona($modRedireciona)
    {
        $this->modRedireciona = $modRedireciona;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getModNome()
    {
        return $this->modNome;
    }

    public function getModStatus()
    {
        return $this->modStatus;
    }

    public function getModIcon()
    {
        return $this->modIcon;
    }

    public function getModInfo()
    {
        return $this->modInfo;
    }

    public function getSistema()
    {
        return $this->sistema;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setModNome($modNome)
    {
        $this->modNome = $modNome;
    }

    public function setModStatus($modStatus)
    {
        $this->modStatus = $modStatus;
    }

    public function setModIcon($modIcon)
    {
        $this->modIcon = $modIcon;
    }

    public function setModInfo($modInfo)
    {
        $this->modInfo = $modInfo;
    }

    public function setSistema($sistema)
    {
        $this->sistema = $sistema;
    }

    public function toArray()
    {
        return array(
            'id'             => $this->id,
            'modNome'        => $this->modNome,
            'modInfo'        => $this->modInfo,
            'modIcon'        => $this->modIcon,
            'modStatus'      => $this->modStatus,
            'modRoute'       => $this->modRoute,
            'modRedireciona' => $this->modRedireciona,
            'modPrioridade'  => $this->modPrioridade,
            'sistema'        => $this->sistema->getId()
        );
    }

}
