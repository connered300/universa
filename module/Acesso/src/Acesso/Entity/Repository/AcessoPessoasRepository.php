<?php

namespace Acesso\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class AcessoPessoasRepository extends EntityRepository
{
    public function validaCredenciais($login, $senha)
    {
        /** @var \Acesso\Entity\AcessoPessoas $usuario */
        $usuario = $this->findOneByLogin($login);

        if ($usuario) {
            if ($usuario->getSenha() == sha1($senha)) {
                $usuario->setDataUltimoLogin(new \DateTime());
                $this->getEntityManager()->persist($usuario);
                $this->getEntityManager()->flush($usuario);

                return $usuario;
            }
        }

        return false;
    }

}
