<?php

namespace Acesso\Entity;

use Doctrine\ORM\Mapping as ORM;

use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AcessoPessoas
 *
 * @ORM\Table(name="acesso_pessoas", indexes={@ORM\Index(name="fk_table1_acesso_usuarios1_idx", columns={"usuario"}), @ORM\Index(name="fk_acesso_pessoas_pessoas_fisicas1_idx", columns={"pes_fisica"}), @ORM\Index(name="fk_acesso_pessoas_pessoa_juridica1_idx", columns={"pes_juridica"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Acesso\Entity\Repository\AcessoPessoasRepository")
 * @LG\LG(id="id",label="login")
 * @Jarvis\Jarvis(title="Lista de usuários",icon="fa fa-table")
 */
class AcessoPessoas
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false, length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="id")
     * @LG\Labels\Attributes(text="código")
     * @LG\Querys\Conditions(type="=")
     */
    private $id;

    /**
     * @var \Acesso\Entity\AcessoUsuarios
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoUsuarios", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario", referencedColumnName="id")
     * })
     */
    private $usuario;

    /**
     * @var \Pessoa\Entity\PessoaFisica
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\PessoaFisica")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_fisica", referencedColumnName="pes_id")
     * })
     */
    private $pesFisica;

    /**
     * @var \Pessoa\Entity\PessoaJuridica
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\PessoaJuridica")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_juridica", referencedColumnName="pes_id")
     * })
     */
    private $pesJuridica;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", nullable=false, length=45)
     * @LG\Labels\Property(name="login")
     * @LG\Labels\Attributes(text="login")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $login;

    /**
     * @var string
     *
     * @ORM\Column(name="senha", type="string", nullable=false, length=255)
     * @LG\Labels\Property(name="senha")
     * @LG\Labels\Attributes(text="senha")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $senha;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", nullable=true, length=255)
     * @LG\Labels\Property(name="email")
     * @LG\Labels\Attributes(text="e-mail")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_ultimo_login", type="datetime", nullable=true)
     */
    private $dataUltimoLogin;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Matricula\Entity\CampusCurso", inversedBy="acessoPessoas")
     * @ORM\JoinTable(name="acesso_pessoas_campus_curso",
     *   joinColumns={
     *     @ORM\JoinColumn(name="acesso_pessoas_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="cursocampus_id", referencedColumnName="cursocampus_id")
     *   }
     * )
     */
    private $cursoCampus;

    public function getCursoCampus()
    {
        return $this->cursoCampus;
    }

    public function setCursoCampus(\Doctrine\Common\Collections\Collection $cursoCampus)
    {
        $this->cursoCampus = $cursoCampus;

        return $this;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $id
     * @return AcessoPessoas
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoUsuarios
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param \Acesso\Entity\AcessoUsuarios $usuario
     * @return AcessoPessoas
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * @return \Pessoa\Entity\PessoaFisica
     */
    public function getPesFisica()
    {
        return $this->pesFisica;
    }

    /**
     * @return \Pessoa\Entity\PessoaFisica|\Pessoa\Entity\PessoaJuridica
     */
    public function getPes()
    {
        return $this->getPesFisica() ? $this->getPesFisica() : $this->getPesJuridica();
    }

    /**
     * @param \Pessoa\Entity\PessoaFisica $pesFisica
     * @return AcessoPessoas
     */
    public function setPesFisica($pesFisica)
    {
        $this->pesFisica = $pesFisica;

        return $this;
    }

    /**
     * @return \Pessoa\Entity\PessoaJuridica
     */
    public function getPesJuridica()
    {
        return $this->pesJuridica;
    }

    /**
     * @param \Pessoa\Entity\PessoaJuridica $pesJuridica
     * @return AcessoPessoas
     */
    public function setPesJuridica($pesJuridica)
    {
        $this->pesJuridica = $pesJuridica;

        return $this;
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param string $login
     * @return AcessoPessoas
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * @return string
     */
    public function getSenha()
    {
        return $this->senha;
    }

    /**
     * @param string $senha
     * @return AcessoPessoas
     */
    public function setSenha($senha)
    {
        $this->senha = $senha;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return AcessoPessoas
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getDataUltimoLogin($format = false)
    {
        $dataUltimoLogin = $this->dataUltimoLogin;

        if ($format && $dataUltimoLogin) {
            $dataUltimoLogin = $dataUltimoLogin->format('d/m/Y H:i:s');
        }

        return $dataUltimoLogin;
    }

    /**
     * @param \Datetime $dataUltimoLogin
     * @return AcessoPessoas
     */
    public function setDataUltimoLogin($dataUltimoLogin)
    {
        if ($dataUltimoLogin) {
            if (is_string($dataUltimoLogin)) {
                $dataUltimoLogin = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $dataUltimoLogin
                );
                $dataUltimoLogin = new \Datetime($dataUltimoLogin);
            }
        } else {
            $dataUltimoLogin = null;
        }
        $this->dataUltimoLogin = $dataUltimoLogin;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    public function toArray()
    {
        if ($this->getPesJuridica() != null) {
            $pesJuridica = $this->getPesJuridica()->getPes()->getPesId();
        } else {
            $pesJuridica = null;
        }

        if ($this->getPesFisica() != null) {
            $pesFisica = $this->getPesFisica()->getPes()->getPesId();
        } else {
            $pesFisica = null;
        }

        return array(
            'id'              => $this->getId(),
            'login'           => $this->getLogin(),
            'senha'           => $this->getSenha(),
            'dataUltimoLogin' => $this->getDataUltimoLogin(true),
            'usuario'         => $this->getUsuario()->getId(),
            'usuarioId'       => $this->getUsuario()->getId(),
            'pesJuridica'     => $pesJuridica,
            'pesFisica'       => $pesFisica,
            'email'           => $this->getEmail(),
            'usrStatus'       => $this->getUsuario() ? $this->getUsuario()->getUsrStatus() : '',
            'campusCurso'     => $this->getCursoCampus()
        );
    }
}
