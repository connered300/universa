<?php

namespace Acesso\Service;

use VersaSpine\Service\AbstractService;

/**
 * Description of AcessoFuncionalidades
 * Classe que prove serviços de funcionalidades
 * @author joao
 */
class AcessoFuncionalidades extends AbstractService
{
    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Acesso\Entity\AcessoFuncionalidades');
        $this->setLabelEntity("func_nome");
        $this->setServicesDependency(array('actions' => 'Acesso\Service\AcessoActions'));
    }

    protected function pesquisaForJson($criterios)
    {
    }

    protected function valida($dados)
    {
    }

    /**
     * Busca a lista de funcionalidades com base nos parametros
     * @param int    $usuario
     * @param int    $sistema
     * @param string $menu_principal
     * @return array
     */
    public function listaFuncionalidades($usuario, $sistema = null, $menu_principal = false, $grupo = false)
    {
        $sistemas        = null;
        $modulos         = null;
        $funcionalidades = null;
        $actions         = null;
        $actLabel        = null;
        $userSession     = $_SESSION['Zend_Auth']['storage']['id'];

        if ($menu_principal) {
            $menu_principal = "func_visivel = 'Sim' AND func_status = 'Ativa' AND";
        }

        $where = (!is_null($sistema)) ? " WHERE acesso_sistemas.id = '$sistema' " : "";

        if ($userSession != 1) {
            $where .= " and acesso_pessoas.id = $userSession ";
        } else {
            $where .= " WHERE acesso_pessoas.id = $userSession ";
        }

        if ($grupo and $userSession != 1) {
            $sql = "
SELECT DISTINCT
    acesso_sistemas.id AS id,
    acesso_sistemas.sis_nome AS sis_nome,
    acesso_sistemas.sis_status AS sis_status,
    acesso_sistemas.sis_icon AS sis_icon,
    acesso_sistemas.sis_info AS sis_info
FROM
    acesso_sistemas
        INNER JOIN
    acesso_modulos ON acesso_modulos.sistema = acesso_sistemas.id
        INNER JOIN
    acesso_funcionalidades ON acesso_funcionalidades.modulo = acesso_modulos.id
        INNER JOIN
    acesso_actions ON acesso_actions.funcionalidade = acesso_funcionalidades.id
        INNER JOIN
    acesso_privilegios_grupo ON acesso_privilegios_grupo.acesso_actions_id = acesso_actions.id
        INNER JOIN
    acesso_grupo ON acesso_grupo.id = acesso_privilegios_grupo.acesso_grupo_id
        INNER JOIN
    acesso_usuarios_grupos ON acesso_usuarios_grupos.grupo_id = acesso_grupo.id
        INNER JOIN
    acesso_usuarios ON acesso_usuarios.id = acesso_usuarios_grupos.usuario_id
        INNER JOIN
    acesso_pessoas ON acesso_pessoas.usuario = acesso_usuarios.id
    ";

            $sql .= $where;
            $sqlModulos = "
SELECT DISTINCT
    acesso_modulos.id AS id, acesso_modulos.mod_nome AS mod_nome
FROM
    acesso_modulos
        INNER JOIN
    acesso_funcionalidades ON acesso_funcionalidades.modulo = acesso_modulos.id
        INNER JOIN
    acesso_actions ON acesso_actions.funcionalidade = acesso_funcionalidades.id
        INNER JOIN
    acesso_privilegios_grupo ON acesso_privilegios_grupo.acesso_actions_id = acesso_actions.id
        INNER JOIN
    acesso_grupo ON acesso_grupo.id = acesso_privilegios_grupo.acesso_grupo_id
        INNER JOIN
    acesso_usuarios_grupos ON acesso_usuarios_grupos.grupo_id = acesso_grupo.id
        INNER JOIN
    acesso_usuarios ON acesso_usuarios.id = acesso_usuarios_grupos.usuario_id
        INNER JOIN
    acesso_pessoas ON acesso_pessoas.usuario = acesso_usuarios.id
WHERE
            ";

            $sqlFuncionalidades = "
SELECT DISTINCT
    acesso_funcionalidades.id AS id,
    acesso_funcionalidades.modulo AS modulo,
    acesso_funcionalidades.func_nome AS func_nome,
    acesso_funcionalidades.func_controller AS func_controller,
    acesso_funcionalidades.func_icon AS func_icon,
    acesso_funcionalidades.func_info AS func_info,
    acesso_funcionalidades.func_status AS func_status,
    acesso_funcionalidades.func_visivel AS func_visivel
FROM
    acesso_funcionalidades
        INNER JOIN
    acesso_actions ON acesso_actions.funcionalidade = acesso_funcionalidades.id
        INNER JOIN
    acesso_privilegios_grupo ON acesso_privilegios_grupo.acesso_actions_id = acesso_actions.id
        INNER JOIN
    acesso_grupo ON acesso_grupo.id = acesso_privilegios_grupo.acesso_grupo_id
        INNER JOIN
    acesso_usuarios_grupos ON acesso_usuarios_grupos.grupo_id = acesso_grupo.id
        INNER JOIN
    acesso_usuarios ON acesso_usuarios.id = acesso_usuarios_grupos.usuario_id
        INNER JOIN
    acesso_pessoas ON acesso_pessoas.usuario = acesso_usuarios.id
WHERE
            ";

            $sqlActions = "
SELECT DISTINCT
    acesso_actions.id as id,
    acesso_actions.funcionalidade as funcionalidade,
    acesso_actions.act_nome as act_nome,
    acesso_actions.act_label as act_label,
    acesso_actions.act_icon as act_icon,
    acesso_actions.act_info as act_info
FROM
    acesso_actions
        INNER JOIN
    acesso_privilegios_grupo ON acesso_privilegios_grupo.acesso_actions_id = acesso_actions.id
        INNER JOIN
    acesso_grupo ON acesso_grupo.id = acesso_privilegios_grupo.acesso_grupo_id
        INNER JOIN
    acesso_usuarios_grupos ON acesso_usuarios_grupos.grupo_id = acesso_grupo.id
        INNER JOIN
    acesso_usuarios ON acesso_usuarios.id = acesso_usuarios_grupos.usuario_id
        INNER JOIN
    acesso_pessoas ON acesso_pessoas.usuario = acesso_usuarios.id
WHERE
            ";
        } else {
            if ($userSession != 1) {
                $sql = "
SELECT distinct
    acesso_sistemas.id AS id,
    acesso_sistemas.sis_nome AS sis_nome,
    acesso_sistemas.sis_status AS sis_status,
    acesso_sistemas.sis_icon AS sis_icon,
    acesso_sistemas.sis_info AS sis_info
FROM
    acesso_sistemas
        INNER JOIN
    acesso_modulos ON acesso_modulos.sistema = acesso_sistemas.id
        INNER JOIN
    acesso_funcionalidades ON acesso_funcionalidades.modulo = acesso_modulos.id
        INNER JOIN
    acesso_actions ON acesso_actions.funcionalidade = acesso_funcionalidades.id
        INNER JOIN
    acesso_usuarios_privilegios ON acesso_usuarios_privilegios.acesso_actions_id = acesso_actions.id
        INNER JOIN
    acesso_usuarios ON acesso_usuarios.id = acesso_usuarios_privilegios.acesso_usuarios_id
        INNER JOIN
    acesso_pessoas ON acesso_pessoas.usuario = acesso_usuarios.id
";

                $sql .= $where;
                $sqlModulos = "
SELECT distinct
    acesso_modulos.id AS id,
    acesso_modulos.mod_nome AS mod_nome
FROM
    acesso_modulos
        INNER JOIN
    acesso_funcionalidades ON acesso_funcionalidades.modulo = acesso_modulos.id
        INNER JOIN
    acesso_actions ON acesso_actions.funcionalidade = acesso_funcionalidades.id
        INNER JOIN
    acesso_usuarios_privilegios ON acesso_usuarios_privilegios.acesso_actions_id = acesso_actions.id
        INNER JOIN
    acesso_usuarios ON acesso_usuarios.id = acesso_usuarios_privilegios.acesso_usuarios_id
        INNER JOIN
    acesso_pessoas ON acesso_pessoas.usuario = acesso_usuarios.id
WHERE
                        ";

                $sqlFuncionalidades = "
SELECT distinct
    acesso_funcionalidades.id AS id,
    acesso_funcionalidades.modulo as modulo,
    acesso_funcionalidades.func_nome as func_nome,
    acesso_funcionalidades.func_controller as func_controller,
    acesso_funcionalidades.func_icon as func_icon,
    acesso_funcionalidades.func_info as func_info,
    acesso_funcionalidades.func_status as func_status,
    acesso_funcionalidades.func_visivel as func_visivel
    FROM
        acesso_funcionalidades
            INNER JOIN
        acesso_actions ON acesso_actions.funcionalidade = acesso_funcionalidades.id
            INNER JOIN
        acesso_usuarios_privilegios ON acesso_usuarios_privilegios.acesso_actions_id = acesso_actions.id
            INNER JOIN
        acesso_usuarios ON acesso_usuarios.id = acesso_usuarios_privilegios.acesso_usuarios_id
            INNER JOIN
        acesso_pessoas ON acesso_pessoas.usuario = acesso_usuarios.id
    WHERE ";

                $sqlActions = "
SELECT distinct
    acesso_actions.id as id,
    acesso_actions.funcionalidade as funcionalidade,
    acesso_actions.act_nome as act_nome,
    acesso_actions.act_label as act_label,
    acesso_actions.act_icon as act_icon,
    acesso_actions.act_info as act_info
FROM
    acesso_actions
        INNER JOIN
    acesso_usuarios_privilegios ON acesso_usuarios_privilegios.acesso_actions_id = acesso_actions.id
        INNER JOIN
    acesso_usuarios ON acesso_usuarios.id = acesso_usuarios_privilegios.acesso_usuarios_id
        INNER JOIN
    acesso_pessoas ON acesso_pessoas.usuario = acesso_usuarios.id
WHERE ";
            } else {
                $sql                = "select * from acesso_sistemas ";
                $sqlModulos         = "select * from acesso_modulos where ";
                $sqlFuncionalidades = "select * from acesso_funcionalidades where ";
                $sqlActions         = "select * from acesso_actions where ";
            }
        }

        $sistemas = $this->executeQuery($sql . "  ORDER BY sis_nome ASC ")->fetchAll();

        foreach ($sistemas as $sis) {
            $modulos[$sis['id']] = $this->executeQuery(
                $sqlModulos . " sistema = '{$sis['id']}' ORDER BY mod_nome ASC "
            )->fetchAll();
            foreach ($modulos[$sis['id']] as $mod) {
                $funcionalidades[$mod['id']] = $this->executeQuery(
                    $sqlFuncionalidades . " $menu_principal modulo = '{$mod['id']}' ORDER BY func_nome ASC "
                )->fetchAll();
                foreach ($funcionalidades[$mod['id']] as $func) {
                    $actions[$func['id']] = $this->executeQuery(
                        $sqlActions . " funcionalidade = '{$func['id']}' ORDER BY act_nome ASC "
                    )->fetchAll();
                }
            }
        }

        return array(
            'sistemas'        => $sistemas,
            'modulos'         => $modulos,
            'funcionalidades' => $funcionalidades,
            'actions'         => $actions,
        );
    }

    /**
     * Adiciona uma nova funcionalidade
     * @param array $dados
     */
    public function adicionar(array $dados)
    {
        parent::begin();

        $entity = parent::adicionar($dados);

        if (count($dados['acessoActions']) > 0) {
            $func = $entity->getId();

            foreach ($dados['acessoActions'] as $indice => $action) {
                $action['funcionalidade'] = $func;

                $entity = $this->actions->adicionar($action);

                if (isset($dados['acessoSubActions'][$indice])) {
                    $this->executeQuery(
                        "REPLACE INTO acesso_actions_filhas VALUES " .
                        "({$entity->getId()}," . implode(
                            $dados['acessoSubActions'][$indice],
                            "),({$entity->getId()},"
                        ) . ")"
                    );
                }
            }
        }
        parent::commit();
    }

    /**
     * Edita um funcionaldade
     * @param array $dados
     * @throws \Exception
     */
    public function edita(array $dados)
    {
        parent::begin();
        $ret = parent::edita($dados);

        foreach ($dados['acessoActions'] as $action) {
            if ($action['id']) {
                $this->actions->edita($action);

                foreach ($dados['acessoSubActions'] as $key => $values) {
                    if ($action['id'] == $key) {
                        try {
                            if (count($values) > 0) {
                                $this->executeQuery("DELETE FROM acesso_actions_filhas WHERE pai = '$key' ");

                                $this->executeQuery(
                                    "INSERT INTO acesso_actions_filhas VALUES " . "($key," . implode(
                                        $values,
                                        "),($key,"
                                    ) . ")"
                                );
                            }
                        } catch (\Exception $ex) {
                            throw new \Exception($ex->getMessage(), $ex->getCode());
                        }
                    }
                }
            }
        }

        $func = $ret->getId();

        foreach ($dados['acessoActions'] as $indice => $action) {
            if (!$action['id']) {
                try {
                    $action['funcionalidade'] = $func;
                    $ret                      = $this->actions->adicionar($action);

                    if (isset($dados['acessoSubActions'][$indice])) {
                        $this->executeQuery(
                            "INSERT INTO acesso_actions_filhas VALUES " . "({$ret->getId()}," . implode(
                                $dados['acessoSubActions'][$indice],
                                "),({$ret->getId()},"
                            ) . ")"
                        );
                    }
                } catch (\Exception $ex) {
                    throw new \Exception($ex->getMessage(), $ex->getCode());
                }
            }
        }

        parent::commit();
    }

    public function buscaFuncionalidadesUsuario($usuario, $redirect = false)
    {
        $sql = "SELECT * FROM (
                (
                    SELECT
                        s.sis_nome, s.id sis_id, am.id mod_id, am.mod_icon,
                        am.mod_route, am.mod_nome, af.id func_id, af.func_controller,
                        af.func_nome, aa.act_nome, aa.act_label, am.mod_prioridade, am.mod_redireciona
                    FROM acesso_usuarios_grupos ug
                    INNER JOIN acesso_grupo ag ON ag.id = ug.grupo_id
                    INNER JOIN acesso_privilegios_grupo pg ON pg.acesso_grupo_id = ag.id
                    INNER JOIN acesso_actions aa ON aa.id = pg.acesso_actions_id
                    INNER JOIN acesso_funcionalidades af ON af.id = aa.funcionalidade
                    INNER JOIN acesso_modulos am ON am.id = af.modulo
                    INNER JOIN acesso_sistemas s ON s.id = am.sistema
                    WHERE ug.usuario_id = $usuario AND af.func_status = 'Ativa' AND af.func_visivel = 'Sim' GROUP BY af.id  ORDER BY am.mod_prioridade DESC )
                    UNION
                    (
                        SELECT
                            s.sis_nome, s.id sis_id, am.id mod_id, am.mod_icon,
                            am.mod_route, am.mod_nome, af.id func_id, af.func_controller,
                            af.func_nome, aa.act_nome, aa.act_label, am.mod_prioridade, am.mod_redireciona
                        FROM acesso_usuarios_privilegios up
                        INNER JOIN acesso_actions aa ON aa.id = up.acesso_actions_id
                        INNER JOIN acesso_funcionalidades af ON af.id = aa.funcionalidade
                        INNER JOIN acesso_modulos am ON am.id = af.modulo
                        INNER JOIN acesso_sistemas s ON s.id = am.sistema
                        WHERE up.acesso_usuarios_id = $usuario AND
                              af.func_status = 'Ativa' AND
                              af.func_visivel = 'Sim'
                        GROUP BY af.id ORDER BY am.mod_prioridade DESC
                    )
                ) AS privilegios
                WHERE mod_prioridade IS NOT NULL AND
                      mod_redireciona IS NOT NULL
                ORDER BY mod_prioridade, sis_nome, mod_nome, func_nome, act_nome";

        $sistemas        = array();
        $modulos         = array();
        $funcionalidades = array();

        if ($redirect) {
            return parent::executeQuery($sql)->fetch();
        }

        $ret = parent::executeQuery($sql)->fetchAll();

        foreach ($ret as $r) {
            $sistemas[$r['sis_id']] = $r;

            $modulos[$r['sis_id']][$r['mod_id']] = array(
                'id'              => $r['mod_id'],
                'mod_icon'        => $r['mod_icon'],
                'mod_nome'        => $r['mod_nome'],
                'mod_route'       => $r['mod_route'],
                'mod_prioridade'  => $r['mod_prioridade'],
                'mod_redireciona' => $r['mod_redireciona']
            );

            $funcionalidades[$r['mod_id']][$r['func_id']] = array(
                'id'              => $r['func_id'],
                'func_nome'       => $r['func_nome'],
                'func_controller' => $r['func_controller']
            );
        }

        return array(
            'sistemas'        => $sistemas,
            'modulos'         => $modulos,
            'funcionalidades' => $funcionalidades,
        );
    }

}
