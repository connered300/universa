<?php

namespace Acesso\Service;

use VersaSpine\Service\AbstractService;

/**
 * Description of AcessoModulos
 * Classe que prove serviços de manipulção de dados
 * @author joao
 */
class AcessoGrupo extends AbstractService
{
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, "Acesso\Entity\AcessoGrupo");
    }

    /**
     * Implementacao da assinatura
     * @param array $a
     * @return array
     */
    public function pesquisaForJson($a)
    {
        $userSession = $_SESSION['Zend_Auth']['storage']['id'];
        if ($userSession != 1) {
            $sql = "
                    SELECT DISTINCT
                    acesso_grupo.id as id,
                    grup_nome,
                    grup_info
                FROM
                    acesso_grupo
                        INNER JOIN
                    acesso_usuarios_grupos ON acesso_usuarios_grupos.grupo_id = acesso_grupo.id
                        INNER JOIN
                    acesso_usuarios ON acesso_usuarios.id = acesso_usuarios_grupos.usuario_id
                        INNER JOIN
                    acesso_pessoas ON acesso_pessoas.usuario = acesso_usuarios.id
                WHERE
                    acesso_grupo.grup_nome like :grupo
                    and
                    acesso_pessoas.id = $userSession

        ";
        } else {
            $sql = "SELECT * FROM acesso_grupo WHERE acesso_grupo.grup_nome LIKE :grupo";
        }

        $parameters = $a['query'] . '%';

        $array = $this->executeQueryWithParam($sql, ['grupo' => $parameters])->fetchAll();

        $grupos = array();
        foreach ($array as $grupo) {
            $grupos[] = $grupo;
        }

        return $grupos;
    }

    /**
     * Busca as ações
     * @param array $grupo
     * @return array
     */
    public function buscaActions($grupo)
    {
        $actions = array();
        $array   = $this->executeQuery(
            "SELECT acesso_actions_id FROM acesso_privilegios_grupo WHERE acesso_grupo_id = '$grupo' "
        )->fetchAll();
        foreach ($array as $a) {
            $actions[] = $a['acesso_actions_id'];
        }

        return $actions;
    }

    public function buscaGrupoUsr($usuario = null)
    {
        if (is_null($usuario)) {
            $usuario = $_SESSION['Zend_Auth']['storage']['usuario'];
        }
        $sql    = "SELECT * FROM acesso_usuarios_grupos WHERE usuario_id = $usuario";
        $stm    = parent::executeQuery($sql);
        $grupos = array();
        while ($grupo = $stm->fetch()) {
            $grupos[] = (int)$grupo['grupo_id'];
        }

        return $grupos;
    }

    public function verificaActionGrupo($controller, $action, $grupos = null)
    {
        $grupos = (isset($grupos) ? $grupos : $_SESSION["Zend_Auth"]["storage"]["grupos"]);

        $grupos = implode(",", $grupos);

        $controller = addslashes($controller);

        $sql = "SELECT
                    actions.act_nome
                FROM
                    acesso_funcionalidades acesso_func
                INNER JOIN
                    acesso_actions actions
                ON
                    acesso_func.id = actions.funcionalidade
                INNER JOIN
                    acesso_privilegios_grupo apg
                ON
                    actions.id = apg.acesso_actions_id
                WHERE
                    acesso_func.func_controller = '{$controller}'
                AND
                    actions.act_nome = '$action'
                AND
                    apg.acesso_grupo_id IN ($grupos)
                  ";
        $smt = parent::executeQuery($sql);

        return (($smt->rowCount()) ? true : false);
    }

    public function verifyUserToGroup($usuario = null, $group)
    {
        if (is_null($usuario)) {
            $usuario = $_SESSION['Zend_Auth']['storage']['usuario'];
        }

        $query = <<<SQL
            SELECT
                grupo_id
            FROM
                acesso_usuarios_grupos
                    INNER JOIN
                acesso_grupo ON acesso_grupo.id = acesso_usuarios_grupos.grupo_id
SQL;

        $condicoes   = array();
        $condicoes[] = "grup_nome = '{$group}'";
        $condicoes[] = "usuario_id = {$usuario}";
        $query .= " WHERE " . implode(" AND ", $condicoes);

        $smt = parent::executeQuery($query);

        return (($smt->rowCount()) ? true : false);
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['id']) {
                /** @var $objAcessoGrupo \Acesso\Entity\AcessoGrupo */
                $objAcessoGrupo = $this->getRepository()->find($arrDados['id']);

                if (!$objAcessoGrupo) {
                    $this->setLastError('Registro de grupo não existe!');

                    return false;
                }
            } else {
                $objAcessoGrupo = new \Acesso\Entity\AcessoGrupo();
            }

            $objAcessoGrupo->setGrupNome($arrDados['grupNome']);
            $objAcessoGrupo->setGrupInfo($arrDados['grupInfo']);
            $objAcessoGrupo->setGrupIcon($arrDados['grupIcon']);

            $this->getEm()->persist($objAcessoGrupo);
            $this->getEm()->flush($objAcessoGrupo);

            $this->getEm()->commit();

            $arrDados['id'] = $objAcessoGrupo->getId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de grupo!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['grupNome']) {
            $errors[] = 'Por favor preencha o campo "nome"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM acesso_grupo";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($id)
    {
        /** @var $objAcessoGrupo \Acesso\Entity\AcessoGrupo */
        $objAcessoGrupo = $this->getRepository()->find($id);

        try {
            $arrDados = $objAcessoGrupo->toArray();
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {

        return $arrDados;
    }

    public function getArrSelect()
    {
        $arrEntities = $this->getRepository()->findBy([], ['grupNome' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Acesso\Entity\AcessoGrupo */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[$objEntity->getId()] = $objEntity->getGrupNome();
        }

        return $arrEntitiesArr;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['id'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['grupNome' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Acesso\Entity\AcessoGrupo */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getId();
            $arrEntity[$params['value']] = $objEntity->getGrupNome();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['id']) {
            $this->setLastError('Para remover um registro de grupo é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objAcessoGrupo \Acesso\Entity\AcessoGrupo */
            $objAcessoGrupo = $this->getRepository()->find($param['id']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAcessoGrupo);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de grupo.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
    }

    public function getArrayAcessoGrupoPeloId($id)
    {
        $array = array();
        $arrId = explode(',', $id);

        foreach ($arrId as $id) {
            $array[] = $this->getRepository()->find($id);
        }

        return $array;
    }
}
