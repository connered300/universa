<?php

namespace Acesso\Service;

use VersaSpine\Service\AbstractService;

/**
 * Description of AcessoModulos
 * Classe que gerencia os serviços da entidade AcessoActions
 * @author joao
 */
class AcessoActions extends AbstractService
{
    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Acesso\Entity\AcessoActions');
    }

    /**
     * Busca dados das actions com base nos parametros
     * @param array $criterios
     * @return array
     */
    public function pesquisaForJson($criterios)
    {
        $where = array();
        $param = array();

        foreach ($criterios as $key => $value) {
            $where[]     = "$key = :$key";
            $param[$key] = $value;
        }

        $query = "SELECT * FROM acesso_actions";

        if (count($where) > 0) {
            $query .= ' WHERE ' . implode(' AND ', $where);
        }

        return $this->executeQueryWithParam($query, $param)->fetchAll();
    }

    protected function valida($dados)
    {
    }

    /**
     * Busca por uma action e verifica se a mesma pode ser removida
     * @param array $post
     * @return integer
     */
    public function removivel($post)
    {
        $sql = "SELECT COUNT(*) as count FROM acesso_actions_filhas WHERE pai = '{$post['action']}' ";

        return $this->executeQuery($sql)->fetch();
    }

    /**
     * Busca as actions filhas
     * @param array $action
     * @return array
     */
    public function buscaActionsFilhas($action)
    {
        $query  = "SELECT filha FROM acesso_actions_filhas WHERE pai = '$action' ";
        $array  = $this->executeQuery($query)->fetchAll();
        $filhas = array();

        foreach ($array as $a) {
            $filhas[] = $a['filha'];
        }

        return $filhas;
    }

}
