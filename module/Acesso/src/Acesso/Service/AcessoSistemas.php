<?php

namespace Acesso\Service;

use VersaSpine\Service\AbstractService;

/**
 * Description of AcessoSistemas
 * Classe que prove os serviços de acesso ao sistemas
 * @author joao
 */
class AcessoSistemas extends AbstractService
{
    protected function pesquisaForJson($criterios)
    {
    }

    protected function valida($dados)
    {
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, "Acesso\Entity\AcessoSistemas");
        $this->setLabelEntity("sis_nome");
    }

}
