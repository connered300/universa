<?php

namespace Acesso\Service;

use Doctrine\DBAL\Platforms\SQLAnywhere11Platform;
use VersaSpine\Service\AbstractService;

/**
 * Description of AcessoLog
 * Classe que prove os serviços de LOG
 * @author jp
 */
class AcessoInterface extends AbstractService
{
    protected function pesquisaForJson($criterios)
    {
        // TODO: Implement pesquisaForJson() method.
    }

    public function __construct($em)
    {
        parent::__construct($em, 'Acesso\Entity\AcessoPessoas');
    }

    public function getDataForDataTables($data)
    {
        $sql = <<<SQL
            select
              login,
              email,
              usr_status status,
              grup_nome grupo,
              pes_nome nome,
              usuario_id,
              pes_id,
              grupo_id
              from acesso_pessoas
              join acesso_usuarios using(id)
              LEFT join acesso_usuarios_grupos on usuario = usuario_id
              LEFT join acesso_grupo on grupo_id = acesso_grupo.id
              LEFT JOIN pessoa on pes_fisica = pessoa.pes_id
SQL;

        $result = parent::paginationDataTablesAjax($sql, $data, null, false);

        return $result;
    }

    protected function valida($dados)
    {
    }


}
