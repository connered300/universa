<?php

namespace Acesso\Service;

use VersaSpine\Service\AbstractService;

/**
 * Description of AcessoModulos
 * Classe que prove serviços de módulos
 * @author joao
 */
class AcessoModulos extends AbstractService
{
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, "Acesso\Entity\AcessoModulos");
        $this->setLabelEntity("mod_nome");
    }

    protected function pesquisaForJson($criterios)
    {
    }

    protected function valida($dados)
    {
    }

}
