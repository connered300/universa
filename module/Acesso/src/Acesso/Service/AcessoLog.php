<?php

namespace Acesso\Service;

use VersaSpine\Service\AbstractService;

/**
 * Description of AcessoLog
 * Classe que prove os serviços de LOG
 * @author jp
 */
class AcessoLog extends AbstractService
{
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, "Acesso\Entity\AcessoLog");
    }

    protected function pesquisaForJson($criterios)
    {
    }

    protected function valida($dados)
    {
    }

}
