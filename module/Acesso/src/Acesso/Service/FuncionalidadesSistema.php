<?php

namespace Acesso\Service;

/**
 * Description of FuncionalidadesSistema
 * Classe que prove servicos de funcionalidades do sistema
 * @author jp
 */
class FuncionalidadesSistema extends \VersaSpine\Service\AbstractService
{
    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     * @param string                      $entity
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, $entity)
    {
        $this->setEm($em);
        $this->setServicesDependency(
            array(
                'service'         => 'Acesso\Service\AcessoSistemas',
                'modulos'         => 'Acesso\Service\AcessoModulos',
                'funcionalidades' => 'Acesso\Service\AcessoFuncionalidades',
                'actions'         => 'Acesso\Service\AcessoActions'
            )
        );
    }

    protected function pesquisaForJson($criterios)
    {
    }

    protected function valida($dados)
    {
    }

    /**
     * Método que busca as funcionalidades e monta e gera o html do mesmo
     * @param int $sistema
     * @param int $modulo
     * @return string
     */
    public function listaFuncionalidadesSistema($sistema = null, $modulo = null)
    {
        $html = "";
        $html .= "<div class='tree smart-form'>";
        $sistemas = $this->service->findAll();

        $html .= "<ul role='tree'>";
        foreach ($sistemas as $sistema) {
            $html .= "<li class='parent_li' role='treeitem'>";
            $html .= "<span title='Collapse this branch'><i class='fa fa-lg fa-minus-circle'></i> {$sistema->getSisNome()}</span>";
            $html .= "<ul class='group'>";
            $modulos = $this->modulos->findBy(array('sistema' => $sistema->getId()));
            foreach ($modulos as $modulo) {
                $html .= "<li class='parent_li' role='treeitem' style='display: list-item;'>";
                $html .= "<span title='Expand this branch'><i class='fa fa-lg fa-plus-circle'></i> {$modulo->getModNome()}</span>";
                $html .= "<ul class='group'>";
                $funcionalidades = $this->funcionalidades->findBy(array('modulo' => $modulo->getId()));
                foreach ($funcionalidades as $func) {
                    $html .= "<li class='parent_li' role='treeitem' style='display: list-item;'>";
                    $html .= "<span title='Expand this branch'><i class='fa fa-lg fa-plus-circle'></i> {$func->getFuncNome()}</span>";
                    $html .= "<ul class='group'>";
                    $actions = $this->actions->findBy(array('funcionalidade' => $func->getId()));
                    foreach ($actions as $act) {
                        $html .= "<li class='parent_li' role='treeitem' style='display: list-item;'>";
                        $html .= "<label class='checkbox inline-block'>
                                    <input type='checkbox' name='funcionalidades[{$sistema->getId()}][{$modulo->getId()}][{$act->getId()}]'>
                                    <i></i>{$act->getActLabel()}</label>";
                        $html .= "</li>";
                    }
                    $html .= "</ul>";
                    $html .= "</li>";
                }
                $html .= "</ul>";
                $html .= "</li>";
            }
            $html .= "</ul>";
        }
        $html .= "</ul>";
        $html .= "</div>";

        return $html;
    }

}
