<?php

namespace Acesso\Service;

use VersaSpine\Service\AbstractService;

/**
 * Classe que prove serviços para manipular os dados das pessoas e seus usuários
 */
class AcessoPessoas extends AbstractService
{
    protected $allowedSources = array(
        'acesso-pessoas'
    );

    private $__lastError = null;

    /** @var \Sistema\Service\SisConfig */
    private $config = null;

    /**
     * @return array
     */
    protected function getArrAllowedSources()
    {
        return $this->allowedSources;
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @return \Sistema\Service\SisConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param \Sistema\Service\SisConfig $config
     * @return AcessoPessoas
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, $arrConfig = array())
    {
        parent::__construct($em, 'Acesso\Entity\AcessoPessoas');
        $this->setConfig(new \Sistema\Service\SisConfig($em, $arrConfig));
    }

    /**
     * @param $params
     * @return mixed
     */
    public function pesquisaForJson($params)
    {
        $parameters = array();
        $sql        = '
        SELECT
        pes_id, pes_nome, id, login
        FROM acesso_pessoas
        LEFT JOIN pessoa ON pes_id = coalesce(pes_fisica,pes_juridica)
        LEFT JOIN acesso_usuarios_grupos ON acesso_usuarios_grupos.usuario_id = acesso_pessoas.usuario
         WHERE 1=1';
        $login      = false;
        $id         = false;

        if ($params['grupo']) {
            $sql .= ' AND acesso_usuarios_grupos.grupo_id IN (' . $params['grupo'] . ')';
        }

        if ($params['q']) {
            $login = $params['q'];
        } elseif ($params['query']) {
            $login = $params['query'];
        }

        if ($params['id']) {
            $id = $params['id'];
            $sql .= ' AND id like :id';
        }

        if ($login) {
            $parameters['login'] = $login . '%';
            $sql .= ' AND (login like :login OR pes_nome COLLATE UTF8_GENERAL_CI LIKE :login)';
        }

        if ($id) {
            $parameters['id'] = $id;
            $sql .= ' AND id <> :id';
        }

        if ($params['nome']) {
            $parameters['nome'] = $params['nome'] . '%';
            $sql .= ' AND pes_nome COLLATE UTF8_GENERAL_CI LIKE :nome';
        }

        if ($params['agruparPessoas']) {
            $sql .= " GROUP BY pes_id";
        }

        if ($params['agruparPorUsuario']) {
            $sql .= " GROUP BY id";
        }

        $sql .= " ORDER BY login";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    /** Retorna true se o login estiver sendo usado */

    public function ifLoginIsUsed($params)
    {
        $where = "";

        if ($params["id"]) {
            $where .= " AND usuario != :id";
        } else {
            unset($params["id"]);
        }

        $sql = "
            SELECT
                  login, usuario
            FROM
                  acesso_pessoas
            WHERE
                  acesso_pessoas.login LIKE :text $where
        ";

        $result = $this->executeQueryWithParam($sql, $params)->fetch();

        return $result;
    }

    public function getArrSelect2UsuarioLogado()
    {
        $usuarioLogado = $this->retornaUsuarioLogado();

        if ($usuarioLogado) {
            return array(
                'id'   => $usuarioLogado->getId(),
                'text' => $usuarioLogado->getPes()->getPes()->getPesNome(),
            );
        }

        return array();
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $objAcessoUsuario      = null;
        $serviceAcessoUsuarios = new \Acesso\Service\AcessoUsuarios($this->getEm());
        $serviceAcessoGrupo    = new \Acesso\Service\AcessoGrupo($this->getEm());
        $servicePessoaFisica   = new \Pessoa\Service\PessoaFisica($this->getEm());
        $servicePessoaJuridica = new \Pessoa\Service\PessoaJuridica($this->getEm());
        $serviceCampusCurso    = new \Matricula\Service\CampusCurso($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['id']) {
                /** @var $objAcessoPessoas \Acesso\Entity\AcessoPessoas */
                $objAcessoPessoas = $this->getRepository()->find($arrDados['id']);

                if (!$objAcessoPessoas) {
                    $this->setLastError('Registro de pessoas não existe!');

                    return false;
                }

                $objAcessoUsuario = $objAcessoPessoas->getUsuario();
            } else {
                $objAcessoPessoas = new \Acesso\Entity\AcessoPessoas();
            }

            if ($arrDados['usuario'] && !$objAcessoUsuario) {
                $objAcessoUsuario = $serviceAcessoUsuarios->getRepository()->find($arrDados['usuario']);
            } elseif (!$objAcessoUsuario) {
                $objAcessoUsuario = new \Acesso\Entity\AcessoUsuarios(array());
                $objAcessoUsuario->setAcessoDataRegistro(new \DateTime('now'));
                $objAcessoUsuario->setAcessoDataSenhaSalva(new \DateTime('now'));
            }

            if ($arrDados['acessoGrupo']) {
                preg_match_all('/[\d]+/', $arrDados['acessoGrupo'], $arrGrupoId);
                $arrGrupo = new \Doctrine\Common\Collections\ArrayCollection(
                    $serviceAcessoGrupo->getRepository('Acesso\Entity\AcessoGrupo')->findBy(['id' => $arrGrupoId[0]])
                );
            } else {
                $arrGrupo = new \Doctrine\Common\Collections\ArrayCollection();
            }

            if ($arrDados['campusCurso']) {
                preg_match_all('/[\d]+/', $arrDados['campusCurso'], $arrCampusCurso);
                $arrCollectionCampuscurso = new \Doctrine\Common\Collections\ArrayCollection(
                    $serviceCampusCurso->getRepository()->findBy(['cursocampusId' => $arrCampusCurso[0]])
                );

                $objAcessoPessoas->setCursoCampus($arrCollectionCampuscurso);
            } else {
                $objAcessoPessoas->setCursoCampus((new \Doctrine\Common\Collections\ArrayCollection()));
            }

            $objAcessoUsuario->setUsrStatus($arrDados['usrStatus']);
            $objAcessoUsuario->setGrupo($arrGrupo);

            $this->getEm()->persist($objAcessoUsuario);
            $this->getEm()->flush($objAcessoUsuario);

            $objAcessoPessoas->setUsuario($objAcessoUsuario);

            if ($arrDados['pesFisica']) {
                /** @var $objPessoaFisica \Pessoa\Entity\PessoaFisica */
                $objPessoaFisica = $servicePessoaFisica->getRepository()->find($arrDados['pesFisica']);

                if (!$objPessoaFisica) {
                    $this->setLastError('Registro de física não existe!');

                    return false;
                }

                $objAcessoPessoas->setPesFisica($objPessoaFisica);
            } else {
                $objAcessoPessoas->setPesFisica(null);
            }

            if ($arrDados['pesJuridica']) {
                /** @var $objPessoaJuridica \Pessoa\Entity\PessoaJuridica */
                $objPessoaJuridica = $servicePessoaJuridica->getRepository()->find($arrDados['pesJuridica']);

                if (!$objPessoaJuridica) {
                    $this->setLastError('Registro de jurídica não existe!');

                    return false;
                }

                $objAcessoPessoas->setPesJuridica($objPessoaJuridica);
            } else {
                $objAcessoPessoas->setPesJuridica(null);
            }

            $objAcessoPessoas->setLogin($arrDados['login']);
            $objAcessoPessoas->setEmail($arrDados['email']);

            if ($arrDados['senha']) {
                $objAcessoPessoas->setSenha(sha1($arrDados['senha']));
            }

            $this->getEm()->persist($objAcessoPessoas);
            $this->getEm()->flush($objAcessoPessoas);

            $this->getEm()->commit();

            $arrDados['id'] = $objAcessoPessoas->getId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de pessoas!<br>' . $e->getMessage());
        }

        return false;
    }

    public function validaUsuarioEVerificaSeEstaNoGrupo($login, $senha, $grupos)
    {
        $repository = $this->getEm()->getRepository('Acesso\Entity\AcessoPessoas');

        if (!$login || !$senha || !$grupos) {
            $this->setLastError('Por favor informe o login e senha e o(s) grupo(s) para validação!');

            return false;
        }

        if (is_string($grupos)) {
            $grupos = explode(',', $grupos);
        }

        /** @var \Acesso\Entity\AcessoPessoas $objAcessoPessoas */
        $objAcessoPessoas = $repository->validaCredenciais($login, $senha);

        if (!$objAcessoPessoas) {
            $this->setLastError('Login e/ou senha do supervisor incorretos!');

            return false;
        }

        $gruposUsuario = $objAcessoPessoas->getUsuario()->getGrupo()->toArray();

        for ($i = 0; $i <= count($grupos); $i++) {
            /** @var \Acesso\Entity\AcessoGrupo $grupo */
            foreach ($gruposUsuario as $grupo) {
                if ($grupo->getId() == $grupos[$i]) {
                    return $objAcessoPessoas;
                }
            }
        }

        $this->setLastError('O usuário não está n(o)s grupo(s) informado(s)!');

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['acessoGrupo']) {
            $errors[] = 'Por favor preencha o campo "Grupo"!';
        }
        if (!$arrParam['pesFisica'] && !$arrParam['pesJuridica']) {
            $errors[] = 'Por favor preencha o campo "pessoa física" ou o campo "pessoa jurídica"!';
        }

        if (!$arrParam['login']) {
            $errors[] = 'Por favor preencha o campo "login"!';
        }

        if (!$arrParam['id']) {
            if (!$arrParam['senha']) {
                $errors[] = 'Por favor preencha o campo "senha"!';
            }
        }

        if (!$this->validaLogin($arrParam['login'], $arrParam['id'])) {
            $errors[] = $this->getLastError();
        }

        if ($arrParam['senha'] && $arrParam['senha'] != $arrParam['passwordCheck']) {
            $errors[] = 'As senhas digitadas não conferem!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "
        SELECT
            login,
            ifnull(email,'-') email,
            group_concat(grup_nome SEPARATOR ', ') grup_nome,
            ifnull(pf.pes_nome,pj.pes_nome) pes_nome,
            TRIM(LEADING '0' FROM ap.id) id,
            au.usr_status
        FROM
            acesso_pessoas ap
            LEFT JOIN acesso_usuarios_grupos aug ON ap.usuario = aug.usuario_id
            LEFT JOIN acesso_usuarios au ON au.id = aug.usuario_id
            LEFT JOIN pessoa pf ON ap.pes_fisica = pf.pes_id
            LEFT JOIN pessoa pj ON ap.pes_juridica = pj.pes_id
            LEFT JOIN acesso_grupo ag ON ag.id = aug.grupo_id
        GROUP BY ap.id
        ";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getCampusCursoAcessoPessoaArrSelect2($param = array())
    {
        $sql = "
        SELECT
            cursocampus_id, curso_id, camp_id, camp_nome, curso_nome,
            cursocampus_id AS 'id', concat(camp_nome, ' / ', curso_nome) AS 'text'
        FROM campus_curso
        INNER JOIN acesso_pessoas_campus_curso USING(cursocampus_id)
        INNER JOIN org_campus USING (camp_id)
        INNER JOIN acad_curso USING (curso_id)
        WHERE acesso_pessoas_id = :id";

        $result = $this->executeQueryWithParam($sql, ['id' => $param])->fetchAll();

        return $result;
    }

    public function getArray($id)
    {
        /** @var $objAcessoPessoas \Acesso\Entity\AcessoPessoas */
        $objAcessoPessoas      = $this->getRepository()->find($id);
        $serviceCampusCurso    = new \Matricula\Service\CampusCurso($this->getEm());
        $servicePessoaFisica   = new \Pessoa\Service\PessoaFisica($this->getEm());
        $servicePessoaJuridica = new \Pessoa\Service\PessoaJuridica($this->getEm());

        try {
            $arrDados = $objAcessoPessoas->toArray();

            if ($arrDados['pesFisica']) {
                $arrPessoaFisica       = $servicePessoaFisica->getArrSelect2(['id' => $arrDados['pesFisica']]);
                $arrDados['pesFisica'] = $arrPessoaFisica ? $arrPessoaFisica[0] : null;
            }

            if ($arrDados['pesJuridica']) {
                $arrPessoaJuridica       = $servicePessoaJuridica->getArrSelect2(['id' => $arrDados['pesJuridica']]);
                $arrDados['pesJuridica'] = $arrPessoaJuridica ? $arrPessoaJuridica[0] : null;
            }

            $arrDados['campusCurso'] = $this->getCampusCursoAcessoPessoaArrSelect2($arrDados['usuario']);
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceAcessoUsuarios = new \Acesso\Service\AcessoUsuarios($this->getEm());
        $servicePessoaFisica   = new \Pessoa\Service\PessoaFisica($this->getEm());
        $servicePessoaJuridica = new \Pessoa\Service\PessoaJuridica($this->getEm());

        if (is_array($arrDados['usuario']) && !$arrDados['usuario']['text']) {
            $arrDados['usuario'] = $arrDados['usuario']['id'];
        }

        /*if ($arrDados['usuario'] && is_string($arrDados['usuario'])) {
            $arrDados['usuario'] = $serviceAcessoUsuarios->getArrSelect2(array('id' => $arrDados['usuario']));
            $arrDados['usuario'] = $arrDados['usuario'] ? $arrDados['usuario'][0] : null;
        }*/

        if ($arrDados['usuario']) {
            $arrDados['acessoGrupo'] = $serviceAcessoUsuarios->getUserGroupArrSelect2($arrDados['usuario']);
        }

        if (is_array($arrDados['pesFisica']) && !$arrDados['pesFisica']['text']) {
            $arrDados['pesFisica'] = $arrDados['pesFisica']['pesId'];
        }

        if ($arrDados['pesFisica'] && is_string($arrDados['pesFisica'])) {
            $arrDados['pesFisica'] = $servicePessoaFisica->getArrSelect2(array('id' => $arrDados['pesFisica']));
            $arrDados['pesFisica'] = $arrDados['pesFisica'] ? $arrDados['pesFisica'][0] : null;
        }

        if (is_array($arrDados['pesJuridica']) && !$arrDados['pesJuridica']['text']) {
            $arrDados['pesJuridica'] = $arrDados['pesJuridica']['pesId'];
        }

        /*if ($arrDados['pesJuridica'] && is_string($arrDados['pesJuridica'])) {
            $arrDados['pesJuridica'] = $servicePessoaJuridica->getArrSelect2(array('id' => $arrDados['pesJuridica']));
            $arrDados['pesJuridica'] = $arrDados['pesJuridica'] ? $arrDados['pesJuridica'][0] : null;
        }*/

        $arrDados['passwordCheck'] = '';
        $arrDados['senha']         = '';

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['id'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['login' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Acesso\Entity\AcessoPessoas */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getId();
            $arrEntity[$params['value']] = $objEntity->getLogin();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['id']) {
            $this->setLastError('Para remover um registro de pessoas é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objAcessoPessoas \Acesso\Entity\AcessoPessoas */
            $objAcessoPessoas = $this->getRepository()->find($param['id']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAcessoPessoas);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de pessoas.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        //$serviceAcessoUsuarios = new \Acesso\Service\AcessoUsuarios($this->getEm());
        $servicePessoaFisica   = new \Pessoa\Service\PessoaFisica($this->getEm());
        $servicePessoaJuridica = new \Pessoa\Service\PessoaJuridica($this->getEm());

        //$serviceAcessoUsuarios->setarDependenciasView($view);
        $servicePessoaFisica->setarDependenciasView($view);
        $servicePessoaJuridica->setarDependenciasView($view);
    }

    /**
     * Método que adiciona pessoas já vinculando um usuário para a mesma
     * @param array $dados
     * @return array
     * @throws \Exception
     */
    public function adicionar(array $dados)
    {
        $this->begin();

        $dados['pesJuridica'] = (intval($dados['pesJuridica']) == 0 ? null : intval($dados['pesJuridica']));
        $dados['pesFisica']   = (intval($dados['pesFisica']) == 0 ? null : intval($dados['pesFisica']));

        if (!$dados['usuario']) {
            try {
                $entityUsuario    = $this->usuario->adicionar($dados);
                $dados['usuario'] = $entityUsuario->getId();
            } catch (\Exception $ex) {
                throw new \Exception($ex->getMessage(), $ex->getCode());
            }
        }

        try {
            $ret = parent::adicionar($dados);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), $ex->getCode());
        }
        $this->commit();

        return $ret;
    }

    /**
     * Edita informações do usuário e da pessoa
     * @param array $dados
     * @return array
     */
    public function edita(array $dados)
    {
        if ($dados['senha'] == "") {
            unset($dados['senha']);
        }
        if (!$dados["pesFisica"]) {
            $dados["pesFisica"] = null;
        }
        if (!$dados["pesJuridica"]) {
            $dados["pesJuridica"] = null;
        }

        return parent::edita($dados);
    }

    /**
     * Verifica se o cpf que está sendo usado para criar o usuário já tem um usuário usando o mesmo
     */
    public function validaCpfUsuario($cpf)
    {
        $sql = "SELECT * FROM pessoa_fisica pf INNER JOIN acesso_pessoas ap ON ap.pes_fisica = pf.pes_id WHERE pf.pes_cpf = '$cpf' ";
        if (parent::executeQuery($sql)->rowCount() > 0) {
            return false;
        }

        return true;
    }

    /**
     * @param $login
     * @return bool
     */
    public function validaLogin($login, $usuarioId = null)
    {
        $existeLogin = $this->getRepository()->findOneByLogin($login);

        if ($existeLogin && !$usuarioId) {
            $this->setLastError("Login $login já cadastrado no sistema!");

            return false;
            //para caso de edição, se existir login tem que ser do msm usuário cadastrado no banco
        } elseif ($existeLogin && $existeLogin->getId() != $usuarioId) {
            $this->setLastError("Login $login já cadastrado no sistema para outro usuário!");

            return false;
        }

        return true;
    }

    /**
     * @param int        $page
     * @param bool|false $is_json
     * @return array
     */
    public function pagination($page = 0, $is_json = false)
    {
        $dados = array();
        //        $dados['count'] = $this->executeQuery("SELECT COUNT(*) as row  FROM con__contrato")->fetch()['row'];
        $sql = "SELECT
        acesso_pessoas.id AS id, login, email, pes_nome
        FROM
            acesso_pessoas
        LEFT JOIN
            pessoa_fisica ON pessoa_fisica.pes_id = acesso_pessoas.pes_fisica
        LEFT JOIN
            pessoa USING (pes_id)";

        $dados['dados'] = $this->executeQuery($sql);
        $dados['count'] = count($dados['dados']);
        $dados['dados'] = $this->paginationFormatJson($dados['dados']);

        return $dados;
    }

    /**
     * Executa a query e retorna os dados montando um autocomplete
     * @param array $pesquisa
     * @return array
     */
    public function pesquisaUsuarioJson($pesquisa)
    {
        if (is_numeric($pesquisa)) {
            $sql = "SELECT pes_nome,pes_id FROM pessoa
                LEFT JOIN pessoa_fisica USING(pes_id)
                LEFT OUTER JOIN acesso_pessoas ON pes_fisica = pes_id
                WHERE pes_cpf LIKE '" . $pesquisa . "%' AND pes_tipo='Fisica' AND pes_fisica IS NULL";
        } else {
            $sql = "SELECT pes_nome,pes_id FROM pessoa
                LEFT JOIN pessoa_fisica USING(pes_id)
                LEFT OUTER JOIN acesso_pessoas ON pes_fisica = pes_id
                WHERE pes_nome LIKE '" . $pesquisa . "%' AND pes_tipo='Fisica' AND pes_fisica IS NULL ";
        }

        $result = array();
        $array  = $this->executeQuery($sql)->fetchAll();
        foreach ($array as $item) {
            $result[] = array('value' => $item['pes_nome'], 'data' => intval($item['pes_id']));
        }

        return $result;
    }

    /**
     * @param $login
     * @return int
     */
    public function checkLogin($login)
    {
        $query  = "SELECT count(id) AS cont FROM acesso_pessoas WHERE email='" . addslashes($login) . "'";
        $result = $this->executeQuery($query)->fetchAll();
        if (intval($result[0]['cont']) > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * @param $email
     * @return array
     */
    public function checkEmail($email)
    {
        $em    = $this->getEm();
        $query = $em->createQuery("SELECT ac FROM Acesso\Entity\AcessoPessoas ac WHERE ac.email = :email");
        $query->setParameter('email', $email);

        $result = $query->getResult();

        return $result;
    }

    /**
     * @param $pes_id
     * @return mixed
     */
    public function buscaEmail($pes_id)
    {
        $pes_id = intval($pes_id);
        $query  = "SELECT con_contato AS email FROM contato
                  WHERE tipo_contato = 1 AND pessoa = " . addslashes($pes_id) . " ";
        $result = $this->executeQuery($query)->fetchAll();

        return $result[0]['email'];
    }

    /**
     * @param $pes_id
     * @return array
     */
    public function buscaUsuario($pes_id)
    {
        $pes_id = intval($pes_id);
        $query  = "SELECT id,login,email,usuario FROM acesso_pessoas
                  WHERE coalesce(pes_fisica, pes_juridica) =" . addslashes($pes_id) . " ";
        $result = array();
        $array  = $this->executeQuery($query)->fetchAll();

        foreach ($array as $item) {
            $result[] = array(
                'id'      => intval($item['id']),
                'login'   => $item['login'],
                'email'   => $item['email'],
                'usuario' => $item['usuario'],
            );
        }

        return $result;
    }

    /**
     * @param $dados
     * @return Object
     * @throws \Exception
     */
    public function editaSenha($dados)
    {
        if ($dados['pesFisica']) {
            $dados['pesFisica'] = $this->getRepository('Pessoa\Entity\PessoaFisica')->findOneBy(
                ['pes' => $dados['pesFisica']]
            );
        } else {
            $dados['pesJuridica'] = $this->getRepository('Pessoa\Entity\PessoaJuridica')->findOneBy(
                ['pes' => $dados['pesJuridica']]
            );
        }

        $dados['usuario'] = $this->getRepository('Acesso\Entity\AcessoUsuarios')->findOneBy(
            ['id' => $dados['usuario']]
        );

        if (!$this->validaLogin($dados['login'], $dados['id'])) {
            return false;
        }

        $entity = $this->getReference($dados['id']);
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($dados, $entity);

        try {
            $this->getEm()->persist($entity);
            $this->getEm()->flush();

            return $entity;
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
        }
    }

    /**
     * @param int        $tamanho
     * @param bool|true  $maiusculas
     * @param bool|true  $numeros
     * @param bool|false $simbolos
     * @return string
     */
    function geraSenha($tamanho = 8, $maiusculas = true, $numeros = true, $simbolos = false)
    {
        // Caracteres de cada tipo
        $lmin = 'abcdefghijklmnopqrstuvwxyz';
        $lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $num  = '1234567890';
        $simb = '!@#$%*-';
        // Variáveis internas
        $retorno    = '';
        $caracteres = '';
        // Agrupamos todos os caracteres que poderão ser utilizados
        $caracteres .= $lmin;

        if ($maiusculas) {
            $caracteres .= $lmai;
        }
        if ($numeros) {
            $caracteres .= $num;
        }
        if ($simbolos) {
            $caracteres .= $simb;
        }

        // Calculamos o total de caracteres possíveis
        $len = strlen($caracteres);

        for ($n = 1; $n <= $tamanho; $n++) {
            // Criamos um número aleatório de 1 até $len para pegar um dos caracteres
            $rand = mt_rand(1, $len);
            // Concatenamos um dos caracteres na variável $retorno
            $retorno .= $caracteres[$rand - 1];
        }

        return $retorno;
    }

    /**
     * Método responsável por permitir recuperar senha
     * @params:
     *  array $dados  - com os dados submetidos do usuario
     *  array $configMensage - configuracao de mensagem de erro
     * @return array com mensagem de erro ou sucess
     */

    public function recuperarSenha(array $dados, array $configEmail)
    {
        $query = $this->getEm()->createQuery(
            'SELECT ac
          FROM \Acesso\Entity\AcessoPessoas ac
          JOIN \Acesso\Entity\AcessoUsuarios au
          WHERE
               ac.usuario=au.id AND
               au.usrStatus=:usrStatus AND
              (ac.email = :email OR ac.login = :login)
          '
        );

        $entity = $query->setParameters(
            array(
                'login'     => $dados['recovery'],
                'email'     => $dados['recovery'],
                'usrStatus' => "Ativo"
            )
        )->getResult();

        if (count($entity) > 1) {
            $logins = [];

            foreach ($entity as $e) {
                $logins[] = $e->getLogin();
            }

            return array(
                'type'    => 'warning',
                'message' => 'Existe mais de um usuário vinculado a este email.'
            );
        }

        $entity = $entity[0];

        if (!$entity) {
            return array('type' => 'warning', 'message' => 'Usuário não encontrado');
        }

        $usuario['email'] = $entity->getEmail();
        $usuario['login'] = $entity->getLogin();

        if ($entity->getPesFisica()) {
            $usuario['pes']     = $entity->getPesFisica()->getPes()->getPesId();
            $dados['pesFisica'] = $entity->getPesFisica()->getPes()->getPesId();
        } elseif ($entity->getPesJuridica()) {
            $usuario['pes']       = $entity->getPesJuridica()->getPes()->getPesId();
            $dados['pesJuridica'] = $entity->getPesJuridica()->getPes()->getPesId();
        } else {
            $usuario['pes']       = null;
            $dados['pesFisica']   = null;
            $dados['pesJuridica'] = null;
        }

        if (!$usuario['email']) {
            $contatoPessoa = $this->getRepository('Pessoa\Entity\Contato')->buscaContatos($usuario['pes']);

            if ($contatoPessoa['E-mail']) {
                $usuario['email'] = $contatoPessoa['E-mail'][0]->getConContato();
            } else {
                return array('type' => 'warning', 'message' => 'Usuário não possui e-mail vinculado.');
            }
        }

        $dados['login']    = $usuario['login'];
        $dados['email']    = $usuario['email'];
        $dados['senhaRaw'] = $this->geraSenha();
        $dados['senha']    = sha1($dados['senhaRaw']);
        $dados['usuario']  = $entity->getId();
        $dados['id']       = $entity->getId();

        $entity->setSenha($dados['senha']);

        $this->getEm()->persist($entity);
        $this->getEm()->flush($entity);

        $configEmail['subject'] = "Acesso - " . $configEmail['projectName'];
        $configEmail['to']      = $dados['email'];
        $message                = "" .
            "<p>Seus dados de acesso são:</p>" .
            "<p>Usuário: " . $dados['login'] . " </p>" .
            "<p>Senha: " . $dados['senhaRaw'] . "</p>" .
            '<p>Para acessar clique <a href="' . $configEmail['linkRedirect'] . '">Aqui</a></p>';

        $retorno = $this->enviaEmail($message, $configEmail);

        if ($retorno) {
            return array(
                'type'    => 'success',
                'message' => 'Senha alterada! Novos dados de acesso enviados para o email'
            );
        }

        return array(
            'type'    => 'error',
            'message' => 'Não foi possível redefinir sua senha, tente novamente, caso persista entre contato com o suporte'
        );
    }

    /**
     * @param       $dados
     * @param array $configEmail
     * @return array
     */
    public function trocaSenha($dados, array $configEmail)
    {
        $entity = $this->getRepository($this->getEntity())->findOneBy(['id' => $dados['usuId']]);

        if (!$entity) {
            return array('type' => 'warning', 'message' => 'Usuário não encontrado');
        }

        if ($entity->getSenha() == sha1($dados['senhaAtual'])) {
            $dados['email']     = $entity->getEmail();
            $dados['senha']     = sha1($dados['novaSenha']);
            $dados['id']        = $dados['usuId'];
            $dados['login']     = $entity->getLogin();
            $dados['usuario']   = $entity->getUsuario()->getId();
            $dados['pesFisica'] = $entity->getPesFisica()->getPes()->getPesId();

            if (!$dados['email']) {
                $contatoPessoa  = $this->getRepository('Pessoa\Entity\Contato')->buscaContatos(
                    $entity->getPesFisica()->getPes()->getPesId()
                );
                $dados['email'] = $contatoPessoa['E-mail'][0]->getConContato();
            }

            //verifica se existe login para outro usuário
            if (!$this->validaLogin($dados['login'], $dados['id'])) {
                return false;
            }

            try {
                $user = $this->edita($dados);
            } catch (\Exception $ex) {
                return array(
                    'type'    => 'error',
                    'message' => 'Erro ao redefinir senha, caso persista contate o suporte'
                );
            }

            $configEmail['subject'] = "Inscrição - " . $configEmail['projectName'];
            $configEmail['to']      = $dados['email'];
            $message                = "" .
                "<p>Seus dados de acesso são:</p>" .
                "<p>Usuário: " . $dados['login'] . " </p>" .
                "<p>Senha: " . $dados['novaSenha'] . "</p>" .
                '<p>Para acessar clique <a href="' . $configEmail['linkRedirect'] . '">Aqui</a></p>';

            $retorno = $this->enviaEmail($message, $configEmail);

            if ($retorno) {
                return array(
                    'type'    => 'success',
                    'message' => 'Senha alterada! Novos dados de acesso enviados para o email'
                );
            }

            return array(
                'type'    => 'error',
                'message' => 'Não foi possível redefinir sua senha, tente novamente, caso persista entre contato com o suporte'
            );
        }

        return array(
            'type'    => 'warning',
            'message' => 'Senha atual informada equivale a senha do usuário'
        );
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getDescricao($id)
    {
        if (!$id) {
            return false;
        }

        if (!is_array($id)) {
            $id = array($id);
        }

        $id = implode($id, ',');

        $sql = "
        SELECT
            group_concat(pes_nome SEPARATOR ', ') AS usuario
        FROM acesso_pessoas
        JOIN pessoa ON pes_id=COALESCE(pes_fisica, pes_juridica)
          WHERE id IN(" . $id . ")";

        $result = $this->executeQueryWithParam($sql)->fetch();

        return $result['usuario'];
    }

    /**
     * @param null $usuarioFallback
     * @return \Acesso\Entity\AcessoPessoas|null
     */
    public function retornaUsuarioLogado($usuarioFallback = null)
    {
        if (!$usuarioFallback) {
            $session         = new \Zend\Authentication\Storage\Session();
            $arrUsuario      = $session->read();
            $usuarioFallback = $arrUsuario['id'];
        }

        $usuarioAutor = null;

        try {
            /* @var $usuarioAutor \Acesso\Entity\AcessoPessoas */
            $usuarioAutor = $this->getRepository()->findOneBy(['id' => $usuarioFallback]);
        } catch (\Exception $e) {
            $this->setLastError('Falha ao pesquisar usuário!' . $e->getMessage());
        }

        if (!$usuarioAutor) {
            $this->setLastError('Usuário não encontrado!');
        }

        return $usuarioAutor;
    }

    /**
     * @param \Pessoa\Entity\Pessoa $objPessoa
     * @return bool
     */
    public function inativaUsuarioPessoa(\Pessoa\Entity\Pessoa $objPessoa)
    {
        try {
            $this->getEm()->beginTransaction();
            $arrUsuariosPessoaFisica   = $this->getRepository()->findBy(['pesFisica' => $objPessoa]);
            $arrUsuariosPessoaJuridica = $this->getRepository()->findBy(['pesJuridica' => $objPessoa]);

            $arrUsuariosPessoa = array_merge($arrUsuariosPessoaFisica, $arrUsuariosPessoaJuridica);

            foreach ($arrUsuariosPessoa as $objAcessoPessoas) {
                /* @var $objAcessoPessoas \Acesso\Entity\AcessoPessoas */
                $objAcessoPessoas->getUsuario()->setUsrStatus("Inativo");
                $this->getEm()->persist($objAcessoPessoas->getUsuario());
                $this->getEm()->flush($objAcessoPessoas->getUsuario());
            }

            $this->getEm()->commit();
        } catch (\Exception $e) {
            $this->setLastError('Falha ao inativar usuário(s) de pessoa!' . $e->getMessage());

            return false;
        }

        return true;
    }

    public function registrarUsuarioPadraoEVinculaGrupo($pesId, $grupo)
    {
        $grupo  = trim(preg_replace('/[^0-9,]/', '', $grupo), ',');
        $grupos = explode(',', $grupo);

        if (!$grupos) {
            return false;
        }

        //Momento em que o vestibulando é cadastrado como um usuário no sistema.
        try {
            $servicePessoa = new \Pessoa\Service\Pessoa($this->getEm());
            $serviceAcesso = new \Acesso\Service\AcessoUsuarios($this->getEm());
            $arrDados      = $servicePessoa->getArray($pesId);

            $login       = '';
            $senha       = '';
            $pesFisica   = '';
            $pesJuridica = '';

            if ($arrDados['pesTipo'] == $servicePessoa::PES_TIPO_JURIDICA) {
                $login       = preg_replace('/[^0-9]/', '', $arrDados['pesCnpj']);
                $pesJuridica = $pesId;
            } else {
                $login     = preg_replace('/[^0-9]/', '', $arrDados['pesCpf']);
                $pesFisica = $pesId;
            }

            $senha = substr($login, 0, 5);

            /** @var \Acesso\Entity\AcessoPessoas $acessoUser */
            $acessoUser = $this->getRepository()->findOneBy(['login' => $login]);

            if (!$acessoUser) {
                $arrDadosUsr = [
                    'pesFisica'     => $pesFisica,
                    'pesJuridica'   => $pesJuridica,
                    'login'         => $login,
                    'senha'         => $senha,
                    'passwordCheck' => $senha,
                    'usrStatus'     => 'Ativo',
                    'usuario'       => '',
                    'email'         => $arrDados['conContatoEmail'],
                    'acessoGrupo'   => $grupos,
                ];
            } else {
                $arrDadosUsr = $acessoUser->toArray();
                unset($arrDadosUsr['senha']);
                $arrDadosUsr['email']       = $arrDados['conContatoEmail'];
                $arrDadosUsr['acessoGrupo'] = array_merge(
                    $serviceAcesso->buscaGruposUsr($arrDadosUsr['usuario']),
                    $grupos
                );
            }

            $arrDadosUsr['acessoGrupo'] = implode(',', $arrDadosUsr['acessoGrupo']);

            return $this->save($arrDadosUsr);
        } catch (\Exception $ex) {
        }

        return false;
    }

    /**
     * Método que busca permissoes por privilegios
     * @param string $controller
     * @param string $action
     * @return boolean
     */
    public function verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade($controller, $action = null)
    {
        /** @var \Acesso\Entity\AcessoPessoas $objAcessoPessoa */
        $objAcessoPessoa = $this->retornaUsuarioLogado();

        if (!$objAcessoPessoa) {
            return false;
        }

        $controller  = str_replace('\\', '\\\\', $controller);
        $usuarioId   = $objAcessoPessoa->getUsuario()->getId();
        $param       = array('controller' => $controller, 'usuario' => $usuarioId);
        $chaveSessao = ('Usuario: ' . $usuarioId . ' / Controller: ' . $controller . ' / Action: ' . $action);
        $chaveSessao = md5($chaveSessao);

        $session = new \Zend\Session\Container('permissoes');

        if ($session->offsetExists($chaveSessao)) {
            return $session->offsetGet($chaveSessao);
        }

        $sql = "
        SELECT COUNT(*) numero_privilegios
        FROM
        (
            SELECT usuario.id usuario_id, funcionalidade.func_controller controller, acao.act_nome AS acao
            FROM acesso_usuarios AS usuario
            INNER JOIN acesso_usuarios_grupos AS grupo ON grupo.usuario_id=usuario.id
            INNER JOIN acesso_privilegios_grupo AS privilegio_grupo ON privilegio_grupo.acesso_grupo_id = grupo.grupo_id
            INNER JOIN acesso_actions AS acao ON acao.id = privilegio_grupo.acesso_actions_id
            INNER JOIN acesso_funcionalidades AS funcionalidade ON funcionalidade.id = acao.funcionalidade
            WHERE funcionalidade.func_controller LIKE :controller AND usuario.id = :usuario
            -- condicao action --
            UNION ALL
            SELECT usuario.id usuario_id, funcionalidade.func_controller controller, acao.act_nome AS acao
            FROM acesso_usuarios AS usuario
            INNER JOIN acesso_usuarios_privilegios AS privilegio ON privilegio.acesso_usuarios_id = usuario.id
            INNER JOIN acesso_actions AS acao ON acao.id = privilegio.acesso_actions_id
            INNER JOIN acesso_funcionalidades AS funcionalidade ON funcionalidade.id = acao.funcionalidade
            WHERE funcionalidade.func_controller LIKE :controller AND usuario.id = :usuario
            -- condicao action --
        ) AS permissao";

        if ($action) {
            $sql             = str_replace('-- condicao action --', ' AND acao.act_nome LIKE :action', $sql);
            $param['action'] = $action;
        }

        $numeroPrivilegios = $this->executeQueryWithParam($sql, $param)->fetch()['numero_privilegios'];
        $permissao         = ($numeroPrivilegios > 0);

        $session->offsetSet($chaveSessao, $permissao);
        $session->setExpirationSeconds(60 * 60, $chaveSessao);

        return $permissao;
    }

    public function retornaCampusCursoDoUsuarioLogado($somenteId = false, $src = null, $usuarioFallback = null)
    {
        $usuarioLogado  = $this->retornaUsuarioLogado($usuarioFallback);
        $arrCampusCurso = array();

        if (!$src) {
            preg_match("/[^\\/]+$/", $_SERVER['HTTP_REFERER'], $src);
            $src = current($src);
        }

        if (
            in_array(get_class($src), $this->getArrAllowedSources()) ||
            in_array($src, $this->getArrAllowedSources()) ||
            !$usuarioLogado
        ) {
            return false;
        }

        $arrCampusCursoCollection = $usuarioLogado->getCursoCampus();

        /** @var \Matricula\Entity\CampusCurso $campusCurso */
        foreach ($arrCampusCursoCollection as $campusCurso) {
            if ($somenteId) {
                $arrCampusCurso[] = $campusCurso->getCursocampusId();
            } else {
                $arrCampusCurso[] = $campusCurso;
            }
        }

        return $arrCampusCurso;
    }

    public function validaPessoaEditaAgenteAluno($objUsuario = false)
    {
        return $this->validaAcessoPessoaConfig('GRUPO_EDITA_AGENTE_EDUCACIONAL_ALUNO', $objUsuario);
    }

    public function validaPessoaEditaPessoaIndicacaoAluno($objUsuario = null)
    {
        return $this->validaAcessoPessoaConfig('GRUPO_EDITA_INDICACAO_ALUNO', $objUsuario);
    }

    public function validaPessoaEditaObservacaoDocumentoAluno($objUsuario = null)
    {
        return $this->validaAcessoPessoaConfig('GRUPO_EDITA_OBSERVACAO_DOCUMENTO_ALUNO', $objUsuario);
    }

    private function validaAcessoPessoaConfig($chave, $objUsuario = null)
    {
        $arrGrupoPermitido = trim($this->getConfig()->localizarChave($chave));

        if (!$arrGrupoPermitido) {
            return true;
        }

        $serviceAcessoPessoa = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceAcessoGrupo  = new \Acesso\Service\AcessoGrupo($this->getEm());

        if (!$objUsuario || !($objUsuario instanceof \Acesso\Entity\AcessoPessoas)) {
            /** @var $objUsuario \Acesso\Entity\AcessoPessoas */
            $objUsuario = $serviceAcessoPessoa->retornaUsuarioLogado(null);
        }

        $arrGrupoPermitido = $arrGrupoPermitido ? explode(',', $arrGrupoPermitido) : [];
        $arrGrupoPessoa    = $serviceAcessoGrupo->buscaGrupoUsr($objUsuario->getUsuario()->getId());

        $arrGruposComuns = array_intersect($arrGrupoPermitido, $arrGrupoPessoa);

        return ($arrGrupoPessoa && $arrGrupoPermitido && $arrGruposComuns);
    }
}
