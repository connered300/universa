<?php

namespace Acesso\Service;

/**
 * Description of AcessoUsuarios
 * Classe que prove os serviços de manipulação de usuário
 * @author joao
 */
class AcessoUsuarios extends \VersaSpine\Service\AbstractService
{
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, "Acesso\Entity\AcessoUsuarios");
        $this->setLabelEntity("usr_status");
    }

    protected function pesquisaForJson($criterios)
    {
    }

    protected function valida($dados)
    {
    }

    public function getUserGroupArrSelect2($param = array())
    {
        $sql = "
        SELECT
            grup_nome as text,
            grupo_id as id
        FROM
            acesso_usuarios_grupos aug
            LEFT JOIN acesso_grupo ag on ag.id = aug.grupo_id
        WHERE
            aug.usuario_id = :id
        ";

        $result = $this->executeQueryWithParam($sql, ['id' => $param])->fetchAll();

        return $result;
    }

    public function getRotaDefault($usuarioId)
    {
        $sql = "
        SELECT
          af.func_controller as controller,
          aa.act_nome AS action,
          am.mod_route as route
        FROM acesso_funcionalidades af INNER JOIN acesso_modulos am ON af.modulo = am.id
          INNER JOIN acesso_actions aa ON af.id = aa.funcionalidade
          INNER JOIN acesso_privilegios_grupo apg ON apg.acesso_actions_id = aa.id
          INNER JOIN acesso_usuarios_grupos aug ON aug.grupo_id = apg.acesso_grupo_id
        WHERE aug.usuario_id = :usuario_id
        ORDER BY if(aa.act_nome='index', 0, 1), am.mod_prioridade ASC";

        $result = $this->executeQueryWithParam($sql, ['usuario_id' => $usuarioId])->fetch();

        if ($result) {
            $spinePsr0 = new \VersaSpine\ViewHelper\Psr0();

            $controller = $result['controller'];
            $controller = explode('\\', $controller);
            $controller = array_pop($controller);
            $controller = $spinePsr0->strToPsr0($controller);

            $result['controller'] = $controller;
        }

        return $result;
    }

    /**
     * Busca permissoes por grupo
     * @param int     $usuario
     * @param string  $controller
     * @param string  $action
     * @param boolean $returnActions
     * @return boolean
     */
    public function permissaoPorGrupo($usuario, $controller, $action, $returnActions = false)
    {
        if (!$returnActions) {
            $sql = "SELECT COUNT(*) count "
                . "FROM  acesso_funcionalidades af "
                . "INNER JOIN acesso_actions aa ON af.id = aa.funcionalidade "
                . "INNER JOIN acesso_privilegios_grupo apg ON apg.acesso_actions_id = aa.id "
                . "INNER JOIN acesso_usuarios_grupos aug ON aug.grupo_id = apg.acesso_grupo_id "
                . "WHERE af.func_controller LIKE '" . str_replace(
                    "\\",
                    "\\\\\\\\",
                    $controller
                ) . "' AND aa.act_nome LIKE '$action' AND aug.usuario_id = '$usuario' ";

            return $this->executeQuery($sql)->fetch()['count'] > 0;
        }

        $sql = "SELECT af.id func_id, af.func_nome, af.func_controller, am.id mod_id, am.mod_nome, am.mod_route, am.mod_redireciona, am.mod_prioridade"
            . " FROM  acesso_funcionalidades af"
            . " INNER JOIN acesso_modulos am ON af.modulo = am.id"
            . " INNER JOIN acesso_actions aa ON af.id = aa.funcionalidade"
            . " INNER JOIN acesso_privilegios_grupo apg ON apg.acesso_actions_id = aa.id"
            . " INNER JOIN acesso_usuarios_grupos aug ON aug.grupo_id = apg.acesso_grupo_id"
            . " WHERE aug.usuario_id = '$usuario'";

        return $this->executeQuery($sql)->fetchAll();
    }

    /**
     * Método de busca pelos grupos do usuário
     * @param type $usuario
     * @return array
     */
    public function buscaGruposUsr($usuario)
    {
        $array  = $this->executeQuery(
            "SELECT DISTINCT grupo_id FROM acesso_usuarios_grupos WHERE usuario_id = '$usuario' "
        )->fetchAll();
        $grupos = array();

        foreach ($array as $grupo) {
            $grupos[] = (int)$grupo['grupo_id'];
        }

        return $grupos;
    }

    /**
     * Método que busca os privilegios do usuário
     * @param array $usuario
     * @return array
     */
    public function buscaPrivilegiosUsr($usuario)
    {
        $array  = $this->executeQuery(
            "SELECT DISTINCT acesso_actions_id FROM acesso_usuarios_privilegios WHERE acesso_usuarios_id = '$usuario' "
        )->fetchAll();
        $grupos = array();
        foreach ($array as $grupo) {
            $grupos[] = $grupo['acesso_actions_id'];
        }

        return $grupos;
    }

    /**
     * Método que busca permissoes por privilegios
     * @param int    $usuario
     * @param string $controller
     * @param string $action
     * @return boolean
     */
    public function permissaoPorPrivilegios($usuario, $controller, $action)
    {
        $sql   = "SELECT COUNT(*) count "
            . "FROM "
            . "acesso_funcionalidades af "
            . "INNER JOIN acesso_actions aa ON af.id = aa.funcionalidade "
            . "INNER JOIN acesso_usuarios_privilegios aup ON aup.acesso_actions_id = aa.id "
            . "WHERE af.func_controller LIKE '" . str_replace(
                "\\",
                "\\\\\\\\",
                $controller
            ) . "' AND aa.act_nome LIKE '$action' AND aup.acesso_usuarios_id = '$usuario' ";
        $count = $this->executeQuery($sql)->fetch()['count'];
        if ($count > 0) {
            return true;
        }

        return false;
    }

    /**
     * Busca as actions filhas
     * @param string $action
     * @return boolean
     */
    public function actionChildren($action)
    {

        //TODO: Método precisa ser revisado, esta verificando se a action depende de outra, desconsiderando as permissões
        $sql   = "SELECT COUNT(*) count FROM acesso_actions aa INNER JOIN acesso_actions_filhas aaf ON aaf . filha = aa . id WHERE aa . act_nome LIKE '$action' ";
        $count = $this->executeQuery($sql)->fetch()['count'];
        if ($count > 0) {
            return true;
        }

        return false;
    }

    /**
     * Vincula Usuários ao Grupos
     * @param array $users
     * @param       $grupos
     * @return boolean
     */
    public function vinculaGrupoUser(array $users, $recursive = true)
    {
        parent::begin();

        $date_inicio = (new \DateTime('now'))->format("Y-m-d");
        $sql         = "INSERT INTO acesso_usuarios_grupos(usuario_id, grupo_id, us_data_inicio, us_ativo, us_data_fim) VALUES  ";

        if ($recursive) {
            foreach ($users as $user) {
                $date_fim = (array_key_exists('date_fim', $user)) ? $user['date_fim'] : 'NULL';
                $sql .= "({$user['usuario']},{$user['grupo']}, '$date_inicio', '{$user['ativo']}', $date_fim) ,";
            }
            $sql = substr($sql, 0, -1);
        } else {
            $date_fim = (array_key_exists('date_fim', $users)) ? $users['date_fim'] : 'NULL';
            $sql .= "({$users['usuario']},{$users['grupo']}, '$date_inicio', '{$users['ativo']}', $date_fim)";
        }

        parent::commit();
        if ($this->exec($sql)) {
            return true;
        }

        return false;
    }

    public function buscaPessoaSemUsuarioPorTipo($tipo)
    {
        $table  = "";
        $SELECT = "";
        $INNER  = "";
        if ($tipo == 'Professores') {
            $table = 'acadgeral__docente';
        }
        if ($tipo == 'Aluno') {
            $SELECT = 'acadgeral__aluno_curso.alunocurso_id as alunocursoId, ';
            $INNER  = 'INNER JOIN acadgeral__aluno_curso ON acadgeral__aluno_curso.aluno_id = acadgeral__aluno.aluno_id';
            $table  = 'acadgeral__aluno';
        }

        $query = "
          SELECT
            $SELECT
            pessoa . pes_id as pes,
            pes_nome as pesNome,
            pes_cpf as pesCpf,
            email . con_contato as email
          FROM
            $table
          $INNER
          LEFT JOIN
            acesso_pessoas ON {
                $table}.pes_id = acesso_pessoas . pes_fisica
          INNER JOIN
            pessoa_fisica ON pessoa_fisica . pes_id = {
                $table}.pes_id
          INNER JOIN
            pessoa ON pessoa . pes_id = {
                $table}.pes_id
          LEFT JOIN
            contato email ON email . pessoa = pessoa . pes_id AND email . tipo_contato = 1
          WHERE
            acesso_pessoas . pes_fisica IS null
        ";

        $result = $this->executeQuery($query);
        if ($result->rowCount() > 0) {
            return $result->fetchAll();
        }

        return null;
    }

    public function formataDadosDaPessoaParaCriarUsuarioDeAcordoComtipo(array $data, $tipo)
    {
        if ($tipo == 'Professores') {
            $userName         = explode(' ', $data['pesNome']);
            $data['username'] = strtolower(
                preg_replace(
                    '/[`^~\'"]/',
                    null,
                    iconv('UTF - 8', 'ASCII//TRANSLIT', $userName[0] . '.' . end($userName))
                )
            );
            $data['password'] = substr(str_replace('.', '', $data['pesCpf']), 0, 5);
        }
        if ($tipo == 'Aluno') {
            $data['username'] = ltrim($data['alunocursoId'], '0');
            $data['password'] = substr(str_replace('.', '', $data['pesCpf']), 0, 5);
        }

        return $data;
    }

    public function addUsuario(array $data, $tipo)
    {
        try {
            $acesso     = new \Acesso\Service\AcessoPessoas($this->getEm());
            $acessoUser = $acesso->getRepository($acesso->getEntity())->findOneBy(['login' => $data['username']]);
            if (!$acessoUser) {
                $serviceAcesso = new \Acesso\Service\AcessoUsuarios($this->getEm());
                $datetime      = (new \DateTime('now'));
                $this->begin();
                $acessoUsuario = $serviceAcesso->adicionar(
                    [
                        'id'                   => '',
                        'acessoDataRegistro'   => $datetime,
                        'acessoDataSenhaSalva' => $datetime,
                        'usrStatus'            => 'Ativo'
                    ]
                );

                $usuario = [
                    'pesFisica' => $data['pes'],
                    'login'     => $data['username'],
                    'senha'     => $data['password'],
                    'usrStatus' => 'Ativo',
                    'usuario'   => $acessoUsuario->getId(),
                    'email'     => $data['email']
                ];
                $adUser  = $acesso->adicionar($usuario);

                $grupo    = $this->getEm()->getRepository('Acesso\Entity\AcessoGrupo')->findOneBy(
                    array('grupNome' => $tipo)
                );
                $usuGrupo = [
                    'ativo'   => 'Sim',
                    'usuario' => $acessoUsuario->getId(),
                    'grupo'   => $grupo->getId(),
                ];
                $usuGrupo = $serviceAcesso->vinculaGrupoUser($usuGrupo, false);

                //                if($data['contEmail']){
                //                    $this->enviaDadosCadastro($data['email'], $data['username'], $data['password']);
                //                }
                $this->commit();

                return $usuario;
            }
        } catch (\Exception $ex) {
            throw new \Exception('Erro ao Cadastrar o usuario' . $ex->getMessage());
        }
    }

    public function addUsuarios(array $pessoas, $tipo)
    {
        $this->begin();
        foreach ($pessoas as $pessoa) {
            $pessoa = $this->formataDadosDaPessoaParaCriarUsuarioDeAcordoComtipo($pessoa, $tipo);
            $user   = $this->addUsuario($pessoa, $tipo);
        }

        $this->commit();
    }
}
