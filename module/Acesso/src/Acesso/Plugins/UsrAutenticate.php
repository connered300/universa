<?php

namespace Acesso\Plugins;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

/**
 * Description of UsrAutenticate
 * Plugin que prove as informações do usuário logado no sistema
 * @author jp
 */
class UsrAutenticate extends AbstractPlugin
{
    /**
     * Construtor
     */
    public function __construct()
    {
        ;
    }

    /**
     * Retorna o id do usuário logado
     * @return boolean | int
     */
    public function getUsr()
    {
        if (isset($_SESSION['Zend_Auth']['storage']['usuario'])) {
            return $_SESSION['Zend_Auth']['storage']['usuario'];
        }

        return false;
    }

}
