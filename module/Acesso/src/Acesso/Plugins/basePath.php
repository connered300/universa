<?php
namespace Acesso\Plugins;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class basePath extends AbstractPlugin
{
    private $event;

    /**
     * @return mixed
     */
    public function getEvent()
    {
        if (!$this->event) {
            throw new Exception('Falta definir o Evento!');
        }

        return $this->event;
    }

    /**
     * @param mixed $myEvent
     */
    public function setEvent($myEvent)
    {
        $this->event = $myEvent;
    }

    public function basePath($route)
    {
        $objEvent             = $this->getEvent();
        $objServiceManager    = $objEvent->getApplication()->getServiceManager();
        $objRendererInterface = $objServiceManager->get('Zend\View\Renderer\RendererInterface');
        $url                  = $objRendererInterface->basePath($route);

        return $url;
    }
}
