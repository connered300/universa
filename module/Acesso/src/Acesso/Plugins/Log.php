<?php
namespace Acesso\Plugins;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

/**
 * Description of Log
 * Plugin que prove acesso aos serviços de log do sistema
 * @author jp
 */
class Log extends AbstractPlugin
{
    /**
     * Método para realizar a escrita de log no banco de dados
     * @param int    $priority
     * @param string $msg
     * @throws \Exception
     */
    public function writeLog($priority, $msg)
    {
        $params                 = $this->getController()->params()->fromRoute();
        $array['logController'] = $params['controller'];
        $array['logAction']     = $params['action'];
        $array['usuario']       = $this->getController()->usrAuth()->getUsr();
        $array['logPriority']   = $priority;
        $array['logMsg']        = $msg;
        try {
            $this->getController()->services()->getService('Acesso\Service\AcessoLog')->adicionar($array);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

}
