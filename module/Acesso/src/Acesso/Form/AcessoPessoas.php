<?php

namespace Acesso\Form;

use Zend\Form\Form,
    Zend\Form\Element,
    Zend\Form\Element\Select;

class AcessoPessoas extends \VersaSpine\Form\SmartAdmin\AbstractForm
{
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name, $options);
        $this->setAttribute('method', 'post');
        $this->setAttribute("class", "smart-form");
        $this->setAttribute("data-validate", "yes");
        $this->setAttribute("enctype", "multipart/form-data");

        $id            = new Element\Hidden("id");
        $grupo         = new Element\Hidden("grupo[]");
        $acessoActions = new Element\Hidden("acessoActions[]");
        $this->add($id);
        $this->add($grupo);
        $this->add($acessoActions);

        $usuario = new Element\Hidden("usuario");
        $this->add($usuario);

        $pesquisa = new Element\Text("pesquisa");
        $pesquisa->setAttributes(
            array(
                'data-autocomplete-url' => '/acesso/acesso-pessoas/pesquisa-usuario-json',
                'col'                   => 4,
                'icon'                  => 'icon-prepend fa fa-user',
                'label'                 => 'Pesquisa por Usuário',
                'placeholder'           => 'Pesquisa por: CPF',
                'wrap'                  => true,
            )
        );
        $this->add($pesquisa);

        $pessoaFisica = new Element\Hidden("pesFisica");
        //        $pessoaFisica->setAttributes(
        //            array(
        //                'data-autocomplete-url' => '/acesso/acesso-pessoas/autocomplete-json',
        //                'col'                   => 4,
        //                'icon'                  => 'icon-prepend fa fa-user',
        //                'label'                 => 'Pessoa responsável pelo usuário',
        //                'placeholder'           => 'Digite um CPF',
        //                'wrap'                  => true,
        //            )
        //        );
        $this->add($pessoaFisica);

        $pessoaJuridica = new Element\Hidden("pesJuridica");
        //        $pessoaJuridica->setAttributes(
        //            array(
        //                'data-autocomplete-url' => '/acesso/acesso-pessoas/autocomplete-json',
        //                'col'                   => 4,
        //                'icon'                  => 'icon-prepend fa fa-user',
        //                'label'                 => 'Empresa responsável pelo usuário',
        //                'placeholder'           => 'Digite um CNPJ',
        //                'wrap'                  => true,
        //            )
        //        );
        $this->add($pessoaJuridica);

        $login = new Element\Text("login");
        $login->setAttributes(
            array(
                'col'         => 4,
                'label'       => 'Login de acesso',
                'placeholder' => 'Digite o login valido',
                'icon'        => 'icon-prepend fa fa-user',
                'wrap'        => true
            )
        );
        $this->add($login);

        $email = new Element\Text("email");
        $email->setAttributes(
            array(
                'col'         => 4,
                'label'       => 'Email para contato',
                'placeholder' => 'Digite o endereço de email',
                'icon'        => 'icon-prepend fa fa-envelope',
                'wrap'        => true
            )
        );
        $this->add($email);

        $senha = new Element\Password("senha");
        $senha->setAttributes(
            array(
                'col'         => 4,
                'label'       => 'Senha para acesso',
                'placeholder' => 'Digite sua senha',
                'icon'        => 'icon-prepend fa fa-unlock',
                'wrap'        => true,
                'form-search' => false
            )
        );
        $this->add($senha);

        $confirmeSenha = new Element\Password("confimeSenha");
        $confirmeSenha->setAttributes(
            array(
                'col'         => 4,
                'label'       => 'Confirme sua senha para acesso',
                'placeholder' => 'Confirme sua senha',
                'icon'        => 'icon-prepend fa fa-unlock',
                'wrap'        => true,
                'form-search' => false
            )
        );
        $this->add($confirmeSenha);

        $status = new \VersaSpine\Form\SmartAdmin\Elements\AlternateOptions("usrStatus");
        $status->setAttributes(
            array(
                'col'         => 2,
                'label'       => 'Deseja ativar este usuário a partir do cadastro',
                'toggle_type' => 'checkbox',
                'wrap'        => true,
                'form-search' => false,
            )
        );
        $status->setValueOptions(
            array(
                'Ativar' => array('on' => 'Sim', 'off' => 'Não', 'value' => 'Ativo')
            )
        );
        $this->add($status);

        $grupos = new Element\Button("grupos");
        $grupos->setLabel("Permissões por Grupos de Usuários");
        $grupos->setAttributes(
            array(
                'col'   => 2,
                'style' => 'width: 100%; height: 30px;',
                'addGrupos',
                'class' => 'btn btn-primary',
                'id'    => 'addGrupo'
            )
        );
        $this->add($grupos);

        $actions = new Element\Button("actions");
        $actions->setLabel("Premissões adicionais");
        $actions->setAttributes(
            array(
                'style' => 'width: 100%; height: 40px;',
                'col'   => 2,
                'class' => 'btn btn-primary',
                'id'    => 'addAction'
            )
        );
        $this->add($actions);

        $gruposSelect = new Select("gruposSelect[]");
        $gruposSelect->setAttributes(
            array(
                'col'   => 4,
                'label' => 'Grupos de Acesso',
                'id'    => 'gruposSelect'
            )
        );
        $this->add($gruposSelect);

        $submit = new Element\Submit("enviar");
        $submit->setAttributes(
            array(
                'value' => 'Salvar',
                'class' => 'pull-left btn btn-primary'
            )
        );
        $this->add($submit);

        $submit = new Element\Submit("cancelar");
        $submit->setAttributes(
            array(
                'value' => 'Cancelar',
                'class' => 'pull-left btn btn-primary'
            )
        );
        $this->add($submit);
    }
}
