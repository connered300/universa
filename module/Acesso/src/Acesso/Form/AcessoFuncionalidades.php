<?php

namespace Acesso\Form;

use Zend\Form\Form,
    Zend\Form\Element;

/**
 * Description of AcessoSistemas
 *
 * @author joao
 */
class AcessoFuncionalidades extends Form
{
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name, $options);
        $this->setAttribute("class", "smart-form");
        $this->setAttribute("data-validate", "yes");

        $id = new Element\Hidden("id");
        $this->add($id);

        $nome = new Element\Text("funcNome");
        $nome->setAttributes(
            array(
                'col'              => 4,
                'placeholder'      => 'Digite o nome do funcionalidade',
                'label'            => 'Nome',
                'icon'             => 'icon-prepend fa fa-user',
                //            'wrap' => true,
                'data-validations' => 'String'
            )
        );
        $this->add($nome);

        $controller = new Element\Text("funcController");
        $controller->setAttributes(
            array(
                'col'              => 4,
                'placeholder'      => 'Digite o nome do controlador',
                'label'            => 'Controlador',
                'icon'             => 'icon-prepend fa fa-user',
                'wrap'             => true,
                'data-validations' => 'Required'
            )
        );
        $this->add($controller);

        $sistema = new Element\Text("modulo");
        $sistema->setAttributes(
            array(
                'data-autocomplete-url' => '/acesso/acesso-modulos/autocomplete-json',
                'col'                   => 4,
                'icon'                  => 'icon-prepend fa fa-user',
                'placeholder'           => 'Digite o nome do sistema',
                'label'                 => 'Digite o nome do módulo que o funcionalidade pertence',
                //            'wrap' => true
            )
        );
        $this->add($sistema);

        $icon = new Element\Text("funcIcon");
        $icon->setAttributes(
            array(
                'col'         => 4,
                'label'       => 'Ícone do funcionalidade',
                'placeholder' => 'Digite o ícone do funcionalidade',
                'icon'        => 'icon-prepend fa fa-user',
                'wrap'        => true
            )
        );
        $this->add($icon);

        $info = new Element\Textarea("funcInfo");
        $info->setAttributes(
            array(
                'col'         => 4,
                'label'       => 'Informações sobre o funcionalidade',
                'placeholder' => 'Digite as informações',
                'icon'        => 'icon-prepend fa fa-edit',
                //            'wrap' => true
            )
        );
        $this->add($info);

        $status = new \VersaSpine\Form\SmartAdmin\Elements\AlternateOptions("funcStatus");
        $status->setAttributes(
            array(
                'col'              => 2,
                'label'            => 'Status do funcionalidade',
                'toggle_type'      => 'radio',
                'wrap'             => true,
                'data-validations' => 'Required'
            )
        );
        $status->setValueOptions(
            array(
                'Ativa'   => array('on' => 'Sim', 'off' => 'Não', 'value' => 'Ativa'),
                'Inativa' => array('on' => 'Sim', 'off' => 'Não', 'value' => 'Inativa'),
            )
        );
        $this->add($status);

        $visivel = new \VersaSpine\Form\SmartAdmin\Elements\AlternateOptions("funcVisivel");
        $visivel->setAttributes(
            array(
                'col'              => 2,
                'label'            => 'Visível no menu',
                'toggle_type'      => 'radio',
                'wrap'             => true,
                'data-validations' => 'Required'
            )
        );
        $visivel->setValueOptions(
            array(
                'Visível'   => array('on' => 'Sim', 'off' => 'Não', 'value' => 'Sim'),
                'Invisível' => array('on' => 'Sim', 'off' => 'Não', 'value' => 'Não'),
            )
        );
        $this->add($visivel);

        $buttonAction = new Element\Button("addAction");
        $buttonAction->setLabel("Adicionar ação");
        $buttonAction->setAttributes(
            array(
                'type'  => 'button',
                'col'   => 4,
                'class' => 'btn btn-primary',
                'id'    => 'addAction'
            )
        );
        $this->add($buttonAction);

        $submit = new Element\Submit("enviar");
        $submit->setValue("Salvar");
        $submit->setAttribute('class', 'pull-left btn btn-primary');
        $this->add($submit);
    }

}
