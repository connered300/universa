<?php


namespace Acesso\Form;

use Zend\Form\Form,
    Zend\Form\Element;

class RecuperarSenha extends Form
{
    public function __construct()
    {
        parent::__construct('recuperarSenha');
        $this->setAttribute("class", 'smart-form');
        $this->setAttribute('data-validate', 'yes');
        $this->setAttribute('method', 'POST');

        $login = new Element\Text('login');
        $login->setAttributes(
            [
                'label'            => 'Informe seu usuário',
                'placeholder'      => 'digite aqui o seu usuário.',
                'col'              => '4',
                'data-validations' => 'Required',
                'icon'             => 'icon-prepend fa fa-user'
            ]
        );
        $this->add($login);

        $email = new Element\Text('email');
        $email->setAttributes(
            [
                'label'            => 'Informe seu e-mail:',
                'placeholder'      => 'digite aqui o seu e-mail',
                'icon'             => 'icon-prepend fa-envelope-square',
                'col'              => '4',
                'data-validations' => 'Required'
            ]
        );
        $this->add($email);

        $submit = new Element\Submit("enviar");
        $submit->setValue("Recuperar Senha");
        $submit->setAttribute('class', 'pull-right btn btn-primary');
        $this->add($submit);

        /**
         * Estes elementos serão utilizados para redirecionar após a execução da ação,
         * você pode utiliza-lo para que após a geração da nova senha o seu usuário seja redirecionado
         * para uma ação específica.
         * Ex: Tenho dois tipos de login no meu sistema, vestibulando e secretaria, o vestibulando loga em
         * uma área específica e a secretaria em outra, para isso ao recuperar a senha eu preencho esses campos
         * com a rota '/vestibular/selecao-inscricao/login'.
         * */
        $redirect = new Element\Hidden('redirectRoute');
        $this->add($redirect);

        $redirect = new Element\Hidden('redirectController');
        $this->add($redirect);

        $redirect = new Element\Hidden('redirectAction');
        $this->add($redirect);
    }
}