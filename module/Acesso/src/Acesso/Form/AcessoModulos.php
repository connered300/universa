<?php

namespace Acesso\Form;

use Zend\Form\Form,
    Zend\Form\Element;

/**
 * Description of AcessoSistemas
 *
 * @author joao
 */
class AcessoModulos extends Form
{
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name, $options);
        $this->setAttribute("class", "smart-form");
        $this->setAttribute("data-validate", "yes");

        $id = new Element\Hidden("id");
        $this->add($id);

        $nome = new Element\Text("modNome");
        $nome->setAttributes(
            array(
                'col'              => 4,
                'placeholder'      => 'Digite o nome do módulo',
                'label'            => 'Nome',
                'icon'             => 'icon-prepend fa fa-user',
                'wrap'             => true,
                'data-validations' => 'Required'
            )
        );
        $this->add($nome);

        $sistema = new Element\Text("sistema");
        $sistema->setAttributes(
            array(
                'data-autocomplete-url' => '/acesso/acesso-sistemas/autocomplete-json',
                'col'                   => 4,
                'icon'                  => 'icon-prepend fa fa-user',
                'placeholder'           => 'Digite o nome do sistema',
                'label'                 => 'Digite o nome do sistema que o módulo pertence',
                'wrap'                  => true
            )
        );
        $this->add($sistema);

        $icon = new Element\Text("modIcon");
        $icon->setAttributes(
            array(
                'col'         => 4,
                'label'       => 'Ícone do módulo',
                'placeholder' => 'Digite o ícone do módulo',
                'icon'        => 'icon-prepend fa fa-user',
                'wrap'        => true
            )
        );
        $this->add($icon);
        $route = new Element\Text("modRoute");
        $route->setAttributes(
            array(
                'col'         => 4,
                'label'       => 'Informações sobre a rota',
                'placeholder' => 'Digite a rota do módulo',
                'icon'        => 'icon-prepend fa fa-edit',
                'wrap'        => true
            )
        );
        $this->add($route);

        $info = new Element\Textarea("modInfo");
        $info->setAttributes(
            array(
                'col'         => 6,
                'label'       => 'Informações sobre o módulo',
                'placeholder' => 'Digite as informações',
                'icon'        => 'icon-prepend fa fa-edit',
                'wrap'        => true
            )
        );
        $this->add($info);

        $status = new \VersaSpine\Form\SmartAdmin\Elements\AlternateOptions("modStatus");
        $status->setAttributes(
            array(
                'col'              => 2,
                'label'            => 'Status do módulo',
                'toggle_type'      => 'radio',
                'wrap'             => true,
                'data-validations' => 'Required'
            )
        );
        $status->setValueOptions(
            array(
                'Ativo'   => array('on' => 'Sim', 'off' => 'Não', 'value' => 'Ativo'),
                'Inativo' => array('on' => 'Sim', 'off' => 'Não', 'value' => 'Inativo'),
            )
        );
        $this->add($status);

        $submit = new Element\Submit("enviar");
        $submit->setValue("Salvar");
        $submit->setAttribute('class', 'pull-left btn btn-primary');
        $this->add($submit);
    }

}
