<?php

namespace Acesso\Form;

use Zend\Form\Form,
    Zend\Form\Element;

/**
 * Description of AcessoSistemas
 *
 * @author joao
 */
class AcessoActions extends Form
{
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name, $options);
        $this->setAttribute("class", "smart-form");
        $this->setAttribute("data-validate", "yes");

        $id = new Element\Hidden("id");
        $this->add($id);

        $funcionalidade = new Element\Text("funcionalidade");
        $funcionalidade->setAttributes(
            array(
                'col'                   => 4,
                'placeholder'           => 'Digite o nome do funcionalidade Pai',
                'label'                 => 'Nome',
                'icon'                  => 'icon-prepend fa fa-user',
                'wrap'                  => true,
                'data-validations'      => 'Required',
                'data-autocomplete-url' => '/acesso/acesso-funcionalidades/autocomplete-json',
            )
        );
        $this->add($funcionalidade);

        $nome = new Element\Text("actNome");
        $nome->setAttributes(
            array(
                'col'              => 4,
                'placeholder'      => 'Digite o nome da action',
                'label'            => 'Action',
                'icon'             => 'icon-prepend fa fa-user',
                'wrap'             => true,
                'data-validations' => 'Required'
            )
        );
        $this->add($nome);

        $label = new Element\Text("actLabel");
        $label->setAttributes(
            array(
                'col'         => 4,
                'icon'        => 'icon-prepend fa fa-user',
                'placeholder' => 'Digite o label da action',
                'label'       => 'Digite o Label da action',
                'wrap'        => true
            )
        );
        $this->add($label);

        $icon = new Element\Text("actIcon");
        $icon->setAttributes(
            array(
                'col'         => 4,
                'label'       => 'Ícone',
                'placeholder' => 'Digite o nome dp ícone da action',
                'icon'        => 'icon-prepend fa fa-user',
                'wrap'        => true
            )
        );
        $this->add($icon);

        $info = new Element\Textarea("actInfo");
        $info->setAttributes(
            array(
                'col'         => 6,
                'label'       => 'Informações sobre a action',
                'placeholder' => 'Digite as informações',
                'icon'        => 'icon-prepend fa fa-edit',
                'wrap'        => true
            )
        );
        $this->add($info);

        $submit = new Element\Submit("enviar");
        $submit->setValue("Salvar");
        $submit->setAttribute('class', 'pull-left btn btn-primary');
        $this->add($submit);
    }

}
