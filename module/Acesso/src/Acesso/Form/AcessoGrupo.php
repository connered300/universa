<?php

namespace Acesso\Form;

use Zend\Form\Form,
    Zend\Form\Element;

/**
 * Description of AcessoSistemas
 *
 * @author joao
 */
class AcessoGrupo extends Form
{
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name, $options);
        $this->setAttribute("class", "smart-form");
        $this->setAttribute("data-validate", "yes");

        $id = new Element\Hidden("id");
        $this->add($id);

        $id = new Element\Hidden("acessoActions");
        $this->add($id);

        $id = new Element\Hidden("usuario");
        $this->add($id);

        $nome = new Element\Text("grupNome");
        $nome->setAttributes(
            array(
                'col'              => 4,
                'placeholder'      => 'Digite o nome do grupo',
                'label'            => 'Nome',
                'icon'             => 'icon-prepend fa fa-user',
                'wrap'             => true,
                'data-validations' => 'Required'
            )
        );
        $this->add($nome);

        $grupIcon = new Element\Text("grupIcon");
        $grupIcon->setAttributes(
            array(
                'col'         => 4,
                'placeholder' => 'Digite o nome do ícone',
                'label'       => 'Ícone',
                'icon'        => 'icon-prepend fa fa-user',
                'wrap'        => true,
            )
        );
        $this->add($grupIcon);

        $grupInfo = new Element\Textarea("grupInfo");
        $grupInfo->setAttributes(
            array(
                'col'         => 4,
                'placeholder' => 'Digite as informações do grupo',
                'label'       => 'Informação do grupo',
                'icon'        => 'icon-prepend fa fa-user',
                'wrap'        => true,
            )
        );
        $this->add($grupInfo);

        $button = new \Zend\Form\Element\Button("permissoes");
        $button->setAttributes(
            array(
                'col'   => 2,
                'class' => 'btn btn-primary',
                'style' => 'width:100%; height:30px;',
                'id'    => 'addPermissoes'
            )
        );
        $button->setLabel("Permissões do grupo");
        $this->add($button);

        $submit = new Element\Submit("enviar");
        $submit->setValue("Salvar");
        $submit->setAttribute('class', 'pull-left btn btn-primary');
        $this->add($submit);
    }

}
