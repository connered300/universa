<?php

namespace Acesso\Form;

use Zend\Form\Form,
    Zend\Form\Element;

/**
 * Description of AcessoSistemas
 *
 * @author joao
 */
class AcessoSistemas extends Form
{
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name, $options);
        $this->setAttribute("class", "smart-form");
        $this->setAttribute("data-validate", "yes");

        $id = new Element\Hidden("id");
        $this->add($id);

        $nome = new Element\Text("sisNome");
        $nome->setAttributes(
            array(
                'col'              => 4,
                'placeholder'      => 'Digite o nome do sistema',
                'label'            => 'Nome',
                'icon'             => 'icon-prepend fa fa-user',
                'wrap'             => true,
                'data-validations' => 'Required'
            )
        );
        $this->add($nome);

        $icon = new Element\Text("sisIcon");
        $icon->setAttributes(
            array(
                'col'         => 4,
                'label'       => 'Ícone do sistema',
                'placeholder' => 'Digite o ícone do sistema',
                'icon'        => 'icon-prepend fa fa-user',
                'wrap'        => true
            )
        );
        $this->add($icon);

        $info = new Element\Textarea("sisInfo");
        $info->setAttributes(
            array(
                'col'         => 6,
                'label'       => 'Informações sobre o sistema',
                'placeholder' => 'Digite as informações',
                'icon'        => 'icon-prepend fa fa-edit',
                'wrap'        => true
            )
        );
        $this->add($info);

        $status = new \VersaSpine\Form\SmartAdmin\Elements\AlternateOptions("sisStatus");
        $status->setAttributes(
            array(
                'col'              => 2,
                'label'            => 'Status do sistema',
                'toggle_type'      => 'radio',
                'wrap'             => true,
                'data-validations' => 'Required'
            )
        );
        $status->setValueOptions(
            array(
                'Ativo'   => array('on' => 'Sim', 'off' => 'Não', 'value' => 'Ativo'),
                'Inativo' => array('on' => 'Sim', 'off' => 'Não', 'value' => 'Inativo'),
            )
        );
        $this->add($status);

        $submit = new Element\Submit("enviar");
        $submit->setValue("Salvar");
        $submit->setAttribute('class', 'pull-left btn btn-primary');
        $this->add($submit);
    }

}
