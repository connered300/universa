<?php

namespace Acesso\Adapter;

/**
 *
 * @author JoaoPaulo
 */
interface AcessoInterface
{
    public function beforeAcess($controllerEvent = false, $controller = null, $action = null, $usuario = null);

    public function afterAcess($controllerEvent = false, $controller = null, $action = null, $usuario = null);

}
