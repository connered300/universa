<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Acesso\Adapter;

use Acesso\Adapter\AcessoInterface;

/**
 * Description of Acesso
 *
 * @author JoaoPaulo
 */
class Acesso
{
    /**
     * @var AcessoInterface
     */

    private $adapter;

    private $controllerEvent;

    private $controller;

    private $action;

    private $usuario;

    public function __construct(AcessoInterface $adapter, $controllerEvent, $controller, $action, $usuario)
    {
        $this->action          = $action;
        $this->controller      = $controller;
        $this->adapter         = $adapter;
        $this->usuario         = $usuario;
        $this->controllerEvent = $controllerEvent;
    }

    public function beforeAccess()
    {
        return $this->adapter->beforeAcess($this->controllerEvent, $this->controller, $this->action, $this->usuario);
    }

    public function afterAcess()
    {
        return $this->adapter->afterAcess($this->controllerEvent, $$this->controller, $this->action, $this->usuario);
    }

}
