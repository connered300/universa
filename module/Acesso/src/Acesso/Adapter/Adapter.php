<?php

namespace Acesso\Adapter;

use Zend\Authentication\Adapter\AdapterInterface,
    Zend\Authentication\Result;

use Doctrine\ORM\EntityManager;

class Adapter implements AdapterInterface
{
    /**
     * Gerenciador de entidade
     * @var Doctrine\ORM\EntityManager
     */
    protected $em;
    /**
     * Login do usuário de acesso
     * @var string
     */
    protected $login;
    /**
     * senha
     * @var string
     */
    protected $Senha;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Retorna o login do usuario
     * @return string
     */
    public function getLogin()
    {
        return $this->username;
    }

    /**
     * Seta o login a ser verificado
     * @param string $login
     */
    public function setLogin($login)
    {
        $this->username = $login;
    }

    /**
     * Retorna a seha do usuário
     * @return string
     */
    public function getSenha()
    {
        return $this->password;
    }

    /**
     * Seta a senha do usuário
     * @param string $Senha
     */
    public function setSenha($Senha)
    {
        $this->password = $Senha;
    }

    /**
     * Método que verifica a autenticidade do usuário
     * @return \Zend\Authentication\Result
     */
    public function authenticate()
    {
        $repository = $this->em->getRepository("Acesso\Entity\AcessoPessoas");
        $user       = $repository->validaCredenciais($this->getLogin(), $this->getSenha());

        if ($user) {
            if ($this->em->getReference("Acesso\Entity\AcessoUsuarios", $user->getUsuario()->getId())->getUsrStatus(
                ) == "Inativo"
            ) {
                return new Result(Result::FAILURE_UNCATEGORIZED, $user, array('Este usuário está inativo no sistema!'));
            }

            return new Result(Result::SUCCESS, array('user' => $user->toArray()), array('OK'));
        } else {
            return new Result(Result::FAILURE_CREDENTIAL_INVALID, null, array());
        }
    }

}
