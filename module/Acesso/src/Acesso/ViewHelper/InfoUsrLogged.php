<?php

namespace Acesso\ViewHelper;

use Zend\View\Helper\AbstractHelper;

/**
 * View Helper responsável por prover informações do usuário que está logado no sistema
 * @author joao
 */
class InfoUsrLogged extends AbstractHelper
{
    /**
     *
     * @var \Doctrine\ORM\EntityManager $em
     */
    private $em;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Retorna o próprio objeto
     * @return \Acesso\ViewHelper\InfoUsrLogged
     */
    public function __invoke()
    {
        return $this;
    }

    /**
     * Retorna o nome do usuário logado no sistema
     * @return string
     */
    public function getNome()
    {
        $session = new \Zend\Session\Container("AcessoVersaSkeleton");
        if (!isset($session->usrLoggedName)) {
            if (isset($_SESSION['Zend_Auth']['storage']['pesFisica'])) {
                $pesFisica              = $_SESSION['Zend_Auth']['storage']['pesFisica'];
                $session->usrLoggedName = $this->em->getReference("Pessoa\\Entity\\PessoaFisica", $pesFisica)->getPes(
                )->getPesNome();
            } else {
                if (isset($_SESSION['Zend_Auth']['storage']['pesJuridica'])) {
                    $pesJuridica            = $_SESSION['Zend_Auth']['storage']['pesJuridica'];
                    $session->usrLoggedName = $this->em->getReference(
                        "Pessoa\\Entity\\PessoaJuridica",
                        $pesJuridica
                    )->getPes()->getPesNome();
                }
            }
        }

        return $session->usrLoggedName;
    }

    /**
     * Retorna o login do usuário logado no sistema
     * @return string
     */
    public function getLogin()
    {
        $session = new \Zend\Session\Container("AcessoVersaSkeleton");

        if (!isset($session->usrLoggedLogin)) {
            if (isset($_SESSION['Zend_Auth']['storage']['login'])) {
                $session->usrLoggedLogin = $_SESSION['Zend_Auth']['storage']['login'];
            }
        }

        return $session->usrLoggedLogin;
    }
}
