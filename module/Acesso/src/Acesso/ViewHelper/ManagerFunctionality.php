<?php

namespace Acesso\ViewHelper;

use Zend\View\Helper\AbstractHelper;

/**
 * Helper que gerencia o menu principal do sistema, o mesmo coleta informações do usuário logado e carrega o menu com base nas suas permissões
 * @version 1.0
 * @author jp
 */
class ManagerFunctionality extends AbstractHelper
{
    /**
     * Gereciador de entidade
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * Serviço que prove as funcionalidade do usuário
     * @var \Acesso\Service\AcessoFuncionalidades
     */
    private $serviceFunc;

    /**
     *
     * @var array
     */
    private $dados;

    /**
     * Parametros da rota
     * @var array
     */
    private $paramsRoute;

    public function __construct(\Doctrine\ORM\EntityManager $em, $paramsRoute)
    {
        $this->em          = $em;
        $this->dados       = $this->getFunc()->buscaFuncionalidadesUsuario(
            $_SESSION['Zend_Auth']['storage']['usuario']
        );
        $this->paramsRoute = $paramsRoute;
    }

    /**
     *
     * @return \Acesso\ViewHelper\ManagerFunctionality
     */
    public function __invoke()
    {
        return $this;
    }

    /**
     * Retorna uma instancia de \Acesso\Service\AcessoFuncionalidades
     * @return \Acesso\Service\AcessoFuncionalidades
     */
    private function getFunc()
    {
        if ($this->serviceFunc == null) {
            $this->serviceFunc = new \Acesso\Service\AcessoFuncionalidades($this->em);
        }

        return $this->serviceFunc;
    }

    /**
     * Gera o html do menu principal
     * @return string
     */
    public function showMenu()
    {
        try {
            if ($this->paramsRoute) {
                $url = $this->view->url(
                    $this->paramsRoute->getMatchedRouteName(),
                    array('controller' => $this->paramsRoute->getParams()['__CONTROLLER__'])
                );
            } else {
                $url = '';
            }
        } catch (\Exception $ex) {
        }

        $sistemaSelected = $this->getSisSelected();

        if (!$sistemaSelected) {
            $sistemaSelected = $this->dados['sistemas'][0]['id'];
        }

        $html = "<ul>";

        foreach ($this->dados['modulos'][$sistemaSelected] as $sistema => $mod) {
            if (count($this->dados['funcionalidades'][$mod['id']]) > 0) {
                $html .= "<li>";
                $html .= "<a href='#'><i class='fa fa-lg fa-fw {$mod['mod_icon']}'></i> <span class='menu-item-parent'> {$mod['mod_nome']} </span> </a>";
                $html .= "<ul>";

                foreach ($this->dados['funcionalidades'][$mod['id']] as $func) {
                    $html .= "<li>";
                    $html .= "<a data-li-active='{$func['id']}' data-li-type='func' href='" . $this->view->url(
                            $mod['mod_route'],
                            array(
                                'controller' => $this->view->spinePsr0()->strToPsr0(
                                    explode('\\', $func['func_controller'])[2]
                                )
                            )
                        ) . "'> {$func['func_nome']} </a>";
                    $html .= "</li>";
                }

                $html .= "</ul>";
                $html .= "</li>";
            } else {
                $html .= "<li><a data-li-active='{$mod['id']}' data-li-type='modulo' href='/" . $this->view->spinePsr0(
                    )->strToPsr0(
                        $this->view->spineString()->removeAcento($mod['mod_route'])
                    ) . "'><i class='fa fa-lg fa-fw {$mod['mod_icon']}'></i> <span class='menu-item-parent'> {$mod['mod_nome']} </span> </a></li>";
            }
        }

        $html .= "</ul>";
        ?>
        <script>
            $(function () {
                $("a[href^='<?=$url?>']").parent().addClass("active");
            });
        </script>
        <?php
        return $html;
    }

    /**
     * Monta uma lista de LIs com as funcionalidades que podem ser acessadas
     * @return string
     */
    public function showListSistem()
    {
        $html = "";

        foreach ($this->dados['sistemas'] as $sistema) {
            $html .= "<li>
                <a href='" . $this->view->url(
                    "acesso/config-session-sistem",
                    array(
                        'controller' => 'config-session',
                        'action'     => 'select-sistem',
                        'sistema'    => $sistema['sis_id']
                    )
                ) . "'class='jarvismetro-tile big-cubes bg-color-blue'> <span class='iconbox'> <i class='fa {$sistema['sis_icon']} fa-4x'></i> <span>{$sistema['sis_nome']}</span> </span> </a>
            </li>";
        }

        return $html;
    }

    /**
     * Retorna o sistema selecionado pelo usuário
     * @return string
     */
    public function getSisSelected()
    {
        $selected = false;

        if ($this->paramsRoute) {
            $route = $this->paramsRoute->getMatchedRouteName();
        } else {
            $route = '';
        }

        foreach ($this->dados['sistemas'] as $sistema) {
            if ($sistema['mod_route'] == $route) {
                $selected = $sistema['sis_id'];
                break;
            }
        }

        if (!$selected) {
            $selected = (new \Zend\Session\Container("AcessoVersaSkeleton"))->sistemaSelected;
        }

        return $selected;
    }
}
