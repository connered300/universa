<?php
namespace Acesso;

use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session as SessionStorage;
use Zend\Http\Response;
use Zend\ModuleManager\ModuleManager;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();

        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $em       = $e->getApplication()->getServiceManager()->get('Doctrine\ORM\EntityManager');
        $platform = $em->getConnection()->getDatabasePlatform();
        $platform->registerDoctrineTypeMapping('enum', 'string');
        $platform->registerDoctrineTypeMapping('set', 'string');

        $eventManager->attach(
            MvcEvent::EVENT_ROUTE,
            function (MvcEvent $r) {
                $config = $r->getApplication()->getServiceManager()->get('Config');

                if ($config['maintenance']) {
                    $response = $r->getResponse();
                    $response->setStatusCode(503);

                    $arquivoManutencao = 'error' . DIRECTORY_SEPARATOR . '503.phtml';

                    if (is_file($arquivoManutencao)) {
                        $response->setContent(file_get_contents($arquivoManutencao));
                    } else {
                        $response->setContent('<h1>Sistema em manutenção!</h1>');
                    }

                    return $response;
                }
            },
            1000
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function init(ModuleManager $moduleManager)
    {
        $sharedEvents = $moduleManager->getEventManager()->getSharedManager();

        $sharedEvents->attach(
            "Zend\Mvc\Controller\AbstractActionController",
            MvcEvent::EVENT_DISPATCH,
            array($this, 'validaAuth'),
            100
        );
        //        $sharedEvents->attach("Zend\Mvc\Controller\AbstractActionController",
        //                MvcEvent::EVENT_DISPATCH,
        //                array($this,'writeLogHigh'),80);
    }

    public function writeLogLowLevel($controller, $action)
    {
        $basePath = __DIR__ . "/../../../data/log/";
        $folder   = date("Y/m/");
        $fileName = $basePath . $folder;

        if (!file_exists($fileName)) {
            if (!mkdir($fileName, 0777, true)) {
                throw new \Exception(
                    "O Versa Skeleton não conseguiu criar a pasta $fileName dentro da pasta data, crie a pasta ou de permissão de escrita!"
                );
            }
        }

        $file = fopen($fileName . date('d') . ".log", "a+");
        if ($file) {
            if (!fwrite(
                $file,
                "Usuário {$_SESSION['Zend_Auth']['storage']['usuario']} acessou o controller $controller e action $action no horário: " . date(
                    "H:i:s"
                ) . "\n"
            )
            ) {
                throw new \Exception(
                    "O Sistema de log em baixo nível não conseguiu escrever o log, verifique as permissões no servidor!"
                );
            }
        }
    }

    public function validaAuth($e)
    {
        $auth = new AuthenticationService;
        $auth->setStorage(new SessionStorage());

        $controller                = $e->getTarget();
        $matchedRoute              = $controller->getEvent()->getRouteMatch()->getMatchedRouteName();
        $rotasLiberadas            = $controller->getServiceLocator()->get("config")['rotas_livres'];
        $funcionaldiades_liberadas = $controller->getServiceLocator()->get("config")['funcionalidades_liberadas'];

        $controller_name = $controller->params()->fromRoute()['controller'];
        $action_name     = $controller->params()->fromRoute()['action'];

        if (
            array_search($action_name, $funcionaldiades_liberadas['actions']) !== false ||
            array_search($controller_name, $funcionaldiades_liberadas['controllers']) !== false
        ) {
            return true;
        }

        $request = $e->getRequest();
        $headers = $request->getHeaders(); // recupera cabeçalho da requisição

        $requestAjax = $headers->get('X-Requested-With'); // tenta recupera objeto ajax

        $requestTypeAjax = false;

        if ($requestAjax) {
            if ($requestAjax->getFieldValue() == "XMLHttpRequest") {
                $requestTypeAjax = true;
            }
        }

        if (!$auth->hasIdentity() AND array_search($matchedRoute, $rotasLiberadas) === false) {
            if ($requestTypeAjax) {
                // interrompe o processamento de outras tarefas
                $e->stopPropagation(true);

                // monta mensagem de resposta
                $response = new Response();

                $response->setStatusCode(Response::STATUS_CODE_401);
                $response->setContent('Sessão Expirada');

                return $response;
            }

            return $controller->redirect()->toRoute("acesso-autenticacao-login");
        }

        $this->writeLogLowLevel($controller_name, $action_name);

        /** @var \Acesso\Service\AcessoUsuarios $service */
        $service = $controller->services()->getService("Acesso\Service\AcessoUsuarios");

        $horaAtual = new \DateTime('now');
        $horaLogin = $_SESSION['Zend_Auth']['storage']['datetime_login'];

        if (!is_object($horaLogin)) {
            return $controller->redirect()->toRoute("acesso-autenticacao-logout");
        }

        if (count($_SESSION['Zend_Auth']['storage']['grupos']) <= 0) {
            $_SESSION['Zend_Auth']['storage']['grupos'] = $controller->services()->getService(
                "Acesso\Service\AcessoGrupo"
            )->buscaGrupoUsr();
        }

        $interval = $horaAtual->diff($horaLogin);

        if ($interval->format("%i") >= $_SESSION['Zend_Auth']['storage']['cache_expire']) {
            if ($requestTypeAjax) {
                // interrompe o processamento de outras tarefas
                $e->stopPropagation(true);

                // monta mensagem de resposta
                $response = new Response();

                $response->setStatusCode(Response::STATUS_CODE_401);
                $response->setContent('Sessão Expirada');

                return $response;
            }

            return $controller->redirect()->toRoute("acesso-autenticacao-logout");
        }

        $_SESSION['Zend_Auth']['storage']['datetime_login'] = new \DateTime("now");
        $usuarioId                                          = $_SESSION['Zend_Auth']['storage']['usuario'];
        $temAcesso                                          = false;

        $this->beforeAccess($controller, $controller_name, $action_name, $usuarioId);

        /**
         * Executa as validacoes padroes do modulo acesso
         */

        $acesso = $service->permissaoPorPrivilegios($usuarioId, $controller_name, $action_name);

        if ($acesso) {
            $temAcesso = true;
        }

        $acesso = $service->permissaoPorGrupo($usuarioId, $controller_name, $action_name);

        if ($acesso) {
            $temAcesso = true;
        }

        $acesso = $service->actionChildren($action_name);
        if ($acesso) {
            $temAcesso = true;
        }

        /**
         * Executa rotina da aplicacao pos validacoes
         */
        $this->afterAcess($controller, $controller_name, $action_name, $usuarioId);

        if ($temAcesso) {
            return true;
        }

        if ($requestTypeAjax) {
            // interrompe o processamento de outras tarefas
            $e->stopPropagation(true);

            // monta mensagem de resposta
            $response = new Response();

            $response->setStatusCode(Response::STATUS_CODE_401);
            $response->setContent('Acesso Negado');

            return $response;
        }

        $rotaPrioritaria = $service->getRotaDefault($usuarioId);

        if ($rotaPrioritaria) {
            $route = $controller->url()->fromRoute(
                $rotaPrioritaria['route'],
                array('controller' => $rotaPrioritaria['controller'], 'action' => $rotaPrioritaria['action'])
            );

            return $controller->redirect()->toUrl($route);
        }

        $url = $controller->url()->fromRoute("acesso-autorizacao");

        return $controller->redirect()->toRoute("acesso-autorizacao");
    }

    public function beforeAccess($controllerEvent, $controller, $action, $usuario)
    {
        $config  = $controllerEvent->getServiceLocator()->get("config");
        $adapter = $this->getAdapter($config);

        if (!is_object($adapter)) {
            return false;
        }

        $execAdapter = new \Acesso\Adapter\Acesso($adapter, $controllerEvent, $controller, $action, $usuario);

        return $execAdapter->beforeAccess();
    }

    public function afterAcess($controllerEvent, $controller, $action, $usuario)
    {
        $config  = $controllerEvent->getServiceLocator()->get("config");
        $adapter = $this->getAdapter($config);

        if (!is_object($adapter)) {
            return false;
        }

        return $adapter->afterAcess();
    }

    private function getAdapter($config)
    {
        if (empty($config['acesso_execute'])) {
            return false;
        }
        $class = new $config['acesso_execute'];

        return new $class;
    }

    public function writeLogHigh($e)
    {
        $controller       = $e->getTarget();
        $config           = $controller->getServiceLocator()->get('config')['logs_level_high'];
        $controller_class = $controller->params()->fromRoute()['controller'];
        $action_name      = $controller->params()->fromRoute()['action'];

        if (isset($config[$controller_class][$action_name])) {
            $service = $controller->services()->getService("Acesso\Service\AcessoLog");
            try {
                $array = array(
                    'logAction'     => $action_name,
                    'logController' => $controller_class,
                    'logPriority'   => $config[$controller_class][$action_name],
                    'usuario'       => $controller->usrAuth()->getUsr(),
                    'logInfo'       => null
                );
                $service->adicionar($array);
            } catch (\Exception $ex) {
                throw new \Exception($ex->getMessage(), $code, $previous);
            }
        }
    }

    public function getViewHelperConfig()
    {
        return array(
            //            'invokables' => array(
            //                'infoUsrLogged' => '\Acesso\ViewHelper\InfoUsrLogged'
            //            ),
            'factories' => array(
                'infoUsrLogged'        => function ($sm) {
                    return new \Acesso\ViewHelper\InfoUsrLogged(
                        $sm->getServiceLocator()->get('Doctrine\ORM\EntityManager')
                    );
                },
                'listaFuncionalidades' => function ($sm) {
                    $routeParams = $sm->getServiceLocator()->get('application')->getMvcEvent()->getRouteMatch();

                    return new \Acesso\ViewHelper\ManagerFunctionality(
                        $sm->getServiceLocator()->get("Doctrine\ORM\EntityManager"), $routeParams
                    );
                },
                'layoutInformation'    => function ($sm) {
                    return new \Layout\ViewHelper\LayoutInformation($sm->getServiceLocator());
                },
                'layoutComponent'      => function ($sm) {
                    return new \Layout\ViewHelper\LayoutComponent($sm->getServiceLocator());
                },
            )
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Teste' => function ($sm) {
                    return $sm->get("Doctrine\ORM\EntityManager");
                }
            )
        );
    }
}
