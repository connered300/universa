<?php

/**
 * Arquivo de configurações do módulo Acesso
 */

namespace Acesso;

return array(
    'router'                    => array(
        'routes' => array(
            'acesso'                     => array(
                'type'          => 'Literal',
                'options'       => array(
                    'route'    => '/acesso',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Acesso\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes'  => array(
                    'default'               => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'       => '/[:controller[/:action[/:id]]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults'    => array(),
                        ),
                    ),
                    'config-session-sistem' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/config-session/select-sistem[/:sistema]',
                            'defaults' => array(
                                'controller' => 'ConfigSession',
                                'action'     => 'select-sistem'
                            )
                        )
                    )
                ),
            ),
            'acesso-autenticacao-login'  => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/acesso/autenticacao/login',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Acesso\Controller',
                        'controller'    => 'Autenticacao',
                        'action'        => 'login',
                    ),
                )
            ),
            'acesso-recovery-password'   => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/acesso/acesso-pessoas/recovery-password',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Acesso\Controller',
                        'controller'    => 'AcessoPessoas',
                        'action'        => 'recovery-password',
                    ),
                )
            ),
            'acesso-autenticacao-logout' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/acesso/autenticacao/logout',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Acesso\Controller',
                        'controller'    => 'Autenticacao',
                        'action'        => 'logout',
                    ),
                )
            ),
            'acesso-autorizacao'         => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/acesso/autorizacao/acesso-negado',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Acesso\Controller',
                        'controller'    => 'Autorizacao',
                        'action'        => 'acesso-negado',
                    ),
                )
            ),
        ),
    ),
    'controllers'               => array(
        'invokables' => array(
            'Acesso\Controller\Index'                 => 'Acesso\Controller\IndexController',
            'Acesso\Controller\Autenticacao'          => 'Acesso\Controller\AutenticacaoController',
            'Acesso\Controller\AcessoPessoas'         => 'Acesso\Controller\AcessoPessoasController',
            'Acesso\Controller\AcessoSistemas'        => 'Acesso\Controller\AcessoSistemasController',
            'Acesso\Controller\AcessoModulos'         => 'Acesso\Controller\AcessoModulosController',
            'Acesso\Controller\AcessoFuncionalidades' => 'Acesso\Controller\AcessoFuncionalidadesController',
            'Acesso\Controller\AcessoGrupo'           => 'Acesso\Controller\AcessoGrupoController',
            'Acesso\Controller\AcessoActions'         => 'Acesso\Controller\AcessoActionsController',
            'Acesso\Controller\ConfigSession'         => 'Acesso\Controller\ConfigSessionController',
            'Acesso\Controller\AcessoUsuarios'        => 'Acesso\Controller\AcessoUsuariosController',
            'Acesso\Controller\Autorizacao'           => 'Acesso\Controller\AutorizacaoController',
            'Acesso\Controller\AcessoInterface'       => 'Acesso\Controller\AcessoInterfaceController',
        ),
    ),
    'controller_plugins'        => array(
        'invokables' => array(
            'usrAuth'  => 'Acesso\Plugins\UsrAutenticate',
            'log'      => 'Acesso\Plugins\Log',
            'basePath' => 'Acesso\Plugins\basePath'

        )
    ),
    'view_manager'              => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies'          => array(
            'ViewJsonStrategy'
        )
    ),
    'doctrine'                  => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default'             => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            ),
        ),
    ),
    /**
     * Todas as rotas que forem adicionadas nessa sessao automaticamente são liberadas
     */
    'rotas_livres'              => array(
        'acesso-autenticacao-login',
        'acesso-autenticacao-logout',
    ),
    /**
     * Sessão para definição de controllers onde será feito log automaticamente,
     * ou seja, para criar logs automaticamente especifique o nome do controller e as actions para gravar o log
     */
    'logs_level_high'           => array(),
    /**
     * Funcionalidade liberadas, mesmo objetivo dos rotas livres, mas nessa sessão especifique o controller e actions a serem liberadas
     */
    'funcionalidades_liberadas' => array(
        'controllers' => array(
            'Acesso\Controller\Autorizacao',
            'Acesso\Controller\Autenticacao',

        ),
        'actions'     => array(
            "show-form",
            "search-for-json",
            //            "search",
            //            "pagination",
            "autocomplete-json",
            "select-sistem",
            'valida-login',
            'valida-cnpj',
            'pesquisa-usuario-json',
            'check-login',
            'busca-email',
            'busca-usuario',
            'recovery-password',
            'troca-senha-json',
            'altera-senha',
            'cria-usuarios-por-tipo-automaticamente'
        )
    ),
    'emailRecovery'             => array(
        'connection_class' => 'login',
        'name'             => 'Nome remetente',
        'host'             => 'url smtp remetente',
        'userName'         => 'email/usuario smtp remetente',
        'password'         => 'senha smtp remetente',
        'port'             => 'porta remetente',
        'from'             => 'email remetente',
    ),
    'namesDictionary'           => array(
        'Acesso\Autenticacao' => 'Autenticação',
    )
);
