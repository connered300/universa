<?php

namespace Biblioteca\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * Autor
 *
 * @ORM\Table(name="biblioteca__autor")
 * @ORM\Entity
 * @LG\LG(id="autorId",label="autorReferencia")
 * @Jarvis\Jarvis(title="Lista de autores",icon="fa fa-table")
 */
class Autor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="autor_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="autor_id")
     * @LG\Labels\Attributes(text="Índice")
     * @LG\Querys\Conditions(type="=")
     */
    private $autorId;

    /**
     * @var string
     *
     * @ORM\Column(name="autor_referencia", type="string", length=200, nullable=false)
     * @LG\Labels\Property(name="autor_referencia")
     * @LG\Labels\Attributes(text="Referência")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $autorReferencia;

    /**
     * @var string
     *
     * @ORM\Column(name="autor_cutter", type="string", length=10, nullable=true)
     * @LG\Labels\Property(name="autor_cutter")
     * @LG\Labels\Attributes(text="Cutter")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $autorCutter;

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \VersaSpine\Stdlib\Hydrator())->extract($this);
    }

    /**
     * @return int
     */
    public function getAutorId()
    {
        return $this->autorId;
    }

    /**
     * @param int $autorId
     */
    public function setAutorId($autorId)
    {
        $this->autorId = $autorId;

        return $this;
    }

    /**
     * @return string
     */
    public function getAutorReferencia()
    {
        return $this->autorReferencia;
    }

    /**
     * @param string $autorReferencia
     */
    public function setAutorReferencia($autorReferencia)
    {
        $this->autorReferencia = $autorReferencia;

        return $this;
    }

    /**
     * @return string
     */
    public function getAutorCutter()
    {
        return $this->autorCutter;
    }

    /**
     * @param string $autorCutter
     */
    public function setAutorCutter($autorCutter)
    {
        $this->autorCutter = $autorCutter;

        return $this;
    }
}