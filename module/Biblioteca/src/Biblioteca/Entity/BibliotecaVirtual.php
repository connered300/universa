<?php

namespace Biblioteca\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * Empresa
 *
 * @ORM\Table(name="biblioteca__virtual")
 * @ORM\Entity
 * @LG\LG(id="pesId",label="pesId")
 * @Jarvis\Jarvis(title="Lista de empresas",icon="fa fa-table")
 */
class BibliotecaVirtual
{
    /**
     * @var \Pessoa\Entity\Pessoa
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     */
    private $pes;

    /**
     * @var integer
     *
     * @ORM\Column(name="usuario_categoria", type="integer", nullable=false, length=1)
     * @LG\Labels\Property(name="usuario_categoria")
     * @LG\Labels\Attributes(text="Categoria")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $usuarioCategoria;

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return \Pessoa\Entity\Pessoa
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @param \Pessoa\Entity\Pessoa $pes
     * @return BibliotecaVirtual
     */
    public function setPes($pes)
    {
        $this->pes = $pes;

        return $this;
    }

    /**
     * @return integer
     */
    public function getUsuarioCategoria()
    {
        return $this->usuarioCategoria;
    }

    /**
     * @param integer $usuarioCategoria
     * @return BibliotecaVirtual
     */
    public function setUsuarioCategoria($usuarioCategoria)
    {
        $this->usuarioCategoria = $usuarioCategoria;

        return $this;
    }

    public function toArray()
    {
        return array(
            'pes'              => $this->getPes() ? $this->getPes()->toArray() : array(),
            'usuarioCategoria' => $this->getUsuarioCategoria()
        );
    }
}