<?php

namespace Biblioteca\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * Emprestimo
 *
 * @ORM\Table(name="biblioteca__emprestimo", indexes={@ORM\Index(name="fk_biblioteca__emprestimo_biblioteca__exemplar1_idx", columns={"exemplar_id"}), @ORM\Index(name="fk_biblioteca__emprestimo_biblioteca__pessoa1_idx", columns={"pes_id"}), @ORM\Index(name="fk_biblioteca__emprestimo_biblioteca__modalidade_emprestimo_idx", columns={"modalidade_emprestimo_id"}), @ORM\Index(name="fk_biblioteca__emprestimo_acesso_pessoas1_idx", columns={"usuario_id"}), @ORM\Index(name="fk_biblioteca__emprestimo_acesso_pessoas2_idx", columns={"usuario_id_devolucao"})})
 * @ORM\Entity
 * @LG\LG(id="emprestimoId",label="emprestimoId")
 * @Jarvis\Jarvis(title="Lista de empréstimos",icon="fa fa-table")
 */
class Emprestimo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="emprestimo_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="emprestimo_id")
     * @LG\Labels\Attributes(text="Índice")
     * @LG\Querys\Conditions(type="=")
     */
    private $emprestimoId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="emprestimo_data", type="datetime", nullable=false)
     * @LG\Labels\Property(name="emprestimo_data")
     * @LG\Labels\Attributes(text="data de empréstimo")
     * @LG\Querys\Conditions(type="=")
     */
    private $emprestimoData;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="emprestimo_devolucao_data_previsao", type="datetime", nullable=false)
     * @LG\Labels\Property(name="emprestimo_devolucao_data_previsao")
     * @LG\Labels\Attributes(text="data prevista para devolução")
     * @LG\Querys\Conditions(type="=")
     */
    private $emprestimoDevolucaoDataPrevisao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="emprestimo_devolucao_data_efetuada", type="datetime", nullable=true)
     * @LG\Labels\Property(name="emprestimo_devolucao_data_efetuada")
     * @LG\Labels\Attributes(text="data da devolução do exemplar")
     * @LG\Querys\Conditions(type="=")
     */
    private $emprestimoDevolucaoDataEfetuada;

    /**
     * @var string
     *
     * @ORM\Column(name="emprestimo_multa_valor", type="decimal", precision=10, scale=0, nullable=true)
     * @LG\Labels\Property(name="emprestimo_multa_valor")
     * @LG\Labels\Attributes(text="valor da multa")
     * @LG\Querys\Conditions(type="=")
     */
    private $emprestimoMultaValor;

    /**
     * @var string
     *
     * @ORM\Column(name="emprestimo_multa_valor_pago", type="decimal", precision=10, scale=0, nullable=true)
     * @LG\Labels\Property(name="emprestimo_multa_valor_pago")
     * @LG\Labels\Attributes(text="valor pago da multa")
     * @LG\Querys\Conditions(type="=")
     */
    private $emprestimoMultaValorPago;

    /**
     * @var string
     *
     * @ORM\Column(name="emprestimo_multa_observacao", type="text", nullable=true)
     * @LG\Labels\Property(name="emprestimo_multa_observacao")
     * @LG\Labels\Attributes(text="observação da multa")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $emprestimoMultaObservacao;

    /**
     * @var string
     *
     * @ORM\Column(name="emprestimo_cancelado", type="string", nullable=false)
     * @LG\Labels\Property(name="emprestimo_cancelado")
     * @LG\Labels\Attributes(text="Cancelado")
     * @LG\Querys\Conditions(type="=")
     */
    private $emprestimoCancelado = 'N';

    /**
     * @var \Acesso\Entity\AcessoPessoas
     *
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * })
     */
    private $usuario;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     *
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id_devolucao", referencedColumnName="id")
     * })
     */
    private $usuarioDevolucao;

    /**
     * @var \Biblioteca\Entity\Exemplar
     *
     * @ORM\ManyToOne(targetEntity="Biblioteca\Entity\Exemplar")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="exemplar_id", referencedColumnName="exemplar_id")
     * })
     */
    private $exemplar;

    /**
     * @var \Biblioteca\Entity\ModalidadeEmprestimo
     *
     * @ORM\ManyToOne(targetEntity="Biblioteca\Entity\ModalidadeEmprestimo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="modalidade_emprestimo_id", referencedColumnName="modalidade_emprestimo_id")
     * })
     */
    private $modalidadeEmprestimo;

    /**
     * @var \Biblioteca\Entity\Pessoa
     *
     * @ORM\ManyToOne(targetEntity="Biblioteca\Entity\Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     */
    private $pes;

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \VersaSpine\Stdlib\Hydrator())->extract($this);
    }

    /**
     * @return int
     */
    public function getEmprestimoId()
    {
        return $this->emprestimoId;
    }

    /**
     * @param int $emprestimoId
     * @return Emprestimo
     */
    public function setEmprestimoId($emprestimoId)
    {
        $this->emprestimoId = $emprestimoId;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEmprestimoData()
    {
        return $this->emprestimoData;
    }

    /**
     * @param \DateTime $emprestimoData
     * @return Emprestimo
     */
    public function setEmprestimoData($emprestimoData)
    {
        $this->emprestimoData = $emprestimoData;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEmprestimoDevolucaoDataPrevisao()
    {
        return $this->emprestimoDevolucaoDataPrevisao;
    }

    /**
     * @param \DateTime $emprestimoDevolucaoDataPrevisao
     * @return Emprestimo
     */
    public function setEmprestimoDevolucaoDataPrevisao($emprestimoDevolucaoDataPrevisao)
    {
        $this->emprestimoDevolucaoDataPrevisao = $emprestimoDevolucaoDataPrevisao;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEmprestimoDevolucaoDataEfetuada()
    {
        return $this->emprestimoDevolucaoDataEfetuada;
    }

    /**
     * @param \DateTime $emprestimoDevolucaoDataEfetuada
     * @return Emprestimo
     */
    public function setEmprestimoDevolucaoDataEfetuada($emprestimoDevolucaoDataEfetuada)
    {
        $this->emprestimoDevolucaoDataEfetuada = $emprestimoDevolucaoDataEfetuada;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmprestimoMultaValor()
    {
        return $this->emprestimoMultaValor;
    }

    /**
     * @param string $emprestimoMultaValor
     * @return Emprestimo
     */
    public function setEmprestimoMultaValor($emprestimoMultaValor)
    {
        $this->emprestimoMultaValor = $emprestimoMultaValor;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmprestimoMultaValorPago()
    {
        return $this->emprestimoMultaValorPago;
    }

    /**
     * @param string $emprestimoMultaValorPago
     * @return Emprestimo
     */
    public function setEmprestimoMultaValorPago($emprestimoMultaValorPago)
    {
        $this->emprestimoMultaValorPago = $emprestimoMultaValorPago;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmprestimoMultaObservacao()
    {
        return $this->emprestimoMultaObservacao;
    }

    /**
     * @param string $emprestimoMultaObservacao
     * @return Emprestimo
     */
    public function setEmprestimoMultaObservacao($emprestimoMultaObservacao)
    {
        $this->emprestimoMultaObservacao = $emprestimoMultaObservacao;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmprestimoCancelado()
    {
        return $this->emprestimoCancelado;
    }

    /**
     * @param string $emprestimoCancelado
     * @return Emprestimo
     */
    public function setEmprestimoCancelado($emprestimoCancelado)
    {
        $this->emprestimoCancelado = $emprestimoCancelado;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuario
     * @return Emprestimo
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuarioDevolucao()
    {
        return $this->usuarioDevolucao;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuarioDevolucao
     * @return Emprestimo
     */
    public function setUsuarioDevolucao($usuarioDevolucao)
    {
        $this->usuarioDevolucao = $usuarioDevolucao;

        return $this;
    }

    /**
     * @return Exemplar
     */
    public function getExemplar()
    {
        return $this->exemplar;
    }

    /**
     * @param Exemplar $exemplar
     * @return Emprestimo
     */
    public function setExemplar($exemplar)
    {
        $this->exemplar = $exemplar;

        return $this;
    }

    /**
     * @return ModalidadeEmprestimo
     */
    public function getModalidadeEmprestimo()
    {
        return $this->modalidadeEmprestimo;
    }

    /**
     * @param ModalidadeEmprestimo $modalidadeEmprestimo
     * @return Emprestimo
     */
    public function setModalidadeEmprestimo($modalidadeEmprestimo)
    {
        $this->modalidadeEmprestimo = $modalidadeEmprestimo;

        return $this;
    }

    /**
     * @return Pessoa
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @param Pessoa $pes
     * @return Emprestimo
     */
    public function setPes($pes)
    {
        $this->pes = $pes;

        return $this;
    }
}
