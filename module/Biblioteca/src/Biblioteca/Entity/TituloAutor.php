<?php

namespace Biblioteca\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * TituloAutor
 *
 * @ORM\Table(name="biblioteca__titulo_autor", indexes={@ORM\Index(name="fk_biblioteca__titulo_biblioteca__autor_biblioteca__autor1_idx", columns={"autor_id"}), @ORM\Index(name="fk_biblioteca__titulo_biblioteca__autor_biblioteca__titulo1_idx", columns={"titulo_id"})})
 * @ORM\Entity
 * @LG\LG(id="tituloAutorId",label="tituloAutorId")
 * @Jarvis\Jarvis(title="Lista de autores de títulos",icon="fa fa-table")
 */
class TituloAutor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="titulo_autor_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="titulo_autor_id")
     * @LG\Labels\Attributes(text="Índice")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloAutorId;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo_autor_tipo", type="string", nullable=false)
     * @LG\Labels\Property(name="titulo_autor_tipo")
     * @LG\Labels\Attributes(text="tipo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $tituloAutorTipo;

    /**
     * @var \Biblioteca\Entity\Autor
     *
     * @ORM\ManyToOne(targetEntity="Biblioteca\Entity\Autor")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="autor_id", referencedColumnName="autor_id")
     * })
     */
    private $autor;

    /**
     * @var \Biblioteca\Entity\Titulo
     *
     * @ORM\ManyToOne(targetEntity="Biblioteca\Entity\Titulo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="titulo_id", referencedColumnName="titulo_id")
     * })
     */
    private $titulo;

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \VersaSpine\Stdlib\Hydrator())->extract($this);
    }

    /**
     * @return int
     */
    public function getTituloAutorId()
    {
        return $this->tituloAutorId;
    }

    /**
     * @param int $tituloAutorId
     * @return TituloAutor
     */
    public function setTituloAutorId($tituloAutorId)
    {
        $this->tituloAutorId = $tituloAutorId;

        return $this;
    }

    /**
     * @return string
     */
    public function getTituloAutorTipo()
    {
        return $this->tituloAutorTipo;
    }

    /**
     * @param string $tituloAutorTipo
     * @return TituloAutor
     */
    public function setTituloAutorTipo($tituloAutorTipo)
    {
        $this->tituloAutorTipo = $tituloAutorTipo;

        return $this;
    }

    /**
     * @return Autor
     */
    public function getAutor()
    {
        return $this->autor;
    }

    /**
     * @param Autor $autor
     * @return TituloAutor
     */
    public function setAutor($autor)
    {
        $this->autor = $autor;

        return $this;
    }

    /**
     * @return Titulo
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param Titulo $titulo
     * @return TituloAutor
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }
}
