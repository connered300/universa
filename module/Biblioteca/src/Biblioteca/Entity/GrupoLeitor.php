<?php

namespace Biblioteca\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * GrupoLeitor
 *
 * @ORM\Table(name="biblioteca__grupo_leitor", indexes={@ORM\Index(name="fk_biblioteca__grupo_leitor_acesso_grupo1_idx", columns={"acesso_grupo_id"})})
 * @ORM\Entity
 * @LG\LG(id="grupoLeitorId",label="grupoLeitorNome")
 * @Jarvis\Jarvis(title="Lista de grupos de leitores",icon="fa fa-table")
 */
class GrupoLeitor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="grupo_leitor_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="grupo_leitor_id")
     * @LG\Labels\Attributes(text="Índice")
     * @LG\Querys\Conditions(type="=")
     */
    private $grupoLeitorId;

    /**
     * @var string
     *
     * @ORM\Column(name="grupo_leitor_nome", type="string", length=30, nullable=false)
     * @LG\Labels\Property(name="grupo_leitor_nome")
     * @LG\Labels\Attributes(text="Nome")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $grupoLeitorNome;

    /**
     * @var \Acesso\Entity\AcessoGrupo
     *
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoGrupo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="acesso_grupo_id", referencedColumnName="id")
     * })
     */
    private $acessoGrupo;

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \VersaSpine\Stdlib\Hydrator())->extract($this);
    }

    /**
     * @return int
     */
    public function getGrupoLeitorId()
    {
        return $this->grupoLeitorId;
    }

    /**
     * @param int $grupoLeitorId
     */
    public function setGrupoLeitorId($grupoLeitorId)
    {
        $this->grupoLeitorId = $grupoLeitorId;
    }

    /**
     * @return string
     */
    public function getGrupoLeitorNome()
    {
        return $this->grupoLeitorNome;
    }

    /**
     * @param string $grupoLeitorNome
     */
    public function setGrupoLeitorNome($grupoLeitorNome)
    {
        $this->grupoLeitorNome = $grupoLeitorNome;
    }

    /**
     * @return \Acesso\Entity\AcessoGrupo
     */
    public function getAcessoGrupo()
    {
        return $this->acessoGrupo;
    }

    /**
     * @param \Acesso\Entity\AcessoGrupo $acessoGrupo
     */
    public function setAcessoGrupo($acessoGrupo)
    {
        $this->acessoGrupo = $acessoGrupo;
    }
}