<?php

namespace Biblioteca\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AreaCnpq
 *
 * @ORM\Table(name="biblioteca__area_cnpq")
 * @ORM\Entity
 * @LG\LG(id="areaCnpqId",label="areaCnpqDescricao")
 * @Jarvis\Jarvis(title="Lista de áreas CNPq",icon="fa fa-table")
 */
class AreaCnpq
{
    /**
     * @var integer
     *
     * @ORM\Column(name="area_cnpq_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="area_cnpq_id")
     * @LG\Labels\Attributes(text="Índice")
     * @LG\Querys\Conditions(type="=")
     */
    private $areaCnpqId;

    /**
     * @var string
     *
     * @ORM\Column(name="area_cnpq_codigo", type="string", length=15, nullable=false)
     * @LG\Labels\Property(name="area_cnpq_codigo")
     * @LG\Labels\Attributes(text="Código")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $areaCnpqCodigo;

    /**
     * @var string
     *
     * @ORM\Column(name="area_cnpq_descricao", type="string", length=100, nullable=false)
     * @LG\Labels\Property(name="area_cnpq_descricao")
     * @LG\Labels\Attributes(text="Descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $areaCnpqDescricao;

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \VersaSpine\Stdlib\Hydrator())->extract($this);
    }

    /**
     * @return int
     */
    public function getAreaCnpqId()
    {
        return $this->areaCnpqId;
    }

    /**
     * @param int $areaCnpqId
     * @return AreaCnpq
     */
    public function setAreaCnpqId($areaCnpqId)
    {
        $this->areaCnpqId = $areaCnpqId;

        return $this;
    }

    /**
     * @return string
     */
    public function getAreaCnpqCodigo()
    {
        return $this->areaCnpqCodigo;
    }

    /**
     * @param string $areaCnpqCodigo
     * @return AreaCnpq
     */
    public function setAreaCnpqCodigo($areaCnpqCodigo)
    {
        $this->areaCnpqCodigo = $areaCnpqCodigo;

        return $this;
    }

    /**
     * @return string
     */
    public function getAreaCnpqDescricao()
    {
        return $this->areaCnpqDescricao;
    }

    /**
     * @param string $areaCnpqDescricao
     * @return AreaCnpq
     */
    public function setAreaCnpqDescricao($areaCnpqDescricao)
    {
        $this->areaCnpqDescricao = $areaCnpqDescricao;

        return $this;
    }
}
