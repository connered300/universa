<?php

namespace Biblioteca\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * Cutter
 *
 * @ORM\Table(name="biblioteca__cutter")
 * @ORM\Entity
 * @LG\LG(id="cutterId",label="cutterNome")
 * @Jarvis\Jarvis(title="Lista de cutter",icon="fa fa-table")
 */
class Cutter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="cutter_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="cutter_id")
     * @LG\Labels\Attributes(text="Índice")
     * @LG\Querys\Conditions(type="=")
     */
    private $cutterId;

    /**
     * @var string
     *
     * @ORM\Column(name="cutter_nome", type="string", length=100, nullable=true)
     * @LG\Labels\Property(name="cutter_nome")
     * @LG\Labels\Attributes(text="Nome")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $cutterNome;
    /**
     * @var integer
     *
     * @ORM\Column(name="cutter_num", type="integer", nullable=true)
     * @LG\Labels\Property(name="cutter_num")
     * @LG\Labels\Attributes(text="Número")
     * @LG\Querys\Conditions(type="=")
     */
    private $cutterNum;

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \VersaSpine\Stdlib\Hydrator())->extract($this);
    }

    /**
     * @return int
     */
    public function getCutterId()
    {
        return $this->cutterId;
    }

    /**
     * @param int $cutterId
     */
    public function setCutterId($cutterId)
    {
        $this->cutterId = $cutterId;
    }

    /**
     * @return string
     */
    public function getCutterNome()
    {
        return $this->cutterNome;
    }

    /**
     * @param string $cutterNome
     */
    public function setCutterNome($cutterNome)
    {
        $this->cutterNome = $cutterNome;
    }

    /**
     * @return int
     */
    public function getCutterNum()
    {
        return $this->cutterNum;
    }

    /**
     * @param int $cutterNum
     */
    public function setCutterNum($cutterNum)
    {
        $this->cutterNum = $cutterNum;
    }
}