<?php

namespace Biblioteca\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * Multa
 *
 * @ORM\Table(name="biblioteca__multa", indexes={@ORM\Index(name="fk_biblioteca__multa_financeiro__titulo1_idx", columns={"titulo_id"})})
 * @ORM\Entity
 * @LG\LG(id="emprestimo_id",label="emprestimo_id")
 * @Jarvis\Jarvis(title="Lista de multas",icon="fa fa-table")
 */
class Multa
{
    /**
     * @var \Financeiro\Entity\FinanceiroTitulo
     *
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\FinanceiroTitulo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="titulo_id", referencedColumnName="titulo_id")
     * })
     */
    private $titulo;

    /**
     * @var \Biblioteca\Entity\Emprestimo
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Biblioteca\Entity\Emprestimo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="emprestimo_id", referencedColumnName="emprestimo_id")
     * })
     */
    private $emprestimo;

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \VersaSpine\Stdlib\Hydrator())->extract($this);
    }

    /**
     * @return \Financeiro\Entity\FinanceiroTitulo
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroTitulo $titulo
     * @return Multa
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * @return Emprestimo
     */
    public function getEmprestimo()
    {
        return $this->emprestimo;
    }

    /**
     * @param Emprestimo $emprestimo
     * @return Multa
     */
    public function setEmprestimo($emprestimo)
    {
        $this->emprestimo = $emprestimo;

        return $this;
    }
}
