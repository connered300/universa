<?php

namespace Biblioteca\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * Setor
 *
 * @ORM\Table(name="biblioteca__setor")
 * @ORM\Entity
 * @LG\LG(id="setorId",label="setorDescricao")
 * @Jarvis\Jarvis(title="Lista de setores",icon="fa fa-table")
 */
class Setor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="setor_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="setor_id")
     * @LG\Labels\Attributes(text="Índice")
     * @LG\Querys\Conditions(type="=")
     */
    private $setorId;

    /**
     * @var string
     *
     * @ORM\Column(name="setor_sigla", type="string", length=5, nullable=true)
     * @LG\Labels\Property(name="setor_sigla")
     * @LG\Labels\Attributes(text="sigla")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $setorSigla;

    /**
     * @var string
     *
     * @ORM\Column(name="setor_descricao", type="string", length=255, nullable=true)
     * @LG\Labels\Property(name="setor_descricao")
     * @LG\Labels\Attributes(text="descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $setorDescricao;

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \VersaSpine\Stdlib\Hydrator())->extract($this);
    }

    /**
     * @return int
     */
    public function getSetorId()
    {
        return $this->setorId;
    }

    /**
     * @param int $setorId
     * @return Setor
     */
    public function setSetorId($setorId)
    {
        $this->setorId = $setorId;

        return $this;
    }

    /**
     * @return string
     */
    public function getSetorDescricao()
    {
        return $this->setorDescricao;
    }

    /**
     * @param string $setorDescricao
     * @return Setor
     */
    public function setSetorDescricao($setorDescricao)
    {
        $this->setorDescricao = $setorDescricao;

        return $this;
    }

    /**
     * @return string
     */
    public function getSetorSigla()
    {
        return $this->setorSigla;
    }

    /**
     * @param string $setorSigla
     * @return Setor
     */
    public function setSetorSigla($setorSigla)
    {
        $this->setorSigla = $setorSigla;

        return $this;
    }
}
