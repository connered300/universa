<?php

namespace Biblioteca\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * Suspensao
 *
 * @ORM\Table(name="biblioteca__suspensao", indexes={@ORM\Index(name="fk_biblioteca_suspensao_acesso_pessoas1_idx", columns={"usuario_id"})})
 * @ORM\Entity
 * @LG\LG(id="emprestimo_id",label="emprestimo_id")
 * @Jarvis\Jarvis(title="Lista de suspenssões",icon="fa fa-table")
 */
class Suspensao
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="suspensao_inicio", type="datetime", nullable=true)
     * @LG\Labels\Property(name="suspensao_inicio")
     * @LG\Labels\Attributes(text="data de início da suspensão")
     * @LG\Querys\Conditions(type="=")
     */
    private $suspensaoInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="suspensao_fim", type="datetime", nullable=true)
     * @LG\Labels\Property(name="suspensao_fim")
     * @LG\Labels\Attributes(text="data do fim da suspenssão")
     * @LG\Querys\Conditions(type="=")
     */
    private $suspensaoFim;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="suspensao_liberacao_data", type="datetime", nullable=true)
     * @LG\Labels\Property(name="suspensao_liberacao_data")
     * @LG\Labels\Attributes(text="data de liberação da suspensão")
     * @LG\Querys\Conditions(type="=")
     */
    private $suspensaoLiberacaoData;

    /**
     * @var string
     *
     * @ORM\Column(name="suspensao_liberacao_motivo", type="string", length=255, nullable=true)
     * @LG\Labels\Property(name="suspensao_liberacao_motivo")
     * @LG\Labels\Attributes(text="motivo da liberação da multa")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $suspensaoLiberacaoMotivo;

    /**
     * @var \Biblioteca\Entity\Emprestimo
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Biblioteca\Entity\Emprestimo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="emprestimo_id", referencedColumnName="emprestimo_id")
     * })
     */
    private $emprestimo;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     *
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * })
     */
    private $usuario;

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \VersaSpine\Stdlib\Hydrator())->extract($this);
    }

    /**
     * @return \DateTime
     */
    public function getSuspensaoInicio()
    {
        return $this->suspensaoInicio;
    }

    /**
     * @param \DateTime $suspensaoInicio
     * @return Suspensao
     */
    public function setSuspensaoInicio($suspensaoInicio)
    {
        $this->suspensaoInicio = $suspensaoInicio;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getSuspensaoFim()
    {
        return $this->suspensaoFim;
    }

    /**
     * @param \DateTime $suspensaoFim
     * @return Suspensao
     */
    public function setSuspensaoFim($suspensaoFim)
    {
        $this->suspensaoFim = $suspensaoFim;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getSuspensaoLiberacaoData()
    {
        return $this->suspensaoLiberacaoData;
    }

    /**
     * @param \DateTime $suspensaoLiberacaoData
     * @return Suspensao
     */
    public function setSuspensaoLiberacaoData($suspensaoLiberacaoData)
    {
        $this->suspensaoLiberacaoData = $suspensaoLiberacaoData;

        return $this;
    }

    /**
     * @return string
     */
    public function getSuspensaoLiberacaoMotivo()
    {
        return $this->suspensaoLiberacaoMotivo;
    }

    /**
     * @param string $suspensaoLiberacaoMotivo
     * @return Suspensao
     */
    public function setSuspensaoLiberacaoMotivo($suspensaoLiberacaoMotivo)
    {
        $this->suspensaoLiberacaoMotivo = $suspensaoLiberacaoMotivo;

        return $this;
    }

    /**
     * @return Emprestimo
     */
    public function getEmprestimo()
    {
        return $this->emprestimo;
    }

    /**
     * @param Emprestimo $emprestimo
     * @return Suspensao
     */
    public function setEmprestimo($emprestimo)
    {
        $this->emprestimo = $emprestimo;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuario
     * @return Suspensao
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }
}
