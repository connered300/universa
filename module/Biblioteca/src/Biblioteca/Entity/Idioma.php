<?php

namespace Biblioteca\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * Idioma
 *
 * @ORM\Table(name="biblioteca__idioma")
 * @ORM\Entity
 * @LG\LG(id="idiomaId",label="idiomaDescricao")
 * @Jarvis\Jarvis(title="Lista de idiomas",icon="fa fa-table")
 */
class Idioma
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idioma_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="idioma_id")
     * @LG\Labels\Attributes(text="Índice")
     * @LG\Querys\Conditions(type="=")
     */
    private $idiomaId;

    /**
     * @var string
     *
     * @ORM\Column(name="idioma_descricao", type="string", length=60, nullable=false)
     * @LG\Labels\Property(name="idioma_descricao")
     * @LG\Labels\Attributes(text="descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $idiomaDescricao;

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \VersaSpine\Stdlib\Hydrator())->extract($this);
    }

    /**
     * @return int
     */
    public function getIdiomaId()
    {
        return $this->idiomaId;
    }

    /**
     * @param int $idiomaId
     * @return Idioma
     */
    public function setIdiomaId($idiomaId)
    {
        $this->idiomaId = $idiomaId;

        return $this;
    }

    /**
     * @return string
     */
    public function getIdiomaDescricao()
    {
        return $this->idiomaDescricao;
    }

    /**
     * @param string $idiomaDescricao
     * @return Idioma
     */
    public function setIdiomaDescricao($idiomaDescricao)
    {
        $this->idiomaDescricao = $idiomaDescricao;

        return $this;
    }
}
