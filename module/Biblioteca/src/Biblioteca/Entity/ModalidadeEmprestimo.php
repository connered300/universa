<?php

namespace Biblioteca\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * ModalidadeEmprestimo
 *
 * @ORM\Table(name="biblioteca__modalidade_emprestimo", indexes={@ORM\Index(name="fk_biblioteca__modalidade_emprestimo_biblioteca__grupo_leit_idx", columns={"grupo_leitor_id"})})
 * @ORM\Entity
 * @LG\LG(id="modalidadeEmprestimoId",label="modalidadeEmprestimoDescricao")
 * @Jarvis\Jarvis(title="Lista de modalidades de emprestimo",icon="fa fa-table")
 */
class ModalidadeEmprestimo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="modalidade_emprestimo_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="modalidade_emprestimo_id")
     * @LG\Labels\Attributes(text="Índice")
     * @LG\Querys\Conditions(type="=")
     */
    private $modalidadeEmprestimoId;

    /**
     * @var string
     *
     * @ORM\Column(name="modalidade_emprestimo_descricao", type="string", length=60, nullable=false)
     * @LG\Labels\Property(name="modalidade_emprestimo_descricao")
     * @LG\Labels\Attributes(text="Descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $modalidadeEmprestimoDescricao;

    /**
     * @var integer
     *
     * @ORM\Column(name="modalidade_emprestimo_dias_suspensao", type="smallint", nullable=false)
     * @LG\Labels\Property(name="modalidade_emprestimo_dias_suspensao")
     * @LG\Labels\Attributes(text="Tempo de suspensão")
     * @LG\Querys\Conditions(type="=")
     */
    private $modalidadeEmprestimoDiasSuspensao;

    /**
     * @var string
     *
     * @ORM\Column(name="modalidade_emprestimo_situacao", type="string", nullable=false)
     * @LG\Labels\Property(name="modalidade_emprestimo_situacao")
     * @LG\Labels\Attributes(text="Situação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $modalidadeEmprestimoSituacao = \Biblioteca\Service\ModalidadeEmprestimo::SITUACAO_ATIVA;

    /**
     * @var \Biblioteca\Entity\GrupoLeitor
     *
     * @ORM\ManyToOne(targetEntity="Biblioteca\Entity\GrupoLeitor")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="grupo_leitor_id", referencedColumnName="grupo_leitor_id")
     * })
     */
    private $grupoLeitor;

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \VersaSpine\Stdlib\Hydrator())->extract($this);
    }

    /**
     * @return int
     */
    public function getModalidadeEmprestimoId()
    {
        return $this->modalidadeEmprestimoId;
    }

    /**
     * @param int $modalidadeEmprestimoId
     */
    public function setModalidadeEmprestimoId($modalidadeEmprestimoId)
    {
        $this->modalidadeEmprestimoId = $modalidadeEmprestimoId;

        return $this;
    }

    /**
     * @return string
     */
    public function getModalidadeEmprestimoDescricao()
    {
        return $this->modalidadeEmprestimoDescricao;
    }

    /**
     * @param string $modalidadeEmprestimoDescricao
     */
    public function setModalidadeEmprestimoDescricao($modalidadeEmprestimoDescricao)
    {
        $this->modalidadeEmprestimoDescricao = $modalidadeEmprestimoDescricao;

        return $this;
    }

    /**
     * @return int
     */
    public function getModalidadeEmprestimoDiasSuspensao()
    {
        return $this->modalidadeEmprestimoDiasSuspensao;
    }

    /**
     * @param int $modalidadeEmprestimoDiasSuspensao
     */
    public function setModalidadeEmprestimoDiasSuspensao($modalidadeEmprestimoDiasSuspensao)
    {
        $this->modalidadeEmprestimoDiasSuspensao = $modalidadeEmprestimoDiasSuspensao;

        return $this;
    }

    /**
     * @return string
     */
    public function getModalidadeEmprestimoSituacao()
    {
        return $this->modalidadeEmprestimoSituacao;
    }

    /**
     * @param string $modalidadeEmprestimoSituacao
     */
    public function setModalidadeEmprestimoSituacao($modalidadeEmprestimoSituacao)
    {
        $this->modalidadeEmprestimoSituacao = $modalidadeEmprestimoSituacao;

        return $this;
    }

    /**
     * @return GrupoLeitor
     */
    public function getGrupoLeitor()
    {
        return $this->grupoLeitor;
    }

    /**
     * @param GrupoLeitor $grupoLeitor
     */
    public function setGrupoLeitor($grupoLeitor)
    {
        $this->grupoLeitor = $grupoLeitor;

        return $this;
    }
}