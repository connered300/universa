<?php

namespace Biblioteca\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * Exemplar
 *
 * @ORM\Table(name="biblioteca__exemplar", indexes={@ORM\Index(name="fk_biblioteca__exemplar_biblioteca__titulo1_idx", columns={"titulo_id"}), @ORM\Index(name="fk_biblioteca__exemplar_biblioteca__setor1_idx", columns={"setor_id"})})
 * @ORM\Entity
 * @LG\LG(id="exemplarId",label="exemplarCodigo")
 * @Jarvis\Jarvis(title="Lista de exemplares",icon="fa fa-table")
 */
class Exemplar
{
    /**
     * @var integer
     *
     * @ORM\Column(name="exemplar_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="exemplar_id")
     * @LG\Labels\Attributes(text="Índice")
     * @LG\Querys\Conditions(type="=")
     */
    private $exemplarId;

    /**
     * @var integer
     *
     * @ORM\Column(name="exemplar_codigo", type="integer", nullable=false)
     * @LG\Labels\Property(name="exemplar_codigo")
     * @LG\Labels\Attributes(text="código")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $exemplarCodigo;

    /**
     * @var string
     *
     * @ORM\Column(name="exemplar_emprestado", type="string", nullable=false)
     * @LG\Labels\Property(name="exemplar_emprestado")
     * @LG\Labels\Attributes(text="emprestado")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $exemplarEmprestado = 'N';

    /**
     * @var string
     *
     * @ORM\Column(name="exemplar_reservado", type="string", nullable=false)
     * @LG\Labels\Property(name="exemplar_reservado")
     * @LG\Labels\Attributes(text="reservado")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $exemplarReservado = 'N';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="exemplar_data_cadastro", type="datetime", nullable=false)
     * @LG\Labels\Property(name="exemplar_data_cadastro")
     * @LG\Labels\Attributes(text="data_cadastro")
     * @LG\Querys\Conditions(type="=")
     */
    private $exemplarDataCadastro;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="exemplar_data_alteracao", type="datetime", nullable=false)
     * @LG\Labels\Property(name="exemplar_data_alteracao")
     * @LG\Labels\Attributes(text="data_alteracao")
     * @LG\Querys\Conditions(type="=")
     */
    private $exemplarDataAlteracao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="exemplar_baixa_data", type="datetime", nullable=true)
     * @LG\Labels\Property(name="baixa_data")
     * @LG\Labels\Attributes(text="data da baixa")
     * @LG\Querys\Conditions(type="=")
     */
    private $exemplarBaixaData;

    /**
     * @var string
     *
     * @ORM\Column(name="exemplar_baixa_motivo", type="string", length=20, nullable=true)
     * @LG\Labels\Property(name="exemplar_baixa_motivo")
     * @LG\Labels\Attributes(text="motivo da baixa")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $exemplarBaixaMotivo;

    /**
     * @var string
     *
     * @ORM\Column(name="exemplar_acesso", type="string", nullable=false)
     * @LG\Labels\Property(name="exemplar_acesso")
     * @LG\Labels\Attributes(text="acesso")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $exemplarAcesso;

    /**
     * @var string
     *
     * @ORM\Column(name="exemplar_localizacao", type="string", length=200, nullable=true)
     * @LG\Labels\Property(name="exemplar_localizacao")
     * @LG\Labels\Attributes(text="localizacao")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $exemplarLocalizacao;

    /**
     * @var \Biblioteca\Entity\Setor
     *
     * @ORM\ManyToOne(targetEntity="Biblioteca\Entity\Setor")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="setor_id", referencedColumnName="setor_id")
     * })
     */
    private $setor;

    /**
     * @var \Biblioteca\Entity\Titulo
     *
     * @ORM\ManyToOne(targetEntity="Biblioteca\Entity\Titulo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="titulo_id", referencedColumnName="titulo_id")
     * })
     */
    private $titulo;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     *
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario", referencedColumnName="id")
     * })
     */
    private $usuario;

    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \VersaSpine\Stdlib\Hydrator())->extract($this);
    }

    /**
     * @return int
     */
    public function getExemplarId()
    {
        return $this->exemplarId;
    }

    /**
     * @param int $exemplarId
     * @return Exemplar
     */
    public function setExemplarId($exemplarId)
    {
        $this->exemplarId = $exemplarId;

        return $this;
    }

    /**
     * @return int
     */
    public function getExemplarCodigo()
    {
        return $this->exemplarCodigo;
    }

    /**
     * @param int $exemplarCodigo
     * @return Exemplar
     */
    public function setExemplarCodigo($exemplarCodigo)
    {
        $this->exemplarCodigo = $exemplarCodigo;

        return $this;
    }

    /**
     * @return string
     */
    public function getExemplarEmprestado()
    {
        return $this->exemplarEmprestado;
    }

    /**
     * @param string $exemplarEmprestado
     * @return Exemplar
     */
    public function setExemplarEmprestado($exemplarEmprestado)
    {
        $this->exemplarEmprestado = $exemplarEmprestado;

        return $this;
    }

    /**
     * @return string
     */
    public function getExemplarReservado()
    {
        return $this->exemplarReservado;
    }

    /**
     * @param string $exemplarReservado
     * @return Exemplar
     */
    public function setExemplarReservado($exemplarReservado)
    {
        $this->exemplarReservado = $exemplarReservado;

        return $this;
    }

    /**
     * @return \Datetime
     */
    public function getExemplarDataCadastro()
    {
        return $this->exemplarDataCadastro;
    }

    /**
     * @param \Datetime $exemplarDataCadastro
     * @return Exemplar
     */
    public function setExemplarDataCadastro($exemplarDataCadastro)
    {
        $this->exemplarDataCadastro = $exemplarDataCadastro;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getExemplarDataAlteracao()
    {
        return $this->exemplarDataAlteracao;
    }

    /**
     * @param \DateTime $exemplarDataAlteracao
     * @return Exemplar
     */
    public function setExemplarDataAlteracao($exemplarDataAlteracao)
    {
        $this->exemplarDataAlteracao = $exemplarDataAlteracao;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getExemplarBaixaData()
    {
        return $this->exemplarBaixaData;
    }

    /**
     * @param \DateTime $exemplarBaixaData
     * @return Exemplar
     */
    public function setExemplarBaixaData($exemplarBaixaData)
    {
        $this->exemplarBaixaData = $exemplarBaixaData;

        return $this;
    }

    /**
     * @return string
     */
    public function getExemplarBaixaMotivo()
    {
        return $this->exemplarBaixaMotivo;
    }

    /**
     * @param string $exemplarBaixaMotivo
     * @return Exemplar
     */
    public function setExemplarBaixaMotivo($exemplarBaixaMotivo)
    {
        $this->exemplarBaixaMotivo = $exemplarBaixaMotivo;

        return $this;
    }

    /**
     * @return string
     */
    public function getExemplarAcesso()
    {
        return $this->exemplarAcesso;
    }

    /**
     * @param string $exemplarAcesso
     * @return Exemplar
     */
    public function setExemplarAcesso($exemplarAcesso)
    {
        $this->exemplarAcesso = $exemplarAcesso;

        return $this;
    }

    /**
     * @return string
     */
    public function getExemplarLocalizacao()
    {
        return $this->exemplarLocalizacao;
    }

    /**
     * @param string $exemplarLocalizacao
     * @return Exemplar
     */
    public function setExemplarLocalizacao($exemplarLocalizacao)
    {
        $this->exemplarLocalizacao = $exemplarLocalizacao;

        return $this;
    }

    /**
     * @return Setor
     */
    public function getSetor()
    {
        return $this->setor;
    }

    /**
     * @param Setor $setor
     * @return Exemplar
     */
    public function setSetor($setor)
    {
        $this->setor = $setor;

        return $this;
    }

    /**
     * @return Titulo
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param Titulo $titulo
     * @return Exemplar
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuario
     * @return Exemplar
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }
}
