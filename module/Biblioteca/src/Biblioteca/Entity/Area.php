<?php

namespace Biblioteca\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * Area
 *
 * @ORM\Table(name="biblioteca__area")
 * @ORM\Entity
 * @LG\LG(id="areaId",label="autorReferencia")
 * @Jarvis\Jarvis(title="Lista de áreas",icon="fa fa-table")
 */
class Area
{
    /**
     * @var integer
     *
     * @ORM\Column(name="area_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="area_id")
     * @LG\Labels\Attributes(text="Índice")
     * @LG\Querys\Conditions(type="=")
     */
    private $areaId;

    /**
     * @var string
     *
     * @ORM\Column(name="area_descricao", type="string", length=100, nullable=false)
     * @LG\Labels\Property(name="area_descricao")
     * @LG\Labels\Attributes(text="descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $areaDescricao;

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \VersaSpine\Stdlib\Hydrator())->extract($this);
    }

    /**
     * @return int
     */
    public function getAreaId()
    {
        return $this->areaId;
    }

    /**
     * @param int $areaId
     * @return Area
     */
    public function setAreaId($areaId)
    {
        $this->areaId = $areaId;

        return $this;
    }

    /**
     * @return string
     */
    public function getAreaDescricao()
    {
        return $this->areaDescricao;
    }

    /**
     * @param string $areaDescricao
     * @return Area
     */
    public function setAreaDescricao($areaDescricao)
    {
        $this->areaDescricao = $areaDescricao;

        return $this;
    }
}
