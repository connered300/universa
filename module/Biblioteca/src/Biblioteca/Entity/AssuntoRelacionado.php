<?php

namespace Biblioteca\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AssuntoRelacionado
 *
 * @ORM\Table(name="biblioteca__assunto_relacionado", indexes={@ORM\Index(name="fk_biblioteca__assunto_biblioteca__assunto_biblioteca__assu_idx", columns={"assunto_id_destino"}), @ORM\Index(name="fk_biblioteca__assunto_biblioteca__assunto_biblioteca__assu_idx1", columns={"assunto_id_origem"})})
 * @ORM\Entity
 * @LG\LG(id="assuntoRelacionadoId",label="assuntoRelacionadoId")
 * @Jarvis\Jarvis(title="Lista de assuntos relacionandos",icon="fa fa-table")
 */
class AssuntoRelacionado
{
    /**
     * @var integer
     *
     * @ORM\Column(name="assunto_relacionado_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="assunto_relacionado_id")
     * @LG\Labels\Attributes(text="Índice")
     * @LG\Querys\Conditions(type="=")
     */
    private $assuntoRelacionadoId;

    /**
     * @var \Biblioteca\Entity\Assunto
     *
     * @ORM\ManyToOne(targetEntity="Biblioteca\Entity\Assunto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="assunto_id_origem", referencedColumnName="assunto_id")
     * })
     */
    private $assuntoOrigem;

    /**
     * @var \Biblioteca\Entity\Assunto
     *
     * @ORM\ManyToOne(targetEntity="Biblioteca\Entity\Assunto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="assunto_id_destino", referencedColumnName="assunto_id")
     * })
     */
    private $assuntoDestino;

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \VersaSpine\Stdlib\Hydrator())->extract($this);
    }

    /**
     * @return int
     */
    public function getAssuntoRelacionadoId()
    {
        return $this->assuntoRelacionadoId;
    }

    /**
     * @param int $assuntoRelacionadoId
     * @return AssuntoRelacionado
     */
    public function setAssuntoRelacionadoId($assuntoRelacionadoId)
    {
        $this->assuntoRelacionadoId = $assuntoRelacionadoId;

        return $this;
    }

    /**
     * @return Assunto
     */
    public function getAssuntoOrigem()
    {
        return $this->assuntoOrigem;
    }

    /**
     * @param Assunto $assuntoOrigem
     * @return AssuntoRelacionado
     */
    public function setAssuntoOrigem($assuntoOrigem)
    {
        $this->assuntoOrigem = $assuntoOrigem;

        return $this;
    }

    /**
     * @return Assunto
     */
    public function getAssuntoDestino()
    {
        return $this->assuntoDestino;
    }

    /**
     * @param Assunto $assuntoDestino
     * @return AssuntoRelacionado
     */
    public function setAssuntoDestino($assuntoDestino)
    {
        $this->assuntoDestino = $assuntoDestino;

        return $this;
    }
}
