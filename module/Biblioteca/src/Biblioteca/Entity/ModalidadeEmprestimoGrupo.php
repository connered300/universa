<?php

namespace Biblioteca\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * ModalidadeEmprestimoGrupo
 *
 * @ORM\Table(name="biblioteca__modalidade_emprestimo_grupo", indexes={@ORM\Index(name="fk_biblioteca__modalidade_emprestimo_copy1_biblioteca__moda_idx", columns={"modalidade_emprestimo_id"}), @ORM\Index(name="fk_biblioteca__modalidade_emprestimo_copy1_biblioteca__grup_idx", columns={"grupo_bibliografico_id"})})
 * @ORM\Entity
 * @LG\LG(id="modalidadeEmprestimoGrupoId",label="modalidadeEmprestimoGrupoId")
 * @Jarvis\Jarvis(title="Lista de grupos bibliograficos de modalidades de emprestimos",icon="fa fa-table")
 */
class ModalidadeEmprestimoGrupo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="modalidade_emprestimo_grupo_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="modalidade_emprestimo_grupo_id")
     * @LG\Labels\Attributes(text="Índice")
     * @LG\Querys\Conditions(type="=")
     */
    private $modalidadeEmprestimoGrupoId;

    /**
     * @var integer
     *
     * @ORM\Column(name="modalidade_emprestimo_numero_volumes", type="smallint", nullable=false)
     * @LG\Labels\Property(name="modalidade_emprestimo_numero_volumes")
     * @LG\Labels\Attributes(text="Numero de volumes")
     * @LG\Querys\Conditions(type="=")
     */
    private $modalidadeEmprestimoNumeroVolumes;

    /**
     * @var integer
     *
     * @ORM\Column(name="modalidade_emprestimo_prazo_maximo", type="smallint", nullable=false)
     * @LG\Labels\Property(name="modalidade_emprestimo_prazo_maximo")
     * @LG\Labels\Attributes(text="Prazo máximo")
     * @LG\Querys\Conditions(type="=")
     */
    private $modalidadeEmprestimoPrazoMaximo;

    /**
     * @var string
     *
     * @ORM\Column(name="modalidade_emprestimo_valor_multa", type="decimal", precision=19, scale=4, nullable=false)
     * @LG\Labels\Property(name="modalidade_emprestimo_valor_multa")
     * @LG\Labels\Attributes(text="Valor da multa")
     * @LG\Querys\Conditions(type="=")
     */
    private $modalidadeEmprestimoValorMulta;

    /**
     * @var string
     *
     * @ORM\Column(name="modalidade_emprestimo_dias_nao_uteis", type="string", nullable=true)
     * @LG\Labels\Property(name="modalidade_emprestimo_dias_nao_uteis")
     * @LG\Labels\Attributes(text="dias não úteis incluídos na multa")
     * @LG\Querys\Conditions(type="=")
     */
    private $modalidadeEmprestimoDiasNaoUteis;

    /**
     * @var integer
     *
     * @ORM\Column(name="modalidade_emprestimo_tempo_reserva", type="integer", nullable=false)
     * @LG\Labels\Property(name="modalidade_emprestimo_tempo_reserva")
     * @LG\Labels\Attributes(text="Tempo de vida de uma reserva")
     * @LG\Querys\Conditions(type="=")
     */
    private $modalidadeEmprestimoTempoReserva;

    /**
     * @var string
     *
     * @ORM\Column(name="modalidade_emprestimo_grupo_situacao", type="string", nullable=false)
     * @LG\Labels\Property(name="modalidade_emprestimo_grupo_situacao")
     * @LG\Labels\Attributes(text="Situação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $modalidadeEmprestimoGrupoSituacao = \Biblioteca\Service\ModalidadeEmprestimoGrupo::SITUACAO_ATIVA;

    /**
     * @var \Biblioteca\Entity\GrupoBibliografico
     *
     * @ORM\ManyToOne(targetEntity="Biblioteca\Entity\GrupoBibliografico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="grupo_bibliografico_id", referencedColumnName="grupo_bibliografico_id")
     * })
     */
    private $grupoBibliografico;

    /**
     * @var \Biblioteca\Entity\ModalidadeEmprestimo
     *
     * @ORM\ManyToOne(targetEntity="Biblioteca\Entity\ModalidadeEmprestimo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="modalidade_emprestimo_id", referencedColumnName="modalidade_emprestimo_id")
     * })
     */
    private $modalidadeEmprestimo;

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \VersaSpine\Stdlib\Hydrator())->extract($this);
    }

    /**
     * @return int
     */
    public function getModalidadeEmprestimoGrupoId()
    {
        return $this->modalidadeEmprestimoGrupoId;
    }

    /**
     * @param int $modalidadeEmprestimoGrupoId
     */
    public function setModalidadeEmprestimoGrupoId($modalidadeEmprestimoGrupoId)
    {
        $this->modalidadeEmprestimoGrupoId = $modalidadeEmprestimoGrupoId;
    }

    /**
     * @return int
     */
    public function getModalidadeEmprestimoNumeroVolumes()
    {
        return $this->modalidadeEmprestimoNumeroVolumes;
    }

    /**
     * @param int $modalidadeEmprestimoNumeroVolumes
     */
    public function setModalidadeEmprestimoNumeroVolumes($modalidadeEmprestimoNumeroVolumes)
    {
        $this->modalidadeEmprestimoNumeroVolumes = $modalidadeEmprestimoNumeroVolumes;
    }

    /**
     * @return int
     */
    public function getModalidadeEmprestimoPrazoMaximo()
    {
        return $this->modalidadeEmprestimoPrazoMaximo;
    }

    /**
     * @param int $modalidadeEmprestimoPrazoMaximo
     */
    public function setModalidadeEmprestimoPrazoMaximo($modalidadeEmprestimoPrazoMaximo)
    {
        $this->modalidadeEmprestimoPrazoMaximo = $modalidadeEmprestimoPrazoMaximo;
    }

    /**
     * @return string
     */
    public function getModalidadeEmprestimoValorMulta($format = true)
    {
        if (!$format) {
            return ($this->modalidadeEmprestimoValorMulta);
        }

        return \Boleto\Service\Moeda::decToMoeda($this->modalidadeEmprestimoValorMulta);
    }

    /**
     * @param string $modalidadeEmprestimoValorMulta
     */
    public function setModalidadeEmprestimoValorMulta($modalidadeEmprestimoValorMulta)
    {
        $this->modalidadeEmprestimoValorMulta = \Boleto\Service\Moeda::moedaToDec($modalidadeEmprestimoValorMulta);
    }

    /**
     * @return string
     */
    public function getModalidadeEmprestimoDiasNaoUteis()
    {
        return $this->modalidadeEmprestimoDiasNaoUteis;
    }

    /**
     * @param string $modalidadeEmprestimoDiasNaoUteis
     */
    public function setModalidadeEmprestimoDiasNaoUteis($modalidadeEmprestimoDiasNaoUteis)
    {
        $this->modalidadeEmprestimoDiasNaoUteis = $modalidadeEmprestimoDiasNaoUteis;
    }

    /**
     * @return int
     */
    public function getModalidadeEmprestimoTempoReserva()
    {
        return $this->modalidadeEmprestimoTempoReserva;
    }

    /**
     * @param int $modalidadeEmprestimoTempoReserva
     */
    public function setModalidadeEmprestimoTempoReserva($modalidadeEmprestimoTempoReserva)
    {
        $this->modalidadeEmprestimoTempoReserva = $modalidadeEmprestimoTempoReserva;
    }

    /**
     * @return string
     */
    public function getModalidadeEmprestimoGrupoSituacao()
    {
        return $this->modalidadeEmprestimoGrupoSituacao;
    }

    /**
     * @param string $modalidadeEmprestimoGrupoSituacao
     */
    public function setModalidadeEmprestimoGrupoSituacao($modalidadeEmprestimoGrupoSituacao)
    {
        $this->modalidadeEmprestimoGrupoSituacao = $modalidadeEmprestimoGrupoSituacao;
    }

    /**
     * @return GrupoBibliografico
     */
    public function getGrupoBibliografico()
    {
        return $this->grupoBibliografico;
    }

    /**
     * @param GrupoBibliografico $grupoBibliografico
     */
    public function setGrupoBibliografico($grupoBibliografico)
    {
        $this->grupoBibliografico = $grupoBibliografico;
    }

    /**
     * @return ModalidadeEmprestimo
     */
    public function getModalidadeEmprestimo()
    {
        return $this->modalidadeEmprestimo;
    }

    /**
     * @param ModalidadeEmprestimo $modalidadeEmprestimo
     */
    public function setModalidadeEmprestimo($modalidadeEmprestimo)
    {
        $this->modalidadeEmprestimo = $modalidadeEmprestimo;
    }
}