<?php

namespace Biblioteca\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * TituloAssunto
 *
 * @ORM\Table(name="biblioteca__titulo_assunto", indexes={@ORM\Index(name="fk_biblioteca__assunto_biblioteca__titulo_biblioteca__titul_idx", columns={"titulo_id"}), @ORM\Index(name="fk_biblioteca__assunto_biblioteca__titulo_biblioteca__assun_idx", columns={"assunto_id"})})
 * @ORM\Entity
 * @LG\LG(id="tituloAssuntoId",label="tituloAssuntoId")
 * @Jarvis\Jarvis(title="Lista de assuntos dos títulos",icon="fa fa-table")
 */
class TituloAssunto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="titulo_assunto_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="titulo_assunto_id")
     * @LG\Labels\Attributes(text="Índice")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloAssuntoId;

    /**
     * @var \Biblioteca\Entity\Assunto
     *
     * @ORM\ManyToOne(targetEntity="Biblioteca\Entity\Assunto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="assunto_id", referencedColumnName="assunto_id")
     * })
     */
    private $assunto;

    /**
     * @var \Biblioteca\Entity\Titulo
     *
     * @ORM\ManyToOne(targetEntity="Biblioteca\Entity\Titulo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="titulo_id", referencedColumnName="titulo_id")
     * })
     */
    private $titulo;

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \VersaSpine\Stdlib\Hydrator())->extract($this);
    }

    /**
     * @return int
     */
    public function getTituloAssuntoId()
    {
        return $this->tituloAssuntoId;
    }

    /**
     * @param int $tituloAssuntoId
     * @return TituloAssunto
     */
    public function setTituloAssuntoId($tituloAssuntoId)
    {
        $this->tituloAssuntoId = $tituloAssuntoId;

        return $this;
    }

    /**
     * @return Assunto
     */
    public function getAssunto()
    {
        return $this->assunto;
    }

    /**
     * @param Assunto $assunto
     * @return TituloAssunto
     */
    public function setAssunto($assunto)
    {
        $this->assunto = $assunto;

        return $this;
    }

    /**
     * @return Titulo
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param Titulo $titulo
     * @return TituloAssunto
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }
}
