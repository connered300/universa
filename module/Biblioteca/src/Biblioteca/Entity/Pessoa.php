<?php

namespace Biblioteca\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * Pessoa
 *
 * @ORM\Table(name="biblioteca__pessoa", indexes={@ORM\Index(name="fk_biblioteca__pessoa_biblioteca__grupo_leitor1_idx", columns={"grupo_leitor_id"})})
 * @ORM\Entity
 * @LG\LG(id="pesId",label="pesId")
 * @Jarvis\Jarvis(title="Lista de pessoas",icon="fa fa-table")
 */
class Pessoa
{
    /**
     * @var string
     *
     * @ORM\Column(name="pessoa_senha", type="string", length=35, nullable=true)
     * @LG\Labels\Property(name="pessoa_senha")
     * @LG\Labels\Attributes(text="senha")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $pessoaSenha;

    /**
     * @var string
     *
     * @ORM\Column(name="pessoa_observacao", type="text", nullable=true)
     * @LG\Labels\Property(name="pessoa_observacao")
     * @LG\Labels\Attributes(text="Observação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $pessoaObservacao;

    /**
     * @var string
     *
     * @ORM\Column(name="pessoa_situacao", type="string", nullable=true)
     * @LG\Labels\Property(name="pessoa_situacao")
     * @LG\Labels\Attributes(text="Situação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $pessoaSituacao;

    /**
     * @var \Biblioteca\Entity\GrupoLeitor
     *
     * @ORM\ManyToOne(targetEntity="Biblioteca\Entity\GrupoLeitor")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="grupo_leitor_id", referencedColumnName="grupo_leitor_id")
     * })
     */
    private $grupoLeitor;

    /**
     * @var \Pessoa\Entity\PessoaFisica
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Pessoa\Entity\PessoaFisica")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     */
    private $pes;

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \VersaSpine\Stdlib\Hydrator())->extract($this);
    }

    /**
     * @return string
     */
    public function getPessoaSenha()
    {
        return $this->pessoaSenha;
    }

    /**
     * @param string $pessoaSenha
     * @return Pessoa
     */
    public function setPessoaSenha($pessoaSenha)
    {
        $this->pessoaSenha = $pessoaSenha;

        return $this;
    }

    /**
     * @return string
     */
    public function getPessoaObservacao()
    {
        return $this->pessoaObservacao;
    }

    /**
     * @param string $pessoaObservacao
     * @return Pessoa
     */
    public function setPessoaObservacao($pessoaObservacao)
    {
        $this->pessoaObservacao = $pessoaObservacao;

        return $this;
    }

    /**
     * @return string
     */
    public function getPessoaSituacao()
    {
        return $this->pessoaSituacao;
    }

    /**
     * @param string $pessoaSituacao
     * @return Pessoa
     */
    public function setPessoaSituacao($pessoaSituacao)
    {
        $this->pessoaSituacao = $pessoaSituacao;

        return $this;
    }

    /**
     * @return GrupoLeitor
     */
    public function getGrupoLeitor()
    {
        return $this->grupoLeitor;
    }

    /**
     * @param GrupoLeitor $grupoLeitor
     * @return Pessoa
     */
    public function setGrupoLeitor($grupoLeitor)
    {
        $this->grupoLeitor = $grupoLeitor;

        return $this;
    }

    /**
     * @return \Pessoa\Entity\PessoaFisica
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @param \Pessoa\Entity\PessoaFisica $pes
     * @return Pessoa
     */
    public function setPes($pes)
    {
        $this->pes = $pes;

        return $this;
    }
}
