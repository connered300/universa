<?php

namespace Biblioteca\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * TituloIdioma
 *
 * @ORM\Table(name="biblioteca__titulo_idioma", indexes={@ORM\Index(name="fk_biblioteca__titulo_biblioteca__idioma_biblioteca__idioma_idx", columns={"idioma_id"}), @ORM\Index(name="fk_biblioteca__titulo_biblioteca__idioma_biblioteca__titulo_idx", columns={"titulo_id"})})
 * @ORM\Entity
 * @LG\LG(id="tituloIdiomaId",label="tituloIdiomaId")
 * @Jarvis\Jarvis(title="Lista de idiomas do título",icon="fa fa-table")
 */
class TituloIdioma
{
    /**
     * @var integer
     *
     * @ORM\Column(name="titulo_idioma_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="titulo_idioma_id")
     * @LG\Labels\Attributes(text="Índice")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloIdiomaId;

    /**
     * @var \Biblioteca\Entity\Idioma
     *
     * @ORM\ManyToOne(targetEntity="Biblioteca\Entity\Idioma")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idioma_id", referencedColumnName="idioma_id")
     * })
     */
    private $idioma;

    /**
     * @var \Biblioteca\Entity\Titulo
     *
     * @ORM\ManyToOne(targetEntity="Biblioteca\Entity\Titulo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="titulo_id", referencedColumnName="titulo_id")
     * })
     */
    private $titulo;

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \VersaSpine\Stdlib\Hydrator())->extract($this);
    }

    /**
     * @return int
     */
    public function getTituloIdiomaId()
    {
        return $this->tituloIdiomaId;
    }

    /**
     * @param int $tituloIdiomaId
     * @return TituloIdioma
     */
    public function setTituloIdiomaId($tituloIdiomaId)
    {
        $this->tituloIdiomaId = $tituloIdiomaId;

        return $this;
    }

    /**
     * @return Idioma
     */
    public function getIdioma()
    {
        return $this->idioma;
    }

    /**
     * @param Idioma $idioma
     * @return TituloIdioma
     */
    public function setIdioma($idioma)
    {
        $this->idioma = $idioma;

        return $this;
    }

    /**
     * @return Titulo
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param Titulo $titulo
     * @return TituloIdioma
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }
}
