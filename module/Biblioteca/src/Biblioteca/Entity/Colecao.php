<?php

namespace Biblioteca\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * Colecao
 *
 * @ORM\Table(name="biblioteca__colecao", indexes={@ORM\Index(name="fk_biblioteca__colecao_biblioteca__empresa1_idx", columns={"pes_id"})})
 * @ORM\Entity
 * @LG\LG(id="colecaoId",label="colecaoDescricao")
 * @Jarvis\Jarvis(title="Lista de coleções",icon="fa fa-table")
 */
class Colecao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="colecao_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="colecao_id")
     * @LG\Labels\Attributes(text="Índice")
     * @LG\Querys\Conditions(type="=")
     */
    private $colecaoId;

    /**
     * @var string
     *
     * @ORM\Column(name="colecao_descricao", type="string", length=200, nullable=true)
     * @LG\Labels\Property(name="colecao_descricao")
     * @LG\Labels\Attributes(text="descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $colecaoDescricao;

    /**
     * @var \Biblioteca\Entity\Empresa
     *
     * @ORM\ManyToOne(targetEntity="Biblioteca\Entity\Empresa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     */
    private $pes;

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \VersaSpine\Stdlib\Hydrator())->extract($this);
    }

    /**
     * @return int
     */
    public function getColecaoId()
    {
        return $this->colecaoId;
    }

    /**
     * @param int $colecaoId
     * @return Colecao
     */
    public function setColecaoId($colecaoId)
    {
        $this->colecaoId = $colecaoId;

        return $this;
    }

    /**
     * @return string
     */
    public function getColecaoDescricao()
    {
        return $this->colecaoDescricao;
    }

    /**
     * @param string $colecaoDescricao
     * @return Colecao
     */
    public function setColecaoDescricao($colecaoDescricao)
    {
        $this->colecaoDescricao = $colecaoDescricao;

        return $this;
    }

    /**
     * @return Empresa
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @param Empresa $pes
     * @return Colecao
     */
    public function setPes($pes)
    {
        $this->pes = $pes;

        return $this;
    }
}
