<?php

namespace Biblioteca\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AquisicaoExemplar
 *
 * @ORM\Table(name="biblioteca__aquisicao_exemplar", indexes={@ORM\Index(name="fk_biblioteca__aquisicao_biblioteca__exemplar_biblioteca__e_idx", columns={"exemplar_id"}), @ORM\Index(name="fk_biblioteca__aquisicao_biblioteca__exemplar_biblioteca__a_idx", columns={"aquisicao_id"})})
 * @ORM\Entity
 * @LG\LG(id="aquisicaoExemplarId",label="aquisicaoExemplarId")
 * @Jarvis\Jarvis(title="Lista de aquisições de exemplares",icon="fa fa-table")
 */
class AquisicaoExemplar
{
    /**
     * @var integer
     *
     * @ORM\Column(name="aquisicao_exemplar_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="aquisicao_exemplar_id")
     * @LG\Labels\Attributes(text="Índice")
     * @LG\Querys\Conditions(type="=")
     */
    private $aquisicaoExemplarId;

    /**
     * @var \Biblioteca\Entity\Aquisicao
     *
     * @ORM\ManyToOne(targetEntity="Biblioteca\Entity\Aquisicao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="aquisicao_id", referencedColumnName="aquisicao_id")
     * })
     */
    private $aquisicao;

    /**
     * @var \Biblioteca\Entity\Exemplar
     *
     * @ORM\ManyToOne(targetEntity="Biblioteca\Entity\Exemplar")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="exemplar_id", referencedColumnName="exemplar_id")
     * })
     */
    private $exemplar;

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \VersaSpine\Stdlib\Hydrator())->extract($this);
    }

    /**
     * @return int
     */
    public function getAquisicaoExemplarId()
    {
        return $this->aquisicaoExemplarId;
    }

    /**
     * @param int $aquisicaoExemplarId
     * @return AquisicaoExemplar
     */
    public function setAquisicaoExemplarId($aquisicaoExemplarId)
    {
        $this->aquisicaoExemplarId = $aquisicaoExemplarId;

        return $this;
    }

    /**
     * @return Aquisicao
     */
    public function getAquisicao()
    {
        return $this->aquisicao;
    }

    /**
     * @param Aquisicao $aquisicao
     * @return AquisicaoExemplar
     */
    public function setAquisicao($aquisicao)
    {
        $this->aquisicao = $aquisicao;

        return $this;
    }

    /**
     * @return Exemplar
     */
    public function getExemplar()
    {
        return $this->exemplar;
    }

    /**
     * @param Exemplar $exemplar
     * @return AquisicaoExemplar
     */
    public function setExemplar($exemplar)
    {
        $this->exemplar = $exemplar;

        return $this;
    }
}
