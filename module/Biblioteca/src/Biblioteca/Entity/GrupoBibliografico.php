<?php

namespace Biblioteca\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * GrupoBibliografico
 *
 * @ORM\Table(name="biblioteca__grupo_bibliografico")
 * @ORM\Entity
 * @LG\LG(id="$grupoBibliograficoId",label="grupoBibliograficoNome")
 * @Jarvis\Jarvis(title="Lista de grupos bibliográficos",icon="fa fa-table")
 */
class GrupoBibliografico
{
    /**
     * @var integer
     *
     * @ORM\Column(name="grupo_bibliografico_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="grupo_bibliografico_id")
     * @LG\Labels\Attributes(text="Índice")
     * @LG\Querys\Conditions(type="=")
     */
    private $grupoBibliograficoId;

    /**
     * @var string
     *
     * @ORM\Column(name="grupo_bibliografico_nome", type="string", length=60, nullable=false)
     * @LG\Labels\Property(name="grupo_bibliografico_nome")
     * @LG\Labels\Attributes(text="Nome")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $grupoBibliograficoNome;

    /**
     * @var string
     *
     * @ORM\Column(name="grupo_bibliografico_situacao", type="string", nullable=false)
     * @LG\Labels\Property(name="grupo_bibliografico_situacao")
     * @LG\Labels\Attributes(text="Situação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $grupoBibliograficoSituacao;

    /**
     * @var string
     *
     * @ORM\Column(name="grupo_bibliografico_local_edicao", type="string", columnDefinition="ENUM('Sim', 'Não')", nullable=false)
     * @LG\Labels\Property(name="grupo_bibliografico_local_edicao")
     * @LG\Labels\Attributes(text="Sim/Não")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $grupoBibliograficoLocalEdicao;

    /**
     * @var string
     *
     * @ORM\Column(name="grupo_bibliografico_exemplares",  type="string", columnDefinition="ENUM('Sim', 'Não')", nullable=false)
     * @LG\Labels\Property(name="grupo_bibliografico_exemplares")
     * @LG\Labels\Attributes(text="Quantidade")
     * @LG\Querys\Conditions(type="like")
     */
    private $grupoBibliograficoExemplares;

    /**
     * @var string
     *
     * @ORM\Column(name="grupo_bibliografico_autor",  type="string", columnDefinition="ENUM('Sim', 'Não')", nullable=false)
     * @LG\Labels\Property(name="grupo_bibliografico_autor")
     * @LG\Labels\Attributes(text="Nome autor")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $grupoBibliograficoAutor;

    /**
     * @var string
     *
     * @ORM\Column(name="grupo_bibliografico_acesso_portal",  type="string", columnDefinition="ENUM('Sim', 'Não')", nullable=false)
     * @LG\Labels\Property(name="grupo_bibliografico_acesso_portal")
     * @LG\Labels\Attributes(text="Sim/Não")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $grupoBibliograficoAcessoPortal;

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \VersaSpine\Stdlib\Hydrator())->extract($this);
    }

    /**
     * @return int
     */
    public function getGrupoBibliograficoId()
    {
        return $this->grupoBibliograficoId;
    }

    /**
     * @param int $grupoBibliograficoId
     */
    public function setGrupoBibliograficoId($grupoBibliograficoId)
    {
        $this->grupoBibliograficoId = $grupoBibliograficoId;

        return $this;
    }

    /**
     * @return string
     */
    public function getGrupoBibliograficoNome()
    {
        return $this->grupoBibliograficoNome;
    }

    /**
     * @param string $grupoBibliograficoNome
     */
    public function setGrupoBibliograficoNome($grupoBibliograficoNome)
    {
        $this->grupoBibliograficoNome = $grupoBibliograficoNome;

        return $this;
    }

    /**
     * @return string
     */
    public function getGrupoBibliograficoSituacao()
    {
        return $this->grupoBibliograficoSituacao;
    }

    /**
     * @param string $grupoBibliograficoSituacao
     */
    public function setGrupoBibliograficoSituacao($grupoBibliograficoSituacao)
    {
        $this->grupoBibliograficoSituacao = $grupoBibliograficoSituacao;

        return $this;
    }

    /**
     * @return string
     */
    public function getGrupoBibliograficoLocalEdicao()
    {
        return $this->grupoBibliograficoLocalEdicao;
    }

    /**
     * @return boolean
     */
    public function possuiLocalEdicao()
    {
        return $this->grupoBibliograficoLocalEdicao == \Biblioteca\Service\GrupoBibliografico::GRUPO_BIBLIOGRAFICO_SIM;
    }

    /**
     * @param string $grupoBibliograficoLocalEdicao
     */
    public function setGrupoBibliograficoLocalEdicao($grupoBibliograficoLocalEdicao)
    {
        $this->grupoBibliograficoLocalEdicao = $grupoBibliograficoLocalEdicao;

        return $this;
    }

    /**
     * @return string
     */
    public function getGrupoBibliograficoExemplares()
    {
        return $this->grupoBibliograficoExemplares;
    }

    /**
     * @return boolean
     */
    public function possuiExemplares()
    {
        return $this->grupoBibliograficoExemplares == \Biblioteca\Service\GrupoBibliografico::GRUPO_BIBLIOGRAFICO_SIM;
    }

    /**
     * @param string $grupoBibliograficoExemplares
     */
    public function setGrupoBibliograficoExemplares($grupoBibliograficoExemplares)
    {
        $this->grupoBibliograficoExemplares = $grupoBibliograficoExemplares;

        return $this;
    }

    /**
     * @return string
     */
    public function getGrupoBibliograficoAutor()
    {
        return $this->grupoBibliograficoAutor;
    }
    /**
     * @return boolean
     */
    public function possuiAutor()
    {
        return $this->grupoBibliograficoAutor == \Biblioteca\Service\GrupoBibliografico::GRUPO_BIBLIOGRAFICO_SIM;
    }

    /**
     * @param string $grupoBibliograficoAutor
     */
    public function setGrupoBibliograficoAutor($grupoBibliograficoAutor)
    {
        $this->grupoBibliograficoAutor = $grupoBibliograficoAutor;

        return $this;
    }

    /**
     * @return string
     */
    public function getGrupoBibliograficoAcessoPortal()
    {
        return $this->grupoBibliograficoAcessoPortal;
    }

    /**
     * @return boolean
     */
    public function possuiAcessoPortal()
    {
        return $this->grupoBibliograficoAcessoPortal == \Biblioteca\Service\GrupoBibliografico::GRUPO_BIBLIOGRAFICO_SIM;
    }

    /**
     * @param string $grupoBibliograficoAcessoPortal
     */
    public function setGrupoBibliograficoAcessoPortal($grupoBibliograficoAcessoPortal)
    {
        $this->grupoBibliograficoAcessoPortal = $grupoBibliograficoAcessoPortal;

        return $this;
    }

}