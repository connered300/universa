<?php

namespace Biblioteca\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * Aquisicao
 *
 * @ORM\Table(name="biblioteca__aquisicao", indexes={@ORM\Index(name="fk_biblioteca__aquisicao_biblioteca__pessoa1_idx", columns={"pes_id_doador"}), @ORM\Index(name="fk_biblioteca__aquisicao_biblioteca__empresa1_idx", columns={"pes_id_fornecedor"}), @ORM\Index(name="fk_biblioteca__aquisicao_biblioteca__titulo1_idx", columns={"titulo_id"})})
 * @ORM\Entity
 * @LG\LG(id="aquisicaoId",label="aquisicaoId")
 * @Jarvis\Jarvis(title="Lista de aquisições",icon="fa fa-table")
 */
class Aquisicao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="aquisicao_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="aquisicao_id")
     * @LG\Labels\Attributes(text="Índice")
     * @LG\Querys\Conditions(type="=")
     */
    private $aquisicaoId;

    /**
     * @var string
     *
     * @ORM\Column(name="aquisicao_tipo", type="string", nullable=true)
     * @LG\Labels\Property(name="aquisicao_tipo")
     * @LG\Labels\Attributes(text="tipo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $aquisicaoTipo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="aquisicao_data", type="date", nullable=false)
     * @LG\Labels\Property(name="aquisicao_data")
     * @LG\Labels\Attributes(text="data")
     * @LG\Querys\Conditions(type="=")
     */
    private $aquisicaoData;

    /**
     * @var string
     *
     * @ORM\Column(name="aquisicao_nota_fiscal", type="string", length=50, nullable=true)
     * @LG\Labels\Property(name="aquisicao_nota_fiscal")
     * @LG\Labels\Attributes(text="NF")
     * @LG\Querys\Conditions(type="=")
     */
    private $aquisicaoNotaFiscal;

    /**
     * @var string
     *
     * @ORM\Column(name="aquisicao_permuta_descricao", type="text", nullable=true)
     * @LG\Labels\Property(name="aquisicao_permuta_descricao")
     * @LG\Labels\Attributes(text="descrição de permuta")
     * @LG\Querys\Conditions(type="=")
     */
    private $aquisicaoPermutaDescricao;

    /**
     * @var float
     *
     * @ORM\Column(name="aquisicao_nota_valor", type="float", precision=10, scale=0, nullable=true)
     * @LG\Labels\Property(name="aquisicao_nota_valor")
     * @LG\Labels\Attributes(text="valor")
     * @LG\Querys\Conditions(type="=")
     */
    private $aquisicaoNotaValor;

    /**
     * @var string
     *
     * @ORM\Column(name="aquisicao_observacao", type="text", nullable=true)
     * @LG\Labels\Property(name="aquisicao_observacao")
     * @LG\Labels\Attributes(text="observação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $aquisicaoObservacao;

    /**
     * @var \Biblioteca\Entity\Empresa
     *
     * @ORM\ManyToOne(targetEntity="Biblioteca\Entity\Empresa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id_fornecedor", referencedColumnName="pes_id")
     * })
     */
    private $pesFornecedor;

    /**
     * @var \Biblioteca\Entity\Pessoa
     *
     * @ORM\ManyToOne(targetEntity="Biblioteca\Entity\Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id_doador", referencedColumnName="pes_id")
     * })
     */
    private $pesDoador;

    /**
     * @var \Biblioteca\Entity\Titulo
     *
     * @ORM\ManyToOne(targetEntity="Biblioteca\Entity\Titulo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="titulo_id", referencedColumnName="titulo_id")
     * })
     */
    private $titulo;

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \VersaSpine\Stdlib\Hydrator())->extract($this);
    }

    /**
     * @return int
     */
    public function getAquisicaoId()
    {
        return $this->aquisicaoId;
    }

    /**
     * @param int $aquisicaoId
     * @return Aquisicao
     */
    public function setAquisicaoId($aquisicaoId)
    {
        $this->aquisicaoId = $aquisicaoId;

        return $this;
    }

    /**
     * @return string
     */
    public function getAquisicaoTipo()
    {
        return $this->aquisicaoTipo;
    }

    /**
     * @param string $aquisicaoTipo
     * @return Aquisicao
     */
    public function setAquisicaoTipo($aquisicaoTipo)
    {
        $this->aquisicaoTipo = $aquisicaoTipo;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getAquisicaoData()
    {
        return $this->aquisicaoData;
    }

    /**
     * @param \DateTime $aquisicaoData
     * @return Aquisicao
     */
    public function setAquisicaoData($aquisicaoData)
    {
        $this->aquisicaoData = $aquisicaoData;

        return $this;
    }

    /**
     * @return string
     */
    public function getAquisicaoNotaFiscal()
    {
        return $this->aquisicaoNotaFiscal;
    }

    /**
     * @param string $aquisicaoNotaFiscal
     * @return Aquisicao
     */
    public function setAquisicaoNotaFiscal($aquisicaoNotaFiscal)
    {
        $this->aquisicaoNotaFiscal = $aquisicaoNotaFiscal;

        return $this;
    }

    /**
     * @return string
     */
    public function getAquisicaoPermutaDescricao()
    {
        return $this->aquisicaoPermutaDescricao;
    }

    /**
     * @param string $aquisicaoPermutaDescricao
     * @return Aquisicao
     */
    public function setAquisicaoPermutaDescricao($aquisicaoPermutaDescricao)
    {
        $this->aquisicaoPermutaDescricao = $aquisicaoPermutaDescricao;

        return $this;
    }

    /**
     * @return float
     */
    public function getAquisicaoNotaValor()
    {
        return $this->aquisicaoNotaValor;
    }

    /**
     * @param float $aquisicaoNotaValor
     * @return Aquisicao
     */
    public function setAquisicaoNotaValor($aquisicaoNotaValor)
    {
        $this->aquisicaoNotaValor = $aquisicaoNotaValor;

        return $this;
    }

    /**
     * @return string
     */
    public function getAquisicaoObservacao()
    {
        return $this->aquisicaoObservacao;
    }

    /**
     * @param string $aquisicaoObservacao
     * @return Aquisicao
     */
    public function setAquisicaoObservacao($aquisicaoObservacao)
    {
        $this->aquisicaoObservacao = $aquisicaoObservacao;

        return $this;
    }

    /**
     * @return Empresa
     */
    public function getPesFornecedor()
    {
        return $this->pesFornecedor;
    }

    /**
     * @param Empresa $pesFornecedor
     * @return Aquisicao
     */
    public function setPesFornecedor($pesFornecedor)
    {
        $this->pesFornecedor = $pesFornecedor;

        return $this;
    }

    /**
     * @return Pessoa
     */
    public function getPesDoador()
    {
        return $this->pesDoador;
    }

    /**
     * @param Pessoa $pesDoador
     * @return Aquisicao
     */
    public function setPesDoador($pesDoador)
    {
        $this->pesDoador = $pesDoador;

        return $this;
    }

    /**
     * @return Titulo
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param Titulo $titulo
     * @return Aquisicao
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }
}
