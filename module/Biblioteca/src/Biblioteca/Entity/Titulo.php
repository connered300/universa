<?php

namespace Biblioteca\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * Titulo
 *
 * @ORM\Table(name="biblioteca__titulo", indexes={@ORM\Index(name="fk_biblioteca__titulo_biblioteca__grupo_bibliografico1_idx", columns={"grupo_bibliografico_id"}), @ORM\Index(name="fk_biblioteca__titulo_biblioteca__area_cnpq1_idx", columns={"area_cnpq_id"}), @ORM\Index(name="fk_biblioteca__titulo_biblioteca__area1_idx", columns={"area_id"}), @ORM\Index(name="fk_biblioteca__titulo_biblioteca__empresa1_idx", columns={"pes_id_editora"}), @ORM\Index(name="fk_biblioteca__titulo_biblioteca__colecao1_idx", columns={"colecao_id"})})
 * @ORM\Entity
 * @LG\LG(id="tituloId",label="tituloTitulo")
 * @Jarvis\Jarvis(title="Lista de títulos",icon="fa fa-table")
 */
class Titulo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="titulo_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="titulo_id")
     * @LG\Labels\Attributes(text="Índice")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="titulo_data_cadastro", type="datetime", nullable=false)
     * @LG\Labels\Property(name="titulo_data_cadastro")
     * @LG\Labels\Attributes(text="data de cadastro")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloDataCadastro;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="titulo_data_alteracao", type="datetime", nullable=true)
     * @LG\Labels\Property(name="titulo_data_alteracao")
     * @LG\Labels\Attributes(text="data de alteração")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloDataAlteracao;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo_titulo", type="text", nullable=false)
     * @LG\Labels\Property(name="titulo_titulo")
     * @LG\Labels\Attributes(text="título")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $tituloTitulo;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo_descricao", type="text", nullable=true)
     * @LG\Labels\Property(name="titulo_descricao")
     * @LG\Labels\Attributes(text="descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $tituloDescricao;

    /**
     * @var integer
     *
     * @ORM\Column(name="titulo_edicao_ano", type="integer", nullable=true)
     * @LG\Labels\Property(name="titulo_edicao_ano")
     * @LG\Labels\Attributes(text="ano de edição")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloEdicaoAno;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo_edicao_local", type="string", length=150, nullable=true)
     * @LG\Labels\Property(name="titulo_edicao_local")
     * @LG\Labels\Attributes(text="local de edição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $tituloEdicaoLocal;

    /**
     * @var integer
     *
     * @ORM\Column(name="titulo_edicao_volume", type="integer", nullable=true)
     * @LG\Labels\Property(name="titulo_edicao_volume")
     * @LG\Labels\Attributes(text="volume da edição")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloEdicaoVolume;

    /**
     * @var integer
     *
     * @ORM\Column(name="titulo_edicao_numero", type="integer", nullable=true)
     * @LG\Labels\Property(name="titulo_edicao_numero")
     * @LG\Labels\Attributes(text="número da edição")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloEdicaoNumero;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo_edicao_isbn", type="string", length=18, nullable=true)
     * @LG\Labels\Property(name="titulo_edicao_isbn")
     * @LG\Labels\Attributes(text="ISBN")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $tituloEdicaoIsbn;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo_edicao_cdu", type="string", length=20, nullable=true)
     * @LG\Labels\Property(name="titulo_edicao_cdu")
     * @LG\Labels\Attributes(text="CDU")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $tituloEdicaoCdu;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo_edicao_cutter", type="string", length=20, nullable=true)
     * @LG\Labels\Property(name="titulo_edicao_cutter")
     * @LG\Labels\Attributes(text="cutter")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $tituloEdicaoCutter;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo_edicao_tomo", type="string", length=20, nullable=true)
     * @LG\Labels\Property(name="titulo_edicao_tomo")
     * @LG\Labels\Attributes(text="tomo da edição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $tituloEdicaoTomo;

    /**
     * @var integer
     *
     * @ORM\Column(name="titulo_paginas", type="integer", nullable=true)
     * @LG\Labels\Property(name="titulo_paginas")
     * @LG\Labels\Attributes(text="número de páginas")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloPaginas;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo_nota", type="text", nullable=true)
     * @LG\Labels\Property(name="titulo_nota")
     * @LG\Labels\Attributes(text="nota")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $tituloNota;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo_requisitos_uso", type="text", nullable=true)
     * @LG\Labels\Property(name="titulo_requisitos_uso")
     * @LG\Labels\Attributes(text="requisitos de uso")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $tituloRequisitosUso;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo_situacao", type="string", nullable=false)
     * @LG\Labels\Property(name="titulo_situacao")
     * @LG\Labels\Attributes(text="situação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $tituloSituacao;

    /**
     * @var \Biblioteca\Entity\Area
     *
     * @ORM\ManyToOne(targetEntity="Biblioteca\Entity\Area")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="area_id", referencedColumnName="area_id")
     * })
     */
    private $area;

    /**
     * @var \Biblioteca\Entity\AreaCnpq
     *
     * @ORM\ManyToOne(targetEntity="Biblioteca\Entity\AreaCnpq")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="area_cnpq_id", referencedColumnName="area_cnpq_id")
     * })
     */
    private $areaCnpq;

    /**
     * @var \Biblioteca\Entity\Colecao
     *
     * @ORM\ManyToOne(targetEntity="Biblioteca\Entity\Colecao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="colecao_id", referencedColumnName="colecao_id")
     * })
     */
    private $colecao;

    /**
     * @var \Biblioteca\Entity\Empresa
     *
     * @ORM\ManyToOne(targetEntity="Biblioteca\Entity\Empresa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id_editora", referencedColumnName="pes_id")
     * })
     */
    private $pesEditora;

    /**
     * @var \Biblioteca\Entity\GrupoBibliografico
     *
     * @ORM\ManyToOne(targetEntity="Biblioteca\Entity\GrupoBibliografico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="grupo_bibliografico_id", referencedColumnName="grupo_bibliografico_id")
     * })
     */
    private $grupoBibliografico;

    /**
     * @var \GerenciadorArquivos\Entity\Arquivo
     *
     * @ORM\ManyToOne(targetEntity="GerenciadorArquivos\Entity\Arquivo", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="arq_id_arquivo", referencedColumnName="arq_id")
     * })
     */
    private $arquivoDigital;

    /**
     * @var \GerenciadorArquivos\Entity\Arquivo
     *
     * @ORM\ManyToOne(targetEntity="GerenciadorArquivos\Entity\Arquivo", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="arq_id_capa", referencedColumnName="arq_id")
     * })
     */
    private $arquivoImagem;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo_acesso_portal", type="string", nullable=false)
     * @LG\Labels\Property(name="titulo_acesso_portal")
     * @LG\Labels\Attributes(text="Acesso ao portal")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $tituloAcessoPortal;

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \VersaSpine\Stdlib\Hydrator())->extract($this);
    }

    /**
     * @return int
     */
    public function getTituloId()
    {
        return $this->tituloId;
    }

    /**
     * @param int $tituloId
     * @return Titulo
     */
    public function setTituloId($tituloId)
    {
        $this->tituloId = $tituloId;

        return $this;
    }

    /**
     * @return Area
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param Area $area
     * @return Titulo
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTituloDataCadastro()
    {
        return $this->tituloDataCadastro;
    }

    /**
     * @param \DateTime $tituloDataCadastro
     * @return Titulo
     */
    public function setTituloDataCadastro($tituloDataCadastro)
    {
        $this->tituloDataCadastro = $tituloDataCadastro;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTituloDataAlteracao()
    {
        return $this->tituloDataAlteracao;
    }

    /**
     * @param \DateTime $tituloDataAlteracao
     * @return Titulo
     */
    public function setTituloDataAlteracao($tituloDataAlteracao)
    {
        $this->tituloDataAlteracao = $tituloDataAlteracao;

        return $this;
    }

    /**
     * @return string
     */
    public function getTituloTitulo()
    {
        return $this->tituloTitulo;
    }

    /**
     * @param string $tituloTitulo
     * @return Titulo
     */
    public function setTituloTitulo($tituloTitulo)
    {
        $this->tituloTitulo = $tituloTitulo;

        return $this;
    }

    /**
     * @return string
     */
    public function getTituloDescricao()
    {
        return $this->tituloDescricao;
    }

    /**
     * @param string $tituloDescricao
     * @return Titulo
     */
    public function setTituloDescricao($tituloDescricao)
    {
        $this->tituloDescricao = $tituloDescricao;

        return $this;
    }

    /**
     * @return int
     */
    public function getTituloEdicaoAno()
    {
        return $this->tituloEdicaoAno;
    }

    /**
     * @param int $tituloEdicaoAno
     * @return Titulo
     */
    public function setTituloEdicaoAno($tituloEdicaoAno)
    {
        $this->tituloEdicaoAno = $tituloEdicaoAno;

        return $this;
    }

    /**
     * @return string
     */
    public function getTituloEdicaoLocal()
    {
        return $this->tituloEdicaoLocal;
    }

    /**
     * @param string $tituloEdicaoLocal
     * @return Titulo
     */
    public function setTituloEdicaoLocal($tituloEdicaoLocal)
    {
        $this->tituloEdicaoLocal = $tituloEdicaoLocal;

        return $this;
    }

    /**
     * @return int
     */
    public function getTituloEdicaoVolume()
    {
        return $this->tituloEdicaoVolume;
    }

    /**
     * @param int $tituloEdicaoVolume
     * @return Titulo
     */
    public function setTituloEdicaoVolume($tituloEdicaoVolume)
    {
        $this->tituloEdicaoVolume = $tituloEdicaoVolume;

        return $this;
    }

    /**
     * @return int
     */
    public function getTituloEdicaoNumero()
    {
        return $this->tituloEdicaoNumero;
    }

    /**
     * @param int $tituloEdicaoNumero
     * @return Titulo
     */
    public function setTituloEdicaoNumero($tituloEdicaoNumero)
    {
        $this->tituloEdicaoNumero = $tituloEdicaoNumero;

        return $this;
    }

    /**
     * @return string
     */
    public function getTituloEdicaoIsbn()
    {
        return $this->tituloEdicaoIsbn;
    }

    /**
     * @param string $tituloEdicaoIsbn
     * @return Titulo
     */
    public function setTituloEdicaoIsbn($tituloEdicaoIsbn)
    {
        $this->tituloEdicaoIsbn = $tituloEdicaoIsbn;

        return $this;
    }

    /**
     * @return string
     */
    public function getTituloEdicaoCdu()
    {
        return $this->tituloEdicaoCdu;
    }

    /**
     * @param string $tituloEdicaoCdu
     * @return Titulo
     */
    public function setTituloEdicaoCdu($tituloEdicaoCdu)
    {
        $this->tituloEdicaoCdu = $tituloEdicaoCdu;

        return $this;
    }

    /**
     * @return string
     */
    public function getTituloEdicaoCutter()
    {
        return $this->tituloEdicaoCutter;
    }

    /**
     * @param string $tituloEdicaoCutter
     * @return Titulo
     */
    public function setTituloEdicaoCutter($tituloEdicaoCutter)
    {
        $this->tituloEdicaoCutter = $tituloEdicaoCutter;

        return $this;
    }

    /**
     * @return string
     */
    public function getTituloEdicaoTomo()
    {
        return $this->tituloEdicaoTomo;
    }

    /**
     * @param string $tituloEdicaoTomo
     * @return Titulo
     */
    public function setTituloEdicaoTomo($tituloEdicaoTomo)
    {
        $this->tituloEdicaoTomo = $tituloEdicaoTomo;

        return $this;
    }

    /**
     * @return int
     */
    public function getTituloPaginas()
    {
        return $this->tituloPaginas;
    }

    /**
     * @param int $tituloPaginas
     * @return Titulo
     */
    public function setTituloPaginas($tituloPaginas)
    {
        $this->tituloPaginas = $tituloPaginas;

        return $this;
    }

    /**
     * @return string
     */
    public function getTituloNota()
    {
        return $this->tituloNota;
    }

    /**
     * @param string $tituloNota
     * @return Titulo
     */
    public function setTituloNota($tituloNota)
    {
        $this->tituloNota = $tituloNota;

        return $this;
    }

    /**
     * @return string
     */
    public function getTituloRequisitosUso()
    {
        return $this->tituloRequisitosUso;
    }

    /**
     * @param string $tituloRequisitosUso
     * @return Titulo
     */
    public function setTituloRequisitosUso($tituloRequisitosUso)
    {
        $this->tituloRequisitosUso = $tituloRequisitosUso;

        return $this;
    }

    /**
     * @return string
     */
    public function getTituloSituacao()
    {
        return $this->tituloSituacao;
    }

    /**
     * @param string $tituloSituacao
     * @return Titulo
     */
    public function setTituloSituacao($tituloSituacao)
    {
        $this->tituloSituacao = $tituloSituacao;

        return $this;
    }

    /**
     * @return AreaCnpq
     */
    public function getAreaCnpq()
    {
        return $this->areaCnpq;
    }

    /**
     * @param AreaCnpq $areaCnpq
     * @return Titulo
     */
    public function setAreaCnpq($areaCnpq)
    {
        $this->areaCnpq = $areaCnpq;

        return $this;
    }

    /**
     * @return Colecao
     */
    public function getColecao()
    {
        return $this->colecao;
    }

    /**
     * @param Colecao $colecao
     * @return Titulo
     */
    public function setColecao($colecao)
    {
        $this->colecao = $colecao;

        return $this;
    }

    /**
     * @return Empresa
     */
    public function getPesEditora()
    {
        return $this->pesEditora;
    }

    /**
     * @param Empresa $pesEditora
     * @return Titulo
     */
    public function setPesEditora($pesEditora)
    {
        $this->pesEditora = $pesEditora;

        return $this;
    }

    /**
     * @return GrupoBibliografico
     */
    public function getGrupoBibliografico()
    {
        return $this->grupoBibliografico;
    }

    /**
     * @param GrupoBibliografico $grupoBibliografico
     * @return Titulo
     */
    public function setGrupoBibliografico($grupoBibliografico)
    {
        $this->grupoBibliografico = $grupoBibliografico;

        return $this;
    }

    /**
     * @return \GerenciadorArquivos\Entity\Arquivo
     */
    public function getArquivoDigital()
    {
        return $this->arquivoDigital;
    }

    /**
     * @param \GerenciadorArquivos\Entity\Arquivo $arquivoDigital
     * @return Titulo
     */
    public function setArquivoDigital($arquivoDigital)
    {
        $this->arquivoDigital = $arquivoDigital;

        return $this;
    }

    /**
     * @return \GerenciadorArquivos\Entity\Arquivo
     */
    public function getArquivoImagem()
    {
        return $this->arquivoImagem;
    }

    /**
     * @param \GerenciadorArquivos\Entity\Arquivo $arquivoImagem
     * @return Titulo
     */
    public function setArquivoImagem($arquivoImagem)
    {
        $this->arquivoImagem = $arquivoImagem;

        return $this;
    }

    /**
     * @return String
     */
    public function getTituloAcessoPortal()
    {
        return $this->tituloAcessoPortal;
    }

    /**
     * @param $tituloAcessoPortal
     * @return Titulo
     */
    public function setTituloAcessoPortal($tituloAcessoPortal)
    {
        $this->tituloAcessoPortal = $tituloAcessoPortal;

        return $this;
    }
}
