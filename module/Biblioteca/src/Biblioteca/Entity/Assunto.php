<?php

namespace Biblioteca\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * Assunto
 *
 * @ORM\Table(name="biblioteca__assunto")
 * @ORM\Entity
 * @LG\LG(id="assuntoId",label="assuntoDescricao")
 * @Jarvis\Jarvis(title="Lista de assuntos",icon="fa fa-table")
 */
class Assunto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="assunto_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="assunto_id")
     * @LG\Labels\Attributes(text="Índice")
     * @LG\Querys\Conditions(type="=")
     */
    private $assuntoId;

    /**
     * @var string
     *
     * @ORM\Column(name="assunto_descricao", type="string", length=100, nullable=true)
     * @LG\Labels\Property(name="assunto_descricao")
     * @LG\Labels\Attributes(text="descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $assuntoDescricao;

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \VersaSpine\Stdlib\Hydrator())->extract($this);
    }

    /**
     * @return int
     */
    public function getAssuntoId()
    {
        return $this->assuntoId;
    }

    /**
     * @param int $assuntoId
     * @return Assunto
     */
    public function setAssuntoId($assuntoId)
    {
        $this->assuntoId = $assuntoId;

        return $this;
    }

    /**
     * @return string
     */
    public function getAssuntoDescricao()
    {
        return $this->assuntoDescricao;
    }

    /**
     * @param string $assuntoDescricao
     * @return Assunto
     */
    public function setAssuntoDescricao($assuntoDescricao)
    {
        $this->assuntoDescricao = $assuntoDescricao;

        return $this;
    }
}
