<?php

namespace Biblioteca\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * Empresa
 *
 * @ORM\Table(name="biblioteca__empresa")
 * @ORM\Entity
 * @LG\LG(id="pesId",label="pesId")
 * @Jarvis\Jarvis(title="Lista de empresas",icon="fa fa-table")
 */
class Empresa
{
    /**
     * @var string
     *
     * @ORM\Column(name="empresa_observacao", type="text", nullable=true)
     * @LG\Labels\Property(name="empresa_observacao")
     * @LG\Labels\Attributes(text="Observação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $empresaObservacao;

    /**
     * @var string
     *
     * @ORM\Column(name="empresa_tipo", type="string", nullable=false)
     * @LG\Labels\Property(name="empresa_tipo")
     * @LG\Labels\Attributes(text="Tipo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $empresaTipo;

    /**
     * @var string
     *
     * @ORM\Column(name="empresa_website", type="string", nullable=false)
     * @LG\Labels\Property(name="empresa_website")
     * @LG\Labels\Attributes(text="Website")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $empresaWebsite;

    /**
     * @var \Pessoa\Entity\PessoaJuridica
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Pessoa\Entity\PessoaJuridica")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     */
    private $pes;

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \VersaSpine\Stdlib\Hydrator())->extract($this);
    }

    /**
     * @return string
     */
    public function getEmpresaObservacao()
    {
        return $this->empresaObservacao;
    }

    /**
     * @param string $empresaObservacao
     */
    public function setEmpresaObservacao($empresaObservacao)
    {
        $this->empresaObservacao = $empresaObservacao;
    }

    /**
     * @return string
     */
    public function getEmpresaTipo()
    {
        return $this->empresaTipo;
    }

    /**
     * @param string $empresaTipo
     */
    public function setEmpresaTipo($empresaTipo)
    {
        $this->empresaTipo = $empresaTipo;
    }

    /**
     * @return \Pessoa\Entity\PessoaJuridica
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @param \Pessoa\Entity\PessoaJuridica $pes
     */
    public function setPes($pes)
    {
        $this->pes = $pes;
    }

    /**
     * @return string
     */
    public function getEmpresaWebsite()
    {
        return $this->empresaWebsite;
    }

    /**
     * @param string $empresaWebsite
     */
    public function setEmpresaWebsite($empresaWebsite)
    {
        $this->empresaWebsite = $empresaWebsite;
    }
}