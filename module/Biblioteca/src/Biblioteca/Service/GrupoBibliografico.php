<?php

namespace Biblioteca\Service;

use VersaSpine\Service\AbstractService;

class GrupoBibliografico extends AbstractService
{
    const GRUPO_BIBLIOGRAFICO_SITUACAO_ATIVO   = 'Ativo';
    const GRUPO_BIBLIOGRAFICO_SITUACAO_INATIVO = 'Inativo';
    const SITUACAO_ATIVA                       = 'Ativo';
    const SITUACAO_INATIVA                     = 'Inativo';
    const GRUPO_BIBLIOGRAFICO_SIM              = 'Sim';
    const GRUPO_BIBLIOGRAFICO_NAO              = 'Não';

    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2GrupoBibliograficoSituacao($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getGrupoBibliograficoSituacao());
    }

    public static function getGrupoBibliograficoSituacao()
    {
        return array(self::GRUPO_BIBLIOGRAFICO_SITUACAO_ATIVO, self::GRUPO_BIBLIOGRAFICO_SITUACAO_INATIVO);
    }

    public function getArrSelect2GrupoBibliograficoSimNao($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getGrupoBibliograficoSimNao());
    }

    public static function getGrupoBibliograficoSimNao()
    {
        return array(self::GRUPO_BIBLIOGRAFICO_SIM, self::GRUPO_BIBLIOGRAFICO_NAO);
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Biblioteca\Entity\GrupoBibliografico');
    }

    public function pesquisaForJson($params)
    {
        $sql                    = '
        SELECT
        a.grupoBibliograficoId AS grupo_bibliografico_id,
        a.grupoBibliograficoNome AS grupo_bibliografico_nome,
        a.grupoBibliograficoSituacao AS grupo_bibliografico_situacao
        FROM Biblioteca\Entity\GrupoBibliografico a
        WHERE';
        $grupoBibliograficoNome = false;
        $grupoBibliograficoId   = false;

        if ($params['q']) {
            $grupoBibliograficoNome = $params['q'];
        } elseif ($params['query']) {
            $grupoBibliograficoNome = $params['query'];
        }

        if ($params['grupoBibliograficoId']) {
            $grupoBibliograficoId = $params['grupoBibliograficoId'];
        }

        $parameters = array('grupoBibliograficoNome' => "{$grupoBibliograficoNome}%");
        $sql .= ' a.grupoBibliograficoNome LIKE :grupoBibliograficoNome';

        if ($grupoBibliograficoId) {
            $parameters['grupoBibliograficoId'] = explode(',', $grupoBibliograficoId);
            $parameters['grupoBibliograficoId'] = $grupoBibliograficoId;
            $sql .= ' AND a.grupoBibliograficoId NOT IN(:grupoBibliograficoId)';
        }

        $sql .= " ORDER BY a.grupoBibliograficoNome";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['grupoBibliograficoId']) {
                /** @var $objGrupoBibliografico \Biblioteca\Entity\GrupoBibliografico */
                $objGrupoBibliografico = $this->getRepository()->find($arrDados['grupoBibliograficoId']);

                if (!$objGrupoBibliografico) {
                    $this->setLastError('Registro de grupo bibliográfico não existe!');

                    return false;
                }
            } else {
                $objGrupoBibliografico = new \Biblioteca\Entity\GrupoBibliografico();
            }

            $objGrupoBibliografico->setGrupoBibliograficoNome($arrDados['grupoBibliograficoNome']);
            $objGrupoBibliografico->setGrupoBibliograficoSituacao($arrDados['grupoBibliograficoSituacao']);
            $objGrupoBibliografico->setGrupoBibliograficoLocalEdicao($arrDados['grupoBibliograficoLocalEdicao']);
            $objGrupoBibliografico->setGrupoBibliograficoExemplares($arrDados['grupoBibliograficoExemplares']);
            $objGrupoBibliografico->setGrupoBibliograficoAutor($arrDados['grupoBibliograficoAutor']);
            $objGrupoBibliografico->setGrupoBibliograficoAcessoPortal($arrDados['grupoBibliograficoAcessoPortal']);

            $this->getEm()->persist($objGrupoBibliografico);
            $this->getEm()->flush($objGrupoBibliografico);

            $this->getEm()->commit();

            $arrDados['grupoBibliograficoId'] = $objGrupoBibliografico->getGrupoBibliograficoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de grupo bibliográfico!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['grupoBibliograficoNome']) {
            $errors[] = 'Por favor preencha o campo "nome"!';
        }

        if (!in_array($arrParam['grupoBibliograficoSituacao'], self::getGrupoBibliograficoSituacao())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "Situação"!';
        }

        if (!in_array($arrParam['grupoBibliograficoLocalEdicao'], self::getGrupoBibliograficoSimNao())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "Possui Local de Edição"!';
        }

        if (!in_array($arrParam['grupoBibliograficoExemplares'], self::getGrupoBibliograficoSimNao())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "Possui Exemplares"!';
        }

        if (!in_array($arrParam['grupoBibliograficoAutor'], self::getGrupoBibliograficoSimNao())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "Possui Autor"!';
        }

        if (!in_array($arrParam['grupoBibliograficoAcessoPortal'], self::getGrupoBibliograficoSimNao())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "Permitir Acesso Portal"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM biblioteca__grupo_bibliografico";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($grupoBibliograficoId)
    {
        $arrDados = array();

        if (!$grupoBibliograficoId) {
            $this->setLastError('Grupo bibliográfico inválido!');

            return array();
        }

        /** @var $objGrupoBibliografico \Biblioteca\Entity\GrupoBibliografico */
        $objGrupoBibliografico = $this->getRepository()->find($grupoBibliograficoId);

        if (!$objGrupoBibliografico) {
            $this->setLastError('Grupo bibliográfico não existe!');

            return array();
        }

        try {
            $arrDados = $objGrupoBibliografico->toArray();
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array('grupoBibliograficoSituacao' => self::SITUACAO_ATIVA);

        if ($params['id']) {
            $arrParam['grupoBibliograficoId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['grupoBibliograficoNome' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Biblioteca\Entity\GrupoBibliografico */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getGrupoBibliograficoId();
            $arrEntity[$params['value']] = $objEntity->getGrupoBibliograficoNome();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($grupoBibliograficoId)
    {
        if (!$grupoBibliograficoId) {
            $this->setLastError('Para remover um registro de grupo bibliográfico é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objGrupoBibliografico \Biblioteca\Entity\GrupoBibliografico */
            $objGrupoBibliografico = $this->getRepository()->find($grupoBibliograficoId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objGrupoBibliografico);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de grupo bibliográfico.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $view->setVariable("arrGrupoBibliograficoSituacao", $this->getArrSelect2GrupoBibliograficoSituacao());
    }

    public function getDescricao($id)
    {
        if (count($id) > 1) {
            $id = '"' . implode($id, ',') . '"';
        }

        $query = "select group_concat(grupo_bibliografico_nome SEPARATOR ', ') as grupo
                  from biblioteca__grupo_bibliografico
                  where grupo_bibliografico_id in ({$id})";

        $result = $this->executeQuery($query)->fetch();

        return $result['grupo'];
    }
}