<?php

namespace Biblioteca\Service;

use VersaSpine\Service\AbstractService;

class Cutter extends AbstractService
{
    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Biblioteca\Entity\Cutter');
    }

    public function pesquisaForJson($params)
    {
        $sql        = 'SELECT * FROM biblioteca__cutter';
        $q          = false;
        $parameters = array();

        if ($params['q']) {
            $q = $params['q'];
        } elseif ($params['query']) {
            $q = $params['query'];
        }

        if ($q) {
            $parameters = array('cutter_nome' => "{$q}%", 'cutter_num' => "{$q}%");
            $sql .= ' WHERE cutter_nome LIKE :cutter_nome or cutter_num LIKE :cutter_num or CONCAT(SUBSTRING(cutter_nome,1,1),cutter_num) LIKE :cutter_num';
        }

        $sql .= " ORDER BY cutter_nome";
        $sql .= " LIMIT 0, 40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function buscaDadosCutter($cutterNum, $cutterLetraInicial)
    {
        $sql = 'SELECT * FROM biblioteca__cutter
                WHERE cutter_nome LIKE :cutter_nome AND cutter_num = :cutter_num';

        $parameters = array('cutter_nome' => "{$cutterLetraInicial}%", 'cutter_num' => "{$cutterNum}");

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    protected function valida($dados)
    {
    }
}