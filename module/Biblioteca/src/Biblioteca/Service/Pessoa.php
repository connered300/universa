<?php

namespace Biblioteca\Service;

use Pessoa\Service\TipoContato;
use Respect\Validation\Validator;
use VersaSpine\Service\AbstractService;

/**
 * Class Pessoa
 * @package Biblioteca\Service
 */
class Pessoa extends AbstractService
{
    /**
     * @var null
     */
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Biblioteca\Entity\Pessoa');
    }

    public function pesquisaForJson($params)
    {
        $sql     = '
        SELECT
            p.pes_id,
            p.pes_nome,
            pf.pes_cpf,
            aa.aluno_id,
            aac.alunocurso_id,
            bp.grupo_leitor_id grupoLeitorId,
            bp.grupo_leitor_id,
            IF(grupo_leitor_id IS NULL OR pessoa_situacao<>"Ativo", "inativo", "ativo") situacao,
            IF(
                grupo_leitor_id IS NULL,
                "SEM GRUPO DE LEITOR",
                IF(pessoa_situacao<>"Ativo", "LEITOR INATIVADO", "ATIVO")
            ) situacao_motivo
        FROM biblioteca__pessoa bp
            INNER JOIN pessoa_fisica pf ON pf.pes_id = bp.pes_id
            INNER JOIN pessoa p ON p.pes_id = bp.pes_id
            LEFT JOIN acadgeral__aluno aa ON aa.pes_id=bp.pes_id
            LEFT JOIN acadgeral__aluno_curso aac ON aac.aluno_id=aa.aluno_id
        WHERE 1=1';
        $pesNome = false;
        $pesId   = false;
        $leitor  = false;

        if ($params['q']) {
            $pesNome = $params['q'];
        } elseif ($params['query']) {
            $pesNome = trim($params['query']);
        }

        if ($params['pesId']) {
            $pesId = trim($params['pesId']);
        }

        if ($params['leitor']) {
            $leitor = $params['leitor'];
        }

        if ($leitor) {
            $parameters = array(
                'pes_nome'      => "{$pesNome}%",
                'alunocurso_id' => ltrim($pesNome, '0') . "%",
                'pes_id'        => ltrim($pesNome, '0') . "%"
            );
            $sql .= ' AND (alunocurso_id*1 LIKE :alunocurso_id OR p.pes_id*1 LIKE :pes_id OR pes_nome COLLATE UTF8_GENERAL_CI LIKE :pes_nome)';
        } else {
            $parameters = array('pes_nome' => "{$pesNome}%");
            $sql .= ' AND (pes_nome COLLATE UTF8_GENERAL_CI LIKE :pes_nome OR pes_cpf COLLATE UTF8_GENERAL_CI LIKE :pes_nome)';

            if ($pesId) {
                $parameters['pes_id'] = $pesId;
                $sql .= ' AND pes_id <> :pes_id';
            }
        }

        $sql .= " ORDER BY pes_nome";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function getJSONListingData($data)
    {
        $query = "
    SELECT
        p.pes_id, p.pes_nome, p.pes_tipo, p.pes_nacionalidade,
        pf.pes_cpf, pf.pes_cpf_emissao, pf.pes_rg, pf.pes_rg_emissao, pf.pes_sobrenome, pf.pes_sexo, pf.pes_data_nascimento, pf.pes_estado_civil, pf.pes_filhos, pf.pes_naturalidade, pf.pes_doc_estrangeiro, pf.pes_emissor_rg, pf.pes_nasc_uf,
        en.end_id, en.tipo_endereco, en.end_pais, en.end_estado, en.end_tipo_end, en.end_bairro, en.end_cidade, en.end_cep,
        en.end_logradouro, en.end_tipo_logradouro, en.end_numero, en.end_complemento, en.end_data,
        p.con_contato_email,
        p.con_contato_telefone,
        p.con_contato_celular,
        gp.pessoa_senha, gp.pessoa_observacao, gp.pessoa_situacao,
        gl.grupo_leitor_id,gl.grupo_leitor_nome,
        aa.aluno_id,
        aac.alunocurso_id
    FROM biblioteca__pessoa gp
        LEFT JOIN biblioteca__grupo_leitor gl ON gl.grupo_leitor_id=gp.grupo_leitor_id
        INNER JOIN pessoa_fisica pf ON pf.pes_id = gp.pes_id
        INNER JOIN pessoa p ON p.pes_id = gp.pes_id
        LEFT JOIN endereco en ON en.pes_id=gp.pes_id
        LEFT JOIN acadgeral__aluno aa ON aa.pes_id=gp.pes_id
        LEFT JOIN acadgeral__aluno_curso aac ON aac.aluno_id=aa.aluno_id
    GROUP BY gp.pes_id
    ORDER BY p.pes_id asc
    ";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param bool|false $pesId
     * @param array      $arrDados
     * @return \Pessoa\Entity\PessoaFisica
     */
    private function vinculaPFPessoa($pesId = false, $arrDados = array())
    {
        $objPessoaFisica = null;

        if ($pesId) {
            $objPessoaFisica = new \Pessoa\Service\PessoaFisica($this->getEm());
            $objPessoaFisica = $objPessoaFisica->getRepository()->find($pesId);
        }

        if (!$objPessoaFisica) {
            $objPessoaFisica = new \Pessoa\Entity\PessoaFisica($arrDados);
        }

        $objPessoa = $this->vinculaPessoa($arrDados['pesId'], $arrDados);

        $objPessoaFisica->setPes($objPessoa);
        $objPessoaFisica->setPesCpf($arrDados['pesCpf']);

        $this->getEm()->persist($objPessoaFisica);
        $this->getEm()->flush($objPessoaFisica);

        return $objPessoaFisica;
    }

    /**
     * @param bool|false $pesId
     * @param array      $arrDados
     * @return \Pessoa\Entity\Pessoa
     */
    private function vinculaPessoa($pesId = false, $arrDados = array())
    {
        $objPessoa = null;

        if ($pesId) {
            $objPessoa = new \Pessoa\Service\Pessoa($this->getEm());
            $objPessoa = $objPessoa->getRepository()->find($pesId);
        }

        if (!$objPessoa) {
            $objPessoa = new \Pessoa\Entity\Pessoa($arrDados);
            $objPessoa->setPesNacionalidade('Brasileiro');
            $objPessoa->setPesTipo('Fisica');
        }

        if ($arrDados['pesNome']) {
            $objPessoa->setPesNome($arrDados['pesNome']);
        }

        $this->getEm()->persist($objPessoa);
        $this->getEm()->flush($objPessoa);

        return $objPessoa;
    }

    /**
     * @param $endId
     * @return \Pessoa\Entity\Endereco
     */
    private function vinculaEnderecoPessoa($endId)
    {
        $objEndereco = null;

        if ($endId) {
            $objEndereco = new \Pessoa\Service\Endereco($this->getEm());
            $objEndereco = $objEndereco->getRepository()->find($endId);
        }

        if (!$objEndereco) {
            $objEndereco     = new \Pessoa\Entity\Endereco(array());
            $objTipoEndereco = new \Pessoa\Service\TipoEndereco($this->getEm());

            $objTipoEndereco = $objTipoEndereco->getRepository()->find(1);

            $objEndereco->setTipoEndereco($objTipoEndereco);
            $objEndereco->setEndData('now');
        }

        return $objEndereco;
    }

    /**
     * @param bool|false $conId
     * @param int        $contatoTipo
     * @return \Pessoa\Entity\Contato
     */
    private function vinculaContatoPessoa($conId = false, $contatoTipo = 1)
    {
        $objContato = null;

        if ($conId) {
            $objContato = new \Pessoa\Service\Contato($this->getEm());
            $objContato = $objContato->getRepository()->find($conId);
        }

        if (!$objContato) {
            $objContato     = new \Pessoa\Entity\Contato(array());
            $objTipoContato = new \Pessoa\Service\TipoContato($this->getEm());

            $objTipoContato = $objTipoContato->getRepository()->find($contatoTipo);

            $objContato->setTipoContato($objTipoContato);
        }

        return $objContato;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            $servicePF       = new \Pessoa\Service\PessoaFisica($this->getEm());
            $objPessoaFisica = $servicePF->salvarPessoaFisica($arrDados['pesId'], $arrDados, true, true);

            //Altera/Cria Pessoa Fisica
            if (!$objPessoaFisica) {
                $this->setLastError($servicePF->getLastError());

                return false;
            }

            $objBibliotecaPessoa = null;

            //Altera/Cria Pessoa da biblioteca
            if ($arrDados['pesId']) {
                $objBibliotecaPessoa = $this->getRepository()->find($arrDados['pesId']);
            }

            if (!$objBibliotecaPessoa) {
                $objBibliotecaPessoa = new \Biblioteca\Entity\Pessoa();
            }

            $objBibliotecaPessoa->setPessoaObservacao($arrDados['pessoaObservacao']);
            $objBibliotecaPessoa->setPessoaSituacao($arrDados['pessoaSituacao']);

            if ($arrDados['pessoaSenha']) {
                $objBibliotecaPessoa->setPessoaSenha(sha1($arrDados['pessoaSenha']));
            }

            if ($arrDados['grupoLeitor']) {
                $objGrupoLeitor = new \Biblioteca\Service\GrupoLeitor($this->getEm());
                $objGrupoLeitor = $objGrupoLeitor->getRepository()->find($arrDados['grupoLeitor']);

                if (!$objGrupoLeitor) {
                    $this->setLastError('Grupo de leitor não existe!');

                    return false;
                }

                $objBibliotecaPessoa->setGrupoLeitor($objGrupoLeitor);
            }

            $objBibliotecaPessoa->setPes($objPessoaFisica);

            $this->getEm()->persist($objBibliotecaPessoa);
            $this->getEm()->flush($objBibliotecaPessoa);

            $this->getEm()->commit();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar pessoa!');
        }

        return false;
    }

    /**
     * @param            $pesCpf
     * @param bool|false $pesId
     * @return bool
     */
    public function verificaSeJaExisteCPF($pesCpf, $pesId = false)
    {
        $sql        = 'SELECT count(*) as qtd FROM pessoa_fisica WHERE pes_cpf like :pesCpf';
        $parameters = array('pesCpf' => $pesCpf);

        if ($pesId) {
            $sql .= ' AND pes_id <> :pesId';
            $parameters['pesId'] = $pesId;
        }

        $result = $this->executeQueryWithParam($sql, $parameters)->fetch();

        return $result['qtd'] > 0;
    }

    /**
     * @param            $pesNome
     * @return bool
     */
    public function pesquisaPessoaPorNome($pesNome)
    {
        $sql        = '
    SELECT
        pf.pes_cpf, p.pes_id, p.pes_nome, p.pes_tipo, p.pes_nacionalidade,
        pf.pes_sexo, pf.pes_data_nascimento,
        en.end_estado, en.end_bairro, en.end_cidade, en.end_cep,
        en.end_logradouro, en.end_numero, en.end_complemento
    FROM pessoa_fisica pf
        INNER JOIN pessoa p ON p.pes_id = pf.pes_id
        LEFT JOIN endereco en ON en.pes_id = p.pes_id
        LEFT JOIN biblioteca__pessoa gp ON gp.pes_id = pf.pes_id
    WHERE pes_nome like :pesNome
    GROUP BY p.pes_id
    ORDER BY p.pes_nome asc
    LIMIT 0,15';
        $parameters = array('pesNome' => "$pesNome%");

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        $arrRequerido = array(
            'pesNome'   => 'Nome',
            'endCidade' => 'Cidade',
            'endEstado' => 'Estado',
        );

        foreach ($arrRequerido as $req => $nome) {
            if (!$arrParam[$req]) {
                $errors[] = 'Por favor preencha o campo "' . $nome . '"!';
            }
        }

        if (!empty($arrParam['pesCpf']) && !Validator::cpf()->validate($arrParam['pesCpf'])) {
            $errors[] = "O CPF é inválido!";
        }

        $pesCpf = (int)preg_replace('/[^0-9]/', '', $arrParam['pesCpf']);

        if ($pesCpf != 0 && $this->verificaSeJaExisteCPF($arrParam['pesCpf'], $arrParam['pesId'])) {
            $errors[] =
                "Já existe uma pessoa cadastrada com este CPF. Use a pesquisa de CPF para importa-la para a biblioteca!";
        }

        if (!empty($arrParam['pesSenha']) && $arrParam['pesSenha'] != $arrParam['pesSenhaConfirmacao']) {
            $errors[] = "As senhas digitadas são diferentes!";
        }

        $arrParam['pessoaAluno']   = ($arrParam['pessoaAluno'] == "false" || $arrParam['pessoaAluno'] == "0") ? false : true;
        $arrParam['pessoaUsuario'] = ($arrParam['pessoaUsuario'] == "false" || $arrParam['pessoaUsuario'] == "0") ? false : true;

        if ($arrParam['pessoaAluno'] || $arrParam['pessoaUsuario']) {
            $descricaoNomePessoa       = "do aluno(a)";
            $descricaoGrupoResponsavel = "na secretaria";

            if (!$arrParam['pessoaAluno']) {
                $descricaoNomePessoa       = "desta pessoa";
                $descricaoGrupoResponsavel = "pelo administrador do sistema";
            }

            $arrDataConferir = $this->getArrayPessoaFisica(
                array('pesId' => $arrParam['pesId'], 'ignorarVerificacaoPessoaBiblioteca' => true)
            );

            $pesCpf         = (int)preg_replace('/[^0-9]/', '', $arrParam['pesCpf']);
            $pesCpfConferir = (int)preg_replace('/[^0-9]/', '', $arrDataConferir['pesCpf']);

            if ($arrDataConferir['pesNome'] && $arrParam['pesNome'] != $arrDataConferir['pesNome']) {
                $errors[] = "O nome {$descricaoNomePessoa} só pode ser alterado {$descricaoGrupoResponsavel}!";
            }

            if ($pesCpfConferir != 0 && $arrDataConferir['pesCpf'] && $pesCpf != $pesCpfConferir) {
                $errors[] = "O CPF {$descricaoNomePessoa} só pode ser alterado {$descricaoGrupoResponsavel}!";
            }

            if ($arrParam['pessoaUsuario'] && ($arrParam['pesSenha'] || $arrParam['pesSenhaConfirmacao'])) {
                $errors[] = "A senha de pessoas que possuem usuários só pode ser alterada pelo mesmo, por um administrador ou pela secretaria(alunos)!";
            }
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    /**
     * @param $pesId
     * @return array
     */
    public function getArray($pesId)
    {
        $objPessoa = $this->getRepository()->find($pesId);

        if (!$objPessoa) {
            $this->setLastError('Pessoa não existe!');

            return array();
        }

        $arrDataPessoa = $this->getArrayPessoaFisica(
            array('pesId' => $pesId, 'ignorarVerificacaoPessoaBiblioteca' => true)
        );

        $arrData = array_merge($objPessoa->toArray(), $arrDataPessoa);

        return $arrData;
    }

    /**
     * @param $pesId
     * @return array
     */
    public function getArrayPessoaFisica($param)
    {
        $cpf   = $param['pesCpf'] ? $param['pesCpf'] : false;
        $pesId = $param['pesId'] ? $param['pesId'] : false;

        $ignorarVerificacaoPessoaBiblioteca = $param['ignorarVerificacaoPessoaBiblioteca'] ? $param['ignorarVerificacaoPessoaBiblioteca'] : false;

        $objServicePessoaFisica  = new \Pessoa\Service\PessoaFisica($this->getEm());
        $objServiceEndereco      = new \Pessoa\Service\Endereco($this->getEm());
        $objServiceContato       = new \Pessoa\Service\Contato($this->getEm());
        $objServiceAluno         = new \Matricula\Service\AcadgeralAluno($this->getEm());
        $objServiceAlunoCurso    = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
        $objServiceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());

        try {
            $objPessoaFisicaRepository = $objServicePessoaFisica->getRepository();

            if ($cpf) {
                $objPessoaFisica = $objPessoaFisicaRepository->findOneBy(array('pesCpf' => $cpf));
            } elseif ($pesId) {
                $objPessoaFisica = $objPessoaFisicaRepository->findOneBy(array('pes' => $pesId));
            }

            if (!$objPessoaFisica) {
                $this->setLastError('Pessoa não encontrada no banco de dados!');

                return array();
            }

            $objPessoa = $objPessoaFisica->getPes();
            $pesId     = $objPessoa->getPesId();

            if (!$ignorarVerificacaoPessoaBiblioteca) {
                $objBibliotecaPessoa = $this->getRepository()->find($pesId);

                if ($objBibliotecaPessoa) {
                    $this->setLastError('Pessoa já encontra-se cadastrada, localize-a através da listagem!');

                    return array();
                }
            }

            $objEndereco        = $objServiceEndereco->getRepository()->findOneBy(array('pes' => $pesId));
            $objContatoTelefone = $objServiceContato->getRepository()
                ->findOneBy(array('pessoa' => $pesId, 'tipoContato' => TipoContato::TIPO_CONTATO_TELEFONE));
            $objContatoCelular  = $objServiceContato->getRepository()
                ->findOneBy(array('pessoa' => $pesId, 'tipoContato' => TipoContato::TIPO_CONTATO_CELULAR));
            $objContatoEmail    = $objServiceContato->getRepository()
                ->findOneBy(array('pessoa' => $pesId, 'tipoContato' => TipoContato::TIPO_CONTATO_EMAIL));

            $arrData = array_merge(
                $objPessoa->toArray(),
                $objPessoaFisica->toArray(),
                ($objEndereco ? $objEndereco->toArray() : array())
            );

            if (is_numeric($arrData['endEstado'])) {
                $objServiceEstado = new \Pessoa\Service\Estado($this->getEm());
                $objEstado        = $objServiceEstado->getRepository()->find($arrData['endEstado']);

                if ($objEstado) {
                    $arrData['endEstado'] = $objEstado->getEstNome();
                }
            }

            $objAluno = $objServiceAluno->getRepository()->findOneBy(array('pes' => $pesId));

            $arrData['pessoaAluno'] = (int)false;

            if ($objAluno) {
                $arrData['pessoaAluno'] = (int)true;

                $objAlunoCurso = $objServiceAlunoCurso->getRepository()->findOneBy(
                    array('aluno' => $objAluno->getAlunoId())
                );

                $arrData['pessoaAlunoMatricula'] = ($objAlunoCurso ? $objAlunoCurso->getAlunocursoId() : '');
            }

            $objAcessoPessoas = $objServiceAcessoPessoas->getRepository()->findOneBy(array('pesFisica' => $pesId));

            $arrData['pessoaUsuario'] = (int)false;

            if ($objAcessoPessoas) {
                $arrData['pessoaUsuario'] = (int)true;
            }

            if ($objContatoTelefone) {
                $arrData['conContatoTelefoneId'] = $objContatoTelefone->getConId();
                $arrData['conContatoTelefone']   = $objContatoTelefone->getConContato();
            }

            if ($objContatoCelular) {
                $arrData['conContatoCelularId'] = $objContatoCelular->getConId();
                $arrData['conContatoCelular']   = $objContatoCelular->getConContato();
            }

            if ($objContatoEmail) {
                $arrData['conContatoEmailId'] = $objContatoEmail->getConId();
                $arrData['conContatoEmail']   = $objContatoEmail->getConContato();
            }
        } catch (\Exception $e) {
            $this->setLastError('Erro desconhecido!');

            return array();
        }

        return $arrData;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('pes' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();

        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getPes()->getPes()->getPesId(),
                $params['value'] => $objEntity->getPes()->getPes()->getPesNome()
            );
        }

        return $arrEntitiesArr;
    }

    public function getArrayByPost(&$dadosPessoa)
    {
        if ($dadosPessoa['grupoLeitor'] && is_array($dadosPessoa['grupoLeitor'])) {
            $dadosPessoa['grupoLeitor'] = $dadosPessoa['grupoLeitor']['grupoLeitorId'];
        }
    }

    /**
     * @param $pesId
     * @return array
     */
    public function getArrayDadosLeitor($pesId, $matricula = false)
    {
        /* @var $objPessoa \Biblioteca\Entity\Pessoa */
        $objPessoa = $this->getRepository()->find($pesId);

        if (!$objPessoa) {
            $this->setLastError('Pessoa não existe!');

            return array();
        }

        $objServiceAlunoCurso     = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
        $objServiceEmprestimo     = new \Biblioteca\Service\Emprestimo($this->getEm());
        $objServiceAluno          = new \Matricula\Service\AcadgeralAluno($this->getEm());
        $objServicePeriodoAluno   = new \Matricula\Service\AcadperiodoAluno($this->getEm());
        $objServiceCalendarioData = new \Professor\Service\AcadperiodoCalendarioData($this->getEm());
        $objServiceMulta          = new \Biblioteca\Service\Multa($this->getEm());
        $objServiceSuspensao      = new \Biblioteca\Service\Suspensao($this->getEm());

        $arrData = array(
            'pessoaObservacao'  => $objPessoa->getPessoaObservacao(),
            'pessoaSituacao'    => $objPessoa->getPessoaSituacao(),
            'grupoLeitorId'     => '',
            'grupoLeitorNome'   => 'SEM GRUPO VINCULADO',
            'pesCpf'            => $objPessoa->getPes()->getPesCpf(),
            'pesDataNascimento' => $objPessoa->getPes()->getPesDataNascimento(),
            'pesNome'           => $objPessoa->getPes()->getPes()->getPesNome(),
            'pesId'             => $objPessoa->getPes()->getPes()->getPesId()
        );

        if ($objPessoa->getGrupoLeitor()) {
            $arrData['grupoLeitorId']   = $objPessoa->getGrupoLeitor()->getGrupoLeitorId();
            $arrData['grupoLeitorNome'] = $objPessoa->getGrupoLeitor()->getGrupoLeitorNome();
        }

        $objAluno = $objServiceAluno->getRepository()->findOneBy(array('pes' => $pesId));

        if ($objAluno) {
            $arrData['alunoId'] = $objAluno->getAlunoId();

            $findAlunoCurso = array('aluno' => $objAluno->getAlunoId());

            if ($matricula) {
                $findAlunoCurso = array('alunocursoId' => (integer)$matricula);
            }

            $objAlunoCurso
                = $objServiceAlunoCurso->getRepository()->findOneBy($findAlunoCurso);

            if ($objAlunoCurso) {
                $objCursoCampus      = $objAlunoCurso->getCursoCampus();
                $objCurso            = $objCursoCampus->getCurso();
                $objCampus           = $objCursoCampus->getCamp();
                $objArq              = $objAluno->getArq();
                $objInfoAlunoPeriodo = $objServicePeriodoAluno->buscaAlunoperiodoPorMatricula(
                    $objAlunoCurso->getAlunocursoId()
                );

                $arrData = array_merge(
                    $arrData,
                    array(
                        'alunocursoId' => $objAlunoCurso->getAlunocursoId(),
                        'cursoId'      => $objCurso->getCursoId(),
                        'cursoNome'    => $objCurso->getCursoNome(),
                        'campId'       => $objCampus->getCampId(),
                        'campNome'     => $objCampus->getCampNome(),
                    )
                );

                if ($objArq) {
                    $arrData['imagemAluno'] = $objArq->getArqChave();
                }

                if ($objInfoAlunoPeriodo) {
                    $arrData                      = array_merge($arrData, $objInfoAlunoPeriodo->getTurma()->toArray());
                    $objPeriodoLetivo             = $objInfoAlunoPeriodo->getTurma()->getPer();
                    $arrData['periodoLetivo']     = (
                        $objPeriodoLetivo->getPerSemestre() . "/" .
                        $objPeriodoLetivo->getPerAno()
                    );
                    $arrData['situacaoMatricula'] = $objInfoAlunoPeriodo->getMatSituacao()->getMatsitDescricao();
                }
            }
        }

        $leitorInfoEmprestimosNum       = $objServiceEmprestimo->leitorNumeroDeEmprestimos($arrData['pesId']);
        $leitorInfoEmprestimosAtrasoNum = $objServiceEmprestimo->leitorNumeroDeEmprestimosAtraso($arrData['pesId']);
        $leitorNumeroDeAtrasoSemEntrega = $objServiceEmprestimo->leitorNumeroDeEmprestimosAtrasoSemEntrega(
            $arrData['pesId']
        );
        $leitorInfoEmprestimosInteresse = $objServiceEmprestimo->leitorEmprestimoInteresses($arrData['pesId']);
        $leitorTitulosExemplaresEmPoder = $objServiceEmprestimo->leitorTitulosExemplaresEmPoder($arrData['pesId']);
        $leitorNumeroItensEmprestados   = $objServiceEmprestimo->leitorNumeroItensEmprestadosAtivoPorGrupo(
            $arrData['pesId']
        );
        $leitorInfoEmprestimosAtraso    = round(($leitorInfoEmprestimosAtrasoNum * 100) / $leitorInfoEmprestimosNum, 2);
        $feriados                       = $objServiceCalendarioData->feriadosDepoisDaData(date('Y-m-d'));
        $leitorMultaInfo                = $objServiceMulta->informacaoMultasAbertas($arrData['pesId']);
        $leitorSuspensaoInfo            = $objServiceSuspensao->informacaoSuspensaoAberta($arrData['pesId']);

        $arrData = array_merge(
            $arrData,
            array(
                'leitorInfoEmprestimosNum'       => $leitorInfoEmprestimosNum,
                'leitorInfoEmprestimosAtrasoNum' => $leitorInfoEmprestimosAtrasoNum,
                'leitorNumeroDeAtrasoSemEntrega' => $leitorNumeroDeAtrasoSemEntrega,
                'leitorInfoEmprestimosInteresse' => $leitorInfoEmprestimosInteresse,
                'leitorInfoEmprestimosAtraso'    => $leitorInfoEmprestimosAtraso,
                'leitorNumeroItensEmprestados'   => $leitorNumeroItensEmprestados,
                'leitorTitulosExemplaresEmPoder' => $leitorTitulosExemplaresEmPoder,
                'feriados'                       => $feriados,
                'leitorMultaInfo'                => $leitorMultaInfo,
                'leitorSuspensaoInfo'            => $leitorSuspensaoInfo,
            )
        );

        return $arrData;
    }

    /**
     * @param \Matricula\Entity\AcadgeralAlunoCurso $objAcadgeralAlunoCurso
     * @return bool
     */
    public function removeGrupoLeitor(\Matricula\Entity\AcadgeralAlunoCurso $objAcadgeralAlunoCurso)
    {
        $objPessoaFisica = $objAcadgeralAlunoCurso->getAluno()->getPes();
        $objPessoa       = $this->getRepository()->find($objPessoaFisica->getPes()->getPesId());

        try {
            if ($objPessoa) {
                $this->getEm()->beginTransaction();
                $objPessoa->setGrupoLeitor(null);
                $this->getEm()->persist($objPessoa);
                $this->getEm()->flush($objPessoa);
                $this->getEm()->commit();
            }

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Falha ao remover grupo de leitor!');
        }

        return false;
    }

    /**
     * @param \Matricula\Entity\AcadgeralAlunoCurso $objAcadgeralAlunoCurso
     * @return bool
     */
    public function cadastraLeitorAluno(\Matricula\Entity\AcadgeralAlunoCurso $objAcadgeralAlunoCurso)
    {
        $objPessoaFisica = $objAcadgeralAlunoCurso->getAluno()->getPes();
        $objPessoa       = $this->getRepository()->find($objPessoaFisica->getPes()->getPesId());

        try {
            $this->getEm()->beginTransaction();
            $objGrupoLeitor = new \Biblioteca\Service\GrupoLeitor($this->getEm());
            $objGrupoLeitor = $objGrupoLeitor->getRepository()->find(
                \Biblioteca\Service\GrupoLeitor::GRUPO_ALUNO_GRADUANDO
            );

            if (!$objPessoa) {
                $objPessoa = new \Biblioteca\Entity\Pessoa();
                $objPessoa->setPes($objPessoaFisica);
                $objPessoa->setPessoaSituacao('Ativo');
            }

            if (!$objPessoa->getGrupoLeitor()) {
                $objPessoa->setGrupoLeitor($objGrupoLeitor);
            }

            $this->getEm()->persist($objPessoa);
            $this->getEm()->flush($objPessoa);
            $this->getEm()->commit();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Falha ao registrar leitor!');
        }

        return false;
    }
}