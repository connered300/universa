<?php

namespace Biblioteca\Service;

use VersaSpine\Service\AbstractService;

class Autor extends AbstractService
{
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Biblioteca\Entity\Autor');
    }

    public function pesquisaForJson($params)
    {
        $sql             = 'SELECT * FROM biblioteca__autor WHERE';
        $autorReferencia = false;
        $autorId         = false;

        if ($params['q']) {
            $autorReferencia = $params['q'];
        } elseif ($params['query']) {
            $autorReferencia = $params['query'];
        }

        if ($params['autorId']) {
            $autorId = $params['autorId'];
        }

        $parameters = array('autor_referencia' => "{$autorReferencia}%");
        $sql .= ' autor_referencia LIKE :autor_referencia';

        if ($autorId) {
            $parameters['autor_id'] = $autorId;
            $sql .= ' AND autor_id <> :autor_id';
        }

        $sql .= " ORDER BY autor_referencia";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function getJSONListingData($data)
    {
        $query = "
        SELECT
            a.*,
            -- c.*,
            (select count(DISTINCT titulo_id) from biblioteca__titulo_autor t where t.autor_id=a.autor_id) as autor_titulos
        FROM biblioteca__autor a
        GROUP BY autor_id";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function numeroTitulosDoAutor($autorId)
    {
        $query      = "select count(DISTINCT titulo_id) as qtd from biblioteca__titulo_autor t where t.autor_id=:autorId";
        $parameters = array('autorId' => $autorId);
        $result     = $this->executeQueryWithParam($query, $parameters)->fetch();

        return $result['qtd'];
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->begin();
            $objEntityManager = $this->getEm();
            $objRepository    = $this->getRepository();

            if ($arrDados['autorId']) {
                $objAutor = $objRepository->find($arrDados['autorId']);

                if (!$objAutor) {
                    $this->setLastError('Autor não existe!');

                    return false;
                }
            } else {
                $objAutor = new \Biblioteca\Entity\Autor();
            }

            $objAutor
                ->setAutorCutter($arrDados['autorCutter'])
                ->setAutorReferencia($arrDados['autorReferencia']);

            $objEntityManager->persist($objAutor);
            $objEntityManager->flush();

            $this->commit();

            $arrDados['autorId'] = $objAutor->getAutorId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o autor!');
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        $arrRequerido = array(
            "autorReferencia" => "Referência",
            "autorCutter"     => "Cutter",
        );

        foreach ($arrRequerido as $req => $nome) {
            if (!$arrParam[$req]) {
                $errors[] = "Por favor preencha o campo \"$nome\"!";
            }
        }

        if ($this->verificaSeAutorEstaDuplicado($arrParam['autorReferencia'], $arrParam['autorId'])) {
            $errors[] = "Já existe um autor cadastrado com esta mesma referência!";
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function verificaSeAutorEstaDuplicado($autorReferencia, $autorId = false)
    {
        $sql        = 'SELECT count(*) as qtd FROM biblioteca__autor WHERE autor_referencia like :autorReferencia';
        $parameters = array('autorReferencia' => $autorReferencia);

        if ($autorId) {
            $sql .= ' AND autor_id<>:autorId';
            $parameters['autorId'] = $autorId;
        }

        $result = $this->executeQueryWithParam($sql, $parameters)->fetch();

        return $result['qtd'] > 0;
    }

    public function getArray($autorId)
    {
        $arrData = $this->getReference($autorId);

        try {
            $arrData                 = $arrData->toArray();
            $arrData['autorTitulos'] = $this->numeroTitulosDoAutor($autorId);
        } catch (\Exception $e) {
            return array();
        }

        return $arrData;
    }

    public function getArrayByPost(&$dadosAutor)
    {
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('autorId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();

        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']     => $objEntity->getAutorId(),
                $params['value']   => $objEntity->getAutorReferencia(),
                'autor_id'         => $objEntity->getAutorId(),
                'autor_referencia' => $objEntity->getAutorReferencia(),
                'autor_cutter'     => $objEntity->getAutorCutter()
            );
        }

        return $arrEntitiesArr;
    }
}
