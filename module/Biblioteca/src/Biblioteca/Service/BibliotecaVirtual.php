<?php

namespace Biblioteca\Service;

use Pessoa\Service\TipoContato;
use Respect\Validation\Validator;
use VersaSpine\Service\AbstractService;
use Zend\I18n\Validator\DateTime;
use Zend\View\Model\JsonModel;

/**
 * Class BibliotecaVirtual
 * @package Biblioteca\Service
 */
class BibliotecaVirtual extends AbstractService
{
    const METODO_CRIAR_USUARIO        = "CreatePreRegisterUser";
    const METODO_REMOVER_USUARIO      = "RemovePreRegisterUser";
    const METODO_CRIAR_URL            = "AuthenticatedUrl";
    const USUARIO_CATEGORIA_PROFESSOR = 1;
    const USUARIO_CATEGORIA_ALUNO     = 2;
    const USUARIO_CATEGORIA_OUTROS    = 3;

    /**
     * @var null
     */
    private $lastError = null;

    /** @var \Sistema\Service\SisConfig */
    private $config = null;

    /**
     * @return \Sistema\Service\SisConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param \Sistema\Service\SisConfig $config
     * @return BibliotecaVirtual
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     * @param array $arrConfig
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, $arrConfig = array())
    {
        parent::__construct($em, 'Biblioteca\Entity\BibliotecaVirtual');

        $this->setConfig(new \Sistema\Service\SisConfig($em, $arrConfig));
    }

    /**
     * @param $params
     * @return mixed
     */
    public function pesquisaForJson($params)
    {
        $sql              = '
    SELECT
        p.pes_id, p.pes_nome, pj.pes_nome_fantasia, pj.pes_cnpj, e.empresa_tipo,
        en.end_estado, en.end_cidade
    FROM biblioteca__empresa e
        INNER JOIN pessoa_juridica pj ON pj.pes_id = e.pes_id
        INNER JOIN pessoa p ON p.pes_id = pj.pes_id
        LEFT JOIN endereco en ON en.pes_id=e.pes_id
    WHERE';
        $empresaDescricao = false;
        $empresaId        = false;
        $empresaTipo      = array();

        if ($params['q']) {
            $empresaDescricao = $params['q'];
        } elseif ($params['query']) {
            $empresaDescricao = $params['query'];
        }

        if ($params['empresaId']) {
            $empresaId = $params['empresaId'];
        }

        if ($params['empresaTipo']) {
            $empresaTipo = explode(',', $params['empresaTipo']);
        }

        $parameters = array('empresa_descricao' => "{$empresaDescricao}%");
        $sql .= ' (pes_nome_fantasia COLLATE UTF8_GENERAL_CI LIKE :empresa_descricao)';

        if ($empresaId) {
            $parameters['empresa_id'] = $empresaId;
            $sql .= ' AND e.pes_id <> :empresa_id';
        }

        if ($empresaTipo) {
            $arrConditionsTipo = array();

            foreach ($empresaTipo as $posTipo => $tipoBibliotecaVirtual) {
                $arrConditionsTipo[] = "FIND_IN_SET(:tipo$posTipo, e.empresa_tipo)";

                $parameters['tipo' . $posTipo] = "$tipoBibliotecaVirtual";
            }

            $sql .= ' AND (' . implode(' OR ', $arrConditionsTipo) . ')';
        }

        $sql .= " ORDER BY pj.pes_nome_fantasia, p.pes_nome";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['pesNome']) {
            $errors[] = 'Por favor preencha o campo "nome"!';
        }

        if (!$arrParam['pesSobrenome']) {
            $errors[] = 'Por favor preencha o campo "sobrenome"!';
        }

        if (!$arrParam['conContatoEmail']) {
            $errors[] = 'Por favor preencha o campo "email"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function createAuthenticatedUrl($arrDados)
    {
        if (!$arrDados) {
            $this->setLastError("Nenhum dado informado");

            return false;
        }

        $xml = <<<EOF
<?xml version="1.0" encoding="utf-8"?>
    <CreateAuthenticatedUrlRequest
         xmlns="http://dli.zbra.com.br"
         xmlns:xsd="http://www.w3.org/2001/XMLSchema"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
             <FirstName>{$arrDados["pesNome"]}</FirstName>
             <LastName>{$arrDados["pesSobrenome"]}</LastName>
             <Email>{$arrDados["conContatoEmail"]}</Email>
             <CourseId xsi:nil="true"/>
             <Tag xsi:nil="true"/>
             <Isbn xsi:nil="true"/>
    </CreateAuthenticatedUrlRequest>
EOF;

        return $this->curlSend($xml, self::METODO_CRIAR_URL);
    }

    public function createPreregisterUser($arrDados)
    {
        if (!$arrDados) {
            $this->setLastError("Nenhum dado informado");

            return false;
        }

        $xml = <<<EOF
<?xml version="1.0" encoding="utf-8"?>
    <CreatePreRegisterUserRequest
         xmlns="http://dli.zbra.com.br"
         xmlns:xsd="http://www.w3.org/2001/XMLSchema"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
             <FirstName>{$arrDados["pesNome"]}</FirstName>
             <LastName>{$arrDados["pesSobrenome"]}</LastName>
             <UserName>{$arrDados["conContatoEmail"]}</UserName>
             <CourseId xsi:nil="true"/>
             <UserGroupId>{$arrDados["usuarioCategoria"]}</UserGroupId>
    </CreatePreRegisterUserRequest>
EOF;

        return $this->curlSend($xml, self::METODO_CRIAR_USUARIO);
    }

    public function removePreregisterUser($arrDados)
    {
        if (!$arrDados) {
            $this->setLastError("Nenhum dado informado");

            return false;
        }

        $xml = <<<EOF
<?xml version="1.0" encoding="utf-8"?>
    <RemovePreRegisterUserRequest
         xmlns="http://dli.zbra.com.br"
         xmlns:xsd="http://www.w3.org/2001/XMLSchema"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <UserName>{$arrDados["conContatoEmail"]}</UserName>
    </RemovePreRegisterUserRequest>
EOF;

        return $this->curlSend($xml, self::METODO_REMOVER_USUARIO);
    }

    private function curlSend($data, $method)
    {
        $serviceSisConfig = new \Sistema\Service\SisConfig($this->getEm(), $this->getConfig());

        $producao = $serviceSisConfig->localizarChave('BIBLIOTECA_VIRTUAL_PRODUCAO');
        $apikey   = $serviceSisConfig->localizarChave('BIBLIOTECA_VIRTUAL_API_KEY');

        $service_url = "https://digitallibrary-staging.zbra.com.br/DigitalLibraryIntegrationService/" . $method;

        if ($producao) {
            $service_url = "https://digitallibrary.zbra.com.br/DigitalLibraryIntegrationService/" . $method;
        }

        $curl         = curl_init($service_url);
        $content_size = strlen($data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTPHEADER,
                    array(
                        "Content-Type: application/xml; charset=utf-8",
                        "Host: digitallibrary-staging.zbra.com.br",
                        "Content-Length: $content_size",
                        "Expect: 100-continue",
                        "Accept-Encoding: gzip, deflate",
                        "Connection: Keep-Alive",
                        "X-DigitalLibraryIntegration-API-Key: $apikey"
                    )
        );
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        $curl_response = curl_exec($curl);

        curl_close($curl);

        $response = (new \SimpleXMLElement($curl_response));

        $response = array(
            'message' => ((string)$response->Message),
            'success' => boolval($response->Success),
            'url'     => ((string)$response->AuthenticatedUrl)
        );

        return $response;
    }

    public function save($arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            $objBibliotecaVirtual = null;

            if ($arrDados['pesId']) {
                /** @var $objBibliotecaVirtual \Biblioteca\Entity\BibliotecaVirtual */
                $objBibliotecaVirtual = $this->getRepository()->find($arrDados['pesId']);
            }

            if (!$objBibliotecaVirtual) {
                $objBibliotecaVirtual = new \Biblioteca\Entity\BibliotecaVirtual();
            }

            if ($arrDados['pesId']) {
                $servicePessoa = new \Pessoa\Service\Pessoa($this->getEm());
                /** @var $objPessoa \Pessoa\Entity\Pessoa */
                if (!$objPessoa = $servicePessoa->getRepository()->find($arrDados['pesId'])) {
                    $this->setLastError("Não foi possível salvar o registro, pessoa não encontrada");

                    return false;
                }

                $objBibliotecaVirtual->setPes($objPessoa);
            }

            $objBibliotecaVirtual->setUsuarioCategoria($arrDados['usuarioCategoria']);

            $this->getEm()->persist($objBibliotecaVirtual);
            $this->getEm()->flush($objBibliotecaVirtual);

            $this->getEm()->commit();

            $arrDados['pesId'] = $objBibliotecaVirtual->getPes()->getPesId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro!<br>' . $e->getMessage());
        }

        return false;
    }

    public function getDataForDataTables($data)
    {
        $query = "
        SELECT
            pessoa.*,
            usuario_categoria,
            if(usuario_categoria,'Sim','Não') integrado,
            if(usuario_categoria = 1,'Professor',
            if(usuario_categoria = 2,'Aluno',
            if(usuario_categoria = 3,'Outros','-'))) categoria
          FROM
            pessoa
          LEFT JOIN
            biblioteca__virtual USING(pes_id)";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($pesId)
    {
        $arrDados = array();

        if (!$pesId) {
            $this->setLastError('Pessoa não encontrada!');

            return $arrDados;
        }

        /** @var $objBibliotecaVirtual \Biblioteca\Entity\BibliotecaVirtual */
        if ($objBibliotecaVirtual = $this->getRepository()->find($pesId)) {
            $arrDados                    = $objBibliotecaVirtual->toArray();
            $arrDados['pesNome']         = preg_replace("/ .*/", "", $arrDados['pes']['pesNome']);
            $arrDados['pesSobrenome']    = preg_replace("/.* /", "", $arrDados['pes']['pesNome']);
            $arrDados['conContatoEmail'] = $arrDados['pes']['conContatoEmail'];
        }

        return $arrDados;
    }

    public function integrarAluno($pesId, $retornarUrl = false)
    {
        if (!$pesId) {
            $this->setLastError("Erro ao receber registro do aluno!");

            return false;
        }

        $servicePessoa = new \Pessoa\Service\Pessoa($this->getEm());

        if ($arrDados = $this->getArray($pesId)) {
            return array('success' => true, 'message' => 'Aluno já está integrado', 'url' => $arrDados['url']);
        }

        if ($arrDados = $servicePessoa->getArray($pesId)) {
            $arrDados['usuarioCategoria'] = self::USUARIO_CATEGORIA_ALUNO;
            $arrDados['pesNome']          = preg_replace("/ .*/", "", $arrDados['pesNome']);
            $arrDados['pesSobrenome']     = preg_replace("/.* /", "", $arrDados['pesNome']);

            $result = $this->createPreregisterUser($arrDados);

            if ($result['success']) {
                if (!$retornarUrl) {
                    return $this->save($arrDados);
                }

                return $this->createAuthenticatedUrl($arrDados);
            }
        }

        return false;
    }

    public function remover($pesId)
    {
        if (!$pesId) {
            $this->setLastError('Para remover um registro de pessoa é necessário informar o código . ');

            return false;
        }

        if ($arrDados = $this->getArray($pesId)) {
            $result = $this->removePreregisterUser($arrDados);

            if (!$result['success']) {
                return $result;
            }
        }

        try {
            /** @var $objBibliotecaVirtual \Biblioteca\Entity\BibliotecaVirtual */
            $objBibliotecaVirtual = $this->getRepository()->find($pesId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objBibliotecaVirtual);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de pessoa . ');

            return false;
        }

        return true;
    }

    public function criarUrl($pesId)
    {
        if (!$pesId) {
            $this->setLastError("Por favor, informe um registro válido");

            return false;
        }

        if ($arrDados = $this->getArray($pesId)) {
            return $this->createAuthenticatedUrl($arrDados);
        }

        return $this->integrarAluno($pesId, true);
    }
}