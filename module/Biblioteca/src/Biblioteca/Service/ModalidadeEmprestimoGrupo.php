<?php

namespace Biblioteca\Service;

use VersaSpine\Service\AbstractService;

class ModalidadeEmprestimoGrupo extends AbstractService
{
    const SITUACAO_ATIVA   = 'Ativo';
    const SITUACAO_INATIVA = 'Inativo';
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Biblioteca\Entity\ModalidadeEmprestimoGrupo');
    }

    public function pesquisaForJson($a)
    {
        return array();
    }

    public function valida($arrParam)
    {
        $errors = array();

        $arrRequerido = array(
            'grupoBibliografico'                => 'Grupo bibliográfico',
            'modalidadeEmprestimoNumeroVolumes' => 'Número de exemplares',
            'modalidadeEmprestimoPrazoMaximo'   => 'Prazo de entrega',
            'modalidadeEmprestimoValorMulta'    => 'Valor da multa'
        );

        foreach ($arrRequerido as $req => $nome) {
            if (!$arrParam[$req]) {
                $errors[] = "Por favor preencha o campo \"$nome\"!";
            }
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function pesquisaPelaModalidadeEmprestimo($modalidadeEmprestimoId, $toArray = true)
    {
        $objModalidadeEmprestimoGrupo = $this->getRepository();
        $arrResult                    = $objModalidadeEmprestimoGrupo->findBy(
            array('modalidadeEmprestimo' => $modalidadeEmprestimoId)
        );

        if ($toArray) {
            $arrRetorno = array();

            foreach ($arrResult as $objModalidadeEmprestimoGrupo) {
                $arrRetorno[] = $objModalidadeEmprestimoGrupo->toArray();
            }

            return $arrRetorno;
        }

        return $arrResult;
    }

    public function salvarArray(
        $arrModalidadeEmprestimoGrupos,
        \Biblioteca\Entity\ModalidadeEmprestimo $objModalidadeEmprestimo
    ) {
        $objEntityManager = $this->getEm();
        $arrExcluir       = array();
        $arrEditar        = array();

        $arrModalidadeEmprestimoGruposDB = $this->pesquisaPelaModalidadeEmprestimo(
            $objModalidadeEmprestimo->getModalidadeEmprestimoId(),
            false
        );

        foreach ($arrModalidadeEmprestimoGruposDB as $arrModalidadeEmprestimoGrupoDB) {
            $encontrado                  = false;
            $modalidadeEmprestimoGrupoId = $arrModalidadeEmprestimoGrupoDB->getModalidadeEmprestimoGrupoId();

            foreach ($arrModalidadeEmprestimoGrupos as $arrModalidadeEmprestimoGrupo) {
                if ($arrModalidadeEmprestimoGrupo['modalidadeEmprestimoGrupoId'] == $modalidadeEmprestimoGrupoId) {
                    $encontrado                              = true;
                    $arrEditar[$modalidadeEmprestimoGrupoId] = $arrModalidadeEmprestimoGrupoDB;
                    break;
                }
            }

            if (!$encontrado) {
                $arrExcluir[$modalidadeEmprestimoGrupoId] = $arrModalidadeEmprestimoGrupoDB;
            }
        }

        if ($arrExcluir) {
            foreach ($arrExcluir as $objReg) {
                $objEntityManager->remove($objReg);
                $objEntityManager->flush();
            }
        }

        foreach ($arrModalidadeEmprestimoGrupos as $arrModalidadeEmprestimoGrupo) {
            $objServiceGrupoBibliografico = new \Biblioteca\Service\GrupoBibliografico($objEntityManager);
            $objGrupoBibliografico        = $objServiceGrupoBibliografico->getRepository()->find(
                $arrModalidadeEmprestimoGrupo['grupoBibliografico']
            );

            $fillEntity = function (
                array $arrModalidadeEmprestimoGrupo,
                \Biblioteca\Entity\ModalidadeEmprestimoGrupo $objModalidadeEmprestimoGrupo
            ) use ($objModalidadeEmprestimo, $objGrupoBibliografico) {
                $objModalidadeEmprestimoGrupo->setModalidadeEmprestimo($objModalidadeEmprestimo);
                $objModalidadeEmprestimoGrupo->setGrupoBibliografico($objGrupoBibliografico);

                $objModalidadeEmprestimoGrupo->setModalidadeEmprestimoNumeroVolumes(
                    $arrModalidadeEmprestimoGrupo['modalidadeEmprestimoNumeroVolumes']
                );
                $objModalidadeEmprestimoGrupo->setModalidadeEmprestimoPrazoMaximo(
                    $arrModalidadeEmprestimoGrupo['modalidadeEmprestimoPrazoMaximo']
                );
                $objModalidadeEmprestimoGrupo->setModalidadeEmprestimoValorMulta(
                    $arrModalidadeEmprestimoGrupo['modalidadeEmprestimoValorMulta']
                );
                $objModalidadeEmprestimoGrupo->setModalidadeEmprestimoDiasNaoUteis(
                    $arrModalidadeEmprestimoGrupo['modalidadeEmprestimoDiasNaoUteis']
                );
                $objModalidadeEmprestimoGrupo->setModalidadeEmprestimoTempoReserva(
                    $arrModalidadeEmprestimoGrupo['modalidadeEmprestimoTempoReserva']
                );
            };

            if (!isset($arrEditar[$arrModalidadeEmprestimoGrupo['modalidadeEmprestimoGrupoId']])) {
                $objModalidadeEmprestimoGrupo = new \Biblioteca\Entity\ModalidadeEmprestimoGrupo();
                $fillEntity($arrModalidadeEmprestimoGrupo, $objModalidadeEmprestimoGrupo);
                $objEntityManager->persist($objModalidadeEmprestimoGrupo);
            } else {
                $objModalidadeEmprestimoGrupo = $arrEditar[$arrModalidadeEmprestimoGrupo['modalidadeEmprestimoGrupoId']];
                $fillEntity($arrModalidadeEmprestimoGrupo, $objModalidadeEmprestimoGrupo);
            }

            $objEntityManager->flush();
        }
    }

    public function retornaArrayPelaModalidadeEmprestimo($ModalidadeEmprestimoId)
    {
        $arrObjModalidadeEmprestimoGrupo = $this->pesquisaPelaModalidadeEmprestimo($ModalidadeEmprestimoId, false);
        $arrModalidadeEmprestimoGrupo    = array();

        foreach ($arrObjModalidadeEmprestimoGrupo as $objModalidadeEmprestimoGrupo) {
            $arrItem                           = $objModalidadeEmprestimoGrupo->toArray();
            $arrItem['modalidadeEmprestimo']   = $arrItem['modalidadeEmprestimo']['modalidadeEmprestimoId'];
            $arrItem['grupoBibliograficoId']   = $arrItem['grupoBibliografico']['grupoBibliograficoId'];
            $arrItem['grupoBibliograficoNome'] = $arrItem['grupoBibliografico']['grupoBibliograficoNome'];
            $arrItem['grupoBibliografico']     = $arrItem['grupoBibliografico']['grupoBibliograficoId'];

            $arrModalidadeEmprestimoGrupo[] = $arrItem;
        }

        return $arrModalidadeEmprestimoGrupo;
    }
}