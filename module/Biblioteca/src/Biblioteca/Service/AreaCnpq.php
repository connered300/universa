<?php

namespace Biblioteca\Service;

use VersaSpine\Service\AbstractService;

class AreaCnpq extends AbstractService
{
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Biblioteca\Entity\AreaCnpq');
    }

    public function pesquisaForJson($params)
    {
        $sql               = 'SELECT * FROM biblioteca__area_cnpq WHERE';
        $areaCnpqDescricao = false;
        $areaCnpqId        = false;

        if ($params['q']) {
            $areaCnpqDescricao = $params['q'];
        } elseif ($params['query']) {
            $areaCnpqDescricao = $params['query'];
        }

        if ($params['areaCnpqId']) {
            $areaCnpqId = $params['areaCnpqId'];
        }

        $parameters = array('area_cnpq_descricao' => "{$areaCnpqDescricao}%");
        $sql .= ' (area_cnpq_descricao LIKE :area_cnpq_descricao OR area_cnpq_codigo LIKE :area_cnpq_descricao)';

        if ($areaCnpqId) {
            $parameters['area_cnpq_id'] = $areaCnpqId;
            $sql .= ' AND area_cnpq_id <> :area_cnpq_id';
        }

        $sql .= " ORDER BY area_cnpq_descricao";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function getJSONListingData($data)
    {
        $query = "
        SELECT
            a.*, (select count(*) from biblioteca__titulo t where t.area_cnpq_id=a.area_cnpq_id) as area_cnpq_titulos
        FROM biblioteca__area_cnpq a";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function numeroTitulosDaAreaCnpq($areaCnpqId)
    {
        $query      = "select count(*) as qtd from biblioteca__titulo t where t.area_cnpq_id=:areaCnpqId";
        $parameters = array('areaCnpqId' => $areaCnpqId);
        $result     = $this->executeQueryWithParam($query, $parameters)->fetch();

        return $result['qtd'];
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['areaCnpqId']) {
                $objAreaCnpq = $this->getRepository()->find($arrDados['areaCnpqId']);

                if (!$objAreaCnpq) {
                    $this->setLastError('Área não existe!');

                    return false;
                }
            } else {
                $objAreaCnpq = new \Biblioteca\Entity\AreaCnpq();
            }

            $objAreaCnpq
                ->setAreaCnpqCodigo($arrDados['areaCnpqCodigo'])
                ->setAreaCnpqDescricao($arrDados['areaCnpqDescricao']);

            $this->getEm()->persist($objAreaCnpq);
            $this->getEm()->flush();

            $this->getEm()->commit();

            $arrDados['areaCnpqId'] = $objAreaCnpq->getAreaCnpqId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar a área!');
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        $arrRequerido = array(
            "areaCnpqCodigo"    => "Código",
            "areaCnpqDescricao" => "Descrição",
        );

        foreach ($arrRequerido as $req => $nome) {
            if (!$arrParam[$req]) {
                $errors[] = "Por favor preencha o campo \"$nome\"!";
            }
        }

        if ($this->verificaSeCodigoAreaCnpqEstaDuplicado($arrParam['areaCnpqCodigo'], $arrParam['areaCnpqId'])) {
            $errors[] = "Já existe uma área cadastrada com este mesmo código!";
        }

        if ($this->verificaSeDescricaoAreaCnpqEstaDuplicado($arrParam['areaCnpqDescricao'], $arrParam['areaCnpqId'])) {
            $errors[] = "Já existe uma área cadastrada com esta mesma descrição!";
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function verificaSeDescricaoAreaCnpqEstaDuplicado($areaCnpqDescricao, $areaCnpqId = false)
    {
        $sql        = 'SELECT count(*) as qtd FROM biblioteca__area_cnpq WHERE area_cnpq_descricao like :areaCnpqDescricao';
        $parameters = array('areaCnpqDescricao' => $areaCnpqDescricao);

        if ($areaCnpqId) {
            $sql .= ' AND area_cnpq_id<>:areaCnpqId';
            $parameters['areaCnpqId'] = $areaCnpqId;
        }

        $result = $this->executeQueryWithParam($sql, $parameters)->fetch();

        return $result['qtd'] > 0;
    }

    public function verificaSeCodigoAreaCnpqEstaDuplicado($areaCnpqCodigo, $areaCnpqId = false)
    {
        $sql        = 'SELECT count(*) as qtd FROM biblioteca__area_cnpq WHERE area_cnpq_codigo like :areaCnpqCodigo';
        $parameters = array('areaCnpqCodigo' => $areaCnpqCodigo);

        if ($areaCnpqId) {
            $sql .= ' AND area_cnpq_id<>:areaCnpqId';
            $parameters['areaCnpqId'] = $areaCnpqId;
        }

        $result = $this->executeQueryWithParam($sql, $parameters)->fetch();

        return $result['qtd'] > 0;
    }

    public function getArray($areaCnpqId)
    {
        $arrData = $this->getReference($areaCnpqId);

        try {
            $arrData                    = $arrData->toArray();
            $arrData['areaCnpqTitulos'] = $this->numeroTitulosDaAreaCnpq($areaCnpqId);
        } catch (\Exception $e) {
            return array();
        }

        return $arrData;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('areaCnpqId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();

        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getAreaCnpqId(),
                $params['value'] => $objEntity->getAreaCnpqDescricao()
            );
        }

        return $arrEntitiesArr;
    }
}