<?php

namespace Biblioteca\Service;

use VersaSpine\Service\AbstractService;

class Area extends AbstractService
{
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Biblioteca\Entity\Area');
    }

    public function pesquisaForJson($params)
    {
        $sql           = 'SELECT * FROM biblioteca__area WHERE';
        $areaDescricao = false;
        $areaId        = false;

        if ($params['q']) {
            $areaDescricao = $params['q'];
        } elseif ($params['query']) {
            $areaDescricao = $params['query'];
        }

        if ($params['areaId']) {
            $areaId = $params['areaId'];
        }

        $parameters = array('area_descricao' => "{$areaDescricao}%");
        $sql .= ' area_descricao LIKE :area_descricao';

        if ($areaId) {
            $parameters['area_id'] = $areaId;
            $sql .= ' AND area_id <> :area_id';
        }

        $sql .= " ORDER BY area_descricao";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function getJSONListingData($data)
    {
        $query = "
        SELECT
            a.*, (select count(*) from biblioteca__titulo t where t.area_id=a.area_id) as area_titulos
        FROM biblioteca__area a";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function numeroTitulosDaArea($areaId)
    {
        $query      = "select count(*) as qtd from biblioteca__titulo t where t.area_id=:areaId";
        $parameters = array('areaId' => $areaId);
        $result     = $this->executeQueryWithParam($query, $parameters)->fetch();

        return $result['qtd'];
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['areaId']) {
                $objArea = $this->getRepository()->find($arrDados['areaId']);

                if (!$objArea) {
                    $this->setLastError('Área não existe!');

                    return false;
                }
            } else {
                $objArea = new \Biblioteca\Entity\Area();
            }

            $objArea->setAreaDescricao($arrDados['areaDescricao']);

            $this->getEm()->persist($objArea);
            $this->getEm()->flush();

            $this->getEm()->commit();

            $arrDados['areaId'] = $objArea->getAreaId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar a área!');
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        $arrRequerido = array(
            "areaDescricao" => "Descrição"
        );

        foreach ($arrRequerido as $req => $nome) {
            if (!$arrParam[$req]) {
                $errors[] = "Por favor preencha o campo \"$nome\"!";
            }
        }

        if ($this->verificaSeAreaEstaDuplicado($arrParam['areaDescricao'], $arrParam['areaId'])) {
            $errors[] = "Já existe uma área cadastrada com esta mesma descrição!";
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function verificaSeAreaEstaDuplicado($areaDescricao, $areaId = false)
    {
        $sql        = 'SELECT count(*) as qtd FROM biblioteca__area WHERE area_descricao like :areaDescricao';
        $parameters = array('areaDescricao' => $areaDescricao);

        if ($areaId) {
            $sql .= ' AND area_id<>:areaId';
            $parameters['areaId'] = $areaId;
        }

        $result = $this->executeQueryWithParam($sql, $parameters)->fetch();

        return $result['qtd'] > 0;
    }

    public function getArray($areaId)
    {
        $arrData = $this->getReference($areaId);

        try {
            $arrData                = $arrData->toArray();
            $arrData['areaTitulos'] = $this->numeroTitulosDaArea($areaId);
        } catch (\Exception $e) {
            return array();
        }

        return $arrData;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('areaId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();

        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getAreaId(),
                $params['value'] => $objEntity->getAreaDescricao()
            );
        }

        return $arrEntitiesArr;
    }
}