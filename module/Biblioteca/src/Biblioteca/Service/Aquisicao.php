<?php

namespace Biblioteca\Service;

use VersaSpine\Service\AbstractService;

class Aquisicao extends AbstractService
{
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Biblioteca\Entity\Aquisicao');
    }

    public function pesquisaForJson($a)
    {
        return array();
    }

    public function valida($arrParam)
    {
        $errors = array();

        foreach ($arrParam as $arrParamAquisicao) {
            $arrRequerido = array(
                'tipo'       => 'Código',
                'exemplares' => 'Exemplares',
                'data'       => 'Data'
            );

            foreach ($arrRequerido as $req => $nome) {
                if (!$arrParamAquisicao[$req]) {
                    $errors[] = "Por favor preencha o campo \"$nome\" do autor " . $arrParamAquisicao['index'] . "!";
                }
            }

            if (strtolower($arrParamAquisicao['tipo']) == 'compra') {
                if (!$arrParamAquisicao['fornecedor']) {
                    $errors[] = 'Por favor preencha o campo "fornecedor"!';
                }

                if (!$arrParamAquisicao['notaFiscal']) {
                    $errors[] = 'Por favor preencha o campo "nota fiscal"!';
                }
            } elseif (strtolower($arrParamAquisicao['tipo']) == 'doacao' && !$arrParamAquisicao['doador']) {
                $errors[] = 'Por favor preencha o campo "doador"!';
            } elseif (strtolower($arrParam['tipo']) == 'permuta' && !$arrParamAquisicao['permutaDescricao']) {
                $errors[] = 'Por favor preencha o campo "descrição da permuta"!';
            } elseif (strtolower($arrParamAquisicao['tipo']) == 'outro' && !$arrParamAquisicao['observacao']) {
                $errors[] = 'Por favor preencha o campo "observação"!';
            }
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function pesquisaPeloTitulo($tituloId, $toArray = true)
    {
        $objAquisicao = $this->getRepository();
        $arrResult    = $objAquisicao->findBy(array('titulo' => $tituloId));

        if ($toArray) {
            $arrRetorno = array();

            foreach ($arrResult as $objAquisicao) {
                $arrRetorno[] = $objAquisicao->toArray();
            }

            return $arrRetorno;
        }

        return $arrResult;
    }

    public function salvarArray($arrAquisicoes, \Biblioteca\Entity\Titulo $objTitulo, &$arrObjExemplares = array())
    {
        $arrExcluir = array();
        $arrEditar  = array();

        $arrAquisicoesDB = $this->pesquisaPeloTitulo($objTitulo->getTituloId(), false);

        foreach ($arrAquisicoesDB as $arrAquisicaoDB) {
            $encontrado  = false;
            $aquisicaoId = $arrAquisicaoDB->getAquisicaoId();

            foreach ($arrAquisicoes as $arrAquisicao) {
                if ($arrAquisicao['aquisicaoId'] == $aquisicaoId) {
                    $encontrado              = true;
                    $arrEditar[$aquisicaoId] = $arrAquisicaoDB;
                    break;
                }
            }

            if (!$encontrado) {
                $arrExcluir[$aquisicaoId] = $arrAquisicaoDB;
            }
        }

        $objServiceAquisicaoExemplar = new \Biblioteca\Service\AquisicaoExemplar($this->getEm());
        $objServiceEmpresa           = new \Biblioteca\Service\Empresa($this->getEm());

        if ($arrExcluir) {
            foreach ($arrExcluir as $objReg) {
                $objServiceAquisicaoExemplar->delete(false, $objReg->getAquisicaoId());
                $this->getEm()->remove($objReg);
                $this->getEm()->flush();
            }
        }

        foreach ($arrAquisicoes as $arrAquisicao) {
            if (!isset($arrEditar[$arrAquisicao['aquisicaoId']])) {
                $objAquisicao = new \Biblioteca\Entity\Aquisicao();
            } else {
                $objAquisicao = $arrEditar[$arrAquisicao['aquisicaoId']];
            }

            $objAquisicao->setTitulo($objTitulo);
            $objAquisicao->setAquisicaoTipo($arrAquisicao['tipo']);
            $objAquisicao->setAquisicaoData(new \DateTime($this->formatDateAmericano($arrAquisicao['data'])));
            $objAquisicao->setPesDoador(null);
            $objAquisicao->setPesFornecedor(null);
            $objAquisicao->setAquisicaoNotaValor(null);
            $objAquisicao->setAquisicaoNotaFiscal(null);

            if ($arrAquisicao['tipo'] == 'Compra') {
                $objEmpresa = $objServiceEmpresa->getRepository()->find($arrAquisicao['fornecedor']);

                if (!$objEmpresa) {
                    $this->setLastError('Fornecedor não existe!');

                    return false;
                }

                if ($arrAquisicao['notaValor']) {
                    $arrAquisicao['notaValor'] = str_replace(
                        array('.', ','),
                        array('', '.'),
                        $arrAquisicao['notaValor']
                    );
                } else {
                    $arrAquisicao['notaValor'] = null;
                }

                $objAquisicao->setPesFornecedor($objEmpresa);
                $objAquisicao->setAquisicaoNotaValor($arrAquisicao['notaValor']);
                $objAquisicao->setAquisicaoNotaFiscal($arrAquisicao['notaFiscal']);
            } elseif ($arrAquisicao['tipo'] == 'Doacao') {
                $objServicePessoa = new \Biblioteca\Service\Pessoa($this->getEm());

                list($tipo, $doador) = explode('-', $arrAquisicao['doador']);

                if ($tipo == 'empresa') {
                    $objEmpresa = $objServiceEmpresa->getRepository()->find($doador);
                    $objPessoa  = null;
                } else {
                    $objEmpresa = null;
                    $objPessoa  = $objServicePessoa->getRepository()->find($doador);
                }

                if (!$objEmpresa && !$objPessoa) {
                    $this->setLastError('Doador não existe!');

                    return false;
                }

                $objAquisicao->setPesDoador($objPessoa);
                $objAquisicao->setPesFornecedor($objEmpresa);
            } elseif ($arrAquisicao['tipo'] == 'Permuta') {
                $objAquisicao->setAquisicaoPermutaDescricao((string)$arrAquisicao['permutaDescricao']);
            } elseif ($arrAquisicao['tipo'] == 'Outro') {
                $objAquisicao->setAquisicaoObservacao((string)$arrAquisicao['observacao']);
            }

            $this->getEm()->persist($objAquisicao);
            $this->getEm()->flush();

            $objServiceAquisicaoExemplar->delete(false, $objAquisicao->getAquisicaoId());

            $arrAquisicao['exemplares'] = explode(',', $arrAquisicao['exemplares']);

            foreach ($arrAquisicao['exemplares'] as $strCodigoExemplar) {
                foreach ($arrObjExemplares as $objExemplar) {
                    if ($objExemplar->getExemplarCodigo() == $strCodigoExemplar) {
                        $objAquisicaoExemplar = new \Biblioteca\Entity\AquisicaoExemplar();
                        $objAquisicaoExemplar->setAquisicao($objAquisicao);
                        $objAquisicaoExemplar->setExemplar($objExemplar);
                        $this->getEm()->persist($objAquisicaoExemplar);
                        $this->getEm()->flush();
                        break;
                    }
                }
            }
        }

        return true;
    }

    public function retornaArrayPeloTitulo($TituloId)
    {
        $objServiceAquisicaoExemplar = new \Biblioteca\Service\AquisicaoExemplar($this->getEm());
        $arrObjAquisicao             = $this->pesquisaPeloTitulo($TituloId, false);
        $arrAquisicao                = array();

        /** @var \Biblioteca\Entity\Aquisicao $objAquisicao */
        foreach ($arrObjAquisicao as $objAquisicao) {
            $arrItem    = $objAquisicao->toArray();
            $exemplares = $objServiceAquisicaoExemplar->getIdentificadoresExemplarCodigoPorAquisicao(
                $objAquisicao->getAquisicaoId()
            );

            foreach ($exemplares as $posExemplar => $exemplar) {
                $exemplares[$posExemplar] = array('id' => (string)$exemplar, 'text' => (string)$exemplar);
            }

            $arrDoador     = null;
            $arrFornecedor = null;

            if ($arrItem['aquisicaoTipo'] == 'Doacao') {
                if ($arrItem['pesDoador']) {
                    $arrDoador = array(
                        'id'   => 'pessoa-' . $objAquisicao->getPesDoador()->getPes()->getPes()->getPesId(),
                        'text' => 'Pessoa: ' . $objAquisicao->getPesDoador()->getPes()->getPes()->getPesNome()
                    );
                } elseif ($arrItem['pesFornecedor']) {
                    $arrDoador = array(
                        'id'   => 'empresa-' . $objAquisicao->getPesFornecedor()->getPes()->getPes()->getPesId(),
                        'text' => 'Empresa: ' . $objAquisicao->getPesFornecedor()->getPes()->getPesNomeFantasia()
                    );
                }
            } elseif ($arrItem['aquisicaoTipo'] == 'Compra' && $arrItem['pesFornecedor']) {
                $arrFornecedor = array(
                    'id'   => $objAquisicao->getPesFornecedor()->getPes()->getPes()->getPesId(),
                    'text' => $objAquisicao->getPesFornecedor()->getPes()->getPesNomeFantasia()
                );
            }

            $arrItem = array(
                'aquisicaoId'      => $arrItem['aquisicaoId'],
                'tipo'             => $arrItem['aquisicaoTipo'],
                'doador'           => $arrDoador,
                'permutaDescricao' => (string)$arrItem['aquisicaoPermutaDescricao'],
                'fornecedor'       => $arrFornecedor,
                'notaValor'        => $arrItem['aquisicaoNotaValor'],
                'notaFiscal'       => $arrItem['aquisicaoNotaFiscal'],
                'observacao'       => (string)$arrItem['aquisicaoObservacao'],
                'baixa'            => $arrItem['aquisicaoBaixa'],
                'data'             => $objAquisicao->getAquisicaoData()->format('d/m/Y'),
                'exemplares'       => $exemplares,
            );

            $arrAquisicao[] = $arrItem;
        }

        return $arrAquisicao;
    }
}