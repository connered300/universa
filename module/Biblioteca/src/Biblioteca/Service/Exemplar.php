<?php

namespace Biblioteca\Service;

use VersaSpine\Service\AbstractService;

class Exemplar extends AbstractService
{
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Biblioteca\Entity\Exemplar');
    }

    public function pesquisaForJson($params)
    {
        //TODO: Separar chamadas dos serviços em actions diferentes
        if ($params['local']) {
            return $this->pesquisaLocalForJson($params);
        } elseif ($params['emprestimo']) {
            return $this->pesquisaExemplaresDisponiveisForJson($params);
        } elseif ($params['exemplar']) {
            return $this->pesquisaExemplarForJson($params);
        } elseif ($params['index']) {
            return $this->pesquisaForJsonDatatables($params);
        }

        return array();
    }

    /**
     * @param $data
     * @return array
     */
    public function pesquisaForJsonDatatables($data)
    {
        $serviceGrupoBibliografico = new \Biblioteca\Service\GrupoBibliografico($this->getEm());

        //tamanho do group_concat para codigos de exemplares
        $this->executeQuery('SET SESSION group_concat_max_len = 1000000');

        $conditions = array();

        if ($data["filter"]['grupoBibliografico']) {
            $conditions[] = "gb.grupo_bibliografico_id in(" . $data["filter"]['grupoBibliografico'] . ")";
        }

        if ($data["filter"]['exemplarAcesso']) {
            $exemplarAcesso = str_replace(",", "','", $data["filter"]['exemplarAcesso']);
            $conditions[]   = "ex.exemplar_acesso in('" . $exemplarAcesso . "')";
        }

        if ($data["filter"]['editora']) {
            $conditions[] = "t.pes_id_editora in(" . $data["filter"]['editora'] . ")";
        }

        if ($data["filter"]['areaCnpq']) {
            $conditions[] = "t.area_cnpq_id in(" . $data["filter"]['areaCnpq'] . ")";
        }

        if ($data["filter"]['autor']) {
            $conditions[] = "t.titulo_id in" .
                "(SELECT titulo_id FROM biblioteca__titulo_autor WHERE autor_id in(" . $data["filter"]['autor'] . "))";
        }

        if ($data["filter"]['assunto']) {
            $conditions[] = "t.titulo_id in" .
                "(SELECT titulo_id FROM biblioteca__titulo_assunto WHERE assunto_id in(" . $data["filter"]['assunto'] . "))";
        }

        $query = "
    SELECT
        gb.grupo_bibliografico_id, gb.grupo_bibliografico_nome,
        ex.exemplar_id, ex.exemplar_baixa_data, ex.exemplar_codigo,
        ex.exemplar_acesso, ex.exemplar_emprestado,
        t.titulo_id, t.titulo_titulo, t.titulo_edicao_volume, t.titulo_edicao_tomo,
        t.titulo_edicao_numero, t.titulo_edicao_isbn, t.titulo_edicao_cdu,
        t.titulo_edicao_cutter, t.titulo_edicao_ano,
        (
            SELECT group_concat(DISTINCT a.autor_referencia SEPARATOR '; ')
            FROM biblioteca__titulo_autor tau
              INNER JOIN biblioteca__autor a ON a.autor_id=tau.autor_id
            WHERE tau.titulo_id=t.titulo_id
        ) AS autor_referencia,
        t.pes_id_editora, pj.pes_nome_fantasia,
        -- ap.area_id, ap.area_descricao,
        -- ac.area_cnpq_id, ac.area_cnpq_codigo, ac.area_cnpq_descricao,
        (
            SELECT group_concat(ass.assunto_descricao SEPARATOR ', ')
            FROM biblioteca__titulo_assunto tass
            INNER JOIN biblioteca__assunto ass ON ass.assunto_id=tass.assunto_id
            WHERE tass.titulo_id=t.titulo_id
        ) AS titulo_assuntos,
        IF(ex.exemplar_emprestado='N',NULL,
        (
            SELECT max(emprestimo_id)
            FROM biblioteca__emprestimo em
            WHERE em.exemplar_id=ex.exemplar_id AND em.emprestimo_devolucao_data_efetuada IS NULL
        )) AS emprestimo_id
    FROM
        biblioteca__titulo t
        INNER JOIN biblioteca__grupo_bibliografico gb ON gb.grupo_bibliografico_id=t.grupo_bibliografico_id
        INNER JOIN biblioteca__exemplar ex ON ex.titulo_id=t.titulo_id
        -- editora
        LEFT JOIN biblioteca__empresa e ON e.pes_id=t.pes_id_editora
        LEFT JOIN pessoa_juridica pj ON pj.pes_id=e.pes_id
        -- area do conhecimento principal
        -- LEFT JOIN biblioteca__area ap
        --    ON ap.area_id=t.area_id
        -- area de conhecimento CNPq
        -- LEFT JOIN biblioteca__area_cnpq ac
        --    ON ac.area_cnpq_id=t.area_cnpq_id
        -- idiomas do titulo
        -- LEFT JOIN biblioteca__titulo_idioma tid
        --    ON tid.titulo_id=t.titulo_id
        -- LEFT JOIN biblioteca__idioma i
        --    ON i.idioma_id=tid.idioma_id
    -- CONDITIONS --
    GROUP BY t.titulo_id, ex.exemplar_id";

        $conditions = $conditions ? "WHERE " . implode(' AND ', $conditions) : "";

        $query = str_replace('-- CONDITIONS --', $conditions, $query);

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    private function pesquisaExemplaresDisponiveisForJson($params)
    {
        $sql = "
    SELECT * FROM (
        SELECT
            ex.exemplar_id,
            ex.exemplar_codigo,
            ex.exemplar_reservado,
            ex.exemplar_acesso,
            gb.grupo_bibliografico_id, gb.grupo_bibliografico_nome,
            t.titulo_id, t.titulo_titulo, t.titulo_edicao_volume, t.titulo_edicao_isbn,
            t.titulo_edicao_cdu, t.titulo_edicao_cutter, t.titulo_edicao_ano,
            (
                SELECT group_concat(DISTINCT a.autor_referencia SEPARATOR '; ')
                FROM biblioteca__titulo_autor tau
                  INNER JOIN biblioteca__autor a ON a.autor_id=tau.autor_id
                WHERE tau.titulo_id=t.titulo_id
            ) AS autor_referencia,
            -- ,t.pes_id_editora, pj.pes_nome_fantasia
            -- ,ap.area_id, ap.area_descricao
            -- ,ac.area_cnpq_id, ac.area_cnpq_codigo, ac.area_cnpq_descricao
            IF(
                ex.exemplar_acesso <> 'Restrito' AND
                ex.exemplar_emprestado = 'N' AND
                ex.exemplar_baixa_data IS NULL
                ,
                'S',
                'N'
            ) exemplar_disponibilidade,
            COALESCE(
                IF(ex.exemplar_baixa_data IS NOT NULL, 'Baixado', NULL),
                IF(ex.exemplar_emprestado='S', 'Emprestado', NULL),
                IF(ex.exemplar_acesso='Restrito', 'Restrito', NULL),
                ''
            ) exemplar_disponibilidade_descricao
        FROM
            biblioteca__titulo t
            INNER JOIN biblioteca__grupo_bibliografico gb ON gb.grupo_bibliografico_id=t.grupo_bibliografico_id
            -- editora
            LEFT JOIN biblioteca__empresa e ON e.pes_id=t.pes_id_editora
            LEFT JOIN pessoa_juridica pj ON pj.pes_id=e.pes_id
            -- area do conhecimento principal
            -- LEFT JOIN biblioteca__area ap ON ap.area_id=t.area_id
            -- area de conhecimento CNPq
            -- LEFT JOIN biblioteca__area_cnpq ac ON ac.area_cnpq_id=t.area_cnpq_id
            -- assuntos relacionados ao titulo
            -- LEFT JOIN biblioteca__titulo_assunto tass ON tass.titulo_id=t.titulo_id
            -- LEFT JOIN biblioteca__assunto ass ON ass.assunto_id=tass.assunto_id
            -- idiomas do titulo
            -- LEFT JOIN biblioteca__titulo_idioma tid ON tid.titulo_id=t.titulo_id
            -- LEFT JOIN biblioteca__idioma i ON i.idioma_id=tid.idioma_id
            -- exemplares disponiveis titulo
            INNER JOIN biblioteca__exemplar ex ON ex.titulo_id=t.titulo_id
        GROUP BY ex.exemplar_id
        ORDER BY titulo_titulo, ex.exemplar_codigo
    ) AS exemplares
    WHERE __CONDICAO__
    ORDER BY
      IF(exemplar_id = :codigo,0,1),
      IF(titulo_id = :codigo,0,1),
      IF(exemplar_codigo = :codigo,0,1),
      IF(titulo_titulo LIKE :titulo,0,1),
      IF(autor_referencia LIKE :titulo,0,1),
      titulo_titulo, exemplar_codigo
    LIMIT 0, 40";

        $query                = '';
        $grupoBibliograficoId = '';
        $tituloIgnorar        = '';

        if ($params['q']) {
            $query = $params['q'];
        } elseif ($params['query']) {
            $query = $params['query'];
        }

        if ($params['tituloIgnorar']) {
            $tituloIgnorar = $params['tituloIgnorar'];
            $tituloIgnorar = is_array($tituloIgnorar) ? implode(',', $tituloIgnorar) : $tituloIgnorar;
        }

        if ($params['grupoBibliograficoId']) {
            $grupoBibliograficoId = $params['grupoBibliograficoId'];
        }

        $parameters     = array('titulo' => "{$query}%", 'codigo' => "{$query}",);
        $conditionsOr   = array();
        $conditionsOr[] = 'titulo_titulo LIKE :titulo';
        $conditionsOr[] = 'autor_referencia LIKE :titulo';
        $conditionsOr[] = 'titulo_id = :codigo';
        $conditionsOr[] = 'exemplar_id = :codigo';
        $conditionsOr[] = 'exemplar_codigo = :codigo';
        $conditionsOr   = implode(' OR ', $conditionsOr);
        $conditionsOr   = "({$conditionsOr})";

        $conditionsAnd = array();

        if ($tituloIgnorar) {
            $conditionsAnd[] = "titulo_id NOT IN({$tituloIgnorar})";
        }
        if ($grupoBibliograficoId) {
            $conditionsAnd[] = "grupo_bibliografico_id IN({$grupoBibliograficoId})";
        }

        $conditionsAnd = implode(' AND ', $conditionsAnd);
        $conditionsOr .= $conditionsAnd ? ' AND ' . $conditionsAnd : '';

        $sql = str_replace('__CONDICAO__', $conditionsOr, $sql);

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function getExemplaresTitulo($titulo_id)
    {
        $sql = "
        SELECT ex.*, st.setor_descricao, st.setor_sigla
        FROM biblioteca__exemplar ex
        INNER JOIN biblioteca__setor st ON st.setor_id=ex.setor_id
        WHERE titulo_id=:titulo_id";

        $result = $this->executeQueryWithParam($sql, array('titulo_id' => $titulo_id))->fetchAll();

        return $result;
    }

    public function getExemplaresEtiquetaLombada($inicio = false, $fim = false, $intervalos = false)
    {
        if ($inicio && $fim) {
            $condicao = 'exemplar_id BETWEEN :exemplarInicial AND :exemplarFinal';
            $param    = ['exemplarInicial' => $inicio, 'exemplarFinal' => $fim];
        } else {
            $exemplares = [];
            $total      = 0;
            $intervalos = explode(',', $intervalos);

            foreach ($intervalos as $inicio) {
                $inicio = explode("-", $inicio);
                $fim    = null;

                if (count($inicio) > 1) {
                    $fim    = max($inicio[0], $inicio[1]);
                    $inicio = min($inicio[0], $inicio[1]);

                    if ($fim == $inicio) {
                        $total += 1;
                        $exemplares[] = $inicio;
                    } else {
                        $total += ($fim - $inicio) + 1;

                        for ($i = $inicio; $i <= $fim; $i++) {
                            $exemplares[] = $i;
                        }
                    }
                } else {
                    $total += 1;
                    $exemplares[] = $inicio[0];
                }
            }

            $condicao = 'exemplar_id IN(:intervalo)';
            $param    = ['intervalo' => $exemplares];
        }

        $sql   = "
        SELECT
          titulo_edicao_cdu,
          titulo_edicao_cutter,
          titulo_edicao_volume,
          titulo_edicao_numero,
          titulo_edicao_tomo,
          titulo_edicao_ano,
          exemplar_codigo,
          exemplar_id,
          exemplar_acesso,
          setor_sigla,
          titulo_titulo,
          if(exemplar_acesso='Restrito','F','') as exemplarAcesso,
          titulo_id,
          (
            SELECT a.autor_referencia
            FROM biblioteca__titulo_autor tau
            INNER JOIN biblioteca__autor a ON a.autor_id=tau.autor_id
            WHERE tau.titulo_id=t.titulo_id AND tau.titulo_autor_tipo='Autor principal'
            LIMIT 0,1
          ) AS autor
        FROM biblioteca__exemplar ex
          INNER JOIN biblioteca__setor st USING (setor_id)
          INNER JOIN biblioteca__titulo t USING (titulo_id)
        WHERE " . $condicao;

        $result = $this->executeQueryWithParam($sql, $param)->fetchAll();
        $result = $this->formatarExemplares($result);

        return $result;
    }

    public function formatarExemplares($arrExemplares)
    {
        $arrResult = array();

        foreach ($arrExemplares as $titulo) {
            $titulo['cdu']      = $titulo['titulo_edicao_cdu'];
            $titulo['cutter']   = $titulo['titulo_edicao_cutter'];
            $titulo['volume']   = $titulo['titulo_edicao_volume'];
            $titulo['tomo']     = $titulo['titulo_edicao_tomo'];
            $titulo['edicao']   = $titulo['titulo_edicao_numero'];
            $titulo['ano']      = $titulo['titulo_edicao_ano'];
            $titulo['exemplar'] = $titulo['exemplar_codigo'];
            $titulo['codigo']   = $titulo['exemplar_id'];
            $titulo['acesso']   = $titulo['exemplar_acesso'];
            $titulo['sigla']    = $titulo['setor_sigla'];
            $titulo['titulo']   = $titulo['titulo_titulo'];

            $arrResult[] = $titulo;
        }

        return $arrResult;
    }

    public function getExemplaresBaixados($dataInicial, $dataFinal)
    {
        $sql   = "
        SELECT
           t.titulo_id, t.titulo_titulo,
           ex.exemplar_id, ex.exemplar_codigo, ex.exemplar_baixa_data,
           DATE_FORMAT(ex.exemplar_baixa_data,'%d/%m/%Y') AS exemplar_baixa_data_formatado,
           ex.exemplar_baixa_motivo
        FROM biblioteca__exemplar ex
        INNER JOIN biblioteca__titulo t ON t.titulo_id=ex.titulo_id
        WHERE exemplar_baixa_data BETWEEN date(:dataInicial) AND date(:dataFinal) + 1
        ORDER BY titulo_id, exemplar_id, exemplar_codigo";
        $param = array('dataInicial' => $dataInicial, 'dataFinal' => $dataFinal);

        $result = $this->executeQueryWithParam($sql, $param)->fetchAll();

        return $result;
    }

    private function pesquisaLocalForJson($params)
    {
        $sql   = 'SELECT DISTINCT exemplar_localizacao FROM biblioteca__exemplar WHERE';
        $local = false;

        if ($params['q']) {
            $local = $params['q'];
        } elseif ($params['query']) {
            $local = $params['query'];
        }

        $parameters = array('exemplar_localizacao' => "{$local}%");
        $sql .= ' exemplar_localizacao LIKE :exemplar_localizacao';

        $sql .= " ORDER BY exemplar_localizacao";
        $sql .= " LIMIT 0, 40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    private function pesquisaExemplarForJson($params)
    {
        $exemplarId = false;

        if ($params['q']) {
            $exemplarId = $params['q'];
        } elseif ($params['query']) {
            $exemplarId = $params['query'];
        }

        $result = $this->getExemplaresEtiquetaLombada($exemplarId, $exemplarId);

        return $result[0] ? $result[0] : array();
    }

    public function valida($arrParam)
    {
        $errors = array();

        foreach ($arrParam as $arrParamExemplar) {
            $arrRequerido = array(
                'exemplar_codigo' => 'Código',
                'exemplar_acesso' => 'Acesso',
                'setor_id'        => 'Setor',
            );

            foreach ($arrRequerido as $req => $nome) {
                if (!$arrParamExemplar[$req]) {
                    $errors[] = "Por favor preencha o campo \"$nome\" do exemplar " . $arrParamExemplar['index'] . "!";
                }
            }
        }

        if (empty($arrParam)) {
            $errors[] = "Por favor insira ao menos um exemplar!";
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function pesquisaPeloTitulo($tituloId, $toArray = true)
    {
        $objExemplar = $this->getRepository();
        $arrResult   = $objExemplar->findBy(array('titulo' => $tituloId));

        if ($toArray) {
            $arrRetorno = array();

            foreach ($arrResult as $objExemplar) {
                $arrRetorno[] = $objExemplar->toArray();
            }

            return $arrRetorno;
        }

        return $arrResult;
    }

    public function salvarArray($arrExemplares, \Biblioteca\Entity\Titulo $objTitulo, &$arrObjExemplares = array())
    {
        $session    = new \Zend\Authentication\Storage\Session();
        $arrUsuario = $session->read();

        if (!$arrUsuario) {
            $this->setLastError('Verifique se você está autenticado no sistema!');

            return false;
        }

        $usuarioId               = $arrUsuario['id'];
        $objServiceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());
        /* @var $objUsuario \Acesso\Entity\AcessoPessoas */
        $objUsuario = $objServiceAcessoPessoas->getRepository()->find($usuarioId);

        if (!$objUsuario) {
            $this->setLastError('Verifique se você está autenticado no sistema!');

            return false;
        }

        $arrInformation = array(
            'novo'       => array(),
            'exemplares' => array(),
            'importado'  => array(),
        );

        $objEntityManager = $this->getEm();
        $arrExcluir       = array();
        $arrEditar        = array();

        $arrExemplaresDB = $this->pesquisaPeloTitulo(
            $objTitulo->getTituloId(),
            false
        );

        foreach ($arrExemplaresDB as $arrExemplarDB) {
            $encontrado = false;
            $exemplarId = $arrExemplarDB->getExemplarId();

            foreach ($arrExemplares as $arrExemplar) {
                if ($arrExemplar['exemplar_id'] == $exemplarId) {
                    $encontrado             = true;
                    $arrEditar[$exemplarId] = $arrExemplarDB;
                    break;
                }
            }

            if (!$encontrado) {
                $arrExcluir[$exemplarId] = $arrExemplarDB;
            }
        }

        if ($arrExcluir) {
            foreach ($arrExcluir as $objReg) {
                $objEntityManager->remove($objReg);
                $objEntityManager->flush();
            }
        }

        foreach ($arrExemplares as $arrExemplar) {
            $exemplarId      = $arrExemplar['exemplar_id'] ? (int)$arrExemplar['exemplar_id'] : '';
            $objServiceSetor = new \Biblioteca\Service\Setor($objEntityManager);
            $objSetor        = $objServiceSetor->getRepository()->find($arrExemplar['setor_id']);

            if (!$exemplarId) {
                $objExemplar = new \Biblioteca\Entity\Exemplar();
            } else {
                if (isset($arrEditar[$exemplarId])) {
                    $objExemplar = $arrEditar[$exemplarId];
                } else {
                    $objExemplar = $this->getRepository()->find($exemplarId);

                    if (!$objExemplar) {
                        $objExemplar = new \Biblioteca\Entity\Exemplar();
                    } else {
                        $arrInformation['importado'][] = $objExemplar->getExemplarId();
                    }
                }
            }

            $objExemplar->setTitulo($objTitulo);
            $objExemplar->setSetor($objSetor);

            $objExemplar->setExemplarAcesso($arrExemplar['exemplar_acesso']);
            $objExemplar->setExemplarCodigo($arrExemplar['exemplar_codigo']);
            $objExemplar->setExemplarLocalizacao($arrExemplar['exemplar_localizacao']);
            $objExemplar->setExemplarDataAlteracao(new \DateTime());
            $objExemplar->setUsuario($objUsuario);

            if (!$exemplarId) {
                $objExemplar->setExemplarDataCadastro(new \DateTime());
            }

            if ($arrExemplar['exemplar_baixa_data']) {
                $objExemplar->setExemplarBaixaData(new \DateTime($arrExemplar['exemplar_baixa_data']));
                $objExemplar->setExemplarBaixaMotivo($arrExemplar['exemplar_baixa_motivo']);
            } else {
                $objExemplar->setExemplarBaixaData(null);
                $objExemplar->setExemplarBaixaMotivo('');
            }

            $objEntityManager->persist($objExemplar);
            $objEntityManager->flush();

            $arrInformation['exemplares'][] = $objExemplar->getExemplarId();

            if (!$exemplarId) {
                $arrInformation['novo'][] = $objExemplar->getExemplarId();
            }

            $arrObjExemplares[] = $objExemplar;
        }

        if ($arrInformation['exemplares']) {
            $mensagem = '';

            if ($arrInformation['exemplares']) {
                $mensagem .= 'Todos exemplares: ' . implode(', ', $arrInformation['exemplares']);
            }

            if ($arrInformation['novo']) {
                $mensagem .= '<br>Novos exemplares: ' . implode(', ', $arrInformation['novo']);
            }

            if ($arrInformation['importado']) {
                $mensagem .= '<br>Exemplares importados: ' . implode(', ', $arrInformation['importado']);
            }

            $this->setLastError($mensagem);
        }

        return true;
    }

    public function retornaArrayPeloTitulo($TituloId)
    {
        $arrObjExemplar = $this->pesquisaPeloTitulo($TituloId, false);
        $arrExemplar    = array();

        foreach ($arrObjExemplar as $objExemplar) {
            $arrItem = $objExemplar->toArray();
            $arrItem = array(
                'exemplarId'  => $arrItem['exemplarId'],
                'codigo'      => $arrItem['exemplarCodigo'],
                'emprestado'  => $arrItem['exemplarEmprestado'],
                'reservado'   => $arrItem['exemplarReservado'],
                'baixaData'   => (
                $arrItem['exemplarBaixaData']
                    ?
                    $objExemplar->getExemplarBaixaData()->format('Y-m-d H:i:s')
                    :
                    ''
                ),
                'baixaMotivo' => (string)$arrItem['exemplarBaixaMotivo'],
                'acesso'      => $arrItem['exemplarAcesso'],
                'localizacao' => (string)$arrItem['exemplarLocalizacao'],
                'titulo'      => $arrItem['titulo']['tituloId'],
                'setor'       => $arrItem['setor']['setorId'],
            );

            $arrExemplar[] = $arrItem;
        }

        return $arrExemplar;
    }

    public function efetuaBaixa($params)
    {
        $session    = new \Zend\Authentication\Storage\Session();
        $arrUsuario = $session->read();

        if (!$arrUsuario) {
            $this->setLastError('Verifique se você está autenticado no sistema!');

            return false;
        }

        $objServiceCalendarioData = new \Professor\Service\AcadperiodoCalendarioData($this->getEm());
        $arrFeriados              = $objServiceCalendarioData->feriadosDepoisDaData(date('Y-m-d'));

        $usuarioId               = $arrUsuario['id'];
        $objServiceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());
        /* @var $objUsuario \Acesso\Entity\AcessoPessoas */
        $objUsuario = $objServiceAcessoPessoas->getRepository()->find($usuarioId);

        if (!$objUsuario) {
            $this->setLastError('Verifique se você está autenticado no sistema!');

            return false;
        }

        $acao     = strtolower($params['acao']);
        $devolver = strpos($acao, 'devolver') !== false;
        $baixar   = strpos($acao, 'baixar') !== false;

        $objServiceEmprestimo = new \Biblioteca\Service\Emprestimo($this->getEm());

        if ($devolver && !$params['emprestimo_id']) {
            $this->setLastError('Para devolver um exemplar é necessário especificar o código do empréstimo.');

            return false;
        }

        if ($baixar && !$params['exemplar_id']) {
            $this->setLastError('Para dar baixa em um exemplar é necessário especificar o código do exemplar.');

            return false;
        }

        if ($devolver && !$objServiceEmprestimo->devolveEmprestimo(array('emprestimoId' => $params['emprestimo_id']))) {
            $this->setLastError($objServiceEmprestimo->getLastError());

            return false;
        }

        if ($baixar) {
            try {
                $objExemplar = $this->getRepository()->find($params['exemplar_id']);

                if (!$objExemplar) {
                    $this->setLastError('Exemplar não foi encontrado!');

                    return false;
                }

                $objExemplar->setExemplarDataAlteracao(new \DateTime());
                $objExemplar->setExemplarBaixaData(new \DateTime($params['exemplar_baixa_data']));
                $objExemplar->setExemplarBaixaMotivo($params['exemplar_baixa_motivo']);
                $objExemplar->setUsuario($objUsuario);
                $this->getEm()->persist($objExemplar);
                $this->getEm()->flush();
            } catch (\Exception $ex) {
                $this->setLastError('Ocorreu uma falha ao baixar exemplar!');

                return false;
            }
        }

        return true;
    }
}