<?php

namespace Biblioteca\Service;

use VersaSpine\Service\AbstractService;

class TituloAssunto extends AbstractService
{
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Biblioteca\Entity\TituloAssunto');
    }

    public function pesquisaForJson($a)
    {
        return array();
    }

    public function pesquisaPeloTitulo($tituloId, $toArray = true)
    {
        $objTituloAssunto = $this->getRepository();
        $arrResult        = $objTituloAssunto->findBy(array('titulo' => $tituloId));

        if ($toArray) {
            $arrRetorno = array();

            foreach ($arrResult as $objTituloAssunto) {
                $arrRetorno[] = $objTituloAssunto->toArray();
            }

            return $arrRetorno;
        }

        return $arrResult;
    }

    public function retornaAssuntoIdPeloTitulo($tituloId)
    {
        $arrAssuntos         = array();
        $arrObjTituloAssunto = $this->pesquisaPeloTitulo($tituloId, false);

        foreach ($arrObjTituloAssunto as $objTituloAssunto) {
            $arrAssuntos[] = $objTituloAssunto->getAssunto()->getAssuntoId();
        }

        return $arrAssuntos;
    }

    public function salvarArray($arrAssuntosTitulo, \Biblioteca\Entity\Titulo $objTitulo)
    {
        $objEntityManager = $this->getEm();
        $arrExcluir       = array();
        $arrEditar        = array();

        $arrTituloAssuntosDB = $this->pesquisaPeloTitulo($objTitulo->getTituloId(), false);

        foreach ($arrTituloAssuntosDB as $arrTituloAssuntoDB) {
            $encontrado      = false;
            $tituloAssuntoId = $arrTituloAssuntoDB->getTituloAssuntoId();

            foreach ($arrAssuntosTitulo as $strTituloAssunto) {
                if ($strTituloAssunto['tituloAssuntoId'] == $tituloAssuntoId) {
                    $encontrado                  = true;
                    $arrEditar[$tituloAssuntoId] = $arrTituloAssuntoDB;
                    break;
                }
            }

            if (!$encontrado) {
                $arrExcluir[$tituloAssuntoId] = $arrTituloAssuntoDB;
            }
        }

        if ($arrExcluir) {
            foreach ($arrExcluir as $objReg) {
                $objEntityManager->remove($objReg);
                $objEntityManager->flush();
            }
        }

        foreach ($arrAssuntosTitulo as $strTituloAssunto) {
            $objServiceAssunto = new \Biblioteca\Service\Assunto($objEntityManager);
            $objAssunto        = $objServiceAssunto->getRepository()->find($strTituloAssunto);

            if (!isset($arrEditar[$strTituloAssunto['tituloAssuntoId']])) {
                $objTituloAssunto = new \Biblioteca\Entity\TituloAssunto();
            } else {
                $objTituloAssunto = $arrEditar[$strTituloAssunto['tituloAssuntoId']];
            }

            $objTituloAssunto->setTitulo($objTitulo);
            $objTituloAssunto->setAssunto($objAssunto);

            $objEntityManager->persist($objTituloAssunto);
            $objEntityManager->flush();
        }

        return true;
    }

    public function retornaArrayPeloTitulo($TituloId)
    {
        $arrObjTituloAssunto = $this->pesquisaPeloTitulo($TituloId, false);
        $arrTituloAssunto    = array();

        foreach ($arrObjTituloAssunto as $objTituloAssunto) {
            $arrItem = $objTituloAssunto->toArray();
            $arrItem = array(
                'tituloAssuntoId' => $arrItem['tituloAssuntoId'],
                'titulo'          => $arrItem['titulo']['tituloId'],
                'assunto'         => $arrItem['assunto']['assuntoId'],
                'id'              => $arrItem['assunto']['assuntoId'],
                'text'            => $arrItem['assunto']['assuntoDescricao']
            );

            $arrTituloAssunto[] = $arrItem;
        }

        return $arrTituloAssunto;
    }

    protected function valida($dados)
    {
    }
}