<?php

namespace Biblioteca\Service;

use VersaSpine\Service\AbstractService;

class AssuntoRelacionado extends AbstractService
{
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Biblioteca\Entity\AssuntoRelacionado');
    }

    public function pesquisaForJson($a)
    {
        return array();
    }

    public function valida($arrParam)
    {
    }

    public function pesquisaPelaAssunto($assuntoIdDestino, $toArray = true)
    {
        $objAssuntoRelacionado = $this->getRepository();
        $arrResult             = $objAssuntoRelacionado->findBy(array('assuntoDestino' => $assuntoIdDestino));

        if ($toArray) {
            $arrRetorno = array();

            foreach ($arrResult as $objAssuntoRelacionado) {
                $arrRetorno[] = $objAssuntoRelacionado->toArray();
            }

            return $arrRetorno;
        }

        return $arrResult;
    }

    public function salvarArray($arrAssuntoRelacionados, \Biblioteca\Entity\Assunto $objAssunto)
    {
        $arrExcluir = array();
        $arrIgnorar = array();

        $arrAssuntoRelacionadosDB = $this->pesquisaPelaAssunto($objAssunto->getAssuntoId(), false);

        foreach ($arrAssuntoRelacionadosDB as $arrAssuntoRelacionadoDB) {
            $encontrado      = false;
            $assuntoOrigemId = $arrAssuntoRelacionadoDB->getAssuntoOrigem()->getAssuntoId();

            foreach ($arrAssuntoRelacionados as $assuntoRelacionado) {
                if ($assuntoRelacionado == $assuntoOrigemId) {
                    $encontrado                        = true;
                    $arrIgnorar[(int)$assuntoOrigemId] = $arrAssuntoRelacionadoDB;
                    break;
                }
            }

            if (!$encontrado) {
                $arrExcluir[$assuntoOrigemId] = $arrAssuntoRelacionadoDB;
            }
        }

        if ($arrExcluir) {
            foreach ($arrExcluir as $objReg) {
                $this->getEm()->remove($objReg);
                $this->getEm()->flush();
            }
        }

        foreach ($arrAssuntoRelacionados as $assuntoRelacionado) {
            if (!isset($arrIgnorar[(int)$assuntoRelacionado])) {
                $objServiceAssuntoOrigem = new \Biblioteca\Service\Assunto($this->getEm());
                $objAssuntoOrigem        = $objServiceAssuntoOrigem->getRepository()->find($assuntoRelacionado);

                if (!$objAssuntoOrigem) {
                    $this->setLastError('O assunto relacionado não existe!');

                    return false;
                }
                $objAssuntoRelacionado = new \Biblioteca\Entity\AssuntoRelacionado();
                $objAssuntoRelacionado->setAssuntoDestino($objAssunto);
                $objAssuntoRelacionado->setAssuntoOrigem($objAssuntoOrigem);

                $this->getEm()->persist($objAssuntoRelacionado);
                $this->getEm()->flush();
            }
        }

        return true;
    }

    public function retornaArrayPelaAssunto($AssuntoId)
    {
        $arrObjAssuntoRelacionado = $this->pesquisaPelaAssunto($AssuntoId, false);
        $arrAssuntoRelacionado    = array();

        foreach ($arrObjAssuntoRelacionado as $objAssuntoRelacionado) {
            $arrItem                     = $objAssuntoRelacionado->toArray();
            $arrItem['assuntoId']        = $arrItem['assuntoOrigem']['assuntoId'];
            $arrItem['assuntoDescricao'] = $arrItem['assuntoOrigem']['assuntoDescricao'];

            $arrAssuntoRelacionado[] = $arrItem;
        }

        return $arrAssuntoRelacionado;
    }
}