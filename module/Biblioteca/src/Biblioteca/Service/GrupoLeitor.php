<?php

namespace Biblioteca\Service;

use VersaSpine\Service\AbstractService;

class GrupoLeitor extends AbstractService
{
    const GRUPO_ALUNO_GRADUANDO = 1;

    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Biblioteca\Entity\GrupoLeitor');
    }

    public function pesquisaForJson($params)
    {
        $sql             = '
        SELECT
        a.grupoLeitorId AS grupo_leitor_id,
        a.grupoLeitorNome AS grupo_leitor_nome,
        a.acessoGrupoId AS acesso_grupo_id
        FROM Biblioteca\Entity\GrupoLeitor a
        WHERE';
        $grupoLeitorNome = false;
        $grupoLeitorId   = false;

        if ($params['q']) {
            $grupoLeitorNome = $params['q'];
        } elseif ($params['query']) {
            $grupoLeitorNome = $params['query'];
        }

        if ($params['grupoLeitorId']) {
            $grupoLeitorId = $params['grupoLeitorId'];
        }

        $parameters = array('grupoLeitorNome' => "{$grupoLeitorNome}%");
        $sql .= ' a.grupoLeitorNome LIKE :grupoLeitorNome';

        if ($grupoLeitorId) {
            $parameters['grupoLeitorId'] = explode(',', $grupoLeitorId);
            $parameters['grupoLeitorId'] = $grupoLeitorId;
            $sql .= ' AND a.grupoLeitorId NOT IN(:grupoLeitorId)';
        }

        $sql .= " ORDER BY a.grupoLeitorNome";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceAcessoGrupo = new \Acesso\Service\AcessoGrupo($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['grupoLeitorId']) {
                /** @var $objGrupoLeitor \Biblioteca\Entity\GrupoLeitor */
                $objGrupoLeitor = $this->getRepository()->find($arrDados['grupoLeitorId']);

                if (!$objGrupoLeitor) {
                    $this->setLastError('Registro de grupo leitor não existe!');

                    return false;
                }
            } else {
                $objGrupoLeitor = new \Biblioteca\Entity\GrupoLeitor();
            }

            $objGrupoLeitor->setGrupoLeitorNome($arrDados['grupoLeitorNome']);

            if ($arrDados['acessoGrupo']) {
                /** @var $objAcessoGrupo \Acesso\Entity\AcessoGrupo */
                $objAcessoGrupo = $serviceAcessoGrupo->getRepository()->find($arrDados['acessoGrupo']);

                if (!$objAcessoGrupo) {
                    $this->setLastError('Registro de grupo não existe!');

                    return false;
                }

                $objGrupoLeitor->setAcessoGrupo($objAcessoGrupo);
            } else {
                $objGrupoLeitor->setAcessoGrupo(null);
            }

            $this->getEm()->persist($objGrupoLeitor);
            $this->getEm()->flush($objGrupoLeitor);

            $this->getEm()->commit();

            $arrDados['grupoLeitorId'] = $objGrupoLeitor->getGrupoLeitorId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de grupo leitor!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['grupoLeitorNome']) {
            $errors[] = 'Por favor preencha o campo "nome"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM biblioteca__grupo_leitor";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($grupoLeitorId)
    {
        $arrDados = array();

        if (!$grupoLeitorId) {
            $this->setLastError('Grupo leitor inválido!');

            return array();
        }

        /** @var $objGrupoLeitor \Biblioteca\Entity\GrupoLeitor */
        $objGrupoLeitor = $this->getRepository()->find($grupoLeitorId);

        if (!$objGrupoLeitor) {
            $this->setLastError('Grupo leitor não existe!');

            return array();
        }

        $serviceAcessoGrupo = new \Acesso\Service\AcessoGrupo($this->getEm());

        try {
            $arrDados = $objGrupoLeitor->toArray();

            if ($arrDados['acessoGrupo']) {
                $arrAcessoGrupo          = $serviceAcessoGrupo->getArrSelect2(['id' => $arrDados['acessoGrupo']]);
                $arrDados['acessoGrupo'] = $arrAcessoGrupo ? $arrAcessoGrupo[0] : null;
            }
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceAcessoGrupo = new \Acesso\Service\AcessoGrupo($this->getEm());

        if (is_array($arrDados['acessoGrupo']) && !$arrDados['acessoGrupo']['text']) {
            $arrDados['acessoGrupo'] = $arrDados['acessoGrupo']['id'];
        }

        if ($arrDados['acessoGrupo'] && is_string($arrDados['acessoGrupo'])) {
            $arrDados['acessoGrupo'] = $serviceAcessoGrupo->getArrSelect2(array('id' => $arrDados['acessoGrupo']));
            $arrDados['acessoGrupo'] = $arrDados['acessoGrupo'] ? $arrDados['acessoGrupo'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['grupoLeitorId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['grupoLeitorNome' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Biblioteca\Entity\GrupoLeitor */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getGrupoLeitorId();
            $arrEntity[$params['value']] = $objEntity->getGrupoLeitorNome();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($grupoLeitorId)
    {
        if (!$grupoLeitorId) {
            $this->setLastError('Para remover um registro de grupo leitor é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objGrupoLeitor \Biblioteca\Entity\GrupoLeitor */
            $objGrupoLeitor = $this->getRepository()->find($grupoLeitorId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objGrupoLeitor);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de grupo leitor.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceAcessoGrupo = new \Acesso\Service\AcessoGrupo($this->getEm());

        $serviceAcessoGrupo->setarDependenciasView($view);
    }

    public function getModalidadesGrupo($grupoLeitorId)
    {
        $objModalidadeService      = new \Biblioteca\Service\ModalidadeEmprestimo($this->getEm());
        $objModalidadeGrupoService = new \Biblioteca\Service\ModalidadeEmprestimoGrupo($this->getEm());

        $arrObjModalidade = $objModalidadeService->getRepository()->findBy(array('grupoLeitor' => $grupoLeitorId));

        if (!$arrObjModalidade) {
            $this->setLastError('Sem modalidades cadastradas para o grupo de leitores.');

            return array();
        }

        $arrModalidades = array();

        foreach ($arrObjModalidade as $objModalidade) {
            $arrModalidade = $objModalidade->toArray();

            $arrModalidade['modalidadeEmprestimoGrupo'] = $objModalidadeGrupoService->retornaArrayPelaModalidadeEmprestimo(
                $arrModalidade['modalidadeEmprestimoId']
            );

            $arrModalidades[$arrModalidade['modalidadeEmprestimoId']] = $arrModalidade;
        }

        return $arrModalidades;
    }
}