<?php

namespace Biblioteca\Service;

use VersaSpine\Service\AbstractService;

class Suspensao extends AbstractService
{
    const MOTIVO_ATUALIZACAO = "SUSPENSÃO LIBERADA AUTOMATICAMENTE PELO SISTEMA APÓS A CRIAÇÃO DE UMA NOVA SUSPENSÃO";
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Biblioteca\Entity\Suspensao');
    }

    protected function valida($dados)
    {
    }

    protected function pesquisaForJson($criterios)
    {
    }

    public function retornarSuspensoesLeitor(\Biblioteca\Entity\Pessoa $objPessoa, $apenasAberto = true)
    {
        $arrObjSuspensao = array();

        $sql        = "
        SELECT s.emprestimo_id FROM biblioteca__suspensao s
        INNER JOIN biblioteca__emprestimo em ON em.emprestimo_id=s.emprestimo_id
        WHERE em.pes_id = :pesId";
        $parameters = array('pesId' => $objPessoa->getPes()->getPes()->getPesId());

        if ($apenasAberto) {
            $parameters = array_merge(array('dataAtual' => date('Y-m-d H:i:s')), $parameters);
            $sql        = $sql . '
            AND s.suspensao_liberacao_data IS NULL
            AND :dataAtual BETWEEN s.suspensao_inicio AND s.suspensao_fim';
        }

        $arrSuspensoes = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        foreach ($arrSuspensoes as $suspensao) {
            $arrObjSuspensao[] = $this->getRepository()->find($suspensao['emprestimo_id']);
        }

        return $arrObjSuspensao;
    }

    public function baixarTodasSuspensoesLeitor(\Biblioteca\Entity\Pessoa $objPessoa)
    {
        $arrObjSuspensao = $this->retornarSuspensoesLeitor($objPessoa, true);

        foreach ($arrObjSuspensao as $objSuspensao) {
            $objSuspensao->setSuspensaoLiberacaoData(new \DateTime());
            $objSuspensao->setSuspensaoLiberacaoMotivo(self::MOTIVO_ATUALIZACAO);
            $this->getEm()->persist($objSuspensao);
            $this->getEm()->flush($objSuspensao);
        }
    }

    public function criarSuspensaoLeitor(\Biblioteca\Entity\Emprestimo $objEmprestimo)
    {
        $this->baixarTodasSuspensoesLeitor($objEmprestimo->getPes());

        $session    = new \Zend\Authentication\Storage\Session();
        $arrUsuario = $session->read();

        if (!$arrUsuario) {
            $this->setLastError('Verifique se você está autenticado no sistema!');

            return false;
        }

        $usuarioId               = $arrUsuario['id'];
        $objServiceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());
        $objUsuario              = $objServiceAcessoPessoas->getRepository()->find($usuarioId);

        if (!$objUsuario) {
            $this->setLastError('Verifique se você está autenticado no sistema!');

            return false;
        }

        $diasSuspensao       = $objEmprestimo
            ->getModalidadeEmprestimo()
            ->getModalidadeEmprestimoDiasSuspensao();
        $dataSuspensaoInicio = new \DateTime('now', new \DateTimeZone('America/Sao_Paulo'));
        $dataSuspensaoFim    = new \DateTime('now', new \DateTimeZone('America/Sao_Paulo'));
        $dataSuspensaoFim->add(new \DateInterval('P' . $diasSuspensao . 'D'));

        $objSuspensao = new \Biblioteca\Entity\Suspensao();
        $objSuspensao
            ->setEmprestimo($objEmprestimo)
            ->setSuspensaoInicio($dataSuspensaoInicio)
            ->setSuspensaoFim($dataSuspensaoFim)
            ->setUsuario($objUsuario);

        $this->getEm()->persist($objSuspensao);
        $this->getEm()->flush($objSuspensao);
    }

    public function getJSONListingData($data)
    {
        $query = "
        SELECT
            s.emprestimo_id,
            p.pes_id, p.pes_nome,
            bme.modalidade_emprestimo_id,bme.modalidade_emprestimo_descricao,
            em.emprestimo_data, em.emprestimo_devolucao_data_previsao, em.emprestimo_devolucao_data_efetuada,
            s.suspensao_inicio, s.suspensao_fim,
            s.usuario_id, ap.login,
            s.suspensao_liberacao_data, s.suspensao_liberacao_motivo,
            bt.titulo_id, ex.exemplar_codigo,bt.titulo_titulo,
            bgb.grupo_bibliografico_id, bgb.grupo_bibliografico_nome,
            if(s.suspensao_liberacao_data is null, if(s.suspensao_fim < now(), 'Cumprida', 'Ativa'), 'Liberada') as suspensao_estado,
            if(s.suspensao_liberacao_data is null, if(s.suspensao_fim < now(), 2, 0), 1) as suspensao_ordem
        FROM biblioteca__suspensao s
            INNER JOIN biblioteca__emprestimo em ON em.emprestimo_id=s.emprestimo_id
            INNER JOIN biblioteca__modalidade_emprestimo bme ON bme.modalidade_emprestimo_id=em.modalidade_emprestimo_id
            INNER JOIN biblioteca__exemplar ex ON ex.exemplar_id=em.exemplar_id
            INNER JOIN biblioteca__titulo bt ON bt.titulo_id=ex.titulo_id
            INNER JOIN biblioteca__grupo_bibliografico bgb ON bgb.grupo_bibliografico_id=bt.grupo_bibliografico_id
            INNER JOIN pessoa p ON p.pes_id=em.pes_id
            INNER JOIN acesso_pessoas ap ON s.usuario_id=ap.id
        ORDER BY suspensao_ordem asc, s.suspensao_liberacao_data desc, s.suspensao_fim asc
        ";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function cancelaSuspensao($param)
    {
        if (!$param['emprestimoId']) {
            $this->setLastError('Para cancelar uma suspensão é necessário especificar o código da mesma.');

            return false;
        }

        if (!trim($param['motivo'])) {
            $this->setLastError('Para cancelar uma suspensão é necessário especificar um motivo.');

            return false;
        }

        $objSuspensao = $this->getRepository()->find($param['emprestimoId']);

        if (!$objSuspensao) {
            $this->setLastError('Suspensão não encontrada.');

            return false;
        }

        if ($objSuspensao->getSuspensaoLiberacaoData()) {
            $this->setLastError('Suspensão já foi liberada.');

            return false;
        }

        $session    = new \Zend\Authentication\Storage\Session();
        $arrUsuario = $session->read();

        if (!$arrUsuario) {
            $this->setLastError('Verifique se você está autenticado no sistema!');

            return false;
        }

        $usuarioId               = $arrUsuario['id'];
        $objServiceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());
        $objUsuario              = $objServiceAcessoPessoas->getRepository()->find($usuarioId);

        if (!$objUsuario) {
            $this->setLastError('Verifique se você está autenticado no sistema!');

            return false;
        }

        try {
            $objSuspensao
                ->setSuspensaoLiberacaoData(new \DateTime('now', new \DateTimeZone('America/Sao_Paulo')))
                ->setUsuario($objUsuario)
                ->setSuspensaoLiberacaoMotivo($param['motivo']);

            $this->getEm()->persist($objSuspensao);
            $this->getEm()->flush($objSuspensao);
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao liberar suspensão.');

            return false;
        }

        return true;
    }

    public function informacaoSuspensaoAberta($pesId)
    {
        $sql = '
        SELECT s.* FROM biblioteca__suspensao s
        INNER JOIN biblioteca__emprestimo em ON em.emprestimo_id=s.emprestimo_id
        WHERE em.pes_id = :pesId
            AND s.suspensao_liberacao_data IS NULL
            AND :dataAtual BETWEEN s.suspensao_inicio AND s.suspensao_fim';

        $parameters = array('pesId' => $pesId, 'dataAtual' => date('Y-m-d H:i:s'));
        $result     = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        if ($result[0]) {
            return array(
                'suspensaoInicio' => $result[0]['suspensao_inicio'],
                'suspensaoFim'    => $result[0]['suspensao_fim']
            );
        }

        return array();
    }
}