<?php

namespace Biblioteca\Service;

use VersaSpine\Service\AbstractService;

class TituloAutor extends AbstractService
{
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Biblioteca\Entity\TituloAutor');
    }

    public function pesquisaForJson($a)
    {
        return array();
    }

    public function valida($arrParam)
    {
        $errors = array();

        foreach ($arrParam as $arrParamAutor) {
            $arrRequerido = array(
                'autor' => 'Autor',
                'tipo'  => 'Tipo',
            );

            foreach ($arrRequerido as $req => $nome) {
                if (!$arrParamAutor[$req]) {
                    $errors[] = "Por favor preencha o campo \"$nome\" do autor " . $arrParamAutor['index'] . "!";
                }
            }
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function pesquisaPelaTitulo($tituloId, $toArray = true)
    {
        $objTituloAutor = $this->getRepository();
        $arrResult      = $objTituloAutor->findBy(array('titulo' => $tituloId));

        if ($toArray) {
            $arrRetorno = array();

            foreach ($arrResult as $objTituloAutor) {
                $arrRetorno[] = $objTituloAutor->toArray();
            }

            return $arrRetorno;
        }

        return $arrResult;
    }

    public function salvarArray($arrTituloAutors, \Biblioteca\Entity\Titulo $objTitulo, $cutter = false)
    {
        $objEntityManager = $this->getEm();
        $arrExcluir       = array();
        $arrEditar        = array();

        $arrTituloAutorsDB = $this->pesquisaPelaTitulo($objTitulo->getTituloId(), false);

        foreach ($arrTituloAutorsDB as $arrTituloAutorDB) {
            $encontrado    = false;
            $tituloAutorId = $arrTituloAutorDB->getTituloAutorId();

            foreach ($arrTituloAutors as $arrTituloAutor) {
                if ($arrTituloAutor['tituloAutorId'] == $tituloAutorId) {
                    $encontrado                = true;
                    $arrEditar[$tituloAutorId] = $arrTituloAutorDB;
                    break;
                }
            }

            if (!$encontrado) {
                $arrExcluir[$tituloAutorId] = $arrTituloAutorDB;
            }
        }

        if ($arrExcluir) {
            foreach ($arrExcluir as $objReg) {
                $objEntityManager->remove($objReg);
                $objEntityManager->flush();
            }
        }

        foreach ($arrTituloAutors as $arrTituloAutor) {
            $objServiceAutor = new \Biblioteca\Service\Autor($objEntityManager);
            $objAutor        = $objServiceAutor->getRepository()->find($arrTituloAutor['autor']);

            if (!isset($arrEditar[$arrTituloAutor['tituloAutorId']])) {
                $objTituloAutor = new \Biblioteca\Entity\TituloAutor();
            } else {
                $objTituloAutor = $arrEditar[$arrTituloAutor['tituloAutorId']];
            }

            if ($arrTituloAutor['tipo'] == 'Autor principal' && !$objAutor->getAutorCutter()) {
                if (preg_match('/^[A-Z][0-9]+/i', $cutter, $match, PREG_OFFSET_CAPTURE)) {
                    $objAutor->setAutorCutter($match[0][0]);

                    $objEntityManager->persist($objAutor);
                    $objEntityManager->flush($objAutor);
                }
            }

            $objTituloAutor->setTitulo($objTitulo);
            $objTituloAutor->setAutor($objAutor);
            $objTituloAutor->setTituloAutorTipo($arrTituloAutor['tipo']);

            $objEntityManager->persist($objTituloAutor);
            $objEntityManager->flush($objTituloAutor);
        }

        return true;
    }

    public function retornaArrayPeloTitulo($TituloId)
    {
        $arrObjTituloAutor = $this->pesquisaPelaTitulo($TituloId, false);
        $arrTituloAutor    = array();

        foreach ($arrObjTituloAutor as $objTituloAutor) {
            $arrItem = $objTituloAutor->toArray();
            $arrItem = array(
                'tituloAutorId' => $arrItem['tituloAutorId'],
                'tipo'          => $arrItem['tituloAutorTipo'],
                'titulo'        => $arrItem['titulo']['tituloId'],
                'autor'         => array(
                    'id'               => $arrItem['autor']['autorId'],
                    'text'             => $arrItem['autor']['autorReferencia'],
                    'autor_id'         => $arrItem['autor']['autorId'],
                    'autor_referencia' => $arrItem['autor']['autorReferencia'],
                    'autor_cutter'     => $arrItem['autor']['autorCutter'],
                ),
            );

            $arrTituloAutor[] = $arrItem;
        }

        return $arrTituloAutor;
    }
}
