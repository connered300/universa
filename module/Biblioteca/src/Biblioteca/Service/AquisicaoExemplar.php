<?php

namespace Biblioteca\Service;

use VersaSpine\Service\AbstractService;

class AquisicaoExemplar extends AbstractService
{
    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Biblioteca\Entity\AquisicaoExemplar');
    }

    public function pesquisaForJson($a)
    {
        return array();
    }

    protected function valida($dados)
    {
    }

    public function delete($exemplar = false, $aquisicao = false)
    {
        $objEntityManager     = $this->getEm();
        $objAquisicaoExemplar = $this->getRepository();

        $arrParam = array();

        if ($aquisicao) {
            $arrParam['aquisicao'] = $aquisicao;
        }

        if ($exemplar) {
            $arrParam['exemplar'] = $exemplar;
        }

        $arrResult = $objAquisicaoExemplar->findBy($arrParam);

        try {
            foreach ($arrResult as $objAquisicaoExemplar) {
                $objEntityManager->remove($objAquisicaoExemplar);
                $objEntityManager->flush();
            }

            return true;
        } catch (\Exception $e) {
        }

        return false;
    }

    public function getIdentificadoresExemplarPorAquisicao($aquisicao)
    {
        $arrExemplar           = array();
        $arrAquisicaoExemplars = $this->findByAquisicao($aquisicao, false);

        foreach ($arrAquisicaoExemplars as $objAquisicaoExemplar) {
            $exemplar               = $objAquisicaoExemplar->getExemplar()->getExemplarId();
            $arrExemplar[$exemplar] = $exemplar;
        }

        return $arrExemplar;
    }

    public function getIdentificadoresExemplarCodigoPorAquisicao($aquisicao)
    {
        $arrExemplar           = array();
        $arrAquisicaoExemplars = $this->findByAquisicao($aquisicao, false);

        foreach ($arrAquisicaoExemplars as $objAquisicaoExemplar) {
            $exemplar               = $objAquisicaoExemplar->getExemplar()->getExemplarCodigo();
            $arrExemplar[$exemplar] = $exemplar;
        }

        return $arrExemplar;
    }

    public function findByAquisicao($aquisicao, $toArray = true)
    {
        $objAquisicaoExemplar = $this->getRepository();
        $arrResult            = $objAquisicaoExemplar->findBy(array('aquisicao' => $aquisicao));

        if ($toArray) {
            $arrRetorno = array();

            foreach ($arrResult as $objAquisicaoExemplar) {
                $arrRetorno[] = $objAquisicaoExemplar->toArray();
            }

            return $arrRetorno;
        }

        return $arrResult;
    }
}