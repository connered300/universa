<?php

namespace Biblioteca\Service;

use VersaSpine\Service\AbstractService;

class Assunto extends AbstractService
{
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Biblioteca\Entity\Assunto');
    }

    public function pesquisaForJson($params)
    {
        $sql              = 'SELECT * FROM biblioteca__assunto WHERE';
        $assuntoDescricao = false;
        $assuntoId        = false;

        if ($params['q']) {
            $assuntoDescricao = $params['q'];
        } elseif ($params['query']) {
            $assuntoDescricao = $params['query'];
        }

        if ($params['assuntoId']) {
            $assuntoId = is_array($params['assuntoId']) ? $params['assuntoId'] : explode(',',$params['assuntoId']);
        }

        $parameters = array('assunto_descricao' => "{$assuntoDescricao}%");
        $sql .= ' assunto_descricao LIKE :assunto_descricao';

        if ($assuntoId) {
            $parameters['assunto_id'] = $assuntoId;
            $sql .= ' AND assunto_id not in(:assunto_id)';
        }

        $sql .= " ORDER BY assunto_descricao";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function getJSONListingData($data)
    {
        $query = "
        SELECT
            a.*,
            group_concat(aa.assunto_id) as assunto_relacionado_id,
            group_concat(aa.assunto_descricao) as assunto_relacionado_descricao,
            (
                SELECT COUNT(DISTINCT t.titulo_id)
                FROM biblioteca__titulo_assunto t
                WHERE t.assunto_id=a.assunto_id
            ) AS assunto_titulos
        FROM
            biblioteca__assunto a
            LEFT JOIN biblioteca__assunto_relacionado ar on ar.assunto_id_destino=a.assunto_id
            LEFT JOIN biblioteca__assunto aa on aa.assunto_id=ar.assunto_id_origem
        GROUP BY a.assunto_id";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function numeroTitulosDoAssunto($assuntoId)
    {
        $query      = "
        SELECT COUNT(DISTINCT t.titulo_id) as qtd
        FROM biblioteca__titulo_assunto t
        WHERE t.assunto_id=:assuntoId";
        $parameters = array('assuntoId' => $assuntoId);
        $result     = $this->executeQueryWithParam($query, $parameters)->fetch();

        return $result['qtd'];
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            $objAssuntoRelacionado = new \Biblioteca\Service\AssuntoRelacionado($this->getEm());

            if ($arrDados['assuntoId']) {
                $objAssunto = $this->getRepository()->find($arrDados['assuntoId']);

                if (!$objAssunto) {
                    $this->setLastError('Assunto não existe!');

                    return false;
                }
            } else {
                $objAssunto = new \Biblioteca\Entity\Assunto();
            }

            $objAssunto->setAssuntoDescricao($arrDados['assuntoDescricao']);

            $this->getEm()->persist($objAssunto);
            $this->getEm()->flush();

            if ($arrDados['assuntosRelacionados']) {
                $arrDados['assuntosRelacionados'] = explode(',', $arrDados['assuntosRelacionados']);
            } else {
                $arrDados['assuntosRelacionados'] = array();
            }

            if (!$objAssuntoRelacionado->salvarArray($arrDados['assuntosRelacionados'], $objAssunto)) {
                $this->setLastError('Falha ao víncular assuntos relacionados!');

                return false;
            }

            $this->getEm()->commit();

            $arrDados['assuntoId'] = $objAssunto->getAssuntoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o assunto!');
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        $arrRequerido = array(
            "assuntoDescricao" => "Descrição"
        );

        foreach ($arrRequerido as $req => $nome) {
            if (!$arrParam[$req]) {
                $errors[] = "Por favor preencha o campo \"$nome\"!";
            }
        }

        if ($this->verificaSeAssuntoEstaDuplicado($arrParam['assuntoDescricao'], $arrParam['assuntoId'])) {
            $errors[] = "Já existe um assunto cadastrado com esta mesma descrição!";
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function verificaSeAssuntoEstaDuplicado($assuntoDescricao, $assuntoId = false)
    {
        $sql        = 'SELECT count(*) as qtd FROM biblioteca__assunto WHERE assunto_descricao like :assuntoDescricao';
        $parameters = array('assuntoDescricao' => $assuntoDescricao);

        if ($assuntoId) {
            $sql .= ' AND assunto_id<>:assuntoId';
            $parameters['assuntoId'] = $assuntoId;
        }

        $result = $this->executeQueryWithParam($sql, $parameters)->fetch();

        return $result['qtd'] > 0;
    }

    public function getArray($assuntoId)
    {
        $arrData = $this->getReference($assuntoId);

        try {
            $arrData                   = $arrData->toArray();
            $arrData['assuntoTitulos'] = $this->numeroTitulosDoAssunto($assuntoId);
        } catch (\Exception $e) {
            return array();
        }

        return $arrData;
    }

    public function getArrayByPost(&$dadosAssunto)
    {
        $dadosAssunto['assuntosRelacionados'] = array();

        $objAssuntoRelacionado = new \Biblioteca\Service\AssuntoRelacionado($this->getEm());
        $arrAssuntoRelacionado = $objAssuntoRelacionado->retornaArrayPelaAssunto($dadosAssunto['assuntoId']);

        foreach ($arrAssuntoRelacionado as $assuntoRelacionado) {
            $dadosAssunto['assuntosRelacionados'][] = array(
                'id'   => $assuntoRelacionado['assuntoId'],
                'text' => $assuntoRelacionado['assuntoDescricao']
            );
        }
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('assuntoId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();

        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getAssuntoId(),
                $params['value'] => $objEntity->getAssuntoDescricao()
            );
        }

        return $arrEntitiesArr;
    }
}