<?php

namespace Biblioteca\Service;

use VersaSpine\Service\AbstractService;

class Setor extends AbstractService
{
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Biblioteca\Entity\Setor');
    }

    public function pesquisaForJson($params)
    {
        $sql            = 'SELECT * FROM biblioteca__setor WHERE';
        $setorDescricao = false;
        $setorId        = false;

        if ($params['q']) {
            $setorDescricao = $params['q'];
        } elseif ($params['query']) {
            $setorDescricao = $params['query'];
        }

        if ($params['setorId']) {
            $setorId = $params['setorId'];
        }

        $parameters = array('setor_descricao' => "{$setorDescricao}%", 'setor_sigla' => "{$setorDescricao}%");
        $sql .= ' setor_descricao LIKE :setor_descricao or setor_sigla LIKE :setor_sigla';

        if ($setorId) {
            $parameters['setor_id'] = $setorId;
            $sql .= ' AND setor_id <> :setor_id';
        }

        $sql .= " ORDER BY setor_sigla";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function getJSONListingData($data)
    {
        $query = "
        SELECT
            a.*,
            (
                SELECT COUNT(DISTINCT e.titulo_id)
                FROM biblioteca__exemplar e
                WHERE e.setor_id=a.setor_id
            ) as setor_titulos
        FROM biblioteca__setor a";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function numeroTitulosDoSetor($setorId)
    {
        $query      = "
        SELECT COUNT(DISTINCT t.titulo_id) as qtd
        FROM biblioteca__titulo t
        INNER JOIN biblioteca__exemplar e on e.titulo_id=t.titulo_id
        WHERE e.setor_id=:setorId";
        $parameters = array('setorId' => $setorId);
        $result     = $this->executeQueryWithParam($query, $parameters)->fetch();

        return $result['qtd'];
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['setorId']) {
                $objSetor = $this->getRepository()->find($arrDados['setorId']);

                if (!$objSetor) {
                    $this->setLastError('Setor não existe!');

                    return false;
                }
            } else {
                $objSetor = new \Biblioteca\Entity\Setor();
            }

            $objSetor->setSetorSigla($arrDados['setorSigla']);
            $objSetor->setSetorDescricao($arrDados['setorDescricao']);

            $this->getEm()->persist($objSetor);
            $this->getEm()->flush();

            $this->getEm()->commit();

            $arrDados['setorId'] = $objSetor->getSetorId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o setor!');
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        $arrRequerido = array(
            "setorSigla"     => "Sigla",
            "setorDescricao" => "Descrição",
        );

        foreach ($arrRequerido as $req => $nome) {
            if (!$arrParam[$req]) {
                $errors[] = "Por favor preencha o campo \"$nome\"!";
            }
        }

        $jaExiste = $this->verificaSeSetorEstaDuplicado(
            $arrParam['setorDescricao'],
            $arrParam['setorSigla'],
            $arrParam['setorId']
        );

        if ($jaExiste) {
            $errors[] = "Já existe um setor cadastrado com esta mesma sigla ou descrição!";
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function verificaSeSetorEstaDuplicado($setorDescricao, $setorSigla, $setorId = false)
    {
        $sql        = 'SELECT count(*) as qtd FROM biblioteca__setor WHERE (setor_descricao like :setorDescricao OR setor_sigla like :setorSigla)';
        $parameters = array('setorDescricao' => $setorDescricao, 'setorSigla' => $setorSigla);

        if ($setorId) {
            $sql .= ' AND setor_id<>:setorId';
            $parameters['setorId'] = $setorId;
        }

        $result = $this->executeQueryWithParam($sql, $parameters)->fetch();

        return $result['qtd'] > 0;
    }

    public function getArray($setorId)
    {
        $arrData = $this->getReference($setorId);

        try {
            $arrData                 = $arrData->toArray();
            $arrData['setorTitulos'] = $this->numeroTitulosDoSetor($setorId);
        } catch (\Exception $e) {
            return array();
        }

        return $arrData;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->find($params['id']);
            $arrEntities = $arrEntities ? array($arrEntities) : $arrEntities;
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();

        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getSetorId(),
                $params['value'] => $objEntity->getSetorDescricao()
            );
        }

        return $arrEntitiesArr;
    }
}