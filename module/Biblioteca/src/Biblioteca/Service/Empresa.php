<?php

namespace Biblioteca\Service;

use Pessoa\Service\TipoContato;
use Respect\Validation\Validator;
use VersaSpine\Service\AbstractService;
use Zend\I18n\Validator\DateTime;

/**
 * Class Empresa
 * @package Biblioteca\Service
 */
class Empresa extends AbstractService
{
    /**
     * @var null
     */
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Biblioteca\Entity\Empresa');
    }

    /**
     * @param $params
     * @return mixed
     */
    public function pesquisaForJson($params)
    {
        $sql              = '
    SELECT
        p.pes_id, p.pes_nome, pj.pes_nome_fantasia, pj.pes_cnpj, e.empresa_tipo,
        en.end_estado, en.end_cidade
    FROM biblioteca__empresa e
        INNER JOIN pessoa_juridica pj ON pj.pes_id = e.pes_id
        INNER JOIN pessoa p ON p.pes_id = pj.pes_id
        LEFT JOIN endereco en ON en.pes_id=e.pes_id
    WHERE';
        $empresaDescricao = false;
        $empresaId        = false;
        $empresaTipo      = array();

        if ($params['q']) {
            $empresaDescricao = $params['q'];
        } elseif ($params['query']) {
            $empresaDescricao = $params['query'];
        }

        if ($params['empresaId']) {
            $empresaId = $params['empresaId'];
        }

        if ($params['empresaTipo']) {
            $empresaTipo = explode(',', $params['empresaTipo']);
        }

        $parameters = array('empresa_descricao' => "{$empresaDescricao}%");
        $sql .= ' (pes_nome_fantasia COLLATE UTF8_GENERAL_CI LIKE :empresa_descricao)';

        if ($empresaId) {
            $parameters['empresa_id'] = $empresaId;
            $sql .= ' AND e.pes_id <> :empresa_id';
        }

        if ($empresaTipo) {
            $arrConditionsTipo = array();

            foreach ($empresaTipo as $posTipo => $tipoEmpresa) {
                $arrConditionsTipo[] = "FIND_IN_SET(:tipo$posTipo, e.empresa_tipo)";

                $parameters['tipo' . $posTipo] = "$tipoEmpresa";
            }

            $sql .= ' AND (' . implode(' OR ', $arrConditionsTipo) . ')';
        }

        $sql .= " ORDER BY pj.pes_nome_fantasia, p.pes_nome";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    /**
     * @param $data
     * @return array
     */
    public function getJSONListingData($data)
    {
        $query = "
        SELECT
            p.pes_id, p.pes_nome, p.pes_tipo, p.pes_nacionalidade,
            pj.pes_nome_fantasia, pj.pes_cnpj, pj.pes_insc_municipal, pj.pes_insc_estadual, pj.pes_sigla, pj.pes_natureza_juridica,
            en.end_id, en.tipo_endereco, en.end_pais, en.end_estado, en.end_tipo_end, en.end_bairro, en.end_cidade, en.end_cep,
            en.end_logradouro, en.end_tipo_logradouro, en.end_numero, en.end_complemento, en.end_data,
            p.con_contato_email,
            p.con_contato_telefone,
            e.empresa_observacao, e.empresa_website, e.empresa_tipo
        FROM biblioteca__empresa e
            INNER JOIN pessoa_juridica pj ON pj.pes_id = e.pes_id
            INNER JOIN pessoa p ON p.pes_id = e.pes_id
            LEFT JOIN endereco en ON en.pes_id=e.pes_id
        GROUP BY e.pes_id
        ORDER BY e.pes_id ASC";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param array $arrDados
     * @return \Pessoa\Entity\PessoaJuridica
     */
    private function vinculaPJEmpresa($pesId = false, $arrDados = array())
    {
        $objPessoaJuridica = null;

        if ($pesId) {
            $objPessoaJuridica = new \Pessoa\Service\PessoaJuridica($this->getEm());
            $objPessoaJuridica = $objPessoaJuridica->getRepository()->find($pesId);
        }

        if (!$objPessoaJuridica) {
            $objPessoaJuridica = new \Pessoa\Entity\PessoaJuridica(array());
        }

        $servicePessoa = new \Pessoa\Service\Pessoa($this->getEm());

        $objPessoa = $servicePessoa->salvarPessoa($arrDados);

        $objPessoaJuridica->setPes($objPessoa);
        $objPessoaJuridica->setPesCnpj($arrDados['pesCnpj']);
        $objPessoaJuridica->setPesInscEstadual($arrDados['pesInscEstadual']);
        $objPessoaJuridica->setPesNomeFantasia($arrDados['pesNomeFantasia']);
        $objPessoaJuridica->setPesDataInicio(date('Y-m-d'));

        $this->getEm()->persist($objPessoaJuridica);
        $this->getEm()->flush($objPessoaJuridica);

        return $objPessoaJuridica;
    }

    /**
     * @param array $arrDados
     * @return \Pessoa\Entity\Endereco
     */
    private function vinculaEnderecoEmpresa($endId)
    {
        $objEndereco = null;

        if ($endId) {
            $objEndereco = new \Pessoa\Service\Endereco($this->getEm());
            $objEndereco = $objEndereco->getRepository()->find($endId);
        }

        if (!$objEndereco) {
            $objEndereco     = new \Pessoa\Entity\Endereco(array());
            $objTipoEndereco = new \Pessoa\Service\TipoEndereco($this->getEm());

            $objTipoEndereco = $objTipoEndereco->getRepository()->find(1);

            $objEndereco->setTipoEndereco($objTipoEndereco);
            $objEndereco->setEndData('now');
        }

        return $objEndereco;
    }

    /**
     * @param bool|false $conId
     * @param int        $contatoTipo
     * @return \Pessoa\Entity\Contato
     */
    private function vinculaContatoEmpresa($conId = false, $contatoTipo = 1)
    {
        $objContato = null;

        if ($conId) {
            $objContato = new \Pessoa\Service\Contato($this->getEm());
            $objContato = $objContato->getRepository()->find($conId);
        }

        if (!$objContato) {
            $objContato     = new \Pessoa\Entity\Contato(array());
            $objTipoContato = new \Pessoa\Service\TipoContato($this->getEm());

            $objTipoContato = $objTipoContato->getRepository()->find($contatoTipo);

            $objContato->setTipoContato($objTipoContato);
        }

        return $objContato;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        if ($this->valida($arrDados)) {
            $objRepository = $this->getRepository();

            try {
                $this->getEm()->beginTransaction();

                $arrDados['pesNome'] = !empty($arrDados['pesNome']) ? $arrDados['pesNome'] : $arrDados['pesNomeFantasia'];

                $servicePJ = new \Pessoa\Service\PessoaJuridica($this->getEm());
                //Altera/Cria Pessoa Juridica
                $objPessoaJuridica = $servicePJ->salvarPessoaJuridica($arrDados['pesId'], $arrDados, true, true);

                $objEmpresa = null;

                if ($arrDados['pesId']) {
                    $objEmpresa = $objRepository->find($arrDados['pesId']);
                }

                if (!$objEmpresa) {
                    $objEmpresa = new \Biblioteca\Entity\Empresa();
                }

                //Altera/Cria Empresa
                $objEmpresa->setEmpresaTipo($arrDados['empresaTipo']);
                $objEmpresa->setEmpresaObservacao($arrDados['empresaObservacao']);
                $objEmpresa->setEmpresaWebsite($arrDados['empresaWebsite']);
                $objEmpresa->setPes($objPessoaJuridica);

                $this->getEm()->persist($objEmpresa);
                $this->getEm()->flush($objEmpresa);

                $this->getEm()->commit();

                return true;
            } catch (\Exception $e) {
                $this->setLastError('Não foi possível salvar empresa!');
            }
        }

        return false;
    }

    /**
     * @param        $pesCnpj
     * @param string $pesId
     * @return bool
     */
    public function verificaSeJaExisteCNPJ($pesCnpj, $pesId = "")
    {
        $sql        = '
        SELECT count(*) as qtd FROM biblioteca__empresa e
        INNER JOIN pessoa_juridica pj ON pj.pes_id = e.pes_id
        WHERE pj.pes_cnpj like :pesCnpj';
        $parameters = array('pesCnpj' => $pesCnpj);

        if ($pesId) {
            $sql .= ' AND pj.pes_id <> :pesId';
            $parameters['pesId'] = $pesId;
        }

        $result = $this->executeQueryWithParam($sql, $parameters)->fetch();

        return $result['qtd'] > 0;
    }

    /**
     * @param        $pesNomeFantasia
     * @param string $pesId
     * @return bool
     */
    public function verificaSeJaExisteNomeFantasia($pesNomeFantasia, $pesId = "")
    {
        $sql        = '
        SELECT count(*) as qtd FROM biblioteca__empresa e
        INNER JOIN pessoa_juridica pj ON pj.pes_id = e.pes_id
        INNER JOIN pessoa p ON p.pes_id = e.pes_id
        WHERE pj.pes_nome_fantasia like :pesNomeFantasia';
        $parameters = array('pesNomeFantasia' => $pesNomeFantasia);

        if ($pesId) {
            $sql .= ' AND pj.pes_id <> :pesId';
            $parameters['pesId'] = $pesId;
        }

        $result = $this->executeQueryWithParam($sql, $parameters)->fetch();

        return $result['qtd'] > 0;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        $arrRequerido = array(
            'pesNomeFantasia' => 'Nome fantasia',
            'endCidade'       => 'Cidade',
            'endEstado'       => 'Estado',
        );

        foreach ($arrRequerido as $req => $nome) {
            if (!$arrParam[$req]) {
                $errors[] = "Por favor preencha o campo \"$nome\"!";
            }
        }
        $pesCnpj = (int)preg_replace('/[^0-9]/', '', $arrParam['pesCnpj']);

        if ($pesCnpj != 0 && !Validator::cnpj()->validate($arrParam['pesCnpj'])) {
            $errors[] = "O CNPJ é inválido!";
        }
        if ($pesCnpj != 0 && $this->verificaSeJaExisteCNPJ($arrParam['pesCnpj'], $arrParam['pesId'])) {
            $errors[] = "Já existe uma empresa cadastrada com este CNPJ.";
        }
        if ($pesCnpj != 0 && $this->verificaSeJaExisteNomeFantasia($arrParam['pesNomeFantasia'], $arrParam['pesId'])) {
            $errors[] = "Já existe uma empresa cadastrada com este nome.";
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    /**
     * @param $pesId
     * @return array
     */
    public function getArray($pesId)
    {
        $objEmpresa = $this->getRepository()->find($pesId);

        if (!$objEmpresa) {
            $this->setLastError('Pessoa não existe!');

            return array();
        }

        $objServiceEndereco = new \Pessoa\Service\Endereco($this->getEm());
        $objServiceContato  = new \Pessoa\Service\Contato($this->getEm());

        try {
            $objPessoaJuridica  = $objEmpresa->getPes();
            $objPessoa          = $objPessoaJuridica->getPes();
            $objEndereco        = $objServiceEndereco->getRepository()->findOneBy(array('pes' => $pesId));
            $objContatoTelefone = $objServiceContato->getRepository()->findOneBy(
                array('pessoa' => $pesId, 'tipoContato' => TipoContato::TIPO_CONTATO_TELEFONE)
            );
            $objContatoEmail    = $objServiceContato->getRepository()->findOneBy(
                array('pessoa' => $pesId, 'tipoContato' => TipoContato::TIPO_CONTATO_EMAIL)
            );

            $arrData = array_merge(
                $objEmpresa->toArray(),
                $objPessoa->toArray(),
                $objPessoaJuridica->toArray(),
                ($objEndereco ? $objEndereco->toArray() : array())
            );

            if ($objContatoTelefone) {
                $arrData['conContatoTelefoneId'] = $objContatoTelefone->getConId();
                $arrData['conContatoTelefone']   = $objContatoTelefone->getConContato();
            }

            if ($objContatoEmail) {
                $arrData['conContatoEmailId'] = $objContatoEmail->getConId();
                $arrData['conContatoEmail']   = $objContatoEmail->getConContato();
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrData;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('pes' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();

        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getPes()->getPes()->getPesId(),
                $params['value'] => $objEntity->getPes()->getPesNomeFantasia()
            );
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['pesId']) {
            $this->setLastError('Para remover uma empresa é necessário especificar o código da mesma.');

            return false;
        }

        $objServiceTitulo    = new \Biblioteca\Service\Titulo($this->getEm());
        $objServiceAquisicao = new \Biblioteca\Service\Aquisicao($this->getEm());

        try {
            $qb         = $objServiceTitulo->getRepository()->createQueryBuilder('t');
            $numTitulos = $qb
                ->select('count(t.pesEditora)')
                ->where($qb->expr()->eq('t.pesEditora', $param['pesId']))
                ->getQuery()
                ->getSingleScalarResult();

            if ($numTitulos > 0) {
                $this->setLastError(
                    'Esta empresa está vinculada à ' . $numTitulos . ' título' . ($numTitulos > 1 ? 's' : '') . '.'
                );

                return false;
            }

            $qb            = $objServiceAquisicao->getRepository()->createQueryBuilder('t');
            $numAquisicoes = $qb
                ->select('count(t.pesFornecedor)')
                ->where($qb->expr()->eq('t.pesFornecedor', $param['pesId']))
                ->getQuery()
                ->getSingleScalarResult();

            if ($numAquisicoes > 0) {
                $this->setLastError(
                    'Esta empresa está vinculada à ' . $numAquisicoes . ' aquisiç' . ($numAquisicoes > 1 ? 'ões' : 'ão') . '.'
                );

                return false;
            }

            $objEmpresa = $this->getRepository()->find($param['pesId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objEmpresa);
            $this->getEm()->remove($objEmpresa->getPes());
            $this->getEm()->remove($objEmpresa->getPes()->getPes());
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover empresa.');

            return false;
        }

        return true;
    }

    public function getDescricao($editora = array())
    {
        $sql = "
        SELECT
            group_concat(pe.pes_nome_fantasia SEPARATOR ', ') as editora
        FROM biblioteca__empresa be
        JOIN pessoa_juridica pe USING(pes_id)
        WHERE pes_id in(" . implode(',', $editora) . ")";

        $result = $this->executeQueryWithParam($sql)->fetch();

        return $result['editora'];
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceCidade = new \Pessoa\Service\Cidade($this->getEm());

        $arrEstados = $serviceCidade->getEstadoArrSelect2();

        $view->setVariable("arrEstados", $arrEstados);
    }
}