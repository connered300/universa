<?php

namespace Biblioteca\Service;

use VersaSpine\Service\AbstractService;

class Idioma extends AbstractService
{
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Biblioteca\Entity\Idioma');
    }

    public function pesquisaForJson($params)
    {
        $sql             = 'SELECT * FROM biblioteca__idioma WHERE';
        $idiomaDescricao = false;
        $idiomaId        = false;

        if ($params['q']) {
            $idiomaDescricao = $params['q'];
        } elseif ($params['query']) {
            $idiomaDescricao = $params['query'];
        }

        if ($params['idiomaId']) {
            $idiomaId = $params['idiomaId'];
        }

        $parameters = array('idioma_descricao' => "{$idiomaDescricao}%");
        $sql .= ' idioma_descricao LIKE :idioma_descricao';

        if ($idiomaId) {
            $parameters['idioma_id'] = $idiomaId;
            $sql .= ' AND idioma_id <> :idioma_id';
        }

        $sql .= " ORDER BY idioma_descricao";
        $sql .= " LIMIT 0, 10";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function getJSONListingData($data)
    {
        $query = "
        SELECT a.*, (select count(DISTINCT titulo_id) from biblioteca__titulo_idioma t where t.idioma_id=a.idioma_id) as idioma_titulos
        FROM biblioteca__idioma a";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function numeroTitulosDoIdioma($idiomaId)
    {
        $query      = "select count(DISTINCT titulo_id) as qtd from biblioteca__titulo_idioma t where t.idioma_id=:idiomaId";
        $parameters = array('idiomaId' => $idiomaId);
        $result     = $this->executeQueryWithParam($query, $parameters)->fetch();

        return $result['qtd'];
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['idiomaId']) {
                $objIdioma = $this->getRepository()->find($arrDados['idiomaId']);

                if (!$objIdioma) {
                    $this->setLastError('Idioma não existe!');

                    return false;
                }
            } else {
                $objIdioma = new \Biblioteca\Entity\Idioma();
            }

            $objIdioma->setIdiomaDescricao($arrDados['idiomaDescricao']);

            $this->getEm()->persist($objIdioma);
            $this->getEm()->flush();

            $this->getEm()->commit();

            $arrDados['idiomaId'] = $objIdioma->getIdiomaId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar a idioma!');
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        $arrRequerido = array(
            "idiomaDescricao" => "Descrição"
        );

        foreach ($arrRequerido as $req => $nome) {
            if (!$arrParam[$req]) {
                $errors[] = "Por favor preencha o campo \"$nome\"!";
            }
        }

        if ($this->verificaSeIdiomaEstaDuplicado($arrParam['idiomaDescricao'], $arrParam['idiomaId'])) {
            $errors[] = "Já existe um idioma cadastrada com esta mesma descrição!";
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function verificaSeIdiomaEstaDuplicado($idiomaDescricao, $idiomaId = false)
    {
        $sql        = 'SELECT count(*) as qtd FROM biblioteca__idioma WHERE idioma_descricao like :idiomaDescricao';
        $parameters = array('idiomaDescricao' => $idiomaDescricao);

        if ($idiomaId) {
            $sql .= ' AND idioma_id<>:idiomaId';
            $parameters['idiomaId'] = $idiomaId;
        }

        $result = $this->executeQueryWithParam($sql, $parameters)->fetch();

        return $result['qtd'] > 0;
    }

    public function getArray($idiomaId)
    {
        $arrData = $this->getReference($idiomaId);

        try {
            $arrData                  = $arrData->toArray();
            $arrData['idiomaTitulos'] = $this->numeroTitulosDoIdioma($idiomaId);
        } catch (\Exception $e) {
            return array();
        }

        return $arrData;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('idiomaId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();

        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getIdiomaId(),
                $params['value'] => $objEntity->getIdiomaDescricao()
            );
        }

        return $arrEntitiesArr;
    }
}