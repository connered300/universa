<?php

namespace Biblioteca\Service;

use VersaSpine\Service\AbstractService;

class Colecao extends AbstractService
{
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Biblioteca\Entity\Colecao');
    }

    public function pesquisaForJson($params)
    {
        $sql              = 'SELECT * FROM biblioteca__colecao WHERE';
        $colecaoDescricao = false;
        $colecaoId        = false;

        if ($params['q']) {
            $colecaoDescricao = $params['q'];
        } elseif ($params['query']) {
            $colecaoDescricao = $params['query'];
        }

        if ($params['colecaoId']) {
            $colecaoId = $params['colecaoId'];
        }

        $parameters = array('colecao_descricao' => "{$colecaoDescricao}%");
        $sql .= ' colecao_descricao LIKE :colecao_descricao';

        if ($colecaoId) {
            $parameters['colecao_id'] = $colecaoId;
            $sql .= ' AND colecao_id <> :colecao_id';
        }

        $sql .= " ORDER BY colecao_descricao";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function getJSONListingData($data)
    {
        $query = "
        SELECT
            c.*,
            pj.pes_id as colecao_empresa_id,
            pj.pes_nome_fantasia as colecao_empresa_nome,
            (select count(*) from biblioteca__titulo t where t.colecao_id=c.colecao_id) as colecao_titulos
        FROM biblioteca__colecao c
             LEFT JOIN biblioteca__empresa e on e.pes_id=c.pes_id
             LEFT JOIN pessoa_juridica pj on pj.pes_id=e.pes_id
        ";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function numeroTitulosDaColecao($colecaoId)
    {
        $query      = "select count(*) as qtd from biblioteca__titulo t where t.colecao_id=:colecaoId";
        $parameters = array('colecaoId' => $colecaoId);
        $result     = $this->executeQueryWithParam($query, $parameters)->fetch();

        return $result['qtd'];
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['colecaoId']) {
                $objColecao = $this->getRepository()->find($arrDados['colecaoId']);

                if (!$objColecao) {
                    $this->setLastError('Coleção não existe!');

                    return false;
                }
            } else {
                $objColecao = new \Biblioteca\Entity\Colecao();
            }

            $objEmpresa = new \Biblioteca\Service\Empresa($this->getEm());
            $objEmpresa = $objEmpresa->getRepository()->find($arrDados['editora']);

            if (!$objEmpresa) {
                $this->setLastError('Editora não existe!');

                return false;
            }

            $objColecao->setPes($objEmpresa);
            $objColecao->setColecaoDescricao($arrDados['colecaoDescricao']);

            $this->getEm()->persist($objColecao);
            $this->getEm()->flush();

            $this->getEm()->commit();

            $arrDados['colecaoId'] = $objColecao->getColecaoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar a coleção!');
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        $arrRequerido = array(
            "colecaoDescricao" => "Descrição"
        );

        foreach ($arrRequerido as $req => $nome) {
            if (!$arrParam[$req]) {
                $errors[] = "Por favor preencha o campo \"$nome\"!";
            }
        }

        if ($this->verificaSeColecaoEstaDuplicado($arrParam['colecaoDescricao'], $arrParam['colecaoId'])) {
            $errors[] = "Já existe uma coleção cadastrada com esta mesma descrição!";
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function verificaSeColecaoEstaDuplicado($colecaoDescricao, $colecaoId = false)
    {
        $sql        = 'SELECT count(*) as qtd FROM biblioteca__colecao WHERE colecao_descricao like :colecaoDescricao';
        $parameters = array('colecaoDescricao' => $colecaoDescricao);

        if ($colecaoId) {
            $sql .= ' AND colecao_id<>:colecaoId';
            $parameters['colecaoId'] = $colecaoId;
        }

        $result = $this->executeQueryWithParam($sql, $parameters)->fetch();

        return $result['qtd'] > 0;
    }

    public function getArray($colecaoId)
    {
        $arrData = $this->getReference($colecaoId);

        try {
            $arrData                   = $arrData->toArray();
            $arrData['colecaoTitulos'] = $this->numeroTitulosDaColecao($colecaoId);
        } catch (\Exception $e) {
            return array();
        }

        return $arrData;
    }

    public function getArrayByPost(&$dadosColecao)
    {
        if (!isset($dadosColecao['editora']['text'])) {
            $idEditora  = !is_array($dadosColecao['editora']) ?
                $dadosColecao['pes']['pes']['pes'] :
                $dadosColecao['editora'];
            $objEmpresa = new \Biblioteca\Service\Empresa($this->getEm());
            $arrEmpresa = $objEmpresa->getArrSelect2(array('id' => $idEditora));

            $dadosColecao['editora'] = $arrEmpresa[0];
        }
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('colecaoId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();

        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getColecaoId(),
                $params['value'] => $objEntity->getColecaoDescricao()
            );
        }

        return $arrEntitiesArr;
    }
}