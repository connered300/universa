<?php

namespace Biblioteca\Service;

use VersaSpine\Service\AbstractService;

class Titulo extends AbstractService
{
    const ACESSO_EXTERNO_PORTAL_SIM = 'Sim';
    const ACESSO_EXTERNO_PORTAL_NAO = 'Não';

    /**
     * @var null
     */
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Biblioteca\Entity\Titulo');
    }

    /**
     * @param $data
     * @return array
     */
    public function pesquisaForJson($data)
    {
        $serviceGrupoBibliografico = new \Biblioteca\Service\GrupoBibliografico($this->getEm());
        //tamanho do group_concat para codigos de exemplares
        $this->executeQuery('SET SESSION group_concat_max_len = 1000000');

        $conditions = array();

        if ($data["filter"]['grupoBibliografico']) {
            $conditions[] = "gb.grupo_bibliografico_id in(" . $data["filter"]['grupoBibliografico'] . ")";
        }

        if ($data["filter"]['visualizarTituloPortal']) {
            $conditions[] = "t.titulo_acesso_portal= " . '"' . self::ACESSO_EXTERNO_PORTAL_SIM . '"';
        }

        if ($data["filter"]['editora']) {
            $conditions[] = "t.pes_id_editora in(" . $data["filter"]['editora'] . ")";
        }

        if ($data["filter"]['areaCnpq']) {
            $conditions[] = "t.area_cnpq_id in(" . $data["filter"]['areaCnpq'] . ")";
        }

        if ($data["filter"]['tituloTitulo']) {
            $conditions[] = 't.titulo_titulo LIKE "%' . $data["filter"]['tituloTitulo'] . '%"';
        }

        if ($data["filter"]['autor']) {
            $conditions[] = "t.titulo_id in" .
                "(SELECT titulo_id FROM biblioteca__titulo_autor WHERE autor_id in(" . $data["filter"]['autor'] . "))";
        }

        if ($data["filter"]['assunto']) {
            $conditions[] = "t.titulo_id in" .
                "(SELECT titulo_id FROM biblioteca__titulo_assunto WHERE assunto_id in(" . $data["filter"]['assunto'] . "))";
        }

        $query = "
    SELECT
        gb.grupo_bibliografico_id, gb.grupo_bibliografico_nome,
        t.titulo_id, t.titulo_titulo, t.titulo_edicao_volume, t.titulo_edicao_tomo,
        t.titulo_edicao_numero, t.titulo_edicao_isbn, t.titulo_edicao_cdu,
        t.titulo_edicao_cutter, t.titulo_edicao_ano,
        arquivo.arq_nome as arquivoDigital, capa.arq_chave,
        (
            SELECT group_concat(DISTINCT a.autor_referencia SEPARATOR '; ')
            FROM biblioteca__titulo_autor tau
              INNER JOIN biblioteca__autor a ON a.autor_id=tau.autor_id
            WHERE tau.titulo_id=t.titulo_id
        ) AS autor_referencia,
        t.pes_id_editora, pj.pes_nome_fantasia,
        -- ap.area_id, ap.area_descricao,
        -- ac.area_cnpq_id, ac.area_cnpq_codigo, ac.area_cnpq_descricao,
        (
            SELECT COUNT(ex.exemplar_id) FROM biblioteca__exemplar ex
            WHERE ex.titulo_id=t.titulo_id AND ex.exemplar_baixa_data IS NULL
        ) AS titulo_exemplares_quantidade,
        (
            SELECT COUNT(exd.exemplar_id) FROM biblioteca__exemplar exd
            WHERE exd.titulo_id=t.titulo_id AND exd.exemplar_acesso<>'Restrito' AND exd.exemplar_emprestado='N' AND exd.exemplar_baixa_data IS NULL
        ) AS titulo_exemplares_disponiveis,
        (
            SELECT group_concat(exc.exemplar_id) FROM biblioteca__exemplar exc
            WHERE exc.titulo_id=t.titulo_id
        ) AS titulo_exemplares_codigos,
        (
            SELECT group_concat(ass.assunto_descricao SEPARATOR ', ')
            FROM biblioteca__titulo_assunto tass
            INNER JOIN biblioteca__assunto ass ON ass.assunto_id=tass.assunto_id
            WHERE tass.titulo_id=t.titulo_id
        ) AS titulo_assuntos,      
        t.arq_id_arquivo temArquivo 
    FROM
        biblioteca__titulo t
        INNER JOIN biblioteca__grupo_bibliografico gb
            ON gb.grupo_bibliografico_id=t.grupo_bibliografico_id
        -- editora
        LEFT JOIN biblioteca__empresa e
            ON e.pes_id=t.pes_id_editora
        LEFT JOIN pessoa_juridica pj
            ON pj.pes_id=e.pes_id
        LEFT JOIN arquivo ON arquivo.arq_id = t.arq_id_arquivo
        LEFT JOIN arquivo capa ON capa.arq_id = t.arq_id_capa
        -- area do conhecimento principal
        -- LEFT JOIN biblioteca__area ap
        --    ON ap.area_id=t.area_id
        -- area de conhecimento CNPq
        -- LEFT JOIN biblioteca__area_cnpq ac
        --    ON ac.area_cnpq_id=t.area_cnpq_id
        -- idiomas do titulo
        -- LEFT JOIN biblioteca__titulo_idioma tid
        --    ON tid.titulo_id=t.titulo_id
        -- LEFT JOIN biblioteca__idioma i
        --    ON i.idioma_id=tid.idioma_id
    -- CONDITIONS --
    GROUP BY t.titulo_id";

        $conditions = $conditions ? "WHERE " . implode(' AND ', $conditions) : "";

        $query = str_replace('-- CONDITIONS --', $conditions, $query);

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function save(array &$arrDados)
    {
        $objServiceGrupoBibliografico = new \Biblioteca\Service\GrupoBibliografico($this->getEm());
        $objServiceArea               = new \Biblioteca\Service\Area($this->getEm());
        $objServiceAreaCnpq           = new \Biblioteca\Service\AreaCnpq($this->getEm());
        $objServiceColecao            = new \Biblioteca\Service\Colecao($this->getEm());
        $objServiceEmpresa            = new \Biblioteca\Service\Empresa($this->getEm());
        $objServiceTituloAssunto      = new \Biblioteca\Service\TituloAssunto($this->getEm());
        $serviceTituloAutor           = new \Biblioteca\Service\TituloAutor($this->getEm());
        $objServiceTituloIdioma       = new \Biblioteca\Service\TituloIdioma($this->getEm());
        $objServiceExemplar           = new \Biblioteca\Service\Exemplar($this->getEm());
        $objServiceAquisicao          = new \Biblioteca\Service\Aquisicao($this->getEm());
        $serviceArquivo               = new \GerenciadorArquivos\Service\Arquivo($this->getEm());

        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['tituloId']) {
                $objTitulo = $this->getRepository()->find($arrDados['tituloId']);

                if (!$objTitulo) {
                    $this->setLastError('Título não existe!');

                    return false;
                }
            } else {
                $objTitulo = new \Biblioteca\Entity\Titulo();
            }

            $arquivoImagem    = $arrDados['vfile'][$arrDados['arquivoImagem']];
            $objArquivoImagem = null;

            if ($arquivoImagem) {
                if (empty($arquivoImagem['raw']) && $objTitulo->getArquivoImagem()) {
                    /* @var $objArquivoImagem \GerenciadorArquivos\Entity\Arquivo */
                    $objArquivoImagem = $objTitulo->getArquivoImagem();

                    if (!empty($objArquivoImagem)) {
                        try {
                            $serviceArquivo->excluir($objArquivoImagem);
                        } catch (\Exception $ex) {
                            throw new \Exception('Falha ao remover arquivo antigo!');
                        }
                    }

                    $objArquivoImagem = null;
                } else {
                    $arrDados['arquivoImagem'] = ($arquivoImagem['raw']) ? $arquivoImagem['raw'] : null;

                    if ($arrDados['arquivoImagem'] && is_string($arrDados['arquivoImagem'])) {
                        $objArquivoImagem = $serviceArquivo->getRepository()->find($arrDados['arquivoImagem']);
                    }
                }

                if ($arquivoImagem['file']['error'] == 0 && $arquivoImagem['file']['size'] != 0) {
                    try {
                        $objArquivoImagem = $serviceArquivo->adicionar($arquivoImagem['file']);
                    } catch (\Exception $ex) {
                        throw new \Exception('Falha ao registrar arquivo!');
                    }
                }
            } else {
                $objArquivoImagem = null;
            }

            $arquivoDigital    = $arrDados['vfile'][$arrDados['arquivoDigital']];
            $objArquivoDigital = null;

            if ($arquivoDigital) {
                if (empty($arquivoDigital['raw']) && $objTitulo->getArquivoDigital()) {
                    /* @var $objArquivoDigital \GerenciadorArquivos\Entity\Arquivo */
                    $objArquivoDigital = $objTitulo->getArquivoDigital();

                    if (!empty($objArquivoDigital)) {
                        try {
                            $serviceArquivo->excluir($objArquivoDigital);
                        } catch (\Exception $ex) {
                            throw new \Exception('Falha ao remover arquivo antigo!');
                        }
                    }

                    $objArquivoDigital = null;
                } else {
                    $arrDados['arquivoDigital'] = ($arquivoDigital['raw']) ? $arquivoDigital['raw'] : null;

                    if ($arrDados['arquivoDigital'] && is_string($arrDados['arquivoDigital'])) {
                        $objArquivoDigital = $serviceArquivo->getRepository()->find($arrDados['arquivoDigital']);
                    }
                }

                if ($arquivoDigital['file']['error'] == 0 && $arquivoDigital['file']['size'] != 0) {
                    try {
                        $objArquivoDigital = $serviceArquivo->adicionar($arquivoDigital['file']);
                    } catch (\Exception $ex) {
                        throw new \Exception('Falha ao registrar arquivo!');
                    }
                }
            } else {
                $objArquivoDigital = null;
            }

            if ($arrDados['grupoBibliografico']) {
                $objGrupoBibliografico = $objServiceGrupoBibliografico->getRepository()->find(
                    $arrDados['grupoBibliografico']
                );

                if (!$objGrupoBibliografico) {
                    $this->setLastError('Grupo bibliografico não existe!');

                    return false;
                }

                $objTitulo->setGrupoBibliografico($objGrupoBibliografico);
            }

            if ($arrDados['area']) {
                $objArea = $objServiceArea->getRepository()->find($arrDados['area']);

                if (!$objArea) {
                    $this->setLastError('Área de conhecimento não existe!');

                    return false;
                }

                $objTitulo->setArea($objArea);
            }

            if ($arrDados['areaCnpq']) {
                $objAreaCnpq = $objServiceAreaCnpq->getRepository()->find($arrDados['areaCnpq']);

                if (!$objAreaCnpq) {
                    $this->setLastError('Área de conhecimento CNPq não existe!');

                    return false;
                }

                $objTitulo->setAreaCnpq($objAreaCnpq);
            }

            if ($arrDados['colecao']) {
                $objColecao = $objServiceColecao->getRepository()->find($arrDados['colecao']);

                if (!$objColecao) {
                    $this->setLastError('Coleção não existe!');

                    return false;
                }

                $objTitulo->setColecao($objColecao);
            }

            if ($arrDados['editora']) {
                $objEmpresa = $objServiceEmpresa->getRepository()->find($arrDados['editora']);

                if (!$objEmpresa) {
                    $this->setLastError('Editora não existe!');

                    return false;
                }

                $objTitulo->setPesEditora($objEmpresa);
            }

            if (!$arrDados['tituloId']) {
                $objTitulo->setTituloDataCadastro(new \DateTime());
            }

            $arrDados['tituloPaginas']      = $arrDados['tituloPaginas'] ? $arrDados['tituloPaginas'] : null;
            $arrDados['tituloEdicaoVolume'] = $arrDados['tituloEdicaoVolume'] ? $arrDados['tituloEdicaoVolume'] : null;
            $arrDados['tituloEdicaoNumero'] = $arrDados['tituloEdicaoNumero'] ? $arrDados['tituloEdicaoNumero'] : null;
            $arrDados['tituloEdicaoTomo']   = $arrDados['tituloEdicaoTomo'] ? $arrDados['tituloEdicaoTomo'] : null;

            $objTitulo->setArquivoDigital($objArquivoDigital);
            $objTitulo->setArquivoImagem($objArquivoImagem);
            $objTitulo->setTituloDataAlteracao(new \DateTime());
            $objTitulo->setTituloTitulo($arrDados['tituloTitulo']);
            $objTitulo->setTituloDescricao($arrDados['tituloDescricao']);
            $objTitulo->setTituloEdicaoAno($arrDados['tituloEdicaoAno']);
            $objTitulo->setTituloEdicaoLocal($arrDados['tituloEdicaoLocal'] || null);
            $objTitulo->setTituloEdicaoVolume($arrDados['tituloEdicaoVolume']);
            $objTitulo->setTituloEdicaoNumero($arrDados['tituloEdicaoNumero']);
            $objTitulo->setTituloEdicaoIsbn($arrDados['tituloEdicaoIsbn']);
            $objTitulo->setTituloEdicaoCdu($arrDados['tituloEdicaoCdu']);
            $objTitulo->setTituloEdicaoCutter($arrDados['tituloEdicaoCutter']);
            $objTitulo->setTituloEdicaoTomo($arrDados['tituloEdicaoTomo']);
            $objTitulo->setTituloPaginas($arrDados['tituloPaginas']);
            $objTitulo->setTituloNota($arrDados['tituloNota']);
            $objTitulo->setTituloRequisitosUso($arrDados['tituloRequisitosUso']);
            $objTitulo->setTituloAcessoPortal($arrDados['tituloAcessoPortal']);
            $objTitulo->setTituloSituacao('Ativo');

            $this->getEm()->persist($objTitulo);
            $this->getEm()->flush();

            if ($arrDados['assuntosRelacionados']) {
                $arrDados['assuntosRelacionados'] = explode(',', $arrDados['assuntosRelacionados']);
            } else {
                $arrDados['assuntosRelacionados'] = array();
            }

            if ($arrDados['idiomasRelacionados']) {
                $arrDados['idiomasRelacionados'] = explode(',', $arrDados['idiomasRelacionados']);
            } else {
                $arrDados['idiomasRelacionados'] = array();
            }

            if (!$objServiceTituloAssunto->salvarArray($arrDados['assuntosRelacionados'], $objTitulo)) {
                $this->setLastError('Falha ao víncular assuntos!');

                return false;
            }

            if (!$objServiceTituloIdioma->salvarArray($arrDados['idiomasRelacionados'], $objTitulo)) {
                $this->setLastError('Falha ao víncular idiomas!');

                return false;
            }

            if ($arrDados['autor']) {
                $salvouAutor = $serviceTituloAutor
                    ->salvarArray($arrDados['autor'], $objTitulo, $arrDados['tituloEdicaoCutter']);

                if (!$salvouAutor) {
                    $this->setLastError('Falha ao víncular autores! ' . $serviceTituloAutor->getLastError());

                    return false;
                }
            }

            $arrObjExemplares = array();

            if ($arrDados['exemplares']) {
                if (!$objServiceExemplar->salvarArray($arrDados['exemplares'], $objTitulo, $arrObjExemplares)) {
                    $this->setLastError('Falha ao víncular exemplares! ' . $objServiceExemplar->getLastError());

                    return false;
                } else {
                    $arrDados['exemplarInformation'] = $objServiceExemplar->getLastError();
                }
            }

            if (!$objServiceAquisicao->salvarArray($arrDados['aquisicao'], $objTitulo, $arrObjExemplares)) {
                $this->setLastError('Falha ao víncular aquisições!');

                return false;
            }

            $this->getEm()->commit();

            $arrDados['tituloId'] = $objTitulo->getTituloId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o título!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $objServiceTituloAutor     = new \Biblioteca\Service\TituloAutor($this->getEm());
        $objServiceExemplar        = new \Biblioteca\Service\Exemplar($this->getEm());
        $objServiceAquisicao       = new \Biblioteca\Service\Aquisicao($this->getEm());
        $serviceGrupoBibliografico = new \Biblioteca\Service\GrupoBibliografico($this->getEm());

        $objGrupoBibliografico = $serviceGrupoBibliografico->getRepository()->findOneBy(
            ['grupoBibliograficoId' => $arrParam['grupoBibliografico']]
        );

        /** @var $objGrupoBibliografico \Biblioteca\Entity\GrupoBibliografico */
        if (!$objGrupoBibliografico) {
            $errors[] = "Por favor preencha o campo Grupo Bibliográfico!";
        }

        $errors = array();

        $arrRequerido = array(
            'tituloTitulo'         => 'Título',
            //'tituloPaginas'        => 'Número de Páginas',
            'area'                 => 'Área de Conhecimento',
            'areaCnpq'             => 'Área de Conhecimento CNPq',
            'assuntosRelacionados' => 'Assuntos Relacionados',
            'idiomasRelacionados'  => 'Idiomas',
            //'tituloEdicaoVolume'   => 'Volume da Edição',
            //'tituloEdicaoTomo'     => 'Tomo da Edição',
            //'tituloEdicaoIsbn'     => 'ISBN da Edição',
            //'tituloEdicaoNumero'   => 'Número da Edição',
            'tituloEdicaoAno'      => 'Ano da Edição',
        );

        foreach ($arrRequerido as $req => $nome) {
            if (!$arrParam[$req]) {
                $errors[] = "Por favor preencha o campo \"$nome\"!";
            }
        }

        if ($objGrupoBibliografico->possuiLocalEdicao()) {
            if (!$arrParam['tituloEdicaoLocal']) {
                $errors[] = "Por favor preencha o campo Local !";
            }
        }

        if ($objGrupoBibliografico->possuiAutor()) {
            if (!$objServiceTituloAutor->valida($arrParam['autor'])) {
                $errors[] = 'Autores: ' . $objServiceTituloAutor->getLastError();
            }
        }

        if ($objGrupoBibliografico->possuiExemplares()) {
            if (!$objServiceExemplar->valida($arrParam['exemplares'])) {
                $errors[] = 'Exemplares: ' . $objServiceExemplar->getLastError();
            }
        }

        if (!$objServiceAquisicao->valida($arrParam['aquisicao'])) {
            $errors[] = 'Aquisições: ' . $objServiceAquisicao->getLastError();
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getArray($tituloId)
    {
        try {
            /** @var \Biblioteca\Entity\Titulo $objTitulo */
            $objTitulo                 = $this->getRepository()->find($tituloId);
            $objServiceTituloAssunto = new \Biblioteca\Service\TituloAssunto($this->getEm());
            $objServiceTituloAutor   = new \Biblioteca\Service\TituloAutor($this->getEm());
            $objServiceTituloIdioma  = new \Biblioteca\Service\TituloIdioma($this->getEm());
            $objServiceExemplar      = new \Biblioteca\Service\Exemplar($this->getEm());
            $objServiceAquisicao     = new \Biblioteca\Service\Aquisicao($this->getEm());

            $arrData                    = $objTitulo->toArray();
            $arrData['tituloTitulo']    = htmlspecialchars($arrData['tituloTitulo']);
            $arrData['tituloDescricao'] = htmlspecialchars($arrData['tituloDescricao']);

            $arrData['area']     = array(
                'id'   => $arrData['area']['areaId'],
                'text' => $arrData['area']['areaDescricao']
            );
            $arrData['areaCnpq'] = array(
                'id'   => $arrData['areaCnpq']['areaCnpqId'],
                'text' => $arrData['areaCnpq']['areaCnpqDescricao']
            );
            if ($arrData['colecao']) {
                $arrData['colecao'] = array(
                    'id'   => $arrData['colecao']['colecaoId'],
                    'text' => $arrData['colecao']['colecaoDescricao']
                );
            }

            if ($arrData['editora']) {
                $arrData['editora'] = array(
                    'id'   => $arrData['pesEditora']['pes']['pes'],
                    'text' => $arrData['pesEditora']['pes']['pesNomeFantasia']
                );
            }
            $arrData['grupoBibliografico']   = array(
                'id'                             => $arrData['grupoBibliografico']['grupoBibliograficoId'],
                'text'                           => $arrData['grupoBibliografico']['grupoBibliograficoNome'],
                'grupoBibliograficoLocalEdicao'  => $arrData['grupoBibliografico']['grupoBibliograficoLocalEdicao'],
                'grupoBibliograficoAutor'        => $arrData['grupoBibliografico']['grupoBibliograficoAutor'],
                'grupoBibliograficoExemplares'   => $arrData['grupoBibliografico']['grupoBibliograficoExemplares'],
                'grupoBibliograficoAcessoPortal' => $arrData['grupoBibliografico']['grupoBibliograficoAcessoPortal'],

            );
            $arrData['assuntosRelacionados'] = $objServiceTituloAssunto->retornaArrayPeloTitulo($tituloId);
            $arrData['idiomasRelacionados']  = $objServiceTituloIdioma->retornaArrayPeloTitulo($tituloId);
            $arrData['autor']                = $objServiceTituloAutor->retornaArrayPeloTitulo($tituloId);
            $arrData['exemplares']           = $objServiceExemplar->getExemplaresTitulo($tituloId);
            $arrData['aquisicao']            = $objServiceAquisicao->retornaArrayPeloTitulo($tituloId);
        } catch (\Exception $e) {
            $this->setLastError('Falha ao carregar dados de título!');

            return array();
        }

        return $arrData;
    }

    public function getArrayByPost(&$dadosTitulo)
    {
        $arrDataNormalized = array();

        if ($dadosTitulo['grupoBibliografico'] && !is_array($dadosTitulo['grupoBibliografico'])) {
            $objGrupoBibliografico = new \Biblioteca\Service\GrupoBibliografico($this->getEm());
            $arrSelect2            = $objGrupoBibliografico->getArrSelect2(
                array('id' => $dadosTitulo['grupoBibliografico'])
            );

            $arrDataNormalized['grupoBibliografico'] = $arrSelect2[0];
        } elseif (is_array($dadosTitulo['grupoBibliografico'])) {
            $arrDataNormalized['grupoBibliografico'] = $dadosTitulo['grupoBibliografico'];
        }

        if ($dadosTitulo['area'] && !is_array($dadosTitulo['area'])) {
            $objArea    = new \Biblioteca\Service\Area($this->getEm());
            $arrSelect2 = $objArea->getArrSelect2(array('id' => $dadosTitulo['area']));

            $arrDataNormalized['area'] = $arrSelect2[0];
        } elseif (is_array($dadosTitulo['area'])) {
            $arrDataNormalized['area'] = $dadosTitulo['area'];
        }

        if ($dadosTitulo['areaCnpq'] && !is_array($dadosTitulo['areaCnpq'])) {
            $objAreaCnpq = new \Biblioteca\Service\AreaCnpq($this->getEm());
            $arrSelect2  = $objAreaCnpq->getArrSelect2(array('id' => $dadosTitulo['areaCnpq']));

            $arrDataNormalized['areaCnpq'] = $arrSelect2[0];
        } elseif (is_array($dadosTitulo['areaCnpq'])) {
            $arrDataNormalized['areaCnpq'] = $dadosTitulo['areaCnpq'];
        }

        if ($dadosTitulo['colecao'] && !is_array($dadosTitulo['colecao'])) {
            $objColecao = new \Biblioteca\Service\Colecao($this->getEm());
            $arrSelect2 = $objColecao->getArrSelect2(array('id' => $dadosTitulo['colecao']));

            $arrDataNormalized['colecao'] = $arrSelect2[0];
        } elseif (is_array($dadosTitulo['colecao'])) {
            $arrDataNormalized['colecao'] = $dadosTitulo['colecao'];
        }

        if ($dadosTitulo['idiomasRelacionados']) {
            if (!is_array($dadosTitulo['idiomasRelacionados'])) {
                $dadosTitulo['idiomasRelacionados'] = explode(',', $dadosTitulo['idiomasRelacionados']);
            }

            if (!is_array($dadosTitulo['idiomasRelacionados'][0])) {
                $objIdioma  = new \Biblioteca\Service\Idioma($this->getEm());
                $arrSelect2 = $objIdioma->getArrSelect2(array('id' => $dadosTitulo['idiomasRelacionados']));

                $arrDataNormalized['idiomasRelacionados'] = $arrSelect2;
            } elseif (is_array($dadosTitulo['idiomasRelacionados'])) {
                $arrDataNormalized['idiomasRelacionados'] = $dadosTitulo['idiomasRelacionados'];
            }
        }

        if ($dadosTitulo['assuntosRelacionados']) {
            if (!is_array($dadosTitulo['assuntosRelacionados'])) {
                $dadosTitulo['assuntosRelacionados'] = explode(',', $dadosTitulo['assuntosRelacionados']);
            }

            if (!is_array($dadosTitulo['assuntosRelacionados'][0])) {
                $objAssunto = new \Biblioteca\Service\Assunto($this->getEm());
                $arrSelect2 = $objAssunto->getArrSelect2(array('id' => $dadosTitulo['assuntosRelacionados']));

                $arrDataNormalized['assuntosRelacionados'] = $arrSelect2;
            } elseif (is_array($dadosTitulo['assuntosRelacionados'])) {
                $arrDataNormalized['assuntosRelacionados'] = $dadosTitulo['assuntosRelacionados'];
            }
        }

        if ($dadosTitulo['editora'] && !is_array($dadosTitulo['editora'])) {
            $objEmpresa = new \Biblioteca\Service\Empresa($this->getEm());
            $arrSelect2 = $objEmpresa->getArrSelect2(array('id' => $dadosTitulo['editora']));

            $arrDataNormalized['editora'] = $arrSelect2[0];
        } elseif (is_array($dadosTitulo['editora'])) {
            $arrDataNormalized['editora'] = $dadosTitulo['editora'];
        }

        if ($dadosTitulo['autor']) {
            $objAutor                   = new \Biblioteca\Service\Autor($this->getEm());
            $arrDataNormalized['autor'] = array();

            foreach ($dadosTitulo['autor'] as $autor) {
                if (!is_array($autor['autor'])) {
                    $arrSelect2     = $objAutor->getArrSelect2(array('id' => $autor['autor']));
                    $autor['autor'] = $arrSelect2[0];
                }

                $arrDataNormalized['autor'][] = $autor;
            }
        }

        if ($dadosTitulo['exemplares']) {
            $arrDataNormalized['exemplares'] = $dadosTitulo['exemplares'];
        }

        if ($dadosTitulo['aquisicao']) {
            $arrDataNormalized['aquisicao'] = $dadosTitulo['aquisicao'];
            $objEmpresa                     = new \Biblioteca\Service\Empresa($this->getEm());
            $objPessoa                      = new \Biblioteca\Service\Pessoa($this->getEm());
            $arrDataNormalized['aquisicao'] = array();

            foreach ($dadosTitulo['aquisicao'] as $aquisicao) {
                if ($aquisicao['tipo'] == 'Compra' && !is_array($aquisicao['fornecedor'])) {
                    $arrSelect2              = $objEmpresa->getArrSelect2(array('id' => $aquisicao['fornecedor']));
                    $aquisicao['fornecedor'] = $arrSelect2[0];
                } elseif ($aquisicao['tipo'] == 'Doacao' && !is_array($aquisicao['doador'])) {
                    $arrSelect2          = $objPessoa->getArrSelect2(array('id' => $aquisicao['doador']));
                    $aquisicao['doador'] = $arrSelect2[0];
                }

                if (!is_array($aquisicao['exemplares'])) {
                    $arrExemplares           = explode(',', $aquisicao['exemplares']);
                    $aquisicao['exemplares'] = array();

                    foreach ($arrExemplares as $strExemplar) {
                        $aquisicao['exemplares'][] = array('id' => $strExemplar, 'text' => $strExemplar);
                    }
                }

                $arrDataNormalized['aquisicao'][] = $aquisicao;
            }
        }

        $dadosTitulo['value'] = $arrDataNormalized;
    }

    /**
     * @param $filter
     * @return array
     */
    public function relatorioRankingEmprestimo($filter)
    {
        $conditions = array();
        $param      = array();

        if ($filter['grupoBibliografico']) {
            $conditions[] = "t.grupo_bibliografico_id in(" . $filter['grupoBibliografico'] . ")";
        }

        if ($filter['editora']) {
            $conditions[] = "t.pes_id_editora in(" . $filter['editora'] . ")";
        }

        if ($filter['areaCnpq']) {
            $conditions[] = "t.area_cnpq_id in(" . $filter['areaCnpq'] . ")";
        }

        if ($filter['autor']) {
            $conditions[] = "t.titulo_id in" .
                "(SELECT titulo_id FROM biblioteca__titulo_autor WHERE autor_id in(" . $filter['autor'] . "))";
        }

        if ($filter['assunto']) {
            $conditions[] = "t.titulo_id in" .
                "(SELECT titulo_id FROM biblioteca__titulo_assunto WHERE assunto_id in(" . $filter['assunto'] . "))";
        }

        if ($filter['ocultarTituloZerado'] == 's') {
            $conditions[] = "(
                SELECT COUNT(exd.exemplar_id) FROM biblioteca__exemplar exd
                WHERE exd.titulo_id=t.titulo_id AND exd.exemplar_baixa_data is null
            )>0";
        }

        if ($filter['dataInicial']) {
            $param['dataInicial'] = $filter['dataInicial'];
        }

        if ($filter['dataFinal']) {
            $param['dataFinal'] = $filter['dataFinal'];
        }

        $query = "
    SELECT
        t.grupo_bibliografico_id,
        t.titulo_id, t.titulo_titulo, t.titulo_edicao_volume, t.titulo_edicao_tomo,
        t.titulo_edicao_numero, t.titulo_edicao_isbn, t.titulo_edicao_cdu,
        t.titulo_edicao_cutter, t.titulo_edicao_ano,
        t.pes_id_editora,
        (
            SELECT COUNT(ex.exemplar_id) FROM biblioteca__exemplar ex
            INNER JOIN biblioteca__emprestimo em ON em.exemplar_id = ex.exemplar_id
            WHERE ex.titulo_id=t.titulo_id AND
                  em.emprestimo_data BETWEEN date(:dataInicial) AND date(:dataFinal)
        ) AS titulo_emprestimo_num,
        (
            SELECT COUNT(exd.exemplar_id) FROM biblioteca__exemplar exd
            WHERE exd.titulo_id=t.titulo_id AND exd.exemplar_baixa_data IS NULL
        ) AS titulo_exemplares_disponiveis
    FROM
        biblioteca__titulo t
        INNER JOIN biblioteca__grupo_bibliografico gb
            ON gb.grupo_bibliografico_id=t.grupo_bibliografico_id
    -- CONDITIONS --
    ORDER BY titulo_emprestimo_num " . ($filter['rankingTipo'] == 'mais' ? 'DESC' : 'ASC') . "
    LIMIT 0," . $filter['numeroRegistros'];

        $conditions = $conditions ? "WHERE " . implode(' AND ', $conditions) : "";

        $query = str_replace('-- CONDITIONS --', $conditions, $query);

        $result = $this->executeQueryWithParam($query, $param)->fetchAll();

        return $result;
    }

    public function retornaQuantidadeTitulosEmprestados($arrParam)
    {
        if (!isset($arrParam['pesId'])) {
            $this->setLastError("É necessário informar uma pessoa!");

            return false;
        }

        $sql = '
        SELECT COUNT(DISTINCT(emprestimo_id)) qtdTitulosEmprestados FROM biblioteca__emprestimo
        WHERE 1 AND pes_id IN (:pesId)         
        ';

        $arrFilter['pesId'] = $arrParam['pesId'];

        if($arrParam['titulosAtivos']){
            $sql .= ' AND emprestimo_devolucao_data_efetuada IS NULL ';
        }

        $result = $this->executeQueryWithParam($sql, $arrFilter)->fetch();

        return (int)$result['qtdTitulosEmprestados'];
    }
}
