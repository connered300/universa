<?php

namespace Biblioteca\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class Doador
 * @package Biblioteca\Service
 */
class Doador extends AbstractService
{
    /**
     * @var null
     */
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Biblioteca\Entity\Empresa');
    }

    protected function valida($dados)
    {
    }

    protected function pesquisaForJson($params)
    {
        $sql     = '
    SELECT * FROM(
        SELECT
            p.pes_id, coalesce(pj.pes_nome_fantasia,p.pes_nome) as pes_nome, pj.pes_cnpj as pes_doc,
            "empresa" as pes_tipo
        FROM biblioteca__empresa e
            INNER JOIN pessoa_juridica pj ON pj.pes_id = e.pes_id
            INNER JOIN pessoa p ON p.pes_id = pj.pes_id
        UNION
        SELECT
            p.pes_id, p.pes_nome, pf.pes_cpf as pes_doc,
            "pessoa" as pes_tipo
        FROM biblioteca__pessoa bp
            INNER JOIN pessoa_fisica pf ON pf.pes_id = bp.pes_id
            INNER JOIN pessoa p ON p.pes_id = bp.pes_id
    ) AS doadores
    WHERE';
        $pesNome = false;
        $pesId   = false;
        $pesTipo = false;

        if ($params['q']) {
            $pesNome = $params['q'];
        } elseif ($params['query']) {
            $pesNome = trim($params['query']);
        }

        if ($params['pesId']) {
            $pesId = trim($params['pesId']);
        }

        if ($params['tipo']) {
            $pesTipo = $params['tipo'];
        }

        $parameters = array('pes_nome' => "{$pesNome}%");
        $sql .= ' (pes_nome COLLATE UTF8_GENERAL_CI LIKE :pes_nome OR pes_doc COLLATE UTF8_GENERAL_CI LIKE :pes_nome)';

        if ($pesId) {
            $parameters['pes_id'] = $pesId;
            $sql .= ' AND pes_id <> :pes_id';
        }

        if ($pesTipo) {
            $parameters['pes_tipo'] = $pesTipo;
            $sql .= " AND pes_tipo=:pes_tipo";
        }

        $sql .= " ORDER BY pes_nome";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }
}