<?php

namespace Biblioteca\Service;

use VersaSpine\Service\AbstractService;

class Multa extends AbstractService
{
    const MULTA_OBSERVACAO = 'ENTREGA DO EXEMPLAR %d DO TÍTULO "%s" COM %d DIAS DE ATRASO (VALOR POR DIA: %.2f CONSIDERANDO %s).';
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Biblioteca\Entity\Multa');
    }

    public function informacaoMultasAbertas($pesId)
    {
        $query
            = "
        SELECT
            count(ft.titulo_id) multa_qtd,
            sum(ft.titulo_valor) multa_valor
        FROM biblioteca__multa m
            INNER JOIN biblioteca__emprestimo em ON em.emprestimo_id=m.emprestimo_id
            -- INNER JOIN biblioteca__exemplar ex ON ex.exemplar_id=em.exemplar_id
            -- INNER JOIN biblioteca__titulo bt ON bt.titulo_id=ex.titulo_id
            INNER JOIN financeiro__titulo ft ON m.titulo_id=ft.titulo_id
        WHERE
            em.pes_id=:pes_id AND ft.titulo_estado='Aberto'
        GROUP BY em.pes_id";

        $result = $this->executeQueryWithParam($query, array('pes_id' => $pesId))->fetchAll();

        if ($result[0]) {
            return array(
                'multaQtd'   => $result[0]['multa_qtd'],
                'multaValor' => $result[0]['multa_valor']
            );
        }

        return array();
    }

    public function getJSONListingData($data)
    {
        $query
            = "
        SELECT
            ft.titulo_id, ft.tipotitulo_id,
            ft.pes_id, p.pes_nome,
            apb.login AS login_baixa, ft.usuario_baixa,
            apa.login AS login_autor, ft.usuario_autor,
            ft.titulo_data_processamento, ft.titulo_data_vencimento, ft.titulo_data_pagamento,
            ft.titulo_valor, ft.titulo_valor_pago, ft.titulo_observacoes,
            ft.titulo_estado,
            em.emprestimo_id            
        FROM biblioteca__multa m
            INNER JOIN biblioteca__emprestimo em ON em.emprestimo_id=m.emprestimo_id
            -- INNER JOIN biblioteca__exemplar ex ON ex.exemplar_id=em.exemplar_id
            -- INNER JOIN biblioteca__titulo bt ON bt.titulo_id=ex.titulo_id
            INNER JOIN financeiro__titulo ft ON m.titulo_id=ft.titulo_id
            INNER JOIN pessoa p ON p.pes_id=em.pes_id
            LEFT JOIN acesso_pessoas apa ON ft.usuario_autor=apa.id
            LEFT JOIN acesso_pessoas apb ON ft.usuario_baixa=apb.id
        ";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function criaMultaEmprestimo(
        \Biblioteca\Entity\Emprestimo $item,
        \Biblioteca\Entity\ModalidadeEmprestimoGrupo $objModalidadeEmprestimoGrupo,
        array $arrFeriados = array()
    ) {
        $arrDiasInclusosMulta = $objModalidadeEmprestimoGrupo->getModalidadeEmprestimoDiasNaoUteis();
        $arrDiasInclusosMulta = $arrDiasInclusosMulta ? explode(',', $arrDiasInclusosMulta) : array();
        $contabilizarSabado   = in_array('Sabado', $arrDiasInclusosMulta);
        $contabilizarDomingo  = in_array('Domingo', $arrDiasInclusosMulta);
        $contabilizarFeriado  = in_array('Feriado', $arrDiasInclusosMulta);

        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo($this->getEm());

        $dataAtual       = date('Ymd');
        $objDataPrevisao = $item->getEmprestimoDevolucaoDataPrevisao();

        //Verifica se é sábado, domingo ou feriado
        $dias = 0;
        $objDataPrevisao->add(new \DateInterval('P1D'));

        while ($objDataPrevisao->format('Ymd') <= $dataAtual) {
            $diaDaSemana = $objDataPrevisao->format('N');

            if ($diaDaSemana == 6) {
                if ($contabilizarSabado) {
                    $dias++;
                }
            } elseif ($diaDaSemana == 7) {
                if ($contabilizarDomingo) {
                    $dias++;
                }
            } elseif (in_array($objDataPrevisao->format('Y-m-d'), $arrFeriados)) {
                if ($contabilizarFeriado) {
                    $dias++;
                }
            } else {
                $dias++;
            }

            //Adiciona um dia a data
            $objDataPrevisao->add(new \DateInterval('P1D'));
        };

        if ($dias == 0) {
            return;
        }

        $multaValor = $dias * $objModalidadeEmprestimoGrupo->getModalidadeEmprestimoValorMulta(false);

        if ($multaValor == 0) {
            return;
        }

        $strDiasContabilizados = array();

        if ($contabilizarSabado) {
            $strDiasContabilizados[] = "Sábados";
        }

        if ($contabilizarDomingo) {
            $strDiasContabilizados[] = "Domingos";
        }

        if ($contabilizarFeriado) {
            $strDiasContabilizados[] = "Feriados";
        }

        $strDiasContabilizados = implode(', ', $strDiasContabilizados);

        if (!$strDiasContabilizados) {
            $strDiasContabilizados = "Apenas dias úteis";
        } else {
            $strDiasContabilizados = "dias úteis, " . $strDiasContabilizados;
        }

        $strDiasContabilizados = preg_replace('/^(.*), ([^,]+)$/i', '$1 e $2', $strDiasContabilizados);

        $multaObs = vsprintf(
            self::MULTA_OBSERVACAO,
            array(
                $item->getExemplar()->getExemplarCodigo(),
                $item->getExemplar()->getTitulo()->getTituloTitulo(),
                $dias,
                $objModalidadeEmprestimoGrupo->getModalidadeEmprestimoValorMulta(false),
                $strDiasContabilizados
            )
        );

        $multaObs = mb_strtoupper($multaObs);

        $item->setEmprestimoMultaValor($multaValor);

        $arrDadosTitulo = array(
            'pes'                     => $item->getPes()->getPes()->getPes()->getPesId(),
            'tipotitulo'              => \Boleto\Service\FinanceiroTituloTipo::MULTA_BIBLIOTECA,
            'tituloEstado'            => $serviceFinanceiroTitulo::TITULO_ESTADO_ABERTO,
            'tituloTipoPagamento'     => $serviceFinanceiroTitulo::TITULO_TIPO_PAGAMENTO_DINHEIRO,
            'tituloParcela'           => 1,
            'tituloValor'             => $multaValor,
            'tituloObservacoes'       => $multaObs,
            'tituloDataProcessamento' => date('Y-m-d H:i:s'),
            'tituloDataVencimento'    => date('Y-m-d H:i:s')
        );

        if (!$serviceFinanceiroTitulo->save($arrDadosTitulo)) {
            throw new \Exception($serviceFinanceiroTitulo->getLastError());
        } else {
            /** @var $objFinanceiroTitulo \Financeiro\Entity\FinanceiroTitulo */
            $objFinanceiroTitulo = $serviceFinanceiroTitulo->getRepository()->find($arrDadosTitulo['tituloId']);
        }

        $objFinanceiroTitulo->setAlunocurso(null)->setAlunoPer(null);

        /** @var $objMulta \Biblioteca\Entity\Multa */
        $objMulta = new \Biblioteca\Entity\Multa(array());
        $objMulta->setEmprestimo($item);
        $objMulta->setTitulo($objFinanceiroTitulo);

        $this->getEm()->persist($objMulta);
        $this->getEm()->flush($objFinanceiroTitulo);
        $this->getEm()->flush($objMulta);
    }

    protected function valida($dados)
    {
    }

    protected function pesquisaForJson($criterios)
    {
    }

    public function cancelaMulta($param)
    {
        if (!$param['tituloId']) {
            $this->setLastError('Para cancelar uma multa é necessário especificar o código da mesma.');

            return false;
        }

        if (!trim($param['motivo'])) {
            $this->setLastError('Para cancelar uma multa é necessário especificar um motivo.');

            return false;
        }

        $objServiceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo($this->getEm());

        $observacao = 'MOTIVO CANCELAMENTO: ' . $param['motivo'];

        try {
            $this->getEm()->beginTransaction();
            $objServiceFinanceiroTitulo->cancelaTitulo([$param['tituloId']], $observacao, true);
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao cancelar multa.');

            return false;
        }

        return true;
    }

    public function atualizaTituloMulta($objFinanceiroTituloAntigo, $objFinanceiroTituloNovo)
    {
        /** @var \Biblioteca\Entity\Multa $objMulta */
        $objMulta = $this
            ->getRepository()
            ->findOneBy(array('titulo' => $objFinanceiroTituloAntigo->getTituloId()));

        if ($objMulta) {
            $objMulta->setTitulo($objFinanceiroTituloNovo);
            $this->getEm()->persist($objMulta);
            $this->getEm()->flush($objMulta);
        }
    }

    public function baixaMulta($arrDados)
    {
        if (!$arrDados) {
            $this->setLastError("Informe um título para prosseguir");

            return false;
        }

        $serviceMeioPagamento    = new \Financeiro\Service\FinanceiroMeioPagamento($this->getEm());
        $servicePagamento        = new \Financeiro\Service\FinanceiroPagamento($this->getEm());
        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceAcessoPessoas    = new \Acesso\Service\AcessoPessoas($this->getEm());

        $objTitulo = $serviceFinanceiroTitulo->getRepository()->find($arrDados['tituloId']);

        if ($objTitulo) {
            $arrTitulo = $objTitulo->toArray();

            $arrTitulo['tituloValorPago']     = $arrTitulo['tituloValor'];
            $arrTitulo['tituloEstado']        = $serviceFinanceiroTitulo::TITULO_ESTADO_PAGO;
            $arrTitulo['tituloDataPagamento'] = new \DateTime();
            $arrTitulo['tituloDataBaixa']     = new \DateTime();
            $arrTitulo['usuarioBaixa']        = $serviceAcessoPessoas->retornaUsuarioLogado();

            $arrDados = array(
                'titulo'                  => $objTitulo,
                'meioPagamento'           => $serviceMeioPagamento->retornaMeioDePagamentoDinheiro(),
                'pagamentoUsuario'        => $arrTitulo['usuarioBaixa'],
                'pes'                     => $objTitulo->getPes(),
                'pagamentoValorBruto'     => $arrTitulo['tituloValor'],
                'pagamentoValorJuros'     => 0,
                'pagamentoValorMulta'     => $arrTitulo['tituloMulta'] ? $arrTitulo['tituloMulta'] : 0,
                'pagamentoAcrescimos'     => 0,
                'pagamentoValorDescontos' => 0,
                'pagamentoValorFinal'     => $arrTitulo['tituloValorPago'],
                'pagamentoDescontoManual' => 0,
                'pagamentoDataBaixa'      => $arrTitulo['tituloDataPagamento'],
                'pagamentoObservacoes'    => 0,
            );

            if (!$servicePagamento->save($arrDados)) {
                $this->setLastError($servicePagamento->getLastError());

                return false;
            }

            if (!$serviceFinanceiroTitulo->save($arrTitulo, false)) {
                $this->setLastError($serviceFinanceiroTitulo->getLastError());

                return false;
            }

            return true;
        }

        return false;
    }
}