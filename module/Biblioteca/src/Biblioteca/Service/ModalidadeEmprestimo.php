<?php

namespace Biblioteca\Service;

use VersaSpine\Service\AbstractService;

class ModalidadeEmprestimo extends AbstractService
{
    const SITUACAO_ATIVA   = 'Ativo';
    const SITUACAO_INATIVA = 'Inativo';
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Biblioteca\Entity\ModalidadeEmprestimo');
    }

    public function pesquisaForJson($data)
    {
        $query = "SELECT
                        e.modalidade_emprestimo_id,
                        e.modalidade_emprestimo_descricao,
                        e.modalidade_emprestimo_dias_suspensao,
                        e.modalidade_emprestimo_situacao,
                        g.grupo_leitor_id,
                        g.grupo_leitor_nome,
                        g.acesso_grupo_id
                    FROM biblioteca__modalidade_emprestimo e
                         INNER JOIN biblioteca__grupo_leitor g
                              ON e.grupo_leitor_id = g.grupo_leitor_id";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function save(array &$arrDados)
    {
        if ($this->valida($arrDados)) {
            $objEntityManager = $this->getEm();
            $objRepository    = $this->getRepository();

            $objModalidadeEmprestimoGrupo = new \Biblioteca\Service\ModalidadeEmprestimoGrupo($objEntityManager);

            try {
                $this->begin();

                if ($arrDados['modalidadeEmprestimoId']) {
                    $objModalidadeEmprestimo = $objRepository->find($arrDados['modalidadeEmprestimoId']);

                    if (!$objModalidadeEmprestimo) {
                        $this->setLastError('Modalidade não existe!');

                        return false;
                    }
                } else {
                    $objModalidadeEmprestimo = new \Biblioteca\Entity\ModalidadeEmprestimo();
                }

                $objGrupoLeitor = new \Biblioteca\Service\GrupoLeitor($objEntityManager);
                $objGrupoLeitor = $objGrupoLeitor->getRepository()->find($arrDados['grupoLeitor']);

                if (!$objGrupoLeitor) {
                    $this->setLastError('Grupo de leitor não existe!');

                    return false;
                }

                $objModalidadeEmprestimo
                    ->setModalidadeEmprestimoDescricao($arrDados['modalidadeEmprestimoDescricao'])
                    ->setModalidadeEmprestimoDiasSuspensao($arrDados['modalidadeEmprestimoDiasSuspensao'])
                    ->setModalidadeEmprestimoSituacao($arrDados['modalidadeEmprestimoSituacao'])
                    ->setGrupoLeitor($objGrupoLeitor);

                $objEntityManager->persist($objModalidadeEmprestimo);
                $objEntityManager->flush();

                $objModalidadeEmprestimoGrupo->salvarArray(
                    $arrDados['modalidadeEmprestimoGrupo'],
                    $objModalidadeEmprestimo
                );

                $this->commit();

                return true;
            } catch (\Exception $e) {
                $this->setLastError('Não foi possível salvar modalidade de empréstimo!');
            }
        }

        return false;
    }

    public function valida($arrParam)
    {
        $objServiceModalidadeEmprestimoGrupo = new \Biblioteca\Service\ModalidadeEmprestimoGrupo($this->getEm());

        $errors = array();

        $arrRequerido = array(
            "modalidadeEmprestimoDescricao" => "Descrição",
            //"modalidadeEmprestimoSituacao"  => "Situação",
            "grupoLeitor"                   => "Grupo de Leitor",
        );

        foreach ($arrRequerido as $req => $nome) {
            if (!$arrParam[$req]) {
                $errors[] = "Por favor preencha o campo \"$nome\"!";
            }
        }

        if ($this->verificaSeJaExiste(
            $arrParam['modalidadeEmprestimoDescricao'],
            $arrParam['modalidadeEmprestimoId']
        )
        ) {
            $errors[] = "Já existe uma modalidade com esta descrição!";
        }

        if (empty($arrParam['modalidadeEmprestimoGrupo'])) {
            $errors[] = "Insira ao menos uma regra para a modalidade!";
        }

        if ($objServiceModalidadeEmprestimoGrupo->valida($arrParam['modalidadeEmprestimoGrupo'])) {
            $errors[] = 'Regras:' . $objServiceModalidadeEmprestimoGrupo->getLastError();
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function verificaSeJaExiste($modalidadeEmprestimoDescricao, $modalidadeEmprestimoId = false)
    {
        $sql        = 'SELECT count(*) as qtd FROM biblioteca__modalidade_emprestimo WHERE modalidade_emprestimo_descricao like :modalidadeEmprestimoDescricao';
        $parameters = array('modalidadeEmprestimoDescricao' => $modalidadeEmprestimoDescricao);

        if ($modalidadeEmprestimoId) {
            $sql .= ' AND modalidade_emprestimo_id <> :modalidadeEmprestimoId';
            $parameters['modalidadeEmprestimoId'] = $modalidadeEmprestimoId;
        }

        $result = $this->executeQueryWithParam($sql, $parameters)->fetch();

        return $result['qtd'] > 0;
    }

    public function getArray($modalidadeEmprestimoId)
    {
        $arrData = $this->getReference($modalidadeEmprestimoId);

        $objServiceModalidadeEmprestimoGrupo = new \Biblioteca\Service\ModalidadeEmprestimoGrupo($this->getEm());

        try {
            $arrData = $arrData->toArray();

            $arrData['modalidadeEmprestimoGrupo'] = $objServiceModalidadeEmprestimoGrupo->retornaArrayPelaModalidadeEmprestimo(
                $arrData['modalidadeEmprestimoId']
            );
        } catch (\Exception $e) {
            return array();
        }

        return $arrData;
    }

    public function getArrayByPost(&$arrParam)
    {
        $objServiceGrupoBibliografico    = new \Biblioteca\Service\GrupoBibliografico($this->getEm());
        $objRepositoryGrupoBibliografico = $objServiceGrupoBibliografico->getRepository();

        $arrLegenda = array('Sabado' => 'Sábado', 'Domingo' => 'Domingo', 'Feriado' => 'Feriado');

        foreach ($arrParam['modalidadeEmprestimoGrupo'] as $pos => $arrItem) {
            $objGrupoBibliografico = $objRepositoryGrupoBibliografico->find(
                $arrItem['grupoBibliografico']
            );

            $arrItem['grupoBibliografico'] = array(
                'id'   => $objGrupoBibliografico->getGrupoBibliograficoId(),
                'text' => $objGrupoBibliografico->getGrupoBibliograficoNome()
            );

            $arrModalidadeEmprestimoDiasNaoUteis = explode(',', $arrItem['modalidadeEmprestimoDiasNaoUteis']);

            foreach ($arrModalidadeEmprestimoDiasNaoUteis as $posDiaNaoUtil => $diaNaoUtil) {
                $arrModalidadeEmprestimoDiasNaoUteis[$posDiaNaoUtil] = array(
                    'id'   => $diaNaoUtil,
                    'text' => $arrLegenda[$diaNaoUtil]
                );
            }

            $arrItem['modalidadeEmprestimoDiasNaoUteis'] = $arrModalidadeEmprestimoDiasNaoUteis;

            $arrParam['modalidadeEmprestimoGrupo'][$pos] = $arrItem;
        }
    }
}