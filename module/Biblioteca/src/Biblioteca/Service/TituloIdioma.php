<?php

namespace Biblioteca\Service;

use VersaSpine\Service\AbstractService;

class TituloIdioma extends AbstractService
{
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Biblioteca\Entity\TituloIdioma');
    }

    public function pesquisaForJson($a)
    {
        return array();
    }

    public function pesquisaPeloTitulo($tituloId, $toArray = true)
    {
        $objTituloIdioma = $this->getRepository();
        $arrResult       = $objTituloIdioma->findBy(array('titulo' => $tituloId));

        if ($toArray) {
            $arrRetorno = array();

            foreach ($arrResult as $objTituloIdioma) {
                $arrRetorno[] = $objTituloIdioma->toArray();
            }

            return $arrRetorno;
        }

        return $arrResult;
    }

    public function retornaIdiomaIdPeloTitulo($tituloId)
    {
        $arrIdiomas         = array();
        $arrObjTituloIdioma = $this->pesquisaPeloTitulo($tituloId, false);

        foreach ($arrObjTituloIdioma as $objTituloIdioma) {
            $arrIdiomas[] = $objTituloIdioma->getIdioma()->getIdiomaId();
        }

        return $arrIdiomas;
    }

    public function salvarArray($arrIdiomasTitulo, \Biblioteca\Entity\Titulo $objTitulo)
    {
        $objEntityManager = $this->getEm();
        $arrExcluir       = array();
        $arrEditar        = array();

        $arrTituloIdiomasDB = $this->pesquisaPeloTitulo($objTitulo->getTituloId(), false);

        foreach ($arrTituloIdiomasDB as $arrTituloIdiomaDB) {
            $encontrado     = false;
            $tituloIdiomaId = $arrTituloIdiomaDB->getTituloIdiomaId();

            foreach ($arrIdiomasTitulo as $strTituloIdioma) {
                if ($strTituloIdioma['tituloIdiomaId'] == $tituloIdiomaId) {
                    $encontrado                 = true;
                    $arrEditar[$tituloIdiomaId] = $arrTituloIdiomaDB;
                    break;
                }
            }

            if (!$encontrado) {
                $arrExcluir[$tituloIdiomaId] = $arrTituloIdiomaDB;
            }
        }

        if ($arrExcluir) {
            foreach ($arrExcluir as $objReg) {
                $objEntityManager->remove($objReg);
                $objEntityManager->flush();
            }
        }

        foreach ($arrIdiomasTitulo as $strTituloIdioma) {
            $objServiceIdioma = new \Biblioteca\Service\Idioma($objEntityManager);
            $objIdioma        = $objServiceIdioma->getRepository()->find($strTituloIdioma);

            if (!isset($arrEditar[$strTituloIdioma['tituloIdiomaId']])) {
                $objTituloIdioma = new \Biblioteca\Entity\TituloIdioma();
            } else {
                $objTituloIdioma = $arrEditar[$strTituloIdioma['tituloIdiomaId']];
            }

            $objTituloIdioma->setTitulo($objTitulo);
            $objTituloIdioma->setIdioma($objIdioma);

            $objEntityManager->persist($objTituloIdioma);
            $objEntityManager->flush();
        }

        return true;
    }

    public function retornaArrayPeloTitulo($TituloId)
    {
        $arrObjTituloIdioma = $this->pesquisaPeloTitulo($TituloId, false);
        $arrTituloIdioma    = array();

        foreach ($arrObjTituloIdioma as $objTituloIdioma) {
            $arrItem = $objTituloIdioma->toArray();
            $arrItem = array(
                'tituloIdiomaId' => $arrItem['tituloIdiomaId'],
                'titulo'         => $arrItem['titulo']['tituloId'],
                'idioma'         => $arrItem['idioma']['idiomaId'],
                'id'             => $arrItem['idioma']['idiomaId'],
                'text'           => $arrItem['idioma']['idiomaDescricao']
            );

            $arrTituloIdioma[] = $arrItem;
        }

        return $arrTituloIdioma;
    }

    protected function valida($dados)
    {
    }
}