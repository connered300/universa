<?php

namespace Biblioteca\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class Emprestimo
 * @package Biblioteca\Service
 */
class Emprestimo extends AbstractService
{
    /**
     * Armazena o último erro registrado
     * @var null
     */
    private $lastError = null;

    /** @var \Sistema\Service\SisConfig */
    private $config = null;

    /**
     * Retorna o último erro registrado
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * Altera o último erro registrado
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * @return \Sistema\Service\SisConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param \Sistema\Service\SisConfig $config
     * @return Emprestimo
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     * @param array                       $arrConfig
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, $arrConfig = array())
    {
        parent::__construct($em, 'Biblioteca\Entity\Emprestimo');
        $this->setConfig(new \Sistema\Service\SisConfig($em, $arrConfig));
    }

    /**
     * @param $dados
     */
    protected function valida($dados)
    {
    }

    /**
     * Devolve um empréstimo de um leitor
     *
     * <p>
     * Devolve um empréstimo de um leitor.<br>
     * Caso o parâmetro $cancelar seja true, o empréstimo é apenas cancelado,
     * ou seja, não contabiliza multa nem suspensão.
     * </p>
     *
     * @param \Biblioteca\Entity\Emprestimo $item
     * @param bool|false                    $cancelar
     * @return bool
     */
    private function devolverItem(\Biblioteca\Entity\Emprestimo $item, $cancelar = false)
    {
        $this->getEm()->beginTransaction();

        try {
            if ($item->getEmprestimoCancelado() == 'S') {
                $this->setLastError('Este empréstimo já foi cancelado');

                return false;
            }

            if ($item->getEmprestimoDevolucaoDataEfetuada()) {
                $this->setLastError('Este empréstimo já foi devolvido');

                return false;
            }

            if ($cancelar) {
                $item->setEmprestimoCancelado('S');
            } else {
                $dataAtual    = date('Ymd');
                $dataPrevisao = $item->getEmprestimoDevolucaoDataPrevisao()->format('Ymd');

                $objServiceCalendarioData = new \Professor\Service\AcadperiodoCalendarioData($this->getEm());
                $arrFeriados              = $objServiceCalendarioData->feriadosDepoisDaData(
                    $item->getEmprestimoDevolucaoDataPrevisao()->format('Y-m-d')
                );

                if ($dataAtual > $dataPrevisao) {
                    $objServiceMulta                     = new \Biblioteca\Service\Multa($this->getEm());
                    $objServiceModalidadeEmprestimoGrupo = new \Biblioteca\Service\ModalidadeEmprestimoGrupo(
                        $this->getEm()
                    );
                    $objServiceSuspensao                 = new \Biblioteca\Service\Suspensao($this->getEm());

                    /* @var $objModalidadeEmprestimoGrupo \Biblioteca\Entity\ModalidadeEmprestimoGrupo */
                    $objModalidadeEmprestimoGrupo = $objServiceModalidadeEmprestimoGrupo
                        ->getRepository()
                        ->findOneBy(
                            array(
                                'modalidadeEmprestimo'              => (
                                $item->getModalidadeEmprestimo()->getModalidadeEmprestimoId()
                                ),
                                'grupoBibliografico'                => (
                                $item->getExemplar()->getTitulo()->getGrupoBibliografico()->getGrupoBibliograficoId()
                                ),
                                'modalidadeEmprestimoGrupoSituacao' => \Biblioteca\Service\ModalidadeEmprestimoGrupo::SITUACAO_ATIVA
                            )
                        );

                    if (!$objModalidadeEmprestimoGrupo) {
                        $this->setLastError(
                            'A modalidade de empréstimo não possui regra para ' .
                            '"' .
                            $item->getExemplar()->getTitulo()->getGrupoBibliografico()->getGrupoBibliograficoNome() .
                            '"'
                        );

                        return false;
                    }

                    if ($item->getExemplar()->getExemplarAcesso() != 'Kit Saraiva') {
                        $objServiceMulta->criaMultaEmprestimo($item, $objModalidadeEmprestimoGrupo, $arrFeriados);
                        $objServiceSuspensao->criarSuspensaoLeitor($item);
                    }
                }

                $objServiceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());
                $session                 = new \Zend\Authentication\Storage\Session();
                $arrUsuario              = $session->read();

                /* @var $objAcessoPessoas \Acesso\Entity\AcessoPessoas */
                $objAcessoPessoas = $objServiceAcessoPessoas
                    ->getRepository()
                    ->find($arrUsuario['id']);

                $item->setEmprestimoDevolucaoDataEfetuada(new \DateTime());
                $item->setUsuarioDevolucao($objAcessoPessoas);
            }

            $objExemplar = $item->getExemplar();
            $objExemplar->setExemplarEmprestado('N');

            $this->getEm()->persist($objExemplar);
            $this->getEm()->persist($item);
            $this->getEm()->flush($objExemplar);
            $this->getEm()->flush($item);

            $this->getEm()->commit();

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * Devolve ou cancela vários empréstimos pelo emprestimo_id.
     *
     * <p>
     * Devolve ou cancela vários empréstimos pelo emprestimo_id.<br>
     * Caso o parâmetro $cancelar seja true, o empréstimo é apenas cancelado,
     * ou seja, não contabiliza multa nem suspensão.<br>
     * O parâmetro $param deve possuir um item chamado <b>emprestimoId</b>,
     * este item será um array de identificadores de empréstimos <b>ativos<b>.
     * </p>
     *
     * @param            $param
     * @param bool|false $cancelar
     * @return bool
     */
    public function devolveEmprestimo($param, $cancelar = false)
    {
        $acao = $cancelar ? 'cancelar' : 'devolver';

        if (!$param['emprestimoId']) {
            $this->setLastError('Para ' . $acao . ' um exemplar é necessário especificar ao menos um empréstimo.');

            return false;
        }

        $arrEmprestimos = explode(',', $param['emprestimoId']);
        $erroDevolucao  = array();

        foreach ($arrEmprestimos as $emprestimoId) {
            /* @var $objEmprestimo \Biblioteca\Entity\Emprestimo */
            $objEmprestimo = $this->getRepository()->find($emprestimoId);

            if (!$this->devolverItem($objEmprestimo, $cancelar)) {
                $erroDevolucao[] = $objEmprestimo->getExemplar()->getTitulo()->getTituloTitulo();
            }
        }

        if ($erroDevolucao) {
            $this->setLastError('Não foi possível ' . $acao . ' os itens:<br>' . implode('<br>', $erroDevolucao));

            return false;
        }

        return true;
    }

    /**
     * Cancela vários empréstimos pelo emprestimo_id
     *
     * <p>
     * Cancela vários empréstimos pelo emprestimo_id.<br>
     * O parâmetro $param deve um item chamado <b>emprestimoId</b>,
     * este item será um array de identificadores de empréstimos <b>ativos<b>.
     * </p>
     *
     * @param $param
     * @return bool
     */
    public function cancelaEmprestimo($param)
    {
        return $this->devolveEmprestimo($param, true);
    }

    /**
     * Efetua emprestimo de exemplares de livros
     *
     * <p>
     * Efetua emprestimo de exemplares de livros.<br>
     * O parâmetro $param deve possuir a seguinte estrutura:
     * <ul>
     * <li><b>pesId</b>: Identificador da pessoa do leitor.</li>
     * <li><b>exemplarId</b>: Array de identificadores de exemplar exemplares.</li>
     * <li><b>modalidadeEmprestimoId</b>: Identificador da modalidade de empréstimo.</li>
     * <li><b>isencaoSenha</b>(opcional): Informa se deve desconsiderar a validação de senha.</li>
     * <li><b>senha</b>(opcional): Confirmação de senha do leitor. Parâmetro obrigatório caso isencaoSenha seja igual a um valor falso.</li>
     * </ul>
     * </p>
     *
     * @param array $param
     * @return bool
     */
    public function emprestaExemplar($param)
    {
        $serviceGrupoBibliografico = new \Biblioteca\Service\GrupoBibliografico($this->getEm());

        if (!$param['pesId']) {
            $this->setLastError('Para efetuar empréstimos é necessário especificar o leitor.');

            return false;
        }
        if (!$param['exemplarId']) {
            $this->setLastError('Para efetuar empréstimos é necessário especificar ao menos um exemplar.');

            return false;
        }

        if (!$param['modalidadeEmprestimoId']) {
            $this->setLastError('Para efetuar empréstimos é necessário especificar uma modalidade de empréstimo.');

            return false;
        }

        $pesId                  = $param['pesId'];
        $isencaoSenha           = $param['isencaoSenha'];
        $modalidadeEmprestimoId = $param['modalidadeEmprestimoId'];
        $senha                  = sha1($param['senha']);
        $arrExemplares          = explode(',', $param['exemplarId']);
        $erroEmprestimo         = array();

        $objServicePessoa = new \Biblioteca\Service\Pessoa($this->getEm());
        /* @var $objPessoa \Biblioteca\Entity\Pessoa */
        $objPessoa = $objServicePessoa->getRepository()->find($pesId);

        if (!$objPessoa) {
            $this->setLastError('Leitor não encontrado no banco de dados!');

            return false;
        }
        if (!$isencaoSenha) {
            if (!$param['senha']) {
                $this->setLastError('Para efetuar empréstimos é necessário que o leitor informe sua senha.');

                return false;
            }

            $objServiceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());
            $objAcessoPessoas        = $objServiceAcessoPessoas->getRepository()->findOneBy(
                array('pesFisica' => $pesId),
                array('login' => 'asc')
            );

            if ($objAcessoPessoas) {
                if ($objAcessoPessoas->getSenha() != $senha) {
                    $this->setLastError('A senha do usuário do leitor não confere com a senha digitada.');

                    return false;
                }
            } elseif ($objPessoa->getPessoaSenha() != "" && $objPessoa->getPessoaSenha() != $senha) {
                $this->setLastError('A senha do usuário não confere com a senha cadastrada na biblioteca.');

                return false;
            } elseif ($objPessoa->getPessoaSenha() == "") {
                $this->setLastError('o leitor não possui senha cadastrada nem usuário no sistema.');

                return false;
            }
        }

        $session    = new \Zend\Authentication\Storage\Session();
        $arrUsuario = $session->read();

        if (!$arrUsuario) {
            $this->setLastError('Verifique se você está autenticado no sistema!');

            return false;
        }

        $objServiceCalendarioData = new \Professor\Service\AcadperiodoCalendarioData($this->getEm());
        $arrFeriados              = $objServiceCalendarioData->feriadosDepoisDaData(date('Y-m-d'));

        $usuarioId               = $arrUsuario['id'];
        $objServiceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());
        /* @var $objUsuario \Acesso\Entity\AcessoPessoas */
        $objUsuario = $objServiceAcessoPessoas->getRepository()->find($usuarioId);

        if (!$objUsuario) {
            $this->setLastError('Verifique se você está autenticado no sistema!');

            return false;
        }

        $objServiceModalidadeEmprestimo = new \Biblioteca\Service\ModalidadeEmprestimo($this->getEm());
        /* @var $objModalidadeEmprestimo \Biblioteca\Entity\ModalidadeEmprestimo */
        $objModalidadeEmprestimo = $objServiceModalidadeEmprestimo->getRepository()->find(
            $modalidadeEmprestimoId
        );

        if (!$objModalidadeEmprestimo) {
            $this->setLastError('Modalidade de empréstimo não encontrada no banco de dados!');

            return false;
        }

        $objServiceMulta = new \Biblioteca\Service\Multa($this->getEm());
        $leitorMultaInfo = $objServiceMulta->informacaoMultasAbertas($pesId);

        if ($leitorMultaInfo) {
            $mensagem = (
                'O leitor possui multa(s) em aberto!<br>' .
                'Procure tesouraria ou a direção da biblioteca.'
            );
            $this->setLastError($mensagem);

            return false;
        }

        $objServiceSuspensao = new \Biblioteca\Service\Suspensao($this->getEm());
        $leitorSuspensaoInfo = $objServiceSuspensao->informacaoSuspensaoAberta($pesId);

        if ($leitorSuspensaoInfo) {
            $suspensaoInicio = $this->formatDateBrasileiro($leitorSuspensaoInfo['suspensaoInicio']);
            $suspensaoFim    = $this->formatDateBrasileiro($leitorSuspensaoInfo['suspensaoFim']);
            $mensagem        = (
                'O leitor está suspenso de ' . $suspensaoInicio . ' à ' . $suspensaoFim . '!<br>' .
                'Procure direção da biblioteca.'
            );
            $this->setLastError($mensagem);

            return false;
        }

        $objServiceExemplar = new \Biblioteca\Service\Exemplar($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            foreach ($arrExemplares as $exemplarId) {
                /* @var $objExemplar \Biblioteca\Entity\Exemplar */
                $objExemplar   = $objServiceExemplar
                    ->getRepository()->find($exemplarId);
                $emprestouItem = $this
                    ->emprestarItem($objExemplar, $objModalidadeEmprestimo, $objPessoa, $objUsuario, $arrFeriados);

                if (!$emprestouItem) {
                    $erroExemplar     = $this->getLastError();
                    $erroEmprestimo[] = '' .
                        '<b>' .
                        $objExemplar->getTitulo()->getTituloTitulo() .
                        '</b><br>' .
                        '(' . $erroExemplar . ')';
                } else {
                    $emprestimos[] = $emprestouItem->getEmprestimoId();
                }
            }

            if ($erroEmprestimo) {
                $this->setLastError(
                    'Não foi possível emprestar os itens:<ul><li>' .
                    implode('</li><li>', $erroEmprestimo) .
                    '</li></ul><br>' .
                    'Remova-os e tente novamente.'
                );

                return false;
            }
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError($ex->getMessage());

            return false;
        }

        return $emprestimos;
    }

    /**
     * Empresta um exemplar em uma modalidade para um leitor
     *
     * @param \Biblioteca\Entity\Exemplar             $objExemplar
     * @param \Biblioteca\Entity\ModalidadeEmprestimo $objModalidadeEmprestimo
     * @param \Biblioteca\Entity\Pessoa               $objPessoa
     * @param \Acesso\Entity\AcessoPessoas            $objUsuario
     * @param array                                   $arrFeriados Usado para estender a data de entrega de um empréstimo
     * @return bool
     */
    private function emprestarItem(
        \Biblioteca\Entity\Exemplar $objExemplar,
        \Biblioteca\Entity\ModalidadeEmprestimo $objModalidadeEmprestimo,
        \Biblioteca\Entity\Pessoa $objPessoa,
        \Acesso\Entity\AcessoPessoas $objUsuario,
        array $arrFeriados = array()
    ) {
        try {
            if ($objExemplar->getExemplarBaixaData()) {
                $this->setLastError('Este exemplar foi baixado.');

                return false;
            } elseif ($objExemplar->getExemplarAcesso() == 'Restrito') {
                $this->setLastError('O acesso à este exemplar é restrito');

                return false;
            } elseif ($objExemplar->getExemplarEmprestado() != 'N') {
                $this->setLastError('Este exemplar encontra-se emprestado');

                return false;
            } elseif ($objExemplar->getExemplarReservado() != 'N') {
                $this->setLastError('Este exemplar encontra-se reservado');

                return false;
            }

            $grupoBibliograficoId = $objExemplar->getTitulo()->getGrupoBibliografico()->getGrupoBibliograficoId();
            $pesId                = $objPessoa->getPes()->getPes()->getPesId();

            $objServiceModalidadeEmprestimoGrupo = new \Biblioteca\Service\ModalidadeEmprestimoGrupo($this->getEm());

            $objModalidadeEmprestimoGrupo = $objServiceModalidadeEmprestimoGrupo->getRepository()->findOneBy(
                array(
                    'modalidadeEmprestimo'              =>
                        $objModalidadeEmprestimo->getModalidadeEmprestimoId(),
                    'grupoBibliografico'                => $grupoBibliograficoId,
                    'modalidadeEmprestimoGrupoSituacao' =>
                        \Biblioteca\Service\ModalidadeEmprestimoGrupo::SITUACAO_ATIVA
                )
            );

            if (!$objModalidadeEmprestimoGrupo) {
                $this->setLastError('Modalidade de empréstimo não encontrada no banco de dados');

                return false;
            }

            $NumeroVolumesPermitidos = $objModalidadeEmprestimoGrupo->getModalidadeEmprestimoNumeroVolumes();
            $arrNumItensAtivosGrupos = $this->leitorNumeroItensEmprestadosAtivoPorGrupo($pesId);

            if (
                $arrNumItensAtivosGrupos[$grupoBibliograficoId]['grupoBibliograficoQtd'] >= $NumeroVolumesPermitidos
                &&
                $objExemplar->getExemplarAcesso() != 'Kit Saraiva'
            ) {
                $this->setLastError(
                    'Este leitor já atingiu o número máximo de títulos para este grupo nesta modalidade de empréstimo'
                );

                return false;
            }

            $arrLeitorTitulosExemplaresEmPoder = $this->leitorTitulosExemplaresEmPoder($pesId);

            if ($arrLeitorTitulosExemplaresEmPoder[$objExemplar->getTitulo()->getTituloId()]) {
                $this->setLastError('Este leitor já está com um exemplar deste título');

                return false;
            }

            $objEmprestimo = new \Biblioteca\Entity\Emprestimo();

            $objDataEmprestimo     = new \DateTime();
            $emprestimoPrazoMaximo = $objModalidadeEmprestimoGrupo->getModalidadeEmprestimoPrazoMaximo();

            //Verifica se é sábado, domingo ou feriado
            do {
                $objDiasPrevistos           = new \DateInterval('P' . $emprestimoPrazoMaximo . 'D');
                $objDataEmprestimoDevolucao = (new \DateTime())->add($objDiasPrevistos);
                $emprestimoPrazoMaximo += 1;
            } while (
                $objDataEmprestimoDevolucao->format('N') >= 6 ||
                in_array($objDataEmprestimoDevolucao->format('Y-m-d'), $arrFeriados)
            );

            $objEmprestimo
                ->setPes($objPessoa)
                ->setModalidadeEmprestimo($objModalidadeEmprestimo)
                ->setEmprestimoData($objDataEmprestimo)
                ->setEmprestimoDevolucaoDataPrevisao($objDataEmprestimoDevolucao)
                ->setExemplar($objExemplar)
                ->setUsuario($objUsuario);

            $objExemplar->setExemplarEmprestado('S');

            $this->getEm()->persist($objExemplar);
            $this->getEm()->persist($objEmprestimo);
            $this->getEm()->flush($objExemplar);
            $this->getEm()->flush($objEmprestimo);

            return $objEmprestimo;
        } catch (\Exception $ex) {
            $this->setLastError($ex->getMessage());

            return false;
        }
    }

    /**
     * Retorna dados de empréstimos para uso no plugin datatables.net
     *
     * <p>
     * Retorna dados de empréstimos para uso no plugin datatables.net<br>
     * O parâmetro $data deve possuir a seguinte estrutura:
     * <ul>
     * <li><b>leitorId</b>(opcional): Identificador da pessoa do leitor(tabela biblioteca__pessoa).</li>
     * <li><b>devolucao</b>(opcional): define que serão listados apenas empréstimos que ainda não foram desenvolvidos.</li>
     * <li><b>esconderKitSaraiva</b>(opcional): define que não serão exibidos emprestimos cujos exemplares possuam acesso do tipo "kit Saraiva".</li>
     * </ul>
     * </p>
     *
     * @param $data
     * @return array
     */
    public function pesquisaForJson($data)
    {
        $quantidadeRenovacaoConsecutivas = (int)$this->getConfig()->localizarChave("BIBLIOTECA_RENOVACAO_CONSECUTIVAS");

        $query = "
            SELECT
            (
                SELECT SUM(IF(biblioteca__exemplar.titulo_id = bx.titulo_id, 1, 0))
                FROM biblioteca__emprestimo
                INNER JOIN biblioteca__exemplar USING (exemplar_id)
                WHERE biblioteca__emprestimo.pes_id=be.pes_id
                ORDER BY emprestimo_data DESC
                LIMIT 0, $quantidadeRenovacaoConsecutivas
            ) AS quantidadeDeEmprestimosConcedidos,
            ap.id AS usuario_id,
            ap.login AS usuario_login,
            apd.id AS usuario_id_devolucao,
            apd.login AS usuario_login_devolucao,
            be.pes_id AS leitor_id,
            pf.pes_nome AS leitor_nome,
            be.emprestimo_id,
            be.emprestimo_data,
            be.emprestimo_devolucao_data_previsao,
            be.emprestimo_devolucao_data_efetuada,
            if(
                isnull(be.emprestimo_devolucao_data_efetuada),
                if((DATE_FORMAT(now(),'%Y%m%d')) > DATE_FORMAT(be.emprestimo_devolucao_data_previsao,'%Y%m%d'),'S','N'),
                if(be.emprestimo_devolucao_data_efetuada > be.emprestimo_devolucao_data_previsao,'S','N')
            ) AS emprestimo_atraso,
            be.emprestimo_multa_observacao,
            bx.exemplar_id,
            bx.exemplar_codigo,
            bx.exemplar_reservado,
            bx.exemplar_acesso
            , bme.modalidade_emprestimo_id, bme.modalidade_emprestimo_descricao
            , gb.grupo_bibliografico_id, gb.grupo_bibliografico_nome
            , t.titulo_id, t.titulo_titulo, t.titulo_edicao_volume, t.titulo_edicao_isbn
            , t.titulo_edicao_cdu, t.titulo_edicao_cutter, t.titulo_edicao_ano
            , (
                SELECT group_concat(DISTINCT a.autor_referencia SEPARATOR '; ')
                FROM biblioteca__titulo_autor tau
                  INNER JOIN biblioteca__autor a ON a.autor_id=tau.autor_id
                WHERE tau.titulo_id=t.titulo_id
            ) AS autor_referencia
            -- ,t.pes_id_editora, pj.pes_nome_fantasia,
            -- ,ba.area_id, ba.area_descricao,
            -- ,bac.area_cnpq_id, bac.area_cnpq_codigo, bac.area_cnpq_descricao
        FROM
            biblioteca__emprestimo be
            -- dados do usuário e leitor
            INNER JOIN pessoa pf ON pf.pes_id=be.pes_id
            LEFT JOIN acesso_pessoas ap ON ap.id =be.usuario_id
            LEFT JOIN acesso_pessoas apd ON apd.id =be.usuario_id_devolucao
            -- dados do título/exemplar
            INNER JOIN biblioteca__exemplar bx ON bx.exemplar_id=be.exemplar_id
            INNER JOIN biblioteca__titulo t ON bx.titulo_id=t.titulo_id
            INNER JOIN biblioteca__grupo_bibliografico gb ON gb.grupo_bibliografico_id=t.grupo_bibliografico_id
            INNER JOIN biblioteca__modalidade_emprestimo bme ON bme.modalidade_emprestimo_id=be.modalidade_emprestimo_id
            -- -- editora
            -- LEFT JOIN biblioteca__empresa e ON e.pes_id=t.pes_id_editora
            -- LEFT JOIN pessoa_juridica pj ON pj.pes_id=e.pes_id
            -- -- area do conhecimento principal
            -- LEFT JOIN biblioteca__area ba ON ba.area_id=t.area_id
            -- -- area de conhecimento CNPq
            -- LEFT JOIN biblioteca__area_cnpq bac ON bac.area_cnpq_id=t.area_cnpq_id
            -- -- assuntos relacionados ao titulo
            -- LEFT JOIN biblioteca__titulo_assunto tass ON tass.titulo_id=t.titulo_id
            -- LEFT JOIN biblioteca__assunto ass ON ass.assunto_id=tass.assunto_id
            -- -- idiomas do titulo
            -- LEFT JOIN biblioteca__titulo_idioma tid ON tid.titulo_id=t.titulo_id
            -- LEFT JOIN biblioteca__idioma i ON i.idioma_id=tid.idioma_id
        WHERE
            be.emprestimo_cancelado='N' __CONDICAO_EXTRA__
        GROUP BY be.emprestimo_id
        ORDER BY be.emprestimo_data DESC";

        $condicaoExtra = '';

        if ($data["leitorId"]) {
            $condicaoExtra .= ' AND be.pes_id=' . ((int)$data["leitorId"]);
        }

        if ($data["devolucao"]) {
            $condicaoExtra .= ' AND isnull(be.emprestimo_devolucao_data_efetuada)';
        }

        if ($data["esconderKitSaraiva"]) {
            $condicaoExtra .= ' AND bx.exemplar_acesso<>"kit Saraiva"';
        }

        $query = str_replace('__CONDICAO_EXTRA__', $condicaoExtra, $query);
        $query = str_replace('__DATA_ATUAL__', date('Y-m-d H:i:s'), $query);

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * Retorna o número de empréstimos que o leitor já fez.
     * Obs.: são desconsiderados empréstimos cancelados.
     *
     * @param int $pesId Identificador da tabela biblioteca__pessoa
     * @return int
     */
    public function leitorNumeroDeEmprestimos($pesId)
    {
        $sql        = "
        SELECT count(*) AS qtd
        FROM biblioteca__emprestimo e
        WHERE e.pes_id=:pesId AND emprestimo_cancelado='N'";
        $parameters = array('pesId' => $pesId);

        $result = $this->executeQueryWithParam($sql, $parameters)->fetch();

        return (int)$result['qtd'];
    }

    /**
     * Retorna o número de empréstimos que o leitor possui em atraso ou foram entregues com atraso.
     * Obs.: são desconsiderados empréstimos cancelados e exemplares cujo o acesso seja "Kit Saraiva".
     *
     * @param int $pesId Identificador da tabela biblioteca__pessoa
     * @return int
     */
    public function leitorNumeroDeEmprestimosAtraso($pesId)
    {
        $sql        = "
        SELECT count(*) AS qtd
        FROM biblioteca__emprestimo e
        INNER JOIN biblioteca__exemplar ex ON ex.exemplar_id=e.exemplar_id AND ex.exemplar_acesso<>'Kit Saraiva'
        WHERE pes_id=:pesId AND emprestimo_cancelado='N' AND
        (
            emprestimo_devolucao_data_efetuada > emprestimo_devolucao_data_previsao OR
            (emprestimo_devolucao_data_efetuada IS NULL AND :dataAtual > emprestimo_devolucao_data_previsao)
        )
        ";
        $parameters = array('pesId' => $pesId, 'dataAtual' => date('Y-m-d H:i:s'));

        $result = $this->executeQueryWithParam($sql, $parameters)->fetch();

        return (int)$result['qtd'];
    }

    /**
     * Retorna o número de empréstimos que o leitor possui em atraso e que ainda não foram devolvidos.
     * Obs.: são desconsiderados empréstimos cancelados e exemplares cujo o acesso seja "Kit Saraiva".
     *
     * @param int $pesId Identificador da tabela biblioteca__pessoa
     * @return int
     */
    public function leitorNumeroDeEmprestimosAtrasoSemEntrega($pesId)
    {
        $sql        = "
        SELECT count(*) AS qtd
        FROM biblioteca__emprestimo e
        INNER JOIN biblioteca__exemplar ex ON ex.exemplar_id=e.exemplar_id AND ex.exemplar_acesso<>'Kit Saraiva'
        WHERE
            pes_id=:pesId AND
            emprestimo_cancelado='N' AND
            emprestimo_devolucao_data_efetuada IS NULL AND
            :dataAtual > emprestimo_devolucao_data_previsao
        ";
        $parameters = array('pesId' => $pesId, 'dataAtual' => date('Y-m-d H:i:s'));

        $result = $this->executeQueryWithParam($sql, $parameters)->fetch();

        return (int)$result['qtd'];
    }

    /**
     * Retorna os 10 assuntos(separados por vírgula) mais procurados pelo leitor baseado em seu histórico de empréstimo.
     * Obs.: são desconsiderados empréstimos cancelados
     *
     * @param int $pesId Identificador da tabela biblioteca__pessoa
     * @return string
     */
    public function leitorEmprestimoInteresses($pesId)
    {
        $sql        = "
        SELECT
            group_concat(concat('(',t.hit,') ',t.assunto_descricao) SEPARATOR ', ') AS info
        FROM
        (
            SELECT
                bas.*,
                count(bta.assunto_id) AS hit
            FROM
                biblioteca__emprestimo be
                INNER JOIN biblioteca__exemplar bx ON bx.exemplar_id=be.exemplar_id
                INNER JOIN biblioteca__titulo_assunto bta ON bta.titulo_id=bx.titulo_id
                INNER JOIN biblioteca__assunto bas ON bas.assunto_id=bta.assunto_id
            WHERE be.pes_id=:pesId AND emprestimo_cancelado='N'
            GROUP BY bta.assunto_id
            ORDER BY hit DESC
            LIMIT 0, 10
        ) AS t";
        $parameters = array('pesId' => $pesId);

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result[0]['info'];
    }

    /**
     * Retorna o número de empréstimos ativos do usuário separados pelo grupo bibliográfico.
     *
     * @param int $pesId Identificador da tabela biblioteca__pessoa
     * @return array
     */
    public function leitorNumeroItensEmprestadosAtivoPorGrupo($pesId)
    {
        $sql        = "
        SELECT
            bgb.grupo_bibliografico_id,
            grupo_bibliografico_nome,
            COALESCE(emprestimosGrupo.qtd, 0) AS grupo_bibliografico_qtd
        FROM
            biblioteca__grupo_bibliografico bgb
            LEFT JOIN (
                SELECT
                    bt.grupo_bibliografico_id,
                    COUNT(bt.grupo_bibliografico_id) AS qtd
                FROM
                    biblioteca__emprestimo be
                INNER JOIN biblioteca__exemplar bx ON be.exemplar_id = bx.exemplar_id AND bx.exemplar_acesso<>'Kit Saraiva'
                INNER JOIN biblioteca__titulo bt ON bx.titulo_id = bt.titulo_id
                WHERE
                    be.emprestimo_devolucao_data_efetuada IS NULL AND emprestimo_cancelado='N'
                    AND be.pes_id = :pesId
                GROUP BY bt.grupo_bibliografico_id
            ) AS emprestimosGrupo ON emprestimosGrupo.grupo_bibliografico_id = bgb.grupo_bibliografico_id";
        $parameters = array('pesId' => $pesId);

        $lines           = $this->executeQueryWithParam($sql, $parameters)->fetchAll();
        $arrTitulosGrupo = array();

        foreach ($lines as $line) {
            $arrTitulosGrupo[(int)$line['grupo_bibliografico_id']] = array(
                'grupoBibliograficoId'   => $line['grupo_bibliografico_id'],
                'grupoBibliograficoNome' => $line['grupo_bibliografico_nome'],
                'grupoBibliograficoQtd'  => $line['grupo_bibliografico_qtd'],
            );
        }

        return $arrTitulosGrupo;
    }

    /**
     * Retorna os títulos dos empréstimos ativos do usuário .
     *
     * @param int $pesId Identificador da tabela biblioteca__pessoa
     * @return array
     */
    public function leitorTitulosExemplaresEmPoder($pesId)
    {
        $sql        = "
        SELECT be.exemplar_id, bx.titulo_id, be.emprestimo_id
        FROM biblioteca__emprestimo be
        INNER JOIN biblioteca__exemplar bx ON bx.exemplar_id=be.exemplar_id
        WHERE be.emprestimo_devolucao_data_efetuada IS NULL AND be.emprestimo_cancelado='N' AND be.pes_id = :pesId";
        $parameters = array('pesId' => $pesId);

        $lines        = $this->executeQueryWithParam($sql, $parameters)->fetchAll();
        $arExemplares = array();

        foreach ($lines as $line) {
            $arExemplares[(int)$line['titulo_id']] = $line;
        }

        return $arExemplares;
    }

    public function renovarEmprestimo($param)
    {
        $serviceEmprestimo        = new \Biblioteca\Service\Emprestimo($this->getEm());
        $serviceAcessoPessoas     = new \Acesso\Service\AcessoPessoas($this->getEm());
        $session                  = new \Zend\Authentication\Storage\Session();
        $objServiceCalendarioData = new \Professor\Service\AcadperiodoCalendarioData($this->getEm());

        $validarAluno = $param['validarAluno'] ? true : false;

        $arrEmprestimos          = explode(",", $param['emprestimoId']);
        $arrEntidadesEmprestimos = [];

        /** @var $objEmprestimo \Biblioteca\Entity\Emprestimo */

        foreach ($arrEmprestimos as $value) {
            $objEmprestimo = $serviceEmprestimo->getRepository()->findOneBy(['emprestimoId' => $value]);

            if (!$objEmprestimo) {
                $this->setLastError("Empréstimo não localizado!");

                return false;
            }

            if (!$this->validaRenovacao($objEmprestimo, $validarAluno)) {
                return false;
            }

            $arrEntidadesEmprestimos[] = $objEmprestimo;
        }

        $arrUsuario = $session->read();

        /* @var $objUsuario \Acesso\Entity\AcessoPessoas */
        $objUsuario = $serviceAcessoPessoas->getRepository()->find($arrUsuario['id']);

        $arrFeriados = $objServiceCalendarioData->feriadosDepoisDaData(date('Y-m-d'));

        try {
            $this->getEm()->beginTransaction();

            $this->devolveEmprestimo(['emprestimoId' => $param['emprestimoId']]);

            foreach ($arrEntidadesEmprestimos as $objEmprestimo) {
                if (!$this->emprestarItem(
                    $objEmprestimo->getExemplar(),
                    $objEmprestimo->getModalidadeEmprestimo(),
                    $objEmprestimo->getPes(),
                    $objUsuario,
                    $arrFeriados
                )
                ) {
                    throw new \Exception($this->getLastError());
                }
            }

            $this->commit();
        } catch (\Exception $ex) {
            return false;
        }

        return true;
    }

    public function validaRenovacao($objEmprestimo, $validarAluno = false)
    {
        /** @var $objEmprestimo \Biblioteca\Entity\Emprestimo" */
        if (!is_a($objEmprestimo, "\Biblioteca\Entity\Emprestimo")) {
            $this->setLastError("Empréstimo não localizado!");

            return false;
        }

        $qtdDiasRenovacao          = $this->getConfig()->localizarChave("BIBLIOTECA_RENOVACAO_DIAS_LIMITES", 0);
        $qtdRenovacaoConsecutivas  = $this->getConfig()->localizarChave("BIBLIOTECA_RENOVACAO_CONSECUTIVAS", 0);
        $permiteRenovacaoPeloAluno = $this->getConfig()->localizarChave("BIBLIOTECA_RENOVACAO_PELO_ALUNO_ATIVO", 0);

        if (!$permiteRenovacaoPeloAluno && $validarAluno) {
            $this->setLastError("Você não tem permissão para renovar título! \n Contate a instituição!");

            return false;
        }

        if (!$qtdDiasRenovacao || !$qtdRenovacaoConsecutivas) {
            $this->setLastError(
                "Favor conferir as configurações de dias para renovação e configuração de quantidade máxima de renovações consecutivas!\n Entre em contato com suporte!"
            );

            return false;
        }

        $pesId = $objEmprestimo->getPes()->getPes()->getPes()->getPesId();

        $query = "
        SELECT exemplar_id, titulo_id
        FROM biblioteca__emprestimo
        INNER JOIN biblioteca__exemplar USING (exemplar_id)
        WHERE pes_id=" . $pesId . "
        ORDER BY emprestimo_id DESC
        LIMIT " . $qtdRenovacaoConsecutivas;

        $quantidadeEmprestimos = 0;
        $arrEmprestimos        = $this->executeQuery($query)->fetchAll();

        foreach ($arrEmprestimos as $arrEmprestimo) {
            if ($arrEmprestimo['titulo_id'] == $objEmprestimo->getExemplar()->getTitulo()->getTituloId()) {
                $quantidadeEmprestimos++;
            }
        }

        if ($quantidadeEmprestimos > $qtdRenovacaoConsecutivas) {
            $this->setLastError("A quantidade máxima de renovações já foi atingido!");

            return false;
        }

        $dataDevolucao        = $objEmprestimo->getEmprestimoDevolucaoDataPrevisao();
        $dataInicialDevolucao = clone($dataDevolucao);
        $dataInicialDevolucao = $dataInicialDevolucao->modify(' -' . $qtdDiasRenovacao . ' day');

        $dataAtual            = date('Ymd');
        $dataDevolucao        = $dataDevolucao->format('Ymd');
        $dataInicialDevolucao = $dataInicialDevolucao->format('Ymd');

        if ($dataAtual < $dataInicialDevolucao || $dataAtual > $dataDevolucao) {
            $this->setLastError("Já passou o prazo para renovação!");

            return false;
        }

        return true;
    }

    /**
     * @param $emprestimo
     * @return array|bool
     */
    public function formataDadosComprovante($emprestimo)
    {
        $serviceOrIes   = new \Organizacao\Service\OrgCampus($this->getEm());
        $acessoUsuarios = new \Acesso\Service\AcessoPessoas($this->getEm());
        $result         = [];

        if (!$emprestimo) {
            $this->setLastError("É necessário informar pelo menos um emprestimo!");

            return false;
        }

        if (is_array($emprestimo)) {
            $emprestimo = implode(',', $emprestimo);
        }

        $arrEmprestimo = $this->retornaDadosComprovante($emprestimo);

        foreach ($arrEmprestimo as $valor) {
            $arrExemplares[$valor['pes_id']]['exemplares'][] = [
                'exemplarNome'     => $valor['exemplar_titulo'],
                'codigo'           => $valor['exemplar_id'],
                'dataEntrega'      => $valor['data__devolucao_prevista'],
                'dataEmprestimo'   => $valor['data_emprestimo'],
                'operadorCadastro' => $valor['operador_cadastro']

            ];
            $arrExemplares[$valor['pes_id']]['pessoaNome']   = $valor['pessoa_nome'];
        }

        if (!empty($arrExemplares)) {
            $arrInstituicao = $serviceOrIes->retornaDadosInstituicao();

            /* @var $pessoaLogada \Acesso\Entity\AcessoPessoas */
            $pessoaLogada = $acessoUsuarios->retornaUsuarioLogado();
            $pessoaNome   =
                $pessoaLogada ? $pessoaLogada->getPes()->getPes()->getPesNome() : '';

            $result = [
                'ies'             => $arrInstituicao['iesNome'],
                'mantenedora'     => $arrInstituicao['pesNome'],
                'exemplares'      => $arrExemplares,
                'tituloDescricao' => "Empréstimo de acervo(s) ",
                'pessoaLogada'    => $pessoaNome
            ];
        }

        return $result;
    }

    /**
     * @param $emprestimoId
     * @return bool|array
     */
    public function retornaDadosComprovante($emprestimoId)
    {
        if (!$emprestimoId) {
            $this->setLastError("É necessário informar pelo menos um emprestimo!");

            return false;
        }

        $sql = '
        SELECT
            biblioteca__titulo.titulo_titulo exemplar_titulo,
            biblioteca__exemplar.exemplar_id,
            p.pes_id,
            p.pes_nome pessoa_nome,
            DATE_FORMAT(emprestimo.emprestimo_data,"%d/%m/%Y") data_emprestimo,
            DATE_FORMAT(emprestimo.emprestimo_devolucao_data_previsao,"%d/%m/%Y") data__devolucao_prevista,
            cadastrante.pes_nome operador_cadastro,
            emprestimo.emprestimo_id
        FROM biblioteca__emprestimo emprestimo
            INNER JOIN biblioteca__exemplar USING(exemplar_id)
            INNER JOIN biblioteca__titulo USING(titulo_id)
            INNER JOIN pessoa p ON p.pes_id=emprestimo.pes_id
            LEFT JOIN acesso_pessoas acesso ON acesso.usuario=emprestimo.usuario_id
            LEFT JOIN pessoa cadastrante ON cadastrante.pes_id = COALESCE(acesso.pes_fisica,acesso.pes_juridica)
        WHERE emprestimo_id IN (' . $emprestimoId . ')
        GROUP BY emprestimo_id
        ORDER BY p.pes_nome ASC, biblioteca__titulo.titulo_titulo ASC';

        $result = $this->executeQuery($sql)->fetchAll();

        return $result;
    }
}