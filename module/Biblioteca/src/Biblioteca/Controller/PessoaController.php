<?php

namespace Biblioteca\Controller;

use VersaSpine\Controller\AbstractCoreController;
use Zend\View\Model\JsonModel;

class PessoaController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request       = $this->getRequest();
        $paramsGet     = $request->getQuery()->toArray();
        $paramsPost    = $request->getPost()->toArray();
        $servicePessoa = new \Biblioteca\Service\Pessoa($this->getEntityManager());

        $result = $servicePessoa->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $objPessoa = new \Biblioteca\Service\Pessoa($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $objPessoa->getJSONListingData($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);

            return $this->getJson();
        }

        return $this->getJson();
    }

    public function addAction($alunoId = false)
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $arrDadosPessoa   = array();
        $objServicePessoa = new \Biblioteca\Service\Pessoa($em);
        $objGrupoLeitor   = new \Biblioteca\Service\GrupoLeitor($em);

        if ($alunoId) {
            $arrDadosPessoa = $objServicePessoa->getArray($alunoId);

            if (empty($arrDadosPessoa)) {
                $this->flashMessenger()->addErrorMessage($objServicePessoa->getLastError());

                return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
            }
        }

        if ($request->isPost()) {
            $arrDadosPessoa = array_merge($arrDadosPessoa, $request->getPost()->toArray());
            $arrDadosPessoa = \VersaSpine\Stdlib\Util::sanitizeArray($arrDadosPessoa);
            $salvar         = $objServicePessoa->save($arrDadosPessoa);

            if ($salvar) {
                $this->flashMessenger()->addSuccessMessage('Pessoa salva!');

                return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
            } else {
                $this->flashMessenger()->addErrorMessage($objServicePessoa->getLastError());
            }
        }

        $arrGrupoLeitor = $objGrupoLeitor->pagination()['dados']->fetchAll();

        if ($arrDadosPessoa['pessoaAluno'] || $arrDadosPessoa['pessoaUsuario']) {
            $mensagem = 'ATENÇÃO: As alterações efetuadas refletirão no cadastro __PESSOA__.';
            $mensagem .= '<br>As alterações no <b>nome</b> e <b>CPF</b> podem ser efetuadas __RESPONSAVEL__.';

            if ($arrDadosPessoa['pessoaAluno']) {
                $mensagem = str_replace('__PESSOA__', 'do aluno', $mensagem);
                $mensagem = str_replace('__RESPONSAVEL__', 'pela <b>secretaria</b>', $mensagem);
            } elseif (!$arrDadosPessoa['pessoaAluno'] && $arrDadosPessoa['pessoaUsuario']) {
                $mensagem = str_replace('__PESSOA__', 'desta pessoa', $mensagem);
                $mensagem = str_replace('__RESPONSAVEL__', 'pelo <b>administrador do sistema</b>', $mensagem);
            }

            $this->flashMessenger()->addInfoMessage($mensagem);
        }

        if ($arrDadosPessoa['pessoaUsuario']) {
            $this->flashMessenger()->addInfoMessage(
                'ATENÇÃO: A modificação da <b>senha</b> deste usuário <b>não</b> está disponível na <b>biblioteca</b>.'
            );
        }

        $objServicePessoa->getArrayByPost($arrDadosPessoa);

        $this->getView()->setVariable('grupoLeitor', $arrGrupoLeitor);
        $this->getView()->setVariable('dadosPessoa', $arrDadosPessoa);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $identificador = $this->params()->fromRoute("id", 0);

        if (!$identificador) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($identificador);
    }

    public function searchPessoaFisicaAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $objPessoa     = new \Biblioteca\Service\Pessoa($this->getEntityManager());
            $erro          = false;
            $erroDescricao = '';
            $pesCpf        = $request->getPost('pesCpf');
            $pesNome       = $request->getPost('pesNome');
            $pesId         = $request->getPost('pesId');

            if ($pesCpf) {
                $arrPessoaFisica = $objPessoa->getArrayPessoaFisica(array('pesCpf' => $pesCpf));
            } elseif ($pesId) {
                $arrPessoaFisica = $objPessoa->getArrayPessoaFisica(array('pesId' => $pesId));
            } elseif ($pesNome) {
                $arrPessoaFisica = $objPessoa->pesquisaPessoaPorNome($pesNome);
            }

            if (empty($arrPessoaFisica)) {
                $erro          = true;
                $erroDescricao = $objPessoa->getLastError();
            }

            $this->getJson()->setVariable('pessoaFisica', $arrPessoaFisica);
            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }

    public function informacoesBasicasAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $objPessoa     = new \Biblioteca\Service\Pessoa($this->getEntityManager());
            $erro          = false;
            $erroDescricao = '';
            $pesId         = $request->getPost('pesId');
            $matricula     = $request->getPost('matricula');
            $arrLeitor     = $objPessoa->getArrayDadosLeitor($pesId, $matricula);

            if (empty($arrLeitor)) {
                $erro          = true;
                $erroDescricao = $objPessoa->getLastError();
            }

            $this->getJson()->setVariable('leitor', $arrLeitor);
            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}