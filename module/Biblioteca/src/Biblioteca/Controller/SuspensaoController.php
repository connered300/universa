<?php

namespace Biblioteca\Controller;

use VersaSpine\Controller\AbstractCoreController;

class SuspensaoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
    }

    public function indexAction()
    {
        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $objServiceSuspensao = new \Biblioteca\Service\Suspensao($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $objServiceSuspensao->getJSONListingData($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);

            return $this->getJson();
        }

        return $this->getJson();
    }

    public function cancelarAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $objServiceSuspensao = new \Biblioteca\Service\Suspensao($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $objServiceSuspensao->cancelaSuspensao($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $objServiceSuspensao->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);

            return $this->getJson();
        }

        return $this->getJson();
    }
}