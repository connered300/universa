<?php

namespace Biblioteca\Controller;

use VersaSpine\Controller\AbstractCoreController;

class MultaController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
    }

    public function indexAction()
    {
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEntityManager());

        $permissaoBaixa = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Biblioteca\Controller\Multa',
            'Baixa'
        );

        $permissaoCancelar = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Biblioteca\Controller\Multa',
            'Cancelar'
        );

        $this->getView()->setVariable('permissaoBaixa', ($permissaoBaixa ? 1 : 0));
        $this->getView()->setVariable('permissaoCancelar', ($permissaoCancelar ? 1 : 0));

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $objServiceMulta = new \Biblioteca\Service\Multa($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $objServiceMulta->getJSONListingData($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);

            return $this->getJson();
        }

        return $this->getJson();
    }

    public function cancelarAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $objServiceMulta = new \Biblioteca\Service\Multa($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $objServiceMulta->cancelaMulta($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $objServiceMulta->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);

            return $this->getJson();
        }

        return $this->getJson();
    }

    public function baixaAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $objServiceMulta = new \Biblioteca\Service\Multa($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $objServiceMulta->baixaMulta($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $objServiceMulta->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);

            return $this->getJson();
        }

        return $this->getJson();
    }
}