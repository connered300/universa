<?php

namespace Biblioteca\Controller;

use VersaSpine\Controller\AbstractCoreController;
use Zend\View\Model\JsonModel;

class SetorController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request           = $this->getRequest();
        $paramsGet         = $request->getQuery()->toArray();
        $paramsPost        = $request->getPost()->toArray();
        $objServiceAssunto = new \Biblioteca\Service\Setor($this->getEntityManager());

        $result = $objServiceAssunto->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $this->getView()->setVariable('urlSetorAdd', $this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $objServiceSetor = new \Biblioteca\Service\Setor($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $objServiceSetor->getJSONListingData($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);

            return $this->getJson();
        }

        return $this->getJson();
    }

    public function addAction($setorId = false)
    {
        $em       = $this->getEntityManager();
        $request  = $this->getRequest();
        $objSetor = new \Biblioteca\Service\Setor($em);

        $dadosSetor = array();

        if ($setorId) {
            $dadosSetor = $objSetor->getArray($setorId);
        }

        if ($request->isPost()) {
            $dadosSetor = $request->getPost()->toArray();
            $dadosSetor = \VersaSpine\Stdlib\Util::sanitizeArray($dadosSetor);
            $salvar     = $objSetor->save($dadosSetor);
            $ajax       = false;

            if (isset($dadosSetor['ajax']) && $dadosSetor['ajax']) {
                $this->setView(new JsonModel());
                $ajax = true;
            }

            if ($salvar) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }
            } else {
                $this->getView()->setVariable("erro", array($objSetor->getLastError()));
            }
        }

        $this->getView()->setVariable("dadosSetor", $dadosSetor);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $setorId = $this->params()->fromRoute("id", 0);

        if (!$setorId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($setorId);
    }
}