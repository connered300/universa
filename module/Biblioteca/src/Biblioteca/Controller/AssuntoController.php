<?php

namespace Biblioteca\Controller;

use VersaSpine\Controller\AbstractCoreController;
use Zend\View\Model\JsonModel;

class AssuntoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request           = $this->getRequest();
        $paramsGet         = $request->getQuery()->toArray();
        $paramsPost        = $request->getPost()->toArray();
        $objServiceAssunto = new \Biblioteca\Service\Assunto($this->getEntityManager());

        $result = $objServiceAssunto->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $this->getView()->setVariable('urlAssuntoAdd', $this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $objServiceAssunto = new \Biblioteca\Service\Assunto($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $objServiceAssunto->getJSONListingData($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);

            return $this->getJson();
        }

        return $this->getJson();
    }

    public function addAction($assuntoId = false)
    {
        $em         = $this->getEntityManager();
        $request    = $this->getRequest();
        $objAssunto = new \Biblioteca\Service\Assunto($em);

        $dadosAssunto = array();

        if ($assuntoId) {
            $dadosAssunto = $objAssunto->getArray($assuntoId);
        }

        if ($request->isPost()) {
            $dadosAssunto = $request->getPost()->toArray();
            $dadosAssunto = \VersaSpine\Stdlib\Util::sanitizeArray($dadosAssunto);
            $salvar       = $objAssunto->save($dadosAssunto);
            $ajax         = false;

            if (isset($dadosAssunto['ajax']) && $dadosAssunto['ajax']) {
                $this->setView(new JsonModel());
                $ajax = true;
            }

            if ($salvar) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }
            } else {
                $this->getView()->setVariable("erro", array($objAssunto->getLastError()));
            }
        }

        $objAssunto->getArrayByPost($dadosAssunto);

        $this->getView()->setVariable("dadosAssunto", $dadosAssunto);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $assuntoId = $this->params()->fromRoute("id", 0);

        if (!$assuntoId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($assuntoId);
    }
}