<?php

namespace Biblioteca\Controller;

use VersaSpine\Controller\AbstractCoreController;
use Zend\View\Model\JsonModel;

/**
 * Class EmprestimoController
 * @package Biblioteca\Controller
 */
class EmprestimoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
    }

    /**
     * Painel de empréstimo de itens do acervo
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction()
    {
        $arrConfig              = $this->getServiceManager()->get('Config');
        $serviceSisConfig       = new \Sistema\Service\SisConfig($this->getEntityManager(), $arrConfig);
        $objGrupoBibliografico  = new \Biblioteca\Service\GrupoBibliografico($this->getEntityManager());
        $jsonGrupoBibliografico = json_encode($objGrupoBibliografico->getArrSelect2());

        $quantidadeDiasRenovacao         = $serviceSisConfig->localizarChave("BIBLIOTECA_RENOVACAO_DIAS_LIMITES");
        $quantidadeRenovacaoConsecutivas = $serviceSisConfig->localizarChave("BIBLIOTECA_RENOVACAO_CONSECUTIVAS");

        $dataAtual = new \Datetime('now');
        $dataAtual = $dataAtual->format('Y-m-d');

        $this->getView()->setVariable('jsonGrupoBibliografico', $jsonGrupoBibliografico);
        $this->getView()->setVariable('dataAtual', $dataAtual);
        $this->getView()->setVariable('diasParaRenovacao', $quantidadeDiasRenovacao);
        $this->getView()->setVariable('quantidadeDeRenovacoes', $quantidadeRenovacaoConsecutivas);

        return $this->getView();
    }

    /**
     * Executa uma determinada ação relacionada ao empréstimo.
     * $_POST['acao']='devolver';// Devolve itens
     * $_POST['acao']='cancelar';// Cancela empréstimo
     * $_POST['acao']='emprestar';// Empresta itens
     * @return JsonModel
     */
    public function acaoAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceEmprestimo = new \Biblioteca\Service\Emprestimo(
                $this->getEntityManager(),
                $this->getServiceManager()->get('Config')
            );

            $erro          = false;
            $erroDescricao = '';
            $acao          = $request->getPost('acao');
            $dataPost      = $request->getPost()->toArray();

            if ($acao == 'devolver') {
                $retorno = $serviceEmprestimo->devolveEmprestimo($dataPost);
            } elseif ($acao == 'cancelar') {
                $retorno = $serviceEmprestimo->cancelaEmprestimo($dataPost);
            } elseif ($acao == 'emprestar') {
                $retorno = $serviceEmprestimo->emprestaExemplar($dataPost);
            } elseif ($acao == 'renovar') {
                $retorno = $serviceEmprestimo->renovarEmprestimo($dataPost);
            }

            if (empty($retorno)) {
                $erro          = true;
                $erroDescricao = $serviceEmprestimo->getLastError();
            } else {
                try {
                    $retorno = is_array($retorno) ? implode(',', $retorno) : $retorno;

                    $this->getJson()->setVariable('emprestimoId', $retorno);
                } catch (\Exception $ex) {
                }
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);

            return $this->getJson();
        }

        return $this->getJson();
    }

    /**
     * Retorna informações de empréstimos de leitores para uso em datatables
     * @return JsonModel
     */
    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $objServiceEmprestimo = new \Biblioteca\Service\Emprestimo(
                $this->getEntityManager(),
                $this->getServiceManager()->get('Config')
            );

            $dataPost = $request->getPost()->toArray();
            $result   = $objServiceEmprestimo->pesquisaForJson($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);

            return $this->getJson();
        }

        return $this->getJson();
    }

    public function renovarEmprestimoAlunoAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $objServiceEmprestimo = new \Biblioteca\Service\Emprestimo(
                $this->getEntityManager(),
                $this->getServiceManager()->get('Config')
            );

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();

            $dataPost['validarAluno'] = true;

            $ok = $objServiceEmprestimo->renovarEmprestimo($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $objServiceEmprestimo->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);

            return $this->getJson();
        }

        return $this->getJson();
    }

    public function comprovanteAction()
    {
        $serviceEmprestimo = new \Biblioteca\Service\Emprestimo(
            $this->getEntityManager(),
            $this->getServiceManager()->get('Config')
        );

        /** @var \Zend\Http\Request $request */
        $request  = $this->getRequest();
        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        $this->getView()->setTemplate('/biblioteca/emprestimo/comprovante-emprestimo');

        $result = $serviceEmprestimo->formataDadosComprovante($arrDados['emprestimoId']);

        if ($result) {
            $this->getView()->setVariables(
                ['arrDados' => $result]
            );
        }

        $this->getView()->setTerminal(true);

        return $this->getView();
    }
}