<?php

namespace Biblioteca\Controller;

use DOMPDFModule\View\Model\PdfModel;
use VersaSpine\Controller\AbstractCoreController;
use VersaSpine\Service\AbstractService;

class RelatorioController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
    }

    public function indexAction()
    {
        return $this->getView();
    }

    public function exemplarEtiquetaAction($codigoDeBarra = false)
    {
        $request  = $this->getRequest();
        $mensagem = 'Selecione o intervalo de exemplares para gerar as etiquetas de lombada dos exemplares:';

        $arrLayout = array(
            ['id' => 'exemplar-etiqueta-print', 'text' => 'Layout 1'],
            ['id' => 'exemplar-etiqueta-print-padrao2', 'text' => 'Layout 2'],
        );

        if ($codigoDeBarra) {
            $mensagem = 'Selecione o intervalo de exemplares para gerar as etiquetas de código de barras dos exemplares:';

            $arrLayout = array(
                ['id' => 'exemplar-etiqueta-codigo-barra-print', 'text' => 'Layout 1'],
                ['id' => 'exemplar-etiqueta-codigo-barra-print-padrao2', 'text' => 'Layout 2'],
                ['id' => 'exemplar-etiqueta-codigo-barra-print-padrao3', 'text' => 'Layout 3(6182)']
            );
        }

        $this->getView()->setVariable('arrLayout', json_encode($arrLayout));
        $this->getView()->setVariable('mensagem', $mensagem);

        $this->getView()->setTemplate($this->getTemplateToRoute('exemplar-etiqueta'));

        if ($request->isPost()) {
            $template           = $request->getPost('etiquetaLayout');
            $girar              = ($template == 'exemplar-etiqueta-codigo-barra-print' ? true : false);
            $objServiceExemplar = new \Biblioteca\Service\Exemplar($this->getEntityManager());

            $dataPost   = $request->getPost()->toArray();
            $exemplares = $objServiceExemplar->getExemplaresEtiquetaLombada(
                $dataPost['exemplarInicial'],
                $dataPost['exemplarFinal'],
                $dataPost['exemplarIntervalo'],
                $codigoDeBarra
            );

            $this->getView()->setVariable("exemplarInicial", $dataPost['exemplarInicial']);
            $this->getView()->setVariable("exemplarFinal", $dataPost['exemplarFinal']);
            $this->getView()->setVariable("exemplarIntervalos", $dataPost['exemplarIntervalos']);
            $this->getView()->setVariable("exemplares", $exemplares);
            $this->getView()->setVariable("girar", $girar);
            $this->getView()->setTemplate($this->getTemplateToRoute($template));
        }

        return $this->getView();
    }

    public function exemplarEtiquetaCodigoBarraAction()
    {
        return $this->exemplarEtiquetaAction(true);
    }

    public function exemplarBaixaAction()
    {
        $request                      = $this->getRequest();
        $dataPost                     = array();
        $objServiceExemplar           = new \Biblioteca\Service\Exemplar($this->getEntityManager());
        $objServiceGrupoBibliografico = new \Biblioteca\Service\GrupoBibliografico($this->getEntityManager());
        $objServiceEmpresa            = new \Biblioteca\Service\Empresa($this->getEntityManager());
        $objServiceAcesso             = new \Acesso\Service\AcessoPessoas($this->getEntityManager());
        $objServiceCampus             = new \Organizacao\Service\OrgCampus($this->getEntityManager());
        /** @var \SnowReportsZf2\Service\SnowReportsZf2 $snow */
        $snow = $this->getServiceLocator()->get('SnowReportsZf2\Service\SnowReportsZf2');

        if ($request->isPost()) {
            $dataPost = $request->getPost()->toArray();

            $data_Inicial = $dataPost['dataInicial'];

            $data_final = $dataPost['dataFinal'];

            $dataPost['dataInicial'] = $objServiceExemplar->formatDateAmericano($dataPost['dataInicial']);

            $dataPost['dataFinal'] = $objServiceExemplar->formatDateAmericano($dataPost['dataFinal']);

            $dadosinstituicao = $objServiceCampus->retornaDadosInstituicao();

            $grupos = '';

            $usuarios = '';

            $editoras = '';

            if ($dataPost['grupoBibliografico']) {
                $grupos = $objServiceGrupoBibliografico->getDescricao($dataPost['grupoBibliografico']);
            }

            if ($dataPost['editora']) {
                $editoras = $objServiceEmpresa->getDescricao(
                    explode(',', $dataPost['editora'])
                );
            }

            if ($dataPost['usuario']) {
                $usuarios = $objServiceAcesso->getDescricao(
                    explode(',', $dataPost['usuario'])
                );
            }

            $relatorioinfo = ($grupos ? 'Grupo bibliográfico: ' . $grupos . ' | ' : '') .
                ($editoras ? 'Editora: ' . $editoras . ' | ' : '') .
                ($usuarios ? 'Operador: ' . $usuarios . ' | ' : '') .
                'Período: ' .
                $data_Inicial . ' - ' .
                $data_final .
                ($dataPost['exemplarTipo'] ? ' | ' . $dataPost['exemplarTipo'] : ' ');

            try {
                $snow->setJrxmlPath(__DIR__ . '/../Reports/')
                    ->setJrxmlFile('exemplar-baixa.jrxml')
                    ->setAttrbutes(
                        [
                            'logo'               => $dadosinstituicao['logo'],
                            'mantenedora'        => $dadosinstituicao['mantenedora'],
                            'instituicao'        => $dadosinstituicao['ies'],
                            'instituicaoinfo'    => '',
                            'datainicio'         => $dataPost['dataInicial'],
                            'datafim'            => $dataPost['dataFinal'],
                            'titulo'             => 'Relação de exemplares baixados',
                            'relatorioinfo'      => $relatorioinfo,
                            'endereco'           => $dadosinstituicao['endereco'],
                            'sistema'            => 'Universa',
                            'grupobibliografico' => $dataPost['grupoBibliografico'] ? $dataPost['grupoBibliografico']
                                : null,
                            'editora'            => $dataPost['editora'] ? $dataPost['editora'] : null,
                            'exemplartipo'       => $dataPost['exemplarTipo'] ? $dataPost['exemplarTipo'] : null,
                            'usuario'            => $dataPost['usuario'] ? $dataPost['usuario'] : null
                        ]
                    )->setOutputFile('', 'pdf')
                    ->execute();
            } catch (\Exception $e) {
                echo "Não foi possível imprimir o relatório devido à " . $e;
            }
        } else {
            $dataPost['dataInicial'] = date('Y-m-d', strtotime('-1 month'));

            $dataPost['dataFinal'] = date('Y-m-d');

            $jsonGrupoBibliografico = json_encode($objServiceGrupoBibliografico->getArrSelect2());

            $this->getView()->setVariable('jsonGrupoBibliografico', $jsonGrupoBibliografico);
        }

        $dataInicial = $objServiceExemplar->formatDateBrasileiro($dataPost['dataInicial']);

        $dataFinal = $objServiceExemplar->formatDateBrasileiro($dataPost['dataFinal']);

        $this->getView()->setVariable("dataInicial", $dataInicial);

        $this->getView()->setVariable("dataFinal", $dataFinal);

        return $this->getView();
    }

    public function tituloRankingAction()
    {
        $request          = $this->getRequest();
        $dataPost         = array();
        $objServiceTitulo = new \Biblioteca\Service\Titulo($this->getEntityManager());
        $serviceOrgCampus = new \Organizacao\Service\OrgCampus($this->getEntityManager());

        if ($request->isPost()) {
            $dataPost = $request->getPost()->toArray();

            $dataPost['dataInicial'] = $objServiceTitulo->formatDateAmericano($dataPost['dataInicial']);
            $dataPost['dataFinal']   = $objServiceTitulo->formatDateAmericano($dataPost['dataFinal']);

            $titulos = $objServiceTitulo->relatorioRankingEmprestimo($dataPost);
            $this->setView(new PdfModel());
            $this->getView()->setOption('paperSize', 'a4');
            $this->getView()->setOption('paperOrientation', 'landscape');
            $this->getView()->setOption('filename', 'ranking-titulos');

            $dataPost['titulos']     = $titulos;
            $dataPost['dataInicial'] = $objServiceTitulo->formatDateBrasileiro($dataPost['dataInicial']);
            $dataPost['dataFinal']   = $objServiceTitulo->formatDateBrasileiro($dataPost['dataFinal']);

            $this->getView()->setVariables($dataPost);
            $this->getView()->setVariables($serviceOrgCampus->retornaDadosInstituicao());
            $this->getView()->setTemplate($this->getTemplateToRoute('titulo-ranking-print'));
        } else {
            $objGrupoBibliografico  = new \Biblioteca\Service\GrupoBibliografico($this->getEntityManager());
            $jsonGrupoBibliografico = json_encode($objGrupoBibliografico->getArrSelect2());
            $this->getView()->setVariable('jsonGrupoBibliografico', $jsonGrupoBibliografico);
        }

        return $this->getView();
    }

    public function cadastroAction()
    {
        $request                      = $this->getRequest();
        $objServiceTitulo             = new \Biblioteca\Service\Titulo($this->getEntityManager());
        $serviceOrgCampus             = new \Organizacao\Service\OrgCampus($this->getEntityManager());
        $objServiceGrupoBibliografico = new \Biblioteca\Service\GrupoBibliografico($this->getEntityManager());
        $objServiceEmpresa            = new \Biblioteca\Service\Empresa($this->getEntityManager());
        $objServiceAcesso             = new \Acesso\Service\AcessoPessoas($this->getEntityManager());

        if ($request->isPost()) {
            $dataPost = $request->getPost()->toArray();

            $dadosinstituicao = $serviceOrgCampus->retornaDadosInstituicao();

            $data_inicial = $dataPost['dataInicial'];

            $data_final = $dataPost['dataFinal'];

            $dataPost['dataInicial'] = $objServiceTitulo->formatDateAmericano($dataPost['dataInicial']);

            $dataPost['dataFinal'] = $objServiceTitulo->formatDateAmericano($dataPost['dataFinal']);

            $grupos = '';

            $usuarios = '';

            $editoras = '';

            if ($dataPost['grupoBibliografico']) {
                $grupos = $objServiceGrupoBibliografico->getDescricao(
                    $dataPost['grupoBibliografico']
                );
            }

            if ($dataPost['editora']) {
                $editoras = $objServiceEmpresa->getDescricao(
                    explode(',', $dataPost['editora'])
                );
            }

            if ($dataPost['usuario']) {
                $usuarios = $objServiceAcesso->getDescricao($dataPost['usuario']);
            }

            $relatorioinfo = ($grupos ? 'Grupos bibliográficos: ' . $grupos . ' | ' : '') .
                ($editoras ? 'Editora: ' . $editoras . ' | ' : '') .
                ($usuarios ? 'Operador: ' . $usuarios . ' | ' : '') .
                'Periodo: ' .
                $data_inicial . ' - ' .
                $data_final .
                ($dataPost['exemplarTipo'] ? ' | ' . $dataPost['exemplarTipo'] : ' ');

            $snow = $this->getServiceLocator()->get('SnowReportsZf2\Service\SnowReportsZf2');

            try {
                $snow->setJrxmlPath(__DIR__ . '/../Reports/')
                    ->setJrxmlFile('relatorio-titulos.jrxml')
                    ->setAttrbutes(
                        [
                            'relatorioinfo'      => $relatorioinfo,
                            'logo'               => $dadosinstituicao['logo'],
                            'mantenedora'        => $dadosinstituicao['mantenedora'],
                            'instituicao'        => $dadosinstituicao['ies'],
                            'instituicaoinfo'    => '',
                            'inicio'             => $dataPost['dataInicial'],
                            'titulo'             => 'Relatório de cadastros da Biblioteca',
                            'fim'                => $dataPost['dataFinal'],
                            'grupobibliografico' => $dataPost['grupoBibliografico'] ? $dataPost['grupoBibliografico']
                                : null,
                            'editora'            => $dataPost['editora'] ? $dataPost['editora'] : null,
                            'exemplarTipo'       => $dataPost['exemplarTipo'] ? $dataPost['exemplarTipo'] : null,
                            'usuario'            => $dataPost['usuario'] ? $dataPost['usuario'] : null
                        ]
                    )->setOutputFile('', 'pdf')
                    ->execute();
            } catch (\Exception $erro) {
                echo "Verifique o arquivo " . $erro->getFile() . " na linha " . $erro->getLine();
            }
        } else {
            $objGrupoBibliografico  = new \Biblioteca\Service\GrupoBibliografico($this->getEntityManager());
            $jsonGrupoBibliografico = json_encode($objGrupoBibliografico->getArrSelect2());
            $this->getView()->setVariable('jsonGrupoBibliografico', $jsonGrupoBibliografico);
            $this->getView()->setTemplate($this->getTemplateToRoute('cadastro'));
        }

        $jsonGrupoBibliografico = json_encode($objServiceGrupoBibliografico->getArrSelect2());
        $this->getView()->setVariable('jsonGrupoBibliografico', $jsonGrupoBibliografico);

        return $this->getView();
    }

    public function atendimentoAction()
    {
        $request          = $this->getRequest();
        $serviceOrgCampus = new \Organizacao\Service\OrgCampus($this->getEntityManager());
        $servicePessoa    = new \Acesso\Service\AcessoPessoas($this->getEntityManager());
        $serviceGrupo     = new \Biblioteca\Service\GrupoBibliografico($this->getEntityManager());
        $snowZf2          = $this->getServiceLocator()->get('SnowReportsZf2\Service\SnowReportsZf2');
        $dadosinstituicao = $serviceOrgCampus->retornaDadosInstituicao();

        if ($request->isPost()) {
            $dataPost = $request->getPost()->toArray();
            $grupo    = implode($dataPost['grupobibliografico'], ',');

            if ($grupo != "") {
                $grupoDesc = $serviceGrupo->getDescricao($grupo);
            }

            if ($dataPost['usuario'] != null) {
                $usuarioDesc = $servicePessoa->getDescricao($dataPost['usuario']);
            }

            $operacao = $dataPost['devolucao'] ? $dataPost['devolucao'] : '';
            $operacao .= $dataPost['emprestimo'] ? $dataPost['emprestimo'] : '';

            $datainicial = AbstractService::formatDateAmericano($dataPost['dataInicial']);
            $datafinal   = AbstractService::formatDateAmericano($dataPost['dataFinal']);

            $relatorioinfo = 'Período: ' . $dataPost['dataInicial'] . ' - ' . $dataPost['dataFinal'];
            $relatorioinfo .= $usuarioDesc ? ' | Usuário: ' . $usuarioDesc : '';
            $relatorioinfo .= $operacao == '2' ? ' | Tipo de operação: Devolucao' : '' . ' | ';
            $relatorioinfo .= $operacao == '1' ? ' | Tipo de operação: Empréstimo' : '';
            $relatorioinfo .= $operacao == '21' ? ' | Tipo de operação: Empréstimo, Devolução' : '';
            $relatorioinfo .= $grupoDesc ? ' | Grupo Bibliográfico: ' . $grupoDesc : '';

            try {
                $ret = $snowZf2->setJrxmlPath(__DIR__ . '/../Reports/')
                    ->setJrxmlFile("relatorio-biblioteca-atendimento.jrxml")
                    ->setAttrbutes(
                        [
                            'datainicio'         => $datainicial,
                            'datafim'            => $datafinal,
                            'relatorioinfo'      => $relatorioinfo,
                            'operacao'           => $operacao ? $operacao : null,
                            'usuario'            => $dataPost['usuario'] ? $dataPost['usuario'] : null,
                            'grupobibliografico' => $grupo ? '"' . $grupo . '"' : null,
                            'instituicaoinfo'    => '',
                            'titulo'             => 'Relação de Atendimentos da Biblioteca',
                            'endereco'           => $dadosinstituicao['endereco'],
                            'sistema'            => 'Universa',
                        ]
                    )
                    ->setOutputFile('', 'pdf')
                    ->execute();
            } catch (\Exception $erro) {
                echo "Não foi possível gerar o relatório devido a " . $erro->getLine();
            }
        } else {
            $objGrupoBibliografico  = new \Biblioteca\Service\GrupoBibliografico($this->getEntityManager());
            $jsonGrupoBibliografico = json_encode($objGrupoBibliografico->getArrSelect2());
            $this->getView()->setVariable('jsonGrupoBibliografico', $jsonGrupoBibliografico);
            $this->getView()->setTemplate($this->getTemplateToRoute('atendimento'));
        }

        return $this->getView();
    }

    public function relacaoInadimplentesAction()
    {
        $objServiceGrupoBibliografico = new \Biblioteca\Service\GrupoBibliografico($this->getEntityManager());
        $objServiceEmpresa            = new \Biblioteca\Service\Empresa($this->getEntityManager());
        $objServiceAcesso             = new \Acesso\Service\AcessoPessoas($this->getEntityManager());
        $objServiceCampus             = new \Organizacao\Service\OrgCampus($this->getEntityManager());

        $snow = $this->getServiceLocator()->get('SnowReportsZf2\Service\SnowReportsZf2');

        $dadosinstituicao = $objServiceCampus->retornaDadosInstituicao();

        $request = $this->getRequest();

        if ($request->isPost()) {
            $dataPost = $request->getPost()->toArray();

            $datainicio = AbstractService::formatDateAmericano($dataPost['dataInicial']);

            $datafim = AbstractService::formatDateAmericano($dataPost['dataFinal']);

            $grupo = implode($dataPost['grupoBibliografico'], ',');

            if ($grupo != "") {
                $grupoDesc = $objServiceGrupoBibliografico->getDescricao($grupo);
            }

            $editora = implode($dataPost['editora'], ',');

            if ($editora != "") {
                $editoraDesc = $objServiceEmpresa->getDescricao($dataPost['editora']);
            }

            $usuario = $dataPost['usuario'];

            $leitor = implode($dataPost['leitor'], ',');

            $relatorioinfo = 'Período: ' . $dataPost['dataInicial'] . ' - ' . $dataPost['dataFinal'];

            $relatorioinfo .= $grupoDesc ? ' | Grupo Bibliográfico: ' . $grupoDesc : ' | Grupo Bibliográfico: Todos';
            $relatorioinfo .= $editoraDesc ? ' | Editora: ' . $editoraDesc : ' | Editora: Todas';
            $relatorioinfo .= $dataPost['exemplarTipo'] ? ' | Kit Saraiva Incluido' : ' | Kit saraiva não incluído';

            try {
                $ret = $snow->setJrxmlPath(__DIR__ . '/../Reports/')
                    ->setJrxmlFile('relacao-emprestimos-atrasados.jrxml')
                    ->setAttrbutes(
                        [
                            'logo'               => $dadosinstituicao['logo'],
                            'mantenedora'        => $dadosinstituicao['mantenedora'],
                            'instituicao'        => $dadosinstituicao['ies'],
                            'instituicaoinfo'    => '',
                            'datainicio'         => $datainicio,
                            'datafim'            => $datafim,
                            'titulo'             => 'Inadimplência de empréstimo de Exemplares',
                            'relatorioinfo'      => $relatorioinfo,
                            'endereco'           => $dadosinstituicao['endereco'],
                            'sistema'            => 'Universa',
                            'grupobibliografico' => $grupo ? $grupo : null,
                            'leitor'             => $leitor ? $leitor : null,
                            'editora'            => $editora ? $editora : null,
                            'kitsaraiva'         => $dataPost['exemplarTipo'] ? $dataPost['exemplarTipo']
                                : null,
                            'usuario'            => $dataPost['usuario'] ? $dataPost['usuario'] : null
                        ]
                    )->setOutputFile('', 'pdf')
                    ->execute();
            } catch (\Exception $e) {
                echo "Não foi possível imprimir o relatório devido à " . $e;
            }
        } else {
            $jsonGrupoBibliografico = json_encode($objServiceGrupoBibliografico->getArrSelect2());
            $this->getView()->setVariable('jsonGrupoBibliografico', $jsonGrupoBibliografico);

            $this->getView()->setTemplate($this->getTemplateToRoute('relacao-inadimplentes'));
        }

        return $this->getView();
    }
}