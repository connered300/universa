<?php

namespace Biblioteca\Controller;

use VersaSpine\Controller\AbstractCoreController;
use Zend\Barcode\Barcode;
use Zend\Barcode\Object\Code39;
use Zend\Barcode\Renderer\Image;
use Zend\Config\Config;
use Zend\View\Model\JsonModel;

class ExemplarController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request            = $this->getRequest();
        $paramsGet          = $request->getQuery()->toArray();
        $paramsPost         = $request->getPost()->toArray();
        $params             = [];
        $objServiceExemplar = new \Biblioteca\Service\Exemplar($this->getEntityManager());

        $result = $objServiceExemplar->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        if ($params['index']) {
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);

            return $this->getJson();
        }

        return new JsonModel($result ? $result : array());
    }

    public function addAction()
    {
        $dadosExemplar = array();

        $this->getView()->setVariable("dadosExemplar", $dadosExemplar);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function indexAction()
    {
        $objGrupoBibliografico = new \Biblioteca\Service\GrupoBibliografico($this->getEntityManager());

        $jsonGrupoBibliografico = json_encode($objGrupoBibliografico->getArrSelect2());

        $this->getView()->setVariable('jsonGrupoBibliografico', $jsonGrupoBibliografico);

        return $this->getView();
    }

    public function baixaAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $objServiceExemplar = new \Biblioteca\Service\Exemplar($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();

            $ok = $objServiceExemplar->efetuaBaixa($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $objServiceExemplar->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);

            return $this->getJson();
        }

        return $this->getJson();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $objTitulo = new \Biblioteca\Service\Exemplar($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $objTitulo->pesquisaForJson($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);

            return $this->getJson();
        }

        return $this->getJson();
    }

    public function barcodeAction()
    {
        $identificador = $this->params()->fromRoute("id", 0);
        $girar         = $this->params()->fromQuery("vertical", 0);
        $text          = $this->params()->fromQuery("text", 1);
        $height        = $this->params()->fromQuery("height", false);

        $text  = $text != 0;
        $girar = (boolean)$girar;

        if (is_numeric($identificador)) {
            $identificador = (int)$identificador;
            $identificador = str_pad((int)$identificador, 8, '0', STR_PAD_LEFT);
        }

        if (!$identificador) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        ob_start();
        ob_clean();

        $imgDir='./data/barcode/';
        $imgCache = (
            $imgDir.
            'barcode' .
            '-i-' . $identificador .
            '-g-' . $girar .
            '-t-' . $text .
            '-h-' . $height .
            '.png'
        );

        if (!is_dir($imgDir)) {
            @mkdir($imgDir, 0755, true);
        }

        if (!is_file($imgCache)) {
            $font = getcwd() . DIRECTORY_SEPARATOR . 'arial.ttf';

            $arrBarcode = [
                'imageType' => 'png',
                'font'      => $font,
                'text'      => $identificador,
                'drawText'  => $text
            ];

            if ($height) {
                $arrBarcode['barHeight'] = $height;
            }

            $barcode = new Code39($arrBarcode);

            $renderer = new Image(['barcode' => $barcode]);

            $img = $renderer->draw();

            if ($girar) {
                $img = imagerotate($img, 90, 0);
            }

            imagepng($img, $imgCache, 0, PNG_NO_FILTER);
        }

        header('Content-Type: image/png');
        echo file_get_contents($imgCache);
        exit(0);
    }
}