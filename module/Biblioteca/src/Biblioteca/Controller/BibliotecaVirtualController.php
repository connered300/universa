<?php

namespace Biblioteca\Controller;

use VersaSpine\Controller\AbstractCoreController;
use Zend\View\Model\JsonModel;

class BibliotecaVirtualController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function indexAction()
    {
        return $this->getView();
    }

    public function addAction()
    {
        $servicebibliotecaVirtual = new \Biblioteca\Service\BibliotecaVirtual($this->getEntityManager());
        $request                  = $this->getRequest();
        $result                   = array('success' => true, 'message' => 'Integração concluída com sucesso!');

        if ($request->isPost()) {
            $arrDados = $request->getPost()->toArray();

            if(!$servicebibliotecaVirtual->integrarAluno($arrDados['pes_id'])){
                $result['success'] = false;
                $result['message'] = $servicebibliotecaVirtual->getLastError();
            }
        }

        return new JsonModel($result ? $result : array());
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceBiblioteca = new \Biblioteca\Service\BibliotecaVirtual($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceBiblioteca->getDataForDataTables($dataPost);

            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);

            return $this->getJson();
        }

        return $this->getJson();
    }

    public function removeAction()
    {
        $servicebibliotecaVirtual = new \Biblioteca\Service\BibliotecaVirtual($this->getEntityManager());
        $request                  = $this->getRequest();
        $result                   = array('success' => true, 'message' => 'Acesso removido sucesso!');

        if ($request->isPost()) {
            $arrDados = $request->getPost()->toArray();

            if(!$servicebibliotecaVirtual->remover($arrDados['pes_id'])){
                $result['success'] = false;
                $result['message'] = $servicebibliotecaVirtual->getLastError();
            }
        }

        return new JsonModel($result ? $result : array());
    }

    public function criarUrlAction()
    {
        $servicebibliotecaVirtual = new \Biblioteca\Service\BibliotecaVirtual($this->getEntityManager());
        $serviceAcessoPessoa = new \Acesso\Service\AcessoPessoas($this->getEntityManager());
        $request                  = $this->getRequest();
        $result                   = array();

        if ($request->isPost()) {
            $arrDados = $request->getPost()->toArray();

            if(!$arrDados['pes_id']){
                $objAcessoPessoas = $serviceAcessoPessoa->retornaUsuarioLogado();
                $arrDados['pes_id'] = $objAcessoPessoas->getPes()->getPes()->getPesId();
            }

            if($result = $servicebibliotecaVirtual->criarUrl($arrDados['pes_id'])){
                $result['pesId'] = $arrDados['pes_id'];
            }
        }

        return new JsonModel($result ? $result : array());
    }
}