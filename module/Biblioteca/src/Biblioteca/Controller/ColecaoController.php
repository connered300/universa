<?php

namespace Biblioteca\Controller;

use VersaSpine\Controller\AbstractCoreController;
use Zend\View\Model\JsonModel;

class ColecaoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request           = $this->getRequest();
        $paramsGet         = $request->getQuery()->toArray();
        $paramsPost        = $request->getPost()->toArray();
        $objServiceAssunto = new \Biblioteca\Service\Colecao($this->getEntityManager());

        $result = $objServiceAssunto->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $this->getView()->setVariable('urlColecaoAdd', $this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $objServiceColecao = new \Biblioteca\Service\Colecao($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $objServiceColecao->getJSONListingData($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);

            return $this->getJson();
        }

        return $this->getJson();
    }

    public function addAction($colecao = false)
    {
        $em         = $this->getEntityManager();
        $request    = $this->getRequest();
        $objColecao = new \Biblioteca\Service\Colecao($em);

        $dadosColecao = array();

        if ($colecao) {
            $dadosColecao = $objColecao->getArray($colecao);
        }

        if ($request->isPost()) {
            $dadosColecao = $request->getPost()->toArray();
            $dadosColecao = \VersaSpine\Stdlib\Util::sanitizeArray($dadosColecao);
            $salvar       = $objColecao->save($dadosColecao);
            $ajax         = false;

            if (isset($dadosColecao['ajax']) && $dadosColecao['ajax']) {
                $this->setView(new JsonModel());
                $ajax = true;
            }

            if ($salvar) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }
            } else {
                $this->getView()->setVariable("erro", array($objColecao->getLastError()));
            }
        }

        $objColecao->getArrayByPost($dadosColecao);

        $this->getView()->setVariable("dadosColecao", $dadosColecao);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $colecaoId = $this->params()->fromRoute("id", 0);

        if (!$colecaoId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($colecaoId);
    }
}