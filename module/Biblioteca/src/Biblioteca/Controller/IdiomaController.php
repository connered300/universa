<?php

namespace Biblioteca\Controller;

use VersaSpine\Controller\AbstractCoreController;
use Zend\View\Model\JsonModel;

class IdiomaController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request           = $this->getRequest();
        $paramsGet         = $request->getQuery()->toArray();
        $paramsPost        = $request->getPost()->toArray();
        $objServiceAssunto = new \Biblioteca\Service\Idioma($this->getEntityManager());

        $result = $objServiceAssunto->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $this->getView()->setVariable('urlIdiomaAdd', $this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $objServiceIdioma = new \Biblioteca\Service\Idioma($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $objServiceIdioma->getJSONListingData($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);

            return $this->getJson();
        }

        return $this->getJson();
    }

    public function addAction($idiomaId = false)
    {
        $em        = $this->getEntityManager();
        $request   = $this->getRequest();
        $objIdioma = new \Biblioteca\Service\Idioma($em);

        $dadosIdioma = array();

        if ($idiomaId) {
            $dadosIdioma = $objIdioma->getArray($idiomaId);
        }

        if ($request->isPost()) {
            $dadosIdioma = $request->getPost()->toArray();
            $dadosIdioma = \VersaSpine\Stdlib\Util::sanitizeArray($dadosIdioma);
            $salvar      = $objIdioma->save($dadosIdioma);
            $ajax        = false;

            if (isset($dadosIdioma['ajax']) && $dadosIdioma['ajax']) {
                $this->setView(new JsonModel());
                $ajax = true;
            }

            if ($salvar) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }
            } else {
                $this->getView()->setVariable("erro", array($objIdioma->getLastError()));
            }
        }

        $this->getView()->setVariable("dadosIdioma", $dadosIdioma);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $idiomaId = $this->params()->fromRoute("id", 0);

        if (!$idiomaId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($idiomaId);
    }
}