<?php

namespace Biblioteca\Controller;

use VersaSpine\Controller\AbstractCoreController;

class TituloController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
    }

    public function indexAction()
    {
        $objGrupoBibliografico  = new \Biblioteca\Service\GrupoBibliografico($this->getEntityManager());
        $jsonGrupoBibliografico = json_encode($objGrupoBibliografico->getArrSelect2());
        $this->getView()->setVariable('jsonGrupoBibliografico', $jsonGrupoBibliografico);

        return $this->getView();
    }

    public function consultaAction()
    {
        $objGrupoBibliografico  = new \Biblioteca\Service\GrupoBibliografico($this->getEntityManager());
        $jsonGrupoBibliografico = json_encode($objGrupoBibliografico->getArrSelect2());

        $this->getView()->setVariable('jsonGrupoBibliografico', $jsonGrupoBibliografico);
        $this->getView()->setVariable('permiteVisualizarAcervo', isset($_GET['visualizarAcervo']) ? 1 : 0);

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $objTitulo = new \Biblioteca\Service\Titulo($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $objTitulo->pesquisaForJson($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);

            return $this->getJson();
        }

        return $this->getJson();
    }

    public function addAction($tituloId = false)
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $arrDadosTitulo        = array();
        $objServiceTitulo      = new \Biblioteca\Service\Titulo($em);
        $objGrupoBibliografico = new \Biblioteca\Service\GrupoBibliografico($em);
        $objIdioma             = new \Biblioteca\Service\Idioma($em);
        $objSetor              = new \Biblioteca\Service\Setor($em);

        if ($tituloId) {
            $arrDadosTitulo = $objServiceTitulo->getArray($tituloId);

            if (empty($arrDadosTitulo)) {
                $this->flashMessenger()->addErrorMessage($objServiceTitulo->getLastError());

                return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
            }
        }

        if ($request->isPost()) {
            $arrDadosTitulo = array_merge(
                $arrDadosTitulo,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );

            $arrDadosTitulo['exemplares'] = json_decode($arrDadosTitulo['exemplares'], true);

            $arrDadosTitulo = \VersaSpine\Stdlib\Util::sanitizeArray($arrDadosTitulo);
            $salvar         = $objServiceTitulo->save($arrDadosTitulo);

            if ($salvar) {
                $mensagem = 'Título ' . $arrDadosTitulo['tituloId'] . ' salvo!';
                $mensagem .= ($arrDadosTitulo['exemplarInformation'] ? '<br>' . $arrDadosTitulo['exemplarInformation'] : '');

                $this->flashMessenger()->addSuccessMessage($mensagem);

                return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
            } else {
                $this->flashMessenger()->addErrorMessage($objServiceTitulo->getLastError());
            }
        }

        $objServiceTitulo->getArrayByPost($arrDadosTitulo);

        $jsonGrupoBibliografico = json_encode($objGrupoBibliografico->getArrSelect2());
        $jsonIdioma             = json_encode($objIdioma->getArrSelect2());
        $jsonSetor              = json_encode($objSetor->getArrSelect2(array('key' => 'value')));

        $this->getView()->setVariable('urlExemplarAdd', $this->getTemplateToRoute('add', 'exemplar'));
        $this->getView()->setVariable('jsonGrupoBibliografico', $jsonGrupoBibliografico);
        $this->getView()->setVariable('jsonIdioma', $jsonIdioma);
        $this->getView()->setVariable('jsonSetor', $jsonSetor);
        $this->getView()->setVariable('dadosTitulo', $arrDadosTitulo);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $identificador = $this->params()->fromRoute("id", 0);

        if (!$identificador) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($identificador);
    }

    public function searchTituloFisicaAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $objTitulo = new \Biblioteca\Service\Titulo($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $pesCpf        = $request->getPost('pesCpf');
            $pesNome       = $request->getPost('pesNome');
            $pesId         = $request->getPost('pesId');

            if ($pesCpf) {
                $arrTituloFisica = $objTitulo->getArrayTituloFisica(array('pesCpf' => $pesCpf));
            } elseif ($pesId) {
                $arrTituloFisica = $objTitulo->getArrayTituloFisica(array('pesId' => $pesId));
            } elseif ($pesNome) {
                $arrTituloFisica = $objTitulo->pesquisaTituloPorNome($pesNome);
            }

            if (empty($arrTituloFisica)) {
                $erro          = true;
                $erroDescricao = $objTitulo->getLastError();
            }

            $this->getJson()->setVariable('pessoaFisica', $arrTituloFisica);
            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }

    public function visualizarTituloAction()
    {
        $tituloId = $this->params()->fromRoute('id');

        if (!$tituloId) {
            $this->flashMessenger()->addErrorMessage("Título inválido!");

            return false;
        }

        $serviceArquivo   = new \GerenciadorArquivos\Service\Arquivo($this->getEntityManager());
        $objServiceTitulo = new \Biblioteca\Service\Titulo($this->getEntityManager());

        $arrDadosTitulo = $objServiceTitulo->getArray($tituloId);
        $msgErro        = '';

        if (!$arrDadosTitulo['arquivoDigital'] && !isset($arrDadosTitulo['arquivoDigital']['arqId'])) {
            $msgErro = "Arquivo não informado!";
        } else {

            /** @var $objArquivo \GerenciadorArquivos\Entity\Arquivo */
            $objArquivo = $serviceArquivo->getRepository()->findOneBy(
                ['arqId' => $arrDadosTitulo['arquivoDigital']['arqId']]
            );

            $validator = new \Zend\Validator\File\MimeType('application/pdf');

            if (!$validator->isValid($objArquivo->retornaCaminhoArquivo())) {
                $msgErro = 'Arquivo inválido!';
            }

            if (!$objArquivo) {
                $msgErro = 'Arquivo não localizado!';
            }
        }

        if (empty($arrDadosTitulo) || $msgErro) {
            $this->flashMessenger()->addErrorMessage(
                $objServiceTitulo->getLastError() ? $objServiceTitulo->getLastError() : $msgErro
            );

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        $chave = isset($arrDadosTitulo['arquivoDigital']['arqChave']) ? $arrDadosTitulo['arquivoDigital']['arqChave'] : null;
        $time  = date('Y-m-d H:i:s', strtotime('+10 minute'));
        $time  = base64_encode($time);

        $token = $chave . $time;

        $this->getView()->setTemplate('biblioteca/titulo/visualizar-titulo');
        $this->getView()->setVariable('token', $token);
        $this->getView()->setVariable('titulo', $arrDadosTitulo['tituloTitulo']);

        return $this->getView();
    }
}