<?php

namespace Biblioteca\Controller;

use VersaSpine\Controller\AbstractCoreController;
use Zend\View\Model\JsonModel;

class DoadorController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request           = $this->getRequest();
        $paramsGet         = $request->getQuery()->toArray();
        $paramsPost        = $request->getPost()->toArray();
        $objServiceDoador = new \Biblioteca\Service\Doador($this->getEntityManager());

        $result = $objServiceDoador->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new JsonModel($result ? $result : array());
    }
}