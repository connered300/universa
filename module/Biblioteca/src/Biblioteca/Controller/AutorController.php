<?php

namespace Biblioteca\Controller;

use VersaSpine\Controller\AbstractCoreController;
use Zend\View\Model\JsonModel;

class AutorController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request         = $this->getRequest();
        $paramsGet       = $request->getQuery()->toArray();
        $paramsPost      = $request->getPost()->toArray();
        $objServiceAutor = new \Biblioteca\Service\Autor($this->getEntityManager());

        $result = $objServiceAutor->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $this->getView()->setVariable('urlAutorAdd', $this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $objServiceAutor = new \Biblioteca\Service\Autor($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $objServiceAutor->getJSONListingData($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);

            return $this->getJson();
        }

        return $this->getJson();
    }

    public function addAction($autorId = false)
    {
        $em       = $this->getEntityManager();
        $request  = $this->getRequest();
        $objAutor = new \Biblioteca\Service\Autor($em);

        $dadosAutor = array();

        if ($autorId) {
            $dadosAutor = $objAutor->getArray($autorId);
        }

        if ($request->isPost()) {
            $dadosAutor = $request->getPost()->toArray();
            $dadosAutor = \VersaSpine\Stdlib\Util::sanitizeArray($dadosAutor);
            $salvar     = $objAutor->save($dadosAutor);
            $ajax       = false;

            if (isset($dadosAutor['ajax']) && $dadosAutor['ajax']) {
                $this->setView(new JsonModel());
                $ajax = true;
            }

            if ($salvar) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }
            } else {
                $this->getView()->setVariable("erro", array($objAutor->getLastError()));
            }
        }

        $objAutor->getArrayByPost($dadosAutor);

        $this->getView()->setVariable("dadosAutor", $dadosAutor);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $autorId = $this->params()->fromRoute("id", 0);

        if (!$autorId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($autorId);
    }
}