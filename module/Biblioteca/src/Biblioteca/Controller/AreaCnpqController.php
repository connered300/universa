<?php

namespace Biblioteca\Controller;

use VersaSpine\Controller\AbstractCoreController;
use Zend\View\Model\JsonModel;

class AreaCnpqController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request           = $this->getRequest();
        $paramsGet         = $request->getQuery()->toArray();
        $paramsPost        = $request->getPost()->toArray();
        $objServiceAssunto = new \Biblioteca\Service\AreaCnpq($this->getEntityManager());

        $result = $objServiceAssunto->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $this->getView()->setVariable('urlAreaCnpqAdd', $this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $objServiceAreaCnpq = new \Biblioteca\Service\AreaCnpq($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $objServiceAreaCnpq->getJSONListingData($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);

            return $this->getJson();
        }

        return $this->getJson();
    }

    public function addAction($areaCnpqId = false)
    {
        $em          = $this->getEntityManager();
        $request     = $this->getRequest();
        $objAreaCnpq = new \Biblioteca\Service\AreaCnpq($em);

        $dadosAreaCnpq = array();

        if ($areaCnpqId) {
            $dadosAreaCnpq = $objAreaCnpq->getArray($areaCnpqId);
        }

        if ($request->isPost()) {
            $dadosAreaCnpq = $request->getPost()->toArray();
            $dadosAreaCnpq = \VersaSpine\Stdlib\Util::sanitizeArray($dadosAreaCnpq);
            $salvar        = $objAreaCnpq->save($dadosAreaCnpq);
            $ajax          = false;

            if (isset($dadosAreaCnpq['ajax']) && $dadosAreaCnpq['ajax']) {
                $this->setView(new JsonModel());
                $ajax = true;
            }

            if ($salvar) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }
            } else {
                $this->getView()->setVariable("erro", array($objAreaCnpq->getLastError()));
            }
        }

        $this->getView()->setVariable("dadosAreaCnpq", $dadosAreaCnpq);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $areaId = $this->params()->fromRoute("id", 0);

        if (!$areaId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($areaId);
    }
}