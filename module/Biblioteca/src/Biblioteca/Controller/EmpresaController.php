<?php

namespace Biblioteca\Controller;

use VersaSpine\Controller\AbstractCoreController;
use Zend\View\Model\JsonModel;

class EmpresaController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request           = $this->getRequest();
        $paramsGet         = $request->getQuery()->toArray();
        $paramsPost        = $request->getPost()->toArray();
        $objServiceEmpresa = new \Biblioteca\Service\Empresa($this->getEntityManager());

        $result = $objServiceEmpresa->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceEmpresa = new \Biblioteca\Service\Empresa($this->getEntityManager());

        $serviceEmpresa->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $objEmpresa = new \Biblioteca\Service\Empresa($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $objEmpresa->getJSONListingData($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);

            return $this->getJson();
        }

        return $this->getJson();
    }

    public function addAction($alunoId = false)
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $arrDadosEmpresa   = array();
        $objServiceEmpresa = new \Biblioteca\Service\Empresa($em);

        if ($alunoId) {
            $arrDadosEmpresa = $objServiceEmpresa->getArray($alunoId);

            if (empty($arrDadosEmpresa)) {
                $this->flashMessenger()->addErrorMessage($objServiceEmpresa->getLastError());

                return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
            }
        }

        if ($request->isPost()) {
            $arrDadosEmpresa = $request->getPost()->toArray();
            $arrDadosEmpresa = \VersaSpine\Stdlib\Util::sanitizeArray($arrDadosEmpresa);
            $salvar          = $objServiceEmpresa->save($arrDadosEmpresa);

            if ($salvar) {
                $this->flashMessenger()->addSuccessMessage('Empresa salva!');

                return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
            } else {
                $this->flashMessenger()->addErrorMessage($objServiceEmpresa->getLastError());
            }
        }

        $serviceEmpresa = new \Biblioteca\Service\Empresa($this->getEntityManager());
        $serviceEmpresa->setarDependenciasView($this->getView());

        $this->getView()->setVariable('dadosEmpresa', $arrDadosEmpresa);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $identificador = $this->params()->fromRoute("id", 0);

        if (!$identificador) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($identificador);
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $objServiceEmpresa = new \Biblioteca\Service\Empresa($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $objServiceEmpresa->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $objServiceEmpresa->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);

            return $this->getJson();
        }

        return $this->getJson();
    }
}