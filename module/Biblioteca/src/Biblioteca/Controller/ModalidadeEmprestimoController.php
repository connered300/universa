<?php

namespace Biblioteca\Controller;

use VersaSpine\Controller\AbstractCoreController;

class ModalidadeEmprestimoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
    }

    public function indexAction()
    {
        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $objModalidadeEmprestimo = new \Biblioteca\Service\ModalidadeEmprestimo($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $objModalidadeEmprestimo->pesquisaForJson($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);

            return $this->getJson();
        }

        return $this->getJson();
    }

    public function addAction($alunoId = false)
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $arrDadosModalidadeEmprestimo   = array();
        $objServiceModalidadeEmprestimo = new \Biblioteca\Service\ModalidadeEmprestimo($em);
        $objGrupoBibliografico          = new \Biblioteca\Service\GrupoBibliografico($em);
        $objGrupoLeitor                 = new \Biblioteca\Service\GrupoLeitor($em);

        if ($alunoId) {
            $arrDadosModalidadeEmprestimo = $objServiceModalidadeEmprestimo->getArray($alunoId);
        }

        if ($request->isPost()) {
            $arrDadosModalidadeEmprestimo = $request->getPost()->toArray();
            $arrDadosModalidadeEmprestimo = \VersaSpine\Stdlib\Util::sanitizeArray($arrDadosModalidadeEmprestimo);
            $salvar                       = $objServiceModalidadeEmprestimo->save($arrDadosModalidadeEmprestimo);

            if ($salvar) {
                $this->flashMessenger()->addSuccessMessage('Modalidade de empréstimo salva!');

                return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
            } else {
                $this->flashMessenger()->addErrorMessage($objServiceModalidadeEmprestimo->getLastError());
            }
        }

        $objServiceModalidadeEmprestimo->getArrayByPost($arrDadosModalidadeEmprestimo);

        $arrGrupoLeitor         = $objGrupoLeitor->pagination()['dados']->fetchAll();
        $jsonGrupoBibliografico = json_encode($objGrupoBibliografico->getArrSelect2());

        if ($arrDadosModalidadeEmprestimo['modalidadeEmprestimoGrupo']) {
            $arrDadosModalidadeEmprestimo['modalidadeEmprestimoGrupo'] = json_encode(
                $arrDadosModalidadeEmprestimo['modalidadeEmprestimoGrupo']
            );
        }

        $this->getView()->setVariable('dadosModalidadeEmprestimo', $arrDadosModalidadeEmprestimo);
        $this->getView()->setVariable('jsonGrupoBibliografico', $jsonGrupoBibliografico);
        $this->getView()->setVariable('grupoLeitor', $arrGrupoLeitor);

        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $identificador = $this->params()->fromRoute("id", 0);

        if (!$identificador) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($identificador);
    }
}