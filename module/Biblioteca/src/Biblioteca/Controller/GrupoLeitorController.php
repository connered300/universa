<?php

namespace Biblioteca\Controller;

use VersaSpine\Controller\AbstractCoreController;
use Zend\View\Model\JsonModel;

class GrupoLeitorController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function modalidadesAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $objGrupoLeitor = new \Biblioteca\Service\GrupoLeitor($this->getEntityManager());
            $erro           = false;
            $erroDescricao  = '';
            $grupoLeitorId  = $request->getPost('grupoLeitorId');
            $arrModalidades = $objGrupoLeitor->getModalidadesGrupo($grupoLeitorId);

            if (empty($arrModalidades)) {
                $erro          = true;
                $erroDescricao = $objGrupoLeitor->getLastError();
            }

            $this->getJson()->setVariable('modalidades', $arrModalidades);
            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }

    public function searchForJsonAction()
    {
        $request            = $this->getRequest();
        $paramsGet          = $request->getQuery()->toArray();
        $paramsPost         = $request->getPost()->toArray();
        $serviceGrupoLeitor = new \Biblioteca\Service\GrupoLeitor($this->getEntityManager());

        $result = $serviceGrupoLeitor->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceGrupoLeitor = new \Biblioteca\Service\GrupoLeitor($this->getEntityManager());
        $serviceGrupoLeitor->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceGrupoLeitor = new \Biblioteca\Service\GrupoLeitor($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceGrupoLeitor->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function addAction($grupoLeitorId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados           = array();
        $serviceGrupoLeitor = new \Biblioteca\Service\GrupoLeitor($this->getEntityManager());

        if ($grupoLeitorId) {
            $arrDados = $serviceGrupoLeitor->getArray($grupoLeitorId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceGrupoLeitor->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de grupo leitor salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceGrupoLeitor->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceGrupoLeitor->getLastError());
                }
            }
        }

        $serviceGrupoLeitor->formataDadosPost($arrDados);
        $serviceGrupoLeitor->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $grupoLeitorId = $this->params()->fromRoute("id", 0);

        if (!$grupoLeitorId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($grupoLeitorId);
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceGrupoLeitor = new \Biblioteca\Service\GrupoLeitor($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceGrupoLeitor->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceGrupoLeitor->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}