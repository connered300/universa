<?php

namespace Biblioteca\Controller;

use VersaSpine\Controller\AbstractCoreController;
use Zend\View\Model\JsonModel;

class AreaController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request        = $this->getRequest();
        $paramsGet      = $request->getQuery()->toArray();
        $paramsPost     = $request->getPost()->toArray();
        $objServiceArea = new \Biblioteca\Service\Area($this->getEntityManager());

        $result = $objServiceArea->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $this->getView()->setVariable('urlAreaAdd', $this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $objServiceArea = new \Biblioteca\Service\Area($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $objServiceArea->getJSONListingData($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);

            return $this->getJson();
        }

        return $this->getJson();
    }

    public function addAction($areaId = false)
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();
        $objArea = new \Biblioteca\Service\Area($em);

        $dadosArea = array();

        if ($areaId) {
            $dadosArea = $objArea->getArray($areaId);
        }

        if ($request->isPost()) {
            $dadosArea = $request->getPost()->toArray();
            $dadosArea = \VersaSpine\Stdlib\Util::sanitizeArray($dadosArea);
            $salvar    = $objArea->save($dadosArea);
            $ajax      = false;

            if (isset($dadosArea['ajax']) && $dadosArea['ajax']) {
                $this->setView(new JsonModel());
                $ajax = true;
            }

            if ($salvar) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }
            } else {
                $this->getView()->setVariable("erro", array($objArea->getLastError()));
            }
        }

        $this->getView()->setVariable("dadosArea", $dadosArea);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $areaId = $this->params()->fromRoute("id", 0);

        if (!$areaId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($areaId);
    }
}