<?php

namespace Biblioteca;

return array(
    'router'                    => array(
        'routes' => array(
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'biblioteca' => array(
                'type'          => 'Literal',
                'options'       => array(
                    'route'    => '/biblioteca',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Biblioteca\Controller',
                        'controller'    => 'Emprestimo',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes'  => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'       => '/[:controller[/:action[/:id]]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults'    => array(),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager'           => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases'            => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'doctrine'                  => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default'             => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            ),
        ),
    ),
    'controllers'               => array(
        'invokables' => array(
            'Biblioteca\Controller\Autor'                => 'Biblioteca\Controller\AutorController',
            'Biblioteca\Controller\ModalidadeEmprestimo' => 'Biblioteca\Controller\ModalidadeEmprestimoController',
            'Biblioteca\Controller\Empresa'              => 'Biblioteca\Controller\EmpresaController',
            'Biblioteca\Controller\AreaCnpq'             => 'Biblioteca\Controller\AreaCnpqController',
            'Biblioteca\Controller\Area'                 => 'Biblioteca\Controller\AreaController',
            'Biblioteca\Controller\Pessoa'               => 'Biblioteca\Controller\PessoaController',
            'Biblioteca\Controller\Assunto'              => 'Biblioteca\Controller\AssuntoController',
            'Biblioteca\Controller\Cutter'               => 'Biblioteca\Controller\CutterController',
            'Biblioteca\Controller\Idioma'               => 'Biblioteca\Controller\IdiomaController',
            'Biblioteca\Controller\Colecao'              => 'Biblioteca\Controller\ColecaoController',
            'Biblioteca\Controller\Setor'                => 'Biblioteca\Controller\SetorController',
            'Biblioteca\Controller\Titulo'               => 'Biblioteca\Controller\TituloController',
            'Biblioteca\Controller\Exemplar'             => 'Biblioteca\Controller\ExemplarController',
            'Biblioteca\Controller\Emprestimo'           => 'Biblioteca\Controller\EmprestimoController',
            'Biblioteca\Controller\GrupoBibliografico'   => 'Biblioteca\Controller\GrupoBibliograficoController',
            'Biblioteca\Controller\GrupoLeitor'          => 'Biblioteca\Controller\GrupoLeitorController',
            'Biblioteca\Controller\Suspensao'            => 'Biblioteca\Controller\SuspensaoController',
            'Biblioteca\Controller\Multa'                => 'Biblioteca\Controller\MultaController',
            'Biblioteca\Controller\Relatorio'            => 'Biblioteca\Controller\RelatorioController',
            'Biblioteca\Controller\Doador'               => 'Biblioteca\Controller\DoadorController',
            'Biblioteca\Controller\BibliotecaVirtual'    => 'Biblioteca\Controller\BibliotecaVirtualController',
        ),
    ),
    'view_manager'              => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'console'                   => array(
        'router' => array(
            'routes' => array(),
        ),
    ),
    'funcionalidades_liberadas' => array(
        'controllers' => array(
            'Biblioteca\Controller\GrupoLeitor',
        ),
        'actions'     => array(
            'barcode',
            'modalidades',
            'informacoes-basicas',
            'devolucao',
            'historico',
            'consulta',
            'search',
            'comprovante',
            'visualizar-titulo',
            'comprovante'
        ),
    ),
    'namesDictionary'           => array(
        'Biblioteca'                                           => 'Biblioteca',
        'Biblioteca\Autor'                                     => 'Autor',
        'Biblioteca\Autor::index'                              => 'Listagem',
        'Biblioteca\ModalidadeEmprestimo'                      => 'Modalidade de Empréstimo',
        'Biblioteca\GrupoBibliografico'                        => 'Grupo Bibliográfico',
        'Biblioteca\GrupoLeitor'                               => 'Grupo de Leitor',
        'Biblioteca\Empresa'                                   => 'Editoras e Fornecedores',
        'Biblioteca\Pessoa'                                    => 'Pessoas e Leitores',
        'Biblioteca\Area'                                      => 'Área de conhecimento',
        'Biblioteca\AreaCnpq'                                  => 'Área de conhecimento CNPq',
        'Biblioteca\Titulo'                                    => 'Título',
        'Biblioteca\Titulo::visualizar-titulo'                 => 'Leitor de Título',
        'Biblioteca\Titulo::consulta'                          => '',
        'Biblioteca\Colecao'                                   => 'Coleção',
        'Biblioteca\Emprestimo'                                => 'Empréstimo',
        'Biblioteca\Emprestimo::index'                         => 'Efetuar empréstimos',
        'Biblioteca\Relatorio'                                 => 'Relatórios',
        'Biblioteca\Relatorio::index'                          => 'Relatórios disponíveis',
        'Biblioteca\Relatorio::exemplar-etiqueta'              => 'Etiquetas de lombada',
        'Biblioteca\Relatorio::exemplar-etiqueta-codigo-barra' => 'Etiquetas de código de barras de exemplares',
        'Biblioteca\Relatorio::exemplar-baixa'                 => 'Exemplares baixados',
        'Biblioteca\Relatorio::titulo-ranking'                 => 'Ranking de empréstimo de títulos',
        'Biblioteca\Suspensao'                                 => 'Suspensão',
    )
);
