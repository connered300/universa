<?php

namespace Vestibular\Service;

use VersaSpine\Service\AbstractService;

class SelecaoTipo extends AbstractService
{
    const TIPOSEL_PROVA_SIM = 'Sim';
    const TIPOSEL_PROVA_NAO = 'Não';

    const SELECAOTIPO_VESTIBULAR              = 1;
    const SELECAOTIPO_NOTAS_DO_ENEM           = 2;
    const SELECAOTIPO_ENEM_E_VESTIBULAR       = 3;
    const SELECAOTIPO_OBTENÇÃO_DE_NOVO_TÍTULO = 4;
    const SELECAOTIPO_NÃO_INFORMADO           = 5;
    const SELECAOTIPO_REEINGRESSO             = 6;
    const SELECAOTIPO_TRANSFERÊNCIA           = 7;

    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    public function getArrSelect2TiposelProva($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getTiposelProva());
    }

    public static function getTiposelProva()
    {
        return array(self::TIPOSEL_PROVA_SIM, self::TIPOSEL_PROVA_NAO);
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Vestibular\Entity\SelecaoTipo');
    }

    public function pesquisaForJson($params)
    {
        $sql         = 'SELECT * FROM selecao_tipo WHERE';
        $tiposelNome = false;
        $tiposelId   = false;

        if ($params['q']) {
            $tiposelNome = $params['q'];
        } elseif ($params['query']) {
            $tiposelNome = $params['query'];
        }

        if ($params['tiposelId']) {
            $tiposelId = $params['tiposelId'];
        }

        $parameters = array('tiposel_nome' => "{$tiposelNome}%");
        $sql .= ' tiposel_nome LIKE :tiposel_nome';

        if ($tiposelId) {
            $parameters['tiposel_id'] = $tiposelId;
            $sql .= ' AND tiposel_id <> :tiposel_id';
        }

        $sql .= " ORDER BY tiposel_nome";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['tiposelId']) {
                $objSelecaoTipo = $this->getRepository()->find($arrDados['tiposelId']);

                if (!$objSelecaoTipo) {
                    $this->setLastError('Registro de tipo não existe!');

                    return false;
                }
            } else {
                $objSelecaoTipo = new \Vestibular\Entity\SelecaoTipo();
            }

            $objSelecaoTipo->setTiposelNome($arrDados['tiposelNome']);
            $objSelecaoTipo->setTiposelProva($arrDados['tiposelProva']);

            $this->getEm()->persist($objSelecaoTipo);
            $this->getEm()->flush();

            $this->getEm()->commit();

            $arrDados['tiposelId'] = $objSelecaoTipo->getTiposelId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de tipo!');
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['tiposelNome']) {
            $errors[] = 'Por favor preencha o campo "Nome"!';
        }

        if (!$arrParam['tiposelProva']) {
            $errors[] = 'Por favor preencha o campo "Prova"!';
        }

        if (!in_array($arrParam['tiposelProva'], self::getTiposelProva())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "Prova"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM selecao_tipo";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($tiposelId)
    {
        $arrDados = $this->getRepository()->find($tiposelId);

        try {
            $arrDados = $arrDados->toArray();
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    public function remover($param)
    {
        if (!$param['tiposelId']) {
            $this->setLastError('Para remover um registro de tipo é necessário especificar o código.');

            return false;
        }

        try {
            $objSelecaoTipo = $this->getRepository()->find($param['tiposelId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objSelecaoTipo);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de tipo de seleção.');

            return false;
        }

        return true;
    }

    public function mountArrayTipo()
    {
        $sql       = "
        SELECT u
        FROM \Vestibular\Entity\SelecaoTipo u
        WHERE u.tiposelNome != 'Não Informado'
        ORDER BY u.tiposelNome ASC";
        $arrayTipo = array();

        try {
            $query = $this->getEm()->createQuery($sql);
            $tipos = $query->getResult();

            //            $tipos = $this->getEm()->getRepository("Vestibular\Entity\SelecaoTipo")->findAll();
            foreach ($tipos as $tipo) {
                $arrayTipo[$tipo->getTiposelId()] = $tipo->getTiposelNome();
            }
        } catch (\Exception $ex) {
            die($ex->getMessage());
        }

        return $arrayTipo;
    }

    public function getArrayObjTipo()
    {
        $sql       = "
        SELECT u
        FROM \Vestibular\Entity\SelecaoTipo u
        WHERE u.tiposelNome != 'Não Informado'
        ORDER BY u.tiposelNome ASC";
        $arrayTipo = array();

        try {
            $query     = $this->getEm()->createQuery($sql);
            $arrayTipo = $query->getResult();
        } catch (\Exception $ex) {
            die($ex->getMessage());
        }

        return $arrayTipo;
    }

    public function getArrSelect()
    {
        $arrEntities = $this->getRepository()->findBy([], ['tiposelNome' => 'asc']);

        $arrEntitiesArr = array();

        /* @var $objEntity \Vestibular\Entity\SelecaoTipo */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[$objEntity->getTiposelId()] = $objEntity->getTiposelNome();
        }

        return $arrEntitiesArr;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('tiposelId' => $params['id']));
        } elseIf ($params['text']) {
            $arrEntities = $this->getRepository()->findBy(array('tiposelNome' => $params['text']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();

        /* @var $objEntity \Vestibular\Entity\SelecaoTipo */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getTiposelId(),
                $params['value'] => $objEntity->getTiposelNome()
            );
        }

        return $arrEntitiesArr;
    }
}