<?php

namespace Vestibular\Service;

use VersaSpine\Service\AbstractService;

class SelecaoEdicaoFechamento extends AbstractService
{
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Vestibular\Entity\SelecaoEdicaoFechamento');
    }

    public function pesquisaForJson($criterios){
        //TODO
    }

    public function valida($dados){
        //TODO
    }

    public function encerraVestibular($arrDados)
    {
        try {
            $this->getEm()->beginTransaction();
            $objFechamento = new \Vestibular\Entity\SelecaoEdicaoFechamento();
            $serviceAcesso = new \Acesso\Service\AcessoPessoas($this->getEm());
            $serviceEdicao = new \Vestibular\Service\SelecaoEdicao($this->getEm());

            if($arrDados['edicao']){
                $serviceInscricao = new \Vestibular\Service\SelecaoInscricao($this->getEm());
                $objEdicao = $serviceEdicao->getRepository()->find($arrDados['edicao']);
                $serviceInscricao->atualizaClassificacao($objEdicao->getEdicaoId());
                $objFechamento->setEdicao($objEdicao);
            }

            if($arrDados['usuario']){
                $objUsuario = $serviceAcesso->getRepository()->find($arrDados['usuario']);
                $objFechamento->setUsuarioFechamento($objUsuario);
            }

            if($arrDados['classificados']){
                $objFechamento->setClassificacaoLimite($arrDados['classificados']);
            }

            $data = new \DateTime('now');
            $data = $data->format('Y-m-d H:i:s');

            $objFechamento->setSelecaoFechamentoData(new \DateTime('now'));

            $this->getEm()->persist($objFechamento);
            $this->getEm()->flush($objFechamento);
            $this->getEm()->commit();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Problemas all salvar os dados!');
        }

        return false;
    }
}