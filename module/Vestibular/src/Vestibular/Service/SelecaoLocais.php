<?php

namespace Vestibular\Service;

use VersaSpine\Service\AbstractService;

class SelecaoLocais extends AbstractService
{
    private $__lastError = null;

    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Vestibular\Entity\SelecaoLocais');
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    public function pesquisaForJson($criterios)
    {
        $param = [];
        $where = [];
        $sql   = "
        SELECT 
              selecao_locais.*,
              COALESCE(unidade_end_logradouro,predio_logradouro) endLogradouro,COALESCE(unidade_end_numero,predio_numero) endNumero ,
              COALESCE(unidade_end_cep,predio_cep) endCep ,COALESCE(unidade_end_bairro,predio_bairro) endBairro, 
              COALESCE(unidade_end_cidade,predio_cidade) endCidade,COALESCE(unidade_end_estado,predio_estado)endEstado,
              COALESCE(unidade_telefone,'') telefone ,COALESCE(unidade_nome,predio_nome) nome

        FROM selecao_locais 
            LEFT JOIN org__unidade_estudo ON selecao_locais.unidade_id=org__unidade_estudo.unidade_id
            LEFT JOIN infra_predio  ON selecao_locais.predio_id=infra_predio.predio_id           
        ";

        if ($criterios['edicaoId']) {
            $where[]         = 'selecao_locais.edicao_id IN ( :edicao )';
            $param['edicao'] = $criterios['edicaoId'];
        }

        if ($criterios['somenteUnidade']) {
            $where[] = ' selecao_locais.unidade_id IS NOT NULL';
        }

        if (!empty($where)) {
            $where = implode(" AND ", $where);
            $sql .= ' WHERE ' . $where;
        }

        if($criterios['limit']){
            $sql.=" limit ".$criterios['limit'];
        }

        $retorno = $this->executeQueryWithParam($sql, $param)->fetchAll();

        return $retorno;
    }

    public function retornaSelect2($arrDados, $unidadePredio)
    {
        $servicePessoa = new\Pessoa\Service\Pessoa($this->getEm());

        if (!$arrDados) {
            $this->setLastError("Nenhum dado informado!");

            return [];
        }

        $arrRetorno = [];

        foreach ($arrDados as $value) {
            $id=$value['selelocais_id'];
            $tipo   = isset($value['predio_id']) ? "Prédio: " : " Unidade: ";

            if ($unidadePredio) {
                /* Tratamento, pois trabalhamos com duas tabelas distintas predio e unidade .*/
                $id = $tipo == "Prédio: " ? 'p' . $value['predio_id'] : 'u' . $value['unidade_id'];
            }

            $arrRetorno[] = [
                'id'   => $id,
                'text' => $tipo . $value['nome'] . ' | ' . $servicePessoa->formatarEndereco($value)
            ];
        }

        return $arrRetorno;
    }

    public function retornaTodosLocais()
    {
        $servicePredio        = new \Infraestrutura\Service\InfraPredio($this->getEm());
        $serviceUnidade       = new \Organizacao\Service\OrgUnidadeEstudo($this->getEm());
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());
        $param                = array();

        try{
            $arrCampus = array();
            $arrObjCampusCurso = $serviceAcessoPessoas->retornaCampusCursoDoUsuarioLogado();

            /** @var $objCampusCurso \Matricula\Entity\CampusCurso */
            foreach ($arrObjCampusCurso as $objCampusCurso ){
                $arrCampus[] = $objCampusCurso->getCamp()->getCampId();
            }

            $param['campus'] = implode(',', $arrCampus);
        }catch(\Exception $ex){

        }

        $arrPredios  = $servicePredio->pesquisaForJson($param);
        $arrUnidades = $serviceUnidade->pesquisaForJson($param);

        $arrLocais = array_merge($arrUnidades, $arrPredios);

        return $arrLocais;
    }
}