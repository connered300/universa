<?php

namespace Vestibular\Service;

use VersaSpine\Service\AbstractService;

/**
 * Description of SelecaoInscricaoService
 *
 * @author versa
 */
class SelecaoInscricao extends AbstractService
{
    const INSCRICAO_STATUS_SOLICITADA = 'Solicitada';
    const INSCRICAO_STATUS_ACEITA = 'Aceita';
    const INSCRICAO_STATUS_INCOMPLETA = 'Incompleta';
    const INSCRICAO_RESULTADO_APROVADO = 'aprovado';
    const INSCRICAO_RESULTADO_DESCLASSIFICADO = 'desclassificado';
    const INSCRICAO_RESULTADO_AUSENTE = 'ausente';
    const INSCRICAO_RESULTADO_EXCEDENTE = 'excedente';
    const INSCRICAO_ENTREGA_DOC_SIM = 'Sim';
    const INSCRICAO_ENTREGA_DOC_NAO = 'Não';

    private $lastError = null;

    public $linkLoginCadidato = '';

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    public function getArrSelect2InscricaoStatus($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getInscricaoStatus());
    }

    public static function getInscricaoStatus()
    {
        return array(
            self::INSCRICAO_STATUS_SOLICITADA,
            self::INSCRICAO_STATUS_ACEITA,
            self::INSCRICAO_STATUS_INCOMPLETA
        );
    }

    public function getArrSelect2InscricaoResultado($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getInscricaoResultado());
    }

    public static function getInscricaoResultado()
    {
        return array(
            self::INSCRICAO_RESULTADO_APROVADO,
            self::INSCRICAO_RESULTADO_DESCLASSIFICADO,
            self::INSCRICAO_RESULTADO_AUSENTE,
            self::INSCRICAO_RESULTADO_EXCEDENTE
        );
    }

    public function getArrSelect2InscricaoEntregaDoc($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getInscricaoEntregaDoc());
    }

    public static function getInscricaoEntregaDoc()
    {
        return array(self::INSCRICAO_ENTREGA_DOC_SIM, self::INSCRICAO_ENTREGA_DOC_NAO);
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Vestibular\Entity\SelecaoInscricao');
    }

    public function pesquisaForJson($params)
    {
        $sql         = '
        SELECT * FROM selecao_inscricao
        NATURAL JOIN pes_nome
        WHERE';
        $pesNome     = false;
        $inscricaoId = false;

        if ($params['q']) {
            $pesNome = $params['q'];
        } elseif ($params['query']) {
            $pesNome = $params['query'];
        }

        if ($params['inscricaoId']) {
            $inscricaoId = $params['inscricaoId'];
        }

        $parameters = array('pes_nome' => "{$pesNome}%");
        $sql .= ' pes_nome LIKE :pes_nome';

        if ($inscricaoId) {
            $parameters['inscricao_id'] = $inscricaoId;
            $sql .= ' AND inscricao_id <> :inscricao_id';
        }

        $sql .= " ORDER BY inscricao_entrega_doc";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['edicaoId']) {
            $errors[] = 'Por favor preencha o campo "edição"!';
        }

        if (!$arrParam['inscricaoStatus']) {
            $errors[] = 'Por favor preencha o campo "status"!';
        }

        if (!$arrParam['inscricaoLinguaEstrangeira']) {
            $errors[] = 'Por favor preencha o campo "Lingua Estrangeira"!';
        }

        if (!$arrParam['inscricaoCursos']) {
            $errors[] = 'Por favor preencha o campo "Curso"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $serviceSelecaoEdicao = new \Vestibular\Service\SelecaoEdicao($this->getEm());

        $query = "
            SELECT 
                selecao_inscricao.inscricao_id AS inscricao_id,
                edicao_descricao,
                pes_nome,
                curso_nome AS curso,
                inscricao_status,
                inscricao_lingua_estrangeira,
                IF(inscricao_data_horario_prova IS NOT NULL,
                    'Sim',
                    'Não') AS prova_agendada,
                IF(inscricao_data_horario_prova IS NOT NULL,
                    DATE_FORMAT(inscricao_data_horario_prova, '%d/%m/%Y'),
                    DATE_FORMAT(realizacao_data, '%d/%m/%Y')) AS data_prova,
                IF(inscricao_data_horario_prova IS NOT NULL,
                    DATE_FORMAT(inscricao_data_horario_prova, '%H:%i'),
                    DATE_FORMAT(realizacao_data, '%H:%i')) AS hora_prova,
                (SELECT 
                        bol_id
                    FROM
                        financeiro__titulo
                            LEFT JOIN
                        financeiro__titulo_tipo ON financeiro__titulo_tipo.tipotitulo_id = financeiro__titulo.tipotitulo_id
                            LEFT JOIN
                        boleto ON financeiro__titulo.titulo_id = boleto.titulo_id
                    WHERE
                        pessoa.pes_id = financeiro__titulo.pes_id
                            AND financeiro__titulo_tipo.tipotitulo_nome = 'Vestibular') financeiro_titulo
            FROM
                selecao_inscricao
                    LEFT JOIN
                inscricao_cursos ON inscricao_cursos.inscricao_id = selecao_inscricao.inscricao_id
                    LEFT JOIN
                selecao_cursos ON selecao_cursos.selcursos_id = inscricao_cursos.selcursos_id
                    LEFT JOIN
                campus_curso ON campus_curso.cursocampus_id = selecao_cursos.cursocampus_id
                    LEFT JOIN
                acad_curso ON acad_curso.curso_id = campus_curso.curso_id
                    LEFT JOIN
                selecao_edicao ON selecao_edicao.edicao_id = selecao_inscricao.edicao_id
                    LEFT JOIN
                selecao_data_realizacao ON selecao_data_realizacao.edicao_id = selecao_edicao.edicao_id
                    LEFT JOIN
                pessoa ON pessoa.pes_id = selecao_inscricao.pes_id
            WHERE
                selecao_inscricao.edicao_id = (SELECT 
                        MAX(edicao_id)
                    FROM
                        selecao_edicao)
            GROUP BY inscricao_id
        ";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($inscricaoId)
    {
        if (!is_object($inscricaoId)) {
            $inscricaoId = $inscricaoId * 1;
        }

        /** @var $objSelecaoInscricao \Vestibular\Entity\SelecaoInscricao */
        $objSelecaoInscricao       = $this->getRepository()->find($inscricaoId);
        $arrDados                  = array();
        $serviceAcessoPessoas      = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceInscricaoCursos    = new \Vestibular\Service\InscricaoCursos($this->getEm());
        $serviceSelecaoEdicao      = new \Vestibular\Service\SelecaoEdicao($this->getEm());
        $servicePessoaFisica       = new \Pessoa\Service\PessoaFisica($this->getEm());
        $serviceSelecaoTipoEdicao  = new \Vestibular\Service\SelecaoTipoEdicao($this->getEm());
        $serviceInfraSala          = new \Infraestrutura\Service\InfraSala($this->getEm());
        $serviceTitulo             = new \Boleto\Service\FinanceiroTitulo($this->getEm());
        $serviceBoleto             = new \Boleto\Service\Boleto($this->getEm());
        $servicePessoaNecessidades = new \Pessoa\Service\PessoaNecessidades($this->getEm());

        try {
            $arrDados = $objSelecaoInscricao->toArray();
            $arrDados = array_merge(
                $arrDados,
                $servicePessoaFisica->getArray($objSelecaoInscricao->getPes()->getPesId())
            );

            $inscricaoId = $objSelecaoInscricao->getInscricaoId();
            $pesId       = $objSelecaoInscricao->getPes()->getPesId();
            $objEdicao   = $objSelecaoInscricao->getEdicao();
            $arrTitulo   = $serviceTitulo->buscaTituloPessoa($pesId, 'Vestibular', null, $arrDados['inscricaoData']);
            $arrTitulo   = $arrTitulo ? $arrTitulo[0]->toArray() : array();
            $arrAcesso   = $serviceAcessoPessoas->buscaUsuario($pesId);
            $arrAcesso   = $arrAcesso ? $arrAcesso[0] : array();
            $arrBoleto   = $arrTitulo['tituloId'] ? $serviceBoleto->buscaBoletoPorTitulo($arrTitulo['tituloId']) : [];
            $arrBoleto   = $arrBoleto ? $arrBoleto[0]->toArray() : array();

            $arrDados['vestibularFinalizado'] = $serviceSelecaoEdicao->verificaVestibularFinalizado($objEdicao);
            $arrDados['inscricaoCursos']      = $serviceInscricaoCursos->getArrayInscricaoCurso($inscricaoId);
            $arrDados['tituloFinanceiro']     = $arrTitulo;
            $arrDados['boleto']               = $arrBoleto;
            $arrDados['pessoaNecessidades']   = $servicePessoaNecessidades->getArrayPessoaNecessidades($pesId);
            $arrDados['acesso']               = $arrAcesso;
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    public function formataDadosPost(&$arrDados)
    {
        $serviceSelecaoLocais = new \Vestibular\Service\SelecaoLocais($this->getEm());

        if (is_object($arrDados['selelocais'])) {
            $selelocais = $serviceSelecaoLocais->getArraySelecaoLocais(
                $arrDados['edicaoId'],
                $arrDados['selelocais']->getSelelocaisId()
            );

            $arrDados['selelocais'] = $selelocais ? $selelocais[0] : array();
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('inscricaoId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Vestibular\Entity\SelecaoInscricao */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getInscricaoId(),
                $params['value'] => $objEntity->getPes()->getPesNome()
            );
        }

        return $arrEntitiesArr;
    }

    public function SelectNecessidades()
    {
    }

    /*
     * Função: buscaContatoPessoa
     * Parametros: Id da pessoa e tipo do contato;
     * Retorno: Array com todos os contatos daquele tipo relacionado a pessoa.
     */
    public function verificaUsuarioGrupo($id, $nome)
    {
        $grupo = $this->getEm()->getRepository('Acesso\Entity\AcessoGrupo')->findOneBy(array('grupNome' => $nome));
        if ($grupo) {
            $id_grupo = $grupo->getId();
            $sql      = "SELECT * FROM "
                . "`acesso_usuarios_grupos` "
                . "WHERE grupo_id = '{$id_grupo}' && usuario_id = '{$id}' && us_ativo = 'sim'";
            $result   = $this->executeQuery($sql)->fetchAll();
            if (count($result) > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function buscaContatoPessoa($id, $tipo)
    {
        $sql    = "SELECT "
            . "contato.con_contato, tipo_contato.tip_contato_nome "
            . "FROM "
            . "contato INNER JOIN "
            . "tipo_contato "
            . "ON "
            . "contato.tipo_contato = tipo_contato.tip_contato_id "
            . "WHERE "
            . "contato.pessoa = '{$id}'";
        $result = $this->executeQuery($sql)->fetchAll();
        foreach ($result as $cont) {
            if ($cont['tip_contato_nome'] == $tipo) {
                $contato[] = $cont['con_contato'];
            }
        }

        return $contato;
    }

    //Busca nome e contato de uma pessoa fisica pelo cpf
    public function buscaNomeContato($cpf)
    {
        $cont_email = $this->getEm()->getRepository('Pessoa\Entity\TipoContato')->findOneBy(
            array("tipContatoNome" => 'E-mail')
        );
        $cont_tel   = $this->getEm()->getRepository('Pessoa\Entity\TipoContato')->findOneBy(
            array("tipContatoNome" => 'Telefone')
        );
        $pessoa     = $this->getEm()->getRepository('Pessoa\Entity\PessoaFisica')->findOneBy(array('pesCpf' => $cpf));
        if ($pessoa) {
            $dados['nome'] = $pessoa->getPes()->getPesNome();
            $dados['rg']   = $pessoa->getPesRg();
            $dados         = array_merge($dados, $pessoa->toArray());

            $endereco = $this->getEm()->getRepository('Pessoa\Entity\Endereco')->findOneBy(array('pes' => $pessoa));
            if ($endereco) {
                $dados['endereco'] = $endereco->toArray();
            }

            $cont_mail_pessoa = $this->getEm()->getRepository('Pessoa\Entity\Contato')->findOneBy(
                array("pessoa" => $pessoa->getPes()->getPesId(), 'tipoContato' => $cont_email->getTipContatoId())
            );
            if ($cont_email) {
                if ($cont_mail_pessoa) {
                    $dados['email'] = $cont_mail_pessoa->getConContato();
                }
            }
            if ($cont_tel) {
                $cont_tel_pessoa = $this->getEm()->getRepository('Pessoa\Entity\Contato')->findOneBy(
                    array("pessoa" => $pessoa->getPes()->getPesId(), 'tipoContato' => $cont_tel->getTipContatoId())
                );
                if ($cont_tel_pessoa) {
                    $dados['tel'] = $cont_tel_pessoa->getConContato();
                }
            }

            return $dados;
        }
    }

    //Busca tipo de edicao aceito nos vestibulares..
    //Parametros: Id da edicao ou zero para todos cadastrados.
    public function buscaTipoEdicaoAtual($idEdicao)
    {
        //        if ($idEdicao === 0) {
        //            $entity = $this->getEm()->getRepository('Vestibular\Entity\SelecaoTipo')->findAll();
        //
        //            foreach ($entity as $data) {
        //                $tipoEdicao [$data->getTiposelId()] = $data->getTiposelNome();
        //            }
        //            return $tipoEdicao;
        //        } else {
        $entity = $this->getEm()->getRepository('Vestibular\Entity\SelecaoTipoEdicao')->findBy(
            array('edicao' => $idEdicao)
        );
        foreach ($entity as $data) {
            $tipoEdicao [$data->getSeletipoedicaoId()] = $data->getTiposel()->getTiposelNome();
        }

        return $tipoEdicao;
    }

    /**
     * Recebe o id do campuscurso ou um objeto campusCurso e retorna em um array os turnos disponiveis.
     */
    public function buscaPeriodosEdicao($selCurso)
    {
        // se não foi um objeto do tipo
        if (!($selCurso instanceof \Vestibular\Entity\SelecaoCursos)) {
            $selCurso = $this->getEm()->getRepository('Vestibular\Entity\SelecaoCursos')->findOneBy(
                array('selcursosId' => $selCurso)
            );
        }

        $periodos = [];

        if ($selCurso->getSelcursosVagasVespertino()) {
            $periodos['Vespertino'] = 'Vespertino';
        }

        if ($selCurso->getSelcursosVagasMatutino()) {
            $periodos['Matutino'] = 'Matutino';
        }

        if ($selCurso->getSelcursosVagasNoturno()) {
            $periodos['Noturno'] = 'Noturno';
        }

        if ($selCurso->getSelcursosVagasIntegral()) {
            $periodos['Integral'] = 'Integral';
        }

        return $periodos;
    }

    // Verifica se o cadastro do candidato esta completo e o retorna. Parametro: CPF do candidato.
    //Retorna false se não existir e a entidade caso exista.
    public function buscaInscricaoPorCpf($cpf)
    {
        $serv_pessoa = new \Pessoa\Service\PessoaFisica($this->getEm());
        $pessoa      = $serv_pessoa->pesquisaPessoaPorCpf($cpf);
        $inscricao   = $this->getEm()->getRepository('Vestibular\Entity\SelecaoInscricao')->findOneBy(
            array('pes' => $pessoa['pes_id'], 'edicao' => $this->buscaEdicaoAtual()->getEdicaoId())
        );
        if ($inscricao) {
            if ($inscricao->getInscricaoLinguaEstrangeira() != null) {
                return $inscricao;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //Busca prova lingua estrangeira da edicao para serem exibidas no seletor do candidato;
    //Parametros: Id da edicao atual ou a que deseja busca ou zero para todas cadastradas.
    public function buscaProvaEstrangeiraNaEdicao($edicaoId)
    {
        //        pre($edicaoId);
        if ($edicaoId === 0) {
            $provas = $this->getEm()->getRepository('Provas\Entity\Prova')->findAll();
            foreach ($provas as $obj) {
                if ($obj->getProvaEstrangeira() == 'Sim') {
                    $lingua [$obj->getProvaDescricao()] = $obj->getProvaDescricao();
                }
            }

            return $lingua;
        } else {
            $provas = $this->getEm()->getRepository('Provas\Entity\ProvaEdicao')->findAll();
            foreach ($provas as $obj) {
                if ($obj->getProva()->getProvaEstrangeira() == 'Sim' && $obj->getEdicao()->getEdicaoId() === $edicaoId
                ) {
                    $lingua [$obj->getProva()->getProvaDescricao()] = $obj->getProva()->getProvaDescricao();
                }
            }

            return $lingua;
        }
    }

    //Busca Curso e entidade pelo parametro nome.
    public function buscaCurso($nome)
    {
        return $entity = $this->getEm()->getRepository('Matricula\Entity\AcadCurso')->findOneBy(
            array('cursoNome' => $nome)
        );
    }

    //Busca as informaçõe necessarias para preenchear a tabela inscricao_cursos no Câmpus Curso.
    //Parametros para passar por array: id da Edicao/ Id do Câmpus e o Id do curso escolhido.
    public function buscaSelecaoCurso(array $dados)
    {
        $entity = $this->getEm()->getRepository('Matricula\Entity\CampusCurso')->findOneBy(
            array('camp' => $dados['campusId'], 'curso' => $dados['cursoId'])
        ); //Recupera o Id do curso

        if ($entity) {
            $selecaoCampus = $this->getEm()->getRepository('Vestibular\Entity\SelecaoCursos')->findOneBy(
                array('cursocampus' => $entity->getCursoCampusId(), 'edicao' => $dados['edicao'])
            ); //
            if ($selecaoCampus) {
                return $selecaoCampus;
            }

            return null;
        }

        return null;
    }

    //Busca a Selecao Tipo de Edicao vinculada a tabela do inscrito.
    public function buscaSelecaoTipoEdicao(array $data)
    {
        $entity = $this->getEm()->getRepository('Vestibular\Entity\SelecaoTipoEdicao')->findOneBy(
            array('edicao' => $data['edicao'], 'tiposel' => $data['tiposel'])
        );
        if ($entity) {
            return $entity->getSeletipoedicaoId();
        }

        return null;
    }

    //Busca selecao, caso já exista não insere e retorna Id, se não, insere e retorna id da inserção.
    public function buscaOuAdicionaSelecao($selecao)
    {
        $entity = $this->getEm()->getRepository('Vestibular\Entity\SelecaoTipo')->findOneBy(
            array('tiposelNome' => $selecao)
        );
        if ($entity) {
            return $entity->getTiposelId();
        } else {
            $adSelecao = new \Vestibular\Service\SelecaoTipo($this->getEm());
            $newType   = [
                'tiposelNome' => $selecao,
            ];
            $ret       = $adSelecao->adicionar($newType);

            return $ret->getTiposelId();
        }
    }

    //Busca Necessidade, caso já exista não insere e retorna Id, se não, insere e retorna id da inserção.
    public function buscaOuAdicionaNecessidade(array $necessidade)
    {
        $entity = $this->getEm()->getRepository('Pessoa\Entity\NecessidadesEspeciais')->findOneBy(
            array('necessidadeNome' => $necessidade['necessidadeNome'])
        );
        if ($entity) {
            return $entity->getNecessidadeId();
        } else {
            $adNecessidade = new \Pessoa\Service\NecessidadesEspeciais($this->getEm());
            $ret           = $adNecessidade->adicionar($necessidade);

            return $ret;
        }
    }

    // Busca Edicao Vinculada.
    public function buscaInscricaoEdicao($id)
    {
        $entity = $this->getEm()->getRepository('Vestibular\Entity\SelecaoInscricao')->findOneBy(
            array('inscricaoId' => $id)
        );

        return $entity->getEdicao()->getEdicaoId();
    }

    //Busca Pessoa vinculado a inscricao
    public function buscaPessoaInscricao($id)
    {
        $entity = $this->getEm()->getRepository('Vestibular\Entity\SelecaoInscricao')->findOneBy(
            array('inscricaoId' => $id)
        );

        return $entity->getPes()->getPesId();
    }

    //Busca o tipo de endereco, se não existir o adiociona.
    public function buscaOuAdicionaTipoEndereco(array $tipo)
    {
        $entity = $this->getEm()->getRepository('Pessoa\Entity\TipoEndereco')->findOneBy(
            array('nome' => $tipo['nome'])
        );
        if ($entity) {
            return $entity->getTipoEnderecoId();
        } else {
            $tipoEnd = new \Pessoa\Service\TipoEndereco($this->getEm());
            $ret     = $tipoEnd->adicionar($tipo);

            return $ret->getTipoEnderecoId();
        }
    }

    //Funcão busca Nome, id e CPF do candidato. Obs: $data recebe um objeto vindo no ato da incersão da inscricão.
    public function buscaNomeCpf($data)
    {
        if ($data) {
            $ret             = $data->toArray();
            $ret ['pes']     = $data->getPes()->getPesId();
            $ret ['pesNome'] = $data->getPes()->getPesNome();
            $busca           = $this->getEm()->getRepository("Pessoa\Entity\PessoaFisica")->findOneBy(
                array('pes' => $ret['pes'])
            );
            $ret ['pesCpf']  = $busca->getPesCpf();

            return $ret;
        }
    }

    //Busca todos os tipos edicao do Vestibular e retorna um array com seus nomes;
    public function buscaTipoEdicao()
    {
        $busca  = $this->getEm()->getRepository('Vestibular\Entity\SelecaoTipo')->findAll();
        $edicao = array();
        foreach ($busca as $ed) {
            $edicao[$ed->getTiposelId()] = $ed->getTiposelNome();
        }

        return $edicao;
    }

    /**
     * @return \Vestibular\Entity\SelecaoEdicao
     */
    public function buscaEdicaoAtual()
    {
        $date = (new \DateTime('now'))->format('Y-m-d');

        $sql = '
        SELECT e FROM \Vestibular\Entity\SelecaoEdicao e
        WHERE :curdate >= e.edicaoInicioInscricao AND :curdate <= e.edicaoFimInscricao
        ORDER BY e.edicaoFimInscricao DESC';

        $objSelecaoEdicao = $this->getEm()
            ->createQuery($sql)
            ->setParameter('curdate', $date)
            ->setMaxResults(1)
            ->getResult();

        if ($objSelecaoEdicao) {
            return $objSelecaoEdicao[0];
        }

        $sql = '
        SELECT e FROM \Vestibular\Entity\SelecaoEdicao e
        WHERE :curdate >= e.edicaoInicioInscricao AND :curdate > e.edicaoFimInscricao
        ORDER BY e.edicaoFimInscricao DESC';

        $objSelecaoEdicao = $this->getEm()
            ->createQuery($sql)
            ->setParameter('curdate', $date)
            ->setMaxResults(1)
            ->getResult();

        return $objSelecaoEdicao[0];
    }

    public function buscaInscricaoPelaPessoaEData($pesId, $data)
    {
        $data = (new \DateTime($data))->format('Y-m-d');

        $sql = '
        SELECT e FROM \Vestibular\Entity\SelecaoInscricao e
        WHERE date(e.inscricaoData) = :curdate AND e.pes = :pes
        ORDER BY e.inscricaoId DESC';

        $objSelecaoInscricao = $this->getEm()
            ->createQuery($sql)
            ->setParameter('curdate', $data)
            ->setParameter('pes', $pesId)
            ->setMaxResults(1)
            ->getResult();

        return $objSelecaoInscricao[0];
    }

    public function buscaTipoContato($nome)
    {
        $entity = $this->getEm()->getRepository("Pessoa\Entity\TipoContato")->findOneBy(
            array('tipContatoNome' => $nome)
        );

        return $entity;
    }

    public function buscaAprovados($edicaoId, array $dados)
    {
        $classificacao = "";
        if ($dados['tipo'] === 'Aprovados') {
            $classificacao = " 'aprovado' OR si.inscricao_resultado = 'excedente'";
        } else {
            if ($dados['tipo'] === 'Não-aprovados') {
                $classificacao = " 'desclassificado'";
            } else {
                $classificacao = " 'aprovado' ";
            }
        }

        $ordem = "";
        if ($dados['ordem'] === 'Alfabética') {
            $ordem = "nome";
        } else {
            $ordem = "inscricao_nota DESC, redacao DESC, Portugues DESC, conhecimento DESC";
        }
        $aprovados = $this->executeQuery(
            "SELECT si.inscricao_id AS inscrito, substring(ps.pes_nome,1,30) as nome,(
        (SELECT provainsc_pontuacao
                FROM prova_inscrito
                NATURAL JOIN selecao_inscricao si1
                NATURAL JOIN prova_edicao
                NATURAL JOIN prova p
                WHERE si1.inscricao_id = si.inscricao_id AND (p.prova_descricao = 'INGLES' or p.prova_descricao = 'ESPANHOL')) +
        (SELECT provainsc_pontuacao
                FROM prova_inscrito
                NATURAL JOIN selecao_inscricao si1
                NATURAL JOIN prova_edicao
                NATURAL JOIN prova p
                WHERE si1.inscricao_id = si.inscricao_id AND p.prova_descricao = 'Conhecimentos Gerais')) AS 'conhecimento', (SELECT provainsc_pontuacao
                FROM prova_inscrito
                NATURAL JOIN selecao_inscricao si1
                NATURAL JOIN prova_edicao
                NATURAL JOIN prova p
                WHERE si1.inscricao_id = si.inscricao_id AND p.prova_descricao = 'Língua Portuguesa e Literatura Brasileira') AS 'portugues',
        (SELECT provainsc_pontuacao FROM prova_inscrito
                NATURAL JOIN selecao_inscricao si1
                NATURAL JOIN prova_edicao
                NATURAL JOIN prova p
                WHERE si1.inscricao_id = si.inscricao_id AND p.prova_descricao = 'Redação') redacao,
        si.inscricao_nota AS 'total'
        FROM selecao_inscricao si
        NATURAL JOIN pessoa ps
        NATURAL JOIN prova_inscrito pv
        WHERE si.edicao_id = {$edicaoId}
        AND si.inscricao_resultado = {$classificacao}
        GROUP BY si.inscricao_id
        ORDER BY {$ordem}"
        )->fetchAll();

        $i = 0;
        foreach ($aprovados as $count) {
            $i++;
            foreach ($count as $class) {
                $aprovados[$i - 1]['classificacao'] = $i;
            }
        }

        return $aprovados;
    }

    public function buscaInscritos($edicao)
    {
        $incritos = $this->executeQuery(
            "
          SELECT selecao_inscricao.inscricao_id as inscricao,
            pessoa.pes_nome as nome,
            selecao_inscricao .inscricao_lingua_estrangeira as lingua
          FROM
            selecao_inscricao
          INNER JOIN
            pessoa
          ON
            selecao_inscricao.pes_id = pessoa.pes_id
          WHERE
            selecao_inscricao.inscricao_status = 'Aceita'
          and
            selecao_inscricao.edicao_id = {$edicao}
          ORDER BY
            nome"
        )->fetchAll();

        return $incritos;
    }

    public function excluir($id)
    {
        $erros = array();
        if (!is_array($id)) {
            $id = array($id);
        }
        foreach ($id as $i) {
            $inscricao = $this->getEm()->getRepository('Vestibular\Entity\SelecaoInscricao')->findOneBy(
                array('inscricaoId' => $i)
            )->getInscricaoStatus();
            if ($inscricao != "Incompleta") {
                $ret = new \Exception('Status do usuário não permite remoção!!!');
            } else {
                $ret = $this->removeRegistro($i);
            }
            if (is_object($ret)) {
                $erros[$i] = $ret;
            }
        }
        if (count($erros) > 0) {
            $str_xml = "<erros>";
            $i       = 0;
            foreach ($erros as $chave => $er) {
                $str_xml .= "<erro id='{$i}'>";
                $str_xml .= "<registro>" . $chave . "</registro>";
                $str_xml .= "<msg>" . $er->getMessage() . "</msg>";
                $str_xml .= "<code>" . $er->getCode() . "</code>";
                $str_xml .= "</erro>";
                $i++;
            }
            $str_xml .= "</erros>";
            throw new \Exception($str_xml);
        }

        return true;
    }

    public function resultSearch($post)
    {
        $sql    = ' SELECT * FROM pessoa ';
        $join   = '';
        $where  = " WHERE pessoa.pes_nome LIKE '%{$post['pesNome']}%' ";
        $and    = " ";
        $tabela = "";
        $query  = "";
        $on     = "";
        if (!empty($post['pesCpf'])) {
            $tabela .= 'INNER JOIN pessoa_fisica ON pessoa.pes_id = pessoa_fisica.pes_id ';
            $query .= $sql . $join . $on;
            $and .= " AND pessoa_fisica.pes_cpf LIKE '{$post['pesCpf']}'";
        }

        if ($post['inscricaoLinguaEstrangeira'] != 'NULL') {
            $tabela .= " INNER JOIN  selecao_inscricao ON pessoa.pes_id = selecao_inscricao.pes_id  ";
            $query .= $sql . $join . $on;
            $and .= " AND selecao_inscricao.inscricao_lingua_estrangeira LIKE '{$post['inscricaoLinguaEstrangeira']}'";
        } else {
            $tabela .= " INNER JOIN  selecao_inscricao ON pessoa.pes_id = selecao_inscricao.pes_id  ";
        }
        if ($post['status'] != 'NULL') {
            $query .= $sql . $join . $on;
            $and .= " AND selecao_inscricao.inscricao_status LIKE '{$post['status']}'";
        } else {
            $query .= $sql . $join . $on;
        }
        if ($post['edicaoDescricao'] != 'NULL') {
            $tabela .= "  INNER JOIN selecao_edicao ON selecao_edicao.edicao_id = selecao_inscricao.edicao_id  ";
            $query .= $sql . $join . $on;
            $and .= " AND selecao_edicao.edicao_descricao LIKE '{$post['edicaoDescricao']}' AND financeiro__titulo_tipo.tipotitulo_nome = 'Vestibular' and date(financeiro__titulo.titulo_data_processamento) >= date(selecao_edicao.edicao_inicio_inscricao)";
        } else {
            $tabela .= "  INNER JOIN selecao_edicao ON selecao_edicao.edicao_id = selecao_inscricao.edicao_id  ";
            $query .= $sql . $join . $on;
        }
        $tabela .= 'INNER JOIN financeiro__titulo ON pessoa.pes_id = financeiro__titulo.pes_id INNER JOIN financeiro__titulo_tipo ON financeiro__titulo_tipo.tipotitulo_id = financeiro__titulo.tipotitulo_id';
        $and .= "AND financeiro__titulo_tipo.tipotitulo_nome = 'Vestibular' AND MONTH(titulo_data_processamento) = MONTH(inscricao_data) AND year(titulo_data_processamento) AND year(inscricao_data)";

        $query = $sql . $join . $tabela . $on . $where . $and;
        $data  = $this->executeQuery($query)->fetchAll();
        foreach ($data as $a) {
            $dados[] = [
                'inscricao_id'                 => $a['inscricao_id'],
                'pes_id'                       => $a['pes_nome'],
                'edicao_id'                    => $a['edicao_descricao'],
                'inscricao_lingua_estrangeira' => $a['inscricao_lingua_estrangeira'],
                'inscricao_status'             => $a['inscricao_status'],
                'financeiro_titulo'            => $a['titulo_id']
            ];
        };

        return array(
            'dados' => $dados,
            'count' => count($dados)
        );
    }

    public function adicionar(array $dados)
    {
        //Buscando a edicao atual.
        $edicao_atual = $this->buscaEdicaoAtual();
        $edicao_id = $edicao_atual->getEdicaoId();

        $dados['inscricaoDataFormacao'] = $dados['inscricaoDataFormacao'] != '' ? new \DateTime(
            $dados['inscricaoDataFormacao']
        ) : null;

        $inscDatProva = null;
        if ($dados['inscricaoDataProva'] != '') {
            $inscDatProva = $this->formatDateAmericano($dados['inscricaoDataProva']);

            if ($dados['edicaoHorarioRealizacao'] == '' || $dados['edicaoHorarioRealizacao'] == null) {
                $inscDatProva = new \DateTime($inscDatProva . " " . $dados['inscricaoHorarioProva']);
            } else {
                $inscDatProva = new \DateTime($inscDatProva . " " . $dados['edicaoHorarioRealizacao']);
            }
        } else {
            $dataProvaEdicao = $this->getEm()->getRepository("Vestibular\Entity\SelecaoDataRealizacao")->findOneBy(
                ['edicao' => $edicao_id]
            );
            $inscDatProva    = $dataProvaEdicao->getRealizacaoData();
        }

        $dados['inscricaoDataHorarioProva']      = $inscDatProva;
        $dados['inscricaoIp']                    = $_SERVER['REMOTE_ADDR'];
        $dados['inscricaoNotaEnemCiencHumanas']  = (float)$dados['inscricaoNotaEnemCiencHumanas'];
        $dados['inscricaoNotaEnemCiencNatureza'] = (float)$dados['inscricaoNotaEnemCiencNatureza'];
        $dados['inscricaoNotaEnemPortugues']     = (float)$dados['inscricaoNotaEnemPortugues'];
        $dados['inscricaoNotaEnemRedacao']       = (float)$dados['inscricaoNotaEnemRedacao'];
        $dados['inscricaoNotaEnemMatematica']    = (float)$dados['inscricaoNotaEnemMatematica'];
        $dados['pesNome']                        = mb_strtoupper($dados['pesNome']);

        if (!array_key_exists('inscricaoEntregaDoc', $dados)) {
            $dados['inscricaoEntregaDoc'] = ($this->buscaSelecaoTipo(
                    $dados['seletipoedicao']
                ) != 'Vestibular') ? 'Não' : 'Sim';
        }

        $this->begin();

        try {
            $dados['edicao'] = $edicao_id;
        } catch (\Exception $exc) {
            throw new \Exception(
                'Não foi possivel encontrar a Edição vinculada a este Vestibular' . $exc->getMessage()
            );
        }

        $dados['tipoEndereco'][0] = $this->getEm()->getRepository("Pessoa\Entity\TipoEndereco")->findOneBy(
            ['nome' => "Residêncial"]
        )->getTipoEnderecoId();
        $pessoa                   = new \Pessoa\Service\PessoaFisica($this->getEm());
        if ($dados['pesNacionalidade'] == 'Estrangeiro') {
            $dados['pesCpf'] = $dados['pesDocEstrangeiro'];
        }
        $pessoaCadastrada = $pessoa->getRepository($pessoa->getEntity())->findOneBy(['pesCpf' => $dados['pesCpf']]);
        if ($pessoaCadastrada) {
            $dados['pes'] = $pessoaCadastrada->getPes()->getPesId();
            $pessoa       = $pessoa->edita($dados);
        } else {
            $pessoa = $pessoa->adicionar($dados);
        }
        $dados['pes'] = $pessoa->getPes()->getPesId();
        if (isset($dados['pesNecessidades'])) {
            $pessoaNecessidades    = new \Pessoa\Service\PessoaNecessidades($this->getEm());
            $dados['necessidades'] = $pessoaNecessidades->adicionar($dados);
        }

        $cont = new \Pessoa\Service\Contato($this->getEm());

        try {
            // Insere contato do tipo e-mail do candidato.
            if (!empty($dados['contEmail'])) {
                $array_cont_email = array(
                    'conContato'  => array($dados['contEmail']),
                    'pessoa'      => $dados['pes'],
                    'tipoContato' => array('E-mail')
                );
                $cont->adicionar($array_cont_email);
            }
            //Insere o contato  Telefone do candidato no banco.
            if (!empty($dados['contTel'])) {
                $array_cont_tel = array(
                    'conContato'  => array($dados['contTel']),
                    'pessoa'      => $dados['pes'],
                    'tipoContato' => array('Celular')
                );
                $cont->adicionar($array_cont_tel);
            }
        } catch (\Exception $exc) {
            throw new \Exception(
                'Erro ao inserir dados de contatos do candidato, verifique se o tipo existe' . $exc->getMessage()
            );
        }

        $dados['inscricaoData']   = (new \DateTime("now"));
        $dados['sala']            = null;
        $dados['inscricaoStatus'] = ($dados['tipoPagamento'] == 'Dinheiro' || $dados['tipoPagamento'] == 'Isento') ? 'Aceita' : 'Solicitada';
        $dados['edicao']          = (new \Vestibular\Service\SelecaoEdicao($this->getEm()))->findOneBy(
            ['edicaoId' => $dados['edicao']]
        );
        $dados['selelocais'] = (new \Vestibular\Service\SelecaoLocais($this->getEm()))->findOneBy(
            ['selelocaisId' => $dados['selelocais']]
        );
        $dados['seletipoedicao'] = (new \Vestibular\Service\SelecaoTipoEdicao($this->getEm()))->findOneBy(
            ['seletipoedicaoId' => $dados['seletipoedicao']]
        );
        $dados['pes']            = (new \Pessoa\Service\Pessoa($this->getEm()))->findOneBy(['pesId' => $dados['pes']]);

        $selecaoInsc = new \Vestibular\Entity\SelecaoInscricao($dados);

        try {
            $this->getEm()->persist($selecaoInsc);
            $this->getEm()->flush();
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
        }

        $ret = $selecaoInsc;

        //        $ret = parent::adicionar($dados);
        //Momento onde a tabela inscricao Curso é preenchida.
        if ($dados['cursoSelecao']) {
            if (!is_array($dados['cursoSelecao'])) {
                $dados['cursoSelecao'] = array($dados['cursoSelecao']);
            }

            foreach ($dados['cursoSelecao'] as $cursoSelecao) {
                $turno='';
                try {
                    if ($dados['turno']) {
                        $turno = $dados['turno'];
                    } elseif ($dados['inscricaoTurno']) {
                        $turno = $dados['inscricaoTurno'];

                        if (is_array($dados['inscricaoTurno'])) {
                            $turno = $dados['inscricaoTurno'][0];
                        }
                    }

                    $inscricaoCurso = [
                        'selcursos'      => $cursoSelecao,
                        'inscricaoTurno' => $turno,
                        'inscricao' => $ret->getInscricaoId(),
                    ];

                    $serviceInscricaoCurso = new \Vestibular\Service\InscricaoCursos($this->getEm());
                    $ret_inscricao         = $serviceInscricaoCurso->adicionar($inscricaoCurso);
                } catch (\Exception $ex) {
                    throw new \Exception('Erro ao preencher a tabela Inscricao cursos' . $ex->getMessage());
                }
            }
        }

        //        $this->buscaPeriodosEdicao($inscricaoCurso);

        if (is_array($dados['campus'])) {
            $dados['campus'] = array_shift($dados['campus']);
        }

        //Momento em que o vestibulando é cadastrado como um usuário no sistema.
        try {
            $acesso     = new \Acesso\Service\AcessoPessoas($this->getEm());
            $acessoUser = $acesso->getRepository($acesso->getEntity())->findOneBy(['login' => $dados['username']]);

            if (!$acessoUser) {
                $serviceAcesso = new \Acesso\Service\AcessoUsuarios($this->getEm());
                $datetime      = (new \DateTime('now'));
                $acessoUsuario = $serviceAcesso->adicionar(
                    [
                        'id'                   => '',
                        'acessoDataRegistro'   => $datetime,
                        'acessoDataSenhaSalva' => $datetime,
                        'usrStatus'            => 'Ativo'
                    ]
                );

                $usuario = [
                    'pesFisica' => $ret->getPes()->getPesId(),
                    'login'     => $dados['username'],
                    'senha'     => sha1($dados['password']),
                    'usrStatus' => 'Ativo',
                    'usuario'   => $acessoUsuario->getId(),
                    'email'     => $dados['contEmail']
                ];

                $adUser = $acesso->adicionar($usuario);

                $grupo = $this->getEm()->getRepository('Acesso\Entity\AcessoGrupo')->findOneBy(
                    array('grupNome' => 'Vestibulandos')
                );

                $usuGrupo = [
                    'ativo'   => 'Sim',
                    'usuario' => $acessoUsuario->getId(),
                    'grupo'   => $grupo->getId(),
                ];

                $usuGrupo = $serviceAcesso->vinculaGrupoUser($usuGrupo, false);

                if ($dados['contEmail']) {
                    if ($dados['inscricaoDataHorarioProva']) {
                        $this->enviaDadosCadastroAgendado(
                            $dados['contEmail'],
                            $dados['username'],
                            $dados['password'],
                            true,
                            $dados['campus'],
                            $dados['inscricaoDataHorarioProva']
                        );
                    } else {
                        $this->enviaDadosCadastro(
                            $dados['contEmail'],
                            $dados['username'],
                            $dados['password'],
                            true,
                            $dados['campus']
                        );
                    }
                }
            } else {
                $usuario['id']        = $acessoUser->getId();
                $usuario['login']     = $acessoUser->getLogin();
                $usuario['usuario']   = $acessoUser->getUsuario();
                $usuario['pesFisica'] = $pessoa;
                $usuario['senha']     = sha1($dados['password']);
                $usuario['email']     = $dados['contEmail'];
                $acesso->editaSenha($usuario);

                if ($dados['contEmail']) {
                    if ($dados['inscricaoDataHorarioProva']) {
                        $this->enviaDadosCadastroAgendado(
                            $dados['contEmail'],
                            $dados['username'],
                            $dados['password'],
                            true,
                            $dados['campus'],
                            $dados['inscricaoDataHorarioProva']
                        );
                    } else {
                        $this->enviaDadosCadastro(
                            $dados['contEmail'],
                            $dados['username'],
                            $dados['password'],
                            true,
                            $dados['campus']
                        );
                    }
                }
            }
        } catch (\Exception $ex) {
            throw new \Exception('Erro ao Cadastrar o vestibulando como Usuario' . $ex->getMessage());
        }

        $this->commit();

        return $ret;
    }

    public function edita(array $dados)
    {
        if ($dados['inscricaoDataFormacao']) {
            if (!is_object($dados['inscricaoDataFormacao'])) {
                $dados['inscricaoDataFormacao'] = new \DateTime($dados['inscricaoDataFormacao']);
            }
        } else {
            $dados['inscricaoDataFormacao'] = null;
        }

        $inscDatProva = null;

        if ($dados['inscricaoDataProva'] != '') {
            $inscDatProva = $this->formatDateAmericano($dados['inscricaoDataProva']);
            $inscDatProva = new \DateTime($inscDatProva . " " . $dados['inscricaoHorarioProva']);
        }

        $dados['inscricaoDataHorarioProva'] = $inscDatProva;

        if (!array_key_exists('inscricaoEntregaDoc', $dados)) {
            $dados['inscricaoEntregaDoc'] = ($this->buscaSelecaoTipo(
                    $dados['seletipoedicao']
                ) != 'Vestibular') ? 'Não' : 'Sim';
        }

        parent::begin();

        //Inserindo endereco da pessoa.
        try {
            $end                      = new \Pessoa\Service\Endereco($this->getEm());
            $tipoEnd                  = [
                'nome'      => 'Residencial',
                'descricao' => 'Endereco Residencial'
            ];
            $dados['tipoEndereco'][0] = $this->buscaOuAdicionaTipoEndereco($tipoEnd);
        } catch (\Exception $exc) {
            throw new \Exception('Erro ao adicionar  tipo de endereco!' . $exc->getMessage());
        }

        //Editando Tabela Pessoa Fisica
        try {
            $pf  = new \Pessoa\Service\PessoaFisica($this->getEm());
            $pes = new \Pessoa\Service\Pessoa($this->getEm());

            $dados['pes']               = $this->buscaPessoaInscricao($dados['inscricaoId']);
            $dados['pesId']             = $dados['pes'];
            $dados['pesDataNascimento'] = new \DateTime($this->formatDateAmericano($dados['pesDataNascimento']));

            if ($dados['pesNacionalidade'] == 'Estrangeiro') {
                $dados['pesCpf'] = $dados['pesDocEstrangeiro'];
            }

            $pessoa       = $pes->edita($dados);
            $pessoaFisica = $pf->edita($dados);

            if (isset($dados['pesNecessidades'])) {
                $pessoaNecessidades   = new \Pessoa\Service\PessoaNecessidades($this->getEm());
                $dados['necessidade'] = $pessoaNecessidades->edita($dados);
            }

            //Desvio de caminho importante********
            //Momento em que a instituição edita um vestibulando caso sua incricao esteja como 'solicitada'.
            if ($_SESSION['Zend_Auth'] && $dados['inscricaoStatus'] === 'Solicitada') {
                $dados['inscricao']      = $dados['inscricaoId'];
                $dados['inscricaoTurno'] = $dados['turno'];

                if ($dados['inscricaoStatus'] != 'Aceita') {
                    $dados['inscricaoStatus'] = ($dados['tipoPagamento'] == 'Dinheiro' || $dados['tipoPagamento'] == 'Isento') ? 'Aceita' : 'Solicitada';
                }
                $service_end = new \Pessoa\Service\Endereco($this->getEm());
                $end         = [
                    'pes'            => $dados['pes'],
                    'tipoEndereco'   => $dados['tipoEndereco'][0],
                    'endId'          => $dados['endId'][0],
                    'endCep'         => $dados['endCep'][0],
                    'endLogradouro'  => $dados['endLogradouro'][0],
                    'endNumero'      => ($dados['endNumero'][0] == '') ? '' : $dados['endNumero'][0],
                    'endComplemento' => ($dados['endComplemento'][0] == '') ? '' : $dados['endComplemento'][0],
                    'endBairro'      => $dados['endBairro'][0],
                    'endCidade'      => $dados['endCidade'][0],
                    'endEstado'      => $dados['endEstado'][0],
                    'endData'        => new \DateTime('now')
                ];

                //Registro Endereço recebe alterações.
                $edit_end = $service_end->edita($end);
            }

            //Momento onde a tabela inscricao Curso é preenchida.
            try {
                $inscricaoCurso = [
                    'selcursos'        => $dados['cursoSelecao'],
                    'inscricaoTurno'   => ($dados['turno']) ? $dados['turno'] : '',
                    'inscricao'        => $dados['inscricaoId'],
                    'incricaoCursosId' => $dados['incricaoCursosId']
                ];

                $serviceInscricaoCurso = new \Vestibular\Service\InscricaoCursos($this->getEm());

                if ($inscricaoCurso['incricaoCursosId']) {
                    $serviceInscricaoCurso->edita($inscricaoCurso);
                } else {
                    $serviceInscricaoCurso->adicionar($inscricaoCurso);
                }
            } catch (\Exception $ex) {
                throw new \Exception('Erro ao preencher a tabela Inscricao cursos' . $ex->getMessage());
            }

            $edit_contato    = new \Pessoa\Service\Contato($this->getEm());
            $reference_email = $this->buscaTipoContato('E-mail');
            $reference_tel   = $this->buscaTipoContato('Telefone');

            if ($dados['contatoMail'] != null) {
                $cont_mail = [
                    'conId'       => $dados['contatoMail']['conId'],
                    'pessoa'      => $dados['pes'],
                    'tipoContato' => $reference_email->getTipContatoId(),
                    'conContato'  => $dados['contEmail']
                ];
                //Registro de contato de e-mail recebe alterações.
                $edit_mail = $edit_contato->edita($cont_mail);
            } else {
                $array_cont_email = [
                    'conId'       => '',
                    'pessoa'      => $dados['pes'],
                    'tipoContato' => array('E-mail'),
                    'conContato'  => array($dados['contEmail'])
                ];
                $edit_contato->adicionar($array_cont_email);
            }

            if ($dados['contatoTel'] != null) {
                $cont_tel = [
                    'conId'       => $dados['contatoTel']['conId'],
                    'pessoa'      => $dados['pes'],
                    'tipoContato' => $reference_tel->getTipContatoId(),
                    'conContato'  => $dados['contTel']
                ];
                //Registro de telefones recebe as alterações.
                $edit_tel = $edit_contato->edita($cont_tel);
            } else {
                //Insere o contato  Telefone do candidato no banco.
                $array_cont_tel = [
                    'conId'       => '',
                    'pessoa'      => $dados['pes'],
                    'tipoContato' => array('Telefone'),
                    'conContato'  => array($dados['contTel'])
                ];
                $edit_contato->adicionar($array_cont_tel);
            }

            $dados['edicao'] = $this->getReference($dados['inscricaoId'], $this->getEntity())->getEdicao()->getEdicaoId(
            );

            // Edita todos os dados.
            $ret = parent::edita($dados);
            parent::commit();

            return $ret;
        } catch (\Exception $exc) {
            throw new \Exception('Erro ' . $exc->getMessage());
        }
    }

    public function buscaValorIsncricao($id)
    {
        $query = "
            SELECT
              selcursos_valor_inscricao
            FROM
              selecao_cursos
            WHERE
              edicao_id = {$id}
        ";

        $valor = $this->executeQuery($query);
        $valor = $valor->fetch();

        return $valor['selcursos_valor_inscricao'];
    }

    public function pagination(array $data)
    {
        $serviceAcessoPessoas        = new \Acesso\Service\AcessoPessoas($this->getEm());
        $edicaoAtual                 = $this->buscaEdicaoAtual();
        $serviceFinanceiroTipotitulo = new \Financeiro\Service\FinanceiroTituloTipo($this->getEm());

        $tipoVestibular = $serviceFinanceiroTipotitulo::TIPOTITULOVESTIBULAR ? $serviceFinanceiroTipotitulo::TIPOTITULOVESTIBULAR : 'Vestibular';

        if (!$edicaoAtual) {
            return [];
        }

        $where = "selecao_inscricao.edicao_id = '{$edicaoAtual->getEdicaoId()}'";

        $arrCampusCursoPermitido = implode(",", $serviceAcessoPessoas->retornaCampusCursoDoUsuarioLogado(true));

        if ($arrCampusCursoPermitido) {
            $where .= " AND (campus_curso.cursocampus_id in (" . $arrCampusCursoPermitido . ") OR campus_curso.cursocampus_id IS NULL)";
        }

        if ($data['filter']) {
            if (!$where) {
                $where = '1=1';
            }
            /*Uso de variavel SQL não deu certo*/

            if ($data['filter']['dataInicial']) {
                $dataInicial = $this::formatDateAmericano($data['filter']['dataInicial']);

                $where .= " AND DATE(coalesce(if(inscricao_data_horario_prova IS NOT NULL, DATE_FORMAT(inscricao_data_horario_prova, '%Y-%m-%d'), DATE_FORMAT(realizacao_data, '%Y-%m-%d')),'0000-00-00'))>=DATE('$dataInicial')";
            }

            if ($data['filter']['dataFinal']) {
                $dataFinal = $this::formatDateAmericano($data['filter']['dataFinal']);
                $where.=" AND DATE(coalesce(if(inscricao_data_horario_prova IS NOT NULL, DATE_FORMAT(inscricao_data_horario_prova, '%Y-%m-%d'), DATE_FORMAT(realizacao_data, '%Y-%m-%d')),'0000-00-00'))<=DATE('$dataFinal')";
            }

            if ($data['filter']['horarioInicial']) {
                $horarioInicial = explode(':', $data['filter']['horarioInicial']);
                $horarioInicial = ($horarioInicial[0] * 3600) + ($horarioInicial[1]*60);

                $where .= " AND (TIME_TO_SEC(COALESCE(if(inscricao_data_horario_prova IS NOT NULL, DATE_FORMAT(inscricao_data_horario_prova, '%H:%i'), DATE_FORMAT(realizacao_data, '%H:%i')),0))) >=$horarioInicial ";
            }

            if ($data['filter']['horarioFinal']) {
                $horarioFinal = explode(':', $data['filter']['horarioFinal']);
                $horarioFinal = ($horarioFinal[0] * 3600) + ($horarioFinal[1]*60);

                $where .= " AND (TIME_TO_SEC(COALESCE(if(inscricao_data_horario_prova IS NOT NULL, DATE_FORMAT(inscricao_data_horario_prova, '%H:%i'), DATE_FORMAT(realizacao_data, '%H:%i')),0)))<=$horarioFinal";
            }
        }

        $query  = <<<SQL
            SELECT
                selecao_inscricao.inscricao_id as inscricao_id,
                edicao_descricao,
                pes_nome,
                curso_nome as curso,
                IF(matsit_descricao is null,inscricao_status,matsit_descricao) inscricao_status,
                inscricao_lingua_estrangeira,
                if(inscricao_data_horario_prova IS NOT NULL, 'Sim', 'Não') as prova_agendada,
                if(inscricao_data_horario_prova IS NOT NULL, DATE_FORMAT(inscricao_data_horario_prova, '%d/%m/%Y'), DATE_FORMAT(realizacao_data, '%d/%m/%Y')) as data_prova,
                if(inscricao_data_horario_prova IS NOT NULL, DATE_FORMAT(inscricao_data_horario_prova, '%H:%i'), DATE_FORMAT(realizacao_data, '%H:%i')) as hora_prova,
                (
                SELECT 
                    bol_id
                FROM
                    financeiro__titulo
                        LEFT JOIN
                    financeiro__titulo_tipo ON financeiro__titulo_tipo.tipotitulo_id = financeiro__titulo.tipotitulo_id
                        LEFT JOIN
                    boleto ON financeiro__titulo.titulo_id = boleto.titulo_id
                WHERE
                    pessoa.pes_id = financeiro__titulo.pes_id
                        AND financeiro__titulo_tipo.tipotitulo_nome = '$tipoVestibular'  AND titulo_data_processamento >= inscricao_data
                ) financeiro_titulo,
                DATE_FORMAT(inscricao_data,'%d/%m/%Y %H:%i:%s') inscricao_data,
                IF(inscricao_data_horario_prova > now(),'Agendada',if(inscricao_nota IS NULL,'Indefinida',inscricao_nota)) prova,
                inscricao_data data_inscricao,
                inscricao_data_horario_prova prova_data
            FROM
                selecao_inscricao
            INNER JOIN
                inscricao_cursos ON inscricao_cursos.inscricao_id = selecao_inscricao.inscricao_id
            INNER JOIN
                selecao_cursos ON selecao_cursos.selcursos_id = inscricao_cursos.selcursos_id
            INNER JOIN
                campus_curso ON campus_curso.cursocampus_id = selecao_cursos.cursocampus_id
            INNER JOIN
                acad_curso ON acad_curso.curso_id = campus_curso.curso_id
            INNER JOIN
                selecao_edicao ON selecao_edicao.edicao_id = selecao_inscricao.edicao_id
            INNER JOIN
                selecao_data_realizacao ON selecao_data_realizacao.edicao_id =  selecao_edicao.edicao_id
            INNER JOIN
                pessoa ON pessoa.pes_id = selecao_inscricao.pes_id
            LEFT JOIN

                acadgeral__aluno aluno ON aluno.pes_id = pessoa.pes_id
            LEFT JOIN
                acadgeral__aluno_curso aluno_curso ON aluno_curso.aluno_id = aluno.aluno_id
            LEFT JOIN
                (SELECT
                        *
                    FROM
                        acadperiodo__aluno
                    WHERE
                        1
                    GROUP BY alunocurso_id
                    HAVING MIN(alunoper_id)) aluno_periodo ON aluno_periodo.alunocurso_id = aluno_curso.alunocurso_id
             LEFT JOIN
                acadgeral__situacao situacao ON situacao.situacao_id = aluno_periodo.matsituacao_id
            WHERE
                {$where}
            GROUP BY inscricao_id
            ORDER BY inscricao_id DESC
SQL;
        $result = $this->paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * Verifica se algum candidato ja possui alguma prova cadastrada para ele.
     * */
    public function verificaCorrecao($edicaoId)
    {
        if (!$edicaoId) {
            $this->setLastError("Nenhuma edição informada !");

            return false;
        };

        $result = $this->executeQuery(
            "
        SELECT *
        FROM prova_inscrito
        INNER JOIN prova_edicao ON prova_edicao.proedicao_id = prova_inscrito.proedicao_id
        WHERE edicao_id = '{$edicaoId}'"
        )->fetchAll();
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Função monta a classificação do vestibular.
     * */
    public function buscaClassificacao($edicaoId,$inscricaoId=false)
    {
        $classificacao = $this->executeQuery(
            "
            SELECT pes_nome, inscricao_nota, inscricao.inscricao_id,inscricao_classificacao classificacao
              FROM inscricao_cursos
            INNER JOIN selecao_inscricao inscricao on inscricao.inscricao_id = inscricao_cursos.inscricao_id
            JOIN pessoa USING(pes_id)
            WHERE inscricao.inscricao_id IS NOT NULL AND edicao_id = '{$edicaoId}'
             ".($inscricaoId?' AND inscricao.inscricao_id ='.$inscricaoId:'' )."
             AND inscricao_nota > 0 AND inscricao_resultado NOT IN('desclassificado')
            ORDER BY inscricao_nota DESC
        "
        )->fetchAll();
        /*
 * habilitar quando extiver em processo seletivo
 */
        //        $numeroVagas = $this->executeQuery("
        //            SELECT
        //              selcursos_vagas_matutino+selcursos_vagas_noturno+selcursos_vagas_vespertino as nvagas
        //            FROM
        //              selecao_cursos
        //            WHERE
        //              edicao_id = {$edicaoId}")->fetchAll();
        //        $numeroVagas = $numeroVagas[0]['nvagas'];

        return $classificacao;
    }

    /**
     * Método para buscar o tipo de ingresso.
     * $seletipoedicaoId recebe o ID da tabela seletipoedicao
     * */
    public function buscaSelecaoTipo($seletipoedicaoId)
    {
        $nome = $this->executeQuery(
            "SELECT tiposel_nome
                             FROM selecao_tipo_edicao as seletipo
                             INNER JOIN selecao_tipo as tipo ON seletipo.tiposel_id = tipo.tiposel_id
                             WHERE seletipo.seletipoedicao_id = {$seletipoedicaoId}"
        )->fetch();

        return ($nome) ? $nome['tiposel_nome'] : null;
    }

    /**
     * Método para gerar a senha.
     * */
    public function passGeneration($length = 8)
    {
        $password = '';
        for ($i = 0; $i < $length; $i++) {
            $password .= rand(1, 9);
        }

        return str_replace(" ", '', $password);
    }

    public function enviaDadosCadastro($email, $usuario, $senha, $cadastro = true, $campus, $linkVest = '')
    {
        if (!$linkVest) {
            $linkVest = $this->linkLoginCadidato;
        }

        if (is_numeric($campus)) {
            $campus = $this->getRepository('Organizacao\Entity\OrgCampus')->findOneBy(['campId' => $campus]);
        }

        $campusNome         = $campus->getIes()->getPes()->getPes()->getPesNome();
        $campusNomeFantasia = $campus->getIes()->getPes()->getpesNomeFantasia();

        $mensagem = "<p>Obrigado por escolher a $campusNomeFantasia</p>";

        if ($cadastro) {
            $mensagem .= "<p>Sua inscrição foi realizada com sucesso. Seus dados de acesso em nosso site são: </p>";
        } else {
            $mensagem .= "<p>Seus dados para acesso virtual foram alterados com sucesso. E agora são:</p>";
        }

        $mensagem .= "<p>Usuário:$usuario</p> <p>Senha: $senha</p> <p>Acesse sua inscrição clicando <a href='$linkVest'>Aqui</a></p>";

        $mensagemHtml = "<div style='width:820px; margin:10px 0 0 5px;padding:0 0 50px 0;background:#00689b;'><div><div style='width:120px;text-align:center;float:left;'><img style='width:100px;' src='img/email.png'></div><div style='width:120px;text-align:center;float:left;'><i class='fa fa-envelope fa-4'></i></div><div style='float:left;width:700px;'><h1 style='font-family:arial;color:white;font-size:18pt;text-align:center;margin:15px 0 0 0;'>$campusNome</h1><h1 style='font-family:arial;color:white;font-size:12pt;text-align:center;margin:4px 0 15px 0;'>Chegou uma nova mensagem para você.</h1></div><span style='clear:both;height:0px;font-size:0px;display:block;'></span></div><div style='padding:10px;margin:1px;text-align:left;background:#FFF;-webkit-border-radius: 20px;-moz-border-radius: 20px;border-radius: 20px;'><br>$mensagem<br><br>Qualquer dúvida entre em contato:<br>Telefone: {$campus->getCampTelefone()}<br>Email: {$campus->getCampEmail()}<br>Site: {$campus->getCampSite()}</div></div>";

        $retorno = $this->enviaEmail(
            $mensagemHtml,
            [
                'subject' => 'Inscrição Vestibular',
                'to'      => $email,
            ]
        );
    }

    public function enviaDadosCadastroAgendado(
        $email,
        $usuario,
        $senha,
        $cadastro = true,
        $campus,
        $dataProva,
        $linkVest = ''
    ) {
        if (!$linkVest) {
            $linkVest = $this->linkLoginCadidato;
        }

        /** @var \Organizacao\Entity\OrgCampus $campus */
        $campus             = $this->getRepository('Organizacao\Entity\OrgCampus')->findOneBy(['campId' => $campus]);
        $campusNome         = $campus->getIes()->getIesNome();
        $campusNomeFantasia = $campus->getIes()->getIesSigla();

        $mensagem = "<p>Obrigado por escolher a instituição $campusNomeFantasia</p>";

        if ($cadastro) {
            $mensagem .= "<p>Sua inscrição foi realizada com sucesso. Seus dados de acesso em nosso site são:          </p>";
        }

        $mensagem .= "<p>Usuário:$usuario</p> <p>Senha: $senha</p> <p>Acesse sua inscrição clicando <a href='$linkVest'>Aqui</a></p><br>";
        $mensagem .= "<p>Sua prova está agendada para   dia {$dataProva->format('d/m/Y')} ás {$dataProva->format('H:i')} horas.</p>";
        $mensagem .= "<p>Atenção, lembre-se de comparecer no local da prova com pelo menos 30 minutos de antecedência e munido do comprovante de pagamento do boleto!</p>";

        $mensagemHtml = "
        <div style='width:820px; margin:10px 0 0 5px;padding:0 0 50px 0;background:#00689b;'>
            <div>
                <div style='width:120px;text-align:center;float:left;'>
                    <img style='width:100px;' src='img/email.png'>
                </div>
                <div style='width:120px;text-align:center;float:left;'>
                    <i class='fa fa-envelope fa-4'></i>
                    </div><div style='float:left;width:700px;'><h1 style='font-family:arial;color:white;font-size:18pt;text-align:center;margin:15px 0 0 0;'>$campusNome</h1><h1 style='font-family:arial;color:white;font-size:12pt;text-align:center;margin:4px 0 15px 0;'>Chegou uma nova mensagem para você.</h1></div><span style='clear:both;height:0px;font-size:0px;display:block;'></span></div><div style='padding:10px;margin:1px;text-align:left;background:#FFF;-webkit-border-radius: 20px;-moz-border-radius: 20px;border-radius: 20px;'><br>$mensagem<br><br>Qualquer dúvida entre em contato:<br>Telefone: {$campus->getCampTelefone()}<br>Email: {$campus->getCampEmail()}<br>Site: {$campus->getCampSite()}</div></div>";

        $retorno = $this->enviaEmail(
            $mensagemHtml,
            [
                'subject' => $campusNomeFantasia . ': Confirmação de Inscrição no Vestibular',
                'to'      => $email,
            ]
        );
    }

    /*
     * Busca informações
     * */
    public function relatorioInscritos($dataIni, $dataFim)
    {
        $inscHj = $this->executeQuery(
            "
            SELECT * FROM view__titulos
            WHERE
              DATE(pagamento) >= DATE('{$dataIni}') AND DATE(pagamento) <= DATE('{$dataFim}')
        "
        )->fetchAll();

        return $inscHj;
    }

    /*TODO: Este serviço precisa ser alterada quando o vestibular passar por modificações. As informações que são geradas por ele utilizam combinações de dados que  podem não ser exatas. */
    public function relatorioFinanceiroInscricoesVest($arrParam)
    {
        $serviceMeioPagamento = new \Financeiro\Service\FinanceiroMeioPagamento($this->getEm());
        $formaPagBoleto       = $serviceMeioPagamento::BOLETO;
        $formaPagIsento       = $serviceMeioPagamento::ISENTO;

        if (empty($par['edicao_id']) OR isset($par['edicao_id'])) {
            $arrParam['edicao_id'] = $this->buscaEdicaoAtual()->getEdicaoId();
        }

        $query ="
        SELECT
            inscricao_id Inscricao,
            DATE(titulo_data_pagamento) Data,
            SUM(titulo_valor_pago) AS ValorPago,
            1 Pagos,
            IF(
                meio_pagamento_descricao = '{$formaPagBoleto}',
                'Internet',
                IF(
                    (titulo_tipo_pagamento = '{$formaPagIsento}' OR meio_pagamento_descricao = '{$formaPagIsento}'),
                    'Isento',
                    'Local'
                )
            ) Origem
        FROM selecao_inscricao
        LEFT JOIN financeiro__titulo ON financeiro__titulo.pes_id = selecao_inscricao.pes_id AND YEAR(titulo_data_processamento) = YEAR(inscricao_data)
        LEFT JOIN financeiro__titulo_tipo USING (tipotitulo_id)
        LEFT JOIN financeiro__pagamento on financeiro__pagamento.titulo_id = financeiro__titulo.titulo_id
        LEFT JOIN financeiro__meio_pagamento USING (meio_pagamento_id)
        WHERE
            DATE(titulo_data_pagamento) >= DATE(:data_inicial)
            AND DATE(titulo_data_pagamento) <= DATE(:data_final)
            AND tipotitulo_nome = 'Vestibular'
            AND edicao_id = :edicao_id
        GROUP BY selecao_inscricao.pes_id, inscricao_id, titulo_tipo_pagamento";

        return $this->executeQueryWithParam($query, $arrParam)->fetchAll();
    }

    public function redefinirSenha($inscricao, $password)
    {
        $acessoPessoasService     = new \Acesso\Service\AcessoPessoas($this->getEm());
        $referenceAcesso          = $this->getRepository('Acesso\Entity\AcessoPessoas')->findOneBy(
            ['pesFisica' => $inscricao->getPes()->getPesId()]
        );
        $referenceAcesso          = $referenceAcesso->toArray();
        $referenceAcesso['senha'] = $password;
        $acessoPessoasService->edita($referenceAcesso);
        $referenceAcesso['contato'] = $this->getRepository("Pessoa\Entity\Contato")->buscaContatos(
            $inscricao->getPes()->getPesId()
        );

        return $referenceAcesso;
    }

    public function buscaInscricao($pesId, $edicao = null)
    {
        $candidato = $this->findOneBy(['pes' => $pesId, 'edicao' => $edicao]);

        if ($candidato) {
            return $candidato;
        }

        return false;
    }

    public function atualizaNotaInscricao($inscricaoId, $nota)
    {
        try {
            $this->exec(
                "UPDATE inscricao_cursos SET inscricao_nota = '{$nota}' WHERE inscricao_id = '{$inscricaoId}'"
            );
        } catch (\Exception $ex) {
            throw new \Exception($ex->getCode(), $ex->getMessage(), $ex->getPrevious());
        }
    }

    public function insereClassificacao($inscricaoId, $classificacao)
    {
        try {
            $this->exec(
                "UPDATE inscricao_cursos SET inscricao_classificacao = '{$classificacao}' WHERE inscricao_id = '{$inscricaoId}'"
            );
        } catch (\Exception $ex) {
            throw new \Exception($ex->getCode(), $ex->getMessage(), $ex->getPrevious());
        }
    }

    public function Classificar($edicaoAtual)
    {
        $candidatos = $this->buscaClassificacao($edicaoAtual);

        $this->insereClassificacaoTabela($candidatos);
    }

    public function atualizaClassificacao($edicaoId)
    {
        $arrClassificacao = $this->buscaClassificacao($edicaoId);

        foreach ($arrClassificacao as $c => $value) {
            $this->atualizaNotaInscricao($value['inscricao_id'], $value['inscricao_nota']);
            $this->insereClassificacao($value['inscricao_id'], $value['classificacao']);
        }

        return true;
    }

    public function insereClassificacaoTabela($arrayDados)
    {
        foreach ($arrayDados as $dados) {
            if (is_integer($dados['inscricao_id'])) {
                $this->executeQuery(
                    "
                UPDATE
                    inscricao_cursos
                SET
                    inscricao_nota  = {$dados['inscricao_nota']},
                    inscricao_classificacao = {$dados['classificacao']}
                WHERE
                    inscricao_id  = {$dados['inscricao_id']}
            "
                );
            }
        }
    }

    public function mudaClassificacao($inscricaoId, $edicaoId)
    {
        $arrClassificacao = $this->buscaClassificacao($edicaoId);
        $candidato        = array();

        foreach ($arrClassificacao as $candidato => $value) {
            if ($value['inscricao_id'] == $inscricaoId) {
                $candidato = $value;
            }
        }

        foreach ($arrClassificacao as $C => $candidatoAtual) {
            if ($candidato['inscricao_nota'] > $candidatoAtual['inscricao_nota']) {
                $move      = $candidatoAtual;
                $candidato = $move;
            }
        }

        $this->insereClassificacaoTabela($arrClassificacao);

        return true;
    }

    public function buscaClassificacaoCandidato(\Vestibular\Entity\SelecaoInscricao $inscricao)
    {
        $classificacaoTotal = $this->buscaClassificacao(
            $inscricao->getEdicao()->getEdicaoId(),
            $inscricao->getInscricaoId()
        );

        foreach ($classificacaoTotal as $candidato) {
            if ($candidato['inscricao_id'] == $inscricao->getInscricaoId()) {
                return $candidato;
            }
        }

        return 0;
    }

    public function buscaInscricaoAlfabetica($edicaoId)
    {
        $inscricoes = $this->executeQuery(
            "
          SELECT
           *
          FROM
           selecao_inscricao
          NATURAL JOIN
          selecao_tipo_edicao
          NATURAL JOIN
          selecao_tipo
          INNER JOIN
           pessoa ON selecao_inscricao.pes_id = pessoa.pes_id
          WHERE edicao_id = {$edicaoId} AND inscricao_status = 'Aceita' AND tiposel_prova = 'Sim'
          ORDER BY pes_nome"
        )->fetchAll();

        return $inscricoes;
    }

    public function atualizaSala($inscricao)
    {
        try {
            $this->exec(
                "UPDATE selecao_inscricao SET sala_id = {$inscricao['sala_id']} WHERE inscricao_id = {$inscricao['inscricao_id']}"
            );
        } catch (\Exception $ex) {
            throw new \Exception("Erro ao atualizar sala do candidato de inscriçao Nº: {$inscricao['inscricao_id']}");
        }
    }

    public function buscaSalasAlocadas($edicao)
    {
        $salasPreenchidas = $this->executeQuery(
            "
          SELECT
            selecao_inscricao.sala_id, count(selecao_inscricao.sala_id) as insc, sala_nome, sala_capacidade
          FROM
            selecao_inscricao
          INNER JOIN
            infra_sala ON infra_sala.sala_id = selecao_inscricao.sala_id
          WHERE
            selecao_inscricao.sala_id <> 'NULL' AND edicao_id = '{$edicao}'
          GROUP BY
            selecao_inscricao.sala_id
        "
        )->fetchAll();

        return $salasPreenchidas;
    }

    public function buscaAlunosPorSala($sala)
    {
        return $this->executeQuery(
            "
            SELECT
                inscricao_id as inscricaoId, pes_nome as pesNome, sala_id as sala
            FROM
                selecao_inscricao
            INNER JOIN
                pessoa
            ON
                pessoa.pes_id = selecao_inscricao.pes_id
            WHERE
                selecao_inscricao.sala_id = '{$sala}' AND edicao_id = '{$this->buscaEdicaoAtual()->getEdicaoId()}'
            ORDER BY pesNome ASC
            "
        )->fetchAll();
    }

    public function buscaCandidatosBoletoAtrasado($edicao, $limit = null, $offset = null)
    {
        $sql = "
            SELECT
              DISTINCT pessoa.pes_nome as Nome,
                financeiro__titulo.titulo_data_vencimento as Vencimento,
                financeiro__titulo.titulo_estado as Situação,
                (
					SELECT con_contato FROM contato
					WHERE pessoa=pessoa.pes_id AND tipo_contato=1 AND con_contato!=''
					ORDER BY con_id DESC LIMIT 0,1
				) as 'E-mail',
                boleto.bol_id  as boleto
            FROM financeiro__titulo
            LEFT JOIN boleto ON boleto.titulo_id = financeiro__titulo.titulo_id
            INNER JOIN pessoa on financeiro__titulo.pes_id = pessoa.pes_id
            WHERE
              boleto.bol_id in (
                SELECT
                    bol_id
                FROM
                    selecao_inscricao
                INNER JOIN
                    financeiro__titulo ON financeiro__titulo.pes_id = selecao_inscricao.pes_id
                    inner join boleto on boleto.titulo_id = financeiro__titulo.titulo_id
                WHERE
                    inscricao_status = 'Solicitada' and
                    selecao_inscricao.edicao_id = $edicao and
                    date(selecao_inscricao.inscricao_data) = date(financeiro__titulo.titulo_data_processamento)
            )
            AND
              financeiro__titulo.titulo_estado = 'Aberto'
            ORDER BY boleto.bol_id DESC
            LIMIT 1";

        $sql .= ($limit) ? " LIMIT $limit" : "";

        $sql .= ($offset) ? " OFFSET $offset" : "";

        $boletos = $this->executeQuery($sql);
        if ($boletos) {
            foreach ($boletos as $boleto) {
                $array[] = $boleto;
            }

            return $array;
        }
    }

    public function buscaCandidatosInscricaoAceita($edicao, $limit = null, $offset = null)
    {
        if (!$edicao) {
            $this->setLastError('É necessário informar uma edição !');

            return false;
        }
        $aceita = "'" . self::INSCRICAO_STATUS_ACEITA . "'";

        $sql = "select
                      year(inscricao_data) as dataEdicao,
                      edicao_descricao,
                      selelocais_id 'local',
                      pes_nome as Nome,
                      inscricao_status as Situação,
                      (SELECT con_contato FROM contato
                          WHERE pessoa=pessoa.pes_id AND tipo_contato=1 AND con_contato!=''
                          ORDER BY con_id DESC LIMIT 0,1) as 'e-mail',
                      inscricao_id as Inscrição
                from selecao_inscricao
                      INNER JOIN pessoa on selecao_inscricao.pes_id = pessoa.pes_id
                      INNER JOIN selecao_edicao se on se.edicao_id=selecao_inscricao.edicao_id
                where selecao_inscricao.edicao_id =:edicao and selecao_inscricao.inscricao_status = $aceita
                ORDER BY inscricao_id
                ";

        $sql .= ($limit) ? " LIMIT $limit" : "";

        $sql .= ($offset) ? " OFFSET $offset" : "";

        $inscricoes = $this->executeQueryWithParam($sql, ['edicao' => $edicao])->fetchAll();
        if ($inscricoes) {
            foreach ($inscricoes as $inscricao) {
                $array[] = $inscricao;
            }

            return $array;
        }
    }

    public function buscaInscricaoPorSalas($salaId)
    {
        $inscricoes = $this->executeQuery(
            "
          SELECT
           *
          FROM
           selecao_inscricao
          INNER JOIN
           pessoa ON selecao_inscricao.pes_id = pessoa.pes_id
          WHERE
            edicao_id = {$this->buscaEdicaoAtual()->getEdicaoId()}
            AND
            inscricao_status = 'Aceita'
            AND
            sala_id = '{$salaId}'
          ORDER BY pes_nome"
        )->fetchAll();

        return $inscricoes;
    }

    public function buscaClassificacaoVestibular($edicao, $order = null)
    {
        if ($order) {
            $order = " ORDER BY " . $order;
        }

        $query  = <<<SQL
        select
                v.pes_nome,
                v.edicao_id,
                v.inscricao_id,
                v.pes_id,
                v.inscricao_lingua_estrangeira,
                v.ingresso,
                v.conhecimento,
                v.redacao,
                v.portugues,
                v.inscricao_nota,
                v.resultado,
                inscricao_classificacao classificacao
            from
                selecao_inscricao
	          	LEFT JOIN	
		inscricao_cursos using(inscricao_id)
            		LEFT JOIN
                view__vestibular_classificacao v on v.inscricao_id = selecao_inscricao.inscricao_id
            where v.edicao_id = {$edicao} 
            {$order}
SQL;
        $result = $this->executeQuery($query)->fetchAll();

        return $result;
    }

    /* Atenção: Função provisoria: Esta consulta deverá ser alterada ou removida ou mesclada com a buscaClassificacaoVestibular conforme a evoluão do processo de aprovação do vestibular. */
    public function buscaClassificacaoVestibular2($edicao, $situacao, $ordem = "")
    {
        $selectClassificados = "SELECT
                            *
                        FROM
                            (SELECT
                                COALESCE(classificacao_limite, 0) classificados
                            FROM
                                `selecao_edicao_fechamento`
                            WHERE
                                edicao_id = $edicao
                            ORDER BY selecaofechamento_data DESC) class
                        LIMIT 1";

        $classificados = $this->executeQuery($selectClassificados)->fetch();

        if ($situacao == 'classificado') {
            $limit     = 'limit ' . $classificados['classificados'];
            $resultado = 'classificado';
        } elseif ($situacao == 'excedente') {
            $limit     = 'limit ' . $classificados['classificados'] . ', 1000';
            $resultado = 'classificado';
        } else {
            $limit     = '';
            $resultado = 'desclassificado';
        }
        if ($ordem === 'Alfabética') {
            $orderBy = " ORDER BY pes_nome";
        }

        $select = "SELECT * FROM (SELECT *
                            FROM
                              view__vestibular_classificacao
                            WHERE
                              edicao_id = {$edicao}
                            AND
                             resultado = '{$resultado}'
                            {$limit}) class $orderBy";

        $classificacao = $this->executeQuery($select)->fetchAll();

        if ($ordem != 'Alfabética') {
            if ($situacao == 'classificado') {
                for ($i = 0; $i < count($classificacao); $i++) {
                    $classificacao[$i]['classificacao'] = $i + 1;
                }
            } elseif ($situacao == 'excedente') {
                for ($i = 0; $i < count($classificacao); $i++) {
                    $classificacao[$i]['classificacao'] = $i + $classificados['classificados'] + 1;
                }
            } else {
                for ($i = 0; $i < count($classificacao); $i++) {
                    $count                              = $this->executeQuery(
                        "SELECT count(pes_id) as classificados
                                FROM view__vestibular_classificacao
                            WHERE
                              edicao_id = {$edicao}
                            AND
                             resultado = 'classificado'
                            {$limit}"
                    )->fetchAll();
                    $classificacao[$i]['classificacao'] = $i + $count[0]['classificados'] + 1;
                }
            }
        }

        return $classificacao;
    }

    /*Este serviço deverá ser revisto após a view de classificação ter sido analisada*/
    public function buscaFormadeIngresso($pessoa)
    {
        return $this->executeQuery(
            "SELECT ingresso, MAX(edicao_id) as edicao
                                        FROM view__vestibular_classificacao
                                        WHERE pes_id = {$pessoa} "
        )->fetch();
    }

    /**
     * Retorna classificação do último vestibular feito pela pessoa
     * @param $pesId
     * @return array|null
     */
    public function retornaUltimaClassificacaoPessoaVestibular($pesId)
    {
        $query = "
        SELECT x.*, count(*) AS qtd FROM
        (
            SELECT
                NULL AS incricao_cursos_id,
                NULL AS inscricao_id,
                1 AS inscricao_curso_opcao,
                NULL AS selcursos_id,
                NULL AS inscricao_turno,
                NULL AS inscricao_resultado,
                alunoingresso_vestibular_classificacao AS inscricao_classificacao,
                alunoingresso_vestibular_pontuacao AS inscricao_nota,
                1 AS inscricao_opcao,
                concat('(Legado) Processo seletivo ',IF(alunoingresso_vestibular_semestre,concat(alunoingresso_vestibular_semestre,'/'),''),alunoingresso_vestibular_ano) AS inscricao_observacao,
                NULL AS inscricao_data_alteracao,
                NULL AS edicao_id,
                acadgeral__aluno.pes_id AS pes_id,
                concat('Processo seletivo ',IF(alunoingresso_vestibular_semestre,concat(alunoingresso_vestibular_semestre,'/'),''),alunoingresso_vestibular_ano) AS edicao_descricao,
                'Não' AS edicao_agendado,
                'legado' AS tipo
            FROM acadgeral__aluno_ingresso_legado
            INNER JOIN acadgeral__aluno_curso USING(alunocurso_id)
            INNER JOIN acadgeral__aluno USING(aluno_id)
            WHERE acadgeral__aluno.pes_id = :pes_id
            UNION ALL
            SELECT
              inscricao_cursos.*,
              edicao_id,
              pes_id,
              edicao_descricao,
              edicao_agendado,
              'sistema' AS tipo
            FROM selecao_inscricao
            INNER JOIN selecao_edicao USING(edicao_id)
            INNER JOIN inscricao_cursos USING(inscricao_id)
            WHERE pes_id = :pes_id
            ORDER BY edicao_id DESC
        ) AS x
        ORDER BY pes_id, tipo DESC";

        $stmt     = $this->executeQueryWithParam($query, array('pes_id' => $pesId));
        $arrDados = $stmt->fetch();

        if (!$arrDados) {
            $this->setLastError("Não foi encontrado processo seletivo vinculado a pessoa fornecida");

            return false;
        }

        $arrRet = array(
            'edicaoId'           => $arrDados['edicao_id'],
            'inscricaoId'        => $arrDados['inscricao_id'],
            'pesId'              => $arrDados['pes_id'],
            'inscricaoResultado' => $arrDados['inscricao_resultado'],
            'inscricaoNota'      => $arrDados['inscricao_nota'],
            'edicaoDescricao'    => $arrDados['edicao_descricao'],
            'edicaoAgendado'     => $arrDados['edicao_agendado']
        );

        return $arrRet;
    }

    public function salvarInscricao(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceSelecaoEdicao     = new \Vestibular\Service\SelecaoEdicao($this->getEm());
        $serviceSelecaoTipoEdicao = new \Vestibular\Service\SelecaoTipoEdicao($this->getEm());
        $serviceInscricaoCursos   = new \Vestibular\Service\InscricaoCursos($this->getEm());
        $serviceInfraSala         = new \Infraestrutura\Service\InfraSala($this->getEm());
        $serviceSelecaolocais     = new \Vestibular\Service\SelecaoLocais($this->getEm());
        $servicePessoaFisica      = new \Pessoa\Service\PessoaFisica($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['inscricaoId']) {
                $objSelecaoInscricao = $this->getRepository()->find($arrDados['inscricaoId']);

                if (!$objSelecaoInscricao) {
                    $this->setLastError('Registro de inscrição não existe!');

                    return false;
                }
            } else {
                $objSelecaoInscricao = new \Vestibular\Entity\SelecaoInscricao();
            }

            if ($arrDados['edicaoId']) {
                $objSelecaoEdicao = $serviceSelecaoEdicao->getRepository()->find($arrDados['edicaoId']);

                if (!$objSelecaoEdicao) {
                    $this->setLastError('Edição não existe!');

                    return false;
                }

                $objSelecaoInscricao->setEdicao($objSelecaoEdicao);
            }

            //Altera/Cria Pessoa Fisica
            $objPessoaFisica = $servicePessoaFisica->salvarPessoaFisica(
                $arrDados['pesId'],
                $arrDados,
                true,//salvar endereço
                true,//salvar Contatos
                true//salvar deficiências
            );

            $objSelecaoInscricao->setPes($objPessoaFisica->getPes());

            if ($arrDados['seletipoedicao']) {
                $objSelecaoTipoEdicao = $serviceSelecaoTipoEdicao->getRepository()->find($arrDados['seletipoedicao']);

                if (!$objSelecaoTipoEdicao) {
                    $this->setLastError('Tipo edição não existe!');

                    return false;
                }

                $objSelecaoInscricao->setSeletipoedicao($objSelecaoTipoEdicao);
            }

            if ($arrDados['salaId']) {
                $objInfraSala = $serviceInfraSala->getRepository()->find($arrDados['salaId']);

                if (!$objInfraSala) {
                    $this->setLastError('Registro de sala não existe!');

                    return false;
                }

                $objSelecaoInscricao->setSala($objInfraSala);
            } else {
                $objSelecaoInscricao->setSala(null);
            }

            if ($arrDados['selelocais']) {
                $objSelecaolocais = $serviceSelecaolocais->getRepository()->find($arrDados['selelocais']);

                if (!$objSelecaolocais) {
                    $this->setLastError('Registro de local não existe!');

                    return false;
                }

                $objSelecaoInscricao->setSelelocais($objSelecaolocais);
            } else {
                $objSelecaoInscricao->setSala(null);
            }

            $arrDados['inscricaoIp'] = $arrDados['inscricaoIp'] ? $arrDados['inscricaoIp'] : $_SERVER['REMOTE_ADDR'];

            $objSelecaoInscricao->setInscricaoData($arrDados['inscricaoData']);
            $objSelecaoInscricao->setInscricaoDataHorarioProva($arrDados['inscricaoDataHorarioProva']);
            $objSelecaoInscricao->setInscricaoIp($arrDados['inscricaoIp']);
            $objSelecaoInscricao->setInscricaoStatus($arrDados['inscricaoStatus']);
            $objSelecaoInscricao->setInscricaoGabarito($arrDados['inscricaoGabarito']);
            $objSelecaoInscricao->setInscricaoLinguaEstrangeira($arrDados['inscricaoLinguaEstrangeira']);
            $objSelecaoInscricao->setInscricaoNotaEnemCiencHumanas($arrDados['inscricaoNotaEnemCiencHumanas']);
            $objSelecaoInscricao->setInscricaoNotaEnemCiencNatureza($arrDados['inscricaoNotaEnemCiencNatureza']);
            $objSelecaoInscricao->setInscricaoNotaEnemPortugues($arrDados['inscricaoNotaEnemPortugues']);
            $objSelecaoInscricao->setInscricaoNotaEnemRedacao($arrDados['inscricaoNotaEnemRedacao']);
            $objSelecaoInscricao->setInscricaoNotaEnemMatematica($arrDados['inscricaoNotaEnemMatematica']);
            $objSelecaoInscricao->setInscricaoUltimaFormacao($arrDados['inscricaoUltimaFormacao']);
            $objSelecaoInscricao->setInscricaoDataFormacao($arrDados['inscricaoDataFormacao']);
            $objSelecaoInscricao->setInscricaoCidadeFormacao($arrDados['inscricaoCidadeFormacao']);
            $objSelecaoInscricao->setInscricaoFormacaoInstituicao($arrDados['inscricaoFormacaoInstituicao']);
            $objSelecaoInscricao->setInscricaoEntregaDoc($arrDados['inscricaoEntregaDoc']);

            $this->getEm()->persist($objSelecaoInscricao);
            $this->getEm()->flush($objSelecaoInscricao);

            if (!$serviceInscricaoCursos->salvarMultiplos($arrDados, $objSelecaoInscricao)) {
                throw new \Exception($serviceInscricaoCursos->getLastError());
            }

            $this->getEm()->commit();

            $arrDados['inscricaoId'] = $objSelecaoInscricao->getInscricaoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar inscrição!');
        }

        return false;
    }

    /**
     * @param \Financeiro\Entity\Boleto $objBoleto
     * @param \Financeiro\Entity\FinanceiroTitulo $objTitulo
     * @return bool|null|object
     */
    public function atualizaSituacaoInscricaoPorBaixaBoleto($objBoleto = null,$objTitulo = null)
    {
        if (is_object($objBoleto)) {
            $objTitulo = $objBoleto->getTitulo();
        }

        if (is_object($objTitulo)) {
            /*
            ** $edicaoAtual: Esta variável será utilizado até que seja criado a relação entre o Vestibular e o Boleto do Candidado
            */
            /** @var \Vestibular\Entity\SelecaoEdicao $edicaoAtual */
            $edicaoAtual = $this->buscaEdicaoAtual();

            if ($edicaoAtual) {
                $objInscricao = $this->getRepository($this->getEntity())->findOneBy(
                    [
                        'pes'             => $objTitulo->getPes()->getPesId(),
                        'edicao'          => $edicaoAtual->getEdicaoId(),
                        'inscricaoStatus' => $this::INSCRICAO_STATUS_SOLICITADA
                    ]
                );

                if ($objInscricao) {

                    try {
                        $this->getEm()->beginTransaction();
                        $objInscricao->setInscricaoStatus($this::INSCRICAO_STATUS_ACEITA);
                        $this->getEm()->persist($objInscricao);
                        $this->getEm()->flush($objInscricao);
                        $this->getEm()->commit();
                    } catch (\Exception $e) {
                        $this->setLastError('Não foi possível atualizar inscrição!' . $e->getMessage());

                        return false;
                    }
                } else {
                    $this->setLastError(
                        'Candidato com a inscrição pendente não encontrado.' . $this->getLastError()
                    );

                    return false;
                }
            } else {
                $this->setLastError('Processo seletivo não encontrado!');

                return false;
            }

            return $objInscricao;
        }

        $this->setLastError('Parâmetro informado para boleto é inválido!');

        return false;
    }

    public function verificaSeAprovado($pesId)
    {
        if (!$pesId) {
            $this->setLastError("Parãmetro informado está vazio!");

            return false;
        }

        $query = "SELECT
                      inscricao_resultado
                    FROM
                      inscricao_cursos
                    LEFT JOIN
                      selecao_inscricao USING (inscricao_id)
                    WHERE
                      pes_id = " . $pesId . "
                    ORDER BY
                      edicao_id DESC";

        try {
            $result = $this->executeQuery($query)->fetch();

            return $result['inscricao_resultado'];
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());
        }

        return false;
    }
}
