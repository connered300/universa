<?php


namespace Vestibular\Service;

use VersaSpine\Service\AbstractService;
use Vestibular\Service\SelecaoEdicao as Edicao;

class DashboardService extends AbstractService{
    public function __construct(\Doctrine\ORM\EntityManager $em){
        parent::__construct($em, 'Vestibular\Entity\SelecaoInscricao');
    }
    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {

    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {

    }

    public function relatorioInscTipo($edicaoAtual)
    {
        $query = "SELECT
    count(*) as res,
    (
    SELECT count(*)
	FROM
		selecao_inscricao
	where inscricao_status = 'Aceita'
	AND edicao_id = {$edicaoAtual->getEdicaoId()}
	)
    as aceita,

    (
	SELECT count(*)
	FROM
		financeiro__titulo
	NATURAL JOIN
		pessoa_fisica
	NATURAL JOIN
		selecao_inscricao
	NATURAL JOIN
		pessoa
	NATURAL JOIN
		financeiro__titulo_tipo
	natural join selecao_edicao
	where titulo_data_pagamento is null and titulo_tipo_pagamento = 'Boleto'
	and date(titulo_data_vencimento) <= date(now())
	AND edicao_id = {$edicaoAtual->getEdicaoId()} and titulo_data_processamento >= edicao_inicio_inscricao
	)
    as boleto
                   FROM
			financeiro__titulo
				NATURAL JOIN
			pessoa_fisica
				NATURAL JOIN
			selecao_inscricao
				NATURAL JOIN
			pessoa
				NATURAL JOIN
			financeiro__titulo_tipo
				natural join selecao_edicao
				where titulo_data_pagamento is null and titulo_tipo_pagamento = 'Boleto'
				and date(titulo_data_vencimento) > date(now()) AND selecao_edicao.edicao_id = {$edicaoAtual->getEdicaoId()}
				and titulo_data_processamento >= edicao_inicio_inscricao";

        return $this->executeQuery($query)->fetch();
    }
}