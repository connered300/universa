<?php


namespace Vestibular\Service;

use Doctrine\DBAL\Platforms\SQLAnywhere11Platform;
use VersaSpine\Service\AbstractService;
use Zend\Form\Element\DateTime;

class SelecaoDataRealizacao extends AbstractService
{
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Vestibular\Entity\SelecaoDataRealizacao');
    }

    public function pesquisaForJson($params)
    {
        $sql            = 'SELECT * FROM selecao_data_realizacao WHERE';
        $realizacaoData = false;
        $realizacaoId   = false;

        if ($params['q']) {
            $realizacaoData = $params['q'];
        } elseif ($params['query']) {
            $realizacaoData = $params['query'];
        }

        if ($params['realizacaoId']) {
            $realizacaoId = $params['realizacaoId'];
        }

        $parameters = array('realizacao_data' => "{$realizacaoData}%");
        $sql .= ' realizacao_data LIKE :realizacao_data';

        if ($realizacaoId) {
            $parameters['realizacao_id'] = $realizacaoId;
            $sql .= ' AND realizacao_id <> :realizacao_id';
        }

        $sql .= " ORDER BY realizacao_data";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceSelecaoEdicao = new \Vestibular\Service\SelecaoEdicao($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['realizacaoId']) {
                $objSelecaoDataRealizacao = $this->getRepository()->find($arrDados['realizacaoId']);

                if (!$objSelecaoDataRealizacao) {
                    $this->setLastError('Registro de data realização não existe!');

                    return false;
                }
            } else {
                $objSelecaoDataRealizacao = new \Vestibular\Entity\SelecaoDataRealizacao();
            }

            if ($arrDados['edicaoId']) {
                $objSelecaoEdicao = $serviceSelecaoEdicao->getRepository()->find($arrDados['edicaoId']);

                if (!$objSelecaoEdicao) {
                    $this->setLastError('Registro de edição não existe!');

                    return false;
                }

                $objSelecaoDataRealizacao->setEdicaoId($objSelecaoEdicao);
            }

            $objSelecaoDataRealizacao->setRealizacaoData($arrDados['realizacaoData']);

            $this->getEm()->persist($objSelecaoDataRealizacao);
            $this->getEm()->flush();

            $this->getEm()->commit();

            $arrDados['realizacaoId'] = $objSelecaoDataRealizacao->getRealizacaoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de data realização!');
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['edicaoId']) {
            $errors[] = 'Por favor preencha o campo "edição"!';
        }

        if (!$arrParam['realizacaoData']) {
            $errors[] = 'Por favor preencha o campo "data"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM selecao_data_realizacao";
        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($realizacaoId)
    {
        $arrDados = array();
        /** @var $objSelecaoDataRealizacao \Vestibular\Entity\SelecaoDataRealizacao */
        $objSelecaoDataRealizacao = $this->getRepository()->find($realizacaoId);
        $serviceSelecaoEdicao     = new \Vestibular\Service\SelecaoEdicao($this->getEm());

        try {
            $arrDados = $objSelecaoDataRealizacao->toArray();
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('realizacaoId' => $params['id']));
        } elseif ($params['edicao']) {
            $arrEntities = $this->getRepository()->findBy(array('edicao' => $params['edicao']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();

        /* @var $objEntity \Vestibular\Entity\SelecaoDataRealizacao */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getRealizacaoId(),
                $params['value'] => $objEntity->getRealizacaoData()->format('d/m/Y H:i'),
                'dataFormt1'     => $objEntity->getRealizacaoData()->format('d/m/Y'),
                'dataFormt2'     => $objEntity->getRealizacaoData()->format('j-n-Y'),
                'horario'        => $objEntity->getRealizacaoData()->format('H:i')
            );
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['realizacaoId']) {
            $this->setLastError('Para remover um registro de data realizacao é necessário especificar o código.');

            return false;
        }

        try {
            $objSelecaoDataRealizacao = $this->getRepository()->find($param['realizacaoId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objSelecaoDataRealizacao);
            $this->getEm()->flush($objSelecaoDataRealizacao);
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de data realização.');

            return false;
        }

        return true;
    }

    // Busca datas e horários disponíveis para realização da prova. Obs: Não retorna datas passadas, ainda que registradas.

    public function buscasDatasEHorariosProvasPorEdicao($edicaoId)
    {
        $arrayResult = array();
        $dateNow =  (new \DateTime('now'));

        if ($edicaoId) {
            $datas = $this->getRepository()->findBy(['edicao' => $edicaoId],['realizacaoData' => 'ASC']);

            if ($datas) {
                foreach ($datas as $data) {
                    $dateProva =  $data->getRealizacaoData();

                    if( $dateProva > $dateNow){
                        $arrayResult[] = [
                            //com /
                            'dataFormt1' => $data->getRealizacaoData()->format('d/m/Y'),
                            // com -
                            'dataFormt2' => $data->getRealizacaoData()->format('j-n-Y'),
                            'horario'    => $data->getRealizacaoData()->format('H:i')
                        ];
                    }
                }
            }
        }

        return $arrayResult;
    }

    public function quantidadeCandidatosPorData($data, $edicaoId){
        if(!$data){
            return false;
        }

        $query = <<<SQL
        SELECT
          count(pes_id) contagem
        FROM selecao_data_realizacao
          INNER JOIN selecao_inscricao USING(edicao_id)
        WHERE realizacao_data = date({$data})
          AND
        selecao_data_realizacao.edicao_id = {$edicaoId}
SQL;

        $result = $this->executeQuery($query)->fetch();

        return $result['contagem'];
    }
}