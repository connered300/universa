<?php


namespace Vestibular\Service;

use VersaSpine\Service\AbstractService;

class SelecaoCursos extends AbstractService
{
    const SELCURSOS_PERIODOS_MATUTINO = 'matutino';
    const SELCURSOS_PERIODOS_VESPERTINO = 'vespertino';
    const SELCURSOS_PERIODOS_NOTURNO = 'noturno';
    const SELCURSOS_PERIODOS_INTEGRAL = 'integral';
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    public function getArrSelect2SelcursosPeriodos($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getSelcursosPeriodos());
    }

    public static function getSelcursosPeriodos()
    {
        return array(
            self::SELCURSOS_PERIODOS_MATUTINO,
            self::SELCURSOS_PERIODOS_VESPERTINO,
            self::SELCURSOS_PERIODOS_NOTURNO,
            self::SELCURSOS_PERIODOS_INTEGRAL
        );
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Vestibular\Entity\SelecaoCursos');
    }

    public function pesquisaForJson($params)
    {
        $sql                    = 'SELECT * FROM selecao_cursos WHERE';
        $selcursosVagasIntegral = false;
        $selcursosId            = false;

        if ($params['q']) {
            $selcursosVagasIntegral = $params['q'];
        } elseif ($params['query']) {
            $selcursosVagasIntegral = $params['query'];
        }

        if ($params['selcursosId']) {
            $selcursosId = $params['selcursosId'];
        }

        $parameters = array('selcursos_vagas_integral' => "{$selcursosVagasIntegral}%");
        $sql .= ' selcursos_vagas_integral LIKE :selcursos_vagas_integral';

        if ($selcursosId) {
            $parameters['selcursos_id'] = $selcursosId;
            $sql .= ' AND selcursos_id <> :selcursos_id';
        }

        $sql .= " ORDER BY selcursos_vagas_integral";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceSelecaoEdicao = new \Vestibular\Service\SelecaoEdicao($this->getEm());
        $serviceCampusCurso   = new \Matricula\Service\CampusCurso($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['selcursosId']) {
                $objSelecaoCursos = $this->getRepository()->find($arrDados['selcursosId']);

                if (!$objSelecaoCursos) {
                    $this->setLastError('Registro de curso da edição não existe!');

                    return false;
                }
            } else {
                $objSelecaoCursos = new \Vestibular\Entity\SelecaoCursos();
            }

            if ($arrDados['edicaoId']) {
                /** @var $objSelecaoEdicao \Vestibular\Entity\SelecaoEdicao */
                $objSelecaoEdicao = $serviceSelecaoEdicao->getRepository()->find($arrDados['edicaoId']);

                if (!$objSelecaoEdicao) {
                    $this->setLastError('Registro de edição não existe!');

                    return false;
                }

                $objSelecaoCursos->setEdicao($objSelecaoEdicao);
            }

            if ($arrDados['cursocampusId']) {
                /** @var $objCampusCurso \Matricula\Entity\CampusCurso */
                $objCampusCurso = $serviceCampusCurso->getRepository()->find($arrDados['cursocampusId']);

                if (!$objCampusCurso) {
                    $this->setLastError('Registro de curso do câmpus não existe!');

                    return false;
                }

                $objSelecaoCursos->setCursocampus($objCampusCurso);
            }

            $objSelecaoCursos->setSelcursosPeriodos($arrDados['selcursosPeriodos']);
            $objSelecaoCursos->setSelcursosValorInscricao($arrDados['selcursosValorInscricao']);
            $objSelecaoCursos->setSelcursosVagasVespertino($arrDados['selcursosVagasVespertino']);
            $objSelecaoCursos->setSelcursosVagasMatutino($arrDados['selcursosVagasMatutino']);
            $objSelecaoCursos->setSelcursosVagasNoturno($arrDados['selcursosVagasNoturno']);
            $objSelecaoCursos->setSelcursosVagasIntegral($arrDados['selcursosVagasIntegral']);

            $this->getEm()->persist($objSelecaoCursos);
            $this->getEm()->flush();

            $this->getEm()->commit();

            $arrDados['selcursosId'] = $objSelecaoCursos->getSelcursosId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de curso da edição!' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['edicaoId']) {
            $errors[] = 'Por favor preencha o campo "edição"!';
        }

        if (!$arrParam['cursocampusId']) {
            $errors[] = 'Por favor preencha o campo "curso do câmpus"!';
        }

        if (!$arrParam['selcursosPeriodos']) {
            $errors[] = 'Por favor selecione valores válidos para o campo "turnos"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM selecao_cursos";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($selcursosId)
    {
        $arrDados = array();
        /** @var $objSelecaoCursos \Vestibular\Entity\SelecaoCursos */
        $arrDados             = $this->getRepository()->find($selcursosId);
        $serviceSelecaoEdicao = new \Vestibular\Service\SelecaoEdicao($this->getEm());
        $serviceCampusCurso   = new \Matricula\Service\CampusCurso($this->getEm());

        try {
            $arrDados                = $objSelecaoCursos->toArray();
            $arrDados['edicao']      = $objSelecaoCursos->getEdicao()->getEdicaoId();
            $arrDados['cursocampus'] = $objSelecaoCursos->getCursocampus()->getCursocampusId();
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('selcursosId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Vestibular\Entity\SelecaoCursos */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getSelcursosId(),
                $params['value'] => (
                    $objEntity->getCursocampus()->getCamp()->getCampNome() . ' - ' .
                    $objEntity->getCursocampus()->getCurso()->getCursoNome()
                )
            );
        }

        return $arrEntitiesArr;
    }

    public static function retornaPeriodosCurso(\Vestibular\Entity\SelecaoCursos $selCurso)
    {
        $arrPeriodos = array();

        if ($selCurso->getSelcursosVagasVespertino()) {
            $arrPeriodos['Vespertino'] = 'Vespertino';
        }

        if ($selCurso->getSelcursosVagasMatutino()) {
            $arrPeriodos['Matutino'] = 'Matutino';
        }

        if ($selCurso->getSelcursosVagasNoturno()) {
            $arrPeriodos['Noturno'] = 'Noturno';
        }

        if ($selCurso->getSelcursosVagasIntegral()) {
            $arrPeriodos['Integral'] = 'Integral';
        }

        return $arrPeriodos;
    }

    public function getArraySelecaoCursos($edicaoId, $selcursosId = false)
    {
        $serviceCampusCurso = new \Matricula\Service\CampusCurso($this->getEm());

        $param = ['edicao' => $edicaoId];

        if ($selcursosId) {
            $param['selcursosId'] = $selcursosId;
        }

        $arrSelecaoCursosRetorno = [];
        $arrSelecaoCursos        = $this->getRepository()->findBy($param);

        /* @var $objSelecaoCursos \Vestibular\Entity\SelecaoCursos */
        foreach ($arrSelecaoCursos as $objSelecaoCursos) {
            $arrDados = $serviceCampusCurso->pesquisaForJson(
                array('cursocampusId'           => $objSelecaoCursos->getCursocampus()->getCursocampusId(),
                      'validaCampusCursoLogado' => false
                )
            );

            $arrDados                  = $arrDados[0];
            $arrSelecaoCursosRetorno[] = array(
                'id'            => $objSelecaoCursos->getSelcursosId(),
                'selcursosId'   => $objSelecaoCursos->getSelcursosId(),
                'text'          => $arrDados['camp_nome'] . " / " . $arrDados['curso_nome'],
                'cursocampusId' => $arrDados['cursocampus_id'],
                'campId'        => $arrDados['camp_id'],
                'campNome'      => $arrDados['camp_nome'],
                'cursoId'       => $arrDados['curso_id'],
                'cursoNome'     => $arrDados['curso_nome'],
                'periodos'      => $this->retornaPeriodosCurso($objSelecaoCursos),
            );
        }

        return $arrSelecaoCursosRetorno;
    }

    public function remover($param)
    {
        if (!$param['selcursosId']) {
            $this->setLastError('Para remover um registro de cursos é necessário especificar o código.');

            return false;
        }

        try {
            $objSelecaoCursos = $this->getRepository()->find($param['selcursosId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objSelecaoCursos);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de cursos.');

            return false;
        }

        return true;
    }

    public function buscaValorIsncricaoPorSelecaoCurso($id)
    {
        if ($id) {
            $selecaoCurso = $this->getReference($id);
            if ($selecaoCurso) {
                return $selecaoCurso->getSelcursosValorInscricao();
            }
        }

        return null;
    }

    public function quantidadeCandidatosNoProcessoSeletivo($edicaoId, $cursocampusId){
        if(!$edicaoId || !$cursocampusId){
            $this->setLastError( "Por favor, selecione a edição e curso do câmpus");
            return false;
        }

        $query = <<<SQL
        SELECT count(pes_id) contagem FROM selecao_cursos INNER JOIN selecao_inscricao USING(edicao_id)
        WHERE edicao_id = {$edicaoId} AND cursocampus_id = {$cursocampusId}
SQL;

        $result = $this->executeQuery($query)->fetch();

        return $result['contagem'];
    }

    public function edita($arrDados){
        return parent::edita($arrDados);
    }
}