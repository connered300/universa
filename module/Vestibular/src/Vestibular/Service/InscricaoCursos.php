<?php

namespace Vestibular\Service;

use VersaSpine\Service\AbstractService;

/**
 * Description of InscricaoCurso
 *
 * @author versa
 */
class InscricaoCursos extends AbstractService
{
    const  INSCRICAO_RESULTADO_APROVADO = 'Aprovado';
    const  INSCRICAO_RESULTADO_DESCLASSIFICADO = 'Desclassificado';
    const  INSCRICAO_RESULTADO_AUSENTE = 'Ausente';
    const  INSCRICAO_RESULTADO_EXCEDENTE = 'Excedente';

    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * @return array
     */
    public static function getInscricaoResultado()
    {
        return array(
            self::INSCRICAO_RESULTADO_APROVADO,
            self::INSCRICAO_RESULTADO_AUSENTE,
            self::INSCRICAO_RESULTADO_DESCLASSIFICADO,
            self::INSCRICAO_RESULTADO_EXCEDENTE
        );
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2InscricaoResultado($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getInscricaoResultado());
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Vestibular\Entity\InscricaoCursos');
    }

    public function pesquisaForJson($params)
    {
        $sql              = 'SELECT * FROM inscricao_cursos WHERE';
        $inscricaoTurno   = false;
        $incricaoCursosId = false;

        if ($params['q']) {
            $inscricaoTurno = $params['q'];
        } elseif ($params['query']) {
            $inscricaoTurno = $params['query'];
        }

        if ($params['incricaoCursosId']) {
            $incricaoCursosId = $params['incricaoCursosId'];
        }

        $parameters = array('inscricao_turno' => "{$inscricaoTurno}%");
        $sql .= ' inscricao_turno LIKE :inscricao_turno';

        if ($incricaoCursosId) {
            $parameters['incricao_cursos_id'] = $incricaoCursosId;
            $sql .= ' AND incricao_cursos_id <> :incricao_cursos_id';
        }

        $sql .= " ORDER BY inscricao_turno";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function salvarInscricaoCurso(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceSelecaoInscricao = new \Vestibular\Service\SelecaoInscricao($this->getEm());
        $serviceSelecaoCursos    = new \Vestibular\Service\SelecaoCursos($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['incricaoCursosId']) {
                $objInscricaoCursos = $this->getRepository()->find($arrDados['incricaoCursosId']);

                if (!$objInscricaoCursos) {
                    $this->setLastError('Registro de curso de candidato não existe!');

                    return false;
                }
            } else {
                $objInscricaoCursos = new \Vestibular\Entity\InscricaoCursos();
            }

            if ($arrDados['inscricaoId']) {
                $objSelecaoInscricao = $serviceSelecaoInscricao->getRepository()->find($arrDados['inscricaoId']);

                if (!$objSelecaoInscricao) {
                    $this->setLastError('Registro de inscricao não existe!');

                    return false;
                }

                $objInscricaoCursos->setInscricao($objSelecaoInscricao);
            }

            $objInscricaoCursos->setInscricaoCursoOpcao($arrDados['inscricaoCursoOpcao']);

            if ($arrDados['selcursosId']) {
                $objSelecaoCursos = $serviceSelecaoCursos->getRepository()->find($arrDados['selcursosId']);

                if (!$objSelecaoCursos) {
                    $this->setLastError('Registro de cursos não existe!');

                    return false;
                }

                $objInscricaoCursos->setSelcursos($objSelecaoCursos);
            }

            $objInscricaoCursos->setInscricaoTurno($arrDados['inscricaoTurno']);

            $this->getEm()->persist($objInscricaoCursos);
            $this->getEm()->flush($objInscricaoCursos);

            $this->getEm()->commit();

            $arrDados['incricaoCursosId'] = $objInscricaoCursos->getIncricaoCursosId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de curso!');
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['inscricaoId']) {
            $errors[] = 'Por favor preencha o campo "inscrição"!';
        }

        if (!$arrParam['selcursosId']) {
            $errors[] = 'Por favor preencha o campo "curso"!';
        }

        if (!$arrParam['inscricaoTurno']) {
            $errors[] = 'Por favor preencha o campo "turno"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    public function saveNotas(array &$arrDados)
    {
        if (!$this->validaNotas($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['incricaoCursosId']) {
                /** @var $objInscricaoCursos \Vestibular\Entity\InscricaoCursos */
                $objInscricaoCursos = $this->getRepository()->find($arrDados['incricaoCursosId']);

                if (!$objInscricaoCursos) {
                    $this->setLastError('Registro de candidato não encontrado!');

                    return false;
                }
            } else {
                $objInscricaoCursos = new \Vestibular\Entity\InscricaoCursos();
            }

            if($arrDados['inscricaoNota']){
                $arrDados['inscricaoNota'] = str_replace(',','.',$arrDados['inscricaoNota']);
            }else{
                $arrDados['inscricaoNota'] = 0.00;
            }

            $objInscricaoCursos->setInscricaoNota($arrDados['inscricaoNota']);
            $objInscricaoCursos->setInscricaoResultado($arrDados['inscricaoResultado']);

            $this->getEm()->persist($objInscricaoCursos);
            $this->getEm()->flush($objInscricaoCursos);

            $this->getEm()->commit();

            $arrDados['incricaoCursosId'] = $objInscricaoCursos->getIncricaoCursosId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro do candidato!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function validaNotas($arrParam)
    {
        $errorsNotas = array();

        if (!$arrParam['inscricaoResultado']) {
            $errorsNotas[] = 'Por favor preencha o campo "Situação"!';
        }

        if($arrParam['inscricaoNota'] != 0){
            if (!$arrParam['inscricaoNota']) {
                $errorsNotas[] = 'Por favor preencha o campo "Nota"!';
            }
        }


        if (!empty($errorsNotas)) {
            $this->setLastError(implode("\n", $errorsNotas));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT 
                            inscricao_id,
                            incricao_cursos_id,
                            pes_nome,
                            edicao_descricao,
                            curso_nome,
                            inscricao_opcao,
                            inscricao_status,
                            edicao_agendado,
                            IF(inscricao_status = 'Aceita',
                                IF((inscricao_resultado = ''
                                        OR inscricao_resultado IS NULL),
                                    'Ausente',
                                    inscricao_resultado),
                                'Indefinida') inscricao_resultado,
                            IFNULL(format(inscricao_nota,2,'de_DE'), '-') inscricao_nota
                        FROM
                            inscricao_cursos
                                INNER JOIN
                            selecao_inscricao USING (inscricao_id)
                                INNER JOIN
                            pessoa USING (pes_id)
                                INNER JOIN
                            selecao_edicao USING (edicao_id)
                                INNER JOIN 
							selecao_cursos using(selcursos_id)
								INNER JOIN 
							campus_curso using(cursocampus_id)
								INNER JOIN 
							acad_curso using(curso_id)
                        GROUP BY incricao_cursos_id
                        ORDER BY selecao_edicao.edicao_id DESC, pessoa.pes_nome ASC
                        
";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArrayInscricaoCurso($inscricaoId)
    {
        $arrInscricaoCursoRetorno = [];
        $arrInscricaoCurso        = $this->getRepository()->findBy(['inscricao' => $inscricaoId]);

        $serviceSelecaoCurso = new \Vestibular\Service\SelecaoCursos($this->getEm());

        /* @var $objInscricaoCurso \Vestibular\Entity\InscricaoCursos */
        foreach ($arrInscricaoCurso as $objInscricaoCurso) {
            $objSelecaoInscricao = $objInscricaoCurso->getInscricao();
            $objSelcursos        = $objInscricaoCurso->getSelcursos();
            $selcursos           = $serviceSelecaoCurso->getArraySelecaoCursos(
                $objSelcursos->getEdicao()->getEdicaoId(),
                $objSelcursos->getSelcursosId()
            );

            $arrInscricaoCursoRetorno[] = [
                'incricaoCursosId'    => $objInscricaoCurso->getIncricaoCursosId(),
                'inscricaoId'         => $objSelecaoInscricao->getInscricaoId(),
                'pesNome'             => $objSelecaoInscricao->getPes()->getPesNome(),
                'selcursos'           => $selcursos[0],
                'selcursosId'         => $selcursos[0],
                'campusCursoNome'     => (
                    $objSelcursos->getCursocampus()->getCamp()->getCampNome() . ' - ' .
                    $objSelcursos->getCursocampus()->getCurso()->getCursoNome()
                ),
                'inscricaoCursoOpcao' => $objInscricaoCurso->getInscricaoCursoOpcao(),
                'inscricaoTurno'      => $objInscricaoCurso->getInscricaoTurno(),
            ];
        }

        return $arrInscricaoCursoRetorno;
    }

    /**
     * @param $motivoId
     * @return array
     */
    public function getArray($incricaoCursosId)
    {
        try {
            /** @var \Vestibular\Entity\InscricaoCursos $objInscricaoCurso */
            $objInscricaoCurso = $this->getRepository()->find($incricaoCursosId);

            $arrDados = $objInscricaoCurso->toArray();
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível buscar o registro do candidato!' . $e->getMessage());

            return array();
        }

        return $arrDados;
    }

    public function salvarMultiplos(array &$arrParam, \Vestibular\Entity\SelecaoInscricao $objSelecaoInscricao)
    {
        $arrExcluir = array();

        try {
            $this->getEm()->beginTransaction();

            $arrInscricaoCursos   = $arrParam['inscricaoCursos'];
            $arrInscricaoCursosDB = $this->getRepository()->findBy(
                ['inscricao' => $objSelecaoInscricao->getInscricaoId()]
            );

            /* @var $objInscricaoCurso \Vestibular\Entity\InscricaoCursos */
            foreach ($arrInscricaoCursosDB as $objInscricaoCurso) {
                $encontrado       = false;
                $IncricaoCursosId = $objInscricaoCurso->getIncricaoCursosId();

                foreach ($arrInscricaoCursos as $arrInscricaoCursoItem) {
                    if ($IncricaoCursosId == $arrInscricaoCursoItem['incricaoCursosId']) {
                        $encontrado = true;
                        break;
                    }
                }

                if (!$encontrado) {
                    $arrExcluir[$IncricaoCursosId] = $objInscricaoCurso;
                }
            }

            if ($arrExcluir) {
                foreach ($arrExcluir as $objInscricaoCurso) {
                    $this->getEm()->remove($objInscricaoCurso);
                    $this->getEm()->flush();
                }
            }

            foreach ($arrInscricaoCursos as $arrInscricaoCurso) {
                $arrInscricaoCurso['inscricaoId'] = $objSelecaoInscricao->getInscricaoId();

                if (!$this->salvarInscricaoCurso($arrInscricaoCurso)) {
                    throw new \Exception($this->getLastError());
                }
            }

            $this->getEm()->commit();

            return true;
        } catch (\Exception $e) {
            $this->getEm()->rollback();
            $this->setLastError(
                'Não foi possível salvar o registros de aluno no curso!' .
                '<br>' .
                $e->getMessage()
            );
        }

        return false;
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $view->setVariable("arrInscricaoResultado", $this->getArrSelect2InscricaoResultado());
    }
}