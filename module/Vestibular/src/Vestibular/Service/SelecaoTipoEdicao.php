<?php


namespace Vestibular\Service;

use VersaSpine\Service\AbstractService;

class SelecaoTipoEdicao extends AbstractService
{
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    public function pesquisaForJson($params)
    {
        $sql              = 'SELECT * FROM selecao_tipo_edicao NATURAL JOIN selecao_tipo WHERE';
        $tiposelNome      = false;
        $seletipoedicaoId = false;

        if ($params['q']) {
            $tiposelNome = $params['q'];
        } elseif ($params['query']) {
            $tiposelNome = $params['query'];
        }

        if ($params['seletipoedicaoId']) {
            $seletipoedicaoId = $params['seletipoedicaoId'];
        }

        $parameters = array('tiposel_nome' => "{$tiposelNome}%");
        $sql .= ' tiposel_nome LIKE :tiposel_nome';

        if ($seletipoedicaoId) {
            $parameters['seletipoedicao_id'] = $seletipoedicaoId;
            $sql .= ' AND seletipoedicao_id <> :seletipoedicao_id';
        }

        $sql .= " ORDER BY tiposel_id";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceSelecaoEdicao = new \Vestibular\Service\SelecaoEdicao($this->getEm());
        $serviceSelecaoTipo   = new \Vestibular\Service\SelecaoTipo($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['seletipoedicaoId']) {
                $objSelecaoTipoEdicao = $this->getRepository()->find($arrDados['seletipoedicaoId']);

                if (!$objSelecaoTipoEdicao) {
                    $this->setLastError('Registro de tipo edição não existe!');

                    return false;
                }
            } else {
                $objSelecaoTipoEdicao = new \Vestibular\Entity\SelecaoTipoEdicao();
            }

            if ($arrDados['edicaoId']) {
                $objSelecaoEdicao = $serviceSelecaoEdicao->getRepository()->find($arrDados['edicaoId']);

                if (!$objSelecaoEdicao) {
                    $this->setLastError('Registro de edição não existe!');

                    return false;
                }

                $objSelecaoTipoEdicao->setEdicaoId($objSelecaoEdicao);
            }

            if ($arrDados['tiposelId']) {
                $objSelecaoTipo = $serviceSelecaoTipo->getRepository()->find($arrDados['tiposelId']);

                if (!$objSelecaoTipo) {
                    $this->setLastError('Registro de tipo não existe!');

                    return false;
                }

                $objSelecaoTipoEdicao->setTiposelId($objSelecaoTipo);
            }

            $this->getEm()->persist($objSelecaoTipoEdicao);
            $this->getEm()->flush();

            $this->getEm()->commit();

            $arrDados['seletipoedicaoId'] = $objSelecaoTipoEdicao->getSeletipoedicaoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de tipo edicão!');
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['edicaoId']) {
            $errors[] = 'Por favor preencha o campo "edicão"!';
        }

        if (!$arrParam['tiposelId']) {
            $errors[] = 'Por favor preencha o campo "tipo de seleção"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM selecao_tipo_edicao NATURAL JOIN selecao_tipo";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($seletipoedicaoId)
    {
        $arrDados             = $this->getRepository()->find($seletipoedicaoId);
        $serviceSelecaoEdicao = new \Vestibular\Service\SelecaoEdicao($this->getEm());
        $serviceSelecaoTipo   = new \Vestibular\Service\SelecaoTipo($this->getEm());

        try {
            $arrDados = $arrDados->toArray();

            if ($arrDados['edicaoId']) {
                $arrEdicaoId          = [];
                $arrDados['edicaoId'] = is_array($arrDados['edicaoId']) ? $arrDados['edicaoId'] : explode(
                    ',',
                    $arrDados['edicaoId']
                );

                foreach ($arrDados['edicaoId'] as $edicaoId) {
                    /** @var $objSelecaoEdicao \Vestibular\Entity\SelecaoEdicao */
                    $objSelecaoEdicao = $serviceSelecaoEdicao->getRepository()->find($edicaoId);

                    if ($objSelecaoEdicao) {
                        $arrEdicaoId[] = array(
                            'id'   => $objSelecaoEdicao->getEdicaoId(),
                            'text' => $objSelecaoEdicao->getEdicaoDescricao()
                        );
                    }
                }

                $arrDados['edicaoId'] = $arrEdicaoId;
            }

            if ($arrDados['tiposelId']) {
                $arrTiposelId          = [];
                $arrDados['tiposelId'] = is_array($arrDados['tiposelId']) ? $arrDados['tiposelId'] : explode(
                    ',',
                    $arrDados['tiposelId']
                );

                foreach ($arrDados['tiposelId'] as $tiposelId) {
                    /** @var $objSelecaoTipo \Vestibular\Entity\SelecaoTipo */
                    $objSelecaoTipo = $serviceSelecaoTipo->getRepository()->find($tiposelId);

                    if ($objSelecaoTipo) {
                        $arrTiposelId[] = array(
                            'id'   => $objSelecaoTipo->getTiposelId(),
                            'text' => $objSelecaoTipo->getTiposelNome()
                        );
                    }
                }

                $arrDados['tiposelId'] = $arrTiposelId;
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('seletipoedicaoId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Vestibular\Entity\SelecaoTipoEdicao */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getSeletipoedicaoId(),
                $params['value'] => $objEntity->getTiposel()->getTiposelNome()
            );
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['seletipoedicaoId']) {
            $this->setLastError('Para remover um registro de tipo edição é necessário especificar o código.');

            return false;
        }

        try {
            $objSelecaoTipoEdicao = $this->getRepository()->find($param['seletipoedicaoId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objSelecaoTipoEdicao);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de tipo edicão.');

            return false;
        }

        return true;
    }

    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Vestibular\Entity\SelecaoTipoEdicao');
    }

    public function buscaTiposEdicao($edicao)
    {
        $query = "
        SELECT * FROM selecao_tipo_edicao
        NATURAL JOIN selecao_tipo
        WHERE edicao_id = {$edicao}";
        $tipos = $this->executeQuery($query)->fetchAll();

        return $tipos;
    }

    //Busca tipo de edicao aceito nos vestibulares..
    //Parametros: Id da edicao ou zero para todos cadastrados.
    public function buscaTipoEdicaoAtual($idEdicao)
    {
        $arrEntity  = $this->getRepository()->findBy(array('edicao' => $idEdicao));
        $tipoEdicao = array();
        /** @var $objSelecaoTipoEdicao \Vestibular\Entity\SelecaoTipoEdicao */
        foreach ($arrEntity as $objSelecaoTipoEdicao) {
            $tipoEdicao [$objSelecaoTipoEdicao->getSeletipoedicaoId()] = $objSelecaoTipoEdicao
                ->getTiposel()
                ->getTiposelNome();
        }

        return $tipoEdicao;
    }

    public function adicionar($dados)
    {
        $tipoEdicao = $this->executeQuery(
            "SELECT * FROM selecao_tipo_edicao WHERE edicao_id = {$dados['edicao']} AND tiposel_id = {$dados['tiposel']}"
        )->fetchAll();
        if (!$tipoEdicao) {
            return parent::adicionar($dados);
        }
    }
}