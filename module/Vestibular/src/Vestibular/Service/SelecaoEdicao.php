<?php
namespace Vestibular\Service;

use Matricula\Service\CampusCurso;
use VersaSpine\Service\AbstractService;

class SelecaoEdicao extends AbstractService
{
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Vestibular\Entity\SelecaoEdicao');
    }

    /**
     * @param $edicaoId
     * @return array|null|object
     */
    public function getArray($edicaoId)
    {
        $arrDados = $this->getRepository()->find($edicaoId);

        try {
            $arrDados = $arrDados->toArray();
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    public function adicionar($request)
    {
        /**
         * Instanciando todos os serviços utilizados dentro da action
         **/
        $selecaoDataRealizacao = new \Vestibular\Service\SelecaoDataRealizacao($this->getEm());
        $selecaoTipoEdicao     = new \Vestibular\Service\SelecaoTipoEdicao($this->getEm());
        $selecaoCursos         = new \Vestibular\Service\SelecaoCursos($this->getEm());

        parent::begin();

        try {
            /**
             * Adicionando o Processo seletivo
             * */

            if (!$request['edicaoId']) {
                $request['edicaoId'] = parent::adicionar($request);
                $edicaoId            = $request['edicaoId']->getEdicaoId();
            } else {
                $edicaoId = $request['edicaoId'];
            }

            /**
             *  Adicionando as Datas de realizaçao.
             **/
            for ($i = 0; $i < sizeof($request['edicaoDataRealizacao']); $i++) {
                $realizacao['edicao']         = $edicaoId;
                $realizacao['realizacaoId']   = "";
                $realizacao['realizacaoData'] = new \DateTime(
                    $request['edicaoDataRealizacao'][$i]->format(
                        'd-m-Y'
                    ) . " " . $request['edicaoHorarioRealizacao'][$i]
                );

                $ret = $selecaoDataRealizacao->adicionar($realizacao);
            }

            /**
             *  Adicionando os tipos de ingresso.
             **/
            foreach ($request['tiposel'] as $seleTipo) {
                $tipos['seletipoedicaoId'] = "";
                $tipos['edicao']           = $edicaoId;
                $tipos['tiposel']          = $seleTipo;
                $ret                       = $selecaoTipoEdicao->adicionar($tipos);
            }

            /** @var $objEdicao |Vestibular\Entity\SelecaoEdicao */
            $objEdicao = $request['edicaoId'];

            if (empty($request['predio'])) {
                $this->setLastError("Nenhum local para realização do mesmo informado!");

                return false;
            }

            if (!is_array($request['predio'])) {
                $request['predio'] = explode(',', $request['predio']);
            }

            foreach ($request['predio'] as $predio) {
                $unidadeId = null;
                $predioId  = substr($predio, 1);

                if (substr($predio, 0, 1) == 'u') {
                    $unidadeId = substr($predio, 1);
                    $predioId  = null;
                }


                $arrayPredios['selelocaisId']  = null;
                $arrayPredios['edicao']        = $objEdicao->getEdicaoId();
                $arrayPredios['predio']        = $predioId;
                $arrayPredios['unidadeEstudo'] = $unidadeId;

                $ret = (new \Vestibular\Service\SelecaoLocais($this->getEm()))->adicionar($arrayPredios);
            }

            /**
             * Adicionando Cursos.
             * */
            foreach ($request['campusCurso'] as $key => $campusCurso) {
                $cursos['selcursosId'] = "";
                $cursos['cursocampus'] = $campusCurso;
                //                    $cursos['selcursosPeriodos'] = "matutino,vespertino,noturno";
                $cursos['selcursosVagasMatutino']   = $request['vagasMatutino'][$key];
                $cursos['selcursosVagasVespertino'] = $request['vagasVespertino'][$key];
                $cursos['selcursosVagasNoturno']    = $request['vagasNoturno'][$key];
                $cursos['selcursosValorInscricao']  = str_replace(',', '.', $request['selcursosValorInscricao']);
                $cursos['edicao']                   = $edicaoId;

                $ret = $selecaoCursos->adicionar($cursos);
            }
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
        }

        parent::commit();
    }

    public function edita(array $dados)
    {
        parent::begin();
        $selecaoCursos         = new \Vestibular\Service\SelecaoCursos($this->getEm());
        $selecaoDataRealizacao = new \Vestibular\Service\SelecaoDataRealizacao($this->getEm());
        $error                 = array(); //Armazena todas as exceptions encontradas durante a execução

        try {
            /**
             * Editando o processo seletivo.
             * */
            $edicao = parent::edita($dados);

            /**
             * Excluindo os cursos.
             * */
            if (isset($dados['excluirCursos'])) {
                foreach ($dados['excluirCursos'] as $selCurso) {
                    $selecaoCursos->excluir($selCurso);
                }
            }

            /**
             * Excluindo as Datas.
             * */
            if (isset($dados['datasExcluir'])) {
                foreach ($dados['datasExcluir'] as $dataExcluir) {
                    $selecaoDataRealizacao->excluir($dataExcluir);
                }
            }

            /**
             * Editando as Datas.
             * */
            foreach ($dados['datasId'] as $key => $dataId) {
                $dataEditar = [
                    'realizacaoId'   => $dataId,
                    'realizacaoData' => $dados['edicaoDataRealizacao'][$key],
                    'edicao'         => $dados['edicaoId'],
                ];
                $selecaoDataRealizacao->edita($dataEditar);
            }

            /**
             * Adicionando as novas datas de realização inseridas.
             * */
            if (isset($dados['edicaoDataRealizacaoAdicionar'])) {
                for ($i = 0; $i < sizeof($dados['edicaoDataRealizacaoAdicionar'][$i]); $i++) {
                    $adicionar = [
                        'realizacaoId'   => '',
                        'realizacaoData' => new \DateTime(
                            $selecaoDataRealizacao->formatDateAmericano(
                                $dados['edicaoDataRealizacaoAdicionar'][$i]
                            ) . " " . $dados['edicaoHorarioRealizacaoAdicionar'][$i]
                        ),
                        'edicao'         => $dados['edicaoId'],
                    ];
                    $selecaoDataRealizacao->adicionar($adicionar);
                }
            }

            /**
             * Adicionando os novos cursos.
             * */
            if (isset($dados['campusCursoAdicionar'])) {
                foreach ($dados['campusCursoAdicionar'] as $key => $campusCurso) {
                    $cursos['selcursosId']              = "";
                    $cursos['cursocampus']              = $campusCurso;
                    $cursos['selcursosVagasMatutino']   = (int)$dados['vagasMatutinoAdicionar'][$key];
                    $cursos['selcursosVagasVespertino'] = (int)$dados['vagasVespertinoAdicionar'][$key];
                    $cursos['selcursosVagasNoturno']    = (int)$dados['vagasNoturnoAdicionar'][$key];
                    $cursos['edicao']                   = $dados['edicaoId'];
                    $cursos['selcursosValorInscricao']  = str_replace(',', '.', $dados['selcursosValorInscricao']);
                    $selecaoCursos->adicionar($cursos);
                }
            }

            /**
             * Editando os cursos.
             * */
            foreach ($dados['selCurso'] as $key => $selCurso) {
                $curso = [
                    'selcursosId'              => $selCurso,
                    'edicao'                   => $dados['edicaoId'],
                    'cursocampus'              => $dados['campusCurso'][$key],
                    'selcursosVagasVespertino' => (int)$dados['vagasVespertino'][$key],
                    'selcursosVagasMatutino'   => (int)$dados['vagasMatutino'][$key],
                    'selcursosVagasNoturno'    => (int)$dados['vagasNoturno'][$key],
                    'selcursosValorInscricao'  => str_replace(',', '.', $dados['selcursosValorInscricao']),
                ];

                $selcursoPeriodos = array();

                if ($curso['selcursosVagasMatutino'] > 0) {
                    $selcursoPeriodos[] = "matutino";
                }

                if ($curso['selcursosVagasNoturno'] > 0) {
                    $selcursoPeriodos[] = "noturno";
                }

                if ($curso['selcursosVagasVespertino'] > 0) {
                    $selcursoPeriodos[] = "integral";
                }

                $curso['selcursosPeriodos'] = implode(',', $selcursoPeriodos);

                $selecaoCursos->edita($curso);
            }

            /**
             * Tipos de ingresso.
             * */
            $seleTipoEdicaoService = new \Vestibular\Service\SelecaoTipoEdicao($this->getEm());
            $tiposExistentes       = $seleTipoEdicaoService->buscaTiposEdicao($dados['edicaoId']);

            foreach ($tiposExistentes as $tipo) {
                $seletipo[] = (int)$tipo['tiposel_id'];
            }

            //A diferença do que ta cadastrado para o que veio.
            if (isset($dados['tiposel'])) {
                $tiposDiferentes = array_diff($seletipo, $dados['tiposel']);
            } else { // Se não veio nada da view, todos os tipos são diferentes.
                $tiposDiferentes = $seletipo;
            }

            /**
             * Excluindo os que foram removidos
             * */
            foreach ($tiposDiferentes as $dif) {
                try {
                    $this->exec(
                        "DELETE FROM selecao_tipo_edicao WHERE edicao_id = {$dados['edicaoId']} AND tiposel_id = {$dif}"
                    );
                } catch (\Exception $ex) {
                    $tipoErro = $seleTipoEdicaoService->getRepository("Vestibular\Entity\SelecaoTipo")->findOneBy(
                        ['tiposelId' => $dif]
                    );
                    $error[]  = "Não foi possível excluir o tipo de ingresso '" . $tipoErro->getTiposelNome(
                        ) . "' porque já existem inscrições vinculadas à este tipo.";
                }
            }

            foreach ($dados['tiposel'] as $tiposel) {
                $tipoInserir = [
                    'seletipoedicaoId' => '',
                    'edicao'           => $dados['edicaoId'],
                    'tiposel'          => $tiposel
                ];
                $ret         = $seleTipoEdicaoService->adicionar($tipoInserir);
            }

            /**
             * Locais de realização das provas.
             * */
            $this->exec("DELETE FROM selecao_locais WHERE edicao_id = {$dados['edicaoId']}");

            if (empty($dados['predio'])) {
                $this->setLastError("Nenhum local para realização do mesmo informado!");

                return false;
            }

            if (!is_array($dados['predio'])) {
                $dados['predio'] = explode(',', $dados['predio']);
            }

            foreach ($dados['predio'] as $predio) {
                $unidadeId = null;
                $predioId  = substr($predio, 1);

                if (substr($predio, 0, 1) == 'u') {
                    $unidadeId = substr($predio, 1);
                    $predioId  = null;
                }

                $arrayPredios['selelocaisId']  = null;
                $arrayPredios['edicao']        = $dados['edicaoId'];
                $arrayPredios['predio']        = $predioId;
                $arrayPredios['unidadeEstudo'] = $unidadeId;

                $ret = (new \Vestibular\Service\SelecaoLocais($this->getEm()))->adicionar($arrayPredios);
            }
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
        }

        parent::commit();

        return array(
            'entity' => $edicao,
            'error'  => $error
        );
    }

    protected function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['edicaoInicioInscricao']) {
            $errors[] = 'Por favor preencha o campo "data de início das inscrições"!';
        }

        if (!$arrParam['edicaoFimInscricao']) {
            $errors[] = 'Por favor preencha o campo "data de término das inscrições"!';
        }

        if (!$arrParam['edicaoEdital']) {
            $errors[] = 'Por favor preencha o campo "edicaoEdital"!';
        }

        if (!$arrParam['confcont']) {
            $errors[] = 'Por favor preencha o campo "conta bancária"!';
        }

        if (!$arrParam['edicaoSemestre']) {
            $errors[] = 'Por favor preencha o campo "semestre"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function pesquisaForJson($params)
    {
        $sql             = 'SELECT * FROM selecao_edicao WHERE';
        $edicaoDescricao = false;
        $edicaoId        = false;

        if ($params['q']) {
            $edicaoDescricao = $params['q'];
        } elseif ($params['query']) {
            $edicaoDescricao = $params['query'];
        }

        if ($params['edicaoId']) {
            $edicaoId = $params['edicaoId'];
        }

        $parameters = array('edicao_descricao' => "{$edicaoDescricao}%");
        $sql .= ' edicao_descricao LIKE :edicao_descricao';

        if ($edicaoId) {
            $parameters['edicao_id'] = $edicaoId;
            $sql .= ' AND edicao_id <> :edicao_id';
        }

        $sql .= " ORDER BY edicao_descricao";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function pagination($page = 0, $is_json = false)
    {
        $dados                   = array();
        $dados['count']          = $this->executeQuery("SELECT COUNT(*) as row  FROM selecao_edicao")->fetch()['row'];
        $selecaoInscricaoService = new \Vestibular\Service\SelecaoInscricao($this->getEm());
        $edicaoAtual             = $selecaoInscricaoService->buscaEdicaoAtual();

        if ($edicaoAtual) {
            $dados['dados'] = $this->executeQuery(
                "SELECT * FROM selecao_edicao WHERE edicao_id = '{$edicaoAtual->getEdicaoId()}'"
            )->fetchAll();

            foreach ($dados['dados'] as $key => $edicao) {
                $dados['dados'][$key]['edicao_inicio_inscricao'] = (
                new \DateTime(
                    $edicao['edicao_inicio_inscricao']
                )
                )->format('d/m/Y');
                $dados['dados'][$key]['edicao_fim_inscricao']    = (
                new \DateTime(
                    $edicao['edicao_fim_inscricao']
                )
                )->format('d/m/Y');
                $dados['dados'][$key]['edicao_data_divulgacao']  = (
                new \DateTime(
                    $edicao['edicao_data_divulgacao']
                )
                )->format('d/m/Y');
            }
        }

        if ($is_json) {
            $dados['dados'] = $this->paginationFormatJson($dados['dados']);
        }

        return $dados;
    }

    public function buscaEdicaoData($data)
    {
        //        $data = "2015-05-12";
        $query  = "
          SELECT
            edicao_id
          FROM
            selecao_edicao as edicao
          WHERE
            edicao.edicao_inicio_inscricao <= '{$data}' AND edicao.edicao_fim_inscricao >= '{$data}'
        ";
        $result = $this->executeQuery($query);
        $array  = $result->fetch();

        return $array['edicao_id'];
    }

    public function buscaUltimaEdicao()
    {
        $sql = '
        SELECT e FROM \Vestibular\Entity\SelecaoEdicao e ORDER BY e.edicaoFimInscricao DESC';

        $objSelecaoEdicao = $this->getEm()
            ->createQuery($sql)
            ->setMaxResults(1)
            ->getResult();

        return $objSelecaoEdicao[0];
    }

    public function resultSearch($post)
    {
        $conditions = (new \VersaSpine\ListGenerator\Annotations\ReaderQuerys($this->getEntity()))->getConditions();
        $metadados  = $this->getMetadados();

        $tableOrig = $metadados->table['name'];
        $chaveOrig = $metadados->identifier[0];

        $sql        = " SELECT t0.* FROM {$tableOrig} t0 ";
        $chaves     = array_keys($post);
        $apelido    = 0;
        $where      = " WHERE ";
        $whereJoins = "";
        $joins      = "";

        foreach ($chaves as $input) {
            // verifica se esta nos campos simples

            if (isset($metadados->fieldMappings[$input]) && !empty($post[$input])) {
                $e = $metadados->fieldMappings[$input];

                if ($post[$e['fieldName']]) {
                    if ($conditions[$e['columnName']]['type'] == "LIKE") {
                        $where .= " t0.{$e['columnName']} LIKE '{$post[$e['fieldName']]}' AND";
                    } else {
                        if ($conditions[$e['columnName']]['type'] == "LIKE%") {
                            $where .= " t0.{$e['columnName']} LIKE '%{$post[$e['fieldName']]}%' AND";
                        } else {
                            if ($conditions[$e['columnName']]['type'] == "=") {
                                $where .= " t0.{$e['columnName']} = '{$post[$e['fieldName']]}' AND";
                            }
                        }
                    }
                }
            } else {
                if (isset($metadados->associationMappings[$input]) && !empty($post[$input])) {
                    $fj = $metadados->associationMappings[$input];
                    $apelido++;
                    $entityTarget = $this->em->getClassMetadata($fj['targetEntity']);

                    $s              = str_replace("Entity", "Service", $fj['targetEntity']);
                    $labelInnerJoin = (new $s($this->em))->getLabelEntity();
                    $tableInnerJoin = $entityTarget->table['name'];

                    $nomeCampoTable = $fj['joinColumns'][0]['name'];
                    $chaveInnerJoin = $entityTarget->fieldMappings[$fj['sourceToTargetKeyColumns'][$nomeCampoTable]]['fieldName'];

                    $labelPsr0 = $this->strToPsr0($labelInnerJoin);

                    if (isset($entityTarget->fieldMappings[$labelPsr0])) {
                        $tipoLabelDestino = $entityTarget->fieldMappings[$labelPsr0]['type'];
                    } else {
                        if (isset($entityTarget->associationMappings[$labelPsr0])) {
                            die('A desenvolver');
                        }
                    }

                    $t = "t$apelido";

                    $joins .= " INNER JOIN $tableInnerJoin $t ON t0.$chaveOrig = $t.$chaveInnerJoin";

                    if ($conditions[$nomeCampoTable]['type'] == "LIKE") {
                        $whereJoins .= " $t.$labelInnerJoin LIKE '{$post[$input]}' AND";
                    } else {
                        if ($conditions[$nomeCampoTable]['type'] == "LIKE%") {
                            $whereJoins .= " $t.$labelInnerJoin LIKE '%{$post[$input]}%' AND";
                        } else {
                            if ($conditions[$nomeCampoTable]['type'] == "=") {
                                $whereJoins .= " $t.$labelInnerJoin = '{$post[$input]}' AND";
                            }
                        }
                    }
                }
            }
        }
        $query = substr($sql . $joins . $where . $whereJoins, 0, -3);

        $dados = $this->executeQuery($query)->fetchAll();

        foreach ($dados as $key => $edicao) {
            $dados[$key]['edicao_inicio_inscricao'] = (new \DateTime($edicao['edicao_inicio_inscricao']))->format(
                'd/m/Y'
            );
            $dados[$key]['edicao_fim_inscricao']    = (new \DateTime($edicao['edicao_fim_inscricao']))->format('d/m/Y');
            $dados[$key]['edicao_data_divulgacao']  = (new \DateTime($edicao['edicao_data_divulgacao']))->format(
                'd/m/Y'
            );
        }

        return array(
            'dados' => $dados,
            'count' => count($dados)
        );
    }

    public function buscaEdicaoAtual()
    {
        $date = (new \DateTime('now'))->format('Y-m-d');

        $sql = '
        SELECT e FROM \Vestibular\Entity\SelecaoEdicao e
        WHERE :curdate >= e.edicaoInicioInscricao and :curdate >= e.edicaoFimInscricao
        ORDER BY e.edicaoFimInscricao DESC';

        $objSelecaoEdicao = $this->getEm()
            ->createQuery($sql)
            ->setParameter('curdate', $date)
            ->setMaxResults(1)
            ->getResult();

        return $objSelecaoEdicao[0];
    }

    public static function verificaVestibularFinalizado(\Vestibular\Entity\SelecaoEdicao $objSelecaoEdicao)
    {
        $date = (new \DateTime('now'))->format('Ymd');

        return $date > $objSelecaoEdicao->getEdicaoFimInscricao()->format('Ymd');
    }

    public function retornaConfiguracoesEdicao($selecaoEdicao)
    {
        $objEdicao = $selecaoEdicao;

        if (!is_object($objEdicao)) {
            $objEdicao = $this->getRepository()->find($selecaoEdicao);
        }

        $serviceCursos                = new \Vestibular\Service\SelecaoCursos($this->getEm());
        $serviceLocais                = new \Vestibular\Service\SelecaoLocais($this->getEm());
        $serviceTipoEdicao            = new \Vestibular\Service\SelecaoTipoEdicao($this->getEm());
        $serviceProvaEdicao           = new \Provas\Service\ProvaEdicao($this->getEm());
        $serviceDataRealizacao        = new \Vestibular\Service\SelecaoDataRealizacao($this->getEm());
        $serviceNecessidadesEspeciais = new \Pessoa\Service\NecessidadesEspeciais($this->getEm());

        $arrRetorno = array(
            'selecaoCursos'  => $serviceCursos->getArraySelecaoCursos($objEdicao->getEdicaoId()),
            'edicaoFiliacao' => ($objEdicao->getEdicaoFiliacao() == 'Sim') ? true : false,
            'edicaoIsencao'  => ($objEdicao->getEdicaoIsencao() == 'Sim') ? true : false,
        );

        $edicaoNumCursosOpcionais = $objEdicao->getEdicaoNumCursosOpcionais() + 1;

        if ($edicaoNumCursosOpcionais > count($arrRetorno['selecaoCursos'])) {
            $edicaoNumCursosOpcionais = count($arrRetorno['selecaoCursos']);
        }

        $arrRetorno['edicaoNumCursosOpcionais'] = $edicaoNumCursosOpcionais;

        if ($objEdicao->getEdicaoAgendado() == 'Sim') {
            $datas = $serviceDataRealizacao->getArrSelect2(['edicao' => $objEdicao->getEdicaoId()]);

            $arrRetorno['agendado']          = true;
            $arrRetorno['arrDataRealizacao'] = $datas;
            $arrRetorno['dataInicioInsc']    = $objEdicao->getEdicaoInicioInscricao();
            $arrRetorno['dataFimInsc']       = $objEdicao->getEdicaoFimInscricao();
        }

        $arrRetorno['provaLocais']           =
            $serviceLocais->pesquisaForJson(['edicaoId' => $objEdicao->getEdicaoId()]);
        $arrRetorno['provaLocais']           = $serviceLocais->retornaSelect2($arrRetorno['provaLocais']);
        $arrRetorno['linguaEstrangeira']     =
            $serviceProvaEdicao->buscaProvaEstrangeiraNaEdicao($objEdicao->getEdicaoId());
        $arrRetorno['tipoEdicao']            = $serviceTipoEdicao->buscaTipoEdicaoAtual($objEdicao->getEdicaoId());
        $arrRetorno['necessidadesEspeciais'] = $serviceNecessidadesEspeciais->getArrSelect2();

        return $arrRetorno;
    }

    public function search($data = null)
    {
        $query = <<<SQL
SELECT
  DISTINCT CONCAT(edicao_ano,'/',if(edicao_semestre = 'Segundo',2,1)) descricao,
           ifnull(ultimo.encerramento,' - ') ultimo,
           sel.edicao_id id,
           DATE_FORMAT(edicao_inicio_inscricao,'%d/%m/%Y') inicio,
           DATE_FORMAT(edicao_fim_inscricao,'%d/%m/%Y') termino,
           DATE_FORMAT(edicao_data_divulgacao,'%d/%m/%Y') divulgacao,
           IFNULL(aceitos.qtd,0) as solicitado,
           IFNULL(solicitadas.qtd,0) as aceito,
           (IFNULL(aceitos.qtd,0) + IFNULL(solicitadas.qtd,0)) total
FROM
  selecao_edicao sel
  LEFT JOIN
  (
    SELECT
      edicao_id,
      DATE_FORMAT(MAX(selecaofechamento_data), '%d/%m/%Y') as encerramento
    FROM selecao_edicao_fechamento
    GROUP BY edicao_id
    ORDER BY selecaofechamento_data ASC
  )ultimo on ultimo.edicao_id = sel.edicao_id
  LEFT JOIN
  (
    SELECT
      edicao_id,
      count(inscricao_id) qtd
    FROM
      selecao_inscricao
    WHERE
      inscricao_status not in ('Aceita')
    GROUP BY
      edicao_id
  )as aceitos on aceitos.edicao_id = sel.edicao_id
  LEFT JOIN
  (
    SELECT
      edicao_id,
      count(inscricao_id) qtd
    FROM
      selecao_inscricao
    WHERE
      inscricao_status not in ('Solicitada')
    GROUP BY
      edicao_id
  )as solicitadas on solicitadas.edicao_id = sel.edicao_id
SQL;

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function edita2($arrDados)
    {
        if (!$arrDados['edicaoId']) {
            $this->setLastError("Por favor preencha o código da edição!");

            return false;
        }
        /** @var \Vestibular\Entity\SelecaoEdicao $objSelecaoEdicao */
        $objSelecaoEdicao = $this->getRepository()->find($arrDados['edicaoId']);

        try {
            $serviceSelecaoCursos = new SelecaoCursos($this->getEm());

            /** @var \Vestibular\Entity\SelecaoDataRealizacao $objDataRealizacao */
            $arrObjDataRealizacao = $this->getRepository('Vestibular\Entity\SelecaoDataRealizacao')->findBy(
                array('edicao' => $arrDados['edicaoId'])
            );

            $this->tratarEsalvarDataRealizacao($arrObjDataRealizacao, $arrDados, $objSelecaoEdicao);

            $arrSelecaoEdicao = $objSelecaoEdicao->toArray();

            foreach ($arrSelecaoEdicao as $chave => $valor) {
                $arrSelecaoEdicao[$chave] = $arrDados[$chave];
            }

            if (!$this->save($arrSelecaoEdicao)) {
                $this->setLastError("Não foi possível salvar a edição do vestibular: " . $this->getLastError());

                return false;
            }

            $arrCampusCurso = array_unique($arrDados['campusCurso']);

            if ($arrDados['excluirCursos']) {
                foreach ($arrDados['excluirCursos'] as $campusCursoExcluir) {
                    $objSelecaoCurso = $serviceSelecaoCursos->getRepository()->findOneBy(
                        [
                            'edicao'      => $arrDados['edicaoId'],
                            'cursocampus' => $campusCursoExcluir
                        ]
                    );

                    $qtd = $serviceSelecaoCursos->quantidadeCandidatosNoProcessoSeletivo(
                        $arrDados['edicaoId'],
                        $campusCursoExcluir
                    );

                    if ($qtd != 0) {
                        try {
                            $serviceSelecaoCursos->remover($objSelecaoCurso->toArray());
                            continue;
                        } catch (\Exception $e) {
                            $this->setLastError("Não foi possível remover o curso" . $e->getMessage());
                        }
                    } else {
                        $this->setLastError("O curso não será removido pois possui candidatos inscritos!");

                        return false;
                    }
                }
            }

            $arrObjSelecaoCursos = $this->getRepository('Vestibular\Entity\SelecaoCursos')->findBy(
                array('edicao' => $arrDados['edicaoId'])
            );

            if ($arrCampusCurso) {
                foreach ($arrObjSelecaoCursos as $objSelecaoCurso) {
                    foreach ($arrCampusCurso as $index => $campusCurso) {
                        $arrSelecaoCursos                  = $this->retornaArrSelecaoCursosTratada(
                            $arrDados,
                            $index
                        );
                        $arrSelecaoCursos['cursocampusId'] = $campusCurso;
                        $arrSelecaoCursos['cursocampus']   = $campusCurso;
                        $arrSelecaoCursos['edicaoId']      = $objSelecaoEdicao->getEdicaoId();
                        $arrSelecaoCursos['edicao']        = $objSelecaoEdicao;

                        if ($campusCurso != $objSelecaoCurso->getCursocampus()->getCursocampusId()) {
                            try {
                                $serviceSelecaoCursos->save($arrSelecaoCursos);
                            } catch (\Exception $e) {
                                $this->setLastError(
                                    "Não foi possível salvar o registro de cursos. " . $e->getMessage()
                                );

                                return false;
                            }
                        } else {
                            $arrSelecaoCursos['selcursosId'] = $objSelecaoCurso->getSelcursosId();
                            try {
                                $serviceSelecaoCursos->edita($arrSelecaoCursos);
                            } catch (\Exception $e) {
                                $this->setLastError(
                                    "Não foi possível salvar o registro de cursos. " . $e->getMessage()
                                );

                                return false;
                            }
                        }
                    }
                }

                if (!$arrObjSelecaoCursos && $arrCampusCurso) {
                    foreach ($arrCampusCurso as $index => $campusCurso) {
                        $arrSelecaoCursos                  = $this->retornaArrSelecaoCursosTratada(
                            $arrDados,
                            $index
                        );
                        $arrSelecaoCursos['cursocampusId'] = $campusCurso;
                        $arrSelecaoCursos['cursocampus']   = $campusCurso;
                        $arrSelecaoCursos['edicaoId']      = $objSelecaoEdicao->getEdicaoId();
                        $arrSelecaoCursos['edicao']        = $objSelecaoEdicao;

                        try {
                            $serviceSelecaoCursos->save($arrSelecaoCursos);
                        } catch (\Exception $e) {
                            $this->setLastError(
                                "Não foi possível salvar o registro de cursos. " . $e->getMessage()
                            );

                            return false;
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            $this->setLastError("Não foi possível editar o vestibular." . $e->getMessage());

            return false;
        }

        return true;
    }

    public function retornaArrSelecaoCursosTratada($arrDados, $index)
    {
        if (!$arrDados) {
            return false;
        }

        $serviceSelecaoCursos = new SelecaoCursos($this->getEm());

        $arrSelecaoCursos['selcursosVagasMatutino']   = $arrDados['vagasMatutino'][$index];
        $arrSelecaoCursos['selcursosVagasVespertino'] = $arrDados['vagasVespertino'][$index];
        $arrSelecaoCursos['selcursosVagasNoturno']    = $arrDados['vagasNoturno'][$index];
        $arrSelecaoCursos['selcursosVagasIntegral']   = $arrDados['vagasIntegral'][$index];
        $arrSelecaoCursos['selcursosValorInscricao']  = $arrDados['selcursosValorInscricao'][$index];

        if ($arrSelecaoCursos['selcursosVagasMatutino']) {
            $arrSelecaoCursos['selcursosPeriodos'][] = $serviceSelecaoCursos::SELCURSOS_PERIODOS_MATUTINO;
        }

        if ($arrSelecaoCursos['selcursosVagasVespertino']) {
            $arrSelecaoCursos['selcursosPeriodos'][] = $serviceSelecaoCursos::SELCURSOS_PERIODOS_VESPERTINO;
        }

        if ($arrSelecaoCursos['selcursosVagasNoturno']) {
            $arrSelecaoCursos['selcursosPeriodos'][] = $serviceSelecaoCursos::SELCURSOS_PERIODOS_NOTURNO;
        }

        if ($arrSelecaoCursos['selcursosVagasIntegral']) {
            $arrSelecaoCursos['selcursosPeriodos'][] = $serviceSelecaoCursos::SELCURSOS_PERIODOS_INTEGRAL;
        }

        return $arrSelecaoCursos;
    }

    public function tratarEsalvarDataRealizacao(&$arrObjDataRealizacao, &$arrDados, $objSelecaoEdicao)
    {
        $serviceDataRealizacao = new SelecaoDataRealizacao($this->getEm());

        if ($arrObjDataRealizacao && $arrDados['edicaoDataRealizacao']) {
            foreach ($arrDados['edicaoDataRealizacao'] as $dataRealizacao) {
                foreach ($arrObjDataRealizacao as $objDataRealizacao) {
                    if ($objDataRealizacao->getRealizacaoData() == $dataRealizacao) {
                        continue;
                    }

                    $arrDataRealizacao           = $objDataRealizacao->toArray();
                    $arrDataRealizacao['edicao'] = $objSelecaoEdicao;

                    if ($dataRealizacao) {
                        $arrDataRealizacao['realizacaoData'] = $dataRealizacao;
                    } else {
                        $qtd = $serviceDataRealizacao->quantidadeCandidatosPorData(
                            $arrDataRealizacao['realizacaoData'],
                            $arrDados['edicaoId']
                        );

                        if ($qtd == 0) {
                            $serviceDataRealizacao->remover($arrDataRealizacao);
                        }

                        continue;
                    }

                    try {
                        $serviceDataRealizacao->save($arrDataRealizacao);
                    } catch (\Exception $e) {
                        $this->setLastError("Não foi possível salvar a data de realização!" . $e->getMessage());

                        return false;
                    }
                }
            }
        } elseif (!$arrDados['edicaoDataRealizacao'] && $arrObjDataRealizacao) {
            foreach ($arrObjDataRealizacao as $dataRealizacao) {
                $arrDataRealizacao = $dataRealizacao->toArray();
                $qtd               = $serviceDataRealizacao->quantidadeCandidatosPorData(
                    $arrDataRealizacao['realizacaoData'],
                    $arrDados['edicaoId']
                );

                if ($qtd == 0) {
                    try {
                        $serviceDataRealizacao->remover($arrDataRealizacao);
                    } catch (\Exception $e) {
                        $this->setLastError("Não foi possível remover a data de realização!" . $e->getMessage());

                        return false;
                    }
                }
            }
        }

        return true;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceSelecaoEdicao = new \Vestibular\Service\SelecaoEdicao($this->getEm());
        $serviceConfCont      = new \Financeiro\Service\BoletoConfConta($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['edicaoId']) {
                /** @var $objSelecaoEdicao \Vestibular\Entity\SelecaoEdicao */
                $objSelecaoEdicao = $serviceSelecaoEdicao->getRepository()->find($arrDados['edicaoId']);

                if (!$objSelecaoEdicao) {
                    $this->setLastError('Registro de edição não existe!');

                    return false;
                }
            } else {
                $objSelecaoEdicao = new \Vestibular\Entity\SelecaoEdicao();
            }

            if ($arrDados['confcont']) {
                /** @var \Financeiro\Entity\BoletoConfConta $objConfCont */
                $objConfCont = $serviceConfCont->getRepository()->find($arrDados['confcont']);

                if (!$objConfCont) {
                    $this->setLastError('Registro de configuração de conta não existe!');

                    return false;
                }

                $objSelecaoEdicao->setConfcont($objConfCont);
            } else {
                $objSelecaoEdicao->setConfcont(null);
            }

            $objSelecaoEdicao->setEdicaoAno($arrDados['edicaoAno']);
            $objSelecaoEdicao->setEdicaoEdital($arrDados['edicaoEdital']);
            $objSelecaoEdicao->setEdicaoIsencao($arrDados['edicaoIsencao']);
            $objSelecaoEdicao->setEdicaoSemestre($arrDados['edicaoSemestre']);
            $objSelecaoEdicao->setEdicaoAgendado($arrDados['edicaoAgendado']);
            $objSelecaoEdicao->setEdicaoFiliacao($arrDados['edicaoFiliacao']);
            $objSelecaoEdicao->setEdicaoDescricao($arrDados['edicaoDescricao']);
            $objSelecaoEdicao->setEdicaoFimInscricao($arrDados['edicaoFimInscricao']);
            $objSelecaoEdicao->setEdicaoDataDivulgacao($arrDados['edicaoDataDivulgacao']);

            $this->getEm()->persist($objSelecaoEdicao);
            $this->getEm()->flush($objSelecaoEdicao);

            $this->getEm()->commit();

            $arrDados['edicaoId'] = $objSelecaoEdicao->getEdicaoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de curso da edição!' . $e->getMessage());
        }

        return false;
    }
}
