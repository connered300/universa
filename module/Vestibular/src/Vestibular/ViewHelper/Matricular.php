<?php


namespace Vestibular\ViewHelper;


use Zend\View\Helper\HelperInterface;
use Zend\View\Renderer\RendererInterface as Renderer;

class Matricular implements HelperInterface{
    public function __construct()
    {
    }

    /**
     * Set the View object
     *
     * @param  Renderer $view
     * @return HelperInterface
     */
    public function setView(Renderer $view)
    {

    }

    /**
     * Get the View object
     *
     * @return Renderer
     */
    public function getView()
    {

    }

    public function getButton($id)
    {
        if($id != 'Desclassificado' || $id != null){
            return "<a href='/matricula/acadperiodo-aluno/add/$id'><button class='btn btn-default' style='padding: 5px'> <i class='fa fa-check '></i> Matricular Aluno </button></a>";
        } else {
            return "Desclassificado";
        }
    }
}