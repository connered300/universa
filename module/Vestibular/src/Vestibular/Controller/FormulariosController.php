<?php


namespace Vestibular\Controller;

use DOMPDFModule\View\Model\PdfModel;
use VersaSpine\Controller\AbstractCoreController;

class FormulariosController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function indexAction()
    {
        return $this->view;
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {

    }

    public function relatoriosSalasAction()
    {
        $service          = $this->services()->getService('Vestibular\Service\SelecaoInscricao');
        $salaService      = $this->services()->getService('Infraestrutura\Service\InfraSala');
        $serviceOrgCampus = new \Organizacao\Service\OrgCampus($this->getEntityManager());
        $request          = $this->getRequest();

        if ($request->isPost()) {
            $dados = $request->getPost()->toArray();
            /**
             * Mudar quando inserir a tabela de salas do vestibular.
             * */
            $sala       = $dados['sala'];
            $inscricoes = $service->buscaInscricaoPorSalas($dados['sala']);
            $salaNome   = $salaService->mountSelect()[$sala];
            $pdf        = new PdfModel();
            //            $pdf->setOption('paperSize', 'a4'); // Defaults to "8x11"
            $pdf->setOption('paperOrientation', 'portrait'); // Defaults to "portrait"
            /**
             * O template vai depender do tipo
             * por isso ao cadastrar um novo tipo no formulário setar o valor como sendo a rota do template
             * Ex:
             * array('vestibular/formularios/etiqueta' => 'Etiquetas')
             * */
            $pdf->setTemplate($dados['tipo']);
            $pdf->setVariables(
                [
                    'inscricoes'          => $inscricoes,
                    'salaNome'            => $salaNome,
                    'vestibularDescricao' => $service->buscaEdicaoAtual()->getEdicaoDescricao()
                ]
            );

            $pdf->setVariables($serviceOrgCampus->retornaDadosInstituicao());

            return $pdf;
        }
        $form = new \Vestibular\Form\RelatorioSala($salaService->mountSelect());
        $this->view->setVariables(
            [
                'form' => $form
            ]
        );

        return $this->view;
    }

    public function relatorioInscritosAction()
    {
        $service             = $this->services()->getService('Vestibular\Service\SelecaoInscricao');
        $serviceSelecaoCurso = $this->services()->getService('Vestibular\Service\SelecaoCursos');
        $serviceCampusCurso  = $this->services()->getService('Matricula\Service\CampusCurso');
        $serviceOrgCampus    = new \Organizacao\Service\OrgCampus($this->getEntityManager());

        $edicao    = $service->buscaEdicaoAtual();
        $inscritos = $service->buscaInscritos($edicao->getEdicaoId());

        $selecaoCurso = $serviceSelecaoCurso->getRepository('Vestibular\Entity\SelecaoCursos')->findOneBy(
            ['edicao' => $edicao->getEdicaoId()]
        );

        $pdf = new PdfModel();
        $pdf->setVariables(
            $serviceOrgCampus->retornaDadosInstituicao($selecaoCurso->getCursocampus()->getCamp())
        );
        $pdf->setOption('paperOrientation', 'portrait'); // Defaults to "portrait"
        $pdf->setVariables(
            [
                'inscritos' => $inscritos,
                'edicao'    => $edicao->getEdicaoDescricao(),
            ]
        );

        return $pdf;
    }
}