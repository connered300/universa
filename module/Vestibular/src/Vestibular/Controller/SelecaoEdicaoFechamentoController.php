<?php

namespace Vestibular\Controller;

use VersaSpine\Controller\AbstractCoreController;
use Zend\Authentication\Storage\Session;

class SelecaoEdicaoFechamentoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function indexAction()
    {
        return $this->view;
    }

    public function editAction()
    {
        $request = $this->getRequest();

        if($request->isPost()) {
            $session = new Session();
            $service = new \Vestibular\Service\SelecaoEdicaoFechamento($this->getEntityManager());

            $session = $session->read();
            $dados['classificados'] = $this->params()->fromPost('classificados');
            $dados['edicao'] = $this->params()->fromPost('edicao');
            $dados['usuario'] = $session['usuario'];

            $result = $service->encerraVestibular($dados);

            if ($result) {
                $result = ['type' => 'success', 'message' => 'Vestibular encerrado com sucesso'];
                $this->json->setVariable('result', $result);
            } else {
                $result = ['type' => 'error', 'message' => 'Não foi possível concluir o encerramento do vestibular'];
                $this->json->setVariable('result', $result);
            }
        }

        return $this->json;
    }

}