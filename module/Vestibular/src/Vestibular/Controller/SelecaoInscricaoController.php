<?php

namespace Vestibular\Controller;

use Boleto\Service\Moeda;
use DOMPDFModule\View\Model\PdfModel;
use Financeiro\Service\FinanceiroTitulo;
use VersaSpine\Controller\AbstractCoreController;
use Vestibular\Form\busca;
use Zend\View\Helper\Placeholder\Container;
use Zend\View\Model\JsonModel;

//Formularios usados neste controlador

/**
 * Description of SelecaoInscricaoController
 *
 * @author versa
 */
class SelecaoInscricaoController extends AbstractCoreController
{
    public $em;

    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request    = $this->getRequest();
        $paramsGet  = $request->getQuery()->toArray();
        $paramsPost = $request->getPost()->toArray();
        $param      = array_merge($paramsGet, $paramsPost);

        $serviceSelecaoTipo = new \Vestibular\Service\SelecaoTipo($this->getEntityManager());

        $arrDados = $serviceSelecaoTipo->pesquisaForJson($param);

        return new \Zend\View\Model\JsonModel($arrDados ? $arrDados : array());
    }

    public function buscaPesCpfAction()
    {
        $json = new JsonModel();
        $id   = $this->getEm()->getRepository("Pessoa\Entity\PessoaFisica")->findOneby(
            array('pes' => $this->getRequest()->getPost("id"))
        )->getPesCpf();
        $json->setVariable("dados", $id);

        return $json;
    }

    //Busca dados de pessoa fisica, tais como telefone e email para retornar para view add.
    public function buscaCadastroPessoaFisicaAction()
    {
        $json    = new JsonModel();
        $service = $this->services()->getService($this->getService());
        $json->setVariable('dados', $service->buscaNomeContato($this->getRequest()->getPost("cpf")));

        return $json;
    }

    //Busca contato candidato
    public function buscaContatoCandidatoAction()
    {
        $json    = new JsonModel();
        $service = $this->services()->getService($this->getService());
        $json->setVariable('dados', $service->buscaContatoPessoa($this->getRequest()->getPost("tipo")));

        return $json;
    }

    //Busca o cadastro do candidato inscrito para atender a requisicao via ajax na view add.
    public function buscaCadastroAtivoAction()
    {
        $json    = new JsonModel();
        $service = $this->services()->getService($this->getService());
        $json->setVariable('dados', $service->buscaInscricaoPorCpf($this->getRequest()->getPost("cpf")));

        return $json;
    }

    public function indexAction()
    {
        $service = $this->services()->getService($this->getService());

        foreach ($_SESSION['Zend_Auth'] as $session) {
            $id = $session['usuario'];
        };

        $autVest = $service->verificaUsuarioGrupo($id, 'Vestibulandos');

        if ($autVest) {
            return $this->redirect()->toRoute(
                "vestibular/default",
                array("controller" => "selecao-inscricao", "action" => "dados-candidato")
            );
        }

        return $this->getView();
    }

    public function paginationAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceInscricao = new \Vestibular\Service\SelecaoInscricao($this->getEntityManager());

            $data   = $request->getPost()->toArray();
            $result = $serviceInscricao->pagination($data);

            $this->getJson()->setVariable("draw", $result["draw"] ? $result["draw"] : 0);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"] ? $result["recordsTotal"] : 0);
            $this->getJson()->setVariable(
                "recordsFiltered",
                $result["recordsFiltered"] ? $result["recordsFiltered"] : 0
            );
            $this->getJson()->setVariable("data", $result["data"] ? $result["data"] : []);
        }

        return $this->getJson();
    }

    public function showFormAction()
    {
        $this->view->setTerminal(true);
        $form = $this->getForm();
        if (!class_exists($form)) {
            $this->view->setVariable("erro", "Formulário de busca não foi encontrado!");

            return $this->view;
        }
        $service   = $this->services()->getService($this->getService());
        $curso     = $service->buscaCurso('Direito');
        $cursoId   = $curso->getCursoId();
        $cursoNome = $curso->getCursoNome();

        $edicao            = $service->buscaEdicaoAtual();
        $linguaEstrangeira = $service->buscaProvaEstrangeiraNaEdicao(
            $edicao->getEdicaoId()
        ); // Passar a edicao Atual!!!

        //Para preencher tabela de turno

        $periodos = [
            'Matutino'   => 'Matutino',
            'Noturno'    => 'Noturno',
            'Integral'   => 'Integral',
            'Vespertino' => 'Vespertino'
        ];

        // Retorna os tipo de edica(Enem/Prouni) existentes na edicao.
        $tipoEdicao = $service->buscaTipoEdicaoAtual($edicao->getEdicaoId());
        $form       = new \Vestibular\Form\SelecaoInscricaoCadastro(
            $cursoId,
            $cursoNome,
            $linguaEstrangeira,
            $periodos,
            $tipoEdicao
        );
        $entity     = $this->getEm()->getRepository('Vestibular\Entity\SelecaoEdicao')->findAll();
        if ($entity) {
            foreach ($entity as $nome) {
                $data[$nome->getEdicaoDescricao()] = $nome->getEdicaoDescricao();
            }
        }
        $form->remove("selecaoTipo")
            ->remove("cursoNome")
            ->remove("dataProva")
            ->remove("necessidades")
            ->remove("possuiNecessidades")
            ->remove("pesCpfEmissao")
            ->remove("pesRg")
            ->remove("pesRgEmissao")
            ->remove("pesSobrenome")
            ->remove("pesSexo")
            ->remove("pesSexo")
            ->remove("pesDataNascimento")
            ->remove("pesEstadoCivil")
            ->remove("pesFilhos")
            ->remove('turno')
            ->remove('inscricaoFormacaoInstituicao')
            ->remove('inscricaoEntregaDoc')
            ->remove('inscricaoCidadeFormacao')
            ->remove('inscricaoDataFormacao')
            ->remove('inscricaoUltimaFormacao')
            ->remove('inscricaoNotaEnemMatematica')
            ->remove('inscricaoNotaEnemRedacao')
            ->remove('inscricaoNotaEnemPortugues')
            ->remove('inscricaoNotaEnemCiencNatureza')
            ->remove('pesEmissorRg')
            ->remove('tipoPagamento')
            ->remove('pesDocEstrangeiro')
            ->remove('inscricaoNotaEnemCiencHumanas');
        $form->get('edicaoDescricao')->setValueOptions($data);
        $form->get('pesNome')->setAttribute('label', 'Nome do Candidato:');
        $form->get('pesCpf')->setAttribute("label", "CPF do Candidato:");
        $form->get('inscricaoLinguaEstrangeira')->setAttribute("label", "Buscar por lingua estrangeira:");
        $form->get('status')->setAttribute("label", "Buscar por status do candidato:");
        $form->get('edicaoDescricao')->setAttribute("label", "Buscar por processo Seletivo:");

        $form->setAttribute("method", 'get');
        $this->view->setVariable("config", $this->config);
        $this->view->setVariable("form", $form);

        return $this->view;
    }

    public function captacaoAction()
    {
        $renderer = $this->serviceLocator->get('Zend\View\Renderer\RendererInterface');
        $url      = $renderer->basePath('/boleto-bancario/boleto/exibe/');

        /** @var \Vestibular\Service\SelecaoInscricao $service */
        $service               = $this->services()->getService($this->getService());
        $serviceSelecaoCurso   = $this->services()->getService('Vestibular\Service\SelecaoCursos');
        $serviceSelecaoLocais  = $this->services()->getService('Vestibular\Service\SelecaoLocais');
        $serviceDataRealizacao = $this->services()->getService('Vestibular\Service\SelecaoDataRealizacao');
        $serviceCampusCurso    = new \Matricula\Service\CampusCurso($this->getEntityManager());

        $formContato = new \Vestibular\Form\SelecaoInscricao();

        $serviceEndereco   = new \Pessoa\Service\Endereco($this->getEm());
        $arrOptionsEstados = $serviceEndereco->buscaEstados();
        $service           = $this->services()->getService($this->getService());
        $formEndereco      = new \Pessoa\Form\Endereco($arrOptionsEstados);

        // recupera configurações de email da aplicação e seta no serviço
        $configEmail = $this->getServiceManager()->get('Config')['emailRecovery'];
        $service->setConfigEmail($configEmail);

        $linkRedirect = $this
            ->getServiceManager()
            ->get('ViewHelperManager')->get('ServerUrl')
            ->__invoke();
        $linkRedirect .= $this->url()->fromRoute('vestibular-login');
        $service->linkLoginCadidato = $linkRedirect;

        $formEndereco->get("endCep[]")->removeAttribute("data-masked", '');
        /* @var $edicaoEntity \Vestibular\Entity\SelecaoEdicao */
        $edicaoEntity = $service->buscaEdicaoAtual();

        if (!$edicaoEntity) {
            echo 'Sem processo seletivo em andamento!';
            exit();
        }

        $edicao       = $edicaoEntity->getEdicaoId();

        // verifica se é pra exibir os campos de pais no vestibular
        $edicaoFiliacao = ($edicaoEntity->getEdicaoFiliacao() == 'Sim') ? true : false;
        $edicaoIsencao  = ($edicaoEntity->getEdicaoIsencao() == 'Sim') ? true : false;

        $this->view->setVariable('edicaoFiliacao', $edicaoFiliacao);
        $this->view->setVariable('edicaoIsencao', $edicaoIsencao);

        if ($edicaoEntity->getEdicaoAgendado() == 'Sim') {
            $datas = $serviceDataRealizacao->buscasDatasEHorariosProvasPorEdicao($edicaoEntity->getEdicaoId());

            $this->view->setVariable('agendado', true);
            $this->view->setVariable('datasProvas', $datas);
            $this->view->setVariable('dataInicioInsc', $edicaoEntity->getEdicaoInicioInscricao());
            $this->view->setVariable('dataFimInsc', $edicaoEntity->getEdicaoFimInscricao());
        }

        $arrSelecaoLocais = $serviceSelecaoLocais->pesquisaForJson(['edicaoId' => $edicao]);
        $arrSelecaoLocais = $serviceSelecaoLocais->retornaSelect2($arrSelecaoLocais);
        $this->view->setVariable('provaLocais', $arrSelecaoLocais);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $secretRecaptcha = $this->getServiceLocator()->get('Config')['secret-key-recaptcha'];
            $remoteIp        = $request->getServer()->get('REMOTE_ADDR');

            try {
                $dados = $request->getPost()->toArray();

                if ($dados['g-recaptcha-response']) {
                    $Recaptcha = new \ReCaptcha\ReCaptcha($secretRecaptcha);

                    $resp = $Recaptcha->verify($dados['g-recaptcha-response'], $remoteIp);

                    if (!$resp->isSuccess()) {
                        $this->flashMessenger()->addWarningMessage("Verifique o Captcha!");

                        return $this->redirect()->toRoute(
                            "vestibular/default",
                            array("controller" => "selecao-inscricao", "action" => "captacao")
                        );
                    }
                } else {
                    $this->flashMessenger()->addWarningMessage("Verifique o Captcha!");

                    return $this->redirect()->toRoute(
                        "vestibular/default",
                        array("controller" => "selecao-inscricao", "action" => "captacao")
                    );
                }

                $dados['password'] = $service->passGeneration();

                if ($edicaoEntity->getEdicaoIsencao() == 'Sim') {
                    $dados['tipoPagamento'] = "Isento";
                }

                $cursoVazio  = false;
                $campusVazio = false;

                if (!$dados['cursoSelecao'] || empty($dados['cursoSelecao'])) {
                    $cursoVazio = true;
                }
                if (!$dados['campus']) {
                    $campusVazio = true;
                }

                /*Tratamento caso passe o campusCurso inves cursoSelecao*/
                foreach ($dados['campusCurso'] as $value) {
                    if ($value) {
                        /** @var $objCampusCurso \Matricula\Entity\CampusCurso */
                        $objCampusCurso =
                            $serviceCampusCurso->getRepository()->findOneBy(['cursocampusId' => $value]);

                        if ($objCampusCurso) {
                            if ($cursoVazio) {
                                $dados['cursoSelecao'][] = $objCampusCurso->getCurso()->getCursoId();
                            }

                            if ($campusVazio) {
                                $dados['campus'][] = $objCampusCurso->getCamp()->getCampId();
                            }
                        }
                    }
                }

                /* @var $ret \Vestibular\Entity\SelecaoInscricao */
                $ret = $service->adicionar($dados);
            } catch (\Exception $exc) {
                throw new \Exception('Erro ao concluir cadastro de inscricao' . $exc->getMessage());
            }

            if ($ret->getPes()->getPesId()) {
                $this->view->setVariable('id', $ret->getPes()->getPesId());
            }

            // A fazer tratar parte da configuracao do boleto no vestibular; tratar data de vencimento diferenciada

            $tituloRetorno = null;
            $objBoleto     = null;

            try {
                $serviceFinanceiroTitulo        = new \Boleto\Service\FinanceiroTitulo($this->getEntityManager());
                $serviceFinanceiroTituloNovo    = new \Financeiro\Service\FinanceiroTitulo($this->getEntityManager());
                $serviceFinanceiroBoletoNovo    = new \Financeiro\Service\Boleto($this->getEntityManager());
                $serviceFinanceiroMeioPagamento = new \Financeiro\Service\FinanceiroMeioPagamento(
                    $this->getEntityManager()
                );
                $pagIsento                      = $serviceFinanceiroMeioPagamento::ISENTO;
                $pagBoleto                      = $serviceFinanceiroMeioPagamento::BOLETO;

                $titulo['tituloDataProcessamento'] = $ret->getInscricaoData();

                if (!is_object($titulo['tituloDataProcessamento'])) {
                    $titulo['tituloDataProcessamento'] = new \DateTime($titulo['tituloDataProcessamento']);
                }

                // ESTA IMPLEMENTAÇÃO DEVERÁ SER ALTERADA APÓS ATUALIZAÇÃO DAS BRANCHS
                $dataFimVestibular              = $edicaoEntity->getEdicaoFimInscricao();
                $dataMaxVencimento              = (new \DateTime(date('Y-m-d')))->modify('+10 day');
                $titulo['tituloDataVencimento'] = $dataMaxVencimento > $dataFimVestibular ? $dataFimVestibular : $dataMaxVencimento;

                if (is_array($dados['cursoSelecao'])) {
                    $cursoSelecao = array_shift($dados['cursoSelecao']);
                } else {
                    $cursoSelecao = $dados['cursoSelecao'];
                }

                $titulo['tipotitulo']   = \Boleto\Service\FinanceiroTituloTipo::VESTIBULAR;
                $titulo['tituloValor']  =
                    $serviceSelecaoCurso->buscaValorIsncricaoPorSelecaoCurso($cursoSelecao);
                $titulo['tituloMulta']  = 0;
                $titulo['tituloJuros']  = 0;
                $titulo['tituloEstado'] = 'Aberto';
                $titulo['pes']          = $ret->getPes()->getPesId();
                $titulo['usuarioBaixa'] = null;
                $titulo['usuarioAutor'] = null;
                $titulo['titulo_novo']  = null;
                $titulo['usuario']      = $_SESSION['Zend_Auth']['storage']['id'];
                $titulo['tituloconf']   = null;

                $titulo['desabilitaVerificacaoValor'] = true;

                if ($edicaoEntity->getEdicaoIsencao() == 'Sim') {
                    $titulo['tituloTipoPagamento'] = $pagIsento;
                    $titulo['tituloEstado']        = "Pago";
                    $titulo['tituloValorPago']     = '0.00';
                    $titulo['tituloDataPagamento'] = new \DateTime('now');
                    $titulo['usuarioBaixa']        = $_SESSION['Zend_Auth']['storage']['id'];
                    $titulo['usuarioAutor']        = $_SESSION['Zend_Auth']['storage']['id'];
                } else {
                    $titulo['tituloTipoPagamento'] = $pagBoleto;
                }

                if ($serviceFinanceiroTituloNovo->save($titulo)) {
                    /** @var \Financeiro\Entity\FinanceiroTitulo $tituloRetorno */
                    $tituloRetorno = $serviceFinanceiroTituloNovo->getRepository()->find($titulo['tituloId']);

                    if ($titulo['tituloTipoPagamento'] == $pagBoleto) {
                        if ($serviceFinanceiroBoletoNovo->registrarBoletoParaTitulo($tituloRetorno)) {
                            $objBoleto = $serviceFinanceiroBoletoNovo->pesquisaBoletoPorTitulo($tituloRetorno);

                            $infobol = "<br>Imprima o boleto clicando <strong><a target='_blank' href='/boleto-bancario/boleto/exibe/{$objBoleto->getBolId()}'>Aqui</a></strong>";
                        } else {
                            throw new \Exception(
                                'Erro ao gerar o boleto para o candidato! ' . $serviceFinanceiroBoletoNovo->getLastError(
                                )
                            );
                        }
                    }
                } else {
                    throw new \Exception(
                        'Erro ao gerar o titulo para o candidato! ' . $serviceFinanceiroTituloNovo->getLastError()
                    );
                }
            } catch (\Exception $exc) {
                throw new \Exception('Erro ao gerar o titulo para o candidato! ' . $exc->getMessage());
            }

            if ($dados['tipoPagamento'] == 'Dinheiro' || $dados['tipoPagamento'] == 'Isento') {
                $infobol = "<br>Imprima o recibo clicando <strong><a target='_blank' href='/boleto-bancario/financeiro-titulo/comprovante/{$tituloRetorno->getTituloId()}'>Aqui</a></strong>";
            }

            $mensagem = "Candidado cadastrado com sucesso!</br> Usuário: {$dados['username']} </br>Senha: {$dados['password']}";

            if ($edicaoEntity->getEdicaoIsencao() != 'Sim' && $objBoleto) {
                $mensagem .= "
                <script>
                if(confirm('Deseja imprimir seu boleto agora ?')){
                    window.open('" . $url . $objBoleto->getBolId() . "', 'Boleto');
                }
                </script>";
            }

            $this->flashMessenger()->addSuccessMessage($mensagem);

            return $this->redirect()->toRoute(
                "vestibular/default",
                array("controller" => "selecao-inscricao", "action" => "login")
            );
        }

        if ($_SESSION['Zend_Auth']) {
            if (!$service->buscaEdicaoAtual()) {
                $this->flashMessenger()->addInfoMessage(
                    'Não é possivel efetuar novos cadastros. Aguarde a abertura de um novo processo de Vestibular!'
                );

                return $this->redirect()->toRoute("vestibular/default", array("controller" => "selecao-inscricao"));
            }

            return $this->redirect()->toRoute(
                "vestibular/default",
                array("controller" => "selecao-inscricao", "action" => "cadastro")
            );
        }

        if ($this->flashMessenger()->hasErrorMessages()) {
            $this->getView()->setVariable("erro", $this->flashMessenger()->getErrorMessages());
        }
        if ($this->flashMessenger()->hasInfoMessages()) {
            $this->view->setVariable("info", $this->flashMessenger()->getInfoMessages());
        }
        if ($this->flashMessenger()->hasWarningMessages()) {
            $this->view->setVariable("warning", $this->flashMessenger()->getWarningMessages());
        }

        if ($this->flashMessenger()->hasSuccessMessages()) {
            $this->view->setVariable("success", $this->flashMessenger()->getSuccessMessages());
        }

        session_destroy();

        $linguaEstrangeira = $service->buscaProvaEstrangeiraNaEdicao($edicao); // Passar a edicao Atual!!!

        if (!$linguaEstrangeira) {
            $this->flashMessenger()->addErrorMessage("Opa! Não encontramos nenhum caderno de provas para esta edição");

            return $this->redirect()->toRoute("vestibular/default", array("controller" => "selecao-inscricao"));
        }

        // Identificação do câmpus vinculado ao processo, recuperando nome do curso, id e id do câmpus.
        $selecaoCursos = $this->getEm()->getRepository('Vestibular\Entity\SelecaoCursos')->findBy(
            array('edicao' => $edicao)
        );

        if (!$selecaoCursos) {
            $this->flashMessenger()->addErrorMessage(
                "Verifique se há algum Câmpus vinculado ao processo de Vestibular!"
            );

            return $this->redirect()->toRoute("vestibular/default", array("controller" => "selecao-inscricao"));
        }

        $arrCampusCursos = array(); // Opções de Cursos nos Câmpus
        $arrCursoTurnos  = array(); // Opções de Turnos dos Cursos

        /** @var $selecaoCurso \Vestibular\Entity\SelecaoCursos */
        foreach ($selecaoCursos as $selecaoCurso) {
            $campusCurso = $selecaoCurso->getCursocampus();
            $campus      = $campusCurso->getCamp();

            // Agrupa informações Cursos por Câmpus
            $arrCampusCursos[] = [
                'nomeCurso'      => $campusCurso->getCurso()->getCursoNome(),
                'text'           => (
                    $campusCurso->getCamp()->getCampNome() . ' / ' .
                    $campusCurso->getCurso()->getCursoNome()
                ),
                'idCursoSelecao' => $selecaoCurso->getSelcursosId(),
                'id'             => $campusCurso->getCursocampusId(),
                'idCurso'        => $campusCurso->getCurso()->getCursoId(),
            ];

            if ($selecaoCurso->getSelcursosPeriodos() !== null) {
                $arrCursoTurnos[$selecaoCurso->getSelcursosId()] = explode(',', $selecaoCurso->getSelcursosPeriodos());

                foreach ($arrCursoTurnos[$selecaoCurso->getSelcursosId()] as $value) {
                    $arrCursoTurnos[$selecaoCurso->getSelcursosId()]['text'] = strtoupper($value);
                    $arrCursoTurnos[$selecaoCurso->getSelcursosId()]['id']   = $value;
                }
            }
        }

        $edicaoNumCursosOpcionais = intval($edicaoEntity->getEdicaoNumCursosOpcionais()) + 1;

        if ($edicaoNumCursosOpcionais > count($selecaoCursos)) {
            $edicaoNumCursosOpcionais = count($selecaoCursos);
        }

        $this->view->setVariable('edicaoNumCursosOpcionais', $edicaoNumCursosOpcionais);

        // Retorna os tipo de edica(Enem/Prouni) existentes na edicao.
        $tipoEdicao = $service->buscaTipoEdicaoAtual($edicao);

        $necessidadesEspeciais = $this->services()->getService("Pessoa\Service\NecessidadesEspeciais")->mountSelect();
        $formCadastro          = new \Vestibular\Form\SelecaoInscricaoCadastro(
            array(),
            $linguaEstrangeira,
            $tipoEdicao,
            $necessidadesEspeciais
        );

        $formEndereco->setAttribute('class', 'smart-form');

        $this->view->setVariable('formCadastro', $formCadastro);
        $this->view->setVariable('formEndereco', $formEndereco);
        $this->view->setVariable('formContato', $formContato);
        $this->view->setVariable('arrCampusCursos', $arrCampusCursos);
        $this->view->setVariable('arrCursoTurnos', $arrCursoTurnos);

        $this->view->setTerminal(true);

        return $this->view;
    }

    /**
     * Action Cadastro.
     * Action utilizada pela instituiçao que cadastra o usuario por meio desta view e para conclusao do cadastro por parte do vestibulando.
     */
    public function cadastroAction()
    {
        /*
         * $candidato
         * Quando carregar as informações que o candidato visualisará setara a variável para true
         * informando que entrou no ambiente do candidato e não no da secretaria.
         * */
        $service               = new \Vestibular\Service\SelecaoInscricao($this->getEm());
        $serviceSelecaoLocais  = new \Vestibular\Service\SelecaoLocais($this->getEm());
        $serviceDataRealizacao = new \Vestibular\Service\SelecaoDataRealizacao($this->getEm());
        $serviceOrgUnidadeEstudo = new \Organizacao\Service\OrgUnidadeEstudo($this->getEntityManager());

        $edicaoEntity = $service->buscaEdicaoAtual();
        $edicao       = $edicaoEntity->getEdicaoId();

        // recupera configurações de email da aplicação e seta no serviço
        $configEmail = $this->getServiceManager()->get('Config')['emailRecovery'];
        $service->setConfigEmail($configEmail);

        $linkRedirect = $this
            ->getServiceManager()
            ->get('ViewHelperManager')->get('ServerUrl')
            ->__invoke();
        $linkRedirect .= $this->url()->fromRoute('vestibular-login');
        $service->linkLoginCadidato = $linkRedirect;

        // verifica se é pra exibir os campos de pais no vestibular
        $edicaoFiliacao = ($edicaoEntity->getEdicaoFiliacao() == 'Sim') ? true : false;
        $this->view->setVariable('edicaoFiliacao', $edicaoFiliacao);
        $edicaoIsencao = ($edicaoEntity->getEdicaoIsencao() == 'Sim') ? true : false;
        $this->view->setVariable('edicaoIsencao', $edicaoIsencao);

        if ($edicaoEntity->getEdicaoAgendado() == 'Sim') {
            $datas = $serviceDataRealizacao->buscasDatasEHorariosProvasPorEdicao($edicaoEntity->getEdicaoId());
            $this->view->setVariable('agendado', true);
            $this->view->setVariable('datasProvas', $datas);
            $this->view->setVariable('dataInicioInsc', $edicaoEntity->getEdicaoInicioInscricao());
            $this->view->setVariable('dataFimInsc', $edicaoEntity->getEdicaoFimInscricao());
        }

        $request = $this->getRequest();

        if ($request->isPost()) {
            $dados                          = $request->getPost()->toArray();
            $dados['inscricaoDataFormacao'] = ($dados['inscricaoDataFormacao']) ? $dados['inscricaoDataFormacao'] : null;
            $dados['password']              = $service->passGeneration();

            if (!$dados['provaUnidade'] && !$dados['selelocais']) {
                $this->flashMessenger()->addErrorMessage("Selecione um local para realizar a prova!");

                return false;
            }

            $ret = $service->adicionar($dados);

            if ($ret->getPes()->getPesId()) {
                $this->view->setVariable('id', $ret->getPes()->getPesId());
            }

            $infobol = '';

            try {
                $serviceFinanceiroTitulo        = new FinanceiroTitulo($this->getEm());
                $serviceFinanceiroPagamento     = new \Financeiro\Service\FinanceiroPagamento($this->getEm());
                $serviceFinanceiroMeioPagamento = new \Financeiro\Service\FinanceiroMeioPagamento($this->getEm());

                $serviceFinanceiroTituloNovo = new \Financeiro\Service\FinanceiroTitulo($this->getEntityManager());
                $serviceFinanceiroBoletoNovo = new \Financeiro\Service\Boleto($this->getEntityManager());

                $dataFimVestibular              = $edicaoEntity->getEdicaoFimInscricao();
                $dataMaxVencimento              = (new \DateTime(date('Y-m-d')))->modify('+10 day');
                $titulo['tituloDataVencimento'] = $dataMaxVencimento > $dataFimVestibular ? $dataFimVestibular : $dataMaxVencimento;

                $titulo['tituloDataProcessamento'] = $ret->getInscricaoData();
                $titulo['tituloTipoPagamento']     = $dados['tipoPagamento'];
                $titulo['tituloDataPagamento']     = ($dados['tipoPagamento'] == 'Dinheiro') ? new \DateTime(
                    'now',
                    new \DateTimeZone('America/Sao_Paulo')
                ) : '';
                $titulo['tipotitulo']              = \Boleto\Service\FinanceiroTituloTipo::VESTIBULAR;
                $titulo['tituloValor']             = $service->buscaValorIsncricao(
                    $ret->getEdicao()->getEdicaoId()
                );

                $servAcesso = new \Acesso\Service\AcessoPessoas($this->getEm());

                if ($usrLogado = $servAcesso->retornaUsuarioLogado()) {
                    $titulo['tituloDataPagamento']     = new \DateTime('now');
                    $titulo['usuarioBaixa']            = $_SESSION['Zend_Auth']['storage']['id'];
                    $titulo['usuarioAutor']            = $_SESSION['Zend_Auth']['storage']['id'];
                    $titulo['pagamentoValorBruto']     = $titulo['tituloValor'];
                    $titulo['pagamentoValorFinal']     = $titulo['tituloValorPago'];
                    $titulo['pagamentoDataBaixa']      = $titulo['tituloDataPagamento'];
                    $titulo['pagamentoObservacoes']    = 'Pagamento Isento';
                    $titulo['pagamentoValorJuros']     = '0.00';
                    $titulo['pagamentoValorMulta']     = '0.00';
                    $titulo['pagamentoAcrescimos']     = '0.00';
                    $titulo['pagamentoValorDescontos'] = '0.00';
                    $titulo['pagamentoValorFinal']     = '0.00';
                    $titulo['pagamentoDescontoManual'] = '0.00';
                    $titulo['pagamentoUsuario']        = $usrLogado;
                    $titulo['pagamentoDataBaixa']      = new \DateTime();
                }

                if ($dados['tipoPagamento'] == 'Dinheiro') {
                    $pagamento                      = $serviceFinanceiroMeioPagamento->criaSeNecessarioERetornaMeioPagamento(
                        $serviceFinanceiroMeioPagamento::DINHEIRO
                    );
                    $titulo['tituloValorPago']      = $titulo['tituloValor'];
                    $titulo['meioPagamento']        = $pagamento->getMeioPagamentoId();
                    $titulo['pagamentoObservacoes'] = 'Pagamento de Inscrição in Loco';
                } else {
                    if ($dados['tipoPagamento'] == 'Isento') {
                        $pagamento                      = $serviceFinanceiroMeioPagamento->criaSeNecessarioERetornaMeioPagamento(
                            $serviceFinanceiroMeioPagamento::ISENTO
                        );
                        $titulo['tituloValorPago']      = '0.00';
                        $titulo['pagamentoObservacoes'] = 'Isento de Pagamento';
                        $titulo['meioPagamento']        = $pagamento->getMeioPagamentoId();;
                    } else {
                        $titulo['tituloDataPagamento'] = null;
                        $titulo['usuarioBaixa']        = null;
                        $titulo['usuarioAutor']        = null;
                    }
                }

                $titulo['tituloMulta']   = 0;
                $titulo['tituloJuros']   = 0;
                $titulo['tituloParcela'] = 1;
                $titulo['titulo_novo']   = null;
                $titulo['tituloEstado']  = $dados['tipoPagamento'] == 'Dinheiro' || $dados['tipoPagamento'] == 'Isento' ? "Pago" : "Aberto";
                $titulo['pes']           = $ret->getPes()->getPesId();
                $titulo['usuario']       = $_SESSION['Zend_Auth']['storage']['id'];

                if ($serviceFinanceiroTituloNovo->save($titulo)) {
                    $tituloRetorno = $serviceFinanceiroTituloNovo->getRepository()->find($titulo['tituloId']);

                    if ($titulo['tituloTipoPagamento'] == 'Boleto') {
                        if ($serviceFinanceiroBoletoNovo->registrarBoletoParaTitulo($tituloRetorno)) {
                            $objBoleto = $serviceFinanceiroBoletoNovo->pesquisaBoletoPorTitulo($tituloRetorno);

                            $infobol = "<br>Imprima o boleto clicando <strong><a target='_blank' href='/boleto-bancario/boleto/exibe/{$objBoleto->getBolId()}'>Aqui</a></strong>";
                        } else {
                            throw new \Exception(
                                'Erro ao gerar o boleto para o candidato! ' . $serviceFinanceiroBoletoNovo->getLastError(
                                )
                            );
                        }
                    } else {

                        try {
                            $titulo['titulo'] = $titulo['tituloId'];

                            if (!$serviceFinanceiroPagamento->save($titulo)) {
                                throw new \Exception(
                                    'Erro ao registrar Pagamento para a Inscrição! ' . $serviceFinanceiroPagamento->getLastError(
                                    )
                                );
                            }
                        } catch (\Exception $exc) {
                            throw new \Exception(
                                'Erro ao registrar Pagamento para a Inscrição! ' . $serviceFinanceiroTituloNovo->getLastError(
                                )
                            );
                        }
                    }
                } else {
                    throw new \Exception(
                        'Erro ao gerar o titulo para o candidato! ' . $serviceFinanceiroTituloNovo->getLastError()
                    );
                }
                //$tituloRetorno          = $serviceFinanceiroTitulo->adicionar($titulo);
            } catch (\Exception $exc) {
                throw new \Exception('Erro ao gerar o titulo para o candidato! ' . $exc->getMessage());
            }

            if ($dados['tipoPagamento'] == 'Dinheiro' || $dados['tipoPagamento'] == 'Isento') {
                $infobol = "<br>Imprima o recibo clicando <strong><a target='_blank' href='/boleto-bancario/financeiro-titulo/comprovante/{$tituloRetorno->getTituloId()}'>Aqui</a></strong>";
            }

            if ($edicaoEntity->getEdicaoIsencao() == 'Sim') {
                $this->flashMessenger()->addSuccessMessage(
                    "INSCRIÇAO de <strong>{$ret->getPes()->getPesNome()}</strong> efetuada com sucesso!"
                );
            } else {
                $this->flashMessenger()->addSuccessMessage(
                    "INSCRIÇAO de <strong>{$ret->getPes()->getPesNome()}</strong> efetuada com sucesso!</br>{$infobol}"
                );
            }

                return $this->redirect()->toRoute(
                "vestibular/default",
                array("controller" => "selecao-inscricao", 'id' => $ret->getPes()->getPesId())
            );
        }

        $candidato = false;

        $formContato = new \Vestibular\Form\SelecaoInscricao();

        $options_estados = $this->services()->getService("Pessoa\Service\Endereco")->buscaEstados();

        $formEndereco = new \Pessoa\Form\Endereco($options_estados);

        $formEndereco->get("endCep[]")->removeAttribute("data-masked", '');

        // Passar a edicao Atual!!!
        $linguaEstrangeira =
            $service->buscaProvaEstrangeiraNaEdicao($service->buscaEdicaoAtual()->getEdicaoId());

        if (is_null($linguaEstrangeira)) {
            $this->flashMessenger()->addErrorMessage("Por favor, cadastre as opções de lingua estrangeira!");

            return $this->redirect()->toRoute("vestibular/default", array("controller" => "selecao-inscricao"));
        }

        // Identificação dos cursos dos câmpus que participão da edição
        $selecaoCursos = $this->getEm()->getRepository('Vestibular\Entity\SelecaoCursos')->findBy(
            array('edicao' => $edicao)
        );

        if (!$selecaoCursos) {
            $this->flashMessenger()->addErrorMessage(
                "Verifique se há algum Câmpus vinculado ao processo de Vestibular!"
            );

            return $this->redirect()->toRoute("vestibular/default", array("controller" => "selecao-inscricao"));
        }

        $arrCampus       = array(); // Opçõs de Câmpus
        $arrCampusCursos = array(); // Opções de Cursos nos Câmpus
        $arrCursoTurnos  = array(); // Opções de Turnos dos Cursos

        /** @var $selecaoCurso \Vestibular\Entity\SelecaoCursos */
        foreach ($selecaoCursos as $selecaoCurso) {
            $campusCurso = $selecaoCurso->getCursocampus();
            $campus      = $campusCurso->getCamp();
            $curso       = $campusCurso->getCurso();

            // Agrupa informações dos Câmpus
            $arrCampus[$campus->getCampId()] = array(
                'idCampus'   => $campus->getCampId(),
                'nomeCampus' => $campus->getCampNome(),
            );

            // Agrupa informações Cursos por Câmpus
            $arrCampusCursos[$campus->getCampId()][$selecaoCurso->getSelcursosId()] = [
                'idCursoSelecao' => $selecaoCurso->getSelcursosId(),
                'idCurso'        => $curso->getCursoId(),
                'nomeCurso'      => $curso->getCursoNome(),
            ];

            // Agrupa Turnos dos Cursos disponíveis por selecaoCursosId
            $arrCursoTurnos[$selecaoCurso->getSelcursosId()] = $service->buscaPeriodosEdicao($selecaoCurso);
        }

        $arrSelecaoLocais = $serviceSelecaoLocais->pesquisaForJson(['edicaoId' => $edicao]);
        $arrSelecaoLocais = $serviceSelecaoLocais->retornaSelect2($arrSelecaoLocais);
        $this->view->setVariable('unidadeEstudo', $arrSelecaoLocais);

        if (!$arrSelecaoLocais) {
            $this->flashMessenger()->addErrorMessage("Não há local de prova disponível!");

            return $this->redirect()->toRoute("vestibular/default", array("controller" => "selecao-inscricao"));
        }

        // Retorna os tipo de edica(Enem/Prouni) existentes na edicao.
        $tipoEdicao = $service->buscaTipoEdicaoAtual($edicao);

        $necessidadesEspeciais = $this->services()->getService("Pessoa\Service\NecessidadesEspeciais")->mountSelect();
        $formCadastro          = new \Vestibular\Form\SelecaoInscricaoCadastro(
            $options_estados,
            $linguaEstrangeira,
            $tipoEdicao,
            $necessidadesEspeciais
        );

        $formEndereco->setAttribute('class', 'smart-form');

        $this->view->setVariable('formCadastro', $formCadastro);
        $this->view->setVariable('formEndereco', $formEndereco);
        $this->view->setVariable('formContato', $formContato);
        if ($candidato) {
            $this->view->setVariable('candidato', true);
            $this->view->setTerminal(true);
        }

        $this->view->setVariable('arrCampus', $arrCampus);
        $this->view->setVariable('arrCampusCursos', $arrCampusCursos);
        $this->view->setVariable('arrCursoTurnos', $arrCursoTurnos);
        $this->view->setVariable('arrEstados', $options_estados);

        return $this->view;
    }

    //Controlador responsável por redirecionar o candidato para interface "Vestibulando".
    public function dadosCandidatoAction()
    {
        //        //Recebe cpf do candidato logado para receber suas informações
        foreach ($_SESSION['Zend_Auth'] as $session) {
            $pesFisica = $session['pesFisica'];
        };

        $service = $this->services()->getService($this->getService());

        $edicao = $service->buscaEdicaoAtual(); //Recebe a edicao atual

        if (!$edicao) {
            $this->flashMessenger()->addErrorMessage("Não existem processos seletivos ativos!");

            return $this->redirect()->toRoute(
                "vestibular/default",
                array("controller" => "selecao-inscricao", 'action' => 'logout')
            );
        }

        $entityPessoa = $this->getEm()->getRepository('Pessoa\Entity\PessoaFisica')->findOneBy(
            array('pes' => $pesFisica)
        );
        $inscricao    = $this->getEm()->getRepository('Vestibular\Entity\SelecaoInscricao')->findOneBy(
            array('edicao' => $edicao->getEdicaoId(), 'pes' => $pesFisica)
        );
        $dataProva    = $this->getEm()->getRepository('Vestibular\Entity\SelecaoDataRealizacao')->findOneBy(
            array('edicao' => $edicao->getEdicaoId())
        );
        $usuario      = $this->getEm()->getRepository('Acesso\Entity\AcessoPessoas')->findOneBy(
            array('pesFisica' => $pesFisica)
        );

        if (!$inscricao) {
            $this->flashMessenger()->addErrorMessage("Candidato não possui inscrição no processo atual!");

            return $this->redirect()->toRoute(
                "vestibular/default",
                array("controller" => "selecao-inscricao", 'action' => 'logout')
            );
        }

        if ($inscricao->getSeleLocais()) {
            $localProva = $inscricao->getSeleLocais();
            $this->view->setVariable('localProva', $localProva);
        }
        // recupera e monta array de cursos ja cadastrados
        $inscricaoCursos = $this->getEm()->getRepository("Vestibular\Entity\InscricaoCursos")->findBy(
            array('inscricao' => $inscricao->getInscricaoId())
        );

        $cursosSelects = array();
        $campusSelects = array();
        $opcoesCurso   = array();

        /* @var $inscricaoCurso \Vestibular\Entity\InscricaoCursos */
        foreach ($inscricaoCursos as $inscricaoCurso) {
            $selCurso = $inscricaoCurso->getSelcursos();
            $campus   = $selCurso->getCursocampus()->getCamp();
            $curso    = $selCurso->getCursocampus()->getCurso();

            $cursosSelects[] = $selCurso->getSelcursosId();
            $campusSelects[] = $campus->getCampId();

            $opcoesCurso[] = array(
                'selcursosId'   => $selCurso->getSelcursosId(),
                'cursocampusId' => $selCurso->getCursocampus()->getCursocampusId(),
                'cursoId'       => $curso->getCursoId(),
                'curso'         => $curso->getCursoNome(),
                'campId'        => $campus->getCampId(),
                'campus'        => $campus->getCampNome(),
                'opcao'         => $inscricaoCurso->getInscricaoCursoOpcao(),
            );
        }

        // Identificação do câmpus vinculado ao processo, recuperando nome do curso, id e id do campus.
        $selecaoCursos = $this->getEm()->getRepository('Vestibular\Entity\SelecaoCursos')->findBy(
            array('edicao' => $edicao->getEdicaoId())
        );

        $arrCampus       = array(); // Opçõs de Câmpus
        $arrCampusCursos = array(); // Opções de Cursos nos Câmpus
        $arrCursoTurnos  = array(); // Opções de Turnos dos Cursos

        /** @var $selecaoCurso \Vestibular\Entity\SelecaoCursos */
        foreach ($selecaoCursos as $selecaoCurso) {
            $campusCurso = $selecaoCurso->getCursocampus();
            $campus      = $campusCurso->getCamp();
            $curso       = $campusCurso->getCurso();

            // Agrupa informações dos Câmpus
            $arrCampus[$campus->getCampId()] = array(
                'idCampus'   => $campus->getCampId(),
                'nomeCampus' => $campus->getCampNome(),
            );

            // Agrupa informações Cursos por Câmpus
            $arrCampusCursos[$campus->getCampId()][$selecaoCurso->getSelcursosId()] = [
                'idCursoSelecao' => $selecaoCurso->getSelcursosId(),
                'idCurso'        => $curso->getCursoId(),
                'nomeCurso'      => $curso->getCursoNome(),
            ];

            // Agrupa Turnos dos Cursos disponíveis por selecaoCursosId
            $arrCursoTurnos[$selecaoCurso->getSelcursosId()] = $service->buscaPeriodosEdicao($selecaoCurso);
        }

        $boleto         = $this->services()->getService('Boleto\Service\Boleto')->buscaBoletoPorPessoa(
            $entityPessoa->getPes()->getPesId()
        );
        $boleto         = $boleto[sizeof($boleto) - 1];
        $dataVencimento = (new \DateTime($boleto['data_vencimento']));
        $dataAtual      = (new \DateTime());

        if ($dataAtual > $dataVencimento) {
            $gerarNovaDataVencimento = true;
        } else {
            $gerarNovaDataVencimento = false;
        }

        if ($boleto['titulo_estado'] == "Aberto") {
            $this->view->setVariable('boletoId', $boleto['bol_id']);
            $this->view->setVariable('gerarNovaDataVencimento', $gerarNovaDataVencimento);
            $this->view->setVariable('boletoTitulo', $boleto['titulo_id']);
            $this->view->setVariable('boletoVia', $boleto['bol_via']);
            $this->view->setVariable('edicaoFim', 'Nao');
            $this->view->setVariable('pagou', 'nao');
        } else {
            $this->view->setVariable('pagou', 'sim');
        }

        if ($inscricao->getInscricaoDataHorarioProva()) {
            $dataProva = $inscricao->getInscricaoDataHorarioProva()->format("d/m/Y H:i");
        } else {
            $dataProva = $dataProva->getRealizacaoData()->format('d/m/Y H:i');
        }

        $data = [
            'statusInscricao' => $inscricao->getInscricaoStatus(),
            //Status da inscricao do candidato no processo de vestibular
            'edicaoDescricao' => $edicao->getEdicaoDescricao(),
            //Descricao da edicao do curso do vestibulando
            'dataProva'       => $dataProva,
            //Data da realização da prova do candidato
            'pesNome'         => $entityPessoa->getPes()->getPesNome(),
            //Nome do candidato
            'docEntregue'     => $inscricao->getInscricaoEntregaDoc()
        ];

        $form = new \Vestibular\Form\SelecaoInscricaoCadastro(array(), array(), array(), array());
        $form->setData($data);

        $edicaoIsencao = ($edicao->getEdicaoIsencao() == 'Sim') ? true : false;

        $this->view->setVariable('edicaoIsencao', $edicaoIsencao);
        $this->view->setVariable('form', $form);
        $this->view->setVariable('status', $data['statusInscricao']);
        $this->view->setVariable('inscricaoId', $inscricao->getInscricaoId());
        $this->view->setVariable('dados', $data);
        $this->view->setVariable('usuario', $usuario);
        $this->view->setVariable('recuperarSenha', new \Acesso\Form\RecuperarSenha());

        $this->view->setVariable('arrCampus', $arrCampus);
        $this->view->setVariable('arrCampusCursos', $arrCampusCursos);
        $this->view->setVariable('arrCursoTurnos', $arrCursoTurnos);
        $this->view->setVariable('cursosSelects', $cursosSelects);
        $this->view->setVariable('campusSelects', $campusSelects);
        $this->view->setVariable('opcoesCurso', $opcoesCurso);

        if ($service->verificaCorrecao($inscricao->getEdicao()->getEdicaoId())) {
            $this->view->setVariable('classificacao', $service->buscaClassificacaoCandidato($inscricao));
        }

        $this->view->setTerminal(true);

        return $this->view;
    }

    /**
     * Função para gerar a ficha de inscrição.
     * */
    public function fichaAction()
    {
        $id = $this->params()->fromRoute('id');
        /** @var  \Vestibular\Entity\SelecaoInscricao $insc */
        $insc = $this->getEm()->getRepository('Vestibular\Entity\SelecaoInscricao')->findOneBy(['inscricaoId' => $id]);
        /** @var  \Vestibular\Entity\SelecaoEdicao $edicaoEntity */
        $edicaoEntity = $insc->getEdicao();

        $edicaoId = $edicaoEntity->getEdicaoId();

        /** @var \Vestibular\Entity\SelecaoDataRealizacao $edicao */
        $edicao = $this->getEm()->getRepository('Vestibular\Entity\SelecaoDataRealizacao')->findOneBy(
            ['edicao' => $edicaoId]
        );

        /** @var  \Pessoa\Entity\PessoaFisica $pes */
        $pes = $this->getEm()->getRepository('Pessoa\Entity\PessoaFisica')->findOneBy(
            ['pes' => $insc->getPes()->getPesId()]
        );
        /** @var $inscCursos \Vestibular\Entity\InscricaoCursos */
        $inscCursos = $this->getEm()->getRepository("Vestibular\Entity\InscricaoCursos")->findBy(
            ['inscricao' => $insc->getInscricaoId()]
        );
        $inscCursos = $inscCursos[0]->toArray();

        $dataRealizacao = $edicao->getRealizacaoData();

        $dados = [
            'inscricaoNumero' => $insc->getInscricaoId(),
            'edicao'          => $edicao->getEdicao()->getEdicaoDescricao(),
            'nome'            => $insc->getPes()->getPesNome(),
            'cpf'             => $pes->getPesCpf(),
            'rg'              => $pes->getPesRg(),
            'data'            => $dataRealizacao->format('d/m/y H:i'),
            'curso'           => $inscCursos['selcursos']->getCursocampus()->getCurso()->getCursoNome(),
            'turno'           => $inscCursos['inscricaoTurno'],
        ];

        if ($insc->getSelelocais()) {
            $localInscricao = $insc->getSelelocais()->getPredio();
            $endProva       = $localInscricao;

            $dados['endereco']   = $localInscricao->getPredioLogradouro();
            $dados['endNumero']  = $localInscricao->getPredioNumero();
            $dados['bairro']     = $localInscricao->getPredioBairro();
            $dados['cidade']     = $localInscricao->getPredioCidade();
            $dados['estado']     = $localInscricao->getPredioEstado();
            $dados['predioNome'] = $localInscricao->getPredioNome();
        } else {
            $dados['endereco']   = '';
            $dados['endNumero']  = '';
            $dados['bairro']     = '';
            $dados['cidade']     = '';
            $dados['estado']     = '';
            $dados['predioNome'] = '';
        }

        // if ($insc->getSala()) {
        //     $sala = $this->getEm()->getRepository('Infraestrutura\Entity\InfraSala')->findOneBy(['salaId' => $insc->getSala()]);
        //     $dados['sala'] = $sala->getSalaNome();
        // }

        $pdf              = new PdfModel();
        $serviceOrgCampus = new \Organizacao\Service\OrgCampus($this->getEntityManager());

        if ($insc->getInscricaoDataHorarioProva()) {
            $pdf->setVariable('dataProva', $insc->getInscricaoDataHorarioProva()->format("d/m/Y H:i"));
            $pdf->setVariable('horario', $insc->getInscricaoDataHorarioProva()->format("H:i"));
        }

        $pdf->setOption('paperSize', 'a4'); // Defaults to "8x11"
        $pdf->setOption('paperOrientation', 'portrait'); // Defaults to "portrait"
        $pdf->setVariable('dados', $dados);
        $pdf->setVariable('endProva', $endProva);
        $pdf->setVariable('edicaoObservacao', $edicao->getEdicao()->getEdicaoObservacao());
        $pdf->setVariables(
            $serviceOrgCampus->retornaDadosInstituicao($inscCursos['selcursos']->getCursocampus()->getCamp())
        );
        $pdf->setTerminal(true);
        $pdf->setTemplate("/vestibular/selecao-inscricao/ficha");

        return $pdf;
    }

    /*
     * Action Edit
     * Esta action e utilizada pela instiuição que conclui o cadastro do vestibulando pela view 'edit' e tambem pela instituiçao que edita o candidato na view 'cadastro'.
     */

    public function editAction()
    {
        $request                 = $this->getRequest();
        $serviceSelecaoInscricao = new \Vestibular\Service\SelecaoInscricao($this->getEntityManager());
        $serviceSelecaoEdicao    = new \Vestibular\Service\SelecaoEdicao($this->getEntityManager());
        $servicePessoa           = new \Pessoa\Service\Pessoa($this->getEntityManager());
        $serviceCampusCurso      = new \Matricula\Service\CampusCurso($this->getEntityManager());

        $inscricaoId = $this->params()->fromRoute('id', 0);
        $arrDados    = $serviceSelecaoInscricao->getArray($inscricaoId);

        if (!$arrDados) {
            $this->flashMessenger()->addErrorMessage("Candidato não existe!");

            return $this->redirect()->toRoute("vestibular/default", array("controller" => "selecao-inscricao"));
        }

        // Se houver tentativa de candidatos cuja a edição já expirou!
        // Caso o usuario não possa ser editado o mesmo é redirecionado para listagem novamente.
        if ($arrDados['vestibularFinalizado']) {
            $erro = $arrDados['edicaoDescricao'] . " encerrado! ";
            $erro .= "Candidato " . $arrDados['pesNome'] . " não pode ser editado!";
            $this->flashMessenger()->addInfoMessage($erro);

            return $this->redirect()->toRoute("vestibular/default", array("controller" => "selecao-inscricao"));
        }

        if ($arrDados['inscricaoDataFormacao']) {
            $inscricaoDataFormacao             = $arrDados['inscricaoDataFormacao'];
            $inscricaoDataFormacao             = $inscricaoDataFormacao->format('d/m/Y');
            $arrDados['inscricaoDataFormacao'] = $inscricaoDataFormacao;
        };

        /*
         * Satisfaz esta condição se: Algum dado estiver no post e se não há um curso cadastrado.
         * Este caso ocorrera quando o usuário estiver cadastrado, tiver uma inscrição, mas não a concluiu.
         */
        if ($request->isPost()) {
            $arrDados                    = array_merge(
                $arrDados,
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
            $arrDados['inscricaoStatus'] = $arrDados['inscricaoStatus'] ? $arrDados['inscricaoStatus'] : 'Incompleta';

            $salvar = $serviceSelecaoInscricao->salvarInscricao($arrDados);

            if ($salvar) {
                $this->flashMessenger()->addSuccessMessage('Dados de candidato salvos!');

                return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
            } else {
                $this->flashMessenger()->addErrorMessage($serviceSelecaoInscricao->getLastError());
            }
        }

        // Identificação do câmpus vinculado ao processo, recuperando nome do curso, id e id do câmpus.
        $arrConfiguracaoEdicao =
            $serviceSelecaoEdicao->retornaConfiguracoesEdicao($arrDados['edicaoId']);

        if (!$arrConfiguracaoEdicao['selecaoCursos']) {
            $erro = "Verifique se há algum Câmpus vinculado ao processo de Vestibular!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute("vestibular/default", array("controller" => "selecao-inscricao"));
        }

        $serviceSelecaoInscricao->formataDadosPost($arrDados);

        $arrCampusCurso         = $serviceCampusCurso->getArrSelect2();
        $arrPesNacionalidade    = $servicePessoa->getArrSelect2PesNacionalidade();
        $arrInscricaoEntregaDoc = $serviceSelecaoInscricao->getArrSelect2InscricaoEntregaDoc();
        $arrInscricaoStatus     = $serviceSelecaoInscricao->getArrSelect2InscricaoStatus();

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setVariable("arrCampusCurso", $arrCampusCurso);
        $this->getView()->setVariable("arrPesNacionalidade", $arrPesNacionalidade);
        $this->getView()->setVariable("arrInscricaoEntregaDoc", $arrInscricaoEntregaDoc);
        $this->getView()->setVariable("arrInscricaoStatus", $arrInscricaoStatus);

        $this->getView()->setVariables($arrConfiguracaoEdicao);
        $this->getView()->setTemplate($this->getTemplateToRoute());

        return $this->getView();
    }

    public function loginAction()
    {
        $this->setForm(new \Vestibular\Form\LoginVestibular());
        $request = $this->getRequest();

        if ($this->flashMessenger()->hasErrorMessages()) {
            $this->getView()->setVariable("erro", $this->flashMessenger()->getErrorMessages());
        }
        if ($this->flashMessenger()->hasInfoMessages()) {
            $this->view->setVariable("info", $this->flashMessenger()->getInfoMessages());
        }
        if ($this->flashMessenger()->hasWarningMessages()) {
            $this->view->setVariable("warning", $this->flashMessenger()->getWarningMessages());
        }
        if ($this->flashMessenger()->hasSuccessMessages()) {
            $this->view->setVariable("success", $this->flashMessenger()->getSuccessMessages());
        }

        session_destroy();

        if ($request->isPost()) {
            $adapter = new \Acesso\Adapter\Adapter($this->getEm());
            $adapter->setSenha($request->getPost("password"));
            $adapter->setLogin($request->getPost("username"));

            $session = new \Zend\Authentication\Storage\Session();

            $authService = new \Zend\Authentication\AuthenticationService();
            $authService->setStorage($session);

            $result = $authService->authenticate($adapter);
            //            pre($request->getPost()->toArray());
            $ret = $result->isValid();
            if ($ret) {
                $user                 = $authService->getIdentity()['user'];
                $user['cache_expire'] = 30;

                if (is_array($session_config_app = $this->getServiceLocator()->get("config")['session']['config'])) {
                    $user['cache_expire'] = $session_config_app['options']['cache_expire'];
                }

                $user['datetime_login'] = new \DateTime('now');
                $session->write($user);

                $srvModulos = new \Acesso\Service\AcessoFuncionalidades($this->getEm());
                $session    = new \Zend\Session\Container("AcessoVersaSkeleton");
                $modulos    = $srvModulos->buscaFuncionalidadesUsuario($user['usuario'], true);

                $session->sistemaSelected = $modulos['sis_id'];

                if ($modulos['mod_redireciona'] == "" || $modulos['mod_redireciona'] == null) {
                    $modulos['mod_redireciona'] = "/";
                }

                return $this->redirect()->toRoute(
                    'vestibular/default',
                    ['controller' => 'selecao-inscricao', 'action' => 'dados-candidato']
                );
            } elseif ($result->getCode() == -3) {
                $this->flashMessenger()->addErrorMessage("Credenciais inválidas!");

                return $this->redirect()->toRoute(
                    'vestibular/default',
                    ['controller' => 'selecao-inscricao', 'action' => 'login']
                );
            } elseif ($result->getCode() == -4) {
                $this->flashMessenger()->addInfoMessage($result->getMessages()[0]);

                return $this->redirect()->toRoute(
                    'vestibular/default',
                    ['controller' => 'selecao-inscricao', 'action' => 'login']
                );
            }

            return $this->redirect()->toRoute('vestibular/default');
        }

        $recuperarSenha = new \Acesso\Form\RecuperarSenha();
        $this->view->setTerminal(true);
        $this->view->setVariable('loginForm', $this->getForm());
        $this->view->setVariable('recuperarSenha', $recuperarSenha);

        return $this->view;
    }

    public function logoutAction()
    {
        $messages = $this->flashMessenger()->getErrorMessages();

        session_destroy();
        session_start();

        foreach ($messages as $m) {
            $this->flashMessenger()->addErrorMessage($m);
        }

        return $this->redirect()->toRoute(
            "vestibular/default",
            array("controller" => "selecao-inscricao", "action" => "login")
        );
    }

    protected function getEm()
    {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get("Doctrine\ORM\EntityManager");
        }

        return $this->em;
    }

    public function listaAprovadosAction()
    {
        $service     = $this->services()->getService($this->getService());
        $edicaoAtual = $service->buscaEdicaoAtual();
        if (!$edicaoAtual) {
            $this->flashMessenger()->addWarningMessage("Os cadernos de provas ainda não foram corrigidos");

            return $this->redirect()->toRoute("vestibular/default", ['controller' => 'selecao-inscricao']);
        }
        if (!$service->verificaCorrecao($edicaoAtual->getEdicaoId())) {
            $this->flashMessenger()->addWarningMessage("Os cadernos de provas ainda não foram corrigidos");

            return $this->redirect()->toRoute("vestibular/default", ['controller' => 'selecao-inscricao']);
        } else {
            $this->config['table']['labels'] = array();

            $this->config['table']['labels']['classificacao']  = array(
                'params' => 'classificacao',
                'text'   => 'Classificacao'
            );
            $this->config['table']['labels']['pes_nome']       = array(
                'params' => 'inscricao_nome',
                'text'   => 'Nome do Candidato'
            );
            $this->config['table']['labels']['inscricao_nota'] = array(
                'params' => 'inscricao_pontuacao',
                'text'   => 'Pontuacao (%)'
            );
            $this->config['table']['labels']['inscricao_id']   = array(
                'type'     => 'viewhelper',
                'name'     => 'matricular',
                'function' => 'getButton',
                'params'   => "inscricao_id",
                'text'     => 'Matricular',
            );
            $this->config['dados']                             = $service->buscaClassificacao(
                $service->buscaEdicaoAtual()->getEdicaoId()
            );
            $this->config['table']['dados']                    = $this->config['dados'];
            $this->config['pagination']['count']               = count($this->config['dados']);
            $this->config['count']                             = $this->config['pagination']['count'];
            $this->view->setVariable("config", $this->config);
            $this->view->setVariable("dados", $this->config['dados']);
            $this->view->setVariable('count', $this->config['count']);

            return $this->view;
        }
    }

    /**
     * Função provisória para exibir a quantidade de inscritos em um dia
     * mostrará também a quantidade de inscritos por boleto e por pagamento à vista.
     * */
    public function relatorioAction($inscLocal)
    {
        $request = $this->getRequest();

        $service             = new \Vestibular\Service\SelecaoInscricao($this->getEntityManager());
        $serviceSelecaoCurso = new \Vestibular\Service\SelecaoCursos($this->getEntityManager());
        $serviceCampusCurso  = new \Matricula\Service\CampusCurso($this->getEntityManager());
        $serviceOrgCampus    = new \Organizacao\Service\OrgCampus($this->getEntityManager());

        $edicao       = $service->buscaEdicaoAtual();
        $selecaoCurso = $serviceSelecaoCurso->getRepository('Vestibular\Entity\SelecaoCursos')->findOneBy(
            ['edicao' => $edicao->getEdicaoId()]
        );

        if ($request->isPost()) {
            $dados = $request->getPost()->toArray();

            $par['data_inicial'] = $service->formatDateAmericano($dados['dataInicio']);
            $par['data_final']   = $service->formatDateAmericano($dados['dataFim']);
            $relatorio           = $service->relatorioFinanceiroInscricoesVest($par);

            $local        = 0;
            $internet     = 0;
            $inscLocal    = 0;
            $inscIsento   = 0;
            $inscInternet = 0;

            $this->view->setTemplate('vestibular/selecao-inscricao/relatorio-caixa');

            foreach ($relatorio as $linhaRelatorio) {
                if ($linhaRelatorio['Origem'] == 'Local') {
                    $local += $linhaRelatorio['ValorPago'];
                    $inscLocal += $linhaRelatorio['Pagos'];
                } elseif ($linhaRelatorio['Origem'] == 'Internet') {
                    $internet += $linhaRelatorio['ValorPago'];
                    $inscInternet += $linhaRelatorio['Pagos'];
                } elseif ($linhaRelatorio['Origem'] == 'Isento') {
                    $inscIsento += $linhaRelatorio['Pagos'];
                }
            }

            $valorTotal = $local + $internet;
            $inscGeral  = $inscInternet + $inscLocal + $inscIsento;
            $this->view->setVariables(
                [
                    'dataIni'      => $service->formatDateBrasileiro($dados['dataInicio']),
                    'dataFim'      => $service->formatDateBrasileiro($dados['dataFim']),
                    'local'        => Moeda::decToMoeda($local),
                    'internet'     => Moeda::decToMoeda($internet),
                    'total'        => Moeda::decToMoeda($valorTotal),
                    'inscLocal'    => $inscLocal,
                    'inscInternet' => $inscInternet,
                    'inscIsento'   => $inscIsento,
                    'inscGeral'    => $inscGeral,
                ]
            );
            $this->view->setVariables(
                $serviceOrgCampus->retornaDadosInstituicao($selecaoCurso->getCursocampus()->getCamp())
            );

            $this->view->setTerminal(true);

            return $this->view;
        }

        $this->view->setVariables(
            [
                'formRelatorio' => new \Vestibular\Form\Relatorio(),
            ]
        );

        return $this->view;
    }

    public function redefinirSenhaAction()
    {
        $id = $this->params()->fromRoute('id', 0);

        $service = $this->services()->getService($this->getService());

        // recupera configurações de email da aplicação e seta no serviço
        $configEmail = $this->getServiceManager()->get('Config')['emailRecovery'];
        $service->setConfigEmail($configEmail);

        $linkRedirect = $this
            ->getServiceManager()
            ->get('ViewHelperManager')->get('ServerUrl')
            ->__invoke();
        $linkRedirect .= $this->url()->fromRoute('vestibular-login');
        $service->linkLoginCadidato = $linkRedirect;

        $inscricao = $this->getEm()->getReference($this->getEntity(), $id);
        $campus    = $inscricao->getSelelocais()->getPredio()->getCamp();

        $password   = $service->passGeneration();
        $novoAcesso = $service->redefinirSenha($inscricao, $password);
        if (isset($novoAcesso['contato']['E-mail'])) {
            $service->enviaDadosCadastro(
                $novoAcesso['contato']['E-mail'][0]->getConContato(),
                $novoAcesso['login'],
                $novoAcesso['senha'],
                false,
                $campus
            );
        }
        $this->flashMessenger()->addSuccessMessage(
            "Senha Alterada com sucesso!</br> Usuário: {$novoAcesso['login']} </br>Senha: {$novoAcesso['senha']}"
        );

        return $this->redirect()->toRoute("vestibular/default", array("controller" => "selecao-inscricao"));
    }

    public function alteraDataVencimentoTituloAction()
    {
        $request = $this->getRequest();
        $service = $this->services()->getService('Boleto\Service\FinanceiroTitulo');
        $result  = false;
        if ($request->isPost()) {
            $dados         = $request->getPost()->toArray();
            $dados['dias'] = 0;
            $titulo        = $service->alteraDataVencimento($dados);
            if ($titulo) {
                $result = true;
            }
        }
        $this->json->setVariable('result', $result);

        return $this->json;
    }

    public function alocacaoAction()
    {
        $serviceInscricao   = $this->services()->getService($this->getService());
        $serivceSalas       = $this->services()->getService('Infraestrutura\Service\InfraSala');
        $serviceInfraClassi = $this->services()->getService('Infraestrutura\Service\InfraClassificacao');
        $serviceLocais      = $this->services()->getService('Vestibular\Service\SelecaoLocais');
        $edicaoAtual        = $serviceInscricao->buscaEdicaoAtual();
        $edicaoLocais       = $serviceLocais->getRepository($serviceLocais->getEntity())->findBy(
            ['edicao' => $edicaoAtual->getEdicaoId()]
        );
        $predio             = $edicaoLocais[0]->getPredio();
        $inscricoes         = $serviceInscricao->buscaInscricaoAlfabetica($edicaoAtual->getEdicaoId());
        $salaClassificacao  = $serviceInfraClassi->getRepository($serviceInfraClassi->getEntity())->findOneBy(
            ['classificacaoNome' => 'Sala de Aula']
        );
        $salas              = $serivceSalas->getRepository($serivceSalas->getEntity())->findBy(
            ['predio' => $predio->getPredioId(), 'classificacao' => $salaClassificacao]
        );
        $inscIni            = 0;
        foreach ($salas as $sala) {
            if ($inscIni >= count($inscricoes)) {
                break;
            }
            for ($i = $inscIni; $i < $inscIni + $sala->getSalaCapacidade(); $i++) {
                if (!array_key_exists($i, $inscricoes)) {
                    break;
                }
                $inscricoes[$i]['sala_id'] = $sala->getSalaId();
                try {
                    $serviceInscricao->atualizaSala($inscricoes[$i]);
                } catch (\Exception $ex) {
                    $this->flashMessenger()->addErrorMessage($ex->getMessage());

                    return $this->redirect()->toRoute(
                        'vestibular/default',
                        ['controller' => 'selecao-inscricao', 'action' => 'index']
                    );
                }
            }
            $inscIni = $i;
        }
        if ($inscIni < count($inscricoes)) {
            $candFalta = count($inscricoes) - $inscIni;
            $this->flashMessenger()->addWarningMessage(
                "{$candFalta} inscritos ficaram sem alocação. Todas as salas existentes foram preenchidas."
            );
        } else {
            $this->flashMessenger()->addSuccessMessage("Candidatos alocados com sucesso!");
        }

        return $this->redirect()->toRoute(
            'vestibular/default',
            ['controller' => 'selecao-inscricao', 'action' => 'alocacao-manual']
        );
    }

    public function alocacaoManualAction()
    {
        $service = $this->services()->getService($this->getService());
        $request = $this->getRequest();
        if ($request->isPost()) {
            $dados = $request->getPost()->toArray();
            foreach ($dados['inscricao'] as $key => $value) {
                $inscricao = [
                    'inscricao_id' => $value,
                    'sala_id'      => $dados['sala'][$key]
                ];
                try {
                    $service->atualizaSala($inscricao);
                } catch (\Exception $ex) {
                    $this->flashMessenger()->addErrorMessage("Erro ao atualizar as salas do candidato Nº: {$value}");

                    return $this->redirect()->toRoute(
                        "vestibular/default",
                        ['controller' => 'selecao-inscricao', 'action' => 'alocacao-manual']
                    );
                }
            }
            $this->flashMessenger()->addSuccessMessage("Salas Atualizadas");

            return $this->redirect()->toRoute(
                "vestibular/default",
                ['controller' => 'selecao-inscricao', 'action' => 'alocacao-manual']
            );
        }
        $salasOcupadas = $service->buscaSalasAlocadas($service->buscaEdicaoAtual()->getEdicaoId());
        if (!$salasOcupadas) {
            return $this->redirect()->toRoute(
                "vestibular/default",
                ['controller' => 'selecao-inscricao', 'action' => 'alocacao']
            );
        }
        $this->view->setVariable('salas', $salasOcupadas);

        return $this->view;
    }

    public function candidatosSalaAction()
    {
        $request     = $this->getRequest();
        $service     = $this->services()->getService($this->getService());
        $serviceSala = $this->services()->getService('Infraestrutura\Service\InfraSala');
        if ($request->isPost()) {
            $dados         = $request->getPost()->toArray();
            $idSala        = $dados['id'];
            $idEdicaoAtual = $service->buscaEdicaoAtual()->getEdicaoId();
            $alunosSala    = $service->buscaAlunosPorSala($idSala);
            $salas         = $serviceSala->mountSelect();
            $form          = new \Vestibular\Form\CandidatoSala($salas);
            $this->view->setVariable('form', $form);
            $this->view->setVariable('alunos', $alunosSala);
            $this->view->setTerminal(true);

            return $this->view;
        }
    }

    public function impressaoFichaAction()
    {
        $request = $this->getRequest();
        $service = $this->services()->getService($this->getService());

        if ($request->isPost()) {
            $dados      = $request->getPost()->toArray();
            $inscricoes = array();
            if ($dados['tipo'] == 'todos') {
                //                $inscricoes = $service->getRepository($service->getEntity())->findBy(['edicao' => $service->buscaEdicaoAtual(), 'inscricaoStatus' => 'Aceita', 'sala' => $dados['sala']]);
                $tmp = $service->buscaInscricaoPorSalas($dados['sala']);
                foreach ($tmp as $insc) {
                    $inscricoes[] = $service->getRepository($service->getEntity())->findOneBy(
                        ['inscricaoId' => $insc['inscricao_id']]
                    );
                }
            } else {
                foreach ($dados['inscricao'] as $insc) {
                    $tmp = $service->getRepository($service->getEntity())->findOneBy(['inscricaoId' => $insc]);
                    if ($tmp) {
                        $inscricoes[] = $tmp;
                    }
                }
            }

            $pdf = new PdfModel();
            $pdf->setOption('paperSize', 'a4'); // Defaults to "8x11"
            $pdf->setOption('paperOrientation', 'portrait'); // Defaults to "portrait"

            if ($dados['tipoFicha'] === 'redacao') {
                $pdf->setTemplate("vestibular/selecao-inscricao/impressao-ficha-redacao-pdf");
            } else {
                if ($dados['tipoFicha'] === 'provas') {
                    $pdf->setTemplate("vestibular/selecao-inscricao/impressao-ficha-gabarito-pdf");
                }
            }

            $pdf->setVariables(['inscricoes' => $inscricoes]);

            return $pdf;
        }

        $salaService = $this->services()->getService('Infraestrutura\Service\InfraSala');
        $form        = new \Vestibular\Form\ImpressaoFicha($salaService->mountSelect());
        $this->view->setVariable('form', $form);

        return $this->view;
    }

    public function enviaBoletoAtrasadoEmailAction()
    {
        /** @var \Vestibular\Entity\SelecaoEdicao $ultimaEdicao */
        $ultimaEdicao = (new \Vestibular\Service\SelecaoEdicao($this->getEm()))->buscaUltimaEdicao();
        $boletos      = $this->services()->getService($this->getService())->buscaCandidatosBoletoAtrasado(
            $ultimaEdicao->getEdicaoId()
        );

        if (!$ultimaEdicao) {
            $this->flashMessenger()->addErrorMessage("Nenhuma edição localizada!");

            return false;
        }

        /** @var \Vestibular\Entity\SelecaoCursos $objSelecaoCursos */
        $objSelecaoCursos = (
        new \Vestibular\Service\SelecaoCursos(
            $this->getEm()
        )
        )->getRepository()->findOneBy(
                ['edicao' => $ultimaEdicao->getEdicaoId()]
            );

        $listEmails = [];

        foreach ($boletos as $boleto) {
            $linkRedirect = $this
                ->getServiceManager()
                ->get('ViewHelperManager')->get('ServerUrl')
                ->__invoke();

            if ($boleto['E-mail'] != "") {
                $linkRedirect .= $this->url()->fromRoute(
                        'boleto-bancario/default',
                        ['controller' => 'boleto', 'action' => 'exibe']
                    ) . '/' . $boleto['boleto'];
                $boleto['E-mail'] = strtolower($boleto['E-mail']);
                $html             = "
                    <p>Olá.</p>
                    <p>Estamos honrados em receber sua inscrição no Vestibular {$objSelecaoCursos->getCursocampus()->getCamp()->getIes()->getIesNome()} {$ultimaEdicao->getEdicaoDescricao()} {$ultimaEdicao->getEdicaoAno()}.</p>
                    <p>Você escolheu uma instituição que preza pela qualidade do ensino, formando profissionais com excelência. </p>
                    <p>É um passo importante rumo a uma carreira de sucesso. </p>
                    <p>Verificamos que sua inscrição no processo seletivo ainda não foi concluída. A inscrição será confirmada com o pagamento do boleto gerado no momento em que se inscreveu.</p>
                    <p>Para agilizar o processo estamos lhe enviando a 2ª via do boleto. </p>
                    <a href=$linkRedirect>Clique aqui para visualizar o Boleto</a></p>
                    <p>Venha fazer parte de nossa história.</p>
                    <p></p>
                    <p>Fazer {$objSelecaoCursos->getCursocampus()->getCurso()->getCursoNome()} é bom. Na {$objSelecaoCursos->getCursocampus()->getCamp()->getIes()->getIesNome()}, é ótimo. </p>
                    <p></p>
                    <p>Atenciosamente </p>
                    <p></p>
                    <p></p>
                    <p>*Caso tenha efetuado o pagamento, desconsidere esta mensagem. </p>
                ";

                try {
                    $email                   = $this->services()->getService('Pessoa\Service\Pessoa')->enviaEmail(
                        $html,
                        $boleto['E-mail'],
                        "{$objSelecaoCursos->getCursocampus()->getCamp()->getIes()->getIesNome()} ",
                        "2ª via do boleto - {$objSelecaoCursos->getCursocampus()->getCamp()->getCampNome()} "
                    );
                    $listEmails['eviados'][] = $boleto['E-mail'];
                } catch (\Exception $e) {
                    $listEmails['nao-eviados'][] = $boleto['E-mail'] . " - " . $e->getMessage();
                }
            }
        }
        pre($listEmails);
    }

    public function enviaCartaoInscricaoAction()
    {
        $ultimaEdicao     = (new \Vestibular\Service\SelecaoEdicao($this->getEm()))->buscaUltimaEdicao();
        $inscricoes       = $this->services()->getService($this->getService())->buscaCandidatosInscricaoAceita(
            $ultimaEdicao->getEdicaoId()
        );
        $serviceOrgCampus = new \Organizacao\Service\OrgCampus($this->getEm());

        $listEmails = [];

        foreach ($inscricoes as $inscricao) {
            $linkRedirect = $this
                ->getServiceManager()
                ->get('ViewHelperManager')->get('ServerUrl')
                ->__invoke();

            $campus  = '';
            $assunto = '';
            $campus  = $serviceOrgCampus->retornaCampus(['local' => $inscricao['local']]);

            if (!$campus) {
                continue;
            }

            $assunto = 'Cartão de Inscrição - ' . $campus["instituto"] . ' ' . $inscricao['dataEdicao'];

            if ($inscricao['e-mail'] != "") {
                $linkRedirect .= $this->url()->fromRoute(
                        'vestibular/default',
                        ['controller' => 'selecao-inscricao', 'action' => 'ficha']
                    ) . '/' . $inscricao['Inscrição'];
                $inscricao['e-mail'] = strtolower($inscricao['e-mail']);
                $html                = "
                    <p>Olá.</p>
                    <p>Estamos honrados em receber sua inscrição no Vestibular {$campus["instituto"]} - {$inscricao['edicao_descricao']}.</p>
                    <p>Você escolheu uma instituição que preza pela qualidade do ensino, formando profissionais com excelência. </p>
                    <p>É um passo importante rumo a uma carreira de sucesso. </p>
                    <p>Verificamos que sua inscrição no processo seletivo foi concluída, sendo assim, estamos lhe enviando o Cartão de Inscrição referente a sua inscrição nesta edição do vestibular</p>
                    <a href=$linkRedirect>Clique aqui para visualizar o Cartão de Inscrição</a></p>
                    <p>Venha fazer parte de nossa história.</p>
                    <p></p>
                    <p>Atenciosamente </p>
                    <p></p>
                    <p>{$campus["nome"]} - {$campus["instituto"]}. </p>
                    <p></p>
                    <p></p>
                ";

                try {
                    $email                   = $this->services()->getService('Pessoa\Service\Pessoa')->enviaEmail(
                        $html,
                        $inscricao['e-mail'],
                        "{$campus["instituto"]}",
                        $assunto
                    );
                    $listEmails['eviados'][] = $inscricao['e-mail'];
                } catch (\Exception $e) {
                    $listEmails['nao-eviados'][] = $inscricao['e-mail'] . " - " . $e->getMessage();
                }
            }
        }
        pre($listEmails);
    }

    public function impressaoFichaGabaritoAction()
    {
        $pdf = new PdfModel();
        $pdf->setOption('paperSize', 'a4'); // Defaults to "8x11"
        $pdf->setOption('paperOrientation', 'portrait'); // Defaults to "portrait"
        $pdf->setTemplate("vestibular/selecao-inscricao/impressao-ficha-gabarito-pdf");

        return $pdf;
    }
}