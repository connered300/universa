<?php


namespace Vestibular\Controller;

use DOMPDFModule\View\Model\PdfModel;
use VersaSpine\Controller\AbstractCoreController;
use Vestibular\Form\RelatorioVestibular;

class ListaAprovadosController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function indexAction()
    {
        $service     = $this->services()->getService("Vestibular\Service\SelecaoInscricao");
        $edicaoAtual = $service->buscaEdicaoAtual();
        if (!$edicaoAtual) {
            $this->flashMessenger()->addWarningMessage("Os cadernos de provas ainda não foram corrigidos");

            return $this->redirect()->toRoute("vestibular/default", ['controller' => 'selecao-inscricao']);
        }
        if (!$service->verificaCorrecao($edicaoAtual->getEdicaoId())) {
            $this->flashMessenger()->addWarningMessage("Os cadernos de provas ainda não foram corrigidos");

            return $this->redirect()->toRoute("vestibular/default", ['controller' => 'selecao-inscricao']);
        } else {
            //            $this->config['table']['labels'][] = array(
            //                'type' => 'viewhelper',
            //                'name' => 'agronomica',
            //                'function' => 'gerUrlProjetos',
            //                'params' => "lto_id",
            //                'text' => 'Acessar projetos'
            //            );
            $this->config['table']['labels'] = array();

            $this->config['table']['labels']['classificacao']  = array(
                'params' => 'classificacao',
                'text'   => 'Classificacao'
            );
            $this->config['table']['labels']['pes_nome']       = array(
                'params' => 'inscricao_nome',
                'text'   => 'Nome do Candidato'
            );
            $this->config['table']['labels']['inscricao_nota'] = array(
                'params' => 'inscricao_nota',
                'text'   => 'Nota'
            );
            $this->config['table']['labels']['inscricao_id']   = array(
                'type'     => 'viewhelper',
                'name'     => 'matricular',
                'function' => 'getButton',
                'params'   => "inscricao_id",
                'text'     => 'Matricular',
            );
            //Classificacao
            $this->config['dados']               = $service->buscaClassificacaoVestibular(
                $service->buscaEdicaoAtual()->getEdicaoId(),
                'inscricao_classificacao'
            );
            $this->config['table']['dados']      = $this->config['dados'];
            $this->config['pagination']['count'] = count($this->config['dados']);
            $this->config['count']               = $this->config['pagination']['count'];
            $this->view->setVariable("config", $this->config);
            $this->view->setVariable("dados", $this->config['dados']);
            $this->view->setVariable('count', $this->config['count']);

            return $this->view;
        }
    }

    public function relatorioVestibularAction()
    {
        $form             = new RelatorioVestibular();
        $serviceOrgCampus = new \Organizacao\Service\OrgCampus($this->getEntityManager());
        $request          = $this->getRequest();
        $service          = $this->services()->getService('Vestibular\Service\SelecaoInscricao');
        $ObjEdicoes = $service->getRepository('Vestibular\Entity\SelecaoEdicao')->findAll();

        foreach($ObjEdicoes as $key=>$edicao){
            $arrEdicoesVest[$edicao->getEdicaoId()]= $edicao->getEdicaoDescricao();
        }



        if ($request->isPost()) {
            $dados               = $request->getPost()->toArray();
            $serviceSelecaoCurso = $this->services()->getService('Vestibular\Service\SelecaoCursos');
            $serviceCampusCurso  = $this->services()->getService('Matricula\Service\CampusCurso');
            $selecaoCurso        = $serviceSelecaoCurso->getRepository('Vestibular\Entity\SelecaoCursos')->findOneBy(
                ['edicao' => $dados['edicaoId']]
            );
            //            $classificados = $service->buscaAprovados($ediçãoId,$dados);
            $classificados    = $service->buscaClassificacaoVestibular2($dados['edicaoId'],$dados['tipo'], $dados['ordem']);
            $edicaoDescricao['edicao'] = $arrEdicoesVest[$dados['edicaoId']];

            $pdf = new PdfModel();
            //            $pdf->setOption('paperSize', 'a4'); // Defaults to "8x11"
            $pdf->setOption('paperOrientation', 'portrait'); // Defaults to "portrait"
            $pdf->setTemplate('vestibular/lista-aprovados/classificados');
            $pdf->setVariables(
                [
                    'classificados' => $classificados,
                    'edicao'        => $edicaoDescricao,
                    'ordem'         => $dados['ordem'],
                    'tipo'          => $dados['tipo']
                ]
            );

            $pdf->setVariables($serviceOrgCampus->retornaDadosInstituicao($selecaoCurso->getCursocampus()->getCamp()));

            return $pdf;
        }

        $form = new \Vestibular\Form\RelatorioVestibular($arrEdicoesVest);


        $this->view->setVariable("form", $form);

        return $this->view;
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {

    }

    public function searchAction()
    {
    }
}