<?php


namespace Vestibular\Controller;

use VersaSpine\Controller\AbstractCoreController;

class DashboardController extends AbstractCoreController{
    public function __construct() {
        parent::__construct(__CLASS__);
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {

    }

    public function indexAction()
    {
        $service = $this->services()->getService('Vestibular\Service\DashboardService');
        $serviceInscricao = $this->services()->getService('Vestibular\Service\SelecaoInscricao');
        $edicaoAtual = $serviceInscricao->buscaEdicaoAtual();
        $inscTipo = $service->relatorioInscTipo($edicaoAtual);
        $this->view->setVariables([
            'inscTipo' => $inscTipo
        ]);
        return $this->view;
    }

}