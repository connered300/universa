<?php

namespace Vestibular\Controller;

use VersaSpine\Controller\AbstractCoreController;

class InscricaoCursosController extends AbstractCoreController
{
    public function searchForJsonAction()
    {
        $request                      = $this->getRequest();
        $paramsGet                    = $request->getQuery()->toArray();
        $paramsPost                   = $request->getPost()->toArray();
        $serviceInscricaoCursos = new \Vestibular\Service\InscricaoCursos($this->getEntityManager());

        $result = $serviceInscricaoCursos->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceInscricaoCursos = new \Vestibular\Service\InscricaoCursos($this->getEntityManager());
        $serviceInscricaoCursos->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceInscricaoCursos = new \Vestibular\Service\InscricaoCursos($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceInscricaoCursos->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function addAction($incricaoCursosId)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados                        = array();
        $serviceInscricaoCursos = new \Vestibular\Service\InscricaoCursos($this->getEntityManager());

        if ($incricaoCursosId) {
            $arrDados = $serviceInscricaoCursos->getArray($incricaoCursosId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", true);
                $this->getView()->setVariable("mensagem", $serviceInscricaoCursos->getLastError());

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceInscricaoCursos->saveNotas($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Alterações salvas com sucesso!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceInscricaoCursos->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceInscricaoCursos->getLastError());
                }
            }
        }

        $serviceInscricaoCursos->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));


        return $this->getView();
    }

    public function editAction()
    {
        $identificador = $this->params()->fromRoute("id", 0);

        if (!$identificador) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($identificador);
    }
}
?>