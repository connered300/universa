<?php
namespace Vestibular\Controller;

use Matricula\Service\AcadperiodoLetivo;
use Matricula\Service\CampusCurso;
use VersaSpine\Controller\AbstractCoreController;
use Zend\View\Model\ViewModel;

class SelecaoEdicaoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function addAction()
    {
        $view    = new ViewModel();
        $request = $this->getRequest();

        /**
         * Instanciando todos os serviços utilizados dentro da action
         **/
        $serviceSelecaoEdicao   = new \Vestibular\Service\SelecaoEdicao($this->getEntityManager());
        $serviceSelecaoTipo     = new \Vestibular\Service\SelecaoTipo($this->getEntityManager());
        $serviceSelecaoLocal    = new \Vestibular\Service\SelecaoLocais($this->getEntityManager());
        $serviceCampusCurso     = new \Matricula\Service\CampusCurso($this->getEntityManager());
        $serviceBoletoConfConta = new \Boleto\Service\BoletoConfConta($this->getEntityManager());
        $servicePeriodoLetivo   = new AcadperiodoLetivo($this->getEntityManager());

        if ($request->isPost()) {
            $request = $request->getPost()->toArray();

            $request['edicaoAgendado']           = ($request['edicaoAgendado'] == 'on') ? 'Sim' : 'Não';
            $request['edicaoFiliacao']           = ($request['edicaoFiliacao'] == 'on') ? 'Sim' : 'Não';
            $request['edicaoIsencao']            = ($request['edicaoIsencao'] == 'on') ? 'Sim' : 'Não';
            $request['edicaoNumCursosOpcionais'] = (int)$request['edicaoNumCursosOpcionais'];

            $edicaoInicioInscricao = $serviceSelecaoEdicao->formatDateAmericano($request['edicaoInicioInscricao']);
            $edicaoFimInscricao    = $serviceSelecaoEdicao->formatDateAmericano($request['edicaoFimInscricao']);
            $edicaoDataDivulgacao  = $serviceSelecaoEdicao->formatDateAmericano($request['edicaoDataDivulgacao']);

            $request['edicaoInicioInscricao'] = new \DateTime($edicaoInicioInscricao);
            $request['edicaoFimInscricao']    = new \DateTime($edicaoFimInscricao);
            $request['edicaoDataDivulgacao']  = new \DateTime($edicaoDataDivulgacao);

            foreach ($request["edicaoDataRealizacao"] as $key => $data) {
                $edicaoDataRealizacao                  = $serviceSelecaoEdicao->formatDateAmericano($data);
                $request['edicaoDataRealizacao'][$key] = new \DateTime($edicaoDataRealizacao);
            }

            try {
                $serviceSelecaoEdicao->adicionar($request);
                $this->flashMessenger()->addSuccessMessage(
                    "Processo seletivo criado com sucesso! Cadastre um <strong><a href='/provas/prova-caderno/add'>Caderno de Provas</a></strong> para abrir as inscrições."
                );

                return $this->redirect()->toRoute('vestibular/default', ['controller' => 'selecao-edicao']);
            } catch (\Exception $ex) {
                throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
            }
        }

        $arrayTipo        = $serviceSelecaoTipo->mountArrayTipo();
        $option['tipo']   = $arrayTipo;
        $option['periodo'] = $servicePeriodoLetivo->getAllPerToForm();
        $option['predio']  = $serviceSelecaoLocal->retornaSelect2($serviceSelecaoLocal->retornaTodosLocais(),true);

        $arrayCampusCurso = $serviceCampusCurso->buscaCursoCampus();
        $confContas       = $serviceBoletoConfConta->buscaContas();

        $form = new \Vestibular\Form\SelecaoEdicao(null, $option);

        $form->get("confcont")->setValueOptions($confContas);

        $view->setVariable('form', $form);
        $view->setVariable('arrayCampusCurso', $arrayCampusCurso);
        $view->setVariable('arrPredio', $option['predio']);

        return $view;
    }

    public function editAction()
    {
        $view    = new ViewModel();
        $request = $this->getRequest();

        $serviceSelecaoEdicao   = new \Vestibular\Service\SelecaoEdicao($this->getEntityManager());
        $serviceSelecaoTipo     = new \Vestibular\Service\SelecaoTipo($this->getEntityManager());
        $serviceCampusCurso     = new \Matricula\Service\CampusCurso($this->getEntityManager());
        $serviceCampus          = new \Organizacao\Service\OrgCampus($this->getEntityManager());
        $serviceBoletoConfConta = new \Boleto\Service\BoletoConfConta($this->getEntityManager());
        $serviceSelecaoLocal    = new \Vestibular\Service\SelecaoLocais($this->getEntityManager());

        if ($request->isPost()) {
            $request                             = $request->getPost()->toArray();
            $request['edicaoAgendado']           = ($request['edicaoAgendado'] == 'on') ? 'Sim' : 'Não';
            $request['edicaoFiliacao']           = ($request['edicaoFiliacao'] == 'on') ? 'Sim' : 'Não';
            $request['edicaoIsencao']            = ($request['edicaoIsencao'] == 'on') ? 'Sim' : 'Não';
            $request['edicaoNumCursosOpcionais'] = (int)$request['edicaoNumCursosOpcionais'];

            $edicaoInicioInscricao = $serviceSelecaoEdicao->formatDateAmericano($request['edicaoInicioInscricao']);
            $edicaoFimInscricao    = $serviceSelecaoEdicao->formatDateAmericano($request['edicaoFimInscricao']);
            $edicaoDataDivulgacao  = $serviceSelecaoEdicao->formatDateAmericano($request['edicaoDataDivulgacao']);

            $request['edicaoInicioInscricao'] = new \DateTime($edicaoInicioInscricao);
            $request['edicaoFimInscricao']    = new \DateTime($edicaoFimInscricao);
            $request['edicaoDataDivulgacao']  = new \DateTime($edicaoDataDivulgacao);

            foreach ($request["edicaoDataRealizacao"] as $key => $data) {
                $edicaoDataRealizacao                  = $serviceSelecaoEdicao->formatDateAmericano($data);
                $request['edicaoDataRealizacao'][$key] = new \DateTime($edicaoDataRealizacao);
            }

            if (!$serviceSelecaoEdicao->edita($request)) {
                $this->flashMessenger()->addErrorMessage($serviceSelecaoEdicao->getLastError());
            }

            $this->flashMessenger()->addSuccessMessage("Salvo com sucesso");

            return $this->redirect()->toRoute("vestibular/default", ['controller' => 'selecao-edicao']);
        }

        $arrayTipo         = $serviceSelecaoTipo->mountArrayTipo();
        $confContas        = $serviceBoletoConfConta->buscaContas();
        $option['tipo']    = $arrayTipo;
        $option['predio']  = $serviceSelecaoLocal->retornaSelect2($serviceSelecaoLocal->retornaTodosLocais(),true);

        $form = new \Vestibular\Form\SelecaoEdicao(null, $option);

        $form->get("confcont")->setValueOptions($confContas);

        $id = $this->params()->fromRoute('id');

        /* @var $entityEdicao \Vestibular\Entity\SelecaoEdicao */
        $entityEdicao = $serviceSelecaoEdicao->getRepository()->find($id);

        $local = ($serviceSelecaoLocal->retornaSelect2(($serviceSelecaoLocal->pesquisaForJson(['edicaoId' => $id])),true));

        /**
         * Buscando pelas datas de realização do processo seletivo
         * */
        $datasRealizacao = $serviceSelecaoEdicao->executeQuery(
            "SELECT * FROM selecao_data_realizacao WHERE edicao_id = '{$id}'"
        );

        foreach ($datasRealizacao as $key => $data) {
            $arrayDatas[$key] = [
                'id'      => $data['realizacao_id'],
                'data'    => (new \DateTime($data['realizacao_data']))->format('d/m/Y'),
                'horario' => (new \DateTime($data['realizacao_data']))->format('H:i'),
            ];
        }

        /**
         * Buscando pelos tipos de egresso que este vestibular possui.
         * */
        $edicaoTiposSelecao = $serviceSelecaoEdicao->executeQuery(
            "SELECT
                              selecao_tipo_edicao.tiposel_id, selecao_tipo.tiposel_nome
                            FROM
                              selecao_tipo_edicao
                            INNER JOIN
                              selecao_tipo ON selecao_tipo_edicao.tiposel_id = selecao_tipo.tiposel_id
                            WHERE
                             edicao_id = '{$id}'"
        );

        foreach ($edicaoTiposSelecao as $key => $tiposel) {
            $arrayTiposel[$key] = $tiposel['tiposel_id'];
        }

        $arrayTiposel = implode(",", $arrayTiposel);

        /**
         * Buscando pelos locais que o processo possui.
         * */
        $edicaoLocais = $this->services()
            ->getService("Vestibular\Service\SelecaoEdicao")
            ->executeQuery(
                "SELECT
                                selecao_locais.predio_id
                            FROM
                                selecao_locais
                            WHERE
                                selecao_locais.edicao_id = '{$id}'"
            );

        foreach ($edicaoLocais as $key => $locais) {
            $arrayLocais[$key] = $locais['predio_id'];
        }

        $arrayLocais = implode(",", $arrayLocais);

        /**
         * Buscando pelos Cursos do processo seletivo.
         * */
        $selecaoCursos = $this->services()
            ->getService("Vestibular\Service\SelecaoEdicao")
            ->executeQuery(
                "
            SELECT
                selcursos_id, selecao_cursos.edicao_id, selecao_cursos.cursocampus_id, curso_nome, selcursos_vagas_matutino, selcursos_vagas_vespertino, selcursos_vagas_noturno, selcursos_valor_inscricao, camp_nome, org_campus.camp_id, acad_curso.curso_id
            FROM
                selecao_cursos
            INNER JOIN
                selecao_edicao ON selecao_cursos.edicao_id = selecao_edicao.edicao_id
            INNER JOIN
                campus_curso ON campus_curso.cursocampus_id = selecao_cursos.cursocampus_id
            INNER JOIN
                acad_curso ON acad_curso.curso_id = campus_curso.curso_id
            INNER JOIN
                org_campus ON campus_curso.camp_id = org_campus.camp_id
            WHERE
                selecao_edicao.edicao_id = '{$id}'
            "
            );
        foreach ($selecaoCursos as $key => $cursos) {
            $arrayCursos[$key] = $cursos;
        }

        $arrayCampusCurso = $serviceCampusCurso->buscaCursoCampus();
        $arrayCampus = $serviceCampus->getArrSelect2();

        $view->setVariable('arrayCursos', $arrayCursos);
        $view->setVariable('arrayCampusCurso', $arrayCampusCurso);
        $view->setVariable('arrayCampus', $arrayCampus);

        $formulario = $entityEdicao->toArray();

        $formulario['edicaoInicioInscricao']   = $formulario['edicaoInicioInscricao']->format("d/m/Y");
        $formulario['edicaoFimInscricao']      = $formulario['edicaoFimInscricao']->format("d/m/Y");
        $formulario['edicaoDataDivulgacao']    = $formulario['edicaoDataDivulgacao']->format("d/m/Y");
        $formulario['selcursosValorInscricao'] = $arrayCursos[0]['selcursos_valor_inscricao'];
        //        pre($arrayCursos);

        $formulario['tiposel'] = $arrayTiposel;
        $formulario['predio']  = $arrayLocais;
        $form->setData($formulario);

        $view->setVariable('edicaoFiliacao', $entityEdicao->getEdicaoFiliacao());
        $view->setVariable('edicaoIsencao', $entityEdicao->getEdicaoIsencao());
        $view->setVariable('edicaoNumCursosOpcionais', $entityEdicao->getEdicaoNumCursosOpcionais());
        $view->setVariable('edicaoAgendado', $formulario['edicaoAgendado']);
        $view->setVariable("arrayDatas", $arrayDatas);
        $view->setVariable('arrayTiposel', $arrayTiposel);
        $view->setVariable('form', $form);
        $view->setVariable('arrPredio', $option['predio']);
        $view->setVariable('local', (!empty($local)) ? $local : array());
        $view->setVariable(
            'confcont',$entityEdicao->getConfcont() ? $entityEdicao->getConfcont()->getConfcontId() : 'NULL');

        return $view;
    }

    public function showFormAction()
    {
        $arrayTipo        = $this->services()->getService("Vestibular\Service\SelecaoTipo")->mountArrayTipo();
        $option['tipo']   = $arrayTipo;
        $option['predio'] = $this->services()->getService("Infraestrutura\Service\InfraPredio")->mountArrayPredios();

        $this->view->setTerminal(true);
        $form = $this->getForm();

        if (!class_exists($form)) {
            $this->view->setVariable("erro", "Formulário de busca não foi encontrado!");

            return $this->view;
        }

        $form = new \Vestibular\Form\SelecaoEdicao(null, $option);
        $form->remove('tiposel')
            ->remove('edicaoEdital')
            ->remove('predio')
            ->remove('camp')
            ->remove('edicaoAgendado')
            ->remove('edicaoSemestre')
            ->remove('confcont')
            ->remove('selcursoValorInscricao');

        $form->setAttribute("method", 'get');
        $this->view->setVariable("config", $this->config);
        $this->view->setVariable("form", $form);

        return $this->view;
    }

    /**
     * Função para gerar o select dos cursos.
     * */
    public function optionAction()
    {
        $serviceCampusCurso =  new CampusCurso($this->getEntityManager());
        $view = new ViewModel();

        $request = $this->getRequest()->getPost()->toArray();

        $cursos = $serviceCampusCurso->pesquisaForJson($request);

        $view->setTerminal(true);
        $view->setVariable("cursos", $cursos);

        return $view;
    }

    /**
     * Action para verificar se existe um processo sletivo na mesma data em que o usuário está tentando cadastrar.
     * */
    public function verificaEdicaoAction()
    {
        $view = new \Zend\View\Model\JsonModel();
        $view->setTerminal(true);
        $request = $this->getRequest()->getPost()->toArray();

        $service           = $this->services()->getService($this->getService());
        $request['string'] = $service->formatDateAmericano($request['string']);
        $year              = (new \DateTime($request['string']))->format('Y-m-d');

        $sql  = "SELECT * FROM selecao_edicao WHERE 1";
        $data = $service->executeQuery($sql)->fetchAll();

        foreach ($data as $date) {
            if (($year >= $date['edicao_inicio_inscricao']) && $year <= $date['edicao_fim_inscricao']) {
                //                $entity = $this->getEm()->getRepository("Vestibular\Entity\SelecaoEdicao")->findOneBy(array('edicaoId' => $date['edicao_id']));
                $view->setVariable('nestaData', true);

                return $view;
            }
        }

        $view->setVariable('nestaData', false);

        return $view;
    }

    public function searchForJsonAction()
    {
        $request          = $this->getRequest();
        $paramsGet        = $request->getQuery()->toArray();
        $paramsPost       = $request->getPost()->toArray();
        $serviceSelecaoEdicao = new \Vestibular\Service\SelecaoEdicao($this->getEntityManager());

        $result = $serviceSelecaoEdicao->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $arrayData = $request->getPost()->toArray();

            $serviceAluno = $this->services()->getService($this->getService());
            $result       = $serviceAluno->search($arrayData);

            $this->json->setVariable("draw", $result["draw"]);
            $this->json->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->json->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->json->setVariable("data", $result["data"]);
        }

        return $this->json;
    }
}