<?php

namespace Vestibular\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SelecaoCursos
 *
 * @ORM\Table(name="selecao_cursos", uniqueConstraints={@ORM\UniqueConstraint(name="unq_edicao_curso", columns={"edicao_id", "cursocampus_id"})}, indexes={@ORM\Index(name="fk_selecao_cursos_selecao_edicao1_idx", columns={"edicao_id"}), @ORM\Index(name="fk_selecao_cursos_campus_curso1_idx", columns={"cursocampus_id"})})
 * @ORM\Entity
 */
class SelecaoCursos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="selcursos_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $selcursosId;

    /**
     * @var string
     *
     * @ORM\Column(name="selcursos_periodos", type="string", nullable=true)
     */
    private $selcursosPeriodos;

    /**
     * @var string
     *
     * @ORM\Column(name="selcursos_valor_inscricao", type="float", precision=10, scale=2, nullable=true)
     */
    private $selcursosValorInscricao;

    /**
     * @var integer
     *
     * @ORM\Column(name="selcursos_vagas_vespertino", type="integer", nullable=true)
     */
    private $selcursosVagasVespertino = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="selcursos_vagas_matutino", type="integer", nullable=true)
     */
    private $selcursosVagasMatutino = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="selcursos_vagas_noturno", type="integer", nullable=true)
     */
    private $selcursosVagasNoturno = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="selcursos_vagas_integral", type="integer", nullable=true)
     */
    private $selcursosVagasIntegral = '0';

    /**
     * @var \Matricula\Entity\CampusCurso
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\CampusCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cursocampus_id", referencedColumnName="cursocampus_id")
     * })
     */
    private $cursocampus;

    /**
     * @var SelecaoEdicao
     *
     * @ORM\ManyToOne(targetEntity="SelecaoEdicao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="edicao_id", referencedColumnName="edicao_id")
     * })
     */
    private $edicao;

    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getSelcursosId()
    {
        return $this->selcursosId;
    }

    /**
     * @param int $selcursosId
     */
    public function setSelcursosId($selcursosId)
    {
        $this->selcursosId = $selcursosId;
    }

    /**
     * @return string
     */
    public function getSelcursosPeriodos()
    {
        return $this->selcursosPeriodos;
    }

    /**
     * @param string $selcursosPeriodos
     */
    public function setSelcursosPeriodos($selcursosPeriodos)
    {
        $this->selcursosPeriodos = $selcursosPeriodos;
    }

    /**
     * @return int
     */
    public function getSelcursosVagas()
    {
        return $this->selcursosVagas;
    }

    /**
     * @param int $selcursosVagas
     */
    public function setSelcursosVagas($selcursosVagas)
    {
        $this->selcursosVagas = $selcursosVagas;
    }

    /**
     * @return string
     */
    public function getSelcursosValorInscricao()
    {
        return $this->selcursosValorInscricao;
    }

    /**
     * @param string $selcursosValorInscricao
     */
    public function setSelcursosValorInscricao($selcursosValorInscricao)
    {
        $this->selcursosValorInscricao = $selcursosValorInscricao;
    }

    /**
     * @return \Matricula\Entity\CampusCurso
     */
    public function getCursocampus()
    {
        return $this->cursocampus;
    }

    /**
     * @param \Matricula\Entity\CampusCurso $cursocampus
     */
    public function setCursocampus($cursocampus)
    {
        $this->cursocampus = $cursocampus;
    }

    /**
     * @return SelecaoEdicao
     */
    public function getEdicao()
    {
        return $this->edicao;
    }

    /**
     * @param SelecaoEdicao $edicao
     */
    public function setEdicao($edicao)
    {
        $this->edicao = $edicao;
    }

    /**
     * @return int
     */
    public function getSelcursosVagasVespertino()
    {
        return $this->selcursosVagasVespertino;
    }

    /**
     * @param int $selcursosVagasVespertino
     */
    public function setSelcursosVagasVespertino($selcursosVagasVespertino)
    {
        $this->selcursosVagasVespertino = $selcursosVagasVespertino;
    }

    /**
     * @return int
     */
    public function getSelcursosVagasMatutino()
    {
        return $this->selcursosVagasMatutino;
    }

    /**
     * @param int $selcursosVagasMatutino
     */
    public function setSelcursosVagasMatutino($selcursosVagasMatutino)
    {
        $this->selcursosVagasMatutino = $selcursosVagasMatutino;
    }

    /**
     * @return int
     */
    public function getSelcursosVagasNoturno()
    {
        return $this->selcursosVagasNoturno;
    }

    /**
     * @param int $selcursosVagasNoturno
     */
    public function setSelcursosVagasNoturno($selcursosVagasNoturno)
    {
        $this->selcursosVagasNoturno = $selcursosVagasNoturno;
    }

    /**
     * @return int
     */
    public function getSelcursosVagasIntegral()
    {
        return $this->selcursosVagasIntegral;
    }

    /**
     * @param int $selcursosVagasIntegral
     */
    public function setSelcursosVagasIntegral($selcursosVagasIntegral)
    {
        $this->selcursosVagasIntegral = $selcursosVagasIntegral;
    }

    public function toArray()
    {
        return array_merge(
            ($this->getEdicao() ? $this->getEdicao()->toArray() : array()),
            ($this->getCursocampus() ? $this->getCursocampus()->toArray() : array()),
            [
                'selcursosId'              => $this->getSelcursosId(),
                'selcursosPeriodos'        => $this->getSelcursosPeriodos(),
                'selcursosValorInscricao'  => $this->getSelcursosValorInscricao(),
                'selcursosVagasVespertino' => $this->getSelcursosVagasVespertino(),
                'selcursosVagasMatutino'   => $this->getSelcursosVagasMatutino(),
                'selcursosVagasNoturno'    => $this->getSelcursosVagasNoturno(),
                'selcursosVagasIntegral'   => $this->getSelcursosVagasIntegral(),
                'cursocampus'              => $this->getCursocampus()->getCursocampusId(),
                'edicao'                   => $this->getEdicao()->getEdicaoId(),
            ]
        );
    }
}
