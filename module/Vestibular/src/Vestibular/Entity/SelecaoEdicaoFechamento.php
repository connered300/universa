<?php

namespace Vestibular\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * SelecaoLocais
 *
 * @ORM\Table(name="selecao_edicao_fechamento", indexes={
 *      @ORM\Index(name="fk_acesso_pessoas_selecao_edicao_acesso_pessoas1_idx", columns={"id"}),
 *      @ORM\Index(name="fk_acesso_pessoas_selecao_edicao_selecao_edicao1_idx", columns={"edicao_id"})})
 * @ORM\Entity
 */
class selecaoEdicaoFechamento{

    /**
     * @var integer
     *
     * @ORM\Column(name="selecaofechamento_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $selecaoFechamento;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     *
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuariofechamento_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $usuarioFechamento;

    /**
     * @var SelecaoEdicao
     *
     * @ORM\ManyToOne(targetEntity="SelecaoEdicao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="edicao_id", referencedColumnName="edicao_id", nullable=false)
     * })
     */
    private $edicao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="selecaofechamento_data", type="datetime", nullable=false)
     */
    private $selecaoFechamentoData;

    /** @var integer
     *
     * @ORM\Column(name="classificacao_limite", type="integer", nullable=false)
     */
    private $classificacao_limite;

    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getSelecaoFechamento()
    {
        return $this->selecaoFechamento;
    }

    /**
     * @param int $selecaoFechamento
     * @return seleaoEdicaoFechamento
     */
    public function setSelecaoFechamento($selecaoFechamento)
    {
        $this->selecaoFechamento = $selecaoFechamento;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuarioFechamento()
    {
        return $this->usuarioFechamento;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuarioFechamento
     * @return seleaoEdicaoFechamento
     */
    public function setUsuarioFechamento($usuarioFechamento)
    {
        $this->usuarioFechamento = $usuarioFechamento;

        return $this;
    }

    /**
     * @return SelecaoEdicao
     */
    public function getEdicao()
    {
        return $this->edicao;
    }

    /**
     * @param SelecaoEdicao $edicao
     * @return seleaoEdicaoFechamento
     */
    public function setEdicao($edicao)
    {
        $this->edicao = $edicao;

        return $this;
    }

    /**
     * @return \Datetime
     */
    public function getSelecaoFechamentoData()
    {
        return $this->selecaoFechamentoData;
    }

    /**
     * @param \Datetime $selecaoFechamentoDat
     * @return seleaoEdicaoFechamento
     */
    public function setSelecaoFechamentoData($selecaoFechamentoData)
    {
        $this->selecaoFechamentoData = $selecaoFechamentoData;

        return $this;
    }

    /**
     * @return int
     */
    public function getClassificacaoLimite()
    {
        return $this->classificacao_limite;
    }

    /**
     * @param int $classificacao_limite
     * @return selecaoEdicaoFechamento
     */
    public function setClassificacaoLimite($classificacao_limite)
    {
        $this->classificacao_limite = $classificacao_limite;

        return $this;
    }


}