<?php

namespace Vestibular\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * SelecaoEdicao
 *
 * @ORM\Table(name="selecao_edicao", indexes={
 *      @ORM\Index(name="fk_selecao_edicao_periodo_letivo1_idx", columns={"per_id"}),
 *      @ORM\Index(name="fk_selecao_edicao_boleto_conf1_idx", columns={"confcont_id"}),
 *      @ORM\Index(name="fk_edicao_usuario_fechamento_idx", columns={"usuario"})})
 * @ORM\Entity
 * @LG\LG(id="edicao_id",label="edicao_id")
 * @Jarvis\Jarvis(title="Lista de Vestibulares",icon="fa fa-table")
 */
class SelecaoEdicao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="edicao_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="edicao_id")
     * @LG\Labels\Attributes(text="Índice",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $edicaoId;

    /**
     * @var integer
     *
     * @ORM\Column(name="edicao_ano", type="integer", nullable=false)
     * @LG\Labels\Property(name="edicao_ano")
     * @LG\Labels\Attributes(text="Ano do Processo",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $edicaoAno;
    /**
     * @var string
     *
     * @ORM\Column(name="edicao_semestre", type="string", nullable=true)
     * @LG\Labels\Property(name="edicao_semestre")
     * @LG\Labels\Attributes(text="Semestre",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE%")
     */
    private $edicaoSemestre = 'Primeiro';

    /**
     * @var string
     *
     * @ORM\Column(name="edicao_descricao", type="string", length=45, nullable=false)
     * @LG\Labels\Property(name="edicao_descricao")
     * @LG\Labels\Attributes(text="Descrição do Processo",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $edicaoDescricao;

    /**
     * @var string
     *
     * @ORM\Column(name="edicao_observacao", type="string", nullable=true)
     * @LG\Labels\Property(name="edicao_observacao")
     * @LG\Labels\Attributes(text="Observação",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $edicaoObservacao;

    /**
     * @var boolean
     *
     * @ORM\Column(name="edicao_sequencial", type="boolean", nullable=false)
     */
    private $edicaoSequencial = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="edicao_inicio_inscricao", type="date", nullable=false)
     * @LG\Labels\Property(name="edicao_inicio_inscricao")
     * @LG\Labels\Attributes(text="Início das Inscrições",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $edicaoInicioInscricao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="edicao_fim_inscricao", type="date", nullable=true)
     * @LG\Labels\Property(name="edicao_fim_inscricao")
     * @LG\Labels\Attributes(text="Término das Inscrições",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $edicaoFimInscricao;

    /**
     * @var string
     *
     * @ORM\Column(name="edicao_agendado", type="string", nullable=false)
     */
    private $edicaoAgendado = 'Não';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="edicao_data_divulgacao", type="date", nullable=false)
     * @LG\Labels\Property(name="edicao_data_divulgacao")
     * @LG\Labels\Attributes(text="Divulgação",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $edicaoDataDivulgacao;

    /**
     * @var string
     *
     * @ORM\Column(name="edicao_edital", type="string", length=45, nullable=true)
     */
    private $edicaoEdital;

    /**
     * @var string
     *
     * @ORM\Column(name="edicao_filiacao", type="string", nullable=false)
     */
    private $edicaoFiliacao = 'Não';

    /**
     * @var string
     *
     * @ORM\Column(name="edicao_isencao", type="string", nullable=false)
     */
    private $edicaoIsencao = 'Não';

    /**
     * @var integer
     *
     * @ORM\Column(name="edicao_num_cursos_opcionais", type="integer", nullable=false)
     */
    private $edicaoNumCursosOpcionais = 0;

    /**
     * @var \Financeiro\Entity\BoletoConfConta
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\BoletoConfConta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="confcont_id", referencedColumnName="confcont_id")
     * })
     */
    private $confcont;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Matricula\Entity\AcadperiodoLetivo", mappedBy="selecaoEdicao")
     */
    private $per;

    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getEdicaoId()
    {
        return $this->edicaoId;
    }

    /**
     * @param int $edicaoId
     */
    public function setEdicaoId($edicaoId)
    {
        $this->edicaoId = $edicaoId;
    }

    /**
     * @return int
     */
    public function getEdicaoAno()
    {
        return $this->edicaoAno;
    }

    /**
     * @param int $edicaoAno
     */
    public function setEdicaoAno($edicaoAno)
    {
        $this->edicaoAno = $edicaoAno;
    }

    /**
     * @return string
     */
    public function getEdicaoDescricao()
    {
        return $this->edicaoDescricao;
    }

    /**
     * @param string $edicaoDescricao
     */
    public function setEdicaoDescricao($edicaoDescricao)
    {
        $this->edicaoDescricao = $edicaoDescricao;
    }

    /**
     * @return boolean
     */
    public function isEdicaoSequencial()
    {
        return $this->edicaoSequencial;
    }

    /**
     * @param boolean $edicaoSequencial
     */
    public function setEdicaoSequencial($edicaoSequencial)
    {
        $this->edicaoSequencial = $edicaoSequencial;
    }

    /**
     * @return \DateTime
     */
    public function getEdicaoInicioInscricao()
    {
        return $this->edicaoInicioInscricao;
    }

    /**
     * @param \DateTime $edicaoInicioInscricao
     */
    public function setEdicaoInicioInscricao($edicaoInicioInscricao)
    {
        $this->edicaoInicioInscricao = $edicaoInicioInscricao;
    }

    /**
     * @return \DateTime
     */
    public function getEdicaoFimInscricao()
    {
        return $this->edicaoFimInscricao;
    }

    /**
     * @param \DateTime $edicaoFimInscricao
     */
    public function setEdicaoFimInscricao($edicaoFimInscricao)
    {
        $this->edicaoFimInscricao = $edicaoFimInscricao;
    }

    /**
     * @return string
     */
    public function getEdicaoAgendado()
    {
        return $this->edicaoAgendado;
    }

    /**
     * @param string $edicaoAgendado
     */
    public function setEdicaoAgendado($edicaoAgendado)
    {
        $this->edicaoAgendado = $edicaoAgendado;
    }

    /**
     * @return \DateTime
     */
    public function getEdicaoDataDivulgacao()
    {
        return $this->edicaoDataDivulgacao;
    }

    /**
     * @param \DateTime $edicaoDataDivulgacao
     */
    public function setEdicaoDataDivulgacao($edicaoDataDivulgacao)
    {
        $this->edicaoDataDivulgacao = $edicaoDataDivulgacao;
    }

    /**
     * @return string
     */
    public function getEdicaoEdital()
    {
        return $this->edicaoEdital;
    }

    /**
     * @param string $edicaoEdital
     */
    public function setEdicaoEdital($edicaoEdital)
    {
        $this->edicaoEdital = $edicaoEdital;
    }

    /**
     * @return string $edicaoFiliacao
     */
    public function getEdicaoFiliacao()
    {
        return $this->edicaoFiliacao;
    }

    /**
     * @param string $edicaoFiliacao
     */
    public function setEdicaoFiliacao($edicaoFiliacao)
    {
        $this->edicaoFiliacao = $edicaoFiliacao;
    }

    /**
     * @return string
     */
    public function getEdicaoIsencao()
    {
        return $this->edicaoIsencao;
    }

    /**
     * @param string $edicaoIsencao
     */
    public function setEdicaoIsencao($edicaoIsencao)
    {
        $this->edicaoIsencao = $edicaoIsencao;
    }

    /**
     * @return int
     */
    public function getEdicaoNumCursosOpcionais()
    {
        return $this->edicaoNumCursosOpcionais;
    }

    /**
     * @param int $edicaoNumCursosOpcionais
     * @return SelecaoEdicao
     */
    public function setEdicaoNumCursosOpcionais($edicaoNumCursosOpcionais)
    {
        $this->edicaoNumCursosOpcionais = $edicaoNumCursosOpcionais;

        return $this;
    }

    /**
     * @return string
     */
    public function getEdicaoSemestre()
    {
        return $this->edicaoSemestre;
    }

    /**
     * @param string $edicaoSemestre
     */
    public function setEdicaoSemestre($edicaoSemestre)
    {
        $this->edicaoSemestre = $edicaoSemestre;
    }

    /**
     * @return \Financeiro\Entity\BoletoConfConta
     */
    public function getConfcont()
    {
        return $this->confcont;
    }

    /**
     * @param \Financeiro\Entity\BoletoConfConta $confcont
     * @return SelecaoEdicao
     */
    public function setConfcont($confcont)
    {
        $this->confcont = $confcont;

        return $this;
    }

    /**
     * @return string
     */
    public function getEdicaoObservacao()
    {
        return $this->edicaoObservacao;
    }

    /**
     * @param string $edicaoObservacao
     * @return SelecaoEdicao
     */
    public function setEdicaoObservacao($edicaoObservacao)
    {
        $this->edicaoObservacao = $edicaoObservacao;

        return $this;
    }

    public function toArray()
    {
        $retorno = array_merge(
            $this->getConfcont() ? $this->getConfcont()->toArray() : array(),
            [
                'edicaoId'                 => $this->getEdicaoId(),
                'edicaoAno'                => $this->getEdicaoAno(),
                'edicaoInicioInscricao'    => $this->getEdicaoInicioInscricao(),
                'edicaoFimInscricao'       => $this->getEdicaoFimInscricao(),
                'edicaoAgendado'           => $this->getEdicaoAgendado(),
                'edicaoDataDivulgacao'     => $this->getEdicaoDataDivulgacao(),
                'edicaoEdital'             => $this->getEdicaoEdital(),
                'edicaoDescricao'          => $this->getEdicaoDescricao(),
                'edicaoObservacao'         => $this->getEdicaoObservacao(),
                'edicaoSemestre'           => $this->getEdicaoSemestre(),
                'edicaoFiliacao'           => $this->getEdicaoFiliacao(),
                'edicaoIsencao'            => $this->getEdicaoIsencao(),
                'edicaoNumCursosOpcionais' => $this->getEdicaoNumCursosOpcionais(),
                'per'                      => $this->getPer()
            ]
        );

        return $retorno;
    }

    /**
     * @param \Matricula\Entity\AcadperiodoLetivo $per
     */
    public function setPer($per)
    {
        $this->per = $per;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPer()
    {
        return $this->per;
    }
}
