<?php

namespace Vestibular\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * /**
 * SelecaoTipoEdicao
 *
 * @ORM\Table(name="selecao_tipo_edicao", indexes={@ORM\Index(name="fk_selecao_edicao_selecao_tipo_selecao_tipo1_idx", columns={"tiposel_id"}), @ORM\Index(name="fk_selecao_edicao_selecao_tipo_selecao_edicao1_idx", columns={"edicao_id"})})
 * @ORM\Entity
 */
class SelecaoTipoEdicao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="seletipoedicao_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $seletipoedicaoId;

    /**
     * @var SelecaoEdicao
     *
     * @ORM\ManyToOne(targetEntity="SelecaoEdicao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="edicao_id", referencedColumnName="edicao_id")
     * })
     */
    private $edicao;

    /**
     * @var SelecaoTipo
     *
     * @ORM\ManyToOne(targetEntity="SelecaoTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tiposel_id", referencedColumnName="tiposel_id")
     * })
     */
    private $tiposel;

    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getSeletipoedicaoId()
    {
        return $this->seletipoedicaoId;
    }

    /**
     * @param int $seletipoedicaoId
     */
    public function setSeletipoedicaoId($seletipoedicaoId)
    {
        $this->seletipoedicaoId = $seletipoedicaoId;
    }

    /**
     * @return SelecaoEdicao
     */
    public function getEdicao()
    {
        return $this->edicao;
    }

    /**
     * @param SelecaoEdicao $edicao
     */
    public function setEdicao($edicao)
    {
        $this->edicao = $edicao;
    }

    /**
     * @return SelecaoTipo
     */
    public function getTiposel()
    {
        return $this->tiposel;
    }

    /**
     * @param SelecaoTipo $tiposel
     */
    public function setTiposel($tiposel)
    {
        $this->tiposel = $tiposel;
    }

    public function toArray()
    {
        return array_merge(
            $this->getEdicao() ? $this->getEdicao()->toArray() : array(),
            $this->getTiposel() ? $this->getTiposel()->toArray() : array(),
            array(
                'seletipoedicaoId' => $this->getSeletipoedicaoId(),
                'edicao'           => $this->getEdicao() ? $this->getEdicao()->getEdicaoId() : null,
                'tiposel'          => $this->getTiposel() ? $this->getTiposel()->getTiposelId() : null,
            )
        );
    }
}
