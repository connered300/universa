<?php

namespace Vestibular\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * SelecaoInscricao
 *
 * @ORM\Table(name="selecao_inscricao", uniqueConstraints={@ORM\UniqueConstraint(name="unq_edicao_pes_campus", columns={"edicao_id", "pes_id"}), @ORM\UniqueConstraint(name="index7", columns={"edicao_id", "pes_id"})}, indexes={@ORM\Index(name="fk_selecao_inscricao_infra_sala1_idx", columns={"sala_id"}), @ORM\Index(name="fk_selecao_inscricao_pessoa_idx", columns={"pes_id"}), @ORM\Index(name="fk_selecao_inscricao_selecao_tipo_edicao1_idx", columns={"seletipoedicao_id"}), @ORM\Index(name="fk_selecao_inscricao_selecao_edicao1_idx", columns={"edicao_id"}), @ORM\Index(name="fk_selecao_inscricao_selecao_locais1_idx", columns={"selelocais_id"})})
 * @ORM\Entity
 * @LG\LG(id="inscricao_id",label="inscricao_id")
 * @Jarvis\Jarvis(title="Lista de candidatos ao Vestibular",icon="fa fa-table")
 * @LG\Querys\OrderBy(params={"edicao_id"="DESC"})
 * @LG\Querys\GroupBy()
 */
class SelecaoInscricao
{

    /**
     * @var integer
     *
     * @ORM\Column(name="inscricao_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="inscricao_id")
     * @LG\Labels\Attributes(text="Indice",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $inscricaoId;

    /**
     * @var SelecaoEdicao
     *
     * @ORM\ManyToOne(targetEntity="SelecaoEdicao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="edicao_id", referencedColumnName="edicao_id")
     * })
     * @LG\Labels\Property(name="edicao_id")
     * @LG\Labels\Attributes(text="Edicao do Vestibular",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE")
     * @LG\Querys\Joins(table="selecao_edicao",joinType="inner",joinCampDest="edicao_id",joinCampDestLabel="edicao_descricao")
     */
    private $edicao;

    /**
     * @var \Pessoa\Entity\Pessoa
     *
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     * @LG\Labels\Property(name="pes_id")
     * @LG\Labels\Attributes(text="Nome do Candidato",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE")
     * @LG\Querys\Joins(table="pessoa",joinType="inner",joinCampDest="pes_id",joinCampDestLabel="pes_nome")
     */
    private $pes;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="inscricao_data", type="datetime", nullable=false)
     */
    private $inscricaoData;

    /**
     * @var string
     *
     * @ORM\Column(name="inscricao_status", type="string", nullable=false)
     * @LG\Labels\Property(name="inscricao_status")
     * @LG\Labels\Attributes(text="Status do cadastro",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $inscricaoStatus = \Vestibular\Service\SelecaoInscricao::INSCRICAO_STATUS_INCOMPLETA;

    /**
     * @var string
     *
     * @ORM\Column(name="inscricao_gabarito", type="string", length=255, nullable=true)
     */
    private $inscricaoGabarito;

    /**
     * @var string
     *
     * @ORM\Column(name="inscricao_lingua_estrangeira", type="string", length=45, nullable=true)
     ** @LG\Labels\Property(name="inscricao_lingua_estrangeira")
     * @LG\Labels\Attributes(text="Lingua Estrangeira",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $inscricaoLinguaEstrangeira;

    /**
     * @var float
     *
     * @ORM\Column(name="selecao_inscricao_acertos", type="float", precision=10, scale=0, nullable=true)
     */
    private $selecaoInscricaoAcertos;

    /**
     * @var float
     *
     * @ORM\Column(name="inscricao_nota_enem_cienc_humanas", type="float", precision=10, scale=0, nullable=true)
     */
    private $inscricaoNotaEnemCiencHumanas;

    /**
     * @var float
     *
     * @ORM\Column(name="inscricao_nota_enem_cienc_natureza", type="float", precision=10, scale=0, nullable=true)
     */
    private $inscricaoNotaEnemCiencNatureza;

    /**
     * @var float
     *
     * @ORM\Column(name="inscricao_nota_enem_portugues", type="float", precision=10, scale=0, nullable=true)
     */
    private $inscricaoNotaEnemPortugues;

    /**
     * @var float
     *
     * @ORM\Column(name="inscricao_nota_enem_redacao", type="float", precision=10, scale=0, nullable=true)
     */
    private $inscricaoNotaEnemRedacao;

    /**
     * @var float
     *
     * @ORM\Column(name="inscricao_nota_enem_matematica", type="float", precision=10, scale=0, nullable=true)
     */
    private $inscricaoNotaEnemMatematica;

    /**
     * @var string
     *
     * @ORM\Column(name="inscricao_ultima_formacao", type="string", length=45, nullable=true)
     */
    private $inscricaoUltimaFormacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="inscricao_data_formacao", type="datetime", nullable=true)
     */
    private $inscricaoDataFormacao;

    /**
     * @var string
     *
     * @ORM\Column(name="inscricao_cidade_formacao", type="string", length=45, nullable=true)
     */
    private $inscricaoCidadeFormacao;

    /**
     * @var string
     *
     * @ORM\Column(name="inscricao_formacao_instituicao", type="string", length=45, nullable=true)
     */
    private $inscricaoFormacaoInstituicao;

    /**
     * @var string
     *
     * @ORM\Column(name="inscricao_entrega_doc", type="string", nullable=true)
     */
    private $inscricaoEntregaDoc = \Vestibular\Service\SelecaoInscricao::INSCRICAO_ENTREGA_DOC_NAO;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="inscricao_data_horario_prova", type="datetime", nullable=true)
     */
    private $inscricaoDataHorarioProva;

    /**
     * @var string
     *
     * @ORM\Column(name="mae_cpf", type="string", length=45, nullable=true)
     */
    private $maeCpf;

    /**
     * @var string
     *
     * @ORM\Column(name="mae_nome", type="string", length=45, nullable=true)
     */
    private $maeNome;

    /**
     * @var string
     *
     * @ORM\Column(name="pai_cpf", type="string", length=45, nullable=true)
     */
    private $paiCpf;

    /**
     * @var string
     *
     * @ORM\Column(name="pai_nome", type="string", length=45, nullable=true)
     */
    private $paiNome;

    /**
     * @var string
     *
     * @ORM\Column(name="inscricao_ip", type="string", length=45, nullable=true)
     */
    private $inscricaoIp;

    /**
     * @var \Infraestrutura\Entity\InfraSala
     *
     * @ORM\ManyToOne(targetEntity="Infraestrutura\Entity\InfraSala")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sala_id", referencedColumnName="sala_id")
     * })
     */
    private $sala;

    /**
     * @var SelecaoTipoEdicao
     *
     * @ORM\ManyToOne(targetEntity="SelecaoTipoEdicao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="seletipoedicao_id", referencedColumnName="seletipoedicao_id")
     * })
     */
    private $seletipoedicao;

    /**
     * @var SelecaoLocais
     *
     * @ORM\ManyToOne(targetEntity="SelecaoLocais")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="selelocais_id", referencedColumnName="selelocais_id")
     * })
     */
    private $selelocais;

    /**
     * @var string
     *
     * @LG\Labels\Property(name="financeiro_titulo")
     * @LG\Labels\Attributes(text="Titulo financeiro",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $financeiroTitulo;

    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return integer
     */
    public function getInscricaoId()
    {
        return $this->inscricaoId;
    }

    /**
     * @param integer $inscricaoId
     * @return SelecaoInscricao
     */
    public function setInscricaoId($inscricaoId)
    {
        $this->inscricaoId = $inscricaoId;

        return $this;
    }

    /**
     * @return SelecaoEdicao
     */
    public function getEdicao()
    {
        return $this->edicao;
    }

    /**
     * @param SelecaoEdicao $edicao
     * @return SelecaoInscricao
     */
    public function setEdicao($edicao)
    {
        $this->edicao = $edicao;

        return $this;
    }

    /**
     * @return \Pessoa\Entity\Pessoa
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @param \Pessoa\Entity\Pessoa $pes
     * @return SelecaoInscricao
     */
    public function setPes($pes)
    {
        $this->pes = $pes;

        return $this;
    }

    /**
     * @return Datetime
     */
    public function getInscricaoData()
    {
        return $this->inscricaoData;
    }

    /**
     * @param Datetime $inscricaoData
     * @return SelecaoInscricao
     */
    public function setInscricaoData($inscricaoData)
    {
        if ($inscricaoData) {
            if (is_string($inscricaoData)) {
                if (!preg_match('/\d{4}-\d{2}-\d{2}/', $inscricaoData)) {
                    $inscricaoData = \VersaSpine\Service\AbstractService::formatDateAmericano($inscricaoData);
                }
            } else {
                $inscricaoData = $inscricaoData;
            }
        } else {
            $inscricaoData = null;
        }

        $this->inscricaoData = $inscricaoData;

        return $this;
    }

    /**
     * @return string
     */
    public function getInscricaoStatus()
    {
        return $this->inscricaoStatus;
    }

    /**
     * @param string $inscricaoStatus
     * @return SelecaoInscricao
     */
    public function setInscricaoStatus($inscricaoStatus)
    {
        $this->inscricaoStatus = $inscricaoStatus;

        return $this;
    }

    /**
     * @return string
     */
    public function getInscricaoResultado()
    {
        return $this->inscricaoResultado;
    }

    public function getInscricaoGabarito()
    {
        return $this->inscricaoGabarito;
    }

    /**
     * @param string $inscricaoGabarito
     * @return SelecaoInscricao
     */
    public function setInscricaoGabarito($inscricaoGabarito)
    {
        $this->inscricaoGabarito = $inscricaoGabarito;

        return $this;
    }

    /**
     * @return string
     */
    public function getInscricaoLinguaEstrangeira()
    {
        return $this->inscricaoLinguaEstrangeira;
    }

    /**
     * @param string $inscricaoLinguaEstrangeira
     * @return SelecaoInscricao
     */
    public function setInscricaoLinguaEstrangeira($inscricaoLinguaEstrangeira)
    {
        $this->inscricaoLinguaEstrangeira = $inscricaoLinguaEstrangeira;

        return $this;
    }

    /**
     * @return float
     */
    public function getSelecaoInscricaoAcertos()
    {
        return $this->selecaoInscricaoAcertos;
    }

    /**
     * @param float $selecaoInscricaoAcertos
     * @return SelecaoInscricao
     */
    public function setSelecaoInscricaoAcertos($selecaoInscricaoAcertos)
    {
        $this->selecaoInscricaoAcertos = $selecaoInscricaoAcertos;

        return $this;
    }

    /**
     * @return float
     */
    public function getInscricaoNotaEnemCiencHumanas()
    {
        return $this->inscricaoNotaEnemCiencHumanas;
    }

    /**
     * @param float $inscricaoNotaEnemCiencHumanas
     * @return SelecaoInscricao
     */
    public function setInscricaoNotaEnemCiencHumanas($inscricaoNotaEnemCiencHumanas)
    {
        $this->inscricaoNotaEnemCiencHumanas = $inscricaoNotaEnemCiencHumanas;

        return $this;
    }

    /**
     * @return float
     */
    public function getInscricaoNotaEnemCiencNatureza()
    {
        return $this->inscricaoNotaEnemCiencNatureza;
    }

    /**
     * @param float $inscricaoNotaEnemCiencNatureza
     * @return SelecaoInscricao
     */
    public function setInscricaoNotaEnemCiencNatureza($inscricaoNotaEnemCiencNatureza)
    {
        $this->inscricaoNotaEnemCiencNatureza = $inscricaoNotaEnemCiencNatureza;

        return $this;
    }

    /**
     * @return float
     */
    public function getInscricaoNotaEnemPortugues()
    {
        return $this->inscricaoNotaEnemPortugues;
    }

    /**
     * @param float $inscricaoNotaEnemPortugues
     * @return SelecaoInscricao
     */
    public function setInscricaoNotaEnemPortugues($inscricaoNotaEnemPortugues)
    {
        $this->inscricaoNotaEnemPortugues = $inscricaoNotaEnemPortugues;

        return $this;
    }

    /**
     * @return float
     */
    public function getInscricaoNotaEnemRedacao()
    {
        return $this->inscricaoNotaEnemRedacao;
    }

    /**
     * @param float $inscricaoNotaEnemRedacao
     * @return SelecaoInscricao
     */
    public function setInscricaoNotaEnemRedacao($inscricaoNotaEnemRedacao)
    {
        $this->inscricaoNotaEnemRedacao = $inscricaoNotaEnemRedacao;

        return $this;
    }

    /**
     * @return float
     */
    public function getInscricaoNotaEnemMatematica()
    {
        return $this->inscricaoNotaEnemMatematica;
    }

    /**
     * @param float $inscricaoNotaEnemMatematica
     * @return SelecaoInscricao
     */
    public function setInscricaoNotaEnemMatematica($inscricaoNotaEnemMatematica)
    {
        $this->inscricaoNotaEnemMatematica = $inscricaoNotaEnemMatematica;

        return $this;
    }

    /**
     * @return string
     */
    public function getInscricaoUltimaFormacao()
    {
        return $this->inscricaoUltimaFormacao;
    }

    /**
     * @param string $inscricaoUltimaFormacao
     * @return SelecaoInscricao
     */
    public function setInscricaoUltimaFormacao($inscricaoUltimaFormacao)
    {
        $this->inscricaoUltimaFormacao = $inscricaoUltimaFormacao;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getInscricaoDataFormacao()
    {
        return $this->inscricaoDataFormacao;
    }

    /**
     * @param \DateTime $inscricaoDataFormacao
     * @return SelecaoInscricao
     */
    public function setInscricaoDataFormacao($inscricaoDataFormacao)
    {
        if ($inscricaoDataFormacao) {
            if (is_string($inscricaoDataFormacao)) {
                if (!preg_match('/\d{4}-\d{2}-\d{2}/', $inscricaoDataFormacao)) {
                    $inscricaoDataFormacao = \VersaSpine\Service\AbstractService::formatDateAmericano(
                        $inscricaoDataFormacao
                    );
                }
            } else {
                $inscricaoDataFormacao = $inscricaoDataFormacao->format('Y-m-d');
            }
        } else {
            $inscricaoDataFormacao = null;
        }

        $this->inscricaoDataFormacao = $inscricaoDataFormacao;

        return $this;
    }

    /**
     * @return string
     */
    public function getInscricaoCidadeFormacao()
    {
        return $this->inscricaoCidadeFormacao;
    }

    /**
     * @param string $inscricaoCidadeFormacao
     * @return SelecaoInscricao
     */
    public function setInscricaoCidadeFormacao($inscricaoCidadeFormacao)
    {
        $this->inscricaoCidadeFormacao = $inscricaoCidadeFormacao;

        return $this;
    }

    /**
     * @return string
     */
    public function getInscricaoFormacaoInstituicao()
    {
        return $this->inscricaoFormacaoInstituicao;
    }

    /**
     * @param string $inscricaoFormacaoInstituicao
     * @return SelecaoInscricao
     */
    public function setInscricaoFormacaoInstituicao($inscricaoFormacaoInstituicao)
    {
        $this->inscricaoFormacaoInstituicao = $inscricaoFormacaoInstituicao;

        return $this;
    }

    /**
     * @return string
     */
    public function getInscricaoEntregaDoc()
    {
        return $this->inscricaoEntregaDoc;
    }

    /**
     * @param string $inscricaoEntregaDoc
     * @return SelecaoInscricao
     */
    public function setInscricaoEntregaDoc($inscricaoEntregaDoc)
    {
        $this->inscricaoEntregaDoc = $inscricaoEntregaDoc;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getInscricaoDataHorarioProva($formatar = false)
    {
        if ($formatar) {
            return $this->inscricaoDataHorarioProva ? $this->inscricaoDataHorarioProva->format("d/m/Y H:i:s") : '';
        }

        return $this->inscricaoDataHorarioProva;
    }

    /**
     * @param \DateTime $inscricaoDataHorarioProva
     * @return SelecaoInscricao
     */
    public function setInscricaoDataHorarioProva($inscricaoDataHorarioProva)
    {
        if ($inscricaoDataHorarioProva) {
            if (is_string($inscricaoDataHorarioProva)) {
                $inscricaoDataHorarioProva = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $inscricaoDataHorarioProva
                );
                $inscricaoDataHorarioProva = new \DateTime($inscricaoDataHorarioProva);
            }
        } else {
            $inscricaoDataHorarioProva = null;
        }

        $this->inscricaoDataHorarioProva = $inscricaoDataHorarioProva;

        return $this;
    }

    /**
     * @return string
     */
    public function getMaeCpf()
    {
        return $this->maeCpf;
    }

    /**
     * @param string $maeCpf
     * @return SelecaoInscricao
     */
    public function setMaeCpf($maeCpf)
    {
        $this->maeCpf = $maeCpf;

        return $this;
    }

    /**
     * @return string
     */
    public function getMaeNome()
    {
        return $this->maeNome;
    }

    /**
     * @param string $maeNome
     * @return SelecaoInscricao
     */
    public function setMaeNome($maeNome)
    {
        $this->maeNome = $maeNome;

        return $this;
    }

    /**
     * @return string
     */
    public function getPaiCpf()
    {
        return $this->paiCpf;
    }

    /**
     * @param string $paiCpf
     * @return SelecaoInscricao
     */
    public function setPaiCpf($paiCpf)
    {
        $this->paiCpf = $paiCpf;

        return $this;
    }

    /**
     * @return string
     */
    public function getPaiNome()
    {
        return $this->paiNome;
    }

    /**
     * @param string $paiNome
     * @return SelecaoInscricao
     */
    public function setPaiNome($paiNome)
    {
        $this->paiNome = $paiNome;

        return $this;
    }

    /**
     * @return string
     */
    public function getInscricaoIp()
    {
        return $this->inscricaoIp;
    }

    /**
     * @param string $inscricaoIp
     * @return SelecaoInscricao
     */
    public function setInscricaoIp($inscricaoIp)
    {
        $this->inscricaoIp = $inscricaoIp;

        return $this;
    }

    /**
     * @return \Infraestrutura\Entity\InfraSala
     */
    public function getSala()
    {
        return $this->sala;
    }

    /**
     * @param \Infraestrutura\Entity\InfraSala $sala
     * @return SelecaoInscricao
     */
    public function setSala($sala)
    {
        $this->sala = $sala;

        return $this;
    }

    /**
     * @return SelecaoTipoEdicao
     */
    public function getSeletipoedicao()
    {
        return $this->seletipoedicao;
    }

    /**
     * @param SelecaoTipoEdicao $seletipoedicao
     * @return SelecaoInscricao
     */
    public function setSeletipoedicao($seletipoedicao)
    {
        $this->seletipoedicao = $seletipoedicao;

        return $this;
    }

    /**
     * @return SelecaoLocais
     */
    public function getSelelocais()
    {
        return $this->selelocais;
    }

    /**
     * @param SelecaoLocais $selelocais
     * @return SelecaoInscricao
     */
    public function setSelelocais($selelocais)
    {
        $this->selelocais = $selelocais;

        return $this;
    }

    /**
     * @return string
     */
    public function getFinanceiroTitulo()
    {
        return $this->financeiroTitulo;
    }

    /**
     * @param string $financeiroTitulo
     * @return SelecaoInscricao
     */
    public function setFinanceiroTitulo($financeiroTitulo)
    {
        $this->financeiroTitulo = $financeiroTitulo;

        return $this;
    }

    public function toArray()
    {
        $inscricaoDataHorarioProva = '';
        $seletipoedicao            = null;
        $sala                      = null;
        $selelocais                = null;

        if ($this->getInscricaoDataHorarioProva()) {
            $inscricaoDataHorarioProva = $this->getInscricaoDataHorarioProva()->format('d/m/Y H:i');
        }

        if ($this->getSeletipoedicao()) {
            $seletipoedicao = $this->getSeletipoedicao();
        }

        if ($this->getSala()) {
            $sala = $this->getSala();
        }

        if ($this->getSelelocais()) {
            $selelocais = $this->getSelelocais();
        }

        $retorno = array(
            'inscricaoId'                    => $this->getInscricaoId(),
            'edicao'                         => $this->getEdicao()->getEdicaoId(),
            'pes'                            => $this->getPes()->getPesId(),
            'seletipoedicao'                 => $seletipoedicao,
            'sala'                           => $sala,
            'inscricaoData'                  => $this->getInscricaoData(),
            'inscricaoStatus'                => $this->getInscricaoStatus(),
            'inscricaoGabarito'              => $this->getInscricaoGabarito(),
            'inscricaoLinguaEstrangeira'     => $this->getInscricaoLinguaEstrangeira(),
            'inscricaoNotaEnemCiencHumanas'  => $this->getInscricaoNotaEnemCiencHumanas(),
            'inscricaoNotaEnemCiencNatureza' => $this->getInscricaoNotaEnemCiencNatureza(),
            'inscricaoNotaEnemPortugues'     => $this->getInscricaoNotaEnemPortugues(),
            'inscricaoNotaEnemRedacao'       => $this->getInscricaoNotaEnemRedacao(),
            'inscricaoNotaEnemMatematica'    => $this->getInscricaoNotaEnemMatematica(),
            'inscricaoUltimaFormacao'        => $this->getInscricaoUltimaFormacao(),
            'inscricaoDataFormacao'          => $this->getInscricaoDataFormacao(),
            'inscricaoCidadeFormacao'        => $this->getInscricaoCidadeFormacao(),
            'inscricaoFormacaoInstituicao'   => $this->getInscricaoFormacaoInstituicao(),
            'inscricaoEntregaDoc'            => $this->getInscricaoEntregaDoc(),
            'inscricaoDataHorarioProva'      => $inscricaoDataHorarioProva,
            'selelocais'                     => $selelocais->getSelelocaisId(),
            'maeCpf'                         => $this->getMaeCpf(),
            'maeNome'                        => $this->getMaeNome(),
            'paiCpf'                         => $this->getPaiCpf(),
            'paiNome'                        => $this->getPaiNome(),
            'inscricaoIp'                    => $this->getInscricaoIp(),
        );

        if ($retorno['edicao']) {
            $arrEdicao = $this->getEdicao()->toArray();
        } else {
            $arrEdicao = array();
        }

        if ($retorno['pes']) {
            $arrPes = $this->getPes()->toArray();
        } else {
            $arrPes = array();
        }

        if ($retorno['seletipoedicao']) {
            $arrSeletipoedicao = $this->getSeletipoedicao()->toArray();
        } else {
            $arrSeletipoedicao = array();
        }

        if ($retorno['sala']) {
            $arrSala = $this->getSala()->toArray();
        } else {
            $arrSala = array();
        }

        if ($retorno['selelocais']) {
            try {
                $arrSelelocais = $this->getSelelocais()->toArray();
            } catch (\Exception $ex) {
                $arrSelelocais = array();
            }
        } else {
            $arrSelelocais = array();
        }

        return array_merge(
            $arrEdicao,
            $arrPes,
            $arrSeletipoedicao,
            $arrSala,
            $arrSelelocais,
            $retorno
        );
    }
}