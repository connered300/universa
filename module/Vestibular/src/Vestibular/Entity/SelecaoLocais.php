<?php

namespace Vestibular\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SelecaoLocais
 *
 * @ORM\Table(name="selecao_locais", indexes={@ORM\Index(name="fk_selecao_locais_selecao_edicao1_idx", columns={"edicao_id"}), @ORM\Index(name="fk_selecao_locais_infra_predio1_idx", columns={"predio_id"})})
 * @ORM\Entity
 */
class SelecaoLocais
{
    /**
     * @var integer
     *
     * @ORM\Column(name="selelocais_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $selelocaisId;

    /**
     * @var \Infraestrutura\Entity\InfraPredio
     *
     * @ORM\ManyToOne(targetEntity="Infraestrutura\Entity\InfraPredio")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="predio_id", referencedColumnName="predio_id")
     * })
     */
    private $predio;

    /**
     * @var SelecaoEdicao
     *
     * @ORM\ManyToOne(targetEntity="SelecaoEdicao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="edicao_id", referencedColumnName="edicao_id")
     * })
     */
    private $edicao;

    /**
     * @var \Organizacao\Entity\OrgUnidadeEstudo
     *
     * @ORM\ManyToOne(targetEntity="Organizacao\Entity\OrgUnidadeEstudo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="unidade_id", referencedColumnName="unidade_id")
     * })
     */
    private $unidadeEstudo;


    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getSelelocaisId()
    {
        return $this->selelocaisId;
    }

    /**
     * @param int $selelocaisId
     */
    public function setSelelocaisId($selelocaisId)
    {
        $this->selelocaisId = $selelocaisId;
    }

    /**
     * @return \Infraestrutura\Entity\InfraPredio
     */
    public function getPredio()
    {
        return $this->predio;
    }

    /**
     * @param \Infraestrutura\Entity\InfraPredio $predio
     */
    public function setPredio($predio)
    {
        $this->predio = $predio;
    }

    /**
     * @return SelecaoEdicao
     */
    public function getEdicao()
    {
        return $this->edicao;
    }

    /**
     * @param SelecaoEdicao $edicao
     */
    public function setEdicao($edicao)
    {
        $this->edicao = $edicao;
    }

    /**
     * @return \Organizacao\Entity\OrgUnidadeEstudo
     */
    public function getUnidadeEstudo()
    {
        return $this->unidadeEstudo;
    }

    /**
     * @param \Organizacao\Entity\OrgUnidadeEstudo $unidade
     */
    public function setUnidadeEstudo($unidade)
    {
        $this->unidadeEstudo = $unidade;
    }

    public function toArray()
    {
        $retorno = [
            'selelocaisId' => $this->getSelelocaisId(),
            'edicao'       => $this->getEdicao() ? $this->getEdicao()->getEdicaoId() : null,
            'predio'       => $this->getPredio() ? $this->getPredio()->getPredioId() : null,
        ];

        if ($this->getEdicao()) {
            $objEdicao = $this->getEdicao();
        } else {
            $objEdicao = new \Vestibular\Entity\SelecaoEdicao();
        }

        if ($this->getPredio()) {
            $objPredio = $this->getEdicao();
        } else {
            $objPredio = new \Infraestrutura\Entity\InfraPredio();
        }

        $retorno = array_merge($objEdicao->toArray(), $objPredio->toArray(), $retorno);

        return $retorno;
    }
}
