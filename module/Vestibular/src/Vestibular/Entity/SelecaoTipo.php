<?php

namespace Vestibular\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SelecaoTipo
 *
 * @ORM\Table(name="selecao_tipo")
 * @ORM\Entity(repositoryClass="Vestibular\Entity\Repository\SelecaoTipoRepository")
 */
class SelecaoTipo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="tiposel_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $tiposelId;

    /**
     * @var string
     *
     * @ORM\Column(name="tiposel_nome", type="string", length=45, nullable=false)
     */
    private $tiposelNome;

    /**
     * @var string
     *
     * @ORM\Column(name="tiposel_prova", type="string", nullable=false)
     */
    private $tiposelProva = 'Sim';

    public function __construct(array $data)
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getTiposelId()
    {
        return $this->tiposelId;
    }

    /**
     * @param int $tiposelId
     */
    public function setTiposelId($tiposelId)
    {
        $this->tiposelId = $tiposelId;
    }

    /**
     * @return string
     */
    public function getTiposelNome()
    {
        return $this->tiposelNome;
    }

    /**
     * @param string $tiposelNome
     */
    public function setTiposelNome($tiposelNome)
    {
        $this->tiposelNome = $tiposelNome;
    }

    /**
     * @return string
     */
    public function getTiposelProva()
    {
        return $this->tiposelProva;
    }

    /**
     * @param string $tiposelProva
     */
    public function setTiposelProva($tiposelProva)
    {
        $this->tiposelProva = $tiposelProva;
    }

    public function toArray()
    {
        return array(
            'tiposelId'    => $this->getTiposelId(),
            'tiposelNome'  => $this->getTiposelNome(),
            'tiposelProva' => $this->getTiposelProva(),
        );
    }
}
