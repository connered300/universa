<?php

namespace Vestibular\Entity\Repository;

use Doctrine\ORM\EntityRepository;
/**
 * Description of SelecaoTipoRepository
 *
 * @author versa
 */
class SelecaoTipoRepository extends EntityRepository{
    //put your code here

    public function listaTipos(){
        $dados = $this->findAll();

        $array = array();
        foreach ($dados as $tipo) {
            $array[$tipo->getTiposelId()] = $tipo->getTiposelNome();
        }
        return $array;
    }
}
