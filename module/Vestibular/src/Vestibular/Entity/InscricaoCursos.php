<?php

namespace Vestibular\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InscricaoCursos
 *
 * @ORM\Table(name="inscricao_cursos", indexes={@ORM\Index(name="fk_inscricao_cursos_selecao_inscricao1_idx", columns={"inscricao_id"}), @ORM\Index(name="fk_inscricao_cursos_selecao_cursos1_idx", columns={"selcursos_id"})})
 * @ORM\Entity
 */
class InscricaoCursos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="incricao_cursos_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $incricaoCursosId;

    /**
     * @var integer
     *
     * @ORM\Column(name="inscricao_curso_opcao", type="integer", nullable=true)
     */
    private $inscricaoCursoOpcao;

    /**
     * @var SelecaoCursos
     *
     * @ORM\ManyToOne(targetEntity="SelecaoCursos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="selcursos_id", referencedColumnName="selcursos_id")
     * })
     */
    private $selcursos;

    /**
     * @var SelecaoInscricao
     *
     * @ORM\ManyToOne(targetEntity="SelecaoInscricao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="inscricao_id", referencedColumnName="inscricao_id")
     * })
     */
    private $inscricao;

    /**
     * @var string
     *
     * @ORM\Column(name="inscricao_turno", type="string", length=45, nullable=false)
     */
    private $inscricaoTurno;

    /**
     * @var string
     *
     * @ORM\Column(name="inscricao_resultado", type="string", nullable=true)
     *
     */
    private $inscricaoResultado;

    /**
     * @var integer
     *
     * @ORM\Column(name="inscricao_classificacao", type="integer", length=10, nullable=true)
     */
    private $inscricaoClassificacao;
    /**
     * @var float
     *
     * @ORM\Column(name="inscricao_nota", type="float")
     */
    private $inscricaoNota;

    /**
     * @var integer
     *
     * @ORM\Column(name="inscricao_opcao", type="integer", length=10, nullable=true)
     */
    private $inscricaoOpcao;

    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    function getInscricaoTurno()
    {
        return $this->inscricaoTurno;
    }

    function setInscricaoTurno($inscricaoTurno)
    {
        $this->inscricaoTurno = $inscricaoTurno;
    }

    function getIncricaoCursosId()
    {
        return $this->incricaoCursosId;
    }

    function getInscricaoCursoOpcao()
    {
        return $this->inscricaoCursoOpcao;
    }

    function getSelcursos()
    {
        return $this->selcursos;
    }

    function getInscricao()
    {
        return $this->inscricao;
    }

    function setIncricaoCursosId($incricaoCursosId)
    {
        $this->incricaoCursosId = $incricaoCursosId;
    }

    function setInscricaoCursoOpcao($inscricaoCursoOpcao)
    {
        $this->inscricaoCursoOpcao = $inscricaoCursoOpcao;
    }

    function setSelcursos($selcursos)
    {
        $this->selcursos = $selcursos;
    }

    function setInscricao($inscricao)
    {
        $this->inscricao = $inscricao;
    }

    /**
     * @return string
     */
    public function getInscricaoResultado()
    {
        return $this->inscricaoResultado;
    }

    /**
     * @param string $inscricaoResultado
     * @return InscricaoCursos
     */
    public function setInscricaoResultado($inscricaoResultado)
    {
        $this->inscricaoResultado = $inscricaoResultado;

        return $this;
    }

    /**
     * @return string
     */

    public function getInscricaoClassificacao()
    {
        return $this->inscricaoClassificacao;
    }

    /**
     * @param string $inscricaoClassificacao
     * @return InscricaoCursos
     */
    public function setInscricaoClassificacao($inscricaoClassificacao)
    {
        $this->inscricaoClassificacao = $inscricaoClassificacao;

        return $this;
    }

    /**
     * @return float
     */
    public function getInscricaoNota()
    {
        return $this->inscricaoNota;
    }

    /**
     * @param float $inscricaoNota
     * @return InscricaoCursos
     */
    public function setInscricaoNota($inscricaoNota)
    {
        $this->inscricaoNota = $inscricaoNota;

        return $this;
    }

    /**
     * @return int
     */
    public function getInscricaoOpcao()
    {
        return $this->inscricaoOpcao;
    }

    /**
     * @param int $inscricaoOpcao
     * @return InscricaoCursos
     */
    public function setInscricaoOpcao($inscricaoOpcao)
    {
        $this->inscricaoOpcao = $inscricaoOpcao;

        return $this;
    }

    public function toArray()
    {
        $arrData = [
            'incricaoCursosId'          => $this->getIncricaoCursosId(),
            'inscricaoCursoOpcao'       => $this->getInscricaoCursoOpcao(),
            'selcursos'                 => $this->getSelcursos(),
            'inscricao'                 => $this->getInscricao(),
            'inscricaoId'               => '',
            'edicaoId'                  => '',
            'edicaoDescricao'           => '',
            'inscricaoDataHorarioProva' => '',
            'pesNome'                   => '',
            'campNome'                  => '',
            'cursoNome'                 => '',
            'inscricaoStatus'           => '',
            'inscricaoTurno'            => $this->getInscricaoTurno(),
            'inscricaoResultado'        => $this->getInscricaoResultado(),
            'inscricaoClassificacao'    => $this->getInscricaoClassificacao(),
            'inscricaoNota'             => $this->getInscricaoNota(),
            'inscricaoOpcao'            => $this->getInscricaoOpcao(),
        ];

        if ($this->getSelcursos()) {
            $arrData['campNome']  = $this->getSelcursos()->getCursocampus()->getCamp()->getCampNome();
            $arrData['cursoNome'] = $this->getSelcursos()->getCursocampus()->getCurso()->getCursoNome();
        }

        if ($this->getInscricao()) {
            $arrData['inscricaoId']                 = $this->getInscricao()->getInscricaoId();
            $arrData['edicaoDescricao']             = $this->getInscricao()->getEdicao()->getEdicaoDescricao();
            $arrData['inscricaoDataHorarioProva']   = $this->getInscricao()->getInscricaoDataHorarioProva(true);
            $arrData['inscricaoDataHorarioProvaUS'] = $this->getInscricao()->getInscricaoDataHorarioProva();
            $arrData['pesNome']                     = $this->getInscricao()->getPes()->getPesNome();
            $arrData['inscricaoStatus']             = $this->getInscricao()->getInscricaoStatus();

            if ($arrData['inscricaoDataHorarioProvaUS']) {
                $arrData['inscricaoDataHorarioProvaUS'] = (
                $arrData['inscricaoDataHorarioProvaUS']->format("Y-m-d h:i:s")
                );
            }
        }

        return $arrData;
    }
}