<?php

namespace Vestibular\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SelecaoDataRealizacao
 *
 * @ORM\Table(name="selecao_data_realizacao", indexes={@ORM\Index(name="fk_selecao_data_realizacao_selecao_edicao1_idx", columns={"edicao_id"})})
 * @ORM\Entity
 */
class SelecaoDataRealizacao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="realizacao_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $realizacaoId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="realizacao_data", type="datetime", nullable=false)
     */
    private $realizacaoData;

    /**
     * @var SelecaoEdicao
     *
     * @ORM\ManyToOne(targetEntity="SelecaoEdicao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="edicao_id", referencedColumnName="edicao_id")
     * })
     */
    private $edicao;

    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getRealizacaoId()
    {
        return $this->realizacaoId;
    }

    /**
     * @param int $realizacaoId
     */
    public function setRealizacaoId($realizacaoId)
    {
        $this->realizacaoId = $realizacaoId;
    }

    /**
     * @return \DateTime
     */
    public function getRealizacaoData()
    {
        return $this->realizacaoData;
    }

    /**
     * @param \DateTime $realizacaoData
     */
    public function setRealizacaoData($realizacaoData)
    {
        $this->realizacaoData = $realizacaoData;
    }

    /**
     * @return SelecaoEdicao
     */
    public function getEdicao()
    {
        return $this->edicao;
    }

    /**
     * @param SelecaoEdicao $edicao
     */
    public function setEdicao($edicao)
    {
        $this->edicao = $edicao;
    }

    public function toArray()
    {
        return array(
            'edicao'         => $this->getEdicao()->getEdicaoId(),
            'realizacaoId'   => $this->getRealizacaoId(),
            'realizacaoData' => $this->getRealizacaoData(),
        );
    }
}
