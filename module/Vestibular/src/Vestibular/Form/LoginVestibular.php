<?php

namespace Vestibular\Form;

use Zend\Form\Form;
use Zend\Form\Element;

/**
 * Description of LoginVestibular
 *
 * @author versa
 */
class LoginVestibular extends Form{
    public function __construct() {
         parent::__construct("loginVestibular");
          $this->setAttribute("data-validate", "yes");

        $this->setAttribute("class", "smart-form");
        $this->setAttribute("data-validate", "yes");
        $this->setAttribute("action","");
	$this->setAttribute("target","_blank");
		        
        $cpf = new Element\Text('username');
        $cpf->setAttributes(array(
            'col' => 4,
            'label' => 'Usuário:',
            'data-validations' => 'Required',
//            'data-masked' => 'Cpf',
            'wrap' => true,
            'icon' => "icon-append fa fa-user",
            'placeholder'=> 'Entre com seu usuario'

        ));
        $this->add($cpf);
        
        $password = new Element\Password('password');
        $password->setAttributes(array(
            'col' => 4,
            'label' => 'Senha:',
            'icon'=>'icon-append fa fa-lock',
            'wrap' => true,
            'placeholder'=>'Entre com a Senha de acesso'
        ));
        $this->add($password);
        
        
        
        $entrar = new Element\Submit('entrar');
        $entrar->setValue('Entrar');
        $entrar->setAttribute('class', 'pull-left btn btn-primary');      
        
        $this->add($entrar);
 }
}
