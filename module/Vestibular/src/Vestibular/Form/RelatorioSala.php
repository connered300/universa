<?php


namespace Vestibular\Form;

use Zend\Form\Form,
    Zend\Form\Element;
use Zend\Form\View\Helper\FormMultiCheckbox;

class RelatorioSala extends Form{
    public function __construct($salas)
    {
        parent::__construct('relatorioSalas');

        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', '');
        $this->setAttribute('role', 'form');
        $this->setAttribute('target', '_blank');

        $salaForm = new Element\Select('sala');
        $salaForm->setAttributes([
            'label'            => 'Selecione a sala',
            'col'              => 4,
            'data-validations' => 'Required'
        ]);
        if($salas){
            $salaForm->setValueOptions($salas);
        }
        $salaForm->setValue('sala');
        $this->add( $salaForm );

        $tipo = new Element\Radio('tipo');
        $tipo->setAttributes([
            'label'            => 'Tipo do relatório',
            'col'              => 4,
            'data-validations' => 'Required'
        ]);
        $tipo->setValueOptions([
            'vestibular/formularios/lista-presenca' => 'Lista de Presença',
            'vestibular/formularios/lista-alunos'   => 'Lista de Candidatos',
            'vestibular/formularios/etiqueta'       => 'Etiquetas'
        ]);
        $tipo->setValue('presenca');
        $this->add($tipo);

        $print = new Element\Submit('Imprimir');
        $print->setAttributes([
            'class' => 'btn btn-primary pull-left',
            'value' => 'Imprimir'
        ]);
        $this->add($print);
    }

}