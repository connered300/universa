<?php

namespace Vestibular\Form;

use Zend\Form\Element;
use Zend\Form\Form;

/**
 * Description of SelecaoInscricao
 *
 * @author Edvaldo
 */
class SelecaoInscricao extends Form {
    public function __construct() {
        parent::__construct("selecaoInscricao");

        $this->setAttribute("class", "smart-form");
        $this->setAttribute("data-validate", "yes");

        $inscricaoId = new Element\Hidden('inscricaoId');
        $this->add($inscricaoId);

        $sala = new Element\Hidden('sala');
        $this->add($sala);

        $pes = new Element\Hidden('pes');
        $this->add($pes);

        $edicao = new Element\Hidden('edicao');
        $this->add($edicao);

        $seleTipoEdicao = new Element\Hidden('seletipoedicao');
        $this->add($seleTipoEdicao);


        $pesCpf = new Element\Text('pesCpf');
        $pesCpf->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Informe o CPF',
            'label' => 'CPF Candidato:',
            'data-validations' => 'Required,Cpf',
            'data-masked' => 'Cpf',
            'wrap' => true,
            'icon'=>'icon-prepend fa fa-credit-card'
        ));
        $this->add($pesCpf);

        $pesNome = new Element\Text('pesNome');
        $pesNome->setAttributes(array(
            'col' => 4,
            'maxlength' => 255,
            'placeholder' => 'Informe o Nome Completo  ',
            'label' => 'Nome do Candidato:',
            'wrap' => true,
            'data-validations' => 'Required,String',
            'icon'=>'icon-prepend fa fa-user'

        ));
        $this->add($pesNome);

        $maeCpf = new Element\Text('maeCpf');
        $maeCpf->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Informe o CPF da Mãe',
            'label' => 'CPF da Mãe:',
            'data-validations' => 'Cpf',
            'data-masked' => 'Cpf',
            'wrap' => true,
            'icon'=>'icon-prepend fa fa-credit-card'
        ));
        $this->add($maeCpf);

        $maeNome = new Element\Text('maeNome');
        $maeNome->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Informe o Nome Completo da Mãe ',
            'label' => 'Nome da Mãe:',
            'wrap' => true,
            'data-validations' => 'String',
            'icon'=>'icon-prepend fa fa-user'

        ));
        $this->add($maeNome);


        $paiCpf = new Element\Text('paiCpf');
        $paiCpf->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Informe o CPF do Pai',
            'label' => 'CPF do Pai:',
            'data-validations' => 'Cpf',
            'data-masked' => 'Cpf',
            'wrap' => true,
            'icon'=>'icon-prepend fa fa-credit-card'
        ));
        $this->add($paiCpf);

        $paiNome = new Element\Text('paiNome');
        $paiNome->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Informe o Nome Completo do Pai',
            'label' => 'Nome do Pai:',
            'wrap' => true,
            'data-validations' => 'String',
            'icon'=>'icon-prepend fa fa-user'

        ));
        $this->add($paiNome);



        $cont_tel = new Element\Text('contTel');
        $cont_tel->setAttributes(array(
           'col' => 4,
           'placeholder' => 'Telefone para contato',
           'label' => 'Telefone:',
           'wrap' => true,
           'data-validations' => 'Required',
           'data-masked' => 'Fone',
           'icon' =>'icon-prepend fa fa-fax'

        ));
        $this->add($cont_tel);

        $cont_email = new Element\Text('contEmail');
        $cont_email->setAttributes(array(
            'col' => 4,
            'placeholder' => 'E-mail para contato',
            'label' => 'E-mail:',
            'wrap' => true,
            'data-validations' => 'Required,String',
            'data-validations' => 'Required,Email',
            'icon' => 'icon-prepend fa fa-envelope-square'

      ));
        $this->add($cont_email);

        $inscricaoNotaEnemCiencHumanas = new Element\Text('inscricaoNotaEnemCiencHumanas');
        $inscricaoNotaEnemCiencHumanas->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Nota Ciẽncias Humanas',
            'label' => 'Ciencias Humanas:',
            'wrap' => true,
            'data-validations' => 'Required',

        ));
        $this->add($inscricaoNotaEnemCiencHumanas);

        $inscricaoNotaEnemCiencNatureza = new Element\Text('inscricaoNotaEnemCiencNatureza');
        $inscricaoNotaEnemCiencNatureza->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Nota Ciẽncias da Natureza',
            'label' => 'Ciencias da Natureza:',
            'wrap' => true,
            'data-validations' => 'Required',

        ));
        $this->add($inscricaoNotaEnemCiencNatureza);

        $inscricaoNotaEnemPortugues = new Element\Text('inscricaoNotaEnemPortugues');
        $inscricaoNotaEnemPortugues->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Nota Portugês',
            'label' => 'Portugês:',
            'wrap' => true,
            'data-validations' => 'Required',

        ));
        $this->add($inscricaoNotaEnemPortugues);

        $inscricaoNotaEnemRedacao = new Element\Text('inscricaoNotaEnemRedacao');
        $inscricaoNotaEnemRedacao->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Nota Redação',
            'label' => 'Redação:',
            'wrap' => true,
            'data-validations' => 'Required',

        ));
        $this->add($inscricaoNotaEnemRedacao);

        $inscricaoNotaEnemMatematica = new Element\Text('inscricaoNotaEnemMatematica');
        $inscricaoNotaEnemMatematica->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Nota Matemática',
            'label' => 'Matemática:',
            'wrap' => true,
            'data-validations' => 'Required',

        ));
        $this->add($inscricaoNotaEnemMatematica);

        $inscricaoUltimaFormacao = new Element\Text('inscricaoUltimaFormacao');
        $inscricaoUltimaFormacao->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Ultima formação',
            'label' => 'Ultima formação:',
            'wrap' => true,
            'data-validations' => 'Required',

        ));
        $this->add($inscricaoUltimaFormacao);

        $inscricaoDataFormacao = new Element\Text('inscricaoDataFormacao');
        $inscricaoDataFormacao->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Data de formação',
            'label' => 'Data:',
            'wrap' => true,
            'data-validations' => 'Required,Data',
            'data-masked' => 'Date'

        ));
        $this->add($inscricaoDataFormacao);

        $inscricaoCidadeFormacao = new Element\Text('inscricaoCidadeFormacao');
        $inscricaoCidadeFormacao->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Cidade de formação',
            'label' => 'Cidade:',
            'wrap' => true,
            'data-validations' => 'Required',

        ));
        $this->add($inscricaoCidadeFormacao);

        $inscricaoFormacaoInstituicao = new Element\Text('inscricaoFormacaoInstituicao');
        $inscricaoFormacaoInstituicao->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Instituição de formação',
            'label' => 'Instituição:',
            'wrap' => true,
            'data-validations' => 'Required',
        ));
        $this->add($inscricaoFormacaoInstituicao);

        $cadastrar = new Element\Submit('cadastrar');
        $cadastrar->setValue('Cadastrar-se');
        $cadastrar->setAttribute('class', 'pull-left btn btn-primary');
        $this->add($cadastrar);
    }
}
