<?php


namespace Vestibular\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class ImpressaoFicha extends Form{
    public function __construct($salas)
    {
        parent::__construct('impressaoFicha');
        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', '');
        $this->setAttribute('role', 'form');
        $this->setAttribute('target', '_blank');

        $tipo = new Element\Radio('tipo');
        $tipo->setAttributes([
            'label'            => 'Marque a opção que deseja',
            'col'              => 4,
            'data-validations' => 'Required'
        ]);
        $tipo->setValueOptions([
            'todos'  => 'Todos',
            'alguns' => 'Alguns'
        ]);
        $tipo->setValue('todos');
        $this->add($tipo);

        $inscricao = new Element\Text('inscricao[]');
        $inscricao->setAttributes([
            'label'            => 'Digite o Nº de inscrição',
            'placeholder'      => 'Nº de inscrição',
            'col'              => 5,
            'data-validations' => 'Required'
        ]);
        $this->add($inscricao);


        $tipoFicha = new Element\Select('tipoFicha');
        $tipoFicha->setAttributes([
            'label'            => 'Marque o tipo da ficha',
            'col'              => 4,
            'data-validations' => 'Required'
        ]);
        $tipoFicha->setValueOptions([
            'redacao'  => 'Redação',
            'provas' => 'Provas'
        ]);
        $tipoFicha->setValue('redacao');
        $this->add($tipoFicha);

        $salaForm = new Element\Select('sala');
        $salaForm->setAttributes([
            'label'            => 'Selecione a sala',
            'col'              => 4,
            'data-validations' => 'Required'
        ]);
        if($salas){
            $salaForm->setValueOptions($salas);
        }
        $this->add($salaForm);

        $print = new Element\Submit('Imprimir');
        $print->setAttributes([
            'class' => 'btn btn-primary pull-left',
            'value' => 'Imprimir'
        ]);
        $this->add($print);
    }
}