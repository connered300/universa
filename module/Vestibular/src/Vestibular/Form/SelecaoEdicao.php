<?php

namespace Vestibular\Form;

use Matricula\Service\AcadperiodoLetivo;
use Zend\Form\Form,
    Zend\Form\Element;
use Zend\Form\View\Helper\FormMultiCheckbox;

class SelecaoEdicao extends Form
{
    public function __construct($name = null, $options = array())
    {
        parent::__construct("cadastroVestibular");

        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->setAttribute('action', '');
        $this->setAttribute('role', 'form');

        $edicaoId = new Element\Hidden('edicaoId');

        $edicaoDescricao = new Element\Text('edicaoDescricao');
        $edicaoDescricao->setAttributes(
            [
                'col'   => 4,
                'label' => 'Descrição:'
            ]
        );
        $this->add($edicaoDescricao);

        $this->add($edicaoId);

        $edicao = new Element\Text('edicaoAno');
        $edicao->setAttributes(
            [
                'col'         => '4',
                'placeholder' => '0000',
                'wrap'        => true,
                'label'       => 'Ano do Vestibular:',
            ]
        );
        $this->add($edicao);

        $edicaoSemestre = new Element\Select('edicaoSemestre');
        $edicaoSemestre->setAttributes(
            [
                'col'      => '2',
                'label'    => 'Semestre',
                'required' => 'Required'
            ]
        );
        $edicaoSemestre->setValueOptions(
            [
                'Primeiro' => 'Primeiro',
                'Segundo'  => 'Segundo',
            ]
        );
        $this->add($edicaoSemestre);

        $edicaoPeriodo = new Element\Select('edicaoPeriodo');
        $edicaoPeriodo->setAttributes(
            [
                'col'      => '2',
                'label'    => 'Período',
                'required' => 'Required'
            ]
        );

        if ($options['periodo']) {
            $edicaoPeriodo->setValueOptions($options['periodo']);
        }

        $this->add($edicaoPeriodo);

        $edicaoInscricaoInicio = new Element\Text('edicaoInicioInscricao');
        $edicaoInscricaoInicio->setAttributes(
            [
                'required'    => true,
                'col'         => '4',
                'placeholder' => '00/00/0000',
                //'wrap' => true,
                'label'       => 'Início das Inscrições:',
                'data-masked' => 'Date',
            ]
        );
        $this->add($edicaoInscricaoInicio);

        $edicaoFimInscricao = new Element\Text('edicaoFimInscricao');
        $edicaoFimInscricao->setAttributes(
            [
                'required'    => true,
                'col'         => '4',
                'placeholder' => '00/00/0000',
                'wrap'        => true,
                'label'       => 'Término das Inscrições:',
                'data-masked' => 'Date',
            ]
        );
        $this->add($edicaoFimInscricao);

        $edicaoNumCursosOpcionais = new Element\Text('edicaoNumCursosOpcionais');
        $edicaoNumCursosOpcionais->setAttributes(
            [
                'col'         => '4',
                'placeholder' => '0',
                'wrap'        => true,
                'label'       => 'Número de opções adicionais de curso:',
                'data-masked' => 'Numeric',
            ]
        );
        $this->add($edicaoNumCursosOpcionais);

        $confcont = new Element\Select('confcont');
        $confcont->setAttributes(
            [
                'required' => true,
                'col'      => '4',
                'wrap'     => true,
                'label'    => 'Configuração Bancaria',
            ]
        );
        $this->add($confcont);

        $edicaoAgendado = new \VersaSpine\Form\SmartAdmin\Elements\AlternateOptions("edicaoAgendado");
        $edicaoAgendado->setAttributes(
            array(
                //            'required' => true,
                'col'         => 2,
                'label'       => 'Agendado',
                'toggle_type' => 'radio',
                'wrap'        => true,
                //            'data-validations' => 'Required'
            )
        );
        $edicaoAgendado->setValueOptions(
            array(
                'Agendado'   => array('on' => 'Sim', 'off' => 'Não', 'value' => 'Sim'),
                'Data Única' => array('on' => 'Sim', 'off' => 'Não', 'value' => 'Não'),
            )
        );
        $this->add($edicaoAgendado);

        $edicaoValor = new Element\Text('selcursosValorInscricao');
        $edicaoValor->setAttributes(
            [
                'col'         => '4',
                'placeholder' => 'Valor da Inscrição',
                'wrap'        => true,
                'label'       => 'Valor da Inscrição',
            ]
        );
        $this->add($edicaoValor);

        $dataRealizacao = new Element\Text('edicaoDataRealizacao[]');
        $dataRealizacao->setAttributes(
            [
                'required'    => true,
                'col'         => '2',
                'placeholder' => '00/00/0000',
                'wrap'        => true,
                'label'       => 'Data de Realização:',
                'data-masked' => 'Date',
            ]
        );
        $this->add($dataRealizacao);

        $dataDivulgacao = new Element\Text('edicaoDataDivulgacao');
        $dataDivulgacao->setAttributes(
            [
                'required'    => true,
                'col'         => '4',
                'placeholder' => '00/00/0000',
                'wrap'        => true,
                'label'       => 'Data de Divulgação:',
                'data-masked' => 'Date',
            ]
        );
        $this->add($dataDivulgacao);

        $edicaoEdital = new Element\File("edicaoEdital");
        $edicaoEdital->setAttributes(
            [
                'col'   => '4',
                'label' => 'Edital:',
                'wrap'  => true,
            ]
        );
        $this->add($edicaoEdital);

        $selecaoTipo = new Element\MultiCheckbox('tiposel');
        $selecaoTipo->setAttributes(
            [
                //            'required' => true,
                'label' => 'Modalidades de Ingresso:',
                'col'   => '3',
            ]
        );
        $selecaoTipo->setValueOptions($options['tipo']);
        $this->add($selecaoTipo);

        $camp = new Element\Text('camp');
        $camp->setAttributes(
            [
                'label'                 => 'Câmpus',
                'col'                   => '4',
                'wrap'                  => true,
                'data-autocomplete-url' => "/organizacao/org-campus/autocomplete-json",
            ]
        );
        $this->add($camp);

        $edicaoObservacao = new Element\Textarea('edicaoObservacao');
        $edicaoObservacao->setAttributes(
            [
                'col'   => 10,
                'label' => 'Observação da ficha de Inscrição:'
            ]
        );
        $this->add($edicaoObservacao);

        $cadastrar = new Element\Submit('Cadastrar');
        $cadastrar->setAttributes(
            [
                'class' => 'btn btn-primary',
                'value' => 'Salvar',

            ]
        );
        $this->add($cadastrar);
    }
}