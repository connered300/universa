<?php


namespace Vestibular\Form;
use Zend\Form\Element;
use Zend\Form\Form;

class CandidatoSala extends Form{
    public function __construct($salaSelect = null)
    {
        parent::__construct('candidatoSala');
        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', '');
        $this->setAttribute('role', 'form');

        $inscId = new Element\Text('inscricaoId');
        $inscId->setAttributes([
            'label'    => 'Nº de Inscrição',
            'col'      => '2',
            'disabled' => 'disabled'
        ]);
        $this->add($inscId);

        $insc = new Element\Hidden('inscricao');
        $this->add($insc);

        $nome = new Element\Text('pesNome');
        $nome->setAttributes([
            'label'    => 'Nome',
            'col'      => '6',
            'disabled' => 'disabled'
        ]);
        $this->add($nome);

        $sala = new Element\Select('sala');
        $sala->setAttributes([
            'label' => 'Sala',
            'col'   => '2',
        ]);
        if($salaSelect){
            $sala->setValueOptions($salaSelect);
        }
        $this->add($sala);
    }
}