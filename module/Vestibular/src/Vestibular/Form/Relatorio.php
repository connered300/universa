<?php


namespace Vestibular\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class Relatorio extends Form{
    public function __construct() {
        parent::__construct("relatorio");
        $this->setAttribute("data-validate", "yes");

        $this->setAttribute("class", "smart-form");
        $this->setAttribute("data-validate", "yes");
        $this->setAttribute("action","");
        $this->setAttribute("target","_blank");

        $inicio = new Element\Text('dataInicio');
        $inicio->setAttributes([
            'col' => '6',
            'label' => 'Data de Início:',
            'icon'=>'icon-calendar',
            'wrap' => true,
            'placeholder'=>'00/00/0000',
            'data-masked' => 'Date',
            'data-validations' => 'Data,Required'
        ]);
        $this->add($inicio);

        $inicio = new Element\Text('dataFim');
        $inicio->setAttributes([
            'col' => '6',
            'label' => 'Data de Termino:',
            'icon'=>'icon-calendar',
            'wrap' => true,
            'placeholder'=>'00/00/0000',
            'data-masked' => 'Date',
            'data-validations' => 'Data,Required'
        ]);
        $this->add($inicio);

        $cadastrar = new Element\Submit('gerar');
        $cadastrar->setAttributes([
            'class' => 'btn btn-primary',
            'value' => 'Gerar Relatorio',
        ]);
        $this->add($cadastrar);
    }
}