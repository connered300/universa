<?php


namespace Vestibular\Form;

use Zend\Form\Form,
    Zend\Form\Element;

class SelecaoTipo extends Form{
    public function __contruct($name = null, $options = null){
        parent::__construct($name);

        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', '');
        $this->setAttribute('role', 'form');

        $tiposelId = new Element\Hidden('tiposelId');
        $this->add($tiposelId);


        $tiposelNome = new Element\Text('tiposelNome');
        $tiposelNome->setAttributes([
            'col'      => '2',
            'wrap'     => false,
            'label'    => 'Ensino anterior: ',
            'required' => 'Required',
        ]);
        $this->add($tiposelNome);


        $salvar = new Element\Submit('salvar');
        $salvar->setAttributes([
            'value'   => 'Salvar',
            'class'   => 'btn btn-primary pull-left',
        ]);
        $this->add($salvar);

    }

}