<?php
namespace Vestibular\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class RelatorioVestibular extends Form{
    public function __construct( $options = array()) {
        parent::__construct("relatorioVestibular");
        $this->setAttribute("data-validate", "yes");

        $this->setAttribute("class", "smart-form");
        $this->setAttribute("data-validate", "yes");
        $this->setAttribute("action","");
        $this->setAttribute("target","_blank");
      
        $tipo = new Element\Radio('tipo');
        $tipo->setAttributes(array(
            'col' => 4,
            'label' => 'Lista de:',
            'data-validations' => 'Required'
        ));
        $tipo->setValueOptions(array(
            'classificado'    =>'Aprovados',
            'excedente'       =>'Excedente',
            'desclassificado' =>'Não-aprovados'
        ));
        $this->add($tipo);

        $ordem = new Element\Select('ordem');
        $ordem->setValueOptions(array(
           'Alfabética'    => 'Alfabética',
           'Classificação' => 'Classificação'
        ));
        $ordem->setAttributes(array(
            'col' => 3,
            'label' => 'Ordenar por:',
            'data-validations' => 'Required',
        ));
        $this->add($ordem);
        
        $print = new Element\Submit('Imprimir');
        $print->setAttributes([
            'class' => 'btn btn-primary pull-left',
            'value' => 'Imprimir'
        ]);
        $this->add($print);

        $edicoes = new Element\Select('edicaoId');
        $edicoes->setAttributes(
            [
                //            'required' => true,
                'label' => 'Selecione a edição do Vestibular:',
                'col'   => '3',
                'data-validations' => 'Required'
            ]
        );
        $edicoes->setValueOptions($options);
        $this->add($edicoes);
    }
}