<?php

namespace Vestibular\Form;

use Pessoa\Form\PessoaFisica;
use Zend\Form\Element;

/**
 * Description of SelecaoInscricaoCadastro
 *
 * @author versa
 */

class SelecaoInscricaoCadastro extends PessoaFisica {
    /**
     * @param array $cursoOptions: opcões de cursos
     * @param array $provaLinguas: opções de línguas estrangeiras
     * @param array $tipoEdicao
     * @param null  $necessidadesEspeciais: opçoes de necessidades especiais
     */

    public function __construct(array $cursoOptions, array $provaLinguas, array $tipoEdicao, $necessidadesEspeciais = null)
    {
        parent::__construct($necessidadesEspeciais);
        
        //Id de cadastro do candidato
        $inscricaoId = new Element\Hidden("inscricaoId");
        $this->add($inscricaoId);

        //Dados de Necessidades especiais temporarios (Estes atributos serão pertencentes o VersaSkeleton)
        $possuiNecessidades = new Element\Radio('possuiNecessidades');
        $possuiNecessidades->setAttributes(array(
            'col' => 4,
            'label' => 'Possui Necessidades Especiais:',
            'data-validations' => 'Required'
        ));
        $possuiNecessidades->setValueOptions(array(
            'Nao' => 'Não',
            'Sim' => 'Sim',
        ));
        $this->add($possuiNecessidades);
        
        //Tipo de ações (Nota Enem, Sistema de cotas) aceitos no vestibular (vide Banco).
        $selecaoTipo = new Element\Select('seletipoedicao');
        $selecaoTipo->setAttributes(array(
           'col' => 3,
           'label' => 'Forma de Ingresso',
           'icon' => 'icon-prepend fa fa-road',
           'data-validations' => 'Required'
        ));
        $selecaoTipo->setValueOptions($tipoEdicao);
        foreach($tipoEdicao as $key => $tipo){
            if($tipo == 'Vestibular'){
                $selecaoTipo->setValue($key);
            }
        }
        $this->add($selecaoTipo);
        
        $submit = new Element\Submit('submit');
        $submit->setAttribute('class', 'pull-left btn btn-primary');       
        $submit->setValue('Enviar');
        $this->add($submit);
        
        //Selects de cursos
        $cursos = new Element\Select('curso');
        $cursos->setAttributes(array(
            'col'   => 5,
            'label' => 'Selecione o Curso',
            'icon'  => 'icon-prepend fa fa-graduation-cap'
        ));
        $cursos->setValueOptions($cursoOptions);
        $this->add($cursos);
        
        //Opções para provas de linguas estrangeira.
        $lingua = new Element\Select('inscricaoLinguaEstrangeira');
        $lingua->setValueOptions($provaLinguas);
        $lingua->setAttributes(array(
           'col' => 3,
           'label' => 'Prova Estrangeira:',
           'icon-prepend' => ' fa fa-language',
            ));
        $this->add($lingua);
        
        //Senha para acesso ao sistema.
        $password = new Element\Password('password');
        $password->setAttributes(array(
            'col' => 4,
            'label' => 'Senha para acesso:',
            'icon'=>'icon-append fa fa-lock',
            'wrap' => true,
            'placeholder'=> 'Digite uma senha',
            'data-validations' => 'Required',
        ));
        $this->add($password);     
        
        //Status do candidato.
        $status = new Element\Select('status');
        $status->setValueOptions(array(
            'Aceita'     => 'Aceita',
            'Solicitada' => 'Solicitada',
            'Incompleta' => 'Incompleta'
        ));
        $status->setAttributes(array(
            'readonly'=>'readonly',
            'label'   => 'Status da inscricão:',
            'icon'    => 'icon-prepend fa fa-building'
        ));
        $this->add($status);
        
        //Data de realização da prova do candidato.
        $dataProva = new Element\Text('dataProva');
        $dataProva->setAttributes(array(
            
            'readonly'=>'readonly',
            'label'=> 'Data da Prova:',
            'icon'    => 'icon-prepend fa fa-building',
            'data-validations' => 'Date'

        ));
        $this->add($dataProva);
        
        //Descricao da edicao que o candidato esta vinculado e/ou inscrito.
        $edicaoDescricao = new Element\Select('edicaoDescricao');
        $edicaoDescricao->setAttributes(array(
            'col' => 4,
            'readonly'=>'readonly',
            'label'   => 'Processo de seleção:',
            'icon'    => 'icon-prepend fa fa-university' 
            	
        ));
        $this->add($edicaoDescricao);
        
        $dataNascimento = $this->get('pesDataNascimento');
        $dataNascimento->setAttribute('data-validations', 'Data');

        $inscricaoNotaEnemCiencHumanas = new Element\Text('inscricaoNotaEnemCiencHumanas');
        $inscricaoNotaEnemCiencHumanas->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Nota Ciências Humanas',
            'label' => 'Ciencias Humanas:',
            'wrap' => true,
            'data-validations' => 'Required',
            'data-masked'  => 'Enem'

        ));
        $this->add($inscricaoNotaEnemCiencHumanas);

        $inscricaoNotaEnemCiencNatureza = new Element\Text('inscricaoNotaEnemCiencNatureza');
        $inscricaoNotaEnemCiencNatureza->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Nota Ciẽncias da Natureza',
            'label' => 'Ciencias da Natureza:',
            'wrap' => true,
            'data-validations' => 'Required',
            'data-masked'  => 'Enem'
        ));
        $this->add($inscricaoNotaEnemCiencNatureza);

        $inscricaoNotaEnemPortugues = new Element\Text('inscricaoNotaEnemPortugues');
        $inscricaoNotaEnemPortugues->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Nota Portugês',
            'label' => 'Portugês:',
            'wrap' => true,
            'data-validations' => 'Required',
            'data-masked'  => 'Enem'
        ));
        $this->add($inscricaoNotaEnemPortugues);

        $inscricaoNotaEnemRedacao = new Element\Text('inscricaoNotaEnemRedacao');
        $inscricaoNotaEnemRedacao->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Nota Redação',
            'label' => 'Redação:',
            'wrap' => true,
            'data-validations' => 'Required',
            'data-masked'  => 'Enem'
        ));
        $this->add($inscricaoNotaEnemRedacao);

        $inscricaoNotaEnemMatematica = new Element\Text('inscricaoNotaEnemMatematica');
        $inscricaoNotaEnemMatematica->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Nota Matemática',
            'label' => 'Matemática:',
            'wrap' => true,
            'data-validations' => 'Required',
            'data-masked'  => 'Enem'
        ));
        $this->add($inscricaoNotaEnemMatematica);

        $inscricaoUltimaFormacao = new Element\Text('inscricaoUltimaFormacao');
        $inscricaoUltimaFormacao->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Ultima formação',
            'label' => 'Ultima formação:',
            'wrap' => true,
            'data-validations' => 'Required',

        ));
        $this->add($inscricaoUltimaFormacao);

        $inscricaoDataFormacao = new Element\Text('inscricaoDataFormacao');
        $inscricaoDataFormacao->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Data de formação',
            'label' => 'Data:',
            'wrap' => true,
            'data-validations' => 'Required,Data',
            'data-masked' => 'Date'


        ));
        $this->add($inscricaoDataFormacao);

        $inscricaoCidadeFormacao = new Element\Text('inscricaoCidadeFormacao');
        $inscricaoCidadeFormacao->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Cidade de formação',
            'label' => 'Cidade:',
            'wrap' => true,
            'data-validations' => 'Required',

        ));
        $this->add($inscricaoCidadeFormacao);

        $inscricaoFormacaoInstituicao = new Element\Text('inscricaoFormacaoInstituicao');
        $inscricaoFormacaoInstituicao->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Instituição de formação',
            'label' => 'Instituição:',
            'wrap' => true,
            'data-validations' => 'Required',

        ));
        $this->add($inscricaoFormacaoInstituicao);


        $inscricaoEntregaDoc = new Element\Radio('inscricaoEntregaDoc');
        $inscricaoEntregaDoc->setAttributes(array(
            'col' => 4,
            'label' => 'Documentos entregues:',
            'data-validations' => 'Required'
        ));
        $inscricaoEntregaDoc->setValueOptions(array(
            'Não' => 'Não',
            'Sim' => 'Sim',

        ));
        $this->add($inscricaoEntregaDoc);

        $tipoPagamento = new Element\Radio('tipoPagamento');
        $tipoPagamento->setAttributes(array(
            'col' => 4,
            'label' => 'Tipo de Pagamento:',
            'data-validations' => 'Required'
        ));
        $tipoPagamento->setValueOptions(array(
            'Boleto' => 'Boleto',
            'Dinheiro' => 'Dinheiro',
            'Isento' => 'Isento'
        ));
        $this->add($tipoPagamento);


        $statusInscricao = new Element\Text('statusInscricao');
        $statusInscricao->setAttributes(array(
            'readonly'=>'readonly',
            'label'   => 'Status da inscricão:',
            'icon'    => 'icon-prepend fa fa-building',
            'disabled' => 'disabled'
        ));
        $this->add($statusInscricao);

        $maeCpf = new Element\Text('maeCpf');
        $maeCpf->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Informe o CPF da Mãe',
            'label' => 'CPF da Mãe:',
            'data-validations' => 'Required,Cpf',
            'data-masked' => 'Cpf',
            'wrap' => true,
            'icon'=>'icon-prepend fa fa-credit-card'
        ));
        $this->add($maeCpf);

        $maeNome = new Element\Text('maeNome');
        $maeNome->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Informe o Nome Completo da Mãe ',
            'label' => 'Nome da Mãe:',
            'wrap' => true,
            'data-validations' => 'Required,String',
            'icon'=>'icon-prepend fa fa-user'

        ));
        $this->add($maeNome);


        $paiCpf = new Element\Text('paiCpf');
        $paiCpf->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Informe o CPF do Pai',
            'label' => 'CPF do Pai:',
            'data-masked' => 'Cpf',
            'wrap' => true,
            'icon'=>'icon-prepend fa fa-credit-card'
        ));
        $this->add($paiCpf);

        $paiNome = new Element\Text('paiNome');
        $paiNome->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Informe o Nome Completo do Pai',
            'label' => 'Nome do Pai:',
            'wrap' => true,
            'icon'=>'icon-prepend fa fa-user'

        ));
        $this->add($paiNome);
    }

}
