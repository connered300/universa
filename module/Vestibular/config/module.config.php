<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Vestibular;

return array(

    'router'                    => array(
        'routes' => array(
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'vestibular'                   => array(
                'type'          => 'Literal',
                'options'       => array(
                    'route'    => '/vestibular',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Vestibular\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes'  => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'       => '/[:controller[/:action[/:id]]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults'    => array(),
                        ),
                    ),
                ),
            ),
            'vestibular-login'             => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/vestibular/selecao-inscricao/login',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Vestibular\Controller',
                        'controller'    => 'SelecaoInscricao',
                        'action'        => 'login',
                    ),
                )
            ),
            'vestibular-selecao-inscricao' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/vestibular/selecao-inscricao/index',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Vestibular\Controller',
                        'controller'    => 'SelecaoInscricao',
                        'action'        => 'index',
                    ),
                )
            ),
        ),
    ),
    'service_manager'           => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases'            => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'doctrine'                  => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default'             => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            ),
        ),
    ),
    'controllers'               => array(
        'invokables' => array(
            'Vestibular\Controller\Cadastro'                => 'Vestibular\Controller\CadastroController',
            'Vestibular\Controller\SelecaoEdicao'           => 'Vestibular\Controller\SelecaoEdicaoController',
            'Vestibular\Controller\SelecaoInscricao'        => 'Vestibular\Controller\SelecaoInscricaoController',
            'Vestibular\Controller\InscricaoCursos'         => 'Vestibular\Controller\InscricaoCursosController',
            'Vestibular\Controller\ListaAprovados'          => 'Vestibular\Controller\ListaAprovadosController',
            'Vestibular\Controller\Dashboard'               => 'Vestibular\Controller\DashboardController',
            'Vestibular\Controller\Formularios'             => 'Vestibular\Controller\FormulariosController',
            'Vestibular\Controller\SelecaoEdicaoFechamento' => 'Vestibular\Controller\SelecaoEdicaoFechamentoController'
        ),
    ),
    'view_manager'              => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies'          => array(
            'ViewJsonStrategy'
        )
    ),
    // Placeholder for console routes
    'console'                   => array(
        'router' => array(
            'routes' => array(),
        ),
    ),
    'funcionalidades_liberadas' => array(
        'controllers' => array(),
        'actions'     => array(
            //'add', //TODO:mudar rota de inscrição do vestibular
            'login',
            'redefinir-senha',
            'dados-candidato',
            'logout',
            'busca-cadastro-ativo',
            'busca-cadastro-pessoa-fisica',
            'altera-data-vencimento-titulo',
            'alocacao',            //Remover.
            'alocacao-manual',     //Remover.
            'candidatos-sala',     // Remover.
            'impressao-ficha',     // Remover.
            'relatorios-salas', // Remover.
            'envia-boleto-atrasado-email',
            'envia-cartao-inscricao'
        )
    )
);