<?php


namespace CorrecaoProva\Core;


class Core {
    private $input;

    private $layout;

    private $output;

    private $config;

    function __construct(array $moduleConfig, $fileName, $quantQuestoes)
    {
        $this->config = new \CorrecaoProva\Config\Config($moduleConfig);
        $this->input = new \CorrecaoProva\Input\Input($fileName);

        $layout = "\\CorrecaoProva\\Layouts\\".$this->config->getLayout()."";
        $this->layout = new $layout($quantQuestoes);

        $output = "\\CorrecaoProva\\Output\\".$this->config->getOutput()."";
        $this->output = new $output($this->processaDados());
    }

    /**
     * @return mixed
     */
    public function getInput()
    {
        return $this->input;
    }

    /**
     * @param mixed $input
     */
    public function setInput($input)
    {
        $this->input = $input;
    }

    /**
     * @return mixed
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * @param mixed $layout
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;
    }

    /**
     * @return mixed
     */
    public function getOutput()
    {
        return $this->output;
    }

    /**
     * @param mixed $output
     */
    public function setOutput($output)
    {
        $this->output = $output;
    }

    /**
     * @return mixed
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param mixed $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    public function processaDados()
    {
        $dados = array();
        $arquivo = $this->input->readFile();
        foreach($arquivo as $linha) {
            $inscricao = '';
            $gabarito  = '';
            for($i = $this->layout->getInscricao()['inicio']; $i <= $this->layout->getInscricao()['fim']; $i++){
                $inscricao .= $linha[$i];
            }
            for ($i = $this->layout->getQuestoes()['inicio']; $i < $this->layout->getQuestoes()['fim']; $i++) {
                $gabarito .= $linha[$i];
            }
            $dados[$inscricao] = $gabarito;
        }
        return $dados;
    }
}