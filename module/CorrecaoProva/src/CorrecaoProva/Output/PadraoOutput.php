<?php


namespace CorrecaoProva\Output;


class PadraoOutput extends AbstractOutput {
    function __construct($dados)
    {
        parent::__construct($dados);
    }


    /**
     * Implementar esta função para retornar os dados no formato JSON.
     * */
    public function getJson()
    {
        // TODO: Implement getJson() method.
    }

    /**
     * Implementar esta função para retornar os dados no formato XML.
     * */
    public function getXml()
    {
        // TODO: Implement getXml() method.
    }

    /**
     * Implementar esta função para retornar os dados no formato Array.
     * */
    public function getArray()
    {
        return $this->getDados();
    }
}