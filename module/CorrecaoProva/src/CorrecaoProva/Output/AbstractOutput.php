<?php


namespace CorrecaoProva\Output;


abstract class AbstractOutput {
    private $dados;



    function __construct($dados)
    {
        $this->dados = $dados;
    }

    /**
     * @return mixed
     */
    public function getDados()
    {
        return $this->dados;
    }

    /**
     * @param mixed $dados
     */
    public function setDados($dados)
    {
        $this->dados = $dados;
    }

    /**
     * Implementar esta função para retornar os dados no formato JSON.
     * */
    public abstract function getJson();

    /**
     * Implementar esta função para retornar os dados no formato XML.
     * */
    public abstract function getXml();

    /**
     * Implementar esta função para retornar os dados no formato Array.
     * */
    public abstract function getArray();
}