<?php


namespace CorrecaoProva\Input;


class Input {
    private $fileName;

    function __construct($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return mixed
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param mixed $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }



    /**
     * Função para leitura do arquivo.
     * Retorna um array com as linhas do arquivo ou a linha que foi passada como parametro.
     * */
    public function readFile($pos = null)
    {
        if($pos){
            $arq = file($this->getFileName());
            if($arq[$pos]){
                return $arq[$pos];
            } else {
                throw new \Exception('Posição inválida.');
            }
        }
        return file($this->getFileName());
    }

}