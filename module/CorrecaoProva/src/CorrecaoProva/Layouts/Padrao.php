<?php
namespace CorrecaoProva\Layouts;


class Padrao extends AbstractLayout{
    function __construct($quantQuestoes)
    {
        $inscricao = array(
            'inicio' => 40,
            'fim'    => 44
        );
        $questoes = array(
            'inicio' => 45,
            'fim'    => 45 + $quantQuestoes
        );
        parent::__construct($inscricao, $questoes);
    }
}