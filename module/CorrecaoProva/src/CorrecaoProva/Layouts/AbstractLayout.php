<?php


namespace CorrecaoProva\Layouts;


abstract class AbstractLayout {
    /**
     *
    array (
    'inicio' => Recebe a posição em que inicia-se o número de inscricao
    'fim'    => Recebe a posição onde termina o número da inscrição
    ),
     * */
    private $inscricao;

    /**
     *
    array (
    'inicio' => Recebe a posição em que inicia-se as questões.
    'fim'    => Recebe a posição onde terminam as questões.
    ),
     * */
    private $questoes;

    function __construct(array $inscricao, array $questoes)
    {
        $this->inscricao = $inscricao;
        $this->questoes = $questoes;
    }

    /**
     * @return mixed
     */
    public function getInscricao()
    {
        return $this->inscricao;
    }

    /**
     * @param mixed $inscricao
     */
    public function setInscricao($inscricao)
    {
        $this->inscricao = $inscricao;
    }

    /**
     * @return mixed
     */
    public function getQuestoes()
    {
        return $this->questoes;
    }

    /**
     * @param mixed $questoes
     */
    public function setQuestoes($questoes)
    {
        $this->questoes = $questoes;
    }

}