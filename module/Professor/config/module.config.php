<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Professor;

return array(
    'router' => array(
        'routes' => array(
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'professor' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/professor',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Professor\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/[:controller[/:action[/:id]]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Professor\Controller\Index'                      => 'Professor\Controller\IndexController',
            'Professor\Controller\Disciplinas'                => 'Professor\Controller\DisciplinasController',
            'Professor\Controller\AcadperiodoAnotacaoArquivo' => 'Professor\Controller\AcadperiodoAnotacaoArquivoController',
            'Professor\Controller\AcadperiodoAnotacao'        => 'Professor\Controller\AcadperiodoAnotacaoController',
            'Professor\Controller\AcadperiodoEtapaAluno'      => 'Professor\Controller\AcadperiodoEtapaAlunoController',
            'Professor\Controller\Relatorios'                 => 'Professor\Controller\RelatoriosController',
            'Professor\Controller\Biblioteca'                 => 'Professor\Controller\BibliotecaController',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
    'funcionalidades_liberadas' => array(
        'controllers' => array(
            "Professor\Controller\Index",
            "Professor\Controller\Relatorios",
        ),
        'actions' => array(
//            'anotacoes',//CADASTRAR -> DisciplinasController
//            'save-anotacao',//CADASTRAR -> DisciplinasController
//            'remove',//CADASTRAR -> DisciplinasController
//            'insert-file', // CADASTRAR
//            'remove-file', // CADASTRAR
//            'busca-arquivos-anotacao', // CADASTRAR
//            'notas', // Cadastrar -> DisciplinasController
//            'save-etapa-aluno', // CADASTRAR -> AcadperiodoEtapaAluno
            'diario',
//            'relatorio-anotacao' // CADASTRAR -> AcadperiodoAnotacao
            'documentos',
            'diario-classe',
            'diario-notas',
            'diario-anotacoes-semestral',
            'diario-notas'
        ),
    )
);
