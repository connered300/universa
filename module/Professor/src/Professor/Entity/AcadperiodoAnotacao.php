<?php

namespace Professor\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoAnotacao
 *
 * @ORM\Table(name="acadperiodo__anotacao", indexes={@ORM\Index(name="fk_acadperiodo__anotacao_acadperiodo__docente_disciplina1_idx", columns={"docdisc_id"})})
 * @ORM\Entity
 */
class AcadperiodoAnotacao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="anot_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $anotId;

    /**
     * @var string
     *
     * @ORM\Column(name="anot_titulo", type="string", length=45, nullable=false)
     */
    private $anotTitulo;

    /**
     * @var string
     *
     * @ORM\Column(name="anot_descricao", type="text", nullable=false)
     */
    private $anotDescricao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="anot_data", type="datetime", nullable=false)
     */
    private $anotData;

    /**
     * @var \Matricula\Entity\AcadperiodoDocenteDisciplina
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoDocenteDisciplina")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="docdisc_id", referencedColumnName="docdisc_id")
     * })
     */
    private $docdisc;

    function __construct(array $data)
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getAnotId()
    {
        return $this->anotId;
    }

    /**
     * @param int $anotId
     */
    public function setAnotId($anotId)
    {
        $this->anotId = $anotId;
    }

    /**
     * @return string
     */
    public function getAnotTitulo()
    {
        return $this->anotTitulo;
    }

    /**
     * @param string $anotTitulo
     */
    public function setAnotTitulo($anotTitulo)
    {
        $this->anotTitulo = $anotTitulo;
    }

    /**
     * @return string
     */
    public function getAnotDescricao()
    {
        return $this->anotDescricao;
    }

    /**
     * @param string $anotDescricao
     */
    public function setAnotDescricao($anotDescricao)
    {
        $this->anotDescricao = $anotDescricao;
    }

    /**
     * @return DateTime
     */
    public function getAnotData()
    {
        return $this->anotData;
    }

    /**
     * @param DateTime $anotData
     */
    public function setAnotData($anotData)
    {
        $this->anotData = $anotData;
    }

    /**
     * @return \Matricula\Entity\AcadperiodoDocenteDisciplina
     */
    public function getDocdisc()
    {
        return $this->docdisc;
    }

    /**
     * @param \Matricula\Entity\AcadperiodoDocenteDisciplina $docdisc
     */
    public function setDocdisc($docdisc)
    {
        $this->docdisc = $docdisc;
    }

    public function toArray()
    {
        return array(
            'anotId'        => $this->getAnotId(),
            'anotTitulo'    => $this->getAnotTitulo(),
            'anotDescricao' => $this->getAnotDescricao(),
            'anotData'      => $this->getAnotData(),
            'docdisc'       => $this->getDocdisc()->getDocdiscId()
        );
    }
}
