<?php

namespace Professor\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoEtapaDiario
 *
 * @ORM\Table(name="acadperiodo__etapa_diario", indexes={@ORM\Index(name="fk_acadperiodo__encerramento_diario_acadperiodo__etapas1_idx", columns={"etapa_id"}), @ORM\Index(name="fk_acadperiodo__encerramento_diario_acadperiodo__docente_di_idx", columns={"docdisc_id"})})
 * @ORM\Entity
 */
class AcadperiodoEtapaDiario
{
    /**
     * @var integer
     *
     * @ORM\Column(name="diario_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $diarioId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="diario_data_envio", type="datetime", nullable=true)
     */
    private $diarioDataEnvio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="diario_data_envio_final", type="datetime", nullable=true)
     */
    private $diarioDataEnvioFinal;

    /**
     * @var \Matricula\Entity\AcadperiodoDocenteDisciplina
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoDocenteDisciplina")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="docdisc_id", referencedColumnName="docdisc_id")
     * })
     */
    private $docdisc;

    /**
     * @var \Matricula\Entity\AcadperiodoEtapas
     *
     * @ORM\ManyToOne(targetEntity="AcadperiodoEtapas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="etapa_id", referencedColumnName="etapa_id")
     * })
     */
    private $etapa;

    function __construct(array $data)
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getDiarioId()
    {
        return $this->diarioId;
    }

    /**
     * @param int $diarioId
     */
    public function setDiarioId($diarioId)
    {
        $this->diarioId = $diarioId;
    }

    /**
     * @return \DateTime
     */
    public function getDiarioDataEnvio()
    {
        return $this->diarioDataEnvio;
    }

    /**
     * @param \DateTime $diarioDataEnvio
     */
    public function setDiarioDataEnvio($diarioDataEnvio)
    {
        $this->diarioDataEnvio = $diarioDataEnvio;
    }

    /**
     * @return \DateTime
     */
    public function getDiarioDataEnvioFinal()
    {
        return $this->diarioDataEnvioFinal;
    }

    /**
     * @param \DateTime $diarioDataEnvioFinal
     */
    public function setDiarioDataEnvioFinal($diarioDataEnvioFinal)
    {
        $this->diarioDataEnvioFinal = $diarioDataEnvioFinal;
    }

    /**
     * @return \Matricula\Entity\AcadperiodoDocenteDisciplina
     */
    public function getDocdisc()
    {
        return $this->docdisc;
    }

    /**
     * @param \Matricula\Entity\AcadperiodoDocenteDisciplina $docdisc
     */
    public function setDocdisc($docdisc)
    {
        $this->docdisc = $docdisc;
    }

    /**
     * @return \Matricula\Entity\AcadperiodoEtapas
     */
    public function getEtapa()
    {
        return $this->etapa;
    }

    /**
     * @param \Matricula\Entity\AcadperiodoEtapas $etapa
     */
    public function setEtapa($etapa)
    {
        $this->etapa = $etapa;
    }
}
