<?php


namespace Professor\Entity\Repository;


use Doctrine\ORM\EntityRepository;

class AcadperiodoGrade extends EntityRepository{
    public function buscaGradeHorariaPorTurma($turma)
    {
        $grade = null;
        if($turma){
            $grade =$this->findOneBy(['turma' => $turma, 'gradeCiclo' => null]);
        }
        return $grade;
    }

    public function buscaGradeHorariaPorTurmaEData($turma, $data)
    {
        $grade = null;
        if($turma){
            $query = $this->getEntityManager()->createQuery("SELECT u FROM Professor\Entity\AcadperiodoGrade u WHERE u.turma = {$turma} AND ('{$data}' >= u.gradeInicio AND '{$data}' <= u.gradeFim )");
            $grade = $query->getResult()[0];
            if(!$grade){
                $grade =  $this->buscaGradeHorariaPorTurma($turma);
            }

        }
        return $grade;
    }

}