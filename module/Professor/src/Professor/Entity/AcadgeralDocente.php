<?php

namespace Professor\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadgeralDocente
 *
 * @ORM\Table(name="acadgeral__docente", indexes={@ORM\Index(name="fk_acadgeral__docente_pessoa_fisica1_idx", columns={"pes_id"})})
 * @ORM\Entity
 */
class AcadgeralDocente
{
    /**
     * @var integer
     *
     * @ORM\Column(name="docente_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $docenteId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="docente_data_inicio", type="datetime", nullable=false)
     */
    private $docenteDataInicio;

    /**
     * @var string
     *
     * @ORM\Column(name="docente_ativo", type="string", nullable=false)
     */
    private $docenteAtivo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="docente_data_fim", type="datetime", nullable=true)
     */
    private $docenteDataFim;

    /**
     * @var string
     *
     * @ORM\Column(name="docente_cur_lattes", type="string", length=255, nullable=true)
     */
    private $docenteCurLattes;

    /**
     * @var integer
     *
     * @ORM\Column(name="docente_num_mec", type="integer", nullable=true)
     */
    private $docenteNumMec;

    /**
     * @var string
     *
     * @ORM\Column(name="docente_tituto", type="string", length=45, nullable=true)
     */
    private $docenteTituto;

    /**
     * @var \Pessoa\Entity\PessoaFisica
     *
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\PessoaFisica")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     */
    private $pes;

    /**
     * @return int
     */
    public function getDocenteId()
    {
        return $this->docenteId;
    }

    /**
     * @param int $docenteId
     */
    public function setDocenteId($docenteId)
    {
        $this->docenteId = $docenteId;
    }

    /**
     * @return \DateTime
     */
    public function getDocenteDataInicio()
    {
        return $this->docenteDataInicio;
    }

    /**
     * @param \DateTime $docenteDataInicio
     */
    public function setDocenteDataInicio($docenteDataInicio)
    {
        $this->docenteDataInicio = $docenteDataInicio;
    }

    /**
     * @return string
     */
    public function getDocenteAtivo()
    {
        return $this->docenteAtivo;
    }

    /**
     * @param string $docenteAtivo
     */
    public function setDocenteAtivo($docenteAtivo)
    {
        $this->docenteAtivo = $docenteAtivo;
    }

    /**
     * @return \DateTime
     */
    public function getDocenteDataFim()
    {
        return $this->docenteDataFim;
    }

    /**
     * @param \DateTime $docenteDataFim
     */
    public function setDocenteDataFim($docenteDataFim)
    {
        $this->docenteDataFim = $docenteDataFim;
    }

    /**
     * @return string
     */
    public function getDocenteCurLattes()
    {
        return $this->docenteCurLattes;
    }

    /**
     * @param string $docenteCurLattes
     */
    public function setDocenteCurLattes($docenteCurLattes)
    {
        $this->docenteCurLattes = $docenteCurLattes;
    }

    /**
     * @return int
     */
    public function getDocenteNumMec()
    {
        return $this->docenteNumMec;
    }

    /**
     * @param int $docenteNumMec
     */
    public function setDocenteNumMec($docenteNumMec)
    {
        $this->docenteNumMec = $docenteNumMec;
    }

    /**
     * @return string
     */
    public function getDocenteTituto()
    {
        return $this->docenteTituto;
    }

    /**
     * @param string $docenteTituto
     */
    public function setDocenteTituto($docenteTituto)
    {
        $this->docenteTituto = $docenteTituto;
    }

    /**
     * @return \Pessoa\Entity\PessoaFisica
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @param \Pessoa\Entity\PessoaFisica $pes
     */
    public function setPes($pes)
    {
        $this->pes = $pes;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \VersaSpine\Stdlib\Hydrator())->extract($this);
    }
}
