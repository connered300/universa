<?php

namespace Professor\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoFrequencia
 *
 * @ORM\Table(name="acadperiodo__frequencia", uniqueConstraints={@ORM\UniqueConstraint(name="frequencia_id_UNIQUE", columns={"frequencia_id"})}, indexes={@ORM\Index(name="fk_acadperiodo__frequencia_acadperiodo__docente_disciplina1_idx", columns={"docdisc_id"}), @ORM\Index(name="fk_acadperiodo__frequencia_acadperiodo__aluno_disciplina1_idx", columns={"alunodisc_id"})})
 * @ORM\Entity
 */
class AcadperiodoFrequencia
{
    /**
     * @var integer
     *
     * @ORM\Column(name="frequencia_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $frequenciaId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="frequencia_data", type="datetime", nullable=false)
     */
    private $frequenciaData;

    /**
     * @var \Matricula\Entity\AcadperiodoDocenteDisciplina
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoDocenteDisciplina")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="docdisc_id", referencedColumnName="docdisc_id")
     * })
     */
    private $docdisc;

    /**
     * @var \Matricula\Entity\AcadperiodoAlunoDisciplina
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoAlunoDisciplina")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="alunodisc_id", referencedColumnName="alunodisc_id")
     * })
     */
    private $alunodisc;

    function __construct(array $data)
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getFrequenciaId()
    {
        return $this->frequenciaId;
    }

    /**
     * @param int $frequenciaId
     */
    public function setFrequenciaId($frequenciaId)
    {
        $this->frequenciaId = $frequenciaId;
    }

    /**
     * @return \DateTime
     */
    public function getFrequenciaData()
    {
        return $this->frequenciaData;
    }

    /**
     * @param \DateTime $frequenciaData
     */
    public function setFrequenciaData($frequenciaData)
    {
        $this->frequenciaData = $frequenciaData;
    }

    /**
     * @return \Matricula\Entity\AcadperiodoDocenteDisciplina
     */
    public function getDocdisc()
    {
        return $this->docdisc;
    }

    /**
     * @param \Matricula\Entity\AcadperiodoDocenteDisciplina $docdisc
     */
    public function setDocdisc($docdisc)
    {
        $this->docdisc = $docdisc;
    }

    /**
     * @return \Matricula\Entity\AcadperiodoAlunoDisciplina
     */
    public function getAlunodisc()
    {
        return $this->alunodisc;
    }

    /**
     * @param \Matricula\Entity\AcadperiodoAlunoDisciplina $alunodisc
     */
    public function setAlunodisc($alunodisc)
    {
        $this->alunodisc = $alunodisc;
    }

    public function toArray()
    {
        return array(
            'frequenciaId'   => $this->getFrequenciaId(),
            'alunodiscId'    => $this->getAlunodisc()->getAlunodiscId(),
            'frequenciaData' => $this->getFrequenciaData(),
            'docdisc'        => $this->getDocdisc()->getDocdiscId()
        );
    }
}
