<?php

namespace Professor\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoCalendarioData
 *
 * @ORM\Table(name="acadperiodo__calendario_data", indexes={@ORM\Index(name="fk_acadperiodo__calendario_data_acadperiodo__calendario1_idx", columns={"calendario_id"})})
 * @ORM\Entity
 */
class AcadperiodoCalendarioData
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_id", type="date", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $dataId;

    /**
     * @var string
     *
     * @ORM\Column(name="data_letivo", type="string", nullable=true)
     */
    private $dataLetivo;

    /**
     * @var \AcadperiodoCalendario
     *
     * @ORM\ManyToOne(targetEntity="AcadperiodoCalendario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="calendario_id", referencedColumnName="calendario_id")
     * })
     */
    private $calendario;

    function __construct(array $data)
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return \DateTime
     */
    public function getDataId()
    {
        return $this->dataId;
    }

    /**
     * @param \DateTime $dataId
     */
    public function setDataId($dataId)
    {
        $this->dataId = $dataId;
    }

    /**
     * @return string
     */
    public function getDataLetivo()
    {
        return $this->dataLetivo;
    }

    /**
     * @param string $dataLetivo
     */
    public function setDataLetivo($dataLetivo)
    {
        $this->dataLetivo = $dataLetivo;
    }

    /**
     * @return \AcadperiodoCalendario
     */
    public function getCalendario()
    {
        return $this->calendario;
    }

    /**
     * @param \AcadperiodoCalendario $calendario
     */
    public function setCalendario($calendario)
    {
        $this->calendario = $calendario;
    }

    public function toArray()
    {
        return array(
            'dataId' => $this->getDataId(),
            'dataLetivo' => $this->getDataLetivo(),
            'calendario' => $this->getCalendario()
        );
    }

}
