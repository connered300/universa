<?php

namespace Professor\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoCalendarioTipoEvento
 *
 * @ORM\Table(name="acadperiodo__calendario_tipo_evento")
 * @ORM\Entity
 */
class AcadperiodoCalendarioTipoEvento
{
    /**
     * @var integer
     *
     * @ORM\Column(name="tipoevento_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $tipoeventoId;

    /**
     * @var string
     *
     * @ORM\Column(name="tipoevento_label", type="string", length=45, nullable=false)
     */
    private $tipoeventoLabel;

    /**
     * @var string
     *
     * @ORM\Column(name="tipoevento_letivo", type="string", nullable=false)
     */
    private $tipoeventoLetivo = 'Sim';

    /**
     * @var string
     *
     * @ORM\Column(name="tipoevento_legenda", type="string", length=45, nullable=false)
     */
    private $tipoeventoLegenda;

    /**
     * @var string
     *
     * @ORM\Column(name="tipoevento_padrao", type="string", nullable=false)
     */
    private $tipoeventoPadrao = 'Sim';

    function __construct(array $data)
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getTipoeventoId()
    {
        return $this->tipoeventoId;
    }

    /**
     * @param int $tipoeventoId
     */
    public function setTipoeventoId($tipoeventoId)
    {
        $this->tipoeventoId = $tipoeventoId;
    }

    /**
     * @return string
     */
    public function getTipoeventoLabel()
    {
        return $this->tipoeventoLabel;
    }

    /**
     * @param string $tipoeventoLabel
     */
    public function setTipoeventoLabel($tipoeventoLabel)
    {
        $this->tipoeventoLabel = $tipoeventoLabel;
    }

    /**
     * @return string
     */
    public function getTipoeventoLetivo()
    {
        return $this->tipoeventoLetivo;
    }

    /**
     * @param string $tipoeventoLetivo
     */
    public function setTipoeventoLetivo($tipoeventoLetivo)
    {
        $this->tipoeventoLetivo = $tipoeventoLetivo;
    }

    /**
     * @return string
     */
    public function getTipoeventoLegenda()
    {
        return $this->tipoeventoLegenda;
    }

    /**
     * @param string $tipoeventoLegenda
     */
    public function setTipoeventoLegenda($tipoeventoLegenda)
    {
        $this->tipoeventoLegenda = $tipoeventoLegenda;
    }

    /**
     * @return string
     */
    public function getTipoeventoPadrao()
    {
        return $this->tipoeventoPadrao;
    }

    /**
     * @param string $tipoeventoPadrao
     */
    public function setTipoeventoPadrao($tipoeventoPadrao)
    {
        $this->tipoeventoPadrao = $tipoeventoPadrao;
    }

    public function toArray()
    {
        return array(
            'tipoeventoId'      => $this->getTipoeventoId(),
            'tipoeventoLabel'   => $this->getTipoeventoLabel(),
            'tipoeventoLetivo'  => $this->getTipoeventoLetivo(),
            'tipoeventoLegenda' => $this->getTipoeventoLegenda(),
            'tipoeventoPadrao'  => $this->getTipoeventoPadrao()
        );
    }

}
