<?php

namespace Professor\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadgeralDisciplinaGrade
 *
 * @ORM\Table(name="acadperiodo__disciplina_grade", uniqueConstraints={@ORM\UniqueConstraint(name="index3", columns={"grade_id", "disc_id"})}, indexes={@ORM\Index(name="fk_acadgeral__disciplina_grd_grd1_idx", columns={"grade_id"}), @ORM\Index(name="fk_acadgeral__disciplina_grd_acadgeral__disciplina1_idx", columns={"disc_id"})})
 * @ORM\Entity
 */
class AcadperiodoDisciplinaGrade
{
    /**
     * @var integer
     *
     * @ORM\Column(name="disc_grade_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $discGradeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="disc_id", type="integer", nullable=false)
     */
    private $discId;

    /**
     * @var integer
     *
     * @ORM\Column(name="grade_id", type="integer", nullable=false)
     */
    private $gradeId;

    /**
     * @var string
     *
     * @ORM\Column(name="gradedisc_dia", type="string", nullable=false)
     */
    private $gradediscDia;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="gradedisc_hora", type="time", nullable=false)
     */
    private $gradediscHora;

    /**
     * @return int
     */
    public function getDiscGradeId()
    {
        return $this->discGradeId;
    }

    /**
     * @param int $discGradeId
     */
    public function setDiscGradeId($discGradeId)
    {
        $this->discGradeId = $discGradeId;
    }

    /**
     * @return int
     */
    public function getDiscId()
    {
        return $this->discId;
    }

    /**
     * @param int $discId
     */
    public function setDiscId($discId)
    {
        $this->discId = $discId;
    }

    /**
     * @return int
     */
    public function getGradeId()
    {
        return $this->gradeId;
    }

    /**
     * @param int $gradeId
     */
    public function setGradeId($gradeId)
    {
        $this->gradeId = $gradeId;
    }

    /**
     * @return string
     */
    public function getGradediscDia()
    {
        return $this->gradediscDia;
    }

    /**
     * @param string $gradediscDia
     */
    public function setGradediscDia($gradediscDia)
    {
        $this->gradediscDia = $gradediscDia;
    }

    /**
     * @return \DateTime
     */
    public function getGradediscHora()
    {
        return $this->gradediscHora;
    }

    /**
     * @param \DateTime $gradediscHora
     */
    public function setGradediscHora($gradediscHora)
    {
        $this->gradediscHora = $gradediscHora;
    }

}
