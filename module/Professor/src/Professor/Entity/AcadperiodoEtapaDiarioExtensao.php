<?php

namespace Professor\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoEtapaDiarioExtensao
 *
 * @ORM\Table(name="acadperiodo__etapa_diario_extensao", uniqueConstraints={@ORM\UniqueConstraint(name="etapadiario_extensao_id_UNIQUE", columns={"etapadiario_extensao_id"})}, indexes={@ORM\Index(name="fk_acadperiodo__etapa_diario_extensao_acadperiodo__etapa_di_idx", columns={"diario_id"}), @ORM\Index(name="fk_acadperiodo__etapa_diario_extensao_acesso_pessoas1_idx", columns={"usuario_liberacao"})})
 * @ORM\Entity
 */
class AcadperiodoEtapaDiarioExtensao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="etapadiario_extensao_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $etapadiarioExtensaoId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="etapadiario_extensao_data_solicitacao", type="datetime", nullable=true)
     */
    private $etapadiarioExtensaoDataSolicitacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="etapadiario_extensao_data_alteracao", type="datetime", nullable=true)
     */
    private $etapadiarioExtensaoDataAlteracao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="etapadiario_extensao_data", type="datetime", nullable=true)
     */
    private $etapadiarioExtensaoData;

    /**
     * @var string
     *
     * @ORM\Column(name="etapadiario_extensao_justificativa", type="text", length=65535, nullable=true)
     */
    private $etapadiarioExtensaoJustificativa;

    /**
     * @var string
     *
     * @ORM\Column(name="etapadiario_extensao_observacao", type="text", length=65535, nullable=true)
     */
    private $etapadiarioExtensaoObservacao;

    /**
     * @var AcadperiodoEtapaDiario
     *
     * @ORM\ManyToOne(targetEntity="AcadperiodoEtapaDiario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="diario_id", referencedColumnName="diario_id")
     * })
     */
    private $diario;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     *
     * @ORM\ManyToOne(targetEntity="\Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_liberacao", referencedColumnName="id")
     * })
     */
    private $usuarioLiberacao;

    /**
     * @return int
     */

    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    public function getEtapadiarioExtensaoId()
    {
        return $this->etapadiarioExtensaoId;
    }

    /**
     * @param int $etapadiarioExtensaoId
     */
    public function setEtapadiarioExtensaoId($etapadiarioExtensaoId)
    {
        $this->etapadiarioExtensaoId = $etapadiarioExtensaoId;
    }

    /**
     * @return \DateTime
     */
    public function getEtapadiarioExtensaoDataSolicitacao()
    {
        return $this->etapadiarioExtensaoDataSolicitacao;
    }

    /**
     * @param \DateTime $etapadiarioExtensaoDataSolicitacao
     */
    public function setEtapadiarioExtensaoDataSolicitacao($etapadiarioExtensaoDataSolicitacao)
    {
        $this->etapadiarioExtensaoDataSolicitacao = $etapadiarioExtensaoDataSolicitacao;
    }

    /**
     * @return \DateTime
     */
    public function getEtapadiarioExtensaoDataAlteracao()
    {
        return $this->etapadiarioExtensaoDataAlteracao;
    }

    /**
     * @param \DateTime $etapadiarioExtensaoDataAlteracao
     */
    public function setEtapadiarioExtensaoDataAlteracao($etapadiarioExtensaoDataAlteracao)
    {
        $this->etapadiarioExtensaoDataAlteracao = $etapadiarioExtensaoDataAlteracao;
    }

    /**
     * @return \DateTime
     */
    public function getEtapadiarioExtensaoData()
    {
        return $this->etapadiarioExtensaoData;
    }

    /**
     * @param \DateTime $etapadiarioExtensaoData
     */
    public function setEtapadiarioExtensaoData($etapadiarioExtensaoData)
    {
        $this->etapadiarioExtensaoData = $etapadiarioExtensaoData;
    }

    /**
     * @return string
     */
    public function getEtapadiarioExtensaoJustificativa()
    {
        return $this->etapadiarioExtensaoJustificativa;
    }

    /**
     * @param string $etapadiarioExtensaoJustificativa
     */
    public function setEtapadiarioExtensaoJustificativa($etapadiarioExtensaoJustificativa)
    {
        $this->etapadiarioExtensaoJustificativa = $etapadiarioExtensaoJustificativa;
    }

    /**
     * @return string
     */
    public function getEtapadiarioExtensaoObservacao()
    {
        return $this->etapadiarioExtensaoObservacao;
    }

    /**
     * @param string $etapadiarioExtensaoObservacao
     */
    public function setEtapadiarioExtensaoObservacao($etapadiarioExtensaoObservacao)
    {
        $this->etapadiarioExtensaoObservacao = $etapadiarioExtensaoObservacao;
    }

    /**
     * @return AcadperiodoEtapaDiario
     */
    public function getDiario()
    {
        return $this->diario;
    }

    /**
     * @param AcadperiodoEtapaDiario $diario
     */
    public function setDiario($diario)
    {
        $this->diario = $diario;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuarioLiberacao()
    {
        return $this->usuarioLiberacao;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuarioLiberacao
     */
    public function setUsuarioLiberacao($usuarioLiberacao)
    {
        $this->usuarioLiberacao = $usuarioLiberacao;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \VersaSpine\Stdlib\Hydrator())->extract($this);
    }

}

