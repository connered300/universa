<?php

namespace Professor\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoEtapaAluno
 *
 * @ORM\Table(name="acadperiodo__etapa_aluno", indexes={@ORM\Index(name="fk_acadperiodo__aluno_etapa_acadperiodo__encerramento_diari_idx", columns={"diario_id"}), @ORM\Index(name="fk_acadperiodo__aluno_etapa_acadperiodo__aluno_disciplina1_idx", columns={"alunodisc_id"}), @ORM\Index(name="fk_acadperiodo__etapa_aluno_acesso_pessoas1_idx", columns={"usuario_id"})})
 * @ORM\Entity
 */
class AcadperiodoEtapaAluno
{
    /**
     * @var integer
     *
     * @ORM\Column(name="alunoetapa_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $alunoetapaId;

    /**
     * @var float
     *
     * @ORM\Column(name="alunoetapa_nota", type="decimal", precision=5, scale=1, nullable=true)
     */
    private $alunoetapaNota;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="alunoetapa_carga_horaria", type="time", nullable=true)
     */
    private $alunoetapaCargaHoraria;

    /**
     * @var string
     *
     * @ORM\Column(name="alunoetapa_autor", type="string", nullable=false)
     */
    private $alunoetapaAutor;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     *
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * })
     */
    private $usuario;

    /**
     * @var \Matricula\Entity\AcadperiodoAlunoDisciplina
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoAlunoDisciplina")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="alunodisc_id", referencedColumnName="alunodisc_id")
     * })
     */
    private $alunodisc;

    /**
     * @var AcadperiodoEtapaDiario
     *
     * @ORM\ManyToOne(targetEntity="AcadperiodoEtapaDiario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="diario_id", referencedColumnName="diario_id")
     * })
     */
    private $diario;

    function __construct(array $data)
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getAlunoetapaId()
    {
        return $this->alunoetapaId;
    }

    /**
     * @param int $alunoetapaId
     */
    public function setAlunoetapaId($alunoetapaId)
    {
        $this->alunoetapaId = $alunoetapaId;
    }

    /**
     * @return float
     */
    public function getAlunoetapaNota()
    {
        return $this->alunoetapaNota;
    }

    /**
     * @param float $alunoetapaNota
     */
    public function setAlunoetapaNota($alunoetapaNota)
    {
        $this->alunoetapaNota = $alunoetapaNota;
    }

    /**
     * @return \DateTime
     */
    public function getAlunoetapaCargaHoraria()
    {
        return $this->alunoetapaCargaHoraria;
    }

    /**
     * @param \DateTime $alunoetapaCargaHoraria
     */
    public function setAlunoetapaCargaHoraria($alunoetapaCargaHoraria)
    {
        $this->alunoetapaCargaHoraria = $alunoetapaCargaHoraria;
    }

    /**
     * @return \Matricula\Entity\AcadperiodoAlunoDisciplina
     */
    public function getAlunodisc()
    {
        return $this->alunodisc;
    }

    /**
     * @param \Matricula\Entity\AcadperiodoAlunoDisciplina $alunodisc
     */
    public function setAlunodisc($alunodisc)
    {
        $this->alunodisc = $alunodisc;
    }

    /**
     * @return AcadperiodoEtapaDiario
     */
    public function getDiario()
    {
        return $this->diario;
    }

    /**
     * @param AcadperiodoEtapaDiario $diario
     */
    public function setDiario($diario)
    {
        $this->diario = $diario;
    }

    /**
     * @return string
     */
    public function getAlunoetapaAutor()
    {
        return $this->alunoetapaAutor;
    }

    /**
     * @param string $alunoetapaAutor
     */
    public function setAlunoetapaAutor($alunoetapaAutor)
    {
        $this->alunoetapaAutor = $alunoetapaAutor;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuario
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }

    public function toArray()
    {
        return [
            'alunoetapaId'           => $this->getAlunoetapaId(),
            'alunodisc'              => $this->getAlunodisc()->getAlunodiscId(),
            'diario'                 => $this->getDiario()->getDiarioId(),
            'alunoetapaNota'         => $this->getAlunoetapaNota(),
            'alunoetapaCargaHoraria' => $this->getAlunoetapaCargaHoraria()
        ];
    }

}
