<?php

namespace Professor\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoGrade
 * @ORM\Table(name="acadperiodo__grade", uniqueConstraints={@ORM\UniqueConstraint(name="turma_id_UNIQUE", columns={"turma_id"})}, indexes={@ORM\Index(name="fk_acadperiodo__grade_acadperiodo__grade1_idx", columns={"grade_ciclo"})})
 * @ORM\Entity(repositoryClass="Professor\Entity\Repository\AcadperiodoGrade")
 */
class AcadperiodoGrade
{
    /**
     * @var integer
     *
     * @ORM\Column(name="grade_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $gradeId;

    /**
     * @var \AcadperiodoTurma
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoTurma")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="turma_id", referencedColumnName="turma_id")
     * })
     */
    private $turma;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="grade_inicio", type="date", nullable=false)
     */
    private $gradeInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="grade_fim", type="date", nullable=true)
     */
    private $gradeFim;

    /**
     * @var \AcadperiodoGrade
     *
     * @ORM\ManyToOne(targetEntity="AcadperiodoGrade")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="grade_ciclo", referencedColumnName="grade_id")
     * })
     */
    private $gradeCiclo;


    /**
     * @return int
     */
    public function getGradeId()
    {
        return $this->gradeId;
    }

    /**
     * @param int $gradeId
     */
    public function setGradeId($gradeId)
    {
        $this->gradeId = $gradeId;
    }

    /**
     * @return AcadperiodoTurma
     */
    public function getTurma()
    {
        return $this->turma;
    }

    /**
     * @param AcadperiodoTurma $turma
     */
    public function setTurma($turma)
    {
        $this->turma = $turma;
    }

    /**
     * @return \DateTime
     */
    public function getGradeInicio()
    {
        return $this->gradeInicio;
    }

    /**
     * @param \DateTime $gradeInicio
     */
    public function setGradeInicio($gradeInicio)
    {
        $this->gradeInicio = $gradeInicio;
    }

    /**
     * @return \DateTime
     */
    public function getGradeFim()
    {
        return $this->gradeFim;
    }

    /**
     * @param \DateTime $gradeFim
     */
    public function setGradeFim($gradeFim)
    {
        $this->gradeFim = $gradeFim;
    }

    /**
     * @return \AcadperiodoGrade
     */
    public function getGradeCiclo()
    {
        return $this->gradeCiclo;
    }

    /**
     * @param \AcadperiodoGrade $gradeCiclo
     */
    public function setGradeCiclo($gradeCiclo)
    {
        $this->gradeCiclo = $gradeCiclo;
    }
}
