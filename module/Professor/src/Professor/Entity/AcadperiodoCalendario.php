<?php

namespace Professor\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoCalendario
 *
 * @ORM\Table(name="acadperiodo__calendario")
 * @ORM\Entity
 */
class AcadperiodoCalendario
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="calendario_id", type="date", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $calendarioId;

    /**
     * @var string
     *
     * @ORM\Column(name="calendario_descricao", type="string", length=45, nullable=true)
     */
    private $calendarioDescricao;

    function __construct(array $data)
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return \DateTime
     */
    public function getCalendarioId()
    {
        return $this->calendarioId;
    }

    /**
     * @param \DateTime $calendarioId
     */
    public function setCalendarioId($calendarioId)
    {
        $this->calendarioId = $calendarioId;
    }

    /**
     * @return string
     */
    public function getCalendarioDescricao()
    {
        return $this->calendarioDescricao;
    }

    /**
     * @param string $calendarioDescricao
     */
    public function setCalendarioDescricao($calendarioDescricao)
    {
        $this->calendarioDescricao = $calendarioDescricao;
    }

    public function toArray()
    {
        return array(
            'calendarioId' => $this->getCalendarioId(),
            'calendarioDescricao' => $this->getCalendarioDescricao()
        );
    }

}
