<?php

namespace Professor\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoCalendarioDataEvento
 *
 * @ORM\Table(name="acadperiodo__calendario_data_evento", indexes={@ORM\Index(name="fk_acadgeral__calendario_data_acadgeral__calendario_tipo_ev_idx", columns={"tipoevento_id"}), @ORM\Index(name="fk_acadperiodo__data_evento_acadperiodo__calendario_data1_idx", columns={"data_id"})})
 * @ORM\Entity
 */
class AcadperiodoCalendarioDataEvento
{
    /**
     * @var integer
     *
     * @ORM\Column(name="evento_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $eventoId;

    /**
     * @var string
     *
     * @ORM\Column(name="evento_descricao", type="string", length=45, nullable=true)
     */
    private $eventoDescricao;

    /**
     * @var \AcadperiodoCalendarioTipoEvento
     *
     * @ORM\ManyToOne(targetEntity="AcadperiodoCalendarioTipoEvento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipoevento_id", referencedColumnName="tipoevento_id")
     * })
     */
    private $tipoevento;

    /**
     * @var \AcadperiodoCalendarioData
     *
     * @ORM\ManyToOne(targetEntity="AcadperiodoCalendarioData")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="data_id", referencedColumnName="data_id")
     * })
     */
    private $data;

    function __construct(array $data)
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getEventoId()
    {
        return $this->eventoId;
    }

    /**
     * @param int $eventoId
     */
    public function setEventoId($eventoId)
    {
        $this->eventoId = $eventoId;
    }

    /**
     * @return string
     */
    public function getEventoDescricao()
    {
        return $this->eventoDescricao;
    }

    /**
     * @param string $eventoDescricao
     */
    public function setEventoDescricao($eventoDescricao)
    {
        $this->eventoDescricao = $eventoDescricao;
    }

    /**
     * @return \AcadperiodoCalendarioTipoEvento
     */
    public function getTipoevento()
    {
        return $this->tipoevento;
    }

    /**
     * @param \AcadperiodoCalendarioTipoEvento $tipoevento
     */
    public function setTipoevento($tipoevento)
    {
        $this->tipoevento = $tipoevento;
    }

    /**
     * @return \AcadperiodoCalendarioData
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param \AcadperiodoCalendarioData $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    public function toArray()
    {
        return array(
            'eventoId'        => $this->getEventoId(),
            'eventoDescricao' => $this->getEventoDescricao(),
            'tipoevento'      => $this->getTipoevento(),
            'data'            => $this->getData()
        );
    }

}
