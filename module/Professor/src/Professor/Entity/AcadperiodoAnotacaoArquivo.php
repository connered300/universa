<?php

namespace Professor\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoAnotacaoArquivo
 *
 * @ORM\Table(name="acadperiodo__anotacao_arquivo", indexes={@ORM\Index(name="fk_acadperiodo_anotacao_arquivo_arquivo1_idx", columns={"arq_id"}), @ORM\Index(name="fk_acadperiodo_anotacao_arquivo_acadperiodo_anotacao1_idx", columns={"anot_id"})})
 * @ORM\Entity
 */
class AcadperiodoAnotacaoArquivo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_anot_arquivo", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idAnotArquivo;

    /**
     * @var \AcadperiodoAnotacao
     *
     * @ORM\ManyToOne(targetEntity="AcadperiodoAnotacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="anot_id", referencedColumnName="anot_id")
     * })
     */
    private $anot;

    /**
     * @var GerenciadorArquivos\Entity\Arquivo
     *
     * @ORM\ManyToOne(targetEntity="GerenciadorArquivos\Entity\Arquivo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="arq_id", referencedColumnName="arq_id")
     * })
     */
    private $arq;

    function __construct(array $data)
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getIdAnotArquivo()
    {
        return $this->idAnotArquivo;
    }

    /**
     * @param int $idAnotArquivo
     */
    public function setIdAnotArquivo($idAnotArquivo)
    {
        $this->idAnotArquivo = $idAnotArquivo;
    }

    /**
     * @return AcadperiodoAnotacao
     */
    public function getAnot()
    {
        return $this->anot;
    }

    /**
     * @param AcadperiodoAnotacao $anot
     */
    public function setAnot($anot)
    {
        $this->anot = $anot;
    }

    /**
     * @return \GerenciadorArquivos\Entity\Arquivo
     */
    public function getArq()
    {
        return $this->arq;
    }

    /**
     * @param \GerenciadorArquivos\Entity\Arquivo $arq
     */
    public function setArq($arq)
    {
        $this->arq = $arq;
    }


    public function toArray()
    {
        return array(
            'idAnotArquivo' => $this->getIdAnotArquivo(),
            'anot'          => $this->getAnot()->getAnotId(),
            'arq'           => $this->getArq()->getArqId()
        );
    }
}
