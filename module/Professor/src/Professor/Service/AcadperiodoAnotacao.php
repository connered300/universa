<?php


namespace Professor\Service;

use VersaSpine\Service\AbstractService;

class AcadperiodoAnotacao extends AbstractService
{
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Professor\Entity\AcadperiodoAnotacao');
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {

    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {

    }

    public function save($dados)
    {
        if ($dados['anotId'] !== '') {
            return $this->edita($dados);
        }

        return $this->adicionar($dados);
    }

    public function buscaAnotacaoMes($mes, $docdisc)
    {
        $anotacoes = $this->executeQuery(
            "
            SELECT anot_id FROM acadperiodo__anotacao WHERE docdisc_id = {$docdisc} AND MONTH(anot_data) = {$mes} ORDER BY anot_data ASC
        "
        )->fetchAll();
        $anotObj   = [];
        foreach ($anotacoes as $anot) {
            $anotObj[] = $this->getRepository($this->getEntity())->findOneBy(['anotId' => $anot['anot_id']]);
        }

        return $anotObj;
    }

    public function buscaAnotacoesJson($data)
    {
        $sql = '
            SELECT
              a.*, (
                  SELECT COUNT(*)
                  FROM acadperiodo__anotacao_arquivo aa
                  WHERE aa.anot_id=a.anot_id
              ) as anot_anexo_qtd
            FROM acadperiodo__anotacao a
            WHERE a.docdisc_id = "' . $data["filter"]['docdiscId'] . '"
            ORDER BY a.anot_data DESC
        ';

        $result = parent::paginationDataTablesAjax($sql, $data, null, false);

        return $result;
    }
}