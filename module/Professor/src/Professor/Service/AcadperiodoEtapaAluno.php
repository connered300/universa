<?php

namespace Professor\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class AcadperiodoEtapaAluno
 * @package Professor\Service
 */
class AcadperiodoEtapaAluno extends AbstractService
{
    /**
     * @var null
     */
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Professor\Entity\AcadperiodoEtapaAluno');
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {
    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {
    }

    /**
     * Retorna notas de alunos na docência de acordo com as etapas
     * @param integer $docdiscId
     * @param array   $diarios array de objetos Professor\Entity\AcadperiodoEtapaDiarioExtensao
     * @return array
     */
    public function buscaAlunoEtapa($docdiscId, $diarios)
    {
        $situacaoId = (new \Matricula\Service\AcadgeralSituacao($this->getEm()))->situacoesAtividade();

        $docenteDisciplina = $this->getRepository('Matricula\Entity\AcadperiodoDocenteDisciplina')->findOneBy(
            ['docdiscId' => $docdiscId]
        );
        $alunos            = $this->getRepository('Matricula\Entity\AcadperiodoAlunoDisciplina')->findBy(
            [
                'turma'    => $docenteDisciplina->getTurma(),
                'disc'     => $docenteDisciplina->getDisc(),
                'situacao' => $situacaoId
            ]
        );

        $arr = array();

        foreach ($alunos as $key => $aluno) {

            //Referencia para as etapas de notas
            $etapas = 0;

            //Procura as notas para os alunos nas etapas do semestre letivo
            foreach ($diarios as $diario) {
                $etapaAluno = $this->getRepository($this->getEntity())->findOneBy(
                    [
                        'alunodisc' => $aluno->getAlunodiscId(),
                        'diario'    => $diario->getDiarioId(),
                    ]
                );

                //Se existe o registro da nota para etapa é retorna o valor referente.
                if ($etapaAluno) {
                    $arr[$key]['aluno'] = $aluno;
                    $arr[$key][$etapas] = [
                        'etapas' => $etapaAluno
                    ];
                    // Caso não existe o registro da nota um valor nulo é alocado.
                } else {
                    $arr[$key]['aluno'] = $aluno;
                    $arr[$key][$etapas] = [
                        'etapas' => null,
                    ];
                }

                $etapas = $etapas + 1;
            }
        }

        return $arr;
    }

    public function adicionarMultiplos(array $data)
    {
        $reposityEtapaAluno       = $this->getRepository('Professor\Entity\AcadperiodoEtapaAluno');
        $reposityEtapaDiario      = $this->getRepository('Professor\Entity\AcadperiodoEtapaDiario');
        $reposityAlunoDisiciplina = $this->getRepository('Matricula\Entity\AcadperiodoAlunoDisciplina');
        $serviceAcassoPessoas     = (new \Acesso\Service\AcessoPessoas($this->getEm()));
        $usuarioAutor             = $serviceAcassoPessoas->findOneBy(['id' => $_SESSION['Zend_Auth']['storage']['id']]);
        $this->begin();
        foreach ($data as $aluno) {
            $alunoEtapa = $reposityEtapaAluno->findOneBy(
                ['diario' => $aluno['diario_id'], 'alunodisc' => $aluno['alunodisc_id']]
            );

            if ($alunoEtapa) {
                $alunoEtapa->setAlunoetapaNota(($aluno['NFD'] >= $aluno['NDI']) ? $aluno['NFD'] : $aluno['NDI']);
                $alunoEtapa->setAlunoetapaAutor('Secretaria');
                $alunoEtapa->setUsuario($usuarioAutor);
            } else {
                $alunoEtapa = [
                    'alunoetapaNota'  => ($aluno['NFD'] >= $aluno['NDI']) ? $aluno['NFD'] : $aluno['NDI'],
                    'diario'          => $reposityEtapaDiario->findOneBy(['diarioId' => $aluno['diario_id']]),
                    'alunodisc'       => $reposityAlunoDisiciplina->findOneBy(
                        ['alunodiscId' => $aluno['alunodisc_id']]
                    ),
                    'alunoEtapaAutor' => 'Secretaria',
                    'usuario'         => $usuarioAutor,
                ];
                $alunoEtapa = new \Professor\Entity\AcadperiodoEtapaAluno($alunoEtapa);
            }
            try {
                $this->getEm()->persist($alunoEtapa);
            } catch (\Exception $ex) {
                throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
            }
            unset($alunoEtapa);
        }
        $this->getEm()->flush();
        $this->commit();
        $this->getEm()->clear();
    }

    /**
     * Retorna média de notas de alunos nas etapas
     * @param integer $diarioId
     * @return float|string
     */
    public function buscaMediaEtapa($diarioId)
    {
        $media = $this
            ->executeQuery("SELECT avg(alunoetapa_nota) m FROM acadperiodo__etapa_aluno WHERE diario_id = {$diarioId}")
            ->fetchAll();

        if ($media) {
            return (float)$media[0]['m'];
        } else {
            return '';
        }
    }

    /**
     * Retorna array com nome do alunos, nome da etapa, nota do aluno na etapa e matricula do aluno
     * @param integer $docdiscId
     * @param string  $criterio
     * @param string  $nota
     * @return array|null
     */
    public function buscaNotaAlunosEtapa($docdiscId, $criterio, $nota)
    {
        $select = "
            SELECT pes_nome, etapa_descricao, alunoetapa_nota, alunocurso_id
            FROM acadperiodo__etapas
                NATURAL JOIN acadperiodo__etapa_diario
                NATURAL JOIN acadperiodo__docente_disciplina
                NATURAL JOIN acadperiodo__aluno_disciplina
                NATURAL JOIN acadperiodo__etapa_aluno
                NATURAL JOIN acadperiodo__aluno
                NATURAL JOIN acadgeral__aluno_curso
                NATURAL JOIN acadgeral__aluno
                NATURAL JOIN pessoa
            WHERE
                docdisc_id = {$docdiscId} AND alunoetapa_nota $criterio $nota ";

        return $this->executeQuery($select)->fetchAll();
    }

    /**
     * Retorna array de alunos com notas pendentes
     * @param integer $docdiscId
     * @return array
     */
    public function buscaNotaNulasAlunosEtapa($docdiscId)
    {
        $situacoes = \Matricula\Service\AcadgeralSituacao::situacoesAtividade();
        $param     = array('docdiscId' => $docdiscId);
        $sql       = '
        SELECT
			pes.*,
            docdisc.*,
            count(DISTINCT etapa.etapa_id) AS notas_pendentes,
            group_concat(DISTINCT etapa.etapa_descricao ORDER BY etapa.etapa_ordem SEPARATOR ", ") AS etapas_pendentes
        FROM acadperiodo__docente_disciplina docdisc
            LEFT JOIN acadperiodo__aluno_disciplina alunodisc
                ON alunodisc.disc_id=docdisc.disc_id AND alunodisc.turma_id=docdisc.turma_id
            LEFT JOIN acadperiodo__etapa_aluno etapaaluno
                ON etapaaluno.alunodisc_id=alunodisc.alunodisc_id
            LEFT JOIN acadperiodo__etapa_diario etapadiario
                ON etapadiario.diario_id=etapaaluno.diario_id
            LEFT JOIN acadperiodo__etapas etapa
                ON etapa.etapa_id=etapadiario.etapa_id
            LEFT JOIN acadperiodo__aluno alunoperiodo
                ON alunoperiodo.alunoper_id=alunodisc.alunoper_id
            LEFT JOIN acadgeral__aluno_curso alunocurso
                ON alunocurso.alunocurso_id=alunoperiodo.alunocurso_id
            LEFT JOIN acadgeral__aluno aluno
                ON aluno.aluno_id=alunocurso.aluno_id
            LEFT JOIN pessoa pes
                ON pes.pes_id=aluno.pes_id
        WHERE
            docdisc.docdisc_id = :docdiscId AND
            alunoetapa_nota IS NULL AND
            alunoperiodo.matsituacao_id IN(' . implode(', ', $situacoes) . ') AND
            alunodisc.situacao_id IN(' . implode(', ', $situacoes) . ')
        GROUP BY pes.pes_id, docdisc.docdisc_id
        HAVING count(etapa.etapa_id)>0';

        $stmt         = $this->executeQueryWithParam($sql, $param);
        $arrDocencias = $stmt->fetchAll();

        return $arrDocencias;
    }

    /**
     * Retorna array de notas de alunos cada nota em uma linha
     * @param integer $docdiscId
     * @return array
     */
    public function buscaNotasAlunos($docdiscId, $arrSituacoes = array())
    {
        $conditions = array();
        $param      = array('docdiscId' => $docdiscId);

        if ($arrSituacoes) {
            $conditions[] = 'alunoperiodo.matsituacao_id in(' . implode(', ', $arrSituacoes) . ')';
            $conditions[] = 'alunodisc.situacao_id in(' . implode(', ', $arrSituacoes) . ')';
        }

        $conditions = implode(' AND ', $conditions);

        if ($conditions) {
            $conditions = ' AND ' . $conditions;
        }

        $sql = '
        SELECT * FROM (
            SELECT
                docdisc.docdisc_id,
                etapaaluno.diario_id,
                alunoperiodo.alunocurso_id,
                alunodisc.alunoper_id,
                alunodisc.alunodisc_id,
                etapaaluno.alunoetapa_id,
                format(etapaaluno.alunoetapa_nota,2) AS alunoetapa_nota
            FROM acadperiodo__docente_disciplina docdisc
                LEFT JOIN acadperiodo__aluno_disciplina alunodisc
                    ON alunodisc.disc_id=docdisc.disc_id AND alunodisc.turma_id=docdisc.turma_id
                LEFT JOIN acadperiodo__aluno alunoperiodo
                    ON alunoperiodo.alunoper_id=alunodisc.alunoper_id
                LEFT JOIN acadperiodo__etapa_aluno etapaaluno
                    ON etapaaluno.alunodisc_id=alunodisc.alunodisc_id
            WHERE
                docdisc.docdisc_id = :docdiscId
                ' . $conditions . '
            ORDER BY alunoperiodo.alunoper_id, etapaaluno.alunodisc_id, etapaaluno.diario_id
        ) AS x
        GROUP BY alunoper_id, alunodisc_id, diario_id';

        $stmt         = $this->executeQueryWithParam($sql, $param);
        $arrDocencias = $stmt->fetchAll();

        return $arrDocencias;
    }

    /**
     * Retorna array com informações das disciplinas que o aluno faz em um período
     *
     * @param $alunoper
     * @return array|null
     */
    public function buscaNotasAlunoPorAlunoper($alunoper)
    {
        $serviceCursoConfig          = new \Matricula\Service\AcadCursoConfig($this->getEm());
        $serviceAcadperiodoLetivo    = (new \Matricula\Service\AcadperiodoLetivo($this->getEm()));
        $serviceAcadperiodoAlunoDisc = (new \Matricula\Service\AcadperiodoAlunoDisciplina($this->getEm()));
        $alunoDisciplinas            = $serviceAcadperiodoAlunoDisc->buscaDisciplinasMatriculadasAlunoPeriodo(
            $alunoper
        );

        $disciplinasEtapas = array();

        if ($alunoDisciplinas) {
            for ($i = 0; $i < sizeof($alunoDisciplinas); $i++) {
                $alunoCurso = $alunoDisciplinas[$i]['alunodisc']->getAlunoper()->getAlunocurso();

                $alunoResumo = $this->getRepository("Matricula\Entity\AcadperiodoAlunoResumo")->findOneBy(
                    array(
                        'alunocursoId' => $alunoCurso->getAlunocursoId(),
                        'discId'       => $alunoDisciplinas[$i]['disc'],
                        'alunoper'     => $alunoper
                    ),
                    array('resId' => 'DESC')
                );

                $discplinaFinal = $this->getRepository("Matricula\Entity\AcadperiodoAlunoDisciplinaFinal")->findOneBy(
                    array('alunodisc' => $alunoDisciplinas[$i]['alunodiscId'])
                );

                $objDocente = $this->getRepository('Matricula\Entity\AcadperiodoDocenteDisciplina')->findOneBy(
                    ['turma' => $alunoDisciplinas[$i]['turmaId'], 'disc' => $alunoDisciplinas[$i]['disc']]
                );

                $notaFinalLancada    = "-";
                $notaResumoCalculada = '-';
                $faltas              = 0;

                if ($alunoResumo) {
                    $notaResumoCalculada = $alunoResumo->getResalunoNota();
                    $situacao            = $alunoResumo->getResalunoSituacao();
                    $faltas              = $alunoResumo->getResalunoFaltas();
                } else {
                    $queryFaltas = "SELECT * FROM acadperiodo__frequencia WHERE alunodisc_id = {$alunoDisciplinas[$i]['alunodiscId']};";
                    $findFaltas  = $this->executeQuery($queryFaltas)->fetchAll();
                    $faltas      = count($findFaltas);
                    $situacao    = 'Cursando';
                }

                if ($discplinaFinal) {
                    $notaFinalLancada = $discplinaFinal->getAlunofinalNota();

                    if ($objDocente && is_null($objDocente->getDocdiscDataFechamentoFinal())) {
                        $situacao            = "E.Final";
                        $notaResumoCalculada = '-';
                    }
                }

                $disciplinasEtapas[$i] = [
                    'alunodiscId'       => $alunoDisciplinas[$i]['alunodiscId'],
                    'discNome'          => $alunoDisciplinas[$i]['discNome'],
                    'turmaNome'         => $alunoDisciplinas[$i]['turmaNome'],
                    'turmaId'           => $alunoDisciplinas[$i]['alunodisc']->getTurma()->getTurmaId(),
                    'situaçãoMatricula' => $alunoDisciplinas[$i]['situacao'],
                    'ef'                => $notaResumoCalculada,
                    'mf'                => $notaFinalLancada,
                    'faltas'            => $faltas,
                    'situacao'          => $situacao
                ];

                $notas = $this->getRepository()->findBy(
                    [
                        'alunodisc' => $alunoDisciplinas[$i]['alunodiscId']
                    ],
                    ['diario' => 'asc']
                );

                if ($notas) {
                    $notaTotal = 0;
                    /** @var \Professor\Entity\AcadperiodoEtapaAluno $nota */
                    foreach ($notas as $nota) {
                        $disciplinasEtapas[$i]['notas'][$nota->getDiario()->getEtapa()->getEtapaOrdem()] = [
                            'alunoetapaNota' => round($nota->getAlunoetapaNota(), 2),
                            'alunoetapaId'   => $nota->getAlunoetapaId()
                        ];
                        $disciplinasEtapas[$i]['discId'] = $nota->getAlunodisc()->getdisc()->getDiscId();
                    }

                    foreach ($disciplinasEtapas[$i]['notas'] as $notaCalc) {
                        $notaTotal += $notaCalc['alunoetapaNota'];
                    }

                    /** @var \Matricula\Entity\AcadCursoConfig $objCursoConfig */
                    $objCursoConfig = $serviceCursoConfig->retornaObjCursoConfig(
                        $nota->getAlunodisc()->getTurma()->getMatCur()->getCurso()->getCursoId()
                    );

                    if (!$objCursoConfig) {
                        $error = $serviceCursoConfig->getLastError();
                        $error = $error ? $error : 'Não foi possível localizar a configuração para o curso!';
                        $this->flashMessenger()->addErrorMessage($error);

                        return $this->redirect()->toRoute(
                            $this->getRoute(),
                            array('controller' => $this->getController())
                        );
                    }

                    $cursoconfigMetodo = $objCursoConfig->getCursoconfigMetodo();

                    if ($cursoconfigMetodo == $serviceCursoConfig::METODO_DESEMPENHO_CALCULO_POR_MEDIA) {
                        $notaTotal = number_format(
                            ($notaTotal / $nota->getAlunodisc()->getTurma()->getPer()->getPerEtapas()),
                            2,
                            ".",
                            ""
                        );
                    }

                    $disciplinasEtapas[$i]['notaGeral'] = $notaTotal;

                    $alunoFinal = $this->getRepository(
                        'Matricula\Entity\AcadperiodoAlunoDisciplinaFinal'
                    )->buscaAlunoAlunoFinal($alunoDisciplinas[$i]['alunodiscId']);

                    if (!empty($alunoFinal)) {
                        $disciplinasEtapas[$i]['notaFinalId'] = $alunoFinal->getAlunofinalId();

                        if ($alunoFinal->getAlunofinalNota() != null) {
                            $disciplinasEtapas[$i]['notaFinal'] = $alunoFinal->getAlunofinalNota();
                            $mediaFinal                         = $this->calculamediaAlunoFinal(
                                $notaTotal,
                                $alunoFinal->getAlunofinalNota()
                            );
                        }
                    }
                }

                usort(
                    $disciplinasEtapas,
                    function ($a, $b) {
                        return strcmp($a["discNome"], $b["discNome"]);
                    }
                );

            }

            return $disciplinasEtapas;
        }

        return null;
    }

    /**
     * Efetua edição de notas de alunos em etapas
     *
     * @param array $dados
     * @throws \Exception
     */
    public function editaNotasAluno(array $dados)
    {
        $dataNow = (new \DateTime('now'))->format("Y-m-d H:i:s");

        $this->begin();

        foreach ($dados['notas'] as $nota) {
            $alunodiscId = $nota['alunodiscId'];
            /** @var \Matricula\Entity\AcadperiodoAlunoDisciplina $alunodisc */
            $alunodisc = $this->getReference(intval($alunodiscId), 'Matricula\Entity\AcadperiodoAlunoDisciplina');

            if ($alunodisc) {
                $notaTotal = 0;

                for ($i = 0; $i < sizeof($nota['alunoetapaNota']); $i++) {
                    $notaTotal += $nota['alunoetapaNota'][$i];

                    if (!empty($nota['alunoetapaId'][$i])) {
                        $alunoEtapa = $this->getRepository()->findOneBy(
                            [
                                'alunoetapaId' => intval($nota['alunoetapaId'][$i])
                            ]
                        );

                        if ($alunoEtapa) {
                            if ($alunoEtapa->getAlunoetapaNota() != $nota['alunoetapaNota'][$i]) {
                                $alunoEtapa->setAlunoetapaNota($nota['alunoetapaNota'][$i]);
                                $alunoEtapa->setAlunoetapaAutor('Secretaria');
                                $usuarioBaixa = (
                                $this
                                    ->getRepository('Acesso\Entity\AcessoPessoas', $this->getEm())
                                    ->findOneBy(['id' => $_SESSION['Zend_Auth']['storage']['id']])
                                );

                                if ($usuarioBaixa instanceof \Acesso\Entity\AcessoPessoas) {
                                    $alunoEtapa->setUsuario($usuarioBaixa);
                                }

                                $alunoEtapa->setUsuario('Secretaria');

                                try {
                                    $this->getEm()->persist($alunoEtapa);
                                    $this->getEm()->flush();
                                } catch (\Exception $ex) {
                                    throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
                                }
                            }
                        }
                    }
                }

                $resumo = $this->getRepository('Matricula\Entity\AcadperiodoAlunoResumo')->findOneBy(
                    [
                        'alunoper' => $alunodisc->getAlunoper()->getAlunoperId(),
                        'discId'   => $alunodisc->getDisc()->getDiscId()
                    ]
                );

                if (!empty($nota['notaFinalId'])) {
                    if (!empty($nota['notaFinal'])) {
                        $alunoFinal = $this->getRepository('Matricula\Entity\AcadperiodoAlunoDisciplinaFinal')
                            ->findOneBy(['alunofinalId' => intval($nota['notaFinalId'])]);

                        if ($alunoFinal) {
                            if ($alunoFinal->getAlunofinalNota() != $nota['notaFinal']) {
                                $mediaFinal = $this->calculamediaAlunoFinal($notaTotal, $nota['notaFinal']);
                                $alunoFinal->setAlunofinalNota($nota['notaFinal']);

                                if ($mediaFinal >= 60) {
                                    $situacao = 'APROVADO';
                                } else {
                                    $situacao = 'REPROVADO';
                                }

                                try {
                                    $this->getEm()->persist($alunoFinal);
                                    $this->getEm()->flush();
                                } catch (\Exception $ex) {
                                    throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
                                }

                                if ($resumo) {
                                    $resumo->setResalunoNota($mediaFinal);
                                    $resumo->setResalunoSituacao($situacao);

                                    try {
                                        $this->getEm()->persist($resumo);
                                        $this->getEm()->flush();
                                    } catch (\Exception $ex) {
                                        throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
                                    }
                                } else {
                                    $matrizDisciplina = $this
                                        ->getRepository('Matricula\Entity\AcadperiodoMatrizDisciplina')
                                        ->findOneBy(['disc' => $alunodisc->getDisc()->getDiscId()]);

                                    $frequencia = $this
                                        ->getRepository('Professor\Entity\AcadperiodoFrequencia')
                                        ->findBy(['alunodisc' => $alunodiscId]);

                                    $frequencia = sizeof($frequencia);

                                    $sql = "
                                          INSERT INTO
                                            acadperiodo__aluno_resumo
                                          VALUES (
                                            NULL,
                                            {$alunodisc->getAlunoper()->getAlunocurso()->getAlunocursoId()},
                                            {$alunodisc->getDisc()->getDiscId()},
                                            1,
                                            '{$dataNow}',
                                            '{$situacao}',
                                            {$mediaFinal},
                                            {$frequencia},
                                            '{$dataNow}',
                                            {$matrizDisciplina->getPerDiscChteorica()},
                                            2015,
                                            {$alunodisc->getTurma()->getPer()->getPerSemestre()},
                                            {$alunodisc->getAlunoper()->getAlunoperId()}
                                          )
                                        ";
                                    try {
                                        $this->executeQuery($sql);
                                    } catch (\Exception $ex) {
                                        throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $this->commit();
    }

    /**
     * Calcula media e arredonda para cima
     * @param float $notaDisciplina
     * @param float $notaDisciplinaFinal
     * @return float
     */
    public function calculamediaAlunoFinal($notaDisciplina, $notaDisciplinaFinal)
    {
        return ceil(($notaDisciplinaFinal + $notaDisciplina) / 2);
    }

    /**
     * Alimenta tabelas de resumo e final te TODAS as docências do período letivo atual
     * TODO: renomear o método e efetuar otimizações. Ver implementação:AcadperiodoDocenteDisciplina::efetuaFechamentoNotasParte1
     * @return null
     * @throws \Exception
     */
    public function buscaNotaTotalAlunosDisciplinas()
    {
        $serviceAcadperiodoLetivo    = (new \Matricula\Service\AcadperiodoLetivo($this->getEm()));
        $serviceAcadperiodoAlunoDisc = (new \Matricula\Service\AcadperiodoAlunoDisciplina($this->getEm()));
        $periodoAtual                = $serviceAcadperiodoLetivo->buscaPeriodoAtual();

        $alunos = $this->getRepository('Matricula\Entity\AcadperiodoAluno')->buscaAlunosMatriculadosPeridoAtual();

        $this->begin();

        if ($alunos) {
            /** @var \Matricula\Entity\AcadperiodoAluno $aluno */
            foreach ($alunos as $aluno) {
                $alunoDisciplinas = $serviceAcadperiodoAlunoDisc->buscaDisciplinasAlunoPeriodo($aluno->getAlunoperId());

                if ($alunoDisciplinas) {
                    for ($i = 0; $i < sizeof($alunoDisciplinas); $i++) {
                        if ($alunoDisciplinas[$i] != "Dispensado") {
                            $dataNow = (new \DateTime('now'))->format("Y-m-d H:i:s");

                            $situacao = "";

                            $matrizDisciplina = $this
                                ->getRepository('Matricula\Entity\AcadperiodoMatrizDisciplina')
                                ->findOneBy(['disc' => $alunoDisciplinas[$i]['disc']]);

                            $frequencia = $this
                                ->getRepository('Professor\Entity\AcadperiodoFrequencia')
                                ->findBy(['alunodisc' => $alunoDisciplinas[$i]['alunodiscId']]);

                            $frequencia = sizeof($frequencia);

                            $notas = $this
                                ->getRepository('Professor\Entity\AcadperiodoEtapaAluno')
                                ->findBy(['alunodisc' => $alunoDisciplinas[$i]['alunodiscId']]);

                            if ($notas) {
                                $notaGeral = 0;

                                foreach ($notas as $nota) {
                                    $notaGeral += $nota->getAlunoetapaNota();
                                }

                                if ($notaGeral >= 70) {
                                    $situacao = 'APROVADO';
                                } elseif ($notaGeral >= 40 && $notaGeral < 70) {
                                    $sql = "
                                    INSERT INTO acadperiodo__aluno_disciplina_final
                                    VALUES (NULL,{$alunoDisciplinas[$i]['alunodiscId']},NULL,NULL,NULL)";

                                    try {
                                        $this->executeQuery($sql);
                                        unset($resumo);
                                    } catch (\Exception $ex) {
                                        throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
                                    }
                                } else {
                                    $situacao = 'REPROVADO';
                                }

                                if ($situacao != "") {
                                    $sql = "
                                INSERT INTO acadperiodo__aluno_resumo
                                VALUES (
                                    NULL,
                                    {$aluno->getAlunocurso()->getAlunocursoId()},
                                    {$alunoDisciplinas[$i]['alunodiscId']},
                                    1,
                                    '{$dataNow}',
                                    '{$situacao}',
                                    {$notaGeral},
                                    {$frequencia},
                                    '{$dataNow}',
                                    {$matrizDisciplina->getPerDiscChteorica()},
                                    2015,
                                    {$aluno->getTurma()->getPer()->getPerSemestre()},
                                    {$aluno->getAlunoperId()}
                                )";

                                    try {
                                        $this->executeQuery($sql);
                                    } catch (\Exception $ex) {
                                        throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $this->commit();
        }

        return null;
    }

    /**
     * Retorna faltas de aluno dentro de um período
     * TODO: verificar possibilidade de levar este método para a classe AcadperiodoFrequencia
     * @param integer $alunodiscId
     * @param string  $inicio
     * @param string  $fim
     * @return array
     */
    public function buscaFaltasAluno($alunodiscId, $inicio, $fim)
    {
        $qb = $this->getEm()->createQueryBuilder()
            ->select('o')
            ->from('Professor\Entity\AcadperiodoFrequencia', 'o')
            ->where('o.alunodisc=' . $alunodiscId)
            ->andWhere("o.frequenciaData BETWEEN '{$inicio}' AND '{$fim}' ");

        return $qb->getQuery()->getResult();
    }

    /**
     * Verifica se todos os alunos da docência estão vinculados a etapa,
     * se o registro não existir o mesmo é criado
     * @param \Professor\Entity\AcadperiodoEtapaDiario $objEtapaDiario
     * @return array
     */
    public function registraAlunosEtapaDiario(\Professor\Entity\AcadperiodoEtapaDiario $objEtapaDiario)
    {
        $arrAlunoDisciplina = $this
            ->getRepository('Matricula\Entity\AcadperiodoAlunoDisciplina')
            ->findBy(
                [
                    'turma' => $objEtapaDiario->getDocdisc()->getTurma(),
                    'disc'  => $objEtapaDiario->getDocdisc()->getDisc()
                ]
            );

        $arrEtapaAlunoRetorno  = array();
        $arrSituacoesAtividade = \Matricula\Service\AcadgeralSituacao::situacoesAtividade();

        try {
            $this->getEm()->beginTransaction();

            /* @var $objAlunoDisciplina \Matricula\Entity\AcadperiodoAlunoDisciplina */
            foreach ($arrAlunoDisciplina as $objAlunoDisciplina) {
                $objAlunoper = $objAlunoDisciplina->getAlunoper();

                if (
                    !in_array($objAlunoDisciplina->getSituacao()->getSituacaoId(), $arrSituacoesAtividade) ||
                    !in_array($objAlunoper->getMatsituacao()->getSituacaoId(), $arrSituacoesAtividade)
                ) {
                    continue;
                }

                $arrEtapaAluno = $this
                    ->getRepository()
                    ->findBy(
                        ['alunodisc' => $objAlunoDisciplina, 'diario' => $objEtapaDiario],
                        ['alunoetapaNota' => 'desc']
                    );

                $objEtapaAluno = null;

                if ($arrEtapaAluno) {
                    $objEtapaAluno = $arrEtapaAluno[0];

                    //Trata duplicação de registros na base
                    if (count($arrEtapaAluno) > 1) {
                        for ($i = 1; $i < count($arrEtapaAluno); $i++) {
                            $this->getEm()->remove($arrEtapaAluno[$i]);
                            $this->getEm()->flush();
                        }
                    }
                }

                if (!$objEtapaAluno) {
                    $objEtapaAluno = new \Professor\Entity\AcadperiodoEtapaAluno(array());
                    $objEtapaAluno->setAlunodisc($objAlunoDisciplina);
                    $objEtapaAluno->setDiario($objEtapaDiario);
                    $objEtapaAluno->setAlunoetapaNota(null);
                    $objEtapaAluno->setAlunoetapaAutor('Secretaria');
                }

                $this->getEm()->persist($objEtapaAluno);
                $this->getEm()->flush();

                $arrEtapaAlunoRetorno[$objAlunoDisciplina->getAlunodiscId()] = $objEtapaAluno;
            }

            $this->commit();
        } catch (\Exception $ex) {
            $this->setLastError($ex->getMessage());
        }

        return $arrEtapaAlunoRetorno;
    }

    /**
     * Efetua lançamento de notas de alunos pelo codigo de matricula do mesmo na disciplina e pelo registro de diário
     * @param int   $docdiscId
     * @param array $notas
     * @return bool
     */
    public function salvaRegistrosNotasDiarioAluno($docdiscId, $notas)
    {
        try {
            $this->begin();
            $serviceEtapaDiario = new \Professor\Service\AcadperiodoEtapaDiario($this->getEm());
            $session            = new \Zend\Authentication\Storage\Session();

            $arrDiariosDocencia = $serviceEtapaDiario->diariosDocenciaArray($docdiscId);
            $arrUsuario         = $session->read();
            $usuarioBaixa       = $this
                ->getRepository('Acesso\Entity\AcessoPessoas', $this->getEm())
                ->findOneBy(['id' => $arrUsuario['id']]);
            $dataHoje           = new \DateTime('now');

            $arrDiarios = array();

            foreach ($arrDiariosDocencia as $arrDiario) {
                $arrDiarios[$arrDiario['diario_id']] = $arrDiario;
            }

            foreach ($notas as $diario => $alunos) {
                $arrDiario = $arrDiarios[$diario];

                if (!$arrDiario) {
                    $this->setLastError('Diário inexistente para a docência.');

                    return false;
                }

                $etapaDescricao       = $arrDiario['etapa_descricao'];
                $etapaNotaMaxima      = $arrDiario['etapa_nota_maxima'];
                $diarioDataEnvio      = $arrDiario['diario_data_envio'];
                $diarioDataEnvioFinal = $arrDiario['diario_data_envio_final'];
                $etapaDataInicio      = new \DateTime($arrDiario['etapa_data_inicio']);
                $etapaDataFim         = new \DateTime($arrDiario['etapa_data_fim']);

                if ($arrDiario['etapadiario_extensao_data']) {
                    $etapaDataFim = new \DateTime($arrDiario['etapadiario_extensao_data']);
                }

                $etapaComecou     = $dataHoje->format('Ymd') >= $etapaDataInicio->format('Ymd');
                $etapaNaoTerminou = $dataHoje->format('Ymd') > $etapaDataFim->format('Ymd');
                $etapaBloqueada   = ($etapaComecou && $etapaNaoTerminou);

                if ($diarioDataEnvio || $diarioDataEnvioFinal) {
                    $this->setLastError('Os lançamentos estão bloqueados para a docência.');

                    return false;
                }

                foreach ($alunos as $alunoDisc => $nota) {
                    $nota          = (string)$nota == "" ? null : $nota;
                    $arrAlunoEtapa = $this
                        ->getRepository()
                        ->findBy(
                            ['diario' => $diario, 'alunodisc' => $alunoDisc],
                            ['alunoetapaNota' => 'desc']
                        );
                    $alunoEtapa    = null;

                    if ($arrAlunoEtapa) {
                        $alunoEtapa = $arrAlunoEtapa[0];

                        //Trata duplicação de registros na base
                        if (count($arrAlunoEtapa) > 1) {
                            for ($i = 1; $i < count($arrAlunoEtapa); $i++) {
                                $this->getEm()->remove($arrAlunoEtapa[$i]);
                                $this->getEm()->flush();
                            }
                        }
                    }

                    if (!$alunoEtapa) {
                        $alunoEtapa         = new \Professor\Entity\AcadperiodoEtapaAluno(array());
                        $objEtapaDiario     = $this
                            ->getRepository('Professor\Entity\AcadperiodoEtapaDiario')
                            ->find($diario);
                        $objAlunoDisciplina = $this
                            ->getRepository('Matricula\Entity\AcadperiodoAlunoDisciplina')
                            ->find($alunoDisc);
                        $alunoEtapa->setAlunodisc($objAlunoDisciplina);
                        $alunoEtapa->setDiario($objEtapaDiario);
                    }

                    if ($nota > $etapaNotaMaxima) {
                        $this->setLastError(
                            'A nota máxima para "' . $etapaDescricao . ' é de ' . $etapaNotaMaxima . '.'
                        );

                        return false;
                    }

                    if ($alunoEtapa->getAlunoetapaNota() !== $nota) {
                        if ($etapaBloqueada) {
                            $this->setLastError(
                                'Os lançamentos estão bloqueados para "' . $etapaDescricao . '".'
                            );

                            return false;
                        }

                        $alunoEtapa->setAlunoetapaNota($nota === "" || $nota === null ? null : number_format($nota, 2));
                        $alunoEtapa->setAlunoetapaAutor('Secretaria');

                        if ($usuarioBaixa instanceof \Acesso\Entity\AcessoPessoas) {
                            $alunoEtapa->setUsuario($usuarioBaixa);
                        }

                        $this->getEm()->persist($alunoEtapa);
                        $this->getEm()->flush();
                    }
                }
            }

            $this->commit();

            return true;
        } catch (\Exception $ex) {
            $this->setLastError('Entre em contato com o suporte.');
        }

        return false;
    }

    /**
     * Método para buscar informações de etapas de um aluno da disciplina
     * @param $alunoDiscId : ID do vinculo de aluno na disciplina
     * @param $arrEtapas : array de etapas que devem ser buscadas
     */
    public function findNotasAlunoEtapas($alunoDiscId, array $arrEtapas)
    {
        if (!$alunoDiscId) {
            return array();
        }
        $qb = $this->getEm()->createQueryBuilder();

        $query = $qb->select(array("t1"))
            ->from('Professor\Entity\AcadperiodoEtapaAluno', 't1')
            ->join('Professor\Entity\AcadperiodoEtapaDiario', 't2', 'WITH', 't1.diario = t2.diarioId')
            ->join('Professor\Entity\AcadperiodoEtapas', 't3', 'WITH', 't3.etapaId = t2.etapa')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->eq('t1.alunodisc', ':alunoDisc'),
                    $qb->expr()->in('t3.etapaOrdem ', ':etapas')
                )
            )
            ->setParameter('alunoDisc', $alunoDiscId)
            ->setParameter('etapas', $arrEtapas)
            ->getQuery();

        $etapasAlunoDisciplina = $query->getResult();

        return $etapasAlunoDisciplina;
    }
}
