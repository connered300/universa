<?php


namespace Professor\Service;

use VersaSpine\Service\AbstractService;

class AcadperiodoEtapas extends AbstractService
{
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Professor\Entity\AcadperiodoEtapas');
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {

    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    public function pesquisaForJson($criterios)
    {
        $sql = "SELECT * FROM acadperiodo__etapas
                WHERE ";

        $params = [];
        $where  = [];
        if($criterios['perId']){
            $params['per_id'] = $criterios['perId'];
            $where[] = " per_id = :per_id ";
        }
        if($criterios['q']){
            $params['etapa_nome'] = "%{$criterios['q']}";
            $where[] = " etapa_nome = :etapa_nome ";
        }

        $sql .= implode(" AND ", $where);
        $sql .= " ORDER BY etapa_ordem";

        return $this->executeQueryWithParam($sql, $params);
    }

    public function retornaEtapasPeriodoLetivo($perId)
    {
        $arrEtapas = $this->getRepository()->findBy(
            ['per' => $perId],
            ['etapaOrdem' => 'asc']
        );

        return $arrEtapas ? $arrEtapas : array();
    }

}