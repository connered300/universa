<?php

namespace Professor\Service;

use VersaSpine\Service\AbstractService;

/**
 *
 */
class AcadperiodoEtapaDiarioExtensao extends AbstractService
{
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Professor\Entity\AcadperiodoEtapaDiarioExtensao');
    }

    protected function valida($dados)
    {
        $arrValidate = array();

        if (!$dados['diario']) {
            $arrValidate[] = "Não existe um registro de Diário para o Docente nessa Disciplina. Entre em contato com o suporte para averiguar tal situação";
        }

        if (!$dados['etapadiarioExtensaoData']) {
            $arrValidate[] = "Informe a data de extensão da entrega";
        }

        return $arrValidate;
    }

    protected function pesquisaForJson($criterios)
    {
    }

    public function save($data)
    {
        $validate = $this->valida($data);

        if (!empty($validate)) {
            return $validate;
        }

        if ($data['etapadiarioExtensaoData'] && is_string($data['etapadiarioExtensaoData'])) {
            $data['etapadiarioExtensaoData'] = parent::formatDateAmericano($data['etapadiarioExtensaoData']);
            $data['etapadiarioExtensaoData'] = new \DateTime($data['etapadiarioExtensaoData']);
        }

        if (is_numeric($data['diario'])) {
            $data['diario'] = $this->getReference($data['diario'], "Professor\\Entity\\AcadperiodoEtapaDiario");
        }

        $objExtensaoDiario = $data['etapadiarioExtensaoId'];

        if ($objExtensaoDiario) {
            if (is_numeric($objExtensaoDiario)) {
                $objExtensaoDiario = $this
                    ->getRepository('Professor\Entity\AcadperiodoEtapaDiarioExtensao')
                    ->find($objExtensaoDiario);
            }

            if ($objExtensaoDiario->getEtapadiarioExtensaoData() == $data['etapadiarioExtensaoData']) {
                $objExtensaoDiario->setDiario($data['diario']);
                $objExtensaoDiario->setEtapadiarioExtensaoData($data['etapadiarioExtensaoData']);
                $objExtensaoDiario->setEtapadiarioExtensaoObservacao($data['etapadiarioExtensaoObservacao']);

                try {
                    $this->getEm()->persist($objExtensaoDiario);
                    $this->getEm()->flush();
                } catch (\Exception $ex) {
                    return array('Ocorreu um erro na solicitação. Caso persista contate o suporte.');
                }

                return $objExtensaoDiario;
            }
        }

        $objExtensaoDiario = new \Professor\Entity\AcadperiodoEtapaDiarioExtensao();

        $objExtensaoDiario->setDiario($data['diario']);
        $objExtensaoDiario->setEtapadiarioExtensaoData($data['etapadiarioExtensaoData']);
        $objExtensaoDiario->setEtapadiarioExtensaoObservacao($data['etapadiarioExtensaoObservacao']);
        $objExtensaoDiario->setEtapadiarioExtensaoJustificativa($data['etapadiarioExtensaoJustificativa']);

        $objExtensaoDiario->setEtapadiarioExtensaoDataAlteracao(new \DateTime('now'));

        $dataSolicitacao = new \DateTime('now');

        if ($data['etapadiarioExtensaoDataSolicitacao']) {
            if ($data['etapadiarioExtensaoDataSolicitacao'] && is_string($data['etapadiarioExtensaoDataSolicitacao'])) {
                $data['etapadiarioExtensaoDataSolicitacao'] = parent::formatDateAmericano(
                    $data['etapadiarioExtensaoDataSolicitacao']
                );
            }

            $dataSolicitacao = new \DateTime($data['etapadiarioExtensaoDataSolicitacao']);
        }

        $objExtensaoDiario->setEtapadiarioExtensaoDataSolicitacao($dataSolicitacao);

        $session = new \Zend\Session\Container("Zend_Auth");
        $user    = $this->getReference($session->storage['id'], 'Acesso\Entity\AcessoPessoas');

        $objExtensaoDiario->setUsuarioLiberacao($user);

        try {
            $this->getEm()->persist($objExtensaoDiario);
            $this->getEm()->flush();
        } catch (\Exception $ex) {
            return array('Ocorreu um erro na solicitação. Caso persista contate o suporte.');
        }

        return $objExtensaoDiario;
    }
}
