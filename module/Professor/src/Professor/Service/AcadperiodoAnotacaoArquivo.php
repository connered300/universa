<?php


namespace Professor\Service;

use VersaSpine\Service\AbstractService;

class AcadperiodoAnotacaoArquivo extends AbstractService
{
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Professor\Entity\AcadperiodoAnotacaoArquivo');
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {

    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {

    }

    public function listaAnexosAnotacao($anotId)
    {
        $arrAnexos = $this->getRepository()->findBy(['anot' => $anotId]);
        $arrResult = array();

        foreach ($arrAnexos as $objAnexo) {
            $arrResult[] = array(
                'id'       => $objAnexo->getIdAnotArquivo(),
                'anotId'   => $objAnexo->getAnot()->getAnotId(),
                'arqChave' => $objAnexo->getArq()->getArqChave(),
                'arqId'    => $objAnexo->getArq()->getArqId(),
                'arqNome'  => $objAnexo->getArq()->getArqNome(),
                'arqTipo'  => $objAnexo->getArq()->getArqTipo(),
            );
        }

        return $arrResult;
    }
}