<?php
namespace Professor\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class AcadperiodoEtapaDiario
 * @package Professor\Service
 */
class AcadperiodoEtapaDiario extends AbstractService
{
    /**
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Professor\Entity\AcadperiodoEtapaDiario');
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {

    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {

    }

    /**
     * Retorna diários da docência
     * @param $docdiscId
     * @return array
     */
    public function buscaDiariosDisciplina($docdiscId)
    {
        return $this->getRepository($this->getEntity())->findBy(['docdisc' => $docdiscId]);
    }

    /**
     * Busca diario das disciplinas e verificar se há extensão da data de entrega,
     * se houver substitui a data de fim da etapa
     *
     * @param integer $docdiscId Id do vinculo de professor e disciplina
     * @return array
     */
    public function buscaDiariosDisciplinaExtensao($docdiscId)
    {
        $repoEtapaDiarioExtensao = $this->getRepository('Professor\Entity\AcadperiodoEtapaDiarioExtensao');
        $etapaDiariosDisciplina  = $this->getRepository($this->getEntity())->findBy(['docdisc' => $docdiscId]);

        foreach ($etapaDiariosDisciplina as $pos => $etpDiario) {
            $etapaDiarioExtensao = $repoEtapaDiarioExtensao->findOneBy(
                ['diario' => $etpDiario->getDiarioId()],
                ['etapadiarioExtensaoId' => 'DESC']
            );

            if ($etapaDiarioExtensao) {
                $dataExtensao = $etapaDiarioExtensao->getEtapadiarioExtensaoData();
                $etapaDiariosDisciplina[$pos]->getEtapa()->setEtapaDataFim($dataExtensao);
            }
        }

        return $etapaDiariosDisciplina;
    }

    /**
     * Verifica se todos os diários da docência estão cadastrados,
     * se o registro não existir o mesmo é criado
     * @param \Matricula\Entity\AcadperiodoDocenteDisciplina $objDocenteDisciplina
     * @return array
     */
    public function registraEtapaDiario(\Matricula\Entity\AcadperiodoDocenteDisciplina $objDocenteDisciplina)
    {
        $arrEtapas = $this
            ->getRepository('Matricula\Entity\AcadperiodoEtapas')
            ->findBy(
                ['per' => $objDocenteDisciplina->getTurma()->getPer()],
                ['etapaOrdem' => 'asc']
            );

        $arrEtapaDiarioRetorno = array();

        try {
            $this->getEm()->beginTransaction();

            /* @var $objEtapa \Matricula\Entity\AcadperiodoEtapas */
            foreach ($arrEtapas as $objEtapa) {
                $arrEtapaDiario = $this
                    ->getRepository()
                    ->findBy(
                        ['docdisc' => $objDocenteDisciplina, 'etapa' => $objEtapa],
                        ['diarioId' => 'asc']
                    );

                $objEtapaDiario = null;

                if ($arrEtapaDiario) {
                    $objEtapaDiario = $arrEtapaDiario[0];

                    //Trata duplicação de registros na base
                    if (count($arrEtapaDiario) > 1) {
                        for ($i = 1; $i < count($arrEtapaDiario); $i++) {
                            $this->getEm()->remove($arrEtapaDiario[$i]);
                            $this->getEm()->flush();
                        }
                    }
                }

                if (!$objEtapaDiario) {
                    $objEtapaDiario = new \Professor\Entity\AcadperiodoEtapaDiario(array());
                    $objEtapaDiario->setDocdisc($objDocenteDisciplina);
                    $objEtapaDiario->setEtapa($objEtapa);

                    $this->getEm()->persist($objEtapaDiario);
                    $this->getEm()->flush($objEtapaDiario);
                }

                $arrEtapaDiarioRetorno[$objEtapa->getEtapaOrdem()] = $objEtapaDiario;
            }

            $this->commit();
        } catch (\Exception $ex) {
            $this->setLastError($ex->getMessage());
        }

        return $arrEtapaDiarioRetorno;
    }

    /**
     * Retorna array com informações de etapa, diário e extensão de etrega dos mesmos
     * @param integer $docdiscId
     * @return array|null
     */
    public function diariosDocenciaArray($docdiscId)
    {
        $param = array('docdiscId' => $docdiscId);
        $sql   = '
        SELECT
            docdisc.docdisc_id, docdisc.disc_id,
            diario_data_envio, diario_data_envio_final,
            turma.turma_id,turma.turma_nome,
            config.cursoconfig_nota_max,
            diario.diario_id,
            etapa.etapa_id, etapa.per_id, etapa.etapa_descricao,
            etapa.etapa_data_inicio, etapa.etapa_data_fim,
            etapa.etapa_ordem, etapa.etapa_percentagem,
            ((config.cursoconfig_nota_max / 100)  * etapa.etapa_percentagem ) etapa_nota_maxima,
            COALESCE((
                SELECT etapadiario_extensao_data from acadperiodo__etapa_diario_extensao
                WHERE diario_id = diario.diario_id
                ORDER BY etapadiario_extensao_id DESC
                LIMIT 1
            ), "") as etapadiario_extensao_data
        FROM acadperiodo__docente_disciplina docdisc
            INNER JOIN acadperiodo__turma turma ON turma.turma_id=docdisc.turma_id
            INNER JOIN campus_curso ON campus_curso.cursocampus_id=turma.cursocampus_id
            INNER JOIN acad_curso_config config ON config.curso_id=campus_curso.curso_id
            LEFT JOIN acadperiodo__etapa_diario diario ON diario.docdisc_id=docdisc.docdisc_id
            LEFT JOIN acadperiodo__etapas etapa ON etapa.etapa_id=diario.etapa_id
        WHERE docdisc.docdisc_id = :docdiscId
        GROUP BY etapa.etapa_id
        ORDER BY etapa.etapa_ordem ASC';

        $stmt         = $this->executeQueryWithParam($sql, $param);
        $arrDocencias = $stmt->fetchAll();

        return $arrDocencias;
    }

    /**
     * Retorna valor máximo da nota do aluno no período
     * TODO: mover método para a classe AcadCursoConfig
     * @param $docdiscId
     * @return mixed
     */
    public function buscaPontuacaoCurso($docdiscId)
    {
        $query = "
        SELECT
           cursoconfig_nota_max notaMax
        FROM
           acadperiodo__docente_disciplina docdisc
        INNER JOIN
           acadgeral__disciplina disciplina ON disciplina.disc_id = docdisc.disc_id
        INNER JOIN
           acadgeral__disciplina_curso disccurso ON disciplina.disc_id = disccurso.disc_id
        INNER JOIN
           acad_curso_config configcurso ON configcurso.curso_id = disccurso.curso_id
        WHERE
           docdisc.docdisc_id = {$docdiscId}";
        $tmp   = $this->executeQuery($query)->fetch();

        return $tmp[0]['notaMax'];
    }

    /**
     * Retorna informações de configuração de notas e frequencia do curso
     * TODO: mover método para a classe AcadCursoConfig
     * @param $docdiscId
     * @return mixed
     */
    public function buscaConfigNota($docdiscId)
    {
        $query = "
        SELECT 
            cursoconfig_nota_max notaMax,
            cursoconfig_nota_min notaMin,
            cursoconfig_nota_final notaFinal,
            cursoconfig_freq_min freqMin
 
        FROM
           acadperiodo__docente_disciplina docdisc
           INNER JOIN acadperiodo__turma USING(turma_id)
           INNER JOIN campus_curso USING(cursocampus_id)
           INNER JOIN acad_curso_config configcurso USING(curso_id)
           
        WHERE
           docdisc.docdisc_id = {$docdiscId}";

        $tmp   = $this->executeQuery($query)->fetch();

        return $tmp;
    }
}