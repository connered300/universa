<?php


namespace Professor\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class AcadperiodoFrequencia
 * @package Professor\Service
 */
class AcadperiodoFrequencia extends AbstractService
{
    /**
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Professor\Entity\AcadperiodoFrequencia');
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {

    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {

    }

    /**
     * Retorna array de frequencia de alunos em uma docência
     * @param \DateTime $dia
     * @param integer   $docdiscId
     * @return array|null
     */
    public function buscaFrequenciaPorDia(\DateTime $dia, $docdiscId)
    {
        $data   = $dia->format('Y-m-d');
        $faltas = $this->executeQuery(
            "SELECT * FROM acadperiodo__frequencia
             WHERE docdisc_id = '{$docdiscId}' AND
             DATE(frequencia_data) = DATE('{$data}')"
        )->fetchAll();

        return $faltas;
    }

    /**
     * Efetua lançamento de ausencia de aluno em uma aula da docência
     * @param array $ausencias
     * @param       $datas
     * @param       $docdisc
     * @throws \Exception
     */
    public function insereAusencia($ausencias, $datas, $docdisc)
    {
        foreach ($ausencias as $ausencia) {
            $ausencia              = explode('$$', $ausencia);
            $acadPeriodoFrequencia = [
                'frequenciaId'   => '',
                'alunodisc'      => $ausencia[0],
                'frequenciaData' => new \DateTime($ausencia[2], new \DateTimeZone('America/Sao_Paulo')),
                'docdisc'        => $ausencia[3]
            ];
            $ausenciaNoDia         = $this->getRepository($this->getEntity())->findOneBy(
                [
                    'alunodisc'      => $ausencia[0],
                    'docdisc'        => $ausencia[3],
                    'frequenciaData' => $acadPeriodoFrequencia['frequenciaData']
                ]
            );

            if (!$ausenciaNoDia) {
                parent::adicionar($acadPeriodoFrequencia);
            }
        }
    }

    /**
     * Remove faltas de um aluno pela data e docência
     * @param integer $alunodiscId
     * @param integer $docdisc
     * @param string  $frequenciaData
     */
    public function removeFalta($alunodiscId, $docdisc, $frequenciaData)
    {
        $ret = $this->getRepository($this->getEntity())->findOneBy(
            [
                'alunodisc'      => $alunodiscId,
                'docdisc'        => $docdisc,
                'frequenciaData' => new \DateTime($frequenciaData, new \DateTimeZone('America/Sao_Paulo')),
            ]
        );

        if ($ret) {
            $this->exec("DELETE FROM acadperiodo__frequencia WHERE frequencia_id = {$ret->getFrequenciaId()}");
        }
    }

    /**
     * Retorna array de frequencia de uma docência
     * Caso o parâmetro $apenasFrequencia seja falso,
     * é retornado em cada linha do array um objeto AcadperiodoAlunoDisciplina
     * e um objeto AcadperiodoMatrizDisciplina
     * @param integer    $alunodiscId
     * @param bool|false $apenasFrequencia
     * @return array
     */
    public function buscaFaltasDisciplina($alunodiscId, $apenasFrequencia = false)
    {
        $sql = '
            SELECT alunodisc_id, count(alunodisc_id) quant
            FROM acadperiodo__frequencia
            WHERE alunodisc_id = :alunodisc_id';

        $arrFaltas = $this->executeQueryWithParam($sql, array('alunodisc_id' => $alunodiscId))->fetchAll();
        $arrFaltas = $arrFaltas[0];

        if ($apenasFrequencia) {
            return $arrFaltas;
        }

        if (!$arrFaltas['alunodisc_id']) {
            $arrFaltas['alunodisc_id'] = $alunodiscId;
        }

        $arrFaltas['alunodisc_id'] = $this
            ->getRepository('Matricula\Entity\AcadperiodoAlunoDisciplina')
            ->findOneBy(['alunodiscId' => $arrFaltas['alunodisc_id']]);

        $arrFaltas['matriz_disciplina'] = $this
            ->getRepository('Matricula\Entity\AcadperiodoMatrizDisciplina')
            ->findOneBy(
                [
                    'matCur' => $arrFaltas['alunodisc_id']->getTurma()->getMatCur(),
                    'disc'   => $arrFaltas['alunodisc_id']->getDisc()
                ]
            );

        return $arrFaltas;
    }

    /**
     * Retorna array de frequencia de alunos em uma docência em um mês
     * @param integer $mes
     * @param integer $docdisc
     * @return array|null
     */
    public function buscaFaltaDisciplinaMes($mes, $docdisc)
    {
        return $this->executeQuery(
            "SELECT
                acadperiodo__aluno.alunocurso_id,
                DAY(frequencia_data) AS dia,
                TIME(frequencia_data) AS horas
             FROM acadperiodo__frequencia
                NATURAL JOIN acadperiodo__aluno_disciplina
                INNER JOIN acadperiodo__aluno ON acadperiodo__aluno.alunoper_id = acadperiodo__aluno_disciplina.alunoper_id
             WHERE month(frequencia_data) = {$mes} AND docdisc_id = {$docdisc} "
        )->fetchAll();
    }

    /**
     * Retorna faltas do aluno na disciplina
     * @param integer $alunoDisc
     * @return array|integer
     */
    public function buscaFaltasAlunoPorDisicplina($alunoDisc)
    {
        $query = "
        SELECT
            DATE_FORMAT(frequencia_data, '%d/%m/%Y') AS diaFalta,
            count(*) as total
        FROM acadperiodo__frequencia
        WHERE alunodisc_id = {$alunoDisc}
        GROUP BY DATE (frequencia_data)";

        $result = $this->executeQuery($query);

        if ($result->rowCount() > 0) {
            return $result->fetchAll();
        }

        return 0;
    }

    /**
     * Retorna faltas do aluno na disciplina
     * @param integer $alunoDisc
     * @return integer
     */
    public function buscaNumeroFaltasAluno($alunoDisc)
    {
        $query = "
        SELECT count(*) as total
        FROM acadperiodo__frequencia
        WHERE alunodisc_id = {$alunoDisc}";

        $result   = $this->executeQuery($query);
        $arrFalta = $result->fetch();

        return $arrFalta['total'];
    }
}