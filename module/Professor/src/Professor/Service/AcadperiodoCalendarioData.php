<?php


namespace Professor\Service;

use VersaSpine\Service\AbstractService;

class AcadperiodoCalendarioData extends AbstractService
{
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Professor\Entity\AcadperiodoCalendarioData');
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {

    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {

    }

    /**
     * @param string $strDate date string in format Y-m-d
     * @return array
     */
    public function feriadosDepoisDaData($strDate)
    {
        $arrFeriados        = array();
        $objQB              = $this
            ->getRepository()
            ->createQueryBuilder('d')
            ->where("d.dataLetivo = 'Não'")
            ->andWhere("d.dataId >= :dataId")
            ->setParameter('dataId', $strDate);
        $arrCalendarioDatas = $objQB->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);

        foreach ($arrCalendarioDatas as $arrCalendarioData) {
            $arrFeriados[] = $arrCalendarioData['dataId']->format('Y-m-d');
        }

        return $arrFeriados;
    }
}