<?php
namespace Professor\Service;

use VersaSpine\Service\AbstractService;

class AcadperiodoDisciplinaGrade extends AbstractService
{
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Professor\Entity\AcadperiodoDisciplinaGrade');
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {
    }

    /**lta
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {
    }

    public function buscaDatasSemana($numSemana, $dias = array())
    {
        $traducaoDias = [
            'Segunda' => 1,
            'Terca'   => 2,
            'Quarta'  => 3,
            'Quinta'  => 4,
            'Sexta'   => 5,
            'Sabado'  => 6,
            'Domingo' => 7,
        ];
        $semana       = new \DateTime('now', new \DateTimeZone('America/Sao_Paulo'));
        $semana       = $semana->setISODate($semana->format('Y'), $numSemana);
        $datas        = array();
        foreach ($dias as $dia) {
            $diaHora = (new \DateTime('now', new \DateTimeZone('America/Sao_Paulo')))->setISODate(
                $semana->format('Y'),
                $numSemana,
                $traducaoDias[$dia['nome']]
            );
            $hora    = explode(':', $dia['hora']);
            $diaHora->setTime($hora[0], $hora[1], $hora[2]);
            $datas[] = $diaHora;
        }

        return $datas;
    }

    public function buscaDiasDeAula($gradeId, $disciplinaId)
    {
        $dias  = $this->executeQuery(
            "
            SELECT
             gradedisc_dia, gradedisc_hora
            FROM
             acadperiodo__disciplina_grade
            WHERE
             grade_id = {$gradeId}
            AND
             disc_id = {$disciplinaId}
            GROUP BY gradedisc_dia
        "
        )->fetchAll();
        $nomes = array();
        foreach ($dias as $dia) {
            $nomes[] = [
                'nome' => $dia['gradedisc_dia'],
                'hora' => $dia['gradedisc_hora']
            ];
        }

        return $nomes;
    }

    public function buscaPrimeiroDia($dia, $mes, $ano)
    {
        $traducaoDias = [
            'Segunda' => '0',
            'Terca' => '1',
            'Quarta' => '2',
            'Quinta' => '3',
            'Sexta' => '4',
            'Sabado' => '5',
            'Domingo' => '6'
        ];

        $dayNumber = $traducaoDias[$dia];

        for ($i = 1; $i < 8; $i++) {
            $date          = $ano . '/' . $mes . '/' . $i;
            $weekDayNumber = $this->executeQuery("SELECT WEEKDAY('{$date}')")->fetch();
            if ($weekDayNumber["WEEKDAY('{$date}')"] == $dayNumber) {
                break;
            }
        }
        $dia = [
            'diaSemana' => $dia,
            'diaMes'    => $date,
        ];

        return $dia;
    }

    public function buscaTodosDiasMes($param)
    {
        $dateDay = explode('/', $param['date']);

        $ano = $dateDay[0];

        $perAtual = [
            'mesInicio'  => is_bool($param['objPeriodo']->getPerDataInicio()) ? " " : date_format($param['objPeriodo']->getPerDataInicio(), 'm'),
            'anoInicio'  => is_bool($param['objPeriodo']->getPerDataInicio()) ? " " : date_format($param['objPeriodo']->getPerDataInicio(), 'Y'),
            'dataInicio' => is_bool($param['objPeriodo']->getPerDataInicio()) ? " "  :  date_format($param['objPeriodo']->getPerDataInicio(), 'd') ,
            'dataFim'    => is_bool($param['objPeriodo']->getPerDataFim())    ? " "  : date_format($param['objPeriodo']->getPerDataFim(), 'd'),
            'mesFim'     => is_bool($param['objPeriodo']->getPerDataFim())    ? " " : date_format($param['objPeriodo']->getPerDataFim(), 'm') ,
            'anoFim'     => is_bool($param['objPeriodo']->getPerDataFim())    ? " " : date_format($param['objPeriodo']->getPerDataFim(), 'Y') ,
        ];

        if (!$ano && $perAtual['anoInicio']) {
            $ano = $perAtual['anoInicio'];
        }

        if (!is_null($param['date_inicio'])) {
            $perAtual['dataInicio'] = explode('/', $param['date_inicio'])[2];
            $perAtual['mesInicio']  = explode('/', $param['date_inicio'])[1];
        }

        if (!is_null($param['date_fim'])) {
            $perAtual['dataFim'] = explode('/', $param['date_fim'])[2];
            $perAtual['mesFim']  = explode('/', $param['date_fim'])[1];
        }

        if (($dateDay[1] == 4) or ($dateDay[1] == 6) or ($dateDay[1] == 9) or ($dateDay[1] == 11)) {
            $diasMes = 30;
        } elseif (($ano % 4) == 0 and ($ano % 100) != 0 and $dateDay[1] == 2) {
            $diasMes = 29;
        } elseif (($ano % 4) != 0 and ($dateDay[1] == 2)) {
            $diasMes = 28;
        } else {
            $diasMes = 31;
        }
        $feriados = $this->executeQuery(
            "SELECT DAY(data_id) AS dia
            FROM acadperiodo__calendario_data
            WHERE
            data_letivo = 'não'
            AND
            MONTH (data_id) = {$dateDay[1]} AND YEAR(data_id) = {$ano}"
        )->fetchAll();

        $dates = array();

        do {
            $feriado = false;
            $date    = $dateDay[0] . '/' . $dateDay['1'] . '/' . $dateDay[2];

            if (!empty($feriados)) {
                foreach ($feriados as $dias) {
                    if ($dias['dia'] == $dateDay[2]) {
                        $feriado = true;
                        break;
                    }
                }
            }

            if ($feriado == false) {
                if ($perAtual['mesInicio'] <= $dateDay[1] and $perAtual['mesFim'] >= $dateDay[1]) {
                    if ($perAtual['mesFim'] == $dateDay[1]) {
                        if ($perAtual['dataFim'] >= $dateDay[2]) {
                            $dates[][] = $date;
                        }
                    } else {
                        $dates[][] = $date;
                    }
                }
            }

            $dateDay[2] = $dateDay['2'] + 7;
        } while ($dateDay[2] <= $diasMes);

        return $dates;
    }

    public function buscaHorasAula($diaSemana, $grade, $disciplina)
    {
        $query = $this->executeQuery(
            "SELECT *
                                        FROM
                                          acadperiodo__disciplina_grade
                                          WHERE
                                          grade_id = {$grade}
                                          and
                                          disc_id = {$disciplina}
                                          and
                                          gradedisc_dia = '{$diaSemana}' "
        )->fetchAll();

        return $query;
    }
}