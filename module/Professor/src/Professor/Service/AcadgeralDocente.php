<?php


namespace Professor\Service;

use VersaSpine\Service\AbstractService;

class AcadgeralDocente extends AbstractService{
    public function __construct(\Doctrine\ORM\EntityManager $em){
        parent::__construct($em, 'Professor\Entity\AcadgeralDocente');
    }

    protected function valida($dados)
    {

    }

    protected function pesquisaForJson($criterios)
    {

    }

    public function buscaDocentePorUsuario()
    {
        $usuario = ($this->getRepository('Acesso\Entity\AcessoPessoas', $this->getEm())->findOneBy(
            ['id' => $_SESSION['Zend_Auth']['storage']['id']]
        ));

        $pes = $usuario->getPesFisica()->getPes()->getPesId();

        $docente = $this->getRepository()->findOneBy(['pes' => $pes]);

        if($docente){
            return $docente;
        }

        return false;
    }
}
