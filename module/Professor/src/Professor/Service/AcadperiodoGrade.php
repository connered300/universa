<?php


namespace Professor\Service;

use VersaSpine\Service\AbstractService;

class AcadperiodoGrade extends AbstractService
{
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Professor\Entity\AcadperiodoGrade');
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {

    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {

    }

    public function buscaGrade($alunoperId)
    {
        if (!$alunoperId) {
            return [];
        }

        $query = "
        SELECT
            gradedisc_dia, gradedisc_hora, disc_nome
        FROM
            acadperiodo__aluno aluno
        INNER JOIN
            acadperiodo__turma turma ON aluno.turma_id = turma.turma_id
        INNER JOIN
            acadperiodo__grade grade ON grade.turma_id = turma.turma_id
        INNER JOIN
            acadperiodo__disciplina_grade discgrade ON discgrade.grade_id = grade.grade_id
        INNER JOIN
			acadgeral__disciplina disc ON disc.disc_id = discgrade.disc_id
        WHERE grade_fim is null";

        if (is_array($alunoperId)) {
            $arrDisc  = array();
            $arrTurma = array();

            foreach ($alunoperId as $disciplina) {
                $arrDisc[]  = $disciplina['disc'];
                $arrTurma[] = $disciplina['turmaId'];
            }

            $query .= " AND disc.disc_id in (" . implode(", ", $arrDisc) . ")";
            $query .= " AND turma.turma_id in (" . implode(", ", $arrTurma) . ")";
            $query .= " GROUP BY discgrade.disc_grade_id ";
        } else {
            $query .= " AND alunoper_id = " . $alunoperId;
        }

        $query .= " ORDER BY gradedisc_dia, gradedisc_hora";

        $result = $this->executeQuery($query)->fetchAll();

        if ($result) {
            return $result;
        }

        return false;
    }

    public function buscaGradePorAluno($alunoperId)
    {
        return $this->executeQuery("
        SELECT
            gradedisc_dia, gradedisc_hora, disc_nome
        FROM
            acadperiodo__aluno aluno
        INNER JOIN
            acadperiodo__turma turma ON aluno.turma_id = turma.turma_id
        INNER JOIN
            acadperiodo__grade grade ON grade.turma_id = turma.turma_id
        INNER JOIN
            acadperiodo__disciplina_grade discgrade ON discgrade.grade_id = grade.grade_id
        INNER JOIN
			acadgeral__disciplina disc ON disc.disc_id = discgrade.disc_id
        WHERE
            alunoper_id = {$alunoperId}
        ORDER BY
        gradedisc_dia, gradedisc_hora ASC
        ")->fetchAll();
    }

    public function formataGradeHorariaAluno($gradeAluno)
    {
        $gradeTratada = array();

        $gradeTratada['dias'] = array(
            'Segunda' => null,
            'Terca'   => null,
            'Quarta'  => null,
            'Quinta'  => null,
            'Sexta'   => null
        );

        foreach ($gradeAluno as $grade) {
            if (!isset($gradeTratada[$grade['gradedisc_hora']])) {
                $gradeTratada[$grade['gradedisc_hora']] = $gradeTratada['dias'];
            }

            if (!isset($gradeTratada[$grade['gradedisc_hora']][$grade['gradedisc_dia']])) {
                $gradeTratada[$grade['gradedisc_hora']][$grade['gradedisc_dia']] = $grade['disc_nome'];
            }
        }

        ksort($gradeTratada);

        return $gradeTratada;
    }

    /**
     * @param array ()
     */

    public function buscaUltimaHorariaValida($param)
    {
        $query    = <<<SQL
                SELECT
                    acadperiodo__grade.*
                FROM
                    acadperiodo__grade
                        INNER JOIN
                    acadperiodo__disciplina_grade USING(grade_id)
                WHERE
                    turma_id = :turma_id  AND disc_id = :disc_id
SQL;
        $gradeVal = $this->executeQueryWithParam($query, $param)->fetch();

        return $gradeVal;

    }
}