<?php


namespace Professor\ViewHelper;

use Zend\Navigation;
use Zend\View\Helper\Navigation\AbstractHelper;

class TrocaSenha extends AbstractHelper{
    /**
     * Armazenará o conteúdo da viewhelper
     * */
    private $content;

    private $script;

    public function __construct()
    {
        $this->setContent("
        <div class='page-section'>
            <div class='panel panel-default'>
                <div class='panel-body'>
                    <form name='trocaSenha' id='trocaSenha'>
                        <div class='row'>
                            <div class='form-group form-control-material static required'>
                                <input class='form-control' name='senhaAtual' id='senhaAtualTroca' placeholder='Senha atual' type='password'><span class='ma-form-highlight'></span><span class='ma-form-bar'></span>
                                <label for='senhaAtualTroca'>Digite sua senha</label>
                            </div>
                        </div>
                        <div class='row'>
                            <div class='form-group form-control-material static required'>
                                <input class='form-control' name='novaSenha' id='novaSenhaTroca' placeholder='Nova senha' type='password'><span class='ma-form-highlight'></span><span class='ma-form-bar'></span>
                                <label for='novaSenhaTroca'>Digite a nova senha</label>
                            </div>
                        </div>
                        <div class='row'>
                            <div class='form-group form-control-material static required'>
                                <input class='form-control' name='confereNovaSenha' id='confereNovaSenhaTroca' placeholder='Repetir nova senha' type='password'><span class='ma-form-highlight'></span><span class='ma-form-bar'></span>
                                <label for='confereNovaSenhaTroca'>Repita a nova senha</label>
                            </div>
                        </div>
                        <button type='button' class='btn btn-primary' id='sendNewPassword'>Alterar</button>
                    </form>
                </div>
            </div>
        </div>
        ");

        $this->setScript("
        <script>
            var elemento = document.getElementById('sendNewPassword');
            elemento.addEventListener('click', function () {
                var novaSenha     = $('#novaSenhaTroca').val();
                var novaSenhaConf = $('#confereNovaSenhaTroca').val();
                var senhaAtual    = $('#senhaAtualTroca').val();

                if(novaSenha === novaSenhaConf){
                    submit(novaSenha, senhaAtual, novaSenhaConf);
                } else {
                    alert('As senhas não conferem.');
                }
            });
            function submit(novaSenha, senhaAtual, senhaConf) {
                    $.ajax({
                        url: '/acesso/acesso-pessoas/troca-senha-json',
                        dataType: 'json',
                        type: 'post',
                        data: {
                            novaSenha : novaSenha,
                            senhaAtual: senhaAtual,
                            senhaConf :  senhaConf
                        },
                        success: function (json) {
                            if(json.erro){
                                alert(json.erro);
                            } else {
                                alert('Senha trocada com sucesso! Faça login novamente no sistema');
                                    window.location.href = '/acesso/autenticacao/logout';
                            }
                        },
                        erro: function () {
                            alert('Erro ao tentar trocar a senha! Verifique sua conexão com a internet.');
                        }
                });
            }
        </script>
        ");
    }

    /**
     * Renders helper
     *
     * @param  string|Navigation\AbstractContainer $container [optional] container to render.
     *                                         Default is null, which indicates
     *                                         that the helper should render
     *                                         the container returned by {@link
     *                                         getContainer()}.
     * @return string helper output
     * @throws \Zend\View\Exception\ExceptionInterface
     */
    public function render($container = null)
    {
        $content =  $this->getContent().$this->getScript();
        return $content;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getScript()
    {
        return $this->script;
    }

    /**
     * @param mixed $script
     */
    public function setScript($script)
    {
        $this->script = $script;
    }

}