<?php


namespace Professor\ViewHelper;


use Zend\Navigation;
use Zend\View\Helper\Navigation\AbstractHelper;

class Feriados extends AbstractHelper{
    public $em;

    public $feriados;

    public function __construct($em){
        $this->setEm($em);
        $ano = (new \DateTime('now', new \DateTimeZone('America/Sao_Paulo')))->format('Y');
        $sql = $this->getEm()->getConnection()->prepare("
        SELECT
            data_id,evento_descricao,tipoevento_label
        FROM
            acadperiodo__calendario
                NATURAL JOIN
            acadperiodo__calendario_data
                NATURAL JOIN
            acadperiodo__calendario_data_evento
                NATURAL JOIN
            acadperiodo__calendario_tipo_evento
        WHERE
            tipoevento_label IN ('Feriado' , 'Recesso')
                AND data_letivo = 'Não'
                AND calendario_id = {$ano}
        ");
        $sql->execute();
        $this->setFeriados($sql->fetchAll());
    }
    
    /**
     * @return mixed
     */
    public function getEm()
    {
        return $this->em;
    }

    /**
     * @param mixed $em
     */
    public function setEm($em)
    {
        $this->em = $em;
    }

    /**
     * @return mixed
     */
    public function getFeriados()
    {
        return $this->feriados;
    }

    /**
     * @param mixed $feriados
     */
    public function setFeriados($feriados)
    {
        $this->feriados = $feriados;
    }



    public function isHoliday(\DateTime $data)
    {
        foreach($this->getFeriados() as $feriado){
            $dataFeriado = new \DateTime($feriado['data_id']);
            if( $data->format('Y-m-d') == $dataFeriado->format('Y-m-d') ){
                return true;
            }
        }
        return false;
    }
    
    /**
     * Renders helper
     *
     * @param  string|Navigation\AbstractContainer $container [optional] container to render.
     *                                         Default is null, which indicates
     *                                         that the helper should render
     *                                         the container returned by {@link
     *                                         getContainer()}.
     * @return string helper output
     * @throws \Zend\View\Exception\ExceptionInterface
     */
    public function render($container = null)
    {
        // TODO: Implement render() method.
    }
}