<?php


namespace Professor\Controller;

use VersaSpine\Controller\AbstractCoreController;

class IndexController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function indexAction()
    {
        $arrConfig        = $this->getServiceManager()->get('Config');
        $serviceSisConfig = new \Sistema\Service\SisConfig($this->getEntityManager(), $arrConfig);
        $serviceAcesso    = new \Acesso\Service\AcessoPessoas($this->getEntityManager());

        $tokenBibliotecaPearsonConfig = $serviceSisConfig->localizarChave('TOKEN_BIBLIOTECA_PEARSON');
        $enderecoBibliotacaPearson    = $serviceSisConfig->localizarChave('ENDERECO_BIBLIOTECA_PEARSON');

        /** @var $objPessoaLogada \Acesso\Entity\AcessoPessoas */
        $objPessoaLogada = $serviceAcesso->retornaUsuarioLogado();

        $pesCpf = '';

        if($objPessoaLogada && $objPessoaLogada->getPesFisica()){
            $pesCpf = preg_replace("/[^0-9]/", "", $objPessoaLogada->getPesFisica()->getPesCpf());
        }

        $bibliotecaPerson       = false;
        $tokenBibliotecaPearson = '';

        $pesquisaInstitucionalProfessorEndereco = "#";
        $pesquisaInstitucionalProfessorAtiva = $serviceSisConfig->localizarChave('PESQUISA_INSTITUCIONAL_PROFESSOR');
        $pesquisaInstitucionalProfessorMsg = "";
        
        if($pesquisaInstitucionalProfessorAtiva){
            $pesquisaInstitucionalProfessorEndereco = $serviceSisConfig->localizarChave('PESQUISA_INSTITUCIONAL_PROFESSOR_ENDERECO');
            $pesquisaInstitucionalProfessorMsg = $serviceSisConfig->localizarChave('PESQUISA_INSTITUCIONAL_PROFESSOR_MSG');

            $pesId = $objPessoaLogada->getPes()->getPes() ? $objPessoaLogada->getPes()->getPes()->getPesId() : null;
            $pesquisaInstitucionalProfessorEndereco .= $pesId;
        }

        if ($enderecoBibliotacaPearson && $pesCpf && $tokenBibliotecaPearsonConfig) {
            $tokenBibliotecaPearson = md5($pesCpf . $tokenBibliotecaPearsonConfig);
            $bibliotecaPerson       = true;
        }

        $this->view->setVariables(
            [
                'enderecoBibliotacaPearson'              => $enderecoBibliotacaPearson,
                'pesCpf'                                 => $pesCpf,
                'tokenBibliotecaPearson'                 => $tokenBibliotecaPearson,
                'bibliotecaPerson'                       => $bibliotecaPerson,
                'pesquisaInstitucionalProfessorAtivado'  => $pesquisaInstitucionalProfessorAtiva,
                'pesquisaInstitucionalProfessorEndereco' => $pesquisaInstitucionalProfessorEndereco,
                'pesquisaInstitucionalProfessorMsg'      => $pesquisaInstitucionalProfessorMsg,
            ]
        );

        return $this->view;
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {
    }
}