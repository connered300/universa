<?php
namespace Professor\Controller;

use Matricula\Service\AcadperiodoIntegradoraConf;
use VersaSpine\Controller\AbstractCoreController;
use Zend\View\Model;

/**
 * Class DisciplinasController
 * @package Professor\Controller
 */
class DisciplinasController extends AbstractCoreController
{
    /**
     * Método construtor
     */
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    /**
     * Lista e registra frequência de alunos
     * @return \Zend\Http\Response|Model\ViewModel
     */
    public function indexAction()
    {
        $docdiscId                  = $this->params()->fromRoute('id');
        $service                    = new \Matricula\Service\AcadgeralDocente($this->getEntityManager());
        $serviceAcadGrade           = new \Professor\Service\AcadperiodoDisciplinaGrade($this->getEntityManager());
        $serviceFrequencia          = new \Professor\Service\AcadperiodoFrequencia($this->getEntityManager());
        $serviceSituacao            = new \Matricula\Service\AcadgeralSituacao($this->getEntityManager());
        $serviceDocente             = new \Professor\Service\AcadgeralDocente($this->getEntityManager());
        $serviceGrade               = new \Professor\Service\AcadperiodoGrade($this->getEntityManager());
        $serviceCursoConfig         = new \Matricula\Service\AcadCursoConfig($this->getEntityManager());
        $serviceAcadgeralAlunoCurso = new \Matricula\Service\AcadgeralAlunoCurso($this->getEntityManager());

        if (!empty($docdiscId)) {
            $docente = $serviceDocente->buscaDocentePorUsuario();

            $situacaoId =
                array($serviceSituacao::ADAPTANTE, $serviceSituacao::MATRICULADO, $serviceSituacao::DEPENDENTE);

            /** @var \Matricula\Entity\AcadperiodoDocenteDisciplina $docenteDisciplina */
            $docenteDisciplina = $service->getRepository("Matricula\Entity\AcadperiodoDocenteDisciplina")->findOneBy(
                ['docdiscId' => $docdiscId, 'docente' => $docente->getDocenteId()]
            );

            if (empty($docenteDisciplina)) {
                $this->flashMessenger()->addErrorMessage(
                    "Caríssimo, você não tem permisão para trabalhar com esta disciplana! Qualquer dúvida procure a cordenaão."
                );

                return $this->redirect()->toRoute($this->getRoute());
            }

            $lancamentoBloqueado = (
                !($docenteDisciplina->getDocdiscDataFechamento() == null) ||
                !($docenteDisciplina->getDocdiscDataFechamentoFinal() == null)
            );

            if ($this->getRequest()->isPost()) {
                $dados = $this->getRequest()->getPost()->toArray();
                $year  = date_format(new \DateTime('now'), 'Y');

                if ($dados['datebusca'] == 'true') {
                    $date         = new \DateTime($service->formatDateAmericano($dados['datepicker']));
                    $numeroSemana = $date->format('W');
                } else {
                    $date         = (new \DateTime('now', new \DateTimeZone('America/Sao_Paulo')))->setISODate(
                        $year,
                        $dados['numSemana']
                    );
                    $numeroSemana = $date->format('W');
                }

                if ($dados['salvar'] == 'true' && !$lancamentoBloqueado) {
                    $ausentes = $dados['alunoFaltaDia'];

                    if ($dados['alunoFaltaAula']) {
                        $ausentes = array_diff($ausentes, $dados['alunoFaltaAula']);
                    }

                    $serviceFrequencia->insereAusencia($ausentes, $dados['datas'], $docenteDisciplina);

                    foreach ($dados['alunoFaltaAula'] as $presenca) {
                        $presenca = explode("$$", $presenca);
                        $serviceFrequencia->removeFalta($presenca[0], $presenca[3], $presenca[2]);
                    }
                }
            } else {
                $date         = new \DateTime('now', new \DateTimeZone('America/Sao_Paulo'));
                $numeroSemana = $date->format('W');
            }

            $gradeHoraria = null;
            $gradeId      = null;

            if ($date) {
                $gradeHoraria = $service
                    ->getRepository('Professor\Entity\AcadperiodoGrade')
                    ->buscaGradeHorariaPorTurmaEData(
                        $docenteDisciplina->getTurma()->getTurmaId(),
                        $date->format('Y-m-d')
                    );
            } else {
                $gradeHoraria = $service
                    ->getRepository('Professor\Entity\AcadperiodoGrade')
                    ->buscaGradeHorariaPorTurma($docenteDisciplina->getTurma());
            }

            if ($gradeHoraria) {
                $gradeId = $gradeHoraria->getGradeId();
            }

            $alunos = $service->getRepository("Matricula\Entity\AcadperiodoAlunoDisciplina")->findBy(
                [
                    'turma'    => $docenteDisciplina->getTurma(),
                    'disc'     => $docenteDisciplina->getDisc(),
                    'situacao' => $situacaoId
                ]
            );

            $repAluno = $service->getRepository("Matricula\Entity\AcadperiodoAluno");
            /** @var $aluno \Matricula\Entity\AcadperiodoAlunoDisciplina */
            foreach ($alunos as $aluno) {
                $alunoPerMatriculado = $aluno->getAlunoper()->getMatsituacao() == $serviceSituacao::MATRICULADO;
                $alunoCursoDeferido  = $aluno->getAlunoper()->getAlunocurso()->getAlunocursoSituacao() ==
                    $serviceAcadgeralAlunoCurso::ALUNOCURSO_SITUACAO_DEFERIDO;

                if ($alunoPerMatriculado && $alunoCursoDeferido) {
                    $ObjalunoAtivoPeriodo = $repAluno->findOneBy(
                        [
                            'matsituacao' => $situacaoId,
                            'alunoperId'  => $aluno->getAlunoPer()
                        ]
                    );

                    if (!is_null($ObjalunoAtivoPeriodo)) {
                        $ObjAluno[] = $aluno;
                    }
                }
            }

            if ($gradeHoraria) {
                $horarios = $service->getRepository('Professor\Entity\AcadperiodoDisciplinaGrade')->findBy(
                    ['gradeId' => $gradeId, 'discId' => $docenteDisciplina->getDisc()]
                );

                if (!$horarios) {
                    $parametros         = [
                        'turma_id' => $docenteDisciplina->getTurma()->getTurmaId(),
                        'disc_id'  => $docenteDisciplina->getDisc()->getDiscId(),

                    ];
                    $gradeHorariaValida = $serviceGrade->buscaUltimaHorariaValida($parametros);

                    $horarios = $service->getRepository('Professor\Entity\AcadperiodoDisciplinaGrade')->findBy(
                        ['gradeId' => $gradeHorariaValida['grade_id'], 'discId' => $docenteDisciplina->getDisc()]
                    );

                    if ($gradeHorariaValida) {
                        $gradeId = $gradeHorariaValida['grade_id'];

                        $numeroSemana = (new \DateTime($gradeHorariaValida['grade_inicio']))->format('W');
                    }
                }

                $diasDeAula = $serviceAcadGrade->buscaDiasDeAula(
                    $gradeId,
                    $docenteDisciplina->getDisc()->getDiscId()
                );
            } else {
                $diasDeAula[] = ['nome' => 'Segunda'];
                $diasDeAula[] = ['nome' => 'Terca'];
                $diasDeAula[] = ['nome' => 'Quarta'];
                $diasDeAula[] = ['nome' => 'Quinta'];
                $diasDeAula[] = ['nome' => 'Sexta'];
                $diasDeAula[] = ['nome' => 'Sabado'];
                $diasDeAula[] = ['nome' => 'Domingo'];
            }

            $datas = $serviceAcadGrade->buscaDatasSemana($numeroSemana, $diasDeAula);

            $faltas = array();

            foreach ($datas as $key => $data) {
                $faltas[$key] = $serviceFrequencia->buscaFrequenciaPorDia($data, $docenteDisciplina->getDocdiscId());
            }

            if ($docenteDisciplina) {
                /** @var $objTurma \Matricula\Entity\AcadperiodoTurma */
                $objTurma = $docenteDisciplina->getTurma();

                /** @var $objcursoConfig  \Matricula\Entity\AcadCursoConfig */
                $objcursoConfig = $serviceCursoConfig->retornaObjCursoConfig(
                    $objTurma->getMatCur()->getCurso()->getCursoId()
                );

                if (!$objcursoConfig) {
                    $error = $serviceCursoConfig->getLastError();
                    $error = $error ? $error : 'Não foi possível localizar a configuração para o curso!';
                    $this->flashMessenger()->addErrorMessage($error);

                    return $this->redirect()->toRoute(
                        $this->getRoute(),
                        array('controller' => $this->getController())
                    );
                }
                $arrConfigcurso                                = $objcursoConfig->toArray();
                $arrConfigcurso['etapas']                      = $objTurma->getPer()->getPerEtapas();
                $arrConfigcurso['confCalcunoNotaInicialMedia'] = $serviceCursoConfig::METODO_DESEMPENHO_CALCULO_POR_MEDIA;
                $arrConfigcurso['confCalcunoNotaFinalMedia']   = $serviceCursoConfig::METODO_FINAL_POR_MEDIA;

                $periodo = $docenteDisciplina->getTurma()->getPer();
            }

            $this->getView()->setVariables(
                [
                    'configCurso'         => $arrConfigcurso,
                    'docdiscId'           => $docdiscId,
                    'alunos'              => $ObjAluno,
                    'turma'               => $docenteDisciplina->getTurma(),
                    'horarios'            => $horarios,
                    'datas'               => $datas,
                    'numeroSemana'        => $numeroSemana,
                    'diasDeAula'          => $diasDeAula,
                    'docenteDisciplina'   => $docenteDisciplina,
                    'faltas'              => $faltas,
                    'periodo'             => $periodo,
                    'lancamentoBloqueado' => (
                        !($docenteDisciplina->getDocdiscDataFechamento() == null) ||
                        !($docenteDisciplina->getDocdiscDataFechamentoFinal() == null)
                    ),
                ]
            );

            return $this->getView();
        }

        return $this->redirect()->toRoute($this->getRoute());
    }

    /**
     * Registra nova anotação e retorna anotações da docência
     * TODO: separar o método em dois
     * @return Model\ViewModel
     */
    public function anotacoesAction()
    {
        $request         = $this->getRequest();
        $serviceDocente  = new \Matricula\Service\AcadgeralDocente($this->getEntityManager());
        $serviceAnotacao = new \Professor\Service\AcadperiodoAnotacao($this->getEntityManager());

        if ($request->isPost()) {
            $dados = $request->getPost()->toArray();

            if (isset($dados['dados'])) {
                $dados['dados']['anotData'] = new \DateTime(
                    $serviceDocente->formatDateAmericano($dados['dados']['anotData'])
                );

                $anotacao = $serviceAnotacao->save($dados['dados']);

                $this->view->setVariable("saved", true);
            } else {
                $docenteDisciplina = $serviceDocente
                    ->getRepository('Matricula\Entity\AcadperiodoDocenteDisciplina')
                    ->findOneBy(['docdiscId' => $dados['docdiscId']]);
                $anotacoes         = $serviceDocente
                    ->getRepository('Professor\Entity\AcadperiodoAnotacao')
                    ->findBy(['docdisc' => $docenteDisciplina]);

                $this->view->setVariables(
                    [
                        'anotacoes' => $anotacoes
                    ]
                );
            }
        }

        $this->view->setTerminal(true);

        return $this->view;
    }

    /**
     * Retorna meses em que a docência é ministrada
     * @return Model\ViewModel
     */
    public function documentosAction()
    {
        $mesAula = array();
        $dados   = $this->getRequest()->getPost()->toArray();

        $servicePeriodoLetivo   = new \Matricula\Service\AcadperiodoLetivo($this->getEntityManager());
        $serviceTurma           = new \Matricula\Service\AcadperiodoTurma($this->getEntityManager());
        $serviceGrade           = new \Professor\Service\AcadperiodoGrade($this->getEntityManager());
        $serviceDisciplinaGrade = new \Professor\Service\AcadperiodoDisciplinaGrade($this->getEntityManager());

        $docenteDisciplina = $serviceTurma->buscaDocenteTurmaDisciplina($dados['docdiscId']);

        $data['turma'] = $docenteDisciplina['turma_id'];
        $objTurma      = $serviceTurma->getRepository('Matricula\Entity\AcadperiodoTurma')->findOneBy(
            ['turmaId' => $data['turma']]
        );
        $objPeriodo    = $objTurma->getPer();

        $mesesPeriodo = $servicePeriodoLetivo->buscaMesesPeriodo(
            $objPeriodo->getperDataInicio(),
            $objPeriodo->getPerDataFim()
        );
        $grade        = $serviceGrade->getRepository('Professor\Entity\AcadperiodoGrade')->findOneBy(
            array('turma' => $data['turma'])
        );

        /*
         São buscados os dias letivos de aula do docente e assim encontrado os meses que o mesmo  lecionar.
        */
        if (!empty($grade) and !is_null($grade)) {
            $buscaDias = $serviceDisciplinaGrade->buscaDiasDeAula($grade->getGradeId(), $docenteDisciplina['disc_id']);
            $pos       = 0;
            $mesAula   = array();

            foreach ($mesesPeriodo as $mes) {
                foreach ($buscaDias as $dias) {
                    $diasSemana = $serviceDisciplinaGrade->buscaPrimeiroDia($dias['nome'], $mes['referencia']);

                    $parm    = [
                        'date'       => $diasSemana['diaMes'],
                        'objPeriodo' => $objPeriodo
                    ];
                    $diasMes = $serviceDisciplinaGrade->buscaTodosDiasMes($parm);

                    if (!empty($diasMes)) {
                        if ($mes['referencia'] !== $mesAula[$pos - 1]['referencia']) {
                            $pos++;
                            $mesAula[] = $mes;
                        }
                    }
                }
            }
        }

        $this->getView()->setVariables(
            [
                'meses'     => $mesesPeriodo,
                'docdiscId' => $dados['docdiscId'],
            ]
        );

        $this->getView()->setTerminal(true);

        return $this->getView();
    }

    /**
     * Registra nova anotação e seus arquivos
     * @return Model\JsonModel
     */
    public function saveAnotacaoAction()
    {
        $service                = new \Matricula\Service\AcadgeralDocente($this->getEntityManager());
        $serviceArquivoAnotacao = new \Professor\Service\AcadperiodoAnotacaoArquivo($this->getEntityManager());
        $serviceAnotacao        = new \Professor\Service\AcadperiodoAnotacao($this->getEntityManager());

        $request = $this->getRequest();

        if ($request->isPost()) {
            $dados                      = $request->getPost()->toArray();
            $dados['dados']['anotData'] = new \DateTime($service->formatDateAmericano($dados['dados']['anotData']));

            try {
                $anotacao = $serviceAnotacao->save($dados['dados']);

                if ($dados['dados']['arquivos']) {
                    foreach ($dados['dados']['arquivos'] as $arq) {
                        $anotArquivo = [
                            'idAnotArquivo' => '',
                            'anot'          => $anotacao->getAnotId(),
                            'arq'           => $arq
                        ];
                        $serviceArquivoAnotacao->adicionar($anotArquivo);
                    }
                }
            } catch (\Exception $ex) {
                $this->getJson()->setVariable('error', $ex->getMessage());
            }

            if ($anotacao) {
                $this->getJson()->setVariable("saved", true);
            } else {
                $this->getJson()->setVariable("saved", false);
            }
        }

        return $this->getJson();
    }

    /**
     * Exclui anotações e seus arquivos
     * TODO: renomear método/rota para ficar mais objetivo o objetivo do método
     * @return Model\JsonModel
     */
    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $service                = $this->services()->getService('Professor\Service\AcadgeralDocente');
            $dados                  = $request->getPost()->toArray();
            $serviceAnotacao        = new \Professor\Service\AcadperiodoAnotacao($this->getEntityManager());
            $serviceAnotacaoArquivo = new \Professor\Service\AcadperiodoAnotacaoArquivo($this->getEntityManager());
            $documentosProfessor    = $service
                ->getRepository('GerenciadorArquivos\Entity\ArquivoDiretorios')
                ->findOneBy(array('arqDiretorioNome' => 'DocumentosProfessores'));
            $serviceArquivo         = new \GerenciadorArquivos\Service\Arquivo(
                $this->getEntityManager(),
                $documentosProfessor
            );

            try {
                foreach ($dados['id'] as $id) {
                    $anotArquivos = $serviceAnotacao
                        ->getRepository('Professor\Entity\AcadperiodoAnotacaoArquivo')
                        ->findBy(['anot' => $id]);

                    if ($anotArquivos) {
                        foreach ($anotArquivos as $anotArquivo) {
                            $serviceAnotacaoArquivo->excluir($anotArquivo->getIdAnotArquivo());
                            $serviceArquivo->excluir($anotArquivo->getArq()->getArqId());
                        }
                    }

                    $serviceAnotacao->excluir($id);
                }
            } catch (\Exception $ex) {
                $this->getJson()->setVariable('erro', $ex->getMessage());
            }
        }

        $this->getJson()->setVariable('excluido', true);

        return $this->getJson();
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {
    }

    /**
     * Exibe e possibilita o lançamento de notas do aluno
     * @return Model\ViewModel
     * @throws \Exception
     */
    public function notasAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $dados     = $request->getPost()->toArray();
            $docdiscId = $dados['docdiscId'];

            if (!$docdiscId) {
                throw new \Exception(
                    'Para efetuar lançamento e consultas de notas é necessario informar o código da docência.'
                );
            }

            $serviceIntegradora       = new \Matricula\Service\AcadperiodoIntegradora($this->getEntityManager());
            $serviceDocenteDisciplina = new \Matricula\Service\AcadperiodoDocenteDisciplina($this->getEntityManager());
            $serviceAlunoDisciplina   = new \Matricula\Service\AcadperiodoAlunoDisciplina($this->getEntityManager());
            $serviceEtapaAluno        = new \Professor\Service\AcadperiodoEtapaAluno($this->getEntityManager());
            $serviceEtapaDiario       = new \Professor\Service\AcadperiodoEtapaDiario($this->getEntityManager());

            /* @var $objDocenteDisciplina \Matricula\Entity\AcadperiodoDocenteDisciplina */
            $objDocenteDisciplina = $serviceDocenteDisciplina->getRepository()->find($docdiscId);

            if (!$objDocenteDisciplina) {
                throw new \Exception(
                    'Para efetuar lançamento e consultas de notas é necessario informar o código da docência.'
                );
            }

            $arrSituacoesAtividade = \Matricula\Service\AcadgeralSituacao::situacoesAtividade();

            $arrAlunos                  = $serviceAlunoDisciplina->alunosDocenciaEtapaArray(
                $objDocenteDisciplina->getTurma()->getTurmaId(),
                $objDocenteDisciplina->getDisc()->getDiscId(),
                $arrSituacoesAtividade
            );
            $arrNotasAlunos             = $serviceEtapaAluno->buscaNotasAlunos(
                $objDocenteDisciplina->getDocdiscId(),
                $arrSituacoesAtividade
            );
            $arrEtapasDiario            = $serviceEtapaDiario->diariosDocenciaArray(
                $objDocenteDisciplina->getDocdiscId()
            );
            $provaIntegradoraFoiFechada = $serviceIntegradora->provaIntegradoraFoiFechada(
                $objDocenteDisciplina->getTurma()->getPer()
            );
            $turmaNaoFazIntegradora     = $serviceIntegradora->turmaNaoFazIntegradora($objDocenteDisciplina);
            $arrAlunosForaDaIntegradora = $serviceIntegradora->alunosQueNaoFizeramIntegradora($docdiscId);

            $this->getJson()->setVariables(
                [
                    'dataAtual'                  => date('Ymd'),
                    'diarios'                    => $arrEtapasDiario,
                    'alunos'                     => $arrAlunos,
                    'notas'                      => $arrNotasAlunos,
                    'alunosForaDaIntegradora'    => $arrAlunosForaDaIntegradora,
                    'provaIntegradoraFoiFechada' => (int)$provaIntegradoraFoiFechada,
                    'turmaNaoFazIntegradora'     => (int)$turmaNaoFazIntegradora,
                    'lancamentoBloqueado'        => (
                        !($objDocenteDisciplina->getDocdiscDataFechamento() == null) ||
                        !($objDocenteDisciplina->getDocdiscDataFechamentoFinal() == null)
                    ),
                ]
            );
        }

        return $this->getJson();
    }

    /**
     * Rota de acesso a funcionalidade de diário de classe,
     * cuja função é gerenciar o fechamento dos lançamentos da docência
     * @return Model\ViewModel
     */
    public function diarioClasseAction()
    {
        $this->getView()->setTerminal(true);

        return $this->getView();
    }

    /**
     * Retorna campos da tabela de docências, informações sobre entrega de diários
     * e número de notas pendentes para lançamento em etapas
     * @return Model\JsonModel
     */
    public function informacaoPendenciaAction()
    {
        $params                = $this->getRequest()->getPost()->toArray();
        $docDiscId             = $params['docdiscId'];
        $informacoesPendencias = array();

        if ($docDiscId) {
            $serviceDocenteDisciplina = new \Matricula\Service\AcadperiodoDocenteDisciplina($this->getEntityManager());
            $serviceDiarioEntrega     = new \Matricula\Service\AcadperiodoDiarioEntrega($this->getEntityManager());

            $objDocenteDisciplina = $serviceDocenteDisciplina->getRepository()->find($docDiscId);

            $informacoesPendencias = $serviceDocenteDisciplina->buscaPendencias($docDiscId);

            if ($objDocenteDisciplina) {
                $informacoesPendencias['diarios'] = $serviceDiarioEntrega
                    ->retornaDataEntregaDiarios($objDocenteDisciplina);
            }
        }

        $this->getJson()->setVariables(['informacoesPendencias' => $informacoesPendencias]);

        return $this->getJson();
    }

    /**
     * Retorna JSON com informações dos alunos que possuem notas pendentes para lançamento em etapas.
     * @return Model\JsonModel
     */
    public function alunosPendenciaNotasAction()
    {
        $params               = $this->getRequest()->getPost()->toArray();
        $docDiscId            = $params['docdiscId'];
        $alunosPendenciaNotas = array();

        if ($docDiscId) {
            $serviceEtapaAluno    = new \Professor\Service\AcadperiodoEtapaAluno($this->getEntityManager());
            $alunosPendenciaNotas = $serviceEtapaAluno->buscaNotaNulasAlunosEtapa($docDiscId);
        }

        $this->getJson()->setVariables(['alunosPendenciaNotas' => $alunosPendenciaNotas]);

        return $this->getJson();
    }

    /**
     * Efetua cálculo de notas de alunos e lança na tabela resumo.
     * Caso haja alunos de final efetua registro dos mesmos na tabela de final.
     * @return Model\JsonModel
     */
    public function finalizarLancamentosParte1Action()
    {
        $params    = $this->getRequest()->getPost()->toArray();
        $docDiscId = $params['docdiscId'];
        $erro      = 0;
        $msg       = 'Fechamento efetuado com sucesso!';

        $serviceDocenteDisciplina = new \Matricula\Service\AcadperiodoDocenteDisciplina($this->getEntityManager());

        if (!$serviceDocenteDisciplina->efetuaFechamentoNotasParte1($docDiscId)) {
            $erro = 1;
            $msg  = $serviceDocenteDisciplina->getLastError();
        }

        $this->getJson()->setVariable("erro", $erro);
        $this->getJson()->setVariable("msg", $msg);

        return $this->getJson();
    }

    /**
     * Retorna JSON de alunos que ficaram de final
     * @return Model\JsonModel
     */
    public function alunosFinalAction()
    {
        $params            = $this->getRequest()->getPost()->toArray();
        $docDiscId         = $params['docdiscId'];
        $alunosNotasFinais = array();
        $configNota        = array();

        if ($docDiscId) {
            $serviceDiario     = new \Professor\Service\AcadperiodoEtapaDiario($this->getEntityManager());
            $serviceEtapaAluno = new \Matricula\Service\AcadperiodoAlunoDisciplinaFinal($this->getEntityManager());
            $alunosNotasFinais = $serviceEtapaAluno->buscaNotasFinalAlunos($docDiscId);
            $configNota        = $serviceDiario->buscaConfigNota($docDiscId);
        }

        $this->getJson()->setVariables(
            [
                'alunosNotasFinais' => $alunosNotasFinais,
                'configNota'        => $configNota
            ]
        );

        return $this->getJson();
    }

    /**
     * Salva notas de alunos que ficaram de exame final.
     * Caso seja recebido o parametro 'finalizar' via post,
     * cuja conversão em boll seja true é feita a atualização da tabela resumo
     * e setado o fechamento da etapa de fato
     * @return Model\JsonModel
     */
    public function finalizarLancamentosParte2Action()
    {
        $params         = $this->getRequest()->getPost()->toArray();
        $docDiscId      = $params['docdiscId'];
        $finalizar      = $params['finalizar'];
        $arrNotasFinais = $params['notasFinais'];
        $erro           = true;
        $msg            = '';

        if ($docDiscId) {
            $serviceAlunoFinal = new \Matricula\Service\AcadperiodoAlunoDisciplinaFinal($this->getEntityManager());

            if (!$serviceAlunoFinal->efetuaLancamentoNotasFinais($docDiscId, $arrNotasFinais)) {
                $erro = true;
                $msg  = $serviceAlunoFinal->getLastError();
            } elseif ($finalizar) {
                $msg = $serviceAlunoFinal->getLastError();

                if (!$msg) {
                    $serviceDocenteDisciplina = new \Matricula\Service\AcadperiodoDocenteDisciplina(
                        $this->getEntityManager()
                    );

                    if ($serviceDocenteDisciplina->efetuaFechamentoNotasParte2($docDiscId)) {
                        $erro = false;
                        $msg  = 'Lançamento de notas finais efetuado com sucesso!';
                    } else {
                        $erro = true;
                        $msg  = $serviceAlunoFinal->getLastError();
                    }
                }
            } else {
                $msg  = 'Notas salvas!';
                $erro = false;
            }
        }

        $this->getJson()->setVariable("erro", (int)$erro);
        $this->getJson()->setVariable("msg", $msg);

        return $this->getJson();
    }
}