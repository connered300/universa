<?php


namespace Professor\Controller;


use DOMPDFModule\View\Model\PdfModel;
use VersaSpine\Controller\AbstractCoreController;

class AcadperiodoAnotacaoController extends AbstractCoreController{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {
    }

    public function relatorioAnotacaoAction()
    {
        $request = $this->getRequest();

        $data = array_merge($request->getQuery()->toArray(), $request->getPost()->toArray());

        $docdiscId = $data['docdiscId'];

        $pdf = new PdfModel();

        $service = $this->services()->getService($this->getService());

        $disc      = $service->getRepository('Matricula\Entity\AcadperiodoDocenteDisciplina')->findOneBy(['docdiscId' => $docdiscId]);
        $anotacoes = $service->buscaAnotacaoMes($data['mes'], $disc->getDocdiscId());
        $mesExtenso =  \Boleto\Service\FinanceiroRelatorios::mesReferencia((strlen($data['mes']) === 1) ? '0'.$data['mes'] : $data['mes']);
        $per       = $disc->getTurma()->getPer();

        $ServiceOrgCampus = new \Organizacao\Service\OrgCampus($this->getEm());

        $infoOrg = $ServiceOrgCampus->retornaDadosInstituicao($disc->getTurma()->getCursocampus()->getCamp());


        $logo = ( empty($infoOrg['logo']) || is_null($infoOrg['logo']))  ? getcwd() . '/public/img/logo.png' :$infoOrg['logo'];


        $pdf->setVariables([
            'anotacoes' => $anotacoes,
            'disc'      => $disc,
            'per'       => $per,
            'mes'       => $mesExtenso,
            'logo'      => $logo
        ]);
        return $pdf;
    }

    protected function getEm() {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get("Doctrine\ORM\EntityManager");
        }
        return $this->em;
    }
}
