<?php
namespace Professor\Controller;

use DOMPDFModule\View\Model\PdfModel;
use VersaSpine\Controller\AbstractCoreController;

class RelatoriosController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {
    }

    /**
     * @return PdfModel
     */
    public function diarioAction()
    {
        /**
         * Deverá passar pela rota somente o docdiscilina, atravez deste, todo a informação será recuperada.
         */

        $request           = $this->getRequest();
        $data              = array_merge($request->getQuery()->toArray(), $request->getPost()->toArray());
        $serviceOrgCampus  = new \Organizacao\Service\OrgCampus($this->getEntityManager());
        $serviceSituacao   = new \Matricula\Service\AcadgeralSituacao($this->getEntityManager());
        $serviceAlunoCurso = new \Matricula\Service\AcadgeralAlunoCurso($this->getEntityManager());

        if ($data) {
            $pdf = new PdfModel();

            $pdf->setVariables($serviceOrgCampus->retornaDadosInstituicao());

            $data['docdisciplina'] = $data['docdiscId'];
            $serviceTurma          = $this->services()->getService('Matricula\Service\AcadperiodoTurma');
            $Acadperiodofrequencia = $this->services()->getService('Professor\Service\AcadperiodoFrequencia');
            $docenteDisciplina     = $serviceTurma->buscaDocenteTurmaDisciplina($data['docdisciplina']);

            if ($docenteDisciplina) {
                $data['disciplina']     = $docenteDisciplina['disc_id'];
                $data['curso']          = $docenteDisciplina['curso_nome'];
                $data['docente']        = $docenteDisciplina['docente_id'];
                $data['turma']          = $docenteDisciplina['turma_id'];
                $disciplinaNome         = $docenteDisciplina['disc_nome'];
                $docente                = $this->services()->getService(
                    'Matricula\Service\AcadgeralDocente'
                )->buscaDadosDocente($data['docente']);
                $serviceGrade           = $this->services()->getService('Professor\Service\AcadperiodoGrade');
                $turma                  = $serviceTurma->getRepository('Matricula\Entity\AcadperiodoTurma')->findOneBy(
                    ['turmaId' => $data['turma']]
                );
                $grade                  = $serviceGrade->getRepository('Professor\Entity\AcadperiodoGrade')->findOneBy(
                    array('turma' => $data['turma'])
                );
                $serviceDisciplinaGrade = $this->services()->getService('Professor\Service\AcadperiodoDisciplinaGrade');

                if ($grade) {
                    do {
                        $a += 1;
                        $buscaDias[$a]               = $serviceDisciplinaGrade->buscaDiasDeAula(
                            $grade->getGradeId(),
                            $data['disciplina']
                        );
                        $buscaDias[$a]['dia_inicio'] = $grade->getGradeInicio();
                        $buscaDias[$a]['dia_fim']    = $grade->getGradeFim();
                        $gradeId[]                   = $grade->getGradeId();
                        $grade                       = $grade->getGradeCiclo();
                    } while (!is_null($grade));
                }

                $situacao =
                    array($serviceSituacao::ADAPTANTE, $serviceSituacao::MATRICULADO, $serviceSituacao::DEPENDENTE);

                $alunosTurma = $serviceTurma->buscaAlunosTurmaDisciplina(
                    $data['turma'],
                    $data['disciplina'],
                    $situacao,
                    ['matSituacao'        => $serviceSituacao::MATRICULADO,
                     'alunoCursoSituacao' => "'".$serviceAlunoCurso::ALUNOCURSO_SITUACAO_DEFERIDO."'"
                    ]
                );

                $faltas      = $Acadperiodofrequencia->buscaFaltaDisciplinaMes($data['mes'], $data['docdisciplina']);
                $diasSemana  = array();

                foreach ($buscaDias as $count => $dias) {
                    foreach ($dias as $dia) {
                        if (is_array($dia)) {
                            $diasSemana [$count][] = $serviceDisciplinaGrade->buscaPrimeiroDia(
                                $dia['nome'],
                                $data['mes'],
                                $turma->getPer()->getPerAno()
                            );
                        };
                    }
                }

                $diasMes   = array();
                $horasAula = array();

                foreach ($diasSemana as $countDiaSemana => $mesDias) {
                    foreach ($mesDias as $mesDia) {
                        $param = [
                            'date'        => $mesDia['diaMes'],
                            'date_inicio' => !is_null($buscaDias[$countDiaSemana]['dia_inicio']) ? date_format(
                                $buscaDias[$countDiaSemana]['dia_inicio'],
                                'y/m/d'
                            ) : null,
                            'date_fim'    => !is_null($buscaDias[$countDiaSemana]['dia_fim']) ? date_format(
                                $buscaDias[$countDiaSemana]['dia_fim'],
                                'y/m/d'
                            ) : null,
                            'objPeriodo'  => $turma->getPer()
                        ];

                        $diasMes[] = $serviceDisciplinaGrade->buscaTodosDiasMes($param);
                    }
                }

                foreach ($buscaDias as $id => $dias) {
                    foreach ($dias as $diasHoras) {
                        if (is_array($diasHoras)) {
                            $horasAula[] = $serviceDisciplinaGrade->buscaHorasAula(
                                $diasHoras['nome'],
                                $gradeId[$id - 1],
                                $data['disciplina']
                            );
                        }
                    }
                }

                $arrDiasMes = array();

                for ($i = 0; $i < count($diasMes); $i++) {
                    for ($x = 0; $x < count($diasMes[$i]); $x++) {
                        foreach ($horasAula[$i] as $horas) {
                            $diasMes[$i][$x][] = $horas['gradedisc_hora'];
                        }
                    }
                }

                foreach ($diasMes as $index => $value) {
                    $arrDiasMes = array_merge($value, $arrDiasMes);
                }

                usort(
                    $arrDiasMes,
                    function ($a, $b) {
                        return strtotime($a[0]) - strtotime($b[0]);
                    }
                );

                $mesNome = array(
                    '',
                    'janeiro',
                    'fevereiro',
                    'março',
                    'abril',
                    'maio',
                    'junho',
                    'julho',
                    'agosto',
                    'setembro',
                    'outubro',
                    'novembro',
                    'dezembro'
                );

                $qtdDias = 0;

                foreach ($arrDiasMes as $dias) {
                    $qtdDias += (count($dias) - 1);
                }

                $pdf->setOption('paperOrientation', 'landscape'); // Defaults to "portrait"
                $pdf->setTemplate('professor/relatorios/diario');
                $pdf->setVariables(
                    [
                        'periodo'     => $turma->getPer(),
                        'turma'       => $turma,
                        'docente'     => $docente[0]['pes_nome'],
                        'alunosTurma' => $alunosTurma,
                        'curso'       => $data['curso'],
                        'horarios'    => $arrDiasMes,
                        'faltas'      => $faltas,
                        'disciplina'  => $disciplinaNome,
                        'mes'         => $mesNome[$data['mes']],
                        'ano'         => $turma->getPer()->getPerAno(),
                        'countDias'   => $qtdDias,
                        'dataBase'    => (new \DateTime('now'))->format("d/m/Y H:i"),
                    ]
                );

                return $pdf;
            }
        } else {
            $this->redirect("/professor/disciplinas/index");
        }
    }

    /**
     * @return PdfModel
     */
    public function diarioNotasAction()

    {
        $request          = $this->getRequest();
        $pdf              = new PdfModel();
        $serviceOrgCampus = new \Organizacao\Service\OrgCampus($this->getEntityManager());
        $pdf->setVariables($serviceOrgCampus->retornaDadosInstituicao());

        $data = array_merge($request->getQuery()->toArray(), $request->getPost()->toArray());

        $data['docdisciplina']       = $data['docdiscId'];
        $serviceDiario               = new \Professor\Service\AcadperiodoEtapaDiario($this->getEntityManager());
        $serviceEtapaAluno           = new \Professor\Service\AcadperiodoEtapaAluno($this->getEntityManager());
        $serviceDocenteDisciplina    = new \Matricula\Service\AcadperiodoDocenteDisciplina($this->getEntityManager());
        $serviceDocente              = new \Matricula\Service\AcadgeralDocente($this->getEntityManager());
        $serviceCursoConfig          = new \Matricula\Service\AcadCursoConfig($this->getEntityManager());
        $serviceAlunoResumo          = new \Matricula\Service\AcadperiodoAlunoResumo($this->getEntityManager());
        $serviceMatrizDisciplina     = new \Matricula\Service\AcadperiodoMatrizDisciplina($this->getEntityManager());
        $serviceAlunoDisciplinaFinal = new \Matricula\Service\AcadperiodoAlunoDisciplinaFinal(
            $this->getEntityManager()
        );

        /** @var \Matricula\Entity\AcadperiodoDocenteDisciplina $docenteDisciplina */
        $docenteDisciplina = $serviceDocenteDisciplina->getRepository()->findOneBy(['docdiscId' => $data['docdiscId']]);
        $docente           = $serviceDocente->buscaDadosDocente($docenteDisciplina->getDocente()->getDocenteId());
        $metodoDesempenho  = $serviceCursoConfig->getArrayConfiguracoes(
            $docenteDisciplina->getTurma()->getCursoCampus()->getCurso()->getCursoId()
        );
        $diarios           = $serviceDiario->buscaDiariosDisciplina($data['docdiscId']);
        $alunoNotas        = $serviceEtapaAluno->buscaAlunoEtapa($data['docdiscId'], $diarios);

        foreach ($alunoNotas as $pos => $aluno) {
            if ($aluno['aluno']) {
                $alunoNotas[$pos]['final'] = $serviceAlunoDisciplinaFinal->getRepository()->findOneBy(
                    ['alunodisc' => $aluno['aluno']->getAlunoDiscId()]
                );
            }
        }
        foreach ($alunoNotas as $pos => $aluno) {
            if ($aluno['aluno']) {
                $alunoNotas[$pos]['resumo'] = $serviceAlunoResumo->getRepository()->findOneBy(
                    [
                        'alunocursoId'     => $aluno['aluno']->getAlunoper()->getAlunocurso()->getAlunocursoId(),
                        'discId'           => $aluno['aluno']->getDisc()->getDiscId(),
                        //                'disc'                =>$aluno['aluno']->getDisc()->getAlunoDiscId(),
                        'resalunoSemestre' => $aluno['aluno']->getTurma()->getPer()->getPerSemestre(),
                        'resalunoAno'      => $aluno['aluno']->getTurma()->getPer()->getPerAno()
                    ]
                );
            }
        }
        foreach ($alunoNotas as $pos => $aluno) {
            if ($aluno['aluno']) {
                $alunoNotas[$pos]['frequencia'] = count(
                    $serviceEtapaAluno->buscaFaltasAluno(
                        $aluno['aluno']->getAlunodiscId(),
                        date_format($docenteDisciplina->getTurma()->getPer()->getPerDataInicio(), 'Y-m-d'),
                        date_format($docenteDisciplina->getTurma()->getPer()->getPerDataFim(), 'Y-m-d')
                    )
                );
            }
        }

        foreach ($alunoNotas as $key => $obj) {
            if ($obj['aluno']) {
                $pesNome[$key] = $obj['aluno']->getAlunoPer()->getAlunocurso()->getAluno()->getPes()->getPes(
                )->getPesNome();
            }
        }

        array_multisort($pesNome, SORT_ASC, $alunoNotas);

        $matrizDisciplina = $serviceMatrizDisciplina->getRepository()->findoneBy(
            [
                'disc'   => $docenteDisciplina->getDisc()->getDiscId(),
                'matCur' => $docenteDisciplina->getTurma()->getMatCur()->getMatCurId()
            ]
        );

        $pdf->setOption('paperOrientation', 'landscape');
        $pdf->setTemplate('/professor/relatorios/diario-notas');
        $pdf->setVariables(
            [
                'docenteDisciplina' => $docenteDisciplina,
                'metodoCalculo'     => $metodoDesempenho[0]['cursoconfigMetodo'],
                'notaFracionada'    => $metodoDesempenho[0]['cursoconfigNotaFracionada'],
                'docente'           => $docente,
                'alunoNotas'        => $alunoNotas,
                'matrizDisciplina'  => $matrizDisciplina,
                'diario'            => $diarios,
                'dataBase'          => (new \DateTime('now'))->format("d/m/Y H:i"),
            ]
        );

        return $pdf;
    }

    public function diarioAnotacoesSemestralAction()
    {
        /**
         * Deverá passar pela rota somente o docdiscilina, atravez deste, todo a informação será recuperada.
         */

        $request          = $this->getRequest();
        $data             = array_merge($request->getQuery()->toArray(), $request->getPost()->toArray());
        $serviceOrgCampus = new \Organizacao\Service\OrgCampus($this->getEntityManager());

        if ($data) {
            $serviceTurma = $this->services()->getService('Matricula\Service\AcadperiodoTurma');

            $pdf = new PdfModel();
            $pdf->setVariables($serviceOrgCampus->retornaDadosInstituicao());

            $data['docdisciplina'] = $data['docdiscId'];
            $docenteDisciplina     = $serviceTurma->buscaDocenteTurmaDisciplina($data['docdisciplina']);

            if ($docenteDisciplina) {
                $data['disciplina']       = $docenteDisciplina['disc_id'];
                $data['disciplina_nome']  = $docenteDisciplina['disc_nome'];
                $data['carga_horaria']    = $docenteDisciplina['carga_horaria'];
                $data['aulas']            = $docenteDisciplina['aulas'];
                $data['curso']            = $docenteDisciplina['curso_nome'];
                $data['coordenador_nome'] = $docenteDisciplina['coordenador_nome'];
                $data['docente']          = $docenteDisciplina['docente_id'];
                $data['turma']            = $docenteDisciplina['turma_id'];
                $data['turma_nome']       = $docenteDisciplina['turma_nome'];
                $data['turma_serie']      = $docenteDisciplina['turma_serie'];
                $data['turma_turno']      = $docenteDisciplina['turma_turno'];
                $data['periodo_letivo']   = $docenteDisciplina['per_nome'];
                $data['fechamento']       = $docenteDisciplina['docdisc_data_fechamento'];
                $data['fechamento_final'] = $docenteDisciplina['docdisc_data_fechamento_final'];
                $data['professor']        = $docenteDisciplina['pes_nome'];

                $turma = $serviceTurma->getRepository('Matricula\Entity\AcadperiodoTurma')->findOneBy(
                    ['turmaId' => $data['turma']]
                );

                $anotacoes = $serviceTurma->getRepository('Professor\Entity\AcadperiodoAnotacao')->findBy(
                    ['docdisc' => $data['docdiscId']]
                );

                $situacao    = \Matricula\Service\AcadgeralSituacao::situacoesAtividade();
                $alunosTurma = $serviceTurma->buscaAlunosTurmaDisciplina(
                    $data['turma'],
                    $data['disciplina'],
                    $situacao
                );

                $pdf->setOption('paperOrientation', 'landscape'); // Defaults to "portrait"
                $pdf->setTemplate('professor/relatorios/diario-anotacoes-semestral');
                $pdf->setVariables(
                    [
                        'docencia'  => $data,
                        'anotacoes' => $anotacoes,
                        'dataBase'  => (new \DateTime('now'))->format("d/m/Y H:i"),
                    ]
                );

                return $pdf;
            }
        } else {
            $this->redirect("/professor/disciplinas/index");
        }
    }

}