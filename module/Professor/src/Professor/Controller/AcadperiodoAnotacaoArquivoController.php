<?php


namespace Professor\Controller;

use VersaSpine\Controller\AbstractCoreController;

class AcadperiodoAnotacaoArquivoController extends AbstractCoreController{
    public function __construct() {
        parent::__construct( __CLASS__ );
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {

    }

    public function insertFileAction()
    {
        $request = $this->getRequest();
        $service = $this->services()->getService($this->getService());

        $documentosProfessor = $service
            ->getRepository('GerenciadorArquivos\Entity\ArquivoDiretorios')
            ->findOneBy(array('arqDiretorioNome' => 'DocumentosProfessores'));

        $serviceArquivo = new \GerenciadorArquivos\Service\Arquivo($this->getServiceLocator()->get('Doctrine\ORM\EntityManager'), $documentosProfessor);
        if ($request->isPost()) {
            $files = $_FILES;
            $filesAdd = array();
            foreach($files as $file){
                $tmp = $serviceArquivo->adicionar($file);
                $filesAdd[] = $tmp->toArray();
            }
            $this->json->setVariable('filesAdd', $filesAdd);
        }
        return $this->json;
    }

    public function removeFileAction()
    {
        $request = $this->getRequest();
        $service = $this->services()->getService($this->getService());
        $documentosProfessor = $service
            ->getRepository('GerenciadorArquivos\Entity\ArquivoDiretorios')
            ->findOneBy(array('arqDiretorioNome' => 'DocumentosProfessores'));
        $serviceArquivo = new \GerenciadorArquivos\Service\Arquivo($this->getServiceLocator()->get('Doctrine\ORM\EntityManager'), $documentosProfessor);
        if ($request->isPost()) {
            $dados = $request->getPost()->toArray();
            $arquivo     = $serviceArquivo->getRepository("GerenciadorArquivos\Entity\Arquivo")->findOneBy([ 'arqId' => $dados['arquivoId'] ]);
            if($arquivo){
                $anotArquivo = $service->getRepository("Professor\Entity\AcadperiodoAnotacaoArquivo")->findOneBy(['arq' => $arquivo]);
                try{
                    if($anotArquivo){
                        $service->excluir( $anotArquivo->getIdAnotArquivo() );
                    }
                    $serviceArquivo->excluir( $arquivo->getArqId() );
                } catch (\Exception $ex){
                    $this->json->setVariable("deletado", false);
                }
            }
            $this->json->setVariable("deletado", true);
        }
        return $this->json;
    }

    public function buscaArquivosAnotacaoAction()
    {
        $request = $this->getRequest();
        $serviceArquivo = $this->services()->getService('Professor\Service\AcadperiodoAnotacaoArquivo');
        if ($request->isPost()) {
            $dados = $request->getPost()->toArray();
            $arquivos     = $serviceArquivo->getRepository("Professor\Entity\AcadperiodoAnotacaoArquivo")->findBy([ 'anot' => $dados['anotacaoId'] ]);
            $ret = [];
            if($arquivos){
                foreach($arquivos as $arquivo){
                    $ret[] = $arquivo->getArq()->toArray();
                }
            }
            $this->json->setVariable("arquivos", $ret);
        }
        return $this->json;
    }
}