<?php
namespace Professor\Controller;

use VersaSpine\Controller\AbstractCoreController;

class AcadperiodoEtapaAlunoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {

    }

    public function saveEtapaAlunoAction()
    {
        $serviceEtapaAluno = new \Professor\Service\AcadperiodoEtapaAluno($this->getEntityManager());
        $request           = $this->getRequest();

        if ($request->isPost()) {
            $dados = $request->getPost()->toArray();
            $docdiscId = $dados['docdiscId'];

            if (!$docdiscId) {
                $this->json->setVariable(
                    'erro',
                    'Para efetuar lançamento e consultas de notas é necessario informar o código da docência.'
                );
            }

            if (!$serviceEtapaAluno->salvaRegistrosNotasDiarioAluno($docdiscId, $dados['notas'])) {
                $this->json->setVariable('erro', $serviceEtapaAluno->getLastError());
            } else {
                $this->json->setVariable('success', true);
            }
        }

        return $this->json;
    }
}