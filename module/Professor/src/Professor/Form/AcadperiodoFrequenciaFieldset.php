<?php


namespace Professor\Form;

use Zend\Form\Fieldset,
    Zend\Form\Element;

class AcadperiodoFrequenciaFieldset extends Fieldset{
    function __construct()
    {
        $id = new Element\Hidden('frequenciaId');
        $this->add($id);

        $descricao = new Element\Text('calendarioDescricao');
        $descricao->setAttributes([
            'placeholder'      => 'Digite a descrição',
            'label'            => 'Descrição',
            'col'              => '6',
        ]);
        $this->add($descricao);

        $alunoDisc = new Element\Hidden('alunodiscId');
        $this->add($alunoDisc);
    }
}