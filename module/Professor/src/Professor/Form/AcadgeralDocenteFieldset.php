<?php


namespace Professor\Form;

use Zend\Form\Fieldset,
    Zend\Form\Element,
    Zend\Stdlib\Hydrator\ClassMethods;

class AcadgeralDocenteFieldset extends Fieldset{
    function __construct()
    {
        $id = new Element\Hidden('docenteId');
        $this->add($id);

        $dataInicio = new Element\Hidden('docenteDataInicio');
        $this->add($dataInicio);

        $ativo = new Element\Checkbox('docenteAtivo');
        $ativo->setOptions([
            'Sim' => 'Sim',
            'Nao' => 'Não'
        ]);
        $ativo->setAttributes([
            'label'            => 'Situação',
            'col'              => '6',
            'data-validations' => 'Required',
        ]);
        $this->add($ativo);

        $dataFim = new Element\Hidden('docenteDataFim');
        $this->add($dataFim);

        $docenteCurLattes = new Element\Text('docenteCurLattes');
        $docenteCurLattes->setAttributes([
            'placeholder'      => 'Informe o link do curriculo lattes',
            'label'            => 'Curriculo Lattes',
            'col'              => '6'
        ]);
        $this->add($docenteCurLattes);

        $numMec = new Element\Text('docenteNumMec');
        $numMec->setAttributes([
            'placeholder'      => 'Digite o número do MEC',
            'label'            => 'Número do MEC',
            'col'              => '6',
            'data-validations' => 'Required'
        ]);
        $this->add($numMec);

        $docenteTituto = new Element\Text('docenteTituto');
        $docenteTituto->setAttributes([
            'placeholder'      => 'Informe a titulação do professor',
            'label'            => 'Titulo',
            'col'              => '6'
        ]);
        $this->add($docenteTituto);
    }
}