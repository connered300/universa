<?php

namespace Infraestrutura\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfraClassificacao
 *
 * @ORM\Table(name="infra_classificacao")
 * @ORM\Entity
 */
class InfraClassificacao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="classificacao_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $classificacaoId;

    /**
     * @var string
     *
     * @ORM\Column(name="classificacao_nome", type="string", length=45, nullable=false)
     */
    private $classificacaoNome;

    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getClassificacaoId()
    {
        return $this->classificacaoId;
    }

    /**
     * @param int $classificacaoId
     */
    public function setClassificacaoId($classificacaoId)
    {
        $this->classificacaoId = $classificacaoId;
    }

    /**
     * @return string
     */
    public function getClassificacaoNome()
    {
        return $this->classificacaoNome;
    }

    /**
     * @param string $classificacaoNome
     */
    public function setClassificacaoNome($classificacaoNome)
    {
        $this->classificacaoNome = $classificacaoNome;
    }

    public function toArray()
    {
        return [
            'classificacaoId'   => $this->getClassificacaoId(),
            'classificacaoNome' => $this->getClassificacaoNome(),
        ];
    }
}
