<?php

namespace Infraestrutura\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * InfraPredio
 *
 * @ORM\Table(name="infra_predio", indexes={@ORM\Index(name="fk_infra_predio_org_campus1_idx", columns={"camp_id"})})
 * @ORM\Entity
 * @LG\LG(id="predio_id",label="predio_id")
 * @Jarvis\Jarvis(title="Lista de Prédios",icon="fa fa-table")
 */
class InfraPredio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="predio_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="predio_id")
     * @LG\Labels\Attributes(text="Índice",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $predioId;

    /**
     * @var string
     *
     * @ORM\Column(name="predio_nome", type="string", length=45, nullable=false)
     * @LG\Labels\Property(name="predio_nome")
     * @LG\Labels\Attributes(text="Nome do Prédio",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE%")
     */
    private $predioNome;

    /**
     * @var string
     *
     * @ORM\Column(name="predio_logradouro", type="string", length=45, nullable=true)
     */
    private $predioLogradouro;

    /**
     * @var integer
     *
     * @ORM\Column(name="predio_numero", type="integer", nullable=true)
     */
    private $predioNumero;

    /**
     * @var string
     *
     * @ORM\Column(name="predio_cep", type="string", length=45, nullable=true)
     */
    private $predioCep;

    /**
     * @var string
     *
     * @ORM\Column(name="predio_bairro", type="string", length=45, nullable=true)
     */
    private $predioBairro;

    /**
     * @var string
     *
     * @ORM\Column(name="predio_cidade", type="string", length=45, nullable=true)
     */
    private $predioCidade;

    /**
     * @var string
     *
     * @ORM\Column(name="predio_estado", type="string", length=45, nullable=true)
     */
    private $predioEstado;

    /**
     * @var string
     *
     * @ORM\Column(name="predio_tipo", type="string", length=9, nullable=false)
     */
    private $predioTipo = 'apoio';

    /**
     * @var \Organizacao\Entity\OrgCampus
     *
     * @ORM\ManyToOne(targetEntity="Organizacao\Entity\OrgCampus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="camp_id", referencedColumnName="camp_id")
     * })
     * @LG\Labels\Property(name="camp_id")
     * @LG\Labels\Attributes(text="Nome do Câmpus",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE%")
     * @LG\Querys\Joins(table="org_campus",joinType="inner",joinCampDest="camp_id",joinCampDestLabel="camp_nome")
     */
    private $camp;

    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getPredioId()
    {
        return $this->predioId;
    }

    /**
     * @param int $predioId
     * @return InfraPredio
     */
    public function setPredioId($predioId)
    {
        $this->predioId = $predioId;

        return $this;
    }

    /**
     * @return string
     */
    public function getPredioNome()
    {
        return $this->predioNome;
    }

    /**
     * @param string $predioNome
     * @return InfraPredio
     */
    public function setPredioNome($predioNome)
    {
        $this->predioNome = $predioNome;

        return $this;
    }

    /**
     * @return string
     */
    public function getPredioLogradouro()
    {
        return $this->predioLogradouro;
    }

    /**
     * @param string $predioLogradouro
     * @return InfraPredio
     */
    public function setPredioLogradouro($predioLogradouro)
    {
        $this->predioLogradouro = $predioLogradouro;

        return $this;
    }

    /**
     * @return int
     */
    public function getPredioNumero()
    {
        return $this->predioNumero;
    }

    /**
     * @param int $predioNumero
     * @return InfraPredio
     */
    public function setPredioNumero($predioNumero)
    {
        $this->predioNumero = $predioNumero ? (int)$predioNumero : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getPredioCep()
    {
        return $this->predioCep;
    }

    /**
     * @param string $predioCep
     * @return InfraPredio
     */
    public function setPredioCep($predioCep)
    {
        $this->predioCep = $predioCep;

        return $this;
    }

    /**
     * @return string
     */
    public function getPredioBairro()
    {
        return $this->predioBairro;
    }

    /**
     * @param string $predioBairro
     * @return InfraPredio
     */
    public function setPredioBairro($predioBairro)
    {
        $this->predioBairro = $predioBairro;

        return $this;
    }

    /**
     * @return string
     */
    public function getPredioCidade()
    {
        return $this->predioCidade;
    }

    /**
     * @param string $predioCidade
     * @return InfraPredio
     */
    public function setPredioCidade($predioCidade)
    {
        $this->predioCidade = $predioCidade;

        return $this;
    }

    /**
     * @return string
     */
    public function getPredioEstado()
    {
        return $this->predioEstado;
    }

    /**
     * @param string $predioEstado
     * @return InfraPredio
     */
    public function setPredioEstado($predioEstado)
    {
        $this->predioEstado = $predioEstado;

        return $this;
    }

    /**
     * @return string
     */
    public function getPredioTipo()
    {
        return $this->predioTipo;
    }

    /**
     * @param string $predioTipo
     * @return InfraPredio
     */
    public function setPredioTipo($predioTipo)
    {
        $this->predioTipo = $predioTipo;

        return $this;
    }

    /**
     * @return \Organizacao\Entity\OrgCampus
     */
    public function getCamp()
    {
        return $this->camp;
    }

    /**
     * @param \Organizacao\Entity\OrgCampus $camp
     * @return InfraPredio
     */
    public function setCamp($camp)
    {
        $this->camp = $camp;

        return $this;
    }

    public function toArray()
    {
        return [
            'predioId'         => $this->getPredioId(),
            'predioNome'       => $this->getPredioNome(),
            'predioLogradouro' => $this->getPredioLogradouro(),
            'predioNumero'     => $this->getPredioNumero(),
            'predioCep'        => $this->getPredioCep(),
            'predioBairro'     => $this->getPredioBairro(),
            'predioCidade'     => $this->getPredioCidade(),
            'predioEstado'     => $this->getPredioEstado(),
            'predioTipo'       => $this->getPredioTipo(),
            'campId'           => $this->getCamp() ? $this->getCamp()->getCampId() : null,
            'camp'             => $this->getCamp()
        ];
    }
}