<?php

namespace Infraestrutura\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * InfraSala
 *
 * @ORM\Table(name="infra_sala", indexes={@ORM\Index(name="fk_infra_sala_infra_predio1_idx", columns={"predio_id"}), @ORM\Index(name="fk_infra_sala_infra_classificacao1_idx", columns={"classificacao_id"})})
 * @ORM\Entity
 * @LG\LG(id="sala_id",label="sala_nome")
 * @Jarvis\Jarvis(title="Lista de Prédios",icon="fa fa-table")
 */
class InfraSala
{
    /**
     * @var integer
     *
     * @ORM\Column(name="sala_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="sala_id")
     * @LG\Labels\Attributes(text="Índice",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $salaId;

    /**
     * @var integer
     *
     * @ORM\Column(name="sala_capacidade", type="integer", nullable=false)
     */
    private $salaCapacidade;

    /**
     * @var string
     *
     * @ORM\Column(name="sala_nome", type="string", length=45, nullable=false)
     * @LG\Labels\Property(name="sala_nome")
     * @LG\Labels\Attributes(text="Nome da Sala",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE%")
     */
    private $salaNome;

    /**
     * @var float
     *
     * @ORM\Column(name="sala_largura", type="float", precision=10, scale=0, nullable=true)
     */
    private $salaLargura;

    /**
     * @var float
     *
     * @ORM\Column(name="sala_comprimento", type="float", precision=10, scale=0, nullable=true)
     */
    private $salaComprimento;

    /**
     * @var boolean
     *
     * @ORM\Column(name="sala_reservavel", type="boolean", nullable=true)
     */
    private $salaReservavel = '0';

    /**
     * @var InfraClassificacao
     *
     * @ORM\ManyToOne(targetEntity="InfraClassificacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="classificacao_id", referencedColumnName="classificacao_id")
     * })
     */
    private $classificacao;

    /**
     * @var InfraPredio
     *
     * @ORM\ManyToOne(targetEntity="InfraPredio")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="predio_id", referencedColumnName="predio_id")
     * })
     */
    private $predio;

    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    function getSalaId()
    {
        return $this->salaId;
    }

    function getSalaCapacidade()
    {
        return $this->salaCapacidade;
    }

    function getSalaNome()
    {
        return $this->salaNome;
    }

    function getSalaLargura()
    {
        return $this->salaLargura;
    }

    function getSalaComprimento()
    {
        return $this->salaComprimento;
    }

    function getSalaReservavel()
    {
        return $this->salaReservavel;
    }

    function getClassificacao()
    {
        return $this->classificacao;
    }

    function getPredio()
    {
        return $this->predio;
    }

    function setSalaId($salaId)
    {
        $this->salaId = $salaId;
    }

    function setSalaCapacidade($salaCapacidade)
    {
        $this->salaCapacidade = $salaCapacidade;
    }

    function setSalaNome($salaNome)
    {
        $this->salaNome = $salaNome;
    }

    function setSalaLargura($salaLargura)
    {
        $this->salaLargura = $salaLargura;
    }

    function setSalaComprimento($salaComprimento)
    {
        $this->salaComprimento = $salaComprimento;
    }

    function setSalaReservavel($salaReservavel)
    {
        $this->salaReservavel = $salaReservavel;
    }

    function setClassificacao($classificacao)
    {
        $this->classificacao = $classificacao;
    }

    function setPredio($predio)
    {
        $this->predio = $predio;
    }

    public function toArray()
    {
        $retorno = [
            'salaId'          => $this->getSalaId(),
            'salaNome'        => $this->getSalaNome(),
            'salaLargura'     => $this->getSalaLargura(),
            'salaComprimento' => $this->getSalaComprimento(),
            'classificacao'   => $this->getClassificacao() ? $this->getClassificacao()->getClassificacaoId() : null,
            'predio'          => $this->getPredio() ? $this->getPredio()->getPredioId() : null,
        ];

        if ($this->getClassificacao()) {
            $objClassificacao = $this->getClassificacao();
        } else {
            $objClassificacao = new \Infraestrutura\Entity\InfraClassificacao();
        }

        if ($this->getPredio()) {
            $objPredio = $this->getPredio();
        } else {
            $objPredio = new \Infraestrutura\Entity\InfraPredio();
        }

        $retorno = array_merge(
            $objClassificacao->toArray(),
            $objPredio->toArray(),
            $retorno
        );

        return $retorno;
    }
}
