<?php


namespace Infraestrutura\Controller;

use Infraestrutura\Form\InfraPredio;
use Infraestrutura\Form\InfraSala;
use VersaSpine\Controller\AbstractCoreController;
use Zend\View\Model\ViewModel;

class InfraPredioController extends AbstractCoreController{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function addAction(){
        $view = new ViewModel();
        $request = $this->getRequest();
        if($request->isPost()){
            $dados = $request->getPost()->toArray();

            $this->services()->getService("Infraestrutura\Service\InfraPredio")->adicionar($dados);
            return $this->redirect()->toRoute('infraestrutura/default', ['controller' => 'infra-predio', 'action' => 'index']);
        }
        $option = array();

        $campi = $this->services()->getService('Organizacao\Service\OrgCampus')->getRepository('Organizacao\Entity\OrgCampus')->findAll();
        foreach ($campi as $campus) {
            $campus = $campus->toArray();
            $option['campus'][$campus["campId"]] = $campus['campNome'];
        }

        $classificacao = $this->services()->getService('Infraestrutura\Service\InfraClassificacao')->getRepository('Infraestrutura\Entity\InfraClassificacao')->findAll();
        foreach ($classificacao as $classif) {
            $aux = $classif->toArray();
            $option['classificacao'][$aux['classificacaoId']] = $aux['classificacaoNome'];
        }


        $option['estados'] = $this->services()->getService('Pessoa\Service\Endereco')->buscaEstados();

        $predioForm = new InfraPredio(null, $option);
//        $predioForm->get('predioCep')->removeAttribute('data-masked');
        $salaForm = new InfraSala(null, $option);

        $view->setVariable('predioForm', $predioForm);
        $view->setVariable('salaForm', $salaForm);

        return $view;
    }

    public function editAction()
    {
        $view = new ViewModel();
        $request = $this->getRequest();
        if($request->isPost()){
            try
            {
                $dados = $request->getPost()->toArray();
                $this->services()->getService("Infraestrutura\Service\InfraPredio")->edita($dados);
                return $this->redirect()->toRoute('infraestrutura/default', ['controller' => 'infra-predio', 'action' => 'index']);
            } catch (\Exception $ex)
            {
                if ($ex->getCode() == 0) {
                    $this->flashMessenger()->addErrorMessage("Não foi possível excluir as salas. Porque existem inscrições do vestibular vinculadas à elas.");
                }
                return $this->redirect()->toRoute('infraestrutura/default', ['controller' => 'infra-predio', 'action' => 'index']);
            }
        }
        /**
         * Recebendo o ID pela rota e obtendo a referência deste ID.
         */
        $id = $this->params()->fromRoute('id');
        $service = $this->services()->getService('Infraestrutura\Service\InfraPredio');
        $predio = $service->getReference($id);

        /**
         * Preparando os formulários.
         * Recebendo as informações básicas do formulário.
         */
        $option = array();
        $campi = $this->services()->getService('Organizacao\Service\OrgCampus')->getRepository('Organizacao\Entity\OrgCampus')->findAll();
        foreach ($campi as $campus) {
            $campus = $campus->toArray();
            $option['campus'][$campus["campId"]] = $campus['campNome'];
        }

        $classificacao = $this->services()->getService('Infraestrutura\Service\InfraClassificacao')->getRepository('Infraestrutura\Entity\InfraClassificacao')->findAll();
        foreach ($classificacao as $classif) {
            $aux = $classif->toArray();
            $option['classificacao'][$aux['classificacaoId']] = $aux['classificacaoNome'];
        }

        $option['estados'] = $this->services()->getService('Pessoa\Service\Endereco')->buscaEstados();

        /**
         * Instanciando os formulários e passando para eles os valores de opção.
         */
        $predioForm = new InfraPredio(null, $option);
        $predioForm->setData($predio->toArray());
        $salaForm = new InfraSala(null, $option);

        $arraySalas = $service->buscaJsonSalas($id);

        $view->setVariable('predioForm', $predioForm);
        $view->setVariable('salaForm', $salaForm);
        $view->setVariable('arraySalas', $arraySalas);
        return $view;
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {

    }


    public function optionAction()
    {
        $view = new ViewModel();
        $this->getRequest()->getPost();
        $classificacao = $this->services()->getService('Infraestrutura\Service\InfraClassificacao')->getRepository('Infraestrutura\Entity\InfraClassificacao')->findAll();
        foreach ($classificacao as $classif) {
            $aux = $classif->toArray();
            $option[$aux['classificacaoId']] = $aux['classificacaoNome'];
        }
//        pre($option);
        $view->setTerminal(true);
        $view->setVariable('option', $option);
        return $view;
    }

    public function showFormAction(){
        $this->view->setTerminal(true);
        $form = $this->getForm();
        if ( !class_exists($form) ){
            $this->view->setVariable("erro", "Formulário de busca não foi encontrado!");
            return $this->view;
        }
        $form = new $form;
        $form->remove('camp')
            ->remove('predioEstado')
            ->remove('predioCidade')
            ->remove('predioLogradouro')
            ->remove('predioCep')
            ->remove('predioBairro')
            ->remove('predioNumero');
        $form->setAttribute("method", 'get');
        $this->view->setVariable("config", $this->config);
        $this->view->setVariable("form", $form);
        return $this->view;
    }

}