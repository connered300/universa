<?php
namespace Infraestrutura\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class InfraPredio
 * @package Infraestrutura\Service
 */
class InfraPredio extends AbstractService
{
    /**
     *
     */
    const PREDIO_TIPO_PRINCIPAL = 'principal';
    /**
     *
     */
    const PREDIO_TIPO_APOIO = 'apoio';
    /**
     * @var null
     */
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2PredioTipo($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getPredioTipo());
    }

    /**
     * @return array
     */
    public static function getPredioTipo()
    {
        return array(self::PREDIO_TIPO_PRINCIPAL, self::PREDIO_TIPO_APOIO);
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Infraestrutura\Entity\InfraPredio');
    }

    /**
     * @param $campId
     * @return array
     */
    public function getArrayPredios($campId)
    {
        $arrPrediosRetorno = [];
        $arrPredios        = $this->getRepository()->findBy(['camp' => $campId]);

        /* @var $objInfraPredio \Infraestrutura\Entity\InfraPredio */
        foreach ($arrPredios as $objInfraPredio) {
            $arrPredioRetorno = $objInfraPredio->toArray();
            unset($arrPredioRetorno['camp']);
            $arrPrediosRetorno[] = $arrPredioRetorno;
        }

        return $arrPrediosRetorno;
    }

    /**
     * @param $campId
     * @return \Infraestrutura\Entity\InfraPredio
     */
    public function getPredioPrincipalCampus($campId)
    {
        /* @var $objInfraPredio \Infraestrutura\Entity\InfraPredio */
        $objInfraPredio = $this->getRepository()->findOneBy(
            ['camp' => $campId, 'predioTipo' => self::PREDIO_TIPO_PRINCIPAL]
        );

        return $objInfraPredio;
    }

    /**
     * @param array $dados
     * @return Object
     * @throws \Exception
     */
    public function adicionar(array $dados)
    {
        //        pre($dados);
        parent::begin();
        try {
            $predio          = parent::adicionar($dados);
            $dados['predio'] = $predio;
            (new \Infraestrutura\Service\InfraSala($this->getEm()))->adicionar($dados);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
        }
        parent::commit();

        return $predio;
    }

    /**
     * @param array $dados
     * @throws \Exception
     */
    public function edita($dados)
    {
        parent::begin();
        try {
            $predio          = parent::edita($dados);
            $dados['predio'] = $predio;
            (new \Infraestrutura\Service\InfraSala($this->getEm()))->edita($dados);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
        }
        parent::commit();
    }

    /**
     * @return mixed
     */
    public function mountArrayPredios()
    {
        try {
            $predios = $this->getRepository("Infraestrutura\Entity\InfraPredio")->findAll();

            foreach ($predios as $predio) {
                $arrayPredios[$predio->getPredioId()] = $predio->getPredioNome();
            }

            return $arrayPredios;
        } catch (\Exception $ex) {
            die($ex->getMessage());
        }
    }

    /**
     * Busca pelas salas presente naquel prédio e retorna um json com as informações das salas.
     * */
    public function buscaJsonSalas($id)
    {
        $salas = $this->executeQuery("SELECT * FROM infra_sala WHERE predio_id = '{$id}'");
        foreach ($salas as $key => $sala) {
            $arraySalas[$key] = $sala;
        }

        //        pre($arraySalas);
        return \Zend\Json\Json::encode($arraySalas);
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['predioNome']) {
            $errors[] = 'Por favor preencha o campo "nome"!';
        }

        if (!$arrParam['campId']) {
            $errors[] = 'Por favor preencha o campo "câmpus"!';
        }

        if (!$arrParam['predioTipo']) {
            $errors[] = 'Por favor preencha o campo "tipo"!';
        }

        if (!in_array($arrParam['predioTipo'], self::getPredioTipo())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "tipo"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function pesquisaForJson($params)
    {
        $sql        = '
       SELECT *,
            predio_logradouro endLogradouro ,
            predio_numero endNumero,
            predio_bairro endBairro ,
            predio_cidade endCidade ,
            predio_estado endEstado ,
            predio_cep endCep,
            predio_nome nome
        FROM selecao_locais
        NATURAL JOIN infra_predio
        WHERE ';
        $predioNome = false;
        $predioId   = false;

        if ($params['q']) {
            $predioNome = $params['q'];
        } elseif ($params['query']) {
            $predioNome = $params['query'];
        }

        if ($params['predio_id']) {
            $predioId = $params['predio_id'];
        }

        $parameters = array('predio_nome' => "{$predioNome}%");
        $sql .= ' predio_nome LIKE :predio_nome ';

        if ($predioId) {
            $parameters['predio_id'] = $predioId;
            $sql .= ' AND predio_id <> :predio_id ';
        }

        if ($params['campus']) {
            $sql .= " AND camp_id in ( :campId ) ";
            $parameters['campId'] = $params['campus'];
        }

        $sql .= " GROUP BY  predio_id";
        $sql .= " ORDER BY predio_nome ";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    /**
     * @param array $post
     * @return array
     */
    public function resultSearch($post)
    {
        $query = "
          SELECT
            infra_predio.*, camp_nome as camp_id
          FROM
            infra_predio
          INNER JOIN
            org_campus ON org_campus.camp_id = infra_predio.camp_id
          WHERE
            predio_nome LIKE '%" . $post['predioNome'] . "%'
          ";

        $dados = $this->executeQuery($query)->fetchAll();

        return array(
            'dados' => $dados,
            'count' => count($dados)
        );
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('predioId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();

        /* @var $objEntity \Infraestrutura\Entity\InfraPredio */
        foreach ($arrEntities as $objEntity) {
            $endereco = '';
            $endereco .= $objEntity->getPredioLogradouro();
            $endereco .= ', ' . $objEntity->getPredioNumero();
            $endereco .= ', ' . $objEntity->getPredioBairro();
            $endereco .= ', ' . $objEntity->getPredioCidade();
            $endereco .= ' - ' . $objEntity->getPredioEstado();
            $endereco = preg_replace('/([\s,-] )([\s,-] ){1,}/', '$1', $endereco);

            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getPredioId(),
                $params['value'] => ($objEntity->getPredioNome() . ' / ' . $endereco)
            );
        }

        return $arrEntitiesArr;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceOrgCampus = new \Organizacao\Service\OrgCampus($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['predioId']) {
                $objInfraPredio = $this->getRepository()->find($arrDados['predioId']);

                if (!$objInfraPredio) {
                    $this->setLastError('Registro de predio não existe!');

                    return false;
                }
            } else {
                $objInfraPredio = new \Infraestrutura\Entity\InfraPredio();
            }

            if ($arrDados['campId']) {
                $objOrgCampus = $serviceOrgCampus->getRepository()->find($arrDados['campId']);

                if (!$objOrgCampus) {
                    $this->setLastError('Registro de câmpus não existe!');

                    return false;
                }

                $objInfraPredio->setCamp($objOrgCampus);
            }

            $objInfraPredio->setPredioNome($arrDados['predioNome']);
            $objInfraPredio->setPredioNumero($arrDados['predioNumero']);
            $objInfraPredio->setPredioCep($arrDados['predioCep']);
            $objInfraPredio->setPredioBairro($arrDados['predioBairro']);
            $objInfraPredio->setPredioCidade($arrDados['predioCidade']);
            $objInfraPredio->setPredioEstado($arrDados['predioEstado']);
            $objInfraPredio->setPredioLogradouro($arrDados['predioLogradouro']);
            $objInfraPredio->setPredioTipo($arrDados['predioTipo']);

            $this->getEm()->persist($objInfraPredio);
            $this->getEm()->flush($objInfraPredio);

            $this->getEm()->commit();

            $arrDados['predioId'] = $objInfraPredio->getPredioId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de predio!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param array                         $arrParam
     * @param \Organizacao\Entity\OrgCampus $objorgCampus
     * @return bool
     */
    public function salvarMultiplos(array &$arrParam, \Organizacao\Entity\OrgCampus $objorgCampus)
    {
        $arrExcluir = array();

        try {
            $this->getEm()->beginTransaction();

            $arrInfraPredio   = json_decode($arrParam['predios'], true);
            $arrInfraPredioDB = $this->getRepository()->findBy(
                ['camp' => $objorgCampus->getCampId()]
            );

            /* @var $objInfraPredio \Infraestrutura\Entity\InfraPredio */
            foreach ($arrInfraPredioDB as $objInfraPredio) {
                $encontrado = false;
                $predioId   = $objInfraPredio->getPredioId();

                foreach ($arrInfraPredio as $arrPredio) {
                    if ($predioId == $arrPredio['predioId']) {
                        $encontrado = true;
                        break;
                    }
                }

                if (!$encontrado) {
                    $arrExcluir[$predioId] = $objInfraPredio;
                }
            }

            if ($arrExcluir) {
                foreach ($arrExcluir as $objInfraPredio) {
                    $this->getEm()->remove($objInfraPredio);
                    $this->getEm()->flush();
                }
            }

            foreach ($arrInfraPredio as $arrPredio) {
                $arrPredio['campId'] = $objorgCampus->getCampId();

                if (!$this->save($arrPredio)) {
                    throw new \Exception($this->getLastError());
                }
            }

            $this->getEm()->commit();

            return true;
        } catch (\Exception $e) {
            $this->getEm()->rollback();
            $this->setLastError('Não foi possível salvar o registros de prédios!<br>' . $e->getMessage());
        }

        return false;
    }
}