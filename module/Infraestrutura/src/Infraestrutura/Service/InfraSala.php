<?php


namespace Infraestrutura\Service;

use VersaSpine\Service\AbstractService;

class InfraSala extends AbstractService{
    public function __construct(\Doctrine\ORM\EntityManager $em){
        parent::__construct($em, "Infraestrutura\Entity\InfraSala");
    }

    public function adicionar($dados)
    {
        parent::begin();
        try{
            foreach ($dados['salaNome'] as $key => $value)
            {
                $sala = [
                    'predio'          => $dados['predio']->getPredioId(),
                    'salaNome'        => $value,
                    'classificacao'   => $dados['classificacao'][$key],
                    'classificacaoId' => $dados['classificacao'][$key],
                    'salaCapacidade'  => $dados['salaCapacidade'][$key],
                    'salaReservavel'  => $dados['reservavel'][$key],
                    'salaLargura'     => ($dados['salaLargura'][$key] == null)? '0' : $dados['salaLargura'][$key],
                    'salaComprimento' => ($dados['salaComprimento'][$key] == null )? '0': $dados['salaComprimento'][$key],
                    'salaId'          => ''
                ];

                $sala = parent::adicionar($sala);
            }
        } catch (\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
        }
        parent::commit();
        return $sala;
    }

    public function edita($dados)
    {
        parent::begin();
        try{
            $salasNoPredio = $this->executeQuery("SELECT sala_id FROM infra_sala WHERE predio_id = '{$dados['predioId']}'")->fetchAll();
            foreach ($salasNoPredio as $sala)
            {
                $arraySalasPredio[] = $sala['sala_id'];
            }
            $salasNovas = array_diff($dados['salaId'], $arraySalasPredio);
            if ($salasNovas) {
                foreach($salasNovas as $key => $sala){
                    $salaAdd = [
                        'salaId' => '',
                        'classificacao' => $dados['classificacao'][$key],
                        'salaCapacidade' => $dados['salaCapacidade'][$key],
                        'salaNome'  => $dados['salaNome'][$key],
                        'salaReservavel' => $dados['reservavel'][$key],
                        'salaLargura'     => ($dados['salaLargura'][$key] == null)? '0' : $dados['salaLargura'][$key],
                        'salaComprimento' => ($dados['salaComprimento'][$key] == null )? '0': $dados['salaComprimento'][$key],
                        'predio' => $dados['predioId'],
                    ];
                    parent::adicionar($salaAdd);
                }
            }
            $salasEditar = array_diff($dados['salaId'], $salasNovas);
            if ($salasEditar){
                foreach($salasEditar as $key => $sala){
                    $salaEdit = [
                        'salaId' => $dados['salaId'][$key],
                        'classificacao' => $dados['classificacao'][$key],
                        'salaCapacidade' => $dados['salaCapacidade'][$key],
                        'predio' => $dados['predioId'],
                        'salaNome'  => $dados['salaNome'][$key],
                        'salaReservavel' => $dados['reservavel'][$key],
                        'salaLargura'     => ($dados['salaLargura'][$key] == null)? '0' : $dados['salaLargura'][$key],
                        'salaComprimento' => ($dados['salaComprimento'][$key] == null )? '0': $dados['salaComprimento'][$key]
                    ];
                    parent::edita($salaEdit);
                }
            }
            $salasExcluir = array_diff($arraySalasPredio, $dados['salaId']);
            if($salasExcluir){
                foreach ($salasExcluir as $key => $sala) {
                    parent::excluir($sala);
                }
            }

        } catch (\Exception $ex){
            throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
        }
        parent::commit();
        return $sala;
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {

    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {

    }

}