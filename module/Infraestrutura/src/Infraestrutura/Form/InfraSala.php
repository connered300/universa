<?php


namespace Infraestrutura\Form;

use Zend\Form\Form,
    Zend\Form\Element;

class InfraSala extends Form{
    public function __construct($name = null, array $option)
    {
        parent::__construct($name);
        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', '');
        $this->setAttribute('role', 'form');


        $salaId = new Element\Hidden('salaId');
        $this->add($salaId);

        $predioId = new Element\Hidden('predioId');
        $this->add($predioId);

        $classificacao = new Element\Select('classificacao');
        $classificacao->setValueOptions($option['classificacao']);
        $classificacao->setAttributes([
            'col'      => '6',
            'label'    => 'Classificaçao da Sala:',
        ]);
        $this->add($classificacao);

        $salaNome = new Element\Text('salaNome');
        $salaNome->setAttributes([
            'col'    => '6',
            'label' => 'Nome:',
            'placeholder' => 'Digite o nome da sala'
        ]);
        $this->add($salaNome);

        $salaCapacidade = new Element\Text('salaCapacidade');
        $salaCapacidade->setAttributes([
            'col'   => '3',
            'label' => 'Capacidade:',
            'placeholder' => 'Digite a capacidade',
            'data-required' => 'onlyNumber'
        ]);
        $this->add($salaCapacidade);

        $largura = new Element\Text('salaLargura');
        $largura->setAttributes([
            'col'             => '3',
            'label'           => 'Largura',
            'data-validations' => 'Integer',
            'placeholder' => 'Digite a largura'
        ]);
        $this->add($largura);

        $comprimento = new Element\Text('salaComprimento');
        $comprimento->setAttributes([
            'col'   => '3',
            'label' => 'Comprimento',
            'placeholder' => 'Digite o Comprimento '
        ]);
        $this->add($comprimento);

        $reservavel = new \VersaSpine\Form\SmartAdmin\Elements\AlternateOptions('salaReservavel');
        $reservavel->setAttributes([
            'col'   => '3',
            'label' => 'Disponível para Reserva',
            'toggle_type' => 'radio',
            'wrap' => true,
        ]);
        $reservavel->setValueOptions([
            'Disponível' => array('on'=>'Sim','off'=>'Não','value'=>'Sim'),
            'Indisponível' => array('on'=>'Sim','off'=>'Não','value'=>'Não'),
        ]);
        $this->add($reservavel);
    }
}