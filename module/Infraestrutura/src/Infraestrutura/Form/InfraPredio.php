<?php


namespace Infraestrutura\Form;

use Zend\Form\Form,
    Zend\Form\Element;


class InfraPredio extends Form{
    public function __construct($name = null, $option = array())
    {
        parent::__construct($name);
        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', '');
        $this->setAttribute('role', 'form');
        $this->setAttribute('id', 'cadastraPredio');

        $predioId = new Element\Hidden('predioId');
        $this->add($predioId);

        $end = new Element\Hidden('end');
        $this->add($end);

        $camp = new Element\Select('camp');
        $camp->setAttributes([
            'col'      => '4',
            'label'    => 'Câmpus',
            'wrap'     => true,
            'required' => true,
            'data-validations' => 'Required'
        ]);
        if($option['campus']){
            $camp->setValueOptions($option['campus']);
        }
        $this->add($camp);

        $nome = new Element\Text('predioNome');
        $nome->setAttributes([
            'label'    => 'Nome do Prédio',
            'data-validations' => 'Required',
            'wrap'     => true,
            'col'      => '4',
            'placeholder' => 'Digite o nome do prédio'
        ]);
        $this->add($nome);

        $cadastrar = new Element\Submit('cadastrar');
        $cadastrar->setAttributes([
            'class' => 'btn btn-primary pull-right',
            'value' => 'Cadastrar'
        ]);
        $this->add($cadastrar);
        /*
         * Endereço
         * */
        $predioEstado = new Element\Select('predioEstado');
        $predioEstado->setAttributes(array(
            'col' => 3,
            'label' => 'Estado',
            'icon' => 'icon-prepend fa fa-road',
            'data-validations' => 'Required',
        ));
        if($option['estados']){
            $predioEstado->setValueOptions($option['estados']);
        }
        $this->add($predioEstado);

        $predioCidade = new Element\Text('predioCidade');
        $predioCidade->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Informe a Cidade',
            'label' => 'Cidade',
            'icon' => 'icon-prepend fa fa-user',
            'data-validations' => 'Required',
        ));
        $this->add($predioCidade);

        $predioBairro = new Element\Text('predioBairro');
        $predioBairro->setAttributes(array(
            'placeholder' => 'Informe o Bairro',
            'label' => 'Bairro',
            'icon' => 'icon-prepend fa fa-info-circle',
//            'data-validations' => 'Required',
        ));
        $this->add($predioBairro);

        $predioLogradouro = new Element\Text('predioLogradouro');
        $predioLogradouro->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Informe o Logradouro',
            'label' => 'Logradouro',
            'icon' => 'icon-prepend fa fa-road',
//            'data-validations' => 'Required',
        ));
        $this->add($predioLogradouro);

        $predioCep = new Element\Text('predioCep');
        $predioCep->setAttributes(array(
            'col' => 2,
            'placeholder' => 'Informe o CEP',
            'label' => 'CEP:',
        ));
        $this->add($predioCep);

        $predioNumero = new Element\Text('predioNumero');
        $predioNumero->setAttributes(array(
            'label' => 'Número',
            'placeholder' => 'N°',
            'icon' => 'icon-prepend fa fa-home',
        ));
        $this->add($predioNumero);
    }
}