<?php
namespace Api;

use Zend\Http\Response;
use Zend\Json\Json;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\JsonModel;

class Module
{
    private $integracaoId;
    private $token;

    /**
     * @return mixed
     */
    public function getIntegracaoId()
    {
        return $this->integracaoId;
    }

    /**
     * @param mixed $integracaoId
     * @return Module
     */
    public function setIntegracaoId($integracaoId)
    {
        $this->integracaoId = $integracaoId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     * @return Module
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function onBootstrap(MvcEvent $e)
    {
        $app = $e->getApplication();
        $em  = $app->getEventManager();
        $sm  = $app->getServiceManager();
        $se  = $em->getSharedManager();

        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($em);

        $se->attach(
            __NAMESPACE__,
            MvcEvent::EVENT_DISPATCH,
            function (MvcEvent $e) use ($sm) {
                $em             = $sm->get('Doctrine\ORM\EntityManager');
                $token          = $_SERVER['PHP_AUTH_USER'];
                $tokenValidacao = $_SERVER['PHP_AUTH_PW'];

                $serviceSisIntegracao = new \Sistema\Service\SisIntegracao($em);
                $objSisIntegracao     = $serviceSisIntegracao->validaCredenciais($token);

                if ($token != $tokenValidacao || !$objSisIntegracao) {
                    /** @var $response \Zend\Http\Response */
                    $response = $e->getResponse();

                    $e->stopPropagation(true);

                    $json = new Json();
                    $view = $json->encode(array('error' => true, 'message' => 'Credenciais inválidas!'));
                    $view = $json->prettyPrint($view);

                    $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
                    $response->setContent($view);
                    $response->setStatusCode(403);

                    return $response;
                } else {
                    $this->setToken($token);
                    $this->setIntegracaoId($objSisIntegracao->getIntegracaoId());
                }
            },
            100
        );
    }

    public function onDispatchError($e)
    {
        return $this->getJsonModelError($e);
    }

    public function onRenderError($e)
    {
        return $this->getJsonModelError($e);
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'getModule' => function () {
                    return $this;
                },
            ),

        );
    }

    public function getJsonModelError($e)
    {
        $error = $e->getError();

        if (!$error) {
            return;
        }

        $response      = $e->getResponse();
        $exception     = $e->getParam('exception');
        $exceptionJson = array();

        if ($exception) {
            $exceptionJson = array(
                'class'      => get_class($exception),
                'file'       => $exception->getFile(),
                'line'       => $exception->getLine(),
                'message'    => $exception->getMessage(),
                'stacktrace' => $exception->getTraceAsString()
            );
        }

        $errorJson = array(
            'message'   => 'An error occurred during execution; please try again later.',
            'error'     => $error,
            'exception' => $exceptionJson,
        );

        if ($error == 'error-router-no-match') {
            $errorJson['message'] = 'Resource not found.';
        }

        $model = new JsonModel(array('errors' => array($errorJson)));
        $e->setResult($model);

        return $model;
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
}
