<?php

namespace Api\Controller;

use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class AlunoController extends AbstractRestfulController
{
    protected $allowedCollectionMethods = array(
        'GET',
        'POST',
    );

    protected $allowedResourceMethods = array(
        'GET',
        'PATCH',
        'PUT',
        'DELETE',
    );

    public function setEventManager(EventManagerInterface $events)
    {
        parent::setEventManager($events);
        $events->attach('dispatch', array($this, 'checkOptions'), 10);
    }

    public function checkOptions($e)
    {
        $matches  = $e->getRouteMatch();
        $response = $e->getResponse();
        $request  = $e->getRequest();
        $method   = $request->getMethod();

        // test if we matched an individual resource, and then test
        // if we allow the particular request method
        if ($matches->getParam('id', false)) {
            if (!in_array($method, $this->allowedResourceMethods)) {
                $response->setStatusCode(405);

                return $response;
            }

            return;
        }

        // We matched a collection; test if we allow the particular request
        // method
        if (!in_array($method, $this->allowedCollectionMethods)) {
            $response->setStatusCode(405);

            return $response;
        }
    }

    /**
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected function getServiceManager()
    {
        return $this->getEvent()->getApplication()->getServiceManager();
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    protected function getEntityManager()
    {
        return $this->getServiceManager()->get('Doctrine\ORM\EntityManager');
    }

    public function create($data)
    {
        $serviceAcadgeralAluno = new \Matricula\Service\AcadgeralAluno($this->getEntityManager());

        $error = false;

        if ($serviceAcadgeralAluno->salvarAluno($data)) {
            $error = true;
        }

        return new JsonModel(
            [
                'error'   => $error,
                'message' => $serviceAcadgeralAluno->getLastError(),
                'aluno'   => $data
            ]
        );
    }

    public function delete($id)
    {
        $serviceAcadgeralAluno = new \Matricula\Service\AcadgeralAluno($this->getEntityManager());

        $error = false;

        if (!$serviceAcadgeralAluno->remover(['alunoId' => $id])) {
            $error = true;
        }

        return new JsonModel(
            [
                'error'   => $error,
                'message' => $serviceAcadgeralAluno->getLastError(),
            ]
        );
    }

    public function changeArrayKey($data)
    {
        foreach($data as $index => $value){
            if(preg_match('/aluno/',$index)) {
                $novoIndex = str_replace('aluno', '', $index);

                if($novoIndex == 'Id'){
                    $novoIndex = 'Matrícula';
                }

                $data[$novoIndex] = $data[$index];
                unset($data[$index]);
            }elseif(preg_match('/pes/',$index)) {
                $novoIndex = str_replace('pes', '', $index);

                if($novoIndex = 'Id'){
                    $novoIndex = 'Pessoa';
                }

                $data[$novoIndex] = $data[$index];
                unset($data[$index]);
            }elseif(preg_match('/end/',$index)) {
                $novoIndex = str_replace('end', '', $index);
                $data[$novoIndex] = $data[$index];
                unset($data[$index]);
            }elseif(preg_match('/con/',$index)) {
                $novoIndex = str_replace('con', '', $index);
                $data[$novoIndex] = $data[$index];
                unset($data[$index]);
            }elseif(preg_match('/formacao/',$index)) {
                $novoIndex = str_replace('formacao', '', $index);
                $data[$novoIndex] = $data[$index];
                unset($data[$index]);
            }
        }

        foreach($data['documentoGrupo'] as $index => $value){
            $dataEntrega = (array) $value['docpessoaEntrega'];
            $status = $data['documentoGrupo'][$index]['docpessoaStatus'] == "Sim"? "Entregue" : "Não Entregue";
            $documento = $data['documentoGrupo'][$index]['tdocDescricao'];
            unset($data['documentoGrupo'][$index]);
            $data['documentoGrupo'][$index] = ['situacao' => $status, 'data' => (new \DateTime($dataEntrega['date']))->format("d/m/Y"), 'documento' => $documento];
        }

        foreach($data['formacoes'] as $index => $value){
            unset($data['formacoes'][$index]['formacaoId']);
            unset($data['formacoes'][$index]['alunoId']);
            unset($data['formacoes'][$index]['pesId']);
            unset($data['formacoes'][$index]['pesNome']);
        }

        foreach($data['Curso'] as $index => $value){
            unset($data['Curso'][$index]['alunocursoId']);
            unset($data['Curso'][$index]['alunoId']);
            unset($data['Curso'][$index]['alunoId']);
            unset($data['Curso'][$index]['pesId']);
            unset($data['Curso'][$index]['pesNome']);
            unset($data['Curso'][$index]['cursocampusId']);
            unset($data['Curso'][$index]['cursoId']);
            unset($data['Curso'][$index]['campId']);
            unset($data['Curso'][$index]['tiposelId']);
        }

        return $data;
    }

    public function modeling($id){
        $serviceAcadgeralAluno = new \Matricula\Service\AcadgeralAluno($this->getEntityManager());

        $data  = $serviceAcadgeralAluno->getArray($id);
        $serviceAcadgeralAluno->formataDadosPost($data);
        $result = $this->changeArrayKey($data);

        return $result;
    }

    public function get($id)
    {
        $serviceAcadgeralAluno = new \Matricula\Service\AcadgeralAluno($this->getEntityManager());

        $data  = $this->modeling($id);
        $error = !$data;

        return new JsonModel(
            [
                'error'   => $error,
                'message' => $serviceAcadgeralAluno->getLastError(),
                'aluno'   => $data,
            ]
        );
    }

}