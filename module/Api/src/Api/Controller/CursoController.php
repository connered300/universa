<?php

namespace Api\Controller;

use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class CursoController extends AbstractRestfulController
{
    protected $allowedCollectionMethods = array(
        'GET',
        'POST',
    );

    protected $allowedResourceMethods = array(
        'GET',
        'PATCH',
        'PUT',
        'DELETE',
    );

    public function setEventManager(EventManagerInterface $events)
    {
        parent::setEventManager($events);
        $events->attach('dispatch', array($this, 'checkOptions'), 10);

    }

    public function checkOptions($e)
    {
        $matches  = $e->getRouteMatch();
        $response = $e->getResponse();
        $request  = $e->getRequest();
        $method   = $request->getMethod();

        // test if we matched an individual resource, and then test
        // if we allow the particular request method
        if ($matches->getParam('id', false)) {
            if (!in_array($method, $this->allowedResourceMethods)) {
                $response->setStatusCode(405);

                return $response;
            }

            return;
        }

        // We matched a collection; test if we allow the particular request
        // method
        if (!in_array($method, $this->allowedCollectionMethods)) {
            $response->setStatusCode(405);

            return $response;
        }
    }

    /**
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected function getServiceManager()
    {
        return $this->getEvent()->getApplication()->getServiceManager();
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    protected function getEntityManager()
    {
        return $this->getServiceManager()->get('Doctrine\ORM\EntityManager');
    }

    public function create($data)
    {
        $serviceAcadCurso = new \Matricula\Service\AcadCurso($this->getEntityManager());

        $error = false;

        if ($serviceAcadCurso->salvarCurso($data)) {
            $error = true;
        }

        return new JsonModel(
            [
                'error'   => $error,
                'message' => $serviceAcadCurso->getLastError(),
                'curso'   => $data
            ]
        );
    }

    public function delete($id)
    {
        $serviceAcadCurso = new \Matricula\Service\AcadCurso($this->getEntityManager());

        $error = false;

        if ($serviceAcadCurso->remover(['cursoId' => $id])) {
            $error = true;
        }

        return new JsonModel(
            [
                'error'   => $error,
                'message' => $serviceAcadCurso->getLastError(),
            ]
        );
    }

    public function get($id)
    {
        $serviceAcadCurso = new \Matricula\Service\AcadCurso($this->getEntityManager());

        $data  = $serviceAcadCurso->getArray($id);
        $error = !$data;

        return new JsonModel(
            [
                'error'   => $error,
                'message' => $serviceAcadCurso->getLastError(),
                'curso'   => $data,
            ]
        );
    }

    public function getList()
    {
        $serviceAcadCurso = new \Matricula\Service\AcadCurso($this->getEntityManager());

        $offset = (int)$this->params()->fromQuery('offset', 0);
        $limit  = (int)$this->params()->fromQuery('limit', 10);

        $data  = $serviceAcadCurso->retornaTodosOsRegistros($offset, $limit);
        $error = !$data;

        return new JsonModel(
            [
                'error'   => $error,
                'message' => $serviceAcadCurso->getLastError(),
                'limit'   => $limit,
                'offset'  => $offset,
                'total'   => $serviceAcadCurso->retornaTotalDeRegistros(),
                'cursos'  => $data,
            ]
        );
    }

    public function update($id, $data)
    {
        if ($id != $data['cursoId']) {
            throw new \Exception();
        }

        $serviceAcadCurso = new \Matricula\Service\AcadCurso($this->getEntityManager());

        $error = false;

        if ($serviceAcadCurso->salvarCurso($data)) {
            $error = true;
        }

        return new JsonModel(
            [
                'error'   => $error,
                'message' => $serviceAcadCurso->getLastError(),
                'curso'   => $data
            ]
        );
    }
}