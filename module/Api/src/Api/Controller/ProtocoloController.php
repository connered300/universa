<?php

namespace Api\Controller;

use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class ProtocoloController extends AbstractRestfulController
{
    protected $allowedCollectionMethods = array(
        'GET',
        'POST',
    );

    protected $allowedResourceMethods = array(
        'GET',
        'PATCH',
        'PUT',
        'DELETE',
    );

    public function setEventManager(EventManagerInterface $events)
    {
        parent::setEventManager($events);
        $events->attach('dispatch', array($this, 'checkOptions'), 10);
    }

    public function checkOptions($e)
    {
        $matches  = $e->getRouteMatch();
        $response = $e->getResponse();
        $request  = $e->getRequest();
        $method   = $request->getMethod();

        // test if we matched an individual resource, and then test
        // if we allow the particular request method
        if ($matches->getParam('id', false)) {
            if (!in_array($method, $this->allowedResourceMethods)) {
                $response->setStatusCode(405);

                return $response;
            }

            return;
        }

        // We matched a collection; test if we allow the particular request
        // method
        if (!in_array($method, $this->allowedCollectionMethods)) {
            $response->setStatusCode(405);

            return $response;
        }
    }

    /**
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected function getServiceManager()
    {
        return $this->getEvent()->getApplication()->getServiceManager();
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    protected function getEntityManager()
    {
        return $this->getServiceManager()->get('Doctrine\ORM\EntityManager');
    }

    public function create($data)
    {
        $serviceProtocolo    = new \Protocolo\Service\Protocolo($this->getEntityManager());
        $serviceSis = new \Sistema\Service\SisIntegracaoAluno($this->getEntityManager());

        $module = $this->getServiceLocator()->get('getModule');

        if (!$data) {
            $data = array_merge($this->getRequest()->getPost()->toArray(), $this->getRequest()->getQuery()->toArray());
        }

        $data = array_merge($data, $this->params()->fromFiles());

        if($data['SK']){
            $data = array_merge($serviceSis->retornaDadosDeTokenCriptografado($data['SK']),$data);
        }else{
            $data['integracaoId'] = $module->getIntegracaoId();
        }

        $error = false;

        if (!$serviceProtocolo->save($data)) {
            $error = true;
        }

        return new JsonModel(
            [
                'error'   => $error,
                'message' => $serviceProtocolo->getLastError(),
                'aluno'   => $data
            ]
        );
    }

    public function get($id)
    {
        $service = new \Protocolo\Service\Protocolo($this->getEntityManager());

        $data  = $service->retornaProtocoloPeloId($id);
        $error = !$data;

        return new JsonModel(
            [
                'error'   => $error,
                'message' => $service->getLastError(),
                'aluno'   => $data,
            ]
        );
    }
}