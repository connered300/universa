<?php

namespace Api\Controller;

use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Protocolo\Service\ProtocoloMensagem;

class ProtocoloMensagemController extends AbstractRestfulController
{
    protected $allowedCollectionMethods = array(
        'GET',
        'POST',
    );

    protected $allowedResourceMethods = array(
        'GET',
        'PATCH',
        'PUT',
        'DELETE',
    );

    public function setEventManager(EventManagerInterface $events)
    {
        parent::setEventManager($events);
        $events->attach('dispatch', array($this, 'checkOptions'), 10);
    }

    public function checkOptions($e)
    {
        $matches  = $e->getRouteMatch();
        $response = $e->getResponse();
        $request  = $e->getRequest();
        $method   = $request->getMethod();

        // test if we matched an individual resource, and then test
        // if we allow the particular request method
        if ($matches->getParam('id', false)) {
            if (!in_array($method, $this->allowedResourceMethods)) {
                $response->setStatusCode(405);

                return $response;
            }

            return;
        }

        // We matched a collection; test if we allow the particular request
        // method
        if (!in_array($method, $this->allowedCollectionMethods)) {
            $response->setStatusCode(405);

            return $response;
        }
    }

    /**
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected function getServiceManager()
    {
        return $this->getEvent()->getApplication()->getServiceManager();
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    protected function getEntityManager()
    {
        return $this->getServiceManager()->get('Doctrine\ORM\EntityManager');
    }

    public function create($dados)
    {
        $service = new ProtocoloMensagem($this->getEntityManager());
        $request = $this->getRequest();
        $error   = false;

        if (!$dados) {
            $dados = array_merge($request->getPost()->toArray(), $request->getQuery()->toArray());
        }

        $dados = array_merge($dados, $this->params()->fromFiles());

        if (!$service->save($dados)) {
            $error = true;
        }

        return new JsonModel(
            [
                'error'   => $error,
                'message' => $service->getLastError(),
                'dados'   => $dados
            ]
        );
    }

    public function delete($id)
    {
        $serviceAcadgeralAluno = new \Matricula\Service\AcadgeralAluno($this->getEntityManager());

        $error = false;

        if (!$serviceAcadgeralAluno->remover(['alunoId' => $id])) {
            $error = true;
        }

        return new JsonModel(
            [
                'error'   => $error,
                'message' => $serviceAcadgeralAluno->getLastError(),
            ]
        );
    }

    public function getList()
    {
        $request = $this->getRequest();
        $dados   = array_merge($request->getQuery()->toArray(), $request->getPost()->toArray());

        if ($dados) {
            return $this->create($dados);
        }

        return false;
    }

}