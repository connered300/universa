<?php

namespace Api\Controller;

use Matricula\Service\AcadgeralDisciplinaCurso;
use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Sistema\Service\SisIntegracaoAluno AS SisIntegracao;
use Matricula\Service\AcadperiodoAluno AS AcadperiodoAluno;
use Financeiro\Service\FinanceiroTituloTipo AS FinanceiroTituloTipo;

class AlunoVerificaRematriculaController extends AbstractRestfulController
{
    protected $allowedCollectionMethods = array(
        'GET',
        'POST',
    );

    protected $allowedResourceMethods = array(
        'GET',
        'PATCH',
        'PUT',
        'DELETE',
    );

    public function setEventManager(EventManagerInterface $events)
    {
        parent::setEventManager($events);
        $events->attach('dispatch', array($this, 'checkOptions'), 10);
    }

    public function checkOptions($e)
    {
        $matches  = $e->getRouteMatch();
        $response = $e->getResponse();
        $request  = $e->getRequest();
        $method   = $request->getMethod();

        // test if we matched an individual resource, and then test
        // if we allow the particular request method
        if ($matches->getParam('id', false)) {
            if (!in_array($method, $this->allowedResourceMethods)) {
                $response->setStatusCode(405);

                return $response;
            }

            return;
        }

        // We matched a collection; test if we allow the particular request
        // method
        if (!in_array($method, $this->allowedCollectionMethods)) {
            $response->setStatusCode(405);

            return $response;
        }
    }

    /**
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected function getServiceManager()
    {
        return $this->getEvent()->getApplication()->getServiceManager();
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    protected function getEntityManager()
    {
        return $this->getServiceManager()->get('Doctrine\ORM\EntityManager');
    }

    public function get($alunoCodigo)
    {
        $serviceSisIntegracao        = new SisIntegracao($this->getEntityManager());
        $serviceAcadDisciplinaCurso  = new AcadgeralDisciplinaCurso($this->getEntityManager());
        $serviceAcadperiodo          = new AcadperiodoAluno($this->getEntityManager());
        $serviceFinanceiroTituloTipo = new FinanceiroTituloTipo($this->getEntityManager());

        $error         = false;
        $arrDadosAluno = [];
        $erroMsg       = '';

        if ($alunoCodigo) {
            $arrDadosAluno = $serviceSisIntegracao->buscaInformacoesAlunoPer($alunoCodigo);

            if ($arrDadosAluno) {
                foreach ($arrDadosAluno as $chave => $dado) {
                    $arrDadosAluno[$chave]['formaDePagamento'] = [];
                    $arrDadosAluno[$chave]['disciplina']       = [];

                    $arrDadosAluno[$chave]['disciplina'] = $serviceAcadDisciplinaCurso->retornaDisciplinaCursoPelaMatrizSerie(
                        $dado['mat_cur_id'],
                        $dado['turma_serie'],
                        $dado['turma_id'],
                        false
                    );

                    //TODO: analisar esta regra
                    $arrDadosAluno[$chave]['formaDePagamento'][] = $serviceAcadperiodo->buscaConfTituloMatricula(
                        $dado['per_id'],
                        $serviceFinanceiroTituloTipo::TIPOTITULO_MENSALIDADE,
                        $dado['cursocampus_id']
                    );
                }
            }
        } else {
            $erroMsg = " Não foi informado ou código do aluno inválido! ";
        }

        if (!$arrDadosAluno) {
            if ($lastErrorSisIntegracao = $serviceSisIntegracao->getLastError()) {
                $erroMsg .= ' Erro Integração: ' . $lastErrorSisIntegracao;
            }

            if ($lastErrorFinanceiro = $serviceFinanceiroTituloTipo->getLastError()) {
                $erroMsg .= ' Erro Financeiro: ' . $lastErrorFinanceiro;
            }

            if ($lastErrorDiscCurso = $serviceAcadDisciplinaCurso->getLastError()) {
                $erroMsg .= " Erro Disciplina: " . $lastErrorDiscCurso;
            }

            $error   = true;
            $erroMsg = $erroMsg ? $erroMsg : "Nenhum dado localzado!";
        }

        return new JsonModel(
            [
                'error'   => $error,
                'message' => $erroMsg,
                'dados'   => $arrDadosAluno
            ]
        );
    }

}