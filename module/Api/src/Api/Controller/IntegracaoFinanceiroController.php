<?php

namespace Api\Controller;

use Sistema\Service\SisConfig;
use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class IntegracaoFinanceiroController extends AbstractRestfulController
{
    protected $allowedCollectionMethods = array(
        'GET',
        'POST',
    );

    protected $allowedResourceMethods = array(
        'GET',
        'PATCH',
        'PUT',
        'DELETE',
    );

    public function setEventManager(EventManagerInterface $events)
    {
        parent::setEventManager($events);
        $events->attach('dispatch', array($this, 'checkOptions'), 10);
    }

    public function checkOptions($e)
    {
        $matches  = $e->getRouteMatch();
        $response = $e->getResponse();
        $request  = $e->getRequest();
        $method   = $request->getMethod();

        // test if we matched an individual resource, and then test
        // if we allow the particular request method
        if ($matches->getParam('id', false)) {
            if (!in_array($method, $this->allowedResourceMethods)) {
                $response->setStatusCode(405);

                return $response;
            }

            return;
        }

        // We matched a collection; test if we allow the particular request
        // method
        if (!in_array($method, $this->allowedCollectionMethods)) {
            $response->setStatusCode(405);

            return $response;
        }
    }

    /**
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected function getServiceManager()
    {
        return $this->getEvent()->getApplication()->getServiceManager();
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    protected function getEntityManager()
    {
        return $this->getServiceManager()->get('Doctrine\ORM\EntityManager');
    }

    protected function changeArrayKey($arrTitulos)
    {
        $serviceSisConfig     = new SisConfig($this->getEntityManager(), $this->getServiceManager()->get('config'));
        $boletoAtivado        = (bool)$serviceSisConfig->localizarChave('BOLETO_ATIVO');
        $boletoRemessaAtivada = (bool)$serviceSisConfig->localizarChave('BOLETO_REMESSA_ATIVADA');
        $pagseguroAtivo       = (bool)$serviceSisConfig->localizarChave('PAGSEGURO_ATIVO');

        $urlBoleto = $this->url()->fromRoute(
            'boleto-bancario/default',
            array('controller' => 'boleto', 'action' => 'exibe')
        );

        $urlPagseguroCobranca = $this->url()->fromRoute(
            'financeiro/default',
            array('controller' => 'financeiro-titulo', 'action' => 'pagseguro-cobranca')
        );

        foreach ($arrTitulos as $indice => $arrTitulo) {
            $tituloId                              = ($arrTitulo['titulo_id'] ? $arrTitulo['titulo_id'] : '');
            $arrTitulos[$indice]['link_pagseguro'] = '';

            if ($pagseguroAtivo) {
                $linkPagseguroCobranca                 = $this->getBasePath() . $urlPagseguroCobranca . '/' . $tituloId;
                $arrTitulos[$indice]['link_pagseguro'] = $linkPagseguroCobranca;
            }

            foreach ($arrTitulo as $index => $value) {
                if (preg_match('/titulo/', $index)) {
                    $novoIndex                       = str_replace('titulo_', '', $index);
                    $arrTitulos[$indice][$novoIndex] = $arrTitulos[$indice][$index];
                    unset($arrTitulos[$indice][$index]);
                } elseif (preg_match('/bol_id/', $index)) {
                    unset($arrTitulos[$indice][$index]);

                    $arrTitulos[$indice]['link_boleto'] = '';

                    if ($value && $boletoAtivado) {
                        if (
                            !$boletoRemessaAtivada ||
                            ($boletoRemessaAtivada && $arrTitulos[$indice]['esta_na_remessa'] > 0)
                        ) {
                            $urlBoletoDownload = $this->getBasePath() . $urlBoleto . '/' . $value;

                            $arrTitulos[$indice]['link_boleto'] = $urlBoletoDownload;
                        }
                    }
                }
            }
        }

        return $arrTitulos;
    }

    public function getBasePath()
    {
        $event   = $this->getEvent();
        $request = $event->getRequest();
        $router  = $event->getRouter();
        $uri     = $router->getRequestUri();
        $baseUrl = sprintf(
            '%s://%s%s:%s',
            $uri->getScheme(),
            $uri->getHost(),
            $request->getBaseUrl(),
            $uri->getPort()
        );

        return $baseUrl;
    }

    protected function modeling($id)
    {
        $module = $this->getServiceLocator()->get('getModule');

        $serviceSis = new \Sistema\Service\SisIntegracaoAluno($this->getEntityManager());

        /** @var \Matricula\Entity\AcadgeralAluno $objAluno */
        $objAluno = $serviceSis->pesquisarObjAlunoPeloCodigoIntegracao($module->getIntegracaoId(), $id);
        $alunoId  = $objAluno ? $objAluno->getAlunoId() : false;
        $pesId    = $objAluno ? $objAluno->getPes()->getPes()->getPesId() : false;

        $serviceFinanceiro = new \Financeiro\Service\FinanceiroTitulo($this->getEntityManager());

        $data = $serviceFinanceiro->getArrayFinanceiroTotais($alunoId, $pesId);

        $result = $this->changeArrayKey($data);

        return $result;
    }

    public function get($id)
    {
        $serviceAcadgeralAluno = new \Matricula\Service\AcadgeralAluno($this->getEntityManager());

        $data  = $this->modeling($id);
        $error = !$data;

        return new JsonModel(
            [
                'error'   => $error,
                'message' => $serviceAcadgeralAluno->getLastError(),
                'titulos' => $data,
            ]
        );
    }
}