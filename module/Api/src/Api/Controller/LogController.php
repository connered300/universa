<?php

namespace Api\Controller;

use Api\Service\ApiLog;
use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class LogController extends AbstractRestfulController
{
    protected $allowedCollectionMethods = array(
        'GET',
        'POST',
    );

    protected $allowedResourceMethods = array(
        'GET',
        'PATCH',
        'PUT',
        'DELETE',
    );

    public function setEventManager(EventManagerInterface $events)
    {
        parent::setEventManager($events);
        $events->attach('dispatch', array($this, 'checkOptions'), 10);
    }

    public function checkOptions($e)
    {
        $matches  = $e->getRouteMatch();
        $response = $e->getResponse();
        $request  = $e->getRequest();
        $method   = $request->getMethod();

        // test if we matched an individual resource, and then test
        // if we allow the particular request method
        if ($matches->getParam('id', false)) {
            if (!in_array($method, $this->allowedResourceMethods)) {
                $response->setStatusCode(405);

                return $response;
            }

            return;
        }

        // We matched a collection; test if we allow the particular request
        // method
        if (!in_array($method, $this->allowedCollectionMethods)) {
            $response->setStatusCode(405);

            return $response;
        }
    }

    /**
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected function getServiceManager()
    {
        return $this->getEvent()->getApplication()->getServiceManager();
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    protected function getEntityManager()
    {
        return $this->getServiceManager()->get('Doctrine\ORM\EntityManager');
    }

    public function create($data)
    {
        $service = new \Matricula\Service\AcadgeralAtividadeLog($this->getEntityManager());

        if (!$data) {
            $data = array_merge($this->getRequest()->getPost()->toArray(), $this->getRequest()->getQuery()->toArray());
        }

        if($data['logId']){
            return $this->get($data['logId']);
        }

        $error = false;

        if (!$service->save($data)) {
            $error = true;
        }

        return new JsonModel(
            [
                'error'   => $error,
                'message' => $service->getLastError(),
                'result'  => $data
            ]
        );
    }

    public function get($id)
    {
        $service = new \Matricula\Service\AcadgeralAtividadeLog($this->getEntityManager());

        $data  = $service->retornaLogPeloId($id);
        $error = !$data;

        return new JsonModel(
            [
                'error'   => $error,
                'message' => $service->getLastError(),
                'result'   => $data,
            ]
        );
    }
}