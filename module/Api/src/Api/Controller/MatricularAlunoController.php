<?php

namespace Api\Controller;

use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class MatricularAlunoController extends AbstractRestfulController
{
    protected $__lastError = null;

    /**
     * @return string
     */
    protected function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    protected function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    protected $allowedCollectionMethods = array(
        'GET',
        'POST',
    );

    protected $allowedResourceMethods = array(
        'GET',
        'PATCH',
        'PUT',
        'DELETE',
    );

    public function setEventManager(EventManagerInterface $events)
    {
        parent::setEventManager($events);
        $events->attach('dispatch', array($this, 'checkOptions'), 10);
    }

    public function checkOptions($e)
    {
        $matches  = $e->getRouteMatch();
        $response = $e->getResponse();
        $request  = $e->getRequest();
        $method   = $request->getMethod();

        // test if we matched an individual resource, and then test
        // if we allow the particular request method
        if ($matches->getParam('id', false)) {
            if (!in_array($method, $this->allowedResourceMethods)) {
                $response->setStatusCode(405);

                return $response;
            }

            return;
        }

        // We matched a collection; test if we allow the particular request
        // method
        if (!in_array($method, $this->allowedCollectionMethods)) {
            $response->setStatusCode(405);

            return $response;
        }
    }

    /**
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected function getServiceManager()
    {
        return $this->getEvent()->getApplication()->getServiceManager();
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    protected function getEntityManager()
    {
        return $this->getServiceManager()->get('Doctrine\ORM\EntityManager');
    }

    public function create($arrDados)
    {
        $alunocursoId = $arrDados['matricula'] ? $arrDados['matricula'] : null;

        $arrResult['error'] = true;

        if ($alunocursoId) {
            $config            = $this->getServiceManager()->get('Config');
            $serviceAluno      = new \Matricula\Service\AcadgeralAluno($this->getEntityManager(), $config);
            $serviceAlunoCurso = new \Matricula\Service\AcadgeralAlunoCurso($this->getEntityManager(), $config);
            $serviceResumo     = new \Matricula\Service\AcadperiodoAlunoResumo($this->getEntityManager());

            //if ($serviceResumo->verificaSeAlunoPossuiRegistrosDeHistorico($alunocursoId)) {
            $serviceAluno->efetuarIntegracaoManualNota($alunocursoId, true);
            //}

            $result = $serviceAlunoCurso->ativaMatriculaRetornandoInfo($alunocursoId);

            if ($result) {
                return new JsonModel($result);
            } else {
                $arrResult['message'] = $serviceAlunoCurso->getLastError();
            }
        } else {
            $arrResult['message'] = 'Necessário informar o registro de curso e de aluno!';
        }

        return new JsonModel($arrResult);
    }
}