<?php

namespace Api\Controller;

use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class AlunoNotasController extends AbstractRestfulController
{
    protected $__lastError = null;

    /**
     * @return string
     */
    protected function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    protected function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    protected $allowedCollectionMethods = array(
        'GET',
        'POST',
    );

    protected $allowedResourceMethods = array(
        'GET',
        'PATCH',
        'PUT',
        'DELETE',
    );

    public function setEventManager(EventManagerInterface $events)
    {
        parent::setEventManager($events);
        $events->attach('dispatch', array($this, 'checkOptions'), 10);
    }

    public function checkOptions($e)
    {
        $matches  = $e->getRouteMatch();
        $response = $e->getResponse();
        $request  = $e->getRequest();
        $method   = $request->getMethod();

        // test if we matched an individual resource, and then test
        // if we allow the particular request method
        if ($matches->getParam('id', false)) {
            if (!in_array($method, $this->allowedResourceMethods)) {
                $response->setStatusCode(405);

                return $response;
            }

            return;
        }

        // We matched a collection; test if we allow the particular request
        // method
        if (!in_array($method, $this->allowedCollectionMethods)) {
            $response->setStatusCode(405);

            return $response;
        }
    }

    public function tratarDados(&$arrDados)
    {
        $errors = array();

        if (!$arrDados['disciplina']) {
            $errors[] = 'Por favor, informe a disciplina!';
        } elseif ($arrDados['codigoCurso'] && is_integer($arrDados['disciplina'])) {
            $errors[] = 'Por favor, informe uma disciplina válida!';
        }

        if (!$arrDados['codigoAluno']) {
            $errors[] = 'Por favor, informe o código de um aluno!';
        }

        if (!$arrDados['situacao']) {
            $errors[] = 'Por favor, informe uma situação!';
        }

        if (!$arrDados['dataInformacao']) {
            $errors[] = 'Por favor, informe a data de geração da informação!';
        }

        if (!$arrDados['anoAprovacao']) {
            $errors[] = 'Por favor, informe o ano da aprovação!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        $serviceIntegracaoAluno = new \Sistema\Service\SisIntegracaoAluno($this->getEntityManager());
        $serviceAlunoCurso      = new \Matricula\Service\AcadgeralAlunoCurso($this->getEntityManager());

        $arrDadosAluno = $serviceIntegracaoAluno->retornaAlunoCursoIdIntegracao(
            $arrDados['integracao'],
            $arrDados['codigoAluno']
        );

        if ($arrDadosAluno) {
            $arrDados['aluno_id'] = $arrDadosAluno['aluno_id'];
        } else {
            $errors[] = 'Aluno informado não possui registro no sistema!';
        }

        //Enviando nota de um curso de pós
        if ($arrDados['codigoCurso']) {
            $serviceIntegracaoCurso = new \Sistema\Service\SisIntegracaoCurso($this->getEntityManager());
            $objCampusCurso         = $serviceIntegracaoCurso->retornarCursoCamposPeloCodigoEIntegracao(
                $arrDados['codigoCurso'],
                $arrDados['integracao']
            );

            if ($objCampusCurso) {
                $arrDados['objCampusCurso'] = $objCampusCurso;
                $arrDados['curso_id']       = $objCampusCurso->getCurso()->getCursoId();

                // Verifica se recebeu o nome da disciplina
                if (is_string($arrDados['disciplina'])) {
                    $serviceDisciplina = new \Matricula\Service\AcadgeralDisciplina($this->getEntityManager());

                    if ($discId = $serviceDisciplina->criaDisciplinaSeNaoExiste($arrDados['disciplina'], $arrDados['curso_id'])) {
                        $arrDados['disc_id'] = $discId;
                    }
                } else {
                    $errors[] = 'A integração de notas de cursos de pós graduação não pode receber disciplinas numéricas!';
                }
            } else {
                $errors[] = 'Curso informado não possui registro no sistema!';
            }
        } elseif (is_numeric($arrDados['disciplina'])) {
            $serviceIntegracaoDisciplina = new \Sistema\Service\SisIntegracaoDisciplina($this->getEntityManager());
            /** @var \Matricula\Entity\AcadgeralDisciplinaCurso $objDisciplinaCurso */
            $objDisciplinaCurso = $serviceIntegracaoDisciplina
                ->retornarDisciplinaPeloCodigoEIntegracao($arrDados['disciplina'], $arrDados['integracao']);

            if ($objDisciplinaCurso) {
                $arrDados['disc_id']  = $objDisciplinaCurso->getDisc()->getDiscId();
                $arrDados['curso_id'] = $objDisciplinaCurso->getCurso()->getCursoId();
            } else {
                $errors[] = 'Disciplina informada não possui registro no sistema!';
            }
        } else {
            $errors[] = 'Para integrar uma nota deve-se, passar ou o código da disciplina ou o código do curso junto do nome da disciplina!';
        }

        if ($arrDados['aluno_id'] && $arrDados['curso_id']) {
            /** @var \Matricula\Entity\AcadgeralAlunoCurso $objAlunoCurso */
            $objAlunoCurso = $serviceAlunoCurso->pesquisaAlunoCursoPeloAlunoEPeloCurso(
                $arrDados['aluno_id'],
                $arrDados['curso_id']
            );

            if ($objAlunoCurso) {
                $arrDados['alunocurso_id'] = $objAlunoCurso->getAlunocursoId();
            } else {
                $errors[] = 'Vínculo de aluno curso não encontrado no sistema!';
            }
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        $serviceAlunoResumo = new \Matricula\Service\AcadperiodoAlunoResumo($this->getEntityManager());

        $arrDados['resId']           = $serviceAlunoResumo->verificaSeRegistroExiste(
            $arrDados['alunocurso_id'],
            $arrDados['disc_id']
        );
        $arrDados['resalunoOrigem']  = \Matricula\Service\AcadperiodoAlunoResumo::RESALUNO_ORIGEM_WEBSERVICE;
        $arrDados['resalunoDetalhe'] = (
        $arrDados['resalunoDetalhe'] ? $arrDados['resalunoDetalhe'] : ($arrDados['detalhe'] ? $arrDados['detalhe'] : null)
        );

        return true;
    }

    /**
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected function getServiceManager()
    {
        return $this->getEvent()->getApplication()->getServiceManager();
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    protected function getEntityManager()
    {
        return $this->getServiceManager()->get('Doctrine\ORM\EntityManager');
    }

    public function create($arrDados)
    {
        $module                 = $this->getServiceLocator()->get('getModule');
        $arrDados['integracao'] = $module->getIntegracaoId();

        if (!$this->tratarDados($arrDados)) {
            return new JsonModel(['error' => true, 'message' => $this->getLastError()]);
        }

        $service      = new \Matricula\Service\AcadperiodoAlunoResumo($this->getEntityManager());
        $serviceAluno = new \Matricula\Service\AcadgeralAluno($this->getEntityManager());

        $serviceAluno->procuraDadosExtrasParaResAluno($arrDados);

        if (!$service->registroSimplificado($arrDados, true)) {
            return new JsonModel(['error' => true, 'message' => $service->getLastError()]);
        }

        return new JsonModel(['error' => false, 'AlunoNota' => $arrDados]);
    }

    public function getList()
    {
        $arrDados = $this->getRequest()->getQuery()->toArray();

        return $this->create($arrDados);
    }
}