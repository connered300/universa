<?php

namespace Api\Controller;

use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Matricula\Service\AcadgeralAluno;
use Sistema\Service\SisIntegracaoCurso;
use Sistema\Service\SisIntegracaoAluno;

class IntegracaoAlunoController extends AbstractRestfulController
{
    protected $allowedCollectionMethods = array(
        'GET',
        'POST'
    );

    protected $allowedResourceMethods = array(
        'GET',
        'PATCH',
        'PUT',
        'POST',
        'DELETE',
    );

    public function setEventManager(EventManagerInterface $events)
    {
        parent::setEventManager($events);
        $events->attach('dispatch', array($this, 'checkOptions'), 10);
    }

    public function checkOptions($e)
    {
        $matches  = $e->getRouteMatch();
        $response = $e->getResponse();
        $request  = $e->getRequest();
        $method   = $request->getMethod();

        // test if we matched an individual resource, and then test
        // if we allow the particular request method
        if ($matches->getParam('id', false)) {
            if (!in_array($method, $this->allowedResourceMethods)) {
                $response->setStatusCode(405);

                return $response;
            }

            return;
        }

        // We matched a collection; test if we allow the particular request
        // method
        if (!in_array($method, $this->allowedCollectionMethods)) {
            $response->setStatusCode(405);

            return $response;
        }
    }

    /**
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected function getServiceManager()
    {
        return $this->getEvent()->getApplication()->getServiceManager();
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    protected function getEntityManager()
    {
        return $this->getServiceManager()->get('Doctrine\ORM\EntityManager');
    }

    protected function changeArrayKey($data)
    {
        $arrSentencas = array(
            ['/^pessoa(.*)$/i', '$1', 'Id', ''],
            ['/^aluno(.*)$/i', '$1', 'Id', 'Matrícula'],
            ['/^pes(.*)$/i', '$1', 'Id', 'Pessoa'],
            ['/^end(.*)$/i', '$1', 'Id', ''],
            ['/^conContato(.*)$/i', '$1', 'Id', ''],
            ['/^formacao(.*)$/i', '$1', 'Id', ''],
            ['/^alunocurso(.*)$/i', '$1', 'Id', ''],
            ['/^cursocampus(.*)$/i', '$1', 'Id', '']
        );

        foreach ($data as $index => $value) {
            if (is_array($value)) {
                $value = $this->changeArrayKey($value);

                $data[$index] = $value;
            }

            foreach ($arrSentencas as $sentenca) {
                if (preg_match($sentenca[0], $index)) {
                    $novoIndex = preg_replace($sentenca[0], $sentenca[1], $index);

                    if ($novoIndex == $sentenca[2]) {
                        $novoIndex = $sentenca[3];
                    }

                    unset($data[$index]);

                    if ($novoIndex) {
                        $data[lcfirst($novoIndex)] = $value;
                    }

                    break;
                }
            }
        }

        foreach ($data['documentoGrupo'] as $index => $value) {
            $dataEntrega = (array)$value['docpessoaEntrega'];
            $status      = $data['documentoGrupo'][$index]['docpessoaStatus'];
            $status      = ($status == "Sim" ? "Entregue" : ($status == "Não" ? "Não Entregue" : 'Dispensável'));
            $documento   = $data['documentoGrupo'][$index]['tdocDescricao'];
            $exigencia   = $data['documentoGrupo'][$index]['docgrupoExigencia'];

            unset($data['documentoGrupo'][$index]);

            $data['documentoGrupo'][$index] = [
                'situacao'  => $status,
                'data'      => (new \DateTime($dataEntrega['date']))->format("d/m/Y"),
                'documento' => $documento,
                'exigencia' => $exigencia
            ];
        }

        if (isset($data['arq']) && $data['arq']) {
            $data['arq']                   = (array)($data['arq']);
            $urlGerenciadorDeArquivosThumb = (
                $this->getBasePath() .
                $this->url()->fromRoute(
                    'gerenciador-arquivos/default',
                    array('controller' => 'arquivo', 'action' => 'thumb', 'id' => $data['arq']['chave'])
                )
            );

            $urlGerenciadorDeArquivosDownload = (
                $this->getBasePath() .
                $this->url()->fromRoute(
                    'gerenciador-arquivos/default',
                    array('controller' => 'arquivo', 'action' => 'download', 'id' => $data['arq']['chave'])
                )
            );

            $data['arq']['thumb']    = $urlGerenciadorDeArquivosThumb;
            $data['arq']['download'] = $urlGerenciadorDeArquivosDownload;
        }

        return $data;
    }

    public function getBasePath()
    {
        $event   = $this->getEvent();
        $request = $event->getRequest();
        $router  = $event->getRouter();
        $uri     = $router->getRequestUri();
        $baseUrl = sprintf(
            '%s://%s%s:%s',
            $uri->getScheme(),
            $uri->getHost(),
            $request->getBaseUrl(),
            $uri->getPort()
        );

        return $baseUrl;
    }

    protected function modeling($id)
    {
        $module = $this->getServiceLocator()->get('getModule');

        $serviceSis = new \Sistema\Service\SisIntegracaoAluno($this->getEntityManager());

        $alunoId = $serviceSis->pesquisarAlunoCodigoIntegracao($module->getIntegracaoId(), $id);

        $serviceAcadgeralAluno = new \Matricula\Service\AcadgeralAluno($this->getEntityManager());

        $data = $serviceAcadgeralAluno->getArray($alunoId);
        $serviceAcadgeralAluno->formataDadosPost($data);
        $result = $this->changeArrayKey($data);

        return $result;
    }

    public function get($id)
    {
        $serviceAcadgeralAluno = new \Matricula\Service\AcadgeralAluno($this->getEntityManager());

        $data  = $this->modeling($id);
        $error = !$data;

        return new JsonModel(
            [
                'error'   => $error,
                'message' => $serviceAcadgeralAluno->getLastError(),
                'aluno'   => $data,
            ]
        );
    }

    public function setAlunoOnCurso($codigoAluno, $codigoCurso)
    {
        try {
            $module                = $this->getServiceLocator()->get('getModule');
            $serviceAcadGeralAluno = new AcadgeralAluno($this->getEntityManager());
            $serviceSisCurso       = new SisIntegracaoCurso($this->getEntityManager());
            $serviceSisAluno       = new SisIntegracaoAluno($this->getEntityManager());

            $alunoId = $serviceSisAluno->pesquisarAlunoCodigoIntegracao(
                $module->getIntegracaoId(),
                $codigoAluno
            );

            $objCampusCurso = $serviceSisCurso->retornarCursoCamposPeloCodigoEIntegracao(
                $codigoCurso,
                $module->getIntegracaoId()
            );

            if ($objCampusCurso && $alunoId) {
                $arrDadosAluno = $serviceAcadGeralAluno->getArray($alunoId);

                $arrDados['campusCurso'] = $objCampusCurso->getCursocampusId();
                $arrDados['alunoId']     = $arrDadosAluno['alunoId'];
            }

            if ($serviceAcadGeralAluno->registroSimplificadoDeAluno($arrDados)) {
                return true;
            }
        } catch (\Exception $ex) {
        }

        return false;
    }

    public function create()
    {
        $request           = $this->getRequest();
        $insercaoConcluida = false;

        if ($request->isPost()) {
            $dados = $request->getPost()->toArray();

            if ($dados['aluno'] && $dados['curso']) {
                $insercaoConcluida = $this->setAlunoOnCurso($dados['aluno'], $dados['curso']);
            }
        }

        return new JsonModel(
            [
                'error' => !$insercaoConcluida,
            ]
        );
    }
}