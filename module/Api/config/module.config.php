<?php
namespace Api;

return array(
    'router'                    => array(
        'routes' => array(
            '_api' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/api',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Api\Controller',
                        'controller'    => 'Index',
                    ),
                ),
            ),
            'api'  => array(
                'type'          => 'Literal',
                'options'       => array(
                    'route'    => '/api/v1',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Api\Controller',
                        'controller'    => 'Index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes'  => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'       => '/[:controller[/:id]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id'         => '[a-zA-Z0-9_-]*',
                            ),
                            'defaults'    => array(),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager'           => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases'            => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'doctrine'                  => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default'             => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            ),
        ),
    ),
    'controllers'               => array(
        'invokables' => array(
            'Api\Controller\Aluno'                    => 'Api\Controller\AlunoController',
            'Api\Controller\Curso'                    => 'Api\Controller\CursoController',
            'Api\Controller\Index'                    => 'Api\Controller\IndexController',
            'Api\Controller\IntegracaoAluno'          => 'Api\Controller\IntegracaoAlunoController',
            'Api\Controller\IntegracaoFinanceiro'     => 'Api\Controller\IntegracaoFinanceiroController',
            'Api\Controller\ProtocoloMensagem'        => 'Api\Controller\ProtocoloMensagemController',
            'Api\Controller\Protocolo'                => 'Api\Controller\ProtocoloController',
            'Api\Controller\ProtocoloSolicitacao'     => 'Api\Controller\ProtocoloSolicitacaoController',
            'Api\Controller\AlunoProtocolo'           => 'Api\Controller\AlunoProtocoloController',
            'Api\Controller\AlunoNotas'               => 'Api\Controller\AlunoNotasController',
            'Api\Controller\Log'                      => 'Api\Controller\LogController',
            'Api\Controller\MatricularAluno'          => 'Api\Controller\MatricularAlunoController',
            'Api\Controller\AlunoVerificaRematricula' => 'Api\Controller\AlunoVerificaRematriculaController',
        ),
    ),
    'view_manager'              => array(
        'template_path_stack'      => array(
            __DIR__ . '/../view',
        ),
        'strategies'               => array(
            'ViewJsonStrategy',
        ),
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
    ),
    'console'                   => array(
        'router' => array(
            'routes' => array(),
        ),
    ),
    'funcionalidades_liberadas' => array(
        'controllers' => array(),
        'actions'     => array(),
    ),
);
