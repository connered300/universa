<?php

namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

class AcadperiodoMatrizCurricular extends AbstractService
{
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadperiodoMatrizCurricular');
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {
    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    public function pesquisaForJson($params)
    {
        $sql = '
        SELECT *
        FROM acadperiodo__matriz_curricular
        INNER JOIN acad_curso USING (curso_id)
        WHERE (mat_cur_descricao like :q OR curso_nome like :q) ';

        $q = '';

        if ($params['q']) {
            $q = $params['q'];
        } elseif ($params['query']) {
            $q = $params['query'];
        }

        $param = array('q' => "{$q}%");

        if ($params['cursoId']) {
            $sql .= " AND curso_id = :cursoId ";
            $param['cursoId'] = $params['cursoId'];
        }

        $result = $this->executeQueryWithParam($sql, $param)->fetchAll();

        return $result;
    }

    public function adicionar($dados)
    {
        $serviceMatDisc = new \Matricula\Service\AcadperiodoMatrizDisciplina($this->getEm());
        try {
            parent::begin();
            $matCur = parent::adicionar($dados);
            foreach ($dados['disc'] as $key => $disc) {
                $matrizDisciplina = [
                    'perDiscId'        => '',
                    'disc'             => $disc,
                    'perDiscPeriodo'   => (int)$dados['perDiscPeriodo'][$key],
                    'perDiscChpratica' => (string)$dados['perDiscChpratica'][$key],
                    'perDiscChteorica' => (string)$dados['perDiscChteorica'][$key],
                    'perDiscChestagio' => (string)$dados['perDiscChestagio'][$key],
                    'perDiscCreditos'  => (int)$dados['perDiscCreditos'][$key],
                    'matCur'           => $matCur->getMatCurId()
                ];
                $serviceMatDisc->adicionar($matrizDisciplina);
            }
            parent::commit();
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        return $matCur;
    }

    public function edita($dados)
    {
        $serviceMatDisc = new \Matricula\Service\AcadperiodoMatrizDisciplina($this->getEm());
        try {
            parent::begin();
            $matCur    = parent::edita($dados);
            $excluidos = explode(',', $dados['excluidos']);
            foreach ($excluidos as $excluir) {
                if ($excluir) {
                    $serviceMatDisc->excluir($excluir);
                }
            }
            foreach ($dados['disc'] as $key => $disc) {
                $matrizDisciplina = [
                    'perDiscId'        => $dados['perDiscId'][$key],
                    'disc'             => $disc,
                    'perDiscPeriodo'   => (int)$dados['perDiscPeriodo'][$key],
                    'perDiscChpratica' => (int)$dados['perDiscChpratica'][$key],
                    'perDiscChteorica' => (int)$dados['perDiscChteorica'][$key],
                    'perDiscChestagio' => (int)$dados['perDiscChestagio'][$key],
                    'perDiscCreditos'  => (int)$dados['perDiscCreditos'][$key],
                    'matCur'           => $matCur->getMatCurId()
                ];
                if ($matrizDisciplina['perDiscId']) {
                    $serviceMatDisc->edita($matrizDisciplina);
                } else {
                    $serviceMatDisc->adicionar($matrizDisciplina);
                }
            }
            parent::commit();
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        return $matCur;
    }

    public function buscaPeriodoTravado($matrizId)
    {
        $servicePeriodo = new \Matricula\Service\AcadperiodoLetivo($this->getEm());
        $objPeriodo     = $servicePeriodo->buscaPeriodoCorrente();

        $perIdAtual = null;

        if ($objPeriodo) {
            $perIdAtual = $objPeriodo->getPerId();
        }

        $query = "
        SELECT turma_serie
        FROM acadperiodo__turma
        WHERE mat_cur_id = {$matrizId} AND per_id !=  {$perIdAtual}";

        $periodo = $this->executeQuery($query)->fetch();
        $periodo = $periodo['turma_serie'] ? intval($periodo['turma_serie']) - 1 : 0;

        return $periodo;
    }

    public function pagination($page = 0, $is_json = false)
    {
        $dados          = array();
        $dados['count'] = $this->executeQuery(
            "SELECT COUNT(*) as row  FROM {$this->getMetadados()->table['name']}"
        )->fetch()['row'];

        $params    = (new \VersaSpine\ListGenerator\Annotations\ReaderQuerys($this->getEntity()))->getOrderBy();
        $joins     = (new \VersaSpine\ListGenerator\Annotations\ReaderQuerys($this->getEntity()))->getJoins();
        $campTable = (new \VersaSpine\ListGenerator\Annotations\ReaderQuerys($this->getEntity()))->getCampos();

        $metadados = $this->getMetadados();

        if (count($campTable) <= 0) {
            return array();
        }

        if (is_object($params)) {
            if (count($params->toArray()['params']) > 0) {
                $order = " ORDER BY ";
                foreach ($params->toArray()['params'] as $key => $value) {
                    $order .= " {$metadados->table['name']}.$key $value, ";
                }
                $order = substr($order, 0, -2);
            }
        }

        if (!$page) {
            if ($is_json) {
                $limit = " LIMIT 10 OFFSET 0 ";
            } else {
                $limit = "";
            }
        } else {
            $offset = (($page - 1) * 10);
            $limit  = " LIMIT 10 OFFSET $offset ";
        }
        $sql = "SELECT ";
        foreach ($campTable as $key => $camp) {
            if (array_search($camp, array_keys($joins)) === false) {
                if ($metadados->fieldMappings[$key]['type'] == 'date') {
                    $sql .= " DATE_FORMAT({$metadados->table['name']}.$camp,'%d/%m/%Y') as $camp ";
                } else {
                    $sql .= " {$metadados->table['name']}.$camp ";
                }
                $sql .= ",";
            }
        }

        $joinsSql = "";
        $labels   = "";

        if (is_array($joins)) {
            foreach ($joins as $chave => $join) {
                if ($join['join_type'] == "natural") {
                    $joinsSql .= " {$join['join_type']} JOIN {$join['table']} {$join['table']} ";
                    $labels .= "{$join['table']}.{$join['join_camp_dest_label']} as $chave, ";
                } else {
                    $campo = (isset($join['join_camp_orig'])) ? $join['join_camp_orig'] : $chave;
                    $joinsSql .= " {$join['join_type']} JOIN {$join['table']} {$join['table']} ON {$metadados->table['name']}.$campo = {$join['table']}.{$join['join_camp_dest']} ";
                    $labels .= "{$join['table']}.{$join['join_camp_dest_label']} as $chave, ";
                }
            }
        }
        if (count($joins) == 0) {
            $sql = substr($sql, 0, -1);
        }

        $sql .= substr($labels, 0, -2);

        $replace  = $metadados->table['name'] . ".mat_cur_id as clonar";
        $replace2 = $metadados->table['name'] . ".mat_cur_id as impressao";

        $sql = str_replace('acadperiodo__matriz_curricular.clonar', $replace, $sql);
        $sql = str_replace('acadperiodo__matriz_curricular.impressao', $replace2, $sql);

        $sql .= " FROM {$metadados->table['name']} {$metadados->table['name']} ";
        $sql .= $joinsSql;
        $sql .= " $order $limit ";
        $dados['dados'] = $this->executeQuery($sql);

        if ($is_json) {
            $dados['dados'] = $this->paginationFormatJson($dados['dados']);
        }

        return $dados;
    }

    public function retornaDisciplinaCursoPelaMatrizSerie($matrizCurricular, $serie)
    {
        if (!$matrizCurricular || !$serie) {
            return false;
        }
        $arrParam = ['matrizCurricular' => $matrizCurricular, 'serie' => $serie];

        $sql = '
            SELECT adc.disc_curso_id FROM acadperiodo__matriz_curricular amc
            LEFT JOIN acadgeral__disciplina_curso adc ON adc.curso_id=amc.curso_id
            LEFT JOIN acadperiodo__matriz_disciplina amd ON amd.disc_id=adc.disc_id
            WHERE amc.mat_cur_id=:matrizCurricular AND amd.per_disc_periodo=:serie AND adc.curso_id IS NOT NULL
            GROUP BY amd.disc_id
        ';

        $result = $this->executeQueryWithParam($sql, $arrParam)->fetchAll();

        if ($result) {
            foreach ($result as $chave => $dados) {
                $result[$chave] = $dados;
            }
        }

        return $result ? $result : null;
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "SELECT  * FROM acadperiodo__matriz_curricular";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

}