<?php


namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

class AcadgeralTurmaTipo extends AbstractService{
    const CONVENCIONAL = 1;
    const DEPENDENCIA  = 2;
    const ADAPTACAO    = 3;
    const REINGRESSO   = 4;
    const OPTATIVA     = 5;

    public function __construct(\Doctrine\ORM\EntityManager $em){
        parent::__construct($em, 'Matricula\Entity\AcadgeralTurmaTipo');
    }

    public static function returnOptions() 
    {
        return [
            'Convencional' => self::CONVENCIONAL,
            'Dependencia'  => self::DEPENDENCIA,
            'Adaptação'    => self::ADAPTACAO,
            'Reingresso'   => self::REINGRESSO,
            'Optativa'     => self::OPTATIVA,
        ];
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {

    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {

    }
}