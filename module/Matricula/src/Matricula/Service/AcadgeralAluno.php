<?php

namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class AcadgeralAluno
 * @package Matricula\Service
 */
class AcadgeralAluno extends AbstractService
{
    const alunoEtniaBranca                   = 'Branca';
    const ALUNO_ETNIA_BRANCA                   = 'Branca';
    const ALUNO_ETNIA_PRETA                    = 'Preta';
    const ALUNO_ETNIA_AMARELA                  = 'Amarela';
    const ALUNO_ETNIA_PARDA                    = 'Parda';
    const ALUNO_ETNIA_INDIGENA                 = 'Indígena';
    const ALUNO_ETNIA_NAO_DISPOE_DA_INFORMACAO = 'Não dispõe da informação';
    const ALUNO_ETNIA_NAO_DECLARADO            = 'Não declarado';

    const DIRETORIO_IMAGEM = 0000000002;

    private $lastError = null;

    /** @var \Sistema\Service\SisConfig */
    private $config = null;

    /**
     * @return \Sistema\Service\SisConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param \Sistema\Service\SisConfig $config
     * @return AcadgeralAluno
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, $arrConfig = array())
    {
        parent::__construct($em, 'Matricula\Entity\AcadgeralAluno');

        $this->setConfig(new \Sistema\Service\SisConfig($em, $arrConfig));
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    public function retornaFiltrosParaConsultaDeAlunos($param)
    {
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());

        $conditions = '';

        if ($param['campusCurso']) {
            $conditions .= " AND campus_curso.cursocampus_id IN({$param['campusCurso']})";
        }

        if ($param['nivel']) {
            $param['nivel'] = is_array($param['nivel']) ? implode(',', $param['nivel']) : $param['nivel'];
            $conditions .= " AND curso.nivel_id in ({$param['nivel']})";
        }

        if ($param['situacao']) {
            $param['situacao'] = trim($param['situacao'], ',');
            $conditions .= " AND FIND_IN_SET(alunoCurso.alunocurso_situacao,\"{$param['situacao']}\")";
        }

        if (($param['agentes'])) {
            $agentes      = explode(",", $param['agentes']);
            $agentesOpcao = $param['agentesOpcao'];

            if ($agentesOpcao == "incluirAgente") {
                if (in_array("-", $agentes)) {
                    $agentes = array_diff($agentes, ['-']);
                    $agentes = implode(',', $agentes);

                    if ($agentes) {
                        $conditions .= "
                        AND (
                            COALESCE (alunoCurso.pes_id_agente, acadgeral__aluno.pes_id_agenciador) IN({$agentes}) OR
                            COALESCE (alunoCurso.pes_id_agente, acadgeral__aluno.pes_id_agenciador) IS NULL
                        )";
                    } else {
                        $conditions .= " AND COALESCE (alunoCurso.pes_id_agente, acadgeral__aluno.pes_id_agenciador) IS NULL";
                    }
                } else {
                    $agentes = implode(',', $agentes);
                    $conditions .= " AND COALESCE (alunoCurso.pes_id_agente, acadgeral__aluno.pes_id_agenciador) IN({$agentes})";
                }
            } else {
                if (in_array("-", $agentes)) {
                    $agentes = array_diff($agentes, ['-']);
                    $agentes = implode(',', $agentes);

                    if ($agentes) {
                        $conditions .= "
                        AND (
                            COALESCE (alunoCurso.pes_id_agente, acadgeral__aluno.pes_id_agenciador) NOT IN({$agentes})
                            AND COALESCE (alunoCurso.pes_id_agente, acadgeral__aluno.pes_id_agenciador) IS NOT NULL
                        )";
                    } else {
                        $conditions .= " AND COALESCE (alunoCurso.pes_id_agente, acadgeral__aluno.pes_id_agenciador) IS NOT NULL";
                    }
                } else {
                    $agentes = implode(',', $agentes);
                    $conditions .= " AND COALESCE (alunoCurso.pes_id_agente, acadgeral__aluno.pes_id_agenciador) NOT IN({$agentes})";
                }
            }
        }

        if ($param['unidadeEstudo']) {
            $unidades = explode(",", $param['unidadeEstudo']);

            if (in_array("-", $unidades)) {
                $unidades = array_diff($unidades, ['-']);

                if ($unidades) {
                    $conditions .= " AND (acadgeral__aluno.unidade_id IN({$unidades}) OR acadgeral__aluno.unidade_id IS NULL)";
                } else {
                    $conditions .= " AND acadgeral__aluno.unidade_id IS NULL";
                }
            } else {
                $unidades = implode(',', $unidades);
                $conditions .= " AND acadgeral__aluno.unidade_id IN({$unidades})";
            }
        }

        if ($param['cidadeAluno']) {
            $conditions .= " AND FIND_IN_SET(end_cidade,\"{$param['cidadeAluno']}\")";
        }

        if ($param['estadoAluno']) {
            $conditions .= ' AND FIND_IN_SET(end_estado, "' . $param['estadoAluno'] . '")';
        }

        if ($param['areaConhecimentoCurso']) {
            $conditions .= " AND curso.area_id IN({$param['areaConhecimentoCurso']})";
        }

        if ($param['sexoAluno']) {
            $conditions .= ' AND FIND_IN_SET(pessoa_fisica.pes_sexo, "' . $param['sexoAluno'] . '")';
        }

        if ($param['operadoresCadastro']) {
            $conditions .= " AND alunoCurso.usuario_cadastro IN({$param['operadoresCadastro'] })   ";
        }

        if ($param['operadoresAlteracao']) {
            $conditions .= " AND alunoCurso.usuario_alteracao IN({$param['operadoresAlteracao'] })";
        }

        $dataCadastroInicial  = $param['dataCadastroInicial'] ? $param['dataCadastroInicial'] : null;
        $dataCadastroFinal    = $param['dataCadastroFinal'] ? $param['dataCadastroFinal'] : null;
        $dataAlteracaoInicial = $param['dataAlteracaoInicial'] ? $param['dataAlteracaoInicial'] : null;
        $dataAlteracaoFinal   = $param['dataAlteracaoFinal'] ? $param['dataAlteracaoFinal'] : null;

        if ($dataCadastroInicial) {
            $dataCadastroInicial = self::formatDateAmericano($dataCadastroInicial);
            $conditions .= ' AND date(alunoCurso.alunocurso_data_cadastro) >= "' . $dataCadastroInicial . '"';
        }

        if ($dataCadastroFinal) {
            $dataCadastroFinal = self::formatDateAmericano($dataCadastroFinal);
            $conditions .= ' AND date(alunoCurso.alunocurso_data_cadastro) <= "' . $dataCadastroFinal . '"';
        }

        if ($dataAlteracaoInicial) {
            $dataAlteracaoInicial = self::formatDateAmericano($dataAlteracaoInicial);
            $conditions .= ' AND date(alunoCurso.alunocurso_data_alteracao) >= "' . $dataAlteracaoInicial . '"';
        }

        if ($dataAlteracaoFinal) {
            $dataAlteracaoFinal = self::formatDateAmericano($dataAlteracaoFinal);
            $conditions .= ' AND date(alunoCurso.alunocurso_data_alteracao) <= "' . $dataAlteracaoFinal . '"';
        }

        if ($param['alunoId']) {
            $conditions .= " AND acadgeral__aluno.aluno_id in (" . $param['alunoId'] . ")";
        }

        if ($param['idadeFinal']) {
            $anoFinal = (new \DateTime())->format('Y') - $param['idadeFinal'];
            $conditions .= " AND year(pes_data_nascimento) >= " . $anoFinal;
        }

        if ($param['idadeInicial']) {
            $anoInicial = (new \DateTime())->format('Y') - $param['idadeInicial'];
            $conditions .= " AND year(pes_data_nascimento) <= " . $anoInicial;
        }

        if ($param['mesAniversario']) {
            $conditions .= " AND month(pes_data_nascimento) IN( " . $param['mesAniversario'] . ")";
        }

        if ($param['diaAniversario']) {
            $conditions .= " AND day(pes_data_nascimento) IN( " . $param['diaAniversario'] . ")";
        }

        $arrCampusCursoPermitido = implode(",", $serviceAcessoPessoas->retornaCampusCursoDoUsuarioLogado(true));

        if ($arrCampusCursoPermitido) {
            $conditions .= " AND campus_curso.cursocampus_id in (" . $arrCampusCursoPermitido . ")";
        }

        return $conditions;
    }

    public function retornaDescricaoFiltrosParaConsultaDeAlunos($param)
    {
        $descricaoFiltro         = [];
        $serviceNivelEnsino      = new \Matricula\Service\AcadNivel($this->getEm());
        $serviceAreaConhecimento = new \Matricula\Service\AcadgeralAreaConhecimento($this->getEm());
        $serviceCampusCurso      = new \Matricula\Service\CampusCurso($this->getEm());
        $serviceUnidade          = new \Organizacao\Service\OrgUnidadeEstudo($this->getEm());
        $serviceOrgAgente        = new \Organizacao\Service\OrgAgenteEducacional($this->getEm());
        $serviceAcesso           = new \Acesso\Service\AcessoPessoas($this->getEm());

        if ($param['campusCurso']) {
            $campus                    = $param['campusCurso'];
            $descricaoFiltro['campus'] = 'Campus/Curso: ' . $serviceCampusCurso->getDescricao($campus);
        }

        if ($param['situacao']) {
            $situacao = $param['situacao'];
            $situacao = str_replace(',', ', ', $situacao);

            $descricaoFiltro['situacao'] = 'Situação: ' . $situacao;
        }

        if ($param['operadoresCadastro']) {
            $operadoresCadastro                    = $param['operadoresCadastro'];
            $descricaoFiltro['operadoresCadastro'] = $serviceAcesso->getDescricao($operadoresCadastro);
            $descricaoFiltro['operadoresCadastro'] = 'Usuário Cadastro: ' . $descricaoFiltro['operadoresCadastro'];
        }

        if ($param['operadoresAlteracao']) {
            $operadoresAlteracao                    = $param['operadoresAlteracao'];
            $descricaoFiltro['operadoresAlteracao'] = $operadoresAlteracao->getDescricao($operadoresAlteracao);
            $descricaoFiltro['operadoresAlteracao'] = 'Usuário Alteração: ' . $descricaoFiltro['operadoresAlteracao'];
        }

        if ($param['agentes']) {
            $agenteDescricao = '';
            $agentes         = explode(",", $param['agentes']);
            $agentesOpcao    = $param['agentesOpcao'];

            if (in_array("-", $agentes)) {
                $agentes         = array_diff($agentes, ['-']);
                $agentes         = implode(',', $agentes);
                $agenteDescricao = $agenteDescricao . 'Sem agente, ';
            } else {
                $agentes = implode(',', $agentes);
            }

            $agenteDescricao .= $serviceOrgAgente->getDescricao($agentes);
            $agenteDescricao = trim($agenteDescricao, ', ');

            if ($agentesOpcao == "incluirAgente") {
                $agenteDescricao = "Alunos dos Agentes: " . $agenteDescricao;
            } else {
                $agenteDescricao = "Alunos que não são dos Agentes: " . $agenteDescricao;
            }

            $descricaoFiltro['agentes'] = $agenteDescricao;
        }

        if ($param['unidadeEstudo']) {
            $unidadeDescricao = null;
            $unidades         = explode(",", $param['unidadeEstudo']);

            if (in_array("-", $unidades)) {
                $unidades = array_diff($unidades, ['-']);

                $unidadeDescricao = 'Sem unidade, ';
            } else {
                $unidades = implode(',', $unidades);
            }

            $descricao = $serviceUnidade->getDescricao($unidades);
            $unidadeDescricao .= $descricao['text'];

            $unidadeDescricao = trim($unidadeDescricao, ', ');

            $unidadeDescricao           = "Unidade de estudo: " . $unidadeDescricao;
            $descricaoFiltro['unidade'] = $unidadeDescricao;
        }

        $dataCadastroInicial  = $param['dataCadastroInicial'] ? $param['dataCadastroInicial'] : null;
        $dataCadastroFinal    = $param['dataCadastroFinal'] ? $param['dataCadastroFinal'] : null;
        $dataAlteracaoInicial = $param['dataAlteracaoInicial'] ? $param['dataAlteracaoInicial'] : null;
        $dataAlteracaoFinal   = $param['dataAlteracaoFinal'] ? $param['dataAlteracaoFinal'] : null;

        if ($dataAlteracaoInicial && $dataAlteracaoFinal) {
            $descricaoFiltro['dataAlteracao'] = ' Data de alteração: ' . $dataAlteracaoInicial . ' - ' . $dataAlteracaoFinal;
        } elseif ($dataAlteracaoInicial) {
            $descricaoFiltro['dataAlteracao'] = ' Alteração a partir de: ' . $dataAlteracaoInicial;
        } elseif ($dataAlteracaoFinal) {
            $descricaoFiltro['dataAlteracao'] = ' Alteração anterior a: ' . $dataAlteracaoFinal;
        }

        if ($dataCadastroInicial && $dataCadastroFinal) {
            $descricaoFiltro['dataCadastro'] = ' Data de Cadastro: ' . $dataCadastroInicial . ' - ' . $dataCadastroFinal;
        } elseif ($dataCadastroInicial) {
            $descricaoFiltro['dataCadastro'] = ' Cadastro a Partir de: ' . $dataCadastroInicial;
        } elseif ($dataCadastroFinal) {
            $descricaoFiltro['dataCadastro'] = ' Cadastro Anterior a: ' . $dataCadastroFinal;
        }

        if ($param['cidadeAluno']) {
            $descricaoFiltro['cidades'] = "Cidade: " . $param['cidadeAluno'];
        }

        if ($param['estadoAluno']) {
            $descricaoFiltro['estado'] = "Estado: " . $param['estadoAluno'];
        }

        if ($param['areaConhecimentoCurso']) {
            $areaConhecimento = $param['areaConhecimentoCurso'];
            $areaConhecimento = $serviceAreaConhecimento->getDescricao($areaConhecimento);

            $descricaoFiltro['areaConhecimento'] = "Áreas de conhecimento: " . $areaConhecimento;
        }

        if ($param['nivel']) {
            $nivel = $param['nivel'];
            $nivel = $serviceNivelEnsino->getDescricao($nivel);

            $descricaoFiltro['nivel'] = "Nível de ensino: " . $nivel;
        }

        if ($param['sexoAluno']) {
            $descricaoFiltro['sexo'] = "Sexo: " . $param['sexoAluno'];
        }

        if ($param['idadeInicial']) {
            $descricaoFiltro['idadeInicial'] = "Idade inicial: " . $param['idadeInicial'];
        }

        if ($param['idadeFinal']) {
            $descricaoFiltro['idadeFinal'] = "Idade Final: " . $param['idadeFinal'];
        }

        if ($param['diaAniversario']) {
            $descricaoFiltro['diaAniversario'] = "Aniversáriante do Dia: " . $param['diaAniversario'];
        }

        if ($param['mesAniversario']) {
            $descricaoFiltro['mesAniversario'] = "Aniversáriante do Mês: " . $param['mesAniversario'];
        }

        return implode(';     ', $descricaoFiltro);
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        if ($data['alunoId']) {
            $data['filter']['alunoId'] = $data['alunoId'];
        }

        $conditions = $this->retornaFiltrosParaConsultaDeAlunos($data['filter']);

        $queryTotal = "
        SELECT count(*) as qtd
        FROM acadgeral__aluno
        LEFT JOIN acadgeral__aluno_curso alunoCurso USING(aluno_id)";

        $query = "
        SELECT
            SQL_CALC_FOUND_ROWS
            acadgeral__aluno.pes_id, acadgeral__aluno.aluno_id, alunoCurso.alunocurso_id,
            pessoa.pes_nome, pes_cpf,
            alunocurso_data_matricula,
            alunocurso_situacao,
            pessoa.con_contato_email,
            pessoa.con_contato_celular,
           curso.curso_id,
           CONCAT(
              CONVERT(camp_nome USING UTF8),' / ',
              CONVERT(IF(
                  curso_sigla='' OR curso_sigla IS NULL,
                  curso_nome,
                  curso_sigla
              ) USING utf8)
          ) AS campus_curso_nome,
          campus_curso.cursocampus_id cursocampusId
        FROM acadgeral__aluno
        INNER JOIN pessoa_fisica ON pessoa_fisica.pes_id=acadgeral__aluno.pes_id
        LEFT JOIN pessoa ON pessoa.pes_id=pessoa_fisica.pes_id
        LEFT JOIN endereco ON endereco.pes_id=pessoa.pes_id
        LEFT JOIN acadgeral__aluno_curso alunoCurso ON alunoCurso.aluno_id=acadgeral__aluno.aluno_id
        LEFT JOIN campus_curso USING(cursocampus_id)
        LEFT JOIN org_campus campus USING(camp_id)
        LEFT JOIN acad_curso curso ON curso.curso_id=campus_curso.curso_id
        WHERE 1=1
        -- CONDICOES --
        GROUP BY alunocurso_id, acadgeral__aluno.aluno_id";

        $params = array();

        $result                 = array(
            "draw"         => $data['draw'],
            "recordsTotal" => 0,
        );
        $result["recordsTotal"] = $this->executeQuery($queryTotal)->fetch(\PDO::FETCH_COLUMN);

        // verifica se existe alguma busca a ser feita
        $vSearch = $data["search"]['value'];

        if (!empty($vSearch)) {
            $conditions .= '
            AND(
                acadgeral__aluno.aluno_id like :vSearch OR
                alunoCurso.alunocurso_id like :vSearch OR
                pessoa.pes_nome like :vSearch OR
                pes_cpf like :vSearch OR
                alunocurso_situacao like :vSearch OR
                end_estado like :vSearch OR
                end_cidade like :vSearch OR
                con_contato_email like :vSearch OR
                con_contato_telefone like :vSearch OR
                con_contato_celular like :vSearch OR
                campus.camp_nome like :vSearch OR
                curso.curso_nome like :vSearch
            )';

            $params['vSearch'] = "%$vSearch%";
        }

        $query = str_replace('-- CONDICOES --', $conditions, $query);

        $orderBy = array();

        foreach ($data['order'] as $order) {
            $orderColumn    = $order["column"];
            $orderColumnDir = $order["dir"];
            $columnName     = $data['columns'][$orderColumn]["name"];

            $orderBy[] = "`{$columnName}` $orderColumnDir";
        }

        $orderBy = implode(" , ", $orderBy);

        if (!empty($orderBy)) {
            $orderBy = " ORDER BY " . $orderBy;
        }

        $query .= $orderBy;

        if ($data['length'] > 0) {
            $query .= " LIMIT {$data['length']} OFFSET {$data['start']}";
        }

        $result["data"]            = $this->executeQueryWithParam($query, $params)->fetchAll();
        $query1                    = "SELECT FOUND_ROWS()";
        $result["recordsFiltered"] = $this->executeQuery($query1)->fetch(\PDO::FETCH_COLUMN);

        return $result;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function pesquisaForJson($params)
    {
        $sql   = '
    SELECT
        pessoa.pes_id pesId, pessoa.pes_nome pesNome,
        pessoaFisica.pes_cpf pesCpf,
        pessoaFisica.pes_doc_estrangeiro pesDocEstrangeiro,
        pessoa.pes_nacionalidade pesNacionalidade,
        pessoaFisica.pes_rg pesRg,
        pessoaFisica.pes_sexo pesSexo,
        date_format(pessoaFisica.pes_data_nascimento, "%d/%m/%Y") pesDataNascimento,
        endereco.end_estado endEstado,
        endereco.end_cidade endCidade,
        aluno.aluno_id alunoId
    FROM pessoa
        INNER JOIN pessoa_fisica pessoaFisica USING (pes_id)
        LEFT JOIN endereco USING (pes_id)
        LEFT JOIN acadgeral__aluno aluno USING (pes_id)
        LEFT JOIN acadgeral__aluno_curso alunoCurso USING (aluno_id)
    WHERE';
        $query = false;

        if ($params['query']) {
            $query = trim($params['query']);
        }
        if ($params['pesId']) {
            $parameters['pes_id'] = trim($params['pesId']);
            $sql .= ' pes_id = :pes_id';
        } elseif ($params['pesCpf']) {
            $parameters['pes_cpf'] = trim($params['pesCpf']);
            $sql .= ' pes_cpf like :pes_cpf';
        } elseif ($params['pesDocEstrangeiro']) {
            $parameters['pes_doc_estrangeiro'] = trim($params['pesDocEstrangeiro']);
            $sql .= ' pes_doc_estrangeiro = :pes_doc_estrangeiro';
        } else {
            $parameters = array(
                'pes_nome'      => "{$query}%",
                'alunocurso_id' => ltrim($query, '0') . "%",
                'pes_id'        => ltrim($query, '0') . "%"
            );
            $sql .= ' (alunocurso_id*1 LIKE :alunocurso_id OR pessoa.pes_id*1 LIKE :pes_id OR pes_nome COLLATE UTF8_GENERAL_CI LIKE :pes_nome)';
        }

        if ($params['somenteAlunos']) {
            $sql .= ' AND aluno_id IS NOT NULL';
        }

        if ($params['agruparAluno']) {
            $sql .= ' GROUP BY pes_id, aluno_id';
        }

        $sql .= " ORDER BY pes_nome";

        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    /**
     * @param $usuarioId
     * @return mixed
     */
    public function buscaDisciplinasUsuario($usuarioId)
    {
        $situacaoId = $this
            ->getRepository('Matricula\Entity\AcadgeralSituacao')
            ->buscaSituacoes(array('Matriculado', 'Adaptante', 'Dependente'));
        $situacaoId = implode(',', $situacaoId);

        $perAtual = (new \Matricula\Service\AcadperiodoLetivo($this->getEm()))->buscaPeriodoCorrente(true);
        $periodos = '';

        /** @var \Matricula\Entity\AcadperiodoLetivo $entidade */
        foreach ($perAtual as $entidade) {
            $periodos = $periodos ? $periodos . ',' . $entidade->getPerId() : $entidade->getPerId();
        }

        $alunodisc = $this->executeQuery(
            "
            SELECT
              alunodisc.*, disc.*
            FROM
              acesso_pessoas ace
            INNER JOIN
              pessoa_fisica pf ON pf.pes_id = ace.pes_fisica
            INNER JOIN
              acadgeral__aluno aluno ON pf.pes_id = aluno.pes_id
            INNER JOIN
               acadgeral__aluno_curso acurso ON aluno.aluno_id = acurso.aluno_id
            INNER JOIN
               acadperiodo__aluno alunoper ON alunoper.alunocurso_id = acurso.alunocurso_id
            INNER JOIN
               acadperiodo__turma turma ON  turma.turma_id = alunoper.turma_id
            INNER JOIN
               acadperiodo__aluno_disciplina alunodisc ON alunodisc.alunoper_id = alunoper.alunoper_id -- AND turma.turma_id = alunodisc.turma_id
            INNER JOIN
               acadgeral__disciplina disc ON alunodisc.disc_id = disc.disc_id
            WHERE
                ace.id = {$usuarioId} AND turma.per_id in({$periodos}) AND alunodisc.situacao_id in({$situacaoId})
            ORDER BY disc.disc_nome
        "
        )->fetchAll();

        return $alunodisc;
    }

    /**
     * @param $usuarioId
     * @return mixed
     */
    public function buscaAlunoPer($usuarioId)
    {
        $perAtual = (new \Matricula\Service\AcadperiodoLetivo($this->getEm()))->buscaPeriodoCorrente();
        $alunoper = $this->executeQuery(
            "
            SELECT
              alunoper.alunoper_id
            FROM
              acesso_pessoas ace
            INNER JOIN
              pessoa_fisica pf ON pf.pes_id = ace.pes_fisica
            INNER JOIN
              acadgeral__aluno aluno ON pf.pes_id = aluno.pes_id
            INNER JOIN
               acadgeral__aluno_curso acurso ON aluno.aluno_id = acurso.aluno_id
            INNER JOIN
               acadperiodo__aluno alunoper ON alunoper.alunocurso_id = acurso.alunocurso_id
            INNER JOIN
               acadperiodo__turma turma ON  turma.turma_id = alunoper.turma_id
            WHERE
                ace.id = {$usuarioId}  AND turma.per_id = {$perAtual->getPerId()}
        "
        )->fetchAll();
        if ($alunoper) {
            return $alunoper[0]['alunoper_id'];
        } else {
            return $alunoper;
        }
    }

    /**
     * @param array   $arrDados
     * @param boolean $cadastrarNoPeriodoLetivo
     * @return bool
     * @throws \Exception
     */
    public function registroSimplificadoDeAluno(array &$arrDados, $cadastrarNoPeriodoLetivo = false)
    {
        $serviceAcadperiodoAluno  = new \Matricula\Service\AcadperiodoAluno($this->getEm());
        $serviceAcadgeralSituacao = new AcadgeralSituacao($this->getEm());
        $serviceSisCampoValor     = new \Sistema\Service\SisCampoValor($this->getEm());

        $arrAlunoCurso = [
            'alunocursoCarteira'        => '',
            'alunocursoObservacoes'     => '',
            'pesIdAgente'               => $arrDados['pesIdAgente'],
            'origem'                    => $arrDados['origem'],
            'captacao'                  => $arrDados['captacao'],
            'alunocursoMediador'        => $arrDados['alunocursoMediador'],
            'alunocursoPessoaIndicacao' => $arrDados['alunocursoPessoaIndicacao'],
            'alunocursoSituacao'        => \Matricula\Service\AcadgeralAlunoCurso::ALUNOCURSO_SITUACAO_PENDENTE,
            'cursocampusId'             => $arrDados['campusCurso'],
            'tiposelId'                 => $this->getConfig()->localizarChave('ALUNO_FORMA_INGRESSO_PADRAO')
        ];

        $arrDados['unidade'] = $arrDados['unidade'] ? $arrDados['unidade'] : $arrDados['unidadeId'];

        if ($arrDados['alunoId']) {
            $serviceAlunoConfigPgtoCurso = new \Financeiro\Service\FinanceiroAlunoConfigPgtoCurso($this->getEm());
            $serviceAcadgeralAlunoCurso  = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
            $serviceOrgUnidadeEstudo     = new \Organizacao\Service\OrgUnidadeEstudo($this->getEm());
            $serviceOrgAgenteAducacional = new \Organizacao\Service\OrgAgenteEducacional($this->getEm());

            $serviceAcadgeralAlunoCurso->setConfig($this->getConfig());

            try {
                $this->getEm()->beginTransaction();

                /** @var \Matricula\Entity\AcadgeralAluno $objAcadgeralAluno */
                $objAcadgeralAluno = $this->getRepository()->find($arrDados['alunoId']);

                if (!$objAcadgeralAluno) {
                    $this->setLastError('Registro de aluno não existe!');

                    return false;
                }

                if ($arrDados['alunocursoMediador'] && !$objAcadgeralAluno->getAlunoMediador()) {
                    $objAcadgeralAluno->setAlunoMediador($arrDados['alunocursoMediador']);
                }

                if (trim($arrDados['alunocursoPessoaIndicacao']) && !$objAcadgeralAluno->getAlunoPessoaIndicacao()) {
                    $objAcadgeralAluno->setAlunoPessoaIndicacao($arrDados['alunocursoPessoaIndicacao']);
                }

                if ($arrDados['unidade'] && !$objAcadgeralAluno->getUnidade()) {
                    /** @var $objOrgUnidadeEstudo \Organizacao\Entity\OrgUnidadeEstudo */
                    $objOrgUnidadeEstudo = $serviceOrgUnidadeEstudo->getRepository()->find($arrDados['unidade']);

                    if (!$objOrgUnidadeEstudo) {
                        $this->setLastError('Registro de unidade estudo não existe!');

                        return false;
                    }

                    $objAcadgeralAluno->setUnidade($objOrgUnidadeEstudo);
                }

                if ($arrDados['pesIdAgente'] && !$objAcadgeralAluno->getPesIdAgenciador()) {
                    /** @var $objAgenteEducacional \Organizacao\Entity\OrgAgenteEducacional */
                    $objAgenteEducacional = $serviceOrgAgenteAducacional->getRepository()->find(
                        $arrDados['pesIdAgente']
                    );

                    if (!$objAgenteEducacional) {
                        $this->setLastError('Registro de agente não existe!');

                        return false;
                    }

                    $objAcadgeralAluno->setPesIdAgenciador($objAgenteEducacional);
                }

                $arrDados['pesIdAgenciador'] = $objAcadgeralAluno->getPesIdAgenciador();

                $arrAlunoCurso['alunoId'] = $objAcadgeralAluno->getAlunoId();

                $cursoExistente = $serviceAcadgeralAlunoCurso->verificaSeCursoEstaRegistradoParaAluno(
                    $objAcadgeralAluno->getAlunoId(),
                    $arrAlunoCurso['cursocampusId']
                );

                if ($cursoExistente) {
                    $this->setLastError('O aluno já se encontra registrado neste curso!');

                    return true;
                }

                $this->equipararArrays($arrAlunoCurso, $arrDados);

                $salvouAluno = $serviceAcadgeralAlunoCurso->salvarAlunoCurso($arrAlunoCurso);

                if (!$salvouAluno) {
                    throw new \Exception($serviceAcadgeralAlunoCurso->getLastError());
                }

                $arrDados['alunoCurso'] = [$arrAlunoCurso];

                if (!$serviceAlunoConfigPgtoCurso->salvarMultiplos($arrDados)) {
                    throw new \Exception($serviceAlunoConfigPgtoCurso->getLastError());
                }

                if ($cadastrarNoPeriodoLetivo) {
                    $arrDados['alunocursoId'] = $arrAlunoCurso['alunocursoId'];
                    $arrDados['situacaoId']   = $serviceAcadgeralSituacao::MATRICULA_PROVISORIA;

                    $serviceAcadperiodoAluno->salvarAlunoPeriodo($arrDados);

                    if ($arrDados['alunoperId']) {
                        $serviceAlunoDisc = new AcadperiodoAlunoDisciplina($this->getEm());

                        foreach ($arrDados['disciplina'] as $discId) {
                            $arrDados['discId'] = $discId;

                            $serviceAlunoDisc->salvarAlunoDisciplina($arrDados);
                        }
                    }
                }

                if ($arrDados['camposPersonalizados']) {
                    $arrAux                         = array();
                    $arrAux['camposPersonalizados'] = $arrDados['camposPersonalizados'];
                    $arrAux['pesId']                = $objAcadgeralAluno->getPes()->getPes()->getPesId();

                    /** @var $objAlunoCurso \Matricula\Entity\AcadgeralAlunoCurso */
                    $objAlunoCurso = $serviceAcadgeralAlunoCurso->getRepository()->findOneBy(
                        ['aluno' => $arrDados['alunoId'], 'cursocampus' => $arrDados['campusCurso']]
                    );

                    if ($objAlunoCurso) {
                        $arrAux['alunocursoId'] = $objAlunoCurso->getAlunocursoId();
                    }

                    $serviceSisCampoValor->salvaMultiplosBuscandoChave($arrAux, false);
                }

                $this->getEm()->persist($objAcadgeralAluno);
                $this->getEm()->flush($objAcadgeralAluno);
                $this->getEm()->commit();

                $arrDados['alunoId'] = $objAcadgeralAluno->getAlunoId();

                return $this->salvarSomenteAlunoEFormacao($arrDados);
            } catch (\Exception $ex) {
                $this->setLastError('Não foi possível salvar o registro de aluno!<br>' . $ex->getMessage());
            }

            return false;
        }

        $arrDados['pesNacionalidade'] = \Pessoa\Service\Pessoa::PES_NACIONALIDADE_BRASILEIRO;
        $arrDados['pesIdAgenciador']  = $arrDados['pesIdAgente'];
        $arrDados['alunoMediador']    = $arrDados['alunocursoMediador'];

        if ($arrDados['formacaoInstituicao']) {
            $arrDados['formacoes'] = [
                'formacaoTipo'        => $arrDados['formacaoTipo'],
                'formacaoTipoEnsino'  => $arrDados['formacaoTipoEnsino'],
                'formacaoAno'         => $arrDados['formacaoAno'],
                'formacaoInstituicao' => $arrDados['formacaoInstituicao'],
                'formacaoCurso'       => $arrDados['formacaoCurso'],
                'formacaoRegime'      => $arrDados['formacaoRegime'],
                'formacaoDuracao'     => $arrDados['formacaoDuracao'],
                'formacaoConcluido'   => $arrDados['formacaoConcluido'],
                'formacaoCidade'      => $arrDados['formacaoCidade'],
                'formacaoEstado'      => $arrDados['formacaoEstado'],
            ];

            if ($arrDados['arrFormacoes']) {
                $arrDados['formacoes'] = $arrDados['arrFormacoes'];
            }

            $arrDados['formacoes'] = json_encode([$arrDados['formacoes']]);
        }

        $arrDados['alunoCurso'] = [];

        $arrDados['alunoCurso'][] = $arrAlunoCurso;

        if ($this->salvarAluno($arrDados)) {
            if ($cadastrarNoPeriodoLetivo && $arrDados['turmaId']) {
                $arrDados['alunocursoId'] = $arrDados['arrCodigoMatricula'][0];
                $arrDados['situacaoId']   = $serviceAcadgeralSituacao::MATRICULA_PROVISORIA;

                if (!$serviceAcadperiodoAluno->salvarAlunoPeriodo($arrDados)) {
                    $this->setLastError(
                        "Erro ao salvar registro de aluno no período. " . $serviceAcadperiodoAluno->getLastError()
                    );

                    return false;
                }

                if ($arrDados['alunoperId']) {
                    $serviceAlunoDisc = new AcadperiodoAlunoDisciplina($this->getEm());

                    foreach ($arrDados['disciplina'] as $discId) {
                        $arrDados['discId'] = $discId;

                        $serviceAlunoDisc->salvarAlunoDisciplina($arrDados);
                    }
                }
            }

            return true;
        }

        return false;
    }

    /**
     * @param array $arrDados
     * @return bool
     * @throws \Exception
     */
    public function salvarAluno(array &$arrDados)
    {
        if (!$arrDados['alunoMediador']) {
            $arrDados['alunoMediador'] = 'registro-interno';

            if ($arrDados['captacao']) {
                $arrDados['alunoMediador'] = 'captacao-externa';
            }
        }

        if (!$this->valida($arrDados)) {
            return false;
        }

        $novoAluno = false;

        $serviceSisIntegracao          = new \Sistema\Service\SisIntegracao($this->getEm());
        $servicePessoaFisica           = new \Pessoa\Service\PessoaFisica($this->getEm());
        $serviceAcadgeralResponsavel   = new \Matricula\Service\AcadgeralResponsavel($this->getEm());
        $serviceAcadgeralAlunoFormacao = new \Matricula\Service\AcadgeralAlunoFormacao($this->getEm());
        $serviceAlunoCurso             = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
        $serviceArquivoDiretorios      = new \GerenciadorArquivos\Service\ArquivoDiretorios($this->getEm());
        $serviceArquivo                = new \GerenciadorArquivos\Service\Arquivo($this->getEm());
        $serviceDocumentoPessoa        = new \Documentos\Service\DocumentoPessoa($this->getEm());
        $serviceCadastroOrigem         = new \Matricula\Service\AcadgeralCadastroOrigem($this->getEm());
        $serviceOrgAgenteEducacional   = new \Organizacao\Service\OrgAgenteEducacional($this->getEm());
        $serviceOrgUnidadeEstudo       = new \Organizacao\Service\OrgUnidadeEstudo($this->getEm());
        $serviceAlunoConfigPgtoCurso   = new \Financeiro\Service\FinanceiroAlunoConfigPgtoCurso($this->getEm());
        $serviceAcessoPessoas          = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceSisCampoValor          = new \Sistema\Service\SisCampoValor($this->getEm());

        $serviceAlunoCurso->setConfig($this->getConfig());
        $serviceAcessoPessoas->setConfig($this->getConfig());

        $objDiretorio = $serviceArquivoDiretorios->getRepository()->find(self::DIRETORIO_IMAGEM);

        if (!$objDiretorio) {
            throw new \Exception('Diretório de arquivo de questões não existe!');
        }

        $serviceArquivo->setDiretorio($objDiretorio);

        try {
            $this->getEm()->beginTransaction();

            /** @var $objAcessoPessoas \Acesso\Entity\AcessoPessoas */
            $objAcessoPessoas = $serviceAcessoPessoas->retornaUsuarioLogado();

            $objAcadgeralAluno = null;

            if ($arrDados['alunoId']) {
                /** @var $objAcadgeralAluno \Matricula\Entity\AcadgeralAluno */
                $objAcadgeralAluno = $this->getRepository()->find($arrDados['alunoId']);

                if (!$objAcadgeralAluno) {
                    $this->setLastError('Registro de aluno não existe!');

                    return false;
                }
            } else {
                if ($arrDados['pesId']) {
                    /** @var \Matricula\Entity\AcadgeralAluno $objAcadgeralAluno */
                    $objAcadgeralAluno = $this->retornaAlunoPeloPesId($arrDados['pesId']);

                    if ($objAcadgeralAluno) {
                        unset($arrDados['alunoMediador']);
                        unset($arrDados['alunoMae']);
                        unset($arrDados['alunoEtnia']);
                    }
                }

                if (!$objAcadgeralAluno) {
                    $novoAluno         = true;
                    $objAcadgeralAluno = new \Matricula\Entity\AcadgeralAluno();
                    $objAcadgeralAluno->setUsuarioCadastro($objAcessoPessoas);
                }
            }

            $alunoPessoaIndicacao = trim($objAcadgeralAluno->getAlunoPessoaIndicacao());

            if (trim($arrDados['alunoPessoaIndicacao']) != $alunoPessoaIndicacao && $alunoPessoaIndicacao) {
                $permitirEdicaoAlunoInficacao = $serviceAcessoPessoas->validaPessoaEditaPessoaIndicacaoAluno();

                if (!$permitirEdicaoAlunoInficacao) {
                    $this->setLastError("Você não tem permissão para editar a Pessoa Indicação!");

                    return false;
                }
            }

            $agente = null;

            if ($objAcadgeralAluno->getPesIdAgenciador()) {
                $agente = $objAcadgeralAluno->getPesIdAgenciador()->getPes()->getPesId();
            }

            if ($arrDados['pesIdAgenciador'] != $agente && $agente) {
                $permitirEdicaoAgente = $serviceAcessoPessoas->validaPessoaEditaAgenteAluno();

                if (!$permitirEdicaoAgente) {
                    $this->setLastError("Você não tem permissão para editar o Agente Educacional!");

                    return false;
                }
            }

            //Altera/Cria Pessoa Fisica
            $objPessoaFisica = $servicePessoaFisica->salvarPessoaFisica(
                $arrDados['pesId'],
                $arrDados,
                true,
                true,
                true
            );

            $objAcadgeralAluno->setPes($objPessoaFisica);
            $objAcadgeralAluno->setUsuarioAlteracao($objAcessoPessoas);
            $objAcadgeralAluno->setAlunoDataAlteracao(new \DateTime());

            $imagem = $arrDados['vfile'][$arrDados['arq']];

            $objArq       = null;
            $objArqAntiga = null;

            if ($imagem) {
                if (empty($imagem['raw']) && $objAcadgeralAluno->getArq()) {
                    /* @var $objArq \GerenciadorArquivos\Entity\Arquivo */
                    $objArqAntiga = $objAcadgeralAluno->getArq();
                } else {
                    $arrDados['arq'] = ($imagem['raw']) ? $imagem['raw'] : null;

                    if ($arrDados['arq'] && is_string($arrDados['arq'])) {
                        $objArq = $serviceArquivo->getRepository()->find($arrDados['arq']);
                    }
                }

                if ($imagem['file']['error'] == 0 && $imagem['file']['size'] != 0) {
                    try {
                        $objArq = $serviceArquivo->adicionar($imagem['file']);
                    } catch (\Exception $ex) {
                        throw new \Exception('Falha ao registrar imagem de aluno!');
                    }
                }
            } else {
                $objArq = null;
            }

            $arrDados['arq'] = $objArq;

            if ($arrDados['origem']) {
                /** @var $objAcadgeralCadastroOrigem \Matricula\Entity\AcadgeralCadastroOrigem */
                $objAcadgeralCadastroOrigem = $serviceCadastroOrigem->getRepository()->find($arrDados['origem']);

                if (!$objAcadgeralCadastroOrigem) {
                    $this->setLastError('Registro de cadastro origem não existe!');

                    return false;
                }

                $objAcadgeralAluno->setOrigem($objAcadgeralCadastroOrigem);
            } else {
                $objAcadgeralAluno->setOrigem(null);
            }

            if ($arrDados['pesIdAgenciador']) {
                /** @var $objOrgAgenteEducacional \Organizacao\Entity\OrgAgenteEducacional */
                $objOrgAgenteEducacional = $serviceOrgAgenteEducacional->getRepository()->find(
                    $arrDados['pesIdAgenciador']
                );

                if (!$objOrgAgenteEducacional) {
                    $this->setLastError('Registro de agente educacional não existe!');

                    return false;
                }

                $objAcadgeralAluno->setPesIdAgenciador($objOrgAgenteEducacional);
            } else {
                $objAcadgeralAluno->setPesIdAgenciador(null);
            }

            if ($arrDados['unidade']) {
                /** @var $objOrgUnidadeEstudo \Organizacao\Entity\OrgUnidadeEstudo */
                $objOrgUnidadeEstudo = $serviceOrgUnidadeEstudo->getRepository()->find($arrDados['unidade']);

                if (!$objOrgUnidadeEstudo) {
                    $this->setLastError('Registro de unidade estudo não existe!');

                    return false;
                }

                $objAcadgeralAluno->setUnidade($objOrgUnidadeEstudo);
            } else {
                $objAcadgeralAluno->setUnidade(null);
            }

            if ($arrDados['alunoDataCadastro']) {
                $arrDados['alunoDataCadastro'] = new \Datetime(
                    $this->formatDateAmericano($arrDados['alunoDataCadastro'])
                );
            } else {
                $arrDados['alunoDataCadastro'] = new \DateTime();
            }

            $objAcadgeralAluno->setArq($objArq);
            $objAcadgeralAluno->setAlunoMae($arrDados['alunoMae']);
            $objAcadgeralAluno->setAlunoPai($arrDados['alunoPai']);
            $objAcadgeralAluno->setAlunoEtnia($arrDados['alunoEtnia']);
            $objAcadgeralAluno->setAlunoMediador($arrDados['alunoMediador']);
            $objAcadgeralAluno->setAlunoCertMilitar($arrDados['alunoCertMilitar']);
            $objAcadgeralAluno->setAlunoZonaEleitoral($arrDados['alunoZonaEleitoral']);
            $objAcadgeralAluno->setAlunoDataCadastro($arrDados['alunoDataCadastro']);
            $objAcadgeralAluno->setAlunoSecaoEleitoral($arrDados['alunoSecaoEleitoral']);
            $objAcadgeralAluno->setAlunoTituloEleitoral($arrDados['alunoTituloEleitoral']);
            $objAcadgeralAluno->setAlunoPessoaIndicacao($arrDados['alunoPessoaIndicacao']);
            $objAcadgeralAluno->setAlunoObservacaoDocumento($arrDados['alunoObservacaoDocumentos']);

            $this->getEm()->persist($objAcadgeralAluno);
            $this->getEm()->flush($objAcadgeralAluno);

            if (!empty($objArqAntiga)) {
                try {
                    $serviceArquivo->excluir($objArqAntiga->getArqId());
                } catch (\Exception $ex) {
                    throw new \Exception('Falha ao remover imagem antiga de aluno!');
                }
            }

            $arrCodigoMatricula = array();

            if (!$serviceAcadgeralResponsavel->salvarMultiplos($arrDados, $objAcadgeralAluno)) {
                throw new \Exception($serviceAcadgeralResponsavel->getLastError());
            }

            if (!$serviceAlunoCurso->salvarMultiplos($arrDados, $objAcadgeralAluno, $arrCodigoMatricula)) {
                throw new \Exception($serviceAlunoCurso->getLastError());
            }

            if (!$serviceAcadgeralAlunoFormacao->salvarMultiplos($arrDados, $objAcadgeralAluno)) {
                throw new \Exception($serviceAcadgeralAlunoFormacao->getLastError());
            }

            if (!$serviceDocumentoPessoa->salvarMultiplos($arrDados['documentoGrupo'], $objPessoaFisica->getPes())) {
                throw new \Exception($serviceDocumentoPessoa->getLastError());
            }

            if (!$serviceAlunoConfigPgtoCurso->salvarMultiplos($arrDados)) {
                throw new \Exception($serviceAlunoConfigPgtoCurso->getLastError());
            }

            if ($arrDados['responsavel']) {
                if (!$this->salvarResponsavelCaptacao($arrDados['responsavel'], $objAcadgeralAluno)) {
                    return false;
                }
            }

            $this->getEm()->commit();

            $serviceSisIntegracao->integracaoInteligenteParaAluno($objAcadgeralAluno);

            $arrDados['alunoId']            = $objAcadgeralAluno->getAlunoId();
            $arrDados['arrCodigoMatricula'] = $arrCodigoMatricula;

            $criarUsuarioDeAluno = $this->getConfig()->localizarChave('ALUNO_CRIAR_USUARIO');
            $AlunoGrupo          = $this->getConfig()->localizarChave('ALUNO_ACESSO_GRUPO');

            if ($criarUsuarioDeAluno && $AlunoGrupo) {
                $serviceAcessoPessoas->registrarUsuarioPadraoEVinculaGrupo($arrDados['pesId'], $AlunoGrupo);
            }

            if ($arrDados['camposPersonalizados']) {
                $arrAux                         = array();
                $arrAux['camposPersonalizados'] = $arrDados['camposPersonalizados'];
                $arrAux['alunoId']              = $objAcadgeralAluno->getAlunoId();
                $arrAux['pesId']                = $objAcadgeralAluno->getPes()->getPes()->getPesId();

                if ($arrDados['alunoId'] && $arrDados['campusCurso']) {
                    /** @var $objAcadgeralAlunoCurso \Matricula\Entity\AcadgeralAlunoCurso */
                    $objAcadgeralAlunoCurso = $serviceAlunoCurso->getRepository()->findOneBy(
                        ['aluno' => $arrDados['alunoId'], 'cursocampus' => $arrDados['campusCurso']]
                    );

                    if ($objAcadgeralAlunoCurso) {
                        $arrAux['alunocursoId'] = $objAcadgeralAlunoCurso->getAlunocursoId();
                    }
                }

                $serviceSisCampoValor->salvaMultiplosBuscandoChave($arrAux, false, true);
            }

            //Somente enviar boas vindas para novos alunos e que possuam ao menos um curso vinculado
            if ($novoAluno && count($arrDados['alunoCurso']) > 0) {
                $serviceOrgComunicacaoModelo = new \Organizacao\Service\OrgComunicacaoModelo($this->getEm());

                $arrInfo = $serviceOrgComunicacaoModelo->trabalharVariaveisPorContexto(
                    array('alunoId' => $arrDados['alunoId']),
                    \Organizacao\Service\OrgComunicacaoTipo::TIPO_CONTEXTO_ALUNO
                );

                if (
                    $this->getConfig()->localizarChave('ENVIAR_SMS_BOAS_VINDAS_ALUNO') &&
                    $this->getConfig()->localizarChave('SMS_ATIVO')
                ) {
                    $serviceOrgComunicacaoModelo->registrarComunicacao(
                        \Organizacao\Service\OrgComunicacaoTipo::TIPO_BOAS_VINDAS_ALUNO,
                        \Organizacao\Service\OrgComunicacaoModelo::MODELO_TIPO_SMS,
                        $arrInfo
                    );
                }

                if ($this->getConfig()->localizarChave('ENVIAR_EMAIL_BOAS_VINDAS_ALUNO')) {
                    $serviceOrgComunicacaoModelo->registrarComunicacao(
                        \Organizacao\Service\OrgComunicacaoTipo::TIPO_BOAS_VINDAS_ALUNO,
                        \Organizacao\Service\OrgComunicacaoModelo::MODELO_TIPO_EMAIL,
                        $arrInfo
                    );
                }
            }

            $this->efetuaIntegracaoAluno($objAcadgeralAluno);

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de aluno!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param array $arrDados
     * @return bool
     * @throws \Exception
     */
    public function salvarSomenteAlunoEFormacao(array &$arrDados)
    {
        if (!$arrDados['alunoMediador']) {
            $arrDados['alunoMediador'] = 'registro-interno';

            if ($arrDados['captacao']) {
                $arrDados['alunoMediador'] = 'captacao-externa';
            }
        }

        if (!$this->validaDados($arrDados)) {
            return false;
        }

        $novoAluno = false;

        $serviceSisIntegracao           = new \Sistema\Service\SisIntegracao($this->getEm());
        $servicePessoaFisica            = new \Pessoa\Service\PessoaFisica($this->getEm());
        $serviceAcadgeralAlunoCurso     = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
        $serviceAcadgeralAlunoFormacao  = new \Matricula\Service\AcadgeralAlunoFormacao($this->getEm());
        $serviceAcadgeralCadastroOrigem = new \Matricula\Service\AcadgeralCadastroOrigem($this->getEm());
        $serviceOrgAgenteEducacional    = new \Organizacao\Service\OrgAgenteEducacional($this->getEm());
        $serviceOrgUnidadeEstudo        = new \Organizacao\Service\OrgUnidadeEstudo($this->getEm());
        $serviceAcessoPessoas           = new \Acesso\Service\AcessoPessoas($this->getEm());

        $serviceAcadgeralAlunoCurso->setConfig($this->getConfig());

        try {
            $this->getEm()->beginTransaction();

            /** @var $objAcessoPessoas \Acesso\Entity\AcessoPessoas */
            $objAcessoPessoas = $serviceAcessoPessoas->retornaUsuarioLogado();

            $objAcadgeralAluno = null;

            if ($arrDados['alunoId']) {
                /** @var \Matricula\Entity\AcadgeralAluno $objAcadgeralAluno */
                $objAcadgeralAluno = $this->getRepository()->find($arrDados['alunoId']);

                if (!$objAcadgeralAluno) {
                    $this->setLastError('Registro de aluno não existe!');

                    return false;
                }
            } else {
                if ($arrDados['pesId']) {
                    /** @var \Matricula\Entity\AcadgeralAluno $objAcadgeralAluno */
                    $objAcadgeralAluno = $this->retornaAlunoPeloPesId($arrDados['pesId']);

                    if ($objAcadgeralAluno) {
                        unset($arrDados['alunoMediador']);
                        unset($arrDados['alunoMae']);
                        unset($arrDados['alunoEtnia']);
                    }
                }

                if (!$objAcadgeralAluno) {
                    $novoAluno         = true;
                    $objAcadgeralAluno = new \Matricula\Entity\AcadgeralAluno();
                    $objAcadgeralAluno->setUsuarioCadastro($objAcessoPessoas);
                }
            }

            //Altera/Cria Pessoa Fisica
            $objPessoaFisica = $servicePessoaFisica->salvarPessoaFisica(
                $arrDados['pesId'],
                $arrDados,
                true,
                true,
                true
            );

            $objAcadgeralAluno->setPes($objPessoaFisica);
            $objAcadgeralAluno->setUsuarioAlteracao($objAcessoPessoas);
            $objAcadgeralAluno->setAlunoDataAlteracao(new \DateTime());

            if ($arrDados['arrFormacoes']) {
                $arrDados['formacoes'] = $arrDados['arrFormacoes'];
            }

            if (
                $arrDados['formacoes'] &&
                !$serviceAcadgeralAlunoFormacao->salvarMultiplos($arrDados, $objAcadgeralAluno)
            ) {
                throw new \Exception($serviceAcadgeralAlunoFormacao->getLastError());
            }

            if ($arrDados['origem']) {
                /** @var $objAcadgeralCadastroOrigem \Matricula\Entity\AcadgeralCadastroOrigem */
                $objAcadgeralCadastroOrigem = $serviceAcadgeralCadastroOrigem->getRepository()->find(
                    $arrDados['origem']
                );

                if (!$objAcadgeralCadastroOrigem) {
                    $this->setLastError('Registro de cadastro origem não existe!');

                    return false;
                }

                $objAcadgeralAluno->setOrigem($objAcadgeralCadastroOrigem);
            } else {
                $objAcadgeralAluno->setOrigem(null);
            }

            if ($arrDados['pesIdAgenciador']) {
                /** @var $objOrgAgenteEducacional \Organizacao\Entity\OrgAgenteEducacional */
                $objOrgAgenteEducacional = $serviceOrgAgenteEducacional->getRepository()->find(
                    $arrDados['pesIdAgenciador']
                );

                if (!$objOrgAgenteEducacional) {
                    $this->setLastError('Registro de agente educacional não existe!');

                    return false;
                }

                $objAcadgeralAluno->setPesIdAgenciador($objOrgAgenteEducacional);
            } else {
                $objAcadgeralAluno->setPesIdAgenciador(null);
            }

            if ($arrDados['unidade']) {
                /** @var $objOrgUnidadeEstudo \Organizacao\Entity\OrgUnidadeEstudo */
                $objOrgUnidadeEstudo = $serviceOrgUnidadeEstudo->getRepository()->find($arrDados['unidade']);

                if (!$objOrgUnidadeEstudo) {
                    $this->setLastError('Registro de unidade estudo não existe!');

                    return false;
                }

                $objAcadgeralAluno->setUnidade($objOrgUnidadeEstudo);
            } else {
                $objAcadgeralAluno->setUnidade(null);
            }

            if ($arrDados['alunoDataCadastro']) {
                $arrDados['alunoDataCadastro'] = new \Datetime(
                    $this->formatDateAmericano($arrDados['alunoDataCadastro'])
                );
            } elseif (isset($arrDados['alunoDataCadastro'])) {
                $arrDados['alunoDataCadastro'] = new \DateTime();
            }
            if ($arrDados['alunoMae']) {
                $objAcadgeralAluno->setAlunoMae($arrDados['alunoMae']);
            }

            if ($arrDados['alunoPai']) {
                $objAcadgeralAluno->setAlunoPai($arrDados['alunoPai']);
            }

            if ($arrDados['alunoEtnia']) {
                $objAcadgeralAluno->setAlunoEtnia($arrDados['alunoEtnia']);
            }

            if ($arrDados['alunoMediador']) {
                $objAcadgeralAluno->setAlunoMediador($arrDados['alunoMediador']);
            }

            if ($arrDados['alunoCertMilitar']) {
                $objAcadgeralAluno->setAlunoCertMilitar($arrDados['alunoCertMilitar']);
            }

            if ($arrDados['alunoZonaEleitoral']) {
                $objAcadgeralAluno->setAlunoZonaEleitoral($arrDados['alunoZonaEleitoral']);
            }

            if ($arrDados['alunoDataCadastro']) {
                $objAcadgeralAluno->setAlunoDataCadastro($arrDados['alunoDataCadastro']);
            }

            if ($arrDados['alunoSecaoEleitoral']) {
                $objAcadgeralAluno->setAlunoSecaoEleitoral($arrDados['alunoSecaoEleitoral']);
            }

            if ($arrDados['alunoTituloEleitoral']) {
                $objAcadgeralAluno->setAlunoTituloEleitoral($arrDados['alunoTituloEleitoral']);
            }

            if ($arrDados['alunoPessoaIndicacao']) {
                $objAcadgeralAluno->setAlunoPessoaIndicacao($arrDados['alunoPessoaIndicacao']);
            }

            $this->getEm()->persist($objAcadgeralAluno);
            $this->getEm()->flush($objAcadgeralAluno);

            $this->getEm()->commit();

            $serviceSisIntegracao->integracaoInteligenteParaAluno($objAcadgeralAluno);

            $arrDados['alunoId'] = $objAcadgeralAluno->getAlunoId();

            $criarUsuarioDeAluno = $this->getConfig()->localizarChave('ALUNO_CRIAR_USUARIO');
            $AlunoGrupo          = $this->getConfig()->localizarChave('ALUNO_ACESSO_GRUPO');

            if ($criarUsuarioDeAluno && $AlunoGrupo) {
                $serviceAcessoPessoas->registrarUsuarioPadraoEVinculaGrupo($arrDados['pesId'], $AlunoGrupo);
            }

            //Somente enviar boas vindas para novos alunos e que possuam ao menos um curso vinculado
            if ($novoAluno && count($arrDados['alunoCurso']) > 0) {
                $serviceOrgComunicacaoModelo = new \Organizacao\Service\OrgComunicacaoModelo($this->getEm());

                $arrInfo = $serviceOrgComunicacaoModelo->trabalharVariaveisPorContexto(
                    array('alunoId' => $arrDados['alunoId']),
                    \Organizacao\Service\OrgComunicacaoTipo::TIPO_CONTEXTO_ALUNO
                );

                if (
                    $this->getConfig()->localizarChave('ENVIAR_SMS_BOAS_VINDAS_ALUNO') &&
                    $this->getConfig()->localizarChave('SMS_ATIVO')
                ) {
                    $serviceOrgComunicacaoModelo->registrarComunicacao(
                        \Organizacao\Service\OrgComunicacaoTipo::TIPO_BOAS_VINDAS_ALUNO,
                        \Organizacao\Service\OrgComunicacaoModelo::MODELO_TIPO_SMS,
                        $arrInfo
                    );
                }

                if ($this->getConfig()->localizarChave('ENVIAR_EMAIL_BOAS_VINDAS_ALUNO')) {
                    $serviceOrgComunicacaoModelo->registrarComunicacao(
                        \Organizacao\Service\OrgComunicacaoTipo::TIPO_BOAS_VINDAS_ALUNO,
                        \Organizacao\Service\OrgComunicacaoModelo::MODELO_TIPO_EMAIL,
                        $arrInfo
                    );
                }
            }

            $this->efetuaIntegracaoAluno($objAcadgeralAluno);

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de aluno!<br>' . $e->getMessage());
        }

        return false;
    }

    public function efetuaIntegracaoAluno(\Matricula\Entity\AcadgeralAluno $objAcadgeralAluno, $capturarErro = false)
    {
        if ($this->getConfig()->localizarChave('EFETUAR_INTEGRACAO_ALUNO')) {
            $serviceSisFilaIntegracao = new \Sistema\Service\SisFilaIntegracao($this->getEm());

            if (!$serviceSisFilaIntegracao->registrarAlunoNaFilaDeIntegracao($objAcadgeralAluno)) {
                if ($capturarErro) {
                    $this->setLastError($serviceSisFilaIntegracao->getLastError());
                }

                return false;
            }
        }

        return true;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $serviceEndereco               = new \Pessoa\Service\Endereco($this->getEm());
        $servicePessoaFisica           = new \Pessoa\Service\PessoaFisica($this->getEm());
        $serviceAcadgeralResponsavel   = new \Matricula\Service\AcadgeralResponsavel($this->getEm());
        $serviceAcadgeralAlunoFormacao = new \Matricula\Service\AcadgeralAlunoFormacao($this->getEm());
        $serviceAcadgeralAlunoCurso    = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());

        $errors = array();

        $arrParam['desabilitarObrigatoriedadeCpf'] = $this->getConfig()->localizarChave(
            'DESABILITAR_OBRIGATORIEDADE_CPF'
        );

        if (!$servicePessoaFisica->valida($arrParam)) {
            $errors[] = $servicePessoaFisica->getLastError();
        }

        if (!$arrParam['conContatoCelular']) {
            $errors[] = 'Por favor preencha o campo "celular"!';
        }

        if (!$arrParam['conContatoEmail']) {
            $errors[] = 'Por favor preencha o campo "email"!';
        }

        if ($arrParam['alunoEtnia'] && !in_array($arrParam['alunoEtnia'], self::getAlunoEtnia())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "etnia"!';
        }

        if (!$serviceEndereco->valida($arrParam)) {
            $errors[] = $serviceEndereco->getLastError();
        }

        if (!$serviceAcadgeralResponsavel->validacaoMultipla($arrParam)) {
            $errors[] = $serviceAcadgeralResponsavel->getLastError();
        }

        if (!$serviceAcadgeralAlunoFormacao->validacaoMultipla($arrParam)) {
            $errors[] = $serviceAcadgeralAlunoFormacao->getLastError();
        }

        if (!$serviceAcadgeralAlunoCurso->validacaoMultipla($arrParam)) {
            $errors[] = $serviceAcadgeralAlunoCurso->getLastError();
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public static function getAlunoEtnia()
    {
        return array(
            self::ALUNO_ETNIA_BRANCA,
            self::ALUNO_ETNIA_PRETA,
            self::ALUNO_ETNIA_AMARELA,
            self::ALUNO_ETNIA_PARDA,
            self::ALUNO_ETNIA_INDIGENA,
            self::ALUNO_ETNIA_NAO_DISPOE_DA_INFORMACAO,
            self::ALUNO_ETNIA_NAO_DECLARADO
        );
    }

    /**
     * @param $pesId
     * @return array
     */
    public function getAlunoByPesId($pesId)
    {
        /* @var $objAcadgeralAluno \Matricula\Entity\AcadgeralAluno */
        $objAcadgeralAluno = $this->getRepository()->findOneBy(['pes' => $pesId]);

        if ($objAcadgeralAluno) {
            $alunoId = $objAcadgeralAluno->getAlunoId();
        }

        return $this->getArray($alunoId);
    }

    /**
     * @param $alunoId
     * @return array
     */
    public function getArray($alunoId)
    {
        if (!$alunoId) {
            return array();
        }

        /* @var $objAcadgeralAluno \Matricula\Entity\AcadgeralAluno */
        $objAcadgeralAluno = $this->getRepository()->find($alunoId);

        if (!$objAcadgeralAluno) {
            $this->setLastError('Aluno não existe!');

            return array();
        }

        $serviceAlunoFormacao      = new \Matricula\Service\AcadgeralAlunoFormacao($this->getEm());
        $servicePessoaFisica       = new \Pessoa\Service\PessoaFisica($this->getEm());
        $serviceAlunoCurso         = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
        $serviceResponsavel        = new \Matricula\Service\AcadgeralResponsavel($this->getEm());
        $serviceDocumentoPessoa    = new \Documentos\Service\DocumentoPessoa($this->getEm());
        $serviceArquivo            = new \GerenciadorArquivos\Service\Arquivo($this->getEm());
        $serviceSelecaoInscricao   = new \Vestibular\Service\SelecaoInscricao($this->getEm());
        $servicePessoaNecessidades = new \Pessoa\Service\PessoaNecessidades($this->getEm());
        $serviceAcessoPessoas      = new \Acesso\Service\AcessoPessoas($this->getEm());

        try {
            $arrDados = $objAcadgeralAluno->toArray();
            $pesId    = $objAcadgeralAluno->getPes()->getPes()->getPesId();
            $alunoId  = $objAcadgeralAluno->getAlunoId();

            $arrDadosPessoa = $servicePessoaFisica->getArray($pesId);

            $arrDados = array_merge($arrDados, $arrDadosPessoa);

            $arq    = $arrDados['arq'];
            $arrArq = null;

            if ($arq) {
                if (is_object($arq)) {
                    $objArquivo = $arq;
                } elseif (is_array($arq)) {
                    $objArquivo = $serviceArquivo->getRepository()->find($arq['id']);
                } else {
                    $objArquivo = $serviceArquivo->getRepository()->find($arq);
                }

                $arrArq = array(
                    'id'      => $objArquivo->getArqId(),
                    'nome'    => $objArquivo->getArqNome(),
                    'tipo'    => $objArquivo->getArqTipo(),
                    'tamanho' => $objArquivo->getArqTamanho(),
                    'chave'   => $objArquivo->getArqChave(),
                );
            }

            $arrAcesso = $serviceAcessoPessoas->buscaUsuario($pesId);
            $arrAcesso = $arrAcesso ? $arrAcesso[0] : array();

            $arrDados['arq']                = $arrArq;
            $arrDados['documentoGrupo']     = $serviceDocumentoPessoa->getArrayDocumentosPessoa($pesId);
            $arrDados['alunoCurso']         = $serviceAlunoCurso->getArrayAlunoCurso(['aluno' => $alunoId]);
            $arrDados['formacoes']          = $serviceAlunoFormacao->getArrayFormacoes($alunoId);
            $arrDados['responsaveis']       = $serviceResponsavel->getArrayResponsaveis($alunoId);
            $arrDados['vestibular']         = $serviceSelecaoInscricao->retornaUltimaClassificacaoPessoaVestibular(
                $pesId
            );
            $arrDados['pessoaNecessidades'] = $servicePessoaNecessidades->getArrayPessoaNecessidades($pesId);
            $arrDados['acesso']             = $arrAcesso;
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $alunoId
     * @param $pesId
     * @return array
     */
    public function getArrayFallback($alunoId, $pesId)
    {
        $arrDados = $this->getArray($alunoId);

        $serviceCampusCurso         = new \Matricula\Service\CampusCurso($this->getEm());
        $serviceAcadgeralAlunoCurso = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());

        if (!$arrDados) {
            $servicePessoaFisica     = new \Pessoa\Service\PessoaFisica($this->getEm());
            $objAcadgeralAluno       = new \Matricula\Entity\AcadgeralAluno();
            $serviceSelecaoInscricao = new \Vestibular\Service\SelecaoInscricao($this->getEm());

            $arrDados               = $servicePessoaFisica->getArray($pesId);
            $arrDados               = array_merge($objAcadgeralAluno->toArray(), $arrDados);
            $arrDados['vestibular'] = $serviceSelecaoInscricao->retornaUltimaClassificacaoPessoaVestibular($pesId);
        }

        $arrObjAlunoCurso = $serviceAcadgeralAlunoCurso->getRepository()->findBy(array('aluno' => $alunoId));

        /** @var \Matricula\Entity\AcadgeralAlunoCurso $objAlunoCurso */
        foreach ($arrObjAlunoCurso as $objAlunoCurso) {
            $cursocampusId = $objAlunoCurso->getCursocampus()->getCursocampusId();

            $arrDados['campusCurso'][] = $serviceCampusCurso->getArrSelect2(array('id' => $cursocampusId));
        }

        $arrDados['selecaoTipo'] = $this->getArrSelecaoTipoPeloAlunoId($arrDados['alunoId']);

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceCampusCurso             = new \Matricula\Service\CampusCurso($this->getEm());
        $serviceArquivo                 = new \GerenciadorArquivos\Service\Arquivo($this->getEm());
        $serviceNecessidadesEspeciais   = new \Pessoa\Service\NecessidadesEspeciais($this->getEm());
        $serviceAcadgeralCadastroOrigem = new \Matricula\Service\AcadgeralCadastroOrigem($this->getEm());
        $serviceOrgAgenteEducacional    = new \Organizacao\Service\OrgAgenteEducacional($this->getEm());
        $serviceOrgUnidadeEstudo        = new \Organizacao\Service\OrgUnidadeEstudo($this->getEm());

        $arrDados['alunoCurso'] = (
        is_string($arrDados['alunoCurso'])
            ?
            json_decode($arrDados['alunoCurso'], true)
            :
            $arrDados['alunoCurso']
        );

        $arrDados['arrCampusCursoSelecao'] = array();

        foreach ($arrDados['alunoCurso'] as $alunoCurso) {
            $arrDados['arrCampusCursoSelecao'][] = $alunoCurso['cursocampusId'];
        }

        $arrDados['responsaveis'] = (
        is_string($arrDados['responsaveis'])
            ?
            json_decode($arrDados['responsaveis'], true)
            :
            $arrDados['responsaveis']
        );

        $arrDados['formacoes'] = (
        is_string($arrDados['formacoes'])
            ?
            json_decode($arrDados['formacoes'], true)
            :
            $arrDados['formacoes']
        );

        if ($arrDados['pessoaNecessidades'] && is_string($arrDados['pessoaNecessidades'])) {
            $arrDados['pessoaNecessidades'] = explode(',', $arrDados['pessoaNecessidades']);
            $arrDados['pessoaNecessidades'] = $serviceNecessidadesEspeciais->getArrSelect2(
                array(
                    'id' => $arrDados['pessoaNecessidades']
                )
            );
        }

        $arq    = $arrDados['arq'];
        $arrArq = null;

        if ($arq) {
            if (is_object($arq)) {
                $objArquivo = $arq;
            } elseif (is_array($arq)) {
                $objArquivo = $serviceArquivo->getRepository()->find($arq['id']);
            } else {
                $objArquivo = $serviceArquivo->getRepository()->find($arq);
            }

            if ($objArquivo) {
                $arrArq = array(
                    'id'      => $objArquivo->getArqId(),
                    'nome'    => $objArquivo->getArqNome(),
                    'tipo'    => $objArquivo->getArqTipo(),
                    'tamanho' => $objArquivo->getArqTamanho(),
                    'chave'   => $objArquivo->getArqChave(),
                );
            }
        }

        $arrDados['arq'] = $arrArq;

        if ($arrDados['campusCurso'] && !in_array($arrDados['campusCurso'], $arrDados['arrCampusCursoSelecao'])) {
            $arrDados['campusCurso'] = $serviceCampusCurso->getArrSelect2(
                array('id' => $arrDados['campusCurso'])
            );
            $arrDados['campusCurso'] = $arrDados['campusCurso'][0];
        } else {
            $arrDados['campusCurso'] = null;
        }

        if ($arrDados['origem'] && !is_array($arrDados['origem'])) {
            $arrDados['origem'] = $serviceAcadgeralCadastroOrigem->getArrSelect2(['id' => $arrDados['origem']]);
            $arrDados['origem'] = $arrDados['origem'] ? $arrDados['origem'][0] : null;
        }

        if ($arrDados['unidade'] && !is_array($arrDados['unidade'])) {
            $arrDados['unidade'] = $serviceOrgUnidadeEstudo->getArrSelect2(['id' => $arrDados['unidade']]);
            $arrDados['unidade'] = $arrDados['unidade'] ? $arrDados['unidade'][0] : null;
        }

        if ($arrDados['pesIdAgente'] && !is_array($arrDados['pesIdAgente'])) {
            $arrDados['pesIdAgente'] = $serviceOrgAgenteEducacional->getArrSelect2(['id' => $arrDados['pesIdAgente']]);
            $arrDados['pesIdAgente'] = $arrDados['pesIdAgente'] ? $arrDados['pesIdAgente'][0] : null;
        }

        if ($arrDados['pesIdAgenciador'] && !is_array($arrDados['pesIdAgenciador'])) {
            $arrDados['pesIdAgenciador'] = $serviceOrgAgenteEducacional->getArrSelect2(
                ['id' => $arrDados['pesIdAgenciador']]
            );
            $arrDados['pesIdAgenciador'] = $arrDados['pesIdAgenciador'] ? $arrDados['pesIdAgenciador'][0] : null;
        }

        if (is_object($arrDados['alunoDataCadastro'])) {
            /** @var \Datetime $alunoDataCadastro */
            $alunoDataCadastro             = $arrDados['alunoDataCadastro'];
            $arrDados['alunoDataCadastro'] = $alunoDataCadastro->format('d/m/Y H:i:s');
        }
        if (is_object($arrDados['alunoDataAlteracao'])) {
            /** @var \Datetime $alunoDataAlteracao */
            $alunoDataAlteracao             = $arrDados['alunoDataAlteracao'];
            $arrDados['alunoDataAlteracao'] = $alunoDataAlteracao->format('d/m/Y H:i:s');
        }

        return $arrDados;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['alunoId']) {
            $this->setLastError('Para remover um registro de aluno é necessário especificar o código.');

            return false;
        }

        try {
            $objAcadgeralAluno = $this->getRepository()->find($param['alunoId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAcadgeralAluno);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de aluno.');

            return false;
        }

        return true;
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function retornaTodosOsRegistros($offset = 0, $limit = 10)
    {
        $arrEntities = $this->getRepository()->findBy([], [], $limit, $offset);

        $arrEntitiesArr = array();
        /* @var $objEntity \Matricula\Entity\AcadgeralAluno */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = $this->getArray($objEntity->getAlunoId());
        }

        return $arrEntitiesArr;
    }

    /**
     * @return int
     */
    public function retornaTotalDeRegistros()
    {
        $query = $this->getEm()->createQueryBuilder()
            ->select('COUNT(a)')
            ->from('\Matricula\Entity\AcadgeralAluno', 'a')
            ->getQuery();

        $total = $query->getSingleScalarResult();

        return $total;
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     * @param array                      $arrDados
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view, $arrDados = [], $dadosCompletos = true)
    {
        $serviceAcadgeralAlunoCurso     = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
        $serviceAcadperiodoLetivo       = new \Matricula\Service\AcadperiodoLetivo($this->getEm());
        $serviceAlunoFormacao           = new \Matricula\Service\AcadgeralAlunoFormacao($this->getEm());
        $servicePessoaFisica            = new \Pessoa\Service\PessoaFisica($this->getEm());
        $serviceAcadgeralCadastroOrigem = new \Matricula\Service\AcadgeralCadastroOrigem($this->getEm());
        $serviceAcadgeralResponsavel    = new \Matricula\Service\AcadgeralResponsavel($this->getEm());
        $serviceDocumentoGrupo          = new \Documentos\Service\DocumentoGrupo($this->getEm());

        $arrPeriodoLetivo = $serviceAcadperiodoLetivo->getArrSelect2();

        if ($dadosCompletos) {
            $arrCadastroOrigem  = $serviceAcadgeralCadastroOrigem->getArrSelect2();
            $arrDocumentosGrupo = $serviceDocumentoGrupo->documentosGrupo('Aluno', $arrDados['documentoGrupo']);
            $arrRespNivel       = $serviceAcadgeralResponsavel->getArrSelect2RespNivel();
            $arrRespVinculo     = $serviceAcadgeralResponsavel->getArrSelect2RespVinculo();

            $view->setVariable("arrDocumentosGrupo", $arrDocumentosGrupo);
            $view->setVariable("arrRespNivel", $arrRespNivel);
            $view->setVariable("arrRespVinculo", $arrRespVinculo);
            $view->setVariable("arrCadastroOrigem", $arrCadastroOrigem);
            $view->setVariable("arrAlunoEtnia", $this->getArrSelect2AlunoEtnia());
        }

        $view->setVariable("arrPeriodoLetivo", $arrPeriodoLetivo);
        $view->setVariable('formaIngressoPadrao', $this->getConfig()->localizarChave('ALUNO_FORMA_INGRESSO_PADRAO'));
        $view->setVariable('alunoPossuiAgente', $this->getConfig()->localizarChave('ALUNO_POSSUI_AGENTE_EDUCACIONAL'));
        $view->setVariable('alunoPossuiUnidade', $this->getConfig()->localizarChave('ALUNO_POSSUI_UNIDADE_DE_ESTUDO'));
        $view->setVariable(
            'alunoCaptacaoFinanceiroMensagem',
            $this->getConfig()->localizarChave('ALUNO_CAPTACAO_FINANCEIRO_MENSAGEM')
        );
        $servicePessoaFisica->setarDependenciasView($view);
        $serviceAlunoFormacao->setarDependenciasView($view);
        $serviceAcadgeralAlunoCurso->setarDependenciasView($view);
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2AlunoEtnia($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getAlunoEtnia());
    }

    public function alunosVinculadosPessoas($arrPesId)
    {
        $arrPesId = implode(',', $arrPesId);
        $query    = '
        SELECT r.pes_id
        FROM acadgeral__aluno a
          LEFT JOIN acadgeral__responsavel r USING (aluno_id)
        WHERE a.pes_id IN (' . $arrPesId . ') OR r.pes_id IN (' . $arrPesId . ')
        ORDER BY pes_id DESC';

        $result = $this->executeQueryWithParam($query)->fetchAll();

        return $result;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('alunoId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Matricula\Entity\AcadgeralAluno */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getAlunoId(),
                $params['value'] => $objEntity->getPes()->getPes()->getPesNome(),
                'pesId'          => $objEntity->getPes()->getPes()->getPesId(),
            );
        }

        return $arrEntitiesArr;
    }

    /**
     * @param integer $cpf
     * @param boolean $buscarMatriculaExistente
     * @return array || boolean
     */
    public function buscaAlunoPorCpf($cpf, $buscarMatriculaExistente = false)
    {
        $arrDados = array();

        if ($buscarMatriculaExistente) {
            $query = "
                SELECT
                  group_concat(DISTINCT alunocurso_id SEPARATOR ', ') matriculaCurso,
                  group_concat(TRIM(LEADING 0 FROM alunoper_id) SEPARATOR ', ') matriculaPeriodo
                FROM
                  acadgeral__aluno_curso
                  LEFT JOIN
                  acadgeral__aluno USING(aluno_id)
                  LEFT JOIN
                  acadperiodo__aluno USING(alunocurso_id)
                  LEFT JOIN
                  pessoa_fisica USING(pes_id)
                WHERE pes_cpf = :pesCpf GROUP BY pes_id
            ";

            $result = $this->executeQueryWithParam($query, ['pesCpf' => $cpf])->fetch();

            if ($result) {
                $arrDados['matriculasEncontradas']['curso']   = $result['matriculaCurso'];
                $arrDados['matriculasEncontradas']['periodo'] = $result['matriculaPeriodo'];
            }
        }

        $query = '
        SELECT aluno_id, pes_id
        FROM pessoa_fisica a
        LEFT JOIN acadgeral__aluno USING (pes_id)
        WHERE pes_cpf = :pesCpf
        ORDER BY aluno_id DESC';

        $result = $this->executeQueryWithParam($query, ['pesCpf' => $cpf])->fetch();

        if ($result) {
            $result   = $this->getArrayFallback($result['aluno_id'], $result['pes_id']);
            $arrDados = array_merge($arrDados, $result);
        }

        return $arrDados;
    }

    public function retornaCampusCursoPorAluno($alunoId)
    {
        $serviceAlunoCurso = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
        $arrObjAlunoCurso  = $serviceAlunoCurso->getRepository()->findBy(['aluno' => $alunoId]);

        $campusCurso = array();

        if ($arrObjAlunoCurso) {
            /** @var \Matricula\Entity\AcadgeralAlunoCurso $objAlunoCurso */
            foreach ($arrObjAlunoCurso as $objAlunoCurso) {
                $campusCurso[] = $objAlunoCurso->getCursocampus()->getCursocampusId();
            }
        }

        return $campusCurso;
    }

    public function efetuarIntegracaoManual($alunoId)
    {
        /** @var \Matricula\Entity\AcadgeralAluno $objAcadgeralAluno */

        $objAcadgeralAluno = $this->getRepository()->find($alunoId);

        if (!$objAcadgeralAluno) {
            $this->setLastError('Registro de aluno não existe!');

            return false;
        }

        return $this->efetuaIntegracaoAluno($objAcadgeralAluno, true);
    }

    public function efetuarIntegracaoManualNota($alunoCurso, $forcarIntegracaoDeNotas = false)
    {
        $serviceAlunoCurso           = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
        $serviceSisFilaIntegracao    = new \Sistema\Service\SisFilaIntegracao($this->getEm());
        $serviceIntegracaoAluno      = new \Sistema\Service\SisIntegracaoAluno($this->getEm());
        $serviceSisIntegracao        = new \Sistema\Service\SisIntegracao($this->getEm());
        $serviceIntegracaoCurso      = new \Sistema\Service\SisIntegracaoCurso($this->getEm());
        $serviceIntegracaoDisciplina = new \Sistema\Service\SisIntegracaoDisciplina($this->getEm());

        $arrObjSisIntegracao = $serviceSisIntegracao->buscaIntegracoesAtivas();
        $erro                = '';
        $sucesso             = '';

        if (empty($arrObjSisIntegracao)) {
            $this->setLastError('Nenhuma integração ativa!');

            return false;
        }

        /** @var \Matricula\Entity\AcadgeralAlunoCurso $objAlunoCurso */
        $objAlunoCurso = $serviceAlunoCurso->getRepository()->find($alunoCurso);

        if (!$objAlunoCurso) {
            $this->setLastError('Registro de aluno não existe!');

            return false;
        }

        /** @var \Sistema\Entity\SisIntegracao $objSisIntegracao */
        foreach ($arrObjSisIntegracao as $objSisIntegracao) {
            $arrIntegracao = $serviceSisIntegracao->getArray($objSisIntegracao->getIntegracaoId());

            foreach ($arrIntegracao['campus'] as $campus) {
                if ($campus['campId'] == $objAlunoCurso->getCursocampus()->getCamp()->getCampId()) {
                    $alunoCodigo = $serviceIntegracaoAluno->pesquisarCodigoIntegracao(
                        $objSisIntegracao->getIntegracaoId(),
                        $objAlunoCurso->getAluno()->getAlunoId()
                    );

                    if (!$objAlunoCurso->getCursocampus()->getCurso()->verificaSeCursoPossuiPeriodoLetivo()) {
                        $codigoIntegracao = $serviceIntegracaoCurso->pesquisarCodigoIntegracao(
                            $objSisIntegracao->getIntegracaoId(),
                            $objAlunoCurso->getCursocampus()->getCurso()->getCursoId()
                        );

                        $arrCodigoDisciplinas[] = $codigoIntegracao;
                    } else {
                        $arrCodigoDisciplinas = $serviceIntegracaoDisciplina->retornaCodigoDisciplinasPorCurso(
                            $objAlunoCurso->getCursocampus()->getCurso()->getCursoId(),
                            $objSisIntegracao->getIntegracaoId(),
                            $objAlunoCurso->getAluno()->getAlunoId()
                        );
                    }

                    $integracaoId = $objSisIntegracao->getIntegracaoId();

                    if ($alunoCodigo && $integracaoId) {
                        foreach ($arrCodigoDisciplinas as $value) {
                            $vigente = is_array($value) ? $value['vigente'] : '0';
                            $codigo  = is_array($value) ? $value['codigo'] : $value;
                            $disc    = is_array($value) ? $value['disc_id'] : $objAlunoCurso->getAlunocursoId();

                            if ($forcarIntegracaoDeNotas) {
                                $vigente = '0';
                            }

                            $retorno = $serviceSisFilaIntegracao->integracaoNotas(
                                $alunoCodigo,
                                $codigo,
                                $integracaoId,
                                ($vigente ? 'Não' : 'Sim')
                            );

                            if ($retorno != 'Sucesso') {
                                if (!$erro) {
                                    $erro = 'Curso do Aluno: ' . $objAlunoCurso->getAlunocursoId();
                                }

                                $erro .= ' | Erro AVA: ' . $retorno . ' Registro: ' . $disc;
                            } else {
                                if (!$sucesso) {
                                    $sucesso = "Integração de nota realizado para o(s) registro(s): ";
                                }

                                $sucesso .= $disc . ', ';
                            }
                        }
                    }
                }
            }
        }

        $erro    = trim($erro, ' ,|.');
        $sucesso = trim($sucesso, ' ,|.');

        return $erro ? ['erro' => $erro, 'sucesso' => $sucesso] : true;
    }

    /**
     * @param $pesId
     * @return null|\Matricula\Entity\AcadgeralAluno
     */
    public function retornaAlunoPeloPesId($pesId)
    {
        /** @var \Matricula\Entity\AcadgeralAluno $objAcadgeralAluno */
        $objAcadgeralAluno = $this->getRepository()->findOneBy(['pes' => $pesId]);

        return $objAcadgeralAluno;
    }

    public function buscaLayoutCarteirinha($alunocursoId)
    {
        if (!$alunocursoId) {
            return null;
        }

        $params = array('alunocursoId' => $alunocursoId);

        $query = <<<SQL
            SELECT
              ifnull(ifnull(ac.arq_id_carteirinha,oc.arq_id_carteirinha),oi.arq_id_carteirinha) arq_id
            FROM org_ies oi
              LEFT JOIN org_campus oc ON oc.ies_id = oi.ies_id
              LEFT JOIN campus_curso cc ON cc.camp_id = oc.camp_id
              LEFT JOIN acad_curso ac ON ac.curso_id = cc.curso_id
              LEFT JOIN acadgeral__aluno_curso aac ON aac.cursocampus_id = cc.cursocampus_id
            WHERE alunocurso_id = :alunocursoId
SQL;

        $result = $this->executeQueryWithParam($query, $params)->fetch();

        if ($arqId = $result['arq_id']) {
            $serviceArquivo = new \GerenciadorArquivos\Service\Arquivo($this->getEm());
            /** @var \GerenciadorArquivos\Entity\Arquivo $objArquivo */

            if ($objArquivo = $serviceArquivo->getRepository()->find($arqId)) {
                return $objArquivo->getDiretorio()->getArqDiretorioEndereco() . '/' . $objArquivo->getArqChave();
            }
        }

        return null;
    }

    public function situacaoAlunoNaDisciplina($alunoId, $disciplinaId)
    {
        if (!$alunoId || !$disciplinaId) {
            self::setLastError("Aluno e disciplina são necessários para continuar");

            return false;
        }

        try {
            $query = <<<SQL
            SELECT
                  *
                  FROM
                acadperiodo__aluno_disciplina
                  INNER JOIN
                acadperiodo__aluno USING (alunoper_id)
                  INNER JOIN
                acadgeral__aluno_curso USING (alunocurso_id)
                  WHERE
                aluno_id = :aluno AND disc_id = :disciplina AND situacao_id NOT IN (:situacao)
SQL;

            $arrSituacao = AcadgeralSituacao::situacoesDesligamento();

            $params = array(
                'aluno'      => $alunoId,
                'disciplina' => $disciplinaId,
                'situacao'   => implode(',', $arrSituacao)
            );

            $result = self::executeQueryWithParam($query, $params)->fetchAll();

            if (count($result) > 0) {
                return false;
            }

            return true;
        } catch (\Exception $e) {
            self::setLastError("Não foi possível concluir esta operação." . $e->getMessage());
        }

        return false;
    }

    public function getArrSelecaoTipoPeloAlunoId($alunoId)
    {
        if (!$alunoId) {
            return false;
        }

        $serviceAlunoCurso = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
        /** @var \Matricula\Entity\AcadgeralAlunoCurso $objAlunoCurso */
        $arrObjAlunoCurso = $serviceAlunoCurso->getRepository()->findBy(array('aluno' => $alunoId));

        $arrSelecaoTipo = array();

        if ($arrObjAlunoCurso) {
            foreach ($arrObjAlunoCurso as $objAlunoCurso) {
                $arrSelecaoTipo[] = $objAlunoCurso->getTiposel()->toArray();
            }
        }

        return $arrSelecaoTipo;
    }

    public function equipararArrays(&$a, &$b)
    {
        if (!$a or !$b) {
            return false;
        }

        foreach ($a as $index => $value) {
            if (!$a[$index] && $b[$index]) {
                $a[$index] = $b[$index];
            }
        }

        return true;
    }

    public function procuraDadosExtrasParaResAluno(&$arrDados)
    {
        if (!$arrDados) {
            $this->setLastError("Array informado está vazio!");

            return false;
        } elseif (!$arrDados['disc_id']) {
            $this->setLastError("Por favor, informe uma disciplina no array!");

            return false;
        }

        $params     = array();
        $conditions = array();

        if ($arrDados['aluno_id']) {
            $params['aluno'] = $arrDados['aluno_id'];
            $conditions[]    = "aluno.aluno_id = :aluno";
        }

        if ($arrDados['disc_id']) {
            $params['disciplina'] = $arrDados['disc_id'];
            $conditions[]         = "disciplina.disc_id = :disciplina";
        }

        if ($arrDados['alunocurso_id']) {
            $params['alunocurso'] = $arrDados['alunocurso_id'];
            $conditions[]         = "aluno.alunocurso_id = :alunocurso";
        }

        $query = <<<SQL
        SELECT
          alunoper_id,
          periodo.*,
          turma.*
            FROM acadperiodo__aluno
              LEFT JOIN acadgeral__aluno_curso aluno USING(alunocurso_id)
              LEFT JOIN acadperiodo__turma turma USING(turma_id)
              LEFT JOIN acadperiodo__letivo periodo USING(per_id)
              LEFT JOIN acadperiodo__matriz_disciplina disciplina USING (mat_cur_id)
SQL;

        if ($conditions) {
            $query .= ' WHERE ' . implode(" AND ", $conditions);
        }

        $arrInfoExtra = $this->executeQueryWithParam($query, $params)->fetch();

        if ($arrInfoExtra) {
            $arrDados['semestre']    = $arrInfoExtra['per_semestre'];
            $arrDados['alunoper_id'] = $arrInfoExtra['alunoper_id'];
            $arrDados['serie']       = $arrInfoExtra['turma_serie'];

            return true;
        }

        return false;
    }

    public function buscaTodosDadosAluno($arrDados)
    {
        if (!$arrDados) {
            $this->setLastError("Por favor, informe algum dado para a busca!");

            return false;
        }

        $conditions = array();
        $params     = array();

        if ($arrDados['pesCpf']) {
            $params['pesCpf']     = $arrDados['pesCpf'];
            $conditions['pesCpf'] = "pes_cpf like :pesCpf";
        }

        if ($arrDados['pesId']) {
            $params['pesId']     = $arrDados['pesId'];
            $conditions['pesId'] = "pes_id like :pesId";
        }

        $query =
            "SELECT
                  *, group_concat(acadgeral__aluno_curso.alunocurso_id SEPARATOR ',') matriculasEncontradas
                FROM pessoa_fisica
                  LEFT JOIN acadgeral__aluno USING (pes_id)
                  LEFT JOIN selecao_inscricao USING (pes_id)
                  LEFT JOIN acadgeral__aluno_curso USING (aluno_id)
                  LEFT JOIN inscricao_cursos USING (inscricao_id)
                  LEFT JOIN selecao_edicao USING (edicao_id)
                  LEFT JOIN selecao_cursos USING (edicao_id)
                  LEFT JOIN campus_curso ON campus_curso.cursocampus_id = selecao_cursos.cursocampus_id";

        $conditions = implode(" AND ", $conditions);

        $query .= ' WHERE ' . $conditions;

        $result = $this->executeQueryWithParam($query, $params)->fetch();

        return $result ? $result : array();
    }

    public function validaDados(&$arrParam)
    {
        $serviceAcadgeralFormacao = new \Matricula\Service\AcadgeralAlunoFormacao($this->getEm());
        $errors                   = array();

        foreach ($arrParam['arrFormacoes'] as &$arrFormacao) {
            if (!$serviceAcadgeralFormacao->valida($arrFormacao)) {
                $errors[] = $serviceAcadgeralFormacao->getLastError();
            }
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    /**
     * @param $alunocursoId int
     * @return integer|null
     */
    public function retornaAlunoperiodoValido($alunocursoId)
    {
        if (!$alunocursoId) {
            $this->setLastError("Nenhum alunoperid informado");

            return false;
        }

        $serviceAcadgeralAlunocurso = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
        $situacao                   = implode('","', $serviceAcadgeralAlunocurso::getAlunocursoSituacaoAtividade());

        $sql = 'SELECT
                  alunoper_id
                FROM acadperiodo__aluno
                  LEFT JOIN acadperiodo__turma USING (turma_id)
                  LEFT JOIN acadperiodo__letivo USING (per_id)
                  LEFT JOIN acadgeral__situacao ON acadgeral__situacao.situacao_id = acadperiodo__aluno.matsituacao_id
                WHERE DATE(now()) BETWEEN DATE(per_data_inicio) AND DATE(per_data_fim)
                      AND (
                          (per_data_finalizacao IS NOT NULL AND DATE(per_data_finalizacao) >= DATE(NOW())) OR
                          per_data_finalizacao IS NULL
                      )
                      AND alunocurso_id IN (' . $alunocursoId . ')
                      AND matsit_descricao NOT IN ("' . $situacao . '")';

        $result = $this->executeQuery($sql)->fetchAll();

        if ($result) {
            return $result[0]['alunoper_id'];
        }

        return false;
    }

    public function pesquisarDadosDeAlunoECursoPelaintegracao(&$arrDados)
    {
        $arrDados['urlIntegracao'] = $arrDados['urlIntegracao'] ? trim($arrDados['urlIntegracao']) : '';

        if (!$arrDados['urlIntegracao']) {
            return false;
        }

        $serviceSisIntegracao = new \Sistema\Service\SisIntegracao($this->getEm());
        $objIntegracao        = $serviceSisIntegracao->retornaIntegracaoPorUrl($arrDados['urlIntegracao']);

        if (!$objIntegracao) {
            return false;
        }

        if ($arrDados['alunoIntegracao']) {
            $serviceSisIntegracaoAluno = new \Sistema\Service\SisIntegracaoAluno($this->getEm());

            $alunoId = $serviceSisIntegracaoAluno->pesquisarAlunoCodigoIntegracao(
                $objIntegracao->getIntegracaoId(),
                $arrDados['alunoIntegracao']
            );

            if ($alunoId) {
                $serviceAcadgeralAluno = new \Matricula\Service\AcadgeralAluno($this->getEm());

                $arrAluno           = $serviceAcadgeralAluno->getArray($alunoId);
                $arrDados['pesCpf'] = $arrAluno['pesCpf'];
            }
        }

        if ($arrDados['cursoIntegracao']) {
            $serviceSisIntegracaoCurso = new \Sistema\Service\SisIntegracaoCurso($this->getEm());

            /** @var \Matricula\Entity\CampusCurso $objCampusCurso */
            $objCampusCurso = $serviceSisIntegracaoCurso->retornarCursoCamposPeloCodigoEIntegracao(
                $arrDados['cursoIntegracao'],
                $objIntegracao->getIntegracaoId()
            );

            if ($objCampusCurso) {
                $arrDados['campusCurso'] = $objCampusCurso->getCursocampusId();
            }
        }
    }

    public function liberaAlunos()
    {
        $serviceSituacao   = new \Matricula\Service\AcadgeralSituacao($this->getEm());
        $situacaoPendencia = $serviceSituacao::PENDENCIAS_ACADEMICAS;

        $query = "
		SELECT alunoPeriodo.alunocurso_id, alunoPeriodo.alunoper_id
        FROM acadperiodo__aluno alunoPeriodo
        LEFT JOIN acadperiodo__turma turma USING(turma_id)
        LEFT JOIN acadperiodo__letivo periodo USING(per_id)
        WHERE alunoPeriodo.matsituacao_id IN ($situacaoPendencia) AND
            DATE(NOW()) BETWEEN DATE(periodo.per_data_inicio) AND DATE(periodo.per_data_fim) AND
            per_data_finalizacao IS NULL";

        $arrPessoas = $this->executeQuery($query)->fetchAll();
        $arrPessoas = $arrPessoas ? $arrPessoas : array();

        $arrErro = array();

        foreach ($arrPessoas as $arrPessoa) {
            if (!$this->atualizarAlunoSemPendencias($arrPessoa['alunocurso_id'])) {
                $arrErro[] = $this->getLastError();
            }
        }

        if (count($arrErro) > 0) {
            $this->setLastError(implode("\n", $arrErro));

            return false;
        }

        return true;
    }

    public function atualizarAlunoSemPendencias($alunocursoId)
    {
        if (!$alunocursoId) {
            $this->setLastError("Informe um alunocursoId!");

            return false;
        }

        $serviceSituacao   = new \Matricula\Service\AcadgeralSituacao($this->getEm());
        $situacaoPendencia = $serviceSituacao::PENDENCIAS_ACADEMICAS;

        $query = "
		SELECT
		aluno.pes_id pesId,
		alunoCurso.aluno_id alunoId,
		alunoCurso.alunocurso_id alunocursoId,
		alunoPeriodo.alunoper_id alunoperId,
		campus_curso.curso_id cursoId
        FROM acadperiodo__aluno alunoPeriodo
        INNER JOIN acadgeral__aluno_curso alunoCurso USING(alunocurso_id)
        INNER JOIN campus_curso on alunoCurso.cursocampus_id = campus_curso.cursocampus_id
        INNER JOIN acadgeral__aluno aluno USING(aluno_id)
        LEFT JOIN acadperiodo__turma turma USING(turma_id)
        LEFT JOIN acadperiodo__letivo periodo USING(per_id)
        WHERE alunoPeriodo.matsituacao_id IN ($situacaoPendencia) AND
            DATE(NOW()) BETWEEN DATE(periodo.per_data_inicio) AND
            DATE(periodo.per_data_fim) AND
            per_data_finalizacao IS NULL AND
            alunoPeriodo.alunocurso_id = {$alunocursoId}";

        $arrAluno = $this->executeQuery($query)->fetch();

        if (!$arrAluno) {
            $this->setLastError("Não foram encontrados dados para o processamento do aluno " . $alunocursoId . "!");

            return false;
        }

        $serviceAlunoCurso       = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
        $serviceAlunoPer         = new \Matricula\Service\AcadperiodoAluno($this->getEm());
        $serviceAlunoPerDisc     = new \Matricula\Service\AcadperiodoAlunoDisciplina($this->getEm());
        $serviceCursoConfig      = new \Matricula\Service\AcadCursoConfig($this->getEm());
        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceResumo           = new \Matricula\Service\AcadperiodoAlunoResumo($this->getEm());
        $serviceSisFila          = new \Sistema\Service\SisFilaIntegracao($this->getEm());

        $serviceFinanceiroTitulo->setConfig($this->getConfig());

        $arrErro    = array();
        $tituloTipo = $this->getConfig()->localizarChave("FINANCEIRO_INADIMPLENCIA_TIPO_TITULO", '');

        $datas = $serviceAlunoCurso->verificaDatas($arrAluno['alunocursoId']);

        if ($datas['colacao'] != null || $datas['expedicao'] != null) {
            $arrErro[] = 'Aluno já possui datas de conclusão lançadas';

            return false;
        }

        if ($tituloTipo) {
            $arrAluno['tipoTitulo'] = $tituloTipo;
        }

        $titulosEmAtraso = $serviceFinanceiroTitulo->buscaTitulosAbertoEmAtrasoAluno($arrAluno);

        if ($titulosEmAtraso > 0) {
            $arrErro[] = 'O aluno possui ' . $titulosEmAtraso . ' título(s) em atraso';
        }

        $numeroMaximoDependencias = 0;
        $numeroDependencias       = $serviceResumo->retornaNumeroDeReprovacoesNaoCumpridas($arrAluno['alunocursoId']);

        /** @var \Matricula\Entity\AcadCursoConfig $objConfig */
        $objConfig = $serviceCursoConfig->retornaObjCursoConfig($arrAluno['cursoId']);

        if ($objConfig) {
            $numeroMaximoDependencias = $objConfig->getCursoconfigNumeroMaximoPendencias();
        }

        if ($numeroDependencias > $numeroMaximoDependencias) {
            $arrErro[] = (
                'O aluno possui ' . $numeroDependencias . ' pendências' .
                '(O máximo permitido é ' . $numeroMaximoDependencias . ')'
            );
        }

        $this->getEm()->beginTransaction();

        if (empty($arrErro)) {
            if (!$serviceAlunoPer->setarAlunoMatriculado($arrAluno['alunoperId'])) {
                $arrErro[] = $serviceAlunoPer->getLastError();
            }

            if (!$serviceAlunoPerDisc->setarAlunoDiscMatriculado($arrAluno['alunoperId'])) {
                $arrErro[] = $serviceAlunoPerDisc->getLastError();
            }
        }

        if (count($arrErro) > 0) {
            $this->setLastError(
                'Não foi possível atualizar a situação acadêmica do aluno ' . $alunocursoId . ':' .
                implode("; ", $arrErro) . '.'
            );

            $this->getEm()->rollback();

            return false;
        }

        $this->getEm()->commit();

        $serviceSisFila->registrarAlunoNaFilaDeIntegracao($arrAluno['alunoId']);

        return true;
    }

    public function salvarResponsavelCaptacao($arrDados, $objAluno)
    {
        $servicePessoa      = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceResponsavel = new \Matricula\Service\AcadgeralResponsavel($this->getEm());

        $academico  = isset($arrDados['academico']['documento']);
        $financeiro = isset($arrDados['financeiro']['documento']);

        if (!$academico && !$financeiro) {
            $this->setLastError("Nenhum dado válido informado!");

            return false;
        }

        /** @var $objAluno \Matricula\Entity\AcadgeralAluno */
        if (!$objAluno) {
            $this->setLastError("Aluno não informado!");

            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            foreach ($arrDados as $index => $value) {

                if (!isset($value['documento'])) {
                    continue;
                }

                $documento  = str_replace([".", "-", "/"], '', $value['documento']);
                $tamanho    = strlen($documento);
                $tipoPessoa = $tamanho > 11 ? 'Juridica' : 'Fisica';

                $documentoFormatado = $tipoPessoa == "Fisica" ?
                    substr($documento, -11, 3) . '.' . substr($documento, -9, 3) . '.' . substr(
                        $documento,
                        -4,
                        3
                    ) . '-' . substr($documento, -2, 2) :
                    substr($documento, -14, 2) . '.' . substr($documento, -12, 3) . '.' . substr(
                        $documento,
                        -9,
                        3
                    ) . '/' .
                    substr($documento, -6, 4) . '-' . substr($documento, -2, 2);

                if (($tamanho < 11 || $tamanho > 14) || ($tamanho > 12 && $tamanho < 14)) {
                    $this->setLastError("CPF/CNPJ inválido");

                    return false;
                }

                $pesId     = $servicePessoa->buscaPessoaPorCPFOuCNPJ($value['documento']);
                $objPessoa = $servicePessoa->getRepository()->findOneBy(['pesId' => $pesId]);

                if (!$pesId && $objAluno->getPes()->getPesCpf() == $documentoFormatado) {
                    $objPessoa = $objAluno->getPes()->getPes();
                }

                if (!$objPessoa) {

                    $objPessoaFisica   = new \Pessoa\Entity\PessoaFisica();
                    $objPessoaJuridica = new \Pessoa\Entity\PessoaJuridica();

                    $equiparado = $documento ==
                        (str_replace([".", "-", "/"], '', $arrDados['academico']['documento']));

                    if ($equiparado && $index == 'financeiro') {

                        $objPessoa->setPesTipo('Equiparada');
                    } else {
                        $objPessoa = new \Pessoa\Entity\Pessoa();

                        $objPessoa
                            ->setPesNome($value['nome'])
                            ->setPesNacionalidade($value['naturalidade'])
                            ->setPesTipo($tipoPessoa)
                            ->setConContatoCelular($value['celular'])
                            ->setConContatoTelefone($value['fixo'])
                            ->setConContatoEmail($value['email']);
                    }

                    $this->getEm()->persist($objPessoa);
                    $this->getEm()->flush($objPessoa);

                    if ($tipoPessoa == 'Fisica') {

                        $objPessoaFisica
                            ->setPes($objPessoa)
                            ->setPesCpf($documentoFormatado)
                            ->setPesFalecido('Não');

                        $this->getEm()->persist($objPessoaFisica);
                        $this->getEm()->flush($objPessoaFisica);
                    } else {

                        $objPessoaJuridica->setPes($objPessoa);
                        $objPessoaJuridica->setPesCnpj($documentoFormatado);
                        $objPessoaJuridica->setPesDataInicio(new \DateTime());

                        $this->getEm()->persist($objPessoaJuridica);
                        $this->getEm()->flush($objPessoaJuridica);
                    }
                }
                $paramResponsavel = [
                    'pesId'       => $objPessoa,
                    'respVinculo' => $value['respVinculo'],
                    'respNivel'   => $index == 'academico' ? 'Academico' : 'Financeiro',
                    'alunoId'     => $objAluno
                ];

                if (!$serviceResponsavel->salvarResponsavel($paramResponsavel)) {
                    $this->setLastError($serviceResponsavel->getLastError());

                    return false;
                }
            }
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError($ex->getMessage());

            return false;
        }

        return true;
    }

    /*Todo: Retorna possiveis tabelas relacionados ao cadastro de alunos...*/
    /**
     * @return array
     */
    public function retornaTabelasRelacionados()
    {
        return array(
            'dadosPessoais' => array(
                'pessoa_fisica',
                'acadgeral__aluno'
            ),
            'cursos'        => array(
                'acadgeral__aluno_curso',
            ),
        );
    }

    /**
     * @return array
     */
    public function retornaCamposPersonalizados($chave = false, $exibicaoExterna = false)
    {
        $serviceSisCampoPersonalizado = new \Sistema\Service\SisCampoPersonalizado($this->getEm());

        $arrEntidades            = $this->retornaTabelasRelacionados();
        $arrCamposPersonalizados = array();
        $params                  = ['campoAtivo' => true];

        $params['exibicaoExterna'] = $exibicaoExterna;
        $params['campoChave']      = $chave;

        foreach ($arrEntidades as $index => $value) {
            $params['entidade']         = $value;
            $arrCamposPersonalizadosTmp = $serviceSisCampoPersonalizado->pesquisaForJson($params, false);

            if (!empty($arrCamposPersonalizadosTmp)) {
                $arrCamposPersonalizados[$index] = $arrCamposPersonalizadosTmp;
            }
        }

        return $arrCamposPersonalizados;
    }
}