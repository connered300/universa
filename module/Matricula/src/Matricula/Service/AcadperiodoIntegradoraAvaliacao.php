<?php


namespace Matricula\Service;

use VersaSpine\Service\AbstractService;
use Zend\I18n\Validator\DateTime;

class AcadperiodoIntegradoraAvaliacao extends AbstractService{
    public function __construct(\Doctrine\ORM\EntityManager $em){
        parent::__construct($em, 'Matricula\Entity\AcadperiodoIntegradoraAvaliacao');
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {

    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {

    }

    public function adicionar(array $data)
    {
        // campos a setar na view
        $data['integavaliacaoTurno'];
        $data['integradora'];
        //fim campos a setar na view
        $avaliacao = new \Matricula\Entity\AcadperiodoIntegradoraAvaliacao();
        $avaliacao->setIntegavaliacaoPeriodo($data['integavaliacaoPeriodo']);
        $avaliacao->setIntegavaliacaoTurno($data['integavaliacaoTurno']);
        $avaliacao->setIntegradora($this->getReference($data['integradora'], '\Matricula\Entity\AcadperiodoIntegradora'));

        $this->begin();
        try {
            $this->getEm()->persist($avaliacao);
            $this->getEm()->flush();
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
        }

        $serviceIntegTurma = (new \Matricula\Service\AcadperiodoIntegradoraTurma($this->getEm()));
        $turmas = explode(',', $data['turmas']);
        foreach ($turmas as $turma) {
            $IntegTurma = new \Matricula\Entity\AcadperiodoIntegradoraTurma();
            $turmaProxy = $this->getRepository('\Matricula\Entity\AcadperiodoTurma')->findOneBy(['turmaId' => $turma]);
            $IntegTurma->setTurma($turmaProxy);
            $IntegTurma->setIntegavaliacao($avaliacao);
            $serviceIntegTurma->adicionar($IntegTurma);
            unset($IntegTurma);
        }

        $serviceIntegCaderno = (new \Matricula\Service\AcadperiodoIntegradoraCaderno($this->getEm()));
        $serviceIntegGabarito = (new \Matricula\Service\AcadperiodoIntegradoraGabarito($this->getEm()));

        foreach ($data['caderno'] as $caderno) {
            $integCaderno = new \Matricula\Entity\AcadperiodoIntegradoraCaderno();
            $integCaderno->setIntegcadernoNome($caderno['nome']);
            $integCaderno->setIntegcadernoData(new \DateTime($this->formatDateAmericano($caderno['data-aplicacao'])));
            $integCaderno->setIntegavaliacao($avaliacao);
            $serviceIntegCaderno->adicionar($integCaderno);

            $gabaritos = \Zend\Json\Json::decode($caderno['gabarito'], \Zend\Json\Json::TYPE_ARRAY);


            foreach ($gabaritos as $gabarito) {
                if(strlen($gabarito['resposta']) > 1){
                    for ($i = 0; $i < strlen($gabarito['resposta']); $i++) {
                        $integCabarito = new \Matricula\Entity\AcadperiodoIntegradoraGabarito();
                        $integCabarito->setDisc($this->getReference($gabarito['disciplina'], '\Matricula\Entity\AcadgeralDisciplina'));
                        $integCabarito->setIntegcaderno($integCaderno);
                        $integCabarito->setGabaritoOrdem($gabarito['ordemGeral']+1);
                        $integCabarito->setGabaritoResposta(strtoupper($gabarito['resposta'][$i]));
                        $integCabarito->setGabaritoValida(!empty($gabarito['invalida']) ? 'Não': 'Sim');
                        $serviceIntegGabarito->adicionar($integCabarito);
                        unset($integCabarito);
                    }

                }else {
                    $integCabarito = new \Matricula\Entity\AcadperiodoIntegradoraGabarito();
                    $integCabarito->setDisc($this->getReference($gabarito['disciplina'], '\Matricula\Entity\AcadgeralDisciplina'));
                    $integCabarito->setIntegcaderno($integCaderno);
                    $integCabarito->setGabaritoOrdem($gabarito['ordemGeral']+1);
                    $integCabarito->setGabaritoResposta(strtoupper($gabarito['resposta']));
                    $integCabarito->setGabaritoValida(!empty($gabarito['invalida']) ? 'Não': 'Sim');
                    $serviceIntegGabarito->adicionar($integCabarito);
                    unset($integCabarito);

                }

            }
            unset($integCaderno);
        }
        $this->commit();

        return $avaliacao;
    }

    public function edita($data)
    {
        $this->begin();

        $avaliacao = $this->getReference($data['integavaliacaoId']);

        $turmas = $this->getRepository('Matricula\Entity\AcadperiodoIntegradoraTurma')
            ->findBy(['integavaliacao' => $data['integavaliacaoId']]);
        foreach ($turmas as $turma) {
            $this->getEm()->remove($turma);
        }
        $cadernos = $this->getRepository('Matricula\Entity\AcadperiodoIntegradoraCaderno')
            ->findBy(['integavaliacao' => $data['integavaliacaoId']]);
        foreach ($cadernos as $caderno) {
            $respostas  = $this->getRepository('Matricula\Entity\AcadperiodoIntegradoraGabarito')
                ->findBy(['integcaderno' => $caderno->getIntegcadernoId()]);

            foreach ($respostas as $resposta) {
                $this->getEm()->remove($resposta);
            }

        }
        foreach ($cadernos as $caderno) {
            $this->getEm()->remove($caderno);
        }
        $this->getEm()->remove($avaliacao);
        $avaliacaoNew = $this->adicionar($data);
        $this->commit();

        return $avaliacaoNew;


    }

    public function pagination($page = 0, $is_json = false)
    {
        $dados = array();
        $sql = "";
        if (!$page) {
            if ($is_json) {
                $limit = " LIMIT 10 OFFSET 0 ";
            } else {
                $limit = '';
            }
        } else {
            $offset = (($page - 1) * 10);
            $limit = " LIMIT 10 OFFSET $offset ";
        }
        $sql = "
          SELECT
            integavaliacao_id,
            integradora_id id,
            integavaliacao_periodo,
            integavaliacao_turno,
            integradora_status
          FROM
            acadperiodo__integradora_avaliacao
          NATURAL JOIN
            acadperiodo__integradora
          WHERE 1
          ORDER BY
            integavaliacao_periodo, integavaliacao_turno
        ";
        $sql .= " $limit ";
        $dados['dados'] = $this->executeQuery($sql);
        $dados['count'] = $dados['dados']->rowCount();
        if ($is_json)
        {
            $dados['dados'] = $this->paginationFormatJson($dados['dados']);
        }
        return $dados;
    }

    public function buscaAvaliacaoPorIntegradoraETurno($integradora, $turno)
    {
        $avaliacoes = $this->getRepository()->findOneBy(['integradora' => $integradora, 'integavaliacaoTurno' => $turno]);

        if($avaliacoes){
            return $avaliacoes;
        }

        return null;
    }

    public function buscaAvaliacaoPorIntegradora($integradora)
    {
        $avaliacoes = $this->getRepository()->findBy(['integradora' => $integradora]);

        if($avaliacoes){
            return $avaliacoes;
        }

        return null;
    }

    public function buscaAvaliacaoPorIntegradoraArray($integradora)
    {
        $avaliacoes = $this->getRepository()->findBy(['integradora' => $integradora]);
        $array = array();
        if($avaliacoes){
            foreach ($avaliacoes as $avaliacao) {
                $array[] = $avaliacao->toArray();
            }

            return $array;
        }

        return null;
    }
}