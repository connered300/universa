<?php

namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

class AcadgeralDisciplina extends AbstractService
{

    const DISC_ATIVO_SIM = 'Sim';
    const DISC_ATIVO_NAO = 'Não';

    const DISC_TIPO_REGULAR      = 1;
    const DISC_TIPO_OPTATIVA     = 2;
    const DISC_TIPO_ESTAGIO      = 3;
    const DISC_TIPO_EQUIVALENCIA = 4;

    private $_lastError = null;

    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadgeralDisciplina');
    }


    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {
        // TODO: Implement pesquisaForJson() method.
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->_lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->_lastError = $lastError;
    }


    protected function valida($arrParam)
    {
        $errors = array();
        if (!$arrParam['discNome']) {
            $errors[] = 'Por favor preencha o campo "discNome"!';
        }

        if (!$arrParam['discSigla']) {
            $errors[] = 'Por favor preencha o campo "discSigla"!';
        }
        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }


    public function getDataForDatatables($data)
    {
        $query = "
              SELECT * FROM acadgeral__disciplina ad
                 ";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * Método responsável por recuperar dados da disciplina
     *
     * @param int $discId
     *
     * @return array $arrDados
     */
    public function getArray($discId)
    {
        $arrDados = $this->getRepository()->find($discId);

        $repoDisciplina           = $this->getRepository();
        $serviceDiscCurso         = new \Matricula\Service\AcadgeralDisciplinaCurso($this->getEm());
        $serviceRequisito         = new \Matricula\Service\AcadgeralDisciplinaRequisito($this->getEm());
        $serviceDiscEquiv         = new \Matricula\Service\AcadgeralDisciplinaEquivalencia($this->getEm());
        $serviceDisciplinaDocente = new \Matricula\Service\AcadperiodoDocenteDisciplina($this->getEm());

        $equivalencia = $serviceDiscEquiv->buscaEquivalencia($discId);
        $coRequisito  = $serviceRequisito->buscaCoRequisito($discId);
        $preRequisito = $serviceRequisito->buscaPreRequisito($discId);
        $cursos       = $serviceDiscCurso->buscaCursosPelaDisciplina($discId);

        $arrEquivalencia = array();
        foreach($equivalencia as $eq){
            $arrEquivalencia[] = ['id' => $eq['disc_id'], 'text' => $eq['disc_nome']];
        }

        $arrCoRequisito = array();
        foreach ($coRequisito as $co) {
            $objDisc          = $repoDisciplina->find($co->getDiscreqDestino());
            $arrCoRequisito[] = ['id' => $objDisc->getDiscId(), 'text' => $objDisc->getDiscNome()];
        }

        $arrPreRequisito = array();
        foreach ($preRequisito as $pre) {
            $objDisc           = $repoDisciplina->find($pre->getDiscreqDestino());
            $arrPreRequisito[] = ['id' => $objDisc->getDiscId(), 'text' => $objDisc->getDiscNome()];
        }

        $arrCursos = array();
        foreach ($cursos as $curso) {
            $arrCursos[] = ['id' => $curso['cursoId'], 'text' => $curso['cursoNome']];
        }

        try {
            $arrDados = $arrDados->toArray();
        } catch (\Exception $e) {
            return array();
        }

        /** @var \Matricula\Entity\AcadperiodoDocenteDisciplina $objDocenteDisciplina */
        $objDocenteDisciplina = $serviceDisciplinaDocente->getRepository()->findOneBy(['disc' => $discId]);

        if ($objDocenteDisciplina) {
            $arrDocente          = $objDocenteDisciplina->toArray();
            $arrDados['docente'] = $arrDocente;
        }

        $arrDados['arrCursos']       = $arrCursos;
        $arrDados['arrCoRequisito']  = $arrCoRequisito;
        $arrDados['arrPreRequisito'] = $arrPreRequisito;
        $arrDados['arrEquivalencia'] = $arrEquivalencia;

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('discId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Matricula\Entity\AcadgeralDisciplina */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getDiscId(),
                $params['value'] => $objEntity->getDiscNome()
            );
        }

        return $arrEntitiesArr;
    }

    public function criarSiglaProvisoria($text)
    {
        if (!$text) {
            return false;
        }

        return preg_replace('/([A-Z0-9])([^\s]*)( |)/i', '$1', $text);
    }


    public function save($arrDados){

        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceAcadgeralDiscCurso = new \Matricula\Service\AcadgeralDisciplinaCurso($this->getEm());
        $objAcadgeralDisciplina = new \Matricula\Entity\AcadgeralDisciplina();

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['discId']) {
                /** @var $objAcadgeralDisciplina \Matricula\Entity\AcadgeralDisciplina */
                $objAcadgeralDisciplina = $this->getRepository()->find($arrDados['discId']);

                if (!$objAcadgeralDisciplina) {
                    $this->setLastError('Registro de disciplina não existe!');

                    return false;
                }
            }

            $objAcadgeralDisciplina->setDiscNome($arrDados['discNome']);
            $objAcadgeralDisciplina->setDiscSigla($arrDados['discSigla']);
            $objAcadgeralDisciplina->setDocenteRegente($arrDados['docenteRegente'] ? $arrDados['docenteRegente'] : null);
            
            $this->getEm()->persist($objAcadgeralDisciplina);
            $this->getEm()->flush($objAcadgeralDisciplina);
            
            $arrDados['discId'] = $objAcadgeralDisciplina->getDiscId();


            if ($arrDados['curso'] && !$serviceAcadgeralDiscCurso->salvarArray($arrDados['curso'], $objAcadgeralDisciplina)) {
                $this->setLastError($serviceAcadgeralDiscCurso->getLastError());

                return false;
            }

            $this->getEm()->commit();

        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de disciplina!<br>' . $e->getMessage());
            return false;
        }
        return $objAcadgeralDisciplina;
    }


    public function remover($param)
    {
        if (!$param['discId']) {
            $this->setLastError('Para remover um registro de disciplina é necessário especificar o código.');

            return false;
        }
        $this->begin();
        try {
            $objAcadgeralDisciplina = $this->getRepository()->find($param['discId']);

            $serviceAcadgeralDisciplinaCurso = new \Matricula\Service\AcadgeralDisciplinaCurso($this->getEm());

            $serviceAcadgeralDisciplinaCurso->removeVinculoDiscCurso($param['discId']);

            $this->getEm()->remove($objAcadgeralDisciplina);
            $this->getEm()->flush();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de disciplina.');
            $this->rollback();
            return false;
        }
        $this->commit();
        return true;
    }

    public function criaDisciplinaSeNaoExiste($disc, $curso)
    {
        if (!$disc) {
            $this->setLastError("Informe o código ou nome de uma disciplina!");

            return false;
        }

        $discId = null;

        if (is_integer($disc)) {
            /** @var \Sistema\Entity\SisIntegracaoDisciplina $objDisciplina */
            $objDisciplina = $this->getRepository('Sistema\Entity\SisIntegracaoDisciplina')->findOneBy(
                ['codigo' => $disc]
            );

            if ($objDisciplina) {
                $discId = $objDisciplina->getCursoDisc()->getDisc()->getDiscId();

                return $discId;
            }
        } else {
            /** @var \Matricula\Entity\AcadgeralDisciplina $objDisciplina */
            $objDisciplina = $this->getRepository()->findOneBy(
                [
                    'discNome' => $disc
                ]
            );

            if ($objDisciplina) {
                $discId = $objDisciplina->getDiscId();

                return $discId;
            }
        }

        $array = array(
            "discNome"  => $disc,
            "discSigla" => $this->criarSiglaProvisoria($disc),
            "curso"     => array(
                (int)$curso => array(
                    'disc_ativo' => AcadgeralDisciplinaCurso::DISC_ATIVO_SIM,
                    'tdisc_id'   => AcadgeralDisciplinaCurso::DISC_TIPO_REGULAR,
                    'curso_id'   => $curso,
                ),
            ),
        );

        if ($this->save($array)) {
            return $array['discId'];
        }

        return false;
    }


}