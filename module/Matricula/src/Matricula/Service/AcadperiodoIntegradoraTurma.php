<?php


namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

class AcadperiodoIntegradoraTurma extends AbstractService{
    public function __construct(\Doctrine\ORM\EntityManager $em){
        parent::__construct($em, 'Matricula\Entity\AcadperiodoIntegradoraTurma');
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {

    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {

    }

    /**
     * @param \Matricula\Entity\AcadperiodoIntegradoraTurma $integradoraTurma
     * @return \Matricula\Entity\AcadperiodoIntegradoraTurma
     * @throws \Exception
     */
    public function adicionar( \Matricula\Entity\AcadperiodoIntegradoraTurma $integradoraTurma)
    {
        $this->begin();
        try {
            $this->getEm()->persist($integradoraTurma);
            $this->getEm()->flush();
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
        }
        $this->commit();
        return $integradoraTurma;
    }

    /**
     * @param \Matricula\Entity\AcadperiodoIntegradoraAvaliacao $avaliacao
     * @return array
     */
    public function buscaTurmasAvaliacao(\Matricula\Entity\AcadperiodoIntegradoraAvaliacao $avaliacao)
    {
        $turmas = array();
        $integTurmas = $this->getRepository()->findBy(['integavaliacao' => $avaliacao->getIntegavaliacaoId()]);

        if($integTurmas){
            foreach ($integTurmas as $integTurma) {
                $turmas[] = $integTurma->getTurma()->getTurmaId();
            }

        }
        return $turmas;
    }

    /**
     * @param \Matricula\Entity\AcadperiodoIntegradoraAvaliacao $avaliacao
     * @return array
     *
     * Esta funcação entende como todoas as turmas posuam a mesma matriz curricular
     */
    public function buscaMAtrizTurmaPorAvaliacao(\Matricula\Entity\AcadperiodoIntegradoraAvaliacao $avaliacao)
    {
        $turmas = array();
        $integTurma = $this->getRepository()->findOneBy(['integavaliacao' => $avaliacao->getIntegavaliacaoId()]);

        return $integTurma->getTurma()->getMatCur()->getMatCurId();
    }

}