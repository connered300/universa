<?php

namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

class AcadperiodoAlunoDisciplinaHistorico extends AbstractService
{
    /**
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadperiodoAlunoDisciplinaHistorico');
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {
    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {
    }

    public function save($alunoDiscHist, $commit = true)
    {
        $serviceAcadgeralSituacao = new \Matricula\Service\AcadgeralSituacao($this->getEm());

        try {
            if ($alunoDiscHist instanceof \Matricula\Entity\AcadperiodoAlunoDisciplinaHistorico) {
                $objAlunoDiscHist = $alunoDiscHist;
            } else {
                $objAlunoDiscHist = new \Matricula\Entity\AcadperiodoAlunoDisciplinaHistorico();

                if (is_integer($alunoDiscHist['alunodisc'])) {
                    $alunoDiscHist['alunodisc'] = $this->getEm()->getReference(
                        "Matricula\Entity\AcadperiodoAlunoDisciplina",
                        $alunoDiscHist['alunodisc']
                    );
                }

                if (is_integer($alunoDiscHist['turma'])) {
                    $alunoDiscHist['turma'] = $this->getEm()->getReference(
                        "Matricula\Entity\AcadperiodoTurma",
                        $alunoDiscHist['turma']
                    );
                }

                if (is_integer($alunoDiscHist['usuarioHistorico'])) {
                    $alunoDiscHist['usuarioHistorico'] = $this->getEm()->getReference(
                        "Acesso\Entity\AcessoPessoas",
                        $alunoDiscHist['usuarioHistorico']
                    );
                }

                if (is_integer($alunoDiscHist['situacao'])) {
                    $alunoDiscHist['situacao'] = $this->getEm()->getReference(
                        "Matricula\Entity\AcadgeralSituacao",
                        $alunoDiscHist['situacao']
                    );
                }

                $alunoDiscHist['situacaodiscData'] = (
                $alunoDiscHist['situacaodiscData'] ? $alunoDiscHist['situacaodiscData'] : new \DateTime('now')
                );

                $objAlunoDiscHist->setSituacao($alunoDiscHist['situacao']);
                $objAlunoDiscHist->setTurma($alunoDiscHist['turma']);
                $objAlunoDiscHist->setAlunodisc($alunoDiscHist['alunodisc']);
                $objAlunoDiscHist->setUsuarioHistorico($alunoDiscHist['usuarioHistorico']);
                $objAlunoDiscHist->setSituacaodiscObservacoes($alunoDiscHist['SituacaodiscObservacoes']);
                $objAlunoDiscHist->setSituacaodiscData($alunoDiscHist['situacaodiscData']);
            }

            if ($commit) {
                $this->begin();
            }

            $this->getEm()->persist($objAlunoDiscHist);
            $this->getEm()->flush($objAlunoDiscHist);

            if ($commit) {
                $this->commit();
            }
        } catch (\Exception $e) {
            $erro = [
                "type"    => "error",
                "message" => "Erro ao gerar histórico das disciplinas alteradas do aluno, tente novamente, caso persista o erro, entre em contato com o suporte"
            ];

            return $erro;
        }

        return $objAlunoDiscHist;
    }
}