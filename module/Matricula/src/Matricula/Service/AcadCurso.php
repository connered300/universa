<?php

namespace Matricula\Service;

use Aluno\Service\Financeiro;
use VersaSpine\Service\AbstractService;

/**
 * Class AcadCurso
 * @package Matricula\Service
 */
class AcadCurso extends AbstractService
{
    const CURSO_UNIDADE_MEDIDA_MENSAL = 'Mensal';
    const CURSO_UNIDADE_MEDIDA_BIMESTRAL = 'Bimestral';
    const CURSO_UNIDADE_MEDIDA_SEMESTRAL = 'Semestral';
    const CURSO_UNIDADE_MEDIDA_ANUAL = 'Anual';
    const CURSO_UNIDADE_MEDIDA_QUADRIMESTRAL = 'Quadrimestral';
    const CURSO_UNIDADE_MEDIDA_TRIMESTRAL = 'Trimestral';

    const CURSO_POSSUI_PERIODO_LETIVO_SIM = 'Sim';
    const CURSO_POSSUI_PERIODO_LETIVO_NAO = 'Não';

    const CURSO_TURNO_NOTURNO = 'Noturno';
    const CURSO_TURNO_VESPERTINO = 'Vespertino';
    const CURSO_TURNO_MATUTINO = 'Matutino';
    const CURSO_TURNO_INTEGRAL = 'Integral';

    const CURSO_SITUACAO_ATIVO = 'Ativo';
    const CURSO_SITUACAO_INATIVO = 'Inativo';

    private $_lastError = null;

    /** @var \Sistema\Service\SisConfig */
    private $config = null;

    /**
     * @return \Sistema\Service\SisConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param \Sistema\Service\SisConfig $config
     * @return AcadCurso
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2CursoPossuiPeriodoLetivo($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getCursoPossuiPeriodoLetivo());
    }

    /**
     * @return array
     */
    public static function getCursoPossuiPeriodoLetivo()
    {
        return array(
            self::CURSO_POSSUI_PERIODO_LETIVO_NAO,
            self::CURSO_POSSUI_PERIODO_LETIVO_SIM
        );
    }

    /**
     * @return array
     */
    public static function getCursoUnidadeMedida()
    {
        return array(
            self::CURSO_UNIDADE_MEDIDA_BIMESTRAL,
            self::CURSO_UNIDADE_MEDIDA_MENSAL,
            self::CURSO_UNIDADE_MEDIDA_SEMESTRAL,
            self::CURSO_UNIDADE_MEDIDA_ANUAL,
            self::CURSO_UNIDADE_MEDIDA_QUADRIMESTRAL,
            self::CURSO_UNIDADE_MEDIDA_TRIMESTRAL,
        );
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2CursoIntegralizacao($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getCursoIntegralizacao());
    }

    /**
     * @return array
     */
    public static function getCursoIntegralizacao()
    {
        return array(
            self::CURSO_UNIDADE_MEDIDA_ANUAL,
            self::CURSO_UNIDADE_MEDIDA_BIMESTRAL,
            self::CURSO_UNIDADE_MEDIDA_MENSAL,
            self::CURSO_UNIDADE_MEDIDA_QUADRIMESTRAL,
            self::CURSO_UNIDADE_MEDIDA_SEMESTRAL,
            self::CURSO_UNIDADE_MEDIDA_TRIMESTRAL
        );
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2CursoTurno($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getCursoTurno());
    }

    /**
     * @return array
     */
    public static function getCursoTurno()
    {
        return array(
            self::CURSO_TURNO_NOTURNO,
            self::CURSO_TURNO_VESPERTINO,
            self::CURSO_TURNO_MATUTINO,
            self::CURSO_TURNO_INTEGRAL
        );
    }

    /**
     * @return array
     */
    public static function getCursoSituacao()
    {
        return array(
            self::CURSO_SITUACAO_ATIVO,
            self::CURSO_SITUACAO_INATIVO
        );
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2CursoSituacao($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getCursoSituacao());
    }

    /**
     * Retorna em forma de string as mensagem de erro
     *
     * @return string $this->_lastError
     */
    public function getLastError()
    {
        return (string)$this->_lastError;
    }

    /**
     * Seta uma mensagem de erro
     *
     * @param string $lastError mensagem de erro
     *
     * @return object AcadCurso retorna a propria classe
     */
    public function setLastError($lastError)
    {
        $this->_lastError = $lastError;

        return $this;
    }

    /**
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, $arrConfig = array())
    {
        parent::__construct($em, 'Matricula\Entity\AcadCurso');

        $this->setConfig(new \Sistema\Service\SisConfig($em, $arrConfig));
    }

    /**
     * @param $params
     * @return mixed
     */
    public function pesquisaForJson($params)
    {
        $sql       = 'SELECT * FROM acad_curso WHERE';
        $cursoNome = false;
        $cursoId   = false;

        if ($params['q']) {
            $cursoNome = $params['q'];
        } elseif ($params['query']) {
            $cursoNome = $params['query'];
        }

        if ($params['cursoId']) {
            $cursoId = $params['cursoId'];
        }

        $parameters = array('curso_nome' => "{$cursoNome}%");
        $sql .= ' curso_nome LIKE :curso_nome';

        if ($cursoId) {
            $parameters['curso_id'] = $cursoId;
            $sql .= ' AND curso_id <> :curso_id';
        }

        if ($params['notInCurso']) {
            $parameters['notInCurso'] = $params['notInCurso'];
            $sql .= ' AND curso_id NOT IN (:notInCurso)';
        }

        /*Quando o nome do curso for passado por paremtro expecifico*/
        if ($params['cursoNome']) {
            $sql .= ' AND curso_nome LIKE :cursoNome ';
            $parameters['cursoNome'] = $params['cursoNome'];
        }

        $sql .= " ORDER BY curso_nome";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    /**
     * @param $cursoId
     * @return mixed
     */
    public function numeroDeAlunosAtivosNoCurso($cursoId)
    {
        $sql = '
        SELECT count(DISTINCT alunocurso_id) AS numeroDeAlunosAtivosNoCurso
        FROM acad_curso
          INNER JOIN campus_curso USING (curso_id)
          INNER JOIN acadgeral__aluno_curso USING (cursocampus_id)
        WHERE alunocurso_data_colacao IS NULL
              AND curso_id = :curso_id';

        $result = $this->executeQueryWithParam($sql, array('curso_id' => $cursoId))->fetch();

        return $result['numeroDeAlunosAtivosNoCurso'];
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function salvarCurso(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        if (!empty($this->pesquisaForJson(
            ['cursoNome' => $arrDados['cursoNome'],'notInCurso'=>$arrDados['cursoId']]))) {
            $this->setLastError("Já existe um curso com esse nome!");

            return false;
        }

        $servicePessoaFisica              = new \Pessoa\Service\PessoaFisica($this->getEm());
        $serviceAcadNivel                 = new \Matricula\Service\AcadNivel($this->getEm());
        $serviceCampusCurso               = new \Matricula\Service\CampusCurso($this->getEm());
        $serviceAcadModalidade            = new \Matricula\Service\AcadModalidade($this->getEm());
        $serviceAcadCursoConfig           = new \Matricula\Service\AcadCursoConfig($this->getEm());
        $serviceArquivo                   = new \GerenciadorArquivos\Service\Arquivo($this->getEm());
        $serviceAcadgeralAreaConhecimento = new \Matricula\Service\AcadgeralAreaConhecimento($this->getEm());
        $serviceArquivoDiretorios         = new \GerenciadorArquivos\Service\ArquivoDiretorios($this->getEm());

        $objDiretorio = $serviceArquivoDiretorios->getRepository()->find(\Organizacao\Service\OrgIes::DIRETORIO_IMAGEM);
        $serviceArquivo->setDiretorio($objDiretorio);

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['cursoId']) {
                $objAcadCurso = $this->getRepository()->find($arrDados['cursoId']);

                if (!$objAcadCurso) {
                    $this->setLastError('Registro de curso não existe!');

                    return false;
                }
            } else {
                $objAcadCurso = new \Matricula\Entity\AcadCurso();
            }

            if ($arrDados['mod']) {
                /** @var $objAcadModalidade \Matricula\Entity\AcadModalidade */
                $objAcadModalidade = $serviceAcadModalidade->getRepository()->find($arrDados['mod']);

                if (!$objAcadModalidade) {
                    $this->setLastError('Registro de modalidade não existe!');

                    return false;
                }

                $objAcadCurso->setMod($objAcadModalidade);
            }

            if ($arrDados['pes']) {
                /** @var $objPessoaFisica \Pessoa\Entity\PessoaFisica */
                $objPessoaFisica = $servicePessoaFisica->getRepository()->find($arrDados['pes']);

                if (!$objPessoaFisica) {
                    $this->setLastError('Registro de pessoa não existe!');

                    return false;
                }

                $objAcadCurso->setPes($objPessoaFisica);
            }

            if ($arrDados['nivel']) {
                /** @var $objAcadNivel \Matricula\Entity\AcadNivel */
                $objAcadNivel = $serviceAcadNivel->getRepository()->find($arrDados['nivel']);

                if (!$objAcadNivel) {
                    $this->setLastError('Registro de nível de ensino não existe!');

                    return false;
                }

                $objAcadCurso->setNivel($objAcadNivel);
            }

            if ($arrDados['area']) {
                /** @var $objAcadgeralAreaConhecimento \Matricula\Entity\AcadgeralAreaConhecimento */
                $objAcadgeralAreaConhecimento = $serviceAcadgeralAreaConhecimento->getRepository()->find(
                    $arrDados['area']
                );

                if (!$objAcadgeralAreaConhecimento) {
                    $this->setLastError('Registro de área de conhecimento não existe!');

                    return false;
                }

                $objAcadCurso->setArea($objAcadgeralAreaConhecimento);
            }

            $imagemCarteirinha = $arrDados['vfile'][$arrDados['arqCarteirinha']];

            $objArqCarteirinha = null;

            if ($imagemCarteirinha) {
                if (empty($imagemCarteirinha['raw']) && $objAcadCurso->getArqCarteirinha()) {
                    /* @var $objArqCarteirinha \GerenciadorArquivos\Entity\Arquivo */
                    $objArqCarteirinha = $objAcadCurso->getArqCarteirinha();

                    if (!empty($objArqCarteirinha)) {
                        try {
                            $serviceArquivo->excluir($objArqCarteirinha);
                        } catch (\Exception $ex) {
                            throw new \Exception('Falha ao remover imagem antiga!');
                        }
                    }

                    $objArqCarteirinha = null;
                } else {
                    $arrDados['arqCarteirinha'] = ($imagemCarteirinha['raw']) ? $imagemCarteirinha['raw'] : null;

                    if ($arrDados['arqCarteirinha'] && is_string($arrDados['arqCarteirinha'])) {
                        $objArqCarteirinha = $serviceArquivo->getRepository()->find($arrDados['arqCarteirinha']);
                    }
                }

                if ($imagemCarteirinha['file']['error'] == 0 && $imagemCarteirinha['file']['size'] != 0) {
                    try {
                        $objArqCarteirinha = $serviceArquivo->adicionar($imagemCarteirinha['file']);
                    } catch (\Exception $ex) {
                        throw new \Exception('Falha ao registrar imagem!');
                    }
                }
            } else {
                $objArqCarteirinha = null;
            }

            $objAcadCurso->setArqCarteirinha($objArqCarteirinha);
            $objAcadCurso->setCursoNome($arrDados['cursoNome']);
            $objAcadCurso->setCursoPossuiPeriodoLetivo($arrDados['cursoPossuiPeriodoLetivo']);
            $objAcadCurso->setCursoCargaHoraria($arrDados['cursoCargaHoraria']);
            $objAcadCurso->setCursoCargaHorariaPratica($arrDados['cursoCargaHorariaPratica']);
            $objAcadCurso->setCursoCargaHorariaTeorica($arrDados['cursoCargaHorariaTeorica']);
            $objAcadCurso->setCursoTurno($arrDados['cursoTurno']);
            $objAcadCurso->setCursoNumeroInep($arrDados['cursoNumeroInep']);
            $objAcadCurso->setCursoNumeroOcde($arrDados['cursoNumeroOcde']);
            $objAcadCurso->setCursoSigla($arrDados['cursoSigla']);
            $objAcadCurso->setCursoNumeroMec($arrDados['cursoNumeroMec']);
            $objAcadCurso->setCursoDataFuncionamento($arrDados['cursoDataFuncionamento']);
            $objAcadCurso->setCursoVagasNoturno($arrDados['cursoVagasNoturno']);
            $objAcadCurso->setCursoVagasVespertino($arrDados['cursoVagasVespertino']);
            $objAcadCurso->setCursoVagasMatutino($arrDados['cursoVagasMatutino']);
            $objAcadCurso->setCursoSituacao($arrDados['cursoSituacao']);
            $objAcadCurso->setCursoInformacoes($arrDados['cursoInformacoes']);
            $objAcadCurso->setCursoPrazoIntegralizacao($arrDados['cursoPrazoIntegralizacao']);
            $objAcadCurso->setCursoPrazoIntegralizacaoMaxima($arrDados['cursoPrazoIntegralizacaoMaxima']);
            $objAcadCurso->setCursoUnidadeMedida($arrDados['cursoUnidadeMedida']);

            $this->getEm()->persist($objAcadCurso);
            $this->getEm()->flush($objAcadCurso);

            if (!$serviceCampusCurso->salvarMultiplos($arrDados, $objAcadCurso)) {
                throw new \Exception($serviceCampusCurso->getLastError());
            }

            if (!$serviceAcadCursoConfig->salvarMultiplos($arrDados, $objAcadCurso)) {
                throw new \Exception($serviceAcadCursoConfig->getLastError());
            }

            $this->getEm()->commit();

            $arrDados['cursoId'] = $objAcadCurso->getCursoId();

            if ($this->getConfig()->localizarChave('EFETUAR_INTEGRACAO_CURSO')) {
                $serviceSisFilaIntegracao = new \Sistema\Service\SisFilaIntegracao($this->getEm());
                $serviceSisFilaIntegracao->registrarCursoNaFilaDeIntegracao($objAcadCurso);
            }

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de curso!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        $serviceModalide = new \Matricula\Service\AcadModalidade($this->getEm());

        if (!$arrParam['cursoNome']) {
            $errors[] = 'Por favor preencha o campo "nome"!';
        }

        if (!$arrParam['cursoCargaHoraria']) {
            $errors[] = 'Por favor preencha o campo "carga horária"!';
        }

        if (!$arrParam['mod']) {
            $errors[] = 'Por favor preencha o campo "modalidade"!';
        }

        if (!$arrParam['pes']) {
            $errors[] = 'Por favor preencha o campo "coordenador"!';
        }

        if (!$arrParam['nivel']) {
            $errors[] = 'Por favor preencha o campo "nível de ensino"!';
        }

        if (!$arrParam['cursoPossuiPeriodoLetivo']) {
            $errors[] = 'Por favor informe se o curso usa período letivo!';
        }

        if (!in_array($arrParam['cursoPossuiPeriodoLetivo'], self::getCursoPossuiPeriodoLetivo())) {
            $errors[] = 'Por favor informe se o curso usa período letivo!';
        }

        if (!$arrParam['cursoTurno'] && $serviceModalide::MODALIDADE_EAD != $arrParam['mod']) {
            $errors[] = 'Por favor preencha o campo "turno"!';
        }

        if (array_diff($arrParam['cursoTurno'], self::getCursoTurno())) {
            $errors[] = 'Por favor selecione valores válidos para o campo "turno"!';
        }

        if (!$arrParam['campus']) {
            $errors[] = 'Por favor selecione ao menos uma câmpus para oferecer o curso!';
        }

        if (!$arrParam['configuracoes']) {
            $errors[] = 'Por favor crie uma configuração para o curso!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());

        $where = null;

        if ($data['filter']['campus']) {
            $where .= "AND camp_id in ({$data['filter']['campus']})";
        }

        if ($data['filter']['areaPesquisa']) {
            $where .= "AND area_id in ({$data['filter']['areaPesquisa']})";
        }

        if ($data['filter']['modalidadePesquisa']) {
            $where .= "AND mod_id in ({$data['filter']['modalidadePesquisa']})";
        }

        if ($data['filter']['nivelPesquisa']) {
            $where .= "AND nivel_id in ({$data['filter']['nivelPesquisa']})";
        }

        if ($data['filter']['turnoPesquisa']) {
            $turno = $data['filter']['turnoPesquisa'];
            $where .= "AND FIND_IN_SET(curso_turno, '{$turno}')";
        }

        $arrCampusCursoPermitido = implode(",", $serviceAcessoPessoas->retornaCampusCursoDoUsuarioLogado(true));

        if ($arrCampusCursoPermitido) {
            $where .= " AND cursocampus_id in (" . $arrCampusCursoPermitido . ")";
        }

        $query = "
        SELECT curso_id, curso_nome, curso_sigla, pes_id, pes_nome, mod_id,
               mod_nome, nivel_id, nivel_nome, curso_possui_periodo_letivo, curso_carga_horaria,
               curso_numero_inep, curso_numero_ocde, curso_numero_mec, cursocampus_id, area_id, curso_turno,
               curso_unidade_medida, curso_situacao
        FROM acad_curso
        INNER JOIN acad_nivel USING(nivel_id)
        INNER JOIN acad_modalidade USING(mod_id)
        LEFT JOIN campus_curso USING(curso_id)
        LEFT JOIN pessoa USING(pes_id)
        WHERE 1 " . $where . "
        GROUP BY curso_id";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $cursoId
     * @return array
     */
    public function getArray($cursoId)
    {
        $arrDados = array();
        /** @var $objAcadCurso \Matricula\Entity\AcadCurso */
        $objAcadCurso = $this->getRepository()->find($cursoId);

        if (!$objAcadCurso) {
            $this->setLastError('Curso não existe!');

            return array();
        }

        $cursoId = $objAcadCurso->getCursoId();

        $serviceCampusCurso               = new \Matricula\Service\CampusCurso($this->getEm());
        $serviceAcadCursoConfig           = new \Matricula\Service\AcadCursoConfig($this->getEm());
        $serviceAcadgeralAreaConhecimento = new \Matricula\Service\AcadgeralAreaConhecimento($this->getEm());

        try {
            $arrDados = $objAcadCurso->toArray();

            if ($arrDados['cursoTurno']) {
                $arrDados['cursoTurno'] = explode(',', $arrDados['cursoTurno']);
            }

            if ($arrDados['pes']) {
                /** @var $objPessoaFisica \Pessoa\Entity\PessoaFisica */
                $objPessoaFisica = $objAcadCurso->getPes();

                if ($objPessoaFisica) {
                    $arrDados['pes'] = array(
                        'id'   => $objPessoaFisica->getPes()->getPesId(),
                        'text' => $objPessoaFisica->getPes()->getPesNome(),
                    );
                }
            }
            if ($arrDados['area']) {
                /** @var $objAcadgeralAreaConhecimento \Matricula\Entity\AcadgeralAreaConhecimento */
                $objAcadgeralAreaConhecimento = $serviceAcadgeralAreaConhecimento->getRepository()->find(
                    $arrDados['area']
                );

                if ($objAcadgeralAreaConhecimento) {
                    $arrDados['area'] = array(
                        'id'   => $objAcadgeralAreaConhecimento->getAreaId(),
                        'text' => $objAcadgeralAreaConhecimento->getAreaDescricao()
                    );
                } else {
                    $arrDados['area'] = null;
                }
            }

            /** @var \GerenciadorArquivos\Entity\Arquivo $objCarteirinha */
            if ($objCarteirinha = $arrDados['arqCarteirinha']) {
                unset($arrDados['arqCarteirinha']);
                $arrDados['arqCarteirinha']['id']      = $objCarteirinha->getArqId();
                $arrDados['arqCarteirinha']['nome']    = $objCarteirinha->getArqNome();
                $arrDados['arqCarteirinha']['tipo']    = $objCarteirinha->getArqTipo();
                $arrDados['arqCarteirinha']['tamanho'] = $objCarteirinha->getArqTamanho();
                $arrDados['arqCarteirinha']['chave']   = $objCarteirinha->getArqChave();
            }

            $arrDados['campus']        = $serviceCampusCurso->getArrayCampus($cursoId);
            $arrDados['configuracoes'] = $serviceAcadCursoConfig->getArrayConfiguracoes($cursoId);

            if ($arrCampusCurso = current($arrDados['campus'])) {
                $arrDados['campuscursoAutorizacao']    = $arrCampusCurso['campuscursoAutorizacao'];
                $arrDados['campuscursoReconhecimento'] = $arrCampusCurso['campuscursoReconhecimento'];
            }
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $servicePessoa                    = new \Pessoa\Service\PessoaFisica($this->getEm());
        $serviceCampus                    = new \Organizacao\Service\OrgCampus($this->getEm());
        $serviceAcadgeralAreaConhecimento = new \Matricula\Service\AcadgeralAreaConhecimento($this->getEm());

        if ($arrDados['cursoTurno'] && is_string($arrDados['cursoTurno'])) {
            $arrDados['cursoTurno'] = array($arrDados['cursoTurno']);
        }

        if (is_array($arrDados['pes'])) {
            if (!$arrDados['pes']['id']) {
                $arrDados['pes']['id']   = $arrDados['pes']['pesId'];
                $arrDados['pes']['text'] = $arrDados['pes']['pesNome'];
            }
        } elseif ($arrDados['pes'] && is_string($arrDados['pes'])) {
            $arrDados['pes'] = $servicePessoa->getArrSelect2(
                array('id' => $arrDados['pes'])
            );
            $arrDados['pes'] = $arrDados['pes'][0];
        }

        if (is_array($arrDados['campus'])) {
            $arrCampus = [];

            foreach ($arrDados['campus'] as $campus) {
                $campus['id']   = $campus['campId'];
                $campus['text'] = $campus['campNome'];
                $arrCampus[]    = $campus;
            }

            $arrDados['campus'] = $arrCampus;
        } elseif ($arrDados['campus'] && is_string($arrDados['campus'])) {
            $arrDados['campus'] = explode(',', $arrDados['campus']);
            $arrDados['campus'] = $serviceCampus->getArrSelect2(
                array('id' => $arrDados['campus'])
            );
        }

        if (is_array($arrDados['area']) && !$arrDados['area']['id']) {
            $arrDados['area']['id']   = $arrDados['area']['areaId'];
            $arrDados['area']['text'] = $arrDados['area']['areaDescricao'];
        } elseif ($arrDados['area'] && is_string($arrDados['area'])) {
            $arrDados['area'] = $serviceAcadgeralAreaConhecimento->getArrSelect2(
                array('id' => $arrDados['area'])
            );
            $arrDados['area'] = $arrDados['area'][0];
        }

        if ($arrDados['configuracoes'] && is_string($arrDados['configuracoes'])) {
            $arrDados['configuracoes'] = json_decode($arrDados['configuracoes'], true);
        }

        return $arrDados;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('cursoId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Matricula\Entity\AcadCurso */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getCursoId(),
                $params['value'] => $objEntity->getCursoNome()
            );
        }

        return $arrEntitiesArr;
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function retornaTodosOsRegistros($offset = 0, $limit = 10)
    {
        $arrEntities = $this->getRepository()->findBy([], [], $limit, $offset);

        $arrEntitiesArr = array();
        /* @var $objEntity \Matricula\Entity\AcadCurso */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = $this->getArray($objEntity->getCursoId());
        }

        return $arrEntitiesArr;
    }

    /**
     * @return int
     */
    public function retornaTotalDeRegistros()
    {
        $query = $this->getEm()->createQueryBuilder()
            ->select('COUNT(a)')
            ->from('\Matricula\Entity\AcadCurso', 'a')
            ->getQuery();

        $total = $query->getSingleScalarResult();

        return $total;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['cursoId']) {
            $this->setLastError('Para remover um registro de curso é necessário especificar o código.');

            return false;
        }

        try {
            $objAcadCurso = $this->getRepository()->find($param['cursoId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAcadCurso);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de curso.');

            return false;
        }

        return true;
    }

    /**
     * Método resposável por retornar todas as opções de curso da instituição
     *
     * @param array $params Parâmetros de busca
     *
     * @return array $result retorno com as opções buscadas
     */
    public function returnOptionsCurso(array $params = array(), $searchJson = false)
    {
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());

        $where      = array();
        $parameters = array();

        $query = "
        SELECT
        ac.curso_id, ac.curso_nome, ac.curso_id AS cursoId, ac.curso_nome AS cursoNome, ac.curso_turno,
        oc.camp_id, oc.camp_nome,
        cc.cursocampus_id,
        aplc.per_id,
        curso_prazo_integralizacao
        FROM acad_curso ac
        LEFT JOIN campus_curso cc ON cc.curso_id= ac.curso_id
        LEFT JOIN org_campus oc ON oc.camp_id = cc.camp_id
        LEFT JOIN acadperiodo__letivo_campus_curso aplc ON aplc.cursocampus_id=cc.cursocampus_id";

        if ($params['perId']) {
            if ($params['perId'] != 'null') {
                $query .= ' AND aplc.per_id = :perId';
                $parameters['perId'] = $params['perId'];
                $where[]             = ' aplc.per_id is not null';
            } else {
                $query .= ' AND  aplc.per_id is null';
            }
        }

        if ($params['q'] || $params['query'] || $params['cursoNome']) {
            $cursoNome = $params['q'] ? $params['q'] : ($params['query'] ? $params['query'] : $params['cursoNome']);

            $where[]                 = " ac.curso_nome LIKE :cursoNome";
            $parameters['cursoNome'] = "%$cursoNome%";
        }

        if ($params['areaConhecimento']) {
            $where .= " AND ac.area_id in (:areaId) ";
            $parameters['areaId'] = $params['areaConhecimento'];
        }

        if ($params['turno']) {
            $where[]                  = " ac.curso_turno like :cursoTurno";
            $parameters['cursoTurno'] = $params['turno'] . '%';
        }

        if ($params['cursoPossuiPeriodoLetivo']) {
            $where[]                                = " ac.curso_possui_periodo_letivo = :cursoPossuiPeriodoLetivo";
            $parameters['cursoPossuiPeriodoLetivo'] = $params['cursoPossuiPeriodoLetivo'];
        }

        if ($params['cursoId']) {
            $where[]               = " ac.curso_id = :cursoId";
            $parameters['cursoId'] = $params['cursoId'];
        }

        /*Quando o nome do curso for passado por paremtro expecifico*/
        if ($params['cursoNome']) {
            $where[] = ' curso_nome LIKE :cursoNome ';
            $parameters['cursoNome'];
        }

        if ($params['campId']) {
            $where[] = " oc.camp_id in (" . $params['campId'] . ")";
        }

        if ($params['notInCurso']) {
            $where[]                  = " ac.curso_id NOT IN ( :notInCurso )";
            $parameters['notInCurso'] = $params['notIn'];
        }

        if ($params['cursocampusId']) {
            $where[]                     = "  cc.cursocampus_id = :cursocampusId";
            $parameters['cursocampusId'] = $params['cursocampusId'];
        }

        if ($params['campus']) {
            $where[]              = " cc.camp_id in (:campus)";
            $parameters['campus'] = $params['campus'];
        }

        if ($params['alunoId']) {
            $where[] = " ac.curso_id not in (select cursocampus_id from acadgeral__aluno_curso where aluno_id= {$params['alunoId']}) ";
        }

        $arrCampusCursoPermitido = implode(",", $serviceAcessoPessoas->retornaCampusCursoDoUsuarioLogado(true));

        if ($arrCampusCursoPermitido) {
            $where[] = " cc.cursocampus_id in (" . $arrCampusCursoPermitido . ")";
        }

        if ($where) {
            $query .= " WHERE " . implode(' AND ', $where);
        }

        $query .= " GROUP BY ac.curso_id";
        $query .= " ORDER BY ac.curso_nome ASC";

        if ($searchJson) {
            $query .= " LIMIT 0,40";
        }

        $result = $this->executeQueryWithParam($query, $parameters)->fetchAll();

        return $result;
    }

    /**
     * @return mixed
     */
    public function mountArrayCurso()
    {
        $cursos = $this->getEm()->getRepository("Matricula\Entity\AcadCurso")->findAll();

        foreach ($cursos as $curso) {
            $aux                          = $curso->toArray();
            $arrayCursos[$aux['cursoId']] = $aux['cursoNome'];
        }

        return $arrayCursos;
    }

    /**
     * @param        $cursoId
     * @param string $dataBase
     * @return bool|string
     */
    public function calcularTempoExpiracaoCurso($cursoId, $dataBase = '')
    {
        if (!$cursoId) {
            $this->setLastError('Para calcular o tempo de expiração de um curso é necessário especificar o código.');

            return false;
        }

        /** @var \Matricula\Entity\AcadCurso $objAcadCurso */
        $objAcadCurso         = $this->getRepository()->find($cursoId);
        $integralizacaoMaxima = $objAcadCurso->getCursoPrazoIntegralizacaoMaxima();
        $tempo                = 0;

        switch ($objAcadCurso->getCursoUnidadeMedida()) {
            case self::CURSO_UNIDADE_MEDIDA_ANUAL:
                $tempo = 12;
                break;
            case self::CURSO_UNIDADE_MEDIDA_QUADRIMESTRAL:
                $tempo = 4;
                break;
            case self::CURSO_UNIDADE_MEDIDA_SEMESTRAL:
                $tempo = 6;
                break;
            case self::CURSO_UNIDADE_MEDIDA_TRIMESTRAL:
                $tempo = 3;
                break;
            case self::CURSO_UNIDADE_MEDIDA_BIMESTRAL:
                $tempo = 2;
                break;
            case self::CURSO_UNIDADE_MEDIDA_MENSAL:
                $tempo = 1;
                break;
        }

        $tempo = $tempo * ($integralizacaoMaxima ? $integralizacaoMaxima : 0);

        $strTime          = trim(self::formatDateAmericano($dataBase) . ' +' . $tempo . ' month');
        $tempoDeExpiracao = strtotime($strTime);
        $dataExpiracao    = date('Y-m-d', $tempoDeExpiracao);

        return $dataExpiracao;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getDescricao($id)
    {
        if (!is_array($id)) {
            $id = array($id);
        }

        $id = implode($id, ',');

        $sql = "
        SELECT group_concat(curso_nome SEPARATOR ', ') AS text
        FROM acad_curso
        WHERE curso_id IN(:cursoId)";

        $result = $this->executeQueryWithParam($sql, ['cursoId' => $id])->fetch();

        return $result['text'];
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceAcadNivel       = new \Matricula\Service\AcadNivel($this->getEm());
        $serviceAcadModalidade  = new \Matricula\Service\AcadModalidade($this->getEm());
        $serviceAcadCursoConfig = new \Matricula\Service\AcadCursoConfig($this->getEm());

        $view->setVariables(
            [
                "arrCursoIntegralizacao"      => $this->getArrSelect2CursoIntegralizacao(),
                "arrCursoPossuiPeriodoLetivo" => $this->getArrSelect2CursoPossuiPeriodoLetivo(),
                "arrCursoTurno"               => $this->getArrSelect2CursoTurno(),
                "arrCursoSituacao"            => $this->getArrSelect2CursoSituacao(),
                "arrNivel"                    => $serviceAcadNivel->getArrSelect2(),
                "arrModalidade"               => $serviceAcadModalidade->getArrSelect2(),
                "arrMetodoDesempenho"         => $serviceAcadCursoConfig->getArrSelect2MetodoDesempenho(),
                "arrMetodoDesempenhoFinal"    => $serviceAcadCursoConfig->getArrSelect2MetodoDesempenhoFinal(),
                "arrNotaFracionada"           => $serviceAcadCursoConfig->getArrSelect2NotaFracionada(),
                'arrSituacaoDeferimento'      => (
                \Matricula\Service\AcadgeralAlunoCurso::getArrSelect2AlunocursoSituacaoDisponiveisDeferimento()
                ),
            ]
        );
    }
}