<?php

namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

class AcadgeralDocente extends AbstractService
{
    const DOCENTE_ATIVO_SIM = 'Sim';
    const DOCENTE_ATIVO_NAO = 'Não';
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2DocenteAtivo($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getDocenteAtivo());
    }

    public static function getDocenteAtivo()
    {
        return array(self::DOCENTE_ATIVO_SIM, self::DOCENTE_ATIVO_NAO);
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadgeralDocente');
    }

    public function pesquisaForJson($params)
    {
        $sql       = '
        SELECT
        a.docenteId AS docente_id,
        a.pesId AS pes_id,
        a.docenteDataInicio AS docente_data_inicio,
        a.docenteAtivo AS docente_ativo,
        a.docenteDataFim AS docente_data_fim,
        a.docenteCurLattes AS docente_cur_lattes,
        a.docenteNumMec AS docente_num_mec,
        a.docenteTituto AS docente_tituto
        FROM Matricula\Entity\AcadgeralDocente a
        WHERE';
        $pesId     = false;
        $docenteId = false;

        if ($params['q']) {
            $pesId = $params['q'];
        } elseif ($params['query']) {
            $pesId = $params['query'];
        }

        if ($params['docenteId']) {
            $docenteId = $params['docenteId'];
        }

        $parameters = array('pesId' => "{$pesId}%");
        $sql .= ' a.pesId LIKE :pesId';

        if ($docenteId) {
            $parameters['docenteId'] = explode(',', $docenteId);
            $parameters['docenteId'] = $docenteId;
            $sql .= ' AND a.docenteId NOT IN(:docenteId)';
        }

        $sql .= " ORDER BY a.pesId";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $servicePessoaFisica = new \Pessoa\Service\PessoaFisica($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['docenteId']) {
                /** @var $objAcadgeralDocente \Matricula\Entity\AcadgeralDocente */
                $objAcadgeralDocente = $this->getRepository()->find($arrDados['docenteId']);

                if (!$objAcadgeralDocente) {
                    $this->setLastError('Registro de docente não existe!');

                    return false;
                }
            } else {
                $objAcadgeralDocente = new \Matricula\Entity\AcadgeralDocente();
            }

            if ($arrDados['pes']) {
                /** @var $objPessoaFisica \Pessoa\Entity\PessoaFisica */
                $objPessoaFisica = $servicePessoaFisica->getRepository()->find($arrDados['pes']);

                if (!$objPessoaFisica) {
                    $this->setLastError('Registro de física não existe!');

                    return false;
                }

                $objAcadgeralDocente->setPes($objPessoaFisica);
            } else {
                $objAcadgeralDocente->setPes(null);
            }

            if (!$arrDados['docenteDataInicio']) {
                $arrDados['docenteDataInicio'] = new \DateTime();
            }

            $objAcadgeralDocente->setDocenteDataInicio($arrDados['docenteDataInicio']);
            $objAcadgeralDocente->setDocenteAtivo($arrDados['docenteAtivo']);
            $objAcadgeralDocente->setDocenteDataFim($arrDados['docenteDataFim']);
            $objAcadgeralDocente->setDocenteCurLattes($arrDados['docenteCurLattes']);
            $objAcadgeralDocente->setDocenteNumMec($arrDados['docenteNumMec']);
            $objAcadgeralDocente->setDocenteTituto($arrDados['docenteTituto']);

            $this->getEm()->persist($objAcadgeralDocente);
            $this->getEm()->flush($objAcadgeralDocente);

            $this->getEm()->commit();

            $arrDados['docenteId'] = $objAcadgeralDocente->getDocenteId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de docente!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['pes']) {
            $errors[] = 'Por favor preencha o campo "código pessoa"!';
        }

        if (!$arrParam['docenteAtivo']) {
            $errors[] = 'Por favor preencha o campo "ativo"!';
        }

        if (!in_array($arrParam['docenteAtivo'], self::getDocenteAtivo())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "ativo"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "
        SELECT * FROM acadgeral__docente
        INNER JOIN pessoa USING (pes_id)";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($docenteId)
    {
        $arrDados = array();

        if (!$docenteId) {
            $this->setLastError('docente inválido!');

            return array();
        }

        /** @var $objAcadgeralDocente \Matricula\Entity\AcadgeralDocente */
        $objAcadgeralDocente = $this->getRepository()->find($docenteId);

        if (!$objAcadgeralDocente) {
            $this->setLastError('docente não existe!');

            return array();
        }

        $servicePessoaFisica = new \Pessoa\Service\PessoaFisica($this->getEm());

        try {
            $arrDados = $objAcadgeralDocente->toArray();

            if ($arrDados['pes']) {
                $arrPessoaFisica = $servicePessoaFisica->getArrSelect2(['id' => $arrDados['pes']]);
                $arrDados['pes'] = $arrPessoaFisica ? $arrPessoaFisica[0] : null;
            }
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $servicePessoaFisica = new \Pessoa\Service\PessoaFisica($this->getEm());

        if (is_array($arrDados['pes']) && !$arrDados['pes']['text']) {
            $arrDados['pes'] = $arrDados['pes']['pesId'];
        }

        if ($arrDados['pes'] && is_string($arrDados['pes'])) {
            $arrDados['pes'] = $servicePessoaFisica->getArrSelect2(array('id' => $arrDados['pes']));
            $arrDados['pes'] = $arrDados['pes'] ? $arrDados['pes'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['docenteId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['pesId' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Matricula\Entity\AcadgeralDocente */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getDocenteId();
            $arrEntity[$params['value']] = $objEntity->getPesId();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($docenteId)
    {
        if (!$docenteId) {
            $this->setLastError('Para remover um registro de docente é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objAcadgeralDocente \Matricula\Entity\AcadgeralDocente */
            $objAcadgeralDocente = $this->getRepository()->find($docenteId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAcadgeralDocente);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de docente.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $servicePessoaFisica = new \Pessoa\Service\PessoaFisica($this->getEm());

        $servicePessoaFisica->setarDependenciasView($view);

        $view->setVariable("arrDocenteAtivo", $this->getArrSelect2DocenteAtivo());
    }

    public function buscaDisciplinasUsuario($usuarioId)
    {
        $perAtual = (new \Matricula\Service\AcadperiodoLetivo($this->getEm()))->buscaPeriodoCorrente(true);

        $periodos = '';

        /** @var \Matricula\Entity\AcadperiodoLetivo $entidade */
        foreach ($perAtual as $entidade) {
            $periodos = $periodos ? $periodos . ',' . $entidade->getPerId() : $entidade->getPerId();
        }

        $query = "
            SELECT
              docentedisc.disc_id, disciplina.disc_nome, turma_nome, docdisc_id
            FROM
              acesso_pessoas ace
            INNER JOIN
              pessoa_fisica pf ON pf.pes_id = ace.pes_fisica
            INNER JOIN
              pessoa pes ON pf.pes_id = pes.pes_id
            INNER JOIN
              acadgeral__docente docente ON docente.pes_id = pes.pes_id
            INNER JOIN
              acadperiodo__docente_disciplina docentedisc ON docentedisc.docente_id = docente.docente_id
            INNER JOIN
              acadperiodo__turma turma ON turma.turma_id = docentedisc.turma_id
            INNER JOIN
              acadgeral__disciplina disciplina ON disciplina.disc_id = docentedisc.disc_id
            WHERE
              ace.id = {$usuarioId} AND turma.per_id IN ($periodos)
        ";

        $disciplinas = $this->executeQuery($query)->fetchAll();

        return $disciplinas;
    }

    public function buscaDadosDocente($idDocente)
    {
        return $this->executeQuery(
            "SELECT
            pessoa_fisica.*, pessoa.*
        FROM acadgeral__docente
        NATURAL JOIN pessoa_fisica
        NATURAL JOIN pessoa
        WHERE docente_id = {$idDocente}"
        )->fetchAll();
    }
}