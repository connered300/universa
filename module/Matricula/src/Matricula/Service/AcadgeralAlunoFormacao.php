<?php

namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class AcadgeralAlunoFormacao
 * @package Matricula\Service
 */
class AcadgeralAlunoFormacao extends AbstractService
{
    const FORMACAO_TIPO_ENSINO_PUBLICO      = 'Publico';
    const FORMACAO_TIPO_ENSINO_PRIVADO      = 'Privado';
    const FORMACAO_TIPO_ENSINO_MEDIO        = 'Ensino Médio';

    const FORMACAO_TIPO_GRADUACAO           = 'Graduação';
    const FORMACAO_TIPO_POS_GRADUACAO       = 'Pós Graduação';
    const FORMACAO_TIPO_MESTRADO            = 'Mestrado';
    const FORMACAO_TIPO_ENSINO_FUNDAMENTAL  = 'Ensino Fundamental';
    const FORMACAO_TIPO_DOUTORADO           = 'Doutorado';

    const FORMACAO_REGIME_ANUAL             = 'Anual';
    const FORMACAO_REGIME_SEMESTRAL         = 'Semestral';
    const FORMACAO_REGIME_TRIMESTRAL        = 'Trimestral';
    const FORMACAO_REGIME_BIMESTRAL         = 'Bimestral';
    const FORMACAO_REGIME_MENSAL            = 'Mensal';

    const FORMACAO_CONCLUIDO_SIM = 'Sim';
    const FORMACAO_CONCLUIDO_NAO = 'Não';

    private $lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadgeralAlunoFormacao');
    }

    /**
     * @param $instituicao
     * @return mixed
     */
    public function buscaInstituicao($instituicao)
    {
        $sql = "
        SELECT formacao_instituicao instituicao
        FROM acadgeral__aluno_formacao
        WHERE formacao_instituicao LIKE :formacao_instituicao
        GROUP BY formacao_instituicao
        LIMIT 0, 30";

        $stmt = parent::executeQueryWithParam($sql, array('formacao_instituicao' => '%' . $instituicao . '%'));

        return $stmt->fetchAll();
    }

    /**
     * @param $curso
     * @return mixed
     */
    public function buscaCurso($curso)
    {
        $sql = "
        SELECT formacao_curso curso
        FROM acadgeral__aluno_formacao
        WHERE formacao_curso LIKE :formacao_curso
        GROUP BY formacao_curso
        LIMIT 0, 30";

        $stmt = parent::executeQueryWithParam($sql, array('formacao_curso' => '%' . $curso . '%'));

        return $stmt->fetchAll();
    }

    /**
     * @param $params
     * @return mixed
     */
    public function pesquisaForJson($params)
    {
        $sql        = 'SELECT * FROM acadgeral__aluno_formacao WHERE';
        $q          = false;
        $formacaoId = $params['formacaoId'] ? $params['formacaoId'] : false;

        if ($params['q']) {
            $q = $params['q'];
        } elseif ($params['query']) {
            $q = $params['query'];
        }

        $parameters = array(
            'formacao_instituicao' => "{$q}%",
            'formacao_curso'       => "{$q}%"
        );

        $sql .= ' (formacao_instituicao LIKE :formacao_instituicao OR formacao_curso LIKE :formacao_curso)';

        if ($formacaoId) {
            $parameters['formacao_id'] = $formacaoId;
            $sql .= ' AND formacao_id <> :formacao_id';
        }
        if (isset($params['alunoId'])) {
            $parameters['aluno_id']      = $params['alunoId'] ? $params['alunoId'] : '-1';
            $parameters['formacao_tipo'] = self::FORMACAO_TIPO_ENSINO_MEDIO;
            $sql .= ' AND aluno_id = :aluno_id AND formacao_tipo <> :formacao_tipo';
        }

        if ($params['agruparPorFormacao']) {
            $sql .= " GROUP BY formacao_tipo";
        }

        $sql .= " ORDER BY formacao_ano";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    /**
     * @param array                            $arrParam
     * @param \Matricula\Entity\AcadgeralAluno $objAcadgeralAluno
     * @return bool
     */
    public function salvarMultiplos(array &$arrParam, \Matricula\Entity\AcadgeralAluno $objAcadgeralAluno)
    {
        $arrExcluir = array();

        try {
            $this->getEm()->beginTransaction();

            $arrFormacoes = $arrParam['formacoes'];

            if ($arrParam['formacoes'] && is_string($arrParam['formacoes'])) {
                $arrFormacoes = json_decode($arrFormacoes, true);
            }

            $arrFormacoesDB = $this->getRepository()->findBy(['aluno' => $objAcadgeralAluno->getAlunoId()]);

            /* @var $objAlunoFormacao \Matricula\Entity\AcadgeralAlunoFormacao */
            foreach ($arrFormacoesDB as $objAlunoFormacao) {
                $encontrado = false;
                $formacaoId = $objAlunoFormacao->getFormacaoId();

                foreach ($arrFormacoes as $arrFormacao) {
                    if ($formacaoId == $arrFormacao['formacaoId']) {
                        $encontrado = true;
                        break;
                    }
                }

                if (!$encontrado) {
                    $arrExcluir[$formacaoId] = $objAlunoFormacao;
                }
            }

            if ($arrExcluir) {
                foreach ($arrExcluir as $objAlunoFormacao) {
                    $this->getEm()->remove($objAlunoFormacao);
                    $this->getEm()->flush();
                }
            }

            foreach ($arrFormacoes as $arrAlunoCurso) {
                $alunoId                  = $objAcadgeralAluno->getAlunoId();
                $arrAlunoCurso['alunoId'] = $alunoId;

                if ($this->hasArray($arrAlunoCurso)) {
                    foreach ($arrAlunoCurso as $arrAluno) {
                        if (is_array($arrAluno)) {
                            $arrAluno['alunoId'] = $alunoId;

                            if (!$this->salvarAlunoFormacao($arrAluno)) {
                                return false;
                            }
                        }
                    }
                } else {
                    if (!$this->salvarAlunoFormacao($arrAlunoCurso)) {
                        throw new \Exception($this->getLastError());
                    }
                }
            }

            $this->getEm()->commit();

            return true;
        } catch (\Exception $e) {
            $this->getEm()->rollback();
            $this->setLastError(
                'Não foi possível salvar o registros de aluno no curso!' .
                '<br>' .
                $e->getMessage()
            );
        }

        return false;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function salvarAlunoFormacao(array &$arrDados)
    {
        $arrDados['formacaoDuracao'] = 1;

        if ($arrDados['formacaoTipo'] == self::FORMACAO_TIPO_ENSINO_MEDIO) {
            $arrDados['formacaoCurso']   = $arrDados['formacaoCurso'] ? $arrDados['formacaoCurso'] : 'Ensino Médio';
            $arrDados['formacaoRegime']  = self::FORMACAO_REGIME_ANUAL;
            $arrDados['formacaoDuracao'] = 3;
        }

        if (!$arrDados['formacaoConcluido']) {
            $arrDados['formacaoConcluido'] = self::FORMACAO_CONCLUIDO_SIM;
        }

        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceAcadgeralAluno = new \Matricula\Service\AcadgeralAluno($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if (preg_match('/-/', $arrDados['formacaoId'])) {
                $arrDados['formacaoId'] = null;
            }

            if ($arrDados['formacaoId']) {
                $objAcadgeralAlunoFormacao = $this->getRepository()->find($arrDados['formacaoId']);

                if (!$objAcadgeralAlunoFormacao) {
                    $this->setLastError('Registro de formação não existe!');

                    return false;
                }
            } else {
                $objAcadgeralAlunoFormacao = new \Matricula\Entity\AcadgeralAlunoFormacao();
            }

            if ($arrDados['alunoId']) {
                /* @var $objAcadgeralAluno \Matricula\Entity\AcadgeralAluno */
                $objAcadgeralAluno = $serviceAcadgeralAluno->getRepository()->find($arrDados['alunoId']);

                if (!$objAcadgeralAluno) {
                    $this->setLastError('Registro de matrícula do aluno não existe!');

                    return false;
                }

                $objAcadgeralAlunoFormacao->setAluno($objAcadgeralAluno);
            }

            $objAcadgeralAlunoFormacao->setFormacaoTipoEnsino($arrDados['formacaoTipoEnsino']);
            $objAcadgeralAlunoFormacao->setFormacaoTipo($arrDados['formacaoTipo']);
            $objAcadgeralAlunoFormacao->setFormacaoCurso($arrDados['formacaoCurso']);
            $objAcadgeralAlunoFormacao->setFormacaoInstituicao($arrDados['formacaoInstituicao']);
            $objAcadgeralAlunoFormacao->setFormacaoCidade($arrDados['formacaoCidade']);
            $objAcadgeralAlunoFormacao->setFormacaoEstado($arrDados['formacaoEstado']);
            $objAcadgeralAlunoFormacao->setFormacaoAno($arrDados['formacaoAno']);
            $objAcadgeralAlunoFormacao->setFormacaoRegime($arrDados['formacaoRegime']);
            $objAcadgeralAlunoFormacao->setFormacaoDuracao($arrDados['formacaoDuracao']);
            $objAcadgeralAlunoFormacao->setFormacaoObs($arrDados['formacaoObs']);
            $objAcadgeralAlunoFormacao->setFormacaoConcluido($arrDados['formacaoConcluido']);

            $this->getEm()->persist($objAcadgeralAlunoFormacao);
            $this->getEm()->flush($objAcadgeralAlunoFormacao);

            $this->getEm()->commit();

            $arrDados['formacaoId'] = $objAcadgeralAlunoFormacao->getFormacaoId();

            return true;
        } catch (\Exception $e) {
            $this->getEm()->rollback();
            $this->setLastError(
                'Não foi possível salvar o registro de formação!' .
                '<br>' .
                $e->getMessage()
            );
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if ($arrParam['formacaoTipoEnsino'] && !in_array(
                $arrParam['formacaoTipoEnsino'],
                self::getFormacaoTipoEnsino()
            )
        ) {
            $errors[] = 'Por favor selecione um valor válido para o campo "tipo de ensino"!';
        }

        if (!$arrParam['formacaoTipo']) {
            $errors[] = 'Por favor preencha o campo "tipo"!';
        }

        if (!in_array($arrParam['formacaoTipo'], self::getFormacaoTipo())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "tipo de formação"!';
        }

        if (!$arrParam['formacaoInstituicao']) {
            $errors[] = 'Por favor preencha o campo "instituição de formação"!';
        }

        if (!$arrParam['formacaoCurso']) {
            $errors[] = 'Por favor preencha o campo "curso de formação
            "!';
        }

        if ($arrParam['formacaoRegime'] && !in_array($arrParam['formacaoRegime'], self::getFormacaoRegime())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "regime"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public static function getFormacaoTipoEnsino()
    {
        return array(self::FORMACAO_TIPO_ENSINO_PUBLICO, self::FORMACAO_TIPO_ENSINO_PRIVADO);
    }

    /**
     * @return array
     */
    public static function getFormacaoTipo()
    {
        return array(
            self::FORMACAO_TIPO_ENSINO_MEDIO,
            self::FORMACAO_TIPO_ENSINO_FUNDAMENTAL,
            self::FORMACAO_TIPO_GRADUACAO,
            self::FORMACAO_TIPO_POS_GRADUACAO,
            self::FORMACAO_TIPO_MESTRADO,
            self::FORMACAO_TIPO_DOUTORADO
        );
    }

    /**
     * @return array
     */
    public static function getFormacaoRegime()
    {
        return array(
            self::FORMACAO_REGIME_ANUAL,
            self::FORMACAO_REGIME_SEMESTRAL,
            self::FORMACAO_REGIME_TRIMESTRAL,
            self::FORMACAO_REGIME_BIMESTRAL,
            self::FORMACAO_REGIME_MENSAL
        );
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function validacaoMultipla($arrParam)
    {
        $errors = [];

        $arrFormacoes               = json_decode($arrParam['formacoes'], true);
        $arrAlunoCurso              = $arrParam['alunoCurso'];
        $formacaoObrigatorioInativa = true;

        if (($arrAlunoCurso && !$arrFormacoes) && !$formacaoObrigatorioInativa) {
            $errors[] = 'Por favor informe a formação do aluno!';
        } elseif ($arrFormacoes) {
            $ensinoMedio           = 0;
            $graduacao             = 0;
            $arrFormacoesValidacao = [];

            foreach ($arrFormacoes as $arrFormacao) {
                if ($arrFormacao['formacaoTipo'] == self::FORMACAO_TIPO_ENSINO_MEDIO) {
                    $ensinoMedio++;
                }

                if ($arrFormacao['formacaoTipo'] == self::FORMACAO_TIPO_GRADUACAO) {
                    $graduacao++;
                }

                $formacaoChave = (
                    trim($arrFormacao['formacaoInstituicao']) . ' - ' . trim($arrFormacao['formacaoCurso'])
                );

                if (!$arrFormacoesValidacao[$formacaoChave]) {
                    $arrFormacoesValidacao[$formacaoChave] = 0;
                }

                $arrFormacoesValidacao[$formacaoChave] += 1;

                if (!$this->valida($arrFormacao)) {
                    $erros[] = (
                        '<p>Formação "' . $formacaoChave . '" possui erros: <br>' . $this->getLastError() . '</p>'
                    );
                }
            }

            //Registro de formação no ensino médio é obrigatório para o censo educacional
            if ($arrAlunoCurso && $ensinoMedio == 0) {
                $erros[] = 'Insira a formação do aluno no ensino médio!';
            }

            //Múltiplos registros de ensino médio
            if ($ensinoMedio > 1) {
                $erros[] = 'Insira somente o último registro de formação do aluno no ensino médio!';
            }

            foreach ($arrFormacoesValidacao as $formacaoChave) {
                if ($arrFormacoesValidacao[$formacaoChave] > 1) {
                    $erros[] = (
                        '<p>Formação "' . $formacaoChave . '" foi registrada ' .
                        $arrFormacoesValidacao[$formacaoChave] . ' vezes.<br>' .
                        'Remova os itens duplicados.'
                    );
                }
            }
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM acadgeral__aluno_formacao";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $formacaoId
     * @return array|null|object
     */
    public function getArray($formacaoId)
    {
        $arrDados = $this->getRepository()->find($formacaoId);

        try {
            $arrDados = $arrDados->toArray();
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('formacaoId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();

        /* @var $objEntity \Matricula\Entity\AcadgeralAlunoFormacao */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getFormacaoId(),
                $params['value'] => $objEntity->getFormacaoInstituicao() . ' / ' . $objEntity->getFormacaoCurso()
            );
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['formacaoId']) {
            $this->setLastError(
                'Para remover um registro de formação é necessário especificar o código.'
            );

            return false;
        }

        try {
            $objAcadgeralAlunoFormacao = $this->getRepository()->find($param['formacaoId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAcadgeralAlunoFormacao);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de formação.');

            return false;
        }

        return true;
    }

    /**
     * @param $alunoId
     * @return array
     */
    public function getArrayFormacoes($alunoId)
    {
        $sql = '
        SELECT
            pessoa.pes_id,
            pessoa.pes_nome,
            formacao.*,
            count(equivalencia.equivalencia_id) AS numero_equivalencias
        FROM acadgeral__aluno_formacao AS formacao
        INNER JOIN acadgeral__aluno AS aluno ON aluno.aluno_id=formacao.aluno_id
        INNER JOIN pessoa ON pessoa.pes_id=aluno.pes_id
        LEFT JOIN acadperiodo__resumo_equivalencia equivalencia
            ON equivalencia.formacao_id = formacao.formacao_id
        WHERE aluno.aluno_id=:aluno_id
        GROUP BY formacao.formacao_id';

        $parameters = array('aluno_id' => $alunoId);

        $arrAlunoFormacoes = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        $arrAlunoFormacaoRetorno = [];
        foreach ($arrAlunoFormacoes as $arrAlunoFormacao) {
            $arrAlunoFormacaoRetorno[] = [
                'formacaoId'          => $arrAlunoFormacao['formacao_id'],
                'alunoId'             => $arrAlunoFormacao['aluno_id'],
                'pesId'               => $arrAlunoFormacao['pes_id'],
                'pesNome'             => $arrAlunoFormacao['pes_nome'],
                'formacaoCurso'       => $arrAlunoFormacao['formacao_curso'],
                'formacaoInstituicao' => $arrAlunoFormacao['formacao_instituicao'],
                'formacaoCidade'      => $arrAlunoFormacao['formacao_cidade'],
                'formacaoEstado'      => $arrAlunoFormacao['formacao_estado'],
                'formacaoTipoEnsino'  => $arrAlunoFormacao['formacao_tipo_ensino'],
                'formacaoTipo'        => $arrAlunoFormacao['formacao_tipo'],
                'formacaoAno'         => $arrAlunoFormacao['formacao_ano'],
                'formacaoRegime'      => $arrAlunoFormacao['formacao_regime'],
                'formacaoConcluido'   => $arrAlunoFormacao['formacao_concluido'],
                'formacaoDuracao'     => $arrAlunoFormacao['formacao_duracao'],
                'formacaoObs'         => $arrAlunoFormacao['formacao_obs'],
                'numeroEquivalencias' => $arrAlunoFormacao['numero_equivalencias'],
            ];
        }

        return $arrAlunoFormacaoRetorno;
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $view->setVariable("arrFormacaoTipoEnsino", $this->getArrSelect2FormacaoTipoEnsino());
        $view->setVariable("arrFormacaoTipo", $this->getArrSelect2FormacaoTipo());
        $view->setVariable("arrFormacaoConcluido", $this->getArrSelect2FormacaoConcluido());
        $view->setVariable("arrFormacaoRegime", $this->getArrSelect2FormacaoRegime());
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2FormacaoTipoEnsino($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getFormacaoTipoEnsino());
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2FormacaoTipo($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getFormacaoTipo());
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2FormacaoConcluido($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getFormacaoConcluido());
    }

    /**
     * @return array
     */
    public static function getFormacaoConcluido()
    {
        return array(self::FORMACAO_CONCLUIDO_SIM, self::FORMACAO_CONCLUIDO_NAO);
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2FormacaoRegime($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getFormacaoRegime());
    }

    public function hasArray($arrDados)
    {
        rsort($arrDados);

        return isset($arrDados[0]) && is_array($arrDados[0]);
    }
}

?>