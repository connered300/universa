<?php

namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

class AcadperiodoDocentes extends AbstractService
{
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Professor\Entity\AcadgeralDocente');
    }

    protected function valida($dados)
    {
    }

    protected function pesquisaForJson($criterios)
    {
    }

    public function search(array $data, $perId)
    {
        $query = <<<SQL
                           SELECT
                DISTINCT docente.docente_id docente_id,
                pessoa.pes_nome docente_descricao,
				qtdDisc.qtd discNum,
                concat(emDia.resultado,' / ',(qtdDisc.qtd * emDia.meses)) Pendencia,
                concat(emDia2.resultado,' / ',(qtdDisc.qtd * emDia2.meses)) anotacao,
                concat(ifnull(diarioFinal.resultado,0),' / ',ifnull(qtdDisc.qtd,0)) Pendencia2,
                (qtdDisc.qtd * emDia.meses),
                emDia.meses
            FROM
                acadgeral__docente docente
            INNER JOIN pessoa USING(pes_id)
            LEFT JOIN
            (
			SELECT
				count(disc.disc_id) qtd,
				acadgeral__docente.docente_id
			FROM
				acadgeral__docente
			JOIN
				acadperiodo__docente_disciplina docDisc USING(docente_id)
			LEFT JOIN
				acadgeral__disciplina disc USING(disc_id)
			LEFT JOIN
				acadperiodo__turma USING(turma_id)
			WHERE
				per_id = {$perId}
				GROUP BY docDisc.docente_id
            )qtdDisc on qtdDisc.docente_id = docente.docente_id
			LEFT JOIN
            (
			SELECT
				count(diario_frequencia_entrega_data) resultado,
                a.docente_id,
                meses
                FROM
					acadperiodo__docente_disciplina a
            LEFT JOIN
					acadgeral__disciplina b USING(disc_id)
			LEFT JOIN
					acadperiodo__diario_entrega c USING(docdisc_id)
								LEFT JOIN
				acadperiodo__turma USING(turma_id)
			JOIN
				(
				SELECT
					TIMESTAMPDIFF(MONTH, per_data_inicio, per_data_fim) + 1 meses
				FROM
					acadperiodo__letivo
				WHERE
					per_id = {$perId}
                )meses
                where per_id = {$perId}
			GROUP BY a.docente_id
            )emDia on emDia.docente_id = docente.docente_id
            LEFT JOIN
			(
			SELECT
				count(diario_anotacoes_entrega_data) resultado,
                acadperiodo__docente_disciplina.docente_id,
                meses
                FROM
					acadperiodo__docente_disciplina
            LEFT JOIN
					acadgeral__disciplina USING(disc_id)
			LEFT JOIN
					acadperiodo__diario_entrega USING(docdisc_id)
					LEFT JOIN
				acadperiodo__turma USING(turma_id)
			JOIN
				(
				SELECT
					TIMESTAMPDIFF(MONTH, per_data_inicio, per_data_fim) + 1 meses
				FROM
					acadperiodo__letivo
				WHERE
					per_id = {$perId}
                )meses
                where per_id = {$perId}
			GROUP BY acadperiodo__docente_disciplina.docente_id
            )emDia2 on emDia2.docente_id = docente.docente_id
            LEFT JOIN
            (
                select
                count(diario_encerramento_entrega_data) resultado,
                acadperiodo__docente_disciplina.docente_id
                from acadperiodo__diario_entrega
                join acadperiodo__docente_disciplina using(docdisc_id)
                left join acadperiodo__turma using(turma_id)
                where diario_encerramento_entrega_data is not null and per_id = {$perId}
                GROUP BY acadperiodo__docente_disciplina.docente_id
            )diarioFinal on diarioFinal.docente_id = docente.docente_id
            WHERE
                docente_ativo = 'Sim'
			AND
				qtdDisc.qtd is not null
SQL;

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getInfoDiario($data = array())
    {
        $result = array();

        $periodoLetivo = $data['periodo'];

        if ($periodoLetivo) {
            if (!is_object($periodoLetivo)) {
                $periodoLetivo = $this->getReference((int)$periodoLetivo, 'Matricula\Entity\AcadperiodoLetivo');
            }
        } else {
            $periodoLetivo = (new \Matricula\Service\AcadperiodoLetivo($this->getEm()))->buscaPeriodoCorrente();
        }

        $dateInitLetivo = $periodoLetivo->getPerDataInicio()->format('Y-m-d');
        $dateFimLetivo  = $periodoLetivo->getPerDataFim()->format('Y-m-d');

        $docenteDisciplinas = $this->getEm()->createQuery(
            "SELECT
                DD
            FROM
                Matricula\Entity\AcadperiodoDocenteDisciplina DD
            WHERE DD.docente = :docente
                AND (DD.docdiscDataInicio >= :perDataInit AND DD.docdiscDataInicio <= :perDataFim)"
        )
            ->setParameter('docente', $data['docente'])
            ->setParameter('perDataInit', $dateInitLetivo)
            ->setParameter('perDataFim', $dateFimLetivo)
            ->getResult();

        $repoDiarioEntrega = $this->getRepository('Matricula\Entity\AcadperiodoDiarioEntrega');

        $result['periodo'] = [
            'id'       => $periodoLetivo->getPerId(),
            'dateInit' => $dateInitLetivo,
            'dateFim'  => $dateFimLetivo,
        ];

        // agrupando turmas
        foreach ($docenteDisciplinas as $docenteDisc) {
            $arrEntregas = array();
            $turma       = $docenteDisc->getTurma();
            $disciplina  = $docenteDisc->getDisc();

            $arrTurma = array(
                'docdiscId' => $docenteDisc->getDocdiscId(),
                'turmaId'   => $turma->getTurmaId(),
                'turmaNome' => $turma->getTurmaNome(),
            );

            $arrDisciplina = array(
                'discId'         => $disciplina->getDiscId(),
                'disciplinaNome' => $disciplina->getDiscNome(),
            );

            $entregas = $repoDiarioEntrega->findBy(array('docdisc' => $docenteDisc->getDocdiscId()));

            foreach ($entregas as $entrega) {
                $entrega = $entrega->toArray();

                $anotacoes    = $entrega['diarioAnotacoesEntregaData'];
                $frequencias  = $entrega['diarioFrequenciaEntregaData'];
                $encerramento = $entrega['diarioEncerramentoEntregaData'];

                $entrega['diarioAnotacoesEntregaData']    = ($anotacoes) ? $anotacoes->format('d/m/Y') : '';
                $entrega['diarioFrequenciaEntregaData']   = ($frequencias) ? $frequencias->format('d/m/Y') : '';
                $entrega['diarioEncerramentoEntregaData'] = ($encerramento) ? $encerramento->format('d/m/Y') : '';
                unset($entrega['docDisc']);

                $arrEntregas[$entrega['diarioEntregaId']] = $entrega;
            }

            $arrDisciplina['entregas']                         = $arrEntregas;
            $arrTurma['disciplinas'][$disciplina->getDiscId()] = $arrDisciplina;
            $result['turmas'][]                                = $arrTurma;
        }

        return $result;
    }

    public function saveInfoDiario(array $data)
    {
        $result = (new \Matricula\Service\AcadperiodoDiarioEntrega($this->getEm()))->save($data);

        if (is_object($result[0])) {
            return array('type' => 'success', 'message' => 'Alteração Realizada com Sucesso');
        }

        return array('type' => 'warning', 'message' => $result);
    }
}
