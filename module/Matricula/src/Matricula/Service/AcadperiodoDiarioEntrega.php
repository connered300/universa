<?php

namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

class AcadperiodoDiarioEntrega extends AbstractService
{
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadperiodoDiarioEntrega');
    }

    /**
     * @param            $data : dados enviados a serem validados
     * @param bool|false $validateEnc : validacao de encerramento opcional - para que as outras entregas sejam salvas
     *                                  mesmo que não seja possível entregar a de encerramento
     * @return array: retorna array com mensagens de validação
     *                    ou array vazio caso esteja tudo válido
     */
    protected function valida($data, $validateEnc = false)
    {
        $arrValidate = array();

        $serviceAcadperiodoLetivo = new \Matricula\Service\AcadperiodoLetivo($this->getEm());

        if (!$data['docDisc']) {
            $arrValidate[] = "Informe o ID de vínculo do docente na disciplina";
        }

        if ($validateEnc && $data['diarioEncerramento']) {

            /*TODO: Em conversa com o gestor das regras da equipe esse metodo não sera utlizado por enquanto, pois não a necessidade de validar Frequência e Anotação por enquanto, porém já foi corrigido e esta 100% funcional, porém como citado anteriormente não sera utilizado por hora. */

            /** @var $objAcadperiodoLetivo \Matricula\Entity\AcadperiodoLetivo */
            $objAcadperiodoLetivo = $serviceAcadperiodoLetivo->getRepository()->find($data['periodo']['id']);

            // obtem quantidade de meses que o periodo tem
            // para validar a entrega de encerramento

            $dateIni = new \DateTime($objAcadperiodoLetivo->getPerDataInicio()->format('Y-m-d'));
            $dateFim = new \DateTime($objAcadperiodoLetivo->getPerDataFim()->format('Y-m-d'));

            // soma +1 porque o valor no obj é a quantidade de meses entre as dastas
            $diffMes = $dateIni->diff($dateFim)->m + 1;

            // verifica se todas as anotações foram entregues
            if ($diffMes != count($data['diarioAnotacoesEntregaData'])) {
                $arrValidate[] = "Não é possível realizar Entrega Final, <strong>constam Anotações à serem entregues</strong>";
            }

            // verifica se todas as frequencias foram entregues
            if ($diffMes != count($data['diarioFrequenciaEntregaData'])) {
                $arrValidate[] = "Não é possível realizar Entrega Final, <strong>constam Frequências à serem entregues</strong>";
            }
        }

        return $arrValidate;
    }

    protected function pesquisaForJson($criterios)
    {
    }

    /**
     * Salva as informações de Diario Entrega,
     * atende a edição e adição de registros,
     * assim como também realiza as mesmas ações com multiplos registros
     * @param array $data
     * @return array|true
     */
    public function save(array $data)
    {
        $serviceAcadperiodoDiarioEntrega = new \Matricula\Service\AcadperiodoDiarioEntrega($this->getEm());

        $validate = $this->valida($data);

        $objEntityManager = $this->getEm();

        if (!empty($validate)) {
            return $validate;
        }

        /** @var  $objDiarioEntregaConfere \Matricula\Entity\AcadperiodoDiarioEntrega */
        $objDiarioEntregaConfere = $serviceAcadperiodoDiarioEntrega->getRepository()->findBy(
            ['docdisc' => $data['docDisc']]
        );

        foreach ($objDiarioEntregaConfere as $value) {
            foreach ($data['diarioEntregaId'] as $valor) {
                /** @var $value \Matricula\Entity\AcadperiodoDiarioEntrega */
                if ($valor != $value->getDiarioEntregaId(
                    ) && $data['diarioEncerramento']['encerramentoId'] != $value->getDiarioEntregaId(
                    ) || (!$data['diarioFrequenciaEntregaData'] && $valor)
                ) {
                    $objEntityManager->remove($value);
                    $objEntityManager->flush();
                }
            }
        }

        $arrResult = array();

        $this->begin();

        try {
            if (is_array($data['diarioEntregaMes'])) {
                foreach ($data['diarioEntregaMes'] as $pos => $mes) {
                    if ($mes) {
                        $objDiarioEntrega = $this->prepareObjectDiarioEntrega(
                            [
                                'diarioEntregaMes'            => $mes,
                                'docDisc'                     => $data['docDisc'],
                                'diarioEntregaId'             => $data['diarioEntregaId'][$pos] != 0 ? $data['diarioEntregaId'][$pos] : null,
                                'diarioAnotacoesEntregaData'  => $data['diarioAnotacoesEntregaData'][$pos] != 0 ? $data['diarioAnotacoesEntregaData'][$pos] : null,
                                'diarioFrequenciaEntregaData' => $data['diarioFrequenciaEntregaData'][$pos] != 0 ? $data['diarioFrequenciaEntregaData'][$pos] : null,
                            ]
                        );

                        $this->getEm()->persist($objDiarioEntrega);

                        $arrResult[] = $objDiarioEntrega;
                    }
                }
            } else {
                $objDiarioEntrega = $this->prepareObjectDiarioEntrega($data);

                $teste = $objDiarioEntrega->getDiarioEncerramentoEntregaData();
                if ($objDiarioEntrega->getDiarioEncerramentoEntregaData()) {
                    $this->getEm()->persist($objDiarioEntrega);
                    $arrResult[] = $objDiarioEntrega;
                }
            }

            $this->getEm()->flush();
            $this->commit();

            if ($data['diarioEncerramento']['encerramentoData']) {
                /*Todo: Em conversa com o gestor das regras, não sera validado a obrigatoriedade do lançamento das datas de Frequência e Anotações*/
                $valid = $this->valida($data);
                if (!empty($valid)) {
                    return $valid;
                }

                $this->begin();

                $encerramento     = $data['diarioEncerramento'];
                $objDiarioEntrega = $this->prepareObjectDiarioEntrega(
                    [
                        'diarioEntregaMes'              => $encerramento['encerramentoEntrega'],
                        'docDisc'                       => $data['docDisc'],
                        'diarioEntregaId'               => $encerramento['encerramentoId'],
                        'diarioAnotacoesEntregaData'    => null,
                        'diarioFrequenciaEntregaData'   => null,
                        'diarioEncerramentoEntregaData' => $encerramento['encerramentoData'],
                    ]
                );

                $this->getEm()->persist($objDiarioEntrega);
                $this->getEm()->flush();
                $this->commit();

                $arrResult[] = $objDiarioEntrega;
            }
        } catch (\Exception $ex) {
            return array('Houve uma falha na solicitação. Caso persista contate o suporte.');
        }

        return $arrResult;
    }

    /**
     * Prepara o Objeto de DiarioEntrega antes de presistir
     * @param array $arrDiarioEntrega : array com as informações de entrega do diário
     * @return \Matricula\Entity\AcadperiodoDiarioEntrega|Object : retorna um objeto AcadperiodoDiarioEntrega populado
     */
    private function prepareObjectDiarioEntrega(array $arrDiarioEntrega)
    {
        if ($arrDiarioEntrega['diarioEntregaId']) {
            $objDiarioEntrega = $this->getReference($arrDiarioEntrega['diarioEntregaId']);
        } else {
            $objDiarioEntrega = new \Matricula\Entity\AcadperiodoDiarioEntrega();
            $objDiarioEntrega->setDiarioEntregaMes($arrDiarioEntrega['diarioEntregaMes']);

            $docDisc = $arrDiarioEntrega['docDisc'];

            if (!is_object($docDisc)) {
                $docDisc = $this->getRepository('Matricula\Entity\AcadperiodoDocenteDisciplina')->find($docDisc);
            }

            $objDiarioEntrega->setDocdisc($docDisc);
        }

        if ($arrDiarioEntrega['diarioAnotacoesEntregaData']) {
            $anotacoes = $arrDiarioEntrega['diarioAnotacoesEntregaData'];

            if (is_string($anotacoes)) {
                $anotacoes = parent::formatDateAmericano($anotacoes);
            }

            $objDiarioEntrega->setDiarioAnotacoesEntregaData(new \DateTime($anotacoes));
        }

        if ($arrDiarioEntrega['diarioFrequenciaEntregaData']) {
            $frequencias = $arrDiarioEntrega['diarioFrequenciaEntregaData'];

            if (is_string($frequencias)) {
                $frequencias = parent::formatDateAmericano($frequencias);
            }

            $objDiarioEntrega->setDiarioFrequenciaEntregaData(new \DateTime($frequencias));
        }

        if ($arrDiarioEntrega['diarioEncerramentoEntregaData']) {
            $encerramento = $arrDiarioEntrega['diarioEncerramentoEntregaData'];

            if (is_string($encerramento)) {
                $encerramento = parent::formatDateAmericano($encerramento);
            }

            $objDiarioEntrega->setDiarioEncerramentoEntregaData(new \DateTime($encerramento));
        }

        return $objDiarioEntrega;
    }

    /**
     * Retorna array com as datas de entrega de diários
     * @param \Matricula\Entity\AcadperiodoDocenteDisciplina $objDocenteDisciplina
     * @return array
     */
    public function retornaDataEntregaDiarios(\Matricula\Entity\AcadperiodoDocenteDisciplina $objDocenteDisciplina)
    {
        $objPeriodoLetivo = $objDocenteDisciplina->getTurma()->getPer();
        $mesInicio        = $objPeriodoLetivo->getPerDataInicio()->format('m');
        $mesFim           = $objPeriodoLetivo->getPerDataFim()->format('m');
        $arrEntrega       = array();

        for ($mes = $mesInicio; $mes <= $mesFim; $mes++) {
            $strMes           = \Boleto\Service\FinanceiroRelatorios::mesReferencia($mes);
            $objDiarioEntrega = $this->getRepository()->findOneBy(
                [
                    'diarioEntregaMes' => $strMes,
                    'docdisc'          => $objDocenteDisciplina->getDocdiscId()
                ]
            );

            $dataAnotacao   = '';
            $dataFrequencia = '';

            if ($objDiarioEntrega) {
                $dataAnotacao   = $objDiarioEntrega->getDiarioAnotacoesEntregaData();
                $dataFrequencia = $objDiarioEntrega->getDiarioFrequenciaEntregaData();
                $dataAnotacao   = $dataAnotacao ? $dataAnotacao->format('d/m/Y') : '';
                $dataFrequencia = $dataFrequencia ? $dataFrequencia->format('d/m/Y') : '';
            }

            $arrEntrega[(int)$mes] = array(
                'mes'            => $strMes,
                'dataAnotacao'   => $dataAnotacao,
                'dataFrequencia' => $dataFrequencia
            );
        }

        return $arrEntrega;
    }
}