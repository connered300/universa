<?php
namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class AcadperiodoAlunoDisciplinaFinal
 * @package Matricula\Service
 */
class AcadperiodoAlunoDisciplinaFinal extends AbstractService
{
    /**
     * @var null
     */
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadperiodoAlunoDisciplinaFinal');
    }

    protected function valida($dados)
    {
    }

    /**
     * @param $criterios
     */
    protected function pesquisaForJson($criterios)
    {
    }

    /**
     * Retorna array de informações de alunos que ficaram de exame final e as notas do mesmos
     * @param integer $docdiscId
     * @return array|null
     */
    public function buscaNotasFinalAlunos($docdiscId)
    {
        $situacoes = \Matricula\Service\AcadgeralSituacao::situacoesAtividade();
        $param     = array('docdiscId' => $docdiscId);
        $sql       = '
        SELECT
            pes.pes_id,
            pes.pes_nome,
            final.alunofinal_id,
            final.alunodisc_id,
            format(final.alunofinal_nota,2) as alunofinal_nota,
            final.alunofinal_data_realizacao,
            final.alunofinal_resultado
        FROM acadperiodo__aluno_disciplina_final final
        INNER JOIN acadperiodo__aluno_disciplina alunodisc
            ON alunodisc.alunodisc_id = final.alunodisc_id
        INNER JOIN acadperiodo__docente_disciplina docdisc
            ON alunodisc.disc_id = docdisc.disc_id AND alunodisc.turma_id = docdisc.turma_id
        INNER JOIN acadperiodo__aluno alunoperiodo
            ON alunoperiodo.alunoper_id = alunodisc.alunoper_id
        INNER JOIN acadgeral__aluno_curso alunocurso
            ON alunocurso.alunocurso_id = alunoperiodo.alunocurso_id
        INNER JOIN acadgeral__aluno aluno
            ON aluno.aluno_id = alunocurso.aluno_id
        INNER JOIN pessoa pes
            ON pes.pes_id = aluno.pes_id
        WHERE docdisc.docdisc_id = :docdiscId AND
            alunoperiodo.matsituacao_id in(' . implode(', ', $situacoes) . ') AND
            alunodisc.situacao_id in(' . implode(', ', $situacoes) . ')';

        $stmt           = $this->executeQueryWithParam($sql, $param);
        $arrNotasFinais = $stmt->fetchAll();

        return $arrNotasFinais;
    }

    /**
     * Grava notas de alunos que ficaram de exame final
     * @param integer $docdiscId
     * @param array $arrNotas
     * @return bool
     */
    public function efetuaLancamentoNotasFinais($docdiscId, $arrNotas)
    {
        if (!$docdiscId) {
            $this->setLastError('Docência inválida!');

            return false;
        }

        $serviceDocenteDisciplina = new \Matricula\Service\AcadperiodoDocenteDisciplina($this->getEm());

        $objDocenteDisciplina = $serviceDocenteDisciplina->getRepository()->find($docdiscId);

        if (!$objDocenteDisciplina) {
            $this->setLastError('Docência inválida!');

            return false;
        }

        if (!$objDocenteDisciplina->getDocdiscDataFechamento()) {
            $this->setLastError('Fechamento de notas ainda não foi efetuado!');

            return false;
        }

        if ($objDocenteDisciplina->getDocdiscDataFechamentoFinal()) {
            $this->setLastError('Fechamento de notas finais já foi efetuado!');

            return false;
        }

        $serviceDiario = new \Professor\Service\AcadperiodoEtapaDiario($this->getEm());

        $alunosNotasFinais = $this->buscaNotasFinalAlunos($docdiscId);
        $configNota        = $serviceDiario->buscaConfigNota($docdiscId);

        $arrErros = array(
            'notaNula' => array(),
            'notaAlta' => array()
        );

        try {
            $this->getEm()->beginTransaction();

            foreach ($alunosNotasFinais as $alunoNotaFinal) {
                $alunoNome    = $alunoNotaFinal['pes_nome'];
                $alunodiscId  = $alunoNotaFinal['alunodisc_id'];
                $alunofinalId = $alunoNotaFinal['alunofinal_id'];
                $nota         = $arrNotas[$alunodiscId];

                if ((string)$nota == "") {
                    $arrErros['notaNula'][] = $alunoNome;
                } elseif ($nota > $configNota['notaMax']) {
                    $arrErros['notaAlta'][] = $alunoNome;
                } else {
                    $objAlunoFinal = $this->getRepository()->find($alunofinalId);

                    if ($objAlunoFinal) {
                        $objAlunoFinal->setAlunofinalNota($nota);
                        $this->getEm()->persist($objAlunoFinal);
                        $this->getEm()->flush($objAlunoFinal);
                    }
                }
            }

            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Aconteceu um erro ao efetuar lançamentos de notas finais!');

            return false;
        }

        $msg = '';

        if (count($arrErros['notaAlta']) > 0) {
            $msg .= 'Existem ' . count($arrErros['notaAlta']) . ' alunos com nota maior que o permitido.';
        }

        if (count($arrErros['notaNula']) > 0) {
            $msg .= 'Existem ' . count($arrErros['notaNula']) . ' alunos com nota vazia.';
        }

        $this->setLastError($msg);

        return true;
    }

    /**
     * Retorna array de todas as informações como falta, nota da Final e o resultado final de alunos que ficaram ou que poderiam de exame final antes do fechamento do professor
     * @param integer $docdiscId, integer $discCHoraria
     * @return array|null
     */

    public function buscaInfoAlunoFinalRelatorio($docdiscId,$discCHoraria){
        $serviceDiario = new \Professor\Service\AcadperiodoEtapaDiario($this->getEm());


        $configNota        = $serviceDiario->buscaConfigNota($docdiscId);


        $query = "SELECT *
                    FROM
                      (SELECT pes_nome,acadperiodo__aluno.alunocurso_id,SUM(alunoetapa_nota) AS 'etapas', alunofinal_nota, resaluno_nota,resaluno_situacao,alunofinal_id,
                        (select count(*)
                          FROM acadperiodo__frequencia
                        WHERE acadperiodo__frequencia.alunodisc_id = acadperiodo__aluno_disciplina.alunodisc_id) as 'faltas'
                      FROM
                        pessoa
                            INNER JOIN
                        acadgeral__aluno ON pessoa.pes_id = acadgeral__aluno.pes_id
                            INNER JOIN
                        acadgeral__aluno_curso ON acadgeral__aluno_curso.aluno_id = acadgeral__aluno.aluno_id
                            INNER JOIN
                        acadperiodo__aluno ON acadperiodo__aluno.alunocurso_id = acadgeral__aluno_curso.alunocurso_id
                            INNER JOIN
                        acadperiodo__aluno_disciplina ON acadperiodo__aluno_disciplina.alunoper_id = acadperiodo__aluno.alunoper_id
                            LEFT JOIN
                        acadperiodo__etapa_aluno USING (alunodisc_id)
                            LEFT JOIN
                        acadperiodo__aluno_disciplina_final ON acadperiodo__aluno_disciplina.alunodisc_id = acadperiodo__aluno_disciplina_final.alunodisc_id
                            LEFT JOIN
                        acadperiodo__docente_disciplina ON acadperiodo__aluno_disciplina.turma_id = acadperiodo__docente_disciplina.turma_id and acadperiodo__docente_disciplina.disc_id = acadperiodo__aluno_disciplina.disc_id
                            LEFT JOIN
                        acadperiodo__aluno_resumo ON acadperiodo__aluno_resumo.alunoper_id = acadperiodo__aluno_disciplina.alunoper_id
                            AND acadperiodo__aluno_resumo.disc_id = acadperiodo__aluno_disciplina.disc_id
                            where acadperiodo__docente_disciplina.docdisc_id = $docdiscId  GROUP BY alunocurso_id) alunosFinal
                    WHERE etapas >= {$configNota['notaFinal']} AND
                          etapas <= {$configNota['notaMin']}  AND
                          ( faltas * 100 / $discCHoraria) <= (100 - {$configNota['freqMin']})";

        return $this->executeQuery($query)->fetchAll();
    }
}