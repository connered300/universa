<?php

namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class AcadgeralAreaConhecimento
 * @package Matricula\Service
 */
class AcadgeralAreaConhecimento extends AbstractService
{
    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadgeralAreaConhecimento');
    }

    public function getDescricao($id)
    {
        if (!is_array($id)) {
            $id = array($id);
        }

        $id = implode($id, ',');

        $query = '
        SELECT group_concat(area_descricao SEPARATOR ", ") text
        FROM acadgeral__area_conhecimento
        WHERE area_id IN (' . $id . ')';

        $result = $this->executeQueryWithParam($query)->fetch();

        return $result['text'];
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function pesquisaForJson($params)
    {
        $sql        = 'SELECT * FROM acadgeral__area_conhecimento';
        $parameters = array();
        $where      = array();

        $areaDescricao = ($params['q']) ? $params['q'] : $params['query'];

        if ($areaDescricao) {
            $where[]                      = ' area_descricao LIKE :area_descricao';
            $parameters['area_descricao'] = $areaDescricao . '%';
        }

        if ($params['areaId']) {
            $where[] .= ' area_id <> :area_id';
            $parameters['area_id'] = $params['areaId'];
        }

        if ($where) {
            $sql .= " WHERE " . implode('AND', $where);
        }

        $sql .= " ORDER BY area_descricao";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceAcadgeralAreaConhecimento = new \Matricula\Service\AcadgeralAreaConhecimento($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['areaId']) {
                /** @var $objAcadgeralAreaConhecimento \Matricula\Entity\AcadgeralAreaConhecimento */
                $objAcadgeralAreaConhecimento = $this->getRepository()->find($arrDados['areaId']);

                if (!$objAcadgeralAreaConhecimento) {
                    $this->setLastError('Registro de área de conhecimento não existe!');

                    return false;
                }
            } else {
                $objAcadgeralAreaConhecimento = new \Matricula\Entity\AcadgeralAreaConhecimento();
            }

            $objAcadgeralAreaConhecimento->setAreaDescricao($arrDados['areaDescricao']);

            if ($arrDados['areaPai']) {
                /** @var $objAcadgeralAreaConhecimento \Matricula\Entity\AcadgeralAreaConhecimento */
                $objAcadgeralAreaConhecimentoPai = $serviceAcadgeralAreaConhecimento->getRepository()->find(
                    $arrDados['areaPai']
                );

                if (!$objAcadgeralAreaConhecimentoPai) {
                    $this->setLastError('Registro de área de conhecimento não existe!');

                    return false;
                }

                $objAcadgeralAreaConhecimento->setAreaPai($objAcadgeralAreaConhecimentoPai);
            } else {
                $objAcadgeralAreaConhecimento->setAreaPai(null);
            }

            $this->getEm()->persist($objAcadgeralAreaConhecimento);
            $this->getEm()->flush($objAcadgeralAreaConhecimento);

            $this->getEm()->commit();

            $arrDados['areaId'] = $objAcadgeralAreaConhecimento->getAreaId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de área de conhecimento!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['areaDescricao']) {
            $errors[] = 'Por favor preencha o campo "descrição"!';
        }

        if ($this->verificaSeAreaEstaDuplicada($arrParam['areaDescricao'], $arrParam['areaId'])) {
            $errors[] = "Já existe uma área cadastrada com esta mesma descrição!";
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function verificaSeAreaEstaDuplicada($areaDescricao, $areaId = false)
    {
        $sql        = 'SELECT count(*) AS qtd FROM acadgeral__area_conhecimento WHERE area_descricao LIKE :areaDescricao';
        $parameters = array('areaDescricao' => $areaDescricao);

        if ($areaId) {
            $sql .= ' AND area_id<>:areaId';
            $parameters['areaId'] = $areaId;
        }

        $result = $this->executeQueryWithParam($sql, $parameters)->fetch();

        return $result['qtd'] > 0;
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "
        SELECT a1.*, a2.area_descricao AS area_pai_descricao FROM acadgeral__area_conhecimento a1
        LEFT JOIN acadgeral__area_conhecimento a2 ON a2.area_id=a1.area_pai
        ";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $areaId
     * @return array
     */
    public function getArray($areaId)
    {
        /** @var $objAcadgeralAreaConhecimento \Matricula\Entity\AcadgeralAreaConhecimento */
        $objAcadgeralAreaConhecimento     = $this->getRepository()->find($areaId);
        $serviceAcadgeralAreaConhecimento = new \Matricula\Service\AcadgeralAreaConhecimento($this->getEm());

        try {
            $arrDados = $objAcadgeralAreaConhecimento->toArray();

            if ($arrDados['areaPai']) {
                /** @var $objAcadgeralAreaConhecimento \Matricula\Entity\AcadgeralAreaConhecimento */
                $objAcadgeralAreaConhecimento = $serviceAcadgeralAreaConhecimento->getRepository()->find(
                    $arrDados['areaPai']
                );

                if ($objAcadgeralAreaConhecimento) {
                    $arrDados['areaPai'] = array(
                        'id'   => $objAcadgeralAreaConhecimento->getAreaId(),
                        'text' => $objAcadgeralAreaConhecimento->getAreaDescricao()
                    );
                } else {
                    $arrDados['areaPai'] = null;
                }
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceAcadgeralAreaConhecimento = new \Matricula\Service\AcadgeralAreaConhecimento($this->getEm());

        if (is_array($arrDados['areaPai']) && !$arrDados['areaPai']['id']) {
            $arrDados['areaPai']['id']   = $arrDados['areaPai']['areaId'];
            $arrDados['areaPai']['text'] = $arrDados['areaPai']['areaDescricao'];
        } elseif ($arrDados['areaPai'] && is_string($arrDados['areaPai'])) {
            $arrDados['areaPai'] = $serviceAcadgeralAreaConhecimento->getArrSelect2(
                array('id' => $arrDados['areaPai'])
            );
        }

        return $arrDados;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('areaId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Matricula\Entity\AcadgeralAreaConhecimento */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getAreaId(),
                $params['value'] => $objEntity->getAreaDescricao()
            );
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['areaId']) {
            $this->setLastError('Para remover um registro de área de conhecimento é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objAcadgeralAreaConhecimento \Matricula\Entity\AcadgeralAreaConhecimento */
            $objAcadgeralAreaConhecimento = $this->getRepository()->find($param['areaId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAcadgeralAreaConhecimento);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de área de conhecimento.');

            return false;
        }

        return true;
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
    }
}
?>