<?php

namespace Matricula\Service;

use VersaSpine\Service\AbstractService;
use Doctrine\ORM\Query\Expr;

class AcadgeralDisciplinaEquivalencia extends \Matricula\Service\AcadgeralDisciplinaCurso{


    public function buscaEquivalencia($discId = null, $cursoId = null){
        $arrWhere  = [];
        $arrParams = [];

        if( $discId ){
            $arrParams = array("disc_id"=>$discId);
            $arrWhere[] = "dis.disc_id = :disc_id";
        }

        if($cursoId){
            $arrParams["curso_id"] = $cursoId;
            $arrWhere[] = " AND curso_id = :curso_id";
        }

        $where = $arrParams ? "WHERE ".implode(" AND ", $arrWhere) : "";

        $sql = "SELECT adc.*, adis.*
                   FROM acadgeral__disciplina_curso adc
                   INNER JOIN acad_curso ac using (curso_id)
                   INNER JOIN (
                        SELECT ade.discequiv_destino
                              FROM acadgeral__disciplina_equivalencia ade
                              INNER JOIN acadgeral__disciplina_curso ON discequiv_origem = disc_curso_id 
                              INNER JOIN acadgeral__disciplina dis using (disc_id)  
                              INNER JOIN acad_curso ac using (curso_id)
                              $where
                   ) destino ON (destino.discequiv_destino = adc.disc_curso_id)
                   INNER JOIN acadgeral__disciplina adis using (disc_id)        
                ";
            ;
        return $this->executeQueryWithParam($sql, $arrParams)->fetchAll();
    }

    public function removeEquivalencia($discId)
    {
        $this->exec("DELETE FROM acadgeral__disciplina_equivalencia WHERE discequiv_origem = {$discId} OR discequiv_destino = {$discId}");
    }

    public function verificaAlunosMatriculados($discId)
    {
        $ret = $this->executeQuery("
            SELECT
                *
            FROM
                acadgeral__disciplina disc
            INNER JOIN
                acadgeral__disciplina_equivalencia ON discequiv_destino = disc.disc_id OR discequiv_origem = disc.disc_id
            RIGHT JOIN
                acadperiodo__aluno_disciplina alunodisc ON alunodisc.disc_id = disc.disc_id
            WHERE
                disc.disc_id = {$discId}
            GROUP BY
                disc_nome
        ")->fetchAll();
        if($ret){
            return $ret[0];
        } else {
            return null;
        }
    }
}