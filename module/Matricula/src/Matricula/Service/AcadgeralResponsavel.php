<?php


namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

class AcadgeralResponsavel extends AbstractService
{
    const RESP_VINCULO_PAI                  = 'Pai';
    const RESP_VINCULO_MAE                  = 'Mãe';
    const RESP_VINCULO_AVO                  = 'Avo';
    const RESP_VINCULO_IRMAO                = 'Irmão';
    const RESP_VINCULO_RESPONSALVEL_LEGAL   = 'Responsalvel Legal';
    const RESP_VINCULO_OUTROS               = 'Outros';
    const RESP_NIVEL_LEGAL                  = 'Legal';
    const RESP_NIVEL_FINANCEIRO             = 'Financeiro';
    const RESP_NIVEL_ACADEMICO              = 'Acadêmico';
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    public static function getRespVinculo()
    {
        return array(
            self::RESP_VINCULO_PAI,
            self::RESP_VINCULO_MAE,
            self::RESP_VINCULO_AVO,
            self::RESP_VINCULO_IRMAO,
            self::RESP_VINCULO_RESPONSALVEL_LEGAL,
            self::RESP_VINCULO_OUTROS
        );
    }

    public static function getRespNivel()
    {
        return array(self::RESP_NIVEL_LEGAL, self::RESP_NIVEL_FINANCEIRO, self::RESP_NIVEL_ACADEMICO);
    }

    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadgeralResponsavel');
    }

    public function salvarMultiplos(array &$arrParam, \Matricula\Entity\AcadgeralAluno $objAcadgeralAluno)
    {
        $arrExcluir = array();

        try {
            $this->getEm()->beginTransaction();

            $arrResponsaveis   = json_decode($arrParam['responsaveis'], true);
            $arrResponsaveisDB = $this->getRepository()->findBy(['aluno' => $objAcadgeralAluno->getAlunoId()]);

            /* @var $objAcadgeralResponsavel \Matricula\Entity\AcadgeralResponsavel */
            foreach ($arrResponsaveisDB as $objAcadgeralResponsavel) {
                $encontrado = false;
                $respId     = $objAcadgeralResponsavel->getRespId();

                foreach ($arrResponsaveis as $arrResponsavel) {
                    if ($respId == $arrResponsavel['respId']) {
                        $encontrado = true;
                        break;
                    }
                }

                if (!$encontrado) {
                    $arrExcluir[$respId] = $objAcadgeralResponsavel;
                }
            }

            if ($arrExcluir) {
                foreach ($arrExcluir as $objAcadgeralResponsavel) {
                    $this->getEm()->remove($objAcadgeralResponsavel);
                    $this->getEm()->flush();
                }
            }

            foreach ($arrResponsaveis as $arrResponsavel) {
                $arrResponsavel['alunoId'] = $objAcadgeralAluno->getAlunoId();

                if (!$this->salvarResponsavel($arrResponsavel)) {
                    throw new \Exception($this->getLastError());
                }
            }

            $this->getEm()->commit();

            return true;
        } catch (\Exception $e) {
            $this->getEm()->rollback();
            $this->setLastError(
                'Não foi possível salvar o registro de responsáveis do aluno!' .
                '<br>' .
                $e->getMessage()
            );
        }

        return false;
    }

    public function salvarResponsavel(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $servicePessoa         = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceAcadgeralAluno = new \Matricula\Service\AcadgeralAluno($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['respId']) {
                $objAcadgeralResponsavel = $this->getRepository()->find($arrDados['respId']);

                if (!$objAcadgeralResponsavel) {
                    $this->setLastError('Registro de responsável de aluno não existe!');

                    return false;
                }
            } else {
                $objAcadgeralResponsavel = new \Matricula\Entity\AcadgeralResponsavel();
            }

            if ($arrDados['alunoId']) {
                /* @var $objAcadgeralAluno \Matricula\Entity\AcadgeralAluno */
                $objAcadgeralAluno = $serviceAcadgeralAluno->getRepository()->find($arrDados['alunoId']);

                if (!$objAcadgeralAluno) {
                    $this->setLastError('Registro de aluno não existe!');

                    return false;
                }

                $objAcadgeralResponsavel->setAluno($objAcadgeralAluno);
            }
            if ($arrDados['pesId']) {
                /* @var $objPessoa \Pessoa\Entity\Pessoa */
                $objPessoa = $servicePessoa->getRepository()->find($arrDados['pesId']);

                if (!$objPessoa) {
                    $this->setLastError('Registro de pessoa não existe!');

                    return false;
                }

                $objAcadgeralResponsavel->setPes($objPessoa);
            }

            $objAcadgeralResponsavel->setRespVinculo($arrDados['respVinculo']);
            $objAcadgeralResponsavel->setRespNivel($arrDados['respNivel']);

            $this->getEm()->persist($objAcadgeralResponsavel);
            $this->getEm()->flush($objAcadgeralResponsavel);

            $this->getEm()->commit();

            $arrDados['respId'] = $objAcadgeralResponsavel->getRespId();

            return true;
        } catch (\Exception $e) {
            $this->getEm()->rollback();
            $this->setLastError(
                'Não foi possível salvar o registro de responsável de aluno!' .
                '<br>' .
                $e->getMessage()
            );
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = [];

        if (!$arrParam['pesId']) {
            $errors[] = 'Por favor preencha o campo "pessoa"!';
        }

        if (!$arrParam['respVinculo']) {
            $errors[] = 'Por favor preencha o campo "vínculo"!';
        }

        if (!$arrParam['respNivel']) {
            $errors[] = 'Por favor preencha o campo "nível"!';
        }

        if (!$arrParam['alunoId']) {
            $errors[] = 'Por favor preencha o campo "aluno"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    public function validacaoMultipla($arrParam)
    {
        $errors = array();

        $arrResponsaveis = json_decode($arrParam['responsaveis'], true);

        if ($arrResponsaveis) {
            foreach ($arrResponsaveis as $arrPessoaResp) {
                if (!$this->valida($arrPessoaResp)) {
                    $errors[] = '<p>Dados do responsável: <br>' . $this->getLastError() . '</p>';
                }
            }
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    protected function pesquisaForJson($criterios)
    {
    }

    public function adicionar(array $dados)
    {
        $pessoa = null;

        if ($dados['pesCpf'] != "") {
            $serviceFisica = (new \Pessoa\Service\PessoaFisica($this->getEm()));
            $pessoa        = $serviceFisica->findOneBy(['pesCpf' => $dados['pesCpf']]);
            if (!$pessoa) {
                $pessoa = (new \Pessoa\Service\PessoaFisica($this->getEm()))->adicionar($dados);
            } else {
                $dados['pes']   = $pessoa->getPes()->getPesId();
                $dados['pesId'] = $pessoa->getPes()->getPesId();
                (new \Pessoa\Service\PessoaFisica($this->getEm()))->edita($dados);
                (new \Pessoa\Service\Pessoa($this->getEm()))->edita($dados);
            }
        } else {
            $serviceJuridica = (new \Pessoa\Service\PessoaJuridica($this->getEm()));
            $pessoa          = $serviceJuridica->findOneBy(['pesCnpj' => $dados['pesCnpj']]);
            if (!$pessoa) {
                $pessoa = (new \Pessoa\Service\PessoaJuridica($this->getEm()))->adicionar($dados);
            } else {
                $dados['pes']   = $pessoa->getPes()->getPesId();
                $dados['pesId'] = $pessoa->getPes()->getPesId();
                (new \Pessoa\Service\PessoaJuridica($this->getEm()))->edita($dados);
                (new \Pessoa\Service\Pessoa($this->getEm()))->edita($dados);
            }
        }
        $dados['pes'] = $pessoa->getPes()->getPesId();

        return parent::adicionar($dados);
    }

    public function buscaDadosResponsavel($responsavel = null)
    {
        $dados = array();
        if ($responsavel) {
            $responsavel = $this->getReference($responsavel)->toArray();

            $dadosPessoa = (new \Pessoa\Service\Pessoa($this->getEm()))->buscaDadosPessoa(
                $responsavel['pes'],
                'f',
                true,
                true
            );
            if (!$dadosPessoa) {
                $dadosPessoa               = (new \Pessoa\Service\Pessoa($this->getEm()))->buscaDadosPessoa(
                    $responsavel['pes'],
                    'j',
                    true,
                    true
                );
                $dadosPessoa['pesCnpjCpf'] = $dadosPessoa['pesCnpj'];
            } else {
                $dadosPessoa['pesCnpjCpf'] = $dadosPessoa['pesCpf'];
            }

            $dados = array_merge($responsavel, $dadosPessoa);
        }

        return $dados;
    }

    public function buscaResponsaveis($aluno = null)
    {
        $array = [];

        if ($aluno) {
            $responsaveis = $this->findBy(['aluno' => $aluno]);

            foreach ($responsaveis as $responsavel) {
                $array[] = $responsavel->getRespId();
            }

            return $array;
        }

        return false;
    }

    public function getArrayResponsaveis($alunoId)
    {
        $servicePessoa = new \Pessoa\Service\Pessoa($this->getEm());

        $arrResponsavelRetorno = [];
        $arrResponsavel        = $this->getRepository()->findBy(['aluno' => $alunoId]);

        /* @var $objResponsavel \Matricula\Entity\AcadgeralResponsavel */
        foreach ($arrResponsavel as $objResponsavel) {
            $arrDados       = $objResponsavel->toArray();
            $arrDadosPessoa = $servicePessoa->getArray($arrDados['pesId']);

            $arrResponsavelRetorno[] = array_merge($arrDados, $arrDadosPessoa);
        }

        return $arrResponsavelRetorno;
    }

    public function getArrSelect2RespNivel($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getRespNivel());
    }

    public function getArrSelect2RespVinculo($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getRespVinculo());
    }

    public function retornaAlunosPorResponsavel($pesId)
    {
        if (!$pesId) {
            $this->setLastError('É nessário informar o código da pessoa!');

            return false;
        }

        $sql = "
        SELECT group_concat(aa.pes_id) alunosPes FROM acadgeral__aluno aa
        LEFT JOIN acadgeral__responsavel ar USING(aluno_id)
        WHERE ar.pes_id=:pesId AND ar.resp_nivel = :nivel";

        $result = $this
            ->executeQueryWithParam($sql, ['pesId' => $pesId, 'nivel' => self::RESP_NIVEL_FINANCEIRO])
            ->fetch();

        return $result['alunosPes'];
    }
}