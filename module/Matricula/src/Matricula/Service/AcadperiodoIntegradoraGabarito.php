<?php


namespace Matricula\Service;

use VersaSpine\Service\AbstractService;
use Vestibular\ViewHelper\Matricular;

class AcadperiodoIntegradoraGabarito extends AbstractService{
    public function __construct(\Doctrine\ORM\EntityManager $em){
        parent::__construct($em, 'Matricula\Entity\AcadperiodoIntegradoraGabarito');
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {

    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {

    }

    public function adicionar( \Matricula\Entity\AcadperiodoIntegradoraGabarito $integradoraGabarito)
    {
        $this->begin();
        try {
            $this->getEm()->persist($integradoraGabarito);
            $this->getEm()->flush();
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
        }
        $this->commit();
        return $integradoraGabarito;
    }

    public function buscaDisciplinasDoCadernoPorOrdem(\Matricula\Entity\AcadperiodoIntegradoraCaderno $caderno = null)
    {
        if($caderno){
            $disciplinas = $this->buscasDisiciplinasAvaliacao($caderno);

            $sql = "
              SELECT
                disc_id,
                gabarito_ordem,
                gabarito_resposta,
                gabarito_valida
              FROM
                acadperiodo__integradora_gabarito
              WHERE
                integcaderno_id = {$caderno->getIntegcadernoId()}
              ORDER BY gabarito_ordem

            ";

            $disciplinasGabarito = $this->executeQuery($sql);
            if($disciplinasGabarito->rowCount() > 0){
                $disciplinasGabarito =  $disciplinasGabarito->fetchAll();

                for($i = 0; $i < sizeof($disciplinas); $i++){
                    $disciplinas[$i]['disc'] = $this->getReference($disciplinas[$i]['disc_id'], 'Matricula\Entity\AcadgeralDisciplina');
                    for($j = 0; $j < sizeof($disciplinasGabarito); $j++){
                        if($disciplinas[$i]['disc_id'] == $disciplinasGabarito[$j]['disc_id']){
                            $disciplinas[$i]['gabarito'][] = array(
                                'ordem' => $disciplinasGabarito[$j]['gabarito_ordem'],
                                'resposta' => $disciplinasGabarito[$j]['gabarito_resposta'],
                                'valida' =>  $disciplinasGabarito[$j]['gabarito_valida']
                            );

                        }
                    }
                }


                return $disciplinas;
            }
        }

        return null;
    }

    public function buscasDisiciplinasAvaliacao(\Matricula\Entity\AcadperiodoIntegradoraCaderno $caderno)
    {
        $sql = "
              SELECT
                disc_id
              FROM
                acadperiodo__integradora_gabarito
              WHERE
                integcaderno_id = {$caderno->getIntegcadernoId()}
              GROUP BY
                disc_id
              ORDER BY gabarito_ordem

            ";

        $disciplinas = $this->executeQuery($sql);
        if($disciplinas->rowCount() > 0){
            return $disciplinas->fetchAll();
        }
    }

    public function buscaGabaritoPorCaderno(\Matricula\Entity\AcadperiodoIntegradoraCaderno $caderno)
    {
        $sql = "
          SELECT
            CONCAT(disc_id*1, '-', SUBSTR(gabarito_ordem-1, -1)) as id,
            SUBSTR(gabarito_ordem-1, -1) as ordem,
            gabarito_resposta as resposta,
            disc_id*1 disc,
            IF(gabarito_valida = 'Sim', 0, 1) as gabarito_valida
          FROM
            acadperiodo__integradora_gabarito
          WHERE
            integcaderno_id = {$caderno->getIntegcadernoId()}
        ";

        $respostas = $this->executeQuery($sql);
        if($respostas->rowCount() > 0){
            $array = array();
            $repostasArray = $respostas->fetchAll();

            //tratar multiplas respostas aqui
            foreach ($repostasArray as $reposta) {
                if($array[$reposta['id']]){
                    $array[$reposta['id']]['resposta'] .= $reposta['resposta'];
                } else {
                    $array[$reposta['id']]['ordemGeral'] = $reposta['ordem'];
                    $array[$reposta['id']]['ordem'] = $reposta['ordem'];
                    $array[$reposta['id']]['disciplina'] = $reposta['disc'];
                    $array[$reposta['id']]['resposta'] = $reposta['resposta'];
                    $array[$reposta['id']]['invalida'] = $reposta['gabarito_valida'];
                }


            }
            $array = json_encode($array);


            return  $array;

        }
    }

}