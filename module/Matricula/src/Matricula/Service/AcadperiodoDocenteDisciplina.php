<?php

namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class AcadperiodoDocenteDisciplina
 * @package Matricula\Service
 */
class AcadperiodoDocenteDisciplina extends AbstractService
{
    /**
     * @var null
     */
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadperiodoDocenteDisciplina');
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {
    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {
    }

    /**
     * Listagem de docências no formato JSON
     * @param array      $dados
     * @param bool|false $is_json
     * @return array
     */
    public function pagination($dados, $is_json = false)
    {
        $dados          = array();
        $dados['count'] = $this
            ->executeQuery("SELECT COUNT(*) as row  FROM {$this->getMetadados()->table['name']}")
            ->fetch()['row'];

        $params    = (new \VersaSpine\ListGenerator\Annotations\ReaderQuerys($this->getEntity()))->getOrderBy();
        $joins     = (new \VersaSpine\ListGenerator\Annotations\ReaderQuerys($this->getEntity()))->getJoins();
        $campTable = (new \VersaSpine\ListGenerator\Annotations\ReaderQuerys($this->getEntity()))->getCampos();

        if (count($campTable) <= 0) {
            return array();
        }

        if (is_object($params)) {
            if (count($params->toArray()['params']) > 0) {
                $order = " ORDER BY ";
                foreach ($params->toArray()['params'] as $key => $value) {
                    $order .= " {$this->getMetadados()->table['name']}.$key $value, ";
                }
                $order = substr($order, 0, -2);
            }
        }

        if (!$page) {
            if ($is_json) {
                $limit = " LIMIT 10 OFFSET 0 ";
            } else {
                $limit = "";
            }
        } else {
            $offset = (($page - 1) * 10);
            $limit  = " LIMIT 10 OFFSET $offset ";
        }
        $sql = "SELECT ";

        foreach ($campTable as $key => $camp) {
            if (array_search($camp, array_keys($joins)) === false) {
                if ($this->getMetadados()->fieldMappings[$key]['type'] == 'date') {
                    $sql .= " DATE_FORMAT({$this->getMetadados()->table['name']}.$camp,'%d/%m/%Y') as $camp ";
                } else {
                    $sql .= " {$this->getMetadados()->table['name']}.$camp ";
                }
                $sql .= ",";
            }
        }

        $joinsSql = "";
        $labels   = "";

        if (is_array($joins)) {
            foreach ($joins as $chave => $join) {
                if ($join['join_type'] == "natural") {
                    $joinsSql .= " {$join['join_type']} JOIN {$join['table']} {$join['table']} ";
                    $labels .= "{$join['table']}.{$join['join_camp_dest_label']} as $chave, ";
                } else {
                    $campo = (isset($join['join_camp_orig'])) ? $join['join_camp_orig'] : $chave;
                    $joinsSql .= " {$join['join_type']} JOIN {$join['table']} {$join['table']} ON {$this->getMetadados()->table['name']}.$campo = {$join['table']}.{$join['join_camp_dest']} ";
                    $labels .= "{$join['table']}.{$join['join_camp_dest_label']} as $chave, ";
                }
            }
        }

        if (count($joins) == 0) {
            $sql = substr($sql, 0, -1);
        }

        $sql .= substr($labels, 0, -2);

        $joinsSql .= "INNER JOIN pessoa_fisica ON acadgeral__docente.pes_id = pessoa_fisica.pes_id INNER JOIN pessoa ON pessoa.pes_id = pessoa_fisica.pes_id";

        $sql .= " FROM {$this->getMetadados()->table['name']} {$this->getMetadados()->table['name']} ";
        $sql .= $joinsSql;
        $sql .= " $order $limit ";

        $sql = str_replace("acadgeral__docente.docente_id as docente_id", 'pessoa.pes_nome as docente_id', $sql);

        $dados['dados'] = $this->executeQuery($sql);

        if ($is_json) {
            $dados['dados'] = $this->paginationFormatJson($dados['dados']);
        }

        return $dados;
    }

    /**
     * Listagem de turmas para uso em datatables
     * @param array $arrayData
     * @return array
     */
    public function paginationAjax(array $arrayData)
    {
        $query = "
            SELECT
                TRIM(LEADING '0' FROM acadperiodo__turma.turma_id) AS turma_id,
                acadperiodo__turma.mat_cur_id AS matriz_id,
                acadperiodo__turma.per_id AS per_id,
                turma_nome AS turma,
                COUNT( DISTINCT acadperiodo__matriz_disciplina.disc_id) AS disciplinas,
                COUNT( DISTINCT acadperiodo__docente_disciplina.docdisc_id) AS professores
            FROM
              acadperiodo__turma
            LEFT JOIN
              acadperiodo__docente_disciplina ON acadperiodo__turma.turma_id = acadperiodo__docente_disciplina.turma_id
            INNER JOIN
            	acadperiodo__matriz_curricular ON acadperiodo__matriz_curricular.mat_cur_id = acadperiodo__turma.mat_cur_id
           	INNER JOIN
              	acadperiodo__matriz_disciplina ON  acadperiodo__matriz_curricular.mat_cur_id = acadperiodo__matriz_disciplina.mat_cur_id
            AND
              acadperiodo__matriz_disciplina.per_disc_periodo = acadperiodo__turma.turma_serie

            INNER JOIN
              acadperiodo__letivo ON acadperiodo__turma.per_id = acadperiodo__letivo.per_id
            WHERE acadperiodo__docente_disciplina.docdisc_data_fim IS NULL
           
        ";

        if ($arrayData['filter']['periodoId']) {
            $query .= " AND acadperiodo__letivo.per_id=" . $arrayData['filter']['periodoId'];
        } else {
            $query .= " AND acadperiodo__letivo.per_id = (SELECT max(per_id) from acadperiodo__letivo)";
        }

        $query .= '
         GROUP BY turma_id
         ORDER BY turma_id ASC
        ';

        $result = $this->paginationDataTablesAjax($query, $arrayData, null, false);

        return $result;
    }

    /**
     * Cadastra ou altera docência
     * @param array $data
     * @throws \Exception
     */
    public function save($data)
    {
        if ($data) {
            $repository = $this->getRepository();

            $this->begin();

            for ($i = 0; $i < sizeof($data['disciplinas']); $i++) {
                if ($data['docenteAtual'][$i]) {
                    $result = $repository->findOneBy(
                        [
                            'turma'          => $data['turma'],
                            'disc'           => $data['disciplinas'][$i],
                            'docente'        => $data['docenteAtual'][$i],
                            'docdiscDataFim' => null,
                        ]
                    );

                    if ($result) {
                        if ($data['docenteAtual'][$i] != $data['docenteNovo'][$i]) {
                            if ($data['docenteNovo'][$i]) {
                                $result->setDocente(
                                    $this->getRepository('Matricula\Entity\AcadgeralDocente')->findOneBy(
                                        ['docenteId' => $data['docenteNovo'][$i]]
                                    )
                                );
                            } else {
                                $result->setDocdiscDataFim(
                                    new \DateTime('now', new \DateTimeZone('America/Sao_Paulo'))
                                );
                            }

                            try {
                                $this->getEm()->persist($result);
                                $this->getEm()->flush();
                            } catch (\Exception $ex) {
                                throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
                            }
                        }
                    }
                } else {
                    if ($data['docenteNovo'][$i]) {
                        $docenteDisciplina = [
                            'turma'             => $this->getRepository('Matricula\Entity\AcadperiodoTurma')->findOneBy(
                                ['turmaId' => $data['turma']]
                            ),
                            'disc'              => $this->getRepository(
                                'Matricula\Entity\AcadgeralDisciplina'
                            )->findOneBy(
                                ['discId' => $data['disciplinas'][$i]]
                            ),
                            'docente'           => $this->getRepository('Matricula\Entity\AcadgeralDocente')->findOneBy(
                                ['docenteId' => $data['docenteNovo'][$i]]
                            ),
                            'docdiscDataInicio' => new \DateTime('now', new \DateTimeZone('America/Sao_Paulo')),
                        ];

                        $docenteDisciplina = new \Matricula\Entity\AcadperiodoDocenteDisciplina($docenteDisciplina);

                        try {
                            $this->getEm()->persist($docenteDisciplina);
                            $this->getEm()->flush();
                        } catch (\Exception $ex) {
                            throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
                        }

                        unset($docenteDisciplina);
                    }
                }
            }

            $this->commit();
        }
    }

    /**
     * Retorna listagem de notas de alunos em uma docência agrupado por etapas
     * @param integer $docdiscId
     * @param array   $situacoes
     * @return array
     */
    public function notasAlunosDocenciaEtapaArray($docdiscId, $situacoes = array())
    {
        $conditions = array();
        $param      = array('docdiscId' => $docdiscId);

        if ($situacoes) {
            $conditions[] = 'situacao_aluno_id in(' . implode(', ', $situacoes) . ')';
            $conditions[] = 'situacao_disciplina_id in(' . implode(', ', $situacoes) . ')';
        }

        $conditions = implode(' AND ', $conditions);

        if ($conditions) {
            $conditions = ' AND ' . $conditions;
        }

        $sql = '
          SELECT
            professor_id, professor_nome, turma_id, turma_nome,
            docdisc_id, disc_id, disc_nome,
            alunocurso_id, aluno_id, aluno_nome, alunodisc_id,
            situacao_aluno_id, situacao_aluno_descricao,
            situacao_disciplina_id, situacao_disciplina_descricao,
            IF(nota_etapa_01 IS NULL , "", nota_etapa_01) nota_etapa_01,
            IF(nota_etapa_02 IS NULL , "", nota_etapa_02) nota_etapa_02,
            IF(nota_etapa_03 IS NULL , "", nota_etapa_03) nota_etapa_03,
            metodoCalculoNotas,metodoCalculoNotasFinal,
            COALESCE (etapasPeriodo,1)  etapasPeriodo
        FROM (
            SELECT
                prof.pes_id professor_id, prof.pes_nome professor_nome,
                turma.turma_id, turma.turma_nome,
                docdisc.docdisc_id, disc.disc_id, disc.disc_nome,
                alunocur.alunocurso_id,
                pesaluno.pes_id aluno_id, pesaluno.pes_nome aluno_nome,
                situacaoGeral.situacao_id situacao_aluno_id, situacaoGeral.matsit_descricao situacao_aluno_descricao,
                situacaoDisc.situacao_id situacao_disciplina_id, situacaoDisc.matsit_descricao situacao_disciplina_descricao,
                alunodisc.alunodisc_id,
                acc.cursoconfig_metodo metodoCalculoNotas, acc.cursoconfig_metodo_final metodoCalculoNotasFinal,
                acadperiodo__letivo.per_etapas etapasPeriodo,
                (
                    SELECT alunoetapa_nota FROM acadperiodo__etapa_aluno
                    NATURAL JOIN acadperiodo__etapa_diario
                    NATURAL JOIN acadperiodo__etapas
                    WHERE etapa_ordem = 1 AND alunodisc.alunodisc_id = alunodisc_id AND alunoetapa_nota IS NOT NULL
                    ORDER BY alunodisc_sit_data DESC LIMIT 1
                ) nota_etapa_01,
                (
                    SELECT alunoetapa_nota FROM acadperiodo__etapa_aluno
                    NATURAL JOIN acadperiodo__etapa_diario
                    NATURAL JOIN acadperiodo__etapas
                    WHERE etapa_ordem = 2 AND alunodisc.alunodisc_id = alunodisc_id AND alunoetapa_nota IS NOT NULL
                    ORDER BY alunodisc_sit_data DESC LIMIT 1
                ) nota_etapa_02,
                (
                    SELECT alunoetapa_nota FROM acadperiodo__etapa_aluno
                    NATURAL JOIN acadperiodo__etapa_diario
                    NATURAL JOIN acadperiodo__etapas
                    WHERE etapa_ordem = 3 AND alunodisc.alunodisc_id = alunodisc_id AND alunoetapa_nota IS NOT NULL
                    ORDER BY alunodisc_sit_data DESC LIMIT 1
                ) nota_etapa_03
            FROM acadperiodo__docente_disciplina docdisc
                INNER JOIN acadperiodo__etapa_diario diario ON docdisc.docdisc_id=diario.docdisc_id
                INNER JOIN acadperiodo__turma turma ON turma.turma_id = docdisc.turma_id
                LEFT JOIN acadgeral__docente docente ON docente.docente_id=docdisc.docente_id
                LEFT JOIN pessoa prof ON prof.pes_id=docente.pes_id
                INNER JOIN acadperiodo__aluno_disciplina alunodisc ON alunodisc.turma_id = docdisc.turma_id AND alunodisc.disc_id = docdisc.disc_id
                LEFT JOIN acadperiodo__etapa_aluno ON alunodisc.alunodisc_id = acadperiodo__etapa_aluno.alunodisc_id
                INNER JOIN acadperiodo__aluno alunoper ON alunoper.alunoper_id = alunodisc.alunoper_id
                INNER JOIN acadgeral__aluno_curso alunocur ON alunocur.alunocurso_id = alunoper.alunocurso_id
                INNER JOIN acadgeral__aluno aluno ON aluno.aluno_id = alunocur.aluno_id
                INNER JOIN pessoa pesaluno ON pesaluno.pes_id = aluno.pes_id
                INNER JOIN acadgeral__situacao situacaoDisc ON situacaoDisc.situacao_id = alunodisc.situacao_id
                INNER JOIN acadgeral__disciplina disc ON disc.disc_id = docdisc.disc_id
                INNER JOIN acadgeral__situacao situacaoGeral ON situacaoGeral.situacao_id = alunoper.matsituacao_id
                INNER JOIN acadperiodo__letivo ON acadperiodo__letivo.per_id = turma.per_id
                INNER JOIN campus_curso cc ON cc.cursocampus_id=alunocur.cursocampus_id
                INNER JOIN acad_curso_config acc ON acc.curso_id=cc.curso_id
                GROUP BY alunodisc.alunodisc_id
        ) AS notas
        WHERE docdisc_id = :docdiscId
        ' . $conditions . '
        ORDER BY aluno_nome ASC';

        $stmt         = $this->executeQueryWithParam($sql, $param);
        $arrDocencias = $stmt->fetchAll();

        return $arrDocencias;
    }

    /**
     * Efetua somatório de todas as notas de alunos com situação ativa na disciplina,
     * lança notas e frequencia na tabela resumo e registra alunos que ficaram de final
     * @param integer $docdiscId
     * @return bool
     */
    public function efetuaFechamentoNotasParte1($docdiscId)
    {
        if (!$docdiscId) {
            $this->setLastError('Docência inválida!');

            return false;
        }

        $objDocenteDisciplina = $this->getRepository()->find($docdiscId);

        if (!$objDocenteDisciplina) {
            $this->setLastError('Docência inválida!');

            return false;
        }

        if ($objDocenteDisciplina->getDocdiscDataFechamento()) {
            $this->setLastError('Fechamento de notas já foi efetuado!');

            return false;
        }

        if ($objDocenteDisciplina->getDocdiscDataFechamentoFinal()) {
            $this->setLastError('Fechamento de notas de final já foi efetuado!');

            return false;
        }

        $serviceAlunoDisciplina      = new \Matricula\Service\AcadperiodoAlunoDisciplina($this->getEm());
        $serviceCursoConfig          = new \Matricula\Service\AcadCursoConfig($this->getEm());
        $serviceFrequencia           = new \Professor\Service\AcadperiodoFrequencia($this->getEm());
        $serviceEtapaAluno           = new \Professor\Service\AcadperiodoEtapaAluno($this->getEm());
        $serviceAlunoResumo          = new \Matricula\Service\AcadperiodoAlunoResumo($this->getEm());
        $serviceMatrizDisciplina     = new \Matricula\Service\AcadperiodoMatrizDisciplina($this->getEm());
        $serviceAlunoDisciplinaFinal = new \Matricula\Service\AcadperiodoAlunoDisciplinaFinal($this->getEm());

        $etapasParaMetodoMedia = 1;

        $arrObjAlunoDisciplina = $serviceAlunoDisciplina->getRepository()->findBy(
            array(
                'turma' => $objDocenteDisciplina->getTurma(),
                'disc'  => $objDocenteDisciplina->getDisc(),
            )
        );

        if (!$arrObjAlunoDisciplina) {
            $this->setLastError('Sem alunos vinculados a docência!');

            return false;
        }

        $arrSituacoesAtividade = \Matricula\Service\AcadgeralSituacao::situacoesAtividade();

        /** @var \Matricula\Entity\AcadCursoConfig $objCursoConfig */
        $objCursoConfig = $serviceCursoConfig->retornaObjCursoConfig(
            $objDocenteDisciplina->getTurma()->getCursocampus()->getCurso()->getCursoId()
        );

        if (!$objCursoConfig) {
            $this->setLastError("Não foi possível localizar a configuração para o curso!");

            return false;
        }

        try {
            $this->begin();
            /** @var \Matricula\Entity\AcadperiodoMatrizDisciplina $objMatrizDisciplina */
            $objMatrizDisciplina = $serviceMatrizDisciplina->getRepository()->findOneBy(
                [
                    'disc'   => $objDocenteDisciplina->getDisc()->getDiscId(),
                    'matCur' => $objDocenteDisciplina->getTurma()->getMatCur()->getMatCurId()
                ]
            );

            $cargaHoraria = (
                $objMatrizDisciplina->getPerDiscChpratica() + $objMatrizDisciplina->getPerDiscChteorica()
            );

            /* @var $objAlunoDisciplina \Matricula\Entity\AcadperiodoAlunoDisciplina */
            foreach ($arrObjAlunoDisciplina as $objAlunoDisciplina) {
                $objAlunoper = $objAlunoDisciplina->getAlunoper();

                if (
                    !in_array($objAlunoDisciplina->getSituacao()->getSituacaoId(), $arrSituacoesAtividade) ||
                    !in_array($objAlunoper->getMatsituacao()->getSituacaoId(), $arrSituacoesAtividade)
                ) {
                    continue;
                }

                $arrObjEtapaAluno = $serviceEtapaAluno->getRepository()->findBy(
                    ['alunodisc' => $objAlunoDisciplina->getAlunodiscId()]
                );

                $arrFrequencia = $serviceFrequencia->buscaFaltasDisciplina($objAlunoDisciplina->getAlunodiscId(), true);
                $faltas        = (int)$arrFrequencia['quant'];
                $porcentagemCh = 100 - (($faltas * 100) / $cargaHoraria);

                if ($arrObjEtapaAluno) {
                    $notaGeral = 0;

                    /* @var $objEtapaAluno \Professor\Entity\AcadperiodoEtapaAluno */
                    foreach ($arrObjEtapaAluno as $objEtapaAluno) {
                        $notaGeral += $objEtapaAluno->getAlunoetapaNota();
                    }

                    //TODO: CRIAR REGRA PARA ARREDONDAMENTO DE NOTA
                    if ($notaGeral == 69) {
                        $notaGeral = 70;
                    }

                    $objAlunocurso           = $objAlunoper->getAlunocurso();
                    $objAlunoDisciplinaFinal = $serviceAlunoDisciplinaFinal->getRepository()->findOneBy(
                        ['alunodisc' => $objAlunoDisciplina->getAlunodiscId()]
                    );

                    $metodoCalculoNotas = $objCursoConfig->getCursoconfigMetodo();

                    if ($metodoCalculoNotas == $serviceCursoConfig::METODO_DESEMPENHO_CALCULO_POR_MEDIA) {
                        $etapasParaMetodoMedia = $objAlunoDisciplina->getTurma()->getPer()->getPerEtapas();
                        $notaGeral = bcdiv($notaGeral,$etapasParaMetodoMedia,2);
                    }

                    if ($porcentagemCh < floatval($objCursoConfig->getCursoconfigFreqMin())) {
                        $situacao = 'REPROVADO';

                        if ($objAlunoDisciplinaFinal) {
                            $this->getEm()->remove($objAlunoDisciplinaFinal);
                        }
                    } elseif ($notaGeral >= $objCursoConfig->getCursoconfigNotaMin()) {
                        $situacao = 'APROVADO';

                        if ($objAlunoDisciplinaFinal) {
                            $this->getEm()->remove($objAlunoDisciplinaFinal);
                        }
                    } elseif (
                        $notaGeral >= $objCursoConfig->getCursoconfigNotaFinal() &&
                        $notaGeral < $objCursoConfig->getCursoconfigNotaMin()
                    ) {
                        if (!$objAlunoDisciplinaFinal) {
                            $objAlunoDisciplinaFinal = new \Matricula\Entity\AcadperiodoAlunoDisciplinaFinal();
                            $objAlunoDisciplinaFinal->setAlunodisc($objAlunoDisciplina);
                        }

                        $situacao = 'REPROVADO';

                        if (!$objAlunoDisciplinaFinal->getAlunofinalNota()) {
                            $objAlunoDisciplinaFinal->setAlunofinalNota(null);
                        }

                        $this->getEm()->persist($objAlunoDisciplinaFinal);
                        $this->getEm()->flush($objAlunoDisciplinaFinal);
                    } else {
                        $situacao = 'REPROVADO';

                        if ($objAlunoDisciplinaFinal) {
                            $this->getEm()->remove($objAlunoDisciplinaFinal);
                        }
                    }

                    $objAlunoResumo = $serviceAlunoResumo->getRepository()->findOneBy(
                        [
                            'alunocursoId'     => $objAlunocurso->getAlunocursoId(),
                            'discId'           => $objDocenteDisciplina->getDisc()->getDiscId(),
                            'resalunoSemestre' => $objDocenteDisciplina->getTurma()->getPer()->getPerSemestre(),
                            'resalunoAno'      => $objDocenteDisciplina->getTurma()->getPer()->getPerAno()
                        ]
                    );

                    if (!$objAlunoResumo) {
                        $objAlunoResumo = new \Matricula\Entity\AcadperiodoAlunoResumo();
                        $objAlunoResumo->setResalunoDataCriacao(new \DateTime('now'));
                    }

                    $objAlunoResumo
                        ->setAlunocursoId($objAlunocurso->getAlunocursoId())
                        ->setDiscId($objAlunoDisciplina->getDisc()->getDiscId())
                        ->setResalunoSerie($objAlunoper->getTurma()->getTurmaSerie())
                        ->setResalunoSituacao($situacao)
                        ->setResalunoNota($notaGeral)
                        ->setResalunoFaltas($faltas)
                        ->setResalunoDataAlteracao(new \DateTime('now'))
                        ->setResalunoCargaHoraria($objMatrizDisciplina->getPerDiscChteorica())
                        ->setResalunoAno($objDocenteDisciplina->getTurma()->getPer()->getPerAno())
                        ->setResalunoSemestre($objDocenteDisciplina->getTurma()->getPer()->getPerSemestre())
                        ->setAlunoper($objAlunoper);

                    $this->getEm()->persist($objAlunoResumo);
                    $this->getEm()->flush($objAlunoResumo);
                } else {
                    //professor não lançou nota para um aluno!
                    $this->setLastError('Existem alunos com lançamentos pendentes!');

                    return false;
                }
            }

            $objDocenteDisciplina->setDocdiscDataFechamento(new \DateTime('now'));
            $this->getEm()->flush($objDocenteDisciplina);

            $this->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Aconteceu um erro ao finalizar lançamentos!');

            return false;
        }

        return true;
    }

    /**
     * Efetua atualização dos registros de tabela resumo de alunos que ficaram de final
     * @param integer $docdiscId
     * @return bool
     */
    public function efetuaFechamentoNotasParte2($docdiscId)
    {
        if (!$docdiscId) {
            $this->setLastError('Docência inválida!');

            return false;
        }

        $objDocenteDisciplina = $this->getRepository()->find($docdiscId);

        if (!$objDocenteDisciplina) {
            $this->setLastError('Docência inválida!');

            return false;
        }

        if (!$objDocenteDisciplina->getDocdiscDataFechamento()) {
            $this->setLastError('Fechamento de notas ainda não foi efetuado!');

            return false;
        }

        if ($objDocenteDisciplina->getDocdiscDataFechamentoFinal()) {
            $this->setLastError('Fechamento de notas finais já foi efetuado!');

            return false;
        }

        $serviceResumo          = new \Matricula\Service\AcadperiodoAlunoResumo($this->getEm());
        $serviceDisciplinaFinal = new \Matricula\Service\AcadperiodoAlunoDisciplinaFinal($this->getEm());
        $serviceEtapaAluno           = new \Professor\Service\AcadperiodoEtapaAluno($this->getEm());
        $serviceCursoConfig     = new \Matricula\Service\AcadCursoConfig($this->getEm());
        $alunosNotasFinais      = $serviceDisciplinaFinal->buscaNotasFinalAlunos($docdiscId);

        $arrErros = ['notaEtapa' => 0, 'notaNula' => 0, 'notaAlta' => 0];

        try {
            $this->getEm()->beginTransaction();

            /** @var \Matricula\Entity\AcadCursoConfig $objCursoConfig */
            $objCursoConfig = $serviceCursoConfig->getRepository()->findOneBy(
                array('curso' => $objDocenteDisciplina->getTurma()->getCursocampus()->getCurso())
            );

            $notaFracionada =  (integer)$objCursoConfig->getCursoconfigNotaFracionada();
            $metodo         =   $objCursoConfig->getCursoconfigMetodo() === 'Media';

            foreach ($alunosNotasFinais as $alunoNotaFinal) {
                $alunofinalId = $alunoNotaFinal['alunofinal_id'];
                /* @var $objAlunoFinal \Matricula\Entity\AcadperiodoAlunoDisciplinaFinal */
                $objAlunoFinal      = $serviceDisciplinaFinal->getRepository()->find($alunofinalId);
                $objAlunoDisciplina = $objAlunoFinal->getAlunodisc();
                $objAlunoResumo     = $serviceResumo->getRepository()->findOneBy(
                    [
                        'alunocursoId'     => $objAlunoDisciplina->getAlunoper()->getAlunocurso()->getAlunocursoId(),
                        'discId'           => $objDocenteDisciplina->getDisc()->getDiscId(),
                        'resalunoSemestre' => $objDocenteDisciplina->getTurma()->getPer()->getPerSemestre(),
                        'resalunoAno'      => $objDocenteDisciplina->getTurma()->getPer()->getPerAno()
                    ]
                );

                $notaFinal  = $objAlunoFinal->getAlunofinalNota();
                $notaEtapas = $objAlunoResumo->getResalunoNota();

                $arrObjEtapaAluno = $serviceEtapaAluno->getRepository()->findBy(
                    ['alunodisc' => $objAlunoDisciplina->getAlunodiscId()]
                );

                if ($arrObjEtapaAluno) {
                    $notaEtapas = 0;

                    /* @var $objEtapaAluno \Professor\Entity\AcadperiodoEtapaAluno */
                    foreach ($arrObjEtapaAluno as $objEtapaAluno) {
                        $notaEtapas += $objEtapaAluno->getAlunoetapaNota();
                    }
                    if($metodo){
                        $notaEtapas = bcdiv($notaEtapas,$objDocenteDisciplina->getTurma()->getPer()->getPerEtapas(),1);
                    }
                }


                if ((string)$notaFinal == "") {
                    $arrErros['notaNula']++;
                    continue;
                } elseif ($notaFinal > $objCursoConfig->getCursoconfigNotaMax()) {
                    $arrErros['notaAlta']++;
                    continue;
                }

                //TODO: Criar configuração para arredondamento

                if($notaFracionada>0){
                    $notaFechamento = bcdiv(($notaFinal + $notaEtapas),2, $notaFracionada);

                }else{
                    $notaFechamento = ceil(($notaFinal + $notaEtapas) / 2);
                }

                if ($notaFechamento >= $objCursoConfig->getCursoconfigMediaFinalMin()) {
                    $situacao = 'APROVADO';
                } else {
                    $situacao = 'REPROVADO';
                }

                $objAlunoResumo
                    ->setResalunoSituacao($situacao)
                    ->setResalunoNota($notaFechamento)
                    ->setResalunoDataAlteracao(new \DateTime('now'));

                $this->getEm()->persist($objAlunoResumo);
                $this->getEm()->flush($objAlunoResumo);
            }

            if ($arrErros['notaAlta'] == 0 && $arrErros['notaNula'] == 0) {
                $objDocenteDisciplina->setDocdiscDataFechamentoFinal(new \DateTime('now'));
                $this->getEm()->flush($objDocenteDisciplina);
            }

            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Aconteceu um erro ao finalizar lançamentos de notas finais!');

            return false;
        }

        $msg = '';

        if ($arrErros['notaAlta'] > 0) {
            $msg .= 'Existe(m) ' . $arrErros['notaAlta'] . ' aluno(s) com nota maior que o permitido.';
        }

        if ($arrErros['notaNula'] > 0) {
            $msg .= 'Existe(m) ' . $arrErros['notaNula'] . ' aluno(s) com nota vazia.';
        }

        $this->setLastError($msg);

        return true;
    }

    /**
     * Retorna informações de docência e número de notas pendêntes para lançamento
     * @param integer $docdiscId
     * @return array
     */
    public function buscaPendencias($docdiscId)
    {
        $situacoes = \Matricula\Service\AcadgeralSituacao::situacoesAtividade();
        $param     = array('docdiscId' => $docdiscId);
        $sql       = '
        SELECT
            docdisc.*, COALESCE(COUNT(DISTINCT alunodisc.alunodisc_id), 0) AS notas_pendentes
        FROM
            acadperiodo__docente_disciplina docdisc
                LEFT JOIN
            acadperiodo__etapa_diario diario ON docdisc.docdisc_id = diario.docdisc_id
                LEFT JOIN
            acadperiodo__aluno_disciplina alunodisc ON alunodisc.disc_id = docdisc.disc_id
                LEFT JOIN
            acadperiodo__aluno periodoaluno ON periodoaluno.alunoper_id = alunodisc.alunoper_id
                AND alunodisc.turma_id = docdisc.turma_id
                LEFT JOIN
            acadperiodo__etapa_aluno etapaaluno ON etapaaluno.alunodisc_id = alunodisc.alunodisc_id
                AND diario.diario_id = etapaaluno.diario_id
        WHERE
            docdisc.docdisc_id = :docdiscId
                AND situacao_id IN(' . implode(', ', $situacoes) . ')
                AND matsituacao_id IN(' . implode(', ', $situacoes) . ')
                AND etapaaluno.alunoetapa_nota IS NULL
    ';

        $stmt         = $this->executeQueryWithParam($sql, $param);
        $arrDocencias = $stmt->fetchAll();

        return isset($arrDocencias[0]) ? $arrDocencias[0] : array();
    }

    /**
     * @param integer $docdiscId
     * @return array
     */
    public function retornaInformacoesLancamentoNotas($docdiscId)
    {
        $arrInformacoesLancamento = array(
            //Informações da docência
            'docencia'    => array(),
            //Configurações do curso
            'cursoConfig' => array(),
            //Diários da disciplina
            'etapaDiario' => array(),
            //Alunos ativos na docência:array(nome,situações de matricula,etapas=>array(alunoetapaId, nota))
            'alunos'      => array()
        );

        if (!$docdiscId) {
            $this->setLastError('Docência inválida!');

            return false;
        }

        $etapasMetodoMedia = 1;

        /* @var $objDocenteDisciplina \Matricula\Entity\AcadperiodoDocenteDisciplina */
        $objDocenteDisciplina = $this->getRepository()->find($docdiscId);

        if (!$objDocenteDisciplina) {
            $this->setLastError('Docência inválida!');

            return false;
        }

        $serviceEtapaDiario = new \Professor\Service\AcadperiodoEtapaDiario($this->getEm());
        $serviceEtapaAluno  = new \Professor\Service\AcadperiodoEtapaAluno($this->getEm());
        $serviceCursoConfig = new \Matricula\Service\AcadCursoConfig($this->getEm());

        /* @var $objCursoConfig \Matricula\Entity\AcadCursoConfig */
        $objCursoConfig = $serviceCursoConfig->getRepository()->findOneBy(
            array('curso' => $objDocenteDisciplina->getTurma()->getCursocampus()->getCurso())
        );

        if ($objCursoConfig->getCursoconfigMetodo() == $serviceCursoConfig::METODO_FINAL_POR_MEDIA) {
            $etapasMetodoMedia = $objDocenteDisciplina->getTurma()->getPer()->getPerEtapas();
        }

        $arrInformacoesLancamento['cursoConfig'] = array(
            //Frequencia mínima
            'freqMin'       => $objCursoConfig->getCursoconfigFreqMin(),
            //Nota máxima no período
            'notaMax'       => $objCursoConfig->getCursoconfigNotaMax(),
            //Nota mínima para aprovação
            'notaMin'       => $objCursoConfig->getCursoconfigNotaMin(),
            //Nota mínima para o aluno ficar de final
            'notaFinal'     => $objCursoConfig->getCursoconfigNotaFinal(),
            //Valor da mínimo da média da nota do exame final e notas da etapa para o aluno ser aprovado
            'mediaFinalMin' => $objCursoConfig->getCursoconfigMediaFinalMin()
        );

        $arrInformacoesLancamento['docencia'] = array(
            'id'                  => $objDocenteDisciplina->getDocdiscId(),
            'nome'                => $objDocenteDisciplina->getDisc()->getDiscNome(),
            'sigla'               => $objDocenteDisciplina->getDisc()->getDiscSigla(),
            'dataInicio'          => $objDocenteDisciplina->getDocdiscDataInicio(),
            'dataFim'             => $objDocenteDisciplina->getDocdiscDataFim(),
            'dataFechamento'      => $objDocenteDisciplina->getDocdiscDataFechamento(),
            'dataFechamentoFinal' => $objDocenteDisciplina->getDocdiscDataFechamentoFinal(),
            'turmaId'             => $objDocenteDisciplina->getTurma()->getTurmaId(),
            'turma'               => $objDocenteDisciplina->getTurma()->getTurmaNome(),
        );

        $arrEtapaDiario = $serviceEtapaDiario->registraEtapaDiario($objDocenteDisciplina);

        $arrInformacoesEtapas = array();
        $arrInformacoesAlunos = array();

        /* @var $objEtapaDiario \Professor\Entity\AcadperiodoEtapaDiario */
        foreach ($arrEtapaDiario as $etapaOrdem => $objEtapaDiario) {
            /* @var $objEtapaDiarioExtensao \Professor\Entity\AcadperiodoEtapaDiarioExtensao */
            $objEtapaDiarioExtensao = $this
                ->getEm()
                ->getRepository('Professor\Entity\AcadperiodoEtapaDiarioExtensao')
                ->findOneBy(['diario' => $objEtapaDiario->getDiarioId()], ['etapadiarioExtensaoId' => 'DESC']);

            $extensaoData = '';

            if ($objEtapaDiarioExtensao) {
                $extensaoData = $objEtapaDiarioExtensao->getEtapadiarioExtensaoData()->format('Y-m-d');
            }

            $porcentagem     = $objEtapaDiario->getEtapa()->getEtapaPercentagem();
            $valorMaximoNota = ($porcentagem * $objCursoConfig->getCursoconfigNotaMax()) / 100;

            $arrInformacoesEtapas[$etapaOrdem] = array(
                'diarioId' => $objEtapaDiario->getDiarioId(),
                'etapa'    => array(
                    'id'           => $objEtapaDiario->getEtapa()->getEtapaId(),
                    'ordem'        => $objEtapaDiario->getEtapa()->getEtapaOrdem(),
                    'dataInicio'   => $objEtapaDiario->getEtapa()->getEtapaDataInicio()->format('Y-m-d'),
                    'dataFim'      => $objEtapaDiario->getEtapa()->getEtapaDataFim()->format('Y-m-d'),
                    'extensaoData' => $extensaoData,
                    'descricao'    => $objEtapaDiario->getEtapa()->getEtapaDescricao(),
                    'porcentagem'  => $porcentagem,
                    'notaMaxima'   => $valorMaximoNota,
                )
            );

            $arrEtapaAluno = $serviceEtapaAluno->registraAlunosEtapaDiario($objEtapaDiario);

            /* @var $objEtapaAluno \Professor\Entity\AcadperiodoEtapaAluno */
            foreach ($arrEtapaAluno as $alunodiscId => $objEtapaAluno) {
                $objAlunoDisciplina = $objEtapaAluno->getAlunodisc();
                $objAlunoPeriodo    = $objAlunoDisciplina->getAlunoper();
                $objAluno           = $objAlunoPeriodo->getAlunocurso()->getAluno()->getPes()->getPes();

                if (!isset($arrInformacoesAlunos[$alunodiscId])) {
                    $arrInformacoesAlunos[$alunodiscId] = array(
                        'nome'                => $objAluno->getPesNome(),
                        'situacaoMatricula'   => $objAlunoPeriodo->getMatsituacao()->getMatsitDescricao(),
                        'situacaoMatriculaId' => $objAlunoPeriodo->getMatsituacao()->getSituacaoId(),
                        'situacaoDocencia'    => $objAlunoDisciplina->getSituacao()->getMatsitDescricao(),
                        'situacaoDocenciaId'  => $objAlunoDisciplina->getSituacao()->getSituacaoId(),
                        'etapas'              => array()
                    );
                }

                $arrInformacoesAlunos[$alunodiscId]['etapas'][$etapaOrdem] = array(
                    'id'   => $objEtapaAluno->getAlunoetapaId(),
                    'nota' => $objEtapaAluno->getAlunoetapaNota(),
                );
            }
        }

        $arrInformacoesLancamento['etapaDiario'] = $arrInformacoesEtapas;
        $arrInformacoesLancamento['alunos']      = $arrInformacoesAlunos;

        return $arrInformacoesLancamento;
    }
}