<?php
namespace Matricula\Service;

use Doctrine\Common\Collections\ArrayCollection;
use VersaSpine\Service\AbstractService;

/**
 * Class AcadperiodoLetivo
 * @package Matricula\Service
 */
class AcadperiodoLetivo extends AbstractService
{
    const PER_SEMESTRE_1 = '1';
    const PER_SEMESTRE_2 = '2';
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2PerSemestre($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getPerSemestre());
    }

    /**
     * @return array
     */
    public static function getPerSemestre()
    {
        return array(self::PER_SEMESTRE_1, self::PER_SEMESTRE_2);
    }

    /**
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadperiodoLetivo');
    }

    /**
     * @return \Matricula\Entity\AcadperiodoLetivo|null
     */
    public function buscaPeriodoCorrente($todosAtivos = false, $retornarId = false)
    {
        $retorno = [];

        /* @var $objPeriodo \Matricula\Entity\AcadperiodoLetivo */
        if ($objPeriodo = $this->buscaPeriodoAtual($todosAtivos)) {
            $retorno = $objPeriodo;
        } elseif ($objPeriodo = $this->buscaUltimoPeriodoFinalizado()) {
            $retorno = $todosAtivos ? [$objPeriodo] : $objPeriodo;
        } elseif ($objPeriodo = $this->buscaUltimoPeriodo()) {
            $retorno = $todosAtivos ? [$objPeriodo] : $objPeriodo;
        }

        if ($retorno && $retornarId) {
            $periodo = '';
            $retorno = is_array($retorno) ? $retorno : [$retorno];

            /** @var \Matricula\Entity\AcadperiodoLetivo $valor */
            foreach ($retorno as $valor) {
                $periodo = $periodo ? $periodo . ',' . $valor->getPerId() : $valor->getPerId();
            }

            $retorno = $periodo;
        }

        return $retorno ? $retorno : null;
    }

    public function getArrSelect2PeriodoCorrente()
    {
        $periodoLetivo = $this->buscaPeriodoCorrente();

        $arrSelect2 = array();

        if ($periodoLetivo) {
            $arrSelect2 = array(
                'id'   => $periodoLetivo->getPerId(),
                'text' => $periodoLetivo->getPerNome()
            );
        }

        return $arrSelect2;
    }

    /**
     * @return \Matricula\Entity\AcadperiodoLetivo|null
     */
    public function buscaPeriodoAtual($todosAtivos = false)
    {
        $query = '
        SELECT per_id FROM acadperiodo__letivo
        WHERE date(now()) BETWEEN per_data_inicio AND per_data_fim
        ORDER BY per_data_inicio';

        if (!$todosAtivos) {
            $query .= ' LIMIT 1';
        }

        $arrPeriodos = $this->executeQuery($query)->fetchAll();
        $perId       = [];

        foreach ($arrPeriodos as $arrPeriodo) {
            $perId[] = $arrPeriodo['per_id'];
        }

        if ($arrPeriodos) {
            /* @var $objPeriodos \Matricula\Entity\AcadperiodoLetivo */
            $objPeriodos = $this->getRepository()->findBy(['perId' => $perId]);

            if ($objPeriodos) {
                return $todosAtivos ? $objPeriodos : $objPeriodos[0];
            }
        }

        return null;
    }

    /**
     * @return \Matricula\Entity\AcadperiodoLetivo|array|null
     */
    public function buscaPeriodoAtualOuComMariculaEmAberto($vigencia = false)
    {
        $addQuery = '';

        if (!$vigencia) {
            $addQuery = " OR date(now()) BETWEEN per_matricula_inicio AND per_matricula_fim";
        }

        $query = "
        SELECT group_concat(per_id) as per_id FROM acadperiodo__letivo
        WHERE(
          date(now()) BETWEEN per_data_inicio AND per_data_fim 
          {$addQuery}
        )
        ORDER BY per_data_inicio,per_matricula_inicio";

        $perId = $this->executeQuery($query)->fetch();

        return $perId['per_id'];
    }

    /**
     * @return \Matricula\Entity\AcadperiodoLetivo|null
     */
    public function buscaUltimoPeriodo($param = array(), $obj = true)
    {
        $query = 'SELECT per_id, per_nome FROM acadperiodo__letivo';

        if ($param['aberto']) {
            $query .= ' WHERE per_data_finalizacao is null';
        }

        $query .= ' ORDER BY per_id DESC LIMIT 1';
        $arrPeriodo = $this->executeQuery($query)->fetch();

        if ($obj) {
            if ($arrPeriodo) {
                /* @var $objPeriodo \Matricula\Entity\AcadperiodoLetivo */
                $objPeriodo = $this->getRepository('\Matricula\Entity\AcadperiodoLetivo')->find($arrPeriodo['per_id']);

                if ($objPeriodo) {
                    return $objPeriodo;
                }
            }
        }

        return $arrPeriodo;
    }

    /**
     * @return \Matricula\Entity\AcadperiodoLetivo|null
     */
    public function buscaProximoPeriodo()
    {
        $query      = '
        SELECT per_id FROM acadperiodo__letivo
        ORDER BY per_id ASC
        LIMIT 1';
        $arrPeriodo = $this->executeQuery($query)->fetch();

        if ($arrPeriodo) {
            /* @var $objPeriodo \Matricula\Entity\AcadperiodoLetivo */
            $objPeriodo = $this->getRepository()->find($arrPeriodo['per_id']);

            if ($objPeriodo) {
                return $objPeriodo;
            }
        }

        return null;
    }

    /**
     * @return \Matricula\Entity\AcadperiodoLetivo|null
     */
    public function buscaUltimoPeriodoFinalizado()
    {
        $query      = '
        SELECT per_id FROM acadperiodo__letivo
        WHERE date(now()) > date(per_data_fim)
        ORDER BY per_id DESC
        LIMIT 1';
        $arrPeriodo = $this->executeQuery($query)->fetch();

        if ($arrPeriodo) {
            /* @var $objPeriodo \Matricula\Entity\AcadperiodoLetivo */
            $objPeriodo = $this->getRepository()->find($arrPeriodo['per_id']);

            if ($objPeriodo) {
                return $objPeriodo;
            }
        }

        return null;
    }

    /**
     * @param $inicioPeriodo
     * @param $fimPeriodo
     * @return array
     */
    public function buscaMesesPeriodo($inicioPeriodo, $fimPeriodo)
    {
        $inicioPeriodo = (is_object($inicioPeriodo) ? $inicioPeriodo : new \DateTime($inicioPeriodo));
        $fimPeriodo    = (is_object($fimPeriodo) ? $fimPeriodo : new \DateTime($fimPeriodo));
        $mesInicio     = (int)$inicioPeriodo->format("m");
        $mesFim        = (int)$fimPeriodo->format("m");
        $meses         = array();

        $mes = [
            '1'  => 'Janeiro',
            '2'  => 'Fevereiro',
            '3'  => 'Marco',
            '4'  => 'Abril',
            '5'  => 'Maio',
            '6'  => 'Junho',
            '7'  => 'Julho',
            '8'  => 'Agosto',
            '9'  => 'Setembro',
            '10' => 'Outubro',
            '11' => 'Novembro',
            '12' => 'Dezembro',
        ];

        for ($i = $mesInicio; $i <= $mesFim; $i++) {
            $meses [] = [
                'mes'        => $mes[$i],
                'referencia' => $i,
            ];
        }

        return $meses;
    }

    /**
     * @param $periodo
     * @return mixed
     */
    public function buscaTurnosPeriodo($periodo)
    {
        $query = "
        SELECT turma_turno FROM acadperiodo__turma
        WHERE per_id = {$periodo} GROUP BY turma_turno";

        return $this->executeQuery($query)->fetchAll();
    }

    /**
     * @param $turno
     * @param $periodo
     * @return mixed
     */
    public function buscaTurmasPeriodoTurno($turno, $periodo)
    {
        $andTurno = '';

        if ($turno !== 'Todos') {
            $andTurno = " AND turma_turno = '{$turno}'";
        }

        $query = "
        SELECT turma_nome FROM acadperiodo__turma
        WHERE per_id = {$periodo}.{$andTurno}";

        return $this->executeQuery($query)->fetchAll();
    }

    /*
     * RETORNA DE FORMA SUMARIZADA OS ALUNOS MATRICULADOS EM UM PERIODO LETIVO
     * OBS: OS PARAMETROS ESTÃO FIXOS DEVIDO A PRIORIDADE DO SPRINT.
     *
     * @param $periodo
     * @param $situacao
     * @param $tipoTurma
     * @param $turno
     * @return mixed
     */
    public function matriculaPorPeriodo($periodo, $situacao, $tipoTurma, $turno)
    {
        if ($tipoTurma === 'Regular') {
            $tipoTurma = " AND tturma_descricao = 'Convencional'";
        } elseif ($tipoTurma === 'Dependência e Adaptacao') {
            $tipoTurma = " AND tturma_descricao IN('Dependencia','Adaptação')";
        } else {
            $tipoTurma = '';
        }

        if (is_null($turno) || $turno !== 'Todos') {
            $turno = " AND acadperiodo__turma.turma_turno = '{$turno}' ";
        } else {
            $turno = '';
        }

        $sql = "
        SELECT turma_serie,turma_nome, COUNT(*) as qtd,turma_turno
        FROM acadperiodo__aluno
        NATURAL JOIN acadperiodo__turma
        NATURAL JOIN acadgeral__turma_tipo
        INNER JOIN acadgeral__situacao situacao ON situacao.situacao_id = matsituacao_id
        WHERE per_id = {$periodo} AND
              	matsit_descricao = '$situacao'  $tipoTurma $turno
        GROUP BY turma_id
        ORDER BY turma_serie, turma_nome";

        $matricula = $this->executeQuery($sql)->fetchAll();

        return $matricula;
    }

    /**
     * Serviço que retorna o periodo de rematricula em vigor se tiver.
     * @return \Matricula\Entity\AcadperiodoLetivo|null
     */
    public function periodoMatricula()
    {
        $query      = '
        SELECT per_id FROM acadperiodo__letivo
        WHERE date(now()) BETWEEN per_matricula_inicio AND per_matricula_fim';
        $arrPeriodo = $this->executeQuery($query)->fetch();

        if ($arrPeriodo) {
            /* @var $objPeriodo \Matricula\Entity\AcadperiodoLetivo */
            $objPeriodo = $this->getRepository()->find($arrPeriodo['per_id']);

            if ($objPeriodo) {
                return $objPeriodo;
            }
        }

        return null;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function pesquisaForJson($params)
    {
        $sql = "
        SELECT
        acadperiodo__letivo.*,
        acadperiodo__letivo_campus_curso.cursocampus_id,
        IF(per_data_finalizacao ,(DATE_FORMAT(per_data_finalizacao, '%d/%m/%Y')),NULL) perDataFormatada
        -- campos --
        FROM acadperiodo__letivo
        LEFT JOIN acadperiodo__letivo_campus_curso ON acadperiodo__letivo.per_id=acadperiodo__letivo_campus_curso.per_id
        -- juncoes --
        WHERE 1 ";

        $perNome = false;
        $perId   = false;

        if ($params['q']) {
            $perNome = $params['q'];
        } elseif ($params['query']) {
            $perNome = $params['query'];
        }

        if ($params['perId']) {
            $perId = $params['perId'];
        }

        $parameters = array('per_nome' => "{$perNome}%");
        $sql .= ' AND per_nome LIKE :per_nome';

        if ($perId) {
            $parameters['per_id'] = $perId;
            $sql .= ' AND per_id = :per_id';
        }

        if ($params['cursocampusId']) {
            $sql .= " AND cursocampus_id = :cursocampusId ";
            $parameters['cursocampusId'] = $params['cursocampusId'];
        }

        if ($params['abertoParaMatricula'] && !$params['vigente']) {
            $sql .= " AND DATE(now()) BETWEEN DATE(per_matricula_inicio) AND DATE(per_matricula_fim)";
        }

        if ($params['vigente'] && !$params['abertoParaMatricula']) {
            $sql .= " AND DATE(now()) BETWEEN DATE(per_data_inicio) AND DATE(per_data_fim)";
        }

        if ($params['abertoParaMatricula'] && $params['vigente']) {
            $sql .= " AND  ( DATE(now()) BETWEEN DATE(per_data_inicio) AND DATE(per_data_fim) OR DATE(now()) BETWEEN DATE(per_matricula_inicio) AND DATE(per_matricula_fim)) ";
        }

        if ($params['alunoperId']) {
            $join = '
             INNER JOIN acadperiodo__turma acadt on acadperiodo__letivo.per_id=acadt.per_id
             INNER JOIN acadperiodo__aluno acada on acadt.turma_id=acada.turma_id ';

            $sql = str_replace("-- juncoes --", $join, $sql);
            $sql = str_replace("-- campos --", ',acadt.*, acada.*', $sql);

            $sql .= " AND acada.alunoper_id=:alunoperId";
            $parameters['alunoperId'] = $params['alunoperId'];
        }

        $sql .= " GROUP BY acadperiodo__letivo.per_id";
        $sql .= " ORDER BY acadperiodo__letivo.per_id DESC";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            $serviceCampusCurso = new CampusCurso($this->getEm());

            if ($arrDados['perId']) {
                $objAcadperiodoLetivo = $this->getRepository()->find($arrDados['perId']);

                if (!$objAcadperiodoLetivo) {
                    $this->setLastError('Registro de período letivo não existe!');

                    return false;
                }
            } else {
                $objAcadperiodoLetivo = new \Matricula\Entity\AcadperiodoLetivo();
            }

            if ($arrDados['campusCurso']) {
                $arrCollectionCampuscurso = new ArrayCollection(
                    $serviceCampusCurso->getArrayCampuscursoPeloId($arrDados['campusCurso'])
                );

                $objAcadperiodoLetivo->setCursoCampus($arrCollectionCampuscurso);
            }

            $objAcadperiodoLetivo->setPerNome($arrDados['perNome']);
            $objAcadperiodoLetivo->setPerAno($arrDados['perAno']);
            $objAcadperiodoLetivo->setPerEtapas($arrDados['perEtapas']);
            $objAcadperiodoLetivo->setPerSemestre($arrDados['perSemestre']);
            $objAcadperiodoLetivo->setPerDataInicio($arrDados['perDataInicio']);
            $objAcadperiodoLetivo->setPerDataFim($arrDados['perDataFim']);
            $objAcadperiodoLetivo->setPerMatriculaInicio($arrDados['perMatriculaInicio']);
            $objAcadperiodoLetivo->setPerMatriculaFim($arrDados['perMatriculaFim']);
            $objAcadperiodoLetivo->setPerRematriculaOnlineInicio($arrDados['perRematriculaInicio']);
            $objAcadperiodoLetivo->setPerRematriculaOnlineFim($arrDados['perRematriculaFim']);

            $this->getEm()->persist($objAcadperiodoLetivo);
            $this->getEm()->flush($objAcadperiodoLetivo);

            $this->getEm()->commit();

            $arrDados['perId'] = $objAcadperiodoLetivo->getPerId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de período letivo!');
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['perNome']) {
            $errors[] = 'Por favor preencha o campo "nome"!';
        }

        if (!$arrParam['perAno']) {
            $errors[] = 'Por favor preencha o campo "ano"!';
        }

        if (!$arrParam['perEtapas']) {
            $errors[] = 'Por favor preencha o campo "número de etapas"!';
        }

        if (!$arrParam['perSemestre']) {
            $errors[] = 'Por favor preencha o campo "semestre"!';
        }

        if (!in_array($arrParam['perSemestre'], self::getPerSemestre())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "semestre"!';
        }

        if (!$arrParam['perMatriculaInicio']) {
            $errors[] = 'Por favor preencha o campo "data de início de matrícula"!';
        }

        if (!$arrParam['perMatriculaFim']) {
            $errors[] = 'Por favor preencha o campo "data final da matrícula"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM acadperiodo__letivo";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $perId
     * @return array|null|object
     */
    public function getArray($perId)
    {
        $arrDados = $this->getRepository()->find($perId);

        try {
            $arrDados = $arrDados->toArray();
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('perId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Matricula\Entity\AcadperiodoLetivo */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getPerId(),
                $params['value'] => $objEntity->getPerNome()
            );
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['perId']) {
            $this->setLastError('Para remover um registro de período letivo é necessário especificar o código.');

            return false;
        }

        try {
            $objAcadperiodoLetivo = $this->getRepository()->find($param['perId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAcadperiodoLetivo);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de período letivo.');

            return false;
        }

        return true;
    }

    public function getArrSelect2PeriodoAberto()
    {
        $arrPeriodo = $this->getRepository()->findAll();
        $array      = array();

        /** @var \Matricula\Entity\AcadperiodoLetivo $objPeriodo */
        foreach ($arrPeriodo as $objPeriodo) {
            if (is_null($objPeriodo->getPerDataFinalizacao())) {
                $array[] = [
                    'id'   => $objPeriodo->getPerId(),
                    'text' => $objPeriodo->getPerNome()
                ];
            }
        }

        return $array;
    }

    public function getAllPerToForm()
    {
        $arrPeriodos = $this->getRepository()->findAll();
        $array       = array();

        /** @var \Matricula\Entity\AcadperiodoLetivo $periodo */
        foreach ($arrPeriodos as $periodo) {
            $array[$periodo->getPerId()] = $periodo->getPerNome();
        }

        return $array;
    }

    public function getArrayPeriodoPeloId($id)
    {
        $array = array();
        $arrId = explode(',', $id);

        foreach ($arrId as $id) {
            $array[] = $this->getRepository()->find($id);
        }

        return $array;
    }

    /**
     * @param $alunocursoId integer
     * @param $obj boolean
     * @return \Matricula\Entity\AcadperiodoLetivo|integer|null
     */
    public function retornaUltimoPeriodoAluno($alunocursoId, $obj = false)
    {
        if (!$alunocursoId) {
            $this->setLastError('É necessário informar a matricula do aluno!');

            return false;
        }

        $sql = '
        SELECT apt.per_id FROM acadperiodo__aluno apa
        INNER JOIN acadperiodo__turma apt ON apt.turma_id=apa.turma_id
        WHERE alunocurso_id=:alunocursoId
        ORDER BY apt.per_id DESC
        LIMIT 0,1';

        $ultimoPerId = $this->executeQueryWithParam($sql, ['alunocursoId' => $alunocursoId])->fetch();

        if ($ultimoPerId) {
            $ultimoPerId = $ultimoPerId['per_id'];
        }

        if ($obj AND $ultimoPerId) {
            /* @var $objPeriodo \Matricula\Entity\AcadperiodoLetivo */
            $objPeriodo = $this->getRepository('\Matricula\Entity\AcadperiodoLetivo')->find($ultimoPerId);

            if (!$objPeriodo) {
                $this->setLastError('Periodo não localizado!');

                return false;
            } else {
                $ultimoPerId = $objPeriodo;
            }
        }

        return $ultimoPerId;
    }

    public function retornaPeriodoAbertoCurso($curso)
    {
        if (!$curso) {
            $this->setLastError("É necessário informar um curso!");

            return false;
        }

        $sql = "
        SELECT
            acadperiodo__letivo.*,
            acadperiodo__letivo.per_id id,
            acadperiodo__letivo.per_nome 'text'
        FROM  acadperiodo__turma
        INNER JOIN acadperiodo__letivo USING(per_id)
        INNER JOIN campus_curso  cc USING(cursocampus_id)
        WHERE DATE(now()) BETWEEN per_matricula_inicio AND per_matricula_fim AND cc.curso_id={$curso}
        GROUP  BY acadperiodo__letivo.per_id";

        $result = $this->executeQuery($sql)->fetchAll();

        return $result;
    }

}