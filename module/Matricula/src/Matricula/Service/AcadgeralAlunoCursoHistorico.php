<?php
namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class AcadgeralAlunoCursoHistorico
 * @package Matricula\Service
 */
class AcadgeralAlunoCursoHistorico extends AbstractService
{
    /**
     * @var null
     */
    private $lastError = null;

    /** @var \Sistema\Service\SisConfig */
    private $config = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, $arrConfig = array())
    {
        parent::__construct($em, 'Matricula\Entity\AcadgeralAlunoCursoHistorico');

        $this->setConfig(new \Sistema\Service\SisConfig($em, $arrConfig));
    }

    /**
     * @return \Sistema\Service\SisConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param \Sistema\Service\SisConfig $config
     * @return AcadgeralAluno
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    protected function  valida($dados)
    {
    }

    protected function pesquisaForJson($dados)
    {
    }

    /**
     * Método para salvar dados na tabela de historico depois que determinados dados forem alterados.
     * @param $arrDados
     * @return array|\Matricula\Entity\AcadgeralAlunoCursoHistorico
     *
     */

    public function save($arrDados)
    {
        $objAcadgaralAlunoCursoHistorico = new \Matricula\Entity\AcadgeralAlunoCursoHistorico();

        $objAcadgaralAlunoCursoHistorico
            ->setAlunocursoId($arrDados['alunocursoId'])
            ->setCursocampus($arrDados['cursocampusId'])
            ->setAluno($arrDados['alunoId'])
            ->setTiposel($arrDados['tiposelId'])
            ->setPesIdAgente($arrDados['pesIdAgente'])
            ->setUsuarioCadastro($arrDados['usuarioCadastro'])
            ->setUsuarioAlteracao($arrDados['usuarioAlteracao'])
            ->setAlunocursoCarteira($arrDados['alunocursoCarteira'])
            ->setAlunocursoObservacoes($arrDados['alunocursoObservacoes'])
            ->setAlunocursoDataColacao($arrDados['alunocursoDataColacao'])
            ->setAlunocursoDataExpedicaoDiploma($arrDados['alunocursoDataExpedicaoDiploma'])
            ->setAlunocursoSituacao($arrDados['alunocursoSituacao'])
            ->setAlunocursoDataCadastro($arrDados['alunocursoDataCadastro'])
            ->setAlunocursoDataMatricula($arrDados['alunocursoDataMatricula'])
            ->setAlunocursoDataAlteracao($arrDados['alunocursoDataAlteracao'])
            ->setMotivo($arrDados['motivo'])
            ->setAlunocursoAlteracaoObservacao($arrDados['alunocursoAlteracaoObservacao']);

        try {
            $this->getEm()->persist($objAcadgaralAlunoCursoHistorico);
            $this->getEm()->flush($objAcadgaralAlunoCursoHistorico);
        } catch (\Exception $ex) {
            $this->setLastError("Erro ao salvar os dados de histórico de aluno!" . $ex->getMessage());

            return false;
        }

        return $objAcadgaralAlunoCursoHistorico;
    }
}