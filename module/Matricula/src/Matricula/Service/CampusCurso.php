<?php
namespace Matricula\Service;

use Doctrine\Common\Collections\ArrayCollection;
use VersaSpine\Service\AbstractService;

/**
 * Class CampusCurso
 * @package Matricula\Service
 */
class CampusCurso extends AbstractService
{
    /**
     * @var null
     */
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\CampusCurso');
    }

    /**
     * @param $params
     * @return mixed
     */
    public function pesquisaForJson($params)
    {
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());

        $validarCampusCursoLogado = isset($params['validaCampusCursoLogado']) ? $params['validaCampusCursoLogado'] : true;

        $sql           = '
        SELECT
            curso_id, camp_id, cursocampus_id, ies_id, camp_nome, camp_telefone, camp_email, camp_site,
            mod_id, nivel_id, area_id, curso_nome, curso_unidade_medida, curso_possui_periodo_letivo, curso_carga_horaria,
            curso_carga_horaria_pratica, curso_carga_horaria_teorica, curso_turno, curso_sigla,
            curso_vagas_noturno, curso_vagas_vespertino, curso_vagas_matutino, curso_prazo_integralizacao,
            curso_prazo_integralizacao_maxima, mod_nome, nivel_nome, unidade_id, unidade_nome
        FROM campus_curso
        INNER JOIN org_campus USING (camp_id)
        INNER JOIN acad_curso USING (curso_id)
        INNER JOIN acad_modalidade modalidade USING (mod_id)
        INNER JOIN acad_nivel nivel USING (nivel_id)
        LEFT JOIN org__unidade_estudo USING (camp_id)
        LEFT JOIN acadperiodo__letivo_campus_curso USING(cursocampus_id)
        WHERE
             (curso_nome LIKE :curso_nome OR camp_nome LIKE :curso_nome) ';
        $cursoNome     = false;
        $campId        = false;
        $cursocampusId = false;
        $nivel         = $params['nivel'] ? $params['nivel'] : false;
        $modalidade    = $params['modalidade'] ? $params['modalidade'] : false;
        $area          = $params['area'] ? $params['area'] : false;

        if ($params['q']) {
            $cursoNome = $params['q'];
        } elseif ($params['query']) {
            $cursoNome = $params['query'];
        }

        if ($params['cursocampusId']) {
            $cursocampusId = $params['cursocampusId'];
        }

        if ($params['campId']) {
            $campId = $params['campId'];
        }

        $parameters = array('curso_nome' => "{$cursoNome}%");

        if ($cursocampusId) {
            $parameters['cursocampus_id'] = $cursocampusId;
            $sql .= ' AND cursocampus_id = :cursocampus_id';
        }

        if ($campId) {
            $parameters['campId'] = $campId;
            $sql .= ' AND camp_id = :campId';
        }

        if ($nivel) {
            $nivel = is_array($nivel) ? implode(',', $nivel) : $nivel;
            $sql .= " AND nivel.nivel_id in ({$nivel})";
        }

        if ($modalidade) {
            $modalidade = is_array($modalidade) ? implode(',', $modalidade) : $modalidade;
            $sql .= " AND modalidade.mod_id in ({$modalidade})";
        }

        if ($area) {
            $area = is_array($area) ? implode(',', $area) : $area;
            $sql .= " AND area_id in ({$area})";
        }

        if ($params['naoIncluirCampusCurso']) {
            $campusCurso = $params['naoIncluirCampusCurso'];

            if (!is_array($campusCurso)) {
                $campusCurso = explode(',', $campusCurso);
            }

            $campusCurso = array_filter($campusCurso);
            $campusCurso = implode(',', $campusCurso);

            if ($campusCurso) {
                $sql .= " AND cursocampus_id NOT IN ({$campusCurso})";
            }
        }

        if ($params['perId']) {
            $sql .= " AND acadperiodo__letivo_campus_curso.per_id in ({$params['perId']})";
        }

        $sql .= " AND curso_situacao != 'Inativo'";

        if ($validarCampusCursoLogado) {
            $arrCampusCursoPermitido = implode(",", $serviceAcessoPessoas->retornaCampusCursoDoUsuarioLogado(true));

            if ($arrCampusCursoPermitido) {
                $sql .= " AND cursocampus_id in (" . $arrCampusCursoPermitido . ")";
            }
        }

        if ($params['agruparPorCampus']) {
            $sql .= " GROUP BY camp_id";
        } elseif ($params['agruparPorCurso']) {
            $sql .= " GROUP BY curso_id";
        } elseif ($params['agruparPorCursocampus']) {
            $sql .= " GROUP BY cursocampus_id";
        } else {
            $sql .= " GROUP BY cursocampus_id";
        }

        $sql .= " ORDER BY curso_id";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function getDescricao($id)
    {
        if (!$id) {
            return false;
        }

        if (!is_array($id)) {
            $id = array($id);
        }

        $id = implode($id, ',');

        $query = '
        SELECT group_concat(concat(campus.camp_nome," / ",curso.curso_nome) SEPARATOR ", ") text
        FROM campus_curso
        LEFT JOIN org_campus campus USING(camp_id)
        LEFT JOIN acad_curso curso USING(curso_id)
        WHERE campus_curso.cursocampus_id IN (' . $id . ')';

        $result = $this->executeQueryWithParam($query)->fetch();

        return $result['text'];
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceOrgCampus     = new \Organizacao\Service\OrgCampus($this->getEm());
        $serviceAcadCurso     = new \Matricula\Service\AcadCurso($this->getEm());
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());
        $servicePeriodoLetivo = new AcadperiodoLetivo($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['cursocampusId']) {
                $objCampusCurso = $this->getRepository()->find($arrDados['cursocampusId']);

                if (!$objCampusCurso) {
                    $this->setLastError('Registro de curso do câmpus não existe!');

                    return false;
                }
            } else {
                $objCampusCurso = new \Matricula\Entity\CampusCurso();
            }

            if ($arrDados['periodo']) {
                $arrCollectionPeriodo = new ArrayCollection(
                    $servicePeriodoLetivo->getArrayPeriodoPeloId($arrDados['periodo'])
                );

                $objCampusCurso->setPer($arrCollectionPeriodo);
            }

            if ($arrDados['acessoPessoas']) {
                $arrCollectionAcessoPessoas = new ArrayCollection(
                    $serviceAcessoPessoas->getArray($arrDados['acessoPessoas'])
                );

                $objCampusCurso->setAcessoPessoas($arrCollectionAcessoPessoas);
            }

            if ($arrDados['camp']) {
                /** @var $objOrgCampus \Organizacao\Entity\OrgCampus */
                $objOrgCampus = $serviceOrgCampus->getRepository()->find($arrDados['camp']);

                if (!$objOrgCampus) {
                    $this->setLastError('Registro de câmpus não existe!');

                    return false;
                }

                $objCampusCurso->setCamp($objOrgCampus);
            }

            if ($arrDados['curso']) {
                /** @var $objAcadCurso \Matricula\Entity\AcadCurso */
                $objAcadCurso = $serviceAcadCurso->getRepository()->find($arrDados['curso']);

                if (!$objAcadCurso) {
                    $this->setLastError('Registro de curso não existe!');

                    return false;
                }

                $objCampusCurso->setCurso($objAcadCurso);
            }

            $objCampusCurso->setCursocampusReconhecimento($arrDados['campuscursoReconhecimento']);
            $objCampusCurso->setCursocampusAutorizacao($arrDados['campuscursoAutorizacao']);

            $this->getEm()->persist($objCampusCurso);
            $this->getEm()->flush($objCampusCurso);

            $this->getEm()->commit();

            $arrDados['cursocampusId'] = $objCampusCurso->getCursocampusId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de curso do câmpus!');
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['camp']) {
            $errors[] = 'Por favor preencha o campo "câmpus"!';
        }

        if (!$arrParam['curso']) {
            $errors[] = 'Por favor preencha o campo "curso"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "
        SELECT * FROM acad_curso
        LEFT JOIN campus_curso USING (curso_id)
        LEFT JOIN org_campus USING (camp_id)";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $cursocampusId
     * @return array
     */
    public function getArray($cursocampusId)
    {
        $arrDados = array();
        /** @var $objCampusCurso \Matricula\Entity\CampusCurso */
        $objCampusCurso = $this->getRepository()->find($cursocampusId);

        try {
            $arrDados = $objCampusCurso->toArray();

            if ($arrDados['campId']) {
                $objOrgCampus = $objCampusCurso->getCamp();

                if ($objOrgCampus) {
                    $arrDados['campId'] = array(
                        'id'   => $objOrgCampus->getCampId(),
                        'text' => $objOrgCampus->getCampNome()
                    );
                }
            }

            if ($arrDados['cursoId']) {
                $objAcadCurso = $objCampusCurso->getCurso();

                if ($objAcadCurso) {
                    $arrDados['cursoId'] = array(
                        'id'                       => $objAcadCurso->getCursoId(),
                        'text'                     => $objAcadCurso->getCursoNome(),
                        'cursoPossuiPeriodoLetivo' => $objAcadCurso->getCursoPossuiPeriodoLetivo()
                    );
                }
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['cursocampusId']) {
            $this->setLastError('Para remover um registro de curso do câmpus é necessário especificar o código.');

            return false;
        }

        try {
            $objCampusCurso = $this->getRepository()->find($param['cursocampusId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objCampusCurso);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de curso do câmpus.');

            return false;
        }

        return true;
    }

    /**
     * Função para Buscar os cursos que um câmpus possui.
     * $campId Recebe o id do câmpus que se deseja buscar.
     * */
    public function buscaCursoCampus($campId = null)
    {
        $query = "
        SELECT campus_curso.curso_id, acad_curso.curso_nome, cursocampus_id, camp_nome, org_campus.camp_id
        FROM campus_curso
        INNER JOIN org_campus ON campus_curso.camp_id = org_campus.camp_id
        INNER JOIN acad_curso ON acad_curso.curso_id = campus_curso.curso_id";

        if ($campId) {
            $query .= " WHERE campus_curso.camp_id = '{$campId}'";
        }

        $arrCursos = $this->executeQuery($query)->fetchAll();

        return $arrCursos;
    }

    /**
     * @return array
     */
    public function mountSelect()
    {
        $query          = "
        SELECT cursocampus_id, curso_nome, camp_nome
        FROM campus_curso
        INNER JOIN acad_curso ON acad_curso.curso_id = campus_curso.curso_id
        INNER JOIN org_campus ON org_campus.camp_id = campus_curso.camp_id";
        $campusCurso    = $this->executeQuery($query)->fetchAll();
        $arrCampusCurso = [];

        foreach ($campusCurso as $camp) {
            $arrCampusCurso[$camp['cursocampus_id']] = $camp['camp_nome'] . " / " . $camp['curso_nome'];
        }

        return $arrCampusCurso;
    }

    /**
     * @return array
     */
    public function getArrayCampusCurso()
    {
        $arrCampusCurso = [];
        $query          = "
        SELECT cursocampus_id, curso_nome, camp_nome
        FROM campus_curso
        INNER JOIN acad_curso ON acad_curso.curso_id = campus_curso.curso_id
        INNER JOIN org_campus ON org_campus.camp_id = campus_curso.camp_id";

        $campusCurso = $this->executeQuery($query)->fetchAll();

        foreach ($campusCurso as $camp) {
            $arrCampusCurso[$camp['cursocampus_id']] = $camp['camp_nome'] . " / " . $camp['curso_nome'];
        }

        return $arrCampusCurso;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('cursocampusId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();

        /* @var $objEntity \Matricula\Entity\CampusCurso */
        foreach ($arrEntities as $objEntity) {
            $arrEntity                   = $objEntity->toArray();
            $arrEntity[$params['key']]   = $objEntity->getCursocampusId();
            $arrEntity[$params['value']] = (
                $objEntity->getCamp()->getCampNome() . ' / ' . $objEntity->getCurso()->getCursoNome()
            );

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    /**
     * @param array                       $arrParam
     * @param \Matricula\Entity\AcadCurso $objAcadCurso
     * @return bool
     */
    public function salvarMultiplos(array &$arrParam, \Matricula\Entity\AcadCurso $objAcadCurso)
    {
        $arrExcluir = array();
        $arrEditar  = array();

        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());

        $arrCampusCursoPermitido = $serviceAcessoPessoas->retornaCampusCursoDoUsuarioLogado(true);

        try {
            $this->getEm()->beginTransaction();

            $arrOrgCampus   = explode(',', $arrParam['campus']);
            $arrOrgCampusDB = $this->getRepository()->findBy(
                ['curso' => $objAcadCurso->getCursoId()]
            );

            /* @var $objCampusCurso \Matricula\Entity\CampusCurso */
            foreach ($arrOrgCampusDB as $objCampusCurso) {
                $encontrado    = false;
                $campId        = $objCampusCurso->getCamp()->getCampId();
                $cursocampusId = $objCampusCurso->getCursocampusId();

                if ($arrCampusCursoPermitido && !in_array(
                        $objCampusCurso->getCursocampusId(),
                        $arrCampusCursoPermitido
                    )
                ) {
                    $this->setLastError("Usuário não tem permissão de acesso ao campus-curso informado!");

                    return false;
                }

                foreach ($arrOrgCampus as $campus) {
                    if ($campId == $campus) {
                        $encontrado         = true;
                        $arrEditar[$campus] = $cursocampusId;
                        break;
                    }
                }

                if (!$encontrado) {
                    $arrExcluir[$campId] = $objCampusCurso;
                }
            }

            if ($arrExcluir) {
                foreach ($arrExcluir as $objCampusCurso) {
                    $this->getEm()->remove($objCampusCurso);
                    $this->getEm()->flush();
                }
            }

            foreach ($arrOrgCampus as $campus) {
                $arrCampus = [
                    'curso'                     => $objAcadCurso->getCursoId(),
                    'camp'                      => $campus,
                    'campuscursoAutorizacao'    => $arrParam['campuscursoAutorizacao'],
                    'campuscursoReconhecimento' => $arrParam['campuscursoReconhecimento']
                ];

                if (isset($arrEditar[$campus])) {
                    $arrCampus['cursocampusId'] = $arrEditar[$campus];
                }

                if (!$this->save($arrCampus)) {
                    throw new \Exception($this->getLastError());
                }
            }

            $this->getEm()->commit();

            return true;
        } catch (\Exception $e) {
            $this->getEm()->rollback();
            $this->setLastError('Não foi possível salvar os registros de cursos do campus!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $cursoId
     * @return array
     */
    public function getArrayCampus($cursoId)
    {
        $arrCampusRetorno = [];
        $arrCampus        = $this->getRepository()->findBy(['curso' => $cursoId]);

        /* @var $objCampusCurso \Matricula\Entity\CampusCurso */
        foreach ($arrCampus as $objCampusCurso) {
            $arrPredioRetorno = $objCampusCurso->toArray();
            unset($arrPredioRetorno['curso']);
            unset($arrPredioRetorno['camp']);
            $arrCampusRetorno[] = $arrPredioRetorno;
        }

        return $arrCampusRetorno;
    }

    public function retornaCampusCursoExistente($campId = false, $cursoId = false)
    {
        $arrParam = [];

        if ($campId) {
            $arrParam['camp'] = $campId;
        }

        if ($cursoId) {
            $arrParam['curso'] = $cursoId;
        }

        if (!$arrParam) {
            return null;
        }

        $objCampusCurso = $this->getRepository()->findBy($arrParam);

        return $objCampusCurso;
    }

    public function getArrayCampuscursoPeloId($id)
    {
        $array = array();
        $arrId = explode(',', $id);

        foreach ($arrId as $id) {
            $array[] = $this->getRepository()->find($id);
        }

        return $array;
    }

    public function getArrayCampusCursoDoProcessoSeletivo(&$arrDados)
    {
        $serviceAcadModalidade = new \Matricula\Service\AcadModalidade($this->getEm());
        $serviceEdicao         = new \Vestibular\Service\SelecaoEdicao($this->getEm());
        $serviceAcadNivel      = new \Matricula\Service\AcadNivel($this->getEm());

        $arrCampusCurso = $this->getArray($arrDados['cursocampus_id']);

        $arrDados['curso'] = $this->getArrSelect2(
            array('id' => $arrCampusCurso['cursocampusId'])
        );

        $arrDados['nivel'] = $serviceAcadNivel->getArray(
            $arrCampusCurso['nivel']->getNivelId()
        );

        $arrDados['modalidade'] = $serviceAcadModalidade->getArray(
            $arrCampusCurso['mod']
        );

        $arrDados['edicao'] = $serviceEdicao->getArray(
            $arrDados['edicao_id']
        );

        $arrDados['cursoPossuiPeriodoLetivo'] = $arrCampusCurso['cursoId']['cursoPossuiPeriodoLetivo'];

        return $arrDados;
    }
}