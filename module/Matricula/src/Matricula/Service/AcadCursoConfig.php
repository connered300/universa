<?php

namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class AcadCursoConfig
 * @package Matricula\Service
 */
class AcadCursoConfig extends AbstractService
{
    const METODO_DESEMPENHO_CALCULO_POR_MEDIA = 'Media';
    const METODO_DESEMPENHO_CALCULO_POR_SOMA  = 'Soma';
    const METODO_FINAL_POR_MEDIA              = 'Media';
    const METODO_FINAL_POR_FINAL              = 'Final';
    const NOTA_FRACIONADA_SIM                 = 'Sim';
    const NOTA_FRACIONADA_NAO                 = 'Não';
    const DEFERIMENTO_AUTOMATICO_SIM          = 'Sim';

    /**
     * @var null
     */
    private $lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadCursoConfig');
    }

    public function retornaMetodoDesempenho()
    {

        return array(
            self::METODO_DESEMPENHO_CALCULO_POR_MEDIA,
            self::METODO_DESEMPENHO_CALCULO_POR_SOMA
        );
    }

    public function retornMetodoDesempenhoFinal()
    {
        return array(
            self::METODO_FINAL_POR_MEDIA,
            self::METODO_FINAL_POR_FINAL
        );
    }

    public function retornValoreNotaFracionada()
    {
        return array(
            self::NOTA_FRACIONADA_SIM,
            self::NOTA_FRACIONADA_NAO
        );
    }

    /**
     * @param $params
     * @return mixed
     */
    public function pesquisaForJson($params)
    {
        $sql           = 'SELECT * FROM acad_curso_config NATURAL JOIN acad_curso WHERE';
        $cursoNome     = false;
        $cursoconfigId = false;

        if ($params['q']) {
            $cursoNome = $params['q'];
        } elseif ($params['query']) {
            $cursoNome = $params['query'];
        }

        if ($params['cursoconfigId']) {
            $cursoconfigId = $params['cursoconfigId'];
        }

        $parameters = array('curso_nome' => "{$cursoNome}%");
        $sql .= ' curso_nome LIKE :curso_nome';

        if ($cursoconfigId) {
            $parameters['cursoconfig_id'] = $cursoconfigId;
            $sql .= ' AND cursoconfig_id <> :cursoconfig_id';
        }

        $sql .= " ORDER BY curso_nome, cursoconfig_id DESC";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM acad_curso_config NATURAL JOIN acad_curso";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $cursoconfigId
     * @return array
     */
    public function getArray($cursoconfigId)
    {
        $arrDados = array();
        /** @var $objAcadCursoConfig \Matricula\Entity\AcadCursoConfig */
        $objAcadCursoConfig = $this->getRepository()->find($cursoconfigId);

        try {
            $arrDados = $objAcadCursoConfig->toArray();

            if ($arrDados['cursoId']) {
                /** @var $objAcadCurso \Matricula\Entity\AcadCurso */
                $objAcadCurso = $objAcadCursoConfig->getCurso();

                if ($objAcadCurso) {
                    $arrDados['cursoId'] = array(
                        'id'   => $objAcadCurso->getCursoId(),
                        'text' => $objAcadCurso->getCursoNome()
                    );
                }
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('cursoconfigId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Matricula\Entity\AcadCursoConfig */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getCursoconfigId(),
                $params['value'] => $objEntity->getCurso()->getCursoNome() . ' - ' . $objEntity->getCursoconfigId()
            );
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['cursoconfigId']) {
            $this->setLastError('Para remover um registro de configuração de curso é necessário especificar o código.');

            return false;
        }

        try {
            $objAcadCursoConfig = $this->getRepository()->find($param['cursoconfigId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAcadCursoConfig);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de configuração de curso.');

            return false;
        }

        return true;
    }

    /**
     * @param $cursoId
     * @return array
     */
    public function getArrayConfiguracoes($cursoId)
    {
        $serviceAcadCurso = new \Matricula\Service\AcadCurso($this->getEm());

        //TODO: revisar método de busca de configuração de cursos
        $sql = '
        SELECT
            config.*,
            periodoInicial.per_nome AS periodoInicial,
            periodoFinal.per_nome AS periodoFinal,
            amc.mat_cur_descricao matrizDescricao
        FROM acad_curso_config AS config
        LEFT JOIN acadperiodo__letivo AS periodoInicial ON periodoInicial.per_id=config.per_ativ_id
        LEFT JOIN acadperiodo__letivo AS periodoFinal ON periodoFinal.per_id=config.per_desat_id
        LEFT JOIN acadperiodo__matriz_curricular amc ON amc.mat_cur_id=config.cursoconfig_matriz_curricular
        WHERE config.curso_id=:curso_id
        GROUP BY config.per_ativ_id DESC, config.per_desat_id DESC';

        $parameters              = array('curso_id' => $cursoId);
        $arrConfiguracoes        = $this->executeQueryWithParam($sql, $parameters)->fetchAll();
        $arrConfiguracoesRetorno = [];

        foreach ($arrConfiguracoes as $arrAlunoFormacao) {
            $arrConfig = [
                'cursoconfigId'                     => $arrAlunoFormacao['cursoconfig_id'],
                'cursoId'                           => $arrAlunoFormacao['curso_id'],
                'perAtivId'                         => $arrAlunoFormacao['per_ativ_id'],
                'perDesatId'                        => $arrAlunoFormacao['per_desat_id'],
                'cursoconfigDataInicio'             => self::formatDateBrasileiro(
                    $arrAlunoFormacao['cursoconfig_data_inicio']
                ),
                'cursoconfigDataFim'                => self::formatDateBrasileiro(
                    $arrAlunoFormacao['cursoconfig_data_fim']
                ),
                'cursoconfigHistId'                 => $arrAlunoFormacao['cursoconfig_hist_id'],
                'cursoconfigNotaMax'                => $arrAlunoFormacao['cursoconfig_nota_max'],
                'cursoconfigNotaMin'                => $arrAlunoFormacao['cursoconfig_nota_min'],
                'cursoconfigNotaFinal'              => $arrAlunoFormacao['cursoconfig_nota_final'],
                'cursoconfigFreqMin'                => $arrAlunoFormacao['cursoconfig_freq_min'],
                'cursoconfigMediaFinalMin'          => $arrAlunoFormacao['cursoconfig_media_final_min'],
                'cursoconfigNumeroMaximoPendencias' => $arrAlunoFormacao['cursoconfig_numero_maximo_pendencias'],
                'periodoInicial'                    => $arrAlunoFormacao['periodoInicial'],
                'periodoFinal'                      => $arrAlunoFormacao['periodoFinal'],
                'cursoconfigMetodo'                 => $arrAlunoFormacao['cursoconfig_metodo'],
                'cursoconfigMetodoFinal'            => $arrAlunoFormacao['cursoconfig_metodo_final'],
                'cursoconfigNotaFracionada'         => $arrAlunoFormacao['cursoconfig_nota_fracionada'],
                'cursoconfigSituacaoDeferimento'    => $arrAlunoFormacao['cursoconfig_situacao_deferimento'],
                'matrizCurricularId'                => $arrAlunoFormacao['cursoconfig_matriz_curricular'],
                'matrizCurricularDesc'              => $arrAlunoFormacao['matrizDescricao'],
                'cursoconfigDeferimentoAutomatico'  => $arrAlunoFormacao['cursoconfig_deferimento_automatico']
            ];

            if ($arrConfig['periodoInicial']) {
                $arrConfig['periodoAtividade'] = $arrConfig['periodoInicial'] . ' - ' . $arrConfig['periodoFinal'];
            } else {
                $arrConfig['periodoAtividade'] = (
                    $arrConfig['cursoconfigDataInicio'] . ' - ' . $arrConfig['cursoconfigDataFim']
                );
            }

            $arrConfiguracoesRetorno[] = $arrConfig;
        }

        return $arrConfiguracoesRetorno;
    }

    /**
     * @param array                       $arrParam
     * @param \Matricula\Entity\AcadCurso $objAcadCurso
     * @return bool
     */
    public function salvarMultiplos(array &$arrParam, \Matricula\Entity\AcadCurso $objAcadCurso)
    {
        $arrExcluir = array();

        try {
            $this->getEm()->beginTransaction();

            $arrConfiguracoes   = json_decode($arrParam['configuracoes'], true);
            $arrConfiguracoesDB = $this->getRepository()->findBy(
                ['curso' => $objAcadCurso->getCursoId()]
            );

            /* @var $objAcadCursoConfig \Matricula\Entity\AcadCursoConfig */
            foreach ($arrConfiguracoesDB as $objAcadCursoConfig) {
                $encontrado    = false;
                $cursoconfigId = $objAcadCursoConfig->getCursoconfigId();

                foreach ($arrConfiguracoes as $arrAcadCursoConfig) {
                    if ($cursoconfigId == $arrAcadCursoConfig['cursoconfigId']) {
                        $encontrado = true;
                        break;
                    }
                }

                if (!$encontrado) {
                    $arrExcluir[$cursoconfigId] = $objAcadCursoConfig;
                }
            }

            if ($arrExcluir) {
                foreach ($arrExcluir as $objAcadCursoConfig) {
                    $this->getEm()->remove($objAcadCursoConfig);
                    $this->getEm()->flush();
                }
            }

            foreach ($arrConfiguracoes as $arrAcadCursoConfig) {
                $arrAcadCursoConfig['cursoId'] = $objAcadCurso->getCursoId();

                if (!$this->save($arrAcadCursoConfig)) {
                    throw new \Exception($this->getLastError());
                }
            }

            $this->getEm()->commit();

            return true;
        } catch (\Exception $e) {
            $this->getEm()->rollback();
            $this->setLastError('Não foi possível salvar configurações do curso!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        $serviceMatrizCurricular = new \Matricula\Service\AcadperiodoMatrizCurricular($this->getEm());

        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceAcadCurso = new \Matricula\Service\AcadCurso($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['cursoconfigId']) {
                $objCursoConfig = $this->getRepository()->find($arrDados['cursoconfigId']);

                if (!$objCursoConfig) {
                    $this->setLastError('Registro de configuração de curso não existe!');

                    return false;
                }
            } else {
                $objCursoConfig = new \Matricula\Entity\AcadCursoConfig();
            }

            $objMatrizCurricular = false;

            if ($arrDados['matrizCurricularId']) {
                /** @var \Matricula\Entity\AcadperiodoMatrizCurricular $objMatrizCurricular */
                $objMatrizCurricular = $serviceMatrizCurricular->getRepository()->findOneBy(
                    ['matCurId' => $arrDados['matrizCurricularId']]
                );

                if (!$objMatrizCurricular) {
                    $this->setLastError("Matriz não encontrada!");

                    return false;
                }

                //TODO: TRATAMENTO PARA CASO A MATRIZ NÃO TENHA CURSO, ATENÇÃO SE NÃO HOUVER ELE PERMITE PROSEGUIR SEMELHANTE AO FUNCIONAMENTO DOS TÍTULOS, PORÉM NO JS PASSA O CURSOID PARA BUSCA
                $cursoMatrizId = (
                $objMatrizCurricular->getCurso() ?
                    $objMatrizCurricular->getCurso()->getCursoId() :
                    $arrDados['cursoId']
                );

                if ($cursoMatrizId != $arrDados['cursoId']) {
                    $this->setLastError("Essa matriz não pertence a este curso!");

                    return false;
                }
            }

            if ($arrDados['cursoId']) {
                /** @var $objAcadCurso \Matricula\Entity\AcadCurso */
                $objAcadCurso = $serviceAcadCurso->getRepository()->find($arrDados['cursoId']);

                if (!$objAcadCurso) {
                    $this->setLastError('Registro de curso não existe!');

                    return false;
                }

                $objCursoConfig->setCurso($objAcadCurso);
            }

            $objCursoConfig->setPerAtivId($arrDados['perAtivId']);
            $objCursoConfig->setPerDesatId($arrDados['perDesatId']);
            $objCursoConfig->setCursoconfigDataInicio($arrDados['cursoconfigDataInicio']);
            $objCursoConfig->setCursoconfigDataFim($arrDados['cursoconfigDataFim']);
            $objCursoConfig->setCursoconfigHistId($arrDados['cursoconfigHistId']);
            $objCursoConfig->setCursoconfigNotaMax($arrDados['cursoconfigNotaMax']);
            $objCursoConfig->setCursoconfigNotaMin($arrDados['cursoconfigNotaMin']);
            $objCursoConfig->setCursoconfigNotaFinal($arrDados['cursoconfigNotaFinal']);
            $objCursoConfig->setCursoconfigFreqMin((int)$arrDados['cursoconfigFreqMin']);
            $objCursoConfig->setCursoconfigMediaFinalMin($arrDados['cursoconfigMediaFinalMin']);
            $objCursoConfig->setCursoconfigNumeroMaximoPendencias((int)$arrDados['cursoconfigNumeroMaximoPendencias']);
            $objCursoConfig->setCursoconfigMetodo($arrDados['cursoconfigMetodo']);
            $objCursoConfig->setCursoconfigMetodoFinal($arrDados['cursoconfigMetodoFinal']);
            $objCursoConfig->setCursoconfigNotaFracionada($arrDados['cursoconfigNotaFracionada']);
            $objCursoConfig->setCursoconfigSituacaoDeferimento($arrDados['cursoconfigSituacaoDeferimento']);
            $objCursoConfig->setMatrizCurricular($objMatrizCurricular ? $objMatrizCurricular : null);
            $objCursoConfig->setCursoconfigDeferimentoAutomatico($arrDados['cursoconfigDeferimentoAutomatico']);

            $this->getEm()->persist($objCursoConfig);
            $this->getEm()->flush($objCursoConfig);

            $this->getEm()->commit();

            $arrDados['cursoconfigId'] = $objCursoConfig->getCursoconfigId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de configuração de curso!');
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        //TODO: validar configurações concorrentes
        $errors = array();

        if (!$arrParam['cursoId']) {
            $errors[] = 'Por favor preencha o campo "curso"!';
        }

        if (!$arrParam['perAtivId'] && !$arrParam['cursoconfigDataInicio']) {
            $errors[] = 'Por favor preencha o campo "período letivo inicial" ou a "data de inicio da configuração"!';
        }

        if (!$arrParam['cursoconfigNotaMax']) {
            $errors[] = 'Por favor preencha o campo "nota máxima"!';
        }

        if (!$arrParam['cursoconfigNotaMin']) {
            $errors[] = 'Por favor preencha o campo "nota mínima para aprovação"!';
        }

        if (!$arrParam['cursoconfigNotaFinal']) {
            $errors[] = 'Por favor preencha o campo "nota mínima para fazer prova final"!';
        }

        if (empty($arrParam['cursoconfigFreqMin'])) {
            $errors[] = 'Por favor preencha o campo "frequencia mínima"!';
        }

        if (!$arrParam['cursoconfigMediaFinalMin']) {
            $errors[] = 'Por favor preencha o campo "média mínima da nota final e nota da prova final"!';
        }

        if (empty($arrParam['cursoconfigNumeroMaximoPendencias'])) {
            $errors[] = 'Por favor preencha o campo "número máximo de pendencias"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2MetodoDesempenho($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->retornaMetodoDesempenho());
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2MetodoDesempenhoFinal($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->retornMetodoDesempenhoFinal());
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2NotaFracionada($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->retornValoreNotaFracionada());
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $view->setVariables(
            [
                "arrMetodoDesempenho"      => $this->getArrSelect2MetodoDesempenho(),
                "arrMetodoDesempenhoFinal" => $this->getArrSelect2MetodoDesempenhoFinal(),
            ]
        );
    }

    /* TODO: Método precisa ser trabalhado para melhorar a filtragem */
    /**
     * @param $cursoId
     * @return bool|null|\Matricula\Entity\AcadCursoConfig
     */
    public function retornaObjCursoConfig($cursoId)
    {
        if (!$cursoId) {
            $this->setLastError('É necessário informar o curso!');

            return false;
        }

        if (is_object($cursoId)) {
            $cursoId = $cursoId->getCursoId();
        }

        $param = ['curso' => $cursoId];

        $sql = "

        SELECT cursoconfig_id FROM acad_curso_config
        WHERE curso_id=:curso ORDER BY cursoconfig_id DESC LIMIT 1
        ";

        $retorno = $this->executeQueryWithParam($sql, $param)->fetch();

        if (!$retorno) {
            $this->setLastError('Essa configuração de curso não existe!');

            return false;
        }

        return $this->getRepository()->find($retorno['cursoconfig_id']);
    }

}