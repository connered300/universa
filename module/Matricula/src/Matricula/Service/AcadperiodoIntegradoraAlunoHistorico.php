<?php

namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

class AcadperiodoIntegradoraAlunoHistorico extends AbstractService
{
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadperiodoIntegradoraAlunoHistorico');
    }

    protected function valida($dados)
    {
    }

    protected function pesquisaForJson($criterios)
    {
    }

    /**
     * Método para salvar histórico de AlunoIntegradora
     * @param array|Objetct $integradoraAluno : array ou objeto com dados de AlunoIntegradora para gerar histórico
     * @return array|\Matricula\Entity\AcadperiodoIntegradoraAlunoHistorico : array com mensagem de erro ou,
     *         objeto do historico inserido
     */
    public function save($integradoraAluno)
    {
        if (is_array($integradoraAluno)) {
            $integradoraAluno = $this->getReference(
                $integradoraAluno['alunointegId'],
                "Matricula\\Entity\\AcadperiodoIntegradoraAluno"
            );

            if (!$integradoraAluno) {
                return array("Informe um registro válido/existente");
            }
        }

        if (!($integradoraAluno instanceof \Matricula\Entity\AcadperiodoIntegradoraAluno)) {
            return array("Objeto Informado não válido");
        }

        $objIntegradoraAlunoHistorico = new \Matricula\Entity\AcadperiodoIntegradoraAlunoHistorico();

        $objIntegradoraAlunoHistorico->setAlunointeg($integradoraAluno);
        $objIntegradoraAlunoHistorico->setAlunodiscHist($integradoraAluno->getAlunodisc());
        $objIntegradoraAlunoHistorico->setAlunointegHistRespostas($integradoraAluno->getAlunointegRespostas());
        $objIntegradoraAlunoHistorico->setAlunointegHistAcertos($integradoraAluno->getAlunointegAcertos());
        $objIntegradoraAlunoHistorico->setAlunointegHistNotaIsolada($integradoraAluno->getAlunointegNotaIsolada());
        $objIntegradoraAlunoHistorico->setAlunointegHistData(new \DateTime('now'));

        try {
            $this->getEm()->persist($objIntegradoraAlunoHistorico);
            $this->getEm()->flush($objIntegradoraAlunoHistorico);
        } catch (\Exception $ex) {
            return array("Erro ao gerar histórico de Integradora do Aluno");
        }

        return $objIntegradoraAlunoHistorico;
    }
}