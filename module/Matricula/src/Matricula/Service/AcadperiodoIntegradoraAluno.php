<?php


namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

class AcadperiodoIntegradoraAluno extends AbstractService
{
    private $confPontosIntegradora;

    private $serviceHistory;

    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadperiodoIntegradoraAluno');
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {
    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {
    }

    /**
     * Sempre que este método e chamado ele exlui todos os registros da
     * tabela acadperiodo__integradora_aluno atual se tiver, para tratar
     * o caso da prova estar sendo corrigida pela segunda vez.
     * Realiza a leitura dos aquivos de gabarito da integradora
     * Realiza a correcao das provas dos alunos de acordo com o gabarito
     * ao fim monta um array com todos acertos dos alunos na diciplinas
     * e insere os registros na tabela acadperiodo__integradora_aluno
     * @param $caminhoArq
     * @return array
     * @throws \Exception
     */
    public function corrigeProvaPorArquivoAlunos($caminhoArq)
    {
        $serviceAlunoper      = (new \Matricula\Service\AcadperiodoAluno($this->getEm()));
        $serviceAlunoDisc     = (new \Matricula\Service\AcadperiodoAlunoDisciplina($this->getEm()));
        $serviceIntegradora   = (new \Matricula\Service\AcadperiodoIntegradora($this->getEm()));
        $alunosNaoEncontrados = array();

        $integradora = $serviceIntegradora->buscaUltimaIntegradoraAtiva();

        if (!$integradora) {
            throw new \Exception("Nenhuma Integradora ativa foi encontrada!");
        }

        $this->deleteCorrecoesAlunosPorIntegradora($integradora->getIntegradoraId());

        $quantidadeQuestoes = $integradora->getIntegconf()->getIntegconfQuestoes();

        $provas = $serviceIntegradora->buscaProvasPorTurnoIntegradora($integradora);

        for ($i = 0; $i < sizeof($caminhoArq); $i++) {
            $gabaritos = file($caminhoArq[$i]);

            $alunosNaoEncontrados = array();
            $k                    = 0;
            foreach ($gabaritos as $gabatiro) {
                $k++;
                $linhaProcessada = substr($gabatiro, 40);
                $linhaProcessada = str_replace(' ', '*', $linhaProcessada);
                $matricula       = (int)trim(substr($linhaProcessada, 0, 5));

                $alunoper = $serviceAlunoper->buscaAlunoperiodoPorMatricula($matricula, $integradora->getPer()->getPerId());

                if ($alunoper) {
                    $turnoAluno = $alunoper->getTurma()->getTurmaTurno();

                    $disciplinasArray = $provas[$turnoAluno]['periodo'][$alunoper->getTurma()->getTurmaSerie()]['gabarito'][$i];
                    $totalquestoes    = $quantidadeQuestoes * sizeof($disciplinasArray);

                    $gabariroCadernoAluno = trim(substr($linhaProcessada, 5, $totalquestoes + 5));
                    $integradoraAlunos[] = call_user_func(function ($disciplinasNotas) use ($alunoper, $disciplinasArray, $gabariroCadernoAluno, $quantidadeQuestoes, $integradora, $serviceAlunoDisc) {
                        $ordemGeral = 1;
                        for ($i = 0; $i < sizeof($disciplinasArray); $i++) {
                            $alunoDisc = $serviceAlunoDisc->buscasAlunoDiscPorAlunoEDisciplina(  $alunoper->getAlunoperId(), (int)$disciplinasArray[$i]['disc_id']);
                            if ($alunoDisc) {
                                $disciplinasNotas[$i]['alunodisc'] = $alunoDisc;
                                $gabatiroDisciplinaAluno = trim(substr($gabariroCadernoAluno, ($i * $quantidadeQuestoes), $quantidadeQuestoes));
                                $disciplinasNotas[$i]['alunointegRespostas'] = $gabatiroDisciplinaAluno;
                                $acertos = 0;
                                for ($j = 1; $j <= $quantidadeQuestoes; $j++) {
                                    $ordemInvalida = '';
                                    foreach ($disciplinasArray[$i]['gabarito'] as $gabaritoAux) {
                                        if ($gabaritoAux['ordem'] == $ordemGeral) {
                                            if ($gabaritoAux['valida'] == 'Não' && $ordemInvalida != $gabaritoAux['ordem']) {
                                                $acertos++;
                                                $ordemInvalida = $gabaritoAux['ordem'];
                                            } else {
                                                if ($gabaritoAux['resposta'] == $gabatiroDisciplinaAluno[$j - 1]) {
                                                    $acertos++;
                                                }
                                            }
                                        }
                                    }
                                    $ordemGeral++;
                                }
                                $disciplinasNotas[$i]['alunointegAcertos']     = $acertos;
                                $disciplinasNotas[$i]['alunointegNotaIsolada'] = $this->calculaNotaIsolada(
                                    $acertos
                                );
                                $disciplinasNotas[$i]['integradora']           = $integradora;
                            } else{
                                $ordemGeral += 10;
                            }
                        }

                        return $disciplinasNotas;
                    }
                    );
                } else {
                    $alunosNaoEncontrados[] = [
                        'matricula' => $matricula,
                        'periodo'   => $integradora->getPer()->getPerId()
                    ];
                }
            }
        }

        $countDuplicados = 0;
        $arrResult       = array('alunosNaoEncontrados' => $alunosNaoEncontrados);
        foreach ($integradoraAlunos as $integradoraAluno) {
            if ($integradoraAluno) {
                try {
                    $this->save($integradoraAluno, false);
                } catch (\Exception $ex) {
                    $arrResult['message'] = "Houve um erro durante a correção ";

                    /**
                     * verifica se erro causado é de duplicação de chave,
                     * pro caso se for realizado mais de uma leitura de respostas de um aluno
                     */
                    if ($ex->getCode() == 1062) {
                        $arrResult['duplicados'] = $countDuplicados++;
                    }
                }
            }
        }
        $this->getEm()->flush();
        $this->getEm()->clear();

        return $arrResult;
    }

    /**
     * Método deleta todos dados de uma integradora do sistema
     * @param $idIntegradora
     */
    public function deleteCorrecoesAlunosPorIntegradora($idIntegradora)
    {
        if ($idIntegradora) {
            $query = "
              DELETE FROM acadperiodo__integradora_aluno WHERE integradora_id = {$idIntegradora}
            ";
            $this->executeQuery($query);
        }
    }

    /**
     * Método verifia se uma integradora ja foi corrigida se sim retorna 1 se não 0.
     * @param $idIntegradora
     * @return int|null
     */
    public function verificarSeAIntegradoraJaFoiCorrigida($idIntegradora)
    {
        if ($idIntegradora) {
            $result = $this->getRepository()->findBy(['integradora' => $idIntegradora]);

            if ($result) {
                return 1;
            }

            return 0;
        }

        return null;
    }

    /**
     * Método resposável por salvar os dados dos alunos na integradora,
     * aceita com multiplos dados de alunos e integradoras
     *
     * @param array     $arrIntegradoraAlunos : array com dados das respostas do aluno na integradora
     * @param bool|true $flush : parâmetro opcional para salvar de fato no banco de dados, padrão valor true
     * @return array retorna um array de objetos criados
     */
    public function save(array $arrIntegradoraAlunos, $flush = true)
    {
        $srvEtapaAluno = new \Professor\Service\AcadperiodoEtapaAluno($this->getEm());
        $arrResult     = array();

        if ($flush) {
            $this->begin();
        }

        // Verifica se é um array com multiplas integradoraAluno pra salvar(se é um array de arrays)
        if (count($arrIntegradoraAlunos) != count($arrIntegradoraAlunos, COUNT_RECURSIVE)) {
            foreach ($arrIntegradoraAlunos as $integradoraAluno) {
                $objIntegradoraAluno = $this->prepareObjectIntegradoraAluno($integradoraAluno, true);

                $this->getEm()->persist($objIntegradoraAluno);
                $arrResult[] = $objIntegradoraAluno;
            }
        } else {
            $objIntegradoraAluno = $this->prepareObjectIntegradoraAluno($arrIntegradoraAlunos, true);
            $this->getEm()->persist($objIntegradoraAluno);

            $arrResult[] = $objIntegradoraAluno;
        }

        if ($flush) {
            $this->getEm()->flush();
            $this->getEm()->clear();
            $this->commit();

            $session = new \Zend\Session\Container('Zend_Auth');
            $usuario = $this->getReference($session->storage['id'], 'Acesso\Entity\AcessoPessoas');

            // atualizar as notas da terceira etapa dos alunos
            foreach ($arrResult as $r) {
                $objIntegradora = $r->getIntegradora();

                if ($objIntegradora->getIntegradoraStatus() == "Encerrada") {
                    $alunoDisc = $r->getAlunodisc();
                    $alunoPer  = $alunoDisc->getAlunoper();

                    $integradoraAtualizada = $this->executeQueryWithParam(
                        "SELECT * FROM  view__integradora WHERE alunoper_id = :alunoPeriodo AND alunodisc_id = :alunoDisc",
                        array(
                            'alunoPeriodo' => $alunoPer->getAlunoperId(),
                            'alunoDisc'    => $alunoDisc->getAlunodiscId(),
                        )
                    )->fetch();

                    if ($integradoraAtualizada['NFD'] > $integradoraAtualizada['NDI']) {
                        $notaFinal = $integradoraAtualizada['NFD'];
                    } else {
                        $notaFinal = $integradoraAtualizada['NDI'];
                    }

                    // busca etapa de integradora do aluno para atualizar nota
                    $etapaIntegradora = $srvEtapaAluno->findNotasAlunoEtapas($alunoDisc->getAlunodiscId(), [3])[0];

                    // criar etapa aqui se não existir registro
                    if ($etapaIntegradora) {
                        $etapaIntegradora->setAlunoetapaNota($notaFinal);
                        $etapaIntegradora->setUsuario($usuario);

                        $this->getEm()->persist($etapaIntegradora);
                        $this->getEm()->flush();
                    }
                }
            }
        }

        return $arrResult;
    }

    /**
     * Método para preparar o objeto antes de persistir com os novos dados ou criar
     * @param array      $arrIntegraAluno : array com os dados da Integradora do Aluno para persistir
     * @param bool|false $saveHistory : se é para salvar historico do registro
     * @return \Matricula\Entity\AcadperiodoIntegradoraAluno : Objeto de IntegradoraAluno pronto pra ser persistido
     */
    protected function prepareObjectIntegradoraAluno(array $arrIntegraAluno, $saveHistory = false)
    {
        if ($arrIntegraAluno['alunointegId']) {
            $integradoraAluno = $this->getReference(
                $arrIntegraAluno['alunointegId'],
                "Matricula\\Entity\\AcadperiodoIntegradoraAluno"
            );

            // verifica se é para realizar histórico do registro que será atualizado e se as respostas são diferentes
            if ($saveHistory &&
                $arrIntegraAluno['alunointegRespostas'] != $integradoraAluno->getAlunointegRespostas()
            ) {
                if (!$this->serviceHistory) {
                    $this->serviceHistory = new \Matricula\Service\AcadperiodoIntegradoraAlunoHistorico($this->getEm());
                }

                $this->serviceHistory->save($integradoraAluno);
            }
        } else {
            $integradoraAluno = new \Matricula\Entity\AcadperiodoIntegradoraAluno();
        }

        if (!is_object($arrIntegraAluno['alunodisc'])) {
            $arrIntegraAluno['alunodisc'] = $this->getReference(
                $arrIntegraAluno['alunodisc'],
                "Matricula\\Entity\\AcadperiodoAlunoDisciplina"
            );
        }

        if (!is_object($arrIntegraAluno['integradora'])) {
            $arrIntegraAluno['integradora'] = $this->getReference(
                $arrIntegraAluno['integradora'],
                "Matricula\\Entity\\AcadperiodoIntegradora"
            );
        }

        $integradoraAluno->setAlunodisc($arrIntegraAluno['alunodisc']);
        $integradoraAluno->setAlunointegRespostas($arrIntegraAluno['alunointegRespostas']);

        if (!$arrIntegraAluno['alunointegNotaIsolada']) {
            $arrIntegraAluno['alunointegNotaIsolada'] = $this->calculaNotaIsolada(
                $arrIntegraAluno['alunointegAcertos']
            );
        }

        $integradoraAluno->setAlunointegAcertos($arrIntegraAluno['alunointegAcertos']);
        $integradoraAluno->setAlunointegNotaIsolada($arrIntegraAluno['alunointegNotaIsolada']);
        $integradoraAluno->setIntegradora($arrIntegraAluno['integradora']);

        return $integradoraAluno;
    }

    /**
     * Método para calcular nota isolada na disciplina
     * de acordo com a quantidade de acertos
     * @param int $acertosDisc : Quantidade de acertos naquela Disciplina
     * @return int Nota Isolada na Disciplina
     */
    protected function calculaNotaIsolada($acertosDisc)
    {
        if (!$this->confPontosIntegradora) {
            $this->confPontosIntegradora = $this->getRepository("Matricula\\Entity\\AcadperiodoIntegradoraConf")
                ->findOneBy(['perFinal' => null]);
        }

        $confPontosIntegradora = $this->confPontosIntegradora->getIntegconfPontos() / 10;

        return $acertosDisc * $confPontosIntegradora;
    }
}