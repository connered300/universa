<?php


namespace Matricula\Service;


use VersaSpine\Service\AbstractService;

class AcadgeralDisciplinaRequisito extends AbstractService
{
    const PRE_REQUISITO = 'Pre-Requisito';
    const CO_REQUISITO  = 'Co-Requisito';

    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadgeralDisciplinaRequisito');
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($arrParam)
    {
        $errors = array();
        if (!$arrParam['discreqOrigem']) {
            $errors[] = 'Por favor informe a disciplina de origem!';
        }

        if (!$arrParam['discreqDestino']) {
            $errors[] = 'Por favor informe a disciplina de destino!';
        }
        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }
        return true;
    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {
    }

    public function buscaPreRequisito($discId)
    {
        return $this->getRepository($this->getEntity())->findBy(['discreqOrigem' => $discId, 'discreqTipo' => self::PRE_REQUISITO]);
    }

    public function buscaCoRequisito($discId)
    {
        return $this->getRepository($this->getEntity())->findBy(['discreqOrigem' => $discId, 'discreqTipo' => self::CO_REQUISITO]);
    }

    /**
     * Adicionando os requisitos da disciplina
     * @param array $arrDados
     * @param string $tipo
     * @return bool
     * @throws \Exception
     */
    public function save(array $arrDados, $tipo = self::PRE_REQUISITO)
    {
        if(!$this->valida($arrDados)){
            return false;
        }

        $serviceAcadgeralDisciplinaCurso = new \Matricula\Service\AcadgeralDisciplinaCurso($this->getEm());
        $objAcadgeralDisciplinaRequisito = new \Matricula\Entity\AcadgeralDisciplinaRequisito();

        try{

            if( $arrDados['discreqId'] ){
                $objAcadgeralDisciplinaRequisito = $this->getRepository()->find($arrDados['discreqId']);
            }

            $repositoryDiscCurso = $serviceAcadgeralDisciplinaCurso->getRepository();

            $objAcadgeralDisciplinaRequisito->setDiscreqOrigem($repositoryDiscCurso->find($arrDados['discreqOrigem']));
            $objAcadgeralDisciplinaRequisito->setDiscreqDestino($repositoryDiscCurso->find($arrDados['discreqDestino']));

            $objAcadgeralDisciplinaRequisito->setDiscreqTipo($tipo);

            $this->getEm()->persist($objAcadgeralDisciplinaRequisito);
            $this->getEm()->flush($objAcadgeralDisciplinaRequisito);

        } catch (\Exception $exception){
            return false;
        }
        return true;
    }


    /**
     * Método para remover uma disciplina de requisito vinculado à uma AcadgeralDisciplinaCurso
     * de acordo com o tipo de requisito passado
     * 
     * @param int    $discId ID de uma disciplina
     * @param string $type   Tipo de requisito da disciplina 
     * 
     * @return false|int : retorna false se tipo requisito passado não estiver dentre as opções
     *                     retorna um inteiro que é o número de linhas alteradas na tabela com a remoção 
     */
    public function removeRequisito($discCursoId, $type)
    {
        if (in_array($type, self::getTipoRequisitos())) {
            $delete = "DELETE \Matricula\Entity\AcadgeralDisciplinaRequisito discReq WHERE discReq.discreqOrigem = :discCursoId AND discReq.discreqTipo = :type";
        
            $delete = $this->getEm()->createQuery($delete);
            $delete->setParameter('discCursoId', $discCursoId);
            $delete->setParameter('type', $type);

            return $delete->getResult();
        }

        return false;
    }

    /**
     * Retorna Tipos de Requisitos
     * 
     * @return array
     */
    public static function getTipoRequisitos()
    {
        return [
            self::PRE_REQUISITO,
            self::CO_REQUISITO,
        ];
    }
}