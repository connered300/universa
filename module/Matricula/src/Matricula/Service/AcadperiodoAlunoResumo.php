<?php

namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

class AcadperiodoAlunoResumo extends AbstractService
{
    const RESALUNO_SITUACAO_APROVADO     = 'Aprovado';
    const RESALUNO_SITUACAO_REPROVADO    = 'Reprovado';
    const RESALUNO_SITUACAO_EQUIVALENCIA = 'Equivalencia';
    const RESALUNO_SITUACAO_CANCELADO    = 'Cancelado';
    /* TODO: remover constantes RESALUNO_SITUACAO_DISPENSADO, RESALUNO_SITUACAO_EFINAL, RESALUNO_SITUACAO_PR */
    const RESALUNO_SITUACAO_DISPENSADO = 'DISPENSADO';
    const RESALUNO_SITUACAO_EFINAL     = 'E.FINAL';
    const RESALUNO_SITUACAO_PR         = 'PR';

    const RESALUNO_ORIGEM_LEGADO        = 'Legado';
    const RESALUNO_ORIGEM_TRANSFERENCIA = 'Transferência';
    const RESALUNO_ORIGEM_NOVO_TITULO   = 'Novo título';
    const RESALUNO_ORIGEM_MANUAL        = 'Manual';
    const RESALUNO_ORIGEM_SISTEMA       = 'Sistema';
    const RESALUNO_ORIGEM_WEBSERVICE    = 'WebService';
    private $lastError = null;

    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadperiodoAlunoResumo');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    public function buscaDisciplinasAluno($matricula, $params = array(), $criterios = array(), $query = null)
    {
        $and = array();

        if ($params['ano']) {
            $and[] = "U.resalunoAno = {$params['ano']}";
        }

        if ($params['semestre']) {
            $and[] = "U.resalunoSemestre = {$params['semestre']}";
        }

        if ($params['situacao']) {
            $and[] = "U.resalunoSituacao like '%{$params['situacao']}%'";
        }

        if ($query) {
            $sql = 'SELECT D
                FROM ($query) U
                JOIN Matricula\Entity\AcadgeralDisciplina D WITH D.discId = U.discId';
        } else {
            $sql = 'SELECT D
                FROM ' . $this->getEntity() . ' U
                JOIN Matricula\Entity\AcadgeralDisciplina D WITH D.discId = U.discId';
        }

        $sql .= " WHERE U.alunocursoId = $matricula";

        if ($and) {
            $sql .= " AND " . implode('AND', $and);
        }

        foreach ($criterios as $criterio) {
            $sql .= " $criterio";
        }
        $query       = $this->getEm()->createQuery($sql);
        $disciplinas = $query->getResult();

        foreach ($disciplinas as $pos => $disciplina) {
            $disciplinas[$pos] = $disciplina->toArray();
        }

        return $disciplinas;
    }

    public function buscaDependenciasAluno($matricula, $discId)
    {
        if (!is_null($matricula)) {
            $matricula = "AND alunocurso_id = {$matricula}";
        }

        if (!is_null($discId)) {
            $discId = "AND disc_id = {$discId}";
        }

        $query = "SELECT
                        pes_id,
                        pes_nome,
                        alunocurso_id,
                        disc_id discId,
                        disc_nome discNome,
                        resaluno_serie,
                        resaluno_nota,
                        resaluno_faltas,
                        resaluno_ano,
                        resaluno_semestre
                    FROM
                        (SELECT
                            *
                        FROM
                            (SELECT
                            *
                        FROM
                            acadperiodo__aluno_resumo
                            WHERE 1
                            {$matricula}
                            {$discId}
                        ORDER BY resaluno_situacao , resaluno_ano , resaluno_semestre DESC) agroup
                        GROUP BY alunocurso_id , disc_id) reprovacoes
                            NATURAL JOIN
                        acadgeral__disciplina
                            NATURAL JOIN
                        acadgeral__aluno_curso
                            NATURAL JOIN
                        acadgeral__aluno
                            NATURAL JOIN
                        pessoa
                    WHERE
                        resaluno_situacao = 'Reprovado'
                    ORDER BY discId;";

        return $this->executeQuery($query)->fetchAll();
    }

    public function registraResumoAlunoDisciplinaDesligamento(
        \Matricula\Entity\AcadperiodoAlunoDisciplina $objAcadperiodoAlunoDisciplina
    ) {
        $serviceCursoConfig          = new \Matricula\Service\AcadCursoConfig($this->getEm());
        $serviceFrequencia           = new \Professor\Service\AcadperiodoFrequencia($this->getEm());
        $serviceEtapaAluno           = new \Professor\Service\AcadperiodoEtapaAluno($this->getEm());
        $serviceAlunoResumo          = new \Matricula\Service\AcadperiodoAlunoResumo($this->getEm());
        $serviceMatrizDisciplina     = new \Matricula\Service\AcadperiodoMatrizDisciplina($this->getEm());
        $serviceAlunoDisciplinaFinal = new \Matricula\Service\AcadperiodoAlunoDisciplinaFinal($this->getEm());
        $serviceDocenteDisciplina    = new \Matricula\Service\AcadperiodoDocenteDisciplina($this->getEm());

        $arrSituacoesAtividade = \Matricula\Service\AcadgeralSituacao::situacoesAtividade();
        $objCursoConfig        = $serviceCursoConfig->getRepository()->findOneBy(
            array('curso' => $objAcadperiodoAlunoDisciplina->getTurma()->getCursocampus()->getCurso())
        );

        /* @var $objDocenteDisciplina \Matricula\Entity\AcadperiodoDocenteDisciplina */
        $objAlunoper   = $objAcadperiodoAlunoDisciplina->getAlunoper();
        $objAlunocurso = $objAlunoper->getAlunocurso();

        /* @var $objDocenteDisciplina \Matricula\Entity\AcadperiodoDocenteDisciplina */
        $objDocenteDisciplina = $serviceDocenteDisciplina->getRepository()->findOneBy(
            array(
                'turma' => $objAcadperiodoAlunoDisciplina->getTurma(),
                'disc'  => $objAcadperiodoAlunoDisciplina->getDisc(),
            ),
            ['docdiscDataFim' => 'desc']
        );

        if (!$objDocenteDisciplina) {
            //desconsidera disciplina pois não tem docente vinculado
            return true;
        }

        try {
            $objMatrizDisciplina = $serviceMatrizDisciplina->getRepository()->findOneBy(
                ['disc' => $objAcadperiodoAlunoDisciplina->getDisc()->getDiscId()]
            );

            if (!in_array($objAcadperiodoAlunoDisciplina->getSituacao()->getSituacaoId(), $arrSituacoesAtividade)) {
                return true;
            }

            $arrObjEtapaAluno = $serviceEtapaAluno->getRepository()->findBy(
                ['alunodisc' => $objAcadperiodoAlunoDisciplina->getAlunodiscId()]
            );

            $arrFrequencia = $serviceFrequencia->buscaFaltasDisciplina(
                $objAcadperiodoAlunoDisciplina->getAlunodiscId(),
                true
            );
            $faltas        = (int)$arrFrequencia['quant'];

            $notaGeral = 0;

            if ($arrObjEtapaAluno) {
                /* @var $objEtapaAluno \Professor\Entity\AcadperiodoEtapaAluno */
                foreach ($arrObjEtapaAluno as $objEtapaAluno) {
                    $notaGeral += $objEtapaAluno->getAlunoetapaNota();
                }

                /* @var $objAlunoDisciplinaFinal \matricula\Entity\AcadperiodoAlunoDisciplinaFinal */
                $objAlunoDisciplinaFinal = $serviceAlunoDisciplinaFinal->getRepository()->findOneBy(
                    ['alunodisc' => $objAcadperiodoAlunoDisciplina->getAlunodiscId()]
                );
            } else {
                //professor não lançou nota para um aluno!
                $notaGeral = 0;
            }

            if ($notaGeral >= $objCursoConfig->getCursoconfigNotaMin()) {
                $situacao = 'APROVADO';
            } else {
                $situacao = 'REPROVADO';
            }

            $objAlunoResumo = $serviceAlunoResumo->getRepository()->findOneBy(
                [
                    'alunocursoId'     => $objAlunocurso->getAlunocursoId(),
                    'discId'           => $objDocenteDisciplina->getDisc()->getDiscId(),
                    'resalunoSemestre' => $objDocenteDisciplina->getTurma()->getPer()->getPerSemestre(),
                    'resalunoAno'      => $objDocenteDisciplina->getTurma()->getPer()->getPerAno()
                ]
            );

            if (!$objAlunoResumo) {
                $objAlunoResumo = new \Matricula\Entity\AcadperiodoAlunoResumo();
                $objAlunoResumo->setResalunoDataCriacao(new \DateTime('now'));
            }

            $objAlunoResumo
                ->setAlunocursoId($objAlunocurso->getAlunocursoId())
                ->setDiscId($objAcadperiodoAlunoDisciplina->getDisc()->getDiscId())
                ->setResalunoSerie($objAlunoper->getTurma()->getTurmaSerie())
                ->setResalunoSituacao($situacao)
                ->setResalunoNota($notaGeral)
                ->setResalunoFaltas($faltas)
                ->setResalunoDataAlteracao(new \DateTime('now'))
                ->setResalunoCargaHoraria($objMatrizDisciplina->getPerDiscChteorica())
                ->setResalunoAno($objDocenteDisciplina->getTurma()->getPer()->getPerAno())
                ->setResalunoSemestre($objDocenteDisciplina->getTurma()->getPer()->getPerSemestre())
                ->setAlunoper($objAlunoper);

            if ($objAlunoDisciplinaFinal && $objAlunoDisciplinaFinal->getAlunofinalNota()) {
                $objAlunoResumo->setResalunoNota($notaGeral);
            }

            $this->getEm()->persist($objAlunoResumo);
            $this->getEm()->flush($objAlunoResumo);
        } catch (\Exception $ex) {
            return false;
        }

        return true;
    }

    public function relatorioHistorico($matricula)
    {
        if (is_null($matricula)) {
            return array();
        }

        $query = "SELECT * FROM(SELECT DISTINCT
            edicao_agendado tipo_agendado,
            date_format(inscricao_data_horario_prova, '%d/%m/%Y') data_prova,
            IFNULL(edicao_ano,infoLegado.alunoingresso_vestibular_ano) edicao_ano,
            IF(IFNULL(edicao_semestre,infoLegado.alunoingresso_vestibular_semestre) IS NULL, '',if(IFNULL(edicao_semestre,infoLegado.alunoingresso_vestibular_semestre) = 'Segundo',2,1)) edicao_semestre,
            IFNULL(inscricao_cursos.inscricao_classificacao,infoLegado.alunoingresso_vestibular_classificacao)  classificacao,
            IFNULL(inscricao_cursos.inscricao_nota,infoLegado.alunoingresso_vestibular_pontuacao) pontuacao,
            formacaoEnsinoMedio.formacao_instituicao intituicao_ensino_medio,
            formacaoEnsinoMedio.formacao_ano ano_ensino_medio,
            formacaoEnsinoMedio.formacao_cidade cidade_ensino_medio,
            formacaoEnsinoMedio.formacao_estado estado_ensino_medio,
            formacao.formacao_cidade as cidade_formacao,
            formacao.formacao_estado as estado,
			formacao.formacao_instituicao as instiuicao,
            formacao.formacao_ano as ano,
            formacao.formacao_tipo as tipo,
			res.res_id,
			res.resaluno_serie,
			CONCAT(res.resaluno_semestre, ' / ', res.resaluno_ano) resaluno_pl,
			res.resaluno_semestre,
			res.resaluno_ano,
			disc.disc_id,
		    IF(equivalencia_id is null, disc.disc_nome, disc_equivalencia.disc_nome ) disc_nome,
			res.resaluno_nota,
			res.resaluno_faltas,
			res.resaluno_situacao,
			res.resaluno_carga_horaria,
			res.resaluno_origem,
            if(res.resaluno_situacao = 'dispensado','DISP',res.resaluno_nota) nota,

           IF(
               formacao.formacao_id,
               CONCAT(CONVERT(formacao.formacao_instituicao USING UTF8),' / ', CONVERT(formacao.formacao_curso using utf8)),
               CONCAT(CONVERT(camp_nome USING UTF8),' / ', CONVERT(curso_nome using utf8))
           )  as res_insituicao,
           disc_equivalencia.disc_nome as disc_nome_,
           disc_equivalencia.disc_id disc_id_,
           res.alunoper_id
       FROM acadgeral__aluno AS aluno
       INNER JOIN acadgeral__aluno_curso AS aluno_curso USING(aluno_id)
	   LEFT JOIN acadgeral__aluno_formacao AS formacaoEnsinoMedio ON aluno.aluno_id = formacaoEnsinoMedio.aluno_id and  formacaoEnsinoMedio.formacao_curso = 'Ensino Medio'
       INNER JOIN acadperiodo__aluno_resumo AS res USING(alunocurso_id)
       LEFT JOIN acadgeral__disciplina AS disc USING(disc_id)
       LEFT JOIN acadgeral__disciplina_curso AS disc_curso1 ON disc_curso1.disc_id=disc.disc_id

       -- Lançamentos manuais no sistema
       LEFT JOIN acadperiodo__resumo_equivalencia AS equivalencia USING(res_id)
       LEFT JOIN acadgeral__aluno_formacao AS formacao ON aluno.aluno_id = formacao.aluno_id  and formacao.formacao_curso != 'Ensino Médio'
       LEFT JOIN acadgeral__disciplina AS disc_equivalencia ON disc_equivalencia.disc_id=equivalencia.disc_id
           LEFT JOIN acadgeral__disciplina_curso AS disc_curso2 ON disc_curso2.disc_id=disc_equivalencia.disc_id


       -- curso do aluno
       LEFT JOIN campus_curso USING(cursocampus_id)
       LEFT JOIN org_campus campus USING(camp_id)
       LEFT JOIN acad_curso curso ON curso.curso_id=campus_curso.curso_id

	   -- VESTIBULAR
       LEFT JOIN selecao_inscricao as selecaoIns on selecaoIns.pes_id = aluno.pes_id
       LEFT JOIN selecao_edicao as selecaoEd USING(edicao_id)
      LEFT JOIN inscricao_cursos USING (inscricao_id)
       LEFT JOIN acadgeral__aluno_ingresso_legado AS infoLegado on infoLegado.alunocurso_id = aluno_curso.alunocurso_id
       WHERE
           (res.alunocurso_id = {$matricula} or aluno_curso.alunocurso_id = {$matricula}) AND res.resaluno_situacao NOT IN('Cancelado')
        ORDER BY res.resaluno_situacao asc
) as t  WHERE resaluno_situacao NOT IN('Cancelado')
       GROUP BY  t.disc_nome
       ORDER BY resaluno_serie, resaluno_ano, resaluno_semestre, disc_nome ASC;";

        return $this->executeQuery($query)->fetchAll();
    }

    public function historicoAluno($data)
    {
        $alunocursoId = $data['alunocursoId'];

        $query = "
                SELECT DISTINCT
                    edicao_ano,
                    edicao_semestre,
                    formacaoEnsinoMedio.formacao_instituicao intituicao_ensino_medio,
                    formacaoEnsinoMedio.formacao_ano ano_ensino_medio,
                    formacaoEnsinoMedio.formacao_cidade AS cidade_formacao,
                    formacaoEnsinoMedio.formacao_estado AS estado,
                    IFNULL(formacao.formacao_instituicao,campus.camp_nome) AS instiuicao,
                    formacao.formacao_ano AS ano,
                    formacao.formacao_tipo AS tipo,
                    formacao.*,
                    res.res_id,
                    res.resaluno_serie,
                    CONCAT(res.resaluno_semestre, ' / ', res.resaluno_ano) resaluno_pl,
                    res.resaluno_semestre,
                    res.resaluno_ano,
                    disc.disc_id,
                    disc.disc_nome,
                    res.resaluno_nota,
                    res.resaluno_faltas,
                    res.resaluno_situacao,
                    res.resaluno_carga_horaria,
                    res.resaluno_origem,
                    if(res.resaluno_situacao = 'dispensado','DISP',res.resaluno_nota) nota,

                   IF(
                       formacao.formacao_id,
                       CONCAT(CONVERT(formacao.formacao_instituicao USING UTF8),' / ', CONVERT(formacao.formacao_curso USING utf8)),
                       CONCAT(CONVERT(camp_nome USING UTF8),' / ', CONVERT(curso_nome USING utf8))
                   )  AS res_insituicao,
                   GROUP_CONCAT(DISTINCT disc_equivalencia.disc_nome SEPARATOR '||') AS res_equivalencias,
                   GROUP_CONCAT(DISTINCT disc_equivalencia.disc_id SEPARATOR '||') AS res_equivalencias_id,
                   disc_equivalencia.disc_nome AS disc_nome_,
                   disc_equivalencia.disc_id disc_id_,
                   res.alunoper_id
               FROM acadgeral__aluno AS aluno
               INNER JOIN acadgeral__aluno_curso AS aluno_curso USING(aluno_id)
               LEFT JOIN acadgeral__aluno_formacao AS formacaoEnsinoMedio ON aluno.aluno_id = formacaoEnsinoMedio.aluno_id AND  formacaoEnsinoMedio.formacao_curso = 'Ensino Medio'
               INNER JOIN acadperiodo__aluno_resumo AS res USING(alunocurso_id)
               LEFT JOIN acadgeral__disciplina AS disc USING(disc_id)
               LEFT JOIN acadgeral__disciplina_curso AS disc_curso1 ON disc_curso1.disc_id=disc.disc_id

               -- Lançamentos manuais no sistema
               LEFT JOIN acadperiodo__resumo_equivalencia AS equivalencia USING(res_id)
               LEFT JOIN acadgeral__aluno_formacao AS formacao ON equivalencia.formacao_id = formacao.formacao_id
               LEFT JOIN acadgeral__disciplina AS disc_equivalencia ON disc_equivalencia.disc_id=equivalencia.disc_id
               LEFT JOIN acadgeral__disciplina_curso AS disc_curso2 ON disc_curso2.disc_id=disc_equivalencia.disc_id


               -- curso do aluno
               LEFT JOIN campus_curso USING(cursocampus_id)
               LEFT JOIN org_campus campus USING(camp_id)
               LEFT JOIN acad_curso curso ON curso.curso_id=campus_curso.curso_id

               -- VESTIBULAR
               LEFT JOIN selecao_inscricao AS selecaoIns ON selecaoIns.pes_id = aluno.pes_id
               LEFT JOIN selecao_edicao AS selecaoEd USING(edicao_id)
       WHERE
            res.alunocurso_id = " . $alunocursoId . "
		GROUP BY res_id
        ORDER BY resaluno_serie, resaluno_semestre, resaluno_ano";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArrSelect2ResalunoSituacao($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getResalunoSituacao());
    }

    public static function getResalunoSituacao()
    {
        return array(
            self::RESALUNO_SITUACAO_APROVADO,
            self::RESALUNO_SITUACAO_DISPENSADO,
            // TODO: analisar remoção das situações abaixo
            //self::RESALUNO_SITUACAO_EFINAL,
            //self::RESALUNO_SITUACAO_PR
            //self::RESALUNO_SITUACAO_CANCELADO,
            //self::RESALUNO_SITUACAO_EQUIVALENCIA,
            self::RESALUNO_SITUACAO_REPROVADO,
        );
    }

    public function save($alunoResumo)
    {
        $validated = $this->valida($alunoResumo);

        if ($validated) {
            return ['type' => 'error', 'message' => implode(',', $validated)];
        }

        $serviceDisciplina         = new \Matricula\Service\AcadgeralDisciplina($this->getEm());
        $serviceAlunoDisciplina    = new \Matricula\Service\AcadperiodoAlunoDisciplina($this->getEm());
        $serviceAlunoResumo        = new \Matricula\Service\AcadperiodoAlunoResumo($this->getEm());
        $serviceResumoEquivalencia = new \Matricula\Service\AcadperiodoResumoEquivalencia($this->getEm());
        $serviceSituacao           = new \Matricula\Service\AcadgeralSituacao($this->getEm());
        $serviceAcadgeralDisciplinaCurso = new \Matricula\Service\AcadgeralDisciplinaCurso($this->getEm());
        $serviceAcadgeralDisciplinaTipo  = new \Matricula\Service\AcadgeralDisciplinaTipo($this->getEm());

        $this->begin();

        if ($alunoResumo['resId']) {
            /* @var $objAlunoResumo \Matricula\Entity\AcadperiodoAlunoResumo */
            $objAlunoResumo = $serviceAlunoResumo->getRepository()->find($alunoResumo['resId']);
        } else {
            $objAlunoResumo = new \Matricula\Entity\AcadperiodoAlunoResumo();
        }

        /* Esta alteração foi necessária para que os usuários lancem notas de atividades complementares,
             quando gerenciarmos está situação, poderemos reativar a obrigatoriedade.
        */
        if ($alunoResumo['resalunoNota'] == '') {
            $alunoResumo['resalunoNota'] = null;
        }

        if (is_numeric($alunoResumo['discId'])) {
            /* @var $objDisciplina \Matricula\Entity\AcadgeralDisciplina */
            $objDisciplina = $serviceDisciplina->getRepository()->find($alunoResumo['discId']);
        } else {
            /* @var $objDisc \Matricula\Entity\AcadgeralDisciplina */
            $objDisciplina = $serviceDisciplina->getRepository()->findOneBy(['discNome' => $alunoResumo['discId']]);
            if (!$objDisciplina) {


                $serviceAlunoCurso = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());

                $arrDisciplinaAux = [
                    'discNome'  => $alunoResumo['discId'],
                    'discSigla' => $serviceDisciplina->criarSiglaProvisoria($alunoResumo['discId']),
                ];

                /* @var $objAlunoCurso \Matricula\Entity\AcadgeralAlunoCurso */
                if ($objAlunoCurso = $serviceAlunoCurso->find($alunoResumo['alunocursoId'])) {
                    $cursoId  = (int) $objAlunoCurso->getCursocampus()->getCurso()->getCursoId();
                    $arrDisciplinaAux['curso'][$cursoId] = [
                        'disc_ativo' => $serviceAcadgeralDisciplinaCurso::DISC_ATIVO_NAO,
                        'tdisc_id'   => $serviceAcadgeralDisciplinaTipo::DISCIPLINA_TIPO_EQUIVALENCIA,
                        'curso_id'   => $cursoId,
                    ];
                }

                // cadastrando a disciplina de equivalencia do aluno,
                // como é um registro apenas para histórico, não deve ser 'ativa'
                $objDisciplina = $serviceDisciplina->save($arrDisciplinaAux);
            }
        }

        if (!$alunoResumo['resalunoDataCriacao']) {
            $alunoResumo['resalunoDataCriacao'] = new \DateTime('now');
        }

        $alunoResumo['resalunoDataAlteracao'] = new \DateTime('now');

        $objAlunoResumo
            ->setAlunocursoId($alunoResumo['alunocursoId'])
            ->setDiscId($objDisciplina->getDiscId())
            ->setResalunoSerie($alunoResumo['resalunoSerie'])
            ->setResalunoDataCriacao($alunoResumo['resalunoDataCriacao'])
            ->setResalunoSituacao($alunoResumo['resalunoSituacao'])
            ->setResalunoNota($alunoResumo['resalunoNota'])
            ->setResalunoFaltas((int)$alunoResumo['resalunoFaltas'])
            ->setResalunoDataAlteracao($alunoResumo['resalunoDataAlteracao'])
            ->setResalunoCargaHoraria($alunoResumo['resalunoCargaHoraria'])
            ->setResalunoAno($alunoResumo['resalunoAno'])
            ->setResalunoSemestre($alunoResumo['resalunoSemestre'])
            ->setAlunoper($alunoResumo['alunoper'])
            ->setResalunoOrigem($alunoResumo['resalunoOrigem']);

        try {
            $this->getEm()->persist($objAlunoResumo);
            $this->getEm()->flush($objAlunoResumo);
        } catch (\Exception $e) {
            return ['type' => 'error', 'message' => 'Não foi possível salvar o registro de resumo do aluno'];
        }

        if ($alunoResumo['equivalencias']) {
            $arrObjResumoEq = $serviceResumoEquivalencia->getRepository()->findBy(
                ['res' => $objAlunoResumo->getResId()]
            );

            // remove as equivalencias anteriores vinculadas
            foreach ($arrObjResumoEq as $objResumoEq) {
                $this->getEm()->remove($objResumoEq);
            }

            $serviceAlunoCurso = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
            $arrDisciplinaAux = [];

            /* @var $objAlunoCurso \Matricula\Entity\AcadgeralAlunoCurso */
            if ($objAlunoCurso = $serviceAlunoCurso->find($alunoResumo['alunocursoId'])) {
                $cursoId  = (int) $objAlunoCurso->getCursocampus()->getCurso()->getCursoId();
                $arrDisciplinaAux['curso'][$cursoId] = [
                    'disc_ativo' => $serviceAcadgeralDisciplinaCurso::DISC_ATIVO_NAO,
                    'tdisc_id'   => $serviceAcadgeralDisciplinaTipo::DISCIPLINA_TIPO_EQUIVALENCIA,
                    'curso_id'   => $cursoId,
                ];
            }


            foreach ($alunoResumo['equivalencias'] as $discId) {
                // se equivalencia não é um ID de alguma disciplina
                if (!is_numeric($discId)) {
                    // verifica se valor de equivalencia é igual ao valor da disciplina vinculada ao resumo do aluno,
                    // se for igual: vincula a mesma disciplina
                    // se não: cadastra a disciplina como equivalencia
                    if ($discId != $objDisciplina->getDiscNome()) {
                        /* @var $objDisc \Matricula\Entity\AcadgeralDisciplina */
                        $objDisciplinaEquivalencia = $serviceDisciplina->getRepository()->findOneBy(
                            ['discNome' => $alunoResumo['discId']]
                        );

                        if (!$objDisciplinaEquivalencia) {

                            $arrDisciplina['discNome'] = $discId;
                            $arrDisciplina['discSigla'] = $discId;

                            // cadastrando a disciplina de equivalencia do aluno,
                            // como é um registro apenas para histórico, não deve ser 'ativa'
                            $objDisciplina = $serviceDisciplina->save($arrDisciplinaAux);
                        }
                        $discId = $objDisciplinaEquivalencia->getDiscId();
                    } else {
                        $discId = $objDisciplina->getDiscId();
                    }
                }

                try {
                    // salva equivalencia
                    $arrEquivalencia = [
                        'formacaoId' => $alunoResumo['formacaoId'],
                        'resId'      => $objAlunoResumo->getResId(),
                        'discId'     => $discId,
                    ];
                    $serviceResumoEquivalencia->save($arrEquivalencia);

                    // alterando situação do aluno nas disciplinas de equivalencia
                    /* TODO: repensar regra
                     $serviceAlunoDisciplina->updateSituacaoDisciplinaByMatriculaAluno(
                        $serviceSituacao::DISPENSADO,
                        $discId,
                        $alunoResumo['alunocursoId']
                    );*/
                } catch (\Exception $e) {
                    return [
                        'type'    => 'error',
                        'message' => 'Não foi possível vincular as equivalências ao item de histórico do aluno'
                    ];
                }
            }
        } else {
            $serviceAlunoDisciplina->updateSituacaoDisciplinaByMatriculaAluno(
                $serviceSituacao::DISPENSADO,
                $objDisciplina->getDiscId(),
                $alunoResumo['alunocursoId']
            );
        }

        try {
            $this->getEm()->flush();
            $this->commit();
        } catch (\Exception $ex) {
            return ['type' => 'error', 'message' => 'Não foi possível salvar o item de histórico do aluno.'];
        }

        return $objAlunoResumo;
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {
        $validate      = array();
        $serviceResumo = new \Matricula\Service\AcadperiodoAlunoResumo($this->getEm());

        if ($dados['resId']) {
            $objAlunoResumo = $serviceResumo->getRepository()->find($dados['resId']);

            if (!$objAlunoResumo) {
                $validate[] = "Histórico de aluno não encontrado";
            } elseif ($objAlunoResumo->getResalunoOrigem() == $serviceResumo::RESALUNO_ORIGEM_SISTEMA) {
                $validate[] = "A origem desse historico é gerada pelo 'Sistema'. Por tanto não pode ser editada";
            }
        }

        /** @var $objAlunoResumo \Matricula\Entity\AcadperiodoAlunoResumo */
        $objAlunoResumo = $this->getRepository()->findOneBy(
            [
                'alunocursoId' => $dados['alunocursoId'],
                'discId'       => $dados['discId']
            ],
            ['resalunoAno' => 'desc', 'resalunoDataAlteracao' => 'desc']
        );

        if ($objAlunoResumo && $objAlunoResumo->getResId() != $dados['resId'] && ($objAlunoResumo->getResalunoSituacao(
                ) == $serviceResumo::RESALUNO_SITUACAO_APROVADO || $objAlunoResumo->getResalunoSituacao(
                ) == $serviceResumo::RESALUNO_SITUACAO_DISPENSADO)) {
            $validate[] = "Um registro de histórico para essa disciplina já existe para esse aluno!";
        }

        if ($dados['formacaoId'] && !$dados['equivalencias']) {
            $validate[] = "Informe pelo menos uma equivalência de displina!";
        } elseif ($dados['equivalencias'] && !$dados['formacaoId']) {
            $validate[] = "Informe o ano de formação do aluno!";
        } elseif ($dados['formacaoId'] == \Matricula\Service\AcadgeralAlunoFormacao::FORMACAO_TIPO_ENSINO_MEDIO) {
            $validate[] = "Tipo de formação não é válida - " . \Matricula\Service\AcadgeralAlunoFormacao::FORMACAO_TIPO_ENSINO_MEDIO;
        }

        if (!$dados['alunocursoId']) {
            $validate[] = "Informe a matrícula do aluno!";
        }

        if (!$dados['discId']) {
            $validate[] = "Informe a disciplina!";
        }

        if (!$dados['resalunoSerie']) {
            $validate[] = "Informe a série do aluno!";
        }

        if (!$dados['resalunoSituacao']) {
            $validate[] = "Informe a situação do aluno na disciplina!";
        } elseif (!in_array($dados['resalunoSituacao'], self::getResalunoSituacao())) {
            $validate[] = "O valor de situação não é uma opção válida!";
        }

        if (!$dados['resalunoOrigem']) {
            $validate[] = "Informe de onde se origina esse historico!";
        } elseif (!in_array($dados['resalunoOrigem'], self::getResalunoOrigem())) {
            $validate[] = "O valor de origem não é uma opção válida!";
        }

        return $validate;
    }

    public static function getResalunoOrigem()
    {
        return array(
            self::RESALUNO_ORIGEM_LEGADO,
            self::RESALUNO_ORIGEM_TRANSFERENCIA,
            self::RESALUNO_ORIGEM_NOVO_TITULO,
            self::RESALUNO_ORIGEM_MANUAL,
            self::RESALUNO_ORIGEM_SISTEMA,
            self::RESALUNO_ORIGEM_WEBSERVICE
        );
    }

    public function remover($param)
    {
        if (!$param['resId']) {
            $this->setLastError(
                'Para remover um registro de resumo do aluno é necessário especificar o código.'
            );

            return false;
        }

        try {
            /* @var $objAcadperiodoAlunoResumo \Matricula\Entity\AcadperiodoAlunoResumo */
            $objAcadperiodoAlunoResumo = $this->getRepository()->find($param['resId']);

            if ($objAcadperiodoAlunoResumo->getResalunoOrigem(
                ) == \Matricula\Service\AcadperiodoAlunoResumo::RESALUNO_ORIGEM_SISTEMA) {
                $this->setLastError(
                    'Não é possível remover registros gerado pelo sistema. ' . 'Entre em contato com o suporte.'
                );

                return false;
            }

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAcadperiodoAlunoResumo);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover item de histórico do aluno.');

            return false;
        }

        return true;
    }

    public function verificaSeAlunoCumpriuGrade($matricula)
    {
        $query = "
        SELECT count(*) as resultado
        FROM (
            SELECT disc.*
            FROM acadperiodo__aluno AS aluno
            LEFT JOIN acadperiodo__turma AS turma USING (turma_id)
            LEFT JOIN campus_curso AS campusCurso USING (cursocampus_id)
            LEFT JOIN acadperiodo__matriz_disciplina AS matDisc USING (mat_cur_id)
            LEFT JOIN acadgeral__disciplina AS disc ON disc.disc_id = matDisc.disc_id
            WHERE aluno.alunoper_id = (
                SELECT MAX(alunoperUltimo.alunoper_id)
                FROM acadperiodo__aluno alunoperUltimo
                WHERE alunoperUltimo.alunocurso_id = {$matricula}
            )
        ) AS discGrade
        LEFT JOIN (
            SELECT
                DISTINCT COALESCE(disc.disc_id, disc_equivalencia.disc_id) disc_id,
                COALESCE(disc.disc_nome, disc_equivalencia.disc_nome) disc_nome
            FROM acadgeral__aluno AS aluno
            INNER JOIN acadgeral__aluno_curso AS aluno_curso USING (aluno_id)
            INNER JOIN acadperiodo__aluno_resumo AS res USING (alunocurso_id)
            LEFT JOIN acadgeral__disciplina AS disc USING (disc_id)
            LEFT JOIN acadgeral__disciplina_curso AS disc_curso1 ON disc_curso1.disc_id = disc.disc_id
            LEFT JOIN acadperiodo__resumo_equivalencia AS equivalencia USING (res_id)
            LEFT JOIN acadgeral__aluno_formacao AS formacao ON alunocurso_id = formacao.aluno_id
            LEFT JOIN acadgeral__disciplina AS disc_equivalencia ON disc_equivalencia.disc_id = equivalencia.disc_id
            LEFT JOIN acadgeral__disciplina_curso AS disc_curso2 ON disc_curso2.disc_id = disc_equivalencia.disc_id
            WHERE
                res.alunocurso_id = {$matricula} AND
                (
                    (disc_curso1.disc_curso_id IS NOT NULL AND disc.disc_id IS NOT NULL) OR
                    (disc_curso2.disc_curso_id IS NOT NULL AND disc_equivalencia.disc_id IS NOT NULL)
                )
            ) resumo ON discGrade.disc_id = resumo.disc_id
            where resumo.disc_id is null
            order by resumo.disc_nome DESC";

        $result = $this->executeQuery($query)->fetch();

        return $result['resultado'] == 0;
    }

    public function verificaSeAlunoPossuiRegistrosDeHistorico($matricula)
    {
        $query = "
        SELECT count(*) as quantidade
        FROM acadperiodo__aluno_resumo res
        WHERE res.alunocurso_id = {$matricula}";

        $result = $this->executeQuery($query)->fetch();

        return $result['quantidade'] > 0;
    }

    public function retornaNumeroDeReprovacoesNaoCumpridas($matricula)
    {
        $serviceAcadgeralSituacao = new \Matricula\Service\AcadgeralSituacao($this->getEm());
        $serviceAcadgeralSituacao->situacoesAtividade();

        $queryDisc = "
        SELECT disc_id FROM (
            SELECT
            disc_id, situacao_id
            FROM (
                SELECT
                    d.disc_id, s.situacao_id
                    FROM acadperiodo__aluno a
                    INNER JOIN acadperiodo__turma t USING(turma_id)
                    LEFT JOIN acadperiodo__matriz_disciplina md ON md.mat_cur_id=t.mat_cur_id
                    LEFT JOIN acadperiodo__aluno_disciplina ad ON ad.alunoper_id=a.alunoper_id
                    LEFT JOIN acadgeral__situacao s ON s.situacao_id=coalesce(ad.situacao_id, a.matsituacao_id)
                    LEFT JOIN acadgeral__disciplina d ON d.disc_id=md.disc_id
                    WHERE alunocurso_id={$matricula}
                    ORDER BY d.disc_id, s.situacao_id
                ) AS x
                -- agrupa disciplinas para evitar discplicações de registro de aluno no periodo
            GROUP BY disc_id

        ) AS y
        -- não considera disciplinas que o aluno não está realmente fazendo
        WHERE situacao_id NOT IN (2,3,5,6,7,10,11,13,15)";

        $arrDisciplinas = $result = $this->executeQuery($queryDisc)->fetchAll(\PDO::FETCH_COLUMN);

        $strDisciplinas = trim(implode(',', $arrDisciplinas), ',');

        if ($strDisciplinas) {
            $strDisciplinas = 'WHERE disc_id IN(' . $strDisciplinas . ')';
        }

        $query = "
        SELECT COUNT(*) AS reprovacoes FROM
        (
            SELECT disciplinasAluno.* FROM
            (
                SELECT res.*
                FROM acadgeral__aluno AS aluno
                INNER JOIN acadgeral__aluno_curso AS aluno_curso USING (aluno_id)
                INNER JOIN acadperiodo__aluno_resumo AS res USING (alunocurso_id)
                LEFT JOIN acadgeral__disciplina AS disc USING (disc_id)
                LEFT JOIN acadgeral__disciplina_curso AS disc_curso1 ON disc_curso1.disc_id = disc.disc_id
                LEFT JOIN acadperiodo__resumo_equivalencia AS equivalencia USING (res_id)
                LEFT JOIN acadgeral__aluno_formacao AS formacao ON alunocurso_id = formacao.aluno_id
                LEFT JOIN acadgeral__disciplina AS disc_equivalencia ON disc_equivalencia.disc_id = equivalencia.disc_id
                LEFT JOIN acadgeral__disciplina_curso AS disc_curso2 ON disc_curso2.disc_id = disc_equivalencia.disc_id
                WHERE res.alunocurso_id = {$matricula}
                ORDER BY res.resaluno_serie*1 DESC, res.resaluno_situacao asc
            ) AS disciplinasAluno
            {$strDisciplinas}
            GROUP BY disc_id
        ) AS reprovacoes
        where resaluno_situacao = 'Reprovado'";

        $result = $this->executeQuery($query)->fetch();

        // Retorna true se o aluno possuir uma ou mais reprovações
        return (int)$result['reprovacoes'];
    }

    public function retornaEquivalencia($matricula)
    {
        $query = "SELECT
                  disc.disc_id as EQUIVALENCIA_ID,
                  disc.disc_nome as DISCIPLINA,
                  equi.disc_nome as EQUIVALENCIA,
                  formacao.formacao_instituicao as INSTITUICAO,
                  res.resaluno_carga_horaria as CH,
                  res.resaluno_semestre as SEM,
                  res.resaluno_ano as ANO
                  FROM acadperiodo__resumo_equivalencia resEq
                  JOIN acadperiodo__aluno_resumo res USING(res_id)
                  LEFT JOIN acadgeral__disciplina as disc on disc.disc_id = res.disc_id
                  LEFT JOIN acadgeral__disciplina as equi on equi.disc_id = resEq.disc_id
                  LEFT JOIN acadgeral__aluno_formacao as formacao ON resEq.formacao_id = formacao.formacao_id and formacao.formacao_curso != 'Ensino Médio'
                  WHERE res.alunocurso_id = {$matricula}
                  ORDER BY INSTITUICAO, ANO, SEM";

        return $this->executeQuery($query)->fetchAll();
    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {
    }

    public function registroSimplificado($arrDados, $verificarAtividadeDisciplina = false)
    {
        if (!$arrDados) {
            $this->setLastError("Não existem dados no array passado");

            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            $serviceAlunoDisc    = new \Matricula\Service\AcadperiodoAlunoDisciplina($this->getEm());
            $serviceCurso        = new \Matricula\Service\AcadCurso($this->getEm());
            $serviceAlunoCurso   = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
            $serviceSituacao     = new \Matricula\Service\AcadgeralSituacao($this->getEm());
            $serviceDisciplina   = new \Matricula\Service\AcadgeralDisciplina($this->getEm());
            $serviceAlunoPeriodo = new \Matricula\Service\AcadperiodoAluno($this->getEm());

            if ($arrDados['resId']) {
                /** @var \Matricula\Entity\AcadperiodoAlunoResumo $objAlunoResumo */
                $objAlunoResumo = $this->getRepository()->find($arrDados['resId']);

                if (!$objAlunoResumo) {
                    $this->setLastError("Registro não encontrado no sistema!");

                    return false;
                }
            } else {
                $objAlunoResumo = new \Matricula\Entity\AcadperiodoAlunoResumo();
            }

            if ($arrDados['alunocurso_id']) {
                /** @var \Matricula\Entity\AcadgeralAlunoCurso $objAlunoCurso */
                $objAlunoCurso = $serviceAlunoCurso->getRepository()->find($arrDados['alunocurso_id']);

                if (!$objAlunoCurso) {
                    $this->setLastError("Registro de aluno no curso não encontrado!");

                    return false;
                }

                $objAlunoResumo->setAlunocursoId($arrDados['alunocurso_id']);
            } else {
                $objAlunoResumo->setAlunocursoId(null);
            }

            if ($arrDados['disc_id']) {
                /** @var \Matricula\Entity\AcadgeralDisciplina $objDisciplina */
                $objDisciplina = $serviceDisciplina->getRepository()->find($arrDados['disc_id']);

                if (!$objDisciplina) {
                    $this->setLastError("Registro de disciplina não encontrado!");

                    return false;
                }

                $objAlunoResumo->setDiscId($arrDados['disc_id']);
            } else {
                $objAlunoResumo->setDiscId(null);
            }

            if (is_string($arrDados['dataInformacao'])) {
                $data = new \DateTime($this->formatDateAmericano($arrDados['dataInformacao']));

                $objAlunoResumo->setResalunoDataCriacao($data);
            } else {
                $objAlunoResumo->setResalunoDataCriacao($arrDados['dataInformacao']);
            }

            if ($arrDados['alunoper_id']) {
                /** @var \Matricula\Entity\AcadperiodoAluno $objAlunoPer */
                $objAlunoPer = $serviceAlunoPeriodo->getRepository()->find($arrDados['alunoper_id']);

                if (!$objAlunoPer) {
                    $this->setLastError("Registro de aluno no período não encontrado");

                    $objAlunoResumo->setAlunoper(null);
                } else {
                    $objAlunoResumo->setAlunoper($objAlunoPer);
                    $arrDados['alunocurso_id'] = $objAlunoPer->getAlunocurso()->getAlunocursoId();
                }
            }

            if ($arrDados['serie']) {
                $objAlunoResumo->setResalunoSerie($arrDados['serie']);
            } else {
                $objAlunoResumo->setResalunoSerie(0);
            }

            if ($arrDados['semestre']) {
                $objAlunoResumo->setResalunoSemestre($arrDados['semestre']);
            } else {
                $objAlunoResumo->setResalunoSemestre(null);
            }

            /** @var \Matricula\Entity\AcadgeralAlunoCurso $objAlunoCurso */
            $objAlunoCurso = $serviceAlunoCurso->getRepository()->find($arrDados['alunocurso_id']);
            $objCurso      = $objAlunoCurso->getCursocampus()->getCurso();

            if ($objCurso->verificaSeCursoPossuiPeriodoLetivo() && $verificarAtividadeDisciplina) {
                $arrParam = [
                    'alunocurso' => $objAlunoCurso->getAlunocursoId(),
                    'disc'       => $arrDados['disc_id'],
                    'situacao'   => $serviceSituacao->situacoesAtividade()
                ];

                $arrDisc = $serviceAlunoDisc->pesquisaDisciplinasAluno($arrParam);

                if (empty($arrDisc)) {
                    $this->setLastError("O aluno não está matriculado na disciplina.");

                    return false;
                }
            }

            $objAlunoResumo->setResalunoDetalhe($arrDados['resalunoDetalhe'] ? $arrDados['resalunoDetalhe'] : null);
            $objAlunoResumo->setResalunoOrigem($arrDados['resalunoOrigem'] ? $arrDados['resalunoOrigem'] : null);
            $objAlunoResumo->setResalunoSituacao($arrDados['situacao']);
            $objAlunoResumo->setResalunoDataAlteracao(new \DateTime());
            $objAlunoResumo->setResalunoNota($arrDados['nota']);
            $objAlunoResumo->setResalunoAno($arrDados['anoAprovacao']);

            $this->getEm()->persist($objAlunoResumo);
            $this->getEm()->flush($objAlunoResumo);
            $this->getEm()->commit();

            $arrDados['resId'] = $objAlunoResumo->getResId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError("Não foi possível salvar os dados. " . $e->getMessage());
        }

        return false;
    }

    public function verificaSeRegistroExiste($alunocursoId, $discId, $resalunoOrigem = self::RESALUNO_ORIGEM_WEBSERVICE)
    {
        $params = array(
            'disc_id'         => $discId,
            'alunocurso_id'   => $alunocursoId,
            'resaluno_origem' => $resalunoOrigem,
        );

        $query = "
        SELECT res_id FROM acadperiodo__aluno_resumo
        WHERE disc_id = :disc_id AND
            alunocurso_id = :alunocurso_id AND
            resaluno_origem = :resaluno_origem";

        $arrInfoExtra = $this->executeQueryWithParam($query, $params)->fetch();

        if ($arrInfoExtra) {
            return $arrInfoExtra['res_id'];
        }

        return false;
    }
}