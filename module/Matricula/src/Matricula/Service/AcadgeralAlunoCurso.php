<?php

namespace Matricula\Service;

use Acesso\Service\AcessoPessoas;
use VersaSpine\Service\AbstractService;

/**
 * Class AcadgeralAlunoCurso
 * @package Matricula\Service
 */
class AcadgeralAlunoCurso extends AbstractService
{
    const ALUNOCURSO_SITUACAO_DEFERIDO              = 'Deferido';
    const ALUNOCURSO_SITUACAO_INDEFERIDO            = 'Indeferido';
    const ALUNOCURSO_SITUACAO_PENDENTE              = 'Pendente';
    const ALUNOCURSO_SITUACAO_TRANCADO              = 'Trancado';
    const ALUNOCURSO_SITUACAO_CANCELADO             = 'Cancelado';
    const ALUNOCURSO_SITUACAO_TRANSFERENCIA         = 'Transferencia';
    const ALUNOCURSO_SITUACAO_CONCLUIDO             = 'Concluido';
    const ALUNOCURSO_SITUACAO_AGUARDANDO_PAGAMENTO  = 'Aguardando Pagamento';

    /**
     * @var null
     */
    private $lastError = null;

    /** @var \Sistema\Service\SisConfig */
    private $config = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, $arrConfig = array())
    {
        parent::__construct($em, 'Matricula\Entity\AcadgeralAlunoCurso');

        $this->setConfig(new \Sistema\Service\SisConfig($em, $arrConfig));
    }

    /**
     * @param array $dados
     * @return bool|\Matricula\Entity\AcadgeralAlunoCurso|void
     */
    public function adicionar(array $dados)
    {
        if (!$this->valida($dados)) {
            return false;
        }

        $serviceSisIntegracao  = new \Sistema\Service\SisIntegracao($this->getEm());
        $serviceCadastroOrigem = new \Matricula\Service\AcadgeralCadastroOrigem($this->getEm());
        $serviceAcessoPessoas  = new \Acesso\Service\AcessoPessoas($this->getEm());

        if ($dados['cursocampus']) {
            $dados['cursocampus'] = $this->getRepository('Matricula\Entity\CampusCurso')->find($dados['cursocampus']);
        }

        if (is_numeric($dados['aluno'])) {
            $dados['aluno'] = $this->getRepository('Matricula\Entity\AcadgeralAluno')->find($dados['aluno']);
        }

        if (is_numeric($dados['tiposel'])) {
            $dados['tiposel'] = $this->getRepository('Vestibular\Entity\SelecaoTipo')->find($dados['tiposel']);
        }

        $objAlunoCurso = new \Matricula\Entity\AcadgeralAlunoCurso();

        $objCurso = $this->getRepository('Matricula\Entity\CampusCurso')->find($dados['cursocampus']);
        $objAlunoCurso->setAlunocursoId($this->criarNumeroDeMatricula($objCurso->getCurso()->getCursoId()));

        if ($dados['origem']) {
            /** @var \Matricula\Entity\AcadgeralCadastroOrigem $objAcadgeralCadastroOrigem */
            $objAcadgeralCadastroOrigem = $serviceCadastroOrigem->getRepository()->find($dados['origem']);

            if ($objAcadgeralCadastroOrigem) {
                $objAlunoCurso->setOrigem($objAcadgeralCadastroOrigem);
            }
        } else {
            $objAlunoCurso->setOrigem(null);
        }

        /** @var $objAcessoPessoas \Acesso\Entity\AcessoPessoas */
        $objAcessoPessoas = $serviceAcessoPessoas->retornaUsuarioLogado();

        $objAlunoCurso->setUsuarioCadastro($objAcessoPessoas);
        $objAlunoCurso->setUsuarioAlteracao($objAcessoPessoas);
        $objAlunoCurso->setAlunocursoCarteira($dados['alunocursoCarteira']);
        $objAlunoCurso->setAlunocursoObservacoes($dados['alunocursoObservacoes']);
        $objAlunoCurso->setAlunocursoMediador($dados['alunocursoMediador']);
        $objAlunoCurso->setAlunocursoPessoaIndicacao($dados['alunocursoPessoaIndicacao']);
        $objAlunoCurso->setAlunocursoDataMatricula(new \DateTime('now'));
        $objAlunoCurso->setAlunocursoDataCadastro(new \DateTime('now'));
        $objAlunoCurso->setAlunocursoDataAlteracao(new \DateTime('now'));
        $objAlunoCurso->setAlunocursoSituacao(self::ALUNOCURSO_SITUACAO_DEFERIDO);
        $objAlunoCurso->setCursocampus($dados['cursocampus']);
        $objAlunoCurso->setAluno($dados['aluno']);
        $objAlunoCurso->setTiposel($dados['tiposel']);
        $objAlunoCurso->setAlunocursoDataColacao($dados['alunocursoDataColacao']);
        $objAlunoCurso->setAlunocursoDataExpedicaoDiploma($dados['alunocursoDataExpedicaoDiploma']);

        try {
            $this->getEm()->persist($objAlunoCurso);
            $this->getEm()->flush($objAlunoCurso);

            $serviceSisIntegracao->integracaoInteligenteParaAluno($objAlunoCurso);

            return $objAlunoCurso;
        } catch (\Exception $e) {
            $this->setLastError("Não foi possível cadastrar aluno." . $e->getMessage());

            return false;
        }
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['tiposelId']) {
            $errors[] = 'Preencha o campo "Forma de ingresso"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    public function criarNumeroDeMatricula($curso, $strPadrao = false)
    {
        $serviceCurso = new \Matricula\Service\AcadCurso($this->getEm());

        $arrPadroes = array(
            'AAAAAAAAAAAA' => 'substr(lpad(alunocurso_id, 12, "0"), 1) AS aluno',
            'YYAAAAAAAAAA' => 'substr(lpad(alunocurso_id, 12, "0"), 1, 2) AS ano, substr(lpad(alunocurso_id, 12, "0"), 3) AS aluno',
            'YYAAAAA'      => 'substr(lpad(alunocurso_id, 12, "0"), -7, 2) AS ano, substr(lpad(alunocurso_id, 12, "0"), -5) AS aluno',
            'YYCCAAAA'     => 'substr(lpad(alunocurso_id, 12, "0"), -8, 2) AS ano, substr(lpad(alunocurso_id, 12, "0"), -6, 2) AS curso, substr(lpad(alunocurso_id, 12, "0"), -4) AS aluno',
            'YYCCAAAAA'    => 'substr(lpad(alunocurso_id, 12, "0"), -9, 2) AS ano, substr(lpad(alunocurso_id, 12, "0"), -7, 2) AS curso, substr(lpad(alunocurso_id, 12, "0"), -5) AS aluno',
            'YYCCCAAA'     => 'substr(lpad(alunocurso_id, 12, "0"), -8, 2) AS ano, substr(lpad(alunocurso_id, 12, "0"), -6, 3) AS curso, substr(lpad(alunocurso_id, 12, "0"), -3) AS aluno',
            'YYCCCAAAA'    => 'substr(lpad(alunocurso_id, 12, "0"), -9, 2) AS ano, substr(lpad(alunocurso_id, 12, "0"), -7, 3) AS curso, substr(lpad(alunocurso_id, 12, "0"), -4) AS aluno',
            'YYCCCAAAAA'   => 'substr(lpad(alunocurso_id, 12, "0"), -10, 2) AS ano, substr(lpad(alunocurso_id, 12, "0"), -8, 3) AS curso, substr(lpad(alunocurso_id, 12, "0"), -5) AS aluno',
            'YYCCCCAAAAA'  => 'substr(lpad(alunocurso_id, 12, "0"), -11, 2) AS ano, substr(lpad(alunocurso_id, 12, "0"), -9, 4) AS curso, substr(lpad(alunocurso_id, 12, "0"), -5) AS aluno',
            'YYCCCCCAAAAA' => 'substr(lpad(alunocurso_id, 12, "0"), -12, 2) AS ano, substr(lpad(alunocurso_id, 12, "0"), -10, 5) AS curso, substr(lpad(alunocurso_id, 12, "0"), -5) AS aluno'
        );

        $objData = new \DateTime();

        if (!$strPadrao) {
            $strPadrao = $this->getConfig()->localizarChave('MATRICULA_PADRAO', 'AAAAAAAAAAAA');
        }

        $queryPadrao = $arrPadroes[$strPadrao];

        /** @var \Matricula\Entity\AcadCurso $objCurso */
        $objCurso = $serviceCurso->getRepository()->find($curso);

        if ($objCurso->verificaSeCursoPossuiPeriodoLetivo()) {
            $servicePeriodoLetivo = new \Matricula\Service\AcadperiodoLetivo($this->getEm());
            /** @var \Matricula\Entity\AcadperiodoLetivo $objPeriodoLetivo */
            $objPeriodoLetivo = $servicePeriodoLetivo->periodoMatricula();

            if (!$objPeriodoLetivo) {
                $objPeriodoLetivo = $servicePeriodoLetivo->buscaPeriodoAtual();
            }

            if (!$objPeriodoLetivo) {
                $objPeriodoLetivo = $servicePeriodoLetivo->buscaPeriodoAtual();
            }

            if ($objPeriodoLetivo) {
                $objData = $objPeriodoLetivo->getPerDataInicio();
            }
        }

        $tamSequenciaAluno = substr_count($strPadrao, 'A');
        $tamSequenciaCurso = substr_count($strPadrao, 'C');
        $tamSequenciaAno   = substr_count($strPadrao, 'Y');

        $ano   = $objData->format('y');
        $curso = substr($curso, 0, $tamSequenciaCurso);
        $curso = str_pad($curso, $tamSequenciaCurso, 0, STR_PAD_LEFT);

        $arrParam     = [];
        $arrCondicoes = [];

        if ($tamSequenciaCurso) {
            $arrParam['curso'] = $curso;
            $arrCondicoes[]    = 'curso=:curso';
        }

        if ($tamSequenciaAno) {
            $arrParam['ano'] = $ano;
            $arrCondicoes[]  = 'ano=:ano';
        }

        $queryData = "
        SELECT COALESCE(max(aluno),0)+1 AS sequencia
        FROM (SELECT {$queryPadrao} FROM acadgeral__aluno_curso) AS matricula";

        if ($arrCondicoes) {
            $queryData .= ' WHERE ' . implode(' AND ', $arrCondicoes);
        }

        $arrData    = $this->executeQueryWithParam($queryData, $arrParam)->fetch();
        $incremento = 0;

        do {
            $sequencia = $arrData['sequencia'] + $incremento;
            $sequencia = substr(str_pad($sequencia, $tamSequenciaAluno, 0, STR_PAD_LEFT), (-1) * $tamSequenciaAluno);

            $matricula = preg_replace('/(Y+)/', $ano, $strPadrao);
            $matricula = preg_replace('/(C+)/', $curso, $matricula);
            $matricula = preg_replace('/(A+)/', $sequencia, $matricula);

            $sqlCheck        = 'SELECT COUNT(*) AS t FROM acadgeral__aluno_curso WHERE alunocurso_id=' . $matricula;
            $matriculaExiste = $this->executeQueryWithParam($sqlCheck)->fetch(\PDO::FETCH_COLUMN);
            $incremento++;
        } while ($matriculaExiste);

        return $matricula;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function validacaoMultipla($arrParam)
    {
        $errors         = [];
        $arrAlunoCursos = json_decode($arrParam['alunoCurso'], true);

        if ($arrAlunoCursos) {
            $arrCursoValidacao = [];

            foreach ($arrAlunoCursos as $arrAlunoCurso) {
                if (!$arrAlunoCurso['cursocampusId']) {
                    $erros[] = 'Preencha o campo "Curso"!';
                } else {
                    if (!$arrCursoValidacao[$arrAlunoCurso['cursocampusId']]) {
                        $arrCursoValidacao[$arrAlunoCurso['cursocampusId']] = 0;
                    }

                    $arrCursoValidacao[$arrAlunoCurso['cursocampusId']] += 1;
                }

                if (!$this->valida($arrAlunoCurso)) {
                    $erros[] = (
                        '<p>Curso "' . $arrAlunoCurso['cursocampusId'] . '" possui erros: <br>' .
                        $this->getLastError() . '</p>'
                    );
                }
            }

            foreach ($arrCursoValidacao as $cursocampusId) {
                if ($arrCursoValidacao[$cursocampusId] > 1) {
                    $erros[] = (
                        'Curso ' . $cursocampusId . ' registrado ' . $arrCursoValidacao[$cursocampusId] . ' vezes.<br>' .
                        'Remova os itens duplicados.'
                    );
                }
            }
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * @param array                            $arrParam
     * @param \Matricula\Entity\AcadgeralAluno $objAcadgeralAluno
     * @param array                            $arrCodigoMatricula
     * @return bool
     */
    public function salvarMultiplos(
        array &$arrParam,
        \Matricula\Entity\AcadgeralAluno $objAcadgeralAluno,
        &$arrCodigoMatricula = array()
    ) {
        $arrExcluir = array();

        try {
            $this->getEm()->beginTransaction();

            $arrAlunoCursos = $arrParam['alunoCurso'];

            if ($arrParam['alunoCurso'] && is_string($arrParam['alunoCurso'])) {
                $arrAlunoCursos = json_decode($arrAlunoCursos, true);
            }

            $arrAlunoCursosDB = $this->getRepository()->findBy(['aluno' => $objAcadgeralAluno->getAlunoId()]);

            /* @var $objAlunoCurso \Matricula\Entity\AcadgeralAlunoCurso */
            foreach ($arrAlunoCursosDB as $objAlunoCurso) {
                $encontrado   = false;
                $alunocursoId = $objAlunoCurso->getAlunocursoId();

                foreach ($arrAlunoCursos as $arrResponsavel) {
                    if ($alunocursoId == $arrResponsavel['alunocursoId']) {
                        $encontrado = true;
                        break;
                    }
                }

                if (!$encontrado) {
                    $arrExcluir[$alunocursoId] = $objAlunoCurso;
                }
            }

            if ($arrExcluir) {
                foreach ($arrExcluir as $objAlunoCurso) {
                    $this->getEm()->remove($objAlunoCurso);
                    $this->getEm()->flush();
                }
            }

            foreach ($arrAlunoCursos as $pos => $arrAlunoCurso) {
                $arrAlunoCurso['alunoId'] = $objAcadgeralAluno->getAlunoId();

                if (!$arrAlunoCurso['tiposelId']) {
                    $arrAlunoCurso['tiposelId'] = $arrParam['tiposelId'];
                }

                if (!$this->salvarAlunoCurso($arrAlunoCurso)) {
                    throw new \Exception($this->getLastError());
                }

                $arrAlunoCursos[$pos] = $arrAlunoCurso;

                $arrCodigoMatricula[] = $arrAlunoCurso['alunocursoId'];
            }

            $arrParam['alunoCurso'] = $arrAlunoCursos;

            $this->getEm()->commit();

            return true;
        } catch (\Exception $e) {
            $this->getEm()->rollback();
            $this->setLastError(
                'Não foi possível salvar o registros de aluno no curso!' .
                '<br>' .
                $e->getMessage()
            );
        }

        return false;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function salvarAlunoCurso(array &$arrDados)
    {
        if (!$arrDados['alunocursoSituacao']) {
            $arrDados['alunocursoSituacao'] = self::ALUNOCURSO_SITUACAO_PENDENTE;
        }

        if (!$arrDados['alunocursoMediador']) {
            $arrDados['alunocursoMediador'] = 'registro-interno';

            if ($arrDados['captacao']) {
                $arrDados['alunocursoMediador'] = 'captacao-externa';
            }
        }

        if (!$this->valida($arrDados)) {
            return false;
        }

        $novoCurso = false;

        $serviceAcadCurso                    = new \Matricula\Service\AcadCurso($this->getEm());
        $serviceAcadgaralAlunoCursoHistorico = new \Matricula\Service\AcadgeralAlunoCursoHistorico($this->getEm());
        $serviceAcadgeralAluno               = new \Matricula\Service\AcadgeralAluno($this->getEm());
        $serviceAcadgeralCadastroOrigem      = new \Matricula\Service\AcadgeralCadastroOrigem($this->getEm());
        $serviceAcadgeralMotivoAlteracao     = new \Matricula\Service\AcadgeralMotivoAlteracao($this->getEm());
        $serviceAcessoPessoas                = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceCampusCurso                  = new \Matricula\Service\CampusCurso($this->getEm());
        $serviceOrgAgenteEducacional         = new \Organizacao\Service\OrgAgenteEducacional($this->getEm());
        $serviceSelecaoTipo                  = new \Vestibular\Service\SelecaoTipo($this->getEm());
        $serviceSisIntegracao                = new \Sistema\Service\SisIntegracao($this->getEm());

        $serviceAcessoPessoas->setConfig($this->getConfig());

        try {
            $this->getEm()->beginTransaction();

            $arrDados['alunocursoId'] = preg_replace('/[^0-9]+/', '', $arrDados['alunocursoId']);

            /** @var $objAcessoPessoas \Acesso\Entity\AcessoPessoas */
            $objAcessoPessoas = $serviceAcessoPessoas->retornaUsuarioLogado();

            if ($arrDados['alunocursoId'] && $arrDados['alunocursoId'] != '-') {
                /** @var $objAcadgeralAlunoCurso \Matricula\Entity\AcadgeralAlunoCurso */
                $objAcadgeralAlunoCurso = $this->getRepository()->find($arrDados['alunocursoId']);

                if (!$objAcadgeralAlunoCurso) {
                    $this->setLastError('Registro de aluno no curso não existe!');

                    return false;
                }
            } else {
                $novoCurso              = true;
                $objAcadgeralAlunoCurso = new \Matricula\Entity\AcadgeralAlunoCurso();
                $objAcadgeralAlunoCurso->setUsuarioCadastro($objAcessoPessoas);
                $objAcadgeralAlunoCurso->setAlunocursoDataCadastro(new \Datetime());
                $objAcadgeralAlunoCurso->setAlunocursoDataMatricula(new \Datetime());

                $objCurso = $this->getRepository('Matricula\Entity\CampusCurso')->find($arrDados['cursocampusId']);
                $objAcadgeralAlunoCurso->setAlunocursoId(
                    $this->criarNumeroDeMatricula($objCurso->getCurso()->getCursoId())
                );
            }

            $agente = null;

            if ($objAcadgeralAlunoCurso->getPesIdAgente()) {
                $agente = $objAcadgeralAlunoCurso->getPesIdAgente()->getPes()->getPesId();
            }

            if ($arrDados['pesIdAgente'] != $agente && $agente) {
                $permitirEdicaoAgente = $serviceAcessoPessoas->validaPessoaEditaAgenteAluno();

                if (!$permitirEdicaoAgente) {
                    $this->setLastError("Você não tem permissão para editar o Agente Educacional!");

                    return false;
                }
            }

            $alunocursoPessoaIndicacao     = trim($objAcadgeralAlunoCurso->getAlunocursoPessoaIndicacao());
            $novoAlunocursoPessoaIndicacao = trim($arrDados['alunocursoPessoaIndicacao']);

            if ($novoAlunocursoPessoaIndicacao != $alunocursoPessoaIndicacao && $alunocursoPessoaIndicacao) {
                $permitirEdicaoAlunoInficacao = $serviceAcessoPessoas->validaPessoaEditaPessoaIndicacaoAluno();

                if (!$permitirEdicaoAlunoInficacao) {
                    $this->setLastError("Você não tem permissão para editar a Pessoa Indicação!");

                    return false;
                }
            }

            $arrDadosEntidade = $objAcadgeralAlunoCurso->toArray();

            if ($arrDados['cursocampusId']) {
                /* @var $objCampusCurso \Matricula\Entity\CampusCurso */
                $objCampusCurso = $serviceCampusCurso->getRepository()->find($arrDados['cursocampusId']);

                if (!$objCampusCurso) {
                    $this->setLastError('Registro de curso do câmpus não existe!');

                    return false;
                }

                $objAcadgeralAlunoCurso->setCursocampus($objCampusCurso);
                /*$cursoPossuiPeriodoLetivo = $objCampusCurso->getCurso()->getCursoPossuiPeriodoLetivo();
                $cursoComPeriodicidade    = (
                    $cursoPossuiPeriodoLetivo != $serviceAcadCurso::CURSO_POSSUI_PERIODO_LETIVO_NAO
                );

                if (
                    $novoCurso &&
                    $cursoComPeriodicidade &&
                    $arrDados['alunocursoSituacao'] == self::ALUNOCURSO_SITUACAO_PENDENTE
                ) {
                    $arrDados['alunocursoSituacao'] = self::ALUNOCURSO_SITUACAO_DEFERIDO;
                }*/
            }

            if ($arrDados['alunoId']) {
                /* @var $objAcadgeralAluno \Matricula\Entity\AcadgeralAluno */
                $objAcadgeralAluno = $serviceAcadgeralAluno->getRepository()->find($arrDados['alunoId']);

                if (!$objAcadgeralAluno) {
                    $this->setLastError('Registro de aluno não existe!');

                    return false;
                }

                $objAcadgeralAlunoCurso->setAluno($objAcadgeralAluno);
            }

            if ($arrDados['tiposelId']) {
                /* @var $objSelecaoTipo \Vestibular\Entity\SelecaoTipo */
                $objSelecaoTipo = $serviceSelecaoTipo->getRepository()->find($arrDados['tiposelId']);

                if (!$objSelecaoTipo) {
                    $this->setLastError('Registro de tipo de seleção não existe!');

                    return false;
                }

                $objAcadgeralAlunoCurso->setTiposel($objSelecaoTipo);
            }

            if ($arrDados['pesIdAgente'] && is_numeric($arrDados['pesIdAgente'])) {
                /** @var $objOrgAgenteEducacional \Organizacao\Entity\OrgAgenteEducacional */
                $objOrgAgenteEducacional = $serviceOrgAgenteEducacional->getRepository()->find(
                    $arrDados['pesIdAgente']
                );

                if (!$objOrgAgenteEducacional) {
                    $this->setLastError('Registro de agente educacional não existe!');

                    return false;
                }

                $objAcadgeralAlunoCurso->setPesIdAgente($objOrgAgenteEducacional);
            } else {
                $objAcadgeralAlunoCurso->setPesIdAgente(null);
            }

            if ($arrDados['motivoAlteracao']) {
                $arrDados['motivo'] = $arrDados['motivoAlteracao'];
            }

            if ($arrDados['alteracaoObservacao']) {
                $arrDados['alunocursoAlteracaoObservacao'] = $arrDados['alteracaoObservacao'];
            }

            if ($arrDados['motivo']) {
                /** @var $objAcadgeralMotivoAlteracao \Matricula\Entity\AcadgeralMotivoAlteracao */
                $objAcadgeralMotivoAlteracao = $serviceAcadgeralMotivoAlteracao->getRepository()->find(
                    $arrDados['motivo']
                );

                if (!$objAcadgeralMotivoAlteracao) {
                    $this->setLastError('Registro de motivo alteração não existe!');

                    return false;
                }

                $objAcadgeralAlunoCurso->setMotivo($objAcadgeralMotivoAlteracao);
            }

            if ($arrDados['alunocursoDataCadastro']) {
                $dataCadastro = new \Datetime($this->formatDateAmericano($arrDados['alunocursoDataCadastro']));
            } else {
                $dataCadastro = $objAcadgeralAlunoCurso->getAlunocursoDataCadastro();
            }

            if ($arrDados['alunocursoDataMatricula']) {
                $dataMatricula = $arrDados['alunocursoDataMatricula'];
            } else {
                $dataMatricula = $objAcadgeralAlunoCurso->getAlunocursoDataMatricula();
            }

            if (!$dataMatricula) {
                $dataMatricula = new \DateTime();
            }

            if ($arrDados['origem']) {
                /** @var \Matricula\Entity\AcadgeralCadastroOrigem $objAcadgeralCadastroFormacao */
                $objAcadgeralCadastroFormacao = $serviceAcadgeralCadastroOrigem->getRepository()->find(
                    $arrDados['origem']
                );

                if ($objAcadgeralCadastroFormacao) {
                    $objAcadgeralAlunoCurso->setOrigem($objAcadgeralCadastroFormacao);
                }
            } else {
                $objAcadgeralAlunoCurso->setOrigem(null);
            }

            $objAcadgeralAlunoCurso->setAlunocursoDataMatricula($dataMatricula);
            $objAcadgeralAlunoCurso->setAlunocursoDataColacao($arrDados['alunocursoDataColacao']);
            $objAcadgeralAlunoCurso->setAlunocursoMediador($arrDados['alunocursoMediador']);
            $objAcadgeralAlunoCurso->setAlunocursoPessoaIndicacao($arrDados['alunocursoPessoaIndicacao']);
            $objAcadgeralAlunoCurso->setAlunocursoDataColacao($arrDados['alunocursoDataColacao']);
            $objAcadgeralAlunoCurso->setAlunocursoDataExpedicaoDiploma($arrDados['alunocursoDataExpedicaoDiploma']);
            $objAcadgeralAlunoCurso->setAlunocursoCarteira($arrDados['alunocursoCarteira']);
            $objAcadgeralAlunoCurso->setAlunocursoObservacoes($arrDados['alunocursoObservacoes']);
            $objAcadgeralAlunoCurso->setAlunocursoSituacao($arrDados['alunocursoSituacao']);
            $objAcadgeralAlunoCurso->setAlunocursoDataCadastro($dataCadastro);
            $objAcadgeralAlunoCurso->setAlunocursoEnade(
                $arrDados['enadeInformacoes'] ? $arrDados['enadeInformacoes'] : $objAcadgeralAlunoCurso->getAlunocursoEnade(
                )
            );

            if ($arrDados['alunocursoAlteracaoObservacao']) {
                $objAcadgeralAlunoCurso->setAlunocursoAlteracaoObservacao($arrDados['alunocursoAlteracaoObservacao']);
            }

            $arrDadosEntidadeNew = $objAcadgeralAlunoCurso->toArray();

            unset($arrDadosEntidade['arq']);
            unset($arrDadosEntidadeNew['arq']);
            unset($arrDadosEntidade['aluno']['arq']);
            unset($arrDadosEntidadeNew['aluno']['arq']);

            $arrDiferenca = array_diff_assoc($arrDadosEntidade, $arrDadosEntidadeNew);

            if ($arrDiferenca && !$novoCurso) {
                $serviceAcadgaralAlunoCursoHistorico->save($arrDadosEntidade);

                $objAcadgeralAlunoCurso->setAlunocursoDataAlteracao(new \DateTime());
                $objAcadgeralAlunoCurso->setUsuarioAlteracao($objAcessoPessoas);
            }

            $this->getEm()->persist($objAcadgeralAlunoCurso);
            $this->getEm()->flush($objAcadgeralAlunoCurso);
            $this->getEm()->commit();

            if ($novoCurso && !$arrDados['captacao']) {
                $this->efetivarDeferimentoAutomaticoCurso($arrDados, true);
            }

            $serviceSisIntegracao->integracaoInteligenteParaAluno($objAcadgeralAlunoCurso);

            $arrDados['alunocursoId'] = $objAcadgeralAlunoCurso->getAlunocursoId();
            $arrDados['novoCurso']    = $novoCurso;

            return true;
        } catch (\Exception $e) {
            $this->setLastError(
                'Não foi possível salvar o registro de aluno no curso!' .
                '<br>' .
                $e->getMessage()
            );
        }

        return false;
    }

    /**
     * @return \Sistema\Service\SisConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param \Sistema\Service\SisConfig $config
     * @return AcadgeralAluno
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * @param array $dados
     * @return bool|\Doctrine\Common\Proxy\Proxy|null|object
     * @throws \Doctrine\ORM\ORMException
     */
    public function salvaObservacoes(array $dados)
    {
        if ($dados['alunocursoId']) {
            /** @var $alunoCurso \Matricula\Entity\AcadgeralAlunoCurso */
            $alunoCurso = $this->getEm()->getReference('Matricula\Entity\AcadgeralAlunoCurso', $dados['alunocursoId']);

            if ($alunoCurso) {
                $observacao          = $alunoCurso->getAlunocursoObservacoes();
                $observacaoEnade     = $alunoCurso->getAlunocursoEnade();
                $observacaoHistorico = $alunoCurso->getAlunocursoObservacaoHistorico();

                if (isset($dados['observacoes'])) {
                    $observacao = $dados['observacoes'];
                }

                if (isset($dados['observacaoEnade'])) {
                    $observacaoEnade = $dados['observacaoEnade'];
                }

                if (isset($dados['observacaoHistorico'])) {
                    $observacaoHistorico = $dados['observacaoHistorico'];
                }

                $alunoCurso->setAlunocursoObservacoes($observacao);
                $alunoCurso->setAlunocursoEnade($observacaoEnade);
                $alunoCurso->setAlunocursoObservacaoHistorico($observacaoHistorico);

                $this->begin();
                $this->getEm()->persist($alunoCurso);
                $this->getEm()->flush();
                $this->commit();

                return $alunoCurso;
            }
        }

        $this->setLastError("Nenhuma matrícula informada!");

        return false;
    }

    /**
     * busca dados pessoais, residenciais e contatos de um aluno
     * @param int $alunocursoId
     * @return array|null
     */
    public function buscaDadosPessoaisResidenciaisContatosAluno($alunocursoId)
    {
        if ($alunocursoId) {
            $dadosAluno = array();

            $serviceEndereco    = (new \Pessoa\Service\Endereco($this->getEm()));
            $serviceContato     = (new \Pessoa\Service\Contato($this->getEm()));
            $estados['estados'] = $serviceEndereco->buscaEstados();

            $dadosAluno = array_merge($dadosAluno, $estados);

            $reference = $this->getReference($alunocursoId);

            if ($reference) {
                $aluno      = $reference->toArray();
                $dadosAluno = array_merge($dadosAluno, $aluno);
                $endereco   = $serviceEndereco->buscaEndereco($aluno['pes'], 'Residencial', false);

                if ($endereco) {
                    // die(var_dump($endereco));
                    $dadosAluno = array_merge($dadosAluno, $endereco);
                }

                $contatos = $serviceContato->buscaContatos($aluno['pes']);
                if ($contatos) {
                    $contatos   = $this->mapContato($contatos);
                    $dadosAluno = array_merge($dadosAluno, $contatos);
                }

                $alunoCurso = $this->buscaAlunoCurso($aluno['alunoId']);

                if ($alunoCurso) {
                    $dadosAluno = array_merge($dadosAluno, $alunoCurso->toArray());
                }

                return $dadosAluno;
            }
        }

        return null;
    }

    /**
     * @param null $aluno
     * @return null|object
     */
    public function buscaAlunoCurso($aluno = null, $alunoCurso = false)
    {
        if (!$aluno) {
            return null;
        }

        $ObjalunoCurso = null;

        if ($alunoCurso) {
            $ObjalunoCurso = $this->getRepository()->find($aluno);
        } else {
            $ObjalunoCurso = $this->getRepository()->findOneBy(['aluno' => $aluno]);
        }

        return $ObjalunoCurso;
    }

    /**
     * @param \Matricula\Entity\AcadgeralAluno $objAluno
     * @return null|object
     */
    public function numeroCursosAtivosAluno($objAluno)
    {
        $sql = '
        SELECT COUNT(a) AS qtd
        FROM Matricula\Entity\AcadgeralAlunoCurso a
        WHERE a.alunocursoSituacao IN (:alunocursoSituacao) AND a.aluno = :aluno';

        $query = $this->getEm()->createQuery($sql);

        $query->setParameter('aluno', $objAluno);
        $query->setParameter('alunocursoSituacao', $this->getAlunocursoSituacaoAtividade());

        $result = $query->setFirstResult(0)->getResult();

        return $result[0]['qtd'];
    }

    /**
     * @return array
     */
    public static function getAlunocursoSituacaoAtividade()
    {
        return array(
            self::ALUNOCURSO_SITUACAO_DEFERIDO,
            self::ALUNOCURSO_SITUACAO_PENDENTE,
            self::ALUNOCURSO_SITUACAO_AGUARDANDO_PAGAMENTO
        );
    }

    /**
     * @return array
     */
    public static function getAlunocursoSituacaoPendente()
    {
        return array(
            self::ALUNOCURSO_SITUACAO_PENDENTE,
            self::ALUNOCURSO_SITUACAO_AGUARDANDO_PAGAMENTO
        );
    }

    /**
     * @return array
     */
    public static function getAlunocursoSituacaoDisponiveisDeferimento()
    {
        return array(
            self::ALUNOCURSO_SITUACAO_DEFERIDO,
            self::ALUNOCURSO_SITUACAO_AGUARDANDO_PAGAMENTO
        );
    }

    /**
     * @param $params
     * @return mixed
     */
    public function pesquisaForJson($params)
    {
        $serviceAcadperiodoLetivo = new \Matricula\Service\AcadperiodoLetivo($this->getEm());
        $objAcadperiodoLetivo     = $serviceAcadperiodoLetivo->buscaPeriodoCorrente();
        $serviceAcessoPessoas     = new \Acesso\Service\AcessoPessoas($this->getEm());

        $pequisa = trim($params['pesquisa']);
        $pequisa = $pequisa ? $pequisa : trim($params['query']);

        $parameters = array(
            'pes_nome'      => "{$pequisa}%",
            'alunocurso_id' => ltrim($pequisa, '0') . "%",
            'pes_id'        => ltrim($pequisa, '0') . "%"
        );

        $pesIdIn = '';

        if ($params['pesId']) {
            $pesIdIn = implode(',', $params['pesId']);
        }

        $condicaoSolicitante = "";
        $condicaocampusCurso = "";

        if ($params['filtrarSolicitante']) {
            $serviceAcessoPessoas = new AcessoPessoas($this->getEm());

            $parameters['solicitantePesId'] = $serviceAcessoPessoas
                ->retornaUsuarioLogado()
                ->getPes()
                ->getPes()
                ->getPesId();
            $parameters['usuario']          = $serviceAcessoPessoas->retornaUsuarioLogado()->getId();
            $condicaoSolicitante            = '
            (
                pes_id_agente = :solicitantePesId or
                pes_id_agenciador = :solicitantePesId
            ) AND';
        }

        $arrCampusCursoPermitido = implode(",", $serviceAcessoPessoas->retornaCampusCursoDoUsuarioLogado(true));

        if ($arrCampusCursoPermitido) {
            $condicaocampusCurso .= " AND cc.cursocampus_id in (" . $arrCampusCursoPermitido . ")";
        }

        $query = "
        SELECT * FROM (
            SELECT
                acadgeral__aluno_curso.alunocurso_id, acadgeral__aluno_curso.alunocurso_id AS alunocursoId, acadgeral__aluno_curso.alunocurso_id matricula,
                acadgeral__aluno_curso.aluno_id, acadgeral__aluno_curso.aluno_id AS alunoId,
                IF(isnull(alunoper_id), 0, 1) AS matriculado,
                COALESCE(alunoper_id, '-') AS alunoper_id, COALESCE(alunoper_id, '-') AS alunoperId, apa.alunoper_id AS alunper_id,
                COALESCE(apt.per_id, '-') AS per_id, COALESCE(apt.per_id, '-') AS perId,
                pessoa.pes_id, pessoa.pes_id AS pesId,
                pes_cpf, pes_cpf AS pesCpf,
                pes_nome, pes_nome AS pesNome, pes_nome nome,
                COALESCE(turma_nome, '-') turma_nome, COALESCE(turma_nome, '-') turmaNome, COALESCE(turma_nome, '-') turma,
                situacao_id, situacao_id AS situacaoId,
                COALESCE(matsit_descricao, '-') AS matsit_descricao, COALESCE(matsit_descricao, '-') AS matsitDescricao,
                cc.cursocampus_id, cc.cursocampus_id AS cursocampusId,
                acad_curso.curso_id, acad_curso.curso_id AS cursoId,
                acadgeral__aluno_curso.alunocurso_situacao, acadgeral__aluno_curso.alunocurso_situacao AS alunocursoSituacao,
                curso_nome, curso_nome AS cursoNome,
                curso_possui_periodo_letivo,
                curso_unidade_medida,
                alunocurso_enade,
                alunocurso__observacao_historico,
                cc.camp_id
                
                -- SELECT --
                
            FROM acadgeral__aluno
            INNER JOIN pessoa USING(pes_id)
            INNER JOIN pessoa_fisica  USING(pes_id)
            INNER JOIN acadgeral__aluno_curso USING(aluno_id)
            INNER JOIN campus_curso cc USING(cursocampus_id)
            INNER JOIN acad_curso USING(curso_id)
            LEFT JOIN  acadperiodo__aluno AS apa USING(alunocurso_id)
            LEFT JOIN acadperiodo__turma AS apt
                ON apt.turma_id=apa.turma_id AND apt.per_id = " . $objAcadperiodoLetivo->getPerId() . "
            LEFT JOIN acadgeral__situacao ON acadgeral__situacao.situacao_id=apa.matsituacao_id
            
            -- JOIN --
             
            WHERE
                " . ($pesIdIn ? ' pessoa.pes_id in(' . $pesIdIn . ') AND' : '') . "
                " . $condicaoSolicitante . "
                (
                    alunocurso_id*1 LIKE :alunocurso_id OR
                    pessoa.pes_id*1 LIKE :pes_id OR
                    pes_nome COLLATE UTF8_GENERAL_CI LIKE :pes_nome
                )
                " . $condicaocampusCurso . "
            ORDER BY
                IF(alunocurso_id = :alunocurso_id, 0, 1),
                IF(pessoa.pes_id = :pes_id, 0, 1),
                IF(pes_nome LIKE :pes_nome, 0, 1),
                alunoper_sit_data DESC,
                matriculado DESC
            ) AS alunos";

        if ($params['aluno']) {
            $query
                .= "
        GROUP BY pes_id
        LIMIT 0,40
        ";
        } else {
            $query
                .= "
            GROUP BY alunocurso_id
            LIMIT 0,40
            ";
        }

        if ($params['cursoConfig']) {
            $select = ' cursoconfig_id, cursoconfig_nota_fracionada, cursoconfig_nota_max ';
            $join   = ' LEFT JOIN (
				  SELECT
				config.*				
				FROM acad_curso_config AS config
				LEFT JOIN acadperiodo__letivo AS periodoInicial ON periodoInicial.per_id=config.per_ativ_id
				LEFT JOIN acadperiodo__letivo AS periodoFinal ON periodoFinal.per_id=config.per_desat_id
				LEFT JOIN acadperiodo__matriz_curricular amc ON amc.mat_cur_id=config.cursoconfig_matriz_curricular				
				GROUP BY config.cursoconfig_id, config.per_ativ_id DESC, config.per_desat_id DESC
            )cursoConfig ON cursoConfig.curso_id=acad_curso.curso_id ';

            $query = str_replace('-- JOIN --', $join, $query);
            $query = str_replace('-- SELECT --', $select, $query);
        }

        $result = $this->executeQueryWithParam($query, $parameters)->fetchAll();

        return $result;
    }

    /**
     * @param $alunocursoId
     * @return array
     */
    public function getAlunocursoArray(
        $alunocursoId,
        $verificarDependencias = true,
        $verificarFinanceiro = true,
        $verificarDocumentos = true,
        $retornarIntegracoes = true,
        $pendenciasConclusao = true,
        $titulosAbertoBiblioteca = false,
        $verificarFinanceiroAcademico = false
    ) {
        /* @var $objAlunoCurso \Matricula\Entity\AcadgeralAlunoCurso */
        $objAlunoCurso = $this->getRepository()->find($alunocursoId);

        if (!$objAlunoCurso) {
            $this->setLastError('Aluno curso não existe!');

            return array();
        }

        $serviceAcadPeriodoLetivo      = new \Matricula\Service\AcadperiodoLetivo($this->getEm());
        $servicePeriodoAluno           = new \Matricula\Service\AcadperiodoAluno($this->getEm());
        $serviceAluno                  = new \Matricula\Service\AcadgeralAluno($this->getEm());
        $serviceEndereco               = new \Pessoa\Service\Endereco($this->getEm());
        $serviceContato                = new \Pessoa\Service\Contato($this->getEm());
        $serviceFinanceiroTitulo       = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceAcadPeriodoAlunoResumo = new \Matricula\Service\AcadperiodoAlunoResumo($this->getEm());
        $serviceSisIntegracaoAluno     = new \Sistema\Service\SisIntegracaoAluno($this->getEm());
        $serviceDocumentoPessoa        = new \Documentos\Service\DocumentoPessoa($this->getEm());
        $serviceBibliotecaTitulo       = new \Biblioteca\Service\Titulo($this->getEm());
        $serviceSisCampoValor          = new \Sistema\Service\SisCampoValor($this->getEm());

        $objAluno        = $objAlunoCurso->getAluno();
        $objCursoCampus  = $objAlunoCurso->getCursoCampus();
        $objPessoaFisica = $objAluno->getPes();
        $objArq          = $objAluno->getArq();
        $objPessoa       = $objPessoaFisica->getPes();
        $objCampus       = $objCursoCampus->getCamp();
        $objCurso        = $objCursoCampus->getCurso();
        $objIes          = $objCursoCampus->getCamp()->getIes();

        $arrData     = $objAlunoCurso->toArray();
        $arrEndereco = array_filter($serviceEndereco->getArrayEndereco($objPessoa->getPesId()));
        $arrContato  = array_filter($serviceContato->getArrayContato($objPessoa->getPesId()));
        $arrData     = array_merge(
            $arrData,
            $objPessoaFisica->toArray(),
            $arrEndereco,
            $arrContato
        );

        if ($titulosAbertoBiblioteca) {
            $qtdTitulosAbertosBiblioteca = $serviceBibliotecaTitulo->retornaQuantidadeTitulosEmprestados(
                ['pesId' => $objAlunoCurso->getAluno()->getPes()->getPes()->getPesId(), 'titulosAtivos' => 1]
            );

            $arrData['qtdTitulosAbertosBiblioteca'] = $qtdTitulosAbertosBiblioteca;
        }

        if ($verificarFinanceiroAcademico) {
            $arrParamTitulos = [
                'alunocursoId'   => $objAlunoCurso->getAlunocursoId(),
                'titulosAtraso'  => false,
                'grupoAcademico' => true,
            ];

            $qtdTitulosAbertosAcademicos = $serviceFinanceiroTitulo->buscaTitulosAbertoEmAtrasoAluno(
                $arrParamTitulos
            );

            $arrData['qtdTitulosAbertosAcademicos'] = $qtdTitulosAbertosAcademicos;
        }

        $arrData['alunoCursosMatriculado'] = $serviceAluno->retornaCampusCursoPorAluno($objAluno->getAlunoId());
        $arrData['alunoCursosMatriculado'] = implode(',', $arrData['alunoCursosMatriculado']);

        $arrData['alunoId'] = $objAluno->getAlunoId();

        $objPeriodoLetivo = $serviceAcadPeriodoLetivo->buscaPeriodoCorrente();

        //Busca informações do aluno no período letivo em vigência
        $objInfoAlunoPeriodo = $servicePeriodoAluno->buscaAlunoperiodoPorMatricula(
            $alunocursoId,
            $objPeriodoLetivo->getPerId()
        );

        //Caso não exista registro para o aluno no período letivo em vigência, procura saber se o aluno possui matrícula prevista para um próximo período.
        if ($objInfoAlunoPeriodo == null) {
            $objPeriodoLetivo = $serviceAcadPeriodoLetivo->retornaUltimoPeriodoAluno($alunocursoId, true);

            if ($objPeriodoLetivo) {
                $objInfoAlunoPeriodo = $servicePeriodoAluno->buscaAlunoperiodoPorMatricula(
                    $alunocursoId,
                    $objPeriodoLetivo->getPerId()
                );
            }
        }

        $arrData = array_merge(
            $arrData,
            array(
                'alunocursoId'             => $objAlunoCurso->getAlunocursoId(),
                'alunocursoObservacoes'    => $objAlunoCurso->getAlunocursoObservacoes(),
                'alunocursoCarteira'       => $objAlunoCurso->getAlunocursoCarteira(),
                'cursoId'                  => $objCurso->getCursoId(),
                'cursoNome'                => $objCurso->getCursoNome(),
                'cursoPossuiPeriodoLetivo' => $objCurso->getCursoUnidadeMedida(),
                'cursoPrazoIntegralizacao' => $objCurso->getCursoPrazoIntegralizacao(),
                'campId'                   => $objCampus->getCampId(),
                'campNome'                 => $objCampus->getCampNome(),
                'iesNome'                  => $objIes->getIesNome()
            )
        );

        $arrData['alunoDesligado'] = true;

        if ($objInfoAlunoPeriodo) {
            $arrData['turmaId']             = $objInfoAlunoPeriodo->getTurma()->getTurmaId();
            $arrData['turmaNome']           = $objInfoAlunoPeriodo->getTurma()->getTurmaNome();
            $arrData['alunoperId']          = $objInfoAlunoPeriodo->getAlunoperId();
            $arrData['situacaoId']          = $objInfoAlunoPeriodo->getMatsituacao()->getSituacaoId();
            $arrData['situacaoDescricao']   = $objInfoAlunoPeriodo->getMatsituacao()->getMatsitDescricao();
            $arrData['turmaTurno']          = $objInfoAlunoPeriodo->getTurma()->getTurmaTurno();
            $arrData['turmaSerie']          = $objInfoAlunoPeriodo->getTurma()->getTurmaSerie();
            $arrData['alunoDesligado']      = $servicePeriodoAluno->verificaSeAlunnoEstaCancelado(
                $objInfoAlunoPeriodo
            );
            $arrData['alunoperObservacoes'] = $objInfoAlunoPeriodo->getAlunoperObservacoes();
            $arrData['dataSituacao']        = date_format($objInfoAlunoPeriodo->getAlunoperSitData(), 'd/m/Y');
        }

        $cursoDeferido = $objAlunoCurso->getAlunocursoSituacao() == self::ALUNOCURSO_SITUACAO_DEFERIDO;

        if ($cursoDeferido && $arrData['alunoDesligado'] == true) {
            $arrData['alunoDesligado'] = false;
        }

        if ($objArq) {
            $arrData['imagemAluno'] = $objArq->getArqChave();
        }

        $arrData['matriculasPeriodosLetivos'] = $servicePeriodoAluno->periodosLetivosAluno($alunocursoId);

        if ($objPeriodoLetivo) {
            $arrData['periodoLetivoAtual'] = $objPeriodoLetivo->toArray();
            $arrData['perId']              = $objPeriodoLetivo->getPerId();
            $arrData['perNome']            = $objPeriodoLetivo->getPerNome();
        }

        if ($verificarDependencias) {
            $arrData['quantidadeDependencias'] = $serviceAcadPeriodoAlunoResumo->retornaNumeroDeReprovacoesNaoCumpridas(
                $alunocursoId
            );
        }

        if ($pendenciasConclusao) {
            $arrData['pendenciasConclusao'] = $this->verificaPendenciasConclusao($alunocursoId);
        }

        if ($verificarFinanceiro) {
            $arrParamTitulos = [
                'alunocursoId' => $objAlunoCurso->getAlunocursoId(),
                'pesId'        => $arrData['pesId'],
            ];

            $arrData['quantidadeTitulosAbertos']  = $serviceFinanceiroTitulo->buscaTitulosAbertoEmAtrasoAluno(
                $arrParamTitulos
            );
            $arrParamTitulos['titulosAtraso']     = true;
            $arrData['quantidadeTitulosVencidos'] = $serviceFinanceiroTitulo->buscaTitulosAbertoEmAtrasoAluno(
                $arrParamTitulos
            );
        }

        if ($verificarDocumentos) {
            $arrDocumentos                    = $serviceDocumentoPessoa->retornaDocumentosNaoEntregues(
                $arrData['pesId']
            );
            $arrData['documentosNaoEntegues'] = implode("; ", $arrDocumentos);
        }

        if ($retornarIntegracoes) {
            $dadosIntegracao = $serviceSisIntegracaoAluno->retornaCodigoPorAlunoFormatado($arrData['alunoId']);

            $arrData['codigosIntegracao'] = implode("; ", $dadosIntegracao);
        }

        $arrDatasPeriodo              = $servicePeriodoAluno->retornaIntervaloDatasPeriodosAluno($alunocursoId);
        $arrData['alunoDatasPeriodo'] = $arrDatasPeriodo;

        $arrData['mantenedora']       = $objIes->getPesMantenedora()->getPes()->getPesNome();
        $arrData['diretorGeral']      = $objIes->getDiretoriaPes()->getPes()->getPesNome();
        $arrData['iesCidade']         = $objIes->getIesEndCidade();
        $arrData['iesEstado']         = $objIes->getIesEndEstado();
        $arrData['iesCredenciamento'] = $objIes->getIesCredenciamento();

        /** @var \Pessoa\Entity\PessoaFisica $secretariaPes */
        if ($secretariaPes = $arrData['cursocampus']['secretariaPes']) {
            $arrData['secretaria'] = $secretariaPes->getPes()->getPesNome();
        }

        if ($objInstituicao = $objCampus->getIes()->getPesMantenedora()) {
            $arrData['instituicao'] = $objInstituicao->getPesNomeFantasia();
            $arrData['sigla']       = $objInstituicao->getPesSigla();

            if ($objMantenedora = $objInstituicao->getPes()) {
                $arrData['mantenedora'] = $objMantenedora->getPesNome();
            }
        }

        $arrData['camposPersonalizados'] = $serviceSisCampoValor->retornaPerguntasReposta(
            [
                'campoChave'     => array($arrData['alunoId'], $arrData['alunocursoId']),
                'entidadeTabela' => array('acadgeral__aluno', 'pessoa_fisica', 'acadgeral__aluno_curso')
            ]
        );

        return $arrData;
    }

    /**
     * Verifica se o aluno tem pendências e retorna um array com as mensagens relativas se houverem
     */
    public function verificaPendenciasConclusao($alunocursoId)
    {
        $serviceAlunoResumo  = new \Matricula\Service\AcadperiodoAlunoResumo($this->getEm());
        $servicePeriodoAluno = new \Matricula\Service\AcadperiodoAluno($this->getEm());

        $datas                = $this->verificaDatas($alunocursoId);
        $alunoPossuiRegistros = $serviceAlunoResumo->verificaSeAlunoPossuiRegistrosDeHistorico($alunocursoId);
        $alunoCumpriuGrade    = $serviceAlunoResumo->verificaSeAlunoCumpriuGrade($alunocursoId);
        $reprovacao           = $serviceAlunoResumo->retornaNumeroDeReprovacoesNaoCumpridas($alunocursoId);
        $discAtiva            = $servicePeriodoAluno->verificaSeAlunoPossuiDisciplinaAtivaEmPeriodo($alunocursoId);
        $result               = ['possuiPendencias' => false, 'pendencias' => [], 'datas' => []];

        if ($reprovacao) {
            $result['pendencias'][]                  = 'Aluno possui reprovações.';
            $result['possuiReprovacoesNaoCumpridas'] = true;
        }

        if ($discAtiva) {
            $result['pendencias'][] = 'Aluno está matriculado em alguma disciplina do período letivo atual.';
        }

        if (!$alunoPossuiRegistros) {
            $result['pendencias'][] = 'Aluno não possui nenhum registro no histórico acadêmico.';
        }

        if (!$alunoCumpriuGrade) {
            $result['pendencias'][] = 'Aluno ainda não cumpriu a matriz de disciplinas.';
        }

        if ($datas['colacao'] != null || $datas['expedicao'] != null) {
            $result['datas'][] = 'Aluno já possui datas de conclusão, no entanto é possível atualizá-las.';
        }

        if (count($result['pendencias']) > 0) {
            $result['possuiPendencias'] = true;
        }

        return $result;
    }

    /**
     * Verificar se o aluno já tem as datas de colação e expedição e as retorna
     */
    public function verificaDatas($matricula)
    {
        $query = "SELECT alunocurso_data_expedicao_diploma AS expedicao,alunocurso_data_colacao AS colacao FROM acadgeral__aluno_curso WHERE alunocurso_id = :matricula";

        $data = $this->executeQueryWithParam(
            $query,
            [
                'matricula' => $matricula
            ]
        )->fetchAll();

        $arrData = current($data);

        $colacao = $this->formatDateBrasileiro($arrData['colacao']);

        $expedicao = $this->formatDateBrasileiro($arrData['expedicao']);

        $arrData = [
            'colacao'   => $colacao,
            'expedicao' => $expedicao
        ];

        return $arrData;
    }

    /**
     * Service que adiciona ou atualiza a data da colação
     * Retorna 1 quando a data está sendo adicionada
     * Retorna 2 quando a data está sendo atualizada
     */
    public function addDataColacao($alunocursoId, $data)
    {
        $queryData = "SELECT alunocurso_data_colacao AS data FROM acadgeral__aluno_curso WHERE alunocurso_id = :alunocursoId";

        $objData = $this->executeQueryWithParam(
            $queryData,
            [
                'alunocursoId' => $alunocursoId
            ]
        )->fetchAll();

        $arrData = current($objData);

        if ($arrData['data'] == null) {
            $query = "update acadgeral__aluno_curso co set alunocurso_data_colacao  = {$data} where alunocurso_id = {$alunocursoId} AND alunocurso_data_colacao is null;";

            $this->executeQuery($query);

            return 1;
        } else {
            $query = "update acadgeral__aluno_curso co set alunocurso_data_colacao  = {$data} where alunocurso_id = {$alunocursoId} AND alunocurso_data_colacao is null;";

            $this->executeQuery($query);

            return 2;
        }
    }

    /**
     * Service que adiciona ou atualiza a data da expedição
     * Retorna 1 quando a data está sendo adicionada
     * Retorna 2 quando a data está sendo atualizada
     */
    public function addDataExpedicao($alunocursoId, $data)
    {
        $queryData = "SELECT alunocurso_data_expedicao_diploma AS data FROM acadgeral__aluno_curso WHERE alunocurso_id = :alunocursoId";

        $objData = $this->executeQueryWithParam(
            $queryData,
            ['alunocursoId' => $alunocursoId]
        )->fetchAll();

        $arrData = current($objData);

        if ($arrData['data'] == null) {
            $query = "update acadgeral__aluno_curso co set alunocurso_data_expedicao_diploma = {$data} where alunocurso_id = {$alunocursoId} AND alunocurso_data_expedicao_diploma is null;";

            $this->executeQuery($query);

            return 1;
        } else {
            $query = "update acadgeral__aluno_curso co set alunocurso_data_expedicao_diploma = {$data} where alunocurso_id = {$alunocursoId} AND alunocurso_data_expedicao_diploma is null;";

            $this->executeQuery($query);

            return 2;
        }
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceSelecaoTipo = new \Vestibular\Service\SelecaoTipo($this->getEm());
        $serviceCampus      = new \Matricula\Service\AcadCurso($this->getEm());
        $arrSelecaoTipo     = $serviceSelecaoTipo->getArrSelect2();

        $serviceCampus->setarDependenciasView($view);
        $arrSituacoes = $this->getArrSelect2AlunocursoSituacaoAlteracao();

        foreach ($arrSituacoes as $x => $value) {
            if ($value['id'] == self::ALUNOCURSO_SITUACAO_CONCLUIDO ||
                $value['text'] == self::ALUNOCURSO_SITUACAO_CONCLUIDO
            ) {
                unset($arrSituacoes[$x]);
            }
        }

        $view->setVariable("arrSelecaoTipo", $arrSelecaoTipo);
        $view->setVariable("arrAlunocursoSituacao", $this->getArrSelect2AlunocursoSituacao());
        $view->setVariable("arrAlunocursoSituacaoAlteracao", $arrSituacoes);
    }

    /**
     * @param array $params
     * @return array
     */
    public static function getArrSelect2AlunocursoSituacao($params = array())
    {
        return self::getArrSelect2Constantes($params, self::getAlunocursoSituacao());
    }

    /**
     * @param array $params
     * @return array
     */
    public static function getArrSelect2AlunocursoSituacaoDisponiveisDeferimento($params = array())
    {
        return self::getArrSelect2Constantes($params, self::getAlunocursoSituacaoDisponiveisDeferimento());
    }

    /**
     * @return array
     */
    public static function getAlunocursoSituacao()
    {
        return array(
            self::ALUNOCURSO_SITUACAO_DEFERIDO,
            self::ALUNOCURSO_SITUACAO_INDEFERIDO,
            self::ALUNOCURSO_SITUACAO_PENDENTE,
            self::ALUNOCURSO_SITUACAO_TRANCADO,
            self::ALUNOCURSO_SITUACAO_CANCELADO,
            self::ALUNOCURSO_SITUACAO_TRANSFERENCIA,
            self::ALUNOCURSO_SITUACAO_CONCLUIDO,
            self::ALUNOCURSO_SITUACAO_AGUARDANDO_PAGAMENTO,
        );
    }

    /**
     * @return array
     */
    public static function retornaSituacoesDesligamentoPedente()
    {
        return array(
            self::ALUNOCURSO_SITUACAO_INDEFERIDO,
            self::ALUNOCURSO_SITUACAO_PENDENTE,
            self::ALUNOCURSO_SITUACAO_TRANCADO,
            self::ALUNOCURSO_SITUACAO_CANCELADO,
            self::ALUNOCURSO_SITUACAO_TRANSFERENCIA,
        );
    }

    /**
     * @param array $params
     * @return array
     */
    public static function getArrSelect2AlunocursoSituacaoAlteracao($params = array())
    {
        return self::getArrSelect2Constantes($params, self::getAlunocursoSituacaoAlteracao());
    }

    /**
     * @return array
     */
    public static function getAlunocursoSituacaoAlteracao()
    {
        return array(
            self::ALUNOCURSO_SITUACAO_DEFERIDO,
            self::ALUNOCURSO_SITUACAO_INDEFERIDO,
            self::ALUNOCURSO_SITUACAO_TRANCADO,
            self::ALUNOCURSO_SITUACAO_CANCELADO,
            self::ALUNOCURSO_SITUACAO_TRANSFERENCIA,
            self::ALUNOCURSO_SITUACAO_AGUARDANDO_PAGAMENTO,
            self::ALUNOCURSO_SITUACAO_CONCLUIDO

        );
    }

    public function deferirCursoDeAluno($alunoId, $campusCurso)
    {
        if (is_a($alunoId, '\Matricula\Entity\AcadgeralAlunoCurso')) {
            $objAlunoCurso = $alunoId;
        }
        /* @var $objAlunoCurso \Matricula\Entity\AcadgeralAlunoCurso */
        $objAlunoCurso = $objAlunoCurso ? $objAlunoCurso :
            $this->getRepository()->findOneBy(['aluno' => $alunoId, 'cursocampus' => $campusCurso]);

        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $registraTitulosDoAluno  = $serviceFinanceiroTitulo->gerarTitulosPelaConfiguracaoDoCursoDoAluno(
            $objAlunoCurso->getAlunocursoId()
        );

        if (!$registraTitulosDoAluno) {
            throw new \Exception($serviceFinanceiroTitulo->getLastError());
        }
    }

    /**
     * @param array $arrParam
     * @return array|bool
     */
    public function deferirCursoAluno($arrParam)
    {
        $serviceOrgComunicacaoModelo = new \Organizacao\Service\OrgComunicacaoModelo($this->getEm());

        $alunocursoId                = false;
        $arrAlunoConfigPagtoCurso    = array();
        $loginSupervisor             = null;
        $senhaSupervisor             = null;
        $justificativa               = null;
        $naoValidarAlteracoesNoPlano = false;
        $validarPresencaDePlano      = false;
        $erroCartao                  = [];

        if ($arrParam['alunocursoId']) {
            $alunocursoId = $arrParam['alunocursoId'];
        }

        if ($arrParam['arrAlunoConfigPagtoCurso']) {
            $arrAlunoConfigPagtoCurso = $arrParam['arrAlunoConfigPagtoCurso'];
        }

        if (!$arrAlunoConfigPagtoCurso && $arrParam['alunoConfigPagtoCurso']) {
            $arrAlunoConfigPagtoCurso = $arrParam['alunoConfigPagtoCurso'];
        }

        if ($arrParam['loginSupervisor']) {
            $loginSupervisor = $arrParam['loginSupervisor'];
        }

        if ($arrParam['senhaSupervisor']) {
            $senhaSupervisor = $arrParam['senhaSupervisor'];
        }

        if ($arrParam['justificativa']) {
            $justificativa = $arrParam['justificativa'];
        }

        if ($arrParam['naoValidarAlteracoesNoPlano']) {
            $naoValidarAlteracoesNoPlano = $arrParam['naoValidarAlteracoesNoPlano'];
        }

        if ($arrParam['validarPresencaDePlano']) {
            $validarPresencaDePlano = $arrParam['validarPresencaDePlano'];
        }

        $serviceCursoConfig      = new \Matricula\Service\AcadCursoConfig($this->getEm());
        $serviceFinanceiroCartao = new \Financeiro\Service\FinanceiroCartao($this->getEm());

        $serviceFinanceiroCartao->setConfig($this->getConfig());

        /* @var $objAlunoCurso \Matricula\Entity\AcadgeralAlunoCurso */
        $objAlunoCurso  = $this->getRepository()->find($alunocursoId);
        $objCursoConfig = $serviceCursoConfig->retornaObjCursoConfig(
            $objAlunoCurso->getCursocampus()->getCurso()->getCursoId()
        );

        $situacaoPosDeferimento = self::ALUNOCURSO_SITUACAO_DEFERIDO;

        if ($objCursoConfig) {
            $situacaoPosDeferimento = $objCursoConfig->getCursoconfigSituacaoDeferimento();
        }

        $cursoSituacao = $objAlunoCurso->getCursocampus()->getCurso()->getCursoSituacao();

        if ($cursoSituacao == \Matricula\Service\AcadCurso::CURSO_SITUACAO_INATIVO) {
            $this->setLastError('Curso se encontra inativo!');

            return false;
        }

        if (!$objAlunoCurso) {
            $this->setLastError('Curso do aluno não existe!');

            return false;
        }

        if ($objAlunoCurso->getAlunocursoSituacao() == $situacaoPosDeferimento) {
            $this->setLastError('Curso do aluno já se encontra deferido!');

            return false;
        }

        $necessitaAutenticacao = false;

        foreach ($arrAlunoConfigPagtoCurso as &$alunoConfigPagtoCurso) {
            if ($alunoConfigPagtoCurso['configPgtoCursoAlterado'] == 'Sim') {
                $necessitaAutenticacao = true;
            } elseif ($alunoConfigPagtoCurso['configPgtoCursoAlterado'] == '-') {
                $alunoConfigPagtoCurso['configPgtoCursoAlterado'] = 'Sim';
            }
        }

        if ($necessitaAutenticacao) {
            if (!$loginSupervisor || !$senhaSupervisor) {
                $this->setLastError('Por favor informe o login e senha do supervisor!');

                return false;
            }

            $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());

            $usuarioSupervisor = $serviceAcessoPessoas->validaUsuarioEVerificaSeEstaNoGrupo(
                $loginSupervisor,
                $senhaSupervisor,
                $this->getConfig()->localizarChave('GRUPO_FINANCEIRO_SUPERVISAO')
            );

            if (!$usuarioSupervisor) {
                $this->setLastError($serviceAcessoPessoas->getLastError());

                return false;
            }

            foreach ($arrAlunoConfigPagtoCurso as &$alunoConfigPagtoCurso) {
                if ($alunoConfigPagtoCurso['configPgtoCursoAlterado'] != 'Não') {
                    $alunoConfigPagtoCurso['usuarioSupervisor']            = $usuarioSupervisor->getId();
                    $alunoConfigPagtoCurso['configPgtoCursoJustificativa'] = $justificativa;
                }
            }
        }

        $serviceAcadgeralAluno       = new \Matricula\Service\AcadgeralAluno($this->getEm());
        $serviceAlunoConfigPgtoCurso = new \Financeiro\Service\FinanceiroAlunoConfigPgtoCurso($this->getEm());
        $serviceFinanceiroTitulo     = new \Financeiro\Service\FinanceiroTitulo($this->getEm());

        $serviceAcadgeralAluno->setConfig($this->getConfig());
        $serviceFinanceiroTitulo->setConfig($this->getConfig());

        if ($validarPresencaDePlano) {
            $arrFinanceiroAlunoConfigPgtoCursos = $serviceAlunoConfigPgtoCurso
                ->retornaConfiguracoesDePagamentoDoCurso($alunocursoId, false, true);

            if (!$arrFinanceiroAlunoConfigPgtoCursos) {
                $this->setLastError('Por favor especifique algum plano de pagamento!');

                return false;
            }
        }

        try {
            $this->getEm()->beginTransaction();

            $objAlunoCurso->setAlunocursoSituacao($situacaoPosDeferimento);

            $this->getEm()->persist($objAlunoCurso);
            $this->getEm()->flush($objAlunoCurso);

            //caso o curso não tenha plano de pagamento não continua com o deferimento
            if (!$naoValidarAlteracoesNoPlano) {
                $registraAlteracoesCurso = $serviceAlunoConfigPgtoCurso
                    ->salvarMultiplosItemsPeloCurso($alunocursoId, $arrAlunoConfigPagtoCurso);

                if (!$registraAlteracoesCurso) {
                    throw new \Exception($serviceAlunoConfigPgtoCurso->getLastError());
                }
            }

            //Não gerará títulos para cursos que possuam vinculo com o periodo letivo
            if (!$objAlunoCurso->getCursocampus()->getCurso()->verificaSeCursoPossuiPeriodoLetivo()) {
                $registraTitulosDoAluno = $serviceFinanceiroTitulo->gerarTitulosPelaConfiguracaoDoCursoDoAluno(
                    $alunocursoId
                );

                if ($registraTitulosDoAluno === false) {
                    throw new \Exception($serviceFinanceiroTitulo->getLastError());
                } elseif (is_array($registraTitulosDoAluno)) {
                    foreach ($registraTitulosDoAluno as $value) {
                        /** @var $objFinanceiroAlunoConfigPgt \Financeiro\Entity\FinanceiroAlunoConfigPgtoCurso */
                        $objFinanceiroAlunoConfigPgt = $value['cursoconfigPgtocurso'];
                        /*Valida se é um objeto*/
                        if (is_a($objFinanceiroAlunoConfigPgt, '\Financeiro\Entity\FinanceiroAlunoConfigPgtoCurso')) {
                            /*Se tem cartão adicionado o meio de pagamento é via cartão*/
                            if ($cartao = $objFinanceiroAlunoConfigPgt->getCartao()) {
                                $ok = $serviceFinanceiroCartao->realizaPagamentoDeferimento($value);

                                if (is_array($ok)) {
                                    $erroCartao[] = $objFinanceiroAlunoConfigPgt->getTipotitulo()->getTipotituloNome();
                                } elseif (!$ok) {
                                    $this->setLastError($serviceFinanceiroCartao->getLastError());

                                    return false;
                                }
                            }
                        }
                    }
                }
            }

            $serviceAcadgeralAluno->efetuaIntegracaoAluno($objAlunoCurso->getAluno());

            $emailPosDeferimento = $this->getConfig()->localizarChave(
                'ENVIAR_EMAIL_COMUNICACAO_ALUNO_APOS_DEFERIMENTO'
            );
            $smsPosDeferimento   = $this->getConfig()->localizarChave(
                'ENVIAR_EMAIL_COMUNICACAO_ALUNO_APOS_DEFERIMENTO'
            );

            if ($emailPosDeferimento || $smsPosDeferimento) {
                $arrInfo = $serviceOrgComunicacaoModelo->trabalharVariaveisPorContexto(
                    array('alunoId' => $objAlunoCurso->getAluno()->getAlunoId()),
                    \Organizacao\Service\OrgComunicacaoTipo::TIPO_CONTEXTO_ALUNO
                );

                if ($emailPosDeferimento) {
                    $serviceOrgComunicacaoModelo->registrarComunicacao(
                        \Organizacao\Service\OrgComunicacaoTipo::TIPO_BOAS_VINDAS_ALUNO_APOS_DEFERIMENTO,
                        \Organizacao\Service\OrgComunicacaoModelo::MODELO_TIPO_EMAIL,
                        $arrInfo
                    );
                }

                if ($smsPosDeferimento) {
                    $serviceOrgComunicacaoModelo->registrarComunicacao(
                        \Organizacao\Service\OrgComunicacaoTipo::TIPO_BOAS_VINDAS_ALUNO_APOS_DEFERIMENTO,
                        \Organizacao\Service\OrgComunicacaoModelo::MODELO_TIPO_SMS,
                        $arrInfo
                    );
                }
            }

            $this->getEm()->commit();
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível deferir curso do aluno!' . '<br>' . $e->getMessage());

            return false;
        }

        if ($erroCartao) {
            $this->setLastError(
                "Não foi possivel pagar os títulos: " .
                implode(',', $erroCartao) .
                ' com cartão, os boletos estão disponiveis para pagamento!'
            );
        }

        return true;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('alunocursoId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Matricula\Entity\AcadgeralAlunoCurso */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getAlunocursoId(),
                $params['value'] => (
                    $objEntity->getCursocampus()->getCamp()->getCampNome() . ' / ' .
                    $objEntity->getCursocampus()->getCurso()->getCursoNome()
                )
            );
        }

        return $arrEntitiesArr;
    }

    public function dadosGeraisAlunoCurso($alunocurso)
    {
        $sql    = <<<SQL
          SELECT DISTINCT
               CAST(alunoCurso.aluno_id AS DECIMAL)   aluno_id,
                alunoCurso.alunocurso_id  alunocurso_id,
                IFNULL(unidade_nome,'NÃO INFORMADO') unidade_nome,
                DATE_FORMAT(alunocurso_data_cadastro, '%d/%m/%Y ') alunocurso_data_cadastro,
                DATE_FORMAT(pes_data_nascimento,'%d/%m%/%Y') pes_data_nascimento ,
                pessoa.pes_id pes_id,
                pessoa.pes_nome,
                pessoa.pes_id pes_id,
                pes_sexo,
                pes_rg,
                pes_emissor_rg,
                pes_estado_civil,
                aluno_etnia,
                aluno_cert_militar,
                pes_cpf,
                pes_nasc_uf,
                pessoa.con_contato_email email,
                pessoa.con_contato_telefone telefone,
                pessoa.con_contato_celular celular,
                end_cep,
                end_logradouro,
                end_bairro,
                end_cidade,
                end_estado,
                end_numero,
                end_complemento,
                aluno_cert_militar,
                aluno_titulo_eleitoral,
                aluno_pai,
                aluno_mae,
                pes_naturalidade,
                pessoa.pes_nacionalidade,
                alunocurso_observacoes,
                pessoaAgente.pes_nome pessoaAgente,
                IF(planoPagamentoCurso.config_pgto_curso_id IS NULL, 'Não','Sim') plano_pagamento,
                planoPagamentoCurso.config_pgto_curso_id config_curso,
                planoPagamentoCurso.config_pgto_curso_parcela parcelas_curso,
                planoPagamentoCurso.config_pgto_curso_valor valor_curso,
                planoPagamentoCurso.config_pgto_curso_vencimento vencimento_curso,
                IF(matriculaPagamento.config_pgto_curso_id IS NULL, 'Não','Sim') matricula_pagamento,
                matriculaPagamento.config_pgto_curso_id config_matricula,
                matriculaPagamento.config_pgto_curso_valor matricula_valor,
                matriculaPagamento.config_pgto_curso_parcela parcelas_matricula,
                campus_curso.camp_id camp_id,
                acad_nivel.nivel_nome nivel_nome,
                curso.curso_nome,
                origem.origem_nome,
                org_campus.camp_nome,
                campus_curso.cursocampus_id cursocampus_id,
                curso.area_id area_id
            FROM
                pessoa
                    LEFT JOIN
                pessoa_fisica ON pessoa.pes_id = pessoa_fisica.pes_id
                    LEFT JOIN
                endereco ON endereco.pes_id = pessoa.pes_id
                    LEFT JOIN
                financeiro__titulo ON financeiro__titulo.pes_id = pessoa.pes_id
                    LEFT JOIN
                acadgeral__aluno alunoGeral ON alunoGeral.pes_id = pessoa.pes_id
                    LEFT JOIN
                org__unidade_estudo on org__unidade_estudo.unidade_id = alunoGeral.unidade_id
                    LEFT JOIN
                acadgeral__aluno_curso alunoCurso ON alunoCurso.aluno_id = alunoGeral.aluno_id
                    LEFT JOIN
                campus_curso ON campus_curso.cursocampus_id = alunoCurso.cursocampus_id
                  LEFT JOIN
                org_campus ON org_campus.camp_id = campus_curso.camp_id
                    LEFT JOIN
                acad_curso curso ON curso.curso_id = campus_curso.curso_id
                    LEFT JOIN
                 acad_nivel ON acad_nivel.nivel_id = curso.nivel_id
                    LEFT JOIN
                acadgeral__cadastro_origem origem ON alunoCurso.origem_id = origem.origem_id 
                    LEFT JOIN
                org__agente_educacional agente ON agente.pes_id = alunoCurso.pes_id_agente
                    LEFT JOIN
                pessoa pessoaAgente ON pessoaAgente.pes_id = agente.pes_id
                    LEFT JOIN
                financeiro__aluno_config_pgto_curso planoPagamentoCurso ON planoPagamentoCurso.alunocurso_id = alunoCurso.alunocurso_id
                    AND planoPagamentoCurso.tipotitulo_id = (SELECT
                        tipotitulo_id
                    FROM
                        financeiro__titulo_tipo
                    WHERE
                        tipotitulo_descricao = 'Mensalidade'
                    LIMIT 1)
                     LEFT JOIN
                financeiro__aluno_config_pgto_curso matriculaPagamento ON matriculaPagamento.alunocurso_id = alunoCurso.alunocurso_id
                    AND planoPagamentoCurso.tipotitulo_id = (SELECT
                        tipotitulo_id
                    FROM
                        financeiro__titulo_tipo
                    WHERE
                        tipotitulo_descricao = 'Taxa de matrícula'
                    LIMIT 1)

            WHERE
                alunoCurso.alunocurso_id = {$alunocurso}
            GROUP BY pessoa.pes_id;
SQL;
        $result = $this->executeQuery($sql)->fetch();

        return $result;
    }

    public function verificaSeCursoEstaRegistradoParaAluno($alunoId, $cursocampusId)
    {
        $arrAlunoCursos = $this->getArrayAlunoCurso(['aluno' => $alunoId]);

        foreach ($arrAlunoCursos as $arrAlunoCurso) {
            if ($arrAlunoCurso['cursocampusId'] == $cursocampusId) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrayAlunoCurso($params)
    {
        $serviceAlunoCurso = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());

        $paramConsulta = array();

        if ($params['aluno']) {
            $paramConsulta['aluno'] = $params['aluno'];
        }

        if ($params['alunocursoId']) {
            $paramConsulta['alunocursoId'] = $params['alunocursoId'];
        }

        $arrAlunoCursoRetorno = [];
        $arrAlunoCurso        = $serviceAlunoCurso->getRepository()->findBy($paramConsulta);

        $serviceAcadCurso            = new \Matricula\Service\AcadCurso($this->getEm());
        $serviceAcadperiodoAluno     = new \Matricula\Service\AcadperiodoAluno($this->getEm());
        $serviceFinanceiroTitulo     = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceAlunoConfigPgtoCurso = new \Financeiro\Service\FinanceiroAlunoConfigPgtoCurso($this->getEm());

        /* @var $objAlunoCurso \Matricula\Entity\AcadgeralAlunoCurso */
        foreach ($arrAlunoCurso as $objAlunoCurso) {
            $arrDadosCurso = $objAlunoCurso->toArray();
            $alunocursoId  = $objAlunoCurso->getAlunocursoId();

            $numPeriodos          = $serviceAcadperiodoAluno->periodosLetivosAluno($alunocursoId);
            $numTitulos           = $serviceFinanceiroTitulo->numeroDeTitulosDoAluno($alunocursoId);
            $alunoConfigPgtoCurso = $serviceAlunoConfigPgtoCurso->retornaConfiguracoesDePagamentoDoCurso($alunocursoId);
            $arrDatasPeriodo      = $serviceAcadperiodoAluno->retornaIntervaloDatasPeriodosAluno($alunocursoId);

            if (!$arrDadosCurso['alunocursoDataCadastro']) {
                $arrDadosCurso['alunocursoDataCadastro'] = date('d/m/Y');
            }

            $alunocursoDataExpiracao = $serviceAcadCurso->calcularTempoExpiracaoCurso(
                $objAlunoCurso->getCursocampus()->getCurso(),
                $arrDadosCurso['alunocursoDataCadastro']
            );

            $arrDadosCurso['numeroPeriodos']          = count($numPeriodos);
            $arrDadosCurso['numeroTitulos']           = $numTitulos;
            $arrDadosCurso['alunocursoDataExpiracao'] = $alunocursoDataExpiracao;
            $arrDadosCurso['alunoConfigPgtoCurso']    = $alunoConfigPgtoCurso;
            $arrDadosCurso['alunoDatasPeriodo']       = $arrDatasPeriodo;

            $arrAlunoCursoRetorno[] = $arrDadosCurso;
        }

        return $arrAlunoCursoRetorno;
    }

    public function alterarDatas($arrDados)
    {
        if (empty($arrDados)) {
            return false;
        }

        if (!$arrDados['alunocursoId']) {
            $this->setLastError('Informe o curso do aluno para alterar as datas!');
        }

        $dataMatricula        = $arrDados['alunocursoDataMatricula'];
        $dataColacao          = $arrDados['alunocursoDataColacao'];
        $dataExpedicaoDiploma = $arrDados['alunocursoDataExpedicaoDiploma'];

        $dataMatricula        = new \Datetime(self::formatDateAmericano($dataMatricula));
        $dataColacao          = $dataColacao ? new \Datetime(self::formatDateAmericano($dataColacao)) : null;
        $dataExpedicaoDiploma = (
        $dataExpedicaoDiploma ? new \Datetime(self::formatDateAmericano($dataExpedicaoDiploma)) : null
        );

        if (new \Datetime(date("Y-m-d")) < $dataMatricula) {
            $this->setLastError("A de Matrícula não pode ser maior que a data atual!");

            return false;
        } else {
            if ($dataColacao && $dataColacao <= $dataMatricula) {
                $this->setLastError("A data de Colação deve ser maior que a data de Matrícula");

                return false;
            }

            if ($dataExpedicaoDiploma && $dataExpedicaoDiploma < $dataColacao) {
                $this->setLastError("A data de Expedição deve ser maior que a data de Colação");

                return false;
            }
        }

        try {
            $serviceAluno = new \Matricula\Service\AcadgeralAluno($this->getEm());

            /** @var \Matricula\Entity\AcadgeralAlunoCurso $objAlunocurso */
            $objAlunocurso = $this->getRepository()->find($arrDados['alunocursoId']);
            $arrAlunoCurso = $objAlunocurso->toArray();

            $arrAlunoCurso['alunocursoDataMatricula']        = $arrDados['alunocursoDataMatricula'];
            $arrAlunoCurso['alunocursoDataColacao']          = $arrDados['alunocursoDataColacao'];
            $arrAlunoCurso['alunocursoDataExpedicaoDiploma'] = $arrDados['alunocursoDataExpedicaoDiploma'];

            if ($dataColacao || $dataExpedicaoDiploma) {
                $arrAlunoCurso['alunocursoSituacao'] = self::ALUNOCURSO_SITUACAO_CONCLUIDO;
            }

            if (!$this->salvarAlunoCurso($arrAlunoCurso)) {
                $this->setLastError('Falha ao alterar situação do aluno! ' . $this->getLastError());

                return false;
            }

            $serviceAluno->efetuaIntegracaoAluno($objAlunocurso->getAluno());
        } catch (\Exception $e) {
            $this->setLastError('Falha ao alterar situação do aluno!' . $e->getMessage());

            return false;
        }

        return true;
    }

    public function trocarCursoAluno($arrDados)
    {
        if (!$arrDados['campusCurso'] && !$arrDados['alunocursoId'] && !$arrDados['alteracaoObservacao']) {
            return false;
        }

        $serviceSisIntegracao = new \Sistema\Service\SisIntegracao($this->getEm());
        $serviceAcesso        = new \Acesso\Service\AcessoPessoas($this->getEm());
        $usuario              = $serviceAcesso->retornaUsuarioLogado();

        /** @var \Matricula\Entity\AcadgeralAlunoCurso $objAlunoCurso */
        $objAlunoCurso = $this->getRepository()->find($arrDados['alunocursoId']);
        /** @var \Matricula\Entity\CampusCurso $objCampusCurso */
        $objCampusCurso = $this->getRepository('Matricula\Entity\CampusCurso')->find($arrDados['campusCurso']);

        if ($objAlunoCurso && $objCampusCurso) {
            try {
                $novoAlunoCurso = clone($objAlunoCurso);
                $novoAlunoCurso->setAlunocursoId(null);
                $novoAlunoCurso->setCursocampus($objCampusCurso);
                $novoAlunoCurso->setAlunocursoSituacao(self::ALUNOCURSO_SITUACAO_PENDENTE);
                $novoAlunoCurso->setAlunocursoDataAlteracao(new \DateTime());
                $novoAlunoCurso->setAlunocursoDataCadastro(new \DateTime());
                $novoAlunoCurso->setAlunocursoDataMatricula(new \DateTime());
                $novoAlunoCurso->setUsuarioAlteracao($usuario);

                $arrAluno = $novoAlunoCurso->toArray();

                $arrAluno['motivoAlteracao']     = $arrDados['motivoAlteracao'];
                $arrAluno['alteracaoObservacao'] = $arrDados['alteracaoObservacao'];

                $this->salvarAlunoCurso($arrAluno);

                $arrDados['alunocursoSituacao'] = self::ALUNOCURSO_SITUACAO_TRANSFERENCIA;

                if (!$this->alteraSituacaoCriandoProtocolo($arrDados)) {
                    $this->setLastError("Não foi possível alterar a situação do aluno!" . $this->getLastError());

                    return false;
                }

                $serviceSisIntegracao->integracaoInteligenteParaAluno($novoAlunoCurso);

                return $arrAluno['alunocursoId'];
            } catch (\Exception $e) {
                $this->setLastError($e->getMessage());
            }
        } else {
            $this->setLastError('Não foi possível instanciar $objAlunoCurso ou $objCampusCurso!');
        }

        return false;
    }

    public function alteraSituacaoCriandoProtocolo($arrDados)
    {
        if (empty($arrDados)) {
            return false;
        }

        if (!$arrDados['alunocursoId']) {
            $this->setLastError('Informe o curso do aluno para alterar a situação!');
        }

        try {
            $serviceAluno             = new \Matricula\Service\AcadgeralAluno($this->getEm());
            $serviceProtocoloMensagem = new \Protocolo\Service\ProtocoloMensagemPadrao($this->getEm());
            $serviceProtocolo         = new \Protocolo\Service\Protocolo($this->getEm());

            $mensagemPadraoId = $this->getConfig()->localizarChave('PROTOCOLO_MENSAGEM_PADRAO_ID');

            /** @var \Matricula\Entity\AcadgeralAlunoCurso $objAlunocurso */
            $objAlunocurso = $this->getRepository()->find($arrDados['alunocursoId']);

            $arrAlunoCurso                        = $objAlunocurso->toArray();
            $arrAlunoCurso['alunocursoSituacao']  = $arrDados['alunocursoSituacao'];
            $arrAlunoCurso['motivoAlteracao']     = $arrDados['motivoAlteracao'];
            $arrAlunoCurso['alteracaoObservacao'] = $arrDados['alteracaoObservacao'];

            if (!$this->salvarAlunoCurso($arrAlunoCurso)) {
                $this->setLastError('Falha ao alterar situação do aluno! ' . $this->getLastError());

                return false;
            }

            $serviceAluno->efetuaIntegracaoAluno($objAlunocurso->getAluno());

            if ($mensagemPadraoId) {
                /** @var \Protocolo\Entity\ProtocoloMensagemPadrao $objProtocoloMensagemPadrao */
                $objProtocoloMensagemPadrao = $serviceProtocoloMensagem->getRepository()->find($mensagemPadraoId);

                if ($objProtocoloMensagemPadrao && $objSolicitacao = $objProtocoloMensagemPadrao->getSolicitacao()) {
                    $arrDadosProtocolo = array(
                        'solicitacao'                    => $objSolicitacao,
                        'protocoloMensagem'              => $objProtocoloMensagemPadrao->getMensagemConteudo(),
                        'protocoloSolicitanteVisivel'    => $serviceProtocolo::PROTOCOLO_SOLICITANTE_VISIVEL_NAO,
                        'protocoloAssunto'               => 'Analise de situação do curso do aluno' . $arrDados['alunocursoId'],
                        'protocoloSolicitanteAlunocurso' => $objAlunocurso
                    );

                    $serviceProtocolo->save($arrDadosProtocolo);
                }
            }
        } catch (\Exception $e) {
            $this->setLastError('Falha ao alterar situação do aluno! ' . $e->getMessage());

            return false;
        }

        return true;
    }

    public function outrasInformacoes($arrDados)
    {
        if (!$arrDados['alunocursoId']) {
            $this->setLastError('É necessário informar um aluno!');

            return false;
        }

        /** @var \Matricula\Entity\AcadgeralAlunoCurso $objAlunoCurso */
        $objAlunoCurso = $this->getRepository()->find($arrDados['alunocursoId']);

        if (!$objAlunoCurso) {
            $this->setLastError(
                'Ocorreu um erro ao tentar localizar este aluno, porfavor verifique o cadastro ou contate o suporte!'
            );

            return false;
        }

        try {
            $arrDadosAlunoCurso = $objAlunoCurso->toArray();

            $arrDadosAlunoCurso['alunocursoObservacoes'] = $arrDados['alunocursoObservacoes'];
            $arrDadosAlunoCurso['alunocursoCarteira']    = $arrDados['alunocursoCarteira'];
            $arrDadosAlunoCurso['enadeInformacoes']      = $arrDados['enadeInformacoes'];

            $this->salvarAlunoCurso($arrDadosAlunoCurso);

            if ($this->getLastError()) {
                $this->setLastError('Falha ao salvar as informações! ' . $this->getLastError());

                return false;
            }
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return false;
        }

        return true;
    }

    public function geraArquivoCarteira($arrDados)
    {
        if (!$arrDados) {
            $this->setLastError("Parâmetros inválidos");

            return false;
        }

        $turma = "";

        $parameters = [
            'cursocampusId' => $arrDados['cursocampusId'],
            'perId'         => $arrDados['perId'],
            'situacao'      => explode(',', $arrDados['matsituacao'])
        ];

        if ($arrDados['turmaId']) {
            $parameters['turmaId'] = is_array($arrDados['turmaId'])
                ? $arrDados['turmaId']
                : explode(
                    ',',
                    $arrDados['turmaId']
                );
            $turma                 = " AND AP.turma_id in(:turmaId) ";
        }

        $query = <<<SQL
        SELECT
            '=' as p1,
            acadgeral__aluno_curso.alunocurso_id as p2,
            acadgeral__aluno_curso.alunocurso_carteira as p3,
            'P' as p4,
            'S' as p5,
            'S' as p6,
            '' as p7,
            'N' as p8,
            '' as p9,
            pes_nome as p10,
            if(CAST(replace(replace(coalesce(pes_cpf,''),'.',''),',','') AS UNSIGNED) = 0,'',left(CAST(replace(replace(coalesce(pes_cpf,''),'.',''),',','') AS UNSIGNED), 5)) as p11,
            'ALUNO' as p12,
            'ALUNO' as p13,
            'CUSTO ALUNO' as p14,
            'ALUNO' as p15,
            'ALUNO' as p16,
            camp_nome as p17,
            '0' as p18,
            '0' as p19,
            '' as p20,
            '0' as p21,
            '0' as p22,
            '0' as p23,
            '0' as p24,
            '0' as p25,
            'N' as p26,
            pes_rg as p27,
            pes_cpf as p28,
            concat(end_logradouro,' ', end_numero,' ',end_complemento) as p29,
            end_bairro as p30,
            end_cep as p31,
            end_estado as p32,
            con_contato_celular as p33,
            end_cidade as p34,
            '' as p35,
            '' as p36,
            '' as p37,
            '' as p38,
            'S' as p39,
            '{$arrDados['acao']}' as p40,
            'N' as p41,
            'N' as p42,
            '' as p43,
            '00:02' as p44,
            '' as pEnd
        FROM
            acadgeral__aluno
                INNER JOIN
            acadgeral__aluno_curso ON acadgeral__aluno.aluno_id = acadgeral__aluno_curso.aluno_id
                INNER JOIN
            pessoa ON acadgeral__aluno.pes_id = pessoa.pes_id
                INNER JOIN
            pessoa_fisica on pessoa.pes_id = pessoa_fisica.pes_id
                INNER JOIN
            endereco on pessoa.pes_id = endereco.pes_id
                INNER JOIN
            campus_curso on acadgeral__aluno_curso.cursocampus_id = campus_curso.cursocampus_id
                INNER JOIN
            org_campus on campus_curso.camp_id = org_campus.camp_id
                INNER JOIN
            acadperiodo__aluno AP on AP.alunocurso_id = acadgeral__aluno_curso.alunocurso_id
                INNER JOIN
            acadperiodo__turma on AP.turma_id = acadperiodo__turma.turma_id
        WHERE
            campus_curso.cursocampus_id = :cursocampusId
            AND
            AP.matsituacao_id in (:situacao)
                        {$turma}
            AND
            acadperiodo__turma.per_id = :perId
            AND
            acadgeral__aluno_curso.alunocurso_carteira IS NOT NULL
            AND
            acadgeral__aluno_curso.alunocurso_carteira != ''
        GROUP BY acadgeral__aluno_curso.alunocurso_id ORDER BY acadgeral__aluno_curso.alunocurso_id
SQL;

        $result = $this->executeQueryWithParam($query, $parameters)->fetchAll();

        $texto = "";

        foreach ($result as $info) {
            $registro = implode(";", $info);
            $texto .= $registro . "\n";
        }

        if ($texto) {
            return $texto;
        }

        return null;
    }

    public function retornaArrTurmaDisciplinaPoralunocursoId($alunocursoId, $todasAsDisciplinas = false)
    {
        if (!$alunocursoId) {
            $this->setLastError("Necessário informar um registro de aluno!");

            return false;
        }

        $arrRetorno             = array();
        $serviceAlunoDisciplina = new AcadperiodoAlunoDisciplina($this->getEm());
        $serviceAlunoPeriodo    = new AcadperiodoAluno($this->getEm());
        $arrAlunoperiodos       = $serviceAlunoPeriodo->periodosLetivosAluno($alunocursoId);

        /** @var \Matricula\Entity\AcadperiodoAluno $objAlunoperiodo */
        foreach ($arrAlunoperiodos as $arrAlunoperiodo) {
            //TODO: Só integrar períodos em andamento
            $objAlunoperiodo = $serviceAlunoPeriodo->getRepository()->find($arrAlunoperiodo['alunoper_id']);
            $turmaId         = $objAlunoperiodo->getTurma()->getTurmaId();

            if ($arrRetorno[$turmaId]) {
                $arrItem = $arrRetorno[$turmaId];
            } else {
                $arrItem                = array();
                $arrItem['turma']       = $objAlunoperiodo->getTurma();
                $arrItem['situacaoId']  = $arrAlunoperiodo['situacao_id'];
                $arrItem['disciplinas'] = [];
            }

            $arrDisc = $serviceAlunoDisciplina->buscaDisciplinasAlunoPeriodo(
                $objAlunoperiodo->getAlunoperId(),
                $todasAsDisciplinas
            );

            foreach ($arrDisc as $disciplina) {
                $turmaId2 = $disciplina['turmaId'];

                if ($turmaId == $turmaId2) {
                    $arrItem['disciplinas'][] = [
                        'discId'     => $disciplina['disc'],
                        'situacaoId' => $disciplina['situacaoId']
                    ];
                } else {
                    if ($arrRetorno[$turmaId]) {
                        $arrItem2 = $arrRetorno[$turmaId];
                    } else {
                        /** @var \Matricula\Entity\AcadperiodoAlunoDisciplina $objAlunoperiodo2 */
                        $objAlunoperiodo2        = $disciplina['alunodisc'];
                        $arrItem2                = array();
                        $arrItem2['turma']       = $objAlunoperiodo2->getTurma();
                        $arrItem2['situacaoId']  = $objAlunoperiodo2->getAlunoper()->getMatsituacao()->getSituacaoId();
                        $arrItem2['disciplinas'] = [];
                    }

                    $arrItem2['disciplinas'][] = [
                        'discId'     => $disciplina['disc'],
                        'situacaoId' => $disciplina['situacaoId']
                    ];
                    $arrRetorno[$turmaId2]     = $arrItem2;
                }
            }

            $arrRetorno[$turmaId] = $arrItem;
        }

        return $arrRetorno;
    }

    public function alteraSituacaoAluno($alunoCurso, $situacao = self::ALUNOCURSO_SITUACAO_DEFERIDO)
    {
        /** @var \Matricula\Entity\AcadgeralAlunoCurso $objAlunoCurso */
        $objAlunoCurso = $this->getRepository()->find($alunoCurso);

        $serviceAcesso = new \Acesso\Service\AcessoPessoas($this->getEm());

        $arrSituacoes = $this->getAlunocursoSituacao();

        if (!$objAlunoCurso) {
            $this->setLastError("É necessário informar um aluno com curso!");

            return false;
        }
        if (!in_array($situacao, $arrSituacoes)) {
            $this->setLastError("É necessário informar uma situação válida");

            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            $objAlunoCurso->setAlunocursoSituacao($situacao);
            $objAlunoCurso->setAlunocursoDataAlteracao(new \DateTime());
            $objAlunoCurso->setUsuarioAlteracao($serviceAcesso->retornaUsuarioLogado());

            $this->getEm()->persist($objAlunoCurso);
            $this->getEm()->flush($objAlunoCurso);

            $this->getEm()->commit();

            return true;
        } catch (\Exception $e) {
            $this->setLastError(
                'Não foi possível salvar o registro de aluno no curso!' .
                '<br>' .
                $e->getMessage()
            );
        }
    }

    /**
     * @param $alunoId
     * @param $cursoId
     * @return bool|null|\Matricula\Entity\AcadgeralAlunoCurso
     */
    public function pesquisaAlunoCursoPeloAlunoEPeloCurso($alunoId, $cursoId)
    {
        if (!$alunoId || !$cursoId) {
            $this->setLastError("Aluno e curso são necessários para efetuar a busca!");

            return false;
        }

        $query = "
        SELECT alunocurso_id
        FROM acadgeral__aluno_curso
        INNER JOIN acadgeral__aluno USING(aluno_id)
        INNER JOIN campus_curso USING(cursocampus_id)
        WHERE aluno_id = :aluno AND curso_id = :curso";

        $params = array('aluno' => $alunoId, 'curso' => $cursoId);
        $result = self::executeQueryWithParam($query, $params)->fetch();

        if ($result) {
            return $this->getRepository()->find($result['alunocurso_id']);
        }

        return false;
    }

    public function emissaoCertificados($arrDados)
    {
        if (!$this->validaDadosEmissaoCertificados($arrDados)) {
            return false;
        }

        $serviceAcadperiodoResumo = new \Matricula\Service\AcadperiodoAlunoResumo($this->getEm());
        $serviceAcadCurso         = new \Matricula\Service\AcadCurso($this->getEm());
        $serviceCampuscurso       = new \Organizacao\Service\OrgCampus($this->getEm());

        $arrData           = $this->getAlunocursoArray($arrDados['alunocursoId']);
        $arrData['pesRg']  = preg_replace("/[^0-9]*/", "", $arrData['pesRg']);
        $arrData['pesCpf'] = $arrData['pesCpf'] ? $arrData['pesCpf'] : 'Sem registro de CPF';

        switch ($arrDados['certificadoModelo']) {
            case 'conclusao-curso':
            case 'conclusao-curso-historico':
            case 'certificado':
                if ($arrData['alunocursoSituacao'] != self::ALUNOCURSO_SITUACAO_CONCLUIDO) {
                    $this->setLastError("Aluno ainda não concluiu o curso");

                    return false;
                }

                break;
            case 'declaracao-matricula':
            case 'declaracao-matricula-frequencia':
                $cursoPossuiPeriodoLetivo = $arrData['campusCurso']['cursoPossuiPeriodoLetivo'] == $serviceAcadCurso::CURSO_POSSUI_PERIODO_LETIVO_SIM;

                if (!$cursoPossuiPeriodoLetivo) {
                    if ($arrData['alunocursoSituacao'] != self::ALUNOCURSO_SITUACAO_DEFERIDO) {
                        $this->setLastError("Aluno está com situação " . $arrData['alunocursoSituacao'] . "!");

                        return false;
                    }
                } elseif ($cursoPossuiPeriodoLetivo) {
                    if (!self::verificaSePossuimatriculaNoPeriodoAtual($arrData['alunocursoId'])) {
                        return false;
                    }
                }

                break;
            case 'historico-disciplinas':
                if (count($serviceAcadperiodoResumo->buscaDisciplinasAluno($arrData['alunocursoId'])) <= 0) {
                    $arrData['naoPossuiDisciplinas'] = true;
                }

                break;
        }

        if ($arrDados['certificadoModelo'] != "declaracao-matricula") {
            $serviceDisciplina = new AcadgeralDisciplina($this->getEm());

            $arrObjResumo = $serviceAcadperiodoResumo->getRepository()->findBy(
                ['alunocursoId' => $arrDados['alunocursoId']]
            );

            $discIdTcc = $this->getConfig()->localizarChave("TCC_DISC_ID");
            $discIdTcc = explode(',', $discIdTcc);

            /** @var \Matricula\Entity\AcadperiodoAlunoResumo $objAlunoRes */
            foreach ($arrObjResumo as $index => $objAlunoRes) {
                $arrResumo         = $objAlunoRes->toArray();
                $arrResumo['disc'] = $serviceDisciplina->getArray($arrResumo['discId']);

                if (in_array($arrResumo['disc']['discId'], $discIdTcc)) {
                    $arrData['monografia']    = $arrResumo['resalunoDetalhe'] ? $arrResumo['resalunoDetalhe']
                        : " - ";
                    $arrData['conceitoFinal'] = $arrResumo['resalunoNota'];
                    $orientador               = $arrResumo['disc']['docente']['docente']['pes']['pesNome'];
                    $arrData['orientador']    = $orientador ? $orientador : " - ";
                }

                $arrData['resumo'][$index] = $arrResumo;
            }

            if ($arrData['alunocursoDataMatricula']) {
                $arrData['alunocursoDataMatricula'] = preg_replace("/ .*/", "", $arrData['alunocursoDataMatricula']);
            } else {
                $arrData['alunocursoDataMatricula'] = " - ";
            }

            if ($arrData['alunocursoDataColacao']) {
                $arrData['alunocursoDataColacao'] = preg_replace("/ .*/", "", $arrData['alunocursoDataColacao']);
            } else {
                $arrData['alunocursoDataColacao'] = " - ";
            }
        }

        $arrData['certificadoInfo'] = $arrData['iesCredenciamento'];

        $arrInstituicao  = $serviceCampuscurso->retornaDadosInstituicao($arrData['campId']);
        $arrData['logo'] = preg_replace("/.*public/", "", $arrInstituicao['logo']);

        $arrDataAtual = explode('-', (new \DateTime())->format('d-m-Y'));

        $arrData['hoje'] = $arrDataAtual[0] . ' de ' . self::mesReferencia(
                $arrDataAtual[1]
            ) . ' de ' . $arrDataAtual[2];

        return $arrData;
    }

    public function validaDadosEmissaoCertificados($arrDados)
    {
        $errors = array();

        if (!$arrDados['certificadoModelo']) {
            $errors[] = 'Preencha o campo "Tipo de Certificado"!';
        }

        if (!$arrDados['alunocursoId']) {
            $errors[] = 'Preencha o campo "Aluno"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    public function verificaSePossuimatriculaNoPeriodoAtual($alunocursoId)
    {
        if (!$alunocursoId) {
            $this->setLastError("Nenhum aluno informado!");

            return false;
        }

        $serviceAcadperiodoLetivo = new \Matricula\Service\AcadperiodoLetivo($this->getEm());

        $objPeriodoAtual    = $serviceAcadperiodoLetivo->buscaPeriodoCorrente();
        $ultimoPeriodoAluno = $serviceAcadperiodoLetivo->retornaUltimoPeriodoAluno($alunocursoId);

        if ($objPeriodoAtual && $ultimoPeriodoAluno) {
            $dataAtual         = (new \DateTime())->getTimestamp();
            $dataPeriodoFim    = $objPeriodoAtual->getPerDataFim()->getTimestamp();
            $dataPeriodoInicio = $objPeriodoAtual->getPerDataInicio()->getTimestamp();

            if ($dataPeriodoInicio >= $dataAtual || $dataAtual >= $dataPeriodoFim) {
                $this->setLastError("Fora da data do período letivo!");

                return false;
            }

            return $objPeriodoAtual->getPerId() === $ultimoPeriodoAluno->getPerId();
        }

        $this->setLastError($serviceAcadperiodoLetivo->getLastError());

        return false;
    }

    public function ativaMatriculaRetornandoInfo($alunocursoId)
    {
        if (!$alunocursoId) {
            $this->setLastError("Nenhuma matrícula foi informado!");

            return false;
        }

        $arrDados                = array();
        $serviceSituacao         = new \Matricula\Service\AcadgeralSituacao($this->getEm());
        $serviceAlunoPeriodo     = new \Matricula\Service\AcadperiodoAluno($this->getEm());
        $serviceAlunoDisciplina  = new \Matricula\Service\AcadperiodoAlunoDisciplina($this->getEm());
        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo($this->getEm());

        $serviceAlunoPeriodo->setConfig($this->getConfig());
        $serviceFinanceiroTitulo->setConfig($this->getConfig());

        try {
            $this->getEm()->beginTransaction();

            /** @var \Matricula\Entity\AcadgeralAlunoCurso $objAlunoCurso */
            $objAlunoCurso = $this->getRepository()->find($alunocursoId);

            if (!$objAlunoCurso) {
                $this->setLastError("Nenhum registro de aluno encontrado!");

                return false;
            }

            /** @var |Matricula\Entity|AcadperiodoAluno $objAlunoPer */
            $objAlunoPer = $serviceAlunoPeriodo->buscaAlunoRematricula($objAlunoCurso->getAlunocursoId());

            if (!$objAlunoPer) {
                $this->setLastError("Nenhuma pré-matrícula encontrada para o aluno informado!");

                return false;
            }

            $arrDados['alunoperId']    = $objAlunoPer->getAlunoperId();
            $arrDados['perId']         = $objAlunoPer->getTurma()->getPer()->getPerId();
            $arrDados['perNome']       = $objAlunoPer->getTurma()->getPer()->getPerNome();
            $arrDados['perDataInicio'] = $objAlunoPer->getTurma()->getPer()->getPerDataInicio();
            $arrDados['perDataFim']    = $objAlunoPer->getTurma()->getPer()->getPerDataFim();
            $arrDados['turmaId']       = $objAlunoPer->getTurma()->getTurmaId();
            $arrDados['turmaNome']     = $objAlunoPer->getTurma()->getTurmaNome();
            $arrDados['alunocursoId']  = $objAlunoPer->getAlunocurso()->getAlunocursoId();
            $arrDados['cursoId']       = $objAlunoPer->getAlunocurso()->getCursocampus()->getCurso()->getCursoId();
            $arrDados['cursoNome']     = $objAlunoPer->getAlunocurso()->getCursocampus()->getCurso()->getcursoNome();
            $arrDados['cursocampusId'] = $objAlunoPer->getAlunocurso()->getCursocampus()->getCursocampusId();
            $arrDados['alunoId']       = $objAlunoPer->getAlunocurso()->getAluno()->getAlunoId();
            $arrDados['pesId']         = $objAlunoPer->getAlunocurso()->getAluno()->getPes()->getPes()->getPesId();
            $arrDados['pesNome']       = $objAlunoPer->getAlunocurso()->getAluno()->getPes()->getPes()->getPesNome();

            $arrPendencia = $this->validacoesConfigRematricula($arrDados);

            if ($arrPendencia) {
                $this->setLastError(
                    "Existem pendências que impossibilitam a rematrícula: <br>" .
                    implode("<br>", $arrPendencia)
                );

                return false;
            }

            $objAlunoPer->setMatsituacao($serviceSituacao->retornaSituacaoMatriculaProvisioria());
            $objAlunoPer->setAlunoperSitData(new \Datetime());

            $this->getEm()->persist($objAlunoPer);
            $this->getEm()->flush($objAlunoPer);

            if (!$serviceAlunoPeriodo->geraMensalidadesAluno($arrDados['pesId'], $arrDados['alunoperId'])) {
                $this->setLastError(
                    "Não foi possível criar os títulos para o Aluno! " . $serviceAlunoPeriodo->getLastError()
                );

                return false;
            }

            $arrDados['titulos']    = $serviceFinanceiroTitulo->buscaTitulosAlunoPeriodo($arrDados['alunoperId']);
            $arrDados['disciplina'] = $serviceAlunoDisciplina->verificaRegistroDeAlunoNaDisciplina($objAlunoPer);

            if (!$arrDados['disciplina']) {
                $this->setLastError($serviceAlunoDisciplina->getLastError());

                return false;
            }

            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Erro:' . $ex->getMessage());
        }

        return $arrDados;
    }

    public function validacoesConfigRematricula($param)
    {
        $serviceFinanceiro      = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceAlunoResumo     = new \Matricula\Service\AcadperiodoAlunoResumo($this->getEm());
        $serviceDocumentoPessoa = new \Documentos\Service\DocumentoPessoa($this->getEm());
        $serviceCursoConfig     = new \Matricula\Service\AcadCursoConfig($this->getEm());

        $validarDocumentosPendentes  =
            $this->getConfig()->localizarChave('VALIDAR_DOCUMENTOS_PEDENTES_REMATRICULA');
        $validarPendenciasAcademicas =
            $this->getConfig()->localizarChave('VALIDAR_PENDENCIAS_ACADEMICAS_REMATRICULA');
        $validarTitulosAbertos       =
            $this->getConfig()->localizarChave('VALIDAR_TITULOS_ABERTOS_REMATRICULA');
        $validarTitulosVencidos      =
            $this->getConfig()->localizarChave('VALIDAR_TITULOS_VENCIDOS_REMATRICULA');

        $arrPendencias = [];

        if ($validarDocumentosPendentes && $validarDocumentosPendentes == 1) {
            $arrDocumentos = $serviceDocumentoPessoa->retornaDocumentosNaoEntregues($param['pesId']);

            if ($arrDocumentos) {
                $arrPendencias['documentos'] = (
                    " * <b>Documentos Pendentes</b>: <br> - " . implode('<br> - ', $arrDocumentos)
                );
            }
        }

        if ($validarPendenciasAcademicas && $validarPendenciasAcademicas == 1) {
            $arrConfigCurso = $serviceCursoConfig->getArrayConfiguracoes($param['cursoId']);
            $arrConfigCurso = $arrConfigCurso[0];

            $pedenciasAcademicas =
                $serviceAlunoResumo->retornaNumeroDeReprovacoesNaoCumpridas($param['alunocursoId']);

            if ($pedenciasAcademicas > $arrConfigCurso['cursoconfigNumeroMaximoPendencias']) {
                $arrPendencias['pedenciaAcademica'] = (
                    " * <b>Pendências Acadêmicas</b>: " . $pedenciasAcademicas
                );
            }
        }

        if ($validarTitulosAbertos && $validarTitulosAbertos == 1) {
            $arrParamTitulos = [
                'alunocursoId' => $param['alunocursoId'],
                'pesId'        => $param['pesId'],
                'alunoPerId'   => $param['alunoPerId'],
            ];

            $quantidadeTitulosAbertos = $serviceFinanceiro->buscaTitulosAbertoEmAtrasoAluno($arrParamTitulos);

            if ($quantidadeTitulosAbertos > 0) {
                $arrPendencias['titulosAbertos'] = (
                    " * <b>Títulos Em Aberto</b>: " . $quantidadeTitulosAbertos
                );
            }
        }

        if ($validarTitulosVencidos && $validarTitulosVencidos == 1) {
            $arrParamTitulos = [
                'alunocursoId'  => $param['alunocursoId'],
                'pesId'         => $param['pesId'],
                'alunoPerId'    => $param['alunoPerId'],
                'titulosAtraso' => true,
            ];

            $quantidadeTitulosVencidos = $serviceFinanceiro->buscaTitulosAbertoEmAtrasoAluno($arrParamTitulos);

            if ($quantidadeTitulosVencidos > 0) {
                $arrPendencias['titulosAbertos'] = (
                    " * <b>Títulos Vencidos</b>: " . $quantidadeTitulosVencidos
                );
            }
        }

        return $arrPendencias;
    }

    public function verificaSeAlunoPossuiRematricula($alunocursoId)
    {
        $serviceAlunoPer   = new \Matricula\Service\AcadperiodoAluno($this->getEm());
        $serviceAlunoDisc  = new \Matricula\Service\AcadgeralDisciplina($this->getEm());
        $serviceTituloTipo = new \Financeiro\Service\FinanceiroTituloTipo($this->getEm());

        $sql = "
        SELECT
            aac.alunocurso_id, aac.cursocampus_id,
            ac.curso_id, ac.curso_nome,
            aa.alunoper_id, aa.turma_id, turma.turma_nome, turma.turma_serie,
            turma.per_id, turma.mat_cur_id,
            al.per_matricula_inicio, al.per_matricula_fim,
            al.per_nome per_nome

        FROM acadgeral__aluno_curso aac

        INNER JOIN acadperiodo__aluno aa ON aa.alunocurso_id=aac.alunocurso_id
        INNER JOIN campus_curso cc ON cc.cursocampus_id=aac.cursocampus_id
        INNER JOIN acad_curso ac ON ac.curso_id=cc.curso_id
        INNER JOIN acadperiodo__turma turma ON turma.turma_id=aa.turma_id
        INNER JOIN acadperiodo__letivo  al ON al.per_id=turma.per_id

        WHERE aa.alunocurso_id=:alunocursoId AND aa.matsituacao_id = " . $serviceAlunoPer::ALUNOPERIODO_SITUACAO_PRE_MATRICULA . "
        AND (now() BETWEEN al.per_rematricula_online_inicio AND al.per_rematricula_online_fim)";

        if ($result = $this->executeQueryWithParam($sql, ['alunocursoId' => $alunocursoId])->fetch()) {
            $result['cursocampus'] = $result['cursocampus_id'];
            $result['perId']       = $result['per_id'];

            $result['titulos']     = $serviceTituloTipo->retornoTiposDeTituloParaEmissaoNaMatricula($result);
            $result['disciplinas'] = $serviceAlunoDisc->retornaDisciplinaCursoPelaMatrizSerie(
                $result['mat_cur_id'],
                $result['turma_serie'],
                $result['turma_id'],
                false
            );
        }

        if (count($result) == 0) {
            return false;
        }

        return $result;
    }

    public function efetivarDeferimentoAutomaticoCurso($arrDados, $ignorarPlanoDePagamento = false)
    {
        $arrParam                      = [];
        $permitirDeferimentoAutomatico = true;
        $serviceAcadCursoConfig        = new \Matricula\Service\AcadCursoConfig($this->getEm());

        if ($arrDados['alunocursoId']) {
            $arrParam['alunocursoId'] = $arrDados['alunocursoId'];
        } else {
            if ($arrDados['alunoId']) {
                $arrParam['aluno'] = $arrDados['alunoId'];
            }

            if ($arrDados['campusCurso']) {
                $arrParam['cursocampus'] = $arrDados['campusCurso'];
            } elseif ($arrDados['cursocampusId']) {
                $arrParam['cursocampus'] = $arrDados['cursocampusId'];
            }
        }

        /** @var $objAcadgeralAlunoCurso \Matricula\Entity\AcadgeralAlunoCurso */
        $objAcadgeralAlunoCurso = $this->getRepository()->findOneBy($arrParam);

        if (!$objAcadgeralAlunoCurso) {
            return false;
        }

        /** @var $objCursoConfig \Matricula\Entity\AcadCursoConfig */
        $objCursoConfig    = $serviceAcadCursoConfig->retornaObjCursoConfig(
            $objAcadgeralAlunoCurso->getCursocampus()->getCurso()->getCursoId()
        );
        $arrConfgPagamento = isset($arrDados['tipoTitulo']) ? $arrDados['tipoTitulo'] : [];

        if (!$arrConfgPagamento && !$ignorarPlanoDePagamento) {
            return false;
        }

        if (!$objCursoConfig || !$objCursoConfig->verificarDeferimentoAutomatico()) {
            return false;
        }

        foreach ($arrConfgPagamento as $value) {
            if (trim($value['configPgtoCursoObs'])) {
                $permitirDeferimentoAutomatico = false;
                break;
            }
        }

        if (!$permitirDeferimentoAutomatico) {
            return false;
        }

        $arrParam = [
            'alunocursoId'                => $objAcadgeralAlunoCurso->getAlunocursoId(),
            'naoValidarAlteracoesNoPlano' => true,
            'validarPresencaDePlano'      => true
        ];

        if ($this->deferirCursoAluno($arrParam)) {
            return true;
        }

        return false;
    }

    public function retornaInformacoesRematricula($alunoperId)
    {
        $serviceAlunoPeriodo     = new \Matricula\Service\AcadperiodoAluno($this->getEm());
        $serviceAlunoDisciplina  = new \Matricula\Service\AcadperiodoAlunoDisciplina($this->getEm());
        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo($this->getEm());

        /** @var |Matricula\Entity|AcadperiodoAluno $objAlunoPer */
        $objAlunoPer = $serviceAlunoPeriodo->getRepository()->find($alunoperId);

        if (!$objAlunoPer) {
            $this->setLastError("Aluno informado não encontrado no sistema!");

            return false;
        }

        $arrDados['alunoperId']    = $objAlunoPer->getAlunoperId();
        $arrDados['perId']         = $objAlunoPer->getTurma()->getPer()->getPerId();
        $arrDados['perNome']       = $objAlunoPer->getTurma()->getPer()->getPerNome();
        $arrDados['perDataInicio'] = $objAlunoPer->getTurma()->getPer()->getPerDataInicio();
        $arrDados['perDataFim']    = $objAlunoPer->getTurma()->getPer()->getPerDataFim();
        $arrDados['turmaId']       = $objAlunoPer->getTurma()->getTurmaId();
        $arrDados['turmaNome']     = $objAlunoPer->getTurma()->getTurmaNome();
        $arrDados['alunocursoId']  = $objAlunoPer->getAlunocurso()->getAlunocursoId();
        $arrDados['cursoId']       = $objAlunoPer->getAlunocurso()->getCursocampus()->getCurso()->getCursoId();
        $arrDados['cursoNome']     = $objAlunoPer->getAlunocurso()->getCursocampus()->getCurso()->getcursoNome();
        $arrDados['cursocampusId'] = $objAlunoPer->getAlunocurso()->getCursocampus()->getCursocampusId();
        $arrDados['alunoId']       = $objAlunoPer->getAlunocurso()->getAluno()->getAlunoId();
        $arrDados['pesId']         = $objAlunoPer->getAlunocurso()->getAluno()->getPes()->getPes()->getPesId();
        $arrDados['pesNome']       = $objAlunoPer->getAlunocurso()->getAluno()->getPes()->getPes()->getPesNome();

        $arrPendencia = $this->validacoesConfigRematricula($arrDados);

        if ($arrPendencia) {
            $this->setLastError(
                "Existem pendências que impossibilitam a rematrícula: <br>" .
                implode("<br>", $arrPendencia)
            );

            return false;
        }

        $arrDados['titulos']    = $serviceFinanceiroTitulo->buscaTitulosAlunoPeriodo($arrDados['alunoperId']);
        $arrDados['disciplina'] = $serviceAlunoDisciplina->verificaRegistroDeAlunoNaDisciplina($objAlunoPer);

        if (!$arrDados['disciplina']) {
            $this->setLastError($serviceAlunoDisciplina->getLastError());

            return false;
        }

        return $arrDados;
    }
}
