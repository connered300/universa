<?php

namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class AcadgeralMotivoAlteracao
 * @package Matricula\Service
 */
class AcadgeralMotivoAlteracao extends AbstractService
{
    const MOTIVO_SITUACAO_DEFERIDO             = 'Deferido';
    const MOTIVO_SITUACAO_INDEFERIDO           = 'Indeferido';
    const MOTIVO_SITUACAO_PENDENTE             = 'Pendente';
    const MOTIVO_SITUACAO_TRANCADO             = 'Trancado';
    const MOTIVO_SITUACAO_CANCELADO            = 'Cancelado';
    const MOTIVO_SITUACAO_TRANSFERENCIA        = 'Transferencia';
    const MOTIVO_SITUACAO_CONCLUIDO            = 'Concluido';
    const MOTIVO_SITUACAO_AGUARDANDO_PAGAMENTO = 'Aguardando Pagamento';

    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadgeralMotivoAlteracao');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2MotivoSituacao($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getMotivoSituacao());
    }

    /**
     * @return array
     */
    public static function getMotivoSituacao()
    {
        return array(
            self::MOTIVO_SITUACAO_DEFERIDO,
            self::MOTIVO_SITUACAO_INDEFERIDO,
            self::MOTIVO_SITUACAO_PENDENTE,
            self::MOTIVO_SITUACAO_TRANCADO,
            self::MOTIVO_SITUACAO_CANCELADO,
            self::MOTIVO_SITUACAO_TRANSFERENCIA,
            self::MOTIVO_SITUACAO_CONCLUIDO,
            self::MOTIVO_SITUACAO_AGUARDANDO_PAGAMENTO
        );
    }

    /**
     * @param $params
     * @return mixed
     */
    public function pesquisaForJson($params)
    {
        $sql        = 'SELECT * FROM acadgeral__motivo_alteracao';
        $parameters = array();
        $where      = array();

        $motivoDescricao = ($params['q']) ? $params['q'] : $params['query'];
        $motivoSituacao  = ($params['motivoSituacao']) ? $params['motivoSituacao'] : '';

        if (!$motivoSituacao) {
            return [];
        }

        if ($motivoDescricao) {
            $where[]                        = ' motivo_descricao LIKE :motivo_descricao';
            $parameters['motivo_descricao'] = $motivoDescricao . '%';
        }

        if ($params['motivoId']) {
            $where[] .= ' motivo_id <> :motivo_id';
            $parameters['motivo_id'] = $params['motivoId'];
        }

        $where[] .= ' FIND_IN_SET(:motivoSituacao,motivo_situacao)';
        $parameters['motivoSituacao'] = $params['motivoSituacao'];

        if ($where) {
            $sql .= " WHERE " . implode('AND', $where);
        }

        $sql .= " ORDER BY motivo_descricao";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['motivoId']) {
                /** @var $objAcadgeralMotivoAlteracao \Matricula\Entity\AcadgeralMotivoAlteracao */
                $objAcadgeralMotivoAlteracao = $this->getRepository()->find($arrDados['motivoId']);

                if (!$objAcadgeralMotivoAlteracao) {
                    $this->setLastError('Registro de motivo de alteração não existe!');

                    return false;
                }
            } else {
                $objAcadgeralMotivoAlteracao = new \Matricula\Entity\AcadgeralMotivoAlteracao();
            }

            $objAcadgeralMotivoAlteracao->setMotivoDescricao($arrDados['motivoDescricao']);
            $objAcadgeralMotivoAlteracao->setMotivoSituacao($arrDados['motivoSituacao']);

            $this->getEm()->persist($objAcadgeralMotivoAlteracao);
            $this->getEm()->flush($objAcadgeralMotivoAlteracao);

            $this->getEm()->commit();

            $arrDados['motivoId'] = $objAcadgeralMotivoAlteracao->getMotivoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de motivo de alteração!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['motivoDescricao']) {
            $errors[] = 'Por favor preencha o campo "descrição"!';
        }

        if (!$arrParam['motivoSituacao']) {
            $errors[] = 'Por favor preencha o campo "situação"!';
        }

        if ($this->verificaSeDescricaoEstaDuplicado($arrParam['motivoDescricao'], $arrParam['motivoId'])) {
            $errors[] = "Já existe um motivo com esta descrição!";
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function verificaSeDescricaoEstaDuplicado($motivoDescricao, $motivoId = false)
    {
        $sql        = 'SELECT count(*) AS qtd FROM acadgeral__motivo_alteracao WHERE motivo_descricao LIKE :motivo_descricao';
        $parameters = array('motivo_descricao' => $motivoDescricao);

        if ($motivoId) {
            $sql .= ' AND motivo_id<>:motivo_id';
            $parameters['motivo_id'] = $motivoId;
        }

        $result = $this->executeQueryWithParam($sql, $parameters)->fetch();

        return $result['qtd'] > 0;
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM acadgeral__motivo_alteracao";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $motivoId
     * @return array
     */
    public function getArray($motivoId)
    {
        try {
            /** @var $objAcadgeralMotivoAlteracao \Matricula\Entity\AcadgeralMotivoAlteracao */
            $objAcadgeralMotivoAlteracao = $this->getRepository()->find($motivoId);

            $arrDados = $objAcadgeralMotivoAlteracao->toArray();

            if ($arrDados['motivoSituacao']) {
                $arrDados['motivoSituacao'] = explode(',', $arrDados['motivoSituacao']);
            }
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível buscar o registro de motivo de alteração!' . $e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        return $arrDados;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('motivoId' => $params['id']));
        } elseif ($params['motivoDescricao']) {
            $arrEntities = $this->getRepository()->findBy(array('motivoDescricao' => $params['motivoDescricao']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Matricula\Entity\AcadgeralMotivoAlteracao */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getMotivoId(),
                $params['value'] => $objEntity->getMotivoDescricao()
            );
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['motivoId']) {
            $this->setLastError('Para remover um registro de motivo de alteração é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objAcadgeralMotivoAlteracao \Matricula\Entity\AcadgeralMotivoAlteracao */
            $objAcadgeralMotivoAlteracao = $this->getRepository()->find($param['motivoId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAcadgeralMotivoAlteracao);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de motivo de alteração.');

            return false;
        }

        return true;
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $view->setVariable("arrMotivoSituacao", $this->getArrSelect2MotivoSituacao());
    }
}
?>