<?php

namespace Matricula\Service;

use Doctrine\DBAL\Platforms\SQLAnywhere11Platform;
use VersaSpine\Service\AbstractService;

class AcadperiodoMatrizDisciplina extends AbstractService
{
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadperiodoMatrizDisciplina');
    }

    protected function valida($dados)
    {
    }

    public function pesquisaForJson($params)
    {
        $sql        = <<<SQL
        SELECT
          *,
          acadgeral__disciplina.disc_id AS disc_id,
          acadgeral__disciplina.disc_id AS discicplina_id,
          acadgeral__disciplina.disc_nome AS disciplina_nome
        FROM
          acadperiodo__matriz_disciplina
        INNER JOIN
          acadgeral__disciplina ON acadgeral__disciplina.disc_id = acadperiodo__matriz_disciplina.disc_id
        INNER JOIN
          acadperiodo__turma USING(mat_cur_id)
        INNER JOIN 
         campus_curso USING (cursocampus_id)
        WHERE 1
SQL;
        $parameters = array();

        $perDiscId = $params['q'] ? $params['q'] : null;

        if ($params['query']) {
            $sql .= " AND  acadgeral__disciplina.disc_nome Like :query";

            $parameters['query'] = "{$params['query']}%";
        }

        if ($perDiscId) {
            $sql .= " AND per_disc_id <> :perdiscId";
            $parameters['perdiscId'] = $perDiscId;
        }

        if ($params['matCurId']) {
            $matCurId = $params['matCurId'];
            $sql .= " AND mat_cur_id = :matCurId";
            $parameters['matCurId'] = $matCurId;
        }

        if ($params['turmaId']) {
            $turmaId = $params['turmaId'];
            $sql .= " AND turma_id = :turmaId AND per_disc_periodo = turma_serie";
            $parameters['turmaId'] = $turmaId;
        }

        if ($params['perId']) {
            $sql .= " AND per_id=:perId";
            $parameters['perId'] = $params['perId'];
        }
        if ($params['cursoId']) {
            $sql .= " AND curso_id=:cursoId";
            $parameters['cursoId'] = $params['cursoId'];
        }
        if ($params['disciplinasOcultar']) {
            $disciplinasOcultar = implode(',', $params['disciplinasOcultar']);
            $sql .= " AND acadperiodo__matriz_disciplina.disc_id NOT IN ({$disciplinasOcultar}) AND
             acadperiodo__turma.mat_cur_id=acadperiodo__matriz_disciplina.mat_cur_id and acadperiodo__turma.mat_cur_id and turma_serie = per_disc_periodo";
        }

        if ($params['perDiscPeriodo']) {
            $sql .= " AND per_disc_periodo=:perDiscPeriodo";
            $parameters['perDiscPeriodo'] = $params['perDiscPeriodo'];
        }

        $sql .= " GROUP BY acadperiodo__matriz_disciplina.disc_id";
        $sql .= " ORDER BY turma_serie,mat_cur_id DESC";

        if (!isset($params['naoLimitar'])) {
            $sql .= " LIMIT 0,40";
        }

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function disciplinasMatrizTurma(\Matricula\Entity\AcadperiodoTurma $objTurma)
    {
        $sql = "
        SELECT apmd.disc_id
        FROM acadperiodo__turma apt
        INNER JOIN acadperiodo__matriz_disciplina apmd ON apmd.mat_cur_id=apt.mat_cur_id AND apmd.per_disc_periodo=apt.turma_serie
        WHERE apt.turma_id=" . $objTurma->getTurmaId();

        $result = $this->executeQueryWithParam($sql)->fetchAll(\PDO::FETCH_COLUMN);

        return $result;
    }

    public function buscaDiscPeriodo($per, $matriz)
    {
        $matrizDisc = $this->executeQuery(
            "
          SELECT disc_id FROM acadperiodo__matriz_disciplina WHERE per_disc_periodo = {$per} AND mat_cur_id = {$matriz}"
        )->fetchAll();
        $arr        = [];
        foreach ($matrizDisc as $disc) {
            $arr[(int)$disc['disc_id']] = 'selected';
        }

        return $arr;
    }

    public function buscaMatrizesQueContenhamDisciplina($disciplinaId = null, $perId = null)
    {
        if (!$perId) {
            $perId = (new \Matricula\Service\AcadperiodoLetivo($this->getEm()))->buscaPeriodoAtual()->getPerId();
        }

        if ($disciplinaId) {
            $matrizes = $this->getRepository()->findBy(['disc' => $disciplinaId]);

            if (!empty($matrizes)) {
                $turmaRepository = $this->getEm()->getRepository('Matricula\Entity\AcadperiodoTurma');
                foreach ($matrizes as $matriz) {
                    $turmas = $turmaRepository->findBy(
                        [
                            'matCur'     => $matriz->getMatCur()->getMatCurId(),
                            'per'        => $perId,
                            'turmaSerie' => $matriz->getPerDiscPeriodo()
                        ]
                    );
                    if (!empty($turmas)) {
                        foreach ($turmas as $turma) {
                            $result[] = ['id' => $turma->getTurmaId(), 'nome' => $turma->getTurmaNome()];
                        }
                    }
                }

                return $result;
            }
        }

        return null;
    }

    /**
     * @param array
     */

    public function buscaDisciplinasMatrizTranzicao($param)
    {
        $serviceTurmas = new \Matricula\Service\AcadperiodoTurma($this->getEm());

        $arrTurmasRegulares = $serviceTurmas->tipoTurmasRegulares();
        $turmasRegulares    = implode(',', $arrTurmasRegulares);
        $sqlCurso           = '';

        if ($param['curso_id']) {
            $sqlCurso = " AND acad_curso.curso_id = :curso_id";
        } else {
            unset($param['curso_id']);
        }

        $select = "SELECT
                        disciplina.disc_id id ,mat_cur_descricao matriz, turma_serie periodo, disc_nome nome, per_disc_creditos creditos, (per_disc_chestagio + per_disc_chpratica + per_disc_chteorica ) 'cargaHoraria'
                    FROM
                        acadperiodo__matriz_curricular matriz
                            INNER JOIN
                        acadperiodo__turma turma ON turma.mat_cur_id = matriz.mat_cur_id
                            INNER JOIN
                        acadperiodo__matriz_disciplina MatrizDisc ON MatrizDisc.mat_cur_id = matriz.mat_cur_id
                            INNER JOIN
                        acadgeral__disciplina disciplina ON disciplina.disc_id = MatrizDisc.disc_id
                            INNER JOIN
                        campus_curso ON campus_curso.cursocampus_id = turma.cursocampus_id
                           INNER JOIN
                        acad_curso ON acad_curso.curso_id = campus_curso.curso_id
                    WHERE
                        tturma_id IN($turmasRegulares) AND per_id = :per_id
                            AND turma_serie = per_disc_periodo {$sqlCurso}
                    GROUP BY turma_serie , matriz.mat_cur_id , disciplina.disc_id
                    ORDER BY turma_serie";

        $disciplinas = $this->executeQueryWithParam($select, $param)->fetchAll();

        return $disciplinas;
    }
}