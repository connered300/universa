<?php

namespace Matricula\Service;

use Doctrine\DBAL\Platforms\SQLAnywhere11Platform;
use VersaSpine\Service\AbstractService;

/**
 * Class AcadNivel
 * @package Matricula\Service
 */
class AcadNivel extends AbstractService
{
    const NIVEL_ENSINO_FUNDAMENTAL = 'Ensino Fundamental';
    const NIVEL_ENSINO_MEDIO = 'Ensino Médio';
    const NIVEL_GRADUACAO = 'Graduação';
    const NIVEL_POS_GRADUACAO = 'Pós Graduação';
    const NIVEL_LATU_SENSU = 'Latu Sensu';
    const NIVEL_STRICTO_SENSU = 'Stricto Sensu';
    const NIVEL_BACHARELADO = 'Bacharelado';
    const NIVEL_LICENCIATURA = 'Licenciatura';
    const NIVEL_TECNOLOGO = 'Tecnólogo';
    const NIVEL_CAPACITACAO = 'Capacitação';
    const NIVEL_COMPLEMENTACAO_PEDAGOGICA = 'Complementação Pedagógica (R2)';
    const NIVEL_SEGUNDA_LICENCIATURA = 'Segunda Licenciatura';
    /**
     * @var null
     */
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadNivel');
    }

    /**
     * @param $params
     * @return mixed
     */
    public function pesquisaForJson($params)
    {
        $sql       = '
        SELECT
        nivel_id, nivel_nome,
        nivel_id AS nivelId, nivel_nome nivelNome
        FROM acad_nivel
        LEFT JOIN acad_curso USING (nivel_id)
        WHERE curso_id IS NOT NULL AND ';
        $nivelNome = false;
        $nivelId   = false;

        if ($params['q']) {
            $nivelNome = $params['q'];
        } elseif ($params['query']) {
            $nivelNome = $params['query'];
        }

        if ($params['nivelId']) {
            $nivelId = $params['nivelId'];
        }

        $parameters = array('nivel_nome' => "{$nivelNome}%");
        $sql .= ' nivel_nome LIKE :nivel_nome';

        if ($nivelId) {
            $parameters['nivel_id'] = $nivelId;
            $sql .= ' AND nivel_id <> :nivel_id';
        }

        $sql .= " GROUP BY nivel_id";
        $sql .= " ORDER BY nivel_nome";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceAcadNivel = new \Matricula\Service\AcadNivel($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['nivelId']) {
                $objAcadNivel = $this->getRepository()->find($arrDados['nivelId']);

                if (!$objAcadNivel) {
                    $this->setLastError('Registro de nível de ensino não existe!');

                    return false;
                }
            } else {
                $objAcadNivel = new \Matricula\Entity\AcadNivel();
            }

            if ($arrDados['nivelPai']) {
                /** @var $objAcadNivelPai \Matricula\Entity\AcadNivel */
                $objAcadNivelPai = $serviceAcadNivel->getRepository()->find($arrDados['nivelPai']);

                if (!$objAcadNivelPai) {
                    $this->setLastError('Registro de nível de ensino não existe!');

                    return false;
                }

                $objAcadNivel->setNivelPai($objAcadNivelPai);
            }

            $objAcadNivel->setNivelNome($arrDados['nivelNome']);
            $objAcadNivel->setNivelObs($arrDados['nivelObs']);

            $this->getEm()->persist($objAcadNivel);
            $this->getEm()->flush($objAcadNivel);

            $this->getEm()->commit();

            $arrDados['nivelId'] = $objAcadNivel->getNivelId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de nível de ensino!');
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['nivelNome']) {
            $errors[] = 'Por favor preencha o campo "nome"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM acad_nivel";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $nivelId
     * @return array
     */
    public function getArray($nivelId)
    {
        $arrDados = array();
        /** @var $objAcadNivel \Matricula\Entity\AcadNivel */
        $objAcadNivel = $this->getRepository()->find($nivelId);

        try {
            $arrDados = $objAcadNivel->toArray();

            if ($arrDados['nivelPai']) {
                /** @var $objAcadNivelPai \Matricula\Entity\AcadNivel */
                $objAcadNivelPai = $objAcadNivel->getNivelPai();

                if ($objAcadNivelPai) {
                    $arrDados['nivelPai'] = array(
                        'id'   => $objAcadNivelPai->getNivelId(),
                        'text' => $objAcadNivelPai->getNivelNome()
                    );
                }
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    public function getDescricao($id)
    {
        if (!is_array($id)) {
            $id = array($id);
        }

        $id = implode($id, ',');

        $query = '
        SELECT group_concat(nivel_nome SEPARATOR ", ") text
        FROM acad_nivel
        WHERE acad_nivel.nivel_id IN (' . $id . ')';

        $result = $this->executeQueryWithParam($query)->fetch();

        return $result['text'];
    }


    public function getArrSelect($params = array()){
        $arrEntities = $this->getRepository()->findBy([], ['nivelNome' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Matricula\Entity\AcadNivel */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[$objEntity->getNivelId()] = $objEntity->getNivelNome();
        }

        return $arrEntitiesArr;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('nivelId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Matricula\Entity\AcadNivel */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getNivelId(),
                $params['value'] => $objEntity->getNivelNome()
            );
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['nivelId']) {
            $this->setLastError('Para remover um registro de nível de ensino é necessário especificar o código.');

            return false;
        }

        try {
            $objAcadNivel = $this->getRepository()->find($param['nivelId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAcadNivel);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de nível de ensino.');

            return false;
        }

        return true;
    }
}
?>