<?php
namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class AcadperiodoAluno
 * @package Matricula\Service
 */
class EtapaFinal extends AbstractService
{
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadperiodoAluno');
    }

    public function valida($dados){}

    public function pesquisaForJson($params){

    }

    public function paginationAjax($arrayData,$periodo)
    {
        $query = "SELECT
   pes_nome Nome,
   TRIM(LEADING 0 FROM alunoCurso.Alunocurso_id) Matricula,
   disc_nome Disciplina,
   count(disciplina.disc_id) Qtd,
   turma_nome Turma,
   letivo.per_nome Periodo,
  if(situacao.alunocurso_id,'SIM','NÃO') Pendencia
FROM
   pessoa
       NATURAL JOIN
   acadgeral__aluno aluno
       NATURAL JOIN
   acadgeral__aluno_curso alunoCurso
       LEFT JOIN
   (SELECT
       MAX(alunoper_id) alunoPeriodo,
           acadperiodo__aluno.alunocurso_id
   FROM
       acadperiodo__aluno
   WHERE
       1
   GROUP BY alunocurso_id) alunoPerMax ON alunoCurso.alunocurso_id = alunoPerMax.alunocurso_id
       LEFT JOIN
   acadperiodo__aluno alunoPeriodo ON alunoPerMax.alunoPeriodo = alunoPeriodo.alunoper_id
       NATURAL JOIN
   acadperiodo__turma turma
       INNER JOIN
   acadperiodo__matriz_curricular matriz ON turma.mat_cur_id = matriz.mat_cur_id
       LEFT JOIN
   acadperiodo__matriz_disciplina matrizDisciplina ON matrizDisciplina.mat_cur_id = matriz.mat_cur_id
       LEFT JOIN
   acadperiodo__aluno_resumo alunoResumo ON alunoResumo.alunocurso_id = alunoCurso.alunocurso_id and alunoResumo.disc_id = matrizDisciplina.disc_id
      left join
      acadgeral__disciplina disciplina on disciplina.disc_id =  matrizDisciplina.disc_id
            LEFT JOIN
      (
			SELECT DISTINCT
			alunocurso_id
		FROM
			financeiro__titulo
				JOIN
			acadgeral__aluno USING (pes_id)
				LEFT JOIN
			acadgeral__aluno_curso USING (aluno_id)
				LEFT JOIN
			acadperiodo__aluno USING (alunocurso_id)
		WHERE
			titulo_data_pagamento IS NULL
				AND titulo_estado = 'Aberto'
      )situacao on situacao.alunocurso_id = alunoCurso.alunocurso_id
      LEFT JOIN
      acadperiodo__letivo letivo USING(per_id)
WHERE
   turma_serie = 10 and res_id is null
   AND per_id = ".$periodo."
   group by alunoCurso.alunocurso_id";

        $arrayData['order'] = 'Matricula';

        $result = $this->paginationDataTablesAjax($query, $arrayData, null, false);

        return $result;
    }

	public function paginationAjaxDisciplinas($arrayData)
	{
		$query = "SELECT
   pes_nome Nome,
   TRIM(LEADING 0 FROM alunoCurso.Alunocurso_id) Matricula,
   disc_nome Disciplina,
   turma_nome Turma,
   CONCAT(TRIM(LEADING 0 FROM per_disc_periodo),'° Período') Periodo,
   alunoPeriodo.alunoper_id alunoperId
FROM
   pessoa
       NATURAL JOIN
   acadgeral__aluno aluno
       NATURAL JOIN
   acadgeral__aluno_curso alunoCurso
       LEFT JOIN
   (SELECT
       MAX(alunoper_id) alunoPeriodo,
           acadperiodo__aluno.alunocurso_id
   FROM
       acadperiodo__aluno
   WHERE
       1
   GROUP BY alunocurso_id) alunoPerMax ON alunoCurso.alunocurso_id = alunoPerMax.alunocurso_id
       LEFT JOIN
   acadperiodo__aluno alunoPeriodo ON alunoPerMax.alunoPeriodo = alunoPeriodo.alunoper_id
       NATURAL JOIN
   acadperiodo__turma turma
       INNER JOIN
   acadperiodo__matriz_curricular matriz ON turma.mat_cur_id = matriz.mat_cur_id
       LEFT JOIN
   acadperiodo__matriz_disciplina matrizDisciplina ON matrizDisciplina.mat_cur_id = matriz.mat_cur_id
       LEFT JOIN
   acadperiodo__aluno_resumo alunoResumo ON alunoResumo.alunocurso_id = alunoCurso.alunocurso_id and alunoResumo.disc_id = matrizDisciplina.disc_id
      left join
      acadgeral__disciplina disciplina on disciplina.disc_id =  matrizDisciplina.disc_id
WHERE
   turma_serie = 10 and res_id is null
   AND alunoCurso.alunocurso_id = ".$arrayData['matricula'];

		$arrayData['order'] = "Nome";

		$result = $this->paginationDataTablesAjax($query, $arrayData, null, false);

		return $result;
	}

    public function buscaInfoAluno(){

    }
}