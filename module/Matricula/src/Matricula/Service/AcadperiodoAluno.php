<?php

namespace Matricula\Service;

use Acesso\Service\AcessoPessoas;
use Respect\Validation\Validator as v;
use VersaSpine\Service\AbstractService;

/**
 * Class AcadperiodoAluno
 * @package Matricula\Service
 */
class AcadperiodoAluno extends AbstractService
{
    const ALUNOPERIODO_SITUACAO_MATRICULADO           = 1;
    const ALUNOPERIODO_SITUACAO_TRANCADO              = 2;
    const ALUNOPERIODO_SITUACAO_DISPENSADO            = 3;
    const ALUNOPERIODO_SITUACAO_TRANSFERENCIA         = 5;
    const ALUNOPERIODO_SITUACAO_CANCELADA             = 6;
    const ALUNOPERIODO_SITUACAO_PRE_MATRICULA         = 7;
    const ALUNOPERIODO_SITUACAO_ADAPTANTE             = 8;
    const ALUNOPERIODO_SITUACAO_DEPENDENTE            = 9;
    const ALUNOPERIODO_SITUACAO_MATRICULA_PROVISORIA  = 10;
    const ALUNOPERIODO_SITUACAO_CONCLUIDO             = 11;
    const ALUNOPERIODO_SITUACAO_MONOGRAFIA            = 12;
    const ALUNOPERIODO_SITUACAO_DESISTENTE            = 13;
    const ALUNOPERIODO_SITUACAO_PENDENCIAS_ACADEMICAS = 14;
    const ALUNOPERIODO_SITUACAO_OBITO                 = 15;

    /** @var \Sistema\Service\SisConfig */
    private $config = null;

    /**
     * @var \GerenciadorArquivos\Service\Arquivo
     */
    protected $diretorioFotosAluno;
    /**
     * @var null
     */
    private $lastError = null;

    /**
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, $arrConfig = array())
    {
        parent::__construct($em, 'Matricula\Entity\AcadperiodoAluno');

        $this->diretorioFotosAluno = $this
            ->getRepository('GerenciadorArquivos\Entity\ArquivoDiretorios')
            ->findOneBy(array('arqDiretorioNome' => 'FotosAlunos'));

        $this->setConfig(new \Sistema\Service\SisConfig($em, $arrConfig));
    }

    /**
     * @return \Sistema\Service\SisConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param \Sistema\Service\SisConfig $config
     * @return AcadgeralAluno
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    public function valida($arrDados)
    {
        $errors = array();

        if (!$arrDados['alunocursoId']) {
            $errors[] = "Por favor, informe a matrícula do aluno no curso";
        }
        if (!$arrDados['turmaId']) {
            $errors[] = "Por favor, informe a turma do aluno";
        }
        if (!$arrDados['situacaoId']) {
            $errors[] = "Por favor, informe a situação do aluno";
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    public function pesquisaForJson($params)
    {
        $sql = "
        SELECT
        TRIM(LEADING '0' FROM alunoCurso.alunocurso_id) alunocursoId,
        TRIM(LEADING '0' FROM alunos.alunoper_id) alunoper_id,
        pessoa.pes_id pesId, pessoa.pes_nome pesNome,
        pessoaFisica.pes_cpf pesCpf,
        date_format(pessoaFisica.pes_data_nascimento, '%d/%m/%Y') pesDataNascimento,
        pessoaFisica.pes_sexo pesSexo,
        campus2.camp_nome campNome,
        curso.curso_nome cursoNome,
        alunos.turma_nome turmaNome,
        alunos.per_nome periodoLetivo,
        alunos.per_id perId,
        alunos.matsit_descricao situacaoPeriodo,
        pessoa.con_contato_telefone conContatoTelefone,
        pessoa.con_contato_celular conContatoCelular,
        pessoa.con_contato_email conContatoEmail,
        endereco.end_cep endCep,
        endereco.end_logradouro endLogradouro,
        endereco.end_numero endNumero,
        endereco.end_bairro endBairro,
        endereco.end_estado endEstado,
        endereco.end_cidade endCidade
        -- select_atividades --
        FROM acadgeral__aluno aluno
		LEFT JOIN acadgeral__aluno_curso alunoCurso USING(aluno_id)
		-- dados do curso
        LEFT JOIN campus_curso campus ON alunoCurso.cursocampus_id = campus.cursocampus_id
        LEFT JOIN acad_curso curso ON campus.curso_id = curso.curso_id
        LEFT JOIN org_campus campus2 ON campus.camp_id = campus2.camp_id
        -- dados do aluno
		LEFT JOIN pessoa pessoa ON pessoa.pes_id=aluno.pes_id
        LEFT JOIN pessoa_fisica pessoaFisica  ON pessoaFisica.pes_id=pessoa.pes_id
        LEFT JOIN endereco ON endereco.pes_id=pessoa.pes_id
        -- dados de aluno em periodos letivos
        LEFT JOIN (
            SELECT
                acadperiodo__aluno.alunocurso_id,
                acadperiodo__aluno.alunoper_id,
                acadperiodo__turma.per_id,
                acadperiodo__turma.turma_nome,
                acadgeral__situacao.matsit_descricao,
                per_nome,
                acadgeral__situacao.situacao_id,
                acadperiodo__aluno.alunoper_sit_data
            FROM acadperiodo__aluno
            INNER JOIN acadperiodo__turma USING(turma_id)
            INNER JOIN acadperiodo__letivo USING(per_id)
            INNER JOIN acadgeral__situacao ON acadgeral__situacao.situacao_id=acadperiodo__aluno.matsituacao_id
        ) alunos ON alunos.alunocurso_id = alunoCurso.alunocurso_id
          -- atividades --
        ";

        if (!$params['perId']) {

            $sql .= " AND alunos.per_id = (SELECT
                MAX(acadperiodo__letivo.per_id) per_id
            FROM
                acadperiodo__aluno ultPerAluno
                    INNER JOIN
                acadperiodo__turma USING (turma_id)
                    INNER JOIN
                acadperiodo__letivo on acadperiodo__letivo.per_id = acadperiodo__turma.per_id
                where ultPerAluno.alunocurso_id = alunoCurso.alunocurso_id and date(now()) between per_data_inicio and per_data_fim
            GROUP BY alunocurso_id)";
            $sql .= " WHERE 1 ";
        } else {
            $parametros['perId'] = $params['perId'];
            $sql .= "AND alunos.per_id = :perId";
        }

        if ($params['somenteAlunosComMatricula']) {
            $sql .= "  AND alunos.alunoper_id IS NOT NULL AND alunoCurso.alunocurso_id IS NOT NULL ";
        }

        if ($params['query']) {
            $sql .= "
            AND (
                pessoa.pes_nome LIKE :pesNome1 OR
                alunoCurso.alunocurso_id LIKE :pesNome2
            )";
            $parametros['pesNome1'] = $params['query'] . "%";
            $parametros['pesNome2'] = "%" . $params['query'] . "%";
        }

        if ($params['pesquisaAtividade'] && $params['alunocursoId']) {
            $atividadeQuery              = " 
                     LEFT JOIN atividadegeral__configuracoes_curso confatividades ON confatividades.cursocampus_id = alunoCurso.cursocampus_id
                     LEFT JOIN atividadegeral__configuracoes conf on confatividades.atividadeconf_portaria = conf.atividadeconf_portaria 
                     LEFT JOIN (
                          SELECT
                          alunonucleo_id,
                          acadperiodo__aluno.alunocurso_id,
                          SUM(coalesce(alunoatividade_horas,0)) totalHoras
                          FROM atividadeperiodo__aluno_atividades
                          INNER JOIN atividadeperiodo__aluno_nucleo using (alunonucleo_id)
                          INNER JOIN acadperiodo__aluno using(alunoper_id)
                          GROUP BY alunocurso_id
                     ) alunoNucleo ON alunoNucleo.alunocurso_id = alunoCurso.alunocurso_id
                     LEFT JOIN (
                        SELECT 
                                date_format( portfolioaluno_data_entrega, '%d/%m/%Y' ) dataPortfolio,
                                alunoperiodo_status status,
                                portfolioaluno_situacao,
                                alunocurso_id                                
                            FROM atividadeperiodo__aluno_portfolio
                            INNER JOIN atividadeperiodo__aluno_nucleo using (alunonucleo_id)
                            INNER JOIN acadperiodo__aluno using(alunoper_id)
                            WHERE alunocurso_id = :alunocurso_id
                            ORDER BY portfolioaluno_situacao ASC,portfolioaluno_data_entrega DESC
                     ) portfolio ON portfolio.alunocurso_id = alunoNucleo.alunocurso_id 
                    ";
            $parametros['alunocurso_id'] = $params['alunocursoId'];

            $stringSelectAtividades = ",
            alunoNucleo.*,
            portfolio.*,
            IF(
                conf.atividadeconf_carga_total_horas <= totalHoras AND
                portfolioaluno_situacao = 'Aprovado',
                'Concluído',
                'Não Concluído'
            ) situacaoDescricao,
            portfolioaluno_situacao situacaoDescricaoPortifolio";

            $sql = str_replace("-- select_atividades --", $stringSelectAtividades, $sql);
            $sql = str_replace("-- atividades --", $atividadeQuery, $sql);

            $sql .= " AND alunoCurso.alunocurso_id = :alunocurso_id ";
        }

        $sql .= ' GROUP BY pessoa.pes_id ORDER BY pesNome';

        if ($params['limite']) {
            $sql .= " limit {$params['limite']}";
        }

        $result = $this->executeQueryWithParam($sql, $parametros)->fetchAll();

        return $result;
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Método retorna informações da prova integrada de um aluno em um período
     * com base em seu vinculo com o periodo: periodo-aluno
     *
     * @param            $perAluno : ID do aluno no periodo
     * @param bool|false $explodeRespAluno : Converte respostas do aluno pra Array
     * @return array|null
     */
    public function buscaProvaIntegradora($perAluno, $explodeRespAluno = false)
    {
        $srvAlunoDisciplina      = new \Matricula\Service\AcadperiodoAlunoDisciplina($this->getEm());
        $repoIntegradoraTurma    = $this->getRepository('Matricula\Entity\AcadperiodoIntegradoraTurma');
        $repoIntegradoraCaderno  = $this->getRepository('Matricula\Entity\AcadperiodoIntegradoraCaderno');
        $repoIntegradoraGabarito = $this->getRepository('Matricula\Entity\AcadperiodoIntegradoraGabarito');

        $disciplinasAlunoPeriodo = $srvAlunoDisciplina->buscaDisciplinasAlunoPeriodo($perAluno);

        $disciplinasAlunoPeriodo = array_filter(
            $disciplinasAlunoPeriodo,
            function ($disc) {
                $turmaDisc      = $disc['alunodisc']->getTurma();
                $turmaPrincipal = $disc['alunodisc']->getAlunoper()->getTurma();

                return ($turmaDisc->getTurmaSerie() == $turmaPrincipal->getTurmaSerie());
            }
        );

        $result = $this->executeQueryWithParam(
            "SELECT * FROM  view__integradora WHERE alunoper_id = :alunoPeriodo",
            array('alunoPeriodo' => $perAluno)
        )->fetchAll();

        if ($result) {
            // recupera todas as disciplinas que tenham vinculo com integradora (dsiciplinas que o aluno teve resposta)
            $arrAlunoIntegradora = array_filter(
                $result,
                function ($r) {
                    return ($r['integradora_id']);
                }
            );

            // verifica se é para transformar as respostas dos alunos em array
            if ($explodeRespAluno) {
                $result = array_map(
                    function ($r) {
                        $r['AcertosRespostas'] = str_split($r['AcertosRespostas']);

                        return $r;
                    },
                    $result
                );
            }

            $situacaoMatriculado = \Matricula\Service\AcadgeralSituacao::MATRICULADO;
            $situacaoDispensado  = \Matricula\Service\AcadgeralSituacao::DISPENSADO;

            $disciplinasIntegradora = array_column($result, 'alunodisc_id');

            foreach ($result as $pos => $r) {
                /* @var $objIntegradoraTurma \Matricula\Entity\AcadperiodoIntegradoraTurma */
                $objIntegradoraTurma = $repoIntegradoraTurma->findOneBy(array('turma' => $r['turma_id']));

                if (!$objIntegradoraTurma) {
                    continue;
                }

                $integAvaliacaoId = $objIntegradoraTurma->getIntegavaliacao()->getIntegavaliacaoId();

                // Recupera Integradora
                /* @var $objIntegradora \Matricula\Entity\AcadperiodoIntegradora */
                $objIntegradora = $objIntegradoraTurma->getIntegavaliacao()->getIntegradora();

                $arrObjCadernosIntegradora = $repoIntegradoraCaderno->findBy(
                    array('integavaliacao' => $integAvaliacaoId)
                );

                foreach ($arrObjCadernosIntegradora as $caderno) {
                    $gabaritoCaderno = $repoIntegradoraGabarito->findBy(
                        array('integcaderno' => $caderno->getIntegcadernoId(), 'disc' => $r['disc_id'])
                    );

                    if ($gabaritoCaderno) {
                        $gabarito         = array();
                        $gabaritoValidate = array();
                        foreach ($gabaritoCaderno as $resp) {
                            $gabarito[]         = $resp->getGabaritoResposta();
                            $gabaritoValidate[] = $resp->getGabaritoValida();
                        }

                        $result[$pos]['gabarito']         = $gabarito;
                        $result[$pos]['gabaritoValidate'] = $gabaritoValidate;

                        break;
                    }
                }
            }

            foreach ($disciplinasAlunoPeriodo as $pos => $discAl) {
                if ($discAl['situacaoId'] == $situacaoMatriculado && $discAl['discTipo'] == "Regular") {
                    $key = array_search($discAl['alunodiscId'], $disciplinasIntegradora);

                    if ($key !== false) {
                        if (is_null($result[$key]['integradora_id'])) {
                            $result[$key]['SituacaoMatricula'] = "Ausente";
                        } else {
                            $result[$key]['SituacaoMatricula'] = "Presente";
                        }

                        $disciplinasAlunoPeriodo[$pos] = $result[$key];
                    }
                } elseif ($discAl['situacaoId'] == $situacaoDispensado) {
                    $disciplinasAlunoPeriodo[$pos] = array(
                        'disc_id'           => $discAl['disc_id'],
                        'Disciplina'        => $discAl['discNome'],
                        'SituacaoMatricula' => $discAl['situacao'],
                        'AcertosDisciplina' => '-',
                        'NDI'               => '-',
                        'NFD'               => '-',
                    );
                } else {
                    unset($disciplinasAlunoPeriodo[$pos]);
                }
            }

            return array(
                'disciplinas'        => $disciplinasAlunoPeriodo,
                'integradora'        => ($objIntegradora ? $objIntegradora->getIntegradoraId() : ''),
                'integradoraFechada' => ($objIntegradora ? $objIntegradora->getIntegradoraStatus() : ''),
                'periodoIntegradora' => $r['periodo_letivo']
            );
        }

        return array();
    }

    public function getArrSelect2PeriodoMatriculadoAluno($alunoPerId)
    {
        if (!$alunoPerId) {
            return false;
        }

        /** @var \Matricula\Entity\AcadperiodoAluno $objAlunoPer */
        $objAlunoPer = $this->getRepository()->find($alunoPerId);

        if ($objAlunoPer) {
            $objPeriodo = $objAlunoPer->getTurma()->getPer();

            return
                array(
                    'id'   => $objPeriodo->getPerId(),
                    'text' => $objPeriodo->getPerNome()
                );
        }

        return false;
    }

    public function getArrSelect2TurmaMatriculadoAluno($alunoPerId)
    {
        if (!$alunoPerId) {
            return false;
        }

        /** @var \Matricula\Entity\AcadperiodoAluno $objAlunoPer */
        $objAlunoPer = $this->getRepository()->find($alunoPerId);

        if ($objAlunoPer) {
            $objTurma = $objAlunoPer->getTurma();

            return
                array(
                    'id'     => $objTurma->getTurmaId(),
                    'text'   => $objTurma->getTurmaNome(),
                    'matCur' => $objTurma->getMatCur()->getMatCurId(),
                    'serie'  => $objTurma->getTurmaSerie()
                );
        }

        return false;
    }

    public function getArrSelect2CursoAlunoMatriculado($alunoPerId)
    {
        if (!$alunoPerId) {
            return false;
        }

        /** @var \Matricula\Entity\AcadperiodoAluno $objAlunoPer */
        $objAlunoPer = $this->getRepository()->find($alunoPerId);

        if ($objAlunoPer) {
            $objCurso = $objAlunoPer->getAlunocurso()->getCursocampus()->getCurso();

            return
                array(
                    'id'   => $objCurso->getCursoId(),
                    'text' => $objCurso->getCursoNome()
                );
        }

        return false;
    }

    /**
     * Busca informações da matrícula do aluno
     * @param int $id
     * @return array
     * @throws \Doctrine\ORM\ORMException
     */
    public function buscaAlunoPerido($id)
    {
        $entity = $this->getEm()->getReference('Matricula\Entity\AcadperiodoAluno', $id);

        if ($entity) {
            $dadosAluno              = $entity->toArray();
            $dadosAluno['matricula'] = $dadosAluno['alunocurso'];

            return $dadosAluno;
        }
    }

    /**
     * Adicona uma pessoa como aluno no periodo atual em um curso com todas suas dependencias como:
     * pessoa, pessoa_fisica, acadgeral__aluno, acadgeral__alunocurso, acadperiodo__aluno, responsaveis e endereco.
     * @param array $dados
     * @return \Matricula\Entity\AcadperiodoAluno
     * @throws \Exception
     */
    public function adicionar(array $dados)
    {
        $this->begin();

        $serviceSisIntegracao = new \Sistema\Service\SisIntegracao($this->getEm());
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceAluno         = new \Matricula\Service\AcadgeralAluno($this->getEm(), $dados);
        $servicePessoaFisica  = new \Pessoa\Service\PessoaFisica($this->getEm());
        $serviceAlunoCurso    = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());

        if ($dados['alunoId']) {
            /** @var  $objAlunoCurso \Matricula\Entity\AcadgeralAlunoCurso */
            $objAlunoCurso = $serviceAlunoCurso->getRepository()->findOneBy(
                [
                    'aluno'       => $dados['alunoId'],
                    'cursocampus' => $dados['cursocampus'] ? $dados['cursocampus'] : $dados['cursoCampus']
                ]
            );

            if ($objAlunoCurso) {
                $this->setLastError('Esta pessoa já esta matriculada neste curso');

                return false;
            }
        }

        /** @var \Acesso\Entity\AcessoPessoas $usuarioLog */
        $usuarioLog = $serviceAcessoPessoas->retornaUsuarioLogado();

        if ($dados['pes'] != "") {
            $pessoaFisica = (new \Pessoa\Service\PessoaFisica($this->getEm()))->edita($dados);
        } else {
            /** @var  $ObjpessoaFisica \Pessoa\Entity\PessoaFisica */
            $ObjpessoaFisica = $servicePessoaFisica->getRepository()->findOneBy(array('pesCpf' => $dados['pesCpf']));

            if ($ObjpessoaFisica) {
                $dados['pes'] = $ObjpessoaFisica->getPes()->getPesId();
                $pessoaFisica = (new \Pessoa\Service\PessoaFisica($this->getEm()))->edita($dados);
            }

            $pessoaFisica = (new \Pessoa\Service\PessoaFisica($this->getEm()))->adicionar($dados);
            $dados['pes'] = $pessoaFisica->getPes()->getPesId();
        }
        if (!empty($dados['alunoFoto']['name'])) {
            $dadosArquivo = array(
                'name'     => $dados['alunoFoto']['name'],
                'type'     => $dados['alunoFoto']['type'],
                'tmp_name' => $dados['alunoFoto']['tmp_name'],
                'error'    => $dados['alunoFoto']['error'],
                'size'     => $dados['alunoFoto']['size'],
            );

            $arquivo = (new \GerenciadorArquivos\Service\Arquivo($this->getEm(), $this->diretorioFotosAluno))
                ->adicionar($dadosArquivo);

            if ($arquivo) {
                $dados['arq'] = $arquivo->getArqId();
            }
        } else {
            $dados['arq'] = null;
        }

        $contatos               = (new \Pessoa\Service\Contato($this->getEm()));
        $contatoAluno['pessoa'] = $dados['pes'];
        $situacaoRepository     = $this->getRepository('Matricula\Entity\AcadgeralSituacao');

        if ($dados['idEmail'] == "") {
            $contatoAluno['tipoContato'] = ['E-mail'];
            $contatoAluno['conContato']  = [$dados['alunoEmail']];
            $contatos->adicionar($contatoAluno);
        } else {
            $contatoAluno['conContato']  = $dados['alunoEmail'];
            $contatoAluno['conId']       = $dados['idEmail'];
            $contatoAluno['tipoContato'] = $dados['idTipoEmail'];
            $contatos->edita($contatoAluno);
        }
        if ($dados['idTelefone'] == "") {
            $contatoAluno['conContato']  = [$dados['alunoTelefone']];
            $contatoAluno['tipoContato'] = ['Telefone'];
            $contatos->adicionar($contatoAluno);
        } else {
            $contatoAluno['conContato']  = $dados['alunoTelefone'];
            $contatoAluno['conId']       = $dados['idTelefone'];
            $contatoAluno['tipoContato'] = $dados['idTipoTelefone'];
            $contatos->edita($contatoAluno);
        }
        if ($dados['idCelular'] == "") {
            $contatoAluno['conContato']  = [$dados['alunoCelular']];
            $contatoAluno['tipoContato'] = ['Celular'];
            $contatos->adicionar($contatoAluno);
        } else {
            $contatoAluno['conContato']  = $dados['alunoCelular'];
            $contatoAluno['conId']       = $dados['idCelular'];
            $contatoAluno['tipoContato'] = $dados['idTipoCelular'];
            $contatos->edita($contatoAluno);
        }

        $dados['alunoDataCadastro']      = (new \DateTime());
        $dados['alunocursoDataCadastro'] = $dados['alunoDataCadastro'];
        $dados['tiposelId']              = $dados['tiposelId'] ? $dados['tiposelId'] : $dados['tiposel'];
        $dados['tiposelId']              = $dados['tiposelId'] ? $dados['tiposelId'] : $dados['alunoFormaIngresso'];
        $dados['motivo']                 = null;

        $dados['pesIdAgente'] = $dados['pesIdAgente'] ? $dados['pesIdAgente'] : null;

        $Objaluno = $serviceAluno->getRepository('Matricula\Entity\AcadgeralAluno')->findOneBy(
            ['pes' => $dados['pes']]
        );

        if ($Objaluno == null) {
            $Objaluno = (new \Matricula\Service\AcadgeralAluno($this->getEm(), $dados))->adicionar($dados);
        }

        for ($i = 0; $i < sizeof($dados['documentos']); $i++) {
            $registro = (new \Documentos\Service\DocumentoPessoa($this->getEm()))->adicionar(
                $this->formataDocumentoAluno($dados['documentos'][$i], $dados['pes'], $dados['documentoGrupo'][$i])
            );
        }

        for ($i = 0; $i < sizeof($dados['responsavelNivel']); $i++) {
            $dadosResponsavel = $this->formataDadosReponsavel($dados, $i);
            if ($dadosResponsavel['pesCnpj']) {
                $dadosResponsavel['pesDataInicio'] = (new \DateTime())->format('d-m-Y');
            }
            $dadosResponsavel['aluno'] = $Objaluno->getAlunoId();
            $responsavel               = (new \Matricula\Service\AcadgeralResponsavel($this->getEm()))->adicionar(
                $dadosResponsavel
            );

            $contatoResp['pessoa'] = $responsavel->getPes()->getPesId();
            if ($dadosResponsavel['Email'] != "") {
                $contatoResp['tipoContato'] = ['E-mail'];
                $contatoResp['conContato']  = [$dadosResponsavel['Email']];
                $contatos->adicionar($contatoResp);
            }
            if ($dadosResponsavel['Telefone'] != "") {
                $contatoResp['conContato']  = [$dadosResponsavel['Telefone']];
                $contatoResp['tipoContato'] = ['Telefone'];
                $contatos->adicionar($contatoResp);
            }
            if ($dadosResponsavel['Celular'] != "") {
                $contatoResp['conContato']  = [$dadosResponsavel['Celular']];
                $contatoResp['tipoContato'] = ['Celular'];
                $contatos->adicionar($contatoResp);
            }
        }

        $dados['aluno'] = $Objaluno->getAlunoId();

        $alunoCurso = $serviceAluno->getRepository('Matricula\Entity\AcadgeralAlunoCurso')->findOneBy(
            ['aluno' => $Objaluno->getAlunoId(), 'cursocampus' => $dados['cursoCampus']]
        );

        if ($alunoCurso !== null) {
            return 'Atenção: Este aluno já se encontra cadastrado. Sua matricula é: ' . $alunoCurso->getAlunocursoId(
            ) . ' Favor concluir a ação pela interface de rematricula!';
        }

        $servicoAlunoCurso = new \Matricula\Service\AcadgeralAlunoCurso(
            $this->getEm(),
            $dados
        );

        $alunoCurso = $servicoAlunoCurso->adicionar($dados);

        if (is_bool($alunoCurso)) {
            return false;
        }

        $dadosAlunoPeriodo['alunocurso']      = $alunoCurso->getAlunocursoId();
        $dadosAlunoPeriodo['matsituacao']     = $situacaoRepository->buscaSituacao('Matricula Provisoria');
        $dadosAlunoPeriodo['turma']           = $dados['alunoTurmaPrincipal'];
        $dadosAlunoPeriodo['alunoperSitData'] = (new \DateTime());
        $dadosAlunoPeriodo['usuarioCriador']  = $usuarioLog->getId();

        /** @var $alunoPeriodo \Matricula\Entity\AcadperiodoAluno */
        $alunoPeriodo = parent::adicionar($dadosAlunoPeriodo);

        $situacaoDependente  = $situacaoRepository->buscaSituacao('Dependente');
        $situacaoAdaptante   = $situacaoRepository->buscaSituacao('Adaptante');
        $situacaoMonografia  = $situacaoRepository->buscaSituacao('Monografia');
        $situacaoMatriculado = $situacaoRepository->buscaSituacao('Matriculado');

        $dependencias = 0;
        $adaptacoes   = 0;
        $monografia   = 0;
        $matriculado  = 0;

        $servicePeriodoAlunoDisciplina = new \Matricula\Service\AcadperiodoAlunoDisciplina($this->getEm());

        for ($i = 0; $i < sizeof($dados['disciplina']); $i++) {
            $disciplina['disc']             = $dados['disciplina'][$i];
            $disciplina['turma']            = $dados['disciplinaTurma'][$i];
            $disciplina['alunoper']         = $alunoPeriodo->getAlunoperId();
            $disciplina['alunodiscSitData'] = (new \DateTime());
            $disciplina['situacao']         = $situacaoRepository->buscaSituacao($dados['disciplinaSituacao'][$i]);
            $disciplina['usuarioCriador']   = $usuarioLog->getId();

            if ($disciplina['situacao'] == $situacaoMatriculado) {
                $matriculado++;
            } elseif ($disciplina['situacao'] == $situacaoAdaptante) {
                $adaptacoes++;
            } elseif ($disciplina['situacao'] == $situacaoDependente) {
                $dependencias++;
            } elseif ($disciplina['situacao'] == $situacaoMonografia) {
                $monografia++;
            }

            $alunoDisciplina = $servicePeriodoAlunoDisciplina->adicionar($disciplina);
        }

        // gera as mensalidades se o aluno estiver se matriculando em alguma disciplina regular
        if ($matriculado) {
            $titulos = $this->geraMensalidadesAluno($dados['pes'], $alunoPeriodo->getAlunoperId());
        }

        $this->geraTitulos(
            array(
                'alunoperId'   => $alunoPeriodo->getAlunoperId(),
                'alunocursoId' => $alunoPeriodo->getAlunocurso()->getAlunocursoId(),
                'pes'          => $dados['pes']
            ),
            array(
                'matriculado'  => $matriculado,
                'dependencias' => $dependencias,
                'adaptacoes'   => $adaptacoes,
                'monografia'   => $monografia
            )
        );

        $info['alunoPeriodo'] = $alunoPeriodo;
        $info['taxas']        = false;

        if (($dependencias > 0) OR ($adaptacoes > 0) OR ($monografia > 0)) {
            $info['taxas'] = true;
        }

        $this->atualizaInformacoesLeitorBiblioteca($alunoPeriodo);

        $this->commit();

        $serviceSisIntegracao->integracaoInteligenteParaAluno($Objaluno);

        return $info;
    }

    /**
     * Formata documentos de aluno
     * @param     $documento
     * @param int $pesId
     * @param     $grupo
     * @return mixed
     */
    public function formataDocumentoAluno($documento, $pesId, $grupo)
    {
        $doc['pes']              = $pesId;
        $doc['docpessoaStatus']  = $documento == "Não Aplicavel" ? "Dispensavel" : $documento;
        $doc['docpessoaEntrega'] = $documento['docpessoaStatus'] != 'Dispensavel' ? (new \DateTime()) : "";
        $doc['docgrupo']         = $grupo;

        return $doc;
    }

    /**
     * Formata e valida dados de responsável para salvar no banco
     * @param array $dados
     * @param       $i
     * @return array
     */
    public function formataDadosReponsavel(array $dados, $i)
    {
        $dadosResponsavel = array();

        $dadosResponsavel['respId']           = $dados['responsavelId'][$i];
        $dadosResponsavel['aluno']            = $dados['responsavelAluno'][$i];
        $dadosResponsavel['pes']              = $dados['responsavelPes'][$i];
        $dadosResponsavel['respNivel']        = $dados['responsavelNivel'][$i];
        $dadosResponsavel['pesNacionalidade'] = $dados['responsavelNacionalidade'][$i];

        if (v::cpf()->validate($dados['responsavelCnpjCpf'][$i])) {
            $dadosResponsavel['pesCpf'] = $dados['responsavelCnpjCpf'][$i];
        }
        if (v::cnpj()->validate($dados['responsavelCnpjCpf'][$i])) {
            $dadosResponsavel['pesCnpj'] = $dados['responsavelCnpjCpf'][$i];
        }

        $dadosResponsavel['pesNome']           = $dados['responsavelNome'][$i];
        $dadosResponsavel['pesDocEstrangeiro'] = $dados['responsavelDocEstrangeiro'][$i];
        $dadosResponsavel['respVinculo']       = $dados['responsavelVinculo'][$i];
        $dadosResponsavel['pesRg']             = $dados['responsavelRg'][$i];
        $dadosResponsavel['pesRgEmissao']      = ($dados['responsavelRgEmissao'][$i] != 'null' && $dados['responsavelRgEmissao'][$i] != '')
            ? $dados['responsavelRgEmissao'][$i] : null;
        $dadosResponsavel['endCep'][]          = $dados['responsavelCep'][$i];
        $dadosResponsavel['tipoEndereco'][]    = $dados['responsavelTipoEndereco'][$i];
        $dadosResponsavel['endLogradouro'][]   = $dados['responsavelLogradouro'][$i];
        $dadosResponsavel['endCidade'][]       = $dados['responsavelCidade'][$i];
        $dadosResponsavel['endEstado'][]       = $dados['responsavelEstado'][$i];
        $dadosResponsavel['endBairro'][]       = $dados['responsavelBairro'][$i];
        $dadosResponsavel['endNumero'][]       = $dados['responsavelNumero'][$i];
        $dadosResponsavel['endComplemento'][]  = $dados['responsavelComplemento'][$i];
        $dadosResponsavel['Telefone']          = $dados['responsavelTelefone'][$i];
        $dadosResponsavel['TelefoneId']        = $dados['responsavelTelefoneId'][$i];
        $dadosResponsavel['idTipoTelefone']    = $dados['responsavelTipoTelefone'][$i];
        $dadosResponsavel['Celular']           = $dados['responsavelCelular'][$i];
        $dadosResponsavel['CelularId']         = $dados['responsavelCelularId'][$i];
        $dadosResponsavel['idTipoCelular']     = $dados['responsavelTipoCelular'][$i];
        $dadosResponsavel['Email']             = $dados['responsavelEmail'][$i];
        $dadosResponsavel['EmailId']           = $dados['responsavelEmailId'][$i];
        $dadosResponsavel['idTipoEmail']       = $dados['responsavelTipoEmail'][$i];
        $dadosResponsavel['pes']               = '';

        return $dadosResponsavel;
    }

    /**
     * Gera mensalidades de um aluno no período
     * @param int    $pesId
     * @param int    $alunoPeriodoId
     * @param string $tipoTitulo
     * @param int    $countTitulos
     * @param int    $mesInicio
     * @return array|bool
     * @throws \Exception
     */
    public function geraMensalidadesAluno(
        $pesId,
        $alunoPeriodoId,
        $tipoTitulo = 'Mensalidade',
        $countTitulos = 6,
        $mesInicio = 0
    ) {
        if (!$this->getConfig()->localizarChave('MATRICULA_GERAR_TITULO_MENSALIDADE')) {
            return true;
        }

        $serviceTitulo = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        /** @var \Matricula\Entity\AcadperiodoAluno $objAlunoPer */
        $objAlunoPer   = $this->getRepository()->find($alunoPeriodoId);
        $periodoLetivo = $objAlunoPer->getTurma()->getPer();

        if (is_array($tipoTitulo)) {
            $confTipoTitulo = $tipoTitulo;
        } else {
            $objCursoCampus = $objAlunoPer->getTurma()->getCursocampus();

            $confTipoTitulo = $this->buscaConfTituloMatricula(
                $periodoLetivo->getPerId(),
                $tipoTitulo,
                $objCursoCampus->getCursocampusId()
            );
        }

        if (!$periodoLetivo) {
            $periodoLetivo = (new \Matricula\Service\AcadperiodoLetivo($this->getEm()))->buscaUltimoPeriodo();
        }

        $tituloDataVencimento = $periodoLetivo->getPerDataInicio();
        $countTitulos         = $confTipoTitulo['parcelas'] ? $confTipoTitulo['parcelas'] : $countTitulos;

        $tituloDataVencimento->setDate($confTipoTitulo['ano'], $confTipoTitulo['mes'], $confTipoTitulo['diaVenc']);

        $param = array(
            'pes'                  => $pesId,
            'alunocurso'           => ($objAlunoPer ? $objAlunoPer->getAlunocurso()->getAlunocursoId() : null),
            'alunoper'             => ($objAlunoPer ? $objAlunoPer : null),
            'tipotitulo'           => $confTipoTitulo['tipotitulo_id'],
            'tituloconf'           => $confTipoTitulo['tituloconfId'],
            'tituloDataVencimento' => $tituloDataVencimento->format('Y-m-d'),
            'tituloNumeroParcelas' => $countTitulos,
            'tituloValorParcelas'  => $confTipoTitulo['preco'],
            'alunoper_id'          => $objAlunoPer->getAlunoperId()
        );

        if ($serviceTitulo->criarNovosTitulos($param, true, false) === false) {
            $this->setLastError("Não foi possível criar os títulos!" . $serviceTitulo->getLastError());

            return false;
        }

        return true;
    }

    /**
     * Retorna configuração de tipo de título no periodo
     * @param int $periodoLetivo : período letivo para pegar as configuracoes de título
     * @param int $tipoTitulo : tipo de título para buscar a configuracao
     * @param int $cursoCampusId : campus curso ao qual pegar as configuracoes de titulo
     * @return array|null
     * @throws \Exception
     */
    public function buscaConfTituloMatricula($periodoLetivo, $tipoTitulo, $cursoCampusId)
    {
        $serviceAcadperiodoLetivo    = new \Matricula\Service\AcadperiodoLetivo($this->getEm());
        $serviceFinanceiroTituloTipo = new \Financeiro\Service\FinanceiroTituloTipo($this->getEm());
        $serviceConfigTitulo         = new \Financeiro\Service\FinanceiroTituloConfig($this->getEm());
        $serviceValores              = new \Financeiro\Service\FinanceiroValores($this->getEm());

        /** @var \Matricula\Entity\AcadperiodoLetivo $objAcadperiodoLetivo */
        $objAcadperiodoLetivo = $serviceAcadperiodoLetivo->getRepository()->find($periodoLetivo);

        /** @var \Financeiro\Entity\FinanceiroTituloTipo $objTipoTitulo */
        if (is_int($tipoTitulo)) {
            $objTipoTitulo = $serviceFinanceiroTituloTipo->getRepository()->find($tipoTitulo);
        } else {
            $objTipoTitulo = $serviceFinanceiroTituloTipo->getRepository()->findOneBy(
                ['tipotituloNome' => $tipoTitulo]
            );
        }

        if (!$objTipoTitulo) {
            throw new \Exception('Tipo de título "' . $tipoTitulo . '" não existe!');
        }

        $arrParamPesquisaConfig = array(
            'tipotitulo'    => $objTipoTitulo->getTipotituloId(),
            'tipotituloId'  => $objTipoTitulo->getTipotituloId(),
            'cursocampus'   => $cursoCampusId,
            'cursocampusId' => $cursoCampusId,
            'cursoCampus'   => $cursoCampusId,
            'cursoCampusId' => $cursoCampusId,
        );
        /** @var \Financeiro\Entity\FinanceiroTituloConfig $objFinanceiroTituloConfig */
        $objFinanceiroTituloConfig = $serviceConfigTitulo->pesquisarConfiguracao($arrParamPesquisaConfig);

        if (!$objFinanceiroTituloConfig) {
            throw new \Exception('Sem configurações cadastrados para tipo de título!');
        }

        $arrArrFinanceiroValores = $serviceValores->buscaValoresAtivosParaTipoTitulo(
            $objTipoTitulo->getTipotituloId(),
            $arrParamPesquisaConfig
        );

        if (!$arrArrFinanceiroValores) {
            throw new \Exception('Sem valores cadastrados para tipo de título!');
        }

        $arrFinanceiroValores  = array_shift($arrArrFinanceiroValores);
        $dataVencimentoInicial = new \DateTime();
        $diaVenc               = $objFinanceiroTituloConfig->getTituloconfDiaVenc();

        if ($objAcadperiodoLetivo) {
            $dataVencimentoInicial = $objAcadperiodoLetivo->getPerDataInicio();

            if ($objAcadperiodoLetivo->getPerDataVencimentoInicial()) {
                $dataVencimentoInicial = $objAcadperiodoLetivo->getPerDataVencimentoInicial();
                $diaVenc               = $objAcadperiodoLetivo->getPerDataVencimentoInicial()->format('d');
            }
        }

        return array(
            'valores_id'      => $arrFinanceiroValores['valoresId'],
            'preco'           => $arrFinanceiroValores['valoresPreco'],
            'parcelas'        => $arrFinanceiroValores['valoresParcela'],
            'per_id'          => $objAcadperiodoLetivo ? $objAcadperiodoLetivo->getPerId() : '',
            'cursocampus_id'  => $cursoCampusId,
            'diaVenc'         => $diaVenc,
            'confId'          => $objFinanceiroTituloConfig->getTituloconfId(),
            'tituloconfId'    => $objFinanceiroTituloConfig->getTituloconfId(),
            'tipotitulo_id'   => $objTipoTitulo->getTipotituloId(),
            'tipotitulo_nome' => $objTipoTitulo->getTipotituloNome(),
            'ano'             => $dataVencimentoInicial->format('Y'),
            'mes'             => $dataVencimentoInicial->format('m'),
        );
    }

    /**
     * Metodo responsável por gerar os titulos com base nos tipos de disciplinas
     * que o aluno foi matriculado/rematriculado.
     * @param array $alunoInfo : informações do aluno
     * @param array $count : contagem dos tipos de disciplinas que o aluno esta sendo matriculado
     */
    private function geraTitulos(array $alunoInfo, array $count)
    {
        /*Todo: Não é mais utilizado, particularidade de uma insância */

        return true;

        /** @var \Matricula\Entity\AcadperiodoAluno $objAlunoPer */
        $objAlunoPer = $this->getEm()->getRepository('Matricula\Entity\AcadperiodoAluno')->find(
            $alunoInfo['alunoperId']
        );

        //$cursoCampusId = $objAlunoPer->getTurma()->getCursocampus()->getCursocampusId();
        $cursoCampusId = $objAlunoPer->getAlunocurso()->getCursocampus()->getCursocampusId();

        $peridoLetivoAtual = (new \Matricula\Service\AcadperiodoLetivo($this->getEm()))->buscaUltimoPeriodo();

        $desativarGeracaoOutrosTitulos = true;

        if (!$desativarGeracaoOutrosTitulos) {
            $configDependencia = $this->buscaConfTituloMatricula(
                $peridoLetivoAtual->getPerId(),
                'Dependência',
                $cursoCampusId
            );
            $configAdaptacao   = $this->buscaConfTituloMatricula(
                $peridoLetivoAtual->getPerId(),
                'Adaptação',
                $cursoCampusId
            );

            $configMonografia = $this->buscaConfTituloMatricula(
                $peridoLetivoAtual->getPerId(),
                'Monografia',
                $cursoCampusId
            );
        }

        if (!$count['matriculado'] && ($count['dependencias'] || $count['adaptacoes'] || $count['monografia'])) {
            // gerando mensalidade de matricula
            $this->geraMensalidadesAluno($alunoInfo['pes'], $alunoInfo['alunoperId'], 'Mensalidade', 1);
        }

        // ATENÇÃO; Remover o retorno abaixo para gerar titulos de depêndencia, monografia e adaptação.
        return false;

        $serviceFinanceiroTitulo = new \Boleto\Service\FinanceiroTitulo($this->getEm());

        if (!$desativarGeracaoOutrosTitulos) {
            if ($count['dependencias']) {
                $configDependencia['valor'] *= $count['dependencias'];

                $titulosDependenciaAberto = $serviceFinanceiroTitulo->buscaTaxasAtivasAluno(
                    $alunoInfo['alunocursoId'],
                    array('tipoTitulo' => " = 'Dependência'", 'estado' => " = 'Aberto'"),
                    false
                );
                $titulosDependenciaPago   = $serviceFinanceiroTitulo->buscaTaxasAtivasAluno(
                    $alunoInfo['alunocursoId'],
                    array('tipoTitulo' => " = 'Dependência'", 'estado' => " != 'Aberto'"),
                    false
                );

                $titulosDependenciaAberto = $titulosDependenciaAberto['taxas'];
                $titulosDependenciaPago   = $titulosDependenciaPago['taxas'];

                // atualiza valor dos titulos que ainda estao em aberto
                $mesTitulosAbertos = array();
                if ($titulosDependenciaAberto) {
                    foreach ($titulosDependenciaAberto as $tituloDependencia) {
                        $data = array(
                            'tituloId'                => $tituloDependencia['tituloId'],
                            'pes'                     => $alunoInfo['pes'],
                            'tipotitulo'              => $configDependencia['tipotitulo_id'],
                            'usuarioBaixa'            => null,
                            'usuarioAutor'            => $tituloDependencia['usuario'],
                            'tituloValor'             => $configDependencia['valor'] + $tituloDependencia['tituloValor'],
                            'tituloDataVencimento'    => $tituloDependencia['tituloDataVencimento'],
                            'tituloDataProcessamento' => 'now',
                            'tituloEstado'            => 'Aberto',
                        );

                        $serviceFinanceiroTitulo->edita($data);

                        $mesTitulosAbertos[] = explode('/', $tituloDependencia['tituloDataVencimento'])[1];
                    }
                }
                // gera titulos pros valores ja pagos com a mesma data
                if ($titulosDependenciaPago) {
                    foreach ($titulosDependenciaPago as $tituloDependencia) {
                        $mesVenc = explode('/', $tituloDependencia['tituloDataVencimento'])[1];
                        if (!in_array($mesVenc, $mesTitulosAbertos)) {
                            $configDependencia['mes'] = $mesVenc;
                            $this->geraMensalidadesAluno(
                                $alunoInfo['pes'],
                                $alunoInfo['alunoperId'],
                                $configDependencia,
                                1
                            );

                            $mesTitulosAbertos[] = $mesVenc;
                        }
                    }
                }

                if (!$titulosDependenciaPago && !$titulosDependenciaAberto) {
                    $this->geraMensalidadesAluno($alunoInfo['pes'], $alunoInfo['alunoperId'], $configDependencia, 5);
                }
            }

            if ($count['adaptacoes']) {
                $configAdaptacao['valor'] *= $count['adaptacoes'];

                $titulosAdaptacaoAberto = $serviceFinanceiroTitulo->buscaTaxasAtivasAluno(
                    $alunoInfo['alunocursoId'],
                    array('tipoTitulo' => " = 'Adaptação'", 'estado' => " = 'Aberto'"),
                    false
                );

                $titulosAdaptacaoPago = $serviceFinanceiroTitulo->buscaTaxasAtivasAluno(
                    $alunoInfo['alunocursoId'],
                    array('tipoTitulo' => " = 'Adaptação'", 'estado' => " != 'Aberto'"),
                    false
                );

                $titulosAdaptacaoAberto = $titulosAdaptacaoAberto['taxas'];
                $titulosAdaptacaoPago   = $titulosAdaptacaoPago['taxas'];

                // atualiza valor dos titulos que ainda estao em aberto
                $mesTitulosAbertos = array();
                if ($titulosAdaptacaoAberto) {
                    foreach ($titulosAdaptacaoAberto as $tituloAdaptacao) {
                        $data = array(
                            'tituloId'                => $tituloAdaptacao['tituloId'],
                            'pes'                     => $alunoInfo['pes'],
                            'tipotitulo'              => $configAdaptacao['tipotitulo_id'],
                            'usuarioBaixa'            => null,
                            'usuarioAutor'            => $tituloAdaptacao['usuario'],
                            'tituloValor'             => $configAdaptacao['valor'] + $tituloAdaptacao['tituloValor'],
                            'tituloDataVencimento'    => $tituloAdaptacao['tituloDataVencimento'],
                            'tituloEstado'            => 'Aberto',
                            'tituloDataProcessamento' => 'now',
                        );

                        $serviceFinanceiroTitulo->edita($data);

                        $mesTitulosAbertos[] = explode('/', $tituloAdaptacao['tituloDataVencimento'])[1];
                    }
                }
                // gera titulos pros valores ja pagos com a mesma data
                if ($titulosAdaptacaoPago) {
                    foreach ($titulosAdaptacaoPago as $tituloAdaptacao) {
                        $mesVenc = explode('/', $tituloAdaptacao['tituloDataVencimento'])[1];
                        if (!in_array($mesVenc, $mesTitulosAbertos)) {
                            $configDependencia['mes'] = $mesVenc;
                            $this->geraMensalidadesAluno(
                                $alunoInfo['pes'],
                                $alunoInfo['alunoperId'],
                                $configAdaptacao,
                                1
                            );
                        }
                    }
                }

                if (!$titulosAdaptacaoPago && !$titulosAdaptacaoAberto) {
                    $this->geraMensalidadesAluno($alunoInfo['pes'], $alunoInfo['alunoperId'], $configAdaptacao, 5);
                }
            }

            if ($count['monografia']) {
                for ($i = 0; $i < $count['monografia']; $i++) {
                    $this->geraMensalidadesAluno($alunoInfo['pes'], $alunoInfo['alunoperId'], 'Monografia', 1);
                }
            }
        }
    }

    /**
     * Atualiza informações de aluno na tabela de pessoas da biblioteca
     * @param \Matricula\Entity\AcadperiodoAluno|null    $objAcadperiodoAluno
     * @param \Matricula\Entity\AcadgeralAlunoCurso|null $objAlunoCurso
     */
    public function atualizaInformacoesLeitorBiblioteca(
        \Matricula\Entity\AcadperiodoAluno $objAcadperiodoAluno = null,
        \Matricula\Entity\AcadgeralAlunoCurso $objAlunoCurso = null
    ) {
        $serviceAlunoCurso = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
        $servicePessoa     = new \Biblioteca\Service\Pessoa($this->getEm());

        $situacoesRemoverGrupo = array(
            \Matricula\Service\AcadgeralSituacao::TRANCADO,
            \Matricula\Service\AcadgeralSituacao::DISPENSADO,
            \Matricula\Service\AcadgeralSituacao::TRANSFERENCIA,
            \Matricula\Service\AcadgeralSituacao::CANCELADA,
            \Matricula\Service\AcadgeralSituacao::CONCLUIDO,
            \Matricula\Service\AcadgeralSituacao::MATRICULA_PROVISORIA,
            \Matricula\Service\AcadgeralSituacao::OBITO,
        );

        $removerAcesso = false;

        if ($objAcadperiodoAluno) {
            $objAlunoCurso = $objAcadperiodoAluno->getAlunocurso();

            if (array_search($objAcadperiodoAluno->getMatsituacao()->getSituacaoId(), $situacoesRemoverGrupo)) {
                $removerAcesso = true;
            }
        } else {
            $alunoCancelado = (
                $objAlunoCurso->getAlunocursoSituacao() == $serviceAlunoCurso::ALUNOCURSO_SITUACAO_CANCELADO
            );
            $alunoTrancado  = (
                $objAlunoCurso->getAlunocursoSituacao() == $serviceAlunoCurso::ALUNOCURSO_SITUACAO_TRANCADO
            );

            if ($alunoCancelado || $alunoTrancado) {
                $removerAcesso = true;
            }
        }

        if ($removerAcesso) {
            $servicePessoa->removeGrupoLeitor($objAlunoCurso);
        } else {
            $servicePessoa->cadastraLeitorAluno($objAlunoCurso);
        }
    }

    /**
     * Edita os dados de um aluno no periodo atual em um curso com todas suas dependencias como:
     * pessoa, pessoa_fisica, acadgeral__aluno, acadgeral__alunocurso, acadperiodo__aluno, responsaveis e endereco.
     * @param array $dados
     * @return Object de Matricula\Entity\AcadperiodoAluno
     * @throws \Exception
     */
    public function edita(array $dados)
    {
        $serviceSisIntegracao             = new \Sistema\Service\SisIntegracao($this->getEm());
        $serviceAcessoPessoas             = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceAcadperiodoAlunoHistorico = new \Matricula\Service\AcadperiodoAlunoHistorico($this->getEm());
        $serviceAcadperiodoAluno          = new \Matricula\Service\AcadperiodoAluno($this->getEm());
        $serviceSituacao                  = new \Matricula\Service\AcadgeralSituacao($this->getEm());
        $serviceAluno                     = new \Matricula\Service\AcadgeralAluno($this->getEm());
        $serviceAlunoCurso                = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
        $serviceTurma                     = new \Matricula\Service\AcadperiodoTurma($this->getEm());
        $serviceAcadgeralResponsavel      = new \Matricula\Service\AcadgeralResponsavel($this->getEm());

        // usuario logado, para fins de log de alterações
        $usuarioLog = $serviceAcessoPessoas->retornaUsuarioLogado();

        $usuarioLog->getUsuario()->getUsrStatus();
        $dados['usuarioCriador'] = $usuarioLog->getId();

        $this->begin();

        if ($dados['pes']) {
            $dados['pesId'] = $dados['pes'];

            /*if(!$servicePessoa->salvarPessoaComValidacao($dados)){
                $this->setLastError("Falha ao salvar registro de pessoa!");
            }*/

            (new \Pessoa\Service\Pessoa($this->getEm()))->edita($dados);
        }
        // edita pessoa fisica
        (new \Pessoa\Service\PessoaFisica($this->getEm()))->edita($dados);

        // edita endereco do aluno
        (new \Pessoa\Service\Endereco($this->getEm()))->editaMultiplos($dados);

        $situacaoRepository = $this->getRepository('Matricula\Entity\AcadgeralSituacao');
        $contatos           = new \Pessoa\Service\Contato($this->getEm());

        if (!empty($dados['alunoFoto']['name'])) {
            $dadosArquivo = array(
                'name'     => $dados['alunoFoto']['name'],
                'type'     => $dados['alunoFoto']['type'],
                'tmp_name' => $dados['alunoFoto']['tmp_name'],
                'error'    => $dados['alunoFoto']['error'],
                'size'     => $dados['alunoFoto']['size'],
            );

            $arquivo = (new \GerenciadorArquivos\Service\Arquivo($this->getEm(), $this->diretorioFotosAluno))
                ->adicionar($dadosArquivo);

            if ($arquivo) {
                $dados['arq'] = $arquivo->getArqId();
            }
        } else {
            if (!empty($dados['arqId'])) {
                $dados['arq'] = $dados['arqId'];
            }
        }

        $arrAluno   = $serviceAluno->getRepository()->find($dados['alunoId'])->toArray();
        $alunoCurso = $serviceAlunoCurso->getRepository()->find($dados['alunocurso'])->toArray();

        $agente = $dados['pesIdAgente'];

        if ($dados['pesIdAgenciador'] && !$agente) {
            $agente = $dados['pesIdAgenciador'];
        }
        if ($alunoCurso['pesIdAgente'] && !$agente) {
            $agente = $alunoCurso['pesIdAgente'];
        }
        if ($arrAluno['pesIdAgenciador'] && !$agente) {
            $agente = $arrAluno['pesIdAgenciador'];
        }

        $origem = $dados['origem'];

        if ($alunoCurso['origem'] && !$origem) {
            $origem = $alunoCurso['origem'];
        }

        if ($arrAluno['origem'] && !$origem) {
            $origem = $arrAluno['origem'];
        }

        $unidade = $dados['unidade'];

        if ($alunoCurso['unidade'] && !$unidade) {
            $unidade = $alunoCurso['unidade'];
        }

        if ($arrAluno['unidade'] && !$unidade) {
            $unidade = $arrAluno['unidade'];
        }

        if ($alunoCurso['pesIdAgente'] != $agente && $alunoCurso['pesIdAgente']) {
            $permitirEdicaoAgente = $serviceAcessoPessoas->validaPessoaEditaAgenteAluno($usuarioLog);

            if (!$permitirEdicaoAgente) {
                $this->setLastError("Você não tem permissão para editar o Agente Educacional!");

                return false;
            }
        }

        $dados['origem']                  = $origem == "" ? null : $origem;
        $dados['pesIdAgente']             = $agente == "" ? null : $agente;
        $dados['unidade']                 = $unidade == "" ? null : $unidade;
        $dados['pesIdAgenciador']         = $arrAluno['pesIdAgenciador'] ? $arrAluno['pesIdAgenciador'] : $agente;
        $alunoCurso['alunocursoCarteira'] = $dados['alunocursoCarteira'];
        $alunoCurso['tiposel']            = $dados['tiposel'];
        $alunoCurso['motivo']             = null;

        $alunoGeral = (new \Matricula\Service\AcadgeralAluno($this->getEm()))->edita($dados);

        if (!empty($dados['arqId']) && !empty($dados['alunoFoto']['name'])) {
            $arquivo = (new \GerenciadorArquivos\Service\Arquivo($this->getEm(), $this->diretorioFotosAluno))->excluir(
                $dados['arqId']
            );
        }

        $contatoAluno['pessoa'] = $dados['pes'];

        if ($dados['idEmail'] == "") {
            $contatoAluno['tipoContato'] = ['E-mail'];
            $contatoAluno['conContato']  = [$dados['alunoEmail']];
            $contatos->adicionar($contatoAluno);
        } else {
            $contatoAluno['conContato']  = $dados['alunoEmail'];
            $contatoAluno['conId']       = $dados['idEmail'];
            $contatoAluno['tipoContato'] = $dados['idTipoEmail'];
            $contatos->edita($contatoAluno);
        }
        if ($dados['idTelefone'] == "") {
            $contatoAluno['conContato']  = [$dados['alunoTelefone']];
            $contatoAluno['tipoContato'] = ['Telefone'];
            $contatos->adicionar($contatoAluno);
        } else {
            $contatoAluno['conContato']  = $dados['alunoTelefone'];
            $contatoAluno['conId']       = $dados['idTelefone'];
            $contatoAluno['tipoContato'] = $dados['idTipoTelefone'];
            $contatos->edita($contatoAluno);
        }
        if ($dados['idCelular'] == "") {
            $contatoAluno['conContato']  = [$dados['alunoCelular']];
            $contatoAluno['tipoContato'] = ['Celular'];
            $contatos->adicionar($contatoAluno);
        } else {
            $contatoAluno['conContato']  = $dados['alunoCelular'];
            $contatoAluno['conId']       = $dados['idCelular'];
            $contatoAluno['tipoContato'] = $dados['idTipoCelular'];
            $contatos->edita($contatoAluno);
        }

        $serviceOrigem     = new \Matricula\Service\AcadgeralCadastroOrigem($this->getEm());
        $serviceAlunoCurso = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());

        /** @var \Matricula\Entity\AcadgeralAlunoCurso $objAlunoCurso */
        if (!($objAlunoCurso = $serviceAlunoCurso->edita($alunoCurso))) {
            $this->setLastError($serviceAlunoCurso->getLastError());

            return false;
        }

        if ($origem) {
            $objAlunoCurso->setOrigem($serviceOrigem->getRepository()->find($origem));
            $this->getEm()->persist($objAlunoCurso);
            $this->getEm()->flush($objAlunoCurso);
        } else {
            $objAlunoCurso->setOrigem(null);
            $this->getEm()->persist($objAlunoCurso);
            $this->getEm()->flush($objAlunoCurso);
        }

        $serviceSisIntegracao->integracaoInteligenteParaAluno($objAlunoCurso);

        $serviceDocumentoPessoa = new \Documentos\Service\DocumentoPessoa($this->getEm());

        for ($i = 0; $i < sizeof($dados['documentos']); $i++) {
            $documento                    = "";
            $documento['pes']             = $dados['pes'];
            $documento['docpessoaStatus'] = (
            $dados['documentos'][$i] == "Não Aplicavel" ? "Dispensavel" : $dados['documentos'][$i]
            );
            $documento['docgrupo']        = $dados['documentoGrupo'][$i];

            $docPessoaGrupo = $serviceDocumentoPessoa->getRepository('Documentos\Entity\DocumentoPessoa')->findOneBy(
                ['pes' => $dados['pes'], 'docgrupo' => $dados['documentoGrupo'][$i]]
            );
            if ($docPessoaGrupo) {
                $serviceDocumentoPessoa->edita($documento);
            } else {
                $serviceDocumentoPessoa->adicionar($documento);
            }
        }

        $responsaveisAluno = $serviceAcadgeralResponsavel->getRepository()->findBy(['aluno' => $alunoGeral]);

        if ($responsaveisAluno) {
            foreach ($responsaveisAluno as $resp) {
                $serviceAcadgeralResponsavel->excluir($resp->getRespId());
            }
        }

        for ($i = 0; $i < sizeof($dados['responsavelNivel']); $i++) {
            $dadosResponsavel = $this->formataDadosReponsavel($dados, $i);

            if ($dadosResponsavel['pesCnpj']) {
                $dadosResponsavel['pesDataInicio'] = (new \DateTime())->format('d-m-Y');
            }

            $dadosResponsavel['aluno'] = $alunoGeral->getAlunoId();
            $responsavel               = $serviceAcadgeralResponsavel->adicionar($dadosResponsavel);

            $contatoResp['pessoa'] = $responsavel->getPes()->getPesId();

            if ($dadosResponsavel['Email'] != "") {
                $contatoResp['tipoContato'] = ['E-mail'];
                $contatoResp['conContato']  = [$dadosResponsavel['Email']];
                $contatos->adicionar($contatoResp);
            }

            if ($dadosResponsavel['Telefone'] != "") {
                $contatoResp['conContato']  = [$dadosResponsavel['Telefone']];
                $contatoResp['tipoContato'] = ['Telefone'];
                $contatos->adicionar($contatoResp);
            }

            if ($dadosResponsavel['Celular'] != "") {
                $contatoResp['conContato']  = [$dadosResponsavel['Celular']];
                $contatoResp['tipoContato'] = ['Celular'];
                $contatos->adicionar($contatoResp);
            }
        }
        $situacoesAtividade = AcadgeralSituacao::situacoesAtividade();

        $situacaoAnalise   = AcadgeralSituacao::situacoesAnalise();
        $situacaoAnalise[] = AcadgeralSituacao::PRE_MATRICULA;
        $situacaoAnalise[] = AcadgeralSituacao::DISPENSADO;

        $situacoesAtivas = array_merge($situacoesAtividade, $situacaoAnalise);

        $DisciplinasCadastradas = $this->getRepository('Matricula\Entity\AcadperiodoAlunoDisciplina')->findBy(
            ['alunoper' => $dados['alunoperId'], 'situacao' => $situacoesAtivas]
        );

        $serviceAlunoDisciplina          = new \Matricula\Service\AcadperiodoAlunoDisciplina($this->getEm());
        $serviceAlunoDisciplinaHistorico = new \Matricula\Service\AcadperiodoAlunoDisciplinaHistorico($this->getEm());

        if ($dados['disciplina'] && !empty($dados['disciplina'])) {
            $idsDisciplinasCadastradas = array();
            /** @var $disciplina \Matricula\Entity\AcadperiodoAlunoDisciplina */
            foreach ($DisciplinasCadastradas as $disciplina) {
                $disciplinaId = $disciplina->getDisc()->getDiscId();

                // pesquisa disciplina cadastrada nos dados enviados
                $discFound = array_search($disciplinaId, $dados['disciplina']);

                /** @var $objSituacao \Matricula\Entity\AcadgeralSituacao */
                $objSituacao = $serviceSituacao->getRepository()->findOneBy(
                    ['matsitDescricao' => $dados['disciplinaSituacao'][$discFound]]
                );

                // armazena turma ATUAL de vinculo da disciplina
                $turmaAtual = $disciplina->getTurma()->getTurmaId();

                // verifica se disciplina enviada foi encontrada dentre as disciplinas ja vinculadas ao aluno no periodo atual
                if ($discFound !== false) {
                    $idsDisciplinasCadastradas[] = $disciplina->getDisc()->getDiscId();

                    // verifica se situação na disciplina mudou ou se a turma de vinculo mudou
                    if (
                        $objSituacao->getSituacaoId() != $disciplina->getSituacao()->getSituacaoId() ||
                        $turmaAtual != $dados['disciplinaTurma'][$discFound]
                    ) {
                        // verifica se turma mudou para não realizar referencia no banco de dados 'atoa'
                        if ($turmaAtual != $dados['disciplinaTurma'][$discFound]) {
                            $turmaNv = $serviceTurma->getRepository()->find(
                                $dados['disciplinaTurma'][$discFound]
                            );
                            $disciplina->setTurma($turmaNv);
                        }

                        $discHistorico = [
                            'alunodisc'        => $disciplina->getAlunodiscId(),
                            'turma'            => $turmaAtual,
                            'usuarioHistorico' => $disciplina->getUsuarioCriador(),
                            'situacao'         => $disciplina->getSituacao()->getSituacaoId(),
                            'situacaodiscData' => $disciplina->getAlunodiscSitData(),
                        ];
                        // salva historico do vinculo da disciplina
                        $serviceAlunoDisciplinaHistorico->save($discHistorico);

                        $disciplina->setSituacao($objSituacao);
                        $disciplina->setUsuarioCriador($usuarioLog);
                        $disciplina->setAlunodiscSitData(new \DateTime('now', new \DateTimeZone('America/Sao_Paulo')));

                        /** @var \Matricula\Entity\AcadperiodoAlunoDisciplina $disciplina */
                        // atualiza valor das disciplinas
                        $serviceAlunoDisciplina->save($disciplina->toArray());
                    }
                } else {
                    $discHistorico = [
                        'alunodisc'        => $disciplina->getAlunodiscId(),
                        'turma'            => $turmaAtual,
                        'usuarioHistorico' => $disciplina->getUsuarioCriador(),
                        'situacao'         => $disciplina->getSituacao()->getSituacaoId(),
                        'situacaodiscData' => $disciplina->getAlunodiscSitData(),
                    ];

                    // salva historico do vinculo da disciplina
                    $serviceAlunoDisciplinaHistorico->save($discHistorico);

                    $disciplina->setSituacao(AcadgeralSituacao::TRANSFERENCIA);
                    $disciplina->setUsuarioCriador($usuarioLog);
                    $disciplina->setAlunodiscSitData(new \DateTime('now', new \DateTimeZone('America/Sao_Paulo')));

                    // atualiza valor das disciplinas
                    $serviceAlunoDisciplina->save($disciplina->toArray());
                }
            }
        }

        $situacaoMatriculado = $situacaoRepository->buscaSituacao('Matriculado');
        $situacaoDependente  = $situacaoRepository->buscaSituacao('Dependente');
        $situacaoAdaptante   = $situacaoRepository->buscaSituacao('Adaptante');
        $situacaoMonografia  = $situacaoRepository->buscaSituacao('Monografia');
        $situacaoProvisoria  = $situacaoRepository->buscaSituacao('Matricula Provisoria');

        $dependencias      = 0;
        $adaptacoes        = 0;
        $monografia        = 0;
        $matriculado       = 0;
        $situacaoMatricula = 0;

        for ($i = 0; $i < sizeof($dados['disciplina']); $i++) {
            $situacao = $situacaoRepository->buscaSituacao($dados['disciplinaSituacao'][$i]);

            if ($dados['novaTurma'][$i] == "true" || empty($dados['novaTurma'][$i])) {
                // verifica se a disciplina ja está vinculada ao aluno
                $cadastrada = in_array($dados['disciplina'][$i], $idsDisciplinasCadastradas);
                // se disciplina não estiver vinculada ao aluno, adiciona
                if (!$cadastrada) {
                    $disciplinaNova['disc']             = $dados['disciplina'][$i];
                    $disciplinaNova['turma']            = $dados['disciplinaTurma'][$i];
                    $disciplinaNova['alunoper']         = $dados['alunoperId'];
                    $disciplinaNova['situacao']         = $situacao;
                    $disciplinaNova['usuarioCriador']   = $usuarioLog;
                    $disciplinaNova['alunodiscSitData'] = new \DateTime('now', new \DateTimeZone('America/Sao_Paulo'));

                    $serviceAlunoDisciplina->save($disciplinaNova);

                    if ($situacao == $situacaoDependente) {
                        $dependencias++;
                    } elseif ($situacao == $situacaoAdaptante) {
                        $adaptacoes++;
                    } elseif ($situacao == $situacaoMonografia) {
                        $monografia++;
                    }
                }
            } else {
                $matriculado++;
            }

            //            if ($situacao == $situacaoMatriculado) {
            //                $situacaoMatricula++;
            //            }
        }

        $situacaoPreMatricula = $situacaoRepository->buscaSituacao('Pré Matricula');

        if ($dados['matsituacao'] == $situacaoPreMatricula) {
            $dados['matsituacao']     = $situacaoRepository->buscaSituacao('Matricula Provisoria');
            $dados['alunoperSitData'] = new \DateTime('now', new \DateTimeZone('America/Sao_Paulo'));

            //            if ($situacaoMatricula) {
            $this->geraMensalidadesAluno($dados['pes'], $dados['alunoperId']);
            $matriculado++;
            //            }
        }

        $dados['turma']            = $dados['alunoTurmaPrincipal'];
        $dados['alunodiscSitData'] = new \DateTime('now', new \DateTimeZone('America/Sao_Paulo'));

        if ($dados['alunoperId']) {

            /** @var $objAcadPeriodoAlunoHistorico \Matricula\Entity\AcadperiodoAluno */
            $objAcadPeriodoAlunoHistorico = $serviceAcadperiodoAluno->getRepository()->find($dados['alunoperId']);

            if ($objAcadPeriodoAlunoHistorico) {
                $objAcadPeriodoAlunoHistorico = clone($objAcadPeriodoAlunoHistorico);
            }
        }

        /** @var \Matricula\Entity\AcadperiodoAluno $alunoPeriodo */
        $alunoPeriodo = parent::edita($dados);

        $situacaoDiferente = (
            $alunoPeriodo->getMatsituacao()->getSituacaoId() <>
            $objAcadPeriodoAlunoHistorico->getMatsituacao()->getSituacaoId()
        );

        $turmaDiferente = (
            $alunoPeriodo->getTurma()->getTurmaId() <>
            $objAcadPeriodoAlunoHistorico->getTurma()->getTurmaId()
        );

        if ($situacaoDiferente || $turmaDiferente) {
            $ok = $serviceAcadperiodoAlunoHistorico->save($objAcadPeriodoAlunoHistorico);

            if (is_array($ok)) {
                $this->setLastError('Não foi possivel salvar os dados do histórico do Aluno');

                return false;
            }
        }

        $situacaoPeriodo = $alunoPeriodo->getMatsituacao()->getSituacaoId();
        $alunoAtivo      = !in_array($situacaoPeriodo, $serviceSituacao::situacoesDesligamento());

        if ($alunoAtivo && $objAlunoCurso->verificaSituacaoPendente()) {
            $objAlunoCurso->setAlunocursoSituacao($serviceAlunoCurso::ALUNOCURSO_SITUACAO_DEFERIDO);
            $this->getEm()->persist($objAlunoCurso);
            $this->getEm()->flush($objAlunoCurso);
        }

        $this->geraTitulos(
            array(
                'alunoperId'   => $dados['alunoperId'],
                'alunocursoId' => $dados['alunocursoId'],
                'pes'          => $dados['pes']
            ),
            array(
                'matriculado'  => $matriculado,
                'dependencias' => $dependencias,
                'adaptacoes'   => $adaptacoes,
                'monografia'   => $monografia
            )
        );

        $info['alunoPeriodo'] = $alunoPeriodo;
        $info['taxas']        = false;

        if (($dependencias > 0) OR ($adaptacoes > 0) OR ($monografia > 0)) {
            $info['taxas'] = true;
        }

        // $this->atualizaInformacoesLeitorBiblioteca($alunoPeriodo);

        $this->commit();

        return $info;
    }

    /**
     * Adiciona alunos da migracao que tem alunocurso mas não tem alunoperido
     * @param array $dados
     * @return \Matricula\Entity\AcadperiodoAluno
     * @throws \Exception
     */
    public function adicionaAlunosMigracaoQueTemAlunocursoMasNaoTemAlunoperido(array $dados)
    {
        $this->begin();
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceAluno         = new \Matricula\Service\AcadgeralAluno($this->getEm());
        $serviceAlunoCurso    = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());

        // usuario logado, para fins de log de alterações
        $usuarioLog = $serviceAcessoPessoas->retornaUsuarioLogado();

        $usuarioLog->getUsuario()->getUsrStatus();
        $dados['usuarioCriador'] = $usuarioLog->getId();

        if ($dados['pes']) {
            $dados['pesId'] = $dados['pes'];
            $pessoa         = (new \Pessoa\Service\Pessoa($this->getEm()))->edita($dados);
        }

        $pessoaFisica = (new \Pessoa\Service\PessoaFisica($this->getEm()))->edita($dados);

        if (!empty($dados['alunoFoto']['name'])) {
            $dadosArquivo = array(
                'name'     => $dados['alunoFoto']['name'],
                'type'     => $dados['alunoFoto']['type'],
                'tmp_name' => $dados['alunoFoto']['tmp_name'],
                'error'    => $dados['alunoFoto']['error'],
                'size'     => $dados['alunoFoto']['size'],
            );

            $arquivo = (new \GerenciadorArquivos\Service\Arquivo($this->getEm(), $this->diretorioFotosAluno))
                ->adicionar($dadosArquivo);

            if ($arquivo) {
                $dados['arq'] = $arquivo->getArqId();
            }
        } else {
            if (!empty($dados['arqId'])) {
                $dados['arq'] = $dados['arqId'];
            }
        }

        $arrAluno   = $serviceAluno->getRepository()->find($dados['alunoId'])->toArray();
        $alunoCurso = $serviceAlunoCurso->getRepository()->find($dados['alunocurso'])->toArray();

        $agente = $dados['pesIdAgente'];

        if ($dados['pesIdAgenciador'] && !$agente) {
            $agente = $dados['pesIdAgenciador'];
        }
        if ($alunoCurso['pesIdAgente'] && !$agente) {
            $agente = $alunoCurso['pesIdAgente'];
        }
        if ($arrAluno['pesIdAgenciador'] && !$agente) {
            $agente = $arrAluno['pesIdAgenciador'];
        }

        $origem = $dados['origem'];

        if ($alunoCurso['origem'] && !$origem) {
            $origem = $alunoCurso['origem'];
        }

        if ($arrAluno['origem'] && !$origem) {
            $origem = $arrAluno['origem'];
        }

        $unidade = $dados['unidade'];

        if ($alunoCurso['unidade'] && !$unidade) {
            $unidade = $alunoCurso['unidade'];
        }

        if ($arrAluno['unidade'] && !$unidade) {
            $unidade = $arrAluno['unidade'];
        }

        if ($alunoCurso['pesIdAgente'] != $agente && $alunoCurso['pesIdAgente']) {
            $permitirEdicaoAgente = $serviceAcessoPessoas->validaPessoaEditaAgenteAluno($usuarioLog);

            if (!$permitirEdicaoAgente) {
                $this->setLastError("Você não tem permissão para editar o Agente Educacional!");

                return false;
            }
        }

        $dados['origem']                  = $origem == "" ? null : $origem;
        $dados['pesIdAgente']             = $agente == "" ? null : $agente;
        $dados['unidade']                 = $unidade == "" ? null : $unidade;
        $dados['pesIdAgenciador']         = $arrAluno['pesIdAgenciador'] ? $arrAluno['pesIdAgenciador'] : $agente;
        $alunoCurso['alunocursoCarteira'] = $dados['alunocursoCarteira'];
        $alunoCurso['tiposel']            = $dados['tiposel'];
        $alunoCurso['motivo']             = null;

        $alunoGeral = (new \Matricula\Service\AcadgeralAluno($this->getEm()))->edita($dados);

        $serviceDocumentoPessoa = new \Documentos\Service\DocumentoPessoa($this->getEm());
        $serviceDocumentoGrupo  = new \Documentos\Service\DocumentoGrupo($this->getEm());

        for ($i = 0; $i < sizeof($dados['documentos']); $i++) {
            $documento                    = "";
            $documento['pes']             = $pessoa->getPesId();
            $documento['docpessoaStatus'] = $dados['documentos'][$i] == "Não Aplicavel" ? "Dispensavel"
                : $dados['documentos'][$i];
            $documento['docgrupo']        = $serviceDocumentoGrupo->findOneBy(
                ['docgrupoId' => $dados['documentoGrupo'][$i]]
            );
            $serviceDocumentoPessoa->adicionar($documento);
        }

        if (!empty($dados['arqId']) && !empty($dados['alunoFoto']['name'])) {
            $arquivo = (new \GerenciadorArquivos\Service\Arquivo($this->getEm(), $this->diretorioFotosAluno))->excluir(
                $dados['arqId']
            );
        }

        $enderecoAluno = (new \Pessoa\Service\Endereco($this->getEm()))->editaMultiplos($dados);

        $contatos               = (new \Pessoa\Service\Contato($this->getEm()));
        $contatoAluno['pessoa'] = $dados['pes'];

        if ($dados['idEmail'] == "") {
            $contatoAluno['tipoContato'] = ['E-mail'];
            $contatoAluno['conContato']  = [$dados['alunoEmail']];
            $contatos->adicionar($contatoAluno);
        } else {
            $contatoAluno['conContato']  = $dados['alunoEmail'];
            $contatoAluno['conId']       = $dados['idEmail'];
            $contatoAluno['tipoContato'] = $dados['idTipoEmail'];
            $contatos->edita($contatoAluno);
        }
        if ($dados['idTelefone'] == "") {
            $contatoAluno['conContato']  = [$dados['alunoTelefone']];
            $contatoAluno['tipoContato'] = ['Telefone'];
            $contatos->adicionar($contatoAluno);
        } else {
            $contatoAluno['conContato']  = $dados['alunoTelefone'];
            $contatoAluno['conId']       = $dados['idTelefone'];
            $contatoAluno['tipoContato'] = $dados['idTipoTelefone'];
            $contatos->edita($contatoAluno);
        }
        if ($dados['idCelular'] == "") {
            $contatoAluno['conContato']  = [$dados['alunoCelular']];
            $contatoAluno['tipoContato'] = ['Celular'];
            $contatos->adicionar($contatoAluno);
        } else {
            $contatoAluno['conContato']  = $dados['alunoCelular'];
            $contatoAluno['conId']       = $dados['idCelular'];
            $contatoAluno['tipoContato'] = $dados['idTipoCelular'];
            $contatos->edita($contatoAluno);
        }

        $responsaveisAluno = $this->getRepository('Matricula\Entity\AcadgeralResponsavel')->findBy(
            ['aluno' => $alunoGeral]
        );

        $servicePessoa = new \Pessoa\Service\Pessoa($this->getEm());

        if ($responsaveisAluno) {
            foreach ($responsaveisAluno as $resp) {
                $servicePessoa->excluir($resp->getPes()->getPesId());
            }
        }

        for ($i = 0; $i < sizeof($dados['responsavelNivel']); $i++) {
            $dadosResponsavel = $this->formataDadosReponsavel($dados, $i);
            if ($dadosResponsavel['pesCnpj']) {
                $dadosResponsavel['pesDataInicio'] = (new \DateTime('now'))->format('d-m-Y');
            }
            $dadosResponsavel['aluno'] = $alunoGeral->getAlunoId();
            $responsavel               = (new \Matricula\Service\AcadgeralResponsavel($this->getEm()))->adicionar(
                $dadosResponsavel
            );

            $contatoResp['pessoa'] = $responsavel->getPes()->getPesId();
            if ($dadosResponsavel['Email'] != "") {
                $contatoResp['tipoContato'] = ['E-mail'];
                $contatoResp['conContato']  = [$dadosResponsavel['Email']];
                $contatos->adicionar($contatoResp);
            }
            if ($dadosResponsavel['Telefone'] != "") {
                $contatoResp['conContato']  = [$dadosResponsavel['Telefone']];
                $contatoResp['tipoContato'] = ['Telefone'];
                $contatos->adicionar($contatoResp);
            }
            if ($dadosResponsavel['Celular'] != "") {
                $contatoResp['conContato']  = [$dadosResponsavel['Celular']];
                $contatoResp['tipoContato'] = ['Celular'];
                $contatos->adicionar($contatoResp);
            }
        }

        /** @var \Matricula\Entity\AcadgeralAlunoCurso $alunoCurso */
        $alunoCurso = (new \Matricula\Service\AcadgeralAlunoCurso($this->getEm()))->edita($alunoCurso);

        $dadosAlunoPeriodo['alunocurso']      = $alunoCurso->getAlunocursoId();
        $dadosAlunoPeriodo['alunocursoId']    = $alunoCurso->getAlunocursoId();
        $dadosAlunoPeriodo['matsituacao']     = $this
            ->getRepository('Matricula\Entity\AcadgeralSituacao')->buscaSituacao('Matricula Provisoria');
        $dadosAlunoPeriodo['situacaoId']      = $dadosAlunoPeriodo['matsituacao'];
        $dadosAlunoPeriodo['turma']           = $dados['alunoTurmaPrincipal'];
        $dadosAlunoPeriodo['turmaId']         = $dados['alunoTurmaPrincipal'];
        $dadosAlunoPeriodo['alunoperSitData'] = new \DateTime('now');
        $dadosAlunoPeriodo['usuarioCriador']  = $usuarioLog->getId();

        //vendo se alunocurso já possui registro na tabela alunoperiodo no periodo letivo informado
        $arrDadosAlunoPeriodoAux = $this
            ->buscaMatriculaAlunoPeriodo($dados['periodoLetivo'], $alunoCurso->getAlunocursoId());

        if ($arrDadosAlunoPeriodoAux) {
            $dadosAlunoPeriodo['alunoperId'] = $arrDadosAlunoPeriodoAux["alunoper_id"];
        }

        //essa função edita caso possua um aluno periodo id ou adiciona um aluno periodo caso não possua
        if (!$this->salvarAlunoPeriodo($dadosAlunoPeriodo)) {
            throw new \Exception($this->getLastError());
        }

        $alunoPeriodo = $this->getRepository()->find($dadosAlunoPeriodo['alunoperId']);

        $dados['alunoperId'] = $alunoPeriodo->getAlunoperId();

        for ($i = 0; $i < sizeof($dados['disciplina']); $i++) {
            $disciplina['disc']             = $dados['disciplina'][$i];
            $disciplina['turma']            = $dados['disciplinaTurma'][$i];
            $disciplina['alunoper']         = $alunoPeriodo->getAlunoperId();
            $disciplina['alunodiscSitData'] = (new \DateTime());
            $disciplina['usuarioCriador']   = $usuarioLog->getId();
            $disciplina['situacao']         = $this->getRepository("Matricula\Entity\AcadgeralSituacao")->findOneBy(
                array('matsitDescricao' => $dados['disciplinaSituacao'][$i])
            )->getSituacaoId();

            $info['taxas'] = false;
            if ($dados['disciplinaSituacao'][$i] == 'Dependente' OR $dados['disciplinaSituacao'][$i] == 'Monografia' OR $dados['disciplinaSituacao'][$i] == 'Adaptante') {
                $info['taxas'] = true;
            }

            $alunoDisciplina = (new \Matricula\Service\AcadperiodoAlunoDisciplina($this->getEm()))->adicionar(
                $disciplina
            );
        }

        if (!in_array(
            $alunoPeriodo->getMatsituacao()->getSituacaoId(),
            \Matricula\Service\AcadgeralSituacao::situacoesDesligamento()
        )
        ) {
            $alunoCurso->setAlunocursoSituacao(\Matricula\Service\AcadgeralAlunoCurso::ALUNOCURSO_SITUACAO_DEFERIDO);
            $this->getEm()->persist($alunoCurso);
            $this->getEm()->flush($alunoCurso);
        }

        $info['alunoPeriodo'] = $alunoPeriodo;

        if (!$this->geraMensalidadesAluno($dados['pes'], $alunoPeriodo->getAlunoperId())) {
            return false;
        }

        $this->atualizaInformacoesLeitorBiblioteca($alunoPeriodo);

        $this->commit();

        return $info;
    }

    /**
     * Método para buscar os dados do candidato lá no vestibular.
     * recebe como parâmetro o id da inscrição e retorna um array
     * $array['endereço'] com o endereço do candidato
     * $array['pes'] com as informações do aluno enquanto pessoa.
     * */
    public function buscaDadosVestibular($inscricaoId)
    {
        $dadosCandidato              = array();
        $dadosCandidato['inscricao'] = $this->getRepository("Vestibular\Entity\SelecaoInscricao")->findOneBy(
            array('inscricaoId' => $inscricaoId)
        )->toArray();
        $dadosCandidato['pes']       = (new \Pessoa\Service\Pessoa($this->getEm()))->buscaDadosPessoa(
            $dadosCandidato['inscricao']['pes']
        );
        $dadosCandidato['endereco']  = $this->getRepository("Pessoa\Entity\Endereco")->findOneBy(
            ['pes' => $dadosCandidato['pes']['pes']]
        );
        if (!empty($dadosCandidato['endereco'])) {
            $dadosCandidato['endereco'] = $dadosCandidato['endereco']->toArrayAux();
        }
        $dadosCandidato['contato'] = $this->getRepository("Pessoa\Entity\Contato")->buscaContatos(
            $dadosCandidato['pes']['pes']
        );
        if ($dadosCandidato['contato']) {
            $dadosCandidato['contato'] = $this->mapContato($dadosCandidato['contato']);
        }

        return $dadosCandidato;
    }

    /**
     * Mapeia informações sobre contato de aluno
     * @param array $contatos
     * @return array
     */
    public function mapContato($contatos)
    {
        $contatoAluno = [];
        foreach ($contatos as $key => $contato) {
            if ($key == "E-mail") {
                $contatoAluno['idEmail']     = $contato[0]->getConId();
                $contatoAluno['alunoEmail']  = $contato[0]->getConContato();
                $contatoAluno['idTipoEmail'] = $contato[0]->getTipoContato()->getTipContatoId();
            }
            if ($key == "Telefone") {
                $contatoAluno['idTelefone']     = $contato[0]->getConId();
                $contatoAluno['alunoTelefone']  = $contato[0]->getConContato();
                $contatoAluno['idTipoTelefone'] = $contato[0]->getTipoContato()->getTipContatoId();
            }
            if ($key == "Celular") {
                $contatoAluno['idCelular']     = $contato[0]->getConId();
                $contatoAluno['alunoCelular']  = $contato[0]->getConContato();
                $contatoAluno['idTipoCelular'] = $contato[0]->getTipoContato()->getTipContatoId();
            }
        }

        return $contatoAluno;
    }

    /**
     * Retorna dados de alunos com siatuação matriculado, Pendências Acadêmicas ou Matricula Provisoria
     * @param array $arrayData
     * @return array
     */
    public function paginationAjax(array $arrayData)
    {
        if ($arrayData['filter']['naoTrazerDados']) {
            return array(
                "draw"            => 0,
                "recordsFiltered" => 0,
                "recordsTotal"    => 0,
                "data"            => [],
            );
        }

        $situacaoAtivo = true;

        if (isset($arrayData['filter']['per_id'])) {
            if ($arrayData['filter']['per_id'] == "desligados") {
                $situacaoAtivo = false;
            }
        }

        $query              = $this->retornaConsultaAcademicaAluno($arrayData['filter']);
        $result             = $this->paginationDataTablesAjax($query, $arrayData, null, false);
        $result['situacao'] = $situacaoAtivo;

        return $result;
    }

    /**
     * Retorna dados de aluno para listagem
     * @param int        $page
     * @param bool|false $is_json
     * @return array
     */
    public function pagination($page = 0, $is_json = false)
    {
        $dados = array();
        $sql   = "";

        if (!$page) {
            if ($is_json) {
                $limit = " LIMIT 10 OFFSET 0 ";
            } else {
                $limit = '';
            }
        } else {
            $offset = (($page - 1) * 10);
            $limit  = " LIMIT 10 OFFSET $offset ";
        }

        $sql = "
            SELECT
              q.*
            FROM
              (  (SELECT
                  antigo.alunoper_id,
                  antigo.Matricula alunocurso_id,
                  antigo.Nome pes_nome,
                  antigo.Periodo periodo,
                  antigo.Turma turma_nome,
                  antigo.Situacao situacao_aluno,
                  novo.alunoper_id rematricula
                  FROM
                    view__alunoperiodo antigo
                  NATURAL JOIN
                    acadperiodo__letivo
                  INNER JOIN
                    view__alunoperiodo novo ON novo.Matricula = antigo.Matricula
                  WHERE
                    antigo.situacao = 'Matriculado'
                  AND
                    novo.situacao = 'Pré Matricula'
                  AND
                    novo.per_id = (SELECT max(per_id) FROM acadperiodo__letivo)
                )
                UNION
                  (SELECT
                    alunoper_id,
                    Matricula alunocurso_id,
                    Nome pes_nome,
                    Periodo periodo,
                    Turma turma_nome,
                    Situacao situacao_aluno,
                    'Indisponível'
                  FROM
                    view__alunoperiodo antigo
                  NATURAL JOIN
                    acadperiodo__letivo

                  WHERE
                    situacao = 'Matriculado'
                  AND
                    tturma_descricao != 'Convencional')
                  UNION
                    (SELECT
                    antigo.alunoper_id,
                    antigo.Matricula alunocurso_id,
                    antigo.Nome pes_nome,
                    antigo.Periodo periodo,
                    antigo.Turma turma_nome,
                    antigo.Situacao situacao_aluno,
                    'Indisponível'
                    FROM
                      view__alunoperiodo antigo
                    NATURAL JOIN
                      acadperiodo__letivo

                    WHERE
                      antigo.situacao = 'Matriculado'

                    AND
                      antigo.per_id = (SELECT max(per_id) FROM acadperiodo__letivo)
                    )
                  UNION (
                      SELECT
                        antigo.alunoper_id,
                        antigo.Matricula alunocurso_id,
                        antigo.Nome pes_nome,
                        antigo.Periodo periodo,
                        antigo.Turma turma_nome,
                        antigo.Situacao situacao_aluno,
                        'Indisponível'
                        FROM
                          view__alunoperiodo antigo
                        NATURAL JOIN
                          acadperiodo__letivo

                        WHERE
                          antigo.situacao = 'Matricula Provisoria'

                        AND
                          antigo.per_id = (SELECT max(per_id) FROM acadperiodo__letivo)
                  )
                  UNION (
                      SELECT
                            antigo.alunoper_id,
                            antigo.Matricula alunocurso_id,
                            antigo.Nome pes_nome,
                            antigo.Periodo periodo,
                            antigo.Turma turma_nome,
                            antigo.Situacao situacao_aluno,
                            antigo.alunoper_id rematricula
                            FROM
                              view__alunoperiodo antigo
                            NATURAL JOIN
                              acadperiodo__letivo
                            WHERE
                              antigo.situacao = 'Pré Matricula'
                            AND
                              antigo.per_id = (SELECT max(per_id) FROM acadperiodo__letivo)

                            AND antigo.Periodo > 2
                  )
                  UNION (
                      SELECT 'Indisponível',
                        acadgeral__aluno_curso.alunocurso_id,
                        pes_nome,
                        'periodo',
                        'Indisponível',
                        'Pré Matricula',
                        'rematriculaCorreria'
                        FROM
                          acadgeral__aluno_curso
                        LEFT JOIN
                          acadperiodo__aluno ON acadperiodo__aluno.alunocurso_id = acadgeral__aluno_curso.alunocurso_id
                        NATURAL JOIN
                          acadgeral__aluno
                        NATURAL JOIN
                          pessoa
                        WHERE 1
                        AND alunoper_id IS NULL
                  )
              ) q
            WHERE 1
            LIMIT 300
        ";

        $sql .= " $limit ";
        $dados['dados'] = $this->executeQuery($sql);
        $dados['count'] = $dados['dados']->rowCount();

        if ($is_json) {
            $dados['dados'] = $this->paginationFormatJson($dados['dados']);
        }

        return $dados;
    }

    /**
     * Retorna dados para listagem de alunos usando datatables
     * @param array      $post
     * @param int        $page
     * @param bool|false $is_json
     * @return array
     */
    public function resultSearch($post, $page = 0, $is_json = false)
    {
        $dados = array();
        //        $dados['count'] = $this->executeQuery("SELECT COUNT(*) as row  FROM {$this->getMetadados()->table['name']}")->fetch()['row'];
        $sql = "";
        if (!$page) {
            if ($is_json) {
                $limit = " LIMIT 10 OFFSET 0 ";
            } else {
                $limit = "";
            }
        } else {
            $offset = (($page - 1) * 10);
            $limit  = " LIMIT 10 OFFSET $offset ";
        }

        $WHERE = "";

        foreach ($post as $key => $value) {
            if ($value != "" && $value != 'NULL') {
                preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $key, $matches);
                $ret = $matches[0];
                foreach ($ret as &$match) {
                    $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
                }
                $key = implode('_', $ret);
                if ($key == 'turma_id') {
                    $key = 'acadperiodo__aluno.turma_id';
                }
                if ($key == 'turma_turno') {
                    $WHERE .= " AND {$key} LIKE '%{$value}%'";
                }
                if ($key == 'pes_nome') {
                    $WHERE .= " AND {$key} LIKE '%{$value}%'";
                }
                if ($key == 'situacao_aluno') {
                    $situacaoNome = (new \Matricula\Service\AcadgeralSituacao($this->getEm()))->findOneBy(
                        ['situacaoId' => $value]
                    );
                    $periodo      = (new \Matricula\Service\AcadperiodoLetivo($this->getEm()))->buscaPeriodoCorrente(
                    )->getPerId();
                    $WHERE .= " AND {$key} = '{$situacaoNome->getMatsitDescricao()}' AND periodo = {$periodo}";
                }
                if ($key == 'acadperiodo__aluno.turma_id') {
                    $turmaNome = (new \Matricula\Service\AcadperiodoTurma($this->getEm()))->findOneBy(
                        ['turmaId' => $value]
                    );
                    $periodo   = (new \Matricula\Service\AcadperiodoLetivo($this->getEm()))->buscaPeriodoAtual(
                    )->getPerId();
                    $WHERE .= " AND turma_nome = '{$turmaNome->getTurmaNome()}' AND periodo = {$periodo}";
                }
                if ($key == 'pes_nome') {
                    $$WHERE .= "AND {$key} = '{$value}'";
                }
                if ($key == 'alunocurso_id') {
                    $WHERE .= "AND {$key} = '{$value}'";
                }
            }
        }

        $sql = "
             SELECT
              *
            FROM
              (  (SELECT
                  antigo.alunoper_id,
                  antigo.Matricula alunocurso_id,
                  antigo.Nome pes_nome,
                  antigo.per_id periodo,
                  antigo.Turma turma_nome,
                  antigo.Situacao situacao_aluno,
                  novo.alunoper_id rematricula
                  FROM
                    view__alunoperiodo antigo
                  NATURAL JOIN
                    acadperiodo__letivo
                  INNER JOIN
                    view__alunoperiodo novo ON novo.Matricula = antigo.Matricula
                  WHERE
                    antigo.situacao = 'Matriculado'
                  AND
                    novo.situacao = 'Pré Matricula'
                  AND
                    novo.per_id = (select max(per_id) from acadperiodo__letivo)
                )

                  UNION
                    (SELECT
                    antigo.alunoper_id,
                    antigo.Matricula alunocurso_id,
                    antigo.Nome pes_nome,
                    antigo.per_id periodo,
                    antigo.Turma turma_nome,
                    antigo.Situacao situacao_aluno,
                    'Indisponível'
                    FROM
                      view__alunoperiodo antigo
                    NATURAL JOIN
                      acadperiodo__letivo

                    WHERE
                      antigo.situacao = 'Matriculado'

                    AND
                      antigo.per_id = (select max(per_id) from acadperiodo__letivo)
                    )
                  UNION (
                      SELECT
                        antigo.alunoper_id,
                        antigo.Matricula alunocurso_id,
                        antigo.Nome pes_nome,
                        antigo.per_id periodo,
                        antigo.Turma turma_nome,
                        antigo.Situacao situacao_aluno,
                        'Indisponível'
                        FROM
                          view__alunoperiodo antigo
                        NATURAL JOIN
                          acadperiodo__letivo

                        WHERE
                          antigo.situacao = 'Matricula Provisoria'

                        AND
                          antigo.per_id = (select max(per_id) from acadperiodo__letivo)
                  )
                  UNION (
                      SELECT
                            antigo.alunoper_id,
                            antigo.Matricula alunocurso_id,
                            antigo.Nome pes_nome,
                            antigo.per_id periodo,
                            antigo.Turma turma_nome,
                            antigo.Situacao situacao_aluno,
                            antigo.alunoper_id rematricula
                            FROM
                              view__alunoperiodo antigo
                            NATURAL JOIN
                              acadperiodo__letivo
                            WHERE
                              antigo.situacao = 'Pré Matricula'
                            AND
                              antigo.per_id = (select max(per_id) from acadperiodo__letivo)

                            AND antigo.Periodo > 2
                  )
                  UNION (
                      SELECT 'Indisponível',
                        acadgeral__aluno_curso.alunocurso_id,
                        pes_nome,
                        'Indisponível',
                        'Indisponível',
                        'Pré Matricula',
                        'rematriculaAlunosMigracaoQueTemAlunocursoMasNaoTemAlunoperido'
                        FROM
                          acadgeral__aluno_curso
                        LEFT JOIN
                          acadperiodo__aluno ON acadperiodo__aluno.alunocurso_id = acadgeral__aluno_curso.alunocurso_id
                        NATURAL JOIN
                          acadgeral__aluno
                        NATURAL JOIN
                          pessoa
                        WHERE 1
                        AND alunoper_id IS NULL
                  )
              ) q
            WHERE 1

            $WHERE

        ";

        $sql .= " $limit ";
        $dados['dados'] = $this->executeQuery($sql);
        $dados['count'] = $dados['dados']->rowCount();
        if ($is_json) {
            $dados['dados'] = $this->vFormatJson($dados['dados']);
        }

        return $dados;
    }

    /**
     * Busca períodos letivos que o alunos esteve matriculado
     * @param int $alunoperId
     * @return array
     */
    public function buscaPeriodoLetivoAluno($alunoperId, $periodosEmAberto = false)
    {
        $query = "
        SELECT
            aluno.alunocurso_id,
            alunoper_id,
            turma.per_id,
            turma.mat_cur_id,
            periodo.*,
            periodo.per_id id,
            periodo.per_nome 'text',
            turma.turma_nome,
            situacao.situacao_id,
            situacao.matsit_descricao,
            turma.turma_serie,
            turma.turma_turno
        FROM acadperiodo__letivo periodo
        INNER JOIN acadperiodo__turma turma ON turma.per_id = periodo.per_id
        INNER JOIN acadperiodo__aluno aluno ON aluno.turma_id = turma.turma_id
        INNER JOIN acadgeral__situacao situacao ON situacao.situacao_id=aluno.matsituacao_id
        WHERE aluno.alunoper_id = " . $alunoperId;

        if ($periodosEmAberto) {
            $query .= " AND date(now()) BETWEEN periodo.per_matricula_inicio AND periodo.per_matricula_fim";
        }

        return $this->executeQuery($query)->fetchAll();
    }

    /**
     * Busca pessoa do aluno
     * @param int $id
     * @return bool
     */
    public function buscaAluno($id)
    {
        if ($id) {
            $query = "
          SELECT
              pessoa.pes_nome,
              pessoa.pes_id as pes
            FROM
              acadperiodo__aluno
            INNER JOIN
              acadgeral__situacao
            ON
              acadgeral__situacao.situacao_id = acadperiodo__aluno.matsituacao_id
            INNER JOIN
              acadgeral__aluno_curso
            ON
              acadgeral__aluno_curso.alunocurso_id = acadperiodo__aluno.alunocurso_id
            INNER JOIN
              acadgeral__aluno
            ON
              acadgeral__aluno.aluno_id = acadgeral__aluno_curso.aluno_id
            INNER JOIN
              pessoa_fisica
            ON
              pessoa_fisica.pes_id = acadgeral__aluno.pes_id
            INNER JOIN
              pessoa
            ON
              pessoa.pes_id = pessoa_fisica.pes_id
            WHERE
              acadperiodo__aluno.alunoper_id = {$id}
        ";

            $result = $this->executeQuery($query);

            if ($result->rowCount() > 0) {
                return $result->fetchAll()[0];
            }
        }

        return false;
    }

    /**
     * Cancela matrícula do aluno no período
     * @param int $id
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     */
    public function cancelarMatriculaPeriodo($id)
    {
        if ($id) {
            $service = new \Sistema\Service\SisIntegracao($this->getEm());
            $aluno   = $this->getEm()->getReference('Matricula\Entity\AcadperiodoAluno', $id);

            if ($aluno) {
                /** @var  $objAcadPeriodoAlunoHistorico \Matricula\Entity\AcadperiodoAluno */
                $objAcadPeriodoAlunoHistorico = clone ($aluno);
            }

            $situacao = $this->getRepository('Matricula\Entity\AcadgeralSituacao')->findOneBy(
                array('matsitDescricao' => 'Cancelada')
            );

            $serviceAcadperiodoAlunoHistorico = new \Matricula\Service\AcadperiodoAlunoHistorico($this->getEm());

            if ($situacao) {
                $aluno->setMatsituacao($situacao);
                $aluno->setAlunoperSitData(new \DateTime());
                $this->getEm()->persist($aluno);
                $this->atualizaInformacoesLeitorBiblioteca($aluno);
                $disciplinas = $this->getEm()->getRepository('Matricula\Entity\AcadperiodoAlunoDisciplina')->findBy(
                    array('alunoper' => $id)
                );
                if ($disciplinas) {
                    foreach ($disciplinas as $disciplina) {
                        $disciplina->setSituacao($situacao);
                        $this->getEm()->persist($disciplina);
                    }

                    $situacaoDiferente = (
                        $aluno->getMatsituacao()->getSituacaoId() <>
                        $objAcadPeriodoAlunoHistorico->getMatsituacao()->getSituacaoId()
                    );

                    $turmaDiferente = (
                        $aluno->getTurma()->getTurmaId() <>
                        $objAcadPeriodoAlunoHistorico->getTurma()->getTurmaId()
                    );

                    if ($situacaoDiferente || $turmaDiferente) {
                        $ok = $serviceAcadperiodoAlunoHistorico->save($objAcadPeriodoAlunoHistorico);

                        if (is_array($ok)) {
                            $this->setLastError('Não foi possivel salvar os dados do histórico do Aluno');

                            return false;
                        }
                    }

                    $mensalidades = $this->getEm()->getRepository('Boleto\Entity\FinanceiroTituloMensalidade')->findBy(
                        array('alunoper' => $id)
                    );

                    if ($mensalidades) {
                        foreach ($mensalidades as $mensalidade) {
                            $titulo = $this->getEm()->getRepository('Boleto\Entity\FinanceiroTitulo')->findOneBy(
                                array(
                                    'tituloId'     => $mensalidade->getTitulo()->getTituloId(),
                                    'tituloEstado' => 'Aberto'
                                )
                            );
                            if ($titulo) {
                                $titulo->setTituloEstado('Cancelado');
                                $this->getEm()->persist($titulo);
                            }
                        }
                        $descontos = $this->getEm()->getRepository('Boleto\Entity\FinanceiroDesconto')->findBy(
                            array('alunoper' => $id)
                        );
                        if ($descontos) {
                            foreach ($descontos as $desconto) {
                                $descontoMensalidades = $this->getEm()->getRepository(
                                    'Boleto\Entity\FinanceiroMensalidadeDesconto'
                                )->findBy(array('desconto' => $desconto->getDescontoId()));
                                if ($descontoMensalidades) {
                                    foreach ($descontoMensalidades as $descontoMensalidade) {
                                        $descontoMensalidade->setDescmensalidadeStatus('Inativo');
                                        $this->getEm()->persist($descontoMensalidade);
                                    }
                                }
                            }
                        }

                        $this->getEm()->flush();

                        return true;
                    }
                }
            }

            $service->integracaoInteligenteParaAluno($aluno);
        }

        return false;
    }

    /**
     * Busca turma principal do aluno
     * @param int $perId
     * @return array()
     */
    public function buscaTurmaPrincipalAluno($perId)
    {
        return $this->executeQuery(
            "SELECT
                    acadperiodo__turma.*
                  FROM
                    acadperiodo__aluno
                  NATURAL JOIN
                    acadgeral__aluno_curso
                  NATURAL JOIN
                    acadperiodo__turma
                  NATURAL JOIN
                    acadperiodo__letivo
                  WHERE
                   alunoper_id = {$perId}"
        )->fetchAll();
    }

    /**
     * Efetua pré matrícula de uma turma
     * @param $turmaOrigem
     * @param $turmaDestino
     * @throws \Exception
     */
    public function preMatriculaAlunoTurma($turmaOrigem, $turmaDestino)
    {
        $ObjSituacaoMatriculado = $this->getRepository('Matricula\Entity\AcadgeralSituacao')->findOneBy(
            ['matsitDescricao' => 'Matriculado']
        );

        $ObjSituacaoPreMatriculado = $this->getRepository('Matricula\Entity\AcadgeralSituacao')->findOneBy(
            ['matsitDescricao' => 'Pré Matricula']
        );

        //criar metodo que filtre os aluno

        $alunos = $this->getRepository()->findBy(
            ['turma' => $turmaOrigem, 'matsituacao' => $ObjSituacaoMatriculado->getSituacaoId()]
        );

        $matrizTurma = $turmaDestino->getMatcur()->getMatCurId();
        $periodo     = $turmaDestino->getTurmaSerie();

        $matrizDisciplinas = (new \Matricula\Service\AcadperiodoMatrizDisciplina($this->getEm()))->findBy(
            ['matCur' => $matrizTurma, 'perDiscPeriodo' => $periodo]
        );

        $session    = new \Zend\Authentication\Storage\Session();
        $arrUsuario = $this->getRepository("Acesso\Entity\AcessoPessoas")->find($session->read()['id']);

        $dataAtual = new \DateTime('now', new \DateTimeZone('America/Sao_Paulo'));
        $this->begin();
        foreach ($alunos as $aluno) {
            $alunoper['alunocurso']      = $aluno->getAlunocurso();
            $alunoper['turma']           = $turmaDestino;
            $alunoper['matsituacao']     = $ObjSituacaoPreMatriculado;
            $alunoper['alunoperSitData'] = $dataAtual;
            $alunoper['usuarioCriador']  = $arrUsuario;
            $entityAlunoper              = new \Matricula\Entity\AcadperiodoAluno($alunoper);

            try {
                $this->getEm()->persist($entityAlunoper);
            } catch (\Exception $ex) {
                throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
            }

            foreach ($matrizDisciplinas as $matrizDisciplina) {
                $alunoDisciplina['disc']             = $matrizDisciplina->getDisc();
                $alunoDisciplina['turma']            = $turmaDestino;
                $alunoDisciplina['alunoper']         = $entityAlunoper;
                $alunoDisciplina['alunodiscSitData'] = $dataAtual;
                $alunoDisciplina['situacao']         = $ObjSituacaoPreMatriculado;
                $alunoDisciplina['usuarioCriador']   = $arrUsuario;

                $alunoDisciplina = new \Matricula\Entity\AcadperiodoAlunoDisciplina($alunoDisciplina);
                try {
                    $this->getEm()->persist($alunoDisciplina);
                } catch (\Exception $ex) {
                    throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
                }
                unset($alunoDisciplina);
            }
            unset($entityAlunoper);
        }
        $this->getEm()->flush();
        $this->getEm()->clear();
        $this->commit();
    }

    /**
     * Retorna matrículas de alunos no período pela matrícula no curso
     * @param int $id alunocursoId
     * @return array|null
     */
    public function buscaAlunoPeriodos($id)
    {
        $alunoPeriodos = $this->getRepository('Matricula\Entity\AcadperiodoAluno')->findBy(['alunocurso' => $id]);

        if ($alunoPeriodos) {
            return $alunoPeriodos;
        }

        return null;
    }

    /**
     * Busca matrícula no periodo pelo alunocursoId
     * @param int|null $matricula alunocursoId
     * @param int|null $periodo
     * @return \Matricula\Entity\AcadperiodoAluno|null
     */
    public function buscaAlunoperiodoPorMatricula($matricula = null, $periodo = null)
    {
        if (!empty($matricula)) {
            $query = '
            SELECT aluno.alunoper_id
            FROM acadperiodo__aluno aluno
            INNER JOIN acadperiodo__turma turma USING(turma_id)
            WHERE aluno.alunocurso_id = ' . $matricula . ($periodo ? ' AND turma.per_id = ' . $periodo : '') . '
            ORDER BY aluno.alunoper_id DESC';

            $arrAlunoPeriodo = $this->executeQuery($query)->fetch();

            if ($arrAlunoPeriodo) {
                return $this->getRepository()->find($arrAlunoPeriodo['alunoper_id']);
            }
        }

        return null;
    }

    /**
     * Ativa matrícula de aluno
     * @param int $id
     * @return \Matricula\Entity\AcadperiodoAluno
     * @throws \Exception
     */
    public function ativaMatriculaAluno($id)
    {
        $serviceAcadperiodoAlunoHistorico = new \Matricula\Service\AcadperiodoAlunoHistorico($this->getEm());
        if ($id) {
            $alunoperiodo = $this->getReference($id);

            if ($alunoperiodo) {
                $objAcadPeriodoAlunoHistorico = clone $alunoperiodo;

                $alunoperiodo->setMatsituacao(
                    $this->getRepository('Matricula\Entity\AcadgeralSituacao')->findOneBy(
                        ['matsitDescricao' => 'Pendências Acadêmicas']
                    )
                );
                $alunoperiodo->setAlunoperSitData((new \DateTime()));

                try {
                    $situacaoDiferente = (
                        $alunoperiodo->getMatsituacao()->getSituacaoId() <>
                        $objAcadPeriodoAlunoHistorico->getMatsituacao()->getSituacaoId()
                    );

                    $turmaDiferente = (
                        $alunoperiodo->getTurma()->getTurmaId() <>
                        $objAcadPeriodoAlunoHistorico->getTurma()->getTurmaId()
                    );

                    if ($situacaoDiferente || $turmaDiferente) {
                        $ok = $serviceAcadperiodoAlunoHistorico->save($objAcadPeriodoAlunoHistorico);

                        if (is_array($ok)) {
                            $this->setLastError('Não foi possivel salvar os dados do histórico do Aluno');

                            return false;
                        }
                    }

                    $this->getEm()->persist($alunoperiodo);
                    $this->getEm()->flush();
                } catch (\Exception $ex) {
                    throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
                }

                $this->atualizaInformacoesLeitorBiblioteca($alunoperiodo);

                return $alunoperiodo;
            }
        }
    }

    /**
     * Vincula fotos aos alunos
     * @param array $imagens
     * @throws \Exception
     */
    public function vinculaFotosAlunos($imagens)
    {
        $count = 0;

        $serviceArquivo       = (new \GerenciadorArquivos\Service\Arquivo($this->getEm(), $this->diretorioFotosAluno));
        $repositoryAlunoCurso = $this->getRepository('Matricula\Entity\AcadgeralAlunoCurso');

        foreach ($imagens as $img) {
            $nomeArquivo = end(explode('/', $img));
            $matricula   = explode('.', $nomeArquivo);
            $matricula   = preg_replace("/[^0-9]/", "", ltrim($matricula[0], '0'));

            if (strlen($matricula) <= 8) {
                $alunoCurso = $repositoryAlunoCurso->findOneBy(['alunocursoId' => $matricula]);

                // && empty($alunoCurso->getAluno()->getArq())
                //acrecentar a linha acima se quiser somente alunos que ainda não posuem fotos
                if (!empty($alunoCurso)) {
                    $aluno = $alunoCurso->getAluno()->getAlunoId();

                    $query = "
                    SELECT
                        *
                    FROM
                      acadgeral__aluno
                    where
                      aluno_id = {$aluno}
                    AND
                      arq_id is null
                    ";

                    $result = $this->executeQuery($query);

                    if ($result->rowCount() > 0) {
                        $dadosArquivo = array(
                            'name'     => $nomeArquivo,
                            'tmp_name' => $img,
                            'type'     => 'image/jpg',
                            'error'    => '',
                            'size'     => filesize($img),
                        );

                        $arquivo = $serviceArquivo->adicionar($dadosArquivo);
                        //$aluno->setArq($arquivo);

                        $count++;
                        try {
                            $query = "UPDATE acadgeral__aluno set arq_id = {$arquivo->getArqId()} WHERE aluno_id = {$aluno}";
                            $this->executeQuery($query);
                        } catch (\Exception $ex) {
                            throw new \Exception($ex->getMessage());
                        }
                        unset($arquivo);
                        unset($alunoCurso);
                        unset($aluno);
                    }
                }
            }
        }
    }

    /**
     * busca dados pessoais, residenciais e contatos de um aluno
     * @param int $alunoperId
     * @return array|null
     */
    public function buscaDadosPessoaisResidenciaisContatosAluno($alunoperId)
    {
        if ($alunoperId) {
            $dadosAluno = array();

            $serviceEndereco    = (new \Pessoa\Service\Endereco($this->getEm()));
            $serviceContato     = (new \Pessoa\Service\Contato($this->getEm()));
            $serviceAlunoCurso  = (new \Matricula\Service\AcadgeralAlunoCurso($this->getEm()));
            $estados['estados'] = $serviceEndereco->buscaEstados();

            $dadosAluno = array_merge($dadosAluno, $estados);

            $reference = $this->getReference($alunoperId);

            if ($reference) {
                $aluno      = $reference->toArray();
                $dadosAluno = array_merge($dadosAluno, $aluno);
                $endereco   = $serviceEndereco->buscaEndereco($aluno['pes'], 'Residencial', false);

                if ($endereco) {
                    // die(var_dump($endereco));
                    $dadosAluno = array_merge($dadosAluno, $endereco);
                }

                $contatos = $serviceContato->buscaContatos($aluno['pes']);
                if ($contatos) {
                    $contatos   = $this->mapContato($contatos);
                    $dadosAluno = array_merge($dadosAluno, $contatos);
                }

                $alunoCurso = $serviceAlunoCurso->buscaAlunoCurso($aluno['alunoId']);

                if ($alunoCurso) {
                    $dadosAluno = array_merge($dadosAluno, $alunoCurso->toArray());
                }

                return $dadosAluno;
            }
        }

        return null;
    }

    /**
     * Muda arquivos de aluno de diretório
     */
    public function mudaArquivosDireotrio()
    {
        $arquivos = (new \GerenciadorArquivos\Service\Arquivo($this->getEm()))->findAll();

        foreach ($arquivos as $arquivo) {
            $diretorio = $arquivo->getDiretorio()->getArqDiretorioEndereco();
            $dirAntigo = $diretorio . "/../" . $arquivo->getArqChave();
            $dirNovo   = $diretorio . '/' . $arquivo->getArqChave();
            copy($dirAntigo, $dirNovo);
            unlink($dirAntigo);
        }
    }

    /**
     * Retorna matriculas de um aluno em períodos letivos
     * @param int $alunocursoId
     * @return array
     */
    public function periodosLetivosAluno($alunocursoId, $periodoCorrente = false)
    {
        $sql = '
        SELECT
            periodoAluno.alunoper_id,
            periodoLetivo.per_id,
            periodoLetivo.per_nome,
            periodoLetivo.per_ano,
            periodoLetivo.per_etapas,
            periodoLetivo.per_semestre,
            periodoLetivo.per_data_inicio,
            periodoLetivo.per_data_fim,
            periodoLetivo.per_matricula_inicio,
            periodoLetivo.per_matricula_fim,
            periodoLetivo.per_rematricula_online_inicio,
            periodoLetivo.per_rematricula_online_fim,
            periodoAluno.alunocurso_id,
            turma.turma_nome,
            situacao.situacao_id,
            situacao.matsit_descricao,
            situacao.situacao_regular,
            turma.turma_serie,
            IF(now() BETWEEN periodoLetivo.per_data_inicio AND periodoLetivo.per_data_fim, 1, 0) AS periodoAberto,
            IF(
                now() BETWEEN LEAST(
                    periodoLetivo.per_data_inicio,
                    COALESCE(periodoLetivo.per_matricula_inicio, periodoLetivo.per_data_inicio),
                    COALESCE(periodoLetivo.per_rematricula_online_inicio, periodoLetivo.per_data_inicio)
                ) AND GREATEST(
                    periodoLetivo.per_data_fim,
                    COALESCE(periodoLetivo.per_matricula_fim, periodoLetivo.per_data_fim),
                    COALESCE(periodoLetivo.per_rematricula_online_fim, periodoLetivo.per_data_fim)
                ) AND
                (
                    periodoLetivo.per_data_finalizacao IS NULL OR
                    now() <= periodoLetivo.per_data_finalizacao
                ),
                1,
                0
            ) AS periodoAbertoAlteraMatricula
        FROM acadperiodo__aluno periodoAluno
            INNER JOIN acadgeral__situacao situacao ON situacao.situacao_id=periodoAluno.matsituacao_id
            INNER JOIN acadperiodo__turma turma ON turma.turma_id = periodoAluno.turma_id
            INNER JOIN acadperiodo__letivo periodoLetivo ON periodoLetivo.per_id = turma.per_id
        WHERE alunocurso_id = :alunocurso_id';

        if ($periodoCorrente) {
            $dataAtual = new \DateTime();
            $dataAtual = $dataAtual->format("Y-m-d") . " 00:00:00";

            $sql .= ' AND "' . $dataAtual . '" BETWEEN per_data_inicio AND per_data_fim ';
            $sql .= ' ORDER BY per_id DESC';
        } else {
            $sql .= ' ORDER BY per_data_inicio DESC';
        }

        $parameters = array('alunocurso_id' => $alunocursoId);

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        if ($periodoCorrente) {
            return isset($result[0]['alunoper_id']) ?
                [
                    'alunoper'      => $result[0]['alunoper_id'],
                    'perDataInicio' => $result[0]['per_data_inicio'],
                    'perDataFim'    => $result[0]['per_data_fim'],
                    'alunoCurso_id' => $result[0]['alunocurso_id']
                ]
                : null;
        }

        return $result;
    }

    /**
     * Busca matrícula de aluno com situação de pré-matricula
     * @param int $matricula
     * @return \Matricula\Entity\AcadperiodoAluno
     */
    public function buscaAlunoRematricula($matricula)
    {
        if (!$matricula) {
            $this->setLastError("Nenhuma matrícula informada!");

            return false;
        }

        $query = "
        SELECT alunoper_id alunoPer
        FROM acadperiodo__aluno
        WHERE
            alunocurso_id = $matricula AND
            matsituacao_id = (
              SELECT situacao_id
              FROM acadgeral__situacao
              WHERE matsit_descricao = 'Pré Matricula'
            )
        ORDER BY alunoper_id DESC
        LIMIT 1";

        $aluno = $this->executeQuery($query)->fetch();

        if ($aluno) {
            return $this->getRepository()->find($aluno['alunoPer']);
        }

        return false;
    }

    public function buscaMatriculaAlunoPeriodo($PeriodoId, $alunocursoId)
    {
        $query        = "SELECT alunoper_id
                    FROM acadperiodo__aluno
                    INNER JOIN acadperiodo__turma
                      ON  acadperiodo__turma.turma_id = acadperiodo__aluno.turma_id
                 WHERE per_id = {$PeriodoId} and alunocurso_Id = {$alunocursoId} ";
        $alunoPeriodo = $this->executeQuery($query)->fetch();

        return $alunoPeriodo;
    }

    /**
     * Retorna array com informações das disciplinas que o aluno faz em um período
     *
     * @param $alunoperId
     * @return array|null
     */
    public function buscaNotasAlunoPorAlunoper($alunoperId)
    {
        $serviceAcadperiodoAlunoDisciplina = new \Matricula\Service\AcadperiodoAlunoDisciplina($this->getEm());
        $serviceAcadperiodoFrequencia      = new \Professor\Service\AcadperiodoFrequencia($this->getEm());
        $serviceAcadperiodoEtapas          = new \Professor\Service\AcadperiodoEtapas($this->getEm());
        $serviceCursoConfig                = new \Matricula\Service\AcadCursoConfig($this->getEm());
        $serviceAlunoResumo                = new \Matricula\Service\AcadperiodoAlunoResumo($this->getEm());

        /* @var $objAcadperiodoAluno \Matricula\Entity\AcadperiodoAluno */
        $objAcadperiodoAluno = $this->getRepository()->find($alunoperId);

        if (!$objAcadperiodoAluno) {
            $this->setLastError('Matrícula do aluno não encontrada no período letivo.');

            return false;
        }

        $objAcadperiodoLetivo          = $objAcadperiodoAluno->getTurma()->getPer();
        $arrAcadperiodoAlunoDisciplina = $serviceAcadperiodoAlunoDisciplina->disciplinasAlunoPeriodo(
            $alunoperId,
            \Matricula\Service\AcadgeralSituacao::situacoesAtividadeLeitura()
        );
        $arrAcadperiodoEtapas          = $serviceAcadperiodoEtapas->retornaEtapasPeriodoLetivo(
            $objAcadperiodoLetivo->getPerId()
        );

        /** @var \Matricula\Entity\AcadCursoConfig $objCursoConfig */
        $objCursoConfig = $serviceCursoConfig->retornaObjCursoConfig(
            $objAcadperiodoAluno->getTurma()->getMatCur()->getCurso()->getCursoId()
        );

        $arrDisciplinas = array();

        $arrCabecalho = array(
            ['nome' => 'Disciplina', 'campo' => 'discSigla'],
            //['nome' => 'Situação', 'campo' => 'situacaoDescricao'],
        );
        /* @var $objAcadperiodoEtapa \Professor\Entity\AcadperiodoEtapas */
        foreach ($arrAcadperiodoEtapas as $objAcadperiodoEtapa) {
            $arrCabecalho[] = [
                'nome'  => $objAcadperiodoEtapa->getEtapaDescricao(),
                'campo' => 'etapa' . $objAcadperiodoEtapa->getEtapaOrdem()
            ];
        }

        $arrCabecalho = array_merge(
            $arrCabecalho,
            array(
                ['nome' => 'Soma', 'campo' => 'soma'],
                ['nome' => 'Faltas', 'campo' => 'faltas'],
                ['nome' => 'E. final', 'campo' => 'final'],
                ['nome' => 'Resultado', 'campo' => 'resultado'],
                ['nome' => 'Situação', 'campo' => 'situacaoFinal'],
            )
        );

        /* @var $objAcadperiodoAlunoDisciplina \Matricula\Entity\AcadperiodoAlunoDisciplina */
        foreach ($arrAcadperiodoAlunoDisciplina as $i => $objAcadperiodoAlunoDisciplina) {
            $objAcadgeralDisciplina = $objAcadperiodoAlunoDisciplina->getDisc();
            $objAcadperiodoTurma    = $objAcadperiodoAlunoDisciplina->getTurma();
            $objAcadgeralAlunoCurso = $objAcadperiodoAlunoDisciplina->getAlunoper()->getAlunocurso();
            $objAcadgeralSituacao   = $objAcadperiodoAlunoDisciplina->getSituacao();

            $arrDisciplina = array(
                'discId'            => $objAcadgeralDisciplina->getDiscId(),
                'discNome'          => $objAcadgeralDisciplina->getDiscNome(),
                'discSigla'         => $objAcadgeralDisciplina->getDiscSigla(),
                'situacaoId'        => $objAcadgeralSituacao->getSituacaoId(),
                'situacaoDescricao' => $objAcadgeralSituacao->getMatsitDescricao(),
                'turmaId'           => $objAcadperiodoTurma->getTurmaId(),
                'turmaNome'         => $objAcadperiodoTurma->getTurmaNome(),
            );

            $arrAcadperiodoEtapaAluno = $this
                ->getRepository('\Professor\Entity\AcadperiodoEtapaAluno')
                ->findBy(
                    [
                        'alunodisc' => $objAcadperiodoAlunoDisciplina->getAlunodiscId()
                    ],
                    ['diario' => 'desc']
                );

            $notaTotal           = 0;
            $notaFinalLancada    = '-';
            $notaResumoCalculada = '-';
            $faltas              = 0;
            $situacaoFinal       = '-';

            /* @var $objAcadperiodoEtapa \Professor\Entity\AcadperiodoEtapas */
            foreach ($arrAcadperiodoEtapas as $objAcadperiodoEtapa) {
                $nota = '';

                /* @var $objAcadperiodoEtapaAluno \Professor\Entity\AcadperiodoEtapaAluno */
                foreach ($arrAcadperiodoEtapaAluno as $objAcadperiodoEtapaAluno) {
                    if ($objAcadperiodoEtapaAluno->getDiario()->getEtapa() == $objAcadperiodoEtapa) {
                        $nota = $objAcadperiodoEtapaAluno->getAlunoetapaNota();
                        $notaTotal += $nota;
                        break;
                    }
                }

                $nota = number_format($nota, $objCursoConfig->getCursoconfigNotaFracionada());

                $arrDisciplina['etapa' . $objAcadperiodoEtapa->getEtapaOrdem()] = $nota;
            }

            if (!$objCursoConfig) {
                $this->setLastError("Não foi possível localizar a configuração do curso!");

                return false;
            }

            $arrDisciplina['soma'] = $notaTotal;

            if ($objCursoConfig->getCursoconfigMetodo() == $serviceCursoConfig::METODO_DESEMPENHO_CALCULO_POR_MEDIA) {
                $numeroEtapas          = $objAcadperiodoTurma->getPer()->getPerEtapas();
                $arrDisciplina['soma'] = number_format(($arrDisciplina['soma'] / $numeroEtapas), 2, ",", "");
            }

            /* @var $objAcadperiodoAlunoResumo \Matricula\Entity\AcadperiodoAlunoResumo */
            $objAcadperiodoAlunoResumo = $this
                ->getRepository('Matricula\Entity\AcadperiodoAlunoResumo')
                ->findOneBy(
                    array(
                        'alunocursoId' => $objAcadgeralAlunoCurso->getAlunocursoId(),
                        'discId'       => $objAcadgeralDisciplina->getDiscId(),
                        'alunoper'     => $alunoperId
                    ),
                    array('resId' => 'DESC')
                );

            /* @var $objAcadperiodoAlunoDisciplinaFinal \Matricula\Entity\AcadperiodoAlunoDisciplinaFinal */
            $objAcadperiodoAlunoDisciplinaFinal = $this
                ->getRepository('Matricula\Entity\AcadperiodoAlunoDisciplinaFinal')
                ->findOneBy(
                    array('alunodisc' => $objAcadperiodoAlunoDisciplina->getAlunodiscId()),
                    array('alunofinalId' => 'DESC')
                );

            if ($objAcadperiodoAlunoResumo) {
                $notaResumoCalculada = $objAcadperiodoAlunoResumo->getResalunoNota();
                $situacaoFinal       = $objAcadperiodoAlunoResumo->getResalunoSituacao();
                $faltas              = $objAcadperiodoAlunoResumo->getResalunoFaltas();
            } else {
                $faltas = $serviceAcadperiodoFrequencia->buscaNumeroFaltasAluno(
                    $objAcadperiodoAlunoDisciplina->getAlunodiscId()
                );
            }

            /* @var $objAcadperiodoAlunoDisciplinaFinal \Matricula\Entity\AcadperiodoAlunoDisciplinaFinal */
            if ($objAcadperiodoAlunoDisciplinaFinal) {
                $notaFinalLancada     = $objAcadperiodoAlunoDisciplinaFinal->getAlunofinalNota();
                $situacaoDeReprovacao = ($situacaoFinal == $serviceAlunoResumo::RESALUNO_SITUACAO_REPROVADO);

                if ($situacaoDeReprovacao && !$objAcadperiodoAlunoDisciplinaFinal->getAlunofinalNota()) {
                    $situacaoFinal = $serviceAlunoResumo::RESALUNO_SITUACAO_EFINAL;
                }
            }

            $arrDisciplina['faltas']        = $faltas;
            $arrDisciplina['final']         = $notaFinalLancada;
            $arrDisciplina['resultado']     = $notaResumoCalculada;
            $arrDisciplina['situacaoFinal'] = $situacaoFinal;

            $arrDisciplinas[] = $arrDisciplina;
        }

        return array(
            /* Estamos passandos esses parametros para se necessário usarmos futuramente*/
            //'metodocalculoNota'                    => $objCursoConfig->getCursoconfigMetodo(),
            'ConstantemetodocalculoNotaMedia'      => $serviceCursoConfig::METODO_DESEMPENHO_CALCULO_POR_MEDIA,
            //'metodocalculoNotaFinal'               => $objCursoConfig->getCursoconfigMetodoFinal(),
            'ConstantemetodocalculoNotaFinalMedia' => $serviceCursoConfig::METODO_FINAL_POR_MEDIA,
            'alunoperId'                           => $objAcadperiodoAluno->getAlunoperId(),
            'alunoDesligado'                       => $this->verificaSeAlunnoEstaCancelado($objAcadperiodoAluno),
            'cabecalho'                            => $arrCabecalho,
            'dados'                                => $arrDisciplinas
        );
    }

    public function verificaSeAlunnoEstaCancelado(\Matricula\Entity\AcadperiodoAluno $aluno)
    {
        $serviceSituacoes = new \Matricula\Service\AcadgeralSituacao($this->getEm());

        if ($aluno->getMatsituacao()->getSituacaoId() == $serviceSituacoes::CANCELADA) {
            return true;
        }
        if ($aluno->getMatsituacao()->getSituacaoId() == $serviceSituacoes::TRANCADO) {
            return true;
        }
        if ($aluno->getMatsituacao()->getSituacaoId() == $serviceSituacoes::TRANSFERENCIA) {
            return true;
        }
        if ($aluno->getMatsituacao()->getSituacaoId() == $serviceSituacoes::OBITO) {
            return true;
        }
        if ($aluno->getMatsituacao()->getSituacaoId() == $serviceSituacoes::DESISTENTE) {
            return true;
        }

        return false;
    }

    /**
     * Verifica se o aluno está matriculado em um determinado periodo letivo
     *
     * @param int $matricula
     * @param int $periodo = false
     * @return bool
     */
    public function verificaSeAlunoPossuiDisciplinaAtivaEmPeriodo($matricula, $periodo = false)
    {
        if (!$periodo) {
            $servicePerLetivo = new \Matricula\Service\AcadperiodoLetivo($this->getEm());
            $periodoAtual     = $servicePerLetivo->buscaPeriodoCorrente();
            $periodo          = $periodoAtual->getPerId();
        }

        $serviceSituacao = new \Matricula\Service\AcadgeralSituacao($this->getEm());
        $arrSituacao     = $serviceSituacao::situacoesAtividade();

        $query = "
        SELECT count(situacao_id) AS quantidadeDisciplinas
        FROM acadperiodo__aluno
        LEFT JOIN acadperiodo__aluno_disciplina USING (alunoper_id)
        LEFT JOIN acadperiodo__turma ON acadperiodo__turma.turma_id = acadperiodo__aluno.turma_id
        LEFT JOIN acadperiodo__letivo USING (per_id)
        WHERE
            per_id = :periodo AND
            alunocurso_id = :matricula AND
            situacao_id IN (:arrSituacao)";

        $params = [
            'periodo'     => $periodo,
            'matricula'   => $matricula,
            'arrSituacao' => $arrSituacao
        ];

        $arrDisc = $this->executeQueryWithParam($query, $params)->fetch();

        //Retorno false se o número de disciplinas for igual a zero
        return $arrDisc['quantidadeDisciplinas'] != 0;
    }

    public function buscaPeriodosExistentes($params)
    {
        $query = " SELECT per_nome,per_id FROM acadperiodo__letivo";

        if ($params['q']) {
            $perid = $params['q'];
        } elseif ($params['query']) {
            $perid = $params['query'];
        }

        if ($perid) {
            $query .= ' where per_id = ' . $perid;
        }

        return $this->executeQuery($query)->fetchAll();
    }

    public function alterarSituacaoQuandoMatriculaProvisoria(\Matricula\Entity\AcadgeralAlunoCurso $objAlunoCurso)
    {
        if (!$objAlunoCurso) {
            $this->setLastError("Por favor informe o curso do aluno!");

            return false;
        }

        $serviceGeralSituacao             = new \Matricula\Service\AcadgeralSituacao($this->getEm());
        $serviceAlunoResumo               = new \Matricula\Service\AcadperiodoAlunoResumo($this->getEm());
        $serviceAlunoDisciplina           = new \Matricula\Service\AcadperiodoAlunoDisciplina($this->getEm());
        $serviceCursoConfig               = new \Matricula\Service\AcadCursoConfig($this->getEm());
        $serviceAcessoPessoas             = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceAlunoDisciplinaHistorico  = new \Matricula\Service\AcadperiodoAlunoDisciplinaHistorico($this->getEm());
        $serviceSisIntegracao             = new \Sistema\Service\SisIntegracao($this->getEm());
        $serviceAlunoCurso                = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
        $serviceAluno                     = new \Matricula\Service\AcadgeralAluno($this->getEm());
        $serviceAcadperiodoAlunoHistorico = new \Matricula\Service\AcadperiodoAlunoHistorico($this->getEm());
        $situacaoCursoDeferido            = \Matricula\Service\AcadgeralAlunoCurso::ALUNOCURSO_SITUACAO_DEFERIDO;

        if (in_array($objAlunoCurso->getAlunocursoSituacao(), $serviceAlunoCurso->getAlunocursoSituacaoPendente())) {
            $objAlunoCurso->setAlunocursoSituacao($situacaoCursoDeferido);
            $this->getEm()->persist($objAlunoCurso);
            $this->getEm()->flush($objAlunoCurso);

            if (!$serviceAluno->efetuaIntegracaoAluno($objAlunoCurso->getAluno())) {
                $this->setLastError($this->getLastError() . "\n" . $serviceAluno->getLastError());

                return false;
            }
        }

        /** @var \Matricula\Entity\AcadperiodoAluno $objPeriodoAluno */
        $objPeriodoAluno = $this->buscaAlunoperiodoPorMatricula($objAlunoCurso->getAlunocursoId());

        if (!$objPeriodoAluno) {
            return true;
        }

        /** @var  $objAcadPeriodoAlunoHistorico \Matricula\Entity\AcadperiodoAluno */
        $objAcadPeriodoAlunoHistorico = $this->getRepository()->find($objPeriodoAluno->getAlunoperId());

        if ($objAcadPeriodoAlunoHistorico) {
            $objAcadPeriodoAlunoHistorico = clone ($objAcadPeriodoAlunoHistorico);
        }

        $objCursoConfig = $serviceCursoConfig->retornaObjCursoConfig(
            $objAlunoCurso->getCursocampus()->getCurso()->getCursoId()
        );

        if (!$objCursoConfig) {
            $this->setLastError("O curso do aluno não possui configuração definida!");

            return false;
        }

        $situacao = $serviceGeralSituacao::PENDENCIAS_ACADEMICAS;

        $arrSituacoesAlterar = [
            $serviceGeralSituacao::PRE_MATRICULA,
            $serviceGeralSituacao::MATRICULA_PROVISORIA,
            $serviceGeralSituacao::PENDENCIAS_ACADEMICAS,
        ];

        $numeroPendenciasAcademicas = $serviceAlunoResumo->retornaNumeroDeReprovacoesNaoCumpridas(
            $objAlunoCurso->getAlunocursoId()
        );

        $possuiMuitasPendencias = (
            $numeroPendenciasAcademicas >= $objCursoConfig->getCursoconfigNumeroMaximoPendencias()
        );

        $periodoEmVigencia    = $objPeriodoAluno->getTurma()->getPer()->verificarVigencia();
        $turmaPrimeiroPeriodo = ($objPeriodoAluno->getTurma()->getTurmaSerie() == 1);

        if ((!$possuiMuitasPendencias && $periodoEmVigencia) || $turmaPrimeiroPeriodo) {
            $situacao = $serviceGeralSituacao::MATRICULADO;
        }

        $objSituacaoPeriodo = $objPeriodoAluno->getMatsituacao()->getSituacaoId();
        $objAcessoPessoas   = $serviceAcessoPessoas->retornaUsuarioLogado();

        //Atualiza situação de aluno com situação provisória
        if (in_array($objSituacaoPeriodo, $arrSituacoesAlterar)) {
            try {
                $this->getEm()->beginTransaction();

                $objSituacao = $serviceGeralSituacao->getRepository()->find($situacao);

                $objPeriodoAluno->setMatsituacao($objSituacao);
                $objPeriodoAluno->setAlunoperSitData(new \DateTime());

                $this->getEm()->persist($objPeriodoAluno);
                $this->getEm()->flush($objPeriodoAluno);

                $arrObjAlunoDisciplina = $serviceAlunoDisciplina->disciplinasAlunoPeriodo(
                    $objPeriodoAluno->getAlunoperId()
                );

                /** @var \Matricula\Entity\AcadperiodoAlunoDisciplina $objAlunoDisciplina */
                foreach ($arrObjAlunoDisciplina as $objAlunoDisciplina) {
                    $objSituacaoDisciplina = $objAlunoDisciplina->getSituacao()->getSituacaoId();

                    //Atualiza situação de disciplinas de aluno com situação provisória
                    if (in_array($objSituacaoDisciplina, $arrSituacoesAlterar)) {
                        $discHistorico = [
                            'alunodisc'        => $objAlunoDisciplina->getAlunodiscId(),
                            'turma'            => $objAlunoDisciplina->getTurma(),
                            'usuarioHistorico' => $objAlunoDisciplina->getUsuarioCriador(),
                            'situacao'         => $objAlunoDisciplina->getSituacao()->getSituacaoId(),
                            'situacaodiscData' => $objAlunoDisciplina->getAlunodiscSitData(),
                        ];

                        $retornoDisciplinaHistorico = $serviceAlunoDisciplinaHistorico->save($discHistorico);

                        // salva historico do vinculo da disciplina
                        if (!is_object($retornoDisciplinaHistorico)) {
                            $this->setLastError("Não foi possível salvar histórico da disciplina do aluno! ");

                            return false;
                        }

                        $objAlunoDisciplina->setSituacao($objSituacao);
                        $objAlunoDisciplina->setUsuarioCriador($objAcessoPessoas);
                        $objAlunoDisciplina->setAlunodiscSitData(new \DateTime());

                        $this->getEm()->persist($objAlunoDisciplina);
                        $this->getEm()->flush($objAlunoDisciplina);
                    }
                }

                $situacaoDiferente = (
                    $objPeriodoAluno->getMatsituacao()->getSituacaoId() <>
                    $objAcadPeriodoAlunoHistorico->getMatsituacao()->getSituacaoId()
                );

                $turmaDiferente = (
                    $objPeriodoAluno->getTurma()->getTurmaId() <>
                    $objAcadPeriodoAlunoHistorico->getTurma()->getTurmaId()
                );

                if ($situacaoDiferente || $turmaDiferente) {
                    $ok = $serviceAcadperiodoAlunoHistorico->save($objAcadPeriodoAlunoHistorico);

                    if (is_array($ok)) {
                        $this->setLastError('Não foi possivel salvar os dados do histórico do Aluno');

                        return false;
                    }
                }

                $this->getEm()->persist($objPeriodoAluno);
                $this->getEm()->flush($objPeriodoAluno);

                $this->getEm()->commit();

                if ($objAlunoCurso->verificaSituacaoPendente()) {
                    if (!$serviceAlunoCurso->alteraSituacaoAluno($objAlunoCurso, $situacaoCursoDeferido)) {
                        $this->setLastError($serviceAlunoCurso->getLastError());

                        return false;
                    }
                }

                $serviceSisIntegracao->integracaoInteligenteParaAluno($objAlunoCurso->getAluno());
            } catch (\Exception $e) {
                $this->setLastError("Não foi possível alterar a situação do curso do aluno! " . $e->getMessage());

                return false;
            }
        }

        return true;
    }

    /**
     * @param integer $alunocursoId
     * @return \Matricula\Entity\AcadperiodoAluno $objAlunoPer
     */
    public
    function retornaAlunoPerPoralunocursoId(
        $alunocursoId
    ) {
        if (!$alunocursoId) {
            return null;
        }

        $servicePeriodo = new \Matricula\Service\AcadperiodoLetivo($this->getEm());
        /** @var \Matricula\Entity\AcadperiodoLetivo $objPeriodo */
        $objPeriodo = $servicePeriodo->buscaPeriodoCorrente();

        $objQuery = $this->getEm()->createQuery(
            "SELECT alunoPer
        FROM \Matricula\Entity\AcadperiodoAluno alunoPer
        INNER JOIN \Matricula\Entity\AcadperiodoTurma turma WITH turma.turmaId = alunoPer.turma
        WHERE alunoPer.alunocurso = :alunocurso AND turma.per = :perId"
        )->setParameters(
            [
                'perId'      => $objPeriodo->getPerId(),
                'alunocurso' => $alunocursoId
            ]
        );

        try {
            $objAlunoPer = $objQuery->getResult()[0];

            return $objAlunoPer;
        } catch (\Exception $e) {
            $this->setLastError("Não foi possível obter o alunoPer. " . $e->getMessage());
        }

        return false;
    }

    public function retornaFiltrosParaConsultadeAlunoPeriodo($param)
    {
        $conditions             = ['1 = 1'];
        $trazerAlunosSemPeriodo = ($param['trazerAlunosSemPeriodo'] && !$param['per_id'] && !$param['turma_id']);

        if (!$trazerAlunosSemPeriodo) {
            $conditions[] = 'curso_possui_periodo_letivo = "Sim" ';

            if (!$param['desligados'] AND $param['per_id'] != "null") {
                $conditions[] = 'alunocurso_situacao in("Deferido","Pendente") ';
            }
        }

        unset($param['trazerAlunosSemPeriodo']);

        foreach ($param as $filtro => $valor) {
            if ($valor != 'null' && is_array($valor)) {
                $valor = implode(',', $valor);
            }

            switch ($filtro) {
                case "per_id":
                    if ($valor != 'null' && $valor != 'egresso') {
                        $conditions[] = "acadperiodo__letivo.per_id in (" . $valor . ")";
                    } elseif ($valor == 'null') {
                        $conditions[] =
                            ' acadperiodo__letivo.per_id IS NULL ';
                    } elseif ($valor == 'egresso') {
                        $conditions[] =
                            '  acadperiodo__aluno.alunocurso_id NOT IN (
                        SELECT alunocurso_id FROM acadperiodo__aluno
                        INNER JOIN acadperiodo__turma USING(turma_id)
                        INNER JOIN acadperiodo__letivo letivo USING(per_id)
                        WHERE NOW() BETWEEN letivo.per_data_inicio AND letivo.per_data_fim ) 
                        AND alunocurso_situacao NOT IN ("Cancelado","Indeferido","Concluido","Aguardando Pagamento") ';
                    } else {
                        $conditions[] = ' acadperiodo__letivo.per_id is null ';
                    }

                    break;
                case "turma_id":
                    $conditions[] = "acadperiodo__turma.turma_id in (" . $valor . ")";
                    break;
                case "desligados":
                    break;
                default:
                    $conditions[] = $filtro . " in (" . $valor . ")";
                    break;
            }
        }

        return $conditions;
    }

    public static function getAlunoperiodoSituacao()
    {
        return array(
            'Matriculado'           => self::ALUNOPERIODO_SITUACAO_MATRICULADO,
            'Trancado'              => self::ALUNOPERIODO_SITUACAO_TRANCADO,
            'Dispensado'            => self::ALUNOPERIODO_SITUACAO_DISPENSADO,
            'Transferência'         => self::ALUNOPERIODO_SITUACAO_TRANSFERENCIA,
            'Cancelada'             => self::ALUNOPERIODO_SITUACAO_CANCELADA,
            'Pré Matrícula'         => self::ALUNOPERIODO_SITUACAO_PRE_MATRICULA,
            'Adaptante'             => self::ALUNOPERIODO_SITUACAO_ADAPTANTE,
            'Dependente'            => self::ALUNOPERIODO_SITUACAO_DEPENDENTE,
            'Matrícula Provisória'  => self::ALUNOPERIODO_SITUACAO_MATRICULA_PROVISORIA,
            'Concluído'             => self::ALUNOPERIODO_SITUACAO_CONCLUIDO,
            'Monografia'            => self::ALUNOPERIODO_SITUACAO_MONOGRAFIA,
            'Desistente'            => self::ALUNOPERIODO_SITUACAO_DESISTENTE,
            'Pendências Acadêmicas' => self::ALUNOPERIODO_SITUACAO_PENDENCIAS_ACADEMICAS,
            'Óbito'                 => self::ALUNOPERIODO_SITUACAO_OBITO
        );
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2AlunoperiodoSituacao()
    {
        $array = array();

        foreach ($this->getAlunoperiodoSituacao() as $key => $value) {
            $array[] = array(
                'text' => $key,
                'id'   => $value
            );
        }

        return $array;
    }

    public function salvarAlunoPeriodo(&$arrDados = array())
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceAcessoPessoas = new AcessoPessoas($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['alunoperId']) {
                $objAlunoper = $this->getRepository()->find($arrDados['alunoperId']);

                if (!$objAlunoper) {
                    $this->setLastError("Registro de aluno no período não encontrado.");

                    return false;
                }
            } else {
                $objAlunoper = new \Matricula\Entity\AcadperiodoAluno(array());
            }

            if ($arrDados['alunocursoId']) {
                /** @var \Matricula\Entity\AcadgeralAlunoCurso $objAlunocurso */
                $objAlunocurso = $this->getRepository('Matricula\Entity\AcadgeralAlunoCurso')->find(
                    $arrDados['alunocursoId']
                );

                if (!$objAlunocurso) {
                    $this->setLastError("Registro de aluno no curso não encontrado!");

                    return false;
                }

                $objAlunoper->setAlunocurso($objAlunocurso);
            } else {
                $objAlunoper->setAlunocurso(null);
            }

            if ($arrDados['turmaId']) {
                /** @var \Matricula\Entity\AcadperiodoTurma $objTurma */
                $objTurma = $this->getRepository('Matricula\entity\AcadperiodoTurma')->find($arrDados['turmaId']);

                if (!$objTurma) {
                    $this->setLastError("Registro de turma não encontrado!");

                    return false;
                }

                $objAlunoper->setTurma($objTurma);
            } else {
                $objAlunoper->setTurma(null);
            }

            if ($arrDados['situacaoId']) {
                /** @var \Matricula\Entity\AcadgeralSituacao $objMatsituacao */
                $objMatsituacao = $this->getRepository('Matricula\Entity\AcadgeralSituacao')->find(
                    $arrDados['situacaoId']
                );

                if (!$objMatsituacao) {
                    $this->setLastError("Registro de situação não encontrado!");

                    return false;
                }

                $objAlunoper->setMatsituacao($objMatsituacao);
            } else {
                $objAlunoper->setMatsituacao(null);
            }

            if ($arrDados['usuarioCriador']) {
                /** @var \Acesso\Entity\AcessoPessoas $objAcessoPessoas */
                $objAcessoPessoas = $this->getRepository('Acesso\Entity\AcessoPessoas')->find(
                    $arrDados['usuarioCriador']
                );

                if (!$objAcessoPessoas) {
                    $this->setLastError("Registro de usuário não encontrado!");

                    return false;
                }

                $objAlunoper->setUsuarioCriador($objAcessoPessoas);
            } else {
                $objAcessoPessoas = $serviceAcessoPessoas->retornaUsuarioLogado();
                $objAlunoper->setUsuarioCriador($objAcessoPessoas);
            }

            if ($arrDados['alunoperSitData']) {
                $objAlunoper->setAlunoperSitData($arrDados['alunoperSitData']);
            } else {
                $objAlunoper->setAlunoperSitData(new \DateTime());
            }

            $objAlunoper->setAlunoperObservacoes($arrDados['alunoperObservacoes']);

            $this->getEm()->persist($objAlunoper);
            $this->getEm()->flush($objAlunoper);

            $this->getEm()->commit();

            $arrDados['alunoperId'] = $objAlunoper->getAlunoperId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError(
                "Não foi possível salvar o registro de matrícula o aluno no período. " . $e->getMessage()
            );
        }

        return false;
    }

    public function retornaDisciplinasQueSeraoCursadas($novaTurmaId)
    {
        if (!$novaTurmaId) {
            return false;
        }

        $serviceMatrizDisciplina = new \Matricula\Service\AcadperiodoMatrizDisciplina($this->getEm());

        /** @var \Matricula\Entity\AcadperiodoTurma $objTurma */
        $objTurma = $this->getRepository('Matricula\Entity\AcadperiodoTurma')->find($novaTurmaId);

        if ($objTurma) {
            $arrDisciplinas = $serviceMatrizDisciplina->getRepository()->buscaDisciplinasPorMatrizCurricular(
                $objTurma->getMatCur()->getMatCurId(),
                $objTurma->getTurmaSerie()
            );

            $arrDisciplinas['turmaOrdem'] = $objTurma->getTurmaOrdem();
            $arrDisciplinas['turmaTurno'] = $objTurma->getTurmaTurno();
            $arrDisciplinas['turmaNome']  = $objTurma->getTurmaNome();
            $arrDisciplinas['turmaSerie'] = $objTurma->getTurmaSerie();

            return $arrDisciplinas;
        }

        return false;
    }

    public function alteraSituacaoAlunoPeriodo($param)
    {
        $serviceSituacao        = new \Matricula\Service\AcadgeralSituacao($this->getEm());
        $serviceAlunoCurso      = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
        $serviceAluno           = new \Matricula\Service\AcadgeralAluno($this->getEm());
        $serviceAlunoHistorico  = new \Matricula\Service\AcadperiodoAlunoHistorico($this->getEm());
        $serviceDisciplinaAluno = new \Matricula\Service\AcadperiodoAlunoDisciplina($this->getEm());

        $situacoesPermitidas = $serviceSituacao->retornaSituacoesPossiveisAlteracao();
        $alteraDisciplinas   = true;

        try {
            if (!$param) {
                $this->setLastError("Nenhum dado informado!");

                return false;
            }

            /** @var \Matricula\Entity\AcadperiodoAluno $objAlunoPer */
            if (!$objAlunoPer = $this->getRepository()->find($param['alunoperId'])) {
                $this->setLastError("Aluno Inválido ou sem período Letivo!");

                return false;
            }

            if (!$this->validaAlteracaoSituacao($objAlunoPer->getTurma()->getPer()->getPerId())) {
                return false;
            }

            $objAlunoHistorico = clone($objAlunoPer);

            /** @var \Matricula\Entity\AcadgeralSituacao $objSituacao */
            if (!$objSituacao = $serviceSituacao->getRepository()->find($param['situacaoAluno'])) {
                $this->setLastError("Nova situação inválida ou não localizada!");

                return false;
            }

            if (!in_array($objSituacao->getSituacaoId(), $situacoesPermitidas)) {
                $this->setLastError("Situação não permtida!");

                return false;
            }

            if (!$param['alteracaoObservacao']) {
                $this->setLastError("Campo de observação obrigatorio!");

                return false;
            }

            if ($objSituacao == $objAlunoPer->getMatsituacao()) {
                $this->setLastError("Selecione uma situação diferente da situação atual!");

                return false;
            }

            $dataAtual = new \DateTime();

            $this->begin();

            $observacaoAluno = $param['alteracaoObservacao'];

            $objAlunoPer->setAlunoperObservacoes($observacaoAluno);
            $objAlunoPer->setMatsituacao($objSituacao);

            $this->getEm()->persist($objAlunoPer);
            $this->getEm()->flush($objAlunoPer);

            $situacaoAlunoCurso = $objAlunoPer->getAlunocurso()->getAlunocursoSituacao();

            if (in_array($situacaoAlunoCurso, $serviceAlunoCurso->retornaSituacoesDesligamentoPedente())) {
                $alterarSituacaoCurso = $serviceAlunoCurso->alteraSituacaoAluno(
                    $objAlunoPer->getAlunocurso(),
                    $serviceAlunoCurso::ALUNOCURSO_SITUACAO_DEFERIDO
                );

                if (!$alterarSituacaoCurso) {
                    $this->setLastError($this->getLastError() . "\n" . $serviceAlunoCurso->getLastError());

                    return false;
                }
            }

            $objAlunoHistorico->setAlunoperObservacoes(
                ($objAlunoPer->getAlunoperObservacoes() ? $objAlunoPer->getAlunoperObservacoes() . "\n"
                    : $objAlunoPer->getAlunoperObservacoes()) .
                ' Alteração da situação do aluno de ' . $objAlunoHistorico->getMatsituacao()->getMatsitDescricao() .
                ' para ' . $objSituacao->getMatsitDescricao() . '. Data: ' . $dataAtual->format('d/m/Y H:i:s')
            );

            if (!$serviceAlunoHistorico->save($objAlunoHistorico)) {
                $this->setLastError($this->getLastError() . "\n" . $serviceAlunoHistorico->getLastError());

                return false;
            }

            $arrObjDisciplinas = $serviceDisciplinaAluno->getRepository()->findBy(
                ['alunoper' => $objAlunoPer->getAlunoperId(), 'turma' => $objAlunoPer->getTurma()->getTurmaId()]
            );

            if ($arrObjDisciplinas) {
                $arrObjDisciplinasHistorico = [];

                /** @var $objDisciplina \Matricula\Entity\AcadperiodoAlunoDisciplina */
                foreach ($arrObjDisciplinas as $index => $objDisciplina) {
                    $objDisciplinaHistorico = clone($objDisciplina);
                    $alterado               = false;

                    if (in_array($objDisciplina->getSituacao()->getSituacaoId(), $situacoesPermitidas)) {
                        $objDisciplina->setSituacao($objSituacao);

                        $alterado = true;
                    }

                    if ($objSituacao->getSituacaoId() == $serviceSituacao::ERRO_MATRICULA) {
                        $objDisciplina->setSituacao($objSituacao);

                        $alterado = true;
                    }

                    if (!$alterado) {
                        unset($arrObjDisciplinas[$index]);
                        continue;
                    }

                    $observacaoDisciplinaHistorico = (
                        ' Alteração da situação do aluno de ' .
                        $objDisciplinaHistorico->getSituacao()->getMatsitDescricao() .
                        ' para ' . $objSituacao->getMatsitDescricao() . '. ' .
                        'Data: ' . $dataAtual->format('d/m/Y H:i:s')
                    );

                    $arrObjDisciplinasHistorico[$index] = array_merge(
                        $objDisciplinaHistorico->toArray(),
                        ['SituacaodiscObservacoes' => $observacaoDisciplinaHistorico]
                    );
                    $arrObjDisciplinas[$index]          = $objDisciplina;
                }

                if (!empty($arrObjDisciplinas)) {
                    $alteraDisciplinas = $serviceDisciplinaAluno->saveMultiplosObjetos(
                        $arrObjDisciplinas,
                        $arrObjDisciplinasHistorico
                    );
                }
            }

            if (!$serviceAluno->efetuaIntegracaoAluno($objAlunoPer->getAlunocurso()->getAluno())) {
                $this->setLastError($this->getLastError() . "\n" . $serviceAluno->getLastError());

                return false;
            }

            if (!$alteraDisciplinas) {
                $this->setLastError($this->getLastError() . "\n" . $serviceDisciplinaAluno->getLastError());

                return false;
            }

            $this->commit();
        } catch (\Exception $ex) {
            return false;
        }

        return true;
    }

    /**
     * @param $alunocursoId
     * @return array
     */
    public function retornaIntervaloDatasPeriodosAluno($alunocursoId)
    {
        $query = '
        SELECT
        *
        FROM (
            (
                SELECT
                    date_format(per_data_inicio,"%d/%m/%Y") AS dataInicio,
                    date_format(per_data_fim,"%d/%m/%Y") AS dataFim,
                    per_nome AS perNome,
                    per_ano AS perAno,
                    date_format(per_data_vencimento_inicial,"%d/%m/%Y") AS perVencimentoInicial,
                    "primeiroVinculo" AS descricaoIntervalo
                FROM acadperiodo__aluno
                INNER JOIN acadperiodo__turma ON acadperiodo__aluno.turma_id = acadperiodo__turma.turma_id
                INNER JOIN acadperiodo__letivo ON acadperiodo__turma.per_id = acadperiodo__letivo.per_id
                WHERE alunocurso_id = :alunocursoId
                ORDER BY alunoper_id ASC
                LIMIT 1
            )
            UNION ALL
            (
                SELECT
                    date_format(per_data_inicio,"%d/%m/%Y") AS dataInicio,
                    date_format(per_data_fim,"%d/%m/%Y") AS dataFim,
                    per_nome AS perNome,
                    per_ano AS perAno,
                    date_format(per_data_vencimento_inicial,"%d/%m/%Y") AS perVencimentoInicial,
                    "primeiroVinculoAtividade" AS descricaoIntervalo
                FROM acadperiodo__aluno
                INNER JOIN acadperiodo__turma ON acadperiodo__aluno.turma_id = acadperiodo__turma.turma_id
                INNER JOIN acadperiodo__letivo ON acadperiodo__turma.per_id = acadperiodo__letivo.per_id
                WHERE alunocurso_id = :alunocursoId AND matsituacao_id IN(:situacao)
                ORDER BY alunoper_id ASC
                LIMIT 1
            )
            UNION ALL
            (
                SELECT
                    date_format(per_data_inicio,"%d/%m/%Y") AS dataInicio,
                    date_format(per_data_fim,"%d/%m/%Y") AS dataFim,
                    per_nome AS perNome,
                    per_ano AS perAno,
                    date_format(per_data_vencimento_inicial,"%d/%m/%Y") AS perVencimentoInicial,
                    "ultimoVinculoAtividade" AS descricaoIntervalo
                FROM acadperiodo__aluno
                INNER JOIN acadperiodo__turma ON acadperiodo__aluno.turma_id = acadperiodo__turma.turma_id
                INNER JOIN acadperiodo__letivo ON acadperiodo__turma.per_id = acadperiodo__letivo.per_id
                WHERE alunocurso_id = :alunocursoId AND matsituacao_id IN(:situacao)
                    AND per_data_fim < NOW()
                ORDER BY alunoper_id DESC
                LIMIT 1
            )
            UNION ALL
            (
                SELECT
                    date_format(per_data_inicio,"%d/%m/%Y") AS dataInicio,
                    date_format(per_data_fim,"%d/%m/%Y") AS dataFim,
                    per_nome AS perNome,
                    per_ano AS perAno,
                    date_format(per_data_vencimento_inicial,"%d/%m/%Y") AS perVencimentoInicial,
                    "periodoCorrente" AS descricaoIntervalo
                FROM acadperiodo__aluno
                INNER JOIN acadperiodo__turma ON acadperiodo__aluno.turma_id = acadperiodo__turma.turma_id
                INNER JOIN acadperiodo__letivo ON acadperiodo__turma.per_id = acadperiodo__letivo.per_id
                WHERE alunocurso_id = :alunocursoId AND matsituacao_id IN(:situacao)
                    AND NOW() BETWEEN per_data_inicio AND per_data_fim
                ORDER BY alunoper_id DESC
                LIMIT 1
            )
        ) AS X';

        $arrParam = [
            'alunocursoId' => $alunocursoId,
            'situacao'     => \Matricula\Service\AcadgeralSituacao::situacoesAtividade()
        ];

        $result = $this->executeQueryWithParam($query, $arrParam)->fetchAll();

        $arrRetorno = [];

        foreach ($result as $linha) {
            $arrRetorno[$linha['descricaoIntervalo']] = $linha;
        }

        return $arrRetorno;
    }

    public function retornaConsultaAcademicaAluno($arrParam)
    {
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());

        if (isset($arrParam['per_id'])) {
            $perId = $arrParam['per_id'];

            if ($perId == "desligados" || $perId == "null") {
                if (isset($arrParam['situacao_id'])) {
                    unset($arrParam['situacao_id']);
                }
            }

            if ($perId == "desligados") {
                $serviceSituacoes      = new \Matricula\Service\AcadgeralSituacao($this->getEm());
                $situacoesDesligamento = $serviceSituacoes::situacoesDesligamento();

                if (isset($arrParam['situacao_id'])) {
                    $arrSituacao             = explode(",", $arrParam['situacao_id']);
                    $arrParam['situacao_id'] = array_merge($arrSituacao, $situacoesDesligamento);
                } else {
                    $arrParam['situacao_id'] = $situacoesDesligamento;
                }

                $arrParam['situacao_id'] = implode(',', $arrParam['situacao_id']);

                $arrParam['desligados'] = true;
                unset($arrParam['per_id']);
            }
        }

        $query = "
        SELECT
            acadperiodo__aluno.alunoper_id,
            acadgeral__aluno_curso.alunocurso_id matricula,
            campus_curso.cursocampus_id,
            curso_nome AS curso,
            camp_nome AS campus,
            pes_nome nome,
            IFNULL (per_nome, '-') periodo,
            IFNULL (turma_nome, '-') turma,
            IFNULL (matsit_descricao, '-') situacao,
            alunoper_sit_data,
            situacao_id,
            con_contato_celular,
            con_contato_telefone,
            con_contato_email,
            IF(now() BETWEEN acadperiodo__letivo.per_data_inicio AND acadperiodo__letivo.per_data_fim, 1, 0) AS periodoAberto,
            IF(
                now() BETWEEN LEAST(
                    acadperiodo__letivo.per_data_inicio,
                    COALESCE(acadperiodo__letivo.per_matricula_inicio, acadperiodo__letivo.per_data_inicio),
                    COALESCE(acadperiodo__letivo.per_rematricula_online_inicio, acadperiodo__letivo.per_data_inicio)
                ) AND GREATEST(
                    acadperiodo__letivo.per_data_fim,
                    COALESCE(acadperiodo__letivo.per_matricula_fim, acadperiodo__letivo.per_data_fim),
                    COALESCE(acadperiodo__letivo.per_rematricula_online_fim, acadperiodo__letivo.per_data_fim)
                ) AND
                (
                    acadperiodo__letivo.per_data_finalizacao IS NULL OR
                    now() <= acadperiodo__letivo.per_data_finalizacao
                ),
                1,
                0
            ) AS periodoAbertoAlteraMatricula
            -- OUTROS CAMPOS --
        FROM acadgeral__aluno
        LEFT JOIN acadgeral__aluno_curso ON acadgeral__aluno.aluno_id = acadgeral__aluno_curso.aluno_id
        LEFT JOIN campus_curso USING (cursocampus_id)
        LEFT JOIN org_campus USING(camp_id)
        LEFT JOIN acad_curso USING (curso_id)
        LEFT JOIN acadperiodo__aluno USING(alunocurso_id)
        LEFT JOIN acadperiodo__turma ON acadperiodo__aluno.turma_id = acadperiodo__turma.turma_id
        LEFT JOIN acadperiodo__letivo ON acadperiodo__letivo.per_id = acadperiodo__turma.per_id
        INNER JOIN pessoa ON acadgeral__aluno.pes_id = pessoa.pes_id
        LEFT JOIN acadgeral__situacao ON acadgeral__situacao.situacao_id = acadperiodo__aluno.matsituacao_id
        -- JOIN --
        WHERE
        -- CONDITIONS --
        -- CONDITIONSEXTRA --
        GROUP BY matricula
        -- HAVING --
        ORDER BY pes_nome ASC";

        //filtrando apenas os alunos que a ultima situação é inativa
        $conditions = $this->retornaFiltrosParaConsultadeAlunoPeriodo($arrParam);

        if ($arrParam['situacao_id']) {
            //$havingInativos = " HAVING situacao_id in ({$arrParam['situacao_id']})";
            $conditions [] = "situacao_id in ({$arrParam['situacao_id']})";
            unset($arrParam['situacao_id']);
        }

        $arrCampusCursoPermitido = implode(",", $serviceAcessoPessoas->retornaCampusCursoDoUsuarioLogado(true));

        if ($arrCampusCursoPermitido) {
            $conditions [] = "campus_curso.cursocampus_id in (" . $arrCampusCursoPermitido . ")";
        }

        $matriculaNotIn = isset($arrParam['matriculasNotIn']) ? implode(",", $arrParam['matriculasNotIn']) : null;

        if ($matriculaNotIn) {
            $conditions [] = "acadgeral__aluno_curso.alunocurso_id NOT IN ( " . $matriculaNotIn . " )";
        }

        if ($conditions) {
            $query = str_replace('-- CONDITIONS --', implode(' AND ', $conditions), $query);
        }

        return $query;
    }

    /**
     * @param $alunoPerId
     * @return array
     */
    public function getArray($alunoPerId)
    {
        if (!$alunoPerId) {
            return array();
        }

        /* @var $objAcadperiodoAluno \Matricula\Entity\AcadperiodoAluno */
        $objAcadperiodoAluno = $this->getRepository()->find($alunoPerId);

        if (!$objAcadperiodoAluno) {
            $this->setLastError('Aluno não existe!');

            return array();
        }

        return $objAcadperiodoAluno->toArray();
    }

    public function validaAlteracaoSituacao($perId)
    {
        $servicePeriodoLetivo = new \Matricula\Service\AcadperiodoLetivo($this->getEm());

        /** @var $objperiodo \Matricula\Entity\AcadperiodoLetivo */
        $objperiodo = $servicePeriodoLetivo->getRepository()->findOneBy(['perId' => $perId]);

        if (!$objperiodo) {
            $this->setLastError("Período não localizado!");

            return false;
        }

        // Caso o periodo esteja encerrado já evito as comprações posteriores
        //if (!$objperiodo->getPerDataFinalizacao()) {
        if (!$objperiodo->verificarVigencia()) {
            $this->setLastError("Período finalizado!");

            return false;
        }

        $dataInicioInscricao       = $objperiodo->getPerMatriculaInicio();
        $dataInicioInscricaoOnline = $objperiodo->getPerRematriculaOnlineInicio();

        // Se tenho as duas datas eu comparo ambas o resultado é pegar a data mais antiga
        if ($dataInicioInscricao && $dataInicioInscricaoOnline) {
            // Se a data inscricao online for menor logo seto ela na variavel que será comparada
            if ($dataInicioInscricao > $dataInicioInscricaoOnline) {
                $dataInicioInscricao = $dataInicioInscricaoOnline;
            }
        } elseif ($dataInicioInscricao || $dataInicioInscricaoOnline) {
            $dataInicioInscricao = $dataInicioInscricao ? $dataInicioInscricao : $dataInicioInscricaoOnline;
        } else {
            // Caso não tenha ambas
            $this->setLastError("Configuração de data para inicio do período, inválidas!");

            return false;
        }

        if (new \DateTime() < $dataInicioInscricao) {
            $this->setLastError(
                "Não é possivel alterar a situação desse aluno, ainda não começou o processo de inscrição!"
            );

            return false;
        }

        return true;
    }

    public function setarAlunoMatriculado($alunoPerId)
    {
        if (!$alunoPerId) {
            $this->setLastError("Nenhum aluno informado");
        }

        /** @var \Matricula\Entity\AcadperiodoAluno $objAlunoPer */
        if ($objAlunoPer = $this->getRepository()->find($alunoPerId)) {
            $serviceAlunoPerHist = new \Matricula\Service\AcadperiodoAlunoHistorico($this->getEm());

            if ($serviceAlunoPerHist->save($objAlunoPer)) {
                $arrAluno = $objAlunoPer->toArray();

                $arrAluno['alunoperObservacoes'] = (
                    "Atualizado de pendências acadêmicas para matriculado em " .
                    date('d/m/Y') . " pela rotina do sistema"
                );
                $arrAluno['matsituacao']         = self::ALUNOPERIODO_SITUACAO_MATRICULADO;
                $arrAluno['alunoperSitData']     = null;

                if ($this->salvarAlunoPeriodo($arrAluno)) {
                    return true;
                }
            }

            $this->setLastError($serviceAlunoPerHist->getLastError());
        }

        return false;
    }
}