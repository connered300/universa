<?php
namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

class Aluno extends AbstractService
{

    /** @var \Sistema\Service\SisConfig */
    private $config = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, $arrConfig = array())
    {
        parent::__construct($em, 'Matricula\Entity\AcadgeralAluno');

        $this->setConfig(new \Sistema\Service\SisConfig($em, $arrConfig));
    }

    /**
     * @return \Sistema\Service\SisConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param \Sistema\Service\SisConfig $config
     * @return AcadgeralAluno
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    protected function valida($dados)
    {
    }

    protected function pesquisaForJson($criterios)
    {
    }

    public function paginationAjax(array $arrayData)
    {
        $serviceAcadperiodoLetivo = new \Matricula\Service\AcadperiodoLetivo($this->getEm());
        $objAcadperiodoLetivo     = $serviceAcadperiodoLetivo->buscaPeriodoCorrente();

        $query = "
            SELECT * FROM(
                SELECT
                    acadgeral__aluno_curso.alunocurso_id,
                    acadgeral__aluno_curso.alunocurso_id matricula,
                    IF(isnull(alunoper_id), 0, 1) AS matriculado,
                    COALESCE(alunoper_id, '-') AS alunoper_id,
                    COALESCE(alunos.per_id, '-') AS per_id,
                    pes_nome nome,
                    COALESCE(turma_nome, '-') turma,
                    situacao_id,
                    COALESCE(matsit_descricao, '-') AS matsit_descricao,
                    curso_id, curso_nome
                FROM acadgeral__aluno
                INNER JOIN pessoa USING(pes_id)
                INNER JOIN acadgeral__aluno_curso USING(aluno_id)
                INNER JOIN campus_curso USING(cursocampus_id)
                INNER JOIN acad_curso USING(curso_id)
                LEFT JOIN (
                    SELECT
                        acadperiodo__aluno.alunocurso_id,
                        acadperiodo__aluno.alunoper_id,
                        acadperiodo__turma.per_id,
                        acadperiodo__turma.turma_nome,
                        acadgeral__situacao.situacao_id,
                        acadgeral__situacao.matsit_descricao,
                        acadperiodo__aluno.alunoper_sit_data
                    FROM acadperiodo__aluno
                    INNER JOIN acadperiodo__turma USING(turma_id)
                    INNER JOIN acadgeral__situacao ON acadgeral__situacao.situacao_id=acadperiodo__aluno.matsituacao_id
                ) alunos ON
                    alunos.alunocurso_id = acadgeral__aluno_curso.alunocurso_id AND
                    alunos.per_id = " . $objAcadperiodoLetivo->getPerId() . "
                ORDER BY alunoper_sit_data DESC, matriculado DESC
            ) AS alunos
            GROUP BY alunocurso_id
            ORDER BY matriculado DESC
        ";

        $result = $this->paginationDataTablesAjax($query, $arrayData, null, false);

        return $result;
    }

    public function desligamentoAlunoPendencias(array $data)
    {
        $serviceFinanceiroTitulo   = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceFinanceiroDesconto = new \Boleto\Service\FinanceiroDesconto($this->getEm());
        $serviceAlunoperiodo       = new \Matricula\Service\AcadperiodoAluno($this->getEm());
        $serviceAlunoCurso         = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
        $serviceBiblioteca         = new \Biblioteca\Service\Emprestimo($this->getEm());
        $serviceSituacoes          = new \Matricula\Service\AcadgeralSituacao($this->getEm());

        $validaTitulos = $this->getConfig()->localizarChave('TITULOS_FINANCEIRO_VALIDACAO_DESLIGAMENTO_ALUNO');

        /* @var $alunoPeriodo \Matricula\Entity\AcadperiodoAluno */
        $alunoPeriodo = $serviceAlunoperiodo->getRepository()->findOneBy(['alunoperId' => $data['alunoperId']]);
        /* @var $alunoCurso \Matricula\Entity\AcadgeralAlunoCurso */
        $alunoCurso = $serviceAlunoCurso->getRepository()->findOneBy(['alunocursoId' => $data['alunocursoId']]);
        $pendencias = array();
        $total      = 0;

        $fnRegraAlunoCalouro    = function () use (&$pendencias, &$total, $alunoPeriodo, $alunoCurso) {
            if ($alunoPeriodo && $alunoPeriodo->getTurma()->getTurmaSerie() != 1) {
                $pendencias['Acadêmico'][] = "O aluno deve estar no primeiro período.";
                $total++;
            }
        };
        $fnRegraAlunoVeterano   = function () use (&$pendencias, &$total, $alunoPeriodo, $alunoCurso) {
            if ($alunoPeriodo && $alunoPeriodo->getTurma()->getTurmaSerie() == 1) {
                $pendencias['Acadêmico'][] = "O aluno não pode estar no primeiro período.";
                $total++;
            }
        };
        $fnRegraFormaDeIngresso = function () use (&$pendencias, &$total, $alunoPeriodo, $alunoCurso) {
            $formaDeIngressoAluno = $alunoCurso->getTiposel()->getTiposelNome();

            if (!($formaDeIngressoAluno != "Obtenção de Novo Título" && $formaDeIngressoAluno != "Transferência")) {
                $pendencias['Acadêmico'][] = "A forma de ingresso do aluno não pode ser transferência ou obtenção de novo título.";
                $total++;
            }
        };
        $fnRegraMatriculaAtiva  = function () use (&$pendencias, &$total, $alunoPeriodo, $alunoCurso) {
            $alunoCancelado = (
                $alunoCurso->getAlunocursoSituacao() == AcadgeralAlunoCurso::ALUNOCURSO_SITUACAO_CANCELADO
            );
            $alunoTrancado  = (
                $alunoCurso->getAlunocursoSituacao() == AcadgeralAlunoCurso::ALUNOCURSO_SITUACAO_TRANCADO
            );

            if ($alunoPeriodo && !$alunoPeriodo->getMatsituacao()->getMatsitDescricao() == 'Matriculado') {
                $pendencias['Acadêmico'][] = "O aluno não está matriculado.";
                $total++;
            } elseif ($alunoCancelado || $alunoTrancado) {
                $pendencias['Acadêmico'][] = "O aluno não está matriculado.";
                $total++;
            }
        };

        $fnRegraTitulosEmAberto = function () use (
            &$pendencias,
            &$total,
            $serviceFinanceiroTitulo,
            $data,
            $alunoCurso
        ) {
            $arrParamTitulos = ['alunocursoId' => $alunoCurso->getAlunocursoId()];
            $titulos         = $serviceFinanceiroTitulo->buscaTitulosAbertoEmAtrasoAluno($arrParamTitulos);

            if ($titulos > 0) {
                $pendencias['Financeiro'][] = "Aluno possui títulos em aberto na tesouraria.";
                $total++;
            }
        };

        $fnRegraFinanciamentoAtivo   = function () use (&$pendencias, &$total, $serviceFinanceiroDesconto, $data) {
            if ($data['alunoperId']) {
                $fies = $serviceFinanceiroDesconto->buscasFiesAlunoPeriodo($data['alunoperId']);

                if ($fies) {
                    $pendencias['Financiamento'][] = "Aluno possui financiamento(FIES) ativo.";
                    $total++;
                }
            }
        };
        $fnRegraEmprestimosAtivos    = function () use (&$pendencias, &$total, $serviceBiblioteca, $alunoCurso) {
            $exemplares = $serviceBiblioteca->leitorTitulosExemplaresEmPoder(
                $alunoCurso->getAluno()->getPes()->getPes()->getPesId()
            );

            if (count($exemplares) > 0) {
                $pendencias['Biblioteca'][] = "Aluno possui empréstimos ativos na biblioteca.";
                $total++;
            }
        };
        $fnRegraSituacoesDesistencia = function () use (&$pendencias, &$total, $alunoPeriodo, $alunoCurso) {
            $alunoPendente = (
                $alunoCurso->getAlunocursoSituacao() == AcadgeralAlunoCurso::ALUNOCURSO_SITUACAO_PENDENTE
            );
            if ($alunoPeriodo) {
                $matsitDescricao = $alunoPeriodo->getMatsituacao()->getMatsitDescricao();

                if (!($matsitDescricao == 'Pré Matricula' || $matsitDescricao == 'Matricula Provisoria')) {
                    $pendencias['Acadêmico'][] = "Aluno deve estar com situação de provisória ou pré-matrícula.";
                    $total++;
                }
            } elseif ($alunoPendente) {
                $pendencias['Acadêmico'][] = "Aluno deve estar com situação pendente.";
                $total++;
            }
        };

        if ($data && $alunoCurso) {
            switch ($data['tipoDesligamento']) {
                case $serviceSituacoes::CANCELADA:
                    $fnRegraMatriculaAtiva();
                    $fnRegraAlunoCalouro();

                    if ($validaTitulos) {
                        $fnRegraTitulosEmAberto();
                    }

                    $fnRegraFinanciamentoAtivo();
                    $fnRegraEmprestimosAtivos();
                    $fnRegraFormaDeIngresso();
                    break;
                case $serviceSituacoes::TRANCADO:
                case $serviceSituacoes::TRANSFERENCIA:
                    $fnRegraAlunoVeterano();
                    $fnRegraMatriculaAtiva();

                    if ($validaTitulos) {
                        $fnRegraTitulosEmAberto();
                    }

                    $fnRegraFinanciamentoAtivo();
                    $fnRegraEmprestimosAtivos();
                    break;
                case $serviceSituacoes::OBITO:
                    break;
                case $serviceSituacoes::DESISTENTE:
                    $fnRegraSituacoesDesistencia();
                    break;
            }
        }

        return array('total' => $total, 'pendencias' => $pendencias);
    }

    public function desligamentoAluno(array $data)
    {
        $arrPendencia = $this->desligamentoAlunoPendencias($data);

        if ($arrPendencia['total']) {
            return false;
        }

        if (!$data['motivoDesligamento']) {
            return false;
        }

        $serviceSisIntegracao              = new \Sistema\Service\SisIntegracao($this->getEm());
        $serviceAcessoPessoas              = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceFinanceiroTitulo           = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceAcadperiodoAluno           = new \Matricula\Service\AcadperiodoAluno($this->getEm());
        $serviceAlunoCurso                 = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
        $serviceAluno                      = new \Matricula\Service\AcadgeralAluno($this->getEm());
        $serviceAcadperiodoAlunoDisciplina = new \Matricula\Service\AcadperiodoAlunoDisciplina($this->getEm());
        $serviceAcadgeralSituacao          = new \Matricula\Service\AcadgeralSituacao($this->getEm());
        $serviceAcadperiodoAlunoResumo     = new \Matricula\Service\AcadperiodoAlunoResumo($this->getEm());

        $serviceAluno->setConfig($this->getConfig());
        $serviceAlunoCurso->setConfig($this->getConfig());

        try {
            $this->getEm()->beginTransaction();
            /* @var $objAcadgeralSituacao \Matricula\Entity\AcadgeralSituacao */
            $objAcadgeralSituacao = $serviceAcadgeralSituacao->getRepository()->find($data['tipoDesligamento']);

            /* @var $objUsuarioDesligamento \Acesso\Entity\AcessoPessoas */
            $objUsuarioDesligamento = $serviceAcessoPessoas->retornaUsuarioLogado();

            /* @var $objAcadperiodoAluno \Matricula\Entity\AcadperiodoAluno */
            $objAcadperiodoAluno = $serviceAcadperiodoAluno
                ->getRepository()
                ->find($data['alunoperId'] ? $data['alunoperId'] : '-');
            /* @var $objAlunoCurso \Matricula\Entity\AcadgeralAlunoCurso */
            $objAlunoCurso = $serviceAlunoCurso->getRepository()->findOneBy(['alunocursoId' => $data['alunocursoId']]);

            $cancelaTitulosVencidos = false;

            if ($objAcadperiodoAluno) {
                $arrAlunoDisciplinas = $serviceAcadperiodoAlunoDisciplina->buscaDisciplinasAlunoPeriodo(
                    $objAcadperiodoAluno->getAlunoperId()
                );

                //Atualiza situação da matricula do aluno nas disciplinas e registra histórico
                foreach ($arrAlunoDisciplinas as $arrAlunoDisciplina) {
                    /* @var $objAcadperiodoAlunoDisciplina \Matricula\Entity\AcadperiodoAlunoDisciplina */
                    $objAcadperiodoAlunoDisciplina = $arrAlunoDisciplina['alunodisc'];

                    //Registra resumo do aluno
                    $registrouResumoAlunoDisciplina = $serviceAcadperiodoAlunoResumo->registraResumoAlunoDisciplinaDesligamento(
                        $objAcadperiodoAlunoDisciplina
                    );

                    if (!$registrouResumoAlunoDisciplina) {
                        throw new \Exception('Falha ao registrar resumo do aluno.');
                    }

                    $objAcadperiodoAlunoDisciplinaHistorico = new \Matricula\Entity\AcadperiodoAlunoDisciplinaHistorico(
                    );
                    $objAcadperiodoAlunoDisciplinaHistorico
                        ->setAlunodisc($objAcadperiodoAlunoDisciplina)
                        ->setSituacaodiscObservacoes($data['motivoDesligamento'])
                        ->setUsuarioHistorico($objUsuarioDesligamento)
                        ->setTurma($objAcadperiodoAlunoDisciplina->getTurma())
                        ->setSituacao($objAcadperiodoAlunoDisciplina->getSituacao())
                        ->setSituacaodiscData(
                            $objAcadperiodoAlunoDisciplina->getAlunodiscSitData() ?
                                $objAcadperiodoAlunoDisciplina->getAlunodiscSitData() :
                                new \DateTime()
                        )
                        ->setAlunodisc($objAcadperiodoAlunoDisciplina);

                    $this->getEm()->persist($objAcadperiodoAlunoDisciplinaHistorico);
                    $this->getEm()->flush($objAcadperiodoAlunoDisciplinaHistorico);

                    $objAcadperiodoAlunoDisciplina->setSituacao($objAcadgeralSituacao);
                    $objAcadperiodoAlunoDisciplina->setAlunodiscSitData(new \DateTime());

                    $this->getEm()->persist($objAcadperiodoAlunoDisciplina);
                    $this->getEm()->flush($objAcadperiodoAlunoDisciplina);
                }

                //Efetua registro do histórico
                $objAcadperiodoAlunoHistorico = new \Matricula\Entity\AcadperiodoAlunoHistorico();
                $objAcadperiodoAlunoHistorico
                    ->setAlunoper($objAcadperiodoAluno)
                    ->setSituacao($objAcadperiodoAluno->getMatsituacao())
                    ->setSituacaoperData(
                        $objAcadperiodoAluno->getAlunoperSitData() ?
                            $objAcadperiodoAluno->getAlunoperSitData() :
                            new \DateTime()
                    )
                    ->setSituacaoperHistoricoObservacoes($objAcadperiodoAluno->getAlunoperObservacoes())
                    ->setTurma($objAcadperiodoAluno->getTurma())
                    ->setSituacaoperHistoricoObservacoes($data['motivoDesligamento'])
                    ->setUsuarioHistorico(
                        $objAcadperiodoAluno->getUsuarioCriador() ?
                            (
                            $objAcadperiodoAluno->getUsuarioCriador()->getId() ?
                                $objAcadperiodoAluno->getUsuarioCriador() :
                                $objUsuarioDesligamento
                            ) :
                            $objUsuarioDesligamento
                    );

                $this->getEm()->persist($objAcadperiodoAlunoHistorico);
                $this->getEm()->flush($objAcadperiodoAlunoHistorico);

                $objservacaoAdicional = (
                    'O usuário: ' .
                    $objUsuarioDesligamento->getPes()->getPes()->getPesNome() .
                    ' selecionou a opção de ' . $mensagem
                );

                $objAcadperiodoAluno->setMatsituacao($objAcadgeralSituacao);
                $objAcadperiodoAluno->setAlunoperObservacoes(
                    $data['motivoDesligamento'] . ' - ' . $objservacaoAdicional
                );
                $objAcadperiodoAluno->setUsuarioCriador($objUsuarioDesligamento);
                $objAcadperiodoAluno->setAlunoperSitData(new \DateTime());

                $this->getEm()->persist($objAcadperiodoAluno);
                $this->getEm()->flush($objAcadperiodoAluno);

                $serviceSisIntegracao->integracaoInteligenteParaAluno($objAcadperiodoAluno);
            }

            $objAlunoCurso->setAlunocursoSituacao(
                \Matricula\Service\AcadgeralAlunoCurso::ALUNOCURSO_SITUACAO_CANCELADO
            );
            $this->getEm()->persist($objAlunoCurso);
            $this->getEm()->flush($objAlunoCurso);

            if ($serviceAlunoCurso->numeroCursosAtivosAluno($objAlunoCurso->getAluno()) == 0) {
                $serviceAcadperiodoAluno->atualizaInformacoesLeitorBiblioteca($objAcadperiodoAluno, $objAlunoCurso);
                $serviceAcessoPessoas->inativaUsuarioPessoa($objAlunoCurso->getAluno()->getPes()->getPes());
            }

            $serviceAluno->efetuaIntegracaoAluno($objAlunoCurso->getAluno());

            $this->getEm()->flush();
            $this->getEm()->commit();

            return true;
        } catch (\Exceplotion $ex) {
            $erro = $ex->getMessage();
        }

        return false;
    }

}