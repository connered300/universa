<?php
namespace Matricula\Service;

use Acesso\Service\AcessoPessoas;
use VersaSpine\Service\AbstractService;

/**
 * Class AcadperiodoAlunoDisciplina
 * @package Matricula\Service
 */
class AcadperiodoAlunoDisciplina extends AbstractService
{
    /**
     * @var null
     */
    private $lastError = null;

    /**
     * @return null
     */
    public function getLastError()
    {
        return $this->lastError;
    }

    /**
     * @param null $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * @param \Doctrine\ORM\EntityManager $em
     */

    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadperiodoAlunoDisciplina');
    }

    public function valida($arrDados)
    {
        $errors = array();

        if (!$arrDados['discId']) {
            $errors[] = "Por favor, informe a disciplina";
        }
        if (!$arrDados['turmaId']) {
            $errors[] = "Por favor, informe a turma";
        }
        if (!$arrDados['alunoperId']) {
            $errors[] = "Por favor, informe a matrícula do aluno no período";
        }
        if (!$arrDados['situacaoId']) {
            $errors[] = "Por favor, informe a situação do aluno na disciplina";
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {
    }

    public function pesquisaDisciplinasAluno($arrParam)
    {
        $query = '
        SELECT
            pessoaaluno.pes_id AS pes_id_aluno,
            pessoaaluno.pes_nome AS pes_nome_aluno,
            aluno_id,
            alunocurso_id,
            alunoper_id,
            docdisc_id,
            alunodisciplina.disc_id,
            disc_nome,
            alunodisciplina.turma_id,
            turma_nome,
            turma.per_id,
            docente_id,
            pessoadocente.pes_id AS pes_id_docente,
            pessoadocente.pes_nome AS pes_nome_docente
        FROM pessoa pessoaaluno
        INNER JOIN pessoa_fisica pf USING(pes_id)
        INNER JOIN acadgeral__aluno aluno USING(pes_id)
        INNER JOIN acadgeral__aluno_curso alunocurso USING(aluno_id)
        INNER JOIN acadperiodo__aluno alunoperiodo USING(alunocurso_id)
        INNER JOIN acadperiodo__aluno_disciplina alunodisciplina USING(alunoper_id)
        INNER JOIN acadgeral__disciplina disciplina USING(disc_id)
        INNER JOIN acadperiodo__turma turma ON alunodisciplina.turma_id = turma.turma_id
        LEFT JOIN acadperiodo__docente_disciplina docentedisciplina ON
            docentedisciplina.turma_id = turma.turma_id AND
            docentedisciplina.disc_id = alunodisciplina.disc_id AND
            docdisc_data_fim IS NULL
        LEFT JOIN acadgeral__docente docente USING(docente_id)
        LEFT JOIN pessoa pessoadocente ON pessoadocente.pes_id = docente.pes_id';

        $conditions = [];
        $params     = [];

        if ($arrParam['pes']) {
            $conditions[]  = 'pessoaaluno.pes_id = :pes';
            $params['pes'] = $arrParam['pes'];
        }

        if ($arrParam['disc']) {
            $conditions[]   = 'alunodisciplina.disc_id in(:disc)';
            $params['disc'] = $arrParam['disc'];
        }

        if ($arrParam['alunocurso']) {
            $conditions[]         = 'alunocurso.alunocurso_id = :alunocurso';
            $params['alunocurso'] = $arrParam['alunocurso'];
        }

        if ($arrParam['alunoper']) {
            $conditions[]       = 'alunoperiodo.alunoper_id = :alunoper';
            $params['alunoper'] = $arrParam['alunoper'];
        }

        if ($arrParam['tturma']) {
            $conditions[]     = 'tturma_id in(:tturma)';
            $params['tturma'] = $arrParam['tturma'];
        }

        if ($arrParam['per']) {
            $conditions[]  = 'turma.per_id in(:per)';
            $params['per'] = $arrParam['per'];
        }

        if ($arrParam['situacao']) {
            $conditions[]       = '(
                alunoperiodo.matsituacao_id in(:situacao) AND
                alunodisciplina.situacao_id in(:situacao)
            )';
            $params['situacao'] = $arrParam['situacao'];
        }

        if ($conditions) {
            $query .= ' WHERE ' . implode(" AND ", $conditions);
        }

        return $this->executeQueryWithParam($query, $params)->fetchAll();
    }

    /**
     * Retorna disciplinas de um aluno qua não sejam transferência
     * @param null $alunoperId Matrícula do aluno
     * @return array|null
     */
    public function buscaDisciplinasAlunoPeriodo($alunoperId = null, $todasAsDisciplinas = false)
    {
        $arrDisciplinas         = array();
        $situacoesDesligamento  = "";
        $serviceDisciplinaCurso = new \Matricula\Service\AcadgeralDisciplinaCurso($this->getEm());

        if (!$todasAsDisciplinas) {
            $serviceSituacoes      = new \Matricula\Service\AcadgeralSituacao($this->getEm());
            $situacoesDesligamento = $serviceSituacoes::situacoesVisiveisMatricula();
            $situacoesDesligamento = implode(',', $situacoesDesligamento);
        }

        if ($alunoperId) {
            $query = $this->getEm()
                ->createQuery(
                    "SELECT u FROM '{$this->getEntity()}' u
                    WHERE u.alunoper = {$alunoperId}" .
                    ($situacoesDesligamento ? " AND u.situacao IN ({$situacoesDesligamento}) " : "")
                );

            $arrAlunoDisciplina = $query->getResult();

            if (!$arrAlunoDisciplina) {
                return array();
            }

            /* @var $objAlunoDisciplina \Matricula\Entity\AcadperiodoAlunoDisciplina */
            foreach ($arrAlunoDisciplina as $objAlunoDisciplina) {
                /* @var $objAcadperiodoTurma \Matricula\Entity\AcadperiodoTurma */
                $objAcadperiodoTurma = $objAlunoDisciplina->getTurma();

                if (!$objAcadperiodoTurma) {
                    $query               = "
                                SELECT turma_id
                                FROM acadperiodo__aluno_disciplina
                                WHERE alunodisc_id = {$objAlunoDisciplina->getAlunodiscId()}
                            ";
                    $result              = $this->executeQuery($query)->fetch();
                    $objAcadperiodoTurma = $this
                        ->getRepository('Matricula\Entity\AcadperiodoTurma')
                        ->find((int)$result['turma_id']);
                }

                /* @var $objCursoDisc \Matricula\Entity\AcadgeralDisciplinaCurso */
                // (campo tipo disciplina foi movido para disc curso, estou fazendo isso aqui, pois pode ser necessário futuramente, já que pode envolver disciplina regular, optativa, e etc)
                $objCursoDisc = $serviceDisciplinaCurso->getRepository()->findOneBy(
                    [
                        'curso' => $objAlunoDisciplina->getTurma()->getCursocampus()->getCurso()->getCursoId(),
                        'disc'  => $objAlunoDisciplina->getDisc()->getDiscId()
                    ]
                );

                $arrDisciplinas[] = [
                    'alunodiscId'   => $objAlunoDisciplina->getAlunodiscId(),
                    'alunodisc'     => $objAlunoDisciplina,
                    'discNome'      => $objAlunoDisciplina->getDisc()->getDiscNome(),
                    'discTipo'      => $objCursoDisc ? $objCursoDisc->getTdisc()->getTdiscDescricao() : "",
                    'disc'          => $objAlunoDisciplina->getDisc()->getDiscId(),
                    'situacaoId'    => $objAlunoDisciplina->getSituacao()->getSituacaoId(),
                    'situacao'      => $objAlunoDisciplina->getSituacao()->getMatsitDescricao(),
                    'turmaId'       => $objAcadperiodoTurma->getTurmaId(),
                    'turmaNome'     => $objAcadperiodoTurma->getTurmaNome(),
                    'turmaTipoId'   => $objAcadperiodoTurma->getTturma() ? $objAcadperiodoTurma->getTturma() : '',
                    'turmaTipoDesc' => $objAcadperiodoTurma->getTturma() ? $objAcadperiodoTurma->getTturma() : '',
                ];
            }
        }

        return $arrDisciplinas;
    }

    /**
     * Busca o aluno na disciplina com situacao de 'Matriculado'
     * @param $aluno
     * @param $discplina
     * @return null|\Matricula\Entity\AcadperiodoAlunoDisciplina
     */
    public function buscasAlunoDiscPorAlunoEDisciplina($aluno, $discplina)
    {
        $situacao        = $this->getRepository("Matricula\Entity\AcadgeralSituacao")->findOneBy(
            array('matsitDescricao' => 'Matriculado')
        )->getSituacaoId();
        $alunoDisciplina = $this->getRepository()->findOneBy(
            ['alunoper' => $aluno, 'disc' => $discplina, 'situacao' => $situacao]
        );

        if ($alunoDisciplina) {
            return $alunoDisciplina;
        }

        return null;
    }

    /** Retorna disciplinas do aluno no período letivo que não estão com situação dispensado e transferência
     * @param null $id Matrícula do aluno
     * @return array|null
     */
    public function buscaDisciplinasMatriculadasAlunoPeriodo($id = null)
    {
        $arrSituacoesAtividade = \Matricula\Service\AcadgeralSituacao::situacoesAtividade();

        //Transforma a array de situações em Atividade em um variavel para passar na consulta SQL
        foreach ($arrSituacoesAtividade as $cont => $situacao) {
            if ($cont === 0) {
                $situcoesAtivas = "'{$situacao}'";
            } elseif (count($arrSituacoesAtividade) - 1 == $cont) {
                $situcoesAtivas .= ",'{$situacao}'";
            } else {
                $situcoesAtivas .= ",'{$situacao}'";
            }
        }

        if ($id) {
            if (!is_null($arrSituacoesAtividade)) {
                $query       = $this->getEm()
                    ->createQuery(
                        "SELECT u FROM '{$this->getEntity()}' u
                        WHERE u.alunoper = {$id} AND u.situacao IN($situcoesAtivas)"
                    );
                $disciplinas = $query->getResult();

                if ($disciplinas) {
                    foreach ($disciplinas as $disciplina) {
                        try {
                            $turma = $this->getReference(
                                $disciplina->getTurma()->getTurmaId(),
                                'Matricula\Entity\AcadperiodoTurma'
                            );
                        } catch (\Exception $ex) {
                            $query  = "
                                SELECT
                                    turma_id
                                FROM
                                  acadperiodo__aluno_disciplina
                                WHERE
                                  alunodisc_id = {$disciplina->getAlunodiscId()}
                            ";
                            $result = $this->executeQuery($query)->fetch();
                            $turma  = $this->getReference(
                                (int)$result['turma_id'],
                                'Matricula\Entity\AcadperiodoTurma'
                            );
                        }

                        $array[] = [
                            'alunodiscId' => $disciplina->getAlunodiscId(),
                            'alunodisc'   => $disciplina,
                            'discNome'    => $disciplina->getDisc()->getDiscNome(),
                            'disc'        => $disciplina->getDisc()->getDiscId(),
                            'situacao'    => $this
                                ->getReference(
                                    $disciplina->getSituacao()->getSituacaoId(),
                                    'Matricula\Entity\AcadgeralSituacao'
                                )
                                ->getMatsitDescricao(),
                            'turmaId'     => $turma->getTurmaId(),
                            'turmaNome'   => $turma->getTurmaNome(),
                        ];
                    }

                    return $array;
                }
            }
        }

        return null;
    }

    /** Retorna disciplinas do aluno no período letivo
     * @param null $alunoperId Matrícula do aluno
     * @return array|null
     */
    public function disciplinasAlunoPeriodo($alunoperId, $arrSituacoes = array())
    {
        $query = '
        SELECT u
        FROM Matricula\Entity\AcadperiodoAlunoDisciplina u
        WHERE u.alunoper = ' . $alunoperId;

        if ($arrSituacoes) {
            $query .= ' AND u.situacao IN(' . implode(',', $arrSituacoes) . ')';
        }

        $query       = $this->getEm()->createQuery($query);
        $disciplinas = $query->getResult();

        return $disciplinas ? $disciplinas : array();
    }

    /**
     * Retorna listagem de alunos de uma docência
     * @param integer $turmaId
     * @param integer $discId
     * @param array   $arrSituacoes
     * @return array
     */
    public function alunosDocenciaEtapaArray($turmaId, $discId, $arrSituacoes = array())
    {
        $conditions = array();
        $param      = array('turmaId' => $turmaId, 'discId' => $discId,);

        if ($arrSituacoes) {
            $conditions[] = 'alunoper.matsituacao_id in(' . implode(', ', $arrSituacoes) . ')';
            $conditions[] = 'alunodisc.situacao_id in(' . implode(', ', $arrSituacoes) . ')';
        }

        $conditions = implode(' AND ', $conditions);

        if ($conditions) {
            $conditions = ' AND ' . $conditions;
        }

        $sql = '
            SELECT
                docdisc.docdisc_id, docdisc.disc_id,
                alunocur.alunocurso_id,
                pesaluno.pes_id aluno_id, pesaluno.pes_nome,
                situacaoGeral.situacao_id, situacaoGeral.matsit_descricao,
                alunodisc.alunodisc_id
            FROM acadperiodo__docente_disciplina docdisc
                INNER JOIN acadperiodo__aluno_disciplina alunodisc
					ON alunodisc.disc_id=docdisc.disc_id AND alunodisc.turma_id=docdisc.turma_id
                INNER JOIN acadperiodo__aluno alunoper
					ON alunoper.alunoper_id = alunodisc.alunoper_id
                INNER JOIN acadgeral__aluno_curso alunocur
					ON alunocur.alunocurso_id = alunoper.alunocurso_id
                INNER JOIN acadgeral__aluno aluno
					ON aluno.aluno_id = alunocur.aluno_id
                INNER JOIN pessoa pesaluno
					ON pesaluno.pes_id = aluno.pes_id
                INNER JOIN acadgeral__situacao situacaoGeral
					ON situacaoGeral.situacao_id = GREATEST(alunoper.matsituacao_id, alunodisc.situacao_id)
            WHERE docdisc.turma_id = :turmaId AND docdisc.disc_id = :discId
            ' . $conditions . '
            ORDER BY pes_nome ASC';

        $stmt         = $this->executeQueryWithParam($sql, $param);
        $arrDocencias = $stmt->fetchAll();

        return $arrDocencias;
    }

    /**
     *
     *
     */

    public function save($discAluno)
    {
        if ($discAluno instanceof \Matricula\Entity\AcadperiodoAlunoDisciplina) {
            $objDiscAluno = $discAluno;
        } else {
            if ($discAluno['alunodiscId']) {
                $objDiscAluno = $this->getReference(
                    $discAluno['alunodiscId'],
                    "Matricula\Entity\AcadperiodoAlunoDisciplina"
                );
            } else {
                $objDiscAluno = new \Matricula\Entity\AcadperiodoAlunoDisciplina(array());
            }
        }

        if (is_numeric($discAluno['disc'])) {
            $discAluno['disc'] = $this->getEm()->getReference(
                "Matricula\Entity\AcadgeralDisciplina",
                $discAluno['disc']
            );
        }

        if (is_numeric($discAluno['alunoper'])) {
            $discAluno['alunoper'] = $this->getEm()->getReference(
                "Matricula\Entity\AcadperiodoAluno",
                $discAluno['alunoper']
            );
        }

        if (is_numeric($discAluno['turma'])) {
            $discAluno['turma'] = $this->getEm()->getReference(
                "Matricula\Entity\AcadperiodoTurma",
                $discAluno['turma']
            );
        }

        if (is_numeric($discAluno['usuarioHistorico'])) {
            $discAluno['usuarioHistorico'] = $this->getEm()->getReference(
                "Acesso\Entity\AcessoPessoas",
                $discAluno['usuarioHistorico']
            );
        }

        if (is_numeric($discAluno['situacao'])) {
            $discAluno['situacao'] = $this->getEm()->getReference(
                "Matricula\Entity\AcadgeralSituacao",
                $discAluno['situacao']
            );
        }

        $discAluno['alunodiscSitData'] = ($discAluno['alunodiscSitData'])
            ? $discAluno['alunodiscSitData']
            : new \DateTime(
                'now'
            );

        $objDiscAluno->setAlunodiscSitData($discAluno['alunodiscSitData']);
        $objDiscAluno->setSituacao($discAluno['situacao']);
        $objDiscAluno->setTurma($discAluno['turma']);
        $objDiscAluno->setAlunoper($discAluno['alunoper']);
        $objDiscAluno->setDisc($discAluno['disc']);
        $objDiscAluno->setUsuarioCriador($discAluno['usuarioCriador']);

        try {
            $this->getEm()->beginTransaction();
            //$this->getEm()->persist($discAluno['usuarioCriador']);
            //$this->getEm()->flush($discAluno['usuarioCriador']);
            $this->getEm()->persist($objDiscAluno);
            $this->getEm()->flush($objDiscAluno);

            $this->getEm()->commit();
        } catch (\Exception $ex) {
            return array('type' => 'error', 'message' => 'Erro ao salvar registro de aluno na disciplina');
        }

        return $objDiscAluno;
    }

    /**
     * Atualiza situação de aluno na disciplina, com base na matricula do aluno
     * @param integer $situacao
     * @param integer $disciplinaId
     * @param integer $matriculaAluno
     */
    public function updateSituacaoDisciplinaByMatriculaAluno(
        $situacao,
        $disciplinaId,
        $matriculaAluno,
        $returnRows = false
    ) {
        $qb = $this->getEm()->createQueryBuilder();

        try {
            $query = $qb->select(array('t1'))
                ->from('\Matricula\Entity\AcadperiodoAlunoDisciplina', 't1')
                ->join('\Matricula\Entity\AcadperiodoAluno', 't2', 'WITH', 't1.alunoper = t2.alunoperId')
                ->where(
                    $qb->expr()->andX(
                        $qb->expr()->eq('t1.disc', ':disc'),
                        $qb->expr()->eq('t2.alunocurso', ':matricula')
                    )
                )
                ->setParameter('disc', $disciplinaId)
                ->setParameter('matricula', $matriculaAluno)
                ->getQuery();

            $rows = $query->getResult();
        } catch (\Exception $e) {
            return false;
        }

        if ($rows) {
            if (is_numeric($situacao)) {
                $situacao = $this->getEm()->getRepository("Matricula\Entity\AcadgeralSituacao")->find($situacao);
            }

            foreach ($rows as $row) {
                $row->setSituacao($situacao);

                $this->getEm()->persist($row);
                $this->getEm()->flush($row);
            }

            return $rows;
        }

        return false;
    }

    public function salvarAlunoDisciplina($arrDados)
    {
        if (!$arrDados) {
            $this->setLastError("Array de dados está vazio!");

            return false;
        }

        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceAcessoPessoas = new AcessoPessoas($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['alunodiscId']) {
                $objAlunodisc = $this->getRepository('Matricula\Entity\AcadperiodoAlunoDisciplina')->find(
                    $arrDados['alunodiscId']
                );

                if (!$objAlunodisc) {
                    $this->setLastError("Registro de alunoDisc não encontrado");

                    return false;
                }
            } else {
                $objAlunodisc = new \Matricula\Entity\AcadperiodoAlunoDisciplina(array());
            }

            if ($arrDados['discId']) {
                /** @var \Matricula\Entity\AcadgeralDisciplina $objDisciplina */
                $objDisciplina = $this->getRepository('Matricula\Entity\AcadgeralDisciplina')->find(
                    $arrDados['discId']
                );

                if (!$objDisciplina) {
                    $this->setLastError("Registro de disciplina não encontrado.");

                    return false;
                }

                $objAlunodisc->setDisc($objDisciplina);
            } else {
                $objAlunodisc->setDisc(null);
            }

            if ($arrDados['turmaId']) {
                /** @var \Matricula\Entity\AcadperiodoTurma $objTurma */
                $objTurma = $this->getRepository('Matricula\Entity\AcadperiodoTurma')->find($arrDados['turmaId']);

                if (!$objTurma) {
                    $this->setLastError("Registro de turma não encontrado");

                    return false;
                }

                $objAlunodisc->setTurma($objTurma);
            } else {
                $objAlunodisc->setTurma(null);
            }

            if ($arrDados['alunoperId']) {
                /** @var \Matricula\Entity\AcadperiodoAluno $objAlunoper */
                $objAlunoper = $this->getRepository('Matricula\Entity\AcadperiodoAluno')->find($arrDados['alunoperId']);

                if (!$objAlunoper) {
                    $this->setLastError("Registro de aluno não encontrado.");

                    return false;
                }

                $objAlunodisc->setAlunoper($objAlunoper);
            } else {
                $objAlunodisc->setAlunoper(null);
            }

            if ($arrDados['situacaoId']) {
                /** @var \Matricula\Entity\AcadgeralSituacao $objMatsituacao */
                $objMatsituacao = $this->getRepository('Matricula\Entity\AcadgeralSituacao')->find(
                    $arrDados['situacaoId']
                );

                if (!$objMatsituacao) {
                    $this->setLastError("Registro de situação não encontrado");

                    return false;
                }

                $objAlunodisc->setSituacao($objMatsituacao);
            } else {
                $objAlunodisc->setSituacao(null);
            }

            if ($arrDados['usuarioCriador']) {
                /** @var \Acesso\Entity\AcessoPessoas $objAcessoPessoas */
                $objAcessoPessoas = $this->getRepository('Acesso\Entity\AcessoPessoas')->find(
                    $arrDados['usuarioCriador']
                );

                if (!$objAcessoPessoas) {
                    $this->setLastError("Registro de usuario não encontrado");

                    return false;
                }

                $objAlunodisc->setUsuarioCriador($objAcessoPessoas);
            } else {
                $objAcessoPessoas = $serviceAcessoPessoas->retornaUsuarioLogado();
                $objAlunodisc->setUsuarioCriador($objAcessoPessoas);
            }

            if ($arrDados['alunodiscSitData']) {
                $objAlunodisc->setAlunodiscSitData($arrDados['alunodiscSitData']);
            } else {
                $objAlunodisc->setAlunodiscSitData(new \DateTime());
            }

            $this->getEm()->persist($objAlunodisc);
            $this->getEm()->flush($objAlunodisc);

            $this->getEm()->commit();

            return $objAlunodisc->getAlunodiscId();
        } catch (\Exception $e) {
            $this->setLastError("Não foi possível salvar a disciplina do aluno!" . $e->getMessage());
        }

        return false;
    }

    public function retornaDisciplinasAlunoPer($dados)
    {
        if (!$dados) {
            $this->setLastError("Nenhum dado informado para busca!");

            return false;
        }

        $sql = "
        SELECT
        ald.disc_id disc,disc_nome discNome,
        tdisc_id discTipo, tdisc_descricao discTipoNome,
        per_disc_id perDiscPeriodo,
        situacao_id situacaoDisc,
        matsit_descricao matDesc,
        turma_nome turmaNome, turma_id turmaId,
        alunodisc_id
        FROM acadperiodo__aluno_disciplina ald
        INNER JOIN acadgeral__disciplina ad ON ad.disc_id=ald.disc_id
        INNER JOIN acadperiodo__matriz_disciplina amd ON amd.disc_id=ald.disc_id
        INNER JOIN acadgeral__disciplina_tipo USING (tdisc_id)
        INNER JOIN acadgeral__situacao USING(situacao_id)
        INNER JOIN acadperiodo__turma USING(turma_id)
        WHERE 1
        ";

        if ($dados['situacoesValidas']) {
            $situacoesDesligamento = \Matricula\Service\AcadgeralSituacao::situacoesDesligamento();
            $situacoesDesligamento = implode(',', $situacoesDesligamento);

            $sql .= " AND ald.situacao_id NOT IN (" . $situacoesDesligamento . ")";
        }

        if ($dados['alunoPer']) {
            $sql .= '  AND alunoper_id =:alunoPer ';
            $param['alunoPer'] = $dados['alunoPer'];
        }

        $sql .= ' GROUP BY ald.disc_id';
        $result = $this->executeQueryWithParam($sql, $param)->fetchAll();

        return $result;
    }

    public function verificaRegistroDeAlunoNaDisciplina(\Matricula\Entity\AcadperiodoAluno $objAlunoPer)
    {
        if (!$objAlunoPer) {
            $this->setLastError("Necessário informar um aluno");

            return false;
        }

        $serviceMatriz      = new \Matricula\Service\AcadperiodoMatrizDisciplina($this->getEm());
        $arrAlunoDisciplina = $this->getRepository()->findBy(['alunoper' => $objAlunoPer]);

        if (!$arrAlunoDisciplina) {
            $arrDados       = $objAlunoPer->toArray();
            $arrDisciplinas = $serviceMatriz->disciplinasMatrizTurma($objAlunoPer->getTurma());

            if (!$arrDisciplinas) {
                $this->setLastError("Nenhuma disciplina encontrada para a turma informada.");

                return false;
            }

            foreach ($arrDisciplinas as $discId) {
                $arrDados['discId']     = $discId;
                $arrDados['situacaoId'] = $objAlunoPer->getMatsituacao();
                $arrDados['turmaId']    = $objAlunoPer->getTurma()->getTurmaId();

                if (!$this->salvarAlunoDisciplina($arrDados)) {
                    return false;
                }
            }
        } else {
            /** @var \Matricula\Entity\AcadperiodoAlunoDisciplina $objAlunoDisciplina */
            foreach ($arrAlunoDisciplina as $objAlunoDisciplina) {
                $objAlunoDisciplina->setSituacao($objAlunoPer->getMatsituacao());
                $objAlunoDisciplina->setAlunodiscSitData(new \DateTime());
                $this->getEm()->persist($objAlunoDisciplina);
                $this->getEm()->flush($objAlunoDisciplina);
            }
        }

        $arrAlunoDisc = $this->buscaDisciplinasAlunoPeriodo($objAlunoPer->getAlunoperId());

        return $arrAlunoDisc;
    }

    public function retornaDadosAlunoDisciplina($param)
    {
        if (!$param['disciplina']) {
            $this->setLastError("É necessário informar a disciplina!");
        }

        $sql = '
        SELECT 
            p.pes_nome alunoNome,
            act.turma_nome turmaNome,
            act.turma_serie turmaSerie,
            act.turma_turno turmaTurno,
            ac.curso_nome cursoNome,
            al.per_semestre perSemestre,
            al.per_ano perAno,
            ags.matsit_descricao situacaoNome,
            aac.alunocurso_id alunoCursoId,
            acad.disc_nome disciplinaNome
        FROM acadperiodo__aluno_disciplina aad
        INNER JOIN acadgeral__disciplina acad ON acad.disc_id=aad.disc_id
        INNER JOIN acadperiodo__turma act ON act.turma_id=aad.turma_id
        LEFT JOIN acadperiodo__letivo al ON al.per_id=act.per_id
        LEFT JOIN acadgeral__situacao ags ON ags.situacao_id=aad.situacao_id
        INNER JOIN acadperiodo__aluno apa ON apa.alunoper_id = aad.alunoper_id
        INNER JOIN acadgeral__aluno_curso aac ON aac.alunocurso_id=apa.alunocurso_id
        INNER JOIN campus_curso cc ON cc.cursocampus_id= aac.cursocampus_id
        INNER JOIN acad_curso ac ON ac.curso_id=cc.curso_id
        INNER JOIN acadgeral__aluno aa ON aa.aluno_id= aac.aluno_id
        LEFT JOIN pessoa p ON p.pes_id= aa.pes_id';

        $sql .= ' WHERE aad.disc_id=:disciplina';
        $filter['disciplina'] = $param['disciplina'];

        if ($param['turmas']) {
            $sql .= ' AND aad.turma_id=:turma';
            $filter['turma'] = $param['turmas'];
        }

        if ($param['situacao']) {
            $sqlSituacao = ' AND aad.situacao_id IN (:situacao) ';

            if (is_string($param['situacao'])) {
                $sqlSituacao = ' AND ags.matsit_descricao IN (:situacao ) ';
            }

            $sql .= $sqlSituacao;

            $filter['situacao'] = $param['filtro'];
        }

        if ($param['situacaoNot']) {
            $sql .= " AND aad.situacao_id  NOT IN (:situacaoNot)";
            $filter['situacaoNot'] = explode(',',$param['situacaoNot']);
        }

        if ($param['periodoLetivo']) {
            $sql .= " AND al.per_id=:perId";
            $filter['perId'] = $param['periodoLetivo'];
        }

        $sql .= ' ORDER BY p.pes_nome';

        $result = $this->executeQueryWithParam($sql, $filter)->fetchAll();

        return $result;
    }

    public function saveMultiplosObjetos($arrObjetosDisciplina, $arrObjetosDisciplinaHistorico)
    {
        $serviceDisciplinaHistorico = new \Matricula\Service\AcadperiodoAlunoDisciplinaHistorico($this->getEm());

        if (!$arrObjetosDisciplina) {
            $this->setLastError("Nenhum dado informado!");

            return false;
        }

        try {
            /** @var $objAlunoDisciplina \Matricula\Entity\AcadperiodoAlunoDisciplina */
            foreach ($arrObjetosDisciplina as $index => $objAlunoDisciplina) {
                if (is_a($objAlunoDisciplina, '\Matricula\Entity\AcadperiodoAlunoDisciplina')) {
                    $this->getEm()->persist($objAlunoDisciplina);
                    $this->getEm()->flush($objAlunoDisciplina);

                    if (!$serviceDisciplinaHistorico->save($arrObjetosDisciplinaHistorico[$index], false)) {
                        throw new \Exception("Falha ao salvar dados de histório da disciplina!");

                        return false;
                    }
                }
            }
        } catch (\Exception $ex) {
            $this->setLastError($ex);

            return false;
        }

        return true;
    }

    /**
     * @param $alunoPerId
     * @return array
     */
    public function getArray($alunoPerId)
    {
        if (!$alunoPerId) {
            return array();
        }

        $arrObjAlunoDisc = $this->getRepository()->findBy(array('alunoper' => $alunoPerId));
        $arrAlunoDisc    = array();

        /** @var \Matricula\Entity\AcadperiodoAlunoDisciplina $objAlunoDisc */
        foreach ($arrObjAlunoDisc as $objAlunoDisc) {
            $arrAlunoDisc[] = $objAlunoDisc->toArray();
        }

        return $arrAlunoDisc;
    }

    public function setarAlunoDiscMatriculado($alunoPerId)
    {
        if (!$alunoPerId) {
            $this->setLastError("Nenhum aluno informado");
        }

        if ($arrAlunoDisc = $this->getArray($alunoPerId)) {
            $serviceAlunoPerDiscHist  = new \Matricula\Service\AcadperiodoAlunoDisciplinaHistorico($this->getEm());
            $serviceAcadgeralSituacao = new \Matricula\Service\AcadgeralSituacao($this->getEm());

            $arrErrors          = array();
            $arrSituacaoAnalise = $serviceAcadgeralSituacao::retornaSituacoesAnaliseMatricula();

            foreach ($arrAlunoDisc as $alunoDisc) {
                if (!in_array($alunoDisc['situacaoId'], $arrSituacaoAnalise)) {
                    continue;
                }

                if ($serviceAlunoPerDiscHist->save($alunoDisc)) {
                    $alunoDisc['situacao'] = $serviceAcadgeralSituacao::MATRICULADO;

                    if (!$this->save($alunoDisc)) {
                        $arrErrors[] = $this->getLastError();
                    }
                }
            }

            if (count($arrErrors) > 0) {
                $this->setLastError(implode("\n", $arrErrors));

                return false;
            }

            return true;
        }

        return false;
    }
}
