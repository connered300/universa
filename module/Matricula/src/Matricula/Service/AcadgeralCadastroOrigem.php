<?php

namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class AcadgeralCadastroOrigem
 * @package Matricula\Service
 */
class AcadgeralCadastroOrigem extends AbstractService
{
    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadgeralCadastroOrigem');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function pesquisaForJson($params)
    {
        $sql        = 'SELECT * FROM acadgeral__cadastro_origem';
        $parameters = array();
        $where      = array();

        $origemNome = ($params['q']) ? $params['q'] : $params['query'];

        if ($origemNome) {
            $where[]                   = ' origem_nome LIKE :origem_nome';
            $parameters['origem_nome'] = $origemNome . '%';
        }

        if ($params['origemId']) {
            $where[] .= ' origem_id <> :origem_id';
            $parameters['origem_id'] = $params['origemId'];
        }

        if ($where) {
            $sql .= " WHERE " . implode('AND', $where);
        }

        $sql .= " ORDER BY origem_nome";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['origemId']) {
                /** @var $objAcadgeralCadastroOrigem \Matricula\Entity\AcadgeralCadastroOrigem */
                $objAcadgeralCadastroOrigem = $this->getRepository()->find($arrDados['origemId']);

                if (!$objAcadgeralCadastroOrigem) {
                    $this->setLastError('Registro de origem de cadastro não existe!');

                    return false;
                }
            } else {
                $objAcadgeralCadastroOrigem = new \Matricula\Entity\AcadgeralCadastroOrigem();
            }

            $objAcadgeralCadastroOrigem->setOrigemNome($arrDados['origemNome']);

            $this->getEm()->persist($objAcadgeralCadastroOrigem);
            $this->getEm()->flush($objAcadgeralCadastroOrigem);

            $this->getEm()->commit();

            $arrDados['origemId'] = $objAcadgeralCadastroOrigem->getOrigemId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de origem de cadastro!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM acadgeral__cadastro_origem";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $origemId
     * @return array
     */
    public function getArray($origemId)
    {
        /** @var $objAcadgeralCadastroOrigem \Matricula\Entity\AcadgeralCadastroOrigem */
        $objAcadgeralCadastroOrigem = $this->getRepository()->find($origemId);

        try {
            $arrDados = $objAcadgeralCadastroOrigem->toArray();
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        return $arrDados;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('origemId' => $params['id']));
        } elseif ($params['origemNome']) {
            $arrEntities = $this->getRepository()->findBy(array('origemNome' => $params['origemNome']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Matricula\Entity\AcadgeralCadastroOrigem */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getOrigemId(),
                $params['value'] => $objEntity->getOrigemNome()
            );
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['origemId']) {
            $this->setLastError('Para remover um registro de origem de cadastro é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objAcadgeralCadastroOrigem \Matricula\Entity\AcadgeralCadastroOrigem */
            $objAcadgeralCadastroOrigem = $this->getRepository()->find($param['origemId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAcadgeralCadastroOrigem);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de origem de cadastro.');

            return false;
        }

        return true;
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
    }
}
?>