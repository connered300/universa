<?php

namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class AcadModalidade
 * @package Matricula\Service
 */
class AcadModalidade extends AbstractService
{
    const MODALIDADE_DESCRICAO_EAD              = 'EAD';
    const MODALIDADE_DESCRICAO_PRESENCIAL       = 'Presencial';
    const MODALIDADE_DESCRICAO_SEMI_PRESENCIAL  = 'Semipresencial';
    const MODALIDADE_PRESENCIAL                 = 1;
    const MODALIDADE_SEMI_PRESENCIAL            = 2;
    const MODALIDADE_EAD                        = 3;

    /**
     * @var null
     */
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadModalidade');
    }

    public function getDescricao($id)
    {
        if (!is_array($id)) {
            $id = array($id);
        }

        $id = implode($id, ',');

        $query = 'SELECT group_concat(mod_nome separator ", ") text from acad_modalidade WHERE mod_id in (:id)';

        return $this->executeQueryWithParam($query, ['id' => $id])->fetch();
    }

    /**
     * @param $params
     * @return mixed
     */
    public function pesquisaForJson($params)
    {
        $sql     = 'SELECT * FROM acad_modalidade LEFT JOIN acad_curso USING(mod_id) WHERE curso_id IS NOT NULL AND';
        $modNome = false;
        $modId   = false;

        if ($params['q']) {
            $modNome = $params['q'];
        } elseif ($params['query']) {
            $modNome = $params['query'];
        }

        if ($params['modId']) {
            $modId = $params['modId'];
        }

        $parameters = array('mod_nome' => "{$modNome}%");
        $sql .= ' mod_nome LIKE :mod_nome';

        if ($modId) {
            $parameters['mod_id'] = $modId;
            $sql .= ' AND mod_id <> :mod_id';
        }

        $sql .= " GROUP BY mod_id";
        $sql .= " ORDER BY mod_nome";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['modId']) {
                $objAcadModalidade = $this->getRepository()->find($arrDados['modId']);

                if (!$objAcadModalidade) {
                    $this->setLastError('Registro de modalidade não existe!');

                    return false;
                }
            } else {
                $objAcadModalidade = new \Matricula\Entity\AcadModalidade();
            }

            $objAcadModalidade->setModNome($arrDados['modNome']);
            $objAcadModalidade->setModObs($arrDados['modObs']);

            $this->getEm()->persist($objAcadModalidade);
            $this->getEm()->flush($objAcadModalidade);

            $this->getEm()->commit();

            $arrDados['modId'] = $objAcadModalidade->getModId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de modalidade!');
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['modNome']) {
            $errors[] = 'Por favor preencha o campo "nome"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM acad_modalidade";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $modId
     * @return array|null|object
     */
    public function getArray($modId)
    {
        $arrDados = $this->getRepository()->find($modId);

        try {
            $arrDados = $arrDados->toArray();
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('modId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Matricula\Entity\AcadModalidade */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getModId(),
                $params['value'] => $objEntity->getModNome()
            );
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['modId']) {
            $this->setLastError('Para remover um registro de modalidade é necessário especificar o código.');

            return false;
        }

        try {
            $objAcadModalidade = $this->getRepository()->find($param['modId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAcadModalidade);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de modalidade.');

            return false;
        }

        return true;
    }
}
?>