<?php


namespace Matricula\Service;

use VersaSpine\Service\AbstractService;
use Zend\Stdlib\Hydrator\Aggregate\HydrateEvent;

/**
 * Class AcadperiodoIntegradora
 * @package Matricula\Service
 */
class AcadperiodoIntegradora extends AbstractService
{
    /**
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadperiodoIntegradora');
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {

    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {

    }

    /**
     * Retorna última integradora cadastrada
     * @return \Matricula\Entity\AcadperiodoIntegradora|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function buscaUltimaIntegradoraAtiva()
    {
        $integradora = $this->getEm()->createQueryBuilder()
            ->select('e')
            ->from($this->getEntity(), 'e')
            ->orderBy('e.integradoraId', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        if ($integradora) {
            return $integradora;
        }

        return null;
    }

    /**
     * Retorna alunos viculados a avaliação integradora
     * @param int $integradoraId
     * @return array|null
     */
    public function buscaDadosAlunosIntegradora($integradoraId)
    {
        $select = "SELECT
                      acadperiodo__etapa_aluno.alunodisc_id alunodisc_id,
                      NDI,
                      NFD,
                      acadperiodo__etapa_aluno.diario_id diario_id
                    FROM
                      view__integradora
                    INNER JOIN
                       acadperiodo__etapa_aluno
                    ON
                       acadperiodo__etapa_aluno.alunodisc_id = view__integradora.alunodisc_id and integradora_id = $integradoraId
                    INNER JOIN
                      acadperiodo__etapa_diario
                    ON
                      acadperiodo__etapa_diario.diario_id = acadperiodo__etapa_aluno.diario_id
                    INNER JOIN
                      acadperiodo__etapas
                    ON
                      acadperiodo__etapas.etapa_id = acadperiodo__etapa_diario.etapa_id and etapa_ordem = 3
                    ";
        $alunos = $this->executeQuery($select)->fetchAll();

        if (count($alunos) > 0) {
            return $alunos;
        }

        return null;
    }

    /**
     * Retorna provas cadastradas para integradora separadas por turno
     * @param int $integradora
     * @return array|null
     */
    public function buscaProvasPorTurnoIntegradora($integradora)
    {
        $provas = null;
        $turno  = null;

        if ($integradora) {
            $provas = array(
                'Matutino'   => array(),
                'Vespertino' => array(),
                'Noturno'    => array(),
            );

            $serviceCaderno  = (new \Matricula\Service\AcadperiodoIntegradoraCaderno($this->getEm()));
            $serviceGabarito = (new \Matricula\Service\AcadperiodoIntegradoraGabarito($this->getEm()));

            $avaliacoes = (new \Matricula\Service\AcadperiodoIntegradoraAvaliacao($this->getEm()))
                ->buscaAvaliacaoPorIntegradora($integradora);

            foreach ($avaliacoes as $avaliacao) {
                if ($avaliacao->getIntegavaliacaoTurno() == 'Noturno') {
                    $turno = 'Noturno';
                } else {
                    if ($avaliacao->getIntegavaliacaoTurno() == 'Vespertino') {
                        $turno = 'Vespertino';
                    } else {
                        if ($avaliacao->getIntegavaliacaoTurno() == 'Matutino') {
                            $turno = 'Matutino';
                        }
                    }
                }

                if ($turno) {
                    //                    $provas[$turno]['perido'] = $avaliacao->getIntegavaliacaoPeriodo();
                    $cadernos = $serviceCaderno->buscaCadernosPorAvaliacao($avaliacao);

                    foreach ($cadernos as $caderno) {
                        $provas[$turno]['periodo'][$avaliacao->getIntegavaliacaoPeriodo(
                        )]['gabarito'][] = $serviceGabarito->buscaDisciplinasDoCadernoPorOrdem($caderno);
                    }
                }
            }
        }

        return $provas;
    }

    /**
     * Retorna true se a turma estiver cadastrada para fazer avaliação integradora
     * @param \Matricula\Entity\AcadperiodoDocenteDisciplina $objDocenteDisciplina
     * @return bool
     */
    public function turmaNaoFazIntegradora(\Matricula\Entity\AcadperiodoDocenteDisciplina $objDocenteDisciplina)
    {
        $objTurma            = $objDocenteDisciplina->getTurma();
        $objTurmaIntegradora = $this->getRepository('\Matricula\Entity\AcadperiodoIntegradoraTurma')
            ->findOneBy(array('turma' => $objTurma->getTurmaId()));

        if (!$objTurmaIntegradora) {
            return true;
        }

        $objIntegavaliacao  = $objTurmaIntegradora->getIntegavaliacao();
        $objIntegradora     = $objIntegavaliacao->getIntegradora();
        $objIntegradoraConf = $objIntegradora->getIntegconf();

        if ($objTurma->getTurmaSerie() > $objIntegradoraConf->getIntegconfSerieMax()) {
            return true;
        }

        if ($objTurma->getTturma()->getTturmaDescricao() == 'Optativa') {
            return true;
        }

        return false;
    }

    /**
     * Retorna true se a avaliação integradora foi fechada ou se ainda não existe registro de integradora para o período
     * @param \Matricula\Entity\AcadperiodoLetivo $objPeriodoLetivo
     * @return bool
     */
    public function provaIntegradoraFoiFechada(\Matricula\Entity\AcadperiodoLetivo $objPeriodoLetivo)
    {
        $objIntegradora = $this->getRepository()->findOneBy(
            array('per' => $objPeriodoLetivo->getPerId())
        );

        if (!$objIntegradora) {
            return true;
        }

        return $objIntegradora->getIntegradoraStatus() != 'Aberta';
    }

    /**
     * Retorna array de alunos que não estão inclusos em avaliação integradora ou que estão com situações que impossibilitem sua participação
     * @param int $docdiscId
     * @return array
     */
    public function alunosQueNaoFizeramIntegradora($docdiscId)
    {
        $situacoes             = \Matricula\Service\AcadgeralSituacao::situacoesAdaptacao();
        $situacoes             = implode(', ', $situacoes);
        $situacoesDesligamento = \Matricula\Service\AcadgeralSituacao::situacoesDesligamento();
        $situacoesDesligamento = implode(', ', $situacoesDesligamento);

        $sql = '
        SELECT alunoDisciplina.alunodisc_id
        FROM acadperiodo__aluno_disciplina alunoDisciplina
        INNER JOIN acadperiodo__docente_disciplina docenteDisciplina
            ON docenteDisciplina.turma_id = alunoDisciplina.turma_id AND docenteDisciplina.disc_id = alunoDisciplina.disc_id
        INNER JOIN acadperiodo__turma turma
            ON turma.turma_id = alunoDisciplina.turma_id
        LEFT JOIN acadperiodo__integradora_aluno integradoraAluno
            ON integradoraAluno.alunodisc_id = alunoDisciplina.alunodisc_id
        WHERE
            docenteDisciplina.docdisc_id = :docdisc_id AND
            (
                alunoDisciplina.situacao_id NOT IN (' . $situacoesDesligamento . ') OR
                turma.tturma_id NOT IN (' . $situacoesDesligamento . ')
            ) AND
            (
                integradoraAluno.alunodisc_id IS NULL OR
                alunoDisciplina.situacao_id IN (' . $situacoes . ')
            )';

        $stmt             = $this->executeQueryWithParam($sql, array('docdisc_id' => $docdiscId));
        $arrLineAlunodisc = $stmt->fetchAll();

        $arrAlunodisc = array();

        foreach ($arrLineAlunodisc as $docdisc) {
            $arrAlunodisc[] = $docdisc['alunodisc_id'];
        }

        return $arrAlunodisc;
    }
}