<?php

namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

class AcadgeralSituacao extends AbstractService
{
    const MATRICULADO =             1;
    const TRANCADO =                2;
    const DISPENSADO =              3;
    const TRANSFERENCIA =           5;
    const CANCELADA =               6;
    const PRE_MATRICULA =           7;
    const ADAPTANTE =               8;
    const DEPENDENTE =              9;
    const MATRICULA_PROVISORIA =    10;
    const CONCLUIDO =               11;
    const MONOGRAFIA =              12;
    const DESISTENTE =              13;
    const PENDENCIAS_ACADEMICAS =   14;
    const OBITO =                   15;
    const ERRO_MATRICULA =          16;
    const SITUACAO_REGULAR_SIM =    "Sim";
    const SITUACAO_REGULAR_NAO =    "Não";

    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadgeralSituacao');
    }

    public static function situacoesAtividade()
    {
        return array(self::MATRICULADO, self::ADAPTANTE, self::DEPENDENTE, self::MONOGRAFIA);
    }

    public static function situacoesVisiveisMatricula()
    {
        return array(
            self::MATRICULADO,
            self::ADAPTANTE,
            self::DEPENDENTE,
            self::MONOGRAFIA,
            self::DISPENSADO,
            self::MATRICULA_PROVISORIA,
            self::PRE_MATRICULA,
            self::PENDENCIAS_ACADEMICAS
        );
    }

    public static function situacoesAnalise()
    {
        return array(self::MATRICULA_PROVISORIA, self::PENDENCIAS_ACADEMICAS);
    }

    public static function situacoesAtividadeLeitura()
    {
        return array(self::MATRICULADO, self::ADAPTANTE, self::DEPENDENTE, self::DISPENSADO);
    }

    public static function situacoesDesligamento()
    {
        return array(
            self::TRANCADO,
            self::DISPENSADO,
            self::CANCELADA,
            self::TRANSFERENCIA,
            self::DESISTENTE,
            self::OBITO,
            self::CONCLUIDO
        );
    }

    public static function situacoesAlunoInativoEtiqueta()
    {
        return array(
            self::TRANCADO,
            self::DISPENSADO,
            self::CANCELADA,
            self::TRANSFERENCIA,
            self::DESISTENTE,
            self::OBITO,
            self::CONCLUIDO,
            self::ERRO_MATRICULA,
            self::PRE_MATRICULA,
            self::MATRICULA_PROVISORIA
        );
    }

    public static function situacoesAdaptacao()
    {
        return array(self::ADAPTANTE, self::DEPENDENTE);
    }

    public static function retornaSituacoesPossiveisAlteracao()
    {
        return array(
            self::MATRICULADO,
            self::PRE_MATRICULA,
            self::MATRICULA_PROVISORIA,
            self::PENDENCIAS_ACADEMICAS,
            self::ERRO_MATRICULA
        );
    }

    public static function retornaSituacoesAnaliseMatricula()
    {
        return array(
            self::PRE_MATRICULA,
            self::MATRICULA_PROVISORIA,
            self::PENDENCIAS_ACADEMICAS
        );
    }

    /**
     * @return \Matricula\Entity\AcadgeralSituacao
     */
    public function retornaSituacaoMatriculaProvisioria()
    {
        return $this->getRepository()->find(self::MATRICULA_PROVISORIA);
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {
    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    public function pesquisaForJson($criterios)
    {
        $sql = ' 
         SELECT *, situacao_id AS id,matsit_descricao AS text 
         FROM acadgeral__situacao 
         WHERE 1 ';

        if ($criterios['situacoesPermitidasAlteracao']) {
            $situacoesPermitidasAlteracao = implode(',', $this->retornaSituacoesPossiveisAlteracao());

            $sql .= ' AND situacao_id IN (' . $situacoesPermitidasAlteracao .')';
        }

        if ($criterios['situacoesDeNaoDesligamentoPedente']) {
            $situacoesDesligamento = implode(',', $this->situacoesDesligamento());

            $sql .= ' AND situacao_id NOT IN (' . $situacoesDesligamento . ',' . self::CONCLUIDO . ')';
        }

        $result = $this->executeQuery($sql)->fetchAll();

        return $result;
    }
}