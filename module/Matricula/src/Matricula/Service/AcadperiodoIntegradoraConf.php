<?php


namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

class AcadperiodoIntegradoraConf extends AbstractService{
    public function __construct(\Doctrine\ORM\EntityManager $em){
        parent::__construct($em, 'Matricula\Entity\AcadperiodoIntegradoraConf');
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {

    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {

    }

    public function buscaConfiguracaoAtual()
    {
        $integradoraConf = $this->getEm()->createQueryBuilder()
            ->select('e')
            ->from($this->getEntity(), 'e')
            ->orderBy('e.integconfId', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        if($integradoraConf){
            return $integradoraConf;
        }
    }
}