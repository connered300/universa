<?php

namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

class AcadgeralDisciplinaCurso extends AbstractService
{
    CONST DISCIPLINA_NUCLEO_COMUM_SIM = 'Sim';
    CONST DISCIPLINA_NUCLEO_COMUM_NAO = 'Nao';
    const DISC_ATIVO_SIM = 'Sim';
    const DISC_ATIVO_NAO = 'Não';

    const DISC_TIPO_REGULAR = 1;
    const DISC_TIPO_OPTATIVA = 2;
    const DISC_TIPO_ESTAGIO = 3;
    const DISC_TIPO_EQUIVALENCIA = 4;
    const DISC_TIPO_BASE_COMUM = 5;

    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadgeralDisciplinaCurso');
    }

    public static function getDiscAtivo()
    {
        return array(self::DISC_ATIVO_SIM, self::DISC_ATIVO_NAO);
    }

    protected function valida($dados)
    {
        $errors = array();

        if (!$dados['curso']) {
            $errors[] = 'Por favor preencha o campo "curso"!';
        }

        if (!$dados['disc']) {
            $errors[] = 'Por favor preencha o campo "disciplina"!';
        }

        if (!$dados['tdisc']) {
            $errors[] = 'Por favor preencha o campo "tipo da disciplina"!';
        }

        if (!$dados['discAtivo']) {
            $errors[] = 'Por favor preencha o campo "disciplina ativo"!';
        }

        if (!in_array($dados['discAtivo'], self::getDiscAtivo())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "disciplina ativo"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    public function pesquisaForJson($params)
    {        
        $sql      = "SELECT *
                     FROM acadgeral__disciplina_curso
                     INNER JOIN acadgeral__disciplina acaddisc using (disc_id) WHERE ";
        $discNome = false;
        $discId   = false;

        if ($params['q']) {
            $discNome = $params['q'];
        } elseif ($params['query']) {
            $discNome = $params['query'];
        }

        if ($params['discId']) {
            $discId = $params['discId'];
        }

        $parameters = array('disc_nome' => "%{$discNome}%");
        $WHERE[]    = ' disc_nome COLLATE UTF8_GENERAL_CI LIKE :disc_nome ';
        
        if($params['cursoId']){
            $parameters['curso_id'] = "{$params['cursoId']}";
            $WHERE[]    = ' curso_id = :curso_id ';
        }

        if ($discId) {
            $parameters['disc_id'] = $discId;
            $WHERE[]               = ' disc_id <> :disc_id ';
        }

        if ($params['optionsInvalid']) {
            $WHERE[] = ' disc_id NOT IN(' . implode(',', $params['optionsInvalid']) . ') ';
        }

        if($params['regular']){
            $WHERE[] = ' tdisc_id = :tdisc_id';
            $parameters['tdisc_id'] = self::DISC_TIPO_REGULAR;
        }

        $sql .= implode('AND', $WHERE);
        $sql .= " ORDER BY disc_nome";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();
        return $result;
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Metodo responsável por salvar vinculo de disciplina e curso
     */
    public function save(array $arrDados, $arrDisc = array()){
        $objAcadgeralDisciplinaCurso = new \Matricula\Entity\AcadgeralDisciplinaCurso();

        $this->begin();
        try{

            //caso tenha essa informação, preciso add a disciplina antes
            if($arrDisc){
                $serviceDisciplina = new \Matricula\Service\AcadgeralDisciplina($this->getEm());
                $arrDados['disc'] = $serviceDisciplina->save($arrDisc)->getDiscId();
            }

            //validando a entidade disciplina curso
            if(!$this->valida($arrDados)){
                $this->getEm()->rollback();
                return false;
            }

            $existeVinculo       = $this->getRepository()->findOneBy(
                ["disc" => $arrDados['disc'], "curso" => $arrDados['curso']]
            );
            $gerarFilaIntegracao = false;

            $serviceDisciplinaTipo    = new \Matricula\Service\AcadgeralDisciplinaTipo($this->getEm());

            //disciplina esta vinculada
            if ($existeVinculo) {
                $objAcadgeralDisciplinaCurso = $existeVinculo;
            } else {

                $serviceDisciplina        = new \Matricula\Service\AcadgeralDisciplina($this->getEm());
                $serviceCurso             = new \Matricula\Service\AcadCurso($this->getEm());

                $objAcadgeralDisciplinaCurso->setCurso($serviceCurso->getRepository()->find($arrDados['curso']));
                $objAcadgeralDisciplinaCurso->setDisc($serviceDisciplina->getRepository()->find($arrDados['disc']));
                $gerarFilaIntegracao = true;
            }
            $objDisciplinaTipo = $serviceDisciplinaTipo->getRepository()->find($arrDados['tdisc']);

            $discequivDestino = new \Doctrine\Common\Collections\ArrayCollection();
            if($arrDados['discequivDestino']){
                $discequivDestino->add($this->getReference($arrDados['discequivDestino']));
            }

            $objAcadgeralDisciplinaCurso->addDiscequivDestino( $discequivDestino );

            $objAcadgeralDisciplinaCurso->setDiscAtivo($arrDados['discAtivo']);
            $objAcadgeralDisciplinaCurso->setTdisc($objDisciplinaTipo);

            $this->getEm()->persist($objAcadgeralDisciplinaCurso);
            $this->getEm()->flush($objAcadgeralDisciplinaCurso);

            $discCursoId = $objAcadgeralDisciplinaCurso->getDiscCursoId();

            $serviceDisciplinaRequisito = new \Matricula\Service\AcadgeralDisciplinaRequisito($this->getEm());

            //pre-requisito e co-requisito
            foreach( $serviceDisciplinaRequisito::getTipoRequisitos() as $tipo ){
                $serviceDisciplinaRequisito->removeRequisito($discCursoId, $tipo);
                if($arrDados[$tipo] && !$serviceDisciplinaRequisito->save([
                       'discreqOrigem'  => $discCursoId,
                       'discreqDestino' => $arrDados[$tipo],
                    ], $tipo)){
                    $this->rollback();
                    return false;
                }
            }

            if ($gerarFilaIntegracao) {
                $serviceSisFilaIntegracao = new \Sistema\Service\SisFilaIntegracao($this->getEm());
                $serviceSisFilaIntegracao->registrarDisciplinaNaFilaDeIntegracao($discCursoId);
            }

        } catch (\Exception $ex) {

            $this->getEm()->rollback();

            $this->setLastError($ex->getMessage());

            return false;
        }

        $this->commit();

        return $objAcadgeralDisciplinaCurso;
    }

    public function getArrSelect2DiscAtivo($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getDiscAtivo());
    }

    /**
     * Método responsável por buscar cursos vinculados ao uma disciplina
     */
    public function buscaCursosPelaDisciplina($discId)
    {
        $query = "SELECT
            c.cursoId,
            c.cursoNome
        FROM
            \Matricula\Entity\AcadgeralDisciplinaCurso acadCurso
        JOIN
            acadCurso.curso c
        WHERE acadCurso.disc = :disc";

        $query = $this->getEm()->createQuery($query);
        $query->setParameter('disc', $discId);

        $results = $query->getResult();

        return $results;
    }

    /**
     * Busca disciplinas regulares pelo curso
     * @param null $cursoId
     * @return mixed
     */
    public function buscaDisciplinasRegularesCursoPeloCurso($cursoId = null){
        return $this->buscaDisciplinasCursoPeloCurso(self::DISC_TIPO_REGULAR, $cursoId);
    }

    /**
     * Busca disciplinas de base comum pelo curso id
     * @param null $cursoId
     * @return mixed
     */
    public function buscaDisciplinasBaseComumCursoPeloCurso($cursoId = null){
        return $this->buscaDisciplinasCursoPeloCurso(self::DISC_TIPO_BASE_COMUM, $cursoId);
    }

    /**
     * Busca todas as disciplinas do curso através do curso id e do tipo
     * @param null $cursoId
     * @return mixed
     */
    protected function buscaDisciplinasCursoPeloCurso($tipoDisc, $cursoId = null){
        $query = "SELECT *
                  FROM acadgeral__disciplina_curso adc
                  INNER JOIN acadgeral__disciplina ad using (disc_id)
                  WHERE adc.tdisc_id = :tdisc_id
                 ";

        $params = ["tdisc_id"=>$tipoDisc];

        if($cursoId){
            $query .= " AND curso_id = :curso_id";
            $params['curso_id'] = $cursoId;
        }
        $query .= " ORDER BY disc_nome";
        return $this->executeQueryWithParam($query, $params)->fetchAll();
    }

    public function getDataForDatatables($discId , $data)
    {
        if( !$discId ){
            return [
                'data'            => [],
                'draw'            => '1',
                'recordsFiltered' => 0,
                'recordsTotal'    => 0,
            ];
        }
        $query = " 
              SELECT ad.disc_nome, ad.disc_sigla, adt.tdisc_descricao, 
                  ac.curso_nome,                      
                  adc.disc__nucleo_comum,
                  adc.tdisc_id,
                  adc.curso_id,
                  adc.disc_ativo,
                  adeq.discequiv_destino as disc_equivalencia,
                  adeq.disc_nome as disc_equivalencia_nome,
                  corequisito.discreq_destino as disc_co_requisito,
                  corequisito.disc_nome as disc_co_requisito_nome,
                  prerequisito.discreq_destino as disc_pre_requisito,
                  prerequisito.disc_nome as disc_pre_requisito_nome
              FROM acadgeral__disciplina ad
              INNER JOIN acadgeral__disciplina_curso adc using (disc_id)
              INNER JOIN acadgeral__disciplina_tipo adt on (adc.tdisc_id = adt.tdisc_id)
              INNER JOIN acad_curso ac using (curso_id)
              LEFT JOIN (
                  SELECT adr.* , disc_nome
                  FROM acadgeral__disciplina_requisito adr
                  INNER JOIN acadgeral__disciplina_curso ON discreq_destino = disc_curso_id 
                  INNER JOIN acadgeral__disciplina using (disc_id)  
              ) corequisito ON (adc.disc_curso_id = corequisito.discreq_origem  AND corequisito.discreq_tipo = 'Co-Requisito')
              LEFT JOIN (
                  SELECT adc.* , disc_nome
                  FROM acadgeral__disciplina_requisito adc
                  INNER JOIN acadgeral__disciplina_curso ON discreq_destino = disc_curso_id 
                  INNER JOIN acadgeral__disciplina using (disc_id)  
              ) prerequisito ON (adc.disc_curso_id = corequisito.discreq_origem  AND prerequisito.discreq_tipo = 'Pre-Requisito')
              LEFT JOIN (
                  SELECT ade.* , disc_nome
                  FROM acadgeral__disciplina_equivalencia ade
                  INNER JOIN acadgeral__disciplina_curso ON discequiv_origem = disc_curso_id 
                  INNER JOIN acadgeral__disciplina using (disc_id)  
              ) adeq ON (adc.disc_curso_id = adeq.discequiv_origem)
              WHERE ad.disc_id = '$discId'
              ";
        $result = parent::paginationDataTablesAjax($query, $data, null, false);
        return $result;
    }

    public function retornaDisciplinaCursoPelaMatrizSerie($matCurId, $serie, $turmaId)
    {
        $erro = '';

        if (!$matCurId) {
            $erro .= ' * Matrícula do curso! <br>';
        }

        if (!$serie) {
            $erro .= ' *  O período! <br>';
        }

        if (!$turmaId) {
            $erro .= ' *  A turma!';
        }

        if ($erro) {
            $this->setLastError('É necessário informar: <br>' . $erro);

            return false;
        }

        $sql = "
            SELECT adc.disc_id AS disciplina 
            FROM acadperiodo__matriz_curricular amc
            LEFT JOIN acadperiodo__matriz_disciplina amd ON amd.mat_cur_id=amc.mat_cur_id
            LEFT JOIN acadgeral__disciplina_curso adc ON adc.curso_id=amc.curso_id AND adc.disc_id=amd.disc_id
            LEFT JOIN acadperiodo__turma apt ON amd.per_disc_periodo=apt.turma_serie
            WHERE amc.mat_cur_id=:matCurId AND amd.per_disc_periodo=:serie AND adc.curso_id IS NOT NULL AND apt.turma_id=:turma
            GROUP BY amd.disc_id
        ";

        $arrRetorno = array();
        $arrparam   = ['matCurId' => $matCurId, 'serie' => $serie, 'turma' => $turmaId];
        $result     = $this->executeQueryWithParam($sql, $arrparam)->fetchAll();

        foreach ($result as $dados) {
            $arrRetorno[] = $dados['disciplina'];
        }

        return $arrRetorno;
    }


    /**
     * Retira vinculo da disciplina no curso pelo id da disciplina
     *
     * @param $discId ID da disciplina que deseja remover os vinculos de curso
     *
     * @return true|false
     */
    public function removeVinculoDiscCurso($discId, $cursoId = null)
    {
        $this->begin();
        try{

            $params = ['disc'=>$discId];
            if($cursoId){
                $params['curso'] = $cursoId;
            }
            $discCurso = $this->getRepository()->findBy($params);

            if(!$discCurso){
                $this->rollback();
                return false;
            }

            $serviceDisciplinaRequisito = new \Matricula\Service\AcadgeralDisciplinaRequisito($this->getEm());
            foreach($discCurso as $cadaDiscCurso){
                $serviceDisciplinaRequisito->removeRequisito($cadaDiscCurso->getDiscCursoId(), $serviceDisciplinaRequisito::CO_REQUISITO);
                $serviceDisciplinaRequisito->removeRequisito($cadaDiscCurso->getDiscCursoId(), $serviceDisciplinaRequisito::PRE_REQUISITO);
                $this->getEm()->remove($cadaDiscCurso);
            }
            $this->getEm()->flush();

        }catch (\Exception $exception){
            $this->rollback();

            $this->setLastError($exception->getMessage());

            return false;
        }
        $this->commit();
        return true;
    }

    public function getArrDisciplinaNucleo()
    {
        return array(
            SELF::DISCIPLINA_NUCLEO_COMUM_SIM,
            SELF::DISCIPLINA_NUCLEO_COMUM_NAO
        );
    }

    public function retornaDiscCursoIdPelaDiscIdTurmaId($disciplinaId, $turmaId)
    {
        if (!$disciplinaId || !$turmaId) {
            $this->setLastError("É necessário informar Disciplina e turma!");

            return false;
        }

        $sql = '
        SELECT adc.disc_curso_id AS disciplinaCursoId
        FROM acadgeral__disciplina_curso adc
        LEFT JOIN campus_curso USING(curso_id)
        LEFT JOIN acadperiodo__turma USING(cursocampus_id)
        WHERE acadperiodo__turma.turma_id=:turmaId AND adc.disc_id=:disciplinaId';

        $result = $this->executeQueryWithParam($sql, ['turmaId' => $turmaId, 'disciplinaId' => $disciplinaId])->fetch();

        return $result['disciplinaCursoId'] ? $result['disciplinaCursoId'] : null;
    }

    public function pesquisaPelaDisciplina($disciplinaId)
    {
        $objAcadgeralDisciplinaCurso = $this->getRepository();
        $arrResult                   = $objAcadgeralDisciplinaCurso->findBy(array('disc' => $disciplinaId));

        return $arrResult;
    }

    public function retornaCursoIdPelaDisciplina($disciplinaId)
    {
        $arrCursos                      = array();
        $arrObjAcadgeralDisciplinaCurso = $this->pesquisaPelaDisciplina($disciplinaId);

        /** @var \Matricula\Entity\AcadgeralDisciplinaCurso $objAcadgeralDisciplinaCurso */
        foreach ($arrObjAcadgeralDisciplinaCurso as $objAcadgeralDisciplinaCurso) {
            $arrCursos[] = $objAcadgeralDisciplinaCurso->getCurso()->getCursoId();
        }

        return $arrCursos;
    }

    /**
     * Vincula vários cursos em uma disciplina
     * @param                                       $arrCursos
     * @param \Matricula\Entity\AcadgeralDisciplina $objDisciplina
     * @return bool
     */
    public function salvarArray($arrCursos, \Matricula\Entity\AcadgeralDisciplina $objDisciplina)
    {
        $resultDiscCursos = [];

        foreach ($arrCursos as $cursoId => $cadaCursoDisc) {

            $resultDiscCursos[] = $this->save([
               'disc' => $objDisciplina->getDiscId(),
               'curso' => $cursoId,
               'discAtivo' => $cadaCursoDisc['disc_ativo'],
               'tdisc' => $cadaCursoDisc['tdisc_id'],
            ]);
        }

        if($resultDiscCursos){
            //fazendo um dql pra deletar todos os curso disciplina que não enviei no array (que não adicionei ou editei)
            //ou seja, um curso disciplina que não existe mais
            $qb                = $this->getRepository()->createQueryBuilder("discCurso");
            $qb->delete()
                ->where($qb->expr()->notIn('discCurso.discCursoId', $resultDiscCursos))
                ->andWhere($qb->expr()->eq('discCurso.disc', $objDisciplina->getDiscId()))
                ->getQuery()
                ->getResult();
        }

        return true;
    }

}
