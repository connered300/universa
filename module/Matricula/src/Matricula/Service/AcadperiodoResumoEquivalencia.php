<?php

namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

class AcadperiodoResumoEquivalencia extends AbstractService
{
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadperiodoResumoEquivalencia');
    }

    public function pesquisaForJson($params)
    {
        $sql            = 'SELECT * FROM acadperiodo__resumo_equivalencia WHERE';
        $discId         = false;
        $equivalenciaId = false;

        if ($params['q']) {
            $discId = $params['q'];
        } elseif ($params['query']) {
            $discId = $params['query'];
        }

        if ($params['equivalenciaId']) {
            $equivalenciaId = $params['equivalenciaId'];
        }

        $parameters = array('disc_id' => "{$discId}%");
        $sql .= ' disc_id LIKE :disc_id';

        if ($equivalenciaId) {
            $parameters['equivalencia_id'] = $equivalenciaId;
            $sql .= ' AND equivalencia_id <> :equivalencia_id';
        }

        $sql .= " ORDER BY disc_id";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceAcadgeralAlunoFormacao = new \Matricula\Service\AcadgeralAlunoFormacao($this->getEm());
        $serviceAcadperiodoAlunoResumo = new \Matricula\Service\AcadperiodoAlunoResumo($this->getEm());
        $serviceAcadgeralDisciplina    = new \Matricula\Service\AcadgeralDisciplina($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['equivalenciaId']) {
                $objAcadperiodoResumoEquivalencia = $this->getRepository()->find($arrDados['equivalenciaId']);

                if (!$objAcadperiodoResumoEquivalencia) {
                    $this->setLastError('Registro de equivalência não existe!');

                    return false;
                }
            } else {
                $objAcadperiodoResumoEquivalencia = new \Matricula\Entity\AcadperiodoResumoEquivalencia();
            }

            $objAcadperiodoResumoEquivalencia->setEquivalenciaId($arrDados['equivalenciaId']);

            if ($arrDados['formacaoId']) {
                $objAcadgeralAlunoFormacao = $serviceAcadgeralAlunoFormacao->getRepository()->find(
                    $arrDados['formacaoId']
                );

                if (!$objAcadgeralAlunoFormacao) {
                    $this->setLastError('Registro de formação não existe!');

                    return false;
                }

                $objAcadperiodoResumoEquivalencia->setFormacao($objAcadgeralAlunoFormacao);
            }

            if ($arrDados['resId']) {
                $objAcadperiodoAlunoResumo = $serviceAcadperiodoAlunoResumo->getRepository()->find($arrDados['resId']);

                if (!$objAcadperiodoAlunoResumo) {
                    $this->setLastError('Registro de resumo não existe!');

                    return false;
                }

                $objAcadperiodoResumoEquivalencia->setRes($objAcadperiodoAlunoResumo);
            }

            if ($arrDados['discId']) {
                $objAcadgeralDisciplina = $serviceAcadgeralDisciplina->getRepository()->find($arrDados['discId']);

                if (!$objAcadgeralDisciplina) {
                    $this->setLastError('Registro de acadgeral__disciplina não existe!');

                    return false;
                }

                $objAcadperiodoResumoEquivalencia->setDisc($objAcadgeralDisciplina);
            }

            $this->getEm()->persist($objAcadperiodoResumoEquivalencia);
            $this->getEm()->flush($objAcadperiodoResumoEquivalencia);

            $this->getEm()->commit();

            $arrDados['equivalenciaId'] = $objAcadperiodoResumoEquivalencia->getEquivalenciaId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possãvel salvar o registro de acadperiodo__resumo_equivalencia!');
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['formacaoId']) {
            $errors[] = 'Por favor preencha o campo "formação"!';
        }

        if (!$arrParam['resId']) {
            $errors[] = 'Por favor preencha o campo "resumo"!';
        }

        if (!$arrParam['discId']) {
            $errors[] = 'Por favor preencha o campo "disciplina"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    public function getArray($equivalenciaId)
    {
        $arrDados = $this->getRepository()->find($equivalenciaId);

        try {
            $arrDados = $arrDados->toArray();
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('equivalenciaId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Matricula\Entity\AcadperiodoResumoEquivalencia */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getEquivalenciaId(),
                $params['value'] => $objEntity->getDisc()->getDiscNome()
            );
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['equivalenciaId']) {
            $this->setLastError(
                'Para remover um registro de equivalência é necessário especificar o código.'
            );

            return false;
        }

        try {
            $objAcadperiodoResumoEquivalencia = $this->getRepository()->find($param['equivalenciaId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAcadperiodoResumoEquivalencia);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de acadperiodo__resumo_equivalencia.');

            return false;
        }

        return true;
    }
}
?>