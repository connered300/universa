<?php


namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

class AcadperiodoIntegradoraCaderno extends AbstractService{
    public function __construct(\Doctrine\ORM\EntityManager $em){
        parent::__construct($em, 'Matricula\Entity\AcadperiodoIntegradoraCaderno');
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {

    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {

    }

    public function adicionar( \Matricula\Entity\AcadperiodoIntegradoraCaderno $integradoraCaderno)
    {
        $this->begin();
        try {
            $this->getEm()->persist($integradoraCaderno);
            $this->getEm()->flush();
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
        }
        $this->commit();
        return $integradoraCaderno;
    }

    public function buscaCadernosPorAvaliacao($integavaliacao = null)
    {
        if($integavaliacao){
            $cadernos = $this->getRepository()->findBy(['integavaliacao' => $integavaliacao]);

            if($cadernos){
                return $cadernos;
            }

        }
        return null;
    }

    public function buscasDisciplinasCaderno(\Matricula\Entity\AcadperiodoIntegradoraCaderno $caderno)
    {
        $query = "
          SELECT
            disc_id*1 as disc_id
          FROM
            acadperiodo__integradora_gabarito
          NATURAL JOIN
            acadgeral__disciplina
          WHERE
            integcaderno_id = {$caderno->getIntegcadernoId()}
          GROUP BY disc_id
          ORDER BY gabarito_ordem
        ";

        $result = $this->executeQuery($query);

        if($result->rowCount() > 0){
            $result = $result->fetchAll();

            $disciplinas = array();

            foreach ($result as $disciplina) {
                $disciplinas[] = $disciplina['disc_id'];
            }


            return $disciplinas;
        }

        return $result;
    }
}