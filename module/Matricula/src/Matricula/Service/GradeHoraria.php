<?php

namespace Matricula\Service;

use VersaSpine\Service\AbstractService;
use Zend\Form\Element\DateTime;

class GradeHoraria extends AbstractService
{
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadgeralHorarioGradePadrao');
    }

    protected function valida($dados)
    {
        // TODO: Implement valida() method.
    }

    protected function pesquisaForJson($params)
    {
        $sql = 'SELECT * FROM acadgeral__horario_grade__padrao WHERE';

        if ($params['q']) {
            $cursoCampus = $params['q'];
        } elseif ($params['query']) {
            $cursoCampus = $params['query'];
        }

        if ($params['cursocampus_id']) {
            $cursoCampus = $params['cursocampus_id'];
        }

        $parameters = array('cursocampus_id' => $cursoCampus);
        $sql .= ' cursocampus_id = :cursocampus_id';

        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function pesquisaForJsonTurma($params, $turmaTipo)
    {
        $parameters = array();
        $sql        = 'SELECT * FROM acadperiodo__turma WHERE tturma_id = ' . $turmaTipo . ' AND';

        if ($params['q']) {
            $turmaNome = $params['q'];
        } elseif ($params['query']) {
            $turmaNome = $params['query'];
        }

        if ($params['perId']) {
            $parameters['per_id'] = $params['perId'];
            $sql .= ' per_id = :per_id AND ';
        }

        if($params['campusCurso']){
            $parameters['cursocampus_id'] = $params['campusCurso'];
            $sql .= ' cursocampus_id = :cursocampus_id AND ';
        }

        $parameters['turma_nome'] = "%{$turmaNome}%";

        $sql .= ' turma_nome LIKE :turma_nome';
        $sql .= " ORDER BY turma_serie, turma_nome ASC";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function pesquisaForJsonDisciplina($params, $turmaTipo)
    {
        $parameters = array();

        $sql = 'SELECT
                    disciplina.disc_id disc_id,
                    disciplina.disc_nome disc_nome
                FROM
                   acadperiodo__matriz_curricular matriz
                       INNER JOIN
                   acadperiodo__turma turma ON turma.mat_cur_id = matriz.mat_cur_id
                       INNER JOIN
                   acadperiodo__matriz_disciplina MatrizDisc ON MatrizDisc.mat_cur_id = matriz.mat_cur_id
                       INNER JOIN
                   acadgeral__disciplina disciplina ON disciplina.disc_id = MatrizDisc.disc_id
                WHERE tturma_id = ' . $turmaTipo . ' AND turma_serie = per_disc_periodo';

        if ($params['q']) {
            $discNome = $params['q'];
        } elseif ($params['query']) {
            $discNome = $params['query'];
        }

        if ($params['discNome']) {
            $parameters['disc_nome'] = "%{$params['discNome']}%";
            $sql .= ' AND disc_nome LIKE :disc_nome';
        }

        if ($params['turmaId']) {
            $parameters['turma_id'] = $params['turmaId'];
            $sql .= ' AND turma_id = :turma_id';
        }

        $sql .= " group by turma_serie,matriz.mat_cur_id, disciplina.disc_id";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function pesquisaForJsonCampus($params)
    {
        $sql = 'select * from campus_curso JOIN org_campus USING(camp_id) WHERE';

        if ($params['q']) {
            $campus = $params['q'];
        } elseif ($params['query']) {
            $campus = $params['query'];
        }

        $parameters = array('camp_nome' => "%{$campus}%");
        $sql .= ' camp_nome LIKE :camp_nome';

        $sql .= " GROUP BY org_campus.camp_id ";
        $sql .= " ORDER BY camp_nome";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function pesquisaForJsonCursos($params)
    {
        $parameters = array();
        $sql        = 'select * from campus_curso JOIN org_campus USING(camp_id) JOIN acad_curso USING(curso_id) WHERE';

        if ($params['q']) {
            $campus = $params['q'];
        } elseif ($params['query']) {
            $campus = $params['query'];
        }

        if ($params['campId']) {
            $parameters['camp_id'] = $params['campId'];
            $sql .= ' camp_id = :camp_id AND';
        }

        $parameters['camp_nome'] = "%{$campus}%";

        $sql .= ' camp_nome LIKE :camp_nome';
        $sql .= " ORDER BY camp_nome";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function pesquisaForJsonGradeDisciplinas($params)
    {
        $parameters = array();
        $sql        = '
        SELECT
            --     turma.turma_id,
            turma.turma_nome,
            turma.turma_turno,
            discgrade.disc_id,
            disciplina.disc_nome,
            --     grade.grade_id,
            discgrade.gradedisc_dia,
            gradedisc_hora
        FROM acadperiodo__grade grade
        INNER JOIN acadperiodo__turma turma ON grade.turma_id = turma.turma_id
        LEFT JOIN acadperiodo__disciplina_grade discgrade on grade.grade_id = discgrade.grade_id
        LEFT JOIN acadgeral__disciplina disciplina ON disciplina.disc_id = discgrade.disc_id
        WHERE grade.grade_fim is null AND';

        if ($params['turma_id']) {
            $parameters['turma_id'] = $params['turma_id'];
            $sql .= ' turma.turma_id = :turma_id';
        }

        $sql .= " ORDER BY turma.turma_serie, turma.turma_turno, turma.turma_ordem, gradedisc_dia,gradedisc_hora";
        $sql .= " LIMIT 0,40";
        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save($array)
    {
        $service          = new \Professor\Service\AcadperiodoGrade($this->getEm());
        $serviceDiscGrade = new \Professor\Service\AcadperiodoDisciplinaGrade($this->getEm());
        $serviceTurma     = new \Matricula\Service\AcadperiodoTurma($this->getEm());

        if ($array['dados']) {

            try {
                $this->getEm()->beginTransaction();

                if ($array['turma']) {

                    /** @var \Matricula\Entity\AcadperiodoTurma $objTurma */
                    $objTurma  = $serviceTurma->getRepository()->find($array['turma']);
                    $objGrade  = $service->getRepository()->findOneBy(['turma' => $objTurma], ['gradeId' => 'desc']);
                    $novaGrade = new \Professor\Entity\AcadperiodoGrade();

                    $novaGrade->setGradeInicio(new \DateTime('now'));
                    $novaGrade->setTurma($objTurma);

                    $this->getEm()->persist($novaGrade);
                    $this->getEm()->flush($novaGrade);

                    if ($objGrade) {
                        $objGrade->setGradeCiclo($novaGrade);
                        $objGrade->setGradeFim(new \DateTime('now'));

                        $this->getEm()->persist($objGrade);
                        $this->getEm()->flush($objGrade);
                    }

                    foreach ($array['dados'] as $horario => $discId) {
                        $result  = $this->distinctNumberFromDay($horario);
                        $objHora = new \DateTime($result['hora']);
                        /** @var \Professor\Entity\AcadperiodoDisciplinaGrade $objDiscGrade */
                        $objDiscGrade = $serviceDiscGrade->getRepository()->findOneBy(
                            [
                                'gradeId'       => $novaGrade->getGradeId(),
                                'gradediscDia'  => $result['dia'],
                                'gradediscHora' => $objHora
                            ]
                        );

                        if (!$objDiscGrade) {
                            $objDiscGrade = new \Professor\Entity\AcadperiodoDisciplinaGrade();
                        } else {
                            $this->getEm()->remove($objDiscGrade);
                            $this->getEm()->flush($objDiscGrade);
                        }

                        $objDiscGrade->setGradeId($novaGrade->getGradeId());
                        $objDiscGrade->setDiscId($discId);
                        $objDiscGrade->setGradediscHora($objHora);
                        $objDiscGrade->setGradediscDia($result['dia']);

                        $this->getEm()->persist($objDiscGrade);
                        $this->getEm()->flush($objDiscGrade);
                    }
                }

                $this->getEm()->commit();

                return true;
            } catch (\Exception $e) {
                $this->getEm()->rollback();
            }
        }

        return false;
    }

    public function distinctNumberFromDay($string)
    {
        $result = false;
        $arr    = [];
        for ($i = 0; $i < strlen($string); $i++) {
            if ($string[$i] != '0' && $string[$i] != '1' && $string[$i] != '2') {
                $result[$i] = $string[$i];
            } else {
                break;
            }
        }
        $arr['dia']  = implode($result);
        $arr['hora'] = str_replace('_', ':', str_replace($arr['dia'], '', $string));

        return $arr;
    }
}