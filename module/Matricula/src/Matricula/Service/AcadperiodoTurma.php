<?php

namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

class AcadperiodoTurma extends AbstractService
{
    const CONVENCIONAL  = 1;
    const DEPENDENCIA   = 2;
    const ADAPTACAO     = 3;
    const REINGRESSO    = 4;
    const OPTATIVA      = 5;

    private $lastError = null;

    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadperiodoTurma');
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    public function pesquisaForJson($params)
    {
        $servicePeriodoLetivo = new \Matricula\Service\AcadperiodoLetivo($this->getEm());

        $sql = "
        SELECT
            turma.*
            -- select --
        FROM
        acadperiodo__turma turma
        LEFT JOIN acadperiodo__matriz_disciplina amd ON
            amd.mat_cur_id = turma.mat_cur_id AND turma_serie = per_disc_periodo
        -- JOIN --
        WHERE 1  ";

        $parameters = array();
        $join       = "";
        $orderBy    = ' GROUP BY turma_id ';

        if ($params['docente']) {
            $periodo = $servicePeriodoLetivo->buscaPeriodoCorrente(true, true);
            $periodo = implode(',', $periodo);

            $sql = str_replace(
                '-- select --',
                '  ,CONCAT(curso_nome, " / ", turma_nome , " / ",ad.disc_nome) descricao, CONCAT(turma_id,"-",ad.disc_id) id',
                $sql
            );

            $join .= ' 
             INNER JOIN acadperiodo__docente_disciplina adoc USING(turma_id)
             INNER JOIN acadgeral__disciplina ad ON ad.disc_id=adoc.disc_id
             INNER JOIN campus_curso USING(cursocampus_id)
             INNER JOIN acad_curso USING(curso_id)            
                  ';

            $orderBy = ' GROUP BY turma_id, disc_id';

            $sql .= " AND adoc.docente_id=:docenteId AND docdisc_data_fim IS NULL";

            $parameters['docenteId'] = $params['docente'];

            if ($periodo) {
                $sql .= " AND turma.per_id in (" . $periodo . ")";
            }
        }

        if ($params['query']) {
            $sql .= " AND turma_nome like :query ";
            $parameters['query'] = '%' . $params['query'] . '%';
        }

        if ($params['perId']) {
            $sql .= " AND per_id in ( :perId )";
            $parameters['perId'] = $params['perId'];
        }

        if ($params['cursoId']) {
            $join .= " left join campus_curso using(cursocampus_id)";
            $sql .= " AND curso_id in (:cursoId )";
            $parameters['cursoId'] = (
            !is_array($params['cursoId']) ? explode(',', $params['cursoId']) : $params['cursoId']
            );
        }

        if ($params['turma']) {
            $sql .= " AND turma_id=:turmaId ";
            $parameters['turmaId'] = $params['turma'];
        }

        if ($params['turmaSerie']) {
            $sql .= " AND turma_serie IN (:turmaSerie )";
            $parameters['turmaSerie'] = $params['turmaSerie'];
        }

        if ($params['discId']) {
            $sql .= " AND amd.disc_id in (:discId) ";
            $parameters['discId'] = $params['discId'];
        }

        if ($params['cursocampusId']) {
            $sql .= " AND cursocampus_id in (:cursocampusId )";
            $parameters['cursocampusId'] = (
            is_array($params['cursocampusId']) ? $params['cursocampusId'] : explode(',', $params['cursocampusId'])
            );
        }

        if ($params['apenasTurmasComDisciplina']) {
            $join .= " LEFT JOIN acadperiodo__matriz_disciplina matriz on matriz.mat_cur_id = amd.mat_cur_id";
            $sql .= " AND matriz.disc_id IS NOT NULL ";
        }

        $sql = str_replace("-- JOIN --", $join, $sql);

        $sql .= $orderBy;
        $sql .= " ORDER BY turma_serie ASC ";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public static function tipoTurmasRegulares()
    {
        return
            [
                self::CONVENCIONAL,
                self::OPTATIVA,
            ];
    }

    public function buscaTurmas($cursoCampusId, $periodo)
    {
        $serviceAcadperiodoLetivo = new \Matricula\Service\AcadperiodoLetivo($this->getEm());
        $objPerioodoAutal         = $serviceAcadperiodoLetivo->buscaPeriodoCorrente();

        $turma = $this->executeQuery(
            "
        SELECT *
        FROM acadperiodo__turma
        NATURAL JOIN acadperiodo__letivo
        WHERE
          acadperiodo__turma.per_id = " . $objPerioodoAutal->getPerId() . "
        ORDER BY turma_serie,turma_nome
        "
        )->fetchAll();

        return $turma;
    }

    public function buscaAlunosTurma(
        $turmaId,
        $situacaoId = 1,
        $per_id,
        $turno = null,
        $periodoSerie = null,
        $formaIngresso,
        $ingrassantes = false,
        $turmaIrregulares = true,
        $campusCurso = null
    ) {
        if ($campusCurso) {
            $campCursoId = " AND acadperiodo__turma.cursocampus_id = {$campusCurso} ";
        }

        if (!empty($turmaId) AND $turmaId !== 0) {
            $turmaId = "and turma_id = {$turmaId}  ";
        } elseif ($turmaId == 0) {
            $turmaId = '';
        }

        if (!is_null($situacaoId)) {
            $situacaoId = "and matsituacao_id = {$situacaoId}";
        }
        if (!is_null($per_id)) {
            $per_id = "and acadperiodo__turma.per_id = {$per_id}";
        }

        if (!is_null($turno) && $turno != "Todos") {
            $turno = "and acadperiodo__turma.turma_turno LIKE '%{$turno}%' ";
        } else {
            $turno = "";
        }

        if (!is_null($periodoSerie) && $periodoSerie != 'Todos') {
            $periodoSerie = "and acadperiodo__turma.turma_serie = {$periodoSerie}";
        } else {
            $periodoSerie = "";
        }

        if ($ingrassantes === true) {
            $dataIngresso = "AND DATE(acadgeral__aluno.aluno_data_cadastro) >= DATE(acadperiodo__letivo.per_matricula_inicio) ";
        }

        if ($turmaIrregulares === false) {
            $turmaIrregular = "AND acadgeral__turma_tipo.tturma_descricao = 'Convencional'  ";
        }

        $alunos = $this->executeQuery(
            "
        SELECT *
        FROM
        acadperiodo__turma
        NATURAL JOIN
          acadperiodo__aluno
        NATURAL JOIN
          acadgeral__aluno_curso
        LEFT JOIN
          acadgeral__aluno ON acadgeral__aluno_curso.aluno_id = acadgeral__aluno.aluno_id
        NATURAL JOIN
        pessoa
        NATURAL JOIN
        acadperiodo__letivo
        LEFT JOIN
        acadgeral__turma_tipo on acadperiodo__turma.tturma_id = acadgeral__turma_tipo.tturma_id
        WHERE 	tiposel_id like '%{$formaIngresso}%'
          {$turmaId}  {$situacaoId}  {$per_id} {$periodoSerie} {$turno} {$dataIngresso} {$turmaIrregular} {$campCursoId}
        ORDER by
          pes_nome,turma_serie,turma_nome "
        )->fetchAll();

        return $alunos;
    }

    public function buscaAlunosTurmaDisciplina($turmaId, $disc, $situacao = array(), $param = array())
    {
        $in = $situacao[0];

        for ($i = 1; $i < count($situacao); $i++) {
            $in = $in . ',' . $situacao[$i];
        }
        if (!is_null($in)) {
            $andSituacao = "AND acadperiodo__aluno_disciplina.situacao_id IN({$in})";
        }

        $resticoes = '';

        if ($param['matSituacao']) {
            $resticoes .= " AND acadperiodo__aluno.matsituacao_id IN ({$param['matSituacao'] })";
        }

        if ($param['alunoCursoSituacao']) {
            $resticoes .= " AND acadgeral__aluno_curso.alunocurso_situacao={$param['alunoCursoSituacao']}";
        }

        $alunos = $this->executeQuery(
            "
        SELECT *
        FROM
        acadperiodo__turma
        NATURAL JOIN
          acadperiodo__aluno
        INNER JOIN
          acadperiodo__aluno_disciplina
          ON
          acadperiodo__aluno.alunoper_id = acadperiodo__aluno_disciplina.alunoper_id
        INNER JOIN
          acadgeral__situacao
          ON
          acadgeral__situacao.situacao_id = acadperiodo__aluno_disciplina.situacao_id
        LEFT JOIN
          acadgeral__aluno_curso
          ON
          acadperiodo__aluno.alunocurso_id = acadgeral__aluno_curso.alunocurso_id
        LEFT JOIN
          acadgeral__aluno
          ON
          acadgeral__aluno.aluno_id = acadgeral__aluno_curso.aluno_id
        LEFT JOIN
        pessoa
          ON
        acadgeral__aluno.pes_id = pessoa.pes_id
        WHERE
          acadperiodo__aluno_disciplina.turma_id = {$turmaId}
          {$andSituacao}
          AND
           acadperiodo__aluno_disciplina.disc_id = {$disc}
           
          {$resticoes}
        ORDER by
          pes_nome "
        )->fetchAll();

        return $alunos;
    }

    public function buscaDocenteTurmaDisciplina($docenteDisc)
    {
        return $this->executeQuery(
            "SELECT acadperiodo__docente_disciplina.docente_id docente_id,
                    acadgeral__disciplina.disc_id disc_id,
                    acadgeral__disciplina.disc_nome,
                    a.turma_id turma_id,
                    a.turma_nome turma_nome,
                    a.turma_turno turma_turno,
                    a.turma_serie turma_serie,
                    (IFNULL(per_disc_chpratica,0)  +  IFNULL(per_disc_chteorica,0)  + IFNULL(per_disc_chestagio,0))  carga_horaria,
                    acadperiodo__matriz_disciplina.per_disc_creditos aulas,
                    acad_curso.curso_nome curso_nome,
                    acadperiodo__letivo.per_nome per_nome,
                    date_format(docdisc_data_fechamento,'%d-%m-%Y %h:%m')  docdisc_data_fechamento,
                    date_format(docdisc_data_fechamento_final,'%d-%m-%Y %h:%m')  docdisc_data_fechamento_final,
                    pessoa.pes_nome pes_nome,
                    coordenador.pes_nome coordenador_nome
                                      FROM
                                        acadperiodo__docente_disciplina
                                        LEFT JOIN acadperiodo__turma a on acadperiodo__docente_disciplina.turma_id = a.turma_id
                                        LEFT JOIN campus_curso c3 on a.cursocampus_id = c3.cursocampus_id
                                        LEFT JOIN acad_curso on c3.curso_id = acad_curso.curso_id
                                        LEFT JOIN acadgeral__disciplina on acadperiodo__docente_disciplina.disc_id = acadgeral__disciplina.disc_id
                                        LEFT JOIN acadgeral__docente on acadperiodo__docente_disciplina.docente_id = acadgeral__docente.docente_id
                                        LEFT JOIN pessoa on acadgeral__docente.pes_id = pessoa.pes_id
                                        LEFT JOIN acadperiodo__letivo on a.per_id = acadperiodo__letivo.per_id
                                        LEFT JOIN acadperiodo__matriz_disciplina on a.mat_cur_id = acadperiodo__matriz_disciplina.mat_cur_id and acadperiodo__matriz_disciplina.disc_id = acadperiodo__docente_disciplina.disc_id
                                        LEFT JOIN pessoa coordenador on coordenador.pes_id = acad_curso.pes_id
                                    WHERE
                                        docdisc_id = {$docenteDisc}"
        )->fetch();
    }

    /**
     * Este método atualizado dados de uma entidade
     * @param array $dados
     * @return type
     * @throws \Exception
     */
    public function edita(array $dados)
    {
        $per         = $dados['per'];
        $tturma      = $dados['tturma'];
        $matCur      = $dados['matCur'];
        $sala        = $dados['sala'];
        $cursocampus = $dados['cursocampus'];
        $unidade     = $dados['unidade'];

        $matriz               = $dados['matCur'];
        $dados['disc']        = null;
        $dados['turmaOrigem'] = null;
        $dados['per']         = $this->getRepository('Matricula\Entity\AcadperiodoLetivo')->findOneBy(
            ['perId' => $per]
        );
        $dados['tturma']      = $this->getRepository('Matricula\Entity\AcadgeralTurmaTipo')->findOneBy(
            ['tturmaId' => $tturma]
        );
        $dados['matCur']      = $this->getRepository('Matricula\Entity\AcadperiodoMatrizCurricular')->findOneBy(
            ['matCurId' => $matCur]
        );
        $dados['sala']        = $this->getRepository('Infraestrutura\Entity\InfraSala')->findOneBy(['salaId' => $sala]);
        $dados['cursocampus'] = $this->getRepository('Matricula\Entity\CampusCurso')->findOneBy(
            ['cursocampusId' => $cursocampus]
        );
        $dados['unidade']     = $this->getRepository('Organizacao\Entity\OrgUnidadeEstudo')->findOneBy(
            ['unidadeId' => $unidade]
        );

        $serviceMatrizCurricular     = new \Matricula\Service\AcadperiodoMatrizCurricular($this->getEm());
        $serviceSisFilaIntegracao    = new \Sistema\Service\SisFilaIntegracao($this->getEm());
        $serviceDisciplinaIntegracao = new \Sistema\Service\SisIntegracaoDisciplina($this->getEm());

        /** @var \Matricula\Entity\AcadperiodoTurma $entity */
        $entity = $this->getReference($dados['turmaId']);

        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($dados, $entity);

        try {
            $this->getEm()->persist($entity);
            $this->getEm()->flush($entity);

            $disciplinas = $serviceMatrizCurricular->retornaDisciplinaCursoPelaMatrizSerie(
                $matriz,
                $dados['turmaSerie']
            );

            if (!$disciplinas) {
                $this->setLastError(
                    'Não foi possivel integrar as disciplinas, dessa matriz curricular!<br>' .
                    'Verifique se as disciplinas estão vinculadas a um curso!'
                );
            } else {
                /* Manter a semântica de funcionamento!*/

                foreach ($disciplinas as $disciplinaId) {
                    foreach ($disciplinaId as $discCurId) {
                        /** @var $objDisciplinaIntegracao \Sistema\Entity\SisIntegracaoDisciplina */
                        $objDisciplinaIntegracao = $serviceDisciplinaIntegracao->getRepository()->findOneBy(
                            ['cursoDisc' => $discCurId]
                        );

                        if (!$objDisciplinaIntegracao) {
                            $serviceSisFilaIntegracao->registrarDisciplinaNaFilaDeIntegracao($discCurId);
                        }
                    }
                }

                return $entity;
            }
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
        }
    }

    public function atualiza($entity)
    {
        try {
            $this->getEm()->persist($entity);
            $this->getEm()->flush();

            return $entity;
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
        }
    }

    public function buscaTurmasRegularesPeriodo($per)
    {
        if (!empty($per)) {
            $qb = $this->getEm()->createQueryBuilder();

            $q = $qb->select(array('u'))
                ->from($this->getEntity(), 'u')
                ->where(
                    $qb->expr()->andX(
                        $qb->expr()->eq('u.per', '?1'),
                        $qb->expr()->isNotNull('u.matCur')
                    )
                )
                ->orderBy('u.turmaNome', 'ASC')
                ->setParameter(1, $per)
                ->getQuery();

            return $q->execute();
        }
    }

    public function geraNomeTurmaProximoPeriodo($turma)
    {
        if ($turma instanceof \Matricula\Entity\AcadperiodoTurma) {
            return strtoupper(
                substr($turma->getCursocampus()->getCurso()->getCursoNome(), 0, 3) . " " .
                ($turma->getTurmaSerie() + 1) . " " .
                substr($turma->getTurmaTurno(), 0, 3) . " " . $turma->getTurmaOrdem()
            );
        }
    }

    public function buscaTurmasOrigemDestinoSugestao(array $data)
    {
        $turmasRegulares = $this->buscaTurmasRegularesPeriodo($data['perOrigem']);
        $turmaDestino    = [];
        $turmas          = [];
        $i               = 0;

        foreach ($turmasRegulares as $turmasRegular) {
            if ($turmasRegular->getTurmaSerie() < 10) {
                $turmaSerie                                 = $turmasRegular->getTurmaSerie() + 1;
                $turmaDestino[$turmaSerie][$i]['turmaId']   = $turmasRegular->getTurmaId();
                $turmaDestino[$turmaSerie][$i]['turmaNome'] = $this->geraNomeTurmaProximoPeriodo($turmasRegular);
                $i++;
            }
        }

        foreach ($turmasRegulares as $turmasRegular) {
            if ($turmasRegular->getTurmaSerie() < 10) {
                $turmas[] = [
                    'origem'  => $turmasRegular->toArray(),
                    'destino' => $turmaDestino[$turmasRegular->getTurmaSerie() + 1]
                ];
            }
        }

        foreach ($turmas as $pos => $turma) {
            $turmas[$pos]['destino'] = array_values($turmas[$pos]['destino']);
        }

        return $turmas;
    }

    public function clonaTurmas(array $data)
    {
        $periodo = $this->getEm()
            ->getPartialReference('\Matricula\Entity\AcadperiodoLetivo', $data['perDest']);
        if ($periodo) {
            $serviceAlunoper = new \Matricula\Service\AcadperiodoAluno($this->getEm());
            $this->begin();
            for ($i = 0; $i < sizeof($data['turmaOrigem']); $i++) {
                $turmaOrigem = $this->getReference($data['turmaDestino'][$i]);
                $turmaExiste = $this->getRepository()->findOneBy(
                    ['turmaNome' => $this->geraNomeTurmaProximoPeriodo($data['turmaOrigem'])]
                );
                if (!empty($turmaExiste)) {
                    $serviceAlunoper->preMatriculaAlunoTurma($turmaOrigem->getTurmaId(), $turmaExiste);
                } else {
                    $turmaDestino = $turmaOrigem->toArrayReal();
                    unset($turmaDestino['turmaId']);
                    $turmaDestino = new \Matricula\Entity\AcadperiodoTurma($turmaDestino);
                    $turmaDestino->setTurmaNome($this->geraNomeTurmaProximoPeriodo($turmaDestino));
                    $turmaDestino->setTurmaSerie($turmaDestino->getTurmaSerie() + 1);
                    $turmaDestino->setPer(
                        $this
                            ->getEm()
                            ->getPartialReference('\Matricula\Entity\AcadperiodoLetivo', $data['perDest'])
                    );

                    try {
                        $this->getEm()->persist($turmaDestino);
                    } catch (\Exception $ex) {
                        throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
                    }
                    $serviceAlunoper->preMatriculaAlunoTurma($turmaOrigem->getTurmaId(), $turmaDestino);
                    unset($turmaDestino);
                    unset($turmaOrigem);
                    unset($turmaExiste);
                }
            }

            $this->getEm()->flush();
            $this->getEm()->clear();
            $this->commit();
        }
    }

    public function buscasDocentesDisciplinasTurmaPorTurma($turmaId)
    {
        $array = array();

        if ($turmaId) {
            $turma = $this->getRepository()->findOneBy(['turmaId' => $turmaId]);

            if ($turma) {
                $disciplinas = $this->getRepository(
                    'Matricula\Entity\AcadperiodoMatrizDisciplina'
                )->buscaDisciplinasPorMatrizCurricular($turma->getMatCur()->getMatCurId(), $turma->getTurmaSerie());

                foreach ($disciplinas as $disciplina) {
                    $docenteDisciplina = $this->getRepository(
                        'Matricula\Entity\AcadperiodoDocenteDisciplina'
                    )->findOneBy(['disc' => $disciplina['disc'], 'turma' => $turmaId, 'docdiscDataFim' => null]);

                    if ($docenteDisciplina) {
                        $disciplina['docenteId'] = $docenteDisciplina->getDocente()->getDocenteId();
                    } else {
                        $disciplina['docenteId'] = "";
                    }
                    $array[] = $disciplina;
                }
            }
        }

        return $array;
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "
        SELECT
            t.*,
            p.per_nome,
            greatest(
                (SELECT count(DISTINCT alunoper_id) FROM acadperiodo__aluno WHERE turma_id=t.turma_id),
                (SELECT count(DISTINCT alunoper_id) FROM acadperiodo__aluno_disciplina WHERE turma_id=t.turma_id)
            ) AS turma_numero_alunos
        FROM acadperiodo__turma t
        LEFT JOIN acadperiodo__letivo p USING(per_id)
        ORDER BY per_id DESC, turma_serie ASC";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function efetuaIntegracaoTurma(\Matricula\Entity\AcadperiodoTurma $objAcadperiodoTurma)
    {
        $serviceSisFilaIntegracao = new \Sistema\Service\SisFilaIntegracao($this->getEm());
        $serviceSisFilaIntegracao->registrarTurmaNaFilaDeIntegracao($objAcadperiodoTurma);

        return false;
    }

    public function verificarErroDeVinculoDisciplinaCursoMatriz(\Matricula\Entity\AcadperiodoTurma $objAcadperiodoTurma)
    {
        $falhaDisciplina = '';

        $serviceCampusCurso     = new \Matricula\Service\CampusCurso($this->getEm());
        $serviceDisciplinaCurso = new \Matricula\Service\AcadgeralDisciplinaCurso($this->getEm());
        
        $arrDisciplinas = $serviceDisciplinaCurso->retornaDisciplinaCursoPelaMatrizSerie(
            $objAcadperiodoTurma->getMatCur()->getMatCurId(),
            $objAcadperiodoTurma->getTurmaSerie(),
            $objAcadperiodoTurma->getTurmaId()
        );

        /** @var $objcampusCurso \Matricula\Entity\CampusCurso */
        $objcampusCurso = $serviceCampusCurso->getRepository()->find(
            $objAcadperiodoTurma->getCursocampus()->getCursocampusId()
        );

        $cursoId = $objcampusCurso->getCurso()->getCursoId();

        foreach ($arrDisciplinas as $disciplinaId) {
            /** @var  $objDisciplinaCurso \Matricula\Entity\AcadgeralDisciplinaCurso */
            $objDisciplinaCurso = $serviceDisciplinaCurso->getRepository()->findOneBy(
                ['curso' => $cursoId, 'disc' => $disciplinaId]
            );

            if (!$objDisciplinaCurso) {
                $falhaDisciplina .= "\nDisciplina: " . $disciplinaId . ' do Curso: ' . $cursoId . "\n";
            }
        }

        $falhaDisciplina = $falhaDisciplina ? 'Disciplinas sem vínculo com curso: ' . $falhaDisciplina : '';

        return $falhaDisciplina;
    }
}