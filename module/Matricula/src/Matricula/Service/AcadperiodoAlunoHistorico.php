<?php

namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

class AcadperiodoAlunoHistorico extends AbstractService
{
    /**
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadperiodoAlunoHistorico');
    }

    /**
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    protected function pesquisaForJson($criterios)
    {
    }

    public function save($objAcadperiodoAluno)
    {
        if (!is_a($objAcadperiodoAluno, '\Matricula\Entity\AcadperiodoAluno')) {
            $this->setLastError('É necessário informar um aluno!');

            return false;
        }

        $servicePeriodoAluno = new \Matricula\Service\AcadperiodoAluno($this->getEm());
        $serviceAcessoPessoa = new \Acesso\Service\AcessoPessoas($this->getEm());

        /** @var $objAcadperiodoAluno \Matricula\Entity\AcadperiodoAluno */
        $objAcadperiodoAluno = $servicePeriodoAluno->getRepository()->find($objAcadperiodoAluno->getAlunoperId());

        /** @var $objAcadperiodoAlunoHistorico \Matricula\Entity\AcadperiodoAlunoHistorico */
        $objAcadperiodoAlunoHistorico = new\Matricula\Entity\AcadperiodoAlunoHistorico();

        try {
            $usuario = $serviceAcessoPessoa->retornaUsuarioLogado();

            if (!$usuario) {
                $usuario = $objAcadperiodoAluno->getUsuarioCriador();
            }

            $objAcadperiodoAlunoHistorico
                ->setAlunoper($objAcadperiodoAluno)
                ->setSituacao($objAcadperiodoAluno->getMatsituacao())
                ->setTurma($objAcadperiodoAluno->getTurma())
                ->setUsuarioHistorico($usuario)
                ->setSituacaoperData((new \DateTime()))
                ->setSituacaoperHistoricoObservacoes($objAcadperiodoAluno->getAlunoperObservacoes());

            $this->getEm()->persist($objAcadperiodoAlunoHistorico);
            $this->getEm()->flush($objAcadperiodoAlunoHistorico);
        } catch (\Exception $e) {
            return [
                "type"     => "error",
                "mensagem" => "Falha ao salvar histórico do aluno, tente novamente, caso persista, entre em contato com o surporte!"
            ];
        }

        return true;
    }
}