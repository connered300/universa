<?php

namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

class AcadgeralDisciplinaTipo extends AbstractService
{
    const DISCIPLINA_TIPO_REGULAR      = 0000000001;
    const DISCIPLINA_TIPO_OPTATIVA     = 0000000002;
    const DISCIPLINA_TIPO_ESTAGIO      = 0000000003;
    const DISCIPLINA_TIPO_EQUIVALENCIA = 0000000004;

    const TDISC_AVALIACAO_HORAS = 'Horas';
    const TDISC_AVALIACAO_NOTAS_E_HORAS = 'Notas e Horas';
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    public static function getTdiscAvaliacao()
    {
        return array(self::TDISC_AVALIACAO_HORAS, self::TDISC_AVALIACAO_NOTAS_E_HORAS);
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadgeralDisciplinaTipo');
    }

    public function pesquisaForJson($params)
    {
        $sql            = 'SELECT * FROM acadgeral__disciplina_tipo WHERE';
        $tdiscAvaliacao = false;
        $tdiscId        = false;

        if ($params['q']) {
            $tdiscAvaliacao = $params['q'];
        } elseif ($params['query']) {
            $tdiscAvaliacao = $params['query'];
        }

        if ($params['tdiscId']) {
            $tdiscId = $params['tdiscId'];
        }

        $parameters = array('tdisc_avaliacao' => "{$tdiscAvaliacao}%");
        $sql .= ' tdisc_avaliacao LIKE :tdisc_avaliacao';

        if ($tdiscId) {
            $parameters['tdisc_id'] = $tdiscId;
            $sql .= ' AND tdisc_id <> :tdisc_id';
        }

        $sql .= " ORDER BY tdisc_avaliacao";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['tdiscId']) {
                $objAcadgeralDisciplinaTipo = $this->getRepository()->find($arrDados['tdiscId']);

                if (!$objAcadgeralDisciplinaTipo) {
                    $this->setLastError('Registro de tipo de disciplina não existe!');

                    return false;
                }
            } else {
                $objAcadgeralDisciplinaTipo = new \Matricula\Entity\AcadgeralDisciplinaTipo();
            }

            $objAcadgeralDisciplinaTipo->setTdiscId($arrDados['tdiscId']);
            $objAcadgeralDisciplinaTipo->setTdiscDescricao($arrDados['tdiscDescricao']);
            $objAcadgeralDisciplinaTipo->setTdiscAvaliacao($arrDados['tdiscAvaliacao']);

            $this->getEm()->persist($objAcadgeralDisciplinaTipo);
            $this->getEm()->flush();

            $this->getEm()->commit();

            $arrDados['tdiscId'] = $objAcadgeralDisciplinaTipo->getTdiscId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de tipo de disciplina!');
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['tdiscId']) {
            $errors[] = 'Por favor preencha o campo "tdiscId"!';
        }

        if (!$arrParam['tdiscDescricao']) {
            $errors[] = 'Por favor preencha o campo "tdiscDescricao"!';
        }

        if (!in_array($arrParam['tdiscAvaliacao'], self::getTdiscAvaliacao())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "tdiscAvaliacao"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM acadgeral__disciplina_tipo";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($tdiscId)
    {
        $arrDados = $this->getRepository()->find($tdiscId);

        try {
            $arrDados = $arrDados->toArray();
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('tdiscId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Matricula\Entity\AcadgeralDisciplinaTipo */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getTdiscId(),
                $params['value'] => $objEntity->getTdiscDescricao()
            );
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['tdiscId']) {
            $this->setLastError(
                'Para remover um registro de tipo de disciplina é necessário especificar o código.'
            );

            return false;
        }

        try {
            $objAcadgeralDisciplinaTipo = $this->getRepository()->find($param['tdiscId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAcadgeralDisciplinaTipo);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de tipo de disciplina.');

            return false;
        }

        return true;
    }

    public function getArrSelect2TdiscAvaliacao($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getTdiscAvaliacao());
    }
}