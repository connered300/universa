<?php

namespace Matricula\Service;

use VersaSpine\Service\AbstractService;

/**
 * Description of Pessoa
 *
 * @author Matheus
 */
class AcadgeralAtividadeLog extends AbstractService
{
    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {
    }

    /**
     * @var null
     */
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Matricula\Entity\AcadgeralAtividadeLog');
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['logData']) {
            $errors[] = 'Por favor preencha o campo correspondente a data"!';
        }

        if (!$arrParam['logAction']) {
            $errors[] = 'Por favor preencha o campo correspondente a action!';
        }

        if (!$arrParam['logUserAgent']) {
            $errors[] = 'Por favor preencha o campo correspondente a assinatura do usuario!';
        }

        if (!$arrParam['logIp']) {
            $errors[] = 'Por favor preencha o campo correspondente ao ip!';
        }

        if (!$arrParam['alunoId']) {
            $errors[] = 'Por favor preencha o campo correspondente ao aluno!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function remover($param)
    {
        if (!$param['logId']) {
            $this->setLastError('Para remover um registro de log é necessário informar o código . ');

            return false;
        }

        try {
            /** @var $objLog \Matricula\Entity\AcadgeralAtividadeLog */
            $objLog = $this->getRepository()->find($param['logId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objLog);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de log . ');

            return false;
        }

        return true;
    }

    public function save($arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            $serviceAcadgeralDisciplina = new AcadgeralDisciplina($this->getEm());
            $serviceAcadgeralAluno      = new AcadgeralAluno($this->getEm());
            $serviceAcadCurso           = new AcadCurso($this->getEm());

            if ($arrDados['logId']) {
                /** @var \Matricula\Entity\AcadgeralAtividadeLog $objLog */
                $objLog = $this->getRepository()->find($arrDados['logId']);

                if (!$objLog) {
                    $this->setLastError("Registro de log não existe");

                    return false;
                }
            } else {
                $objLog = new \Matricula\Entity\AcadgeralAtividadeLog();
            }

            if ($arrDados['alunoId']) {
                /** @var \Matricula\Entity\AcadgeralAluno $objAluno */
                $objAluno = $serviceAcadgeralAluno->getRepository()->find($arrDados['alunoId']);

                if (!$objAluno) {
                    $this->setLastError("Não foi possível encontrar o registro de aluno!");

                    return false;
                }

                $objLog->setAluno($objAluno);
            } else {
                $objLog->setAluno(null);
            }

            if ($arrDados['discId']) {
                /** @var \Matricula\Entity\AcadgeralDisciplina $objDisciplina */
                $objDisciplina = $serviceAcadgeralDisciplina->getRepository()->find($arrDados['discId']);

                if (!$objDisciplina) {
                    $this->setLastError("Não foi possível encontrar o registro de disciplina!");

                    return false;
                }

                $objLog->setDisciplina($objDisciplina);
            } else {
                $objLog->setDisciplina(null);
            }

            if ($arrDados['cursoId']) {
                /** @var \Matricula\Entity\AcadCurso $objCurso */
                $objCurso = $serviceAcadCurso->getRepository()->find($arrDados['cursoId']);

                if (!$objCurso) {
                    $this->setLastError("Não foi possível encontrar o registro de curso!");

                    return false;
                }

                $objLog->setCurso($objCurso);
            } else {
                $objLog->setCurso(null);
            }

            $objLog->setLogData($arrDados['logData']);
            $objLog->setLogAction($arrDados['logAction']);
            $objLog->setLogInfo($arrDados['logInfo']);
            $objLog->setLogUserAgent($arrDados['logUserAgent']);
            $objLog->setLogIp($arrDados['logIp']);

            $this->getEm()->persist($objLog);
            $this->getEm()->flush($objLog);

            $this->getEm()->commit();

            $arrDados['logId'] = $objLog->getLogId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError("Não foi possível salvar o registro de log" . $e->getMessage());
        }

        return false;
    }

    public function retornaLogPeloId($id)
    {
        if (!$id) {
            $this->setLastError("Nenhum registro informado!");

            return false;
        }

        try {
            $objLog = $this->getRepository()->find($id);

            if ($objLog) {
                return $objLog->toArray();
            }
        } catch (\Exception $e) {
            $this->setLastError("Não foi possível localizar o resgistro de log" . $e->getMessage());
        }

        return false;
    }
}
