<?php

namespace Matricula\Form;

use Zend\Form\Form,
    Zend\Form\Element;

class RelatorioAlunos extends Form
{
    public function __construct($turmas)
    {
        parent::__construct('relatorioSalas');

        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', '');
        $this->setAttribute('role', 'form');
        $this->setAttribute('target', '_blank');

        $turmasForm = new Element\Select('turma');
        $turmasForm->setAttributes([
            'label' => 'TURMA:',
            'col' => 4,
            'data-validations' => 'Required'
        ]);
        if ($turmas) {
            $turmasForm->setValueOptions($turmas);
        }
//        $turmaForm->setValue('turma');
        $this->add($turmasForm);

        $tipo = new Element\Radio('tipo');
        $tipo->setAttributes([
            'label' => 'MODELO DO RELATÓRIO:',
            'col' => 4,
            'data-validations' => 'Required'
        ]);
        $tipo->setValueOptions([
            'matricula/relatorios/etiquetas' => 'Etiquetas de Pasta Completa',
            'matricula/relatorios/etiquetas-simples' => 'Etiquetas de Pasta Simples',
            'matricula/relatorios/lista-alunos' => 'Lista dos alunos',
        ]);
        $tipo->setValue('matricula/relatorios/lista-alunos');
        $this->add($tipo);

        $situacao = new Element\Radio('situacao');
        $situacao->setAttributes([
            'label' => 'SITUAÇÃO DOS ALUNOS:',
            'col' => 4,
            'data-validations' => 'Required'
        ]);
        $situacao->setValueOptions([
            'matriculado' => 'Matriculados',
            'Matricula Provisoria'=>'Matricula Provisoria',
            'Pré Matricula'=>'Não Matriculado',
            'Transferencia'=>'Trasferência',
            'cancelada'   => 'Cancelados',
        ]);
        $situacao->setValue('matriculados');
        $this->add($situacao);

        $print = new Element\Submit('Imprimir');
        $print->setAttributes([
            'class' => 'btn btn-primary pull-left',
            'value' => 'Imprimir'
        ]);
        $this->add($print);
    }
}

