<?php


namespace Matricula\Form;

use Zend\Form\Form,
    Zend\Form\Element;

class AcadgeralDisciplina extends Form{
    function __construct($tdisc = array())
    {
        parent::__construct('CadastroDisciplina');
        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', '');
        $this->setAttribute('role', 'form');


        $discId = new Element\Hidden('discId');
        $this->add($discId);

        $tdiscId = new Element\Select('tdiscId');
        $tdiscId->setAttributes([
            'col'              => '3',
            'wrap'             => false,
            'label'            => 'Tipo da Disciplina:',
            'data-validations' => 'Required'
        ]);
        $tdiscId->setValueOptions($tdisc);
        $this->add($tdiscId);

        $discNome = new Element\Text('discNome');
        $discNome->setAttributes([
            'col'              => '3',
            'wrap'             => false,
            'label'            => 'Nome da Disciplina: ',
            'data-validations' => 'Required'
        ]);
        $this->add($discNome);

        $discSigla = new Element\Text('discSigla');
        $discSigla->setAttributes([
            'col'              => '3',
            'wrap'             => false,
            'label'            => 'Sigla:',
            'data-validations' => 'Required'
        ]);
        $this->add($discSigla);

        $discAtivo = new Element\Radio('discAtivo');
        $discAtivo->setAttributes([
            'col'              => '3',
            'wrap'             => false,
            'label'            => 'Ativo:',
            'data-validations' => 'Required'
        ]);
        $discAtivo->setValueOptions(['Sim' => 'Sim', 'Não' => 'Não']);
        $discAtivo->setValue('Sim');
        $this->add($discAtivo);

        $salvar = new Element\Submit('salvar');
        $salvar->setAttributes([
            'value'   => 'Salvar',
            'class'   => 'btn btn-primary pull-left',
        ]);
        $this->add($salvar);

    }
}