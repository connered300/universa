<?php


namespace Matricula\Form;

use Zend\Form\Form,
    Zend\Form\Element;

class AcadperiodoIntegradoraCaderno extends Form
{
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name);

        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', '');
        $this->setAttribute('role', 'form');

    }
}