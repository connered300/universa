<?php


namespace Matricula\Form;

use Pessoa\Form\PessoaFisica;

use Zend\Form\Form,
    Zend\Form\Element;

class AcadperiodoAluno extends PessoaFisica{
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name);

        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', '');
        $this->setAttribute('role', 'form');
        $this->setAttribute('enctype', 'multipart/form-data');

        $pes = new Element\Hidden('pes');
        $this->add($pes);

        $matsituacao = new Element\Hidden('matsituacao');
        $this->add($matsituacao);

        $alunocurso = new Element\Hidden('alunocurso');
        $this->add($alunocurso);

        $turma = new Element\Hidden('turma');
        $this->add($turma);


        $alunoId = new Element\Hidden('alunoId');
        $this->add($alunoId);
        $usuarioCriador = new Element\Hidden('usuarioCriador');
        $this->add($usuarioCriador);

        $alunoMae = new Element\Text('alunoMae');
        $alunoMae->setAttributes([
            'col'      => '4',
            'wrap'     => false,
            'label'    => 'Nome da Mãe: ',
            'placeholder' => 'Informe o nome da mae',
            'icon'     => 'icon-prepend fa fa-user',
        ]);
        $this->add($alunoMae);

        $alunoPai = new Element\Text('alunoPai');
        $alunoPai->setAttributes([
            'col'      => '4',
            'wrap'     => false,
            'label'    => 'Nome do Pai:',
            'placeholder' => 'Informe o nome do Pai',
            'icon'     => 'icon-prepend fa fa-user',
        ]);
        $this->add($alunoPai);

        $turmaNome = new Element\Text('turmaNome');
        $turmaNome->setAttributes([
            'col'      => '2',
            'wrap'     => false,
            'label'    => 'Turma Nome:',
            'readonly'  => 'readonly',
        ]);
        $this->add($turmaNome);

        $turmaTurno = new Element\Text('turmaTurno');
        $turmaTurno->setAttributes([
            'col'      => '2',
            'wrap'     => false,
            'label'    => 'Turma Turno:',
            'readonly'  => 'readonly',
        ]);
        $this->add($turmaTurno);

        $alunoEtnia = new Element\Select('alunoEtnia');
        $alunoEtnia->setAttributes([
            'col'      => '2',
            'wrap'     => false,
            'label'    => 'Etnia: ',
            'required' => 'Required',
            'data-validations'  => 'Required',
        ]);
        $alunoEtnia->setValueOptions(
            array(
                'Branca'                    => 'Branca',
                'Preta'                     => 'Preta',
                'Amarela'                   => 'Amarela',
                'Parda'                     => 'Parda',
                'Indígena'                  => 'Indígena',
                'Não dispõe da informação'  => 'Não dispõe da informação',
                'Não declarado'             => 'Não declarado'
            )
        );
        $this->add($alunoEtnia);

        $alunoCertMilitar = new Element\Text('alunoCertMilitar');
        $alunoCertMilitar->setAttributes([
            'col'               => '2',
            'wrap'              => false,
            'label'             => 'Certificado Militar',
            'placeholder'       => 'Informe o n°',
            'icon'              => 'icon-prepend fa fa-credit-card',
            'maxlength'               => '45'
        ]);
        $this->add($alunoCertMilitar);

        $alunoTituloEleitoral = new Element\Text('alunoTituloEleitoral');
        $alunoTituloEleitoral->setAttributes([
            'col'               => '2',
            'wrap'              => false,
            'label'             => 'Titulo Eleitoral',
            'placeholder'       => 'Informe o n°',
            'icon'              => 'icon-prepend fa fa-credit-card',
            'maxlength'               => 14
        ]);
        $this->add($alunoTituloEleitoral);

        $alunoSecaoEleitoral = new Element\Text('alunoSecaoEleitoral');
        $alunoSecaoEleitoral->setAttributes([
            'col'               => '2',
            'wrap'              => false,
            'label'             => 'Seção Eleitoral',
            'placeholder'       => 'Informe o n°',
            'icon'              => 'icon-prepend fa fa-credit-card',
            'data-validations'  => 'Integer',
            'maxlength'               => 4
        ]);
        $this->add($alunoSecaoEleitoral);

        $alunoZonaEleitoral = new Element\Text('alunoZonaEleitoral');
        $alunoZonaEleitoral->setAttributes([
            'col'               => '2',
            'wrap'              => false,
            'label'             => 'Zona Eleitoral',
            'placeholder'       => 'Informe o n°',
            'icon'              => 'icon-prepend fa fa-credit-card',
            'data-validations'  => 'Integer',
            'maxlength'               => 9
        ]);
        $this->add($alunoZonaEleitoral);

        $this->get("pesNome")->setAttributes(
            array(
                "placeholder" => "Informe o Nome",
                "label" => "Nome:"
            )
        );

        $idTelefone = new Element\Hidden('idTelefone');
        $this->add($idTelefone);
        $idTipoTelefone = new Element\Hidden('idTipoTelefone');
        $this->add($idTipoTelefone);
        $alunoTelefone = new Element\Text('alunoTelefone');
        $alunoTelefone->setAttributes([
            'col'      => '2',
            'wrap'     => false,
            'label'    => 'Numero de telefone:',
            'placeholder' => 'Informe o numero para contato',
            'class'             => 'phone'
        ]);
        $this->add($alunoTelefone);

        $idCelular = new Element\Hidden('idCelular');
        $this->add($idCelular);
        $idTipoCelular = new Element\Hidden('idTipoCelular');
        $this->add($idTipoCelular);
        $alunoCelular = new Element\Text('alunoCelular');
        $alunoCelular->setAttributes([
            'col'      => '2',
            'wrap'     => false,
            'label'    => 'Numero de Celular:',
            'placeholder' => 'Informe o numero do celular',
            'class'             => 'phone'
        ]);
        $this->add($alunoCelular);

        $idEmail = new Element\Hidden('idEmail');
        $this->add($idEmail);
        $idTipoEmail = new Element\Hidden('idTipoEmail');
        $this->add($idTipoEmail);
        $alunoEmail = new Element\Text('alunoEmail');
        $alunoEmail->setAttributes([
            'col'      => '4',
            'wrap'     => false,
            'label'    => 'E-mail:',
            'placeholder' => 'Informe o e-mail para contato',
            'onchange'  => 'validateEmail(this)'
        ]);
        $this->add($alunoEmail);

        $alunoFoto = new Element\File("alunoFoto");
        $alunoFoto->setAttributes([
            'col'      => '4',
            'label'    => 'Foto o Aluno: ',
            'placeholder'   => 'Foto do aluno para ser impressa na carteirinha',
        ]);
        $this->add($alunoFoto);

    }
}