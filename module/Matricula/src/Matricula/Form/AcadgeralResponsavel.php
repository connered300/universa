<?php


namespace Matricula\Form;

use Pessoa\Form\PessoaFisica;
use Zend\Form\Form,
    Zend\Form\Element;

class AcadgeralResponsavel extends PessoaFisica {
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name);

        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', '');
        $this->setAttribute('role', 'form');

        $respId = new Element\Hidden('respId');
        $this->add($respId);

        $pes = new Element\Hidden('pes');
        $this->add($pes);

        $respAluno = new Element\Hidden('aluno');
        $this->add($respAluno);


        $respVinculo = new Element\Select('respVinculo[]');
        $respVinculo->setAttributes([
            'col'      => '2',
            'wrap'     => false,
            'label'    => 'Grau reponsavel: ',
            'required' => 'Required',
        ]);
        $respVinculo->setValueOptions(
            array(
                'Pai'                   => 'Pai',
                'Mãe'                   => 'Mãe',
                'Avo'                   => 'Avo',
                'Irmão'                 => 'Irmão',
                'Responsalvel Legal'    => 'Responsalvel Legal',
                'Outros'                => 'Outros'
            )
        );
        $this->add($respVinculo);

        $respNivel = new Element\Select('respNivel[]');
        $respNivel->setAttributes([
            'col'      => '2',
            'wrap'     => false,
            'label'    => 'Responsável Tipo: ',
            'data-validations' => 'Required'
        ]);
        $respNivel->setValueOptions(
            array(
                'Legal'         => 'Legal',
                'Acadêmico'     => 'Acadêmico',
                'Financeiro'    => 'Financeiro'
            )
        );
        $this->add($respNivel);


        $salvar = new Element\Submit('salvar');
        $salvar->setAttributes([
            'value'   => 'Salvar',
            'class'   => 'btn btn-primary pull-left',
        ]);
        $this->add($salvar);

    }
}