<?php


namespace Matricula\Form;

use Zend\Form\Form,
    Zend\Form\Element;

class AcadperiodoTurma extends Form
{
    public function __construct($name = null)
    {
        parent::__construct($name);

        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', '');
        $this->setAttribute('role', 'form');
        $this->setAttribute('id', 'FormTurma');

        $turmaId = new Element\Hidden('turmaId');
        $this->add($turmaId);

        $turmaId = new Element\Hidden('turmaOrigem');
        $this->add($turmaId);

        $turmaTipo = new Element\Select('tturma');
        $turmaTipo->setAttributes(
            [
                'label'            => 'Tipo da Turma',
                'col'              => 3,
                'data-validations' => 'Required'
            ]
        );
        $this->add($turmaTipo);

        $serie = new Element\Text('turmaSerie');
        $serie->setAttributes(
            [
                'label' => 'Série',
                'col'   => 2,
            ]
        );
        $this->add($serie);

        $turmaOrdem = new Element\Select('turmaOrdem');
        $turmaOrdem->setAttributes(
            [
                'label'            => 'Ordem',
                'col'              => 2,
                'data-validations' => 'Required'
            ]
        );
        $turmaOrdem->setValueOptions(\Matricula\Entity\AcadperiodoTurma::$ordens);
        $this->add($turmaOrdem);

        $turmaCapacidade = new Element\Text('turmaCapacidade');
        $turmaCapacidade->setAttributes(
            [
                'label'            => 'Capacidade',
                'col'              => 2,
                'wrap'             => true,
                'data-validations' => 'Required'
            ]
        );
        $this->add($turmaCapacidade);

        $disc = new Element\Hidden('disc');
        $this->add($disc);

        $cursoCampus = new Element\Select('cursocampus');
        $cursoCampus->setAttributes(
            [
                'label'            => 'Curso/Câmpus',
                'col'              => 3,
                'data-validations' => 'Required'
            ]
        );
        $this->add($cursoCampus);

        $per = new Element\Select('per');
        $per->setAttributes(
            [
                'label'            => 'Período Letivo',
                'col'              => 3,
                'data-validations' => 'Required'
            ]
        );
        $this->add($per);

        $matCur = new Element\Select('matCur');
        $matCur->setAttributes(
            [
                'label' => 'Matriz Curricular',
                'col'   => 3,
                'wrap'  => true
            ]
        );
        $this->add($matCur);

        $sala = new Element\Select('sala');
        $sala->setAttributes(
            [
                'label' => 'Sala',
                'col'   => 3,
            ]
        );
        $this->add($sala);

        $turmaTurno = new Element\Select('turmaTurno');
        $turmaTurno->setAttributes(
            [
                'label'            => 'Turno',
                'col'              => 3,
                'data-validations' => 'Required'
            ]
        );
        $turmaTurno->setValueOptions(
            [
                'Matutino'   => 'Matutino',
                'Vespertino' => 'Vespertino',
                'Noturno'    => 'Noturno',
                'Integral'   => 'Integral',
            ]
        );
        $this->add($turmaTurno);

        $nome = new Element\Text('turmaNome');
        $nome->setAttributes(
            [
                'label'            => 'Nome da Turma',
                'col'              => 3,
                'data-validations' => 'Required',
                'wrap'             => true
            ]
        );
        $this->add($nome);

        $unidade = new Element\Select('unidade');
        $unidade->setAttributes(
            [
                'label' => 'Unidade da Turma',
                'col'   => 3,

            ]
        );
        $this->add($unidade);

        $salvar = new Element\Submit('Salvar');
        $salvar->setAttributes(
            [
                'class' => 'btn btn-primary pull-right',
                'value' => 'Salvar'
            ]
        );
        $this->add($salvar);
    }
}