<?php


namespace Matricula\Form;

use Zend\Form\Form,
    Zend\Form\Element;

class AcadperiodoIntegradoraAvaliacao extends Form
{
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name);

        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', '');
        $this->setAttribute('role', 'form');

        $integcadernoId = new Element\Hidden('integcadernoId');
        $this->add($integcadernoId);

        $integavaliacaoId = new Element\Hidden('integavaliacaoId');
        $this->add($integavaliacaoId);

        $integradora = new Element\Hidden('integradora');
        $this->add($integradora);

        $integcadernoNome = new Element\Text('integcadernoNome');
        $integcadernoNome->setAttributes(
            [
                'col'              => '4',
                'wrap'             => false,
                'label'            => 'Caderno nome: ',
                'placeholder'      => 'Informe o nome do caderno',
                //            'icon'     => 'icon-prepend fa fa-user',
                'data-validations' => 'Required',
            ]
        );
        $this->add($integcadernoNome);

        //        $quantidadeQuestoes = new Element\Text('quantidadeQuestoes');
        //        $quantidadeQuestoes->setAttributes([
        //            'col'      => '1',
        //            'wrap'     => false,
        //            'label'    => 'N. questões: ',
        ////            'icon'     => 'icon-prepend fa fa-user',
        //            'data-validations'  => 'Required',
        //        ]);
        //        $this->add($quantidadeQuestoes);

        $integcadernoData = new Element\Text('data');
        $integcadernoData->setAttributes(
            array(
                'col'              => 2,
                'label'            => 'Data',
                'icon'             => 'icon-prepend fa fa-calendar',
                'data-validations' => 'Required,Data',
                'placeholder'      => 'Data prova:',
                'data-masked'      => 'Date',
                'value'            => date('d/m/Y')
            )
        );
        $this->add($integcadernoData);

        $integcadernoTurma = new Element\Text('turmas');
        $integcadernoTurma->setAttributes(
            array(
                'col'              => 2,
                'label'            => 'Turmas',
                'id'               => 'turmas',
                'icon'             => 'icon-prepend fa fa-group',
                'data-validations' => 'Required',
                'placeholder'      => 'Turmas participantes:',
                'class'            => 'form-control',
            )
        );
        $this->add($integcadernoTurma);

        $integcadernoPeriodo = new Element\Select('integavaliacaoPeriodo');
        $integcadernoPeriodo->setAttributes(
            [
                'col'              => '2',
                'wrap'             => false,
                'label'            => 'Periodo: ',
                'id'               => 'periodo',
                'required'         => 'Required',
                'data-validations' => 'Required',
                'class'            => 'form-control',
            ]
        );
        $this->add($integcadernoPeriodo);

        $integavaliacaoTurno = new Element\Select('integavaliacaoTurno');
        $integavaliacaoTurno->setAttributes(
            [
                'col'              => '2',
                'wrap'             => false,
                'label'            => 'Turno: ',
                'id'               => 'turno',
                'required'         => 'Required',
                'data-validations' => 'Required',
                'class'            => 'form-control',
            ]
        );
        $this->add($integavaliacaoTurno);

        $salvar = new Element\Submit('Salvar');
        $salvar->setAttributes(
            [
                'class' => 'btn btn-primary pull-left',
                'value' => 'salvar'
            ]
        );
        $this->add($salvar);
    }
}