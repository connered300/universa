<?php


namespace Matricula\Form;


use Zend\Form\Form,
    Zend\Form\Element;

class AcadperiodoMatrizDisciplina extends Form{
    public function __construct($name = null)
    {
        parent::__construct($name);

        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', '');
        $this->setAttribute('role', 'form');
        $this->setAttribute('id', 'FormMatrizDisciplina');

        $perDiscId = new Element\Hidden('perDiscId[]');
        $this->add($perDiscId);

        $perDiscPeriodo = new Element\Hidden('perDiscPeriodo[]');
        $this->add($perDiscPeriodo);

        $matCur = new Element\Hidden('matCur');
        $this->add($matCur);

        $perDiscChpratica = new Element\Text('perDiscChpratica[]');
        $perDiscChpratica->setAttributes([
            'label'            => 'Carga H. Prática',
            'col'              => 1,
            'data-validations' =>'Integer',
            'placeholder'      => 'Horas',
            'maxlength'         => 3


        ]);
        $this->add($perDiscChpratica);

        $perDiscChteorica = new Element\Text('perDiscChteorica[]');
        $perDiscChteorica->setAttributes([
            'label'            => 'Carga H. Teórica',
            'col'              => 1,
            'data-validations' =>'Integer',
            'placeholder'      => 'Horas',
            'maxlength'         => 3



        ]);
        $this->add($perDiscChteorica);

        $perDiscChestagio = new Element\Text('perDiscChestagio[]');
        $perDiscChestagio->setAttributes([
            'label'            => 'Carga H. Estágio',
            'col'              => 1,
            'data-validations' =>'Integer',
            'placeholder'      => 'Horas',
            'maxlength'         => 3


        ]);
        $this->add($perDiscChestagio);

        $perDiscCreditos = new Element\Text('perDiscCreditos[]');
        $perDiscCreditos->setAttributes([
            'label'            => 'Créditos',
            'col'              => 1,
            'data-validations' => 'Integer',
            'maxlength'         => 1


        ]);
        $this->add($perDiscCreditos);

        $discNome = new Element\Text('discNome[]');
        $discNome->setAttributes([
            'label'    => 'Disciplina',
            'col'      => 3,
            'readonly' => 'readonly',


        ]);
        $this->add($discNome);

        $disc = new Element\Hidden('disc[]');
        $this->add($disc);

        $perDiscPeriodo = new Element\Hidden('perDiscPeriodo[]');
        $this->add($perDiscPeriodo);

    }

}