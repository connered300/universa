<?php
namespace Matricula\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class AcadgeralAlunoCurso extends Form
{
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name);

        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', '');
        $this->setAttribute('role', 'form');

        $alunocursoId = new Element\Hidden('alunocursoId');
        $this->add($alunocursoId);

        $pesIdAgente = new Element\Hidden('pesIdAgente');
        $this->add($pesIdAgente);

        $origem = new Element\Hidden('origem');
        $this->add($origem);

        $aluno = new Element\Hidden('aluno');
        $this->add($aluno);
        $usuarioCadastro = new Element\Hidden('usuarioCadastro');
        $this->add($usuarioCadastro);
        $motivo = new Element\Hidden('motivo');
        $this->add($motivo);
        $usuarioAlteracao = new Element\Hidden('usuarioAlteracao');
        $this->add($usuarioAlteracao);
        $motivo = new Element\Hidden('motivo');
        $this->add($motivo);
        $matricula = new Element\Text('matricula');
        $matricula->setAttributes(
            [
                'col'      => '4',
                'wrap'     => false,
                'label'    => 'Matricula: ',
                'readonly' => 'readonly',
            ]
        );
        $this->add($matricula);

        $tiposel = new Element\Hidden('tiposel');
        $this->add($tiposel);

        $pesCpf = new Element\Text('pesCpf');
        $pesCpf->setAttributes(
            [
                'col'      => '4',
                'wrap'     => false,
                'label'    => 'Cpf: ',
                'readonly' => 'readonly',
            ]
        );
        $this->add($pesCpf);

        $pesNome = new Element\Text('pesNome');
        $pesNome->setAttributes(
            [
                'col'       => '4',
                'maxlength' => 255,
                'wrap'      => false,
                'label'     => 'Nome: ',
                'readonly'  => 'readonly',
            ]
        );
        $this->add($pesNome);

        $pesRg = new Element\Text('pesRg');
        $pesRg->setAttributes(
            [
                'col'      => '4',
                'wrap'     => false,
                'label'    => 'Rg: ',
                'readonly' => 'readonly',
            ]
        );
        $this->add($pesRg);

        $pesDocEstrangeiro = new Element\Text('pesDocEstrangeiro');
        $pesDocEstrangeiro->setAttributes(
            [
                'col'      => '4',
                'wrap'     => false,
                'label'    => 'Documento Estrangeiro: ',
                'readonly' => 'readonly',
            ]
        );
        $this->add($pesDocEstrangeiro);

        $alunocursoDataCadastro = new Element\Hidden('alunocursoDataCadastro');
        $this->add($alunocursoDataCadastro);

        $alunocursoCarteira = new Element\Text('alunocursoCarteira');
        $alunocursoCarteira->setAttributes(
            [
                'col'              => '2',
                'wrap'             => false,
                'readonly'         => true,
                'label'            => 'Carteira: ',
                'placeholder'      => 'Informe o n°',
                'icon'             => 'icon-prepend fa fa-credit-card',
                'data-validations' => 'Integer',
                'maxlength'        => 14
            ]
        );
        $this->add($alunocursoCarteira);

        $cursocampus = new Element\Text('cursocampus');
        $cursocampus->setAttributes(
            [
                'col'      => '2',
                'wrap'     => false,
                'label'    => 'Ano Formação: ',
                'required' => 'Required',
            ]
        );
        $this->add($cursocampus);
    }
}
