<?php


namespace Matricula\Form;


use Zend\Form\Form,
    Zend\Form\Element;


class AcadperiodoMatrizCurricular extends Form{
    public function __construct($name = null)
    {
        parent::__construct($name);

        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', '');
        $this->setAttribute('role', 'form');
        $this->setAttribute('id', 'FormMatriz');

        $matCurId = new Element\Hidden('matCurId');
        $this->add($matCurId);

        $cursoNome = new Element\Select('curso');
        $cursoNome->setAttributes([
            'label'    => 'Nome do Curso',
            'col'      => 4
        ]);
        $this->add($cursoNome);

        $ano = new Element\Text('matCurAno');
        $ano->setAttributes([
            'label'    => 'Ano',
            'col'      => 2,
            'data-validations' => 'Required,Integer',
            'maxlength'         => 4

        ]);
        $dataAtual = new \DateTime('now', new \DateTimeZone('America/Sao_Paulo'));
        $ano->setValue($dataAtual->format('Y'));
        $this->add($ano);

        $semestre = new Element\Text('matCurSemestre');
        $semestre->setAttributes([
            'label'    => 'Semestre',
            'col'      => 2,
            'wrap'     => true,
            'data-validations' => 'Required,Integer',
            'maxlength'         => 1

        ]);
        $this->add($semestre);

        $cadastrar = new Element\Submit('cadastrar');
        $cadastrar->setAttributes([
            'value'   => 'Cadastrar',
            'class'   => 'btn btn-primary pull-right',
        ]);
        $this->add($cadastrar);

        $matCurDescricao = new Element\Text('matCurDescricao');
        $matCurDescricao->setAttributes([
            'label'    => 'Descrição',
            'col'      => 4,
        ]);
        $this->add($matCurDescricao);

    }
}