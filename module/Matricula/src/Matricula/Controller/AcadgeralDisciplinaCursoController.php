<?php

namespace Matricula\Controller;

use VersaSpine\Controller\AbstractCoreController;

class AcadgeralDisciplinaCursoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request                    = $this->getRequest();
        $paramsGet                  = $request->getQuery()->toArray();
        $paramsPost                 = $request->getPost()->toArray();
        $serviceAcadgeralDisciplinaCurso = new \Matricula\Service\AcadgeralDisciplinaCurso($this->getEntityManager());

        $result = $serviceAcadgeralDisciplinaCurso->pesquisaForJson(array_merge($paramsGet, $paramsPost));
        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }
    
    
    public function searchAction()
    {
        $request = $this->getRequest();

        $viewJson = $this->getJson();
        if( $request->isPost() ){
            $serviceAcadgeralDisciplinaCurso = new \Matricula\Service\AcadgeralDisciplinaCurso($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceAcadgeralDisciplinaCurso->getDataForDatatables($dataPost['discId'] , $dataPost);
            $viewJson->setVariables([
                "draw"            => $result["draw"],
                "recordsTotal"    => $result["recordsTotal"],
                "recordsFiltered" => $result["recordsFiltered"],
                "data"            => $result["data"],
            ]);
        }

        return $viewJson;
    }

    public function addAction($disc = false)
    {

        $request = $this->getRequest();

        //vendo se é uma requisição ajax
        if ($request->isXmlHttpRequest()) {
            $json      = $this->getJson();
            $dadosPost = $request->getPost()->toArray();
            
            $serviceDisciplinaCurso = new \Matricula\Service\AcadgeralDisciplinaCurso($this->getEntityManager());

            $arrCursos = explode(",", $dadosPost['curso_id']);
            $success = false;
            $arrError = array();
            $arrDisciplina = array();

            //quando não for edição eu adiciono a disciplina automaticamente
            if( !$dadosPost['disc_id'] ){
                $arrDisciplina = [
                    'discNome'       => $dadosPost['discNome'],
                    'discSigla'      => $dadosPost['discSigla'],
                    'docenteRegente' => $dadosPost['docenteRegente'],
                ];
            }

            $serviceAcadgeralDisciplinaRequisito = new \Matricula\Service\AcadgeralDisciplinaRequisito($this->getEntityManager());

            foreach($arrCursos as $cadaCursoId){
                $result = $serviceDisciplinaCurso->save([
                    'disc'      => $dadosPost['disc_id'],
                    'curso'     => $cadaCursoId,
                    'discAtivo' => $dadosPost['disc_ativo'],
                    'tdisc'     => $dadosPost['tdisc_id'],
                    'discequivDestino' => $dadosPost['disc_equivalencia'],
                    $serviceAcadgeralDisciplinaRequisito::CO_REQUISITO  => $dadosPost['disc_co_requisito'],
                    $serviceAcadgeralDisciplinaRequisito::PRE_REQUISITO => $dadosPost['disc_pre_requisito'],
                ], $arrDisciplina);

                if ($lastError = $serviceDisciplinaCurso->getLastError()) {
                    $arrError[] = $lastError;
                } else{
                    $success = true;
                }
            }

            $json->setVariables(
                [
                    "erro"    => ['mensagem' => $arrError],
                    "success" => $success,
                    "disc"    => $result ? $result->getDisc()->getDiscId() : null,
                ]
            );

            return $json;
        }

        return $this->getView();
    }

    public function editAction()
    {
        return $this->addAction($this->params()->fromRoute("id", 0));
    }
    
    public function removeAction()
    {
        $request = $this->getRequest();

        if ( $request->isXmlHttpRequest() ) {

            $json      = $this->getJson();
            $dataPost      = $request->getPost()->toArray();
            $serviceDisciplinaCurso = new \Matricula\Service\AcadgeralDisciplinaCurso($this->getEntityManager());

            $cursoId = $dataPost['curso'];
            $discId  = $dataPost['disc_id'];

            try{
                if( $cursoId && $serviceDisciplinaCurso->removeVinculoDiscCurso($discId, $cursoId)){
                    $json->setVariable("success",true);
                } else{
                    $json->setVariable("erro" , ['mensagem' => "Por favor verifique se todos os campos estão corretamente preenchidos!"]);
                }
            } catch (\Exception $ex){
                $json->setVariable("erro" , ['mensagem' => $ex->getMessage()]);
            }


            return $json;
        }

        return $this->getView();
    }
}
