<?php
namespace Matricula\Controller;

use Dompdf\Dompdf;
use DOMPDFModule\Service\DOMPDFFactory;
use DOMPDFModule\View\Model\PdfModel;
use Sistema\Service\SisIntegracao;
use VersaSpine\Controller\AbstractCoreController;

class AcadperiodoAlunoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {
        $request    = $this->getRequest();
        $paramsGet  = $request->getQuery()->toArray();
        $paramsPost = $request->getPost()->toArray();
        $param      = array_merge($paramsGet, $paramsPost);

        $servicePeriodo          = new \Matricula\Service\AcadperiodoLetivo($this->getEntityManager());
        $serviceAcadperiodoAluno = new \Matricula\Service\AcadperiodoAluno($this->getEntityManager());

        if ($param['pesquisaPeriodo']) {
            $alunocursoId    = $param['alunocursoId'];
            $periodoCorrente = isset($param['periodoCorrente']) ? $param['periodoCorrente'] : null;
            $ultimoPeriodo=true;

            $arrDados = $serviceAcadperiodoAluno->periodosLetivosAluno($alunocursoId, $periodoCorrente);
        } else {
            if (!$param['perId']) {
                $param['perId'] = null;
            }

            $arrDados = $serviceAcadperiodoAluno->pesquisaForJson($param);
        }

        if ($ultimoPeriodo) {
            $this->json->setVariable('alunoPerId', $arrDados);

            return $this->json;
        }

        return new \Zend\View\Model\JsonModel($arrDados ? $arrDados : array());
    }

    public function indexAction()
    {
        $arrConfig               = $this->getServiceManager()->get('Config');
        $serviceSisConfig        = new \Sistema\Service\SisConfig($this->getEntityManager(), $arrConfig);
        $servicePeriodo          = new \Matricula\Service\AcadperiodoLetivo($this->getEntityManager());
        $serviceAcadPeriodoAluno = new \Matricula\Service\AcadperiodoAluno($this->getEntityManager());

        $periodoLetivo             = $servicePeriodo->getArrSelect2PeriodoCorrente();
        $ultimoPeriodoLetivoAberto = $servicePeriodo->buscaUltimoPeriodo(['aberto' => true], false);
        $novaTelaDeMatricula       = $serviceSisConfig->localizarChave('ATIVA_NOVA_TELA_MATRICULA');
        $arrSituacao               = $serviceAcadPeriodoAluno->getArrSelect2AlunoperiodoSituacao();

        $this->view->setVariable('novaTelaDeMatricula', $novaTelaDeMatricula);
        $this->view->setVariable('arrSituacao', $arrSituacao);
        $this->view->setVariable('periodoLetivo', $periodoLetivo);
        $this->view->setVariable('ultimoPeriodoLetivo', $ultimoPeriodoLetivoAberto);

        return $this->getView();
    }

    public function listagemAlunosAction()
    {
        $request      = $this->getRequest();
        $serviceAluno = (new \Matricula\Service\AcadperiodoAluno($this->getEntityManager()));

        if ($request->isPost()) {
            $arrayData = $request->getPost()->toArray();

            $result        = $serviceAluno->paginationAjax($arrayData);
            $situacaoAtivo = $result['situacao'];

            $this->json->setVariable("draw", $result["draw"]);
            $this->json->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->json->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->json->setVariable("data", $result["data"]);
            $this->json->setVariable("situacaoDesligamento", $situacaoAtivo);
        }

        return $this->json;
    }

    /**
     * Método que realiza o processo de matricula de um aluno na instituição
     * @return \Zend\View\Model\ViewModel
     * @throws \Exception
     */

    public function addAction()
    {
        $inscricaoId = $this->params()->fromRoute('id');
        $arrConfig   = $this->getServiceLocator()->get('config');

        $serviceMatrizDisciplina = (new \Matricula\Service\AcadperiodoMatrizDisciplina($this->getEntityManager()));
        $servicePeriodo          = (new \Matricula\Service\AcadperiodoLetivo($this->getEntityManager()));

        $formAluno            = $this->forms()->mountForm($this->getForm());
        $formTipoIngresso     = $this->forms()->mountForm('Vestibular\Form\SelecaoTipo');
        $formAlunoCurso       = $this->forms()->mountForm('Matricula\Form\AcadgeralAlunoCurso');
        $formAlunoResponsavel = $this->forms()->mountForm('Matricula\Form\AcadgeralResponsavel');
        $formContato          = $this->forms()->mountForm('Pessoa\Form\Contato');
        $formTipoContato      = $this->forms()->mountForm('Pessoa\Form\TipoContato');
        $options_estados      = (new \Pessoa\Service\Endereco($this->getEntityManager()))->buscaEstados();
        $formEndereco         = new \Pessoa\Form\Endereco($options_estados);

        $formAluno->get('pesNascUf')->setValueOptions($options_estados);

        $request             = $this->getRequest();
        $service             = new \Matricula\Service\AcadperiodoAluno($this->getEntityManager(), $arrConfig);
        $serviceIncricao     = new \Vestibular\Service\SelecaoInscricao($this->getEntityManager());
        $servicePessoaFisica = new \Pessoa\Service\PessoaFisica($this->getEntityManager());

        $arrDadosmatrizTranzicao = [
            'per_id' => $servicePeriodo->buscaPeriodoCorrente()->getPerId(),
        ];

        $arrDisciplina = $serviceMatrizDisciplina->buscaDisciplinasMatrizTranzicao(
            $arrDadosmatrizTranzicao
        );

        if ($request->isPost()) {
            try {
                $dados = array_merge($arrConfig, $request->getPost()->toArray(), $this->params()->fromFiles());

                /* Tratamento para quando não passar o tipo endereço n formulário ou o mesmo vir como string */
                $dados['tipoEndereco'] = (
                is_array($dados['tipoEndereco']) ? $dados['tipoEndereco'] : array($dados['tipoEndereco'])
                );

                foreach ($dados['endCidade'] as $contador => $valor) {
                    $dados['tipoEndereco'][$contador] = (
                    $dados['tipoEndereco'][$contador] ? $dados['tipoEndereco'][$contador] : 1
                    );
                }

                $ret = $service->adicionar($dados);

                if (is_null($this->getRoute())) {
                    $this->setRoute($this->getEvent()->getRouteMatch()->getMatchedRouteName());
                }

                if (is_object($ret['alunoPeriodo'])) {
                    $this->flashMessenger()->addSuccessMessage(
                        "Aluno cadastrado com sucesso!!<br>Para imprimir o contrato clique <b><a href='/matricula/acadperiodo-aluno/contrato-servico/{$ret['alunoPeriodo']->getAlunoperId()}' target='_blank'>Aqui</a></b><br>Para imprimir o requerimento de matricula clique <b><a href='/matricula/acadperiodo-aluno/requerimento-matricula/{$ret['alunoPeriodo']->getAlunoperId()}' target='_blank'>Aqui</a></b>"
                    );
                    if ($ret['taxas']) {
                        $this->flashMessenger()->addWarningMessage(
                            "ATENÇÃO: Este Aluno possuí taxas de Dependências ou Adaptações ou Monografia no seu cadastro. Notifique a tesouraria a  análise de cobrança."
                        );
                    }
                } else {
                    $this->flashMessenger()->addWarningMessage($ret);
                }
            } catch (\Exception $ex) {
                $erro = $ex->getMessage();
                throw new \Exception($ex->getMessage());
                $formAluno->setData($request->getPost()->toArray());
            }

            $is_json = $request->getPost("is_json");

            if (isset($erro)) {
                if ($is_json) {
                    $this->json->setVariable("erro", $erro);

                    return $this->json;
                }

                $this->flashMessenger()->addErrorMessage($erro);
            } else {
                if ($is_json) {
                    if (method_exists($ret, "toArray")) {
                        $dados = $ret->toArray();
                    } else {
                        $dados = "Os dados foram inseridos corretamente, mas um erro ocorreu, verifique se existe o método toArray na entidade {$this->getEntity()}!";
                    }

                    $this->json->setVariable("result", $dados);

                    return $this->json;
                }
            }

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        } else {
            if ($inscricaoId) {
                $this->view->setVariable('inscricaoId', $inscricaoId);
                $dadosCandidato = (
                new \Matricula\Service\AcadperiodoAluno(
                    $this->getEntityManager()
                )
                )->buscaDadosVestibular(
                        $inscricaoId
                    );

                if (!empty($dadosCandidato['pes'])) {
                    $alunoJaCadastrado = $service->getRepository('Matricula\Entity\AcadgeralAluno')->findOneBy(
                        ['pes' => $dadosCandidato['pes']['pes']]
                    );

                    if (empty($alunoJaCadastrado)) {
                        $formAluno->setData($dadosCandidato['pes']);

                        if (!empty($dadosCandidato['endereco'])) {
                            $formEndereco->setData($dadosCandidato['endereco']);
                        }
                        if (!empty($dadosCandidato['contato'])) {
                            $formAluno->setData($dadosCandidato['contato']);
                        }
                    } else {
                        $this->flashMessenger()->addWarningMessage("Aluno já matriculado !");

                        return $this->redirect()->toRoute(
                            'vestibular/default',
                            array('controller' => 'lista-aprovados')
                        );
                    }
                }
            }
        }

        $selecaoTipos = $service->getRepository('Vestibular\Entity\SelecaoTipo')->listaTipos();

        if (!empty($dadosCandidato['inscricao']['inscricaoId'])) {
            $ingresso = $serviceIncricao->buscaFormadeIngresso($dadosCandidato['pes']['pes']);
        }

        $cursosCampus = $service->getRepository('Matricula\Entity\CampusCurso')->buscaCursos();

        $situacoes = $service->getRepository('Matricula\Entity\AcadgeralSituacao')->listaSituacoes();

        $spineForm  = new \VersaSpine\ViewHelper\SpineForm();
        $documentos = new \Documentos\Helper\MountForm('Aluno', $spineForm, $service->getEm());

        $arrPeriodoLetivo = $servicePeriodo->getArrSelect2();

        $this->view->setVariable("arrEstadoCivil", $servicePessoaFisica->getArrSelect2PesEstadoCivil());
        $this->view->setVariable("arrPeriodo", $arrPeriodoLetivo);
        $this->view->setVariable("disciplinas", $arrDisciplina);
        $this->view->setVariable("formAluno", $formAluno);
        $this->view->setVariable("formTipoIngresso", $formTipoIngresso);
        $this->view->setVariable("formAlunoCurso", $formAlunoCurso);
        $this->view->setVariable("formAlunoResponsavel", $formAlunoResponsavel);
        $this->view->setVariable("formContato", $formContato);
        $this->view->setVariable("formTipoContato", $formTipoContato);
        $this->view->setVariable("formEndereco", $formEndereco);
        $this->view->setVariable("documentos", $documentos);
        $this->view->setVariable("selecaoTipos", $selecaoTipos);
        $this->view->setVariable("cursosCampus", $cursosCampus);
        $this->view->setVariable("situacoes", $situacoes);
        $this->view->setVariable("ingresso", $ingresso);

        return $this->view;
    }

    /**
     * Método que realiza o processo de edição do dados de um aluno na instituição
     * @return \Zend\View\Model\ViewModel
     *
     */
    public function editAction()
    {
        /** @var \Matricula\Form\AcadperiodoAluno $formAluno */
        $formAluno            = $this->forms()->mountForm($this->getForm());
        $formTipoIngresso     = $this->forms()->mountForm('Vestibular\Form\SelecaoTipo');
        $formAlunoCurso       = $this->forms()->mountForm('Matricula\Form\AcadgeralAlunoCurso');
        $formAlunoResponsavel = $this->forms()->mountForm('Matricula\Form\AcadgeralResponsavel');
        $formContato          = $this->forms()->mountForm('Pessoa\Form\Contato');
        $formTipoContato      = $this->forms()->mountForm('Pessoa\Form\TipoContato');
        $options_estados      = (new \Pessoa\Service\Endereco($this->getEntityManager()))->buscaEstados();
        $formEndereco         = new \Pessoa\Form\Endereco($options_estados);

        $formAluno->get('pesNascUf')->setValueOptions($options_estados);

        $request   = $this->getRequest();
        $arrConfig = $this->getServiceLocator()->get('config');

        $service                      = new \Matricula\Service\AcadperiodoAluno($this->getEntityManager(), $arrConfig);
        $servicePeriodo               = new \Matricula\Service\AcadperiodoLetivo($this->getEntityManager());
        $serviceMatrizDisciplina      = new \Matricula\Service\AcadperiodoMatrizDisciplina($this->getEntityManager());
        $serviceMatriculaSituacao     = new \Matricula\Service\AcadgeralSituacao($this->getEntityManager());
        $serviceAlunoDisciplina       = new \Matricula\Service\AcadperiodoAlunoDisciplina($this->getEntityManager());
        $serviceAlunoCurso            = new\Matricula\Service\AcadgeralAlunoCurso(
            $this->getEntityManager(), $arrConfig
        );
        $serviceAluno                 = new \Matricula\Service\AcadgeralAluno($this->getEntityManager(), $arrConfig);
        $serviceAcadperiodoDisciplina = new \Matricula\Service\AcadperiodoAlunoDisciplina($this->getEntityManager());
        $servicePessoaFisica          = new \Pessoa\Service\PessoaFisica($this->getEntityManager());
        $serviceAcessoPessoa          = new \Acesso\Service\AcessoPessoas($this->getEntityManager(), $arrConfig);
        $serviceFinanceiroDesconto    = new \Financeiro\Service\FinanceiroDesconto(
            $this->getEntityManager(), $arrConfig
        );

        $rematricula = null;
        $serieAluno  = null;

        if ($request->isPost()) {
            try {
                $dados = array_merge($request->getPost()->toArray(), $request->getFiles()->toArray());
                $ret   = $service->edita($dados);

                if (!$ret) {
                    throw new \Exception($service->getLastError());
                }

                if (is_null($this->getRoute())) {
                    $this->setRoute($this->getEvent()->getRouteMatch()->getMatchedRouteName());
                }

                $this->flashMessenger()->addSuccessMessage(
                    "Aluno editado com sucesso!!<br>Para imprimir o contrato com as alterações clique <b><a href='/matricula/acadperiodo-aluno/contrato-servico/{$ret['alunoPeriodo']->getAlunoperId()}' target='_blank'>Aqui</a></b><br>Para imprimir o requerimento de matricula com as alterações <b><a href='/matricula/acadperiodo-aluno/requerimento-matricula/{$ret['alunoPeriodo']->getAlunoperId()}' target='_blank'>Aqui</a></b>"
                );
                if ($ret['taxas']) {
                    $this->flashMessenger()->addWarningMessage(
                        "ATENÇÃO: Este Aluno possuí taxas de Dependências ou Adaptações ou Monografia no seu cadastro. Notifique a tesouraria a  análise de cobrança."
                    );
                }
            } catch (\Exception $ex) {
                $erro = $ex->getMessage();
                $formAluno->setData($request->getPost()->toArray());
            }

            $is_json = $request->getPost("is_json");

            if (isset($erro)) {
                if ($is_json) {
                    $this->json->setVariable("erro", $erro);

                    return $this->json;
                }

                $this->flashMessenger()->addErrorMessage($erro);
            } else {
                if ($is_json) {
                    if (method_exists($ret, "toArray")) {
                        $dados = $ret->toArray();
                    } else {
                        $dados = "Os dados foram alterados corretamente, mas um erro ocorreu, verifique se existe o método toArray na entidade {$this->getEntity()}!";
                    }

                    $this->json->setVariable("result", $dados);

                    return $this->json;
                }
            }

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        $paramsRoute = $this->params()->fromRoute("id");

        $id            = (int)$paramsRoute;
        $reativarCurso = preg_replace('/.*=/', '', $paramsRoute) == "true" ? true : false;

        if (empty($id)) {
            $this->flashMessenger()->addErrorMessage(
                "Aluno não encontrado ou temporariamente indisponível para edição!"
            );

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        $serviceAlunoResumo = new \Matricula\Service\AcadperiodoAlunoResumo($this->getEntityManager());
        $repositorySituacao = $service->getRepository('Matricula\Entity\AcadgeralSituacao');

        $selecaoTipos = $service->getRepository('Vestibular\Entity\SelecaoTipo')->listaTipos();

        $situacoes = $repositorySituacao->listaSituacoes();

        $reference = $service->getReference($id);

        $objMatSituacao = $reference->getMatsituacao();

        if ($reference) {
            if ($objMatSituacao->getSituacaoId() == $serviceMatriculaSituacao::PRE_MATRICULA ||
                $objMatSituacao->getSituacaoId() == $serviceMatriculaSituacao::CANCELADA &&
                $reativarCurso
            ) {
                $rematricula = 1;
                $this->view->setVariable('alunoRematricula', $reference->getAlunoPerId());
            } else {
                $rematricula = 0;
            }

            $this->view->setVariable('periodoLetivoId', $reference->getTurma()->getPer()->getPerId());

            $dePendências = $serviceAlunoResumo->buscaDependenciasAluno($reference->getAlunocurso()->getAlunocursoId());

            $this->view->setVariable("dependencias", $dePendências);

            $periodoMatricula = $servicePeriodo->periodoMatricula();
            if (!$rematricula) {
                if ($periodoMatricula) {
                    $acadperiodoRematricula = $service->buscaAlunoRematricula(
                        $reference->getAlunocurso()->getAlunocursoId()
                    );
                    if ($acadperiodoRematricula) {
                        $this->view->setVariable('alunoRematricula', $acadperiodoRematricula->getAlunoperId());
                    }
                }
            }

            $serieAluno = $reference->getTurma()->getTurmaSerie();
            $aluno      = $reference->toArray();

            if (!empty($aluno['arq'])) {
                if ($aluno['arq']->getArqId()) {
                    $this->view->setVariable("chaveFoto", $aluno['arq']->getArqChave());
                    $this->view->setVariable("arqId", $aluno['arq']->getArqId());
                }
            }

            /** @var \Pessoa\Entity\PessoaFisica $objPesFisica */
            $objPesFisica            = $servicePessoaFisica->getRepository()->find($aluno['pes']);
            $aluno['pesEstadoCivil'] = $objPesFisica->getPesEstadoCivil();

            $formAluno->setData($aluno);
            $endereco = (new \Pessoa\Service\Endereco($this->getEntityManager()))->buscaEndereco($aluno['pes']);

            if ($endereco) {
                $formEndereco->setData($endereco);
            }

            $contatos = $service->getRepository('Pessoa\Entity\Contato')->buscaContatos($aluno['pes']);

            if ($contatos) {
                $contatos = $service->mapContato($contatos);
                $formAluno->setData($contatos);
            }

            $alunoCurso = (new \Matricula\Service\AcadgeralAlunoCurso($this->getEntityManager()))->buscaAlunoCurso(
                $aluno['alunocurso'],
                true
            );

            if ($alunoCurso) {
                $formAlunoCurso->setData($alunoCurso->toArray());

                $cursosCampus [] = [
                    0 => $alunoCurso->getCursocampus()->getCurso()->getCursoNome(),
                    1 => $alunoCurso->getCursocampus()->getCurso()->getCursoPrazoIntegralizacao(),
                    2 => $alunoCurso->getCursocampus()->getCursocampusId()
                ];
            }
        }

        $spineForm  = new \VersaSpine\ViewHelper\SpineForm();
        $documentos = new \Documentos\Helper\MountForm('Aluno', $spineForm, $service->getEm(), $aluno['pes']);

        $repossaveis = (new \Matricula\Service\AcadgeralResponsavel($this->getEntityManager()))->buscaResponsaveis(
            $aluno['alunoId']
        );

        /** @var \Matricula\Entity\CampusCurso $objCursosCampus */
        $objCursosCampus = $alunoCurso->getCursocampus();

        $campId        = $objCursosCampus->getCamp()->getCampId();
        $cursocampusId = $objCursosCampus->getCamp()->getCampId();
        $cursoDoAluno  = $objCursosCampus->getCurso();

        $arrDadosmatrizTranzicao = [
            'per_id'   => $servicePeriodo->buscaPeriodoCorrente()->getPerId(),
            'curso_id' => $cursoDoAluno->getCursoId(),
        ];

        $arrDisciplina = $serviceMatrizDisciplina->buscaDisciplinasMatrizTranzicao(
            $arrDadosmatrizTranzicao
        );

        $arrDisciplinasAluno = $serviceAlunoDisciplina->buscaDisciplinasAlunoPeriodo($id);
        $arrPeriodoLetivo    = $service->getArrSelect2PeriodoMatriculadoAluno($id);
        $arrAlunoTurma       = $service->getArrSelect2TurmaMatriculadoAluno($id);
        $arrCursoAlunoPer    = $service->getArrSelect2CursoAlunoMatriculado($id);
        $cursosCampus        = $service->getRepository('Matricula\Entity\CampusCurso')->buscaCursos($campId);

        /** @var \Matricula\Entity\AcadgeralAlunoCurso $objAlunoCurso */
        $objAlunoCurso = $serviceAlunoCurso->getRepository()->findOneBy(['alunocursoId' => $aluno['alunocurso']]);

        /** @var \Matricula\Entity\AcadgeralAluno $objAluno */
        $objAluno = $serviceAluno->getRepository()->find($aluno['alunoId']);

        if ($objAluno->getUnidade()) {
            $arrUnidade = [
                'id'   => $objAluno->getUnidade()->getUnidadeId(),
                'text' => $objAluno->getUnidade()->getUnidadeNome()
            ];
        }

        if ($objAlunoCurso) {
            if ($objAlunoCurso->getPesIdAgente() && $objAlunoCurso->getPesIdAgente()->getPes()->getPesId()) {
                $arrAgenteEducacional = [
                    'id'   => $objAlunoCurso->getPesIdAgente()->getPes()->getPesId(),
                    'text' => $objAlunoCurso->getPesIdAgente()->getPes()->getPesNome()
                ];
            }

            if ($objAlunoCurso->getOrigem() && $objAlunoCurso->getOrigem()->getOrigemId() !== 0) {
                $arrOrigemAlunoCurso = [
                    'id'   => $objAlunoCurso->getOrigem()->getOrigemId(),
                    'text' => $objAlunoCurso->getOrigem()->getOrigemNome()
                ];
            } elseif ($objAluno->getOrigem() && $objAluno->getOrigem()->getOrigemId() !== 0) {
                $arrOrigemAlunoCurso = [
                    'id'   => $objAluno->getOrigem()->getOrigemId(),
                    'text' => $objAluno->getOrigem()->getOrigemNome()
                ];
            }
        }

        $arrDisciplinas    = $serviceAcadperiodoDisciplina->disciplinasAlunoPeriodo($aluno['alunoperId']);
        $disciplinasTurmas = array();

        /** @var \Matricula\Entity\AcadperiodoAlunoDisciplina $entidade */
        foreach ($arrDisciplinas as $entidade) {
            $disciplinasTurmas [$entidade->getDisc()->getDiscId()] =
                [
                    'id'   => $entidade->getTurma()->getTurmaId(),
                    'text' => $entidade->getTurma()->getTurmaNome()
                ];
        }

        $estadoCivil = [
            'id'   => $objAluno->getPes()->getPesEstadoCivil(),
            'text' => $objAluno->getPes()->getPesEstadoCivil()
        ];

        $permitirEdicaoAgente = $serviceAcessoPessoa->validaPessoaEditaAgenteAluno();

        $arrDecontoFies = $serviceFinanceiroDesconto->retornaPercentualDescontoFiesAlunoPeriodo(
            $aluno['alunoperId'] ? $aluno['alunoperId'] : $id
        );

        $msgDescontoFies = '-';

        if ($arrDecontoFies) {
            $msgDescontoFies = "Financiamento de " . $arrDecontoFies['percentual__desconto_geral'] . "% no período: " . $arrDecontoFies['perNome'] . ".";
        }

        $this->view->setVariable('descontoFies', $msgDescontoFies);
        $this->view->setVariable('arrEndereco', $endereco);
        $this->view->setVariable("editaAgente", $permitirEdicaoAgente);
        $this->view->setVariable("arrEstadoCivil", $servicePessoaFisica->getArrSelect2PesEstadoCivil());
        $this->view->setVariable("estadoCivil", $estadoCivil);
        $this->view->setVariable("arrUnidade", $arrUnidade);
        $this->view->setVariable("arrAgenteEducacional", $arrAgenteEducacional);
        $this->view->setVariable("arrOrigemAlunoCurso", $arrOrigemAlunoCurso);
        $this->view->setVariable("arrDisciplinasAluno", $arrDisciplinasAluno);
        $this->view->setVariable("rematricula", $rematricula);
        $this->view->setVariable("arrPeriodo", $arrPeriodoLetivo);
        $this->view->setVariable("arrCurso", $arrCursoAlunoPer);
        $this->view->setVariable("alunoTurmaPrincipal", $aluno['turma']);
        $this->view->setVariable("responsaveis", $repossaveis);
        $this->view->setVariable("serieAluno", $serieAluno);
        $this->view->setVariable("alunoId", $aluno['alunoId']);
        $this->view->setVariable("disciplinas", $arrDisciplina);
        $this->view->setVariable("alunocurso", $aluno['alunocurso']);
        $this->view->setVariable("turma", $aluno['turma']);
        $this->view->setVariable("alunoperId", $aluno['alunoperId']);
        $this->view->setVariable("serie", $aluno['serie']);
        $this->view->setVariable("formAluno", $formAluno);
        $this->view->setVariable("formTipoIngresso", $formTipoIngresso);
        $this->view->setVariable("formAlunoCurso", $formAlunoCurso);
        $this->view->setVariable("formAlunoResponsavel", $formAlunoResponsavel);
        $this->view->setVariable("formContato", $formContato);
        $this->view->setVariable("formTipoContato", $formTipoContato);
        $this->view->setVariable("formEndereco", $formEndereco);
        $this->view->setVariable("documentos", $documentos);
        $this->view->setVariable("selecaoTipos", $selecaoTipos);
        $this->view->setVariable("cursosCampus", $cursosCampus);
        $this->view->setVariable("cursoDoAluno", $cursoDoAluno);
        $this->view->setVariable("situacoes", $situacoes);
        $this->view->setVariable("disciplinasTurmas", $disciplinasTurmas);
        $this->view->setVariable("alunoPer", $aluno ? $aluno['alunoperId'] : null);
        $this->view->setVariable("arrTurma", $arrAlunoTurma);
        $this->view->setVariable("ingresso", $alunoCurso->getTiposel()->getTiposelId());

        $this->view->setTemplate($this->getTemplateToRoute());

        return $this->view;
    }

    public function buscaDadosPessoaisResidenciaisContatosAlunoAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $arrayData = $request->getPost()->toArray();
            $dados     = $this->services()->getService(
                $this->getService()
            )->buscaDadosPessoaisResidenciaisContatosAluno($arrayData['alunoperId']);

            if ($dados) {
                $this->json->setVariable("dadosAluno", $dados);

                return $this->json;
            }
        }

        return null;
    }

    public function adicionaAlunosMigracaoQueTemAlunocursoMasNaoTemAlunoperidoAction()
    {
        $formAluno            = $this->forms()->mountForm($this->getForm());
        $formTipoIngresso     = $this->forms()->mountForm('Vestibular\Form\SelecaoTipo');
        $formAlunoCurso       = $this->forms()->mountForm('Matricula\Form\AcadgeralAlunoCurso');
        $formAlunoResponsavel = $this->forms()->mountForm('Matricula\Form\AcadgeralResponsavel');
        $formContato          = $this->forms()->mountForm('Pessoa\Form\Contato');
        $formTipoContato      = $this->forms()->mountForm('Pessoa\Form\TipoContato');
        $options_estados      = (new \Pessoa\Service\Endereco($this->getEntityManager()))->buscaEstados();
        $formEndereco         = new \Pessoa\Form\Endereco($options_estados);

        $formAluno->get('pesNascUf')->setValueOptions($options_estados);

        $arrConfig = $this->getServiceLocator()->get('config');

        $request                 = $this->getRequest();
        $serviceAlunoPeriodo     = new \Matricula\Service\AcadperiodoAluno($this->getEntityManager(), $arrConfig);
        $servicePeriodo          = new \Matricula\Service\AcadperiodoLetivo($this->getEntityManager());
        $serviceAlunoCurso       = new \Matricula\Service\AcadgeralAlunoCurso($this->getEntityManager(), $arrConfig);
        $serviceMatrizDisciplina = new \Matricula\Service\AcadperiodoMatrizDisciplina($this->getEntityManager());

        if ($request->isPost()) {
            try {
                $dados = array_merge($request->getPost()->toArray(), $request->getFiles()->toArray());
                $ret   = $serviceAlunoPeriodo->adicionaAlunosMigracaoQueTemAlunocursoMasNaoTemAlunoperido($dados);

                if (!$ret) {
                    $this->flashMessenger()->addErrorMessage(
                        "Erro ao salvar os dados do aluno, favor entrar em contato com suporte!"
                    );

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                if (is_null($this->getRoute())) {
                    $this->setRoute($this->getEvent()->getRouteMatch()->getMatchedRouteName());
                }

                $this->flashMessenger()->addSuccessMessage(
                    "Aluno Rematriculado com sucesso!!<br>Para imprimir o contrato com as alterações clique <b> <a href = '/matricula/acadperiodo-aluno/contrato-servico/{$ret['alunoPeriodo']->getAlunoperId()}' target = '_blank' > Aqui</a ></b ><br > Para imprimir o requerimento de matricula com as alterações <b><a href = '/matricula/acadperiodo-aluno/requerimento-matricula/{$ret['alunoPeriodo']->getAlunoperId()}' target = '_blank' > Aqui</a ></b > "
                );

                if ($ret['taxas']) {
                    $this->flashMessenger()->addWarningMessage(
                        "ATENÇÃO: Este Aluno possuí taxas de Dependências ou Adaptações ou Monografia no seu cadastro. Notifique a tesouraria a  análise de cobrança."
                    );
                }

                if ($ret) {
                    /** @var \Matricula\Entity\AcadperiodoAluno $alunoPeriodo */
                    $alunoPeriodo = $ret['alunoPeriodo'];
                    (new SisIntegracao($this->getEntityManager()))->integracaoInteligenteParaAluno($alunoPeriodo);
                }
            } catch (\Exception $ex) {
                $erro = $ex->getMessage();
                $formAluno->setData($request->getPost()->toArray());
            }

            $is_json = $request->getPost("is_json");

            if (isset($erro)) {
                if ($is_json) {
                    $this->json->setVariable("erro", $erro);

                    return $this->json;
                }

                $this->flashMessenger()->addErrorMessage($erro);
            } else {
                if ($is_json) {
                    if (method_exists($ret, "toArray")) {
                        $dados = $ret->toArray();
                    } else {
                        $dados
                            = "Os dados foram alterados corretamente, mas um erro ocorreu, verifique se existe o método toArray na entidade {
        $this->getEntity()}!";
                    }

                    $this->json->setVariable("result", $dados);

                    return $this->json;
                }
            }

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        $id = $this->params()->fromRoute("id", 0);

        if (!$id) {
            throw new \Exception(
                "Para editar um registro é necessário informar o identificador do mesmo pela rota!", 1
            );
        }

        $selecaoTipos = $serviceAlunoPeriodo->getRepository('Vestibular\Entity\SelecaoTipo')->listaTipos();
        $cursosCampus = $serviceAlunoPeriodo->getRepository('Matricula\Entity\CampusCurso')->buscaCursos(2);
        $situacoes    = $serviceAlunoPeriodo->getRepository('Matricula\Entity\AcadgeralSituacao')->listaSituacoes();

        /** @var \Matricula\Entity\AcadgeralAlunoCurso $objAlunoCurso */
        $objAlunoCurso = $serviceAlunoCurso->getRepository()->find($id);

        /** @var \Matricula\Entity\AcadgeralAluno $objAluno */
        $objAluno = $objAlunoCurso->getAluno();

        if ($objAluno->getUnidade()) {
            $arrUnidade = [
                'id'   => $objAluno->getUnidade()->getUnidadeId(),
                'text' => $objAluno->getUnidade()->getUnidadeNome()
            ];
        }

        if ($objAlunoCurso->getPesIdAgente() && $objAlunoCurso->getPesIdAgente()->getPes()->getPesId()) {
            $arrAgenteEducacional = [
                'id'   => $objAlunoCurso->getPesIdAgente()->getPes()->getPesId(),
                'text' => $objAlunoCurso->getPesIdAgente()->getPes()->getPesNome()
            ];
        }

        if ($objAlunoCurso->getOrigem() && $objAlunoCurso->getOrigem()->getOrigemId() !== 0) {
            $arrOrigemAlunoCurso = [
                'id'   => $objAlunoCurso->getOrigem()->getOrigemId(),
                'text' => $objAlunoCurso->getOrigem()->getOrigemNome()
            ];
        } elseif ($objAluno->getOrigem() && $objAluno->getOrigem()->getOrigemId() !== 0) {
            $arrOrigemAlunoCurso = [
                'id'   => $objAluno->getOrigem()->getOrigemId(),
                'text' => $objAluno->getOrigem()->getOrigemNome()
            ];
        }

        if ($objAlunoCurso) {
            //            $serieAluno = 1;
            $aluno = $objAlunoCurso->toArrayAlunoMigracaoSemPeriodoLetivo();

            $formAluno->setData($aluno);
            $endereco = (new \Pessoa\Service\Endereco($this->getEntityManager()))->buscaEndereco($aluno['pes']);

            $arrCurso =
                $objAlunoCurso
                    ?
                    [
                        'id'          => $objAlunoCurso->getCursocampus()->getCurso()->getCursoId(),
                        'text'        => $objAlunoCurso->getCursocampus()->getCurso()->getCursoNome(),
                        'cursoCampus' => $objAlunoCurso->getCursocampus()->getCursocampusId()
                    ]
                    :
                    null;

            if ($endereco) {
                $formEndereco->setData($endereco);
            }

            $contatos = $serviceAlunoPeriodo->getRepository('Pessoa\Entity\Contato')->buscaContatos($aluno['pes']);

            if ($contatos) {
                $contatos = $serviceAlunoPeriodo->mapContato($contatos);
                $formAluno->setData($contatos);
            }

            $alunoCurso = (new \Matricula\Service\AcadgeralAlunoCurso($this->getEntityManager()))->buscaAlunoCurso(
                $aluno['alunoId']
            );

            if ($alunoCurso) {
                $formAlunoCurso->setData($alunoCurso->toArray());
            }
        }

        $spineForm  = new \VersaSpine\ViewHelper\SpineForm();
        $documentos = new \Documentos\Helper\MountForm(
            'Aluno', $spineForm, $serviceAlunoPeriodo->getEm(), $aluno['pes']
        );

        $repossaveis = (new \Matricula\Service\AcadgeralResponsavel($this->getEntityManager()))->buscaResponsaveis(
            $aluno['alunoId']
        );

        $periodoLetivoMatricula = $servicePeriodo->periodoMatricula();

        if ($periodoLetivoMatricula == null) {
            $this->flashMessenger()->addInfoMessage(

                "O Período letivo para matricula não está configurado ou foi encerrado. Em caso de dúvidas entre em contato com o suporte"
            );

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        /** @var \Matricula\Entity\CampusCurso $objCursosCampus */
        $objCursosCampus = $objAlunoCurso->getCursocampus();

        if ($objCursosCampus) {
            $campId        = $objCursosCampus->getCamp()->getCampId();
            $cursocampusId = $objCursosCampus->getCamp()->getCampId();
            $cursoDoAluno  = $objCursosCampus->getCurso();

            $cursosCampus = $serviceAlunoPeriodo->getRepository('Matricula\Entity\CampusCurso')->buscaCursos($campId);

            $arrDadosmatrizTranzicao = [
                'per_id'   => $servicePeriodo->buscaPeriodoCorrente()->getPerId(),
                'curso_id' => $cursoDoAluno->getCursoId(),
            ];

            $arrDisciplina = $serviceMatrizDisciplina->buscaDisciplinasMatrizTranzicao(
                $arrDadosmatrizTranzicao
            );
        }

        $arrPeriodoLetivo = $servicePeriodo->getArrSelect2PeriodoAberto();
        $arrPeriodos      = [];

        if ($aluno['alunoperId']) {

            /** @var $objAlunoPeriodo \Matricula\Entity\AcadperiodoAluno */
            $objAlunoPeriodo = $serviceAlunoPeriodo->getRepository()->find($aluno['alunoperId']);

            /** @var  $objPeriodoLetivo  \Matricula\Entity\AcadperiodoLetivo */
            $objPeriodoLetivo = $objAlunoPeriodo->getTurma() ?
                $objAlunoPeriodo->getTurma()->getPer() : null;

            $dataAtual = new \DateTime();

            $abertoParaMatricula =
                $objPeriodoLetivo->getPerMatriculaInicio() >= $dataAtual &&
                $objPeriodoLetivo->getPerMatriculaFim() <= $dataAtual;

            if ($abertoParaMatricula) {
                $arrPeriodos[0]         = $objPeriodoLetivo->toArray();
                $arrPeriodos[0]['id']   = $objPeriodoLetivo->getPerId();
                $arrPeriodos[0]['text'] = $objPeriodoLetivo->getPerNome();
            } else {
                /*Busca Periodos em aberto para este curso*/
                $arrPeriodos = $serviceAlunoPeriodo->buscaPeriodoLetivoAluno($aluno['alunoperId'], true);
            }
        } else {
            $objCurso = null;

            if ($objCursosCampus) {
                $objCurso = $objCursosCampus->getCurso() ? $objCursosCampus->getCurso() : null;
            }

            if ($objCurso) {
                $arrPeriodos = $servicePeriodo->retornaPeriodoAbertoCurso($objCurso->getCursoId());
            }
        }

        if (!isset($arrPeriodos[0]['per_id'])) {
            $this->flashMessenger()->addWarningMessage(
                "Não há períodos ativos para este curso ou turmas registradas para o período letivo!"
            );

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        $this->view->setVariable('arrPeriodosDisponiveis', $arrPeriodos);
        $this->view->setVariable('arrCurso', $arrCurso);
        $this->view->setVariable('periodoLetivoId', $periodoLetivoMatricula->getPerId());
        $this->view->setVariable('arrPeriodo', $arrPeriodoLetivo);
        $this->view->setVariable("responsaveis", $repossaveis);
        //        $this->view->setVariable("serieAluno", $serieAluno);
        $this->view->setVariable("alunoId", $aluno['alunoId']);
        $this->view->setVariable("disciplinas", $arrDisciplina);
        $this->view->setVariable("alunocurso", $aluno['alunocurso']);
        $this->view->setVariable("turma", $aluno['turma']);
        $this->view->setVariable("alunoperId", $aluno['alunoperId']);
        $this->view->setVariable("formAluno", $formAluno);
        $this->view->setVariable("formTipoIngresso", $formTipoIngresso);
        $this->view->setVariable("formAlunoCurso", $formAlunoCurso);
        $this->view->setVariable("formAlunoResponsavel", $formAlunoResponsavel);
        $this->view->setVariable("formContato", $formContato);
        $this->view->setVariable("formTipoContato", $formTipoContato);
        $this->view->setVariable("formEndereco", $formEndereco);
        $this->view->setVariable("documentos", $documentos);
        $this->view->setVariable("selecaoTipos", $selecaoTipos);
        $this->view->setVariable("cursosCampus", $cursosCampus);
        $this->view->setVariable("situacoes", $situacoes);
        $this->view->setVariable("rematricula", 1);
        $this->view->setVariable("ingresso", $objAlunoCurso->getTiposel()->getTiposelId());
        $this->view->setVariable("cursoDoAluno", $cursoDoAluno);
        $this->view->setVariable("alunoMigracao", true);
        $this->view->setVariable("arrUnidade", $arrUnidade);
        $this->view->setVariable("arrAgenteEducacional", $arrAgenteEducacional);
        $this->view->setVariable("arrOrigemAlunoCurso", $arrOrigemAlunoCurso);

        $this->view->setTemplate('/matricula/acadperiodo-aluno/edit');

        return $this->view;
    }

    public function requerimentoMatriculaAction()
    {
        $request   = $this->getRequest();
        $arrParams = $this->params()->fromRoute('id');

        $arrParams = explode('&', $arrParams);

        $id          = $arrParams[0];
        $novaTurmaId = $arrParams[1];

        $serviceSisConfig = new \Sistema\Service\SisConfig(
            $this->getEntityManager(),
            $this->getServiceManager()->get('config')
        );

        $serviceMatricula = (new \Matricula\Service\AcadperiodoAluno($this->getEntityManager()));
        $serviceEndereco  = (new \Pessoa\Service\Endereco($this->getEntityManager()));
        $serviceInscricao = (new \Vestibular\Service\SelecaoInscricao($this->getEntityManager()));
        $service          = (new \Matricula\Service\AcadperiodoAluno($this->getEntityManager()));
        $estados          = $serviceEndereco->buscaEstados();
        /** @var \Matricula\Entity\AcadperiodoAluno $matricula */
        $matricula = $serviceMatricula->getEm()->getRepository($serviceMatricula->getEntity())->findOneBy(
            ['alunoperId' => $id]
        );

        if ($matricula) {
            $pes      = $matricula->getAlunocurso()->getAluno()->getPes()->getPes();
            $endereco = $serviceEndereco->getEm()->getRepository('Pessoa\Entity\Endereco')->findOneBy(
                array('pes' => $pes->getPesId())
            );
            $endereco->setEndEstado($estados[$endereco->getEndEstado()]);
            $telefone    = $serviceInscricao->buscaContatoPessoa($pes->getPesId(), 'Telefone');
            $celular     = $serviceInscricao->buscaContatoPessoa($pes->getPesId(), 'Celular');
            $email       = $serviceInscricao->buscaContatoPessoa($pes->getPesId(), 'E-mail');
            $periodo     = $service->buscaPeriodoLetivoAluno($id);
            $disciplinas = $service->retornaDisciplinasQueSeraoCursadas($novaTurmaId);
            $inscricao   = $serviceInscricao->getRepository($serviceInscricao->getEntity())->findOneBy(
                ['pes' => $pes, 'edicao' => $serviceInscricao->buscaEdicaoAtual()]
            );

            if (!is_null($inscricao)) {
                $classificacaoDados = $serviceInscricao->buscaClassificacaoCandidato($inscricao);
                $classificacao      = (int)$classificacaoDados['classificacao'];
                $nota               = $classificacaoDados['inscricao_nota'];
            }

            $ServiceOrgCampus = new \Organizacao\Service\OrgCampus($this->getEntityManager());
            $infoOrg          = $ServiceOrgCampus->retornaDadosInstituicao(
                $matricula->getAlunocurso()->getCursocampus()->getCamp()
            );
            $logo             = $infoOrg['logo'] ? $infoOrg['logo'] : getcwd() . '/public/img/logo.png';

            $arrEnderecoIes = explode(',', $infoOrg['endereco']);
            $pdf            = new PdfModel();
            $pdf->setOption('paperSize', 'a4'); // Defaults to "8x11"
            $pdf->setOption('paperOrientation', 'portrait'); // Defaults to "portrait"
            $pdf->setVariables(
                [
                    'pes'          => $pes,
                    'endereco'     => $endereco,
                    'matricula'    => $matricula,
                    'telefone'     => $telefone,
                    'disciplinas'  => $disciplinas,
                    'celular'      => $celular,
                    'email'        => $email,
                    'periodo'      => $periodo,
                    'inscricao'    => $inscricao,
                    'nota'         => $nota,
                    'clasificacao' => $classificacao,
                    'logo'         => $logo,
                    'ies'          => $infoOrg['ies'],
                    'curso'        => $matricula->getAlunocurso()->getCursocampus()->getCurso()->getCursoNome(),
                    'cidade'       => $arrEnderecoIes[3],
                    'dataBase'     => (new \DateTime('now'))->format("d/m/Y H:i")

                ]
            );

            if ($periodo[0]['turma_serie'] == '1') {
                $path = "matricula/acadperiodo-aluno/" . $serviceSisConfig->localizarChave(
                        'TEMPLATE_REQUERIMENTO_MATRICULA'
                    );
            } else {
                $path = "matricula/acadperiodo-aluno/" . $serviceSisConfig->localizarChave(
                        'TEMPLATE_REQUERIMENTO_REMATRICULA'
                    );
            };

            $pdf->setTemplate($path);

            if (!$this->verificaSeArquivoExiste($path)) {
                $this->flashMessenger()->addErrorMessage("Modelo de documento não encontrado no sistema!");

                return $this->redirect()->toRoute($this->getRoute(), ['controller' => $this->getController()]);
            }

            return $pdf;
        } else {
            $this->flashMessenger()->addErrorMessage("Requisição Inválida");

            return $this->redirect()->toRoute($this->getRoute(), ['controller' => $this->getController()]);
        }
    }

    public function contratoServicoAction()
    {
        $request = $this->getRequest();
        $id      = $this->params()->fromRoute('id');

        $arrConfig        = $this->getServiceManager()->get('Config');
        $serviceSisConfig = new \Sistema\Service\SisConfig($this->getEntityManager(), $arrConfig);
        $contrato         = $serviceSisConfig->localizarChave('MATRICULA_CONTRATO_SERVICO');

        $serviceMatricula  = new \Matricula\Service\AcadperiodoAluno($this->getEntityManager());
        $serviceOrgCampus  = new \Organizacao\Service\OrgCampus($this->getEntityManager());
        $serviceEndereco   = new \Pessoa\Service\Endereco($this->getEntityManager());
        $serviceInscricao  = new \Vestibular\Service\SelecaoInscricao($this->getEntityManager());
        $service           = new \Matricula\Service\AcadperiodoAluno($this->getEntityManager());
        $serviceAlunoCurso = new \Matricula\Service\AcadgeralAlunoCurso($this->getEntityManager());
        $serviceValores    = new \Financeiro\Service\FinanceiroValores($this->getEntityManager());
        $estados           = $serviceEndereco->buscaEstados();
        /* @var $matricula \Matricula\Entity\AcadperiodoAluno */
        $matricula = $serviceMatricula->getEm()->getRepository($serviceMatricula->getEntity())->findOneBy(
            ['alunoperId' => $id]
        );

        if ($matricula) {
            if ($serviceMatricula->verificaSeAlunnoEstaCancelado($matricula)) {
                $this->flashMessenger()->addErrorMessage(
                    "Não é possível imprimir contrato de serviço para alunos desligados."
                );

                return $this->redirect()->toRoute($this->getRoute(), ['controller' => $this->getController()]);
            }

            $arrAluno = array();

            $arrAluno['alunoCurso'] = $serviceAlunoCurso->getAlunocursoArray(
                $matricula->getAlunocurso()->getAlunocursoId()
            );
            $arrAluno['aluno']      = $arrAluno['alunoCurso']['aluno'];
            $arrAluno['pessoa']     = $arrAluno['alunoCurso']['aluno'];
            $arrAluno['campId']     = $arrAluno['alunoCurso']['campId'];

            $arrValoresMensalidade = $serviceValores->buscaValoresPeriodoConfig(
                \Financeiro\Service\FinanceiroTituloTipo::TIPOTITULO_MENSALIDADE,
                $matricula->getAlunocurso()->getCursocampus()->getCursocampusId(),
                $matricula->getTurma() ? $matricula->getTurma()->getPer()->getPerId() : false
            );

            if (!$arrValoresMensalidade and $serviceValores->getLastError()) {
                $this->flashMessenger()->addErrorMessage(
                    'Não foi localizado valor para Mensalidade' . "\n" . $serviceValores->getLastError()
                );

                $this->getView();

                return false;
            }

            $arrValoresMatricula = $serviceValores->buscaValoresPeriodoConfig(
                \Financeiro\Service\FinanceiroTituloTipo::TIPOTITULO_MATRICULA,
                $matricula->getAlunocurso()->getCursocampus()->getCursocampusId(),
                $matricula->getTurma() ? $matricula->getTurma()->getPer()->getPerId() : false
            );

            if (!$arrValoresMatricula and $serviceValores->getLastError()) {
                $this->flashMessenger()->addErrorMessage(
                    'Não foi localizado valor para Matricula' . "\n" . $serviceValores->getLastError()
                );

                $this->getView();

                return false;
            }

            $pes            = $matricula->getAlunocurso()->getAluno()->getPes()->getPes();
            $acadGeralAluno = $matricula->getAlunocurso()->getAluno();
            $pesFisica      = $matricula->getAlunocurso()->getAluno()->getPes();
            $endereco       = $serviceEndereco->getEm()->getRepository('Pessoa\Entity\Endereco')->findOneBy(
                array('pes' => $pes->getPesId())
            );

            $endereco->setEndEstado($estados[$endereco->getEndEstado()]);
            /**
             * Realizando a busca das informações do responsável pelo candidato.
             * */
            $responsavel = $serviceEndereco->getEm()->getRepository('Matricula\Entity\AcadgeralResponsavel')->findOneBy(
                array('aluno' => $acadGeralAluno, 'respNivel' => ['Legal', 'Financeiro'])
            );

            if ($responsavel) {
                $pesFisicaResp = $serviceEndereco->getEm()->getRepository('Pessoa\Entity\PessoaFisica')->findOneBy(
                    ['pes' => $responsavel->getPes()]
                );
                $respEndereco  = $serviceEndereco->getEm()->getRepository('Pessoa\Entity\Endereco')->findOneBy(
                    array('pes' => $pesFisicaResp->getPes())
                );
                $respEndereco->setEndEstado($estados[$endereco->getEndEstado()]);
                $respTel   = $serviceInscricao->buscaContatoPessoa($pesFisicaResp->getPes()->getPesId(), 'Telefone');
                $respCel   = $serviceInscricao->buscaContatoPessoa($pesFisicaResp->getPes()->getPesId(), 'Celular');
                $respEmail = $serviceInscricao->buscaContatoPessoa($pesFisicaResp->getPes()->getPesId(), 'E-mail');
            }
            /**
             * Fim das buscas por informações do candidato
             * */
            $telefone = $serviceInscricao->buscaContatoPessoa($pes->getPesId(), 'Telefone');
            $celular  = $serviceInscricao->buscaContatoPessoa($pes->getPesId(), 'Celular');
            $email    = $serviceInscricao->buscaContatoPessoa($pes->getPesId(), 'E-mail');
            $periodo  = $service->buscaPeriodoLetivoAluno($id);

            $dataAtual = (new \DateTime('now'));

            $arrayDataAtualFormatado
                = [
                'dia'        => $dataAtual->format('d'),
                'mesExtenso' => $service->mesReferencia($dataAtual->format('m')),
                'ano'        => $dataAtual->format('Y')

            ];

            $ServiceOrgCampus = new \Organizacao\Service\OrgCampus($this->getEntityManager());

            $infoOrg = $ServiceOrgCampus->retornaDadosInstituicao(
                $matricula->getAlunocurso()->getCursocampus()->getCamp()
            );

            $pdf = new PdfModel();
            $pdf->setOption('paperSize', 'a4'); // Defaults to "8x11"
            $pdf->setOption('paperOrientation', 'portrait'); // Defaults to "portrait"

            if ($contrato) {
                $pdf->setTemplate($contrato);
            }

            $pdf->setVariables($infoOrg);
            $pdf->setVariables($arrAluno);
            $pdf->setVariables(
                [
                    'valoresMensalidade' => $arrValoresMensalidade,
                    'valoresMatricula'   => $arrValoresMatricula,
                    'pes'                => $pes,
                    'pesFisica'          => $pesFisica,
                    'obJendereco'        => $endereco,
                    'matricula'          => $matricula,
                    'telefone'           => $telefone,
                    'celular'            => $celular,
                    'email'              => $email,
                    'periodo'            => $periodo,
                    'acadGeralAluno'     => $acadGeralAluno,
                    'responsavel'        => $responsavel,
                    'pesFisicaResp'      => $pesFisicaResp,
                    'respEndereco'       => $respEndereco,
                    'respTel'            => $respTel,
                    'respCel'            => $respCel,
                    'respEmail'          => $respEmail,
                    'mantenedora'        => $infoOrg['mantenedora'],
                    'dataFormatada'      => $arrayDataAtualFormatado
                ]
            );

            return $pdf;
        }
    }

    public function showFormAction()
    {
        $this->view->setTerminal(true);
        $form = $this->getForm();

        if (!class_exists($form)) {
            $this->view->setVariable("erro", "Formulário de busca não foi encontrado!");

            return $this->view;
        }

        $form            = new $form;
        $serviceTurma    = (new \Matricula\Service\AcadperiodoTurma($this->getEntityManager()));
        $serviceSituacao = (new \Matricula\Service\AcadgeralSituacao($this->getEntityManager()));

        $matriculaId = new \Zend\Form\Element\Text('alunocursoId');
        $matriculaId->setAttributes(
            [
                'col'         => '4',
                'wrap'        => false,
                'label'       => 'Matricula: ',
                'placeholder' => 'Informe o n° da matricula',
                'icon'        => 'icon-prepend fa fa-user',
            ]
        );
        $form->add($matriculaId);

        $turmaNome = new \Zend\Form\Element\Select('turmaId');
        $turmaNome->setAttributes(
            [
                'col'   => '2',
                'wrap'  => true,
                'label' => 'Turma Nome:',
            ]
        );
        $turmaNome->setValueOptions(
            $serviceTurma->mountSelect(['per_id' => '= 2'], ['ORDER BY' => 'turma_serie, turma_nome'])
        );
        $form->add($turmaNome);

        $turmaTurno = new \Zend\Form\Element\Text('turmaTurno');
        $turmaTurno->setAttributes(
            [
                'col'   => '2',
                'wrap'  => false,
                'label' => 'Turma Turno:',
            ]
        );
        $form->add($turmaTurno);

        $matsituacao = new \Zend\Form\Element\Select('situacao_aluno');
        $matsituacao->setAttributes(
            [
                'col'   => '2',
                'wrap'  => false,
                'label' => 'Situação da Matrícula:',
            ]
        );
        $matsituacao->setValueOptions($serviceSituacao->mountSelect());
        $form->add($matsituacao);

        $form->remove("pesNascUf");
        $form->remove("pesFalecido");
        $form->remove("pesNaturalidade");
        $form->remove("pesNacionalidade");
        $form->remove("pesCpfEmissao");
        $form->remove("pesRgEmissao");
        $form->remove("pesSobrenome");
        $form->remove("pesSexo");
        $form->remove("pesDataNascimento");
        $form->remove("pesEstadoCivil");
        $form->remove("pesFilhos");
        $form->remove("pesDocEstrangeiro");
        $form->remove("pesEmissorRg");
        $form->remove("alunoMae");
        $form->remove("alunoPai");
        $form->remove("alunoEtnia");
        $form->remove("alunoCertMilitar");
        $form->remove("alunoTituloEleitoral");
        $form->remove("alunoSecaoEleitoral");
        $form->remove("alunoZonaEleitoral");
        $form->remove("alunoTelefone");
        $form->remove("alunoEmail");
        $form->remove("alunoCelular");
        $form->remove("pesNecessidades");
        $form->remove("pesRg");
        $form->remove("alunoFoto");
        $form->remove("turmaNome");
        $form->remove("turmaTurno");
        $form->remove("pesCpf");
        //        $form->remove("turmaId");
        //        $form->remove("matsituacao_id");

        $form->setAttribute("method", 'get');
        $this->view->setVariable("config", $this->config);
        $this->view->setVariable("form", $form);

        return $this->view;
    }

    public function buscaMatriculaAction()
    {
        $request = $this->getRequest();
        $service = (new \Matricula\Service\AcadperiodoAluno($this->getEntityManager()));
        $result  = false;

        if ($request->isPost()) {
            $dados  = $request->getPost()->toArray();
            $result = $service->buscaAluno($dados['matricula']);
        }

        $this->json->setVariable("result", $result);

        return $this->json;
    }

    public function cancelarMatriculaPeriodoAction()
    {
        $id = $this->params()->fromRoute('id');

        if ($id) {
            $service   = (new \Matricula\Service\AcadperiodoAluno($this->getEntityManager()));
            $matricula = $service->cancelarMatriculaPeriodo($id);

            if ($matricula) {
                $this->flashMessenger()->addSuccessMessage("Matricula do aluno cancelada com sucesso . ");
            } else {
                $this->flashMessenger()->addErrorMessage(
                    "Não foi possivel cancelar a matricula do aluno . Favor entrar em contato com suporte . "
                );
            }
        }

        return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
    }

    public function resumoAlunoAction()
    {
        $formAluno      = $this->forms()->mountForm($this->getForm());
        $formAlunoCurso = $this->forms()->mountForm('Matricula\Form\AcadgeralAlunoCurso');

        $service                   = (new \Matricula\Service\AcadperiodoAluno($this->getEntityManager()));
        $serviceFinanceiroDesconto = $this->getServiceLocator()->get('Boleto\Service\FinanceiroDesconto');

        $alunoperId = $this->params()->fromRoute('id');

        $entity = $reference = $service->getReference($alunoperId);

        if ($entity) {
            $dadosAluno              = $entity->toArray();
            $dadosAluno['matricula'] = $dadosAluno['alunocurso'];

            $descontos['descontos'] = $serviceFinanceiroDesconto->buscaDescontosAluno($dadosAluno['alunoperId']);
            if ($descontos) {
                $totalPercentualDesconto = 0;
                foreach ($descontos['descontos'] as $desconto) {
                    $totalPercentualDesconto += $desconto['descontoPercentual'];
                }
                $dadosAluno                            = array_merge($dadosAluno, $descontos);
                $dadosAluno['totalPercentualDesconto'] = $totalPercentualDesconto;
            }

            if (!empty($dadosAluno['arq'])) {
                if ($dadosAluno['arq']->getArqId()) {
                    $this->view->setVariable("chaveFoto", $dadosAluno['arq']->getArqChave());
                    $this->view->setVariable("arqId", $dadosAluno['arq']->getArqId());
                }
            }

            $formAluno->setData($dadosAluno);
            $formAlunoCurso->setData($dadosAluno);
        }

        $this->view->setVariable('formAluno', $formAluno);
        $this->view->setVariable('formAlunoCurso', $formAlunoCurso);
        $this->view->setVariable('dadosAluno', $dadosAluno);

        $this->view->setTerminal(true);
        // caminho para o template que será renderizado
        $this->view->setTemplate('matricula/acadperiodo-aluno/resumo-aluno.phtml');
        $html = $this->getServiceLocator()->get('ViewRenderer')->render($this->view);

        return $this->view;
        // enviar conteudo de $html via ajax
    }

    public function vinculaFotosUsuariosAction()
    {
        $service  = (new \Matricula\Service\AcadperiodoAluno($this->getEntityManager()));
        $dir      = '/var/www/repos-versa/FOTOSALUNOS/';
        $arquivos = glob("$dir{*.jpg,*.png,*.gif,*.bmp}", GLOB_BRACE);
        $service->vinculaFotosAlunos($arquivos);

        return $this->view->setTerminal(true);
    }

    //funcao provisoria
    public function mudaArquivosDireotrioAction()
    {
        $service = (new \Matricula\Service\AcadperiodoAluno($this->getEntityManager()));
        $service->mudaArquivosDireotrio();

        return $this->view->setTerminal(true);
    }

    public function informacoesDisciplinasAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $objAcadperiodoAluno = new \Matricula\Service\AcadperiodoAluno($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $alunoperId    = $request->getPost('alunoperId');
            $arrAlunoCurso = $objAcadperiodoAluno->buscaNotasAlunoPorAlunoper($alunoperId);

            if (empty($arrAlunoCurso)) {
                $erro          = true;
                $erroDescricao = $objAcadperiodoAluno->getLastError();
            }

            $this->getJson()->setVariable('informacoes', $arrAlunoCurso);
            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }

    public function informacoesProvaIntegradoraAction()
    {
        $alunoperId = $this->params()->fromRoute('id');

        $this->getView()->setTerminal(true);

        $serviceAcadPeriodoLetivo = new \Matricula\Service\AcadperiodoLetivo($this->getEntityManager());

        $objPeriodoCorrente = $serviceAcadPeriodoLetivo->buscaPeriodoCorrente();

        if ($alunoperId) {
            $objAcadperiodoAluno = new \Matricula\Service\AcadperiodoAluno($this->getEntityManager());

            $arrInformacoesIntegradora = $objAcadperiodoAluno->buscaProvaIntegradora($alunoperId);

            // Verifica o período em vigência é o mesmo da integradora em questão.

            $arrInformacoesIntegradora['editaNotasIntegradora'] = 'Encerrada';

            if ($objPeriodoCorrente->getPerId() == $arrInformacoesIntegradora['periodoIntegradora']) {
                $arrInformacoesIntegradora['editaNotasIntegradora'] = 'Aberta';
            }

            $this->getView()->setVariables(
                [
                    'integradora' => $arrInformacoesIntegradora
                ]
            );
        }

        return $this->getView();
    }

    public function religamentoAlunoAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $dados = $request->getPost()->toArray();
        }
    }

    public function verificaSeArquivoExiste($path)
    {
        $path = getcwd() . '/module/Matricula/view/' . $path . '.phtml';

        return file_exists($path);
    }

    public function alteraSituacaoAlunoAction()
    {
        $service = (new \Matricula\Service\AcadperiodoAluno($this->getEntityManager()));
        $request = $this->getRequest();

        if ($request->isPost()) {
            if ($ok = $service->alteraSituacaoAlunoPeriodo($request->getPost()->toArray())) {
                $this->getJson()->setVariable("erro", false);
            } else {
                $this->getJson()->setVariable("erro", true);
                $this->getJson()->setVariable("mensagem", $service->getLastError());
            }
        }

        return $this->getJson();
    }

}
