<?php


namespace Matricula\Controller;


use VersaSpine\Controller\AbstractCoreController;

class AcadperiodoIntegradoraCadernoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {
    }

    public function buscaCadernosPorAvaliacaoAction()
    {
        $request = $this->getRequest();
        if($request->isPost()){
            $arrayData = $request->getPost()->toArray();
            $serviceCaderno = $this->Services()->getService($this->getService());
            $cadernos = $serviceCaderno->buscaCadernosPorAvaliacao($arrayData['avaliacao']);
            if($cadernos){
                $this->json->setVariable('cadernos', $cadernos);
            }

            return $this->json;
        }

        return $this->json->setVariable('error','Cadernos não encontrados');
    }

    public function correcaoAction()
    {
        $request = $this->getRequest();
        if($request->isPost()) {
            $dados = array_merge($request->getPost()->toArray(), $request->getFiles()->toArray());

            $file = $this->Services()->getService('GerenciadorArquivos\Service\Arquivo');

            if (!empty($dados['arquivoPrimeiroDia']['name']) && !empty($dados['arquivoSegundoDia']['name'])) {
                if ($dados['arquivoPrimeiroDia']['type'] == 'application/octet-stream' && $dados['arquivoSegundoDia']['type'] == 'application/octet-stream') {
                    $dadosArquivoPrimeiroDia = array(
                        'name' => $dados['arquivoPrimeiroDia']['name'],
                        'type' => $dados['arquivoPrimeiroDia']['type'],
                        'tmp_name' => $dados['arquivoPrimeiroDia']['tmp_name'],
                        'error' => $dados['arquivoPrimeiroDia']['error'],
                        'size' => $dados['arquivoPrimeiroDia']['size'],
                    );

                    $dadosArquivoSegundoDia = array(
                        'name' => $dados['arquivoSegundoDia']['name'],
                        'type' => $dados['arquivoSegundoDia']['type'],
                        'tmp_name' => $dados['arquivoSegundoDia']['tmp_name'],
                        'error' => $dados['arquivoSegundoDia']['error'],
                        'size' => $dados['arquivoSegundoDia']['size'],
                    );

                    try {
                        $arquivoPrimeiroDia = $file->adicionar($dadosArquivoPrimeiroDia);
                        $arquivoSegundoDia = $file->adicionar($dadosArquivoSegundoDia);
                    } catch (\Exception $ex) {
                        $this->flashMessenger()->addErrorMessage("Não foi possivel armazer o arquivo no servidor! Favor entrar em contato com suporte.");
                    }

                    $caminhoArq[0] = $arquivoPrimeiroDia->getDiretorio()->getarqDiretorioEndereco().'/'.$arquivoPrimeiroDia->getArqChave();
                    $caminhoArq[1] = $arquivoSegundoDia->getDiretorio()->getarqDiretorioEndereco().'/'.$arquivoSegundoDia->getArqChave();

                    $serviceIntegradoraAluno  = $this->services()->getService('Matricula\Service\AcadperiodoIntegradoraAluno');

                    try {
                        //tratar aqui como sera exibido os alunos nao encontrados (arquivo-formato, view...)
                        $result             = $serviceIntegradoraAluno->corrigeProvaPorArquivoAlunos($caminhoArq);
                        $alunoNaoEcontrados = $result['alunosNaoEncontrados'];
                    } catch (\Exception $ex) {
                        $this->flashMessenger()->addErrorMessage($ex->getMessage());
                    }

                    $this->flashMessenger()->addSuccessMessage("Prova corrigida com sucesso!");
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $dados['controller']));
                }
                $this->flashMessenger()->addSuccessMessage("Arquivo com formato inválido! Por favor selecione um arquivo válido e tente novamente.");
            }
            $this->flashMessenger()->addErrorMessage("Nenhum arquivo foi enviado! Favor selecionar um arquivo e tentar novamente.");
            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $dados['controller']));
        }
    }

    public function finalizacaoAction()
    {
        $serviceIntegradora = $this->services()->getService('Matricula\Service\AcadperiodoIntegradora');
        $integradora = $serviceIntegradora->buscaUltimaIntegradoraAtiva();


        if(!is_null($integradora)){
            $alunos = $serviceIntegradora->buscaDadosAlunosIntegradora($integradora->getIntegradoraId());
            if(!is_null($alunos)){
                $this->Services()->getService('Professor\Service\AcadperiodoEtapaAluno')->adicionarMultiplos($alunos);

                $integradora->setIntegradoraStatus("Encerrada");

                try {
                    $this->getEntityManager()->persist($integradora);
                    $this->getEntityManager()->flush();
                } catch (\Exception $ex) {
                    throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
                }
                $this->flashMessenger()->addWarningMessage("Notas Lançadas com sucesso!.");
                return $this->redirect()->toRoute('matricula/default', ['controller' => 'acadperiodo-integradora-avaliacao', 'action' => 'index']);
            }else{
                $this->flashMessenger()->addWarningMessage("Favor efetuar a correção das provas antes da finalização!.");
                return $this->redirect()->toRoute('matricula/default', ['controller' => 'acadperiodo-integradora-avaliacao', 'action' => 'index']);
            }
        }else{
            $this->flashMessenger()->addWarningMessage("Não possui integradora para este Periodo!.");
            return $this->redirect()->toRoute('matricula/default', ['controller' => 'acadperiodo-integradora-avaliacao', 'action' => 'index']);
        }
    }
}