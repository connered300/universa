<?php

namespace Matricula\Controller;

use VersaSpine\Controller\AbstractCoreController;

class AcadgeralAreaConhecimentoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request                          = $this->getRequest();
        $paramsGet                        = $request->getQuery()->toArray();
        $paramsPost                       = $request->getPost()->toArray();
        $serviceAcadgeralAreaConhecimento = new \Matricula\Service\AcadgeralAreaConhecimento($this->getEntityManager());

        $result = $serviceAcadgeralAreaConhecimento->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceAcadgeralAreaConhecimento = new \Matricula\Service\AcadgeralAreaConhecimento($this->getEntityManager());
        $serviceAcadgeralAreaConhecimento->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAcadgeralAreaConhecimento = new \Matricula\Service\AcadgeralAreaConhecimento(
                $this->getEntityManager()
            );

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceAcadgeralAreaConhecimento->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function editAction()
    {
        $areaId = $this->params()->fromRoute("id", 0);

        if (!$areaId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($areaId);
    }

    public function addAction($areaId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados                         = array();
        $serviceAcadgeralAreaConhecimento = new \Matricula\Service\AcadgeralAreaConhecimento($this->getEntityManager());

        if ($areaId) {
            $arrDados = $serviceAcadgeralAreaConhecimento->getArray($areaId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceAcadgeralAreaConhecimento->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de área de conhecimento salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceAcadgeralAreaConhecimento->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceAcadgeralAreaConhecimento->getLastError());
                }
            }
        }

        $serviceAcadgeralAreaConhecimento->formataDadosPost($arrDados);
        $serviceAcadgeralAreaConhecimento->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAcadgeralAreaConhecimento = new \Matricula\Service\AcadgeralAreaConhecimento(
                $this->getEntityManager()
            );

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceAcadgeralAreaConhecimento->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceAcadgeralAreaConhecimento->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}
?>