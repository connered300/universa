<?php

namespace Matricula\Controller;

use Matricula\Service\EtapaFinal;
use VersaSpine\Controller\AbstractCoreController;

class EtapaFinalController extends AbstractCoreController
{
    public function __construct(){
        parent::__construct(__CLASS__);
    }

    public function indexAction(){
        return $this->view;
    }

    public function alunosDisciplinaAction(){
        return $this->view;
    }

    public function listagemDisciplinasAction(){
        $matricula = $this->params()->fromRoute("id", 0);

        if (!$matricula) {
            $erro = "Matricula não foi passada";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        $this->view->setVariable('matricula',$matricula);

        return $this->view;
    }

    public function listagemAlunosAction(){
        $request = $this->getRequest();

        if ($request->isPost()) {
            $arrayData = $request->getPost()->toArray();

            $servicePeriodo = new \Matricula\Service\AcadperiodoLetivo($this->getEntityManager());

            $serviceAluno = new EtapaFinal($this->getEntityManager());

            $periodo = $servicePeriodo->buscaPeriodoCorrente()->getPerId();

            $result = $serviceAluno->paginationAjax($arrayData,$periodo);

            $this->json->setVariable("draw", $result["draw"]);
            $this->json->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->json->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->json->setVariable("data", $result["data"]);
        }

        return $this->json;
    }

    public function disciplinasAction(){
        $request = $this->getRequest();

        if ($request->isPost()) {
            $arrayData = $request->getPost()->toArray();

            $serviceAluno = new EtapaFinal($this->getEntityManager());

            $result = $serviceAluno->paginationAjaxDisciplinas($arrayData);

            $this->json->setVariable("draw", $result["draw"]);
            $this->json->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->json->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->json->setVariable("data", $result["data"]);
        }

        return $this->json;
    }

    public function verificacaoLancamentoAction(){
        $request = $this->getRequest();

        if ($request->isPost()) {
            $arrayData = $request->getPost()->toArray();

            $serviceAluno = new EtapaFinal($this->getEntityManager());

            $result = $serviceAluno->paginationAjaxDisciplinas($arrayData);

            $this->json->setVariable("draw", $result["draw"]);
            $this->json->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->json->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->json->setVariable("data", $result["data"]);
        }

        return $this->json;
    }
}
