<?php
namespace Matricula\Controller;

use DOMPDFModule\View\Model\PdfModel;
use Matricula\Service\AcadgeralAluno;
use Sistema\Service\SisConfig;
use VersaSpine\Controller\AbstractCoreController;

class AcadgeralAlunoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {
        $request    = $this->getRequest();
        $paramsGet  = $request->getQuery()->toArray();
        $paramsPost = $request->getPost()->toArray();
        $param      = array_merge($paramsGet, $paramsPost);

        $serviceAcadgeralAluno = new AcadgeralAluno($this->getEntityManager());

        $arrDados = $serviceAcadgeralAluno->pesquisaForJson($param);

        if ($param['pesId'] && $param['dadosDeAluno'] && $arrDados) {
            $arrDados = $arrDados[0];

            $arrDados = $serviceAcadgeralAluno->getArrayFallback($arrDados['alunoId'], $arrDados['pesId']);
            $serviceAcadgeralAluno->formataDadosPost($arrDados);
        }

        return new \Zend\View\Model\JsonModel($arrDados ? $arrDados : array());
    }

    public function captacaoAction($agente = false)
    {
        $request  = $this->getRequest();
        $arrDados = array();

        $arrConfig             = $this->getServiceManager()->get('Config');
        $serviceCadastroOrigem = new \Matricula\Service\AcadgeralCadastroOrigem($this->getEntityManager());
        $serviceOrgAgente      = new \Organizacao\Service\OrgAgenteEducacional($this->getEntityManager());
        $serviceAlunoCurso     = new \Matricula\Service\AcadgeralAlunoCurso($this->getEntityManager());
        $serviceEstatistica    = new \Comunicacao\Service\ComunicacaoCampanhaEstatistica($this->getEntityManager());
        $serviceAcadgeralAluno = new \Matricula\Service\AcadgeralAluno($this->getEntityManager(), $arrConfig);

        $maxiPagoAtivo = $serviceAcadgeralAluno->getConfig()->localizarChave('FINANCEIRO_MAXI_PAGO_ATIVO', 0);

        $arrGet = $request->getQuery()->toArray();

        $arrDados = array_merge(
            $arrConfig,
            $arrDados,
            array_merge_recursive(
                $arrGet,
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            )
        );

        $serviceAcadgeralAluno->pesquisarDadosDeAlunoECursoPelaintegracao($arrDados);

        if (!$request->isPost()) {
            $arrDados['captacaoInicio'] = true;
        }

        if ($arrDados['pesCpf'] && $arrDados['captacaoInicio']) {
            $arrDados['forcarCaptacaoContinue'] = true;
            $serviceEstatistica->atualizarCookies($arrDados);
        }

        if (($request->isPost() && $arrDados['captacaoInicio']) || $arrDados['forcarCaptacaoContinue']) {
            $arrDadosAluno = $serviceAcadgeralAluno->buscaAlunoPorCpf($arrDados['pesCpf']);
            $arrDados      = array_merge($arrDadosAluno, $arrDados);

            if ($arrDados['alunoId']) {
                $arrDados['captacaoContinueCadastradao'] = true;
            } else {
                $arrDados['captacaoContinueNaoCadastradao'] = true;
            }
        }

        if ($request->isPost() && $arrDados['alunoId']) {
            $arrDadosAluno = $serviceAcadgeralAluno->getArray($arrDados['alunoId']);
            $arrDados      = array_merge($arrDadosAluno, $arrDados);
        }

        $arrDados['validaVencimentoCaptacao'] = true;
        $arrDados                             = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);

        if (!$arrDados['pesIdAgente']) {
            if ($arrGet['pesIdAgenciador']) {
                $arrDados['pesIdAgente'] = $arrGet['pesIdAgenciador'];
            } elseif ($arrGet['pesIdAgente']) {
                $arrDados['pesIdAgente'] = $arrGet['pesIdAgente'];
            }
        }

        if (!$arrDados['alunocursoPessoaIndicacao']) {
            if ($arrGet['alunoPessoaIndicacao']) {
                $arrDados['alunocursoPessoaIndicacao'] = $arrGet['alunoPessoaIndicacao'];
            } elseif ($arrGet['alunocursoPessoaIndicacao']) {
                $arrDados['alunocursoPessoaIndicacao'] = $arrGet['alunocursoPessoaIndicacao'];
            }
        }

        if (!$arrDados['alunocursoMediador']) {
            if ($arrGet['alunoMediador']) {
                $arrDados['alunocursoMediador'] = $arrGet['alunoMediador'];
            } elseif ($arrGet['alunocursoMediador']) {
                $arrDados['alunocursoMediador'] = $arrGet['alunocursoMediador'];
            } elseif ($agente) {
                $arrDados['alunocursoMediador'] = 'captacao-agente';
            } else {
                $arrDados['alunocursoMediador'] = 'captacao-externa';
            }
        }

        if ($request->isPost() && !$arrDados['captacaoInicio']) {
            $arrDados['captacao'] = true;

            $salvar = $serviceAcadgeralAluno->registroSimplificadoDeAluno($arrDados);

            if ($salvar) {
                $efetuouDeferimento = $serviceAlunoCurso->efetivarDeferimentoAutomaticoCurso($arrDados);
                $serviceEstatistica->atualizarEstatistica($arrDados);

                $rota         = 'matricula/default';
                $arrParamRota = ['controller' => 'acadgeral-aluno', 'action' => 'captacao', 'query' => []];
                $arrReuse     = ['query' => ['exibirCampanhas' => true]];

                $this->flashMessenger()->addSuccessMessage('Seu cadastro foi registrado com sucesso!');

                if (!$agente) {
                    $this->flashMessenger()->addSuccessMessage('Em breve entraremos em contato!');

                    if ($efetuouDeferimento) {
                        $rota                        = 'financeiro/default';
                        $arrParamRota['controller']  = 'painel';
                        $arrParamRota['action']      = 'titulos';
                        $arrReuse['query']['pesCpf'] = $arrDados['pesCpf'];
                    }
                } else {
                    $arrParamRota['controller'] = 'acadgeral-aluno-captacao';
                }

                if ($arrDados['pesIdAgente']) {
                    $arrReuse['query']['pesIdAgente'] = $arrDados['pesIdAgente'];
                }

                if ($arrDados['alunocursoMediador']) {
                    $arrReuse['query']['alunocursoMediador'] = $arrDados['alunocursoMediador'];
                }

                return $this->redirect()->toRoute($rota, $arrParamRota, $arrReuse);
            } else {
                $this->flashMessenger()->addErrorMessage($serviceAcadgeralAluno->getLastError());
            }
        }

        $serviceAcadgeralAluno->formataDadosPost($arrDados);
        $serviceAcadgeralAluno->setarDependenciasView($this->getView(), $arrDados);

        if (!$agente) {
            $this->layout('layout/simples');
        } else {
            if (!$arrDados['origem']) {
                $arrDados['origem'] = $serviceCadastroOrigem->getArrSelect2(['origemNome' => 'Agente Educacional']);
                $arrDados['origem'] = $arrDados['origem'] ? $arrDados['origem'][0] : null;
            }

            if (!$arrDados['pesIdAgente']) {
                $arrDados['pesIdAgente'] = $serviceOrgAgente->getAgenteLogado();
            }

            if ($arrDados['pesIdAgente']) {
                if ($arrDados['pesIdAgente']['id'] <= 0) {
                    $arrDados['pesIdAgente'] = null;
                } else {
                    $rotaCaptacao = $this->url()->fromRoute(
                        'matricula/default',
                        array('controller' => 'acadgeral-aluno', 'action' => 'captacao')
                    );

                    $linkAgente = (
                        $this->getBasePath() .
                        $rotaCaptacao .
                        '?pesIdAgente=' . $arrDados['pesIdAgente']['id']
                    );
                    $this->getView()->setVariable('linkAgente', $linkAgente);
                }
            }

            if (!$arrDados['pesIdAgente']) {
                return $this->redirect()->toRoute("acesso-autenticacao-logout");
            }
        }

        $tpl = 'captacao-inicio';

        if ($arrDados['captacaoContinueCadastradao']) {
            $tpl = 'captacao-adicao-curso';
        } elseif ($arrDados['captacaoContinueNaoCadastradao']) {
            $tpl = 'captacao-aluno-nao-cadastrado';
        } else {
            $this->getView()->setVariable('inicio', true);
        }

        $arrCamposCaptacao = $serviceAcadgeralAluno->getConfig()->localizarChave('ALUNO_CAMPOS_CAPTACAO', '');
        $arrCamposCaptacao = $arrCamposCaptacao ? explode(',', $arrCamposCaptacao) : [];

        $this->getView()->setVariable('arrCamposPersonalizados', $serviceAcadgeralAluno->retornaCamposPersonalizados());
        $this->getView()->setVariable('arrCamposCaptacao', $arrCamposCaptacao);
        $this->getView()->setVariable('maxiPagoAtivo', $maxiPagoAtivo);
        $this->getView()->setVariable('agente', $agente);
        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute($tpl, 'acadgeral-aluno'));

        return $this->getView();
    }

    public function addAction($alunoId = false)
    {
        $request  = $this->getRequest();
        $arrDados = array();

        $config                = $this->getServiceLocator()->get('config');
        $serviceAcadgeralAluno = new AcadgeralAluno($this->getEntityManager(), $config);
        $serviceAcessoPessoa   = new \Acesso\Service\AcessoPessoas($this->getEntityManager(), $config);
        $serviceEstatistica    = new \Comunicacao\Service\ComunicacaoCampanhaEstatistica($this->getEntityManager());

        if ($alunoId) {
            $arrDados = $serviceAcadgeralAluno->getArray($alunoId);

            if (empty($arrDados)) {
                $this->flashMessenger()->addErrorMessage($serviceAcadgeralAluno->getLastError());

                return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $config,
                $arrDados,
                array(
                    'responsaveis'   => '',
                    'formacoes'      => '',
                    'alunoCurso'     => [],
                    'documentoGrupo' => [],
                ),
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );

            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceAcadgeralAluno->salvarAluno($arrDados);
            $serviceEstatistica->atualizarEstatistica($arrDados);

            if ($salvar) {
                $this->flashMessenger()->addSuccessMessage('Aluno salvo!');

                if ($arrDados['arrCodigoMatricula']) {
                    $this->flashMessenger()->addSuccessMessage(
                        'Matricula(s) do aluno: ' . implode(', ', $arrDados['arrCodigoMatricula'])
                    );
                }

                foreach ($arrDados['alunoCurso'] as $alunocurso) {
                    if ($alunocurso['novoCurso']) {
                        $arrReuse['query'] = ['exibirCampanhas' => true];
                        break;
                    }
                }

                return $this->redirect()
                    ->toRoute($this->getRoute(), ['controller' => $this->getController()], $arrReuse);
            } else {
                $this->flashMessenger()->addErrorMessage($serviceAcadgeralAluno->getLastError());
            }
        } else {
            $arrParam = $request->getQuery()->toArray();

            if ($arrParam['pesId'] && !$alunoId) {
                /** @var \Matricula\Entity\AcadgeralAluno $objAcadgeralAluno */
                $objAcadgeralAluno = $serviceAcadgeralAluno->retornaAlunoPeloPesId($arrParam['pesId']);

                if ($objAcadgeralAluno) {
                    $alunoId = $objAcadgeralAluno->getAlunoId();

                    return $this->redirect()->toRoute(
                        $this->getRoute(),
                        array('controller' => $this->getController(), 'action' => 'edit', 'id' => $alunoId)
                    );
                } else {
                    $arrDados = $serviceAcadgeralAluno->getArrayFallback(false, $arrParam['pesId']);
                }

                if (!$arrDados['alunoMediador'] && $arrParam['alunoMediador']) {
                    $arrDados['alunoMediador'] = $arrParam['alunoMediador'];
                }
            }
        }

        if (!$arrDados['alunoMediador']) {
            $arrDados['alunoMediador'] = 'registro-interno';
        }

        $cpfObrigatorio                         = $serviceAcadgeralAluno->getConfig()
            ->localizarChave('DESABILITAR_OBRIGATORIEDADE_CPF');
        $permitirEdicaoAgente                   = $serviceAcessoPessoa->validaPessoaEditaAgenteAluno();
        $permitirEdicaoAlunoIndicacao           = $serviceAcessoPessoa->validaPessoaEditaPessoaIndicacaoAluno();
        $permitirEdicaoObservacaoDocumentoAluno = $serviceAcessoPessoa->validaPessoaEditaObservacaoDocumentoAluno();

        $serviceAcadgeralAluno->formataDadosPost($arrDados);
        $serviceAcadgeralAluno->setarDependenciasView($this->getView(), $arrDados);

        $chave = $arrDados['alunoId'];

        if ($arrDados['alunoCurso']) {
            foreach ($arrDados['alunoCurso'] as $value) {
                $chave .= ',' . $value['alunocursoId'];
            }
        }

        $arrCamposPersonalizados = $serviceAcadgeralAluno->retornaCamposPersonalizados((int)$chave);

        $this->getView()->setVariable('arrCamposPersonalizados', $arrCamposPersonalizados);
        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setVariable("editaAgente", $permitirEdicaoAgente);
        $this->getView()->setVariable("desabilitarObrigatoriedadeCpf", $cpfObrigatorio);
        $this->getView()->setVariable("editaAlunoIndicacao", $permitirEdicaoAlunoIndicacao);
        $this->getView()->setVariable("editaObservacaoDocumentoAluno", $permitirEdicaoObservacaoDocumentoAluno);
        $this->getView()->setVariable("alunoId", $alunoId);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $identificador = $this->params()->fromRoute("id", 0);

        if (!$identificador) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($identificador);
    }

    public function carteirinhaAction()
    {
        $arrConfig = $this->getServiceLocator()->get('config');

        $serviceAluno      = new AcadgeralAluno($this->getEntityManager());
        $serviceSis        = new SisConfig($this->getEntityManager(), $arrConfig);
        $serviceAlunoCurso = new \Matricula\Service\AcadgeralAlunoCurso($this->getEntityManager());

        $request = $this->getRequest();

        $matricula = false;

        if ($request->isPost()) {
            $matricula = $request->getPost()->toArray()['numMat'];
        }

        if ($matricula) {
            $service = $this->services()->getService($this->getService());
            /** @var \Matricula\Entity\AcadgeralAlunoCurso $matricula */
            $matricula = $service->getRepository('Matricula\Entity\AcadgeralAlunoCurso')->findOneBy(
                ['alunocursoId' => $matricula]
            );

            if (!$matricula) {
                $this->flashMessenger()->addErrorMessage("Matrícula inexistente");

                return $this->redirect()->toRoute(
                    'matricula/default',
                    ['controller' => 'acadgeral-aluno', 'action' => 'carteirinha']
                );
            }

            $arq = $matricula->getAluno()->getArq();

            $layoutCarteira = $serviceAluno->buscaLayoutCarteirinha($matricula->getAlunocursoId());

            if (!$arq) {
                $this->flashMessenger()->addErrorMessage("Esta matrícula não possui foto");

                return $this->redirect()->toRoute(
                    'matricula/default',
                    ['controller' => 'acadgeral-aluno', 'action' => 'carteirinha']
                );
            } else {
                $arq = $arq->getArqId();

                if (!$arq) {
                    $this->flashMessenger()->addErrorMessage("Esta matrícula não possui foto");

                    return $this->redirect()->toRoute(
                        'matricula/default',
                        ['controller' => 'acadgeral-aluno', 'action' => 'carteirinha']
                    );
                }
            }

            $mensagem = $serviceSis->localizarChave('CARTEIRINHA_MENSAGEM');

            $arrInfoAluno = $serviceAlunoCurso->getAlunocursoArray($matricula->getAlunocursoId());

            $arrNovoNome = array();
            $arrNome     = explode(" ", $arrInfoAluno['pesNome']);

            foreach ($arrNome as $index => $nome) {
                $arrNome[$index] = $arrNome[$index] = ucfirst(mb_strtolower($nome, "UTF-8"));
            }

            if (strlen($arrInfoAluno['pesNome']) > 35) {
                $primeiroNome = array_shift($arrNome);
                $ultimoNome   = array_pop($arrNome);

                $tamanhoAtual = strlen($arrInfoAluno['pesNome']);

                $arrNovoNome[] = $primeiroNome;

                while (count($arrNome) > 0 && $tamanhoAtual > 35) {
                    $nome          = array_shift($arrNome);
                    $arrNovoNome[] = $nome[0] . '.';
                    $tamanhoAtual  = strlen(implode(' ', array_merge($arrNovoNome, $arrNome, [$ultimoNome])));
                }

                $arrNovoNome   = array_merge($arrNovoNome, $arrNome);
                $arrNovoNome[] = $ultimoNome;
            } else {
                $arrNovoNome = $arrNome;
            }

            $foto = $matricula->getAluno()->getArq()->getDiretorio()->getArqDiretorioEndereco()
                . "/" . $matricula->getAluno()->getArq()->getArqChave();

            $pdf = new PdfModel();
            $pdf->setOptions(['paperOrientation' => 'landscape', 'paperSize' => 'a4',]);
            $pdf->setVariables(
                [
                    'aluno'     => implode(" ", $arrNovoNome),
                    'matricula' => ltrim($arrInfoAluno['alunocursoId'], 0),
                    'foto'      => $foto,
                    'layout'    => $layoutCarteira,
                    'mensagem'  => $mensagem,
                    'ies'       => $arrInfoAluno['iesNome'],
                    'nivel'     => $arrInfoAluno['cursocampus']['nivelNome']
                ]
            );

            return $pdf;
        } else {
            $this->view->setTemplate('matricula/acadgeral-aluno/form-carteirinha');

            return $this->view;
        }
    }

    public function relatorioAlunosAction()
    {
        $serviceOrgCampus      = new \Organizacao\Service\OrgCampus($this->getEntityManager());
        $serviceAcadgeralAluno = new \Matricula\Service\AcadgeralAluno($this->getEntityManager());
        $request               = $this->getRequest();

        if ($request->isPost()) {
            $pdf                 = $this->getServiceLocator()->get('SnowReportsZf2\Service\SnowReportsZf2');
            $arrDadosInstituicao = $serviceOrgCampus->retornaDadosInstituicao();
            $dados               = $request->getPost()->toArray();
            $info                = $serviceAcadgeralAluno->retornaDescricaoFiltrosParaConsultaDeAlunos($dados);
            $condicoes           = $serviceAcadgeralAluno->retornaFiltrosParaConsultaDeAlunos($dados);

            $dados['tipo'] = $dados['tipo'] == 'xlsx' ? 'xlsx' : 'pdf';

            $arquivo = "relatorio-alunos-" . $dados['tipo'] . ".jrxml";

            $pdf->setJrxmlPath(__DIR__ . '/../Reports/')
                ->setJrxmlFile($arquivo)
                ->setAttrbutes(
                    [
                        'instituicao'   => $arrDadosInstituicao['ies'],
                        'mantenedora'   => $arrDadosInstituicao['mantenedora'],
                        'titulo'        => 'Relatorio de Alunos',
                        'logo'          => $arrDadosInstituicao['logo'],
                        'relatorioinfo' => $info,
                        'sistema'       => 'Universa',
                        'endereco'      => $arrDadosInstituicao['endereco'],
                        'condicoessql'  => $condicoes
                    ]
                )
                ->downloadHtmlPdf(true)
                ->setOutputFile('relatorio-alunos', $dados['tipo'])
                ->execute();
            exit();
        }

        $this->flashMessenger()->addErrorMessage("Sem filtros disponíveis para a geração do relatório!");

        return $this->redirect()->toRoute('matricula/default', ['controller' => 'acadgeral-aluno']);
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAcadgeralAluno = new AcadgeralAluno($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();

            $fazerBusca = 1;

            if ($dataPost['buscaComFiltro'] && count($dataPost['filter']) == 1 && !$dataPost['search']['value']) {
                $fazerBusca = 0;
            }

            if ($fazerBusca) {
                $result = $serviceAcadgeralAluno->getDataForDatatables($dataPost);
            }

            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable(
                "recordsFiltered",
                ($result["recordsFiltered"] ? $result["recordsFiltered"] : 1)
            );
            $this->getJson()->setVariable("data", ($result["data"] ? $result["data"] : []));

            return $this->getJson();
        }

        return $this->getJson();
    }

    public function indexAction($agente = false)
    {
        $time2   = $time = time();
        $arrTime = array();

        $arrConfig                   = $this->getServiceManager()->get('Config');
        $serviceAcessoPessoas        = new \Acesso\Service\AcessoPessoas($this->getEntityManager(), $arrConfig);
        $serviceOrgAgenteEducacional = new \Organizacao\Service\OrgAgenteEducacional($this->getEntityManager());
        $serviceAcadgeralAluno       = new AcadgeralAluno($this->getEntityManager());

        $maxiPagoAtivo = $serviceAcadgeralAluno->getConfig()->localizarChave('FINANCEIRO_MAXI_PAGO_ATIVO', 0);

        $arrTime['servicosController'] = time() - $time2;
        $time2                         = time();

        $serviceAcadgeralAluno->setarDependenciasView($this->getView(), null, false);

        $arrTime['dependencias'] = time() - $time2;
        $time2                   = time();

        $permissaoEditarAluno          = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Matricula\Controller\AcadgeralAluno',
            'edit'
        );
        $permissaoDeferirCurso         = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Matricula\Controller\AcadgeralAlunoCurso',
            'deferir-aluno-curso'
        );
        $permissaoRelatorioAlunos      = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Matricula\Controller\AcadgeralAluno',
            'relatorio-alunos'
        );
        $permissaoAlterarSituacaoCurso = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Matricula\Controller\AcadgeralAlunoCurso',
            'alterar-situacao'
        );
        $permissaoAlterarDatasCurso    = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Matricula\Controller\AcadgeralAlunoCurso',
            'alterar-datas'
        );
        $permissaoFichaInscricaoCurso  = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Matricula\Controller\Relatorios',
            'ficha-inscricao'
        );
        $permissaoEmitirDocumento      = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Documentos\Controller\DocumentoModelo',
            'emitir'
        );
        $permissaoTrocarCurso          = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Matricula\Controller\AcadgeralAlunoCurso',
            'trocar-curso'
        );
        $permissaoOutrasInformacoes    = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Matricula\Controller\AcadgeralAlunoCurso',
            'outras-informacoes'
        );
        $permissaoIntegracaoManual     = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Matricula\Controller\AcadgeralAluno',
            'integracao-manual'
        );
        $permissaoEnvioComunicacao     = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Organizacao\Controller\OrgComunicacaoModelo',
            'enviar-comunicacao'
        );
        $permissaoIntegracaoNota       = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Matricula\Controller\AcadgeralAluno',
            'integracao-manual-nota'
        );

        $arrTime['permissoes'] = time() - $time2;
        $time2                 = time();

        $arrPermissoes = array(
            'editarAluno'        => ($permissaoEditarAluno ? 1 : 0),
            'deferirCurso'       => ($permissaoDeferirCurso ? 1 : 0),
            'alterarSituacao'    => ($permissaoAlterarSituacaoCurso ? 1 : 0),
            'alterarDatas'       => ($permissaoAlterarDatasCurso ? 1 : 0),
            'emitirDocumento'    => ($permissaoEmitirDocumento ? 1 : 0),
            'fichaInscricao'     => ($permissaoFichaInscricaoCurso ? 1 : 0),
            'relatorioAlunos'    => ($permissaoRelatorioAlunos ? 1 : 0),
            'trocarCurso'        => ($permissaoTrocarCurso ? 1 : 0),
            'outrasInformacoes'  => ($permissaoOutrasInformacoes ? 1 : 0),
            'integracaoManual'   => ($permissaoIntegracaoManual ? 1 : 0),
            'envioComunicacao'   => ($permissaoEnvioComunicacao ? 1 : 0),
            'integracaoNota'     => ($permissaoIntegracaoNota ? 1 : 0),
            'integracaoMaxiPago' => ($maxiPagoAtivo ? 1 : 0),
        );

        $this->getView()->setTemplate($this->getTemplateToRoute('index', 'acadgeral-aluno'));

        $this->getView()->setVariable('arrPermissoes', $arrPermissoes);

        if ($agente) {
            $this
                ->getView()
                ->setVariable('agente', true)
                ->setVariable('agenteEducacional', $serviceOrgAgenteEducacional->getAgenteLogado());
        }

        $arrTime['fim']   = time() - $time2;
        $arrTime['total'] = time() - $time;

        if ($_GET['reportTime']) {
            echo '<pre>' . print_r($arrTime, 1) . '</pre>';
            exit(0);
        }

        return $this->getView();
    }

    public function integracaoManualAction()
    {
        $serviceAcadgeralAluno = new \Matricula\Service\AcadgeralAluno(
            $this->getEntityManager(),
            $this->getServiceManager()->get('Config')
        );

        $request            = $this->getRequest();
        $result             = [];
        $result['erro']     = false;
        $result['mensagem'] = "Aluno registrado na fila de integração!";
        $paramsGet          = $request->getQuery()->toArray();
        $paramsPost         = $request->getPost()->toArray();
        $arrDados           = array_merge($paramsGet, $paramsPost);

        if ($arrDados) {
            $arrAlunos = is_array($arrDados['alunoId']) ? implode(',', $arrDados['alunoId']) : $arrDados['alunoId'];
            $arrAlunos = explode(',', $arrAlunos);

            foreach ($arrAlunos as $alunoId) {
                if (!$integracao = $serviceAcadgeralAluno->efetuarIntegracaoManual($alunoId)) {
                    $result['mensagem'] = $serviceAcadgeralAluno->getLastError() . ' Aluno: ' . $alunoId . '.';
                    $result['erro']     = true;
                    break;
                }
            }
        }

        $this->getJson()->setVariables($result);

        return $this->getJson();
    }

    public function integracaoManualNotaAction()
    {
        $serviceAcadgeralAluno = new \Matricula\Service\AcadgeralAluno(
            $this->getEntityManager(),
            $this->getServiceManager()->get('Config')
        );

        $request        = $this->getRequest();
        $result         = [];
        $result['erro'] = false;
        $paramsGet      = $request->getQuery()->toArray();
        $paramsPost     = $request->getPost()->toArray();
        $arrDados       = array_merge($paramsGet, $paramsPost);

        if ($arrDados && isset($arrDados['alunoCursoId'])) {
            $integracao = $serviceAcadgeralAluno->efetuarIntegracaoManualNota($arrDados['alunoCursoId'], true);

            if (is_array($integracao)) {
                $result['sucessoComErro']  = true;
                $result['mensagemSucesso'] = $integracao['sucesso'];
                $result['mensagemErro']    = $integracao['erro'];
            } elseif (!$integracao) {
                $result['erro']     = true;
                $result['mensagem'] = $serviceAcadgeralAluno->getLastError();
            } else {
                $result['mensagem'] = "Integração de nota realizada!";
            }
        } elseif (!isset($arrDados['alunoCursoId'])) {
            $result['erro']     = true;
            $result['mensagem'] = "Para efetuar a integração de notas, informe o aluno!";
        }

        $this->getJson()->setVariables($result);

        return $this->getJson();
    }

    public function testeNumeroMatriculaAction()
    {
        $serviceAcadgeralAluno = new \Matricula\Service\AcadgeralAlunoCurso(
            $this->getEntityManager(),
            $this->getServiceManager()->get('Config')
        );

        $request    = $this->getRequest();
        $paramsGet  = $request->getQuery()->toArray();
        $paramsPost = $request->getPost()->toArray();
        $arrDados   = array_merge($paramsGet, $paramsPost);

        $arrPadroes = array(
            'AAAAAAAAAAAA',
            'YYAAAAAAAAAA',
            'YYAAAAA',
            'YYCCAAAA',
            'YYCCCAAA',
            'YYCCAAAAA',
            'YYCCCAAAA',
            'YYCCCAAAAA',
            'YYCCCCAAAAA',
            'YYCCCCCAAAAA',
        );

        $curso = $arrDados['cursoId'];

        foreach ($arrPadroes as $strPadrao) {
            $matricula = $serviceAcadgeralAluno->criarNumeroDeMatricula($curso, $strPadrao);
            echo $strPadrao . ' -> ' . $matricula . '<br>';
        }

        exit();
    }
}