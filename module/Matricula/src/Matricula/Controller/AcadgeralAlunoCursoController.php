<?php
namespace Matricula\Controller;

use VersaSpine\Controller\AbstractCoreController;

class AcadgeralAlunoCursoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request    = $this->getRequest();
        $paramsGet  = $request->getQuery()->toArray();
        $paramsPost = $request->getPost()->toArray();
        $param      = array_merge($paramsGet, $paramsPost);

        $objServiceAlunoCurso        = new \Matricula\Service\AcadgeralAlunoCurso($this->getEntityManager());
        $serviceFinanceiroTituloTipo = new \Financeiro\Service\FinanceiroTituloTipo($this->getEntityManager());

        if ($param['alunoConfigPagto']) {
            $arrDados = $objServiceAlunoCurso->getArrayAlunoCurso(
                ['alunocursoId' => $param['alunocursoId']]
            );

            $arrDados = $arrDados ? $arrDados[0] : array();

            if ($arrDados) {
                $arrDados['tipoTitulo'] = $serviceFinanceiroTituloTipo->retornoTiposDeTituloParaEmissaoNaMatricula(
                    $arrDados['cursocampusId'],
                    true
                );
            }
        } else {
            $arrDados = $objServiceAlunoCurso->pesquisaForJson($param);
        }

        return new \Zend\View\Model\JsonModel($arrDados ? $arrDados : array());
    }

    public function alunoCursoInformacoesAction()
    {
        $request    = $this->getRequest();
        $paramsGet  = $request->getQuery()->toArray();
        $paramsPost = $request->getPost()->toArray();
        $param      = array_merge($paramsGet, $paramsPost);

        if ($param['alunocursoId']) {
            $serviceAcadgeralAlunoCurso = new \Matricula\Service\AcadgeralAlunoCurso($this->getEntityManager());
            $serviceAcadgeralAluno      = new \Matricula\Service\AcadgeralAluno($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $alunocursoId  = $param['alunocursoId'];
            $alunoId       = $param['alunoId'] ? $param['alunoId'] : '';
            $pesId         = $param['pesId'] ? $param['pesId'] : '';

            $verificarDependencias = $param['verificarDependencias'] ? $param['verificarDependencias'] : false;
            $verificarFinanceiro   = $param['verificarFinanceiro'] ? $param['verificarFinanceiro'] : false;
            $verificarDocumentos   = $param['verificarDocumentos'] ? $param['verificarDocumentos'] : false;
            $retornarIntegracoes   = $param['retornarIntegracoes'] ? $param['retornarIntegracoes'] : false;
            $pendenciasConclusao   = $param['pendenciasConclusao'] ? $param['pendenciasConclusao'] : false;

            $arrAlunoCurso = $serviceAcadgeralAlunoCurso->getAlunoCursoArray(
                $alunocursoId,
                $verificarDependencias,
                $verificarFinanceiro,
                $verificarDocumentos,
                $retornarIntegracoes,
                $pendenciasConclusao,
                true,
                true
            );

            if (empty($arrAlunoCurso) && ($alunoId || $pesId)) {
                $arrAlunoCurso = $serviceAcadgeralAluno->getArrayFallback($alunoId, $pesId);

                if (empty($arrAlunoCurso)) {
                    $erro          = true;
                    $erroDescricao = $serviceAcadgeralAluno->getLastError();
                }
            } elseif (empty($arrAlunoCurso)) {
                $erro          = true;
                $erroDescricao = $serviceAcadgeralAlunoCurso->getLastError();
            }

            $this->getJson()->setVariable('alunoCurso', $arrAlunoCurso);
            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }

    public function deferirAlunoCursoAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $objAcadgeralAlunoCurso = new \Matricula\Service\AcadgeralAlunoCurso(
                $this->getEntityManager(),
                $this->getServiceManager()->get('Config')
            );
            $erro                   = false;
            $mensagem               = '';

            $paramsGet  = $request->getQuery()->toArray();
            $paramsPost = $request->getPost()->toArray();
            $param      = array_merge($paramsGet, $paramsPost);

            $arrParam = [
                'alunocursoId'          => $param['alunocursoId'],
                'alunoConfigPagtoCurso' => $param['alunoConfigPagtoCurso'],
                'loginSupervisor'       => $param['loginSupervisor'],
                'senhaSupervisor'       => $param['senhaSupervisor'],
                'justificativa'         => $param['justificativa']
            ];

            $deferimentoDeCursoDeAluno = $objAcadgeralAlunoCurso->deferirCursoAluno($arrParam);
            $mensagem                  = $objAcadgeralAlunoCurso->getLastError();

            if (!$deferimentoDeCursoDeAluno) {
                $erro = true;
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('mensagem', $mensagem);
        }

        return $this->getJson();
    }

    public function indexAction()
    {
        return $this->matriculaAction();
    }

    public function alterarSituacaoAction()
    {
        $serviceAcadgeralAlunoCurso = new \Matricula\Service\AcadgeralAlunoCurso(
            $this->getEntityManager(),
            $this->getServiceManager()->get('Config')
        );

        $request  = $this->getRequest();
        $erro     = false;
        $mensagem = 'Situação alterada com sucesso!';

        if ($request->isPost()) {
            $dados = $request->getPost()->toArray();

            $result = $serviceAcadgeralAlunoCurso->alteraSituacaoCriandoProtocolo($dados);

            if (!$result) {
                $erro     = true;
                $mensagem = $serviceAcadgeralAlunoCurso->getLastError();
            }
        } else {
            $erro     = true;
            $mensagem = 'Requisição inválida!';
        }

        $this->getJson()->setVariable('erro', $erro);
        $this->getJson()->setVariable('mensagem', $mensagem);

        return $this->getJson();
    }

    public function alterarDatasAction()
    {
        $serviceAcadgeralAlunoCurso = new \Matricula\Service\AcadgeralAlunoCurso(
            $this->getEntityManager(),
            $this->getServiceManager()->get('Config')
        );

        $request  = $this->getRequest();
        $erro     = false;
        $mensagem = 'Datas de curso de aluno alteradas com sucesso!';

        if ($request->isPost()) {
            $dados = $request->getPost()->toArray();

            $result = $serviceAcadgeralAlunoCurso->alterarDatas($dados);

            if (!$result) {
                $erro     = true;
                $mensagem = $serviceAcadgeralAlunoCurso->getLastError();
            }
        } else {
            $erro     = true;
            $mensagem = 'Requisição inválida!';
        }

        $this->getJson()->setVariable('erro', $erro);
        $this->getJson()->setVariable('mensagem', $mensagem);

        return $this->getJson();
    }

    public function trocarCursoAction()
    {
        $service = new \Matricula\Service\AcadgeralAlunoCurso(
            $this->getEntityManager(),
            $this->getServiceManager()->get('Config')
        );

        $request           = $this->getRequest();
        $result['erro']    = false;
        $result['message'] = "Troca efetuada com sucesso!";

        if ($request->isPost()) {
            $arrDados = $request->getPost()->toArray();
            $troca    = $service->trocarCursoAluno($arrDados);

            if (!$troca) {
                $result['erro']    = true;
                $result['message'] = $service->getLastError();
            } else {
                $result['alunocursoId'] = $troca;
            }
        }

        $this->getJson()->setVariable('result', $result);

        return $this->getJson();
    }

    public function outrasInformacoesAction()
    {
        $request = $this->getRequest();
        $service = new \Matricula\Service\AcadgeralAlunoCurso(
            $this->getEntityManager(),
            $this->getServiceManager()->get('Config')
        );

        $result['erro']     = false;
        $result['mensagem'] = "Dadados salvos com sucesso!";

        if ($request->isPost()) {
            $arrDados = $request->getPost()->toArray();
            $salvar   = $service->outrasInformacoes($arrDados);

            if (!$salvar) {
                $result['erro']     = true;
                $result['mensagem'] = $service->getLastError();
            }
        } else {
            $result['erro']     = true;
            $result['mensagem'] = 'Para alterar informações de curso de aluno é necessário!';
        }

        $this->getJson()->setVariables($result);

        return $this->getJson();
    }

    public function matriculaAction()
    {
        $serviceSisConfig             = new \Sistema\Service\SisConfig(
            $this->getEntityManager(),
            $this->getServiceManager()->get('config')
        );
        $serviceNecessidadesEspeciais = new \Pessoa\Service\NecessidadesEspeciais($this->getEntityManager());
        $serviceVestibular            = new \Vestibular\Service\SelecaoInscricao($this->getEntityManager());
        $serviceAluno                 = new \Matricula\Service\AcadgeralAluno($this->getEntityManager());
        $serviceAlunoFromacao         = new \Matricula\Service\AcadgeralAlunoFormacao($this->getEntityManager());
        $serviceAlunoCurso            = new \Matricula\Service\AcadgeralAlunoCurso($this->getEntityManager());
        $serviceSelecaoTipo           = new \Vestibular\Service\SelecaoTipo($this->getEntityManager());
        $serviceEstados               = new \Pessoa\Service\Estado($this->getEntityManager());

        $formaIngressoPadraoId = $serviceSisConfig->localizarChave('ALUNO_FORMA_INGRESSO_PADRAO');

        $formaIngressoPadrao = $serviceSelecaoTipo->getArrSelect2(array('id' => $formaIngressoPadraoId));
        $arrEtnia            = $serviceAluno->getArrSelect2AlunoEtnia();
        $arrEstados          = $serviceEstados->getArrSelect2Estados();

        $request = $this->getRequest();

        if ($request->isPost()) {
            $arrDados = $request->getPost()->toArray();

            $this->getJson()->setVariable('error', false);

            $situacao = $serviceVestibular->verificaSeAprovado($arrDados['pesId']);

            if (!$situacao || $situacao == "aprovado") {
                $salvar = $serviceAluno->registroSimplificadoDeAluno($arrDados, true);

                if ($salvar) {
                    $serviceAlunoCurso
                        ->deferirCursoDeAluno($arrDados['alunoId'], $arrDados['cursocampusId']);
                    $this->getJson()->setVariable('result', 'Cadastro registrado com sucesso!');
                    $this->flashMessenger()->addSuccessMessage('Aluno cadastrado com sucesso!');

                    return $this->getJson();
                } else {
                    $this->getJson()->setVariable('result', $serviceAluno->getLastError());
                }
            } elseif ($serviceVestibular->verificaSeAprovado($arrDados['pesId']) == "Reprovado") {
                $this->getJson()->setVariable('result', 'Aluno foi reprovado no processo seletivo');
            }
            $this->getJson()->setVariable('error', true);

            return $this->getJson();
        }

        $serviceAlunoFromacao->setarDependenciasView($this->getView());
        $this->getView()->setVariable('arrEstados', $arrEstados);
        $this->getView()->setVariable('arrEtnia', $arrEtnia);
        $this->getView()->setVariable('arrNecessidadesEspeciais', $serviceNecessidadesEspeciais->getArrSelect2());
        $this->getView()->setVariable('formaIngressoPadrao', $formaIngressoPadrao);

        return $this->getView();
    }
}