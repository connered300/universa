<?php

namespace Matricula\Controller;

use VersaSpine\Controller\AbstractCoreController;

class AcadNivelController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request          = $this->getRequest();
        $paramsGet        = $request->getQuery()->toArray();
        $paramsPost       = $request->getPost()->toArray();
        $serviceAcadNivel = new \Matricula\Service\AcadNivel($this->getEntityManager());

        $result = $serviceAcadNivel->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAcadNivel = new \Matricula\Service\AcadNivel($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceAcadNivel->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }
}