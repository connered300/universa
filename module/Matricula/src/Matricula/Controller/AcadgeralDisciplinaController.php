<?php

namespace Matricula\Controller;

use VersaSpine\Controller\AbstractCoreController;

class AcadgeralDisciplinaController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function indexAction()
    {
        $this->getView()->setVariable('urlAcadgeralDisciplinaAdd', $this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        $viewJson = $this->getJson();

        if ($request->isPost() && !$request->getPost('discId')) {
            $serviceAcadgeralDisciplina = new \Matricula\Service\AcadgeralDisciplina($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceAcadgeralDisciplina->getDataForDatatables($dataPost);
            $viewJson->setVariables([
                "draw"            => $result["draw"],
                "recordsTotal"    => $result["recordsTotal"],
                "recordsFiltered" => $result["recordsFiltered"],
                "data"            => $result["data"],
            ]);
        }

        return $viewJson;
    }

    public function addAction($disc = false)
    {
        //$service  = $this->services()->getService($this->getService());
        $service = new \Matricula\Service\AcadgeralDisciplina($this->getEntityManager());
        $request  = $this->getRequest();
        $arrDados = array();

        if ($disc && !($ajax = $request->isXmlHttpRequest() ) ) {
            $arrDados = $service->getArray($disc);
            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
            }
        }

        if ($request->isPost()) {
            $arrDados = $request->getPost()->toArray();
            
            if( $arrDados['curso'] ){
                $arrCursoJson = $arrDados['curso'];
                unset($arrDados['curso']);

                foreach($arrCursoJson as $cadaCursoJson){
                    $arrDecodeJsonCurso = (array) json_decode($cadaCursoJson);
                    $arrDados['curso'][(int)$arrDecodeJsonCurso['curso_id']] = $arrDecodeJsonCurso;
                }
            }
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $service->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage('Registro de disciplina salvo!');

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }
            } else {
                $this->getView()->setVariable("erro", array($service->getLastError()));

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($service->getLastError());
                }
            }
        }
        
        $serviceDiscTipo  = new \Matricula\Service\AcadgeralDisciplinaTipo($this->getEntityManager());
        $serviceDiscCurso  = new \Matricula\Service\AcadgeralDisciplinaCurso($this->getEntityManager());
        $serviceAcadCurso = new \Matricula\Service\AcadCurso($this->getEntityManager());
        
        $arrCursos    = $serviceAcadCurso->mountArrayCurso();
        $arrTdiscId   = $serviceDiscTipo->getArrSelect2();
        $disciplinas  = $service->getRepository($this->getEntity())->findBy([], ['discNome' => 'ASC']);
        $arrDiscAtivo = $serviceDiscCurso->getArrSelect2DiscAtivo();
        
        $arrDefaultValues = [
            'ativo'    => $serviceDiscCurso::DISC_ATIVO_SIM,
            'tipoDisc' => $serviceDiscTipo::DISCIPLINA_TIPO_REGULAR
        ];

        $this->getView()->setVariable('arrDados', $arrDados);
        $this->getView()->setVariable('arrCursos', $arrCursos);
        $this->getView()->setVariable('arrTdiscId', $arrTdiscId);
        $this->getView()->setVariable('disciplinas', $disciplinas);
        $this->getView()->setVariable('arrDiscAtivo', $arrDiscAtivo);
        $this->getView()->setVariable('arrDefaultValues', $arrDefaultValues);

        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $disc = $this->params()->fromRoute("id", 0);

        if (!$disc) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($disc);
    }
    
    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAcadgeralDisciplina = new \Matricula\Service\AcadgeralDisciplina($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceAcadgeralDisciplina->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceAcadgeralDisciplina->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);

            return $this->getJson();
        }

        return $this->getJson();
    }
}