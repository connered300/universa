<?php

namespace Matricula\Controller;

use VersaSpine\Controller\AbstractCoreController;

class CampusCursoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request            = $this->getRequest();
        $paramsGet          = $request->getQuery()->toArray();
        $paramsPost         = $request->getPost()->toArray();
        $serviceCampusCurso = new \Matricula\Service\CampusCurso($this->getEntityManager());

        if ($paramsPost['agente']) {
            $arrConfig           = $this->getServiceManager()->get('Config');
            $serviceSis          = new \Sistema\Service\SisConfig($this->getEntityManager(), $arrConfig);
            $paramsPost['nivel'] = $serviceSis->localizarChave('ACADEMICO_CAPTACAO_AGENTE_NIVEIS');
        }

        $result = $serviceCampusCurso->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceCampusCurso = new \Matricula\Service\CampusCurso($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceCampusCurso->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function addAction($cursocampusId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados           = array();
        $serviceCampusCurso = new \Matricula\Service\CampusCurso($this->getEntityManager());

        if ($cursocampusId) {
            $arrDados = $serviceCampusCurso->getArray($cursocampusId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceCampusCurso->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de câmpus curso salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceCampusCurso->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceCampusCurso->getLastError());
                }
            }
        }

        $serviceCampusCurso->formataDadosPost($arrDados);
        $serviceCampusCurso->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $cursocampusId = $this->params()->fromRoute("id", 0);

        if (!$cursocampusId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($cursocampusId);
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceCampusCurso = new \Matricula\Service\CampusCurso($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceCampusCurso->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceCampusCurso->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}
?>