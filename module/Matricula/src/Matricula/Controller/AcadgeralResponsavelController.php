<?php


namespace Matricula\Controller;


use VersaSpine\Controller\AbstractCoreController;

    class AcadgeralResponsavelController extends AbstractCoreController{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {

    }

    public function buscaDadosResponveisAction()
    {
        $request = $this->getRequest();

        if($request->isPost()){
            $data = $request->getPost()->toArray();
            if(sizeof($data) > 0){
                $service = $this->services()->getService( $this->getService() );
                $responsaveis = "";
                for($i = 0; $i < sizeof($data); $i++){
                    $responsaveis[$i] = $service->buscaDadosResponsavel($data['responsaveis'][$i]);
                }
                if($responsaveis){
                    $this->json->setVariable("responsaveis", $responsaveis );
                } else {
                    $this->json->setVariable("responsaveis", 0 );
                }
            }
        }

        return $this->json;
    }
}