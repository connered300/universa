<?php
namespace Matricula\Controller;

use VersaSpine\Controller\AbstractCoreController;

class AcadgeralAlunoFormacaoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {
        $request   = $this->getRequest();
        $arrayData = array_merge($request->getPost()->toArray(), $request->getQuery()->toArray());

        $serviceAcadgeralAlunoFormacao = new \Matricula\Service\AcadgeralAlunoFormacao($this->getEntityManager());

        if (isset($arrayData["curso"])) {
            $this->getJson()->setVariable(
                "dados",
                $serviceAcadgeralAlunoFormacao->buscaCurso($arrayData["curso"])
            );
        } elseif (isset($arrayData["instituicao"])) {
            $this->getJson()->setVariable(
                "dados",
                $serviceAcadgeralAlunoFormacao->buscaInstituicao($arrayData["instituicao"])
            );
        } else {
            $this->getJson()->setVariables(
                $serviceAcadgeralAlunoFormacao->pesquisaForJson($arrayData)
            );
        }

        return $this->getJson();
    }
}