<?php

namespace Matricula\Controller;

class AcadgeralAlunoCaptacaoController extends AcadgeralAlunoController
{
    public function captacaoAction()
    {
        return parent::captacaoAction(true);
    }

    public function indexAction()
    {
        return parent::indexAction(true);
    }
}