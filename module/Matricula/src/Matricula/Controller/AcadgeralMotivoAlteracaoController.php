<?php

namespace Matricula\Controller;

use VersaSpine\Controller\AbstractCoreController;

class AcadgeralMotivoAlteracaoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request                         = $this->getRequest();
        $paramsGet                       = $request->getQuery()->toArray();
        $paramsPost                      = $request->getPost()->toArray();
        $serviceAcadgeralMotivoAlteracao = new \Matricula\Service\AcadgeralMotivoAlteracao($this->getEntityManager());

        $result = $serviceAcadgeralMotivoAlteracao->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceAcadgeralMotivoAlteracao = new \Matricula\Service\AcadgeralMotivoAlteracao($this->getEntityManager());
        $serviceAcadgeralMotivoAlteracao->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAcadgeralMotivoAlteracao = new \Matricula\Service\AcadgeralMotivoAlteracao(
                $this->getEntityManager()
            );

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceAcadgeralMotivoAlteracao->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function addAction($motivoId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados                        = array();
        $serviceAcadgeralMotivoAlteracao = new \Matricula\Service\AcadgeralMotivoAlteracao($this->getEntityManager());

        if ($motivoId) {
            $arrDados = $serviceAcadgeralMotivoAlteracao->getArray($motivoId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", true);
                $this->getView()->setVariable("mensagem", $serviceAcadgeralMotivoAlteracao->getLastError());

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceAcadgeralMotivoAlteracao->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de motivo de alteração salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceAcadgeralMotivoAlteracao->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceAcadgeralMotivoAlteracao->getLastError());
                }
            }
        }

            $serviceAcadgeralMotivoAlteracao->formataDadosPost($arrDados);
            $serviceAcadgeralMotivoAlteracao->setarDependenciasView($this->getView());

            $this->getView()->setVariable("arrDados", $arrDados);
            $this->getView()->setTemplate($this->getTemplateToRoute('add'));


            return $this->getView();
    }

    public function editAction()
    {
        $motivoId = $this->params()->fromRoute("id", 0);

        if (!$motivoId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($motivoId);
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAcadgeralMotivoAlteracao = new \Matricula\Service\AcadgeralMotivoAlteracao(
                $this->getEntityManager()
            );

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceAcadgeralMotivoAlteracao->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceAcadgeralMotivoAlteracao->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}
?>