<?php


namespace Matricula\Controller;


use Matricula\Service\AcadperiodoIntegradoraAvaliacao;
use VersaSpine\Controller\AbstractCoreController;
use Zend\Json\Json;

class AcadperiodoIntegradoraAvaliacaoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {

    }

    public function indexAction()
    {
        $serviceIntegradoraAluno = new \Matricula\Service\AcadperiodoIntegradoraAluno($this->getEntityManager());
        $serviceIntegradora      = new \Matricula\Service\AcadperiodoIntegradora($this->getEntityManager());
        $serviceIntegradoraAvaliacao      = new \Matricula\Service\AcadperiodoIntegradoraAvaliacao($this->getEntityManager());
        $serviceIntegradoraConf  = new \Matricula\Service\AcadperiodoIntegradoraConf($this->getEntityManager());

        $dados                   = $serviceIntegradoraAvaliacao->pagination();

        $integradoraConf = $serviceIntegradoraConf->buscaConfiguracaoAtual();

        if ($integradoraConf) {
            for ($i = 1; $i <= $integradoraConf->getIntegconfSerieMax(); $i++) {
                $periodos[$i] = $i;
            }
        }

        $this->config['table']['dados']      = $dados['dados'];
        //$dadosAvaliacoes = $this->services()->getService($this->getService())->pagination()['dados']->fetchAll();
//        foreach ($dadosAvaliacoes as $avaliacao) {
//            if($avaliacao['integradora_status'] == 'Aberta'){
//                $avaliacoes[$avaliacao['integavaliacao_id']] = $avaliacao['integavaliacao_periodo'].'°'.' - '.$avaliacao['integavaliacao_turno'];
//            }
//        }

        $this->config['pagination']['count'] = $dados['count'];
        $this->config['table']['table_key'] = $this->services()->getService($this->getService())->getTablePkey();
        /**
         * Cria as variaveis para a view
         */
        //$this->view->setVariable("avaliacoes", $avaliacoes);
        $this->view->setVariable('periodos', $periodos);
        $this->view->setVariable("dados", $dados['dados']);
        $this->view->setVariable("count", $dados['count']);
        $this->view->setVariable("config", $this->config);
        $this->view->setTemplate($this->getTemplateToRoute());

        $integradora = $serviceIntegradora->buscaUltimaIntegradoraAtiva();
        $corrigida   = null;

        if ($integradora) {
            $corrigida = $serviceIntegradoraAluno->verificarSeAIntegradoraJaFoiCorrigida(
                $integradora->getIntegradoraId()
            );
        }

        $this->view->setVariable('corrigida', $corrigida);

        return $this->view;
    }

    public function addAction()
    {
        $service = $this->services()->getService($this->getService());
        $form    = $this->forms()->mountForm($this->getForm());
        $servicePeriodoLetivo  = $this->services()->getService('Matricula\Service\AcadperiodoLetivo');
        $serviceIntegradora  = $this->services()->getService('Matricula\Service\AcadperiodoIntegradora');
        $serviceIntegradoraAvaliacao  = $this->services()->getService('Matricula\Service\AcadperiodoIntegradoraAvaliacao');


        $integradora = $serviceIntegradora->buscaUltimaIntegradoraAtiva();
        $this->view->setVariable('quantidaQuestoes', $integradora->getIntegconf()->getIntegconfQuestoes());

        for($i = 1; $i <= $integradora->getIntegconf()->getIntegconfSerieMax(); $i++){
            $periodos[$i] = $i;
        }

        $cursosCampus = $service->getRepository('Matricula\Entity\CampusCurso')->buscaCursos(2);
        $this->view->setVariable('cursosCampus', $cursosCampus[1][2]);

        $periodoLetivo = $integradora->getPer();
        $this->view->setVariable("periodoLetivo", $periodoLetivo->getPerId());


        $form->get('integavaliacaoPeriodo')->setValueOptions($periodos);
        $form->get('integavaliacaoTurno')->setValueOptions(\Matricula\Entity\AcadperiodoIntegradoraAvaliacao::$tunos);
        $form->get('integradora')->setValue($integradora->getIntegradoraId());

        $avaliacoes = $serviceIntegradoraAvaliacao->buscaAvaliacaoPorIntegradoraArray($integradora->getIntegradoraId());
        if($avaliacoes){
            $this->view->setVariable("avaliacoes", json_encode($avaliacoes));
        }


        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $dados = array_merge($request->getPost()->toArray(), $request->getFiles()->toArray());

                $ret   = $service->adicionar($dados);
                $this->flashMessenger()->addSuccessMessage('Avaliação adicionada com sucesso!');
                if (is_null($this->getRoute())) {
                    $this->setRoute($this->getEvent()->getRouteMatch()->getMatchedRouteName());
                }
            } catch (\Exception $ex) {
                $erro = $ex->getMessage();
                $form->setData($request->getPost()->toArray());
                $this->flashMessenger()->addErrorMessage($erro);
            }

             return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        $this->view->setVariable("form", $form);

        return $this->view;
    }

    public function editAction()
    {
        $request = $this->getRequest();
        $form    = $this->forms()->mountForm($this->getForm());
        $service = $this->services()->getService($this->getService());
        $serviceIntegradoraCaderno = $this->services()->getService('Matricula\Service\AcadperiodoIntegradoraCaderno');
        $serviceIntegradoraTurma = $this->services()->getService('Matricula\Service\AcadperiodoIntegradoraTurma');
        $serviceIntegradoraGabarito = $this->services()->getService('Matricula\Service\AcadperiodoIntegradoraGabarito');
        $serviceIntegradora  = $this->services()->getService('Matricula\Service\AcadperiodoIntegradora');

        if ($request->isPost()) {
            try {
                $dados = array_merge($request->getPost()->toArray(), $request->getFiles()->toArray());
                $ret   = $service->edita($dados);
                if (is_null($this->getRoute())) {
                    $this->setRoute($this->getEvent()->getRouteMatch()->getMatchedRouteName());
                }
            } catch (\Exception $ex) {
                $erro = $ex->getMessage();
                $form->setData($request->getPost()->toArray());
            }

            $is_json = $request->getPost("is_json");
            if (isset($erro)) {
                if ($is_json) {
                    $this->json->setVariable("erro", $erro);

                    return $this->json;
                }
                $this->flashMessenger()->addErrorMessage($erro);
            } else {
                if ($is_json) {
                    if (method_exists($ret, "toArray")) {
                        $dados = $ret->toArray();
                    } else {
                        $dados = "Os dados foram alterados corretamente, mas um erro ocorreu, verifique se existe o método toArray na entidade {$this->getEntity()}!";
                    }
                    $this->json->setVariable("result", $dados);

                    return $this->json;
                }
            }

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        $id = $this->params()->fromRoute("id", 0);
        if (!$id) {
            throw new \Exception(
                "Para editar um registro é necessário informar o identificador do mesmo pela rota!", 1
            );
        }

        $reference = $service->getReference($id);

        $cursosCampus = $service->getRepository('Matricula\Entity\CampusCurso')->buscaCursos(2);
        $this->view->setVariable('cursosCampus', $cursosCampus[1][2]);

        $this->view->setVariable('quantidaQuestoes', $reference->getIntegradora()->getIntegconf()->getIntegconfQuestoes());

        $cadernos = $serviceIntegradoraCaderno->buscaCadernosPorAvaliacao($reference->getIntegavaliacaoId());
        $this->view->setVariable("cadernos", $cadernos);

        $turmasResult = $service->getRepository('Matricula\Entity\AcadperiodoTurma')->buscaTurmasPorTurno(
            $cursosCampus[1][2],
            $reference->getIntegavaliacaoPeriodo(),
            $reference->getIntegradora()->getPer()->getPerId(),
            $reference->getIntegavaliacaoTurno()
        );
        $turmasJson = array();
        foreach ($turmasResult as $turmas) {
            $turmasJson[]  = array('id' => $turmas['turmaId'],'text' => $turmas['turmaNome']);
        }
        $this->view->setVariable("turmas", json_encode($turmasJson));

        $turmasSelecionadas = $serviceIntegradoraTurma->buscaTurmasAvaliacao($reference);

        $this->view->setVariable("turmasSelecionadas", json_encode($turmasSelecionadas));

        $matriz = $serviceIntegradoraTurma->buscaMAtrizTurmaPorAvaliacao($reference);

        $disciplinasResult = $service->getRepository('Matricula\Entity\AcadperiodoMatrizDisciplina')
            ->buscaDisciplinasPorMatrizCurricular($matriz, $reference->getIntegavaliacaoPeriodo());

        foreach ($disciplinasResult as $disciplinas) {
            $disciplinasJson[]  = array('id' => $disciplinas['disc'],'text' => $disciplinas['discNome']);
        }
        $this->view->setVariable("disciplinas", json_encode($disciplinasJson));

        $cadernosComponente = array();
        foreach ($cadernos as $caderno) {
            $cadernosComponente[] = array(
                'nome' => $caderno->getIntegcadernoNome(),
                'data-aplicacao' => $caderno->getIntegcadernoData()->format('d/m/Y'),
                'disciplinas' => $serviceIntegradoraCaderno->buscasDisciplinasCaderno($caderno),
                'gabarito' => $serviceIntegradoraGabarito->buscaGabaritoPorCaderno($caderno)
            );
        }
        $this->view->setVariable("cadernosSelecionadas", json_encode($cadernosComponente));

        for($i = 1; $i <= $reference->getIntegradora()->getIntegconf()->getIntegconfSerieMax(); $i++){
            $periodos[$i] = $i;
        }

        $periodoLetivo = $reference->getIntegradora()->getPer()->getPerId();
        $this->view->setVariable("periodoLetivo", $periodoLetivo);

        $form->get('integavaliacaoPeriodo')->setValueOptions($periodos);

        $form->get('integavaliacaoTurno')->setValueOptions(\Matricula\Entity\AcadperiodoIntegradoraAvaliacao::$tunos);

        $avaliacaoArray = $reference->toArray();
        $avaliacaoArray['integavaliacaoPeriodo'] = $reference->getIntegavaliacaoPeriodo();
        $avaliacaoArray['integavaliacaoTurno'] = $reference->getIntegavaliacaoTurno();
        $form->setData($avaliacaoArray);


        $this->view->setVariable("form", $form);
        $this->view->setTemplate($this->getTemplateToRoute('add'));
        $this->view->setVariable("integavaliacaoId", $id);

        return $this->view;
    }
}