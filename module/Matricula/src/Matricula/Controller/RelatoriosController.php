<?php
namespace Matricula\Controller;

use DOMPDFModule\View\Model\PdfModel;
use Matricula\Service\AcadgeralAlunoCurso;
use VersaSpine\Controller\AbstractCoreController;
use Zend\View\Model\JsonModel;

class RelatoriosController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function indexAction()
    {
        $arrConfig            = $this->getServiceManager()->get('Config');
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEntityManager(), $arrConfig);

        $arrRelatorios = array(
            array(
                'descricao'  => 'Relatórios de Alunos por Turma',
                'route'      => 'matricula/default',
                'controller' => 'relatorios',
                'action'     => 'relatorio-alunos',
                'namespace'  => 'Matricula\Controller\Relatorios'
            ),
            array(
                'descricao'  => 'Relatórios Geral de Alunos Matriculados',
                'route'      => 'matricula/default',
                'controller' => 'relatorios',
                'action'     => 'matriculados-por-periodo',
                'namespace'  => 'Matricula\Controller\Relatorios'
            ),
            array(
                'descricao'  => 'Relatório de Alunos por Turma e Disciplina',
                'route'      => 'matricula/default',
                'controller' => 'relatorios',
                'action'     => 'alunos-turma-disciplina',
                'namespace'  => 'Matricula\Controller\Relatorios'
            ),
            array(
                'descricao'  => 'Relatório de Dependências',
                'route'      => 'matricula/default',
                'controller' => 'relatorios',
                'action'     => 'pendencias-reprovacoes',
                'namespace'  => 'Matricula\Controller\Relatorios'
            ),
            array(
                'descricao'  => 'Emissão de Carteira do Estudante',
                'route'      => 'matricula/default',
                'controller' => 'acadgeral-aluno',
                'action'     => 'carteirinha',
                'namespace'  => 'Matricula\Controller\AcadgeralAluno'
            ),
            array(
                'descricao'  => 'Emissão de Boletim do Aluno',
                'route'      => 'matricula/default',
                'controller' => 'relatorios',
                'action'     => 'boletin-aluno-periodo',
                'namespace'  => 'Matricula\Controller\Relatorios'
            ),
            array(
                'descricao'  => 'Emissão de Histórico do Aluno',
                'route'      => 'matricula/default',
                'controller' => 'relatorios',
                'action'     => 'historico-aluno',
                'namespace'  => 'Matricula\Controller\Relatorios'
            ),
            array(
                'descricao'  => 'Relatório de Cursos',
                'route'      => 'matricula/default',
                'controller' => 'relatorios',
                'action'     => 'listagem-cursos',
                'namespace'  => 'Matricula\Controller\Relatorios'
            ),
            array(
                'descricao'  => 'Relatório de Alunos X Agente Educacional',
                'route'      => 'matricula/default',
                'controller' => 'relatorios',
                'action'     => 'aluno-por-agente',
                'namespace'  => 'Matricula\Controller\Relatorios'
            ),
            array(
                'descricao'  => 'Relatório de Operadores X Alunos',
                'route'      => 'matricula/default',
                'controller' => 'relatorios',
                'action'     => 'aluno-por-operador',
                'namespace'  => 'Matricula\Controller\Relatorios'
            ),
            array(
                'descricao'  => 'Geração do arquivo de integração da catraca',
                'route'      => 'matricula/default',
                'controller' => 'relatorios',
                'action'     => 'gera-arquivo-catraca',
                'namespace'  => 'Matricula\Controller\Relatorios'
            ),
            array(
                'descricao'  => 'Emissão de certificados',
                'route'      => 'matricula/default',
                'controller' => 'relatorios',
                'action'     => 'emissao-certificados',
                'namespace'  => 'Matricula\Controller\Relatorios'
            ),
            array(
                'descricao'  => 'Relatório de Contatos de Alunos',
                'route'      => 'matricula/default',
                'controller' => 'relatorios',
                'action'     => 'relatorio-aluno-contato',
                'namespace'  => 'Matricula\Controller\Relatorios'
            ),
        );

        $arrItensPermitidos = [];

        foreach ($arrRelatorios as $arrRelatorio) {
            $permissao = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
                $arrRelatorio['namespace'],
                $arrRelatorio['action']
            );

            if ($permissao) {
                $arrItensPermitidos[] = $arrRelatorio;
            }
        }

        $this->getView()->setVariable('arrRelatorios', $arrItensPermitidos);

        //$this->getView()->setVariable('arrRelatorios', $arrRelatorios);

        return $this->getView();
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {
    }

    public function geraArquivoCatracaAction()
    {
        $serviceAlunoCurso   = new \Matricula\Service\AcadgeralAlunoCurso($this->getEntityManager());
        $serviceAlunoPeriodo = new \Matricula\Service\AcadperiodoAluno($this->getEntityManager());
        $serviceCampusCurso  = new \Matricula\Service\CampusCurso($this->getEntityManager());
        $servicePeriodo      = new \Matricula\Service\AcadperiodoLetivo($this->getEntityManager());

        $request = $this->getRequest();

        if ($request->isPost()) {
            $dados = $request->getPost()->toArray();

            $result = $serviceAlunoCurso->geraArquivoCarteira($dados);

            if (!$result) {
                $this->getJson()->setVariable('error', true);
                $this->getJson()->setVariable('message', $serviceAlunoCurso->getLastError());
            } else {
                $this->getJson()->setVariable('error', false);
                $this->getJson()->setVariable('result', $result);
            }

            return $this->getJson();
        }

        $arrSituacao = $serviceAlunoPeriodo->getArrSelect2AlunoperiodoSituacao();
        $arrCampus   = $serviceCampusCurso->getArrSelect2();
        $arrPer      = $servicePeriodo->getArrSelect2();

        $this->view->setVariables(
            [
                'arrAlunoSituacao' => $arrSituacao,
                'arrCampus'        => $arrCampus,
                'arrPer'           => $arrPer,
            ]
        );

        return $this->view;
    }

    public function relatorioAlunosAction()
    {
        $serviceCampus        = new \Organizacao\Service\OrgCampus($this->getEntityManager());
        $serviceCampusCurso   = new \Matricula\Service\CampusCurso($this->getEntityManager());
        $serviceTipoIng       = new \Vestibular\Service\SelecaoTipo($this->getEntityManager());
        $serviceTurma         = new \Matricula\Service\AcadperiodoTurma($this->getEntityManager());
        $servicePeriodoLetivo = new \Matricula\Service\AcadperiodoLetivo($this->getEntityManager());
        $serviceSituacao      = new \Matricula\Service\AcadgeralSituacao($this->getEntityManager());
        $serviceturma         = new \Matricula\Service\AcadperiodoTurma($this->getEntityManager());

        $per           = $servicePeriodoLetivo->getRepository()->findAll();
        $formaIngresso = $serviceTipoIng->pesquisaForJson();
        $campusCurso   = $serviceCampusCurso->buscaCursoCampus();

        /** @var \Zend\Http\Request $request */
        $request = $this->getRequest();

        if ($request->isPost()) {
            $dados = $request->getPost()->toArray();

            if ($dados['tipo'] == 'matricula/relatorios/lista-alunos') {
                $serviceCampus      = new \Organizacao\Service\OrgCampus($this->getEntityManager());
                $serviceCampusCurso = new \Matricula\Service\CampusCurso($this->getEntityManager());

                /** @var \Matricula\Entity\CampusCurso $objCampusCurso */
                $objCampusCurso = $serviceCampusCurso->getRepository()->find($dados['campus_curso']);

                /** @var \SnowReportsZf2\Service\SnowReportsZf2 $snow */
                $snow = $this->getServiceLocator()->get('SnowReportsZf2\Service\SnowReportsZf2');

                $dadosinstituicao = $serviceCampus->retornaDadosInstituicao($objCampusCurso->getCamp()->getCampId());

                $nomeRelatorio = 'listagem_alunos_' . (new \DateTime('now'))->format('YmdHi');

                $dados['campusCurso'] = $objCampusCurso->getCurso()->getCursoNome();

                /** @var \Matricula\Entity\AcadgeralSituacao $objSituacao */
                $objSituacao = $serviceSituacao->getRepository()->findOneBy(
                    array('matsitDescricao' => $dados['filtro'])
                );
                $situacao    = $objSituacao->getSituacaoId();

                $where = " where 1 ";

                if ($dados['turmas'] AND $dados['relatorio'] != 'todos') {
                    $where .= " AND turma_id = {$dados['turmas']} ";

                    /** @var $objTurma \Matricula\Entity\AcadperiodoTurma */
                    $objTurma = $serviceturma->getRepository()->find($dados['turmas']);
                }

                if ($situacao) {
                    $where .= " AND matsituacao_id = {$situacao} ";
                }
                if (!$dados['turmasIrregulares']) {
                    $where .= " AND acadgeral__turma_tipo.tturma_descricao = 'Convencional' ";
                }
                if ($dados['periodoLetivo']) {
                    $where .= " AND acadperiodo__turma.per_id = {$dados['periodoLetivo']} ";

                    /** @var \Matricula\Entity\AcadperiodoLetivo $objPeriodoLetivo */
                    $objPeriodoLetivo = $servicePeriodoLetivo->getRepository()->find($dados['periodoLetivo']);
                }
                if ($dados['periodoTurno'] && $dados['periodoTurno'] != "Todos") {
                    $turnoPeriodo = '"%' . $dados["periodoTurno"] . '%"';

                    $where .= " AND acadperiodo__turma.turma_turno LIKE {$turnoPeriodo} ";
                }
                if ($dados['periodoSerie'] && $dados['periodoSerie'] != 'Todos') {
                    $where .= " AND acadperiodo__turma.turma_serie = {$dados['periodoSerie']}";
                }
                if ($dados['forma-ingresso']) {
                    if (is_integer($dados['forma-ingresso'] * 1)) {
                        $where .= " AND tiposel_id ={$dados['forma-ingresso']}";
                    } else {
                        $dados['forma-ingresso'] = '"%' . $dados['forma-ingresso'] . '%"';
                        $where .= " AND tiposel_id like '%{$dados['forma-ingresso']}%'";
                    }
                }

                if ($dados['ingressantes'] == "true") {
                    $where .= " AND DATE(acadgeral__aluno.aluno_data_cadastro) >= DATE(acadperiodo__letivo.per_matricula_inicio) ";
                }
                if ($dados['campus_curso']) {
                    $where .= " AND acadperiodo__turma.cursocampus_id = {$dados['campus_curso']} ";
                }

                $where .= "
                    GROUP BY alunocurso_id
                    ORDER by pes_nome,turma_serie,turma_nome ";

                $subTitulo =
                    $dados['filtro'] == 'matriculado' ? "MATRICULADOS" :
                        ($dados['filtro'] == 'cancelada' ? "CANCELADOS" : strtolower($dados['filtro']));

                $snow->setJrxmlPath(__DIR__ . '/../Reports/')
                    ->setJrxmlFile('listagem-alunos-turmas.jrxml')
                    ->setAttrbutes(
                        [
                            'logo'            => ($dadosinstituicao['logo']) ? $dadosinstituicao['logo'] : '-',
                            'mantenedora'     => ($dadosinstituicao['mantenedora']) ? $dadosinstituicao['mantenedora'] : '-',
                            'instituicaoinfo' => '',
                            'instituicao'     => ($dadosinstituicao['ies']) ? $dadosinstituicao['ies'] : '-',
                            'endereco'        => ($dadosinstituicao['endereco']) ? $dadosinstituicao['endereco'] : '-',
                            'sistema'         => 'Universa',
                            'titulo'          => 'RELATÓRIO DOS ALUNOS' . ($subTitulo ? ' ' . $subTitulo : ''),
                            'cursonome'       => $dados['campusCurso'] ? $dados['campusCurso'] : '-',
                            'formaingresso'   => ($dados['forma-ingresso']) ? ($dados['forma-ingresso'] * 1) : 'Todos',
                            'apenasingressos' => ($dados['ingressantes'] != "false" && $dados['ingressantes']) ? "Sim" : 'Não',
                            'periodoletivo'   => ($dados['periodoSerie']) ? $dados['periodoSerie'] : 'Todos',
                            'turno'           => ($dados['periodoTurno']) ? $dados['periodoTurno'] : 'Todos',
                            'semestre'        => $objPeriodoLetivo ? $objPeriodoLetivo->getPerNome() : 'Todos',
                            'turma'           => $objTurma ? $objTurma->getTurmaNome() : 'Todos',
                            'situacao'        => $dados['filtro'],
                            'where'           => $where,
                        ]
                    )->setOutputFile((string)$nomeRelatorio)
                    ->downloadHtmlPdf(true)
                    ->execute();

                return $this->getView();
            } else {
                /** @var \Matricula\Entity\AcadgeralSituacao $objSituacao */
                $objSituacao = $serviceSituacao->getRepository()->findOneBy(
                    array('matsitDescricao' => $dados['filtro'])
                );
                $situacao    = $objSituacao->getSituacaoId();

                if ($dados['relatorio'] === 'todos') {
                    $dados['turmas'] = null;
                } else {
                    $dados['periodoTurno'] = null;
                }

                if ($dados['ingressantes'] === 'true') {
                    $dados['ingressantes'] = true;
                } else {
                    $dados['ingressantes'] = false;
                }

                if ($dados['turmasIrregulares'] === 'true' && $dados['relatorio'] === 'todos') {
                    $dados['turmasIrregulares'] = true;
                } else {
                    $dados['turmasIrregulares'] = false;
                }

                $alunosTurma = $serviceTurma->buscaAlunosTurma(
                    $dados['turmas'],
                    $situacao,
                    $dados['periodoLetivo'],
                    $dados['periodoTurno'],
                    $dados['periodoSerie'],
                    $dados['forma-ingresso'],
                    $dados['ingressantes'],
                    $dados['turmasIrregulares'],
                    $dados['campus_curso']
                );

                /** @var \Matricula\Entity\CampusCurso $curso */

                if ($alunosTurma) {
                    $curso     = $serviceTurma->getRepository('Matricula\Entity\CampusCurso')->find(
                        $alunosTurma[0]['cursocampus_id']
                    );
                    $cursoNome = $curso->getCurso()->getCursoNome();
                    $infoOrg   = $serviceCampus->retornaDadosInstituicao($curso->getCamp());
                } else {
                    $this->flashMessenger()->addWarningMessage(
                        "Dados não encontrados. Verifique se todos os filtros foram passados!"
                    );

                    return $this->redirect()->toRoute(
                        'matricula/default',
                        ['controller' => 'relatorios', 'action' => 'relatorio-alunos']
                    );
                }

                $pdf = new PdfModel();

                $pdf->setOption('paperOrientation', 'portrait'); // Defaults to "portrait"
                /**
                 * O template vai depender do tipo
                 * por isso ao cadastrar um novo tipo no formulário setar o valor como sendo a rota do template
                 * Ex:
                 * array('matricula/relatorios/etiquetas' => 'Etiquetas')
                 * */
                if ($dados['tipo'] === 'matricula/relatorios/cabecalho-gabarito') {
                    $pdf->setOption('paperSize', 'a4');
                }

                $pdf->setTemplate($dados['tipo']);
                $pdf->setVariables($infoOrg);
                $pdf->setVariables(
                    [
                        'alunosTurma'   => $alunosTurma,
                        'curso'         => $cursoNome,
                        'grupo'         => $dados['filtro'],
                        'filtros'       => $dados['turmas'],
                        'gabarito'      => $dados['dia'],
                        'img'           => "/img/gabarito.jpg",
                        'periodoTurno'  => $dados['periodoTurno'],
                        'periodoSerie'  => $dados['periodoSerie'],
                        'formaIngresso' => $dados['forma-ingresso'],
                        'ingressantes'  => $dados['ingressantes']

                    ]
                );

                return $pdf;
            }
        }

        $turmas_nome [0] = 'TODAS TURMAS';
        $form            = new \Matricula\Form\RelatorioAlunos($turmas_nome);

        $this->view->setVariables(
            [
                'form'           => $form,
                'periodos'       => $per,
                'formasIngresso' => $formaIngresso,
                'campi'          => $campusCurso
            ]
        );

        return $this->view;
    }

    public function matrizAction()
    {
        $id                = $id = $this->params()->fromRoute("id", 0);
        $matrizDisciplinas = $this->getEntityManager()->getRepository(
            'Matricula\Entity\AcadperiodoMatrizDisciplina'
        )->findBy(array('matCur' => $id));

        /** @var \Matricula\Entity\AcadperiodoMatrizCurricular $matriz */
        $matriz = $matrizDisciplinas[0]->getMatCur();

        $ServiceOrgCampus = new \Organizacao\Service\OrgCampus($this->getEntityManager());

        $infoOrg = $ServiceOrgCampus->retornaDadosInstituicao();

        $cargaEstagio = 0;
        $cargaPratica = 0;
        $cargaTeorica = 0;

        foreach ($matrizDisciplinas as $per) {
            $periodos[$per->getPerDiscPeriodo()][count($periodos[$per->getPerDiscPeriodo()])] = [
                'disciplina' => $per->getDisc(),
                'estagio'    => is_null($per->getPerDiscChestagio()) ? 0 : $per->getPerDiscChestagio(),
                'teorica'    => is_null($per->getPerDiscChteorica()) ? 0 : $per->getPerDiscChteorica(),
                'creditos'   => is_null($per->getPerDiscCreditos()) ? 0 : $per->getPerDiscCreditos(),
                'pratica'    => is_null($per->getPerDiscChpratica()) ? 0 : $per->getPerDiscChpratica()

            ];

            $cargaEstagio += is_null($per->getPerDiscChestagio()) ? 0 : $per->getPerDiscChestagio();
            $cargaPratica += is_null($per->getPerDiscChpratica()) ? 0 : $per->getPerDiscChpratica();
            $cargaTeorica += is_null($per->getPerDiscChteorica()) ? 0 : $per->getPerDiscChteorica();
        }

        $pdf = new PdfModel();
        $pdf->setOption('paperOrientation', 'portrait'); // Defaults to "portrait"
        $pdf->setTemplate('matricula/relatorios/matriz-pdf');

        $pdf->setVariables($infoOrg);
        $pdf->setVariables(
            [
                'matriz'       => $matriz,
                'periodos'     => $periodos,
                'cargaPratica' => $cargaPratica,
                'cargaEstagio' => $cargaEstagio,
                'cargaTeorica' => $cargaTeorica,
            ]
        );

        return $pdf;
    }

    public function boletinAlunoPeriodoExternoAction()
    {
        return $this->boletinAlunoPeriodoAction(true);
    }

    public function boletinAlunoPeriodoAction($validarUsuario = false)
    {
        $request           = $this->getRequest();
        $semestreLetivos   = $this->getEntityManager()->getRepository('Matricula\Entity\AcadperiodoLetivo')->findAll();
        $serviceAcesso     = new \Acesso\Service\AcessoPessoas($this->getEntityManager());
        $serviceAluno      = new \Matricula\Service\AcadgeralAluno($this->getEntityManager());
        $serviceAlunoCurso = new \Matricula\Service\AcadgeralAlunoCurso($this->getEntityManager());

        $objUsuarioLogado = $serviceAcesso->retornaUsuarioLogado();

        $usrSolicitanteAluno = null;

        if ($objUsuarioLogado) {
            $objPessoa           = $objUsuarioLogado->getPes()->getPes();
            $objAluno            = $serviceAluno->getRepository()->findOneBy(array('pes' => $objPessoa));
            $usrSolicitanteAluno = $serviceAlunoCurso->getRepository()->findOneBy(array('aluno' => $objAluno));
        }

        $ServiceOrgCampus = new \Organizacao\Service\OrgCampus($this->getEntityManager());
        $periodo          = explode('/', $_SERVER['REQUEST_URI'])['4'];

        if ($request->isPost() || $validarUsuario == true) {
            $dados = $request->getPost()->toArray();

            /** @var \Matricula\Entity\AcadgeralAlunoCurso $aluno */

            $dados['PeriodoId'] = $dados['PeriodoId'] > 0 ? $dados['PeriodoId'] : $periodo; //PERIODO ESTA SENDO PASSADO FIXO ATÉ SEJA IMPLENTADO A INTERFACE DE DOCUMENTOS DO ALUNO

            $serviceAlunoPeriodo = $this->services()->getService('Matricula\Service\AcadperiodoAluno');
            $serviceEtapaAluno   = $this->services()->getService('Professor\Service\AcadperiodoEtapaAluno');
            $matrizDisciplina    = $this->services()->getService('Matricula\Service\AcadperiodoMatrizDisciplina');

            $alunoperId = $serviceAlunoPeriodo->BuscaMatriculaAlunoPeriodo(
                $dados['PeriodoId'],
                $dados['matricula'] > 0 ? $dados['matricula'] : $usrSolicitanteAluno->getAlunocursoId()
            );

            if ($alunoperId != false) {
                $reference = $serviceAlunoPeriodo->getRepository($serviceAlunoPeriodo->getEntity())->findOneBy(
                    ['alunoperId' => $alunoperId['alunoper_id']]
                );

                $referenceAlunoCurso = $serviceAlunoPeriodo->getRepository(
                    $serviceAlunoCurso->getEntity()
                )->findOneBy(
                    ['alunocursoId' => $reference->getAlunocurso()->getAlunocursoId()]
                );

                $alunoEtapaNotas = $serviceEtapaAluno->buscaNotasAlunoPorAlunoper($reference->getAlunoPerId());

                $infoOrg = $ServiceOrgCampus->retornaDadosInstituicao(
                    $reference->getAlunocurso()->getCursocampus()->getCamp()
                );

                $logo = (empty($infoOrg['logo']) || is_null($infoOrg['logo'])) ? getcwd() . '/public/img/logo.png'
                    : $infoOrg['logo'];
                foreach ($alunoEtapaNotas as $cont => $alunoDisciplina) {
                    $objAlunoDisc = $this->getEntityManager()->getRepository(
                        'Matricula\Entity\AcadperiodoAlunoDisciplina'
                    )->findOneby(['alunodiscId' => $alunoDisciplina['alunodiscId']]);

                    $cargaHoraria = $matrizDisciplina->getRepository($matrizDisciplina->getEntity())->findOneBy(
                        [
                            'disc'   => $objAlunoDisc->getDisc()->getDiscId(),
                            'matCur' => $objAlunoDisc->getTurma()->getMatCur()->getMatCurId()
                        ]
                    );

                    if ($cargaHoraria) {
                        $alunoEtapaNotas[$cont]['percFreq'] = round(
                            100 - ($alunoDisciplina['faltas'] * 100) / ($cargaHoraria->getPerDiscChteorica(
                                ) + $cargaHoraria->getPerDiscChpratica() + $cargaHoraria->getPerDiscChestagio())
                        );
                    } else {
                        $alunoEtapaNotas[$cont]['percFreq'] = false;
                    }
                }

                $pdf = new PdfModel();

                $serviceConfiCurso = new \Matricula\Service\AcadCursoConfig($this->getEntityManager());

                /** @var \Matricula\Entity\AcadCursoConfig $objCursoConfig */
                $objCursoConfig = $serviceConfiCurso->retornaObjCursoConfig(
                    $reference->getAlunocurso()->getCursocampus()->getCurso()->getCursoId()
                );

                if (!$objCursoConfig) {
                    $error = $serviceConfiCurso->getLastError();
                    $error = $error ? $error : "Não foi possivel localizar a configuração para o curso informado!";
                    $this->flashMessenger()->addErrorMessage($error);

                    return $this->redirect()->toRoute(
                        $this->getRoute(),
                        array('controller' => $this->getController())
                    );
                }

                $pdf->setOption('paperSize', 'a4'); // Defaults to "8x11"
                $pdf->setOption('paperOrientation', 'portrait'); // Defaults to "portrait"
                $pdf->setVariables(
                    [
                        'alunosEtapasNotas' => $alunoEtapaNotas,
                        'dadosGerais'       => $reference,
                        'aluno'             => $referenceAlunoCurso,
                        'dataBase'          => (new \DateTime('now'))->format("d/m/Y H:i"),
                        'logo'              => $logo,
                        'usuario'           => $objPessoa->getPesNome(),
                    ]
                );

                $pdf->setTemplate('matricula/relatorios/boletin-aluno-periodo-pdf');

                return $pdf;
            } else {
                $this->flashMessenger()->addWarningMessage(
                    "Aluno não tem matricula ativa para o periodo relacionado"
                );

                if ($usrSolicitanteAluno) {
                    return $this->redirect()->toRoute('aluno/default');
                }
            }
        }

        $this->view->setVariables(
            [

                'semestreLetivos' => $semestreLetivos,

            ]
        );

        return $this->getView();
    }

    public function matriculadosPorPeriodoAction()
    {
        $servicePeriodo  = $this->services()->getService('Matricula\Service\AcadperiodoLetivo');
        $semestreLetivos = $this->getEntityManager()->getRepository('Matricula\Entity\AcadperiodoLetivo')->findAll();

        $request = $this->getRequest();

        if ($request->isPost()) {
            $dados        = $request->getPost()->toArray();
            $matriculados = $servicePeriodo->matriculaPorPeriodo(
                $dados['periodo'],
                $dados['situacao'],
                $dados['tipoTurma'],
                $dados['periodoTurno']
            );
            $periodo      = $this->getEntityManager()->getRepository('Matricula\Entity\AcadperiodoLetivo')->findOneBy(
                ['perId' => $dados['periodo']]
            );

            $pdf = new PdfModel();

            $pdf->setOptions(
                [
                    'paperSize',
                    'a4',
                    'paperOrientation',
                    'portrait',
                ]
            );

            $pdf->setVariables(
                [
                    'matriculados' => $matriculados,
                    'periodo'      => $periodo,
                    'dataBase'     => (new \DateTime('now'))->format("d/m/Y H:i"),
                    'dados'        => $dados
                ]
            );

            $pdf->setTemplate('matricula/relatorios/matriculados-geral-pdf');

            return $pdf;
        }

        $this->view->setVariables(
            [

                'periodos' => $semestreLetivos,

            ]
        );

        return $this->view;
    }

    public function listagemCursosAction()
    {
        $serviceAcadNivel      = new \Matricula\Service\AcadNivel($this->getEntityManager());
        $serviceAcadModalidade = new \Matricula\Service\AcadModalidade($this->getEntityManager());
        $serviceOrgCampus      = new \Organizacao\Service\OrgCampus($this->getEntityManager());
        $serviceArea           = new \Matricula\Service\AcadgeralAreaConhecimento($this->getEntityManager());
        $serviceAcadCurso      = new \Matricula\Service\AcadCurso(
            $this->getEntityManager(),
            $this->getServiceManager()->get('Config')
        );
        $pdf                   = $this->getServiceLocator()->get('SnowReportsZf2\Service\SnowReportsZf2');
        $request               = $this->getRequest();
        $arrCursoTurno         = $serviceAcadCurso->getArrSelect2CursoTurno();
        $arrNivel              = $serviceAcadNivel->getArrSelect2();
        $arrModalidade         = $serviceAcadModalidade->getArrSelect2();
        $arrDadosInstituicao   = $serviceOrgCampus->retornaDadosInstituicao();

        if ($request->isPost()) {
            $dados = $request->getPost()->toArray();
            $info  = 'Filtros: ';

            $area       = $dados['areaPesquisa'] ? $serviceArea->getDescricao($dados['areaPesquisa']) : null;
            $campus     = $dados['campus'] ? $serviceOrgCampus->getDescricao(
                $dados['campus']
            ) : null;
            $nivel      = $dados['nivelPesquisa'] ? $serviceAcadNivel->getDescricao($dados['nivelPesquisa']) : null;
            $modalidade = $dados['modalidadePesquisa'] ? $serviceAcadModalidade->getDescricao(
                $dados['modalidadePesquisa']
            ) : null;
            $turno      = $dados['turnoPesquisa'] ? $dados['turnoPesquisa'] : null;

            if ($campus) {
                $info .= 'Campus: ' . $campus['text'] . '; ';
            }

            if ($area) {
                $info .= 'Área: ' . $area . '; ';
            }

            if ($modalidade) {
                $info .= 'Modalidade: ' . $modalidade['text'] . '; ';
            }

            if ($nivel) {
                $info .= 'Nível de ensino: ' . $nivel . '; ';
            }

            if ($turno) {
                $info .= 'Turno: ' . $turno . '; ';

                if (count($turno) > 1) {
                    $paramTurno = '"' . str_replace(',', '","', $turno) . '"';
                } else {
                    $paramTurno = '"' . $turno . '"';
                }
            }

            $info = trim($info);
            $info = trim($info, ';');

            $pdf->setJrxmlPath(__DIR__ . '/../Reports/')
                ->setJrxmlFile("listagem-curso.jrxml")
                ->setAttrbutes(
                    [
                        'instituicao'   => $arrDadosInstituicao['ies'],
                        'mantenedora'   => $arrDadosInstituicao['mantenedora'],
                        'titulo'        => 'Relatório de Cursos',
                        'logo'          => $arrDadosInstituicao['logo'],
                        'relatorioinfo' => $info,
                        'sistema'       => 'Universa',
                        'endereco'      => $arrDadosInstituicao['endereco'],
                        'camp'          => $dados['campus'],
                        'nivel'         => $dados['nivelPesquisa'],
                        'areaid'        => $dados['areaPesquisa'],
                        'mod'           => $dados['modalidadePesquisa'],
                        'turno'         => $paramTurno ? $paramTurno : null
                    ]
                )
                ->downloadHtmlPdf(true)
                ->setOutputFile('relatorio-cursos', 'pdf')
                ->execute();
        }

        $this->getView()->setVariable("arrCursoTurno", $arrCursoTurno);
        $this->getView()->setVariable("arrNivel", $arrNivel);
        $this->getView()->setVariable("arrModalidade", $arrModalidade);

        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function pendenciasReprovacoesAction()
    {
        $periodoLetivo = $this->services()->getService('Matricula\Service\AcadperiodoLetivo')->buscaUltimoPeriodo();
        $request       = $this->getRequest();

        if ($request->isPost()) {
            $dados      = $request->getPost()->toArray();
            $situacaoId = $this->getEntityManager()->getRepository('Matricula\Entity\AcadgeralSituacao')->findOneBy(
                ['matsitDescricao' => $dados['status']]
            )->getSituacaoId();
            $alunos     = $this->services()->getService('Matricula\Service\AcadperiodoTurma')->buscaAlunosTurma(
                '',
                $situacaoId,
                $periodoLetivo->getPerId()
            );

            if ($alunos) {
                $resumo = $this->services()->getService('Matricula\Service\AcadperiodoAlunoResumo');
                foreach ($alunos as $aluno) {
                    $disciplinas = $resumo->buscaDependenciasAluno($aluno['alunocurso_id']);
                    if ($disciplinas) {
                        $alunoDependentes[$aluno['alunocurso_id']] = $disciplinas;
                    }
                }
            }
            $pdf = new PdfModel();

            $pdf->setOptions(
                [
                    'paperSize',
                    'a4',
                    'paperOrientation',
                    'portrait',
                ]
            );

            $pdf->setVariables(
                [
                    'dependencias' => $alunoDependentes,
                    'filtros'      => $dados,
                    'PeriodoBase'  => $periodoLetivo,

                ]
            );

            $pdf->setTemplate('matricula/relatorios/pendencias-reprovacoes-pdf');

            return $pdf;
        }

        return $this->getView();
    }

    public function alunosTurmaDisciplinaAction()
    {
        /* LEGADO REMOVER DEPOIS DAS ATUALIZAÇÕES */
        $service                  = $this->services()->getService('Matricula\Service\AcadperiodoLetivo');
        $serviceCampusCurso       = new \Matricula\Service\CampusCurso($this->getEntityManager());
        $serviceOrgCampus         = new \Organizacao\Service\OrgCampus($this->getEntityManager());
        $serviceSituacao          = new \Matricula\Service\AcadgeralSituacao($this->getEntityManager());
        $campusCurso              = $serviceCampusCurso->buscaCursoCampus();
        $serviceMatriculaSituacao = new \Matricula\Service\AcadgeralSituacao($this->getEntityManager());
        $request                  = $this->getRequest();

        if ($request->isPost()) {
            $dados = $request->getPost()->toArray();

            /** @var \Matricula\Entity\AcadgeralSituacao $objSituacao */
            $objSituacao = $serviceSituacao->getRepository()->findOneBy(
                array('matsitDescricao' => $dados['filtro'])
            );

            $situacao = $objSituacao ? $objSituacao->getSituacaoId() : null;

            // GERAÇÃO DE RELATÓRIO PARA ALUNOS EM FINAL PELO CALCULO DAS NOTAS DAS ETAPAS
            if ($dados['tipo'] == 'alunoFinal') {
                $buscaInfoAlunoFinal = $this->Services()->getService(
                    'Matricula\Service\AcadperiodoAlunoDisciplinaFinal'
                );

                $objDoc = $service->getRepository('Matricula\Entity\AcadperiodoDocenteDisciplina')->findOneBy(
                    ['turma' => $dados['turmas'], 'disc' => $dados['disciplina']]
                );

                if (!is_null($objDoc)) {
                    $objMatrizDisciplina = $service->getRepository(
                        'Matricula\Entity\AcadperiodoMatrizDisciplina'
                    )->findOneBy(
                        [
                            'matCur'         => $objDoc->getTurma()->getMatCur()->getMatCurId(),
                            'disc'           => $dados['disciplina'],
                            'perDiscPeriodo' => $objDoc->getTurma()->getTurmaSerie()
                        ]
                    );
                    $discCHoraria        = $objMatrizDisciplina->getPerDiscChteorica(
                        ) + $objMatrizDisciplina->getPerDiscChpratica() + $objMatrizDisciplina->getPerDiscChestagio();

                    $objs = $buscaInfoAlunoFinal->buscaInfoAlunoFinalRelatorio($objDoc->getDocDiscId(), $discCHoraria);

                    if (is_null($objDoc->getDocdiscDataFechamento())) {
                        $message = 'As notas ainda não foram finalizadas para esta turma e estão sujeita a alterações';
                    } elseif (is_null($objDoc->getDocdiscDataFechamento())) {
                        $message = 'As notas finais ainda não foram finalizadas para esta turma e o calculo final do aluno pode apresentar inconsistencia';
                    } else {
                        $message = 'Notas sujeitas a Alterações';
                    }
                } else {
                    $this->flashMessenger()->addWarningMessage("Sem docente responsável por esta turma!");

                    return $this->redirect()->toRoute(
                        'matricula/default',
                        ['controller' => 'relatorios', 'action' => 'alunos-turma-disciplina']
                    );
                }
                /* CORREÇÃO  DO RELATORIO DE LISTAGEM S033-C046-LISTAGEM*/
            } elseif ($dados['tipo'] == 'listagem') {
                $serviceCampus      = new \Organizacao\Service\OrgCampus($this->getEntityManager());
                $serviceDisciplina  = new \Matricula\Service\AcadgeralDisciplina($this->getEntityManager());
                $serviceCampusCurso = new\Matricula\Service\CampusCurso($this->getEntityManager());

                /** @var \Matricula\Entity\AcadgeralDisciplina $objdisciplina */
                $objdisciplina = $serviceDisciplina->getRepository()->find($dados['disciplina']);

                /** @var \Matricula\Entity\CampusCurso $objCampusCurso */
                $objCampusCurso = $serviceCampusCurso->getRepository()->find($dados['campus_curso']);

                /** @var \SnowReportsZf2\Service\SnowReportsZf2 $snow */
                $snow = $this->getServiceLocator()->get('SnowReportsZf2\Service\SnowReportsZf2');

                $dadosinstituicao = $serviceCampus->retornaDadosInstituicao(
                    $objCampusCurso->getCamp()->getCampId()
                );

                $nomeRelatorio = 'listagem_alunos-' . (new \DateTime('now'))->format('YmdHi');

                $dados['disciplinaNome'] = $objdisciplina->getDiscNome();
                $dados['campusCurso']    = $objCampusCurso->getCurso()->getCursoNome();

                $where = " WHERE 1 ";

                if ($dados['filtro'] != 'Todas') {
                    $where .= " AND acs.matsit_descricao LIKE " . '"' . $dados['filtro'] . '"';
                } elseif ($situacoes = $serviceMatriculaSituacao->situacoesDesligamento()) {
                    $situacoes = is_array($situacoes) ? implode(', ', $situacoes) : $situacoes;

                    $where .= " AND acs.situacao_id NOT IN ({$situacoes}) ";
                }

                if ($dados['campus_curso']) {
                    $where .= " AND acadt.cursocampus_id = {$dados['campus_curso']}";
                }
                if ($dados['periodoLetivo']) {
                    $where .= " AND acadt.per_id = {$dados['periodoLetivo']}";
                }
                if ($dados['turmas']) {
                    $where .= " AND aad.turma_id = {$dados['turmas']}";
                }

                if ($dados['disciplina']) {
                    $where .= " AND acadgeral__disciplina.disc_id = {$dados['disciplina']}";
                }

                $where .= " GROUP BY al.alunoper_id ";
                $where .= " ORDER BY pesnome, matricula ";

                $snow->setJrxmlPath(__DIR__ . '/../Reports/')
                    ->setJrxmlFile('listagem-alunos-turma-disciplina.jrxml')
                    ->setAttrbutes(
                        [
                            'titulo'          => 'Relação dos alunos por turma disciplina',
                            'mantenedora'     => isset($dadosinstituicao['mantenedora']) ? $dadosinstituicao['mantenedora'] : '-',
                            'instituicao'     => isset($dadosinstituicao['ies']) ? $dadosinstituicao['ies'] : '-',
                            'logo'            => isset($dadosinstituicao['logo']) ? $dadosinstituicao['logo'] : '-',
                            'endereco'        => isset($dadosinstituicao['endereco']) ? $dadosinstituicao['endereco'] : '-',
                            'sistema'         => 'Universa',
                            'relatorioinfo'   => isset($dados['disciplinaNome']) ? $dados['disciplinaNome'] : '',
                            'cursonome'       => isset($dados['campusCurso']) ? $dados['campusCurso'] : '-',
                            'where'           => $where,
                            'instituicaoinfo' => '',
                            'situacao'        => $dados['filtro']
                        ]
                    )->setOutputFile((string)$nomeRelatorio)
                    ->downloadHtmlPdf(true)
                    ->execute();

                return $this->getView();
            } else {
                $serviceAcadPeriodoAlunoDisciplina = new \Matricula\Service\AcadperiodoAlunoDisciplina(
                    $this->getEntityManager()
                );

                if ($dados['filtro'] != 'Todas') {
                    $dados['situacao'] = $dados['filtro'];
                } elseif ($situacoes = $serviceMatriculaSituacao->situacoesAlunoInativoEtiqueta()) {
                    $situacoes = is_array($situacoes) ? implode(', ', $situacoes) : $situacoes;

                    $dados['situacaoNot'] = $situacoes;
                }

                $result = $serviceAcadPeriodoAlunoDisciplina->retornaDadosAlunoDisciplina($dados);
            }

            $campus = $serviceCampusCurso->getRepository('Matricula\Entity\CampusCurso')->findOneBy(
                ['cursocampusId' => $dados['campus_curso']]
            );

            $infoOrg = $serviceOrgCampus->retornaDadosInstituicao($campus->getCamp());

            $logo = (empty($infoOrg['logo']) || is_null($infoOrg['logo'])) ? getcwd() . '/public/img/logo.png'
                : $infoOrg['logo'];

            $pdf = new PdfModel();

            $template = 'alunos-disciplina-' . $dados['tipo'];

            $pdf->setVariables(
                [
                    'alunos'       => $result,
                    'alunosFinal'  => $objs,
                    'filtro'       => $dados['filtro'],
                    'mensagem'     => $message,
                    'docente'      => $objDoc,
                    'cargaHoraria' => $discCHoraria,
                    'logo'         => $logo
                ]
            );

            $pdf->setTemplate("matricula/relatorios/{$template}");

            return $pdf;
        }

        $per = $service->getRepository('Matricula\Entity\AcadperiodoLetivo')->findAll();

        $this->view->setVariables(
            [
                'periodos' => $per,
                'campi'    => $campusCurso
            ]
        );

        return $this->view;
    }

    public function buscaDisciplinaTurmaPeriodoAction()
    {
        $json    = new JsonModel();
        $service = new \Matricula\Service\AcadperiodoMatrizDisciplina($this->getEntityManager());
        $dados   = $this->getRequest()->getPost()->toArray();

        $obj = $service->getRepository('Matricula\Entity\AcadperiodoMatrizDisciplina')->findBy(
            ['perDiscPeriodo' => $dados['serie'], 'matCur' => $dados['matId']]
        );

        foreach ($obj as $cont => $disc) {
            $arr[$cont]['disc_id']   = $disc->getDisc()->getDiscId();
            $arr[$cont]['disc_nome'] = $disc->getDisc()->getDiscNome();
        }

        $json->setVariable('dados', $arr);

        return $json;
    }

    public function historicoAlunoAction()
    {
        $serviceRelatorioHistorico  = new \Matricula\Service\AcadperiodoAlunoResumo($this->getEntityManager());
        $serviceAcadgeralAlunoCurso = new \Matricula\Service\AcadgeralAlunoCurso($this->getEntityManager());
        $ServiceOrgCampus           = new \Organizacao\Service\OrgCampus($this->getEntityManager());
        $serviceSisConfig           = new \Sistema\Service\SisConfig(
            $this->getEntityManager(),
            $this->getServiceManager()->get('Config')
        );

        $alturaCabecalhoTimbrado = $serviceSisConfig->localizarChave('ALTURA_CABECALHO_TIMBRADO', 155);
        $alturaRodapeTimbrado    = $serviceSisConfig->localizarChave('ALTURA_RODAPE_TIMBRADO', 155);

        /** @var \Zend\Http\Request $request */
        $request  = $this->getRequest();
        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if ($arrDados['matricula']) {
            $matricula        = $arrDados['matricula'];
            $formatoHistorico = $arrDados['formatoHistorico'];
            $observacao       = $arrDados['observacao'];

            if ($arrDados['salvarHistoricos']) {
                $retorno['erro'] = false;
                $historicosSalvo = $serviceAcadgeralAlunoCurso->salvaObservacoes(
                    array_merge(['alunocursoId' => $matricula], $arrDados)
                );

                if (!$historicosSalvo) {
                    $retorno['erro'] = $serviceAcadgeralAlunoCurso->getLastError();
                }

                if (!$arrDados['emitirHistorico']) {
                    return new \Zend\View\Model\JsonModel($retorno);
                }
            }

            if ($matricula) {
                /* @var $objResumo \Matricula\Entity\AcadperiodoAlunoResumo */
                $objResumo = $serviceRelatorioHistorico
                    ->getRepository('Matricula\Entity\AcadperiodoAlunoresumo')
                    ->findOneBy(['alunocursoId' => $matricula]);

                /* @var $objAlunoCurso \Matricula\Entity\AcadgeralAlunoCurso */
                $objAlunoCurso = $serviceAcadgeralAlunoCurso->getRepository()->find($matricula);

                if ($objAlunoCurso != false && $objResumo != false) {
                    $arrHistorico = $serviceRelatorioHistorico->relatorioHistorico($matricula);

                    $serviceEquivalencia = new \Matricula\Service\AcadperiodoAlunoResumo($this->getEntityManager());

                    $equivalencia = $serviceEquivalencia->retornaEquivalencia($matricula);

                    $objAcadgeralAluno = $objAlunoCurso->getAluno();
                    $objPessoaAluno    = $objAcadgeralAluno->getPes();
                    $objCampus         = $objAlunoCurso->getCursocampus()->getCamp();
                    $objCurso          = $objAlunoCurso->getCursocampus()->getCurso();

                    $pdf = new PdfModel();

                    $mesNome = [
                        ''    => '',
                        'Jan' => 'janeiro',
                        'Feb' => 'fevereiro',
                        'Mar' => 'março',
                        'Apr' => 'abril',
                        'May' => 'maio',
                        'Jun' => 'junho',
                        'Jul' => 'julho',
                        'Aug' => 'agosto',
                        'Sep' => 'setembro',
                        'Oct' => 'outubro',
                        'Nov' => 'novembro',
                        'Dec' => 'dezembro'
                    ];

                    $mesAtual   = (new \DateTime('now'))->format("M");
                    $mesExibido = '';

                    foreach ($mesNome as $mes => $key) {
                        if ($mes == $mesAtual) {
                            $mesExibido = $key;
                            break;
                        }
                    }

                    $serviceOrgCampus = new \Organizacao\Service\OrgCampus($this->getEntityManager());

                    $campus = $arrDados['campusId'] ? $arrDados['campusId'] : $objCampus;

                    $infoOrg = $ServiceOrgCampus->retornaDadosInstituicao($campus);

                    $logo = $infoOrg['logo'];

                    if (empty($infoOrg['logo']) || is_null($infoOrg['logo'])) {
                        $logo = getcwd() . '/public/img/logo.png';
                    }

                    $reconhecimento = $objAlunoCurso->getCursocampus()->getCursocampusReconhecimento();
                    $autorizacao    = $objAlunoCurso->getCursocampus()->getCursocampusAutorizacao();

                    $dataColacao          = '*';
                    $dataExpedicaoDiploma = '*';
                    $ingresso             = '';

                    if ($objAlunoCurso->getAlunocursoDataColacao()) {
                        $dataColacao = $objAlunoCurso->getAlunocursoDataColacao()->format("d/m/Y");
                    }

                    if ($objAlunoCurso->getAlunocursoDataExpedicaoDiploma()) {
                        $dataExpedicaoDiploma = $objAlunoCurso->getAlunocursoDataExpedicaoDiploma()->format("d/m/Y");
                    }

                    if ($objResumo && $objResumo->getAlunoper()) {
                        $ingresso = $objResumo->getAlunoper()->getTurma()->getPer()->getPerNome();
                    }

                    $infoGeralHist = current($arrHistorico);
                    $pdf->setVariables(
                        [
                            'alturaCabecalho' => $alturaCabecalhoTimbrado,
                            'alturaRodape'    => $alturaRodapeTimbrado,
                            'equivalencia'    => $equivalencia,
                            'observacao'      => $objAlunoCurso->getAlunocursoObservacaoHistorico(),
                            'emissao'         => (new \DateTime('now'))->format("d/m/Y"),
                            'dia'             => (new \DateTime('now'))->format("d"),
                            'mes'             => $mesExibido,
                            'mesNum'          => (new \DateTime('now'))->format("m"),
                            'ano'             => (new \DateTime('now'))->format("Y"),
                            'logo'            => $logo,
                            'disciplinas'     => $arrHistorico,
                            'info'            => array(
                                [
                                    'fAno'                   => $infoGeralHist['ano_ensino_medio'],
                                    'ensino_medio'           => $infoGeralHist['formacao_tipo'],
                                    'estabelecimento'        => $infoGeralHist['intituicao_ensino_medio'],
                                    'pontos'                 => $infoGeralHist['inscricao_nota'],
                                    'vestAno'                => $infoGeralHist['edicao_ano'],
                                    'vestSemestre'           => $infoGeralHist['edicao_semestre'],
                                    'inscricaoNota'          => $infoGeralHist['inscricao_nota'],
                                    'fEstado'                => $infoGeralHist['estado_ensino_medio'],
                                    'fCidade'                => $infoGeralHist['cidade_ensino_medio'],
                                    'vestClassificacao'      => $infoGeralHist['classificacao'],
                                    'vestPontuacao'          => $infoGeralHist['pontuacao'],
                                    'tipoVestibularAgendado' => $infoGeralHist['tipo_vestibular'],
                                    'dataProva'              => $infoGeralHist['data_prova'],

                                ]
                            ),
                            'resumo'          => array(
                                'nome'                 => $objPessoaAluno->getPes()->getPesNome(),
                                'emissao_rg'           => $objPessoaAluno->getPesRgEmissao(),
                                'situacao'             => $objAlunoCurso->getAlunocursoSituacao(),
                                'matricula'            => $objResumo->getAlunocursoId(),
                                'pai'                  => $objAcadgeralAluno->getAlunoPai(),
                                'mae'                  => $objAcadgeralAluno->getAlunoMae(),
                                'data_de_nascimento'   => $objPessoaAluno->getPesDataNascimento(),
                                'naturalidade'         => $objPessoaAluno->getPesNaturalidade(),
                                'naturalidadeUF'       => $objPessoaAluno->getPesNascUf(),
                                'nacionalidade'        => $objPessoaAluno->getPes()->getPesNacionalidade(),
                                'identidade'           => $objPessoaAluno->getPesRg(),
                                'cpf'                  => $objPessoaAluno->getPesCpf(),
                                'identidade_exp'       => $objPessoaAluno->getPesEmissorRg(),
                                'identidade_exp_data'  => $objPessoaAluno->getPesRgEmissao(),
                                'sm'                   => $objAcadgeralAluno->getAlunoCertMilitar(),
                                'titulo'               => $objAcadgeralAluno->getAlunoTituloEleitoral(),
                                'zona'                 => $objAcadgeralAluno->getAlunoZonaEleitoral(),
                                'secao'                => $objAcadgeralAluno->getAlunoSecaoEleitoral(),
                                'curso_habilitacao'    => $objCurso->getCursoNome(),
                                'curso_nivel'          => $objCurso->getNivel()->getNivelNome(),
                                'curso_carga_horaria'  => $objCurso->getCursoCargaHoraria(),
                                'reconhecimento'       => $reconhecimento,
                                'autorizacao'          => $autorizacao,
                                'enade'                => $objAlunoCurso->getAlunocursoEnade(),
                                'iesCredenciamento'    => $infoOrg['iesCredenciamento'],
                                'forma_ingresso'       => $objAlunoCurso->getTiposel()->getTiposelNome(),
                                'instituicao'          => $infoOrg['ies'],
                                'telefone'             => $infoOrg['telefone'],
                                'site'                 => $infoOrg['site'],
                                'instEnd'              => $infoOrg['endereco'],
                                'instCidade'           => $infoOrg['cidade'],
                                'instEstado'           => $infoOrg['estado'],
                                'sexo'                 => $objPessoaAluno->getPesSexo(),
                                'dataColacao'          => $dataColacao,
                                'dataExpedicaoDiploma' => $dataExpedicaoDiploma,
                                'secretaria'           => ($infoOrg['secretaria'] ? $infoOrg['secretaria'] : '-'),
                                'observacaoEnade'      => $objAlunoCurso->getAlunocursoEnade(),
                                'reconhecimentoCurso'  => $objCurso->getCursoNumeroMec(),
                                'ingresso'             => $ingresso,
                                'exibeCabecalho'       => $arrDados['historicoCabecalho'] == 'exibeCabecalho' ? true : false,
                                'exibeSumarizador'     => $arrDados['historicoSumarizador'] == 'exibeSumarizador',
                                'cargaHorarioCurso'    => (float)$objCurso->getCursoCargaHoraria()
                            ),
                        ]
                    );

                    $pdf->setTemplate("matricula/relatorios/historico-aluno-pdf")
                        ->setOption('paperSize', 'a4');

                    if ($formatoHistorico == 'duasColunas') {
                        $pdf->setTemplate("matricula/relatorios/historico-aluno-2-colunas-pdf");
                    }

                    return $pdf;
                } else {
                    $this->flashMessenger()->addWarningMessage(
                        "Não foram encontrados registros históricos para esse aluno"
                    );

                    return $this->redirect()->toRoute(
                        'matricula/default',
                        ['controller' => 'relatorios', 'action' => 'historico-aluno']
                    );
                }
            }
            $this->flashMessenger()->addWarningMessage("Matrícula não encontrada no sistema");
        }
        $this->getView()->setTemplate('matricula/relatorios/form-historico-aluno');

        $arrCampusSelect = $ServiceOrgCampus->retornarSelect2Campus();
        $valorPadraoSumarizador =
            $serviceSisConfig->localizarChave('MATRICULA_SUMARIZADOR_RELATORIO_ATIVO', 'naoExibeSumarizador');

        $valorPadraoSumarizador=!$valorPadraoSumarizador?'naoExibeSumarizador':'exibeSumarizador';

        $this->getView()->setVariable('arrCampusSelect', $arrCampusSelect);
        $this->getView()->setVariable('sumarizador', $valorPadraoSumarizador);

        return $this->getView();
    }

    public function fichaInscricaoAction()
    {
        $alunoCurso = $this->params()->fromRoute("id");

        if ($alunoCurso) {
            $serviceAlunoCurso = new \Matricula\Service\AcadgeralAlunoCurso($this->getEntityManager());

            $dadosAluno = $serviceAlunoCurso->dadosGeraisAlunoCurso($alunoCurso);

            if ($dadosAluno != null) {
                $ServiceOrgCampus            = new \Organizacao\Service\OrgCampus($this->getEntityManager());
                $serviceDocumentos           = new \Documentos\Service\DocumentoGrupo($this->getEntityManager());
                $servicePesssoa              = new \Documentos\Service\DocumentoPessoa($this->getEntityManager());
                $serviceTipoTitulo           = new \Financeiro\Service\FinanceiroTituloTipo($this->getEntityManager());
                $serviceFinanceiroValores    = new \Financeiro\Service\FinanceiroValores($this->getEntityManager());
                $serviceAlunoConfigPgtoCurso = new \Financeiro\Service\FinanceiroAlunoConfigPgtoCurso(
                    $this->getEntityManager()
                );

                $titulosMatricula = $serviceTipoTitulo->retornoTiposDeTituloParaEmissaoNaMatricula(
                    $dadosAluno['cursocampus_id'],
                    true
                );

                foreach ($titulosMatricula as $key => $plano) {
                    $valores = $serviceFinanceiroValores->buscaValoresAtivosParaTipoTitulo(
                        $serviceFinanceiroValores->getRepository('Financeiro\Entity\FinanceiroTituloTipo')->findBy(
                            ['tipotituloNome' => $plano['tipotituloNome']]
                        ),
                        $dadosAluno
                    );

                    $selecao = $serviceAlunoConfigPgtoCurso->getRepository()->findOneBy(
                        ['alunocurso' => $alunoCurso, 'tipotitulo' => $plano['tipotituloId']]
                    );

                    $selecao = $selecao ? $selecao->toArray() : [];

                    $encontrou = false;

                    foreach ($valores as $pos => $valor) {
                        if ($selecao && $selecao['valoresId'] == $valor['valoresId']) {
                            $valores[$pos]['selecao']        = true;
                            $valores[$pos]['valoresPreco']   = $selecao['configPgtoCursoValor'];
                            $valores[$pos]['valoresParcela'] = $selecao['configPgtoCursoParcela'];
                            $encontrou                       = true;
                        }
                    }

                    if (!$encontrou && $selecao) {
                        $selecao['selecao']        = true;
                        $selecao['valoresPreco']   = $selecao['configPgtoCursoValor'];
                        $selecao['valoresParcela'] = $selecao['configPgtoCursoParcela'];
                        $valores[]                 = $selecao;
                    }

                    $valoresPorTitulo[$plano['tipotituloNome']] = [
                        'tipotituloId'   => $plano['tipotituloId'],
                        'tipotituloNome' => $plano['tipotituloNome'],
                        'valores'        => $valores,
                        'selecao'        => $selecao
                    ];
                }

                $documentosAluno = $servicePesssoa->getArrayDocumentosPessoa($dadosAluno['pes_id']);

                if (!$documentosAluno) {
                    $documentosAluno = $serviceDocumentos->documentosGrupo('Aluno');

                    foreach ($documentosAluno as $key => $documento) {
                        $documentosAluno[$key] = [
                            'tdocDescricao'   => $documento['tdoc_descricao'],
                            'docpessoaStatus' => 'Não Entregue'
                        ];
                    }
                } else {
                    foreach ($documentosAluno as $key => $documento) {
                        if ($documento['docpessoaStatus'] == 'Não') {
                            $documentosAluno[$key]['docpessoaStatus'] = 'Não entregue';
                        } elseif ($documento['docpessoaStatus'] === 'Sim') {
                            $documentosAluno[$key]['docpessoaStatus'] = 'Entregue';
                        }
                    }
                }

                $infoOrg = $ServiceOrgCampus->retornaDadosInstituicao($dadosAluno['camp_id']);

                $pdf = new PdfModel();

                $pdf->setTemplate("matricula/relatorios/ficha-aluno")
                    ->setOption('paperSize', 'Letter');

                $pdf->setVariables(
                    [

                        'dadosAluno'       => $dadosAluno,
                        'logo'             => $infoOrg['logo'],
                        'ies'              => $infoOrg['iesNome'],
                        'valoresPorTitulo' => $valoresPorTitulo,
                        'documentos'       => $documentosAluno,
                        'diaAtual'         => (new \DateTime('now'))->format("d/m/Y H:i"),
                    ]
                );

                return $pdf;
            } else {
                $this->flashMessenger()->addWarningMessage(
                    "Não foi Possivel carregar os dados. Entre em contato com o suporte"
                );

                return $this->redirect()->toRoute($this->getRoute(), array('controller' => 'acadgeral-aluno'));
            }
        }

        return $this->getView();
    }

    public function alunoPorAgenteAction()
    {
        $serviceOrgCampus  = new \Organizacao\Service\OrgCampus($this->getEntityManager());
        $serviceAlunoCurso = new \Matricula\Service\AcadgeralAlunoCurso($this->getEntityManager());
        $servicePessoa     = new \Pessoa\Service\Pessoa($this->getEntityManager());
        $request           = $this->getRequest();

        if ($request->isPost()) {
            $pdf                 = $this->getServiceLocator()->get('SnowReportsZf2\Service\SnowReportsZf2');
            $arrDadosInstituicao = $serviceOrgCampus->retornaDadosInstituicao();
            $dados               = $request->getPost()->toArray();
            $info                = '';

            $dataInicio = $dados['dataInicio'] ?
                str_replace("-", "", $serviceOrgCampus::formatDateAmericano($dados['dataInicio'])) : null;
            $dataFinal  = $dados['dataFinal'] ?
                str_replace("-", "", $serviceOrgCampus::formatDateAmericano($dados['dataFinal'])) : null;

            $situacao = preg_replace('/["]/', '', $dados['alunocursoSituacao']);
            $agente   = $servicePessoa->getDescricao($dados['pesIdAgente']);

            if ($dataInicio && $dataFinal) {
                $info .= ' De: ' . $dados['dataInicio'] . ' até ' . $dados['dataFinal'];
            } elseif ($dataInicio) {
                $info .= ' A partir da data: ' . $dados['dataInicio'];
            } elseif ($dataFinal) {
                $info .= ' Anterior a data: ' . $dados['dataFinal'];
            }

            if ($situacao) {
                $info .= '; Alunos ' . $situacao . 's';
            }

            if ($agente) {
                $info .= '; Agente: ' . $agente;
            }

            $pdf->setJrxmlPath(__DIR__ . '/../Reports/')
                ->setJrxmlFile("relatorio-alunos-por-agente.jrxml")
                ->setOutputFile('relatorio-alunos-por-agente', 'pdf')
                ->downloadHtmlPdf(true)
                ->setAttrbutes(
                    [
                        'instituicao'   => $arrDadosInstituicao['ies'],
                        'mantenedora'   => $arrDadosInstituicao['mantenedora'],
                        'titulo'        => 'Relatório Quantitativo de Captação de Alunos e Cursos para Alunos por Agente',
                        'logo'          => $arrDadosInstituicao['logo'],
                        'relatorioinfo' => trim(trim($info), ','),
                        'sistema'       => 'Universa',
                        'endereco'      => $arrDadosInstituicao['endereco'],
                        'datainicial'   => $dataInicio,
                        'datafinal'     => $dataFinal,
                        'agentealuno'   => $dados['pesIdAgente'],
                        'situacao'      => $dados['alunocursoSituacao']
                    ]
                )
                ->execute();
        }

        $arrSituacao = $serviceAlunoCurso->getArrSelect2AlunocursoSituacao();

        foreach ($arrSituacao as &$situacao) {
            $situacao['id'] = '"' . $situacao['id'] . '"';
        }

        $this->getView()->setVariable('arrAlunoSituacao', $arrSituacao);

        return $this->getView();
    }

    public function alunoPorOperadorAction()
    {
        $serviceOrgCampus  = new \Organizacao\Service\OrgCampus($this->getEntityManager());
        $serviceAlunoCurso = new \Matricula\Service\AcadgeralAlunoCurso($this->getEntityManager());
        $acessoPessoa      = new \Acesso\Service\AcessoPessoas($this->services()->getEm());
        $request           = $this->getRequest();

        if ($request->isPost()) {
            $pdf                 = $this->getServiceLocator()->get('SnowReportsZf2\Service\SnowReportsZf2');
            $arrDadosInstituicao = $serviceOrgCampus->retornaDadosInstituicao();
            $dados               = $request->getPost()->toArray();
            $info                = '';

            $datacadastroinicial = $dados['datacadastroinicial'] ?
                str_replace("-", "", $serviceOrgCampus::formatDateAmericano($dados['datacadastroinicial'])) : null;
            $datacadastrofinal   = $dados['datacadastrofinal'] ?
                str_replace("-", "", $serviceOrgCampus::formatDateAmericano($dados['datacadastrofinal'])) : null;

            $situacaoaluno = preg_replace('/["]/', '', $dados['situacaoaluno']);
            if ($dados['operadormult'] != '') {
                $operadormult = $acessoPessoa->getDescricao($dados['operadormult']);
            }

            if ($datacadastroinicial && $datacadastrofinal) {
                $info .= 'Data de Cadastro De: ' . $dados['datacadastroinicial'] . ' até ' . $dados['datacadastrofinal'];
            } elseif ($datacadastroinicial) {
                $info .= ' A partir da data: ' . $dados['datacadastroinicial'];
            } elseif ($datacadastrofinal) {
                $info .= ' Anterior a data: ' . $dados['datacadastrofinal'];
            }

            if ($situacaoaluno) {
                $info .= '; Alunos: ' . $situacaoaluno . 's';
            }

            if ($operadormult) {
                $info .= '; Operador: ' . $operadormult;
            }

            $pdf->setJrxmlPath(__DIR__ . '/../Reports/')
                ->setJrxmlFile("relatorio-alunos-operadores.jrxml")
                ->setOutputFile('relatorio-alunos-operadores', 'pdf')
                ->downloadHtmlPdf(true)
                ->setAttrbutes(
                    [
                        'instituicao'              => $arrDadosInstituicao['ies'],
                        'mantenedora'              => $arrDadosInstituicao['mantenedora'],
                        'titulo'                   => 'Relatório Quantitativo de Cadastro de Aluno e Cursos p/Alunos',
                        'logo'                     => $arrDadosInstituicao['logo'],
                        'relatorioinfo'            => trim(trim($info), ','),
                        'sistema'                  => 'Universa',
                        'endereco'                 => $arrDadosInstituicao['endereco'],
                        'datacadastroinicialaluno' => $datacadastroinicial,
                        'datacadastrofinalaluno'   => $datacadastrofinal,
                        'datacadastroinicialcurso' => $datacadastroinicial,
                        'datacadastrofinalcurso'   => $datacadastrofinal,
                        'operador'                 => $dados['operadormult'],
                        'situacaoaluno'            => $dados['situacaoaluno']

                    ]
                )
                ->execute();
        }

        $arrSituacao = $serviceAlunoCurso->getArrSelect2AlunocursoSituacao();

        foreach ($arrSituacao as &$situacao) {
            $situacao['id'] = '"' . $situacao['id'] . '"';
        }

        $this->getView()->setVariable('arrAlunoSituacao', $arrSituacao);

        return $this->getView();
    }

    public function emissaoCertificadosAction()
    {
        $serviceAcadgeralAlunocurso = new AcadgeralAlunoCurso(
            $this->getEntityManager(),
            $this->getServiceManager()->get('config')
        );

        $request = $this->getRequest();

        $arrCertificadoModelo = array(
            ["id" => "certificado", "text" => "Certificado de Conclusão de Curso"],
            ["id" => "conclusao-curso", "text" => "Certidão de Conclusão de Curso"],
            ["id" => "conclusao-curso-historico", "text" => "Certidão de Conclusão de Curso C/ Histórico"],
            ["id" => "declaracao-matricula", "text" => "Declaração de Matrícula"],
            ["id" => "declaracao-matricula-frequencia", "text" => "Declaração de Matrícula e Frequência"],
            ["id" => "historico-disciplinas", "text" => "Histórico de Disciplinas"],
        );

        if ($request->isPost()) {
            $arrDados = $request->getPost()->toArray();

            if ($arrDados) {
                $encontrou = false;

                foreach ($arrCertificadoModelo as $arrCertificado) {
                    if ($arrCertificado['id'] == $arrDados['certificadoModelo']) {
                        $encontrou = true;
                        break;
                    }
                }

                if (!$encontrou) {
                    $this->flashMessenger()->addWarningMessage('Modelo de documento não encontrado!');

                    return $this->redirect()->toRoute(
                        'matricula/default',
                        ['controller' => 'relatorios', 'action' => 'emissao-certificados']
                    );
                }

                $template = "matricula/relatorios/emissao-certificados/" . $arrDados['certificadoModelo'] . '.phtml';

                $arrAlunoInfo = $serviceAcadgeralAlunocurso->emissaoCertificados($arrDados);

                if (!$arrAlunoInfo) {
                    $this->flashMessenger()->addWarningMessage($serviceAcadgeralAlunocurso->getLastError());

                    return $this->redirect()->toRoute(
                        'matricula/default',
                        ['controller' => 'relatorios', 'action' => 'emissao-certificados']
                    );
                }

                $this->getView()->setVariable('arrAlunoInfo', $arrAlunoInfo);

                $this->getView()->setTemplate($template);
                $this->getView()->setTerminal(true);
            }
        }

        $this->getView()->setVariable("arrCertificadoModelo", $arrCertificadoModelo);

        return $this->getView();
    }

    public function relatorioAlunoContatoAction()
    {
        /** @var \Zend\Http\Request $request */
        $request                 = $this->getRequest();
        $serviceAcadPeriodoAluno = new \Matricula\Service\AcadperiodoAluno($this->getEntityManager());
        $serviceSituacoes        = new \Matricula\Service\AcadgeralSituacao($this->getEntityManager());
        $arrSituacao             = $serviceAcadPeriodoAluno->getArrSelect2AlunoperiodoSituacao();

        $erro    = false;
        $msgErro = array();

        if ($request->isPost()) {
            $dados = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getQuery()->toArray()
            );

            /** @var \SnowReportsZf2\Service\SnowReportsZf2 $snow */
            $snow          = $this->getServiceLocator()->get('SnowReportsZf2\Service\SnowReportsZf2');
            $nomeRelatorio = 'contato_alunos-' . (new \DateTime('now'))->format('YmdHi');

            $conditions = '';

            if (isset($dados['perId'])) {
                $perId = $dados['perId'];

                if ($perId == "desligados" || $perId == "null") {
                    if (isset($dados['situacao'])) {
                        unset($dados['situacao']);
                    }
                }

                if ($perId == "desligados") {
                    $situacoesDesligamento = $serviceSituacoes::situacoesDesligamento();

                    if (isset($dados['situacao'])) {
                        $arrSituacao       = explode(",", $dados['situacao_id']);
                        $dados['situacao'] = array_merge($arrSituacao, $situacoesDesligamento);
                    } else {
                        $dados['situacao'] = $situacoesDesligamento;
                    }

                    $dados['situacao'] = implode(',', $dados['situacao']);

                    $dados['desligados'] = true;
                    unset($dados['per_id']);
                }
            }

            if ($dados['situacao']) {
                $conditions .= " AND situacao_id in ({$dados['situacao']})";
            }

            if (isset($dados['perId'])) {
                if ($dados['per_id'] == "null") {
                    $conditions .= ' AND acadperiodo__letivo.per_id is null ';
                } else {
                    $conditions .= ' AND acadperiodo__letivo.per_id IN ( ' . $dados['perId'] . ' )';
                }
            }

            if ($dados['turma']) {
                $conditions .= " AND acadperiodo__turma.turma_id in (" . $dados['turma'] . ")";
            }

            if ($dados['turno']) {
                $arrTurnos     = array_filter(explode(',', $dados['turno']));
                $arrTurnoQuery = array();

                foreach ($arrTurnos as $value) {
                    $arrTurnoQuery[] = ' FIND_IN_SET ( "' . $value . '",curso_turno)';
                }

                $conditions .= ' AND ( ' . implode(' OR ', $arrTurnoQuery) . ' )';
            }

            if ($dados['serie']) {
                $conditions .= " AND turma_serie IN (" . $dados['serie'] . " )";
            }

            if ($dados['curso']) {
                $conditions .= " AND curso_id IN (" . $dados['curso'] . " )";
            }

            if ($dados['campus']) {
                $conditions .= " AND camp_id IN (" . $dados['campus'] . " )";
            }

            if ($erro) {
                $this->getView()->setTemplate('matricula/relatorios/aluno-contato');
                $this->getView()->setVariable('arrSituacao', $arrSituacao);
                foreach ($msgErro as $msg) {
                    $this->flashMessenger()->addErrorMessage($msg);
                }

                return $this->getView();
            }

            $where = $conditions;

            $where .= " GROUP BY acadgeral__aluno_curso.alunocurso_id";
            $where .= " ORDER BY pes_nome, acadgeral__aluno_curso.alunocurso_id ";

            $snow->setJrxmlPath(__DIR__ . '/../Reports/')
                ->setAttrbutes(
                    [
                        'sistema'         => 'Universa',
                        'condicoessql'    => $where,
                        'instituicaoinfo' => '',
                    ]
                )->setJrxmlFile('listagem-alunos-contato.jrxml')
                ->setOutputFile($nomeRelatorio, 'xlsx')
                ->downloadHtmlPdf(true)
                ->execute();

            return $this->getView();
        }

        $this->getView()->setTemplate('matricula/relatorios/aluno-contato');
        $this->getView()->setVariable('arrSituacao', $arrSituacao);

        return $this->getView();
    }
}