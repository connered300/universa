<?php

namespace Matricula\Controller;

use VersaSpine\Controller\AbstractCoreController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class AcadperiodoDocenteDisciplinaController extends AbstractCoreController
{
    public function indexAction()
    {
        $serviceAcadperiodoDocenteDisciplina = $this->getServiceLocator()->get(
            'Matricula\Service\AcadperiodoDocenteDisciplina'
        );

        $serviceAcadperiodoLetivo = $this->getServiceLocator()->get('Matricula\Service\AcadperiodoLetivo');

        $periodoLetivoAtual = $serviceAcadperiodoLetivo->buscaUltimoPeriodo();

        $turmas   = $serviceAcadperiodoDocenteDisciplina->getRepository(
            'Matricula\Entity\AcadperiodoTurma'
        )->buscaTurmasPorPeriodo($periodoLetivoAtual->getPerId());
        $docentes = $serviceAcadperiodoDocenteDisciplina->getRepository(
            'Matricula\Entity\AcadgeralDocente'
        )->buscaDocenteAll();

        $this->view->setVariables(
            [
                'docentes'     => $docentes,
                'docentesJson' => \Zend\Json\Json::encode($docentes, true),
                'turmas'       => $turmas
            ]
        );

        return $this->view;
    }

    public function listagemDocenciasAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $arrayData = $request->getPost()->toArray();

            if ($arrayData['somenteComPeriodo'] && $arrayData['filter']['periodoId']) {
                $serviceAcadperiodoDocenteDisciplina = new \Matricula\Service\AcadperiodoDocenteDisciplina(
                    $this->getEntityManager()
                );

                $result = $serviceAcadperiodoDocenteDisciplina->paginationAjax($arrayData);
            }

            $this->json->setVariable("draw", isset($result["draw"]) ? $result["draw"] : 1);
            $this->json->setVariable("recordsTotal", isset($result["recordsTotal"])?$result["recordsTotal"]: 0);
            $this->json->setVariable("recordsFiltered", isset($result["recordsFiltered"])?$result["recordsFiltered"]: 0);
            $this->json->setVariable("data", $result["data"] ? $result["data"] : []);
        }

        return $this->json;
    }

    public function saveAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $arrayData = $request->getPost()->toArray();

            $serviceAcadperiodoDocenteDisciplina = $this->getServiceLocator()->get(
                'Matricula\Service\AcadperiodoDocenteDisciplina'
            );

            try {
                $result = $serviceAcadperiodoDocenteDisciplina->save($arrayData);
                $this->flashMessenger()->addSuccessMessage(
                    "Docencia(s) vinculada(s)/editada(s) com sucesso!"
                );
            } catch (\Exception $ex) {
                $erro = $ex->getMessage();
                throw new \Exception($ex->getMessage());
                $this->flashMessenger()->addErrorMessage($erro);
            }
        }

        return $this->redirect()->toRoute('matricula/default', array('controller' => 'acadperiodo-docente-disciplina'));
    }
}