<?php

namespace Matricula\Controller;

use VersaSpine\Controller\AbstractCoreController;

class AcadperiodoTurmaController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {
        $request    = $this->getRequest();
        $paramsGet  = $request->getQuery()->toArray();
        $paramsPost = $request->getPost()->toArray();
        $param      = array_merge($paramsGet, $paramsPost);

        $serviceTurma = new \Matricula\Service\AcadperiodoTurma($this->getEntityManager());

        $arrDados = $serviceTurma->pesquisaForJson($param);

        return new \Zend\View\Model\JsonModel($arrDados ? $arrDados : array());
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAcadperiodoTurma = new \Matricula\Service\AcadperiodoTurma($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();

            $result = $serviceAcadperiodoTurma->getDataForDatatables($dataPost);

            $this->getJson()->setVariables($result);
        }

        return $this->getJson();
    }

    /**
     * @return \Zend\View\Model\JsonModel
     */
    public function buscaTurmasAction()
    {
        $requisicao = $this->getRequest();

        if ($requisicao->isPost()) {
            $data                     = $requisicao->getPost()->toArray();
            $service                  = new \Matricula\Service\AcadperiodoTurma($this->getEntityManager());
            $serviceAcadperiodoLetivo = $this->Services()->getService('Matricula\Service\AcadperiodoLetivo');

            if ($data['periodoLetivoAtual'] == 'false') {
                $turmas = $service->getRepository('Matricula\Entity\AcadperiodoTurma')->buscaTurmas(
                    $data['cursoCampusId'],
                    $data['periodo'],
                    $serviceAcadperiodoLetivo->buscaUltimoPeriodo()
                );
            } elseif ($data['turmasPeriodoLetivoAtual']) {
                $turmas = $service->buscaTurmas();
            } else {
                $turmas = $service->getRepository('Matricula\Entity\AcadperiodoTurma')->buscaTurmas(
                    $data['cursoCampusId'],
                    $data['periodo'],
                    $serviceAcadperiodoLetivo->buscaPeriodoCorrente()->getPerId()
                );
            }

            $this->json->setVariable("turmas", $turmas);
        }

        return $this->json;
    }

    /**
     * @return \Zend\View\Model\JsonModel
     */
    public function buscaTurmasPorPeriodoLetivoAction()
    {
        $requisicao = $this->getRequest();

        if ($requisicao->isPost()) {
            $data    = $requisicao->getPost()->toArray();
            $service = new \Matricula\Service\AcadperiodoTurma($this->getEntityManager());

            $turmas = $service->getRepository('Matricula\Entity\AcadperiodoTurma')->buscaTurmas(
                $data['cursoCampusId'],
                $data['periodo'],
                $data['periodoLetivo']
            );

            $this->json->setVariable("turmas", $turmas);
        }

        return $this->json;
    }

    // busca turmas por turna de acordo com a integradora.
    public function buscaTurmasPorTurnoAction()
    {
        $requisicao = $this->getRequest();

        if ($requisicao->isPost()) {
            $data    = $requisicao->getPost()->toArray();
            $service = new \Matricula\Service\AcadperiodoTurma($this->getEntityManager());
            $turmas  = $service->getRepository('Matricula\Entity\AcadperiodoTurma')->buscaTurmasPorTurno(
                $data['cursoCampusId'],
                $data['periodo'],
                $data['periodoLetivo'],
                $data['turno']
            );

            $this->json->setVariable("turmas", $turmas);
        }

        return $this->json;
    }

    public function addAction()
    {
        $serviceTurma = new \Matricula\Service\AcadperiodoTurma($this->getEntityManager());

        $request = $this->getRequest();
        $form    = $this->forms()->mountForm($this->getForm());
        if ($request->isPost()) {
            $service = new \Matricula\Service\AcadperiodoTurma($this->getEntityManager());

            try {
                $dados = array_merge($request->getPost()->toArray(), $request->getFiles()->toArray());
                $ret   = $service->adicionar($dados);
                if ($ret) {
                    $serviceTurma->efetuaIntegracaoTurma($ret);
                }

                if (is_null($this->getRoute()) and $ret) {
                    $this->setRoute($this->getEvent()->getRouteMatch()->getMatchedRouteName());
                    $this->flashMessenger()->addSuccessMessage("Turma cadastrada com sucesso!");
                }

                if (!$ret) {
                    $this->flashMessenger()->addErrorMessage($service->getLastError());
                }
            } catch (\Exception $ex) {
                $erro = $ex->getMessage();
                $form->setData($request->getPost()->toArray());
            }
            $is_json = $request->getPost("is_json");
            if (isset($erro)) {
                if ($is_json) {
                    $this->json->setVariable("erro", $erro);

                    return $this->json;
                }
                $this->flashMessenger()->addErrorMessage($erro);
            } else {
                if ($is_json) {
                    if (method_exists($ret, "toArray")) {
                        $dados = $ret->toArray();
                    } else {
                        $dados = "Os dados foram inseridos corretamente, mas um erro ocorreu, verifique se existe o método toArray na entidade {$this->getEntity()}!";
                    }
                    $this->json->setVariable("result", $dados);

                    return $this->json;
                }
            }

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        $this->view->setVariable("form", $form);
        $x = $this->view->getTemplate();
        $this->view->setTemplate($this->getTemplateToRoute());

        return $this->view;
    }

    public function editAction()
    {
        $request      = $this->getRequest();
        $form         = $this->forms()->mountForm($this->getForm());
        $serviceTurma = new \Matricula\Service\AcadperiodoTurma($this->getEntityManager());

        if ($request->isPost()) {
            try {
                $dados = array_merge($request->getPost()->toArray(), $request->getFiles()->toArray());
                $ret   = $serviceTurma->edita($dados);

                if ($ret) {
                    $serviceTurma->efetuaIntegracaoTurma($ret);
                }

                if (is_null($this->getRoute()) and $ret) {
                    $message = $serviceTurma->getLastError();

                    if ($message) {
                        $this->flashMessenger()->addInfoMessage($message);
                    }

                    $this->flashMessenger()->addSuccessMessage("Turma alterada com sucesso!");
                    $this->setRoute($this->getEvent()->getRouteMatch()->getMatchedRouteName());
                }

                if ($serviceTurma->getLastError()) {
                    $this->flashMessenger()->addErrorMessage($serviceTurma->getLastError());
                }
            } catch (\Exception $ex) {
                $erro = $ex->getMessage();

                $form->setData($request->getPost()->toArray());
            }

            $is_json = $request->getPost("is_json");
            if (isset($erro)) {
                if ($is_json) {
                    $this->json->setVariable("erro", $erro);

                    return $this->json;
                }
                $this->flashMessenger()->addErrorMessage($erro);
            } else {
                if ($is_json) {
                    if (method_exists($ret, "toArray")) {
                        $dados = $ret->toArray();
                    } else {
                        $dados = "Os dados foram alterados corretamente, mas um erro ocorreu, verifique se existe o método toArray na entidade {$this->getEntity()}!";
                    }
                    $this->json->setVariable("result", $dados);

                    return $this->json;
                }
            }

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        $id = $this->params()->fromRoute("id", 0);
        if (!$id) {
            throw new \Exception(
                "Para editar um registro é necessário informar o identificador do mesmo pela rota!", 1
            );
        }

        $reference = $serviceTurma->getReference($id);
        $form->setData($reference->toArray());

        $this->view->setVariable("form", $form);
        $this->view->setTemplate('matricula/acadperiodo-turma/add');

        return $this->view;
    }

    public function removeAction()
    {
        $service = new \Matricula\Service\AcadperiodoTurma($this->getEntityManager());
        $id      = $this->params()->fromRoute('id', 0);
        $is_json = $this->getRequest()->getPost("is_json");
        $xml     = new \Zend\Config\Reader\Xml();
        if (!$id) {
            $id = $this->getRequest()->getPost("id");
            if (!$id) {
                if ($is_json) {
                    $this->json->setVariable(
                        "erro",
                        "Para excluir o registro envie o seu valor com a chave id, atraves desse campo o mesmo ira tentar excluir o registro!"
                    );

                    return $this->json;
                }
                throw new \Exception("Deve ser indicado pela rota o id do registro a ser excluído", 0);
            }
        }

        try {
            $alunoTurma = $service->getRepository("Matricula\Entity\AcadperiodoAluno")->findBy(['turma' => $id]);
            if (!$alunoTurma) {
                $service->excluir($id);
            } else {
                if ($is_json) {
                    $erro = [
                        'erro' => [
                            'registro' => $id,
                            'msg'      => "Não foi possível excluir esta turma pois existem alunos vinculados à ela.",
                        ]
                    ];

                    $this->json->setVariable("erro", $erro);

                    return $this->json;
                } else {
                    $this->flashMessenger()->addErrorMessage(
                        "Não foi possível excluir esta turma pois existem alunos vinculados à ela."
                    );

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }
            }
        } catch (\Exception $ex) {
            $erro = $xml->fromString($ex->getMessage());
        }
        if ($is_json) {
            if ($erro) {
                $this->json->setVariable("erro", $erro);
            } else {
                $this->json->setVariable("result", "Registro excluído com sucesso!");
            }

            return $this->json;
        } else {
            $this->flashMessenger()->addErrorMessage($erro);
        }

        return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
    }

    public function criarNovasTurmasAction()
    {
        $form    = $this->forms()->mountForm($this->getForm());
        $request = $this->getRequest();
        if ($request->isPost()) {
            $service = new \Matricula\Service\AcadperiodoTurma($this->getEntityManager());
            $data    = $request->getPost()->toArray();
            try {
                $service->clonaTurmas($data);
                $this->flashMessenger()->addSuccessMessage("Turmas criadas com sucesso!");
            } catch (\Exception $e) {
                $this->flashMessenger()->addErrorMessage($e->getMessage());
                $this->flashMessenger()->addErrorMessage(
                    "Não foi possivel clonar as turmas favor entrar em contato com suporte!"
                );
            }

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }
        $this->view->setVariable('form', $form);

        return $this->view;
    }

    public function turmasOrigemDestinoSugestaoAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $service = new \Matricula\Service\AcadperiodoTurma($this->getEntityManager());
            $data    = $request->getPost()->toArray();
            $turmas  = $service->buscaTurmasOrigemDestinoSugestao($data);
            $this->json->setVariable('turmas', $turmas);

            return $this->json;
        }

        return $this->json->setVariable('error', 'periodo não encontrado');
    }

    public function buscaTurmasTurnoAction($turno)
    {
        $requisicao = $this->getRequest();

        if ($requisicao->isPost()) {
            $data    = $requisicao->getPost()->toArray();
            $service = new \Matricula\Service\AcadperiodoTurma($this->getEntityManager());

            $turmas = $service->getRepository('Matricula\Entity\AcadperiodoTurma')->buscaTurmas(
                $data['cursoCampusId'],
                $data['periodo']
            );

            $this->json->setVariable("turmas", $turmas);

            return $this->json;
        }
    }

    public function buscasDocentesDisciplinasTurmaPorTurmaAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $service     = new \Matricula\Service\AcadperiodoTurma($this->getEntityManager());
            $data        = $request->getPost()->toArray();
            $disciplinas = $service->buscasDocentesDisciplinasTurmaPorTurma($data['turmaId']);
            $this->json->setVariable('disciplinas', $disciplinas);

            return $this->json;
        }

        return $this->json->setVariable('error', 'Disciplinas não encontradas');
    }

}