<?php

namespace Matricula\Controller;

use VersaSpine\Controller\AbstractCoreController;

class AcadCursoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request          = $this->getRequest();
        $paramsGet        = $request->getQuery()->toArray();
        $paramsPost       = $request->getPost()->toArray();
        $serviceAcadCurso = new \Matricula\Service\AcadCurso($this->getEntityManager());

        $result = $serviceAcadCurso->returnOptionsCurso(array_merge($paramsGet, $paramsPost), true);

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceAcadCurso = new \Matricula\Service\AcadCurso(
            $this->getEntityManager(),
            $this->getServiceManager()->get('Config')
        );

        $serviceAcadCurso->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAcadCurso = new \Matricula\Service\AcadCurso($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceAcadCurso->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function addAction($cursoId = false)
    {
        $request = $this->getRequest();

        $arrDados               = array();
        $servicePessoaFisica    = new \Pessoa\Service\PessoaFisica($this->getEntityManager());
        $serviceAcadCurso       = new \Matricula\Service\AcadCurso(
            $this->getEntityManager(),
            $this->getServiceManager()->get('Config')
        );
        $serviceAcadNivel       = new \Matricula\Service\AcadNivel($this->getEntityManager());
        $serviceAcadModalidade  = new \Matricula\Service\AcadModalidade($this->getEntityManager());
        $serviceCampus          = new \Organizacao\Service\OrgCampus($this->getEntityManager());
        $servicePeriodoLetivo   = new \Matricula\Service\AcadperiodoLetivo($this->getEntityManager());
        $serviceAcadCursoConfig = new \Matricula\Service\AcadCursoConfig($this->getEntityManager());

        if ($cursoId) {
            $arrDados = $serviceAcadCurso->getArray($cursoId);

            if (empty($arrDados)) {
                $this->flashMessenger()->addErrorMessage($serviceAcadCurso->getLastError());

                return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );

            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceAcadCurso->salvarCurso($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);

                $this->flashMessenger()->addSuccessMessage('Registro de curso salvo!');

                return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
            }

            $this->flashMessenger()->addErrorMessage($serviceAcadCurso->getLastError());
        }

        $numeroDeAlunosAtivosNoCurso = $serviceAcadCurso->numeroDeAlunosAtivosNoCurso($cursoId);

        if ($numeroDeAlunosAtivosNoCurso) {
            $this->flashMessenger()->addInfoMessage(
                'Atenção: este curso possui ' . $numeroDeAlunosAtivosNoCurso . ' alunos ativos.'
            );
        }

        $serviceAcadCurso->formataDadosPost($arrDados);

        $arrCampus              = $serviceCampus->getArrSelect2();
        $arrPeriodoLetivo       = $servicePeriodoLetivo->getArrSelect2();
        $arrMetodo              = $serviceAcadCursoConfig->getArrSelect2MetodoDesempenho();
        $arrMetodoFinal         = $serviceAcadCursoConfig->getArrSelect2MetodoDesempenhoFinal();
        $arrNotaFracionada      = $serviceAcadCursoConfig->getArrSelect2NotaFracionada();
        $arrSituacaoDeferimento = (
        \Matricula\Service\AcadgeralAlunoCurso::getArrSelect2AlunocursoSituacaoDisponiveisDeferimento()
        );

        $servicePessoaFisica->setarDependenciasView($this->getView());
        $serviceAcadCurso->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrCampus", $arrCampus);
        $this->getView()->setVariable("arrPeriodoLetivo", $arrPeriodoLetivo);
        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setVariable("metodo", $arrMetodo);
        $this->getView()->setVariable("metodoFinal", $arrMetodoFinal);
        $this->getView()->setVariable("notaFracionada", $arrNotaFracionada);
        $this->getView()->setVariable("situacaoDeferimento", $arrSituacaoDeferimento);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $cursoId = $this->params()->fromRoute("id", 0);

        if (!$cursoId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($cursoId);
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAcadCurso = new \Matricula\Service\AcadCurso($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceAcadCurso->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceAcadCurso->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}