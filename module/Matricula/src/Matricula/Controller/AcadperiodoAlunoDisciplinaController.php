<?php


namespace Matricula\Controller;

use Matricula\Entity\AcadperiodoAlunoDisciplina;
use VersaSpine\Controller\AbstractCoreController;

class AcadperiodoAlunoDisciplinaController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {
    }

    public function buscaDisciplinasAlunoPeriodoAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            if (!empty($data['idAlunoPeriodo'])) {
                $service = new \Matricula\Service\AcadperiodoAlunoDisciplina($this->getEntityManager());

                $alunoPeriodo = $this->services()->getService('Matricula\Service\AcadperiodoAluno')->findOneBy(
                    ['alunoperId' => $data['idAlunoPeriodo']]
                );

                $disciplinas    = $service->buscaDisciplinasAlunoPeriodo($data['idAlunoPeriodo']);
                $tiposTurma     = \Matricula\Service\AcadgeralTurmaTipo::returnOptions();
                $turmaPrincipal = $alunoPeriodo->getTurma()->getTurmaId();

                $this->json->setVariable("tiposTurma", $tiposTurma);

                if ($turmaPrincipal) {
                    $this->json->setVariable("turmaPrincipal", $turmaPrincipal);
                }

                if ($disciplinas) {
                    $this->json->setVariable("disciplinas", $disciplinas);
                } else {
                    $this->json->setVariable("disciplinas", 0);
                }
            }
        }

        return $this->getJson();
    }

    public function buscaDisciplinasAlunoPerAction()
    {
        $requisicao = $this->getRequest();
        $service    = new \Matricula\Service\AcadperiodoAlunoDisciplina($this->getEntityManager());

        if ($requisicao->isPost()) {
            $dados = $requisicao->getPost()->toArray();

            $disciplinas = $service->retornaDisciplinasAlunoPer($dados['alunoPerId']);

            $this->json->setVariable("disciplinas", $disciplinas);
        }

        return $this->json;
    }

    public function situacaoAlunoNaDisciplina()
    {
        $objAluno = new AcadperiodoAlunoDisciplina();
    }
}
