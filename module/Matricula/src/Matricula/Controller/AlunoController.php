<?php
namespace Matricula\Controller;

use VersaSpine\Controller\AbstractCoreController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class AlunoController extends AbstractCoreController
{
    /**
     * @var ViewModel
     */
    public $view;

    /**
     * @var JsonModel
     */
    public $json;

    private $em;

    public function __construct()
    {
        parent::__construct(__CLASS__);
        $this->view = new ViewModel();
        $this->json = new JsonModel();
    }

    protected function getEm()
    {
        if (!$this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }

        return $this->em;
    }

    public function indexAction()
    {
        $serviceAcadperiodoAlunoResumo = new \Matricula\Service\AcadperiodoAlunoResumo($this->getEm());
        $serviceSituacoes              = new \Matricula\Service\AcadgeralSituacao($this->getEm());
        $serviceAcessoPessoas          = new \Acesso\Service\AcessoPessoas($this->getEntityManager());

        $arrResalunoSituacao = $serviceAcadperiodoAlunoResumo->getArrSelect2ResalunoSituacao();
        $arrSituacoes        = $serviceSituacoes->pesquisaForJson(['situacoesPermitidasAlteracao' => true]);

        $permissaoEditaSituacaoMatricula = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Matricula\Controller\AcadperiodoAluno',
            'altera-situacao-aluno'
        );

        $this->view->setVariables(
            [
                'arrResalunoSituacao'    => $arrResalunoSituacao,
                'arrSituacoes'           => $arrSituacoes,
                'permitirEditarSituacao' => $permissaoEditaSituacaoMatricula
            ]
        );

        $alunocursoId = $this->params()->fromRoute("id", 0);

        if ($alunocursoId) {
            $serviceAcadgeralAlunoCurso = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
            $arrAlunos                  = $serviceAcadgeralAlunoCurso->pesquisaForJson(['pesquisa' => $alunocursoId]);

            foreach ($arrAlunos as $arrAluno) {
                if (ltrim($arrAluno['alunocursoId'], '0') == ltrim($alunocursoId, '0')) {
                    $this->view->setVariable('arrAluno', $arrAluno);
                    break;
                }
            }
        }

        return $this->view;
    }

    public function listagemAlunosAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $arrayData = $request->getPost()->toArray();

            $serviceAluno = new \Matricula\Service\Aluno($this->getEm(), $this->getServiceManager()->get('Config'));
            $result       = $serviceAluno->paginationAjax($arrayData);

            $this->json->setVariable("draw", $result["draw"]);
            $this->json->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->json->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->json->setVariable("data", $result["data"]);
        }

        return $this->json;
    }

    public function editaNotasAction()
    {
        $alunoperId = $this->params()->fromRoute('id');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $arrayData                    = $request->getPost()->toArray();
            $serviceAcadperiodoEtapaAluno = $this->getServiceLocator()->get('Professor\Service\AcadperiodoEtapaAluno');

            $serviceAcadperiodoEtapaAluno->editaNotasAluno($arrayData);

            $this->view->setTemplate('matricula/notas/dados-aluno');

            return $this->redirect()->toRoute(
                'matricula/default',
                array('controller' => 'notas', 'action' => 'dados-aluno', 'id' => $alunoperId)
            );
        }
    }

    public function finalizaNotasAction()
    {
        $serviceAcadperiodoEtapaAluno = $this->getServiceLocator()->get('Professor\Service\AcadperiodoEtapaAluno');
        $disciplinaNotas              = $serviceAcadperiodoEtapaAluno->buscaNotaTotalAlunosDisciplinas();
    }

    /**
     * Action para atualizar e criar repostas da integradora de alunos,
     * recebe via post os dados a serem atualizados
     *
     * @return JsonModel
     */
    public function updateGabaritoIntegradoraAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $arrDataRequest = $request->getPost()->toArray();

            if ($arrDataRequest['integradoraPrincipal']) {
                $srvIntegradoraAluno = new \Matricula\Service\AcadperiodoIntegradoraAluno($this->getEm());
                $arrData             = array();

                // cria um array de dados para cada gabarito de disciplina
                foreach ($arrDataRequest['respostas'] as $pos => $resposta) {
                    $integradora = $arrDataRequest['integradoras'][$pos];
                    $integradora = ($integradora) ? $integradora : $arrDataRequest['integradoraPrincipal'];
                    $arrData[]   = [
                        'integradora'         => $integradora,
                        'alunointegId'        => $arrDataRequest['respostasId'][$pos],
                        'alunodisc'           => $arrDataRequest['alunoDisc'][$pos],
                        'alunointegRespostas' => strtoupper($resposta),
                        'alunointegAcertos'   => $this->calculaAcertosAlunoIntegradora(
                            strtoupper($resposta),
                            $arrDataRequest['gabarito'][$pos],
                            $arrDataRequest['gabaritoValidate'][$pos]
                        )
                    ];
                }

                try {
                    $result = $srvIntegradoraAluno->save($arrData);

                    // identifica as respostas de disciplinas salvas para retornar para a requisição
                    $saveds = array();
                    foreach ($result as $r) {
                        $alunoDiscId = $r->getAlunodisc()->getAlunodiscId();
                        $saveds[]    = [
                            'alunoDisc'    => $alunoDiscId,
                            'alunoIntegId' => $r->getAlunointegId()
                        ];
                    }

                    $this->json->setVariable('saveds', $saveds);
                    $this->json->setVariable('type', 'success');
                    $this->json->setVariable('message', 'Informações salvas com sucesso!');
                } catch (\Exception $ex) {
                    $this->json->setVariable('type', 'error');
                    $this->json->setVariable(
                        'message',
                        "Ocorreu um erro inesperado durante a execução da funcionalidade, tente novamente, caso persista contate o suporte"
                    );
                }
            } else {
                $this->json->setVariable('type', 'error');
                $this->json->setVariable('message', "Não existem registros de integradora para este Aluno");
            }
        }

        return $this->json;
    }

    /**
     * @param $respostasAluno : repostas do aluno na disciplina
     * @param $respostasGabarito : repostas corretas do gabarito
     * @param $gabaritoValidate : informações de cada questão, se a mesma é valida ('Sim':Válida, diferente de 'SIM': Inválida)
     * @return int : quantidade de acertos na prova
     */
    private function calculaAcertosAlunoIntegradora($respostasAluno, $respostasGabarito, $gabaritoValidate)
    {
        $acertos = 0;

        if (is_string($respostasAluno)) {
            $respostasAluno = str_split($respostasAluno);
        }

        if (is_string($respostasGabarito)) {
            $respostasGabarito = str_split($respostasGabarito);
        }

        if (is_string($gabaritoValidate)) {
            $gabaritoValidate = str_split($gabaritoValidate);
        }

        foreach ($respostasAluno as $pos => $resp) {
            if ($respostasGabarito[$pos] == $resp) {
                $acertos++;
            } elseif ($gabaritoValidate[$pos] != 'Sim') {
                $acertos++;
            }
        }

        return $acertos;
    }

    public function verificacaoDesligamentoAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $data = $request->getPost()->toArray();

            $serviceAluno = new \Matricula\Service\Aluno($this->getEm(), $this->getServiceManager()->get('Config'));
            $result       = $serviceAluno->desligamentoAlunoPendencias($data);

            $this->json->setVariables($result);
        }

        return $this->json;
    }

    public function desligamentoAction()
    {
        $request  = $this->getRequest();
        $erro     = false;
        $mensagem = '';

        if ($request->isPost()) {
            $data = $request->getPost()->toArray();

            $serviceAluno = new \Matricula\Service\Aluno($this->getEm(), $this->getServiceManager()->get('Config'));
            $desligado    = $serviceAluno->desligamentoAluno($data);

            if (!$desligado) {
                $mensagem = "Falha ao efetuar desligamento do aluno.";
            } else {
                $mensagem = "Aluno desligado com sucesso.";
            }

            $this->json->setVariable('erro', $erro);
            $this->json->setVariable('mensagem', $mensagem);

            return $this->json;
        }
    }

    public function historicoRemoverAction()
    {
        $request  = $this->getRequest();
        $erro     = false;
        $mensagem = '';

        if ($request->isPost()) {
            $data = $request->getPost()->toArray();

            $serviceAlunoResumo = new \Matricula\Service\AcadperiodoAlunoResumo($this->getEm());

            if (!$serviceAlunoResumo->remover($data)) {
                $erro     = true;
                $mensagem = $serviceAlunoResumo->getLastError();
            } else {
                $mensagem = "Item de histórico do aluno removido com sucesso.";
            }

            $this->json->setVariable('erro', $erro);
            $this->json->setVariable('mensagem', $mensagem);

            return $this->json;
        }
    }

    /**
     * Action para buscar o histórico escolar do aluno
     *
     * @return JsonModel
     **/
    public function buscaHistoricoEscolarAction()
    {
        $request      = $this->getRequest();
        $arrResultado = array();

        if ($request->isPost()) {
            $data = $request->getPost()->toArray();

            $serviceAluno = new \Matricula\Service\AcadperiodoAlunoResumo($this->getEm());
            $arrResultado = $serviceAluno->historicoAluno($data);
        }

        $this->json->setVariable("draw", $arrResultado["draw"]);
        $this->json->setVariable("recordsTotal", $arrResultado["recordsTotal"]);
        $this->json->setVariable("recordsFiltered", $arrResultado["recordsFiltered"]);
        $this->json->setVariable("data", $arrResultado["data"]);

        return $this->json;
    }

    public function salvaHistoricoEscolarAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $data                  = $request->getPost()->toArray();
            $data['equivalencias'] = $data['equivalencias'] ? explode(',', $data['equivalencias']) : array();

            $srvAlunoResumo = new \Matricula\Service\AcadperiodoAlunoResumo($this->getEm());
            $result         = $srvAlunoResumo->save($data);

            if (is_object($result)) {
                $result = ['type' => 'success', 'message' => 'Item de histórico do aluno salvo com suceso'];
            }
            $this->json->setVariable('result', $result);
        }

        return $this->json;
    }

    public function dataColacaoAction()
    {
        $serviceAlunoCurso = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());

        $request = $this->getRequest();

        if ($request->isPost()) {
            $rawData = $this->params()->fromPost('dataDaColacao');

            $rData = $serviceAlunoCurso->formatDateAmericano($rawData);

            $data = str_replace('-', '', $rData);

            $alunocursoId = $this->params()->fromPost('alunocursoId');

            $datac = $serviceAlunoCurso->addDataColacao($alunocursoId, $data);

            if ($datac == 1) {
                $result = ['type' => 'success', 'message' => 'Data da colação do aluno adicionada com sucesso'];
                $this->json->setVariable('result', $result);
            } elseif ($datac == 2) {
                $result = ['type' => 'info', 'message' => 'Data da colação do aluno atualizada com sucesso'];
                $this->json->setVariable('result', $result);
            } else {
                $result = ['type' => 'error', 'message' => 'Não foi possível adicionar ou atualizar a data da colação'];
                $this->json->setVariable('result', $result);
            }
        }

        return $this->json;
    }

    public function dataExpedicaoAction()
    {
        $serviceAlunoCurso = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());

        $request = $this->getRequest();

        if ($request->isPost()) {
            $rawData = $this->params()->fromPost('dataDaExpedicao');

            $rData = $serviceAlunoCurso->formatDateAmericano($rawData);

            $data = str_replace('-', '', $rData);

            $alunocursoId = $this->params()->fromPost('alunocursoId');

            $datae = $serviceAlunoCurso->addDataExpedicao($alunocursoId, $data);

            if ($datae == 1) {
                $result = [
                    'type'    => 'success',
                    'message' => 'Data da Expedição do diploma do aluno adicionada com sucesso'
                ];
                $this->json->setVariable('result', $result);
            } elseif ($datae == 2) {
                $result = ['type' => 'info', 'message' => 'Data da Expedição do diploma atualizada com sucesso'];
                $this->json->setVariable('result', $result);
            } else {
                $result = [
                    'type'    => 'error',
                    'message' => 'Não foi possível adicionar ou atualizar a data da expedição'
                ];
                $this->json->setVariable('result', $result);
            }
        }

        return $this->json;
    }

}

