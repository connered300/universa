<?php


namespace Matricula\Controller;

use Matricula\Service\AcadperiodoMatrizDisciplina;
use VersaSpine\Controller\AbstractCoreController;

class AcadperiodoMatrizDisciplinaController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {
        $request    = $this->getRequest();
        $paramsGet  = $request->getQuery()->toArray();
        $paramsPost = $request->getPost()->toArray();
        $param      = array_merge($paramsGet, $paramsPost);

        $serviceMatrizDisciplina = new AcadperiodoMatrizDisciplina($this->getEntityManager());

        $arrDados = $serviceMatrizDisciplina->pesquisaForJson($param);

        return new \Zend\View\Model\JsonModel($arrDados ? $arrDados : array());
    }

    public function buscaDisciplinasAction()
    {
        $requisicao = $this->getRequest();

        if ($requisicao->isPost()) {
            $dados                   = $requisicao->getPost()->toArray();
            $service                 = $this->services()->getService($this->getService());
            $serviceAcadPeriodoAluno = new \Matricula\Service\AcadperiodoAlunoDisciplina($this->getEntityManager());

            if ($dados['alunoPer']) {
                $disciplinas = $serviceAcadPeriodoAluno->retornaDisciplinasAlunoPer($dados);
            } else {
                $disciplinas =
                    $service->getRepository(
                        'Matricula\Entity\AcadperiodoMatrizDisciplina'
                    )->buscaDisciplinasPorMatrizCurricular($dados['matriz'], $dados['periodo']);
            }

            $this->json->setVariable("disciplinas", $disciplinas);

            return $this->json;
        }
    }

    public function buscaTurmasQueOfertamDisciplinaAction()
    {
        $requisicao = $this->getRequest();

        if ($requisicao->isPost()) {
            $dados   = $requisicao->getPost()->toArray();
            $service = $this->services()->getService($this->getService());
            $turmas  = $service->buscaMatrizesQueContenhamDisciplina($dados['disciplinasId'], $dados['periodo']);
            if (!empty($turmas)) {
                $this->json->setVariable("turmas", $turmas);
            }

            return $this->json;
        }
    }
}