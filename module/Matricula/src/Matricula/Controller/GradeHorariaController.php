<?php

namespace Matricula\Controller;

use Matricula\Service\GradeHoraria;
use VersaSpine\Controller\AbstractCoreController;

class GradeHorariaController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function indexAction()
    {
        return $this->view;
    }

    public function searchForJsonAction()
    {
        $request    = $this->getRequest();
        $paramsGet  = $request->getQuery()->toArray();
        $paramsPost = $request->getPost()->toArray();
        $service    = new \Matricula\Service\GradeHoraria($this->getEntityManager());

        $result = $service->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function searchForJsonTurmasAction()
    {
        $request      = $this->getRequest();
        $paramsGet    = $request->getQuery()->toArray();
        $paramsPost   = $request->getPost()->toArray();
        $service      = new \Matricula\Service\GradeHoraria($this->getEntityManager());

        $result = $service->pesquisaForJsonTurma(array_merge($paramsGet, $paramsPost), \Matricula\Service\AcadgeralTurmaTipo::CONVENCIONAL);

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function searchForJsonDisciplinasAction()
    {
        $request      = $this->getRequest();
        $paramsGet    = $request->getQuery()->toArray();
        $paramsPost   = $request->getPost()->toArray();
        $service      = new \Matricula\Service\GradeHoraria($this->getEntityManager());

        $result = $service->pesquisaForJsonDisciplina(
            array_merge($paramsGet, $paramsPost),
            \Matricula\Service\AcadgeralTurmaTipo::CONVENCIONAL
        );

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function searchForJsonPeriodoAction()
    {
        $request    = $this->getRequest();
        $paramsGet  = $request->getQuery()->toArray();
        $paramsPost = $request->getPost()->toArray();
        $service    = new \Matricula\Service\AcadperiodoLetivo($this->getEntityManager());

        $result = $service->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function searchForJsonCampusAction()
    {
        $request    = $this->getRequest();
        $paramsGet  = $request->getQuery()->toArray();
        $paramsPost = $request->getPost()->toArray();
        $service    = new \Matricula\Service\GradeHoraria($this->getEntityManager());

        $result = $service->pesquisaForJsonCampus(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function searchForJsonCursosAction()
    {
        $request    = $this->getRequest();
        $paramsGet  = $request->getQuery()->toArray();
        $paramsPost = $request->getPost()->toArray();
        $service    = new \Matricula\Service\GradeHoraria($this->getEntityManager());

        $result = $service->pesquisaForJsonCursos(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function salvarGradeAction()
    {
        $service = new \Matricula\Service\GradeHoraria($this->getEntityManager());
        $request = $this->getRequest();

        if ($request->isPost()) {
            $dados = $request->getPost()->toArray();
            $r     = $service->save($dados);

            if ($r) {
                $result['type']    = 'success';
                $result['message'] = 'Dados salvos com Sucesso!';
            }else{
                $result['type']    = 'error';
                $result['message'] = 'Insira pelo menos uma disciplina!';
            }
        }

        $this->json->setVariable('result', $result);

        return $this->json;
    }

    public function searchForJsonGradeDisciplinasAction()
    {
        $request    = $this->getRequest();
        $paramsGet  = $request->getQuery()->toArray();
        $paramsPost = $request->getPost()->toArray();
        $service    = new \Matricula\Service\GradeHoraria($this->getEntityManager());

        $result = $service->pesquisaForJsonGradeDisciplinas(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }
}