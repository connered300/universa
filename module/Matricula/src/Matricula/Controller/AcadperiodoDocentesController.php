<?php
namespace Matricula\Controller;

use VersaSpine\Controller\AbstractCoreController;
use Zend\View\Model\JsonModel;

class AcadperiodoDocentesController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
    }

    public function indexAction()
    {
        return $this->view;
    }

    public function searchAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $serviceDocente = new \Matricula\Service\AcadperiodoDocentes($this->getEntityManager());

            $data = $request->getPost()->toArray();

            $perId = $data['perId'];

            $result = $serviceDocente->search($data, $perId);

            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function getInfoDiarioAction()
    {
        $request = $this->getRequest();
        if ($this->getRequest()->isPost()) {
            $data = $request->getPost()->toArray();

            $serviceDocente = new \Matricula\Service\AcadperiodoDocentes($this->getEntityManager());
            $result         = $serviceDocente->getInfoDiario($data);

            $this->getJson()->setVariable('result', $result);
        }

        return $this->getJson();
    }

    public function saveInfoDiarioAction()
    {
        $request = $this->getRequest();
        if ($this->getRequest()->isPost()) {
            $serviceDocente = new \Matricula\Service\AcadperiodoDocentes($this->getEntityManager());
            $data           = $request->getPost()->toArray();
            $salvar         = false;

            if ($data['frequencias'] || $data['anotacoes'] || $data['encerramentoInfo']['encerramentoData']) {
                $salvar = true;

                $data = [
                    'docDisc'                     => $data['docdiscId'],
                    'diarioEntregaMes'            => $data['meses'],
                    'diarioEntregaId'             => $data['entregaIds'],
                    'diarioAnotacoesEntregaData'  => $data['anotacoes'],
                    'diarioFrequenciaEntregaData' => $data['frequencias'],
                    'diarioEncerramento'          => $data['encerramentoInfo'],
                    'periodo'                     => $data['periodo'],
                ];
            }

            if ($salvar) {
                $result = $serviceDocente->saveInfoDiario($data);
            } else {
                $result = ['type' => 'Warning', 'message' => "Nenhum dado vinculado para ser salvo!"];
            }

            $this->getJson()->setVariable('result', $result);
        }

        return $this->getJson();
    }

    public
    function getPeriodosAction()
    {
        $request    = $this->getRequest();
        $paramsGet  = $request->getQuery()->toArray();
        $paramsPost = $request->getPost()->toArray();
        $service    = new \Matricula\Service\AcadperiodoAluno($this->getEntityManager());

        $result = $service->buscaPeriodosExistentes(array_merge($paramsGet, $paramsPost));

        return new JsonModel($result ? $result : array());
    }
}
