<?php

namespace Matricula\Controller;

use VersaSpine\Controller\AbstractCoreController;

class AcadgeralCadastroOrigemController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request                        = $this->getRequest();
        $paramsGet                      = $request->getQuery()->toArray();
        $paramsPost                     = $request->getPost()->toArray();
        $serviceAcadgeralCadastroOrigem = new \Matricula\Service\AcadgeralCadastroOrigem($this->getEntityManager());

        $result = $serviceAcadgeralCadastroOrigem->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceAcadgeralCadastroOrigem = new \Matricula\Service\AcadgeralCadastroOrigem($this->getEntityManager());
        $serviceAcadgeralCadastroOrigem->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAcadgeralCadastroOrigem = new \Matricula\Service\AcadgeralCadastroOrigem($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceAcadgeralCadastroOrigem->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function editAction()
    {
        $origemId = $this->params()->fromRoute("id", 0);

        if (!$origemId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($origemId);
    }

    public function addAction($origemId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados                       = array();
        $serviceAcadgeralCadastroOrigem = new \Matricula\Service\AcadgeralCadastroOrigem($this->getEntityManager());

        if ($origemId) {
            $arrDados = $serviceAcadgeralCadastroOrigem->getArray($origemId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceAcadgeralCadastroOrigem->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de origem de cadastro salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceAcadgeralCadastroOrigem->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceAcadgeralCadastroOrigem->getLastError());
                }
            }
        }

        $serviceAcadgeralCadastroOrigem->formataDadosPost($arrDados);
        $serviceAcadgeralCadastroOrigem->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAcadgeralCadastroOrigem = new \Matricula\Service\AcadgeralCadastroOrigem($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceAcadgeralCadastroOrigem->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceAcadgeralCadastroOrigem->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}
?>