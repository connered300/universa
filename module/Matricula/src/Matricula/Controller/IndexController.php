<?php

namespace Matricula\Controller;

use VersaSpine\Controller\AbstractCoreController;

class IndexController extends AbstractCoreController
{
    public function __construct()
    {
    }

    public function indexAction()
    {
        return $this->getView();
    }

    public function liberarAlunosAction()
    {
        $result    = [];
        $request   = $this->getRequest();
        $arrConfig = $this->getServiceLocator()->get('config');

        $serviceAcadgeralAluno = new \Matricula\Service\AcadgeralAluno($this->getEntityManager(), $arrConfig);

        $arrDados = array_merge($request->getPost()->toArray(), $request->getQuery()->toArray());

        if (!$serviceAcadgeralAluno->liberaAlunos()) {
            $result['message'] = $serviceAcadgeralAluno->getLastError();
            $result['error']   = true;
        } else {
            $result['error']   = false;
            $result['message'] = "Atualizado com sucesso";
        }

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }
}
