<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AcadperiodoIntegradoraAvaliacao
 *
 * @ORM\Table(name="acadperiodo__integradora_avaliacao", indexes={@ORM\Index(name="fk_acadperiodo__integradora_avaliacao_acadperiodo__integrad_idx", columns={"integradora_id"})})
 * @ORM\Entity
 * @LG\LG(id="integavaliacao_id",label="integavaliacaoId")
 * @Jarvis\Jarvis(title="Integradora",icon="fa fa-table")
 */
class AcadperiodoIntegradoraAvaliacao
{
    /**
     * @var array
     * Está é uma variavel que armazena os valores que são aceitos pela $integavaliacaoTurno
     */
    static $tunos = [
        'Matutino' => 'Matutino',
        'Vespertino' => 'Vespertino',
        'Noturno' => 'Noturno',
    ];

    /**
     * @var integer
     *
     * @ORM\Column(name="integavaliacao_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="integavaliacao_id")
     * @LG\Labels\Attributes(text="Indice",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $integavaliacaoId;

    /**
     * @var integer
     *
     * @ORM\Column(name="integavaliacao_periodo", type="integer", nullable=false)
     * @LG\Labels\Property(name="integavaliacao_periodo")
     * @LG\Labels\Attributes(text="Período",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $integavaliacaoPeriodo;

    /**
     * @var string
     *
     * @ORM\Column(name="integavaliacao_turno", type="string", nullable=false)
     * @LG\Labels\Property(name="integavaliacao_turno")
     * @LG\Labels\Attributes(text="Turno",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $integavaliacaoTurno;


    /**
     * @LG\Labels\Property(name="integradora_status")
     * @LG\Labels\Attributes(text="Estado",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $integradoraStatus;

    /**
     * @var \AcadperiodoIntegradora
     *
     * @ORM\ManyToOne(targetEntity="AcadperiodoIntegradora")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="integradora_id", referencedColumnName="integradora_id")
     * })
     */
    private $integradora;

    /**
     * @return int
     */
    public function getIntegavaliacaoId()
    {
        return $this->integavaliacaoId;
    }

    /**
     * @param int $integavaliacaoId
     */
    public function setIntegavaliacaoId($integavaliacaoId)
    {
        $this->integavaliacaoId = $integavaliacaoId;
    }

    /**
     * @return int
     */
    public function getIntegavaliacaoPeriodo()
    {
        return $this->integavaliacaoPeriodo;
    }

    /**
     * @param int $integavaliacaoPeriodo
     */
    public function setIntegavaliacaoPeriodo($integavaliacaoPeriodo)
    {
        $this->integavaliacaoPeriodo = $integavaliacaoPeriodo;
    }

    /**
     * @return AcadperiodoIntegradora
     */
    public function getIntegradora()
    {
        return $this->integradora;
    }

    /**
     * @param AcadperiodoIntegradora $integradora
     */
    public function setIntegradora($integradora)
    {
        $this->integradora = $integradora;
    }

    /**
     * @return string
     */
    public function getIntegavaliacaoTurno()
    {
        return $this->integavaliacaoTurno;
    }

    /**
     * @param string $integavaliacaoTurno
     */
    public function setIntegavaliacaoTurno($integavaliacaoTurno)
    {
        $this->integavaliacaoTurno = $integavaliacaoTurno;
    }

    /**
     * @return mixed
     */
    public function getIntegradoraStatus()
    {
        return $this->integradoraStatus;
    }

    /**
     * @param mixed $integradoraStatus
     */
    public function setIntegradoraStatus($integradoraStatus)
    {
        $this->integradoraStatus = $integradoraStatus;
    }


    public function toArray()
    {
        return [
            'integavaliacaoId'      => $this->getIntegavaliacaoId(),
            'integavaliacaoPeriodo' => $this->getIntegavaliacaoPeriodo(),
            'integavaliacaoTurno'   => $this->getIntegavaliacaoTurno(),
            'integradoraStatus'     => $this->getIntegradoraStatus(),
            'integradora'           => $this->getIntegradora()->getIntegradoraId()
        ];
    }
}
