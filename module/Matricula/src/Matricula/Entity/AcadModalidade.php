<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadModalidade
 *
 * @ORM\Table(name="acad_modalidade")
 * @ORM\Entity
 */
class AcadModalidade
{
    /**
     * @var integer
     *
     * @ORM\Column(name="mod_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $modId;

    /**
     * @var string
     *
     * @ORM\Column(name="mod_nome", type="string", length=45, nullable=false)
     */
    private $modNome;

    /**
     * @var string
     *
     * @ORM\Column(name="mod_obs", type="text", nullable=true)
     */
    private $modObs;

    /**
     * @param array $data
     */
    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getModId()
    {
        return $this->modId;
    }

    /**
     * @param int $modId
     * @return AcadModalidade
     */
    public function setModId($modId)
    {
        $this->modId = $modId;

        return $this;
    }

    /**
     * @return string
     */
    public function getModNome()
    {
        return $this->modNome;
    }

    /**
     * @param string $modNome
     * @return AcadModalidade
     */
    public function setModNome($modNome)
    {
        $this->modNome = $modNome;

        return $this;
    }

    /**
     * @return string
     */
    public function getModObs()
    {
        return $this->modObs;
    }

    /**
     * @param string $modObs
     * @return AcadModalidade
     */
    public function setModObs($modObs)
    {
        $this->modObs = $modObs;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'modId'   => $this->getModId(),
            'modNome' => $this->getModNome(),
            'modObs'  => $this->getModObs(),
        ];
    }
}
