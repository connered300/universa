<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoAlunoDisciplina
 *
 * @ORM\Table(name="acadperiodo__aluno_disciplina", indexes={@ORM\Index(name="fk_matricula_aluno_periodo_disciplina_disciplina1_idx", columns={"disc_id"}), @ORM\Index(name="fk_matricula_aluno_periodo_disciplina_matricula_aluno_perio_idx", columns={"alunoper_id"}), @ORM\Index(name="fk_matricula_aluno_disciplina_periodo_turma1_idx", columns={"turma_id"}), @ORM\Index(name="fk_acadperiodo__aluno_disciplina_acadgeral__situacao1_idx", columns={"situacao_id"})})
 * @ORM\Entity
 */
class AcadperiodoAlunoDisciplina
{
    /**
     * @var integer
     *
     * @ORM\Column(name="alunodisc_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $alunodiscId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="alunodisc_sit_data", type="datetime", nullable=true)
     */
    private $alunodiscSitData;

    /**
     * @var \Matricula\Entity\AcadgeralSituacao
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadgeralSituacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="situacao_id", referencedColumnName="situacao_id")
     * })
     */
    private $situacao;

    /**
     * @var \Matricula\Entity\AcadperiodoTurma
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoTurma")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="turma_id", referencedColumnName="turma_id")
     * })
     */
    private $turma;

    /**
     * @var \Matricula\Entity\AcadgeralDisciplina
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadgeralDisciplina")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="disc_id", referencedColumnName="disc_id")
     * })
     */
    private $disc;

    /**
     * @var \Matricula\Entity\AcadperiodoAluno
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoAluno")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="alunoper_id", referencedColumnName="alunoper_id")
     * })
     */
    private $alunoper;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     *
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_criador", referencedColumnName="id")
     * })
     */
    private $usuarioCriador;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return string
     */
    public function getAlunodiscId()
    {
        return $this->alunodiscId;
    }

    /**
     * @param string $alunodiscId
     */
    public function setAlunodiscId($alunodiscId)
    {
        $this->alunodiscId = $alunodiscId;
    }

    /**
     * @return \DateTime
     */
    public function getAlunodiscSitData()
    {
        return $this->alunodiscSitData;
    }

    /**
     * @param \DateTime $alunodiscSitData
     */
    public function setAlunodiscSitData($alunodiscSitData)
    {
        $this->alunodiscSitData = $alunodiscSitData;
    }

    /**
     * @return \Matricula\Entity\AcadgeralSituacao
     */
    public function getSituacao()
    {
        return $this->situacao;
    }

    /**
     * @param \Matricula\Entity\AcadgeralSituacao $situacao
     */
    public function setSituacao($situacao)
    {
        $this->situacao = $situacao;
    }

    /**
     * @return \Matricula\Entity\AcadperiodoTurma
     */
    public function getTurma()
    {
        return $this->turma;
    }

    /**
     * @param \Matricula\Entity\AcadperiodoTurma $turma
     */
    public function setTurma($turma)
    {
        $this->turma = $turma;
    }

    /**
     * @return \Matricula\Entity\AcadgeralDisciplina
     */
    public function getDisc()
    {
        return $this->disc;
    }

    /**
     * @param \Matricula\Entity\AcadgeralDisciplina $disc
     */
    public function setDisc($disc)
    {
        $this->disc = $disc;
    }

    /**
     * @return \Matricula\Entity\AcadperiodoAluno
     */
    public function getAlunoper()
    {
        return $this->alunoper;
    }

    /**
     * @param \Matricula\Entity\AcadperiodoAluno $alunoper
     */
    public function setAlunoper($alunoper)
    {
        $this->alunoper = $alunoper;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas $usuarioCriador
     */
    public function getUsuarioCriador($usuarioCriador)
    {
        return $this->usuarioCriador;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuarioCriador
     */
    public function setUsuarioCriador($usuarioCriador)
    {
        $this->usuarioCriador = $usuarioCriador;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'alunodiscId'      => $this->getAlunodiscId(),
            'alunodisc'        => $this->getAlunodiscId(),
            'turma'            => $this->getTurma()->getTurmaId(),
            'alunodiscSitData' => $this->getAlunodiscSitData(),
            'alunoper'         => $this->getAlunoper()->getAlunoperId(),
            'disc'             => $this->getDisc()->getDiscId(),
            'situacao'         => $this->getSituacao(),
            'situacaoId'       => (
                is_object($this->getSituacao()) ? $this->getSituacao()->getSituacaoId() : $this->getSituacao()
            ),
            'usuarioCriador'   => $this->getUsuarioCriador(),
            'usuarioHistorico' => $this->getUsuarioCriador(),
        ];
    }

    /**
     * @return array
     */
    public function toArrayDirect()
    {
        return (new \VersaSpine\Stdlib\Hydrator())->extract($this);
    }

}
