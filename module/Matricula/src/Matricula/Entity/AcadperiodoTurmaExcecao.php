<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoTurmaExcecao
 *
 * @ORM\Table(name="acadperiodo__turma_excecao", indexes={@ORM\Index(name="fk_acadperiodo__turma_excecao_acadperiodo__turma1_idx", columns={"turma_id"})})
 * @ORM\Entity
 */
class AcadperiodoTurmaExcecao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="perexec_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $perexecId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="perexec_ano", type="date", nullable=false)
     */
    private $perexecAno;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="perexec_inicio", type="date", nullable=false)
     */
    private $perexecInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="perexec_fim", type="date", nullable=false)
     */
    private $perexecFim;

    /**
     * @var string
     *
     * @ORM\Column(name="perexec_descricao", type="string", length=45, nullable=true)
     */
    private $perexecDescricao;

    /**
     * @var \AcadperiodoTurma
     *
     * @ORM\ManyToOne(targetEntity="AcadperiodoTurma")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="turma_id", referencedColumnName="turma_id")
     * })
     */
    private $turma;
}
