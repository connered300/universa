<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadNivel
 *
 * @ORM\Table(name="acad_nivel", indexes={@ORM\Index(name="fk_acad_nivel_acad_nivel1_idx", columns={"nivel_pai"})})
 * @ORM\Entity
 */
class AcadNivel
{
    /**
     * @var integer
     *
     * @ORM\Column(name="nivel_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $nivelId;

    /**
     * @var string
     *
     * @ORM\Column(name="nivel_nome", type="string", length=45, nullable=false)
     */
    private $nivelNome;

    /**
     * @var string
     *
     * @ORM\Column(name="nivel_obs", type="text", nullable=true)
     */
    private $nivelObs;

    /**
     * @var AcadNivel
     *
     * @ORM\ManyToOne(targetEntity="AcadNivel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="nivel_pai", referencedColumnName="nivel_id")
     * })
     */
    private $nivelPai;

    /**
     * @param array $data
     */
    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getNivelId()
    {
        return $this->nivelId;
    }

    /**
     * @param int $nivelId
     * @return AcadNivel
     */
    public function setNivelId($nivelId)
    {
        $this->nivelId = $nivelId;

        return $this;
    }

    /**
     * @return string
     */
    public function getNivelNome()
    {
        return $this->nivelNome;
    }

    /**
     * @param string $nivelNome
     * @return AcadNivel
     */
    public function setNivelNome($nivelNome)
    {
        $this->nivelNome = $nivelNome;

        return $this;
    }

    /**
     * @return string
     */
    public function getNivelObs()
    {
        return $this->nivelObs;
    }

    /**
     * @param string $nivelObs
     * @return AcadNivel
     */
    public function setNivelObs($nivelObs)
    {
        $this->nivelObs = $nivelObs;

        return $this;
    }

    /**
     * @return AcadNivel
     */
    public function getNivelPai()
    {
        return $this->nivelPai;
    }

    /**
     * @param AcadNivel $nivelPai
     * @return AcadNivel
     */
    public function setNivelPai($nivelPai)
    {
        $this->nivelPai = $nivelPai;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'nivelId'   => $this->getNivelId(),
            'nivelNome' => $this->getNivelNome(),
            'nivelObs'  => $this->getNivelObs(),
            'nivelPai'  => $this->getNivelPai() ? $this->getNivelPai()->getNivelId() : null,
        ];
    }
}
