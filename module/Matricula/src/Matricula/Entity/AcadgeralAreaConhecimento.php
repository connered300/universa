<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AcadgeralAreaConhecimento
 *
 * @ORM\Table(name="acadgeral__area_conhecimento")
 * @ORM\Entity
 * @LG\LG(id="areaId",label="AreaDescricao")
 * @Jarvis\Jarvis(title="Listagem de área de conhecimento",icon="fa fa-table")
 */
class AcadgeralAreaConhecimento
{
    /**
     * @var integer
     *
     * @ORM\Column(name="area_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="area_id")
     * @LG\Labels\Attributes(text="código área")
     * @LG\Querys\Conditions(type="=")
     */
    private $areaId;
    /**
     * @var string
     *
     * @ORM\Column(name="area_descricao", type="string", nullable=false, length=45)
     * @LG\Labels\Property(name="area_descricao")
     * @LG\Labels\Attributes(text="descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $areaDescricao;
    /**
     * @var \Matricula\Entity\AcadgeralAreaConhecimento
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadgeralAreaConhecimento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="area_pai", referencedColumnName="area_id")
     * })
     */
    private $areaPai;

    /**
     * @return integer
     */
    public function getAreaId()
    {
        return $this->areaId;
    }

    /**
     * @param integer $areaId
     * @return AcadgeralAreaConhecimento
     */
    public function setAreaId($areaId)
    {
        $this->areaId = $areaId;

        return $this;
    }

    /**
     * @return string
     */
    public function getAreaDescricao()
    {
        return $this->areaDescricao;
    }

    /**
     * @param string $areaDescricao
     * @return AcadgeralAreaConhecimento
     */
    public function setAreaDescricao($areaDescricao)
    {
        $this->areaDescricao = $areaDescricao;

        return $this;
    }

    /**
     * @return \Matricula\Entity\AcadgeralAreaConhecimento
     */
    public function getAreaPai()
    {
        return $this->areaPai;
    }

    /**
     * @param \Matricula\Entity\AcadgeralAreaConhecimento $areaPai
     * @return AcadgeralAreaConhecimento
     */
    public function setAreaPai($areaPai)
    {
        $this->areaPai = $areaPai;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'areaId'        => $this->getAreaId(),
            'areaDescricao' => $this->getAreaDescricao(),
            'areaPai'       => $this->getAreaPai(),
        );

        $array['areaPai'] = $this->getAreaPai() ? $this->getAreaPai()->getAreaId() : null;

        return $array;
    }
}
