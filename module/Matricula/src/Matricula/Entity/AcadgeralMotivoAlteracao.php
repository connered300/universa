<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * MotivoAlteracao
 *
 * @ORM\Table(name="acadgeral__motivo_alteracao")
 * @ORM\Entity
 * @LG\LG(id="motivo_id")
 * @Jarvis\Jarvis(title="Motivo de alteração",icon="fa fa-table")
 */
class AcadgeralMotivoAlteracao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="motivo_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="motivo_id")
     * @LG\Labels\Attributes(text="código")
     * @LG\Querys\Conditions(type="=")
     */
    private $motivoId;

    /**
     * @var string
     *
     * @ORM\Column(name="motivo_descricao", type="string", nullable=false, length=245)
     * @LG\Labels\Property(name="motivo_descricao")
     * @LG\Labels\Attributes(text="descricao")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $motivoDescricao;

    /**
     * @var string
     *
     * @ORM\Column(name="motivo_situacao", type="string", nullable=false, length=10)
     * @LG\Labels\Property(name="motivo_situacao")
     * @LG\Labels\Attributes(text="situação do curso relacioada ao motivo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $motivoSituacao;

    /**
     * @return int
     */
    public function getMotivoId()
    {
        return $this->motivoId;
    }

    /**
     * @param int $motivoId
     * @return AcadgeralMotivoAlteracao
     */
    public function setMotivoId($motivoId)
    {
        $this->motivoId = $motivoId;

        return $this;
    }

    /**
     * @return string
     */
    public function getMotivoDescricao()
    {
        return $this->motivoDescricao;
    }

    /**
     * @param string $motivoDescricao
     * @return AcadgeralMotivoAlteracao
     */
    public function setMotivoDescricao($motivoDescricao)
    {
        $this->motivoDescricao = $motivoDescricao;

        return $this;
    }

    /**
     * @return string
     */
    public function getMotivoSituacao()
    {
        return $this->motivoSituacao;
    }

    /**
     * @param string $motivoSituacao
     * @return AcadgeralMotivoAlteracao
     */
    public function setMotivoSituacao($motivoSituacao)
    {
        if (is_array($motivoSituacao)) {
            $motivoSituacao = implode(',', $motivoSituacao);
        }

        $this->motivoSituacao = $motivoSituacao;

        return $this;
    }

    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'motivoId'        => $this->getMotivoId(),
            'motivoDescricao' => $this->getMotivoDescricao(),
            'motivoSituacao'  => $this->getMotivoSituacao(),
        );

        return $array;
    }

}