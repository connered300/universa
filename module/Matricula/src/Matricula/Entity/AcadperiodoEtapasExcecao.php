<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoEtapasExcecao
 *
 * @ORM\Table(name="acadperiodo__etapas_excecao", indexes={@ORM\Index(name="fk_acadperiodo__etapas_excecao_acadperiodo__turma_excecao1_idx", columns={"perexec_id"}), @ORM\Index(name="fk_acadperiodo__etapas_excecao_acadperiodo__etapas1_idx", columns={"etapa_id"})})
 * @ORM\Entity
 */
class AcadperiodoEtapasExcecao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="etapaexec_exec_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $etapaexecExecId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="etapaexec_data_inicio", type="date", nullable=false)
     */
    private $etapaexecDataInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="etapaexec_data_fim", type="date", nullable=false)
     */
    private $etapaexecDataFim;

    /**
     * @var string
     *
     * @ORM\Column(name="etapaexec_percentagem", type="string", length=45, nullable=true)
     */
    private $etapaexecPercentagem;

    /**
     * @var string
     *
     * @ORM\Column(name="etapaexec_ordem", type="string", length=45, nullable=true)
     */
    private $etapaexecOrdem;

    /**
     * @var \AcadperiodoEtapas
     *
     * @ORM\ManyToOne(targetEntity="AcadperiodoEtapas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="etapa_id", referencedColumnName="etapa_id")
     * })
     */
    private $etapa;

    /**
     * @var \AcadperiodoTurmaExcecao
     *
     * @ORM\ManyToOne(targetEntity="AcadperiodoTurmaExcecao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="perexec_id", referencedColumnName="perexec_id")
     * })
     */
    private $perexec;
}
