<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoIntegradoraGabarito
 *
 * @ORM\Table(name="acadperiodo__integradora_gabarito", indexes={@ORM\Index(name="fk_acadgeral__disciplina_acadperiodo__integradora_caderno_a_idx", columns={"integcaderno_id"}), @ORM\Index(name="fk_acadgeral__disciplina_acadperiodo__integradora_caderno_a_idx1", columns={"disc_id"})})
 * @ORM\Entity
 */
class AcadperiodoIntegradoraGabarito
{
    /**
     * @var integer
     *
     * @ORM\Column(name="gabarito_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $gabaritoId;

    /**
     * @var integer
     *
     * @ORM\Column(name="gabarito_ordem", type="integer", nullable=true)
     */
    private $gabaritoOrdem;

    /**
     * @var string
     *
     * @ORM\Column(name="gabarito_resposta", type="string", length=1, nullable=true)
     */
    private $gabaritoResposta;

    /**
     * @var string
     *
     * @ORM\Column(name="gabarito_valida", type="string", nullable=true)
     */
    private $gabaritoValida;

    /**
     * @var \AcadgeralDisciplina
     *
     * @ORM\ManyToOne(targetEntity="AcadgeralDisciplina")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="disc_id", referencedColumnName="disc_id")
     * })
     */
    private $disc;

    /**
     * @var \AcadperiodoIntegradoraCaderno
     *
     * @ORM\ManyToOne(targetEntity="AcadperiodoIntegradoraCaderno")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="integcaderno_id", referencedColumnName="integcaderno_id")
     * })
     */
    private $integcaderno;

    /**
     * @return int
     */
    public function getGabaritoId()
    {
        return $this->gabaritoId;
    }

    /**
     * @param int $gabaritoId
     */
    public function setGabaritoId($gabaritoId)
    {
        $this->gabaritoId = $gabaritoId;
    }

    /**
     * @return int
     */
    public function getGabaritoOrdem()
    {
        return $this->gabaritoOrdem;
    }

    /**
     * @param int $gabaritoOrdem
     */
    public function setGabaritoOrdem($gabaritoOrdem)
    {
        $this->gabaritoOrdem = $gabaritoOrdem;
    }

    /**
     * @return string
     */
    public function getGabaritoResposta()
    {
        return $this->gabaritoResposta;
    }

    /**
     * @param string $gabaritoResposta
     */
    public function setGabaritoResposta($gabaritoResposta)
    {
        $this->gabaritoResposta = $gabaritoResposta;
    }

    /**
     * @return string
     */
    public function getGabaritoValida()
    {
        return $this->gabaritoValida;
    }

    /**
     * @param string $gabaritoValida
     */
    public function setGabaritoValida($gabaritoValida)
    {
        $this->gabaritoValida = $gabaritoValida;
    }

    /**
     * @return \AcadgeralDisciplina
     */
    public function getDisc()
    {
        return $this->disc;
    }

    /**
     * @param \AcadgeralDisciplina $disc
     */
    public function setDisc($disc)
    {
        $this->disc = $disc;
    }

    /**
     * @return \AcadperiodoIntegradoraCaderno
     */
    public function getIntegcaderno()
    {
        return $this->integcaderno;
    }

    /**
     * @param \AcadperiodoIntegradoraCaderno $integcaderno
     */
    public function setIntegcaderno($integcaderno)
    {
        $this->integcaderno = $integcaderno;
    }
}
