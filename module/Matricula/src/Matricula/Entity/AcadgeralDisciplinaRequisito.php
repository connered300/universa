<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AcadgeralDisciplinaRequisito
 *
 * @ORM\Table(name="acadgeral__disciplina_requisito", indexes={@ORM\Index(name="fk_disciplina_disciplina_disciplina2_idx", columns={"discreq_destino"}), @ORM\Index(name="fk_disciplina_disciplina_disciplina1_idx", columns={"discreq_origem"})})
 * @ORM\Entity
 * @LG\LG(id="discreqId",label="discreq_id")
 * @Jarvis\Jarvis(title="Equivalencia",icon="fa fa-table")
 */
class AcadgeralDisciplinaRequisito
{
    /**
     * @var integer
     *
     * @ORM\Column(name="discreq_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $discreqId;

    /**
     * @var string
     *
     * @ORM\Column(name="discreq_tipo", type="string", nullable=false)
     */
    private $discreqTipo;


    /**
     * @var \Matricula\Entity\AcadgeralDisciplinaCurso
     *
     * @ORM\ManyToOne(targetEntity="AcadgeralDisciplinaCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="discreq_origem", referencedColumnName="disc_curso_id")
     * })
     */
    private $discreqOrigem;

    /**
     * @var \Matricula\Entity\AcadgeralDisciplinaCurso
     *
     * @ORM\ManyToOne(targetEntity="AcadgeralDisciplinaCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="discreq_destino", referencedColumnName="disc_curso_id")
     * })
     */
    private $discreqDestino;


    public function __construct( $data = array() ) {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getDiscreqId()
    {
        return $this->discreqId;
    }

    /**
     * @param int $discreqId
     */
    public function setDiscreqId($discreqId)
    {
        $this->discreqId = $discreqId;
    }

    /**
     * @return string
     */
    public function getDiscreqTipo()
    {
        return $this->discreqTipo;
    }

    /**
     * @param string $discreqTipo
     */
    public function setDiscreqTipo($discreqTipo)
    {
        $this->discreqTipo = $discreqTipo;
    }

    /**
     * @return \Matricula\Entity\AcadgeralDisciplinaCurso
     */
    public function getDiscreqDestino()
    {
        return $this->discreqDestino;
    }

    /**
     * @return \Matricula\Entity\AcadgeralDisciplinaCurso
     */
    public function getDiscreqOrigem()
    {
        return $this->discreqOrigem;
    }

    /**
     * @param \Matricula\Entity\AcadgeralDisciplinaCurso $discreqOrigem
     */
    public function setDiscreqOrigem($discreqOrigem)
    {
        $this->discreqOrigem = $discreqOrigem;
    }

    /**
     * @param \Matricula\Entity\AcadgeralDisciplinaCurso $discreqDestino
     */
    public function setDiscreqDestino($discreqDestino)
    {
        $this->discreqDestino = $discreqDestino;
    }

}
