<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AcadgeralDocente
 *
 * @ORM\Table(name="acadgeral__docente", indexes={@ORM\Index(name="fk_acadgeral__docente_pessoa_fisica1_idx", columns={"pes_id"})})
 * @ORM\Entity(repositoryClass="Matricula\Entity\Repository\AcadgeralDocente")
 * @LG\LG(id="docenteId",label="PesId")
 * @Jarvis\Jarvis(title="Listagem de docente",icon="fa fa-table")
 */
class AcadgeralDocente
{
    /**
     * @var integer
     *
     * @ORM\Column(name="docente_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="docente_id")
     * @LG\Labels\Attributes(text="c�digo docente")
     * @LG\Querys\Conditions(type="=")
     */
    private $docenteId;

    /**
     * @var \Pessoa\Entity\PessoaFisica
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\PessoaFisica")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     */
    private $pes;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="docente_data_inicio", type="datetime", nullable=false)
     * @LG\Labels\Property(name="docente_data_inicio")
     * @LG\Labels\Attributes(text="in�cio data")
     * @LG\Querys\Conditions(type="=")
     */
    private $docenteDataInicio;

    /**
     * @var string
     *
     * @ORM\Column(name="docente_ativo", type="string", nullable=false, length=3)
     * @LG\Labels\Property(name="docente_ativo")
     * @LG\Labels\Attributes(text="ativo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $docenteAtivo = 'Sim';

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="docente_data_fim", type="datetime", nullable=true)
     * @LG\Labels\Property(name="docente_data_fim")
     * @LG\Labels\Attributes(text="fim data")
     * @LG\Querys\Conditions(type="=")
     */
    private $docenteDataFim;

    /**
     * @var string
     *
     * @ORM\Column(name="docente_cur_lattes", type="string", nullable=true, length=255)
     * @LG\Labels\Property(name="docente_cur_lattes")
     * @LG\Labels\Attributes(text="lattes curso")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $docenteCurLattes;

    /**
     * @var integer
     *
     * @ORM\Column(name="docente_num_mec", type="integer", nullable=true, length=11)
     * @LG\Labels\Property(name="docente_num_mec")
     * @LG\Labels\Attributes(text="MEC n�mero")
     * @LG\Querys\Conditions(type="=")
     */
    private $docenteNumMec;

    /**
     * @var string
     *
     * @ORM\Column(name="docente_tituto", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="docente_tituto")
     * @LG\Labels\Attributes(text="tituto")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $docenteTituto;

    /**
     * @return integer
     */
    public function getDocenteId()
    {
        return $this->docenteId;
    }

    /**
     * @param integer $docenteId
     * @return AcadgeralDocente
     */
    public function setDocenteId($docenteId)
    {
        $this->docenteId = $docenteId;

        return $this;
    }

    /**
     * @return \Pessoa\Entity\PessoaFisica
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @param \Pessoa\Entity\PessoaFisica $pes
     * @return AcadgeralDocente
     */
    public function setPes($pes)
    {
        $this->pes = $pes;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getDocenteDataInicio($format = false)
    {
        $docenteDataInicio = $this->docenteDataInicio;

        if ($format && $docenteDataInicio) {
            $docenteDataInicio = $docenteDataInicio->format('d/m/Y H:i:s');
        }

        return $docenteDataInicio;
    }

    /**
     * @param \Datetime $docenteDataInicio
     * @return AcadgeralDocente
     */
    public function setDocenteDataInicio($docenteDataInicio)
    {
        if ($docenteDataInicio) {
            if (is_string($docenteDataInicio)) {
                $docenteDataInicio = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $docenteDataInicio
                );
                $docenteDataInicio = new \Datetime($docenteDataInicio);
            }
        } else {
            $docenteDataInicio = null;
        }
        $this->docenteDataInicio = $docenteDataInicio;

        return $this;
    }

    /**
     * @return string
     */
    public function getDocenteAtivo()
    {
        return $this->docenteAtivo;
    }

    /**
     * @param string $docenteAtivo
     * @return AcadgeralDocente
     */
    public function setDocenteAtivo($docenteAtivo)
    {
        $this->docenteAtivo = $docenteAtivo;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getDocenteDataFim($format = false)
    {
        $docenteDataFim = $this->docenteDataFim;

        if ($format && $docenteDataFim) {
            $docenteDataFim = $docenteDataFim->format('d/m/Y H:i:s');
        }

        return $docenteDataFim;
    }

    /**
     * @param \Datetime $docenteDataFim
     * @return AcadgeralDocente
     */
    public function setDocenteDataFim($docenteDataFim)
    {
        if ($docenteDataFim) {
            if (is_string($docenteDataFim)) {
                $docenteDataFim = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $docenteDataFim
                );
                $docenteDataFim = new \Datetime($docenteDataFim);
            }
        } else {
            $docenteDataFim = null;
        }
        $this->docenteDataFim = $docenteDataFim;

        return $this;
    }

    /**
     * @return string
     */
    public function getDocenteCurLattes()
    {
        return $this->docenteCurLattes;
    }

    /**
     * @param string $docenteCurLattes
     * @return AcadgeralDocente
     */
    public function setDocenteCurLattes($docenteCurLattes)
    {
        $this->docenteCurLattes = $docenteCurLattes;

        return $this;
    }

    /**
     * @return integer
     */
    public function getDocenteNumMec()
    {
        return $this->docenteNumMec;
    }

    /**
     * @param integer $docenteNumMec
     * @return AcadgeralDocente
     */
    public function setDocenteNumMec($docenteNumMec)
    {
        $this->docenteNumMec = $docenteNumMec;

        return $this;
    }

    /**
     * @return string
     */
    public function getDocenteTituto()
    {
        return $this->docenteTituto;
    }

    /**
     * @param string $docenteTituto
     * @return AcadgeralDocente
     */
    public function setDocenteTituto($docenteTituto)
    {
        $this->docenteTituto = $docenteTituto;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'docenteId'         => $this->getDocenteId(),
            'pes'               => $this->getPes(),
            'docenteDataInicio' => $this->getDocenteDataInicio(true),
            'docenteAtivo'      => $this->getDocenteAtivo(),
            'docenteDataFim'    => $this->getDocenteDataFim(true),
            'docenteCurLattes'  => $this->getDocenteCurLattes(),
            'docenteNumMec'     => $this->getDocenteNumMec(),
            'docenteTituto'     => $this->getDocenteTituto(),
        );

        $array['pes'] = $this->getPes() ? $this->getPes()->getPes()->getPesId() : null;

        return $array;
    }
}