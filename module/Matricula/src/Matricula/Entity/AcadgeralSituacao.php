<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AcadgeralSituacao
 *
 * @ORM\Table(name="acadgeral__situacao")
 * @ORM\Entity(repositoryClass="Matricula\Entity\Repository\AcadgeralSituacao")
 * @LG\LG(id="situacao_id",label="matsit_descricao")
 */
class AcadgeralSituacao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="situacao_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $situacaoId;

    /**
     * @var string
     *
     * @ORM\Column(name="matsit_descricao", type="string", length=45, nullable=false)
     */
    private $matsitDescricao;

    /**
     * @var string
     *
     * @ORM\Column(name="situacao_regular", type="string", length=3, nullable=false)
     */
    private $matsitRegular;

    public function __construct(array $data)
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getSituacaoId()
    {
        return $this->situacaoId;
    }

    /**
     * @param int $situacaoId
     */
    public function setSituacaoId($situacaoId)
    {
        $this->situacaoId = $situacaoId;
    }

    /**
     * @return string
     */
    public function getMatsitDescricao()
    {
        return $this->matsitDescricao;
    }

    /**
     * @param string $matsitDescricao
     */
    public function setMatsitDescricao($matsitDescricao)
    {
        $this->matsitDescricao = $matsitDescricao;
    }

    /**
     * @return string
     */
    public function getMatsitRegular()
    {
        return $this->matsitRegular;
    }

    /**
     * @param string $matsitRegular
     * @return AcadgeralSituacao
     */
    public function setMatsitRegular($matsitRegular)
    {
        $this->matsitRegular = $matsitRegular;

        return $this;
    }

    public function toArray()
    {
        return [
            'situacaoId'      => $this->getSituacaoId(),
            'matsitDescricao' => $this->getMatsitDescricao(),
            'matsitRegular'   => $this->getMatsitRegular()
        ];
    }
}
