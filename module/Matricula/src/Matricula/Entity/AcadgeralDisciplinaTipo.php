<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AcadgeralDisciplinaTipo
 *
 * @ORM\Table(name="acadgeral__disciplina_tipo")
 * @ORM\Entity
 * @LG\LG(id="tdisc_id",label="tdisc_descricao")
 * @Jarvis\Jarvis(title="Tipos de Disciplina",icon="fa fa-table")
 */
class AcadgeralDisciplinaTipo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="tdisc_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $tdiscId;

    /**
     * @var string
     *
     * @ORM\Column(name="tdisc_descricao", type="string", length=45, nullable=false)
     */
    private $tdiscDescricao;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tdisc_avaliacao", type="string", nullable=true)
     */
    private $tdiscAvaliacao;


    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getTdiscId()
    {
        return $this->tdiscId;
    }

    /**
     * @param int $tdiscId
     */
    public function setTdiscId($tdiscId)
    {
        $this->tdiscId = $tdiscId;
    }

    /**
     * @return string
     */
    public function getTdiscDescricao()
    {
        return $this->tdiscDescricao;
    }

    /**
     * @param string $tdiscDescricao
     */
    public function setTdiscDescricao($tdiscDescricao)
    {
        $this->tdiscDescricao = $tdiscDescricao;
    }

    /**
     * @return null|string
     */
    public function getTdiscAvaliacao()
    {
        return $this->tdiscAvaliacao;
    }

    /**
     * @param null|string $tdiscAvaliacao
     */
    public function setTdiscAvaliacao($tdiscAvaliacao)
    {
        $this->tdiscAvaliacao = $tdiscAvaliacao;
    }


    public function toArray()
    {
        return array(
            'tdiscId'        => $this->getTdiscId(),
            'tdiscDescricao' => $this->getTdiscDescricao(),
            'tdiscAvaliacao' => $this->getTdiscAvaliacao(),
        );
    }
}
