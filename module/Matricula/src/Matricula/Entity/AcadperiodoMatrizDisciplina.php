<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoMatrizDisciplina
 *
 * @ORM\Table(name="acadperiodo__matriz_disciplina", indexes={@ORM\Index(name="fk_matriz_periodo_disciplina_matriz_curricular1_idx", columns={"mat_cur_id"}), @ORM\Index(name="fk_matriz_periodo_disciplina_disciplina1_idx", columns={"disc_id"})})
 * @ORM\Entity(repositoryClass="Matricula\Entity\Repository\AcadperiodoMatrizDisciplina")
 */
class AcadperiodoMatrizDisciplina
{
    /**
     * @var integer
     *
     * @ORM\Column(name="per_disc_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $perDiscId;

    /**
     * @var integer
     *
     * @ORM\Column(name="per_disc_periodo", type="integer", nullable=false)
     */
    private $perDiscPeriodo;

    /**
     * @var float
     *
     * @ORM\Column(name="per_disc_chpratica", type="float", nullable=true)
     */
    private $perDiscChpratica;

    /**
     * @var float
     *
     * @ORM\Column(name="per_disc_chteorica", type="float", nullable=true)
     */
    private $perDiscChteorica;

    /**
     * @var float
     *
     * @ORM\Column(name="per_disc_chestagio", type="float", nullable=true)
     */
    private $perDiscChestagio;

    /**
     * @var integer
     *
     * @ORM\Column(name="per_disc_creditos", type="integer", nullable=true)
     */
    private $perDiscCreditos;

    /**
     * @var \Matricula\Entity\AcadgeralDisciplina
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadgeralDisciplina")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="disc_id", referencedColumnName="disc_id")
     * })
     */
    private $disc;

    /**
     * @var \Matricula\Entity\AcadperiodoMatrizCurricular
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoMatrizCurricular")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mat_cur_id", referencedColumnName="mat_cur_id")
     * })
     */
    private $matCur;

    public function __construct(array $data)
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getPerDiscId()
    {
        return $this->perDiscId;
    }

    /**
     * @param int $perDiscId
     */
    public function setPerDiscId($perDiscId)
    {
        $this->perDiscId = $perDiscId;
    }

    /**
     * @return int
     */
    public function getPerDiscPeriodo()
    {
        return $this->perDiscPeriodo;
    }

    /**
     * @param int $perDiscPeriodo
     */
    public function setPerDiscPeriodo($perDiscPeriodo)
    {
        $this->perDiscPeriodo = $perDiscPeriodo;
    }

    /**
     * @return \DateTime
     */
    public function getPerDiscChpratica()
    {
        return $this->perDiscChpratica;
    }

    /**
     * @param \DateTime $perDiscChpratica
     */
    public function setPerDiscChpratica($perDiscChpratica)
    {
        $this->perDiscChpratica = $perDiscChpratica;
    }

    /**
     * @return \DateTime
     */
    public function getPerDiscChteorica()
    {
        return $this->perDiscChteorica;
    }

    /**
     * @param \DateTime $perDiscChteorica
     */
    public function setPerDiscChteorica($perDiscChteorica)
    {
        $this->perDiscChteorica = $perDiscChteorica;
    }

    /**
     * @return \DateTime
     */
    public function getPerDiscChestagio()
    {
        return $this->perDiscChestagio;
    }

    /**
     * @param \DateTime $perDiscChestagio
     */
    public function setPerDiscChestagio($perDiscChestagio)
    {
        $this->perDiscChestagio = $perDiscChestagio;
    }

    /**
     * @return int
     */
    public function getPerDiscCreditos()
    {
        return $this->perDiscCreditos;
    }

    /**
     * @param int $perDiscCreditos
     */
    public function setPerDiscCreditos($perDiscCreditos)
    {
        $this->perDiscCreditos = $perDiscCreditos;
    }

    /**
     * @return \Matricula\Entity\AcadgeralDisciplina
     */
    public function getDisc()
    {
        return $this->disc;
    }

    /**
     * @param \Matricula\Entity\AcadgeralDisciplina $disc
     */
    public function setDisc($disc)
    {
        $this->disc = $disc;
    }

    /**
     * @return \Matricula\Entity\AcadperiodoMatrizCurricular
     */
    public function getMatCur()
    {
        return $this->matCur;
    }

    /**
     * @param \Matricula\Entity\AcadperiodoMatrizCurricular $matCur
     */
    public function setMatCur($matCur)
    {
        $this->matCur = $matCur;
    }

    public function toArray()
    {
        return array(
            'perDiscId'        => $this->getPerDiscId(),
            'disc'             => $this->getDisc()->getDiscId(),
            'perDiscPeriodo'   => $this->getPerDiscPeriodo(),
            'perDiscChpratica' => $this->getPerDiscChpratica(),
            'perDiscChteorica' => $this->getPerDiscChteorica(),
            'perDiscChestagio' => $this->getPerDiscChestagio(),
            'perDiscCreditos'  => $this->getPerDiscCreditos(),
            'matCur'           => $this->getMatCur()->getMatCurId()
        );
    }

}
