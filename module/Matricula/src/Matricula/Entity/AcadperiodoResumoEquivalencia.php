<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoResumoEquivalencia
 *
 * @ORM\Table(name="acadperiodo__resumo_equivalencia", indexes={@ORM\Index(name="fk_acadperiodo__resumo_equivalencia_acadgeral__aluno_formac_idx", columns={"formacao_id"}), @ORM\Index(name="fk_acadperiodo__resumo_equivalencia_acadperiodo__aluno_resu_idx", columns={"res_id"}), @ORM\Index(name="fk_acadperiodo__resumo_equivalencia_acadgeral__disciplina1_idx", columns={"disc_id"}), @ORM\Index(name="acadperiodo__resumo_equivalencia_unq", columns={"formacao_id", "res_id", "disc_id"})})
 * @ORM\Entity
 */
class AcadperiodoResumoEquivalencia
{
    /**
     * @var integer
     *
     * @ORM\Column(name="equivalencia_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $equivalenciaId;

    /**
     * @var \Matricula\Entity\AcadgeralAlunoFormacao
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadgeralAlunoFormacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="formacao_id", referencedColumnName="formacao_id")
     * })
     */
    private $formacao;

    /**
     * @var \Matricula\Entity\AcadgeralDisciplina
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadgeralDisciplina")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="disc_id", referencedColumnName="disc_id")
     * })
     */
    private $disc;

    /**
     * @var \Matricula\Entity\AcadperiodoAlunoResumo
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoAlunoResumo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="res_id", referencedColumnName="res_id")
     * })
     */
    private $res;

    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getEquivalenciaId()
    {
        return $this->equivalenciaId;
    }

    /**
     * @param int $equivalenciaId
     * @return AcadperiodoResumoEquivalencia
     */
    public function setEquivalenciaId($equivalenciaId)
    {
        $this->equivalenciaId = $equivalenciaId;

        return $this;
    }

    /**
     * @return AcadgeralAlunoFormacao
     */
    public function getFormacao()
    {
        return $this->formacao;
    }

    /**
     * @param AcadgeralAlunoFormacao $formacao
     * @return AcadperiodoResumoEquivalencia
     */
    public function setFormacao($formacao)
    {
        $this->formacao = $formacao;

        return $this;
    }

    /**
     * @return AcadgeralDisciplina
     */
    public function getDisc()
    {
        return $this->disc;
    }

    /**
     * @param AcadgeralDisciplina $disc
     * @return AcadperiodoResumoEquivalencia
     */
    public function setDisc($disc)
    {
        $this->disc = $disc;

        return $this;
    }

    /**
     * @return AcadperiodoAlunoResumo
     */
    public function getRes()
    {
        return $this->res;
    }

    /**
     * @param AcadperiodoAlunoResumo $res
     * @return AcadperiodoResumoEquivalencia
     */
    public function setRes($res)
    {
        $this->res = $res;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \VersaSpine\Stdlib\Hydrator())->extract($this);
    }
}

