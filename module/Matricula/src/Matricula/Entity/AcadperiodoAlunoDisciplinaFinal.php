<?php namespace Matricula\Entity;



use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoAlunoDisciplinaFinal
 *
 * @ORM\Table(name="acadperiodo__aluno_disciplina_final", indexes={@ORM\Index(name="fk_acadperiodo__aluno_disciplina_final_acadperiodo__aluno_d_idx", columns={"alunodisc_id"})})
 * @ORM\Entity(repositoryClass="Matricula\Entity\Repository\AcadperiodoAlunoDisciplinaFinal")
 */
class AcadperiodoAlunoDisciplinaFinal
{
    /**
     * @var integer
     *
     * @ORM\Column(name="alunofinal_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $alunofinalId;

    /**
     * @var float
     *
     * @ORM\Column(name="alunofinal_nota", type="float", precision=10, scale=0, nullable=true)
     */
    private $alunofinalNota;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="alunofinal_data_realizacao", type="datetime", nullable=true)
     */
    private $alunofinalDataRealizacao;

    /**
     * @var string
     *
     * @ORM\Column(name="alunofinal_resultado", type="string", nullable=true)
     */
    private $alunofinalResultado;

    /**
     * @var \Matricula\Entity\AcadperiodoAlunoDisciplina
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoAlunoDisciplina")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="alunodisc_id", referencedColumnName="alunodisc_id")
     * })
     */
    private $alunodisc;

    /**
     * @return int
     */
    public function getAlunofinalId()
    {
        return $this->alunofinalId;
    }

    /**
     * @param int $alunofinalId
     */
    public function setAlunofinalId($alunofinalId)
    {
        $this->alunofinalId = $alunofinalId;
    }

    /**
     * @return float
     */
    public function getAlunofinalNota()
    {
        return $this->alunofinalNota;
    }

    /**
     * @param float $alunofinalNota
     */
    public function setAlunofinalNota($alunofinalNota)
    {
        $this->alunofinalNota = $alunofinalNota;
    }

    /**
     * @return \DateTime
     */
    public function getAlunofinalDataRealizacao()
    {
        return $this->alunofinalDataRealizacao;
    }

    /**
     * @param \DateTime $alunofinalDataRealizacao
     */
    public function setAlunofinalDataRealizacao($alunofinalDataRealizacao)
    {
        $this->alunofinalDataRealizacao = $alunofinalDataRealizacao;
    }

    /**
     * @return string
     */
    public function getAlunofinalResultado()
    {
        return $this->alunofinalResultado;
    }

    /**
     * @param string $alunofinalResultado
     */
    public function setAlunofinalResultado($alunofinalResultado)
    {
        $this->alunofinalResultado = $alunofinalResultado;
    }

    /**
     * @return \Matricula\Entity\AcadperiodoAlunoDisciplina
     */
    public function getAlunodisc()
    {
        return $this->alunodisc;
    }

    /**
     * @param \Matricula\Entity\AcadperiodoAlunoDisciplina $alunodisc
     */
    public function setAlunodisc($alunodisc)
    {
        $this->alunodisc = $alunodisc;
    }
}
