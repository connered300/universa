<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AcadgeralDisciplinaCurso
 *
 * @ORM\Table(name="acadgeral__disciplina_curso", uniqueConstraints={@ORM\UniqueConstraint(name="index4", columns={"curso_id", "disc_id"})}, indexes={@ORM\Index(name="fk_disciplina_sugestao_curso_acad_curso1_idx", columns={"curso_id"}), @ORM\Index(name="fk_disciplina_sugestao_curso_disciplina1_idx", columns={"disc_id"})})
 * @ORM\Entity
 * @LG\LG(id="discCursoId",label="disc_curso_id")
 * @Jarvis\Jarvis(title="Disciplinas",icon="fa fa-table")
 */
class AcadgeralDisciplinaCurso
{
    /**
     * @var integer
     *
     * @ORM\Column(name="disc_curso_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="disc_curso_id")
     * @LG\Labels\Attributes(text="Índice",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $discCursoId;

    /**
     * @var integer
     *
     * @ORM\Column(name="disc_curs_sug_periodo", type="integer", nullable=true)
     */
    private $discCursSugPeriodo;

    /**
     * @var \Matricula\Entity\AcadCurso
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="curso_id", referencedColumnName="curso_id")
     * })
     */
    private $curso;

    /**
     * @var AcadgeralDisciplina
     *
     * @ORM\ManyToOne(targetEntity="AcadgeralDisciplina")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="disc_id", referencedColumnName="disc_id")
     * })
     */
    private $disc;

    /** @var string
     *
     * @ORM\Column(name="disc__nucleo_comum", type="string", length=45, nullable=false)
     * @LG\Labels\Property(name="disc__nucleo_comum")
     * @LG\Labels\Attributes(text="Disciplina de nucleo comum",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */

    private $nucleoComum = "Não";

    /**
     * @var string
     *
     * @ORM\Column(name="disc_ativo", type="string", nullable=false)
     * @LG\Labels\Property(name="disc_ativo")
     * @LG\Labels\Attributes(text="Estado",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE%")
     */
    private $discAtivo = 'Sim';

    /**
     * @var AcadgeralDisciplinaTipo
     *
     * @ORM\ManyToOne(targetEntity="AcadgeralDisciplinaTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tdisc_id", referencedColumnName="tdisc_id")
     * })
     */
    private $tdisc;
    
    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AcadgeralDisciplinaCurso", inversedBy="discequivOrigem")
     * @ORM\JoinTable(name="acadgeral__disciplina_equivalencia",
     *   joinColumns={
     *     @ORM\JoinColumn(name="discequiv_origem", referencedColumnName="disc_curso_id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="discequiv_destino", referencedColumnName="disc_curso_id")
     *   }
     * )
     */
    private $discequivDestino;
    
    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AcadgeralDisciplinaCurso", mappedBy="discequivDestino")
     * 
     */
    private $discequivOrigem;
    
    
    /**
     * @param array $data
     */
    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
        $this->discequivDestino = new \Doctrine\Common\Collections\ArrayCollection();
        $this->discequivOrigem = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return int
     */
    public function getDiscCursoId()
    {
        return $this->discCursoId;
    }

    /**
     * @param int $discCursoId
     */
    public function setDiscCursoId($discCursoId)
    {
        $this->discCursoId = $discCursoId;
    }

    /**
     * @return int
     */
    public function getDiscCursSugPeriodo()
    {
        return $this->discCursSugPeriodo;
    }

    /**
     * @param int $discCursSugPeriodo
     */
    public function setDiscCursSugPeriodo($discCursSugPeriodo)
    {
        $this->discCursSugPeriodo = $discCursSugPeriodo;
    }

    /**
     * @return \Matricula\Entity\AcadCurso
     */
    public function getCurso()
    {
        return $this->curso;
    }

    /**
     * @param \Matricula\Entity\AcadCurso $curso
     */
    public function setCurso($curso)
    {
        $this->curso = $curso;
    }

    /**
     * @return \Matricula\Entity\AcadgeralDisciplina
     */
    public function getDisc()
    {
        return $this->disc;
    }

    /**
     * @param \Matricula\Entity\AcadgeralDisciplina $discId
     */
    public function setDisc($disc)
    {
        $this->disc = $disc;
    }

    /**
     * @return string
     */
    public function getNucleo()
    {
        return $this->nucleoComum;
    }

    /**
     * @param $nucleo
     */
    public function setNucleo($nucleo)
    {
        $this->nucleoComum = $nucleo;
    }

    /**
     * @return AcadgeralDisciplinaTipo
     */
    public function getTdisc()
    {
        return $this->tdisc;
    }

    /**
     * @param AcadgeralDisciplinaTipo $tdisc
     */
    public function setTdisc($tdisc)
    {
        $this->tdisc = $tdisc;
    }

    /**
     * @return string
     */
    public function getDiscAtivo()
    {
        return $this->discAtivo;
    }

    /**
     * @param string $discAtivo
     */
    public function setDiscAtivo($discAtivo)
    {
        $this->discAtivo = ($discAtivo) ? $discAtivo : 'Sim';
    }
    
    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getDiscequivOrigem()
    {
        return $this->discequivOrigem;
    }

    /**
     * @param int $discequivOrigem
     */
    public function setDiscequivOrigem($discequivOrigem)
    {
        $this->discequivOrigem = $discequivOrigem;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getDiscequivDestino()
    {
        return $this->discequivDestino;
    }

    /**
     * @param int $discequivDestino
     */
    public function setDiscequivDestino($discequivDestino)
    {
        $this->discequivDestino = $discequivDestino;
    }

    /**
     * Autorelacionamento de many to many para adicionar
     * disciplina curso equivalente na tabela acadgeral__disciplina_equivalencia
     * @param \Doctrine\Common\Collections\ArrayCollection $discequivDestino
     */
    public function addDiscequivDestino(\Doctrine\Common\Collections\ArrayCollection $discequivDestino){
        foreach($discequivDestino as $cadaDiscequivDestino) {

            if( !$this->discequivDestino->contains($cadaDiscequivDestino)) {
                $this->discequivDestino->add($cadaDiscequivDestino);
                $cadaDiscequivDestino->addDiscequivOrigem(new \Doctrine\Common\Collections\ArrayCollection (array($this)));
            }
        }
    }

    /**
     * Autorelacionamento de many to many para remover 
     * disciplina curso equivalente na tabela acadgeral__disciplina_equivalencia
     * @param \Doctrine\Common\Collections\ArrayCollection $discequivDestino
     */
    public function removeDiscequivDestino(\Doctrine\Common\Collections\ArrayCollection $discequivDestino){
        foreach($discequivDestino as $cadaDiscequivDestino) {
            if( $this->discequivDestino->contains($cadaDiscequivDestino)) {
                $this->discequivDestino->removeElement($cadaDiscequivDestino);
                $cadaDiscequivDestino->removeDiscequivOrigem(new \Doctrine\Common\Collections\ArrayCollection (array($this)));
            }
        }
    }

    /**
     * Autorelacionamento de many to many para adicionar
     * disciplina curso equivalente na tabela acadgeral__disciplina_equivalencia
     * @param \Doctrine\Common\Collections\ArrayCollection $discequivOrigem
     */
    public function addDiscequivOrigem(\Doctrine\Common\Collections\ArrayCollection $discequivOrigem){
        foreach($discequivOrigem as $cadaDiscequivOrigem) {
            if( !$this->discequivOrigem->contains($cadaDiscequivOrigem) ) {
                $this->discequivOrigem->add($cadaDiscequivOrigem);
                $cadaDiscequivOrigem->addDiscequivDestino(new \Doctrine\Common\Collections\ArrayCollection (array($this)));
            }
        }
    }

    /**
     * Autorelacionamento de many to many para remover 
     * disciplina curso equivalente na tabela acadgeral__disciplina_equivalencia
     * @param \Doctrine\Common\Collections\ArrayCollection $discequivOrigem
     */
    public function removeDiscequivOrigem(\Doctrine\Common\Collections\ArrayCollection $discequivOrigem){
        foreach($discequivOrigem as $cadaDiscequivOrigem) {
            if( $this->discequivOrigem->contains($cadaDiscequivOrigem)) {
                $this->discequivOrigem->removeElement($cadaDiscequivOrigem);
                $cadaDiscequivOrigem->removeDiscequivDestino(new \Doctrine\Common\Collections\ArrayCollection (array($this)));
            }
        }
    }

}
