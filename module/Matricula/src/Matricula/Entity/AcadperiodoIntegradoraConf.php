<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoIntegradoraConf
 *
 * @ORM\Table(name="acadperiodo__integradora_conf", indexes={@ORM\Index(name="fk_acadperiodo__integradora_conf_acadperiodo__letivo1_idx", columns={"per_id_inicial"}), @ORM\Index(name="fk_acadperiodo__integradora_conf_acadperiodo__letivo2_idx", columns={"per_id_final"})})
 * @ORM\Entity
 */
class AcadperiodoIntegradoraConf
{
    /**
     * @var integer
     *
     * @ORM\Column(name="integconf_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $integconfId;

    /**
     * @var float
     *
     * @ORM\Column(name="integconf_pontos", type="float", precision=10, scale=0, nullable=false)
     */
    private $integconfPontos;

    /**
     * @var integer
     *
     * @ORM\Column(name="integconf_questoes", type="integer", nullable=false)
     */
    private $integconfQuestoes;

    /**
     * @var integer
     *
     * @ORM\Column(name="integconf_serie_max", type="integer", nullable=false)
     */
    private $integconfSerieMax;

    /**
     * @var \Matricula\Entity\AcadperiodoLetivo
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoLetivo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="per_id_inicial", referencedColumnName="per_id")
     * })
     */
    private $perInicial;

    /**
     * @var \Matricula\Entity\AcadperiodoLetivo
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoLetivo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="per_id_final", referencedColumnName="per_id")
     * })
     */
    private $perFinal;

    /**
     * @return int
     */
    public function getIntegconfId()
    {
        return $this->integconfId;
    }

    /**
     * @param int $integconfId
     */
    public function setIntegconfId($integconfId)
    {
        $this->integconfId = $integconfId;
    }

    /**
     * @return float
     */
    public function getIntegconfPontos()
    {
        return $this->integconfPontos;
    }

    /**
     * @param float $integconfPontos
     */
    public function setIntegconfPontos($integconfPontos)
    {
        $this->integconfPontos = $integconfPontos;
    }

    /**
     * @return int
     */
    public function getIntegconfQuestoes()
    {
        return $this->integconfQuestoes;
    }

    /**
     * @param int $integconfQuestoes
     */
    public function setIntegconfQuestoes($integconfQuestoes)
    {
        $this->integconfQuestoes = $integconfQuestoes;
    }

    /**
     * @return int
     */
    public function getIntegconfSerieMax()
    {
        return $this->integconfSerieMax;
    }

    /**
     * @param int $integconfSerieMax
     */
    public function setIntegconfSerieMax($integconfSerieMax)
    {
        $this->integconfSerieMax = $integconfSerieMax;
    }

    /**
     * @return \AcadperiodoLetivo
     */
    public function getPerInicial()
    {
        return $this->perInicial;
    }

    /**
     * @param \AcadperiodoLetivo $perInicial
     */
    public function setPerInicial($perInicial)
    {
        $this->perInicial = $perInicial;
    }

    /**
     * @return \AcadperiodoLetivo
     */
    public function getPerFinal()
    {
        return $this->perFinal;
    }

    /**
     * @param \AcadperiodoLetivo $perFinal
     */
    public function setPerFinal($perFinal)
    {
        $this->perFinal = $perFinal;
    }

}
