<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AcadgeralResponsavel
 *
 * @ORM\Table(name="acadgeral__responsavel", indexes={@ORM\Index(name="fk_matricula_aluno_pessoa_fisica_matricula_aluno1_idx", columns={"resp_aluno"}), @ORM\Index(name="fk_matricula_responsavel_pessoa1_idx", columns={"pes_id"})})
 * @ORM\Entity
 * @LG\LG(id="respId",label="resp_id")
 * @Jarvis\Jarvis(title="Alunos",icon="fa fa-table")
 */
class AcadgeralResponsavel
{
    /**
     * @var integer
     *
     * @ORM\Column(name="resp_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="resp_id")
     * @LG\Labels\Attributes(text="Índice",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $respId;

    /**
     * @var string
     *
     * @ORM\Column(name="resp_vinculo", type="string", nullable=false)
     * @LG\Labels\Property(name="resp_vinculo")
     * @LG\Labels\Attributes(text="Vinculo",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $respVinculo;

    /**
     * @var string
     *
     * @ORM\Column(name="resp_nivel", type="string", nullable=false)
     * @LG\Labels\Property(name="resp_nivel")
     * @LG\Labels\Attributes(text="Nivel",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $respNivel;

    /**
     * @var AcadgeralAluno
     *
     * @ORM\ManyToOne(targetEntity="AcadgeralAluno")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="aluno_id", referencedColumnName="aluno_id")
     * })
     */
    private $aluno;

    /**
     * @var \Pessoa\Entity\Pessoa
     *
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     */
    private $pes;

    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getRespId()
    {
        return $this->respId;
    }

    /**
     * @param int $respId
     */
    public function setRespId($respId)
    {
        $this->respId = $respId;
    }

    /**
     * @return string
     */
    public function getRespVinculo()
    {
        return $this->respVinculo;
    }

    /**
     * @param string $respVinculo
     */
    public function setRespVinculo($respVinculo)
    {
        $this->respVinculo = $respVinculo;
    }

    /**
     * @return string
     */
    public function getRespNivel()
    {
        return $this->respNivel;
    }

    /**
     * @param string $respNivel
     */
    public function setRespNivel($respNivel)
    {
        $this->respNivel = $respNivel;
    }

    /**
     * @return AcadgeralAluno
     */
    public function getAluno()
    {
        return $this->aluno;
    }

    /**
     * @param AcadgeralAluno $aluno
     */
    public function setAluno($aluno)
    {
        $this->aluno = $aluno;
    }

    /**
     * @return \Pessoa\Entity\Pessoa
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @param \Pessoa\Entity\Pessoa $pes
     */
    public function setPes($pes)
    {
        $this->pes = $pes;
    }

    public function toArray()
    {
        $arrPessoa = $this->getPes()->toArray();
        $arrDados  = [
            'respId'      => $this->getRespId(),
            'pes'         => $this->getPes()->getPesId(),
            'aluno'       => $this->getAluno()->getAlunoId(),
            'alunoId'     => $this->getAluno()->getAlunoId(),
            'respNivel'   => $this->getRespNivel(),
            'respVinculo' => $this->getRespVinculo()
        ];

        return array_merge($arrDados, $arrPessoa);
    }
}
