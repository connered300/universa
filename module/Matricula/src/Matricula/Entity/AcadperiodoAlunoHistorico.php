<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoAlunoHistorico
 *
 * @ORM\Table(name="acadperiodo__aluno_historico", uniqueConstraints={@ORM\UniqueConstraint(name="situacaoper__historico_id_UNIQUE", columns={"situacaoper__historico_id"})}, indexes={@ORM\Index(name="fk_acadgeral__situacao_acadperiodo__aluno_acadperiodo__alun_idx", columns={"alunoper_id"}), @ORM\Index(name="fk_acadgeral__situacao_acadperiodo__aluno_acadgeral__situac_idx", columns={"situacao_id"}), @ORM\Index(name="fk_acadperiodo__historico_acesso_pessoas1_idx", columns={"usuario_historico"}), @ORM\Index(name="fk_acadperiodo__aluno_historico_acadperiodo__turma1_idx", columns={"turma_id"})})
 * @ORM\Entity
 */
class AcadperiodoAlunoHistorico
{
    /**
     * @var integer
     *
     * @ORM\Column(name="situacaoper__historico_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $situacaoperHistoricoId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="situacaoper_data", type="datetime", nullable=false)
     */
    private $situacaoperData;

    /**
     * @var string
     *
     * @ORM\Column(name="situacaoper_historico_observacoes", type="text", length=65535, nullable=true)
     */
    private $situacaoperHistoricoObservacoes;

    /**
     * @var \Matricula\Entity\AcadgeralSituacao
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadgeralSituacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="situacao_id", referencedColumnName="situacao_id")
     * })
     */
    private $situacao;

    /**
     * @var \Matricula\Entity\AcadperiodoAluno
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoAluno")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="alunoper_id", referencedColumnName="alunoper_id")
     * })
     */
    private $alunoper;

    /**
     * @var \Matricula\Entity\AcadperiodoTurma
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoTurma")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="turma_id", referencedColumnName="turma_id")
     * })
     */
    private $turma;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     *
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_historico", referencedColumnName="id")
     * })
     */
    private $usuarioHistorico;

    /**
     * @return int
     */
    public function getSituacaoperHistoricoId()
    {
        return $this->situacaoperHistoricoId;
    }

    /**
     * @param int $situacaoperHistoricoId
     * @return AcadperiodoAlunoHistorico
     */
    public function setSituacaoperHistoricoId($situacaoperHistoricoId)
    {
        $this->situacaoperHistoricoId = $situacaoperHistoricoId;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getSituacaoperData()
    {
        return $this->situacaoperData;
    }

    /**
     * @param \DateTime $situacaoperData
     * @return AcadperiodoAlunoHistorico
     */
    public function setSituacaoperData($situacaoperData)
    {
        $this->situacaoperData = $situacaoperData;

        return $this;
    }

    /**
     * @return string
     */
    public function getSituacaoperHistoricoObservacoes()
    {
        return $this->situacaoperHistoricoObservacoes;
    }

    /**
     * @param string $situacaoperHistoricoObservacoes
     * @return AcadperiodoAlunoHistorico
     */
    public function setSituacaoperHistoricoObservacoes($situacaoperHistoricoObservacoes)
    {
        $this->situacaoperHistoricoObservacoes = $situacaoperHistoricoObservacoes;

        return $this;
    }

    /**
     * @return AcadgeralSituacao
     */
    public function getSituacao()
    {
        return $this->situacao;
    }

    /**
     * @param AcadgeralSituacao $situacao
     * @return AcadperiodoAlunoHistorico
     */
    public function setSituacao($situacao)
    {
        $this->situacao = $situacao;

        return $this;
    }

    /**
     * @return AcadperiodoAluno
     */
    public function getAlunoper()
    {
        return $this->alunoper;
    }

    /**
     * @param AcadperiodoAluno $alunoper
     * @return AcadperiodoAlunoHistorico
     */
    public function setAlunoper($alunoper)
    {
        $this->alunoper = $alunoper;

        return $this;
    }

    /**
     * @return AcadperiodoTurma
     */
    public function getTurma()
    {
        return $this->turma;
    }

    /**
     * @param AcadperiodoTurma $turma
     * @return AcadperiodoAlunoHistorico
     */
    public function setTurma($turma)
    {
        $this->turma = $turma;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuarioHistorico()
    {
        return $this->usuarioHistorico;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuarioHistorico
     * @return AcadperiodoAlunoHistorico
     */
    public function setUsuarioHistorico($usuarioHistorico)
    {
        $this->usuarioHistorico = $usuarioHistorico;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \VersaSpine\Stdlib\Hydrator())->extract($this);
    }
}