<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoDiarioEntrega
 *
 * @ORM\Table(name="acadperiodo__diario_entrega", indexes={@ORM\Index(name="fk_acadperiodo__diario_entrega_acadperiodo__docente_discipl_idx", columns={"docdisc_id"})})
 * @ORM\Entity
 */
class AcadperiodoDiarioEntrega
{
    /**
     * @var integer
     *
     * @ORM\Column(name="diario_entrega_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $diarioEntregaId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="diario_frequencia_entrega_data", type="datetime", nullable=true)
     */
    private $diarioFrequenciaEntregaData;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="diario_anotacoes_entrega_data", type="datetime", nullable=true)
     */
    private $diarioAnotacoesEntregaData;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="diario_encerramento_entrega_data", type="datetime", nullable=true)
     */
    private $diarioEncerramentoEntregaData;

    /**
     * @var string
     *
     * @ORM\Column(name="diario_entrega_mes", type="string", nullable=false)
     */
    private $diarioEntregaMes;

    /**
     * @var \Matricula\Entity\AcadperiodoDocenteDisciplina
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoDocenteDisciplina")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="docdisc_id", referencedColumnName="docdisc_id")
     * })
     */
    private $docdisc;

    /**
     * @return int
     */
    public function getDiarioEntregaId()
    {
        return $this->diarioEntregaId;
    }

    /**
     * @param int $diarioEntregaId
     */
    public function setDiarioEntregaId($diarioEntregaId)
    {
        $this->diarioEntregaId = $diarioEntregaId;
    }

    /**
     * @return \Matricula\Entity\AcadperiodoDocenteDisciplina
     */
    public function getDocdisc()
    {
        return $this->docdisc;
    }

    /**
     * @param \Matricula\Entity\AcadperiodoDocenteDisciplina $docdisc
     */
    public function setDocdisc($docdisc)
    {
        $this->docdisc = $docdisc;
    }

    /**
     * @return \DateTime
     */
    public function getDiarioFrequenciaEntregaData()
    {
        return $this->diarioFrequenciaEntregaData;
    }

    /**
     * @param \DateTime $diarioFrequenciaEntregaData
     */
    public function setDiarioFrequenciaEntregaData($diarioFrequenciaEntregaData)
    {
        $this->diarioFrequenciaEntregaData = $diarioFrequenciaEntregaData;
    }

    /**
     * @return \DateTime
     */
    public function getDiarioAnotacoesEntregaData()
    {
        return $this->diarioAnotacoesEntregaData;
    }

    /**
     * @param \DateTime $diarioAnotacoesEntregaData
     */
    public function setDiarioAnotacoesEntregaData($diarioAnotacoesEntregaData)
    {
        $this->diarioAnotacoesEntregaData = $diarioAnotacoesEntregaData;
    }

    /**
     * @return \DateTime
     */
    public function getDiarioEncerramentoEntregaData()
    {
        return $this->diarioEncerramentoEntregaData;
    }

    /**
     * @param \DateTime $diarioEncerramentoEntregaData
     */
    public function setDiarioEncerramentoEntregaData($diarioEncerramentoEntregaData)
    {
        $this->diarioEncerramentoEntregaData = $diarioEncerramentoEntregaData;
    }

    /**
     * @return string
     */
    public function getDiarioEntregaMes()
    {
        return $this->diarioEntregaMes;
    }

    /**
     * @param string $diarioEntregaMes
     */
    public function setDiarioEntregaMes($diarioEntregaMes)
    {
        $this->diarioEntregaMes = $diarioEntregaMes;
    }

    public function toArray()
    {
        return array(
            'diarioEntregaId'               => $this->getDiarioEntregaId(),
            'diarioAnotacoesEntregaData'    => $this->getDiarioAnotacoesEntregaData(),
            'diarioEncerramentoEntregaData' => $this->getDiarioEncerramentoEntregaData(),
            'diarioEntregaMes'              => $this->getDiarioEntregaMes(),
            'diarioFrequenciaEntregaData'   => $this->getDiarioFrequenciaEntregaData(),
            'docDisc'                       => $this->getDocdisc(),
        );
    }

}

