<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AcadgeralAlunoCurso
 *
 * @ORM\Table(name="acadgeral__aluno_curso", uniqueConstraints={@ORM\UniqueConstraint(name="index4", columns={"pes_id", "cursocampus_id"})}, indexes={@ORM\Index(name="fk_matricula_aluno_curso_campus_curso1_idx", columns={"cursocampus_id"}), @ORM\Index(name="fk_matricula_aluno_curso_matricula_aluno1_idx", columns={"pes_id"})})
 * @ORM\Entity
 * @LG\LG(id="alunocursoId",label="alunocurso_id")
 * @Jarvis\Jarvis(title="Alunos Por curso",icon="fa fa-table")
 *
 */
class AcadgeralAlunoCurso
{
    /**
     * @var integer
     *
     * @ORM\Column(name="alunocurso_id", type="bigint", nullable=false, length=12)
     * @ORM\Id
     * @LG\Labels\Property(name="alunocurso_id")
     * @LG\Labels\Attributes(text="Índice",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $alunocursoId;

    /**
     * @var string
     *
     * @ORM\Column(name="alunocurso_carteira", type="string", length=45, nullable=true)
     */
    private $alunocursoCarteira;

    /**
     * @var string
     *
     * @ORM\Column(name="alunocurso_observacoes", type="text", nullable=true)
     */
    private $alunocursoObservacoes;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="alunocurso_data_cadastro", type="datetime", nullable=false)
     * @LG\Labels\Property(name="alunocurso_data_cadastro")
     * @LG\Labels\Attributes(text="Data Inscrição",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $alunocursoDataCadastro;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="alunocurso_data_colacao", type="date", nullable=true)
     */
    private $alunocursoDataColacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="alunocurso_data_matricula", type="datetime", nullable=false)
     */
    private $alunocursoDataMatricula;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="alunocurso_data_expedicao_diploma", type="date", nullable=true)
     */
    private $alunocursoDataExpedicaoDiploma;
    /**
     * @var string
     *
     * @ORM\Column(name="alunocurso_situacao", type="string", nullable=false, length=10)
     * @LG\Labels\Property(name="alunocurso_situacao")
     * @LG\Labels\Attributes(text="situação do curso")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $alunocursoSituacao;

    /**
     * @var \Matricula\Entity\CampusCurso
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\CampusCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cursocampus_id", referencedColumnName="cursocampus_id")
     * })
     */
    private $cursocampus;

    /**
     * @var AcadgeralAluno
     *
     * @ORM\ManyToOne(targetEntity="AcadgeralAluno")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="aluno_id", referencedColumnName="aluno_id")
     * })
     */
    private $aluno;

    /**
     * @var \Vestibular\Entity\SelecaoTipo
     *
     * @ORM\OneToOne(targetEntity="Vestibular\Entity\SelecaoTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tiposel_id", referencedColumnName="tiposel_id")
     * })
     */
    private $tiposel;

    /**
     * @var \Organizacao\Entity\OrgAgenteEducacional
     * @ORM\ManyToOne(targetEntity="Organizacao\Entity\OrgAgenteEducacional")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id_agente", referencedColumnName="pes_id")
     * })
     */
    private $pesIdAgente;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_cadastro", referencedColumnName="id")
     * })
     */
    private $usuarioCadastro;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_alteracao", referencedColumnName="id")
     * })
     */
    private $usuarioAlteracao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="alunocurso_data_alteracao", type="datetime", nullable=true)
     * @LG\Labels\Property(name="alunocurso_data_alteracao")
     * @LG\Labels\Attributes(text="Data Alteração",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $alunocursoDataAlteracao;

    /**
     * @var \Matricula\Entity\AcadgeralMotivoAlteracao
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadgeralMotivoAlteracao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="motivo_id", referencedColumnName="motivo_id")
     * })
     */
    private $motivo;

    /**
     * @var string
     *
     * @ORM\Column(name="alunocurso_mediador", type="string", nullable=true, length=255)
     * @LG\Labels\Property(name="alunocurso_mediador")
     * @LG\Labels\Attributes(text="Data Alteração",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $alunocursoMediador;

    /**
     * @var string
     *
     * @ORM\Column(name="alunocurso_pessoa_indicacao", type="string", nullable=true, length=255)
     * @LG\Labels\Property(name="alunocurso_pessoa_indicacao")
     * @LG\Labels\Attributes(text="Data Alteração",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $alunocursoPessoaIndicacao;

    /**
     * @var \Matricula\Entity\AcadgeralCadastroOrigem
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadgeralCadastroOrigem")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="origem_id", referencedColumnName="origem_id")
     * })
     */
    private $origem;

    /**
     * @var string
     *
     * @ORM\Column(name="alunocurso_alteracao_observacao", type="string", nullable=true, length=255)
     * @LG\Labels\Property(name="alunocurso_alteracao_observacao")
     * @LG\Labels\Attributes(text="observação alteração do")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $alunocursoAlteracaoObservacao;

    /**
     * @var string
     *
     * @ORM\Column(name="alunocurso_enade", type="text", nullable=true)
     */
    private $alunocursoEnade;

    /**
     * @var string
     *
     * @ORM\Column(name="alunocurso__observacao_historico", type="text", nullable=true)
     */
    private $alunocursoObservacaoHistorico;

    /**
     * @return integer
     */
    public function getAlunocursoId()
    {
        return $this->alunocursoId;
    }

    /**
     * @param integer $alunocursoId
     * @return AcadgeralAlunoCurso
     */
    public function setAlunocursoId($alunocursoId)
    {
        $this->alunocursoId = $alunocursoId;

        return $this;
    }

    /**
     * @return \Matricula\Entity\CampusCurso
     */
    public function getCursocampus()
    {
        return $this->cursocampus;
    }

    /**
     * @param \Matricula\Entity\CampusCurso $cursocampus
     * @return AcadgeralAlunoCurso
     */
    public function setCursocampus($cursocampus)
    {
        $this->cursocampus = $cursocampus;

        return $this;
    }

    /**
     * @return \Matricula\Entity\AcadgeralAluno
     */
    public function getAluno()
    {
        return $this->aluno;
    }

    /**
     * @param \Matricula\Entity\AcadgeralAluno $aluno
     * @return AcadgeralAlunoCurso
     */
    public function setAluno($aluno)
    {
        $this->aluno = $aluno;

        return $this;
    }

    /**
     * @return \Vestibular\Entity\SelecaoTipo
     */
    public function getTiposel()
    {
        return $this->tiposel;
    }

    /**
     * @param \Vestibular\Entity\SelecaoTipo $tiposel
     * @return AcadgeralAlunoCurso
     */
    public function setTiposel($tiposel)
    {
        $this->tiposel = $tiposel;

        return $this;
    }

    /**
     * @return \Organizacao\Entity\OrgAgenteEducacional
     */
    public function getPesIdAgente()
    {
        return $this->pesIdAgente;
    }

    /**
     * @param \Organizacao\Entity\OrgAgenteEducacional $pesIdAgente
     * @return AcadgeralAlunoCurso
     */
    public function setPesIdAgente($pesIdAgente)
    {
        $this->pesIdAgente = $pesIdAgente;

        return $this;
    }

    /**
     * @param bool|false $formatar
     * @return \Datetime|string
     */
    public function getAlunocursoDataCadastro($formatar = false, $formato = 'd/m/Y H:i:s')
    {
        $alunocursoDataCadastro = $this->alunocursoDataCadastro;

        if ($formatar && $alunocursoDataCadastro) {
            $alunocursoDataCadastro = $alunocursoDataCadastro->format($formato);
        }

        return $alunocursoDataCadastro;
    }

    /**
     * @param \Datetime $alunocursoDataCadastro
     * @return AcadgeralAlunoCurso
     */
    public function setAlunocursoDataCadastro($alunocursoDataCadastro)
    {
        if ($alunocursoDataCadastro) {
            if (is_string($alunocursoDataCadastro)) {
                $alunocursoDataCadastro = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $alunocursoDataCadastro
                );
                $alunocursoDataCadastro = new \Datetime($alunocursoDataCadastro);
            }
        } else {
            $alunocursoDataCadastro = null;
        }

        $this->alunocursoDataCadastro = $alunocursoDataCadastro;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getAlunocursoDataAlteracao($format = false)
    {
        $alunocursoDataAlteracao = $this->alunocursoDataAlteracao;

        if ($format && $alunocursoDataAlteracao) {
            $alunocursoDataAlteracao = $alunocursoDataAlteracao->format('d/m/Y H:i:s');
        }

        return $alunocursoDataAlteracao;
    }

    /**
     * @param \Datetime $alunocursoDataAlteracao
     * @return AcadgeralAlunoCurso
     */
    public function setAlunocursoDataAlteracao($alunocursoDataAlteracao)
    {
        if ($alunocursoDataAlteracao) {
            if (is_string($alunocursoDataAlteracao)) {
                $alunocursoDataAlteracao = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $alunocursoDataAlteracao
                );
                $alunocursoDataAlteracao = new \Datetime($alunocursoDataAlteracao);
            }
        } else {
            $alunocursoDataAlteracao = null;
        }

        $this->alunocursoDataAlteracao = $alunocursoDataAlteracao;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlunocursoCarteira()
    {
        return $this->alunocursoCarteira;
    }

    /**
     * @param string $alunocursoCarteira
     * @return AcadgeralAlunoCurso
     */
    public function setAlunocursoCarteira($alunocursoCarteira)
    {
        $this->alunocursoCarteira = $alunocursoCarteira;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlunocursoObservacoes()
    {
        return $this->alunocursoObservacoes;
    }

    /**
     * @param string $alunocursoObservacoes
     * @return AcadgeralAlunoCurso
     */
    public function setAlunocursoObservacoes($alunocursoObservacoes)
    {
        $this->alunocursoObservacoes = $alunocursoObservacoes;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getAlunocursoDataColacao($format = false)
    {
        $alunocursoDataColacao = $this->alunocursoDataColacao;

        if ($format && $alunocursoDataColacao) {
            $alunocursoDataColacao = $alunocursoDataColacao->format('d/m/Y');
        }

        return $alunocursoDataColacao;
    }

    /**
     * @param \Datetime $alunocursoDataColacao
     * @return AcadgeralAlunoCurso
     */
    public function setAlunocursoDataColacao($alunocursoDataColacao)
    {
        if ($alunocursoDataColacao) {
            if (is_string($alunocursoDataColacao)) {
                $alunocursoDataColacao = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $alunocursoDataColacao
                );
                $alunocursoDataColacao = new \Datetime($alunocursoDataColacao);
            }
        } else {
            $alunocursoDataColacao = null;
        }

        $this->alunocursoDataColacao = $alunocursoDataColacao;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getAlunocursoDataExpedicaoDiploma($format = false)
    {
        $alunocursoDataExpedicaoDiploma = $this->alunocursoDataExpedicaoDiploma;

        if ($format && $alunocursoDataExpedicaoDiploma) {
            $alunocursoDataExpedicaoDiploma = $alunocursoDataExpedicaoDiploma->format('d/m/Y');
        }

        return $alunocursoDataExpedicaoDiploma;
    }

    /**
     * @param \Datetime $alunocursoDataExpedicaoDiploma
     * @return AcadgeralAlunoCurso
     */
    public function setAlunocursoDataExpedicaoDiploma($alunocursoDataExpedicaoDiploma)
    {
        if ($alunocursoDataExpedicaoDiploma) {
            if (is_string($alunocursoDataExpedicaoDiploma)) {
                $alunocursoDataExpedicaoDiploma = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $alunocursoDataExpedicaoDiploma
                );
                $alunocursoDataExpedicaoDiploma = new \Datetime($alunocursoDataExpedicaoDiploma);
            }
        } else {
            $alunocursoDataExpedicaoDiploma = null;
        }

        $this->alunocursoDataExpedicaoDiploma = $alunocursoDataExpedicaoDiploma;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlunocursoSituacao()
    {
        return $this->alunocursoSituacao;
    }

    /**
     * @return bool
     */
    public function verificaSituacaoPendente()
    {
        return in_array(
            $this->alunocursoSituacao,
            \Matricula\Service\AcadgeralAlunoCurso::getAlunocursoSituacaoPendente()
        );
    }

    /**
     * @param string $alunocursoSituacao
     * @return AcadgeralAlunoCurso
     */
    public function setAlunocursoSituacao($alunocursoSituacao)
    {
        $this->alunocursoSituacao = $alunocursoSituacao;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuarioCadastro()
    {
        return $this->usuarioCadastro;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuarioCadastro
     * @return AcadgeralAlunoCurso
     */
    public function setUsuarioCadastro($usuarioCadastro)
    {
        $this->usuarioCadastro = $usuarioCadastro;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuarioAlteracao()
    {
        return $this->usuarioAlteracao;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuarioAlteracao
     * @return AcadgeralAlunoCurso
     */
    public function setUsuarioAlteracao($usuarioAlteracao)
    {
        $this->usuarioAlteracao = $usuarioAlteracao;

        return $this;
    }

    /**
     * @param boolean $formatar
     * @return \DateTime
     */
    public function getAlunocursoDataMatricula($formatar = false, $formato = 'd/m/Y H:i:s')
    {
        $alunocursoDataMatricula = $this->alunocursoDataMatricula;

        if ($formatar && $alunocursoDataMatricula) {
            $alunocursoDataMatricula = $alunocursoDataMatricula->format($formato);
        }

        return $alunocursoDataMatricula;
    }

    /**
     * @param \DateTime $alunocursoDataMatricula
     * @return AcadgeralAlunoCurso
     */
    public function setAlunocursoDataMatricula($alunocursoDataMatricula)
    {
        if ($alunocursoDataMatricula) {
            if (is_string($alunocursoDataMatricula)) {
                $alunocursoDataMatricula = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $alunocursoDataMatricula
                );
                $alunocursoDataMatricula = new \Datetime($alunocursoDataMatricula);
            }
        } else {
            $alunocursoDataMatricula = new \DateTime();
        }

        $this->alunocursoDataMatricula = $alunocursoDataMatricula;

        return $this;
    }

    /**
     * @return \Matricula\Entity\AcadgeralMotivoAlteracao
     */
    public function getMotivo()
    {
        return $this->motivo;
    }

    /**
     * @param \Matricula\Entity\AcadgeralMotivoAlteracao $motivo
     * @return AcadgeralAlunoCurso
     */
    public function setMotivo($motivo)
    {
        $this->motivo = $motivo;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlunocursoAlteracaoObservacao()
    {
        return $this->alunocursoAlteracaoObservacao;
    }

    /**
     * @param string $alunocursoAlteracaoObservacao
     * @return AcadgeralAlunoCurso
     */
    public function setAlunocursoAlteracaoObservacao($alunocursoAlteracaoObservacao)
    {
        $this->alunocursoAlteracaoObservacao = $alunocursoAlteracaoObservacao;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlunocursoMediador()
    {
        return $this->alunocursoMediador;
    }

    /**
     * @return string
     */
    public function getAlunocursoPessoaIndicacao()
    {
        return $this->alunocursoPessoaIndicacao;
    }

    /**
     * @return \Matricula\Entity\AcadgeralCadastroOrigem
     */
    public function getOrigem()
    {
        return $this->origem;
    }

    /**
     * @param string $alunocursoMediador
     * @return AcadgeralAlunoCurso
     */
    public function setAlunocursoMediador($alunocursoMediador)
    {
        $this->alunocursoMediador = $alunocursoMediador;

        return $this;
    }

    /**
     * @param string $alunocursoPessoaIndicacao
     * @return AcadgeralAlunoCurso
     */
    public function setAlunocursoPessoaIndicacao($alunocursoPessoaIndicacao)
    {
        $this->alunocursoPessoaIndicacao = $alunocursoPessoaIndicacao;

        return $this;
    }

    /**
     * @param \Matricula\Entity\AcadgeralCadastroOrigem $origem
     * @return AcadgeralAlunoCurso
     */
    public function setOrigem($origem)
    {
        $this->origem = $origem;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlunocursoEnade()
    {
        return $this->alunocursoEnade;
    }

    /**
     * @param $text
     * @return AcadgeralAlunoCurso
     */
    public function setAlunocursoEnade($text)
    {
        $this->alunocursoEnade = $text;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlunocursoObservacaoHistorico()
    {
        return $this->alunocursoObservacaoHistorico;
    }

    /**
     * @param $texto
     * @return AcadgeralAlunoCurso
     */
    public function setAlunocursoObservacaoHistorico($texto)
    {
        $this->alunocursoObservacaoHistorico = $texto;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $aluno       = $this->getAluno();
        $pesIdagente = $this->getPesIdAgente();
        $pesIdagente = is_int($pesIdagente) ? $pesIdagente : null;

        $arrData = array(
            'alunocursoId'                   => $this->getAlunocursoId(),
            'cursocampus'                    => $this->getCursocampus() ? $this->getCursocampus()->toArray() : null,
            'aluno'                          => $this->getAluno() ? $this->getAluno()->toArray() : null,
            'tiposel'                        => $this->getTiposel() ? $this->getTiposel()->toArray() : null,
            'pesIdAgente'                    => $pesIdagente,
            'pesIdAgenteAluno'               => $pesIdagente,
            'alunocursoDataCadastro'         => $this->getAlunocursoDataCadastro(true),
            'alunocursoDataMatricula'        => $this->getAlunocursoDataMatricula(true),
            'alunocursoDataCadastroSemHora'  => $this->getAlunocursoDataCadastro(true, 'd/m/Y'),
            'alunocursoDataMatriculaSemHora' => $this->getAlunocursoDataMatricula(true, 'd/m/Y'),
            'alunocursoDataAlteracao'        => $this->getAlunocursoDataAlteracao(true),
            'alunocursoCarteira'             => $this->getAlunocursoCarteira(),
            'alunocursoObservacoes'          => $this->getAlunocursoObservacoes(),
            'alunocursoDataColacao'          => $this->getAlunocursoDataColacao(true),
            'alunocursoDataExpedicaoDiploma' => $this->getAlunocursoDataExpedicaoDiploma(true),
            'alunocursoSituacao'             => $this->getAlunocursoSituacao(),
            'usuarioCadastro'                => $this->getUsuarioCadastro(),
            'usuarioAlteracao'               => $this->getUsuarioAlteracao(),
            'alunocursoPessoaIndicacao'      => $this->getAlunocursoPessoaIndicacao(),
            'alunocursoMediador'             => $this->getAlunocursoMediador(),
            'pesId'                          => '',
            'pesNome'                        => '',
            'campId'                         => '',
            'ies'                            => '',
            'campNome'                       => '',
            'tiposelId'                      => '',
            'pesNomeAgente'                  => '',
            'alunoConfigPgtoCurso'           => '',
            'usuarioCadastroLogin'           => '',
            'usuarioAlteracaoLogin'          => '',
            'alunoEtnia'                     => '',
            'origem'                         => '',
            'origemId'                       => '',
            'origemNome'                     => '',
            'motivo'                         => '',
            'motivoId'                       => '',
            'motivoDescricao'                => '',
            'alunocursoAlteracaoObservacao'  => $this->getAlunocursoAlteracaoObservacao(),
            'alunocursoEnade'                => $this->getAlunocursoEnade(),
            'alunocursoObservacaoHistorico'  => $this->getAlunocursoObservacaoHistorico()
        );

        if (!$aluno) {
            $aluno = new \Matricula\Entity\AcadgeralAluno(array());
        }

        $arrData = array_merge($aluno->toArray(), $arrData);

        if ($this->getAluno()) {
            $arrData['alunoId'] = $this->getAluno()->getAlunoId();
            $arrData['pesId']   = $this->getAluno()->getPes()->getPes()->getPesId();
            $arrData['pesNome'] = $this->getAluno()->getPes()->getPes()->getPesNome();
        }

        if ($this->getOrigem() && $this->getOrigem()->getOrigemId() != 0) {
            $arrData['origem']     = $this->getOrigem()->getOrigemId();
            $arrData['origemId']   = $this->getOrigem()->getOrigemId();
            $arrData['origemNome'] = $this->getOrigem()->getOrigemNome();
        }

        if ($this->getCursocampus()) {
            $arrData['cursocampusId']   = $this->getCursocampus()->getCursocampusId();
            $arrData['cursoId']         = $this->getCursocampus()->getCurso()->getCursoId();
            $arrData['cursoNome']       = $this->getCursocampus()->getCurso()->getCursoNome();
            $arrData['campId']          = $this->getCursocampus()->getCamp()->getCampId();
            $arrData['campNome']        = $this->getCursocampus()->getCamp()->getCampNome();
            $arrData['cursocampusNome'] = $arrData['campNome'] . ' / ' . $arrData['cursoNome'];
            $arrData['arrCurso']        = $this->getCursocampus()->getCurso()->toArray();
        }

        if ($this->getTiposel()) {
            $arrData['tiposelId']   = $this->getTiposel()->getTiposelId();
            $arrData['tiposelNome'] = $this->getTiposel()->getTiposelNome();
        }

        $arrData['pesNomeAgente'] = '';

        try {
            if ($this->getPesIdAgente()) {
                $agente = $this->getPesIdAgente();

                $pessoa = $agente ? $agente->getPes() : null;
                $pessoa = $pessoa ? $pessoa->getPesId() : null;

                $arrData['pesIdAgente']   = $pessoa;
                $arrData['pesNomeAgente'] = $pessoa ? $agente->getPes()->getPesNome() : '';
            }
        } catch (\Exception $e) {
        }

        try {
            if ($this->getAluno() && $this->getAluno()->getPesIdAgenciador()) {
                $agente                        = $this->getAluno()->getPesIdAgenciador();
                $arrData['pesIdAgenteAluno']   = $agente ? $agente->getPes()->getPesId() : '';
                $arrData['pesNomeAgenteAluno'] = $agente ? $agente->getPes()->getPesNome() : '';

                if (!$arrData['pesNomeAgente'] && $arrData['pesNomeAgenteAluno']) {
                    $arrData['pesNomeAgente'] = $arrData['pesNomeAgenteAluno'];
                }

                if (!$arrData['pesIdAgente'] && $arrData['pesIdAgenteAluno']) {
                    $arrData['pesIdAgente'] = $arrData['pesIdAgenteAluno'];
                }
            }
        } catch (\Exception $e) {
        }

        if ($this->getUsuarioCadastro()) {
            $arrData['usuarioCadastro']      = $this->getUsuarioCadastro()->getId();
            $arrData['usuarioCadastroLogin'] = $this->getUsuarioCadastro()->getLogin();
        }

        if ($this->getUsuarioAlteracao()) {
            $arrData['usuarioAlteracao']      = $this->getUsuarioAlteracao()->getId();
            $arrData['usuarioAlteracaoLogin'] = $this->getUsuarioAlteracao()->getLogin();
        }

        if ($this->getMotivo()) {
            $arrData['motivo']          = $this->getMotivo()->getMotivoId();
            $arrData['motivoid']        = $this->getMotivo()->getMotivoId();
            $arrData['motivoDescricao'] = $this->getMotivo()->getMotivoDescricao();
        }

        return $arrData;
    }

    public function toArrayAlunoMigracaoSemPeriodoLetivo()
    {
        $pesIdAgente   = $this->getPesIdAgente();
        $pesIdAgente   = $pesIdAgente ? $pesIdAgente->getPes()->getPesId() : null;
        $arrAlunoCurso = $this->getAluno()->toArray();

        $arrData = [
            'alunocursoCarteira'             => $this->getAlunocursoCarteira(),
            'alunocursoObservacoes'          => $this->getAlunocursoObservacoes(),
            'alunocursoDataCadastro'         => $this->getAlunocursoDataCadastro(true),
            'alunocursoDataColacao'          => $this->getAlunocursoDataColacao(true),
            'alunocursoDataExpedicaoDiploma' => $this->getAlunocursoDataExpedicaoDiploma(true),
            'alunocurso'                     => $this->getAlunocursoId(),
            'cursocampus'                    => $this->getCursocampus()->getCursocampusId(),
            'aluno'                          => $this->getAluno()->getAlunoId(),
            'alunoperCurso'                  => $this,
            'alunoId'                        => $this->getAluno()->getAlunoId(),
            'alunoDataCadastro'              => $this->getAluno()->getAlunoDataCadastro(true),
            'alunoDataMatricula'             => $this->getAlunocursoDataMatricula(true),
            'alunoEtnia'                     => $this->getAluno()->getAlunoEtnia(),
            'alunoMae'                       => $this->getAluno()->getAlunoMae(),
            'alunoPai'                       => $this->getAluno()->getAlunoPai(),
            'alunoCertMilitar'               => $this->getAluno()->getAlunoCertMilitar(),
            'alunoSecaoEleitoral'            => $this->getAluno()->getAlunoSecaoEleitoral(),
            'alunoTituloEleitoral'           => $this->getAluno()->getAlunoTituloEleitoral(),
            'alunoZonaEleitoral'             => $this->getAluno()->getAlunoZonaEleitoral(),
            'arq'                            => $this->getAluno()->getArq(),
            'pes'                            => $this->getAluno()->getPes()->getPes()->getPesId(),
            'pesNome'                        => $this->getAluno()->getPes()->getPes()->getPesNome(),
            'pesNacionalidade'               => $this->getAluno()->getPes()->getPes()->getPesNacionalidade(),
            'pesCpf'                         => $this->getAluno()->getPes()->getPesCpf(),
            'pesRg'                          => $this->getAluno()->getPes()->getPesRg(),
            'pesFalecido'                    => $this->getAluno()->getPes()->getPesFalecido(),
            'pesRgEmissao'                   => $this->getAluno()->getPes()->getPesRgEmissao(),
            'pesDataNascimento'              => $this->getAluno()->getPes()->getPesDataNascimento(),
            'pesSexo'                        => $this->getAluno()->getPes()->getPesSexo(),
            'pesIdAgente'                    => $pesIdAgente,
            'alunocursoSituacao'             => $this->getAlunocursoSituacao(),
            'alunocursoEnade'                => $this->getAlunocursoEnade(),
            'alunocursoObservacaoHistorico'  => $this->getAlunocursoObservacaoHistorico(),

        ];

        $arrAlunoCurso = array_merge(array_filter($arrAlunoCurso), array_filter($arrData));

        return $arrAlunoCurso;
    }
}
