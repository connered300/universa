<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoIntegradoraCaderno
 *
 * @ORM\Table(name="acadperiodo__integradora_caderno", indexes={@ORM\Index(name="fk_acadperiodo__integradora_caderno_acadperiodo__integrador_idx", columns={"integavaliacao_id"})})
 * @ORM\Entity
 */
class AcadperiodoIntegradoraCaderno
{
    /**
     * @var integer
     *
     * @ORM\Column(name="integcaderno_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $integcadernoId;

    /**
     * @var string
     *
     * @ORM\Column(name="integcaderno_nome", type="string", length=45, nullable=false)
     */
    private $integcadernoNome;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="integcaderno_data", type="date", nullable=true)
     */
    private $integcadernoData;

    /**
     * @var \AcadperiodoIntegradoraAvaliacao
     *
     * @ORM\ManyToOne(targetEntity="AcadperiodoIntegradoraAvaliacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="integavaliacao_id", referencedColumnName="integavaliacao_id")
     * })
     */
    private $integavaliacao;

    /**
     * @return int
     */
    public function getIntegcadernoId()
    {
        return $this->integcadernoId;
    }

    /**
     * @param int $integcadernoId
     */
    public function setIntegcadernoId($integcadernoId)
    {
        $this->integcadernoId = $integcadernoId;
    }

    /**
     * @return string
     */
    public function getIntegcadernoNome()
    {
        return $this->integcadernoNome;
    }

    /**
     * @param string $integcadernoNome
     */
    public function setIntegcadernoNome($integcadernoNome)
    {
        $this->integcadernoNome = $integcadernoNome;
    }

    /**
     * @return DateTime
     */
    public function getIntegcadernoData()
    {
        return $this->integcadernoData;
    }

    /**
     * @param DateTime $integcadernoData
     */
    public function setIntegcadernoData($integcadernoData)
    {
        $this->integcadernoData = $integcadernoData;
    }

    /**
     * @return AcadperiodoIntegradoraAvaliacao
     */
    public function getIntegavaliacao()
    {
        return $this->integavaliacao;
    }

    /**
     * @param AcadperiodoIntegradoraAvaliacao $integavaliacao
     */
    public function setIntegavaliacao($integavaliacao)
    {
        $this->integavaliacao = $integavaliacao;
    }

}
