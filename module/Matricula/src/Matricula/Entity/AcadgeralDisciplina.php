<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AcadgeralDisciplina
 *
 * @ORM\Table(name="acadgeral__disciplina", indexes={@ORM\Index(name="fk_acadgeral__disciplina_acadgeral__disciplina_tipo1_idx", columns={"tdisc_id"})})
 * @ORM\Entity
 * @LG\LG(id="disc_id",label="disc_nome")
 * @Jarvis\Jarvis(title="Disciplinas",icon="fa fa-table")
 */
class AcadgeralDisciplina
{
    /**
     * @var integer
     *
     * @ORM\Column(name="disc_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="disc_id")
     * @LG\Labels\Attributes(text="Índice",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $discId;

    /**
     * @var string
     *
     * @ORM\Column(name="disc_nome", type="string", length=128, nullable=false)
     * @LG\Labels\Property(name="disc_nome")
     * @LG\Labels\Attributes(text="Nome",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE%")
     */
    private $discNome;

    /**
     * @var string
     *
     * @ORM\Column(name="disc_sigla", type="string", length=45, nullable=false)
     * @LG\Labels\Property(name="disc_sigla")
     * @LG\Labels\Attributes(text="Sigla",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $discSigla;
    
    /**
     * @var \Matricula\Entity\AcadgeralDocente
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadgeralDocente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="docente_id", referencedColumnName="docente_id")
     * })
     * @LG\Labels\Property(name="docente_id")
     * @LG\Labels\Attributes(text="Docente",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     * @LG\Querys\Joins(table="acadgeral__docente",joinType="inner",joinCampDest="docente_id",joinCampDestLabel="docente_id")
     */
    private $docenteRegente;
    
    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getDiscId()
    {
        return $this->discId;
    }

    /**
     * @param int $discId
     */
    public function setDiscId($discId)
    {
        $this->discId = $discId;
    }

    /**
     * @return string
     */
    public function getDiscNome()
    {
        return $this->discNome;
    }

    /**
     * @param string $discNome
     */
    public function setDiscNome($discNome)
    {
        $this->discNome = $discNome;
    }

    /**
     * @return string
     */
    public function getDiscSigla()
    {
        return $this->discSigla;
    }

    /**
     * @param string $discSigla
     */
    public function setDiscSigla($discSigla)
    {
        $this->discSigla = $discSigla;
    }
    
    function getDocenteRegente() {
        return $this->docenteRegente;
    }

    function setDocenteRegente($docenteRegente) {
        $this->docenteRegente = $docenteRegente;
    }
    
    public function toArray()
    {
        return array(
            'discId'          => $this->getDiscId(),
            'discNome'        => $this->getDiscNome(),
            'discSigla'       => $this->getDiscSigla(),
            'docenteRegente'  => $this->getDocenteRegente(), 
        );
    }

}