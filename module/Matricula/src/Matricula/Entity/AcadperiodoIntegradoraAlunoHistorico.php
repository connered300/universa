<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoIntegradoraAlunoHistorico
 *
 * @ORM\Table(name="acadperiodo__integradora_aluno_historico", indexes={@ORM\Index(name="fk_acadperiodo__aluno_integradora_acadperiodo__aluno_discip_idx", columns={"alunodisc_hist_id"}), @ORM\Index(name="fk_acadperiodo__integradora_aluno_historico_acadperiodo__in_idx", columns={"alunointeg_id"})})
 * @ORM\Entity
 */
class AcadperiodoIntegradoraAlunoHistorico
{
    /**
     * @var integer
     *
     * @ORM\Column(name="alunointeg_hist_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $alunointegHistId;

    /**
     * @var string
     *
     * @ORM\Column(name="alunointeg_hist_respostas", type="string", length=255, nullable=false)
     */
    private $alunointegHistRespostas;

    /**
     * @var integer
     *
     * @ORM\Column(name="alunointeg_hist_acertos", type="integer", nullable=false)
     */
    private $alunointegHistAcertos;

    /**
     * @var float
     *
     * @ORM\Column(name="alunointeg_hist_nota_isolada", type="float", precision=10, scale=0, nullable=true)
     */
    private $alunointegHistNotaIsolada;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="alunointeg_hist_data", type="datetime", nullable=true)
     */
    private $alunointegHistData;

    /**
     * @var AcadperiodoAlunoDisciplina
     *
     * @ORM\ManyToOne(targetEntity="AcadperiodoAlunoDisciplina")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="alunodisc_hist_id", referencedColumnName="alunodisc_id")
     * })
     */
    private $alunodiscHist;

    /**
     * @var AcadperiodoIntegradoraAluno
     *
     * @ORM\ManyToOne(targetEntity="AcadperiodoIntegradoraAluno")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="alunointeg_id", referencedColumnName="alunointeg_id")
     * })
     */
    private $alunointeg;

    /**
     * @return int
     */
    public function getAlunointegHistId()
    {
        return $this->alunointegHistId;
    }

    /**
     * @param int $alunointegHistId
     */
    public function setAlunointegHistId($alunointegHistId)
    {
        $this->alunointegHistId = $alunointegHistId;
    }

    /**
     * @return string
     */
    public function getAlunointegHistRespostas()
    {
        return $this->alunointegHistRespostas;
    }

    /**
     * @param string $alunointegHistRespostas
     */
    public function setAlunointegHistRespostas($alunointegHistRespostas)
    {
        $this->alunointegHistRespostas = $alunointegHistRespostas;
    }

    /**
     * @return int
     */
    public function getAlunointegHistAcertos()
    {
        return $this->alunointegHistAcertos;
    }

    /**
     * @param int $alunointegHistAcertos
     */
    public function setAlunointegHistAcertos($alunointegHistAcertos)
    {
        $this->alunointegHistAcertos = $alunointegHistAcertos;
    }

    /**
     * @return float
     */
    public function getAlunointegHistNotaIsolada()
    {
        return $this->alunointegHistNotaIsolada;
    }

    /**
     * @param float $alunointegHistNotaIsolada
     */
    public function setAlunointegHistNotaIsolada($alunointegHistNotaIsolada)
    {
        $this->alunointegHistNotaIsolada = $alunointegHistNotaIsolada;
    }

    /**
     * @return \DateTime
     */
    public function getAlunointegHistData()
    {
        return $this->alunointegHistData;
    }

    /**
     * @param \DateTime $alunointegHistData
     */
    public function setAlunointegHistData($alunointegHistData)
    {
        $this->alunointegHistData = $alunointegHistData;
    }

    /**
     * @return AcadperiodoAlunoDisciplina
     */
    public function getAlunodiscHist()
    {
        return $this->alunodiscHist;
    }

    /**
     * @param AcadperiodoAlunoDisciplina $alunodiscHist
     */
    public function setAlunodiscHist($alunodiscHist)
    {
        $this->alunodiscHist = $alunodiscHist;
    }

    /**
     * @return AcadperiodoIntegradoraAluno
     */
    public function getAlunointeg()
    {
        return $this->alunointeg;
    }

    /**
     * @param AcadperiodoIntegradoraAluno $alunointeg
     */
    public function setAlunointeg($alunointeg)
    {
        $this->alunointeg = $alunointeg;
    }

}

