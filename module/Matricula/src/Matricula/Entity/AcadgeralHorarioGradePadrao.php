<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadNivel
 *
 * @ORM\Table(name="acadgeral__horario_grade__padrao", indexes={@ORM\Index(name="fk_acadgeral__grade_horaria_padrao_campus_curso1_idx", columns={"cursocampus_id"})})
 * @ORM\Entity
 */
class AcadgeralHorarioGradePadrao
{

    /**
     * @var integer
     *
     * @ORM\Column(name="horariograde_padrao_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $horarioGradePadraoId;

    /**
     * @var CampusCurso
     *
     * @ORM\ManyToOne(targetEntity="CampusCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cursocampus_id", referencedColumnName="cursocampus_id", nullable=false)
     * })
     */
    private $cursoCampusId;

    /**
     * @var /time
     *
     * @ORM\Column(name="horariograde_padrao", type="time", nullable=false)
     */
    private $horariogradePadrao;

    /**
     * @var integer
     *
     * @ORM\Column(name="horariograde_duracao_minutos", type="integer", nullable=true)
     */
    private $horariogradeDuracaoMinutos;

    /**
     * @var string
     *
     * @ORM\Column(name="horariograde_tipo", type="string", nullable=true)
     */
    private $horariogradeTipo;

    /**
     * @var string
     *
     * @ORM\Column(name="horariograde_dia", type="string", nullable=false)
     */
    private $horariogradeDia;

    /**
     * @var /datetime
     *
     * @ORM\Column(name="horariograde_data_validade", type="datetime", nullable=false)
     */
    private $horariogradeDataValidade;

    /**
     * @var /datetime
     *
     * @ORM\Column(name="horariograde_data_invalidade", type="datetime", nullable=true)
     */
    private $horariogradeDataInvalidade;

    /**
     * @return int
     */
    public function getHorarioGradePadraoId()
    {
        return $this->horarioGradePadraoId;
    }

    /**
     * @param int $horarioGradePadraoId
     * @return AcadgeralHorarioGradePadrao
     */
    public function setHorarioGradePadraoId($horarioGradePadraoId)
    {
        $this->horarioGradePadraoId = $horarioGradePadraoId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getHorariogradeDataInvalidade()
    {
        return $this->horariogradeDataInvalidade;
    }

    /**
     * @param mixed $horariogradeDataInvalidade
     * @return AcadgeralHorarioGradePadrao
     */
    public function setHorariogradeDataInvalidade($horariogradeDataInvalidade)
    {
        $this->horariogradeDataInvalidade = $horariogradeDataInvalidade;

        return $this;
    }

    /**
     * @return CampusCurso
     */
    public function getCursoCampusId()
    {
        return $this->cursoCampusId;
    }

    /**
     * @param CampusCurso $cursoCampusId
     * @return AcadgeralHorarioGradePadrao
     */
    public function setCursoCampusId($cursoCampusId)
    {
        $this->cursoCampusId = $cursoCampusId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getHorariogradePadrao()
    {
        return $this->horariogradePadrao;
    }

    /**
     * @param mixed $horariogradePadrao
     * @return AcadgeralHorarioGradePadrao
     */
    public function setHorariogradePadrao($horariogradePadrao)
    {
        $this->horariogradePadrao = $horariogradePadrao;

        return $this;
    }

    /**
     * @return int
     */
    public function getHorariogradeDuracaoMinutos()
    {
        return $this->horariogradeDuracaoMinutos;
    }

    /**
     * @param int $horariogradeDuracaoMinutos
     * @return AcadgeralHorarioGradePadrao
     */
    public function setHorariogradeDuracaoMinutos($horariogradeDuracaoMinutos)
    {
        $this->horariogradeDuracaoMinutos = $horariogradeDuracaoMinutos;

        return $this;
    }

    /**
     * @return string
     */
    public function getHorariogradeTipo()
    {
        return $this->horariogradeTipo;
    }

    /**
     * @param string $horariogradeTipo
     * @return AcadgeralHorarioGradePadrao
     */
    public function setHorariogradeTipo($horariogradeTipo)
    {
        $this->horariogradeTipo = $horariogradeTipo;

        return $this;
    }

    /**
     * @return string
     */
    public function getHorariogradeDia()
    {
        return $this->horariogradeDia;
    }

    /**
     * @param string $horariogradeDia
     * @return AcadgeralHorarioGradePadrao
     */
    public function setHorariogradeDia($horariogradeDia)
    {
        $this->horariogradeDia = $horariogradeDia;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getHorariogradeDataValidade()
    {
        return $this->horariogradeDataValidade;
    }

    /**
     * @param mixed $horariogradeDataValidade
     * @return AcadgeralHorarioGradePadrao
     */
    public function setHorariogradeDataValidade($horariogradeDataValidade)
    {
        $this->horariogradeDataValidade = $horariogradeDataValidade;

        return $this;
    }


}