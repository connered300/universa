<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;


/**
 * AcadperiodoMatrizCurricular
 *
 * @ORM\Table(name="acadperiodo__matriz_curricular", indexes={@ORM\Index(name="fk_matriz curricular_acad_curso1_idx", columns={"mat_curso"})})
 * @ORM\Entity
 * @LG\LG(id="mat_cur_id",label="mat_cur_descricao")
 * @Jarvis\Jarvis(title="Matriz Curricular",icon="fa fa-table")
 */
class AcadperiodoMatrizCurricular
{
    /**
     * @var integer
     *
     * @ORM\Column(name="mat_cur_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="mat_cur_id")
     * @LG\Labels\Attributes(text="Index",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $matCurId;

    /**
     * @var string
     *
     * @ORM\Column(name="mat_cur_ano", type="string", nullable=false)
     * @LG\Labels\Property(name="mat_cur_ano")
     * @LG\Labels\Attributes(text="Ano",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $matCurAno;

    /**
     * @var integer
     *
     * @ORM\Column(name="mat_cur_semestre", type="integer", nullable=false)
     * @LG\Labels\Property(name="mat_cur_semestre")
     * @LG\Labels\Attributes(text="Semestre",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $matCurSemestre;

    /**
     * @var string
     *
     * @ORM\Column(name="mat_cur_descricao", type="string", length=45, nullable=true)
     * @LG\Labels\Property(name="mat_cur_descricao")
     * @LG\Labels\Attributes(text="Descrição",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE%")
     */
    private $matCurDescricao;

    /**
     * @var AcadCurso
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="curso_id", referencedColumnName="curso_id")
     * })
     */
    private $curso;
    /**
     * @var string
     *
     * @LG\Labels\Property(name="impressao")
     * @LG\Labels\Attributes(text="Impressão",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $impressao;
    /**
     * @var string
     *
     * @LG\Labels\Property(name="clonar")
     * @LG\Labels\Attributes(text="Clonar",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $clonar;


    public function __construct(  array $data ) {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return \DateTime
     */
    public function getMatCurAno()
    {
        return $this->matCurAno;
    }

    /**
     * @param \DateTime $matCurAno
     */
    public function setMatCurAno($matCurAno)
    {
        $this->matCurAno = $matCurAno;
    }

    /**
     * @return int
     */
    public function getMatCurSemestre()
    {
        return $this->matCurSemestre;
    }

    /**
     * @param int $matCurSemestre
     */
    public function setMatCurSemestre($matCurSemestre)
    {
        $this->matCurSemestre = $matCurSemestre;
    }


    /**
     * @return int
     */
    public function getMatCurId()
    {
        return $this->matCurId;
    }

    /**
     * @param int $matCurId
     */
    public function setMatCurId($matCurId)
    {
        $this->matCurId = $matCurId;
    }

    /**
     * @return string
     */
    public function getMatCurDescricao()
    {
        return $this->matCurDescricao;
    }

    /**
     * @param string $matCurDescricao
     */
    public function setMatCurDescricao($matCurDescricao)
    {
        $this->matCurDescricao = $matCurDescricao;
    }

    /**
     * @return AcadCurso
     */
    public function getCurso()
    {
        return $this->curso;
    }

    /**
     * @param AcadCurso $curso
     */
    public function setCurso($curso)
    {
        $this->curso = $curso;
    }


    public function toArray()
    {
        return array(
            'matCurId'        => $this->getMatCurId(),
            'matCurDescricao' => $this->getMatCurDescricao(),
            'matCurAno'       => $this->getMatCurAno(),
            'matCurSemestre'  => $this->getMatCurSemestre(),
            'curso'           => $this->getCurso()
        );
    }

}
