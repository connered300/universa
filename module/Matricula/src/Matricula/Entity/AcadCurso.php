<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;
use Pessoa\Entity\PessoaFisica;

/**
 * AcadCurso
 *
 * @ORM\Table(name="acad_curso", indexes={@ORM\Index(name="fk_acad_curso_acad_modalidade1_idx", columns={"mod_id"}), @ORM\Index(name="fk_acad_curso_pessoa_fisica1_idx", columns={"pes_id"}), @ORM\Index(name="fk_acad_curso_acad_nivel1_idx", columns={"nivel_id"})})
 * @ORM\Entity
 */
class AcadCurso
{
    /**
     * @var integer
     *
     * @ORM\Column(name="curso_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $cursoId;

    /**
     * @var string
     *
     * @ORM\Column(name="curso_nome", type="string", length=255, nullable=false)
     */
    private $cursoNome;

    /**
     * @var string
     *
     * @ORM\Column(name="curso_sigla", type="string", length=45, nullable=false)
     */
    private $cursoSigla;

    /**
     * @var string
     *
     * @ORM\Column(name="curso_possui_periodo_letivo", type="string", nullable=false)
     */
    private $cursoPossuiPeriodoLetivo;

    /**
     * @var string
     *
     * @ORM\Column(name="curso_unidade_medida", type="string", nullable=false)
     */
    private $cursoUnidadeMedida;

    /**
     * @var integer
     *
     * @ORM\Column(name="curso_carga_horaria", type="integer", nullable=false)
     */
    private $cursoCargaHoraria;

    /**
     * @var integer
     *
     * @ORM\Column(name="curso_carga_horaria_pratica", type="integer", nullable=true, length=11)
     */
    private $cursoCargaHorariaPratica;

    /**
     * @var integer
     *
     * @ORM\Column(name="curso_carga_horaria_teorica", type="integer", nullable=true, length=11)
     */
    private $cursoCargaHorariaTeorica;

    /**
     * @var string
     *
     * @ORM\Column(name="curso_turno", type="string", nullable=false)
     */
    private $cursoTurno;

    /**
     * @var string
     *
     * @ORM\Column(name="curso_numero_mec", type="string", length=45, nullable=true)
     */
    private $cursoNumeroMec;

    /**
     * @var string
     *
     * @ORM\Column(name="curso_numero_inep", type="integer", nullable=true)
     */
    private $cursoNumeroInep;

    /**
     * @var string
     *
     * @ORM\Column(name="curso_numero_ocde", type="integer", nullable=true)
     */
    private $cursoNumeroOcde;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="curso_data_funcionamento", type="date", nullable=true)
     */
    private $cursoDataFuncionamento;

    /**
     * @var integer
     *
     * @ORM\Column(name="curso_vagas_noturno", type="integer", nullable=true)
     */
    private $cursoVagasNoturno;

    /**
     * @var integer
     *
     * @ORM\Column(name="curso_vagas_vespertino", type="integer", nullable=true)
     */
    private $cursoVagasVespertino;

    /**
     * @var integer
     *
     * @ORM\Column(name="curso_vagas_matutino", type="integer", nullable=true)
     */
    private $cursoVagasMatutino;

    /**
     * @var string
     *
     * @ORM\Column(name="curso_informacoes", type="text", nullable=true)
     */
    private $cursoInformacoes;

    /**
     * @var AcadModalidade
     *
     * @ORM\ManyToOne(targetEntity="AcadModalidade")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mod_id", referencedColumnName="mod_id")
     * })
     */
    private $mod;

    /**
     * @var \Matricula\Entity\AcadNivel
     *
     * @ORM\ManyToOne(targetEntity="\Matricula\Entity\AcadNivel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="nivel_id", referencedColumnName="nivel_id")
     * })
     */
    private $nivel;

    /**
     * @var \Matricula\Entity\AcadgeralAreaConhecimento
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadgeralAreaConhecimento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="area_id", referencedColumnName="area_id")
     * })
     */
    private $area;

    /**
     * @var \Pessoa\Entity\PessoaFisica
     *
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\PessoaFisica")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     */
    private $pes;

    /**
     * @var integer
     *
     * @ORM\Column(name="curso_prazo_integralizacao", type="integer", nullable=true)
     */
    private $cursoPrazoIntegralizacao;

    /**
     * @var integer
     *
     * @ORM\Column(name="curso_prazo_integralizacao_maxima", type="integer", nullable=true)
     */
    private $cursoPrazoIntegralizacaoMaxima;

    /**
     * @var string
     *
     * @ORM\Column(name="curso_situacao", type="string", nullable=false)
     */
    private $cursoSituacao;

    /**
     * @var \GerenciadorArquivos\Entity\Arquivo
     *
     * @ORM\ManyToOne(targetEntity="GerenciadorArquivos\Entity\Arquivo", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="arq_id_carteirinha", referencedColumnName="arq_id")
     * })
     */
    private $arqCarteirinha;

    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getCursoId()
    {
        return $this->cursoId;
    }

    /**
     * @param int $cursoId
     * @return AcadCurso
     */
    public function setCursoId($cursoId)
    {
        $this->cursoId = $cursoId;

        return $this;
    }

    /**
     * @return string
     */
    public function getCursoNome()
    {
        return $this->cursoNome;
    }

    /**
     * @param string $cursoNome
     * @return AcadCurso
     */
    public function setCursoNome($cursoNome)
    {
        $this->cursoNome = $cursoNome;

        return $this;
    }

    /**
     * @return string
     */
    public function getCursoPossuiPeriodoLetivo()
    {
        return $this->cursoPossuiPeriodoLetivo;
    }

    /**
     * @return boolean
     */
    public function verificaSeCursoPossuiPeriodoLetivo()
    {
        return $this->cursoPossuiPeriodoLetivo == \Matricula\Service\AcadCurso::CURSO_POSSUI_PERIODO_LETIVO_SIM;
    }

    /**
     * @param string $cursoPossuiPeriodoLetivo
     * @return AcadCurso
     */
    public function setCursoPossuiPeriodoLetivo($cursoPossuiPeriodoLetivo)
    {
        $this->cursoPossuiPeriodoLetivo = $cursoPossuiPeriodoLetivo;

        return $this;
    }

    /**
     * @return int
     */
    public function getCursoCargaHoraria()
    {
        return $this->cursoCargaHoraria;
    }

    /**
     * @param int $cursoCargaHoraria
     * @return AcadCurso
     */
    public function setCursoCargaHoraria($cursoCargaHoraria)
    {
        $this->cursoCargaHoraria = $cursoCargaHoraria;

        return $this;
    }

    /**
     * @return integer
     */
    public function getCursoCargaHorariaPratica()
    {
        return $this->cursoCargaHorariaPratica;
    }

    /**
     * @param integer $cursoCargaHorariaPratica
     * @return AcadCurso
     */
    public function setCursoCargaHorariaPratica($cursoCargaHorariaPratica)
    {
        $this->cursoCargaHorariaPratica = $cursoCargaHorariaPratica;

        return $this;
    }

    /**
     * @return integer
     */
    public function getCursoCargaHorariaTeorica()
    {
        return $this->cursoCargaHorariaTeorica;
    }

    /**
     * @param integer $cursoCargaHorariaTeorica
     * @return AcadCurso
     */
    public function setCursoCargaHorariaTeorica($cursoCargaHorariaTeorica)
    {
        $this->cursoCargaHorariaTeorica = $cursoCargaHorariaTeorica;

        return $this;
    }

    /**
     * @return string
     */
    public function getCursoTurno()
    {
        return $this->cursoTurno;
    }

    /**
     * @param string $cursoTurno
     * @return AcadCurso
     */
    public function setCursoTurno($cursoTurno)
    {
        if (is_array($cursoTurno)) {
            $cursoTurno = implode(',', $cursoTurno);
        }

        $this->cursoTurno = $cursoTurno;

        return $this;
    }

    /**
     * @return string
     */
    public function getCursoNumeroMec()
    {
        return $this->cursoNumeroMec;
    }

    /**
     * @param string $cursoNumeroMec
     * @return AcadCurso
     */
    public function setCursoNumeroMec($cursoNumeroMec)
    {
        $this->cursoNumeroMec = $cursoNumeroMec;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getCursoDataFuncionamento($format = false)
    {
        $cursoDataFuncionamento = $this->cursoDataFuncionamento;

        if ($format && $cursoDataFuncionamento) {
            $cursoDataFuncionamento = $cursoDataFuncionamento->format('d/m/Y');
        }

        return $cursoDataFuncionamento;
    }

    /**
     * @param \Datetime $cursoDataFuncionamento
     * @return AcadCurso
     */
    public function setCursoDataFuncionamento($cursoDataFuncionamento)
    {
        if ($cursoDataFuncionamento) {
            if (is_string($cursoDataFuncionamento)) {
                $cursoDataFuncionamento = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $cursoDataFuncionamento
                );
                $cursoDataFuncionamento = new \Datetime($cursoDataFuncionamento);
            }
        } else {
            $cursoDataFuncionamento = null;
        }
        $this->cursoDataFuncionamento = $cursoDataFuncionamento;

        return $this;
    }

    /**
     * @return int
     */
    public function getCursoVagasNoturno()
    {
        return $this->cursoVagasNoturno;
    }

    /**
     * @param int $cursoVagasNoturno
     * @return AcadCurso
     */
    public function setCursoVagasNoturno($cursoVagasNoturno)
    {
        $this->cursoVagasNoturno = (int)$cursoVagasNoturno;

        return $this;
    }

    /**
     * @return int
     */
    public function getCursoVagasVespertino()
    {
        return $this->cursoVagasVespertino;
    }

    /**
     * @param int $cursoVagasVespertino
     * @return AcadCurso
     */
    public function setCursoVagasVespertino($cursoVagasVespertino)
    {
        $this->cursoVagasVespertino = (int)$cursoVagasVespertino;

        return $this;
    }

    /**
     * @return int
     */
    public function getCursoVagasMatutino()
    {
        return $this->cursoVagasMatutino;
    }

    /**
     * @param int $cursoVagasMatutino
     * @return AcadCurso
     */
    public function setCursoVagasMatutino($cursoVagasMatutino)
    {
        $this->cursoVagasMatutino = (int)$cursoVagasMatutino;

        return $this;
    }

    /**
     * @return string
     */
    public function getCursoInformacoes()
    {
        return $this->cursoInformacoes;
    }

    /**
     * @param string $cursoInformacoes
     * @return AcadCurso
     */
    public function setCursoInformacoes($cursoInformacoes)
    {
        $this->cursoInformacoes = $cursoInformacoes;

        return $this;
    }

    /**
     * @return AcadModalidade
     */
    public function getMod()
    {
        return $this->mod;
    }

    /**
     * @param AcadModalidade $mod
     * @return AcadCurso
     */
    public function setMod($mod)
    {
        $this->mod = $mod;

        return $this;
    }

    /**
     * @return AcadNivel
     */
    public function getNivel()
    {
        return $this->nivel;
    }

    /**
     * @param AcadNivel $nivel
     * @return AcadCurso
     */
    public function setNivel($nivel)
    {
        $this->nivel = $nivel;

        return $this;
    }

    /**
     * @return PessoaFisica
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @param PessoaFisica $pes
     * @return AcadCurso
     */
    public function setPes($pes)
    {
        $this->pes = $pes;

        return $this;
    }

    /**
     * @return int
     */
    public function getCursoPrazoIntegralizacao()
    {
        return $this->cursoPrazoIntegralizacao;
    }

    /**
     * @param int $cursoPrazoIntegralizacao
     * @return AcadCurso
     */
    public function setCursoPrazoIntegralizacao($cursoPrazoIntegralizacao)
    {
        $this->cursoPrazoIntegralizacao = $cursoPrazoIntegralizacao;

        return $this;
    }

    /**
     * @return string
     */
    public function getCursoSigla()
    {
        return $this->cursoSigla;
    }

    /**
     * @param string $cursoSigla
     * @return AcadCurso
     */
    public function setCursoSigla($cursoSigla)
    {
        $this->cursoSigla = $cursoSigla;

        return $this;
    }

    /**
     * @return string
     */
    public function getCursoNumeroInep()
    {
        return $this->cursoNumeroInep;
    }

    /**
     * @param string $cursoNumeroInep
     * @return AcadCurso
     */
    public function setCursoNumeroInep($cursoNumeroInep)
    {
        $this->cursoNumeroInep = $cursoNumeroInep ? $cursoNumeroInep : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getCursoNumeroOcde()
    {
        return $this->cursoNumeroOcde;
    }

    /**
     * @param string $cursoNumeroOcde
     * @return AcadCurso
     */
    public function setCursoNumeroOcde($cursoNumeroOcde)
    {
        $this->cursoNumeroOcde = $cursoNumeroOcde ? $cursoNumeroOcde : null;

        return $this;
    }

    /**
     * @return AcadgeralAreaConhecimento
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param AcadgeralAreaConhecimento $area
     * @return AcadCurso
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * @return int
     */
    public function getCursoPrazoIntegralizacaoMaxima()
    {
        return $this->cursoPrazoIntegralizacaoMaxima;
    }

    /**
     * @param int $cursoPrazoIntegralizacaoMaxima
     * @return AcadCurso
     */
    public function setCursoPrazoIntegralizacaoMaxima($cursoPrazoIntegralizacaoMaxima)
    {
        $this->cursoPrazoIntegralizacaoMaxima = (int)$cursoPrazoIntegralizacaoMaxima;

        return $this;
    }

    /**
     * @return string
     */
    public function getCursoSituacao()
    {
        return $this->cursoSituacao;
    }

    /**
     * @param string $cursoSituacao
     * @return AcadCurso
     */
    public function setCursoSituacao($cursoSituacao)
    {
        $this->cursoSituacao = $cursoSituacao;

        return $this;
    }

    /**
     * @return string
     */
    public function getCursoUnidadeMedida()
    {
        return $this->cursoUnidadeMedida;
    }

    /**
     * @param string $cursoUnidadeMedida
     * @return AcadCurso
     */
    public function setCursoUnidadeMedida($cursoUnidadeMedida)
    {
        $this->cursoUnidadeMedida = $cursoUnidadeMedida;

        return $this;
    }

    /**
     * @return \GerenciadorArquivos\Entity\Arquivo
     */
    public function getArqCarteirinha()
    {
        return $this->arqCarteirinha;
    }

    /**
     * @param \GerenciadorArquivos\Entity\Arquivo $arqCarteirinha
     * @return AcadCurso
     */
    public function setArqCarteirinha($arqCarteirinha)
    {
        $this->arqCarteirinha = $arqCarteirinha;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'cursoId'                        => $this->getCursoId(),
            'cursoNome'                      => $this->getCursoNome(),
            'cursoSigla'                     => $this->getCursoSigla(),
            'cursoPossuiPeriodoLetivo'       => $this->getCursoPossuiPeriodoLetivo(),
            'cursoCargaHoraria'              => $this->getCursoCargaHoraria(),
            'cursoCargaHorariaPratica'       => $this->getCursoCargaHorariaPratica(),
            'cursoCargaHorariaTeorica'       => $this->getCursoCargaHorariaTeorica(),
            'cursoTurno'                     => $this->getCursoTurno(),
            'cursoNumeroMec'                 => $this->getCursoNumeroMec(),
            'cursoNumeroInep'                => $this->getCursoNumeroInep(),
            'cursoNumeroOcde'                => $this->getCursoNumeroOcde(),
            'cursoDataFuncionamento'         => $this->getCursoDataFuncionamento(true),
            'cursoVagasNoturno'              => $this->getCursoVagasNoturno(),
            'cursoVagasVespertino'           => $this->getCursoVagasVespertino(),
            'cursoVagasMatutino'             => $this->getCursoVagasMatutino(),
            'cursoInformacoes'               => $this->getCursoInformacoes(),
            'mod'                            => '',
            'modNome'                        => '',
            'nivel'                          => '',
            'nivelNome'                      => '',
            'pes'                            => '',
            'pesNome'                        => '',
            'area'                           => '',
            'areaDescricao'                  => '',
            'cursoUnidadeMedida'             => $this->getCursoUnidadeMedida(),
            'cursoPrazoIntegralizacao'       => $this->getCursoPrazoIntegralizacao(),
            'cursoSituacao'                  => $this->getcursoSituacao(),
            'cursoPrazoIntegralizacaoMaxima' => $this->getCursoPrazoIntegralizacaoMaxima(),
            'arqCarteirinha'                 => $this->getArqCarteirinha()
        );

        if ($this->getMod()) {
            $array['mod']     = $this->getMod()->getModId();
            $array['modNome'] = $this->getMod()->getModNome();
        }

        if ($this->getNivel()) {
            $array['nivel']     = $this->getNivel()->getNivelId();
            $array['nivelNome'] = $this->getNivel()->getNivelNome();
        }

        if ($this->getPes()) {
            $array['pes']     = $this->getPes()->getPes()->getPesId();
            $array['pesNome'] = $this->getPes()->getPes()->getPesNome();
        }

        if ($this->getMod()) {
            $array['area']          = $this->getArea()->getAreaId();
            $array['areaDescricao'] = $this->getArea()->getAreaDescricao();
        }

        return $array;
    }
}
