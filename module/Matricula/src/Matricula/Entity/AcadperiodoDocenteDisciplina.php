<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AcadperiodoDocenteDisciplina
 *
 * @ORM\Table(name="acadperiodo__docente_disciplina", uniqueConstraints={@ORM\UniqueConstraint(name="unk_leciona", columns={"disc_id", "turma_id", "docente_id"})}, indexes={@ORM\Index(name="fk_acadperiodo__docente_disciplina_acadgeral__disciplina1_idx", columns={"disc_id"}), @ORM\Index(name="fk_acadperiodo__docente_disciplina_acadperiodo__turma1_idx", columns={"turma_id"}), @ORM\Index(name="fk_acadperiodo__docente_disciplina_acadgeral__docente1_idx", columns={"docente_id"})})
 * @ORM\Entity
 * @LG\LG(id="docdisc_id",label="docdiscId")
 * @Jarvis\Jarvis(title="Docências",icon="fa fa-table")
 */
class AcadperiodoDocenteDisciplina
{
    /**
     * @var integer
     *
     * @ORM\Column(name="docdisc_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="docdisc_id")
     * @LG\Labels\Attributes(text="Indice",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $docdiscId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="docdisc_data_inicio", type="datetime", nullable=false)
     */
    private $docdiscDataInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="docdisc_data_fim", type="datetime", nullable=true)
     */
    private $docdiscDataFim;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="docdisc_data_fechamento", type="datetime", nullable=true)
     */
    private $docdiscDataFechamento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="docdisc_data_fechamento_final", type="datetime", nullable=true)
     */
    private $docdiscDataFechamentoFinal;

    /**
     * @var \Matricula\Entity\AcadgeralDisciplina
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadgeralDisciplina")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="disc_id", referencedColumnName="disc_id")
     * })
     * @LG\Labels\Property(name="disc_id")
     * @LG\Labels\Attributes(text="Disciplina",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     * @LG\Querys\Joins(table="acadgeral__disciplina",joinType="inner",joinCampDest="disc_id",joinCampDestLabel="disc_nome")
     */
    private $disc;

    /**
     * @var \Matricula\Entity\AcadgeralDocente
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadgeralDocente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="docente_id", referencedColumnName="docente_id")
     * })
     * @LG\Labels\Property(name="docente_id")
     * @LG\Labels\Attributes(text="Docente",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     * @LG\Querys\Joins(table="acadgeral__docente",joinType="inner",joinCampDest="docente_id",joinCampDestLabel="docente_id")
     */
    private $docente;

    /**
     * @var \Matricula\Entity\AcadperiodoTurma
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoTurma")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="turma_id", referencedColumnName="turma_id")
     * })
     */
    private $turma;

    /**
     * @return int
     */
    public function getDocdiscId()
    {
        return $this->docdiscId;
    }

    /**
     * @param int $docdiscId
     */
    public function setDocdiscId($docdiscId)
    {
        $this->docdiscId = $docdiscId;
    }

    /**
     * @return \DateTime
     */
    public function getDocdiscDataInicio()
    {
        return $this->docdiscDataInicio;
    }

    /**
     * @param \DateTime $docdiscDataInicio
     */
    public function setDocdiscDataInicio($docdiscDataInicio)
    {
        $this->docdiscDataInicio = $docdiscDataInicio;
    }

    /**
     * @return \DateTime
     */
    public function getDocdiscDataFim()
    {
        return $this->docdiscDataFim;
    }

    /**
     * @param \DateTime $docdiscDataFim
     */
    public function setDocdiscDataFim($docdiscDataFim)
    {
        $this->docdiscDataFim = $docdiscDataFim;
    }

    /**
     * @return \Matricula\Entity\AcadgeralDisciplina
     */
    public function getDisc()
    {
        return $this->disc;
    }

    /**
     * @param \Matricula\Entity\AcadgeralDisciplina $disc
     */
    public function setDisc($disc)
    {
        $this->disc = $disc;
    }

    /**
     * @return \Matricula\Entity\AcadgeralDocente
     */
    public function getDocente()
    {
        return $this->docente;
    }

    /**
     * @param \Matricula\Entity\AcadgeralDocente $docente
     */
    public function setDocente($docente)
    {
        $this->docente = $docente;
    }

    /**
     * @return \Matricula\Entity\AcadperiodoTurma
     */
    public function getTurma()
    {
        return $this->turma;
    }

    /**
     * @param \Matricula\Entity\AcadperiodoTurma $turma
     */
    public function setTurma($turma)
    {
        $this->turma = $turma;
    }

    /**
     * @return \DateTime
     */
    public function getDocdiscDataFechamento()
    {
        return $this->docdiscDataFechamento;
    }

    /**
     * @param \DateTime $docdiscDataFechamento
     * @return AcadperiodoDocenteDisciplina
     */
    public function setDocdiscDataFechamento($docdiscDataFechamento)
    {
        $this->docdiscDataFechamento = $docdiscDataFechamento;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDocdiscDataFechamentoFinal()
    {
        return $this->docdiscDataFechamentoFinal;
    }

    /**
     * @param \DateTime $docdiscDataFechamentoFinal
     * @return AcadperiodoDocenteDisciplina
     */
    public function setDocdiscDataFechamentoFinal($docdiscDataFechamentoFinal)
    {
        $this->docdiscDataFechamentoFinal = $docdiscDataFechamentoFinal;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'docdiscId'                  => $this->getDocdiscId(),
            'docdiscDataInicio'          => $this->getDocdiscDataInicio(),
            'docdiscDataFim'             => $this->getDocdiscDataFim(),
            'docdiscDataFechamento'      => $this->getDocdiscDataFechamento(),
            'docdiscDataFechamentoFinal' => $this->getDocdiscDataFechamentoFinal(),
            'disc'                       => $this->getDisc()->getDiscId(),
            'docente'                    => $this->getDocente()->toArray(),
            'turma'                      => $this->getTurma()->getTurmaId()
        ];
    }
}