<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * CampusCurso
 * @ORM\Table(name="campus_curso", uniqueConstraints={@ORM\UniqueConstraint(name="curso_campus", columns={"camp_id", "curso_id"})}, indexes={@ORM\Index(name="fk_org_campus_acad_curso_acad_curso1_idx", columns={"curso_id"}), @ORM\Index(name="fk_org_campus_acad_curso_org_campus1_idx", columns={"camp_id"})})
 * @ORM\Entity(repositoryClass="Matricula\Entity\Repository\CampusCursoRepository")
 * @LG\LG(id="cursocampus_id",label="curso_id")
 */
class CampusCurso
{
    /**
     * @var integer
     *
     * @ORM\Column(name="cursocampus_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $cursocampusId;

    /**
     * @var AcadCurso
     *
     * @ORM\ManyToOne(targetEntity="AcadCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="curso_id", referencedColumnName="curso_id")
     * })
     */
    private $curso;

    /**
     * @var \Organizacao\Entity\OrgCampus
     *
     * @ORM\ManyToOne(targetEntity="Organizacao\Entity\OrgCampus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="camp_id", referencedColumnName="camp_id")
     * })
     */
    private $camp;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Matricula\Entity\AcadperiodoLetivo", mappedBy="cursoCampus")
     */
    private $per;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Acesso\Entity\AcessoPessoas", mappedBy="cursoCampus")
     */
    private $acessoPessoas;

    /**
     * @var string
     *
     * @ORM\Column(name="cursocampus_autorizacao", type="text", nullable=true)
     */
    private $cursocampusAutorizacao;

    /**
     * @var string
     *
     * @ORM\Column(name="cursocampus_reconhecimento", type="text", nullable=true)
     */
    private $cursocampusReconhecimento;

    public function setAcessoPessoas(\Doctrine\Common\Collections\Collection $acessoPessoas)
    {
        $this->acessoPessoas = $acessoPessoas;

        return $this;
    }

    public function getAcessoPessoas()
    {
        return $this->acessoPessoas;
    }

    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getCursocampusId()
    {
        return $this->cursocampusId;
    }

    /**
     * @param int $cursocampusId
     */
    public function setCursocampusId($cursocampusId)
    {
        $this->cursocampusId = $cursocampusId;
    }

    /**
     * @return AcadCurso
     */
    public function getCurso()
    {
        return $this->curso;
    }

    /**
     * @param AcadCurso $curso
     */
    public function setCurso($curso)
    {
        $this->curso = $curso;
    }

    /**
     * @return \Organizacao\Entity\OrgCampus
     */
    public function getCamp()
    {
        return $this->camp;
    }

    /**
     * @param \Organizacao\Entity\OrgCampus $camp
     */
    public function setCamp($camp)
    {
        $this->camp = $camp;
    }

    public function setPer(\Doctrine\Common\Collections\Collection $per)
    {
        $this->per = $per;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPer()
    {
        return $this->per;
    }

    /**
     * @return string
     */
    public function getCursocampusAutorizacao()
    {
        return $this->cursocampusAutorizacao;
    }

    /**
     * @param string $cursocampusAutorizacao
     * @return CampusCurso
     */
    public function setCursocampusAutorizacao($cursocampusAutorizacao)
    {
        $this->cursocampusAutorizacao = $cursocampusAutorizacao;

        return $this;
    }

    /**
     * @return string
     */
    public function getCursocampusReconhecimento()
    {
        return $this->cursocampusReconhecimento;
    }

    /**
     * @param string $cursocampusReconhecimento
     * @return CampusCurso
     */
    public function setCursocampusReconhecimento($cursocampusReconhecimento)
    {
        $this->cursocampusReconhecimento = $cursocampusReconhecimento;

        return $this;
    }

    public function toArray()
    {
        return array_merge(
            ($this->getCurso() ? $this->getCurso()->toArray() : array()),
            ($this->getCamp() ? $this->getCamp()->toArray() : array()),
            [
                'cursocampusId'             => $this->getCursocampusId(),
                'curso'                     => $this->getCurso()->getCursoId(),
                'camp'                      => $this->getCamp()->getCampId(),
                'campNome'                  => $this->getCamp()->getCampNome(),
                'per'                       => $this->getPer(),
                'acessoPessoas'             => $this->getAcessoPessoas(),
                'nivel'                     => $this->getCurso()->getNivel(),
                'modalidade'                => $this->getCurso()->getMod(),
                'cursocampusAutorizacao'    => $this->getCursocampusAutorizacao(),
                'cursocampusReconhecimento' => $this->getCursocampusReconhecimento(),
            ]
        );
    }
}
