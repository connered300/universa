<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoIntegradora
 *
 * @ORM\Table(name="acadperiodo__integradora", indexes={@ORM\Index(name="fk_acadperiodo__integradora_acadperiodo__letivo1_idx", columns={"per_id"}), @ORM\Index(name="fk_acadperiodo__integradora_acadperiodo__integradora_conf1_idx", columns={"integconf_id"})})
 * @ORM\Entity
 */
class AcadperiodoIntegradora
{
    /**
     * @var integer
     *
     * @ORM\Column(name="integradora_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $integradoraId;

    /**
     * @var string
     *
     * @ORM\Column(name="integradora_status", type="string", nullable=true)
     */
    private $integradoraStatus;

    /**
     * @var AcadperiodoIntegradoraConf
     *
     * @ORM\ManyToOne(targetEntity="AcadperiodoIntegradoraConf")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="integconf_id", referencedColumnName="integconf_id")
     * })
     */
    private $integconf;

    /**
     * @var AcadperiodoLetivo
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoLetivo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="per_id", referencedColumnName="per_id")
     * })
     */
    private $per;

    /**
     * @return int
     */
    public function getIntegradoraId()
    {
        return $this->integradoraId;
    }

    /**
     * @param int $integradoraId
     */
    public function setIntegradoraId($integradoraId)
    {
        $this->integradoraId = $integradoraId;
    }

    /**
     * @return string
     */
    public function getIntegradoraStatus()
    {
        return $this->integradoraStatus;
    }

    /**
     * @param string $integradoraStatus
     */
    public function setIntegradoraStatus($integradoraStatus)
    {
        $this->integradoraStatus = $integradoraStatus;
    }

    /**
     * @return AcadperiodoIntegradoraConf
     */
    public function getIntegconf()
    {
        return $this->integconf;
    }

    /**
     * @param AcadperiodoIntegradoraConf $integconf
     */
    public function setIntegconf($integconf)
    {
        $this->integconf = $integconf;
    }

    /**
     * @return AcadperiodoLetivo
     */
    public function getPer()
    {
        return $this->per;
    }

    /**
     * @param AcadperiodoLetivo $per
     */
    public function setPer($per)
    {
        $this->per = $per;
    }
}
