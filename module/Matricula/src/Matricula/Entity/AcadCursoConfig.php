<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadCursoConfig
 *
 * @ORM\Table(name="acad_curso_config", uniqueConstraints={@ORM\UniqueConstraint(name="index4", columns={"cursoconfig_hist_id", "curso_id"})}, indexes={@ORM\Index(name="fk_acad_curso_config_acad_curso_config1_idx", columns={"cursoconfig_hist_id"}), @ORM\Index(name="fk_acad_curso_config_acad_curso1_idx", columns={"curso_id"}), @ORM\Index(name="fk_acad_curso_config_acadperiodo__letivo1_idx", columns={"per_ativ_id"}), @ORM\Index(name="fk_acad_curso_config_acadperiodo__letivo2_idx", columns={"per_desat_id"})})
 * @ORM\Entity
 */
class AcadCursoConfig
{
    /**
     * @var integer
     *
     * @ORM\Column(name="cursoconfig_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $cursoconfigId;

    /**
     * @var \Matricula\Entity\AcadCurso
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="curso_id", referencedColumnName="curso_id")
     * })
     */
    private $curso;

    /**
     * @var integer
     *
     * @ORM\Column(name="per_ativ_id", type="integer", nullable=true, length=10)
     */
    private $perAtivId;

    /**
     * @var integer
     *
     * @ORM\Column(name="per_desat_id", type="integer", nullable=true, length=10)
     */
    private $perDesatId;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="cursoconfig_data_inicio", type="date", nullable=true)
     */
    private $cursoconfigDataInicio;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="cursoconfig_data_fim", type="date", nullable=true)
     */
    private $cursoconfigDataFim;

    /**
     * @var integer
     *
     * @ORM\Column(name="cursoconfig_hist_id", type="integer", nullable=true)
     */
    private $cursoconfigHistId;

    /**
     * @var float
     *
     * @ORM\Column(name="cursoconfig_nota_max", type="float", precision=10, scale=0, nullable=false)
     */
    private $cursoconfigNotaMax;

    /**
     * @var float
     *
     * @ORM\Column(name="cursoconfig_nota_min", type="float", precision=10, scale=0, nullable=false)
     */
    private $cursoconfigNotaMin;

    /**
     * @var float
     *
     * @ORM\Column(name="cursoconfig_nota_final", type="float", precision=10, scale=0, nullable=false)
     */
    private $cursoconfigNotaFinal;

    /**
     * @var float
     *
     * @ORM\Column(name="cursoconfig_freq_min", type="float", precision=10, scale=0, nullable=false)
     */
    private $cursoconfigFreqMin;

    /**
     * @var float
     *
     * @ORM\Column(name="cursoconfig_media_final_min", type="float", precision=10, scale=0, nullable=false)
     */
    private $cursoconfigMediaFinalMin;

    /**
     * @var integer
     *
     * @ORM\Column(name="cursoconfig_numero_maximo_pendencias", type="integer", nullable=false, length=11)
     */
    private $cursoconfigNumeroMaximoPendencias;

    /**
     * @var string
     *
     * @ORM\Column(name="cursoconfig_metodo", type="string",nullable=false)
     */
    private $cursoconfigMetodo;

    /**
     * @var string
     *
     * @ORM\Column(name="cursoconfig_metodo_final", type="string",nullable=false)
     */
    private $cursoconfigMetodoFinal;

    /**
     * @var string
     *
     * @ORM\Column(name="cursoconfig_nota_fracionada", type="integer",nullable=false)
     */
    private $cursoconfigNotaFracionada;

    /**
     * @var string
     *
     * @ORM\Column(name="cursoconfig_situacao_deferimento", type="string",nullable=false)
     */
    private $cursoconfigSituacaoDeferimento;

    /**
     * @var string
     *
     * @ORM\Column(name="cursoconfig_deferimento_automatico", type="string",nullable=true)
     */
    private $cursoconfigDeferimentoAutomatico;

    /**
     * @var \Matricula\Entity\AcadperiodoMatrizCurricular
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoMatrizCurricular")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cursoconfig_matriz_curricular", referencedColumnName="mat_cur_id")
     * })
     */
    private $matrizCurricular;

    /**
     * @return int
     */
    public function getCursoconfigId()
    {
        return $this->cursoconfigId;
    }

    /**
     * @param int $cursoconfigId
     * @return AcadCursoConfig
     */
    public function setCursoconfigId($cursoconfigId)
    {
        $this->cursoconfigId = $cursoconfigId;

        return $this;
    }

    /**
     * @return \Matricula\Entity\AcadCurso
     */
    public function getCurso()
    {
        return $this->curso;
    }

    /**
     * @param \Matricula\Entity\AcadCurso $curso
     * @return AcadCursoConfig
     */
    public function setCurso($curso)
    {
        $this->curso = $curso;

        return $this;
    }

    /**
     * @return int
     */
    public function getPerAtivId()
    {
        return $this->perAtivId;
    }

    /**
     * @param int $perAtivId
     * @return AcadCursoConfig
     */
    public function setPerAtivId($perAtivId)
    {
        $this->perAtivId = $perAtivId;

        return $this;
    }

    /**
     * @return int
     */
    public function getPerDesatId()
    {
        return $this->perDesatId;
    }

    /**
     * @param int $perDesatId
     * @return AcadCursoConfig
     */
    public function setPerDesatId($perDesatId)
    {
        $this->perDesatId = $perDesatId;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getCursoconfigDataInicio($format = false)
    {
        $cursoconfigDataInicio = $this->cursoconfigDataInicio;

        if ($format && $cursoconfigDataInicio) {
            $cursoconfigDataInicio = $cursoconfigDataInicio->format('d/m/Y');
        }

        return $cursoconfigDataInicio;
    }

    /**
     * @param \Datetime $cursoconfigDataInicio
     * @return AcadCursoConfig
     */
    public function setCursoconfigDataInicio($cursoconfigDataInicio)
    {
        if ($cursoconfigDataInicio) {
            if (is_string($cursoconfigDataInicio)) {
                $cursoconfigDataInicio = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $cursoconfigDataInicio
                );
                $cursoconfigDataInicio = new \Datetime($cursoconfigDataInicio);
            }
        } else {
            $cursoconfigDataInicio = null;
        }
        $this->cursoconfigDataInicio = $cursoconfigDataInicio;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getCursoconfigDataFim($format = false)
    {
        $cursoconfigDataFim = $this->cursoconfigDataFim;

        if ($format && $cursoconfigDataFim) {
            $cursoconfigDataFim = $cursoconfigDataFim->format('d/m/Y');
        }

        return $cursoconfigDataFim;
    }

    /**
     * @param \Datetime $cursoconfigDataFim
     * @return AcadCursoConfig
     */
    public function setCursoconfigDataFim($cursoconfigDataFim)
    {
        if ($cursoconfigDataFim) {
            if (is_string($cursoconfigDataFim)) {
                $cursoconfigDataFim = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $cursoconfigDataFim
                );
                $cursoconfigDataFim = new \Datetime($cursoconfigDataFim);
            }
        } else {
            $cursoconfigDataFim = null;
        }
        $this->cursoconfigDataFim = $cursoconfigDataFim;

        return $this;
    }

    /**
     * @return int
     */
    public function getCursoconfigHistId()
    {
        return $this->cursoconfigHistId;
    }

    /**
     * @param int $cursoconfigHistId
     * @return AcadCursoConfig
     */
    public function setCursoconfigHistId($cursoconfigHistId)
    {
        $this->cursoconfigHistId = $cursoconfigHistId;

        return $this;
    }

    /**
     * @return float
     */
    public function getCursoconfigNotaMax()
    {
        return $this->cursoconfigNotaMax;
    }

    /**
     * @param float $cursoconfigNotaMax
     * @return AcadCursoConfig
     */
    public function setCursoconfigNotaMax($cursoconfigNotaMax)
    {
        $this->cursoconfigNotaMax = $cursoconfigNotaMax;

        return $this;
    }

    /**
     * @return float
     */
    public function getCursoconfigNotaMin()
    {
        return $this->cursoconfigNotaMin;
    }

    /**
     * @param float $cursoconfigNotaMin
     * @return AcadCursoConfig
     */
    public function setCursoconfigNotaMin($cursoconfigNotaMin)
    {
        $this->cursoconfigNotaMin = $cursoconfigNotaMin;

        return $this;
    }

    /**
     * @return float
     */
    public function getCursoconfigNotaFinal()
    {
        return $this->cursoconfigNotaFinal;
    }

    /**
     * @param float $cursoconfigNotaFinal
     * @return AcadCursoConfig
     */
    public function setCursoconfigNotaFinal($cursoconfigNotaFinal)
    {
        $this->cursoconfigNotaFinal = $cursoconfigNotaFinal;

        return $this;
    }

    /**
     * @return float
     */
    public function getCursoconfigFreqMin()
    {
        return $this->cursoconfigFreqMin;
    }

    /**
     * @param float $cursoconfigFreqMin
     * @return AcadCursoConfig
     */
    public function setCursoconfigFreqMin($cursoconfigFreqMin)
    {
        $this->cursoconfigFreqMin = $cursoconfigFreqMin;

        return $this;
    }

    /**
     * @return float
     */
    public function getCursoconfigMediaFinalMin()
    {
        return $this->cursoconfigMediaFinalMin;
    }

    /**
     * @param float $cursoconfigMediaFinalMin
     * @return AcadCursoConfig
     */
    public function setCursoconfigMediaFinalMin($cursoconfigMediaFinalMin)
    {
        $this->cursoconfigMediaFinalMin = $cursoconfigMediaFinalMin;

        return $this;
    }

    /**
     * @return integer
     */
    public function getCursoconfigNumeroMaximoPendencias()
    {
        return $this->cursoconfigNumeroMaximoPendencias;
    }

    /**
     * @param integer $cursoconfigNumeroMaximoPendencias
     * @return AcadCursoConfig
     */
    public function setCursoconfigNumeroMaximoPendencias($cursoconfigNumeroMaximoPendencias)
    {
        $this->cursoconfigNumeroMaximoPendencias = $cursoconfigNumeroMaximoPendencias;

        return $this;
    }

    /**
     * @return string
     */
    public function getCursoconfigMetodo()
    {
        return $this->cursoconfigMetodo;
    }

    /**
     * @param $metodo
     */
    public function setCursoconfigMetodo($metodo)
    {
        $this->cursoconfigMetodo = $metodo;
    }

    /**
     * @return string
     */
    public function getCursoconfigMetodoFinal()
    {
        return $this->cursoconfigMetodoFinal;
    }

    /**
     * @param $metodoFinal
     */
    public function setCursoconfigMetodoFinal($metodoFinal)
    {
        $this->cursoconfigMetodoFinal = $metodoFinal;
    }

    /**
     * @return AcadperiodoMatrizCurricular
     */
    public function getMatrizCurricular()
    {
        return $this->matrizCurricular;
    }

    /**
     * @param \Matricula\Entity\AcadperiodoMatrizCurricular $matrizCurricular
     * @return $this
     */
    public function setMatrizCurricular($matrizCurricular)
    {
        $this->matrizCurricular = $matrizCurricular;

        return $this;
    }

    /**
     * @return string
     */
    public function getCursoconfigNotaFracionada()
    {
        return $this->cursoconfigNotaFracionada;
    }

    /**
     * @param integer $cursoconfigNotaFracionada
     * @return AcadCursoConfig
     */
    public function setCursoconfigNotaFracionada($cursoconfigNotaFracionada)
    {
        $this->cursoconfigNotaFracionada = $cursoconfigNotaFracionada;

        return $this;
    }

    /**
     * @return string
     */
    public function getCursoconfigSituacaoDeferimento()
    {
        return $this->cursoconfigSituacaoDeferimento;
    }

    /**
     * @param string $cursoconfigSituacaoDeferimento
     * @return AcadCursoConfig
     */
    public function setCursoconfigSituacaoDeferimento($cursoconfigSituacaoDeferimento)
    {
        $this->cursoconfigSituacaoDeferimento = $cursoconfigSituacaoDeferimento;

        return $this;
    }

    /**
     * @return string
     */
    public function getCursoconfigDeferimentoAutomatico()
    {
        return $this->cursoconfigDeferimentoAutomatico;
    }

    /**
     * @return bool
     */
    public function verificarDeferimentoAutomatico()
    {
        return $this->cursoconfigDeferimentoAutomatico == \Matricula\Service\AcadCursoConfig::DEFERIMENTO_AUTOMATICO_SIM;
    }

    /**
     * @param string $valor
     * @return AcadCursoConfig
     */
    public function setCursoconfigDeferimentoAutomatico($valor)
    {
        $this->cursoconfigDeferimentoAutomatico = $valor;

        return $this;
    }

    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    public function toArray()
    {
        return array(
            'cursoconfigId'                     => $this->getCursoconfigId(),
            'cursoId'                           => $this->getCurso() ? $this->getCurso()->getCursoId() : null,
            'perAtivId'                         => $this->getPerAtivId(),
            'perDesatId'                        => $this->getPerDesatId(),
            'cursoconfigDataInicio'             => $this->getCursoconfigDataInicio(true),
            'cursoconfigDataFim'                => $this->getCursoconfigDataFim(true),
            'cursoconfigHistId'                 => $this->getCursoconfigHistId(),
            'cursoconfigNotaMax'                => $this->getCursoconfigNotaMax(),
            'cursoconfigNotaMin'                => $this->getCursoconfigNotaMin(),
            'cursoconfigNotaFinal'              => $this->getCursoconfigNotaFinal(),
            'cursoconfigFreqMin'                => $this->getCursoconfigFreqMin(),
            'cursoconfigMediaFinalMin'          => $this->getCursoconfigMediaFinalMin(),
            'cursoconfigNumeroMaximoPendencias' => $this->getCursoconfigNumeroMaximoPendencias(),
            'cursoconfigNotaFracionada'         => $this->getCursoconfigNotaFracionada(),
            'cursoconfigSituacaoDeferimento'    => $this->getCursoconfigSituacaoDeferimento(),
            'regraCalculoNota'                  => $this->getCursoconfigMetodo(),
            'regraCalculoNotaFinal'             => $this->getCursoconfigMetodoFinal(),
            'matrizCurricular'                  => $this->getMatrizCurricular(),
            'cursoconfigDeferimentoAutomatico'  => $this->getCursoconfigDeferimentoAutomatico()
        );
    }
}

