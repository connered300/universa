<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AcadgeralDisciplinaEmenta
 *
 * @ORM\Table(name="acadgeral__disciplina_ementa", uniqueConstraints={@ORM\UniqueConstraint(name="index4", columns={"arq_id"})}, indexes={@ORM\Index(name="fk_disciplina_ementa_disciplina_ementa1_idx", columns={"ementa_hist"}), @ORM\Index(name="fk_disciplina_ementa_disciplina1_idx", columns={"disc_id"})})
 * @ORM\Entity
 * @LG\LG(id="ementaId",label="ementa_id")
 * @Jarvis\Jarvis(title="Ementa Curso",icon="fa fa-table")
 */
class AcadgeralDisciplinaEmenta
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ementa_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $ementaId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ementa_data", type="date", nullable=false)
     */
    private $ementaData;

    /**
     * @var \Matricula\Entity\AcadgeralDisciplinaCurso
     *
     * @ORM\ManyToOne(targetEntity="AcadgeralDisciplinaCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="disc_curso_id", referencedColumnName="disc_curso_id")
     * })
     */
    private $discCurso;

    /**
     * @var \Matricula\Entity\AcadgeralDisciplinaCurso
     *
     * @ORM\ManyToOne(targetEntity="AcadgeralDisciplinaEmenta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ementa_hist", referencedColumnName="ementa_id")
     * })
     */
    private $ementaHist;

    /**
     * @var \GerenciadorArquivos\Entity\Arquivo
     *
     * @ORM\ManyToOne(targetEntity="Arquivo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="arq_id", referencedColumnName="arq_id")
     * })
     */
    private $arq;

    public function __construct(  array $data ) {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getEmentaId()
    {
        return $this->ementaId;
    }

    /**
     * @param int $ementaId
     */
    public function setEmentaId($ementaId)
    {
        $this->ementaId = $ementaId;
    }

    /**
     * @return \GerenciadorArquivos\Entity\Arquivo
     */
    public function getArq()
    {
        return $this->arq;
    }

    /**
     * @param \GerenciadorArquivos\Entity\Arquivo $arq
     */
    public function setArq($arq)
    {
        $this->arq = $arq;
    }

    /**
     * @return \Matricula\Entity\AcadgeralDisciplinaCurso
     */
    public function getDiscCurso()
    {
        return $this->discCurso;
    }

    /**
     * @param \Matricula\Entity\AcadgeralDisciplinaCurso $discCurso
     */
    public function setDiscCurso($discCurso)
    {
        $this->discCurso = $discCurso;
    }

    /**
     * @return \DateTime
     */
    public function getEmentaData()
    {
        return $this->ementaData;
    }

    /**
     * @param \DateTime $ementaData
     */
    public function setEmentaData($ementaData)
    {
        $this->ementaData = $ementaData;
    }

    /**
     * @return \Matricula\Entity\AcadgeralDisciplinaCurso
     */
    public function getEmentaHist()
    {
        return $this->ementaHist;
    }

    /**
     * @param \Matricula\Entity\AcadgeralDisciplinaCurso $ementaHist
     */
    public function setEmentaHist($ementaHist)
    {
        $this->ementaHist = $ementaHist;
    }
}
