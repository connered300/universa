<?php namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoAlunoResumo
 *
 * @ORM\Table(name="acadperiodo__aluno_resumo", indexes={@ORM\Index(name="fk_acadperiodo_aluno_acadgeral__aluno_curso1_idx", columns={"alunocurso_id"}), @ORM\Index(name="fk_acadperiodo__aluno_resumo_acadgeral__disciplina1_idx", columns={"disc_id"}), @ORM\Index(name="alunoper_id", columns={"alunoper_id"})})
 * @ORM\Entity
 */
class AcadperiodoAlunoResumo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="res_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $resId;

    /**
     * @var integer
     *
     * @ORM\Column(name="alunocurso_id", type="integer", nullable=false)
     */
    private $alunocursoId;

    /**
     * @var integer
     *
     * @ORM\Column(name="disc_id", type="integer", nullable=false)
     */
    private $discId;

    /**
     * @var integer
     *
     * @ORM\Column(name="resaluno_serie", type="integer", nullable=true)
     */
    private $resalunoSerie;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="resaluno_data_criacao", type="datetime", nullable=false)
     */
    private $resalunoDataCriacao;

    /**
     * @var string
     *
     * @ORM\Column(name="resaluno_situacao", type="string", length=20, nullable=false)
     */
    private $resalunoSituacao;

    /**
     * @var float
     *
     * @ORM\Column(name="resaluno_nota", type="float", precision=10, scale=0, nullable=true)
     */
    private $resalunoNota;

    /**
     * @var integer
     *
     * @ORM\Column(name="resaluno_faltas", type="integer", nullable=true)
     */
    private $resalunoFaltas;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="resaluno_data_alteracao", type="datetime", nullable=true)
     */
    private $resalunoDataAlteracao;

    /**
     * @var integer
     *
     * @ORM\Column(name="resaluno_carga_horaria", type="integer", nullable=true)
     */
    private $resalunoCargaHoraria;

    /**
     * @var string
     *
     * @ORM\Column(name="resaluno_ano", type="string", nullable=true)
     */
    private $resalunoAno;

    /**
     * @var integer
     *
     * @ORM\Column(name="resaluno_semestre", type="integer", nullable=true)
     */
    private $resalunoSemestre;

    /**
     * @var string
     *
     * @ORM\Column(name="resaluno_origem", type="string", nullable=false)
     */
    private $resalunoOrigem = 'Sistema';

    /**
     * @var \Matricula\Entity\AcadperiodoAluno
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoAluno")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="alunoper_id", referencedColumnName="alunoper_id")
     * })
     */
    private $alunoper;

    /**
     * @var string
     *
     * @ORM\Column(name="resaluno_detalhe", type="string", nullable=true)
     */
    private $resalunoDetalhe;

    /**
     * @return int
     */
    public function getResId()
    {
        return $this->resId;
    }

    /**
     * @param int $resId
     * @return AcadperiodoAlunoResumo
     */
    public function setResId($resId)
    {
        $this->resId = $resId;

        return $this;
    }

    /**
     * @return int
     */
    public function getAlunocursoId()
    {
        return $this->alunocursoId;
    }

    /**
     * @param int $alunocursoId
     * @return AcadperiodoAlunoResumo
     */
    public function setAlunocursoId($alunocursoId)
    {
        $this->alunocursoId = $alunocursoId;

        return $this;
    }

    /**
     * @return int
     */
    public function getDiscId()
    {
        return $this->discId;
    }

    /**
     * @param int $discId
     * @return AcadperiodoAlunoResumo
     */
    public function setDiscId($discId)
    {
        $this->discId = $discId;

        return $this;
    }

    /**
     * @return int
     */
    public function getResalunoSerie()
    {
        return $this->resalunoSerie;
    }

    /**
     * @param int $resalunoSerie
     * @return AcadperiodoAlunoResumo
     */
    public function setResalunoSerie($resalunoSerie)
    {
        $this->resalunoSerie = $resalunoSerie;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getResalunoDataCriacao()
    {
        return $this->resalunoDataCriacao;
    }

    /**
     * @param \DateTime $resalunoDataCriacao
     * @return AcadperiodoAlunoResumo
     */
    public function setResalunoDataCriacao($resalunoDataCriacao)
    {
        $this->resalunoDataCriacao = $resalunoDataCriacao;

        return $this;
    }

    /**
     * @return string
     */
    public function getResalunoSituacao()
    {
        return $this->resalunoSituacao;
    }

    /**
     * @param string $resalunoSituacao
     * @return AcadperiodoAlunoResumo
     */
    public function setResalunoSituacao($resalunoSituacao)
    {
        $this->resalunoSituacao = $resalunoSituacao;

        return $this;
    }

    /**
     * @return float
     */
    public function getResalunoNota()
    {
        return $this->resalunoNota;
    }

    /**
     * @param float $resalunoNota
     * @return AcadperiodoAlunoResumo
     */
    public function setResalunoNota($resalunoNota)
    {
        $this->resalunoNota = $resalunoNota;

        return $this;
    }

    /**
     * @return int
     */
    public function getResalunoFaltas()
    {
        return $this->resalunoFaltas;
    }

    /**
     * @param int $resalunoFaltas
     * @return AcadperiodoAlunoResumo
     */
    public function setResalunoFaltas($resalunoFaltas)
    {
        $this->resalunoFaltas = $resalunoFaltas;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getResalunoDataAlteracao()
    {
        return $this->resalunoDataAlteracao;
    }

    /**
     * @param \DateTime $resalunoDataAlteracao
     * @return AcadperiodoAlunoResumo
     */
    public function setResalunoDataAlteracao($resalunoDataAlteracao)
    {
        $this->resalunoDataAlteracao = $resalunoDataAlteracao;

        return $this;
    }

    /**
     * @return int
     */
    public function getResalunoCargaHoraria()
    {
        return $this->resalunoCargaHoraria;
    }

    /**
     * @param int $resalunoCargaHoraria
     * @return AcadperiodoAlunoResumo
     */
    public function setResalunoCargaHoraria($resalunoCargaHoraria)
    {
        $this->resalunoCargaHoraria = $resalunoCargaHoraria;

        return $this;
    }

    /**
     * @return string
     */
    public function getResalunoAno()
    {
        return $this->resalunoAno;
    }

    /**
     * @param string $resalunoAno
     * @return AcadperiodoAlunoResumo
     */
    public function setResalunoAno($resalunoAno)
    {
        $this->resalunoAno = $resalunoAno;

        return $this;
    }

    /**
     * @return int
     */
    public function getResalunoSemestre()
    {
        return $this->resalunoSemestre;
    }

    /**
     * @param int $resalunoSemestre
     * @return AcadperiodoAlunoResumo
     */
    public function setResalunoSemestre($resalunoSemestre)
    {
        $this->resalunoSemestre = $resalunoSemestre;

        return $this;
    }

    /**
     * @return AcadperiodoAluno
     */
    public function getAlunoper()
    {
        return $this->alunoper;
    }

    /**
     * @param AcadperiodoAluno $alunoper
     * @return AcadperiodoAlunoResumo
     */
    public function setAlunoper($alunoper)
    {
        $this->alunoper = $alunoper;

        return $this;
    }

    /**
     * @return string
     */
    public function getResalunoOrigem()
    {
        return $this->resalunoOrigem;
    }

    /**
     * @param string $resalunoOrigem
     * @return AcadperiodoAlunoResumo
     */
    public function setResalunoOrigem($resalunoOrigem)
    {
        $this->resalunoOrigem = $resalunoOrigem;

        return $this;
    }

    /**
     * @return string
     */
    public function getResalunoDetalhe()
    {
        return $this->resalunoDetalhe;
    }

    /**
     * @param string $resalunoDetalhe
     * @return AcadperiodoAlunoResumo
     */
    public function setResalunoDetalhe($resalunoDetalhe)
    {
        $this->resalunoDetalhe = $resalunoDetalhe;

        return $this;
    }

    public function toArray()
    {
        return array(
            'resId' => $this->getResId(),
            'alunocursoId' => $this->getAlunocursoId(),
            'discId' => $this->getDiscId(),
            'resalunoSerie' => $this->getResalunoSerie(),
            'resalunoDataCriacao' => $this->getResalunoDataCriacao(),
            'resalunoSituacao' => $this->getResalunoSituacao(),
            'resalunoNota' => $this->getResalunoNota(),
            'resalunoFaltas' => $this->getResalunoFaltas(),
            'resalunoDataAlteracao' => $this->getResalunoDataAlteracao(),
            'resalunoCargaHoraria' => $this->getResalunoCargaHoraria(),
            'resalunoAno' => $this->getResalunoAno(),
            'resalunoSemestre' => $this->getResalunoSemestre(),
            'alunoper' => $this->getAlunoper(),
            'resalunoOrigem' => $this->getResalunoOrigem(),
            'resalunoDetalhe' => $this->getResalunoDetalhe(),
        );
    }
}
