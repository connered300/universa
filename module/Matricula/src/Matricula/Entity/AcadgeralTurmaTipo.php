<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AcadgeralTurmaTipo
 *
 * @ORM\Table(name="acadgeral__turma_tipo")
 * @ORM\Entity
 * @LG\LG(id="tturma_id",label="tturma_descricao")
 */
class AcadgeralTurmaTipo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="tturma_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $tturmaId;

    /**
     * @var string
     *
     * @ORM\Column(name="tturma_descricao", type="string", length=45, nullable=false)
     */
    private $tturmaDescricao;

    /**
     * @var string
     *
     * @ORM\Column(name="tturma_mista", type="string", nullable=true)
     */
    private $tturmaMista;

    public function __construct(  array $data ) {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getTturmaId()
    {
        return $this->tturmaId;
    }

    /**
     * @param int $tturmaId
     */
    public function setTturmaId($tturmaId)
    {
        $this->tturmaId = $tturmaId;
    }

    /**
     * @return string
     */
    public function getTturmaDescricao()
    {
        return $this->tturmaDescricao;
    }

    /**
     * @param string $tturmaDescricao
     */
    public function setTturmaDescricao($tturmaDescricao)
    {
        $this->tturmaDescricao = $tturmaDescricao;
    }

    /**
     * @return string
     */
    public function getTturmaMista()
    {
        return $this->tturmaMista;
    }

    /**
     * @param string $tturmaMista
     */
    public function setTturmaMista($tturmaMista)
    {
        $this->tturmaMista = $tturmaMista;
    }

}
