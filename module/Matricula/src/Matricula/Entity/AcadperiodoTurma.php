<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AcadperiodoTurma
 *
 * @ORM\Table(name="acadperiodo__turma", indexes={@ORM\Index(name="fk_periodo_turma_campus_curso1_idx", columns={"cursocampus_id"}), @ORM\Index(name="fk_acadperiodo__turma_acadperiodo_letivo1_idx", columns={"per_id"}), @ORM\Index(name="fk_acadperiodo__turma_infra_sala1_idx", columns={"sala_id"}), @ORM\Index(name="fk_acadperiodo__turma_acadgeral__turma_tipo1_idx", columns={"tturma_id"}), @ORM\Index(name="fk_acadperiodo__turma_acadgeral__disciplina1_idx", columns={"disc_id"}), @ORM\Index(name="fk_acadperiodo__turma_acadperiodo__matriz_curricular1_idx", columns={"mat_cur_id"}), @ORM\Index(name="fk_acadperiodo__turma_acadperiodo__turma1_idx", columns={"turma_origem"})})
 * @ORM\Entity(repositoryClass="Matricula\Entity\Repository\AcadperiodoTurma")
 * @LG\LG(id="turma_id",label="turma_nome")
 */
class AcadperiodoTurma
{
    static $ordens = [
        'A' => 'A',
        'B' => 'B',
        'C' => 'C',
        'D' => 'D',
        'E' => 'E',
        'F' => 'F',
        'G' => 'G',
        'H' => 'H',
        'I' => 'I',
        'J' => 'J',
    ];

    /**
     * @var integer
     *
     * @ORM\Column(name="turma_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="turma_id")
     * @LG\Labels\Attributes(text="Indice",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $turmaId;

    /**
     * @var string
     *
     * @ORM\Column(name="turma_nome", type="string", length=45, nullable=false)
     * @LG\Labels\Property(name="turma_nome")
     * @LG\Labels\Attributes(text="Nome da Turma",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE%")
     */
    private $turmaNome;

    /**
     * @var \Matricula\Entity\AcadperiodoLetivo
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoLetivo")                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        )
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="per_id", referencedColumnName="per_id")
     * })
     *  * @LG\Labels\Property(name="per_id")
     * @LG\Labels\Attributes(text="Semestre Letivo",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE")
     * @LG\Querys\Joins(table="acadperiodo__letivo",joinType="inner",joinCampDest="per_id",joinCampDestLabel="per_nome")
     */
    private $per;

    /**
     * @var \Organizacao\Entity\OrgUnidadeEstudo
     * @ORM\ManyToOne(targetEntity="Organizacao\Entity\OrgUnidadeEstudo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="unidade_id", referencedColumnName="unidade_id")
     * })
     *
     * * @LG\Labels\Property(name="unidade_id")
     * @LG\Labels\Attributes(text="Unidade de estudo",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE")
     * @LG\Querys\Joins(table="org__unidade_estudo",joinType="inner",joinCampDest="unidade_id",joinCampDestLabel="unidade_nome")
     */
    private $unidade;

    /**
     * @var string
     *
     * @ORM\Column(name="turma_turno", type="string", nullable=false)
     * @LG\Labels\Property(name="turma_turno")
     * @LG\Labels\Attributes(text="Turno",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE%")
     */
    private $turmaTurno;

    /**
     * @var string
     *
     * @ORM\Column(name="turma_ordem", type="string", length=2, nullable=true)
     */
    private $turmaOrdem;

    /**
     * @var integer
     *
     * @ORM\Column(name="turma_serie", type="integer", nullable=true)
     * @LG\Labels\Property(name="turma_serie")
     * @LG\Labels\Attributes(text="Serie",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE%")
     */
    private $turmaSerie;

    /**
     * @var integer
     *
     * @ORM\Column(name="turma_capacidade", type="integer", nullable=true)
     * @LG\Labels\Property(name="turma_capacidade")
     * @LG\Labels\Attributes(text="Capacidade",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE%")
     */
    private $turmaCapacidade;

    /**
     * @var integer
     *
     * @ORM\Column(name="turma_qtd_matricula", type="integer", nullable=true)
     */
    private $turmaQtdMatricula;

    /**
     * @var \Matricula\Entity\AcadgeralDisciplina
     *
     * @ORM\ManyToOne(targetEntity="AcadgeralDisciplina")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="disc_id", referencedColumnName="disc_id")
     * })
     */
    private $disc;

    /**
     * @var \Matricula\Entity\AcadgeralTurmaTipo
     *
     * @ORM\ManyToOne(targetEntity="AcadgeralTurmaTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tturma_id", referencedColumnName="tturma_id")
     * })
     */
    private $tturma;

    /**
     * @var \Matricula\Entity\AcadperiodoMatrizCurricular
     *
     * @ORM\ManyToOne(targetEntity="AcadperiodoMatrizCurricular")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mat_cur_id", referencedColumnName="mat_cur_id")
     * })
     */
    private $matCur;

    /**
     * @var \Matricula\Entity\AcadperiodoTurma
     *
     * @ORM\ManyToOne(targetEntity="AcadperiodoTurma")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="turma_origem", referencedColumnName="turma_id")
     * })
     */
    private $turmaOrigem;

    /**
     * @var \Infraestrutura\Entity\InfraSala
     *
     * @ORM\ManyToOne(targetEntity="Infraestrutura\Entity\InfraSala")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sala_id", referencedColumnName="sala_id")
     * })
     */
    private $sala;

    /**
     * @var \Matricula\Entity\CampusCurso
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\CampusCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cursocampus_id", referencedColumnName="cursocampus_id")
     * })
     */
    private $cursocampus;

    public function __construct(array $data)
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getTurmaId()
    {
        return $this->turmaId;
    }

    /**
     * @param int $turmaId
     */
    public function setTurmaId($turmaId)
    {
        $this->turmaId = $turmaId;
    }

    /**
     * @return string
     */
    public function getTurmaNome()
    {
        return $this->turmaNome;
    }

    /**
     * @param string $turmaNome
     */
    public function setTurmaNome($turmaNome)
    {
        $this->turmaNome = $turmaNome;
    }

    /**
     * @return string
     */
    public function getTurmaTurno()
    {
        return $this->turmaTurno;
    }

    /**
     * @param string $turmaTurno
     */
    public function setTurmaTurno($turmaTurno)
    {
        $this->turmaTurno = $turmaTurno;
    }

    /**
     * @return int
     */
    public function getTurmaSerie()
    {
        return $this->turmaSerie;
    }

    /**
     * @param boolean $turmaSerie
     */
    public function setTurmaSerie($turmaSerie)
    {
        $this->turmaSerie = $turmaSerie;
    }

    /**
     * @return int
     */
    public function getTurmaCapacidade()
    {
        return $this->turmaCapacidade;
    }

    /**
     * @param int $turmaCapacidade
     */
    public function setTurmaCapacidade($turmaCapacidade)
    {
        $this->turmaCapacidade = $turmaCapacidade;
    }

    /**
     * @return int
     */
    public function getTurmaQtdMatricula()
    {
        return $this->turmaQtdMatricula;
    }

    /**
     * @param int $turmaQtdMatricula
     */
    public function setTurmaQtdMatricula($turmaQtdMatricula)
    {
        $this->turmaQtdMatricula = $turmaQtdMatricula;
    }

    /**
     * @return \Matricula\Entity\AcadgeralDisciplina
     */
    public function getDisc()
    {
        return $this->disc;
    }

    /**
     * @param \Matricula\Entity\AcadgeralDisciplina $disc
     */
    public function setDisc($disc)
    {
        $this->disc = $disc;
    }

    /**
     * @return \Matricula\Entity\AcadgeralTurmaTipo
     */
    public function getTturma()
    {
        return $this->tturma;
    }

    /**
     * @param \Matricula\Entity\AcadgeralTurmaTipo $tturma
     */
    public function setTturma($tturma)
    {
        $this->tturma = $tturma;
    }

    /**
     * @return \Matricula\Entity\AcadperiodoMatrizCurricular
     */
    public function getMatCur()
    {
        return $this->matCur;
    }

    /**
     * @param \Matricula\Entity\AcadperiodoMatrizCurricular $matCur
     */
    public function setMatCur($matCur)
    {
        $this->matCur = $matCur;
    }

    /**
     * @return \Matricula\Entity\AcadperiodoLetivo
     */
    public function getPer()
    {
        return $this->per;
    }

    /**
     * @param \Matricula\Entity\AcadperiodoLetivo $per
     */
    public function setPer($per)
    {
        $this->per = $per;
    }

    /**
     * @return \Infraestrutura\Entity\InfraSala
     */
    public function getSala()
    {
        return $this->sala;
    }

    /**
     * @param \Infraestrutura\Entity\InfraSala $sala
     */
    public function setSala($sala)
    {
        $this->sala = $sala;
    }

    /**
     * @return \Matricula\Entity\CampusCurso
     */
    public function getCursocampus()
    {
        return $this->cursocampus;
    }

    /**
     * @param \Matricula\Entity\CampusCurso $cursocampus
     */
    public function setCursocampus($cursocampus)
    {
        $this->cursocampus = $cursocampus;
    }

    /**
     * @return string
     */
    public function getTurmaOrdem()
    {
        return $this->turmaOrdem;
    }

    /**
     * @param string $turmaOrdem
     */
    public function setTurmaOrdem($turmaOrdem)
    {
        $this->turmaOrdem = $turmaOrdem;
    }

    /**
     * @return \Matricula\Entity\AcadperiodoTurma
     */
    public function getTurmaOrigem()
    {
        return $this->turmaOrigem;
    }

    /**
     * @param \Matricula\Entity\AcadperiodoTurma $turmaOrigem
     */
    public function setTurmaOrigem($turmaOrigem)
    {
        $this->turmaOrigem = $turmaOrigem;
    }

    /**
     * @return \Organizacao\Entity\OrgUnidadeEstudo
     */
    public function getUnidade()
    {
        return $this->unidade;
    }

    /**
     * @param \Organizacao\Entity\OrgUnidadeEstudo $unidadeId
     *
     */
    public function setUnidade($unidadeId)
    {
        $this->unidade = $unidadeId;
    }

    public function toArray()
    {
        return array(
            'turmaId'           => $this->getTurmaId(),
            'turmaNome'         => $this->getTurmaNome(),
            'turmaTurno'        => $this->getTurmaTurno(),
            'turmaSerie'        => $this->getTurmaSerie(),
            'turmaCapacidade'   => $this->getTurmaCapacidade(),
            'turmaQtdMatricula' => $this->getTurmaQtdMatricula(),
            'turmaOrdem'        => $this->getTurmaOrdem(),
            'disc'              => $this->getDisc(),
            'tturma'            => $this->getTturma() ? $this->getTturma()->getTturmaId() : '',
            'matCur'            => $this->getMatCur() ? $this->getMatCur()->getMatCurId() : '',
            'per'               => $this->getPer() ? $this->getPer()->getPerId() : '',
            'sala'              => $this->getSala(),
            'cursocampus'       => $this->getCursocampus() ? $this->getCursocampus()->getCursocampusId() : '',
            'turmaOrigem'       => $this->getTurmaOrigem(),
            'unidade'           => $this->getUnidade(),
        );
    }

    public function toArrayReal()
    {
        return array(
            'turmaId'           => $this->getTurmaId(),
            'turmaNome'         => $this->getTurmaNome(),
            'turmaTurno'        => $this->getTurmaTurno(),
            'turmaSerie'        => $this->getTurmaSerie(),
            'turmaCapacidade'   => $this->getTurmaCapacidade(),
            'turmaQtdMatricula' => $this->getTurmaQtdMatricula(),
            'turmaOrdem'        => $this->getTurmaOrdem(),
            'disc'              => $this->getDisc(),
            'tturma'            => $this->getTturma(),
            'matCur'            => $this->getMatCur(),
            'per'               => $this->getPer(),
            'sala'              => $this->getSala(),
            'cursocampus'       => $this->getCursocampus(),
            'turmaOrigem'       => $this->getTurmaOrigem(),
            'unidade'           => $this->getUnidade(),
        );
    }
}
