<?php

namespace Matricula\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class CampusCursoRepository extends EntityRepository
{
    public function buscaCursos($campusId = null)
    {
        $arrParams = [];

        if ($campusId) {
            $arrParams = ['camp' => $campusId];
        }

        $cursos = $this->findBy($arrParams);

        $array = array();
        /** @var \Matricula\Entity\CampusCurso $curso */
        foreach ($cursos as $curso) {
            $array[$curso->getCurso()->getCursoId()] = array(
                $curso->getCurso()->getCursoNome(),
                $curso->getCurso()->getCursoPrazoIntegralizacao(),
                $curso->getCursocampusId(),
                'cursoPossuiPeriodoLetivo' => $curso->getCurso()->getCursoPossuiPeriodoLetivo(),
            );
        }

        return $array;
    }
}