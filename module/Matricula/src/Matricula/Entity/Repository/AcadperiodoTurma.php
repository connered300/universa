<?php


namespace Matricula\Entity\Repository;


use Doctrine\ORM\EntityRepository;

class AcadperiodoTurma extends EntityRepository{
    public function buscaTurmas( $cursocampus_id, $serie, $periodo = 'Todos' ){
        if($periodo === 'Todos'){
            $turmas = $this->findBy(array('cursocampus' => $cursocampus_id, 'turmaSerie' => $serie ));
        }else{
            $turmas = $this->findBy(array('cursocampus' => $cursocampus_id, 'turmaSerie' => $serie, 'per' => $periodo ));
        }

        $array = array();
        foreach ($turmas as $turma) {
            $array[] = array(
                'turmaNome'         => $turma->getTurmaNome(),
                'turmaCapacidade'   => $turma->getTurmaCapacidade(),
                'turmaQtdMatricula' => $turma->getTurmaQtdMatricula(),
                'turmaId'           => $turma->getTurmaId(),
                'matrizId'          => $turma->getMatCur() == NULL ? $turma->getMatCur() : $turma->getMatCur()->getMatCurId(),
                'tipoTurma'         => $turma->getTturma()->getTturmaDescricao()
            );
        }
        return $array;
    }

    public function buscaTurmasPorTurno( $cursocampus_id, $serie, $periodo, $turno ){
        $tturma = $this->getEntityManager()->getRepository('Matricula\Entity\AcadgeralTurmaTipo')->findOneBy(['tturmaDescricao' => 'Convencional'])->getTturmaId();
        $turmas = $this->findBy(array('cursocampus' => $cursocampus_id, 'turmaSerie' => $serie, 'per' => $periodo, 'turmaTurno' => $turno, 'tturma' => $tturma));


        $array = array();
        foreach ($turmas as $turma) {
            $array[] = array(
                'turmaNome'         => $turma->getTurmaNome(),
                'turmaCapacidade'   => $turma->getTurmaCapacidade(),
                'turmaQtdMatricula' => $turma->getTurmaQtdMatricula(),
                'turmaId'           => $turma->getTurmaId(),
                'matrizId'          => $turma->getMatCur() == NULL ? $turma->getMatCur() : $turma->getMatCur()->getMatCurId()
            );
        }
        return $array;
    }


    public function buscaTurmasPorPeriodo($periodo){
        $array = array();
        if($periodo){
            $turmas = $this->findBy(array('per' => $periodo ));

            foreach ($turmas as $turma) {
                $array[] = array(
                    'turmaNome'         => $turma->getTurmaNome(),
                    'turmaCapacidade'   => $turma->getTurmaCapacidade(),
                    'turmaQtdMatricula' => $turma->getTurmaQtdMatricula(),
                    'turmaId'           => $turma->getTurmaId(),
                    'matrizId'          => $turma->getMatCur() == NULL ? $turma->getMatCur() : $turma->getMatCur()->getMatCurId(),
                    'turmaSerie'        => $turma->getTurmaSerie()
                );
            }
        }

        return $array;
    }

    public function buscaTurma($id){
        $turma = $this->findOneBy(array('turmaId' => $id ));
        $turmaArray = null;
        if ($turma) {
            $turmaArray = array(
                'turmaNome'         => $turma->getTurmaNome(),
                'turmaCapacidade'   => $turma->getTurmaCapacidade(),
                'turmaQtdMatricula' => $turma->getTurmaQtdMatricula(),
                'turmaId'           => $turma->getTurmaId(),
                'matrizId'          => $turma->getMatCur() == NULL ? $turma->getMatCur() : $turma->getMatCur()->getMatCurId(),
                'turmaSerie'        => $turma->getTurmaSerie()
            );
        }
        return $turmaArray;
    }
}