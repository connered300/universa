<?php


namespace Matricula\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class AcadperiodoAluno extends EntityRepository
{
    public function buscaAlunosMatriculadosPeridoAtual()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $q = $qb->select(array('t1'))
            ->from('Matricula\Entity\AcadperiodoAluno', 't1')
            ->join('Matricula\Entity\AcadperiodoTurma', 't2', 'WITH', 't1.turma = t2.turmaId')
            ->Join('Matricula\Entity\AcadgeralSituacao', 't3', 'WITH', 't1.matsituacao = t3.situacaoId')
            ->join('Matricula\Entity\AcadperiodoLetivo', 't4', 'WITH', 't2.per = t4.perId')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->eq('t3.matsitDescricao', '?1'),
                    $qb->expr()->eq('t4.perId', '?2')
                )
            )
            ->setParameter(1, 'Matriculado')
            ->setParameter(2, 1)
            ->getQuery();

        return $q->execute();
    }
}