<?php


namespace Matricula\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class AcadgeralSituacao extends EntityRepository
{
    public function buscaSituacao($descricao)
    {
        if(is_array($descricao)){
            foreach($descricao as $pos=>$desc){
                $result[$pos] = $this->findOneBy(array('matsitDescricao' => $descricao[$pos]))->getSituacaoId();
            }
        }else{
            $result = $this->findOneBy(array('matsitDescricao' => $descricao));

            if($result){
                $result = $result->getSituacaoId();
                return $result;
            }
        }
        return false;
    }

    public function buscaSituacoes($descricao)
    {
        $situacoes    = $this->findBy(array('matsitDescricao' => $descricao));
        $arrSituacoes = array();

        foreach ($situacoes as $situacao) {
            $arrSituacoes[] = $situacao->getSituacaoId();
        }

        return $arrSituacoes;
    }

    public function listaSituacoes()
    {
        $situacoes = $this->findAll();
        $array = array();

        foreach ($situacoes as $situacao) {
            if ($situacao->getMatsitDescricao() != "Transferencia") {
                $array[] = array($situacao->getSituacaoId(), $situacao->getMatsitDescricao());
            }
        }

        return $array;
    }


    public function buscaSituacoesDesligamento()
    {
        $situacoes    = $this->findBy(
            array(
                'matsitDescricao' => array(
                    'Cancelada',
                    'Trancado',
                    'Transferencia',
                    'Desistente',
                    'Óbito',
                )
            )
        );
        $arrSituacoes = array();

        foreach ($situacoes as $situacao) {
            $arrSituacoes[] = $situacao->getSituacaoId();
        }

        return $arrSituacoes;
    }
}