<?php


namespace Matricula\Entity\Repository;


use Doctrine\ORM\EntityRepository;

class AcadperiodoAlunoDisciplinaFinal extends EntityRepository{
    public function buscaAlunoAlunoFinal( $alunodisc ){
        return $this->findOneBy(['alunodisc' => $alunodisc]);

    }
}