<?php

namespace Matricula\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class AcadgeralDocente extends EntityRepository
{
    public function buscaDocenteAll()
    {
        $docentes = $this->findBy(array('docenteAtivo' => 'Sim'));

        $array = array();

        /** @var \Matricula\Entity\AcadgeralDocente $docente */
        foreach ($docentes as $docente) {
            $array[] = array(
                'docenteId' => $docente->getDocenteId(),
                'pesNome'   => $docente->getPes()->getPes()->getPesNome(),
            );
        }

        return $array;
    }
}