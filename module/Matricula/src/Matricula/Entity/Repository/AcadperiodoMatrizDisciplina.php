<?php


namespace Matricula\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class AcadperiodoMatrizDisciplina extends EntityRepository
{
    public function buscaDisciplinasPorMatrizCurricular($matCurId, $perDiscPeriodo)
    {
        $array = array();

        $disciplinas = $this->findBy(array('matCur' => $matCurId, 'perDiscPeriodo' => $perDiscPeriodo));

        $i = 0;
        foreach ($disciplinas as $diciplina) {
            $array[$i] = array(
                'disc'           => $diciplina->getDisc()->getDiscId(),
                'discNome'       => $diciplina->getDisc()->getDiscNome(),
                'discTipo'       => $diciplina->getDisc()->getTdisc()->getTdiscId(),
                'discTipoNome'   => $diciplina->getDisc()->getTdisc()->getTdiscDescricao(),
                'perDiscPeriodo' => $diciplina->getPerDiscPeriodo(),
            );
            $i++;
        }

        //pre(pre($array));
        return $array;
    }
}