<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoEtapas
 *
 * @ORM\Table(name="acadperiodo__etapas", indexes={@ORM\Index(name="fk_periodo_etapas_periodo1_idx", columns={"per_id"})})
 * @ORM\Entity
 */
class AcadperiodoEtapas
{
    /**
     * @var integer
     *
     * @ORM\Column(name="etapa_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $etapaId;

    /**
     * @var string
     *
     * @ORM\Column(name="etapa_descricao", type="string", length=45, nullable=false)
     */
    private $etapaDescricao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="etapa_data_inicio", type="datetime", nullable=false)
     */
    private $etapaDataInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="etapa_data_fim", type="datetime", nullable=false)
     */
    private $etapaDataFim;

    /**
     * @var float
     *
     * @ORM\Column(name="etapa_percentagem", type="float", precision=10, scale=0, nullable=false)
     */
    private $etapaPercentagem;

    /**
     * @var integer
     *
     * @ORM\Column(name="etapa_ordem", type="integer", nullable=true)
     */
    private $etapaOrdem;

    /**
     * @var AcadperiodoLetivo
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoLetivo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="per_id", referencedColumnName="per_id")
     * })
     */
    private $per;

    /**
     * @return int
     */
    public function getEtapaId()
    {
        return $this->etapaId;
    }

    /**
     * @param int $etapaId
     */
    public function setEtapaId($etapaId)
    {
        $this->etapaId = $etapaId;
    }

    /**
     * @return string
     */
    public function getEtapaDescricao()
    {
        return $this->etapaDescricao;
    }

    /**
     * @param string $etapaDescricao
     */
    public function setEtapaDescricao($etapaDescricao)
    {
        $this->etapaDescricao = $etapaDescricao;
    }

    /**
     * @return \DateTime
     */
    public function getEtapaDataInicio()
    {
        return $this->etapaDataInicio;
    }

    /**
     * @param \DateTime $etapaDataInicio
     */
    public function setEtapaDataInicio($etapaDataInicio)
    {
        $this->etapaDataInicio = $etapaDataInicio;
    }

    /**
     * @return \DateTime
     */
    public function getEtapaDataFim()
    {
        return $this->etapaDataFim;
    }

    /**
     * @param \DateTime $etapaDataFim
     */
    public function setEtapaDataFim($etapaDataFim)
    {
        $this->etapaDataFim = $etapaDataFim;
    }

    /**
     * @return float
     */
    public function getEtapaPercentagem()
    {
        return $this->etapaPercentagem;
    }

    /**
     * @param float $etapaPercentagem
     */
    public function setEtapaPercentagem($etapaPercentagem)
    {
        $this->etapaPercentagem = $etapaPercentagem;
    }

    /**
     * @return int
     */
    public function getEtapaOrdem()
    {
        return $this->etapaOrdem;
    }

    /**
     * @param int $etapaOrdem
     */
    public function setEtapaOrdem($etapaOrdem)
    {
        $this->etapaOrdem = $etapaOrdem;
    }

    /**
     * @return AcadperiodoLetivo
     */
    public function getPer()
    {
        return $this->per;
    }

    /**
     * @param AcadperiodoLetivo $per
     */
    public function setPer($per)
    {
        $this->per = $per;
    }

}
