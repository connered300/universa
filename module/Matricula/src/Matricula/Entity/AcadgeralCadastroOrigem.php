<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AcadgeralCadastroOrigem
 *
 * @ORM\Table(name="acadgeral__cadastro_origem")
 * @ORM\Entity
 * @LG\LG(id="origemId",label="OrigemNome")
 * @Jarvis\Jarvis(title="Listagem de origem de cadastro",icon="fa fa-table")
 */
class AcadgeralCadastroOrigem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="origem_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="origem_id")
     * @LG\Labels\Attributes(text="código origem")
     * @LG\Querys\Conditions(type="=")
     */
    private $origemId;
    /**
     * @var string
     *
     * @ORM\Column(name="origem_nome", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="origem_nome")
     * @LG\Labels\Attributes(text="nome")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $origemNome;

    /**
     * @return integer
     */
    public function getOrigemId()
    {
        return $this->origemId;
    }

    /**
     * @param integer $origemId
     * @return AcadgeralCadastroOrigem
     */
    public function setOrigemId($origemId)
    {
        $this->origemId = $origemId;

        return $this;
    }

    /**
     * @return string
     */
    public function getOrigemNome()
    {
        return $this->origemNome;
    }

    /**
     * @param string $origemNome
     * @return AcadgeralCadastroOrigem
     */
    public function setOrigemNome($origemNome)
    {
        $this->origemNome = $origemNome;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'origemId'   => $this->getOrigemId(),
            'origemNome' => $this->getOrigemNome(),
        );

        return $array;
    }
}
