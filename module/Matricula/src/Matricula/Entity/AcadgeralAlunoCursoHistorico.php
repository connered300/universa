<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AcadgeralAlunoCursoHistorico
 *
 * @ORM\Table(name="acadgeral__aluno_curso_historico", uniqueConstraints={@ORM\UniqueConstraint(name="index4", columns={"pes_id", "cursocampus_id"})}, indexes={@ORM\Index(name="fk_matricula_aluno_curso_campus_curso1_idx", columns={"cursocampus_id"}), @ORM\Index(name="fk_matricula_aluno_curso_matricula_aluno1_idx", columns={"pes_id"})})
 * @ORM\Entity
 * @LG\LG(id="alunocursoId",label="alunocurso_id")
 * @Jarvis\Jarvis(title="Alunos Por curso",icon="fa fa-table")
 */
class AcadgeralAlunoCursoHistorico
{
    /**
     * @var integer
     *
     * @ORM\Column(name="alunocurso_id", type="bigint", nullable=false, length=12)
     * @ORM\Id
     * @LG\Labels\Property(name="alunocurso_id")
     * @LG\Labels\Attributes(text="Índice",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $alunocursoId;

    /**
     * @var string
     *
     * @ORM\Column(name="alunocurso_carteira", type="string", length=45, nullable=true)
     */
    private $alunocursoCarteira;

    /**
     * @var string
     *
     * @ORM\Column(name="alunocurso_observacoes", type="text", nullable=true)
     */
    private $alunocursoObservacoes;

    /**
     * @var string
     *
     * @ORM\Column(name="alunocurso_alteracao_observacao", type="text", nullable=true)
     */
    private $alunocursoAlteracaoObservacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="alunocurso_data_cadastro", type="datetime", nullable=false)
     * @LG\Labels\Property(name="alunocurso_data_cadastro")
     * @LG\Labels\Attributes(text="Data Inscrição",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $alunocursoDataCadastro;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="alunocurso_data_colacao", type="date", nullable=true)
     */
    private $alunocursoDataColacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="alunocurso_data_matricula", type="datetime", nullable=false)
     */
    private $alunocursoDataMatricula;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="alunocurso_data_expedicao_diploma", type="date", nullable=true)
     */
    private $alunocursoDataExpedicaoDiploma;
    /**
     * @var string
     *
     * @ORM\Column(name="alunocurso_situacao", type="string", nullable=false, length=10)
     * @LG\Labels\Property(name="alunocurso_situacao")
     * @LG\Labels\Attributes(text="situação do curso")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $alunocursoSituacao;

    /**
     * @var integer
     * @ORM\Column(name="cursocampus_id", type="integer",nullable=true)
     **/

    private $cursocampus;

    /**
     * @var integer
     * @ORM\Column(name="aluno_id", type="integer",nullable=true)
     *
     */
    private $aluno;

    /**
     * @var integer
     * @ORM\Column(name="motivo_id", type="integer",nullable=true)
     *
     */
    private $motivo;

    /**
     * @var integer
     * @ORM\Column(name="tiposel_id", type="integer",nullable=true)
     *
     */
    private $tiposel;

    /**
     * @var integer
     * @ORM\Column(name="pes_id_agente", type="integer",nullable=true)
     *
     **/
    private $pesIdAgente;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     * @ORM\Column(name="usuario_cadastro", type="integer",nullable=true)
     *
     */
    private $usuarioCadastro;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     * @ORM\Column(name="usuario_alteracao", type="integer",nullable=true)
     *
     */
    private $usuarioAlteracao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="alunocurso_data_alteracao", type="datetime", nullable=true)
     * @LG\Labels\Property(name="alunocurso_data_alteracao")
     * @LG\Labels\Attributes(text="Data Alteração",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $alunocursoDataAlteracao;

    /**
     * @return integer
     */
    public function getAlunocursoId()
    {
        return $this->alunocursoId;
    }

    /**
     * @param integer $alunocursoId
     * @return AcadgeralAlunoCursoHistorico
     */
    public function setAlunocursoId($alunocursoId)
    {
        $this->alunocursoId = $alunocursoId;

        return $this;
    }

    /**
     * @return integer
     */
    public function getCursocampus()
    {
        return $this->cursocampus;
    }

    /**
     * @param integer $cursocampus
     * @return AcadgeralAlunoCursoHistorico
     */
    public function setCursocampus($cursocampus)
    {
        $this->cursocampus = $cursocampus;

        return $this;
    }

    /**
     * @return integer
     */
    public function getAluno()
    {
        return $this->aluno;
    }

    /**
     * @param integer $aluno
     * @return AcadgeralAlunoCursoHistorico
     */
    public function setAluno($aluno)
    {
        $this->aluno = $aluno;

        return $this;
    }

    /**
     * @param integer $motivo
     * @return AcadgeralAlunoCursoHistorico
     */
    public function setMotivo($motivo)
    {
        $this->motivo = $motivo;

        return $this;
    }

    /**
     * @return integer $motivo
     */
    public function getMotivo()
    {
        return $this->motivo;
    }

    /**
     * @return integer
     */
    public function getTiposel()
    {
        return $this->tiposel;
    }

    /**
     * @param integer $tiposel
     * @return AcadgeralAlunoCursoHistorico
     */
    public function setTiposel($tiposel)
    {
        $this->tiposel = $tiposel;

        return $this;
    }

    /**
     * @return integer
     */
    public function getPesIdAgente()
    {
        return $this->pesIdAgente;
    }

    /**
     * @param integer $pesIdAgente
     * @return AcadgeralAlunoCursoHistorico
     */
    public function setPesIdAgente($pesIdAgente)
    {
        $this->pesIdAgente = $pesIdAgente;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getAlunocursoDataCadastro($format = false)
    {
        $alunocursoDataCadastro = $this->alunocursoDataCadastro;

        if ($format && $alunocursoDataCadastro) {
            $alunocursoDataCadastro = $alunocursoDataCadastro->format('d/m/Y H:i:s');
        }

        return $alunocursoDataCadastro;
    }

    /**
     * @param \Datetime $alunocursoDataCadastro
     * @return AcadgeralAlunoCursoHistorico
     */
    public function setAlunocursoDataCadastro($alunocursoDataCadastro)
    {
        if ($alunocursoDataCadastro) {
            if (is_string($alunocursoDataCadastro)) {
                $alunocursoDataCadastro = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $alunocursoDataCadastro
                );
                $alunocursoDataCadastro = new \Datetime($alunocursoDataCadastro);
            }
        } else {
            $alunocursoDataCadastro = null;
        }

        $this->alunocursoDataCadastro = $alunocursoDataCadastro;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getAlunocursoDataAlteracao($format = false)
    {
        $alunocursoDataAlteracao = $this->alunocursoDataAlteracao;

        if ($format && $alunocursoDataAlteracao) {
            $alunocursoDataAlteracao = $alunocursoDataAlteracao->format('d/m/Y H:i:s');
        }

        return $alunocursoDataAlteracao;
    }

    /**
     * @param \Datetime $alunocursoDataAlteracao
     * @return AcadgeralAlunoCursoHistorico
     */
    public function setAlunocursoDataAlteracao($alunocursoDataAlteracao)
    {
        if ($alunocursoDataAlteracao) {
            if (is_string($alunocursoDataAlteracao)) {
                $alunocursoDataAlteracao = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $alunocursoDataAlteracao
                );
                $alunocursoDataAlteracao = new \Datetime($alunocursoDataAlteracao);
            }
        } else {
            $alunocursoDataAlteracao = null;
        }

        $this->alunocursoDataAlteracao = $alunocursoDataAlteracao;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlunocursoCarteira()
    {
        return $this->alunocursoCarteira;
    }

    /**
     * @param string $alunocursoCarteira
     * @return AcadgeralAlunoCursoHistorico
     */
    public function setAlunocursoCarteira($alunocursoCarteira)
    {
        $this->alunocursoCarteira = $alunocursoCarteira;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlunocursoObservacoes()
    {
        return $this->alunocursoObservacoes;
    }

    /**
     * @return string
     */
    public function getAlunocursoAlteracaoObservacao()
    {
        return $this->alunocursoAlteracaoObservacao;
    }

    /**
     * @param string $alunocursoObservacoes
     * @return AcadgeralAlunoCursoHistorico
     */
    public function setAlunocursoObservacoes($alunocursoObservacoes)
    {
        $this->alunocursoObservacoes = $alunocursoObservacoes;

        return $this;
    }

    /**
     * @param string $alunocursoAlteracaoObservacao
     * @return AcadgeralAlunoCursoHistorico
     */
    public function setAlunocursoAlteracaoObservacao($alunocursoAlteracaoObservacao)
    {
        $this->alunocursoAlteracaoObservacao = $alunocursoAlteracaoObservacao;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getAlunocursoDataColacao($format = false)
    {
        $alunocursoDataColacao = $this->alunocursoDataColacao;

        if ($format && $alunocursoDataColacao) {
            $alunocursoDataColacao = $alunocursoDataColacao->format('d/m/Y');
        }

        return $alunocursoDataColacao;
    }

    /**
     * @param \Datetime $alunocursoDataColacao
     * @return AcadgeralAlunoCursoHistorico
     */
    public function setAlunocursoDataColacao($alunocursoDataColacao)
    {
        if ($alunocursoDataColacao) {
            if (is_string($alunocursoDataColacao)) {
                $alunocursoDataColacao = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $alunocursoDataColacao
                );
                $alunocursoDataColacao = new \Datetime($alunocursoDataColacao);
            }
        } else {
            $alunocursoDataColacao = null;
        }

        $this->alunocursoDataColacao = $alunocursoDataColacao;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getAlunocursoDataExpedicaoDiploma($format = false)
    {
        $alunocursoDataExpedicaoDiploma = $this->alunocursoDataExpedicaoDiploma;

        if ($format && $alunocursoDataExpedicaoDiploma) {
            $alunocursoDataExpedicaoDiploma = $alunocursoDataExpedicaoDiploma->format('d/m/Y');
        }

        return $alunocursoDataExpedicaoDiploma;
    }

    /**
     * @param \Datetime $alunocursoDataExpedicaoDiploma
     * @return AcadgeralAlunoCursoHistorico
     */
    public function setAlunocursoDataExpedicaoDiploma($alunocursoDataExpedicaoDiploma)
    {
        if ($alunocursoDataExpedicaoDiploma) {
            if (is_string($alunocursoDataExpedicaoDiploma)) {
                $alunocursoDataExpedicaoDiploma = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $alunocursoDataExpedicaoDiploma
                );
                $alunocursoDataExpedicaoDiploma = new \Datetime($alunocursoDataExpedicaoDiploma);
            }
        } else {
            $alunocursoDataExpedicaoDiploma = null;
        }

        $this->alunocursoDataExpedicaoDiploma = $alunocursoDataExpedicaoDiploma;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlunocursoSituacao()
    {
        return $this->alunocursoSituacao;
    }

    /**
     * @param string $alunocursoSituacao
     * @return AcadgeralAlunoCursoHistorico
     */
    public function setAlunocursoSituacao($alunocursoSituacao)
    {
        $this->alunocursoSituacao = $alunocursoSituacao;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuarioCadastro()
    {
        return $this->usuarioCadastro;
    }

    /**
     * @param integer $usuarioCadastro
     * @return AcadgeralAlunoCursoHistorico
     */
    public function setUsuarioCadastro($usuarioCadastro)
    {
        $this->usuarioCadastro = $usuarioCadastro;

        return $this;
    }

    /**
     * @return integer
     */
    public function getUsuarioAlteracao()
    {
        return $this->usuarioAlteracao;
    }

    /**
     * @param integer $usuarioAlteracao
     * @return AcadgeralAlunoCursoHistorico
     */
    public function setUsuarioAlteracao($usuarioAlteracao)
    {
        $this->usuarioAlteracao = $usuarioAlteracao;

        return $this;
    }

    /**
     * @param boolean $format
     * @return \DateTime
     */
    public function getAlunocursoDataMatricula($format = false)
    {
        $alunocursoDataMatricula = $this->alunocursoDataMatricula;

        if ($format && $alunocursoDataMatricula) {
            $alunocursoDataMatricula = $alunocursoDataMatricula->format('d/m/Y H:i:s');
        }

        return $alunocursoDataMatricula;
    }

    /**
     * @param \DateTime $alunocursoDataMatricula
     * @return AcadgeralAlunoCursoHistorico
     */
    public function setAlunocursoDataMatricula($alunocursoDataMatricula)
    {
        if ($alunocursoDataMatricula) {
            if (is_string($alunocursoDataMatricula)) {
                $alunocursoDataMatricula = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $alunocursoDataMatricula
                );
                $alunocursoDataMatricula = new \Datetime($alunocursoDataMatricula);
            }
        } else {
            $alunocursoDataMatricula = null;
        }

        $this->alunocursoDataMatricula = $alunocursoDataMatricula;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

}
