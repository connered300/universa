<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadgeralDocenteFormacao
 *
 * @ORM\Table(name="acadgeral__docente_formacao", indexes={@ORM\Index(name="fk_matricula_formacao_acad_nivel1_idx", columns={"nivel_id"}), @ORM\Index(name="fk_matricula_formacao_area_conhecimento1_idx", columns={"area_id"}), @ORM\Index(name="fk_acadgeral__docente_formacao_acadgeral__docente1_idx", columns={"pes_id"})})
 * @ORM\Entity
 */
class AcadgeralDocenteFormacao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="form_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $formId;

    /**
     * @var string
     *
     * @ORM\Column(name="form_descricao", type="string", length=45, nullable=false)
     */
    private $formDescricao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="form_ano", type="date", nullable=false)
     */
    private $formAno;

    /**
     * @var string
     *
     * @ORM\Column(name="form_cert", type="string", nullable=true)
     */
    private $formCert;

    /**
     * @var string
     *
     * @ORM\Column(name="form_instituicao", type="string", length=255, nullable=true)
     */
    private $formInstituicao;

    /**
     * @var string
     *
     * @ORM\Column(name="form_status", type="string", nullable=true)
     */
    private $formStatus;

    /**
     * @var AcadgeralDocente
     *
     * @ORM\ManyToOne(targetEntity="AcadgeralDocente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     */
    private $pes;

    /**
     * @var AcadNivel
     *
     * @ORM\ManyToOne(targetEntity="AcadNivel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="nivel_id", referencedColumnName="nivel_id")
     * })
     */
    private $nivel;

    /**
     * @var AcadgeralAreaConhecimento
     *
     * @ORM\ManyToOne(targetEntity="AcadgeralAreaConhecimento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="area_id", referencedColumnName="area_id")
     * })
     */
    private $area;

    /**
     * @return int
     */
    public function getFormId()
    {
        return $this->formId;
    }

    /**
     * @param int $formId
     * @return AcadgeralDocenteFormacao
     */
    public function setFormId($formId)
    {
        $this->formId = $formId;

        return $this;
    }

    /**
     * @return string
     */
    public function getFormDescricao()
    {
        return $this->formDescricao;
    }

    /**
     * @param string $formDescricao
     * @return AcadgeralDocenteFormacao
     */
    public function setFormDescricao($formDescricao)
    {
        $this->formDescricao = $formDescricao;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getFormAno()
    {
        return $this->formAno;
    }

    /**
     * @param \DateTime $formAno
     * @return AcadgeralDocenteFormacao
     */
    public function setFormAno($formAno)
    {
        $this->formAno = $formAno;

        return $this;
    }

    /**
     * @return string
     */
    public function getFormCert()
    {
        return $this->formCert;
    }

    /**
     * @param string $formCert
     * @return AcadgeralDocenteFormacao
     */
    public function setFormCert($formCert)
    {
        $this->formCert = $formCert;

        return $this;
    }

    /**
     * @return string
     */
    public function getFormInstituicao()
    {
        return $this->formInstituicao;
    }

    /**
     * @param string $formInstituicao
     * @return AcadgeralDocenteFormacao
     */
    public function setFormInstituicao($formInstituicao)
    {
        $this->formInstituicao = $formInstituicao;

        return $this;
    }

    /**
     * @return string
     */
    public function getFormStatus()
    {
        return $this->formStatus;
    }

    /**
     * @param string $formStatus
     * @return AcadgeralDocenteFormacao
     */
    public function setFormStatus($formStatus)
    {
        $this->formStatus = $formStatus;

        return $this;
    }

    /**
     * @return AcadgeralDocente
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @param AcadgeralDocente $pes
     * @return AcadgeralDocenteFormacao
     */
    public function setPes($pes)
    {
        $this->pes = $pes;

        return $this;
    }

    /**
     * @return AcadNivel
     */
    public function getNivel()
    {
        return $this->nivel;
    }

    /**
     * @param AcadNivel $nivel
     * @return AcadgeralDocenteFormacao
     */
    public function setNivel($nivel)
    {
        $this->nivel = $nivel;

        return $this;
    }

    /**
     * @return AcadgeralAreaConhecimento
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param AcadgeralAreaConhecimento $area
     * @return AcadgeralDocenteFormacao
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }
}
