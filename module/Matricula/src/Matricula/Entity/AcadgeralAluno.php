<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AcadgeralAluno
 *
 * @ORM\Table(name="acadgeral__aluno", uniqueConstraints={@ORM\UniqueConstraint(name="fk_acadgeral__aluno_pessoa_fisica1_idx", columns={"pes_id"})}, indexes={@ORM\Index(name="fk_acadgeral__aluno_arquivo1_idx", columns={"arq_id"})})
 * @ORM\Entity
 * @LG\LG(id="aluno_id",label="alunoId")
 * @Jarvis\Jarvis(title="Alunos",icon="fa fa-table")
 */
class AcadgeralAluno
{
    /**
     * @var integer
     *
     * @ORM\Column(name="aluno_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="aluno_id")
     * @LG\Labels\Attributes(text="Index",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $alunoId;

    /**
     * @var string
     *
     * @ORM\Column(name="aluno_mae", type="string", length=45, nullable=false)
     * @LG\Labels\Property(name="aluno_mae")
     * @LG\Labels\Attributes(text="Nome da Mãe",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE")
     *
     */
    private $alunoMae = "";

    /**
     * @var string
     *
     * @ORM\Column(name="aluno_pai", type="string", length=45, nullable=true)
     * @LG\Labels\Property(name="aluno_pai")
     * @LG\Labels\Attributes(text="Nome do Pai",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $alunoPai = "";

    /**
     * @var string
     *
     * @ORM\Column(name="aluno_etnia", type="string", nullable=false)
     * @LG\Labels\Property(name="aluno_etnia")
     * @LG\Labels\Attributes(text="Etnia",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $alunoEtnia = 'Não declarado';

    /**
     * @var string
     *
     * @ORM\Column(name="aluno_titulo_eleitoral", type="string", length=12, nullable=true)
     */
    private $alunoTituloEleitoral;

    /**
     * @var string
     *
     * @ORM\Column(name="aluno_zona_eleitoral", type="string", length=9, nullable=true)
     */
    private $alunoZonaEleitoral;

    /**
     * @var string
     *
     * @ORM\Column(name="aluno_secao_eleitoral", type="string", length=4, nullable=true)
     */
    private $alunoSecaoEleitoral;

    /**
     * @var string
     *
     * @ORM\Column(name="aluno_cert_militar", type="string", length=45, nullable=true)
     */
    private $alunoCertMilitar;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="aluno_data_cadastro", type="datetime", nullable=true)
     * @LG\Labels\Property(name="aluno_data_cadastro")
     * @LG\Labels\Attributes(text="Data Cadastro",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $alunoDataCadastro;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="aluno_data_alteracao", type="datetime", nullable=true)
     * @LG\Labels\Property(name="aluno_data_alteracao")
     * @LG\Labels\Attributes(text="Data Alteração",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $alunoDataAlteracao;

    /**
     * @var \Pessoa\Entity\PessoaFisica
     *
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\PessoaFisica")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     */
    private $pes;

    /**
     * @var \GerenciadorArquivos\Entity\Arquivo
     *
     * @ORM\ManyToOne(targetEntity="GerenciadorArquivos\Entity\Arquivo", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="arq_id", referencedColumnName="arq_id",nullable=true)
     * })
     */
    private $arq;
    /**
     * @var \Matricula\Entity\AcadgeralCadastroOrigem
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadgeralCadastroOrigem")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="origem_id", referencedColumnName="origem_id")
     * })
     */
    private $origem;
    /**
     * @var \Organizacao\Entity\OrgAgenteEducacional
     * @ORM\ManyToOne(targetEntity="Organizacao\Entity\OrgAgenteEducacional")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id_agenciador", referencedColumnName="pes_id")
     * })
     */
    private $pesIdAgenciador;
    /**
     * @var \Organizacao\Entity\OrgUnidadeEstudo
     * @ORM\ManyToOne(targetEntity="Organizacao\Entity\OrgUnidadeEstudo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="unidade_id", referencedColumnName="unidade_id")
     * })
     */
    private $unidade;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_cadastro", referencedColumnName="id")
     * })
     */
    private $usuarioCadastro;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_alteracao", referencedColumnName="id")
     * })
     */
    private $usuarioAlteracao;

    /**
     * @var string
     *
     * @ORM\Column(name="aluno_pessoa_indicacao", type="string", nullable=true, length=255)
     * @LG\Labels\Property(name="aluno_pessoa_indicacao")
     * @LG\Labels\Attributes(text="indicação pessoa")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $alunoPessoaIndicacao;

    /**
     * @var string
     *
     * @ORM\Column(name="aluno_mediador", type="string", nullable=true, length=255)
     * @LG\Labels\Property(name="aluno_mediador")
     * @LG\Labels\Attributes(text="mediador")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $alunoMediador;

    /**
     * @var string
     *
     * @ORM\Column(name="aluno_observacao_documentos", type="text", nullable=true)
     * @LG\Labels\Property(name="aluno_observacao_documentos")
     * @LG\Labels\Attributes(text="Observação para documentos")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $alunoObservacaoDocumentos;

    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return integer
     */
    public function getAlunoId()
    {
        return $this->alunoId;
    }

    /**
     * @param integer $alunoId
     * @return AcadgeralAluno
     */
    public function setAlunoId($alunoId)
    {
        $this->alunoId = $alunoId;

        return $this;
    }

    /**
     * @return \Pessoa\Entity\PessoaFisica
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @param \Pessoa\Entity\PessoaFisica $pes
     * @return AcadgeralAluno
     */
    public function setPes($pes)
    {
        $this->pes = $pes;

        return $this;
    }

    /**
     * @return \GerenciadorArquivos\Entity\Arquivo
     */
    public function getArq()
    {
        return $this->arq;
    }

    /**
     * @param \GerenciadorArquivos\Entity\Arquivo $arq
     * @return AcadgeralAluno
     */
    public function setArq($arq)
    {
        $this->arq = $arq;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlunoMae()
    {
        return $this->alunoMae;
    }

    /**
     * @param string $alunoMae
     * @return AcadgeralAluno
     */
    public function setAlunoMae($alunoMae = "")
    {
        $this->alunoMae = (string)$alunoMae;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlunoPai()
    {
        return $this->alunoPai;
    }

    /**
     * @param string $alunoPai
     * @return AcadgeralAluno
     */
    public function setAlunoPai($alunoPai = "")
    {
        $this->alunoPai = (string)$alunoPai;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlunoEtnia()
    {
        return $this->alunoEtnia;
    }

    /**
     * @param string $alunoEtnia
     * @return AcadgeralAluno
     */
    public function setAlunoEtnia($alunoEtnia = "")
    {
        $this->alunoEtnia = $alunoEtnia ? $alunoEtnia : "Não declarado";

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getAlunoDataCadastro($format = false)
    {
        $alunoDataCadastro = $this->alunoDataCadastro;

        if ($format && $alunoDataCadastro) {
            $alunoDataCadastro = $alunoDataCadastro->format('d/m/Y H:i:s');
        }

        return $alunoDataCadastro;
    }

    /**
     * @param \Datetime $alunoDataCadastro
     * @return AcadgeralAluno
     */
    public function setAlunoDataCadastro($alunoDataCadastro)
    {
        if ($alunoDataCadastro) {
            if (is_string($alunoDataCadastro)) {
                $alunoDataCadastro = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $alunoDataCadastro
                );
                $alunoDataCadastro = new \Datetime($alunoDataCadastro);
            }
        } else {
            $alunoDataCadastro = null;
        }
        $this->alunoDataCadastro = $alunoDataCadastro;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getAlunoDataAlteracao($format = false)
    {
        $alunoDataAlteracao = $this->alunoDataAlteracao;

        if ($format && $alunoDataAlteracao) {
            $alunoDataAlteracao = $alunoDataAlteracao->format('d/m/Y H:i:s');
        }

        return $alunoDataAlteracao;
    }

    /**
     * @param \Datetime $alunoDataAlteracao
     * @return AcadgeralAluno
     */
    public function setAlunoDataAlteracao($alunoDataAlteracao)
    {
        if ($alunoDataAlteracao) {
            if (is_string($alunoDataAlteracao)) {
                $alunoDataAlteracao = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $alunoDataAlteracao
                );
                $alunoDataAlteracao = new \Datetime($alunoDataAlteracao);
            }
        } else {
            $alunoDataAlteracao = null;
        }
        $this->alunoDataAlteracao = $alunoDataAlteracao;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlunoTituloEleitoral()
    {
        return $this->alunoTituloEleitoral;
    }

    /**
     * @param string $alunoTituloEleitoral
     * @return AcadgeralAluno
     */
    public function setAlunoTituloEleitoral($alunoTituloEleitoral)
    {
        $this->alunoTituloEleitoral = $alunoTituloEleitoral;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlunoZonaEleitoral()
    {
        return $this->alunoZonaEleitoral;
    }

    /**
     * @param string $alunoZonaEleitoral
     * @return AcadgeralAluno
     */
    public function setAlunoZonaEleitoral($alunoZonaEleitoral)
    {
        $this->alunoZonaEleitoral = $alunoZonaEleitoral;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlunoSecaoEleitoral()
    {
        return $this->alunoSecaoEleitoral;
    }

    /**
     * @param string $alunoSecaoEleitoral
     * @return AcadgeralAluno
     */
    public function setAlunoSecaoEleitoral($alunoSecaoEleitoral)
    {
        $this->alunoSecaoEleitoral = $alunoSecaoEleitoral;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlunoCertMilitar()
    {
        return $this->alunoCertMilitar;
    }

    /**
     * @param string $alunoCertMilitar
     * @return AcadgeralAluno
     */
    public function setAlunoCertMilitar($alunoCertMilitar)
    {
        $this->alunoCertMilitar = $alunoCertMilitar;

        return $this;
    }

    /**
     * @return \Matricula\Entity\AcadgeralCadastroOrigem
     */
    public function getOrigem()
    {
        return $this->origem;
    }

    /**
     * @param \Matricula\Entity\AcadgeralCadastroOrigem $origem
     * @return AcadgeralAluno
     */
    public function setOrigem($origem)
    {
        $this->origem = $origem;

        return $this;
    }

    /**
     * @return \Organizacao\Entity\OrgAgenteEducacional
     */
    public function getPesIdAgenciador()
    {
        return $this->pesIdAgenciador;
    }

    /**
     * @param \Organizacao\Entity\OrgAgenteEducacional $pesIdAgenciador
     * @return AcadgeralAluno
     */
    public function setPesIdAgenciador($pesIdAgenciador)
    {
        $this->pesIdAgenciador = $pesIdAgenciador;

        return $this;
    }

    /**
     * @return \Organizacao\Entity\OrgUnidadeEstudo
     */
    public function getUnidade()
    {
        return $this->unidade;
    }

    /**
     * @param \Organizacao\Entity\OrgUnidadeEstudo $unidade
     * @return AcadgeralAluno
     */
    public function setUnidade($unidade)
    {
        $this->unidade = $unidade;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuarioCadastro()
    {
        return $this->usuarioCadastro;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuarioCadastro
     * @return AcadgeralAluno
     */
    public function setUsuarioCadastro($usuarioCadastro)
    {
        $this->usuarioCadastro = $usuarioCadastro;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuarioAlteracao()
    {
        return $this->usuarioAlteracao;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuarioAlteracao
     * @return AcadgeralAluno
     */
    public function setUsuarioAlteracao($usuarioAlteracao)
    {
        $this->usuarioAlteracao = $usuarioAlteracao;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlunoPessoaIndicacao()
    {
        return $this->alunoPessoaIndicacao;
    }

    /**
     * @param string $alunoPessoaIndicacao
     * @return AcadgeralAluno
     */
    public function setAlunoPessoaIndicacao($alunoPessoaIndicacao)
    {
        $this->alunoPessoaIndicacao = $alunoPessoaIndicacao;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlunoMediador()
    {
        return $this->alunoMediador;
    }

    /**
     * @param string $alunoMediador
     * @return AcadgeralAluno
     */
    public function setAlunoMediador($alunoMediador)
    {
        $this->alunoMediador = $alunoMediador;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlunoObservacaoDocumento()
    {
        return $this->alunoObservacaoDocumentos;
    }

    /**
     * @param  string
     * @return AcadgeralAluno
     */
    public function setAlunoObservacaoDocumento($alunoObservacaoDocumentos)
    {
        $this->alunoObservacaoDocumentos = $alunoObservacaoDocumentos;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'alunoId'                   => $this->getAlunoId(),
            'arq'                       => $this->getArq(),
            'alunoMae'                  => $this->getAlunoMae(),
            'alunoPai'                  => $this->getAlunoPai(),
            'alunoEtnia'                => $this->getAlunoEtnia(),
            'alunoDataCadastro'         => $this->getAlunoDataCadastro(true),
            'alunoDataAlteracao'        => $this->getAlunoDataAlteracao(true),
            'alunoTituloEleitoral'      => $this->getAlunoTituloEleitoral(),
            'alunoZonaEleitoral'        => $this->getAlunoZonaEleitoral(),
            'alunoSecaoEleitoral'       => $this->getAlunoSecaoEleitoral(),
            'alunoCertMilitar'          => $this->getAlunoCertMilitar(),
            'alunoPessoaIndicacao'      => $this->getAlunoPessoaIndicacao(),
            'alunoMediador'             => $this->getAlunoMediador(),
            'alunoObservacaoDocumentos' => $this->getAlunoObservacaoDocumento(),
            'pes'                       => '',
            'origem'                    => '',
            'pesIdAgenciador'           => '',
            'unidade'                   => '',
            'usuarioCadastro'           => '',
            'usuarioAlteracao'          => '',
        );

        $arrPes = $this->getPes() ? $this->getPes()->toArray() : array();

        if ($this->getPes()) {
            $array['pes'] = $this->getPes()->getPes()->getPesId();
        }

        if ($this->getOrigem()) {
            $array['origem'] = $this->getOrigem()->getOrigemId();
        }

        if ($this->getPesIdAgenciador()) {
            $array['pesIdAgenciador'] = $this->getPesIdAgenciador()->getPes()->getPesId();
        }

        if ($this->getUnidade()) {
            $array['unidade'] = $this->getUnidade()->getUnidadeId();
        }

        if ($this->getUsuarioCadastro()) {
            $array['usuarioCadastro']      = $this->getUsuarioCadastro()->getId();
            $array['usuarioCadastroLogin'] = $this->getUsuarioCadastro()->getLogin();
        }

        if ($this->getUsuarioAlteracao()) {
            $array['usuarioAlteracao']      = $this->getUsuarioAlteracao()->getId();
            $array['usuarioAlteracaoLogin'] = $this->getUsuarioAlteracao()->getLogin();
        }

        return array_merge($arrPes, $array);
    }
}
