<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AcadperiodoLetivo
 *
 * @ORM\Table(name="acadperiodo__letivo")
 * @ORM\Entity
 * @LG\LG(id="per_id",label="per_nome")
 */
class AcadperiodoLetivo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="per_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $perId;

    /**
     * @var string
     *
     * @ORM\Column(name="per_nome", type="string", length=45, nullable=false)
     */
    private $perNome;

    /**
     * @var string
     *
     * @ORM\Column(name="per_ano", type="string", nullable=false)
     */
    private $perAno;

    /**
     * @var integer
     *
     * @ORM\Column(name="per_etapas", type="integer", nullable=false)
     */
    private $perEtapas;

    /**
     * @var string
     *
     * @ORM\Column(name="per_semestre", type="string", nullable=false)
     */
    private $perSemestre;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="per_data_fim", type="datetime", nullable=true)
     */
    private $perDataFim;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="per_data_inicio", type="datetime", nullable=true)
     */
    private $perDataInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="per_matricula_inicio", type="datetime", nullable=false)
     */
    private $perMatriculaInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="per_matricula_fim", type="datetime", nullable=false)
     */
    private $perMatriculaFim;

    /**
     * @var string
     *
     * @ORM\Column(name="per_periodicidade", type="string")
     */
    private $periodicidade;

    /**
     * @var string
     *
     * @ORM\Column(name="per_descricao", type="string")
     */
    private $descricao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="per_data_finalizacao", type="datetime", nullable=false)
     */
    private $perDataFinalizacao;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Matricula\Entity\CampusCurso", inversedBy="per")
     * @ORM\JoinTable(name="acadperiodo__letivo_campus_curso",
     *   joinColumns={
     *     @ORM\JoinColumn(name="per_id", referencedColumnName="per_id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="cursocampus_id", referencedColumnName="cursocampus_id")
     *   }
     * )
     */
    private $cursoCampus;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Vestibular\Entity\SelecaoEdicao", inversedBy="per")
     * @ORM\JoinTable(name="acadperiodo__letivo_selecao_edicao",
     *   joinColumns={
     *     @ORM\JoinColumn(name="per_id", referencedColumnName="per_id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="edicao_id", referencedColumnName="edicao_id")
     *   }
     * )
     */
    private $selecaoEdicao;

    /**
     * @var \Matricula\Entity\AcadNivel
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadNivel", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="nivel_id", referencedColumnName="nivel_id")
     * })
     */
    private $nivel;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="per_data_vencimento_inicial", type="datetime", nullable=false)
     */
    private $perDataVencimentoInicial;

    /**
     * @var string
     *
     * @ORM\Column(name="per_data_vencimento_inicial_editavel", type="string", nullable=false)
     */
    private $perDataVencimentoInicialEditavel;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="per_rematricula_online_inicio", type="datetime", nullable=false)
     */
    private $perRematriculaOnlineInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="per_rematricula_online_fim", type="datetime", nullable=false)
     */
    private $perRematriculaOnlineFim;

    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getPerId()
    {
        return $this->perId;
    }

    /**
     * @param int $perId
     */
    public function setPerId($perId)
    {
        $this->perId = $perId;

        return $this;
    }

    /**
     * @return string
     */
    public function getPerNome()
    {
        return $this->perNome;
    }

    /**
     * @param string $perNome
     */
    public function setPerNome($perNome)
    {
        $this->perNome = $perNome;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPerAno()
    {
        return $this->perAno;
    }

    /**
     * @param \DateTime $perAno
     */
    public function setPerAno($perAno)
    {
        $this->perAno = $perAno;

        return $this;
    }

    /**
     * @return int
     */
    public function getPerEtapas()
    {
        return $this->perEtapas;
    }

    /**
     * @param int $perEtapas
     */
    public function setPerEtapas($perEtapas)
    {
        $this->perEtapas = $perEtapas;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPerDataFim()
    {
        return $this->perDataFim;
    }

    /**
     * @param \DateTime $perDataFim
     */
    public function setPerDataFim($perDataFim)
    {
        $this->perDataFim = $perDataFim;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPerDataInicio()
    {
        return $this->perDataInicio;
    }

    /**
     * @return bool
     */
    public function verificarVigencia()
    {
        $dataAtual = new \DateTime();

        return ($dataAtual >= $this->getPerDataInicio() && $dataAtual <= $this->getPerDataFim());
    }

    /**
     * @param \DateTime $perDataInicio
     */
    public function setPerDataInicio($perDataInicio)
    {
        $this->perDataInicio = $perDataInicio;

        return $this;
    }

    /**
     * @return string
     */
    public function getPerSemestre()
    {
        return $this->perSemestre;
    }

    /**
     * @param string $perSemestre
     */
    public function setPerSemestre($perSemestre)
    {
        $this->perSemestre = $perSemestre;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPerMatriculaInicio()
    {
        return $this->perMatriculaInicio;
    }

    /**
     * @param \DateTime $perMatriculaInicio
     */
    public function setPerMatriculaInicio($perMatriculaInicio)
    {
        $this->perMatriculaInicio = $perMatriculaInicio;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPerMatriculaFim()
    {
        return $this->perMatriculaFim;
    }

    /**
     * @param \DateTime $perMatriculaFim
     */
    public function setPerMatriculaFim($perMatriculaFim)
    {
        $this->perMatriculaFim = $perMatriculaFim;

        return $this;
    }

    /**
     * @param boolean $dataFormatada
     * @return \DateTime
     */
    public function getPerDataVencimentoInicial($dataFormatada = false)
    {
        if (!$data = $this->perDataVencimentoInicial) {
            $data = $this->perDataInicio;
        }

        if ($dataFormatada) {
            return $data->format('d/m/Y H:i:s');
        }

        return $data;
    }

    /**
     * @param \DateTime $perDataVencimentoInicial
     */
    public function setPerDataVencimentoInicial($perDataVencimentoInicial)
    {
        $this->perDataVencimentoInicial = $perDataVencimentoInicial;

        return $this;
    }

    /**
     * @return string
     */
    public function getPerDataVencimentoInicialEditavel()
    {
        return $this->perDataVencimentoInicialEditavel;
    }

    /**
     * @param string $perDataVencimentoInicialEditavel
     */
    public function setPerDataVencimentoInicialEditavel($perDataVencimentoInicialEditavel)
    {
        $this->perDataVencimentoInicialEditavel = $perDataVencimentoInicialEditavel;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \Zend\Stdlib\Hydrator\ClassMethods())->extract($this);
    }

    public function setPeriodicidade($periodicidade)
    {
        $this->periodicidade = $periodicidade;
    }

    /**
     * @return string
     */
    public function getPeriodicidade()
    {
        return $this->periodicidade;
    }

    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    public function setPerDataFinalizacao($perDataFinalizacao)
    {
        $this->perDataFinalizacao = $perDataFinalizacao;
    }

    /**
     * @return \Datetime
     */
    public function getPerDataFinalizacao()
    {
        return $this->perDataFinalizacao;
    }

    public function setCursoCampus(\Doctrine\Common\Collections\Collection $cursoCampus)
    {
        $this->cursoCampus = $cursoCampus;
    }

    public function getCursoCampus()
    {
        return $this->cursoCampus;
    }

    /**
     * @param \Matricula\Entity\AcadNivel $nivel
     * @return \Matricula\Entity\AcadNivel
     */
    public function setNivel($nivel)
    {
        $this->nivel = $nivel;
    }

    /**
     * @return \Matricula\Entity\AcadNivel
     */
    public function getNivel()
    {
        return $this->nivel;
    }

    public function setSelecaoEdicao(\Doctrine\Common\Collections\Collection $selecaoEdicao)
    {
        $this->selecaoEdicao = $selecaoEdicao;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSelecaoEdicao()
    {
        return $this->selecaoEdicao;
    }

    /**
     * @return \DateTime
     */
    public function getPerRematriculaOnlineInicio()
    {
        return $this->perRematriculaOnlineInicio;
    }

    /**
     * @param \DateTime $perRematriculaOnlineInicio
     * @return AcadperiodoLetivo
     */
    public function setPerRematriculaOnlineInicio($perRematriculaOnlineInicio)
    {
        $this->perRematriculaOnlineInicio = $perRematriculaOnlineInicio;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPerRematriculaOnlineFim()
    {
        return $this->perRematriculaOnlineFim;
    }

    /**
     * @param \DateTime $perRematriculaOnlineFim
     * @return AcadperiodoLetivo
     */
    public function setPerRematriculaOnlineFim($perRematriculaOnlineFim)
    {
        $this->perRematriculaOnlineFim = $perRematriculaOnlineFim;

        return $this;
    }
}
