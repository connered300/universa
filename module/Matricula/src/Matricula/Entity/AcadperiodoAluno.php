<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AcadperiodoAluno
 *
 * @ORM\Table(name="acadperiodo__aluno", indexes={@ORM\Index(name="fk_matricula_aluno_periodo_periodo_turma1_idx", columns={"turma_id"}), @ORM\Index(name="fk_matricula_aluno_periodo_matricula_situacao1_idx", columns={"matsituacao_id"}), @ORM\Index(name="fk_acadperiodo__aluno_acadgeral__aluno_curso1_idx", columns={"alunocurso_id"})})
 * @ORM\Entity(repositoryClass="Matricula\Entity\Repository\AcadperiodoAluno")
 * @LG\LG(id="alunoper_id",label="alunoperId")
 * @Jarvis\Jarvis(title="Alunos",icon="fa fa-table")
 */
class AcadperiodoAluno
{
    /**
     * @var integer
     *
     * @ORM\Column(name="alunoper_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="alunoper_id")
     * @LG\Labels\Attributes(text="Indice",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $alunoperId;

    /**
     * @var AcadgeralAlunoCurso
     *
     * @ORM\ManyToOne(targetEntity="AcadgeralAlunoCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="alunocurso_id", referencedColumnName="alunocurso_id")
     * })
     * @LG\Labels\Property(name="alunocurso_id")
     * @LG\Labels\Attributes(text="Matricula",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $alunocurso;

    /**
     * @LG\Labels\Property(name="pes_nome")
     * @LG\Labels\Attributes(text="Aluno",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */

    private $pesNome;

    /**
     * @LG\Labels\Property(name="periodo")
     * @LG\Labels\Attributes(text="Periodo",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */

    private $periodo;

    /**
     * @LG\Labels\Property(name="turma_nome")
     * @LG\Labels\Attributes(text="Turma",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE%")
     */

    private $turmaNome;

    /**
     * @LG\Labels\Property(name="situacao_aluno")
     * @LG\Labels\Attributes(text="Situção",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE%")
     */

    private $situacaoAluno;

    /**
     * @LG\Labels\Property(name="rematricula")
     * @LG\Labels\Attributes(text="Rematricula",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE%")
     */

    private $rematricula;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="alunoper_sit_data", type="datetime", nullable=false)
     */

    private $alunoperSitData;

    /**
     * @var AcadgeralSituacao
     *
     * @ORM\ManyToOne(targetEntity="AcadgeralSituacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="matsituacao_id", referencedColumnName="situacao_id")
     * })
     */
    private $matsituacao;

    /**
     * @var AcadperiodoTurma
     *
     * @ORM\ManyToOne(targetEntity="AcadperiodoTurma")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="turma_id", referencedColumnName="turma_id")
     * })
     */
    private $turma;

    /**
     * @var string
     *
     * @ORM\Column(name="alunoper_observacoes", type="text", length=65535, nullable=true)
     */
    private $alunoperObservacoes;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     *
     * @ORM\ManyToOne(targetEntity="\Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_criador", referencedColumnName="id")
     * })
     */
    private $usuarioCriador;

    public function __construct(array $data)
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getAlunoperId()
    {
        return $this->alunoperId;
    }

    /**
     * @param int $alunoperId
     */
    public function setAlunoperId($alunoperId)
    {
        $this->alunoperId = $alunoperId;
    }

    /**
     * @return \DateTime
     */
    public function getAlunoperSitData()
    {
        return $this->alunoperSitData;
    }

    /**
     * @param \DateTime $alunoperSitData
     */
    public function setAlunoperSitData($alunoperSitData)
    {
        $this->alunoperSitData = $alunoperSitData;
    }

    /**
     * @return AcadgeralAlunoCurso
     */
    public function getAlunocurso()
    {
        return $this->alunocurso;
    }

    /**
     * @param AcadgeralAlunoCurso $alunocurso
     */
    public function setAlunocurso($alunocurso)
    {
        $this->alunocurso = $alunocurso;
    }

    /**
     * @return AcadgeralSituacao
     */
    public function getMatsituacao()
    {
        return $this->matsituacao;
    }

    /**
     * @param AcadgeralSituacao $matsituacao
     */
    public function setMatsituacao($matsituacao)
    {
        $this->matsituacao = $matsituacao;
    }

    /**
     * @return AcadperiodoTurma
     */
    public function getTurma()
    {
        return $this->turma;
    }

    /**
     * @param AcadperiodoTurma $turma
     */
    public function setTurma($turma)
    {
        $this->turma = $turma;
    }

    /**
     * @return string
     */
    public function getAlunoperObservacoes()
    {
        return $this->alunoperObservacoes;
    }

    /**
     * @param string $alunoperObservacoes
     * @return AcadperiodoAluno
     */
    public function setAlunoperObservacoes($alunoperObservacoes)
    {
        $this->alunoperObservacoes = $alunoperObservacoes;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuarioCriador()
    {
        return $this->usuarioCriador;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuarioCriador
     * @return AcadperiodoAluno
     */
    public function setUsuarioCriador($usuarioCriador)
    {
        $this->usuarioCriador = $usuarioCriador;

        return $this;
    }


    public function retornaArrayParaHistorico()
    {
        return [

            'situacaoperData'                 => $this->getAlunoperSitData(),
            'situacaoperHistoricoObservacoes' => $this->getAlunoperObservacoes(),
            'situacao'                        => $this->getMatsituacao(),
            'turma'                           => $this->getTurma(),
            'usuarioHistorico'                => $this->getUsuarioCriador(),

        ];
    }

    public function toArray()
    {
        $arrAlunoCurso = $this->getAlunocurso() ? $this->getAlunocurso()->toArray() : [];
        $arrDados      = [
            'alunoperId'            => $this->getAlunoperId(),
            'alunoperCurso'         => $this->getAlunocurso(),
            'alunoperSitData'       => $this->getAlunoperSitData(),
            'matsitDescricao'       => $this->getMatsituacao()->getMatsitDescricao(),
            'matsituacao'           => $this->getMatsituacao()->getSituacaoId(),
            'situacaoId'            => $this->getMatsituacao()->getSituacaoId(),
            'alunocurso'            => $this->getAlunocurso()->getAlunocursoId(),
            'turmaId'               => $this->getTurma()->getTurmaId(),
            'turma'                 => $this->getTurma()->getTurmaId(),
            'turmaNome'             => $this->getTurma()->getTurmaNome(),
            'turmaTurno'            => $this->getTurma()->getTurmaTurno(),
            'serie'                 => $this->getTurma()->getTurmaSerie(),
            'PeriodoLetivoAtual'    => $this->getTurma()->getPer(),
            //            'turmaSerie'            => $this->getTurma()->getTurmaSerie(),
            'alunoId'               => $this->getAlunocurso()->getAluno()->getAlunoId(),
            'alunoDataCadastro'     => $this->getAlunocurso()->getAluno()->getAlunoDataCadastro(true),
            'alunoEtnia'            => $this->getAlunocurso()->getAluno()->getAlunoEtnia(),
            'alunoMae'              => $this->getAlunocurso()->getAluno()->getAlunoMae(),
            'alunoPai'              => $this->getAlunocurso()->getAluno()->getAlunoPai(),
            'alunoCertMilitar'      => $this->getAlunocurso()->getAluno()->getAlunoCertMilitar(),
            'alunoSecaoEleitoral'   => $this->getAlunocurso()->getAluno()->getAlunoSecaoEleitoral(),
            'alunoTituloEleitoral'  => $this->getAlunocurso()->getAluno()->getAlunoTituloEleitoral(),
            'alunoZonaEleitoral'    => $this->getAlunocurso()->getAluno()->getAlunoZonaEleitoral(),
            'arq'                   => $this->getAlunocurso()->getAluno()->getArq(),
            'alunocursoObservacoes' => $this->getAlunocurso()->getAlunocursoObservacoes(),
            'pes'                   => $this->getAlunocurso()->getAluno()->getPes()->getPes()->getPesId(),
            'pesNome'               => $this->getAlunocurso()->getAluno()->getPes()->getPes()->getPesNome(),
            'pesNacionalidade'      => $this->getAlunocurso()->getAluno()->getPes()->getPes()->getPesNacionalidade(),
            'pesNaturalidade'       => $this->getAlunocurso()->getAluno()->getPes()->getPesNaturalidade(),
            'pesCpf'                => $this->getAlunocurso()->getAluno()->getPes()->getPesCpf(),
            'pesRg'                 => $this->getAlunocurso()->getAluno()->getPes()->getPesRg(),
            'pesRgEmissao'          => $this->getAlunocurso()->getAluno()->getPes()->getPesRgEmissao(),
            'pesDataNascimento'     => $this->getAlunocurso()->getAluno()->getPes()->getPesDataNascimento(),
            'pesNascUf'             => $this->getAlunocurso()->getAluno()->getPes()->getPesNascUf(),
            'pesFalecido'           => $this->getAlunocurso()->getAluno()->getPes()->getPesFalecido(),
            'pesSexo'               => $this->getAlunocurso()->getAluno()->getPes()->getPesSexo(),
            'usuarioCriador'        => $this->getUsuarioCriador(),
        ];

        return array_merge(array_filter($arrAlunoCurso), $arrDados);
    }
}
