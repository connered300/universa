<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoAlunoDisciplinaHistorico
 *
 * @ORM\Table(name="acadperiodo__aluno_disciplina_historico", indexes={@ORM\Index(name="fk_acadgeral__situacao_acadperiodo__aluno_disciplina_acadpe_idx", columns={"alunodisc_id"}), @ORM\Index(name="fk_acadgeral__situacao_acadperiodo__aluno_disciplina_acadge_idx", columns={"situacao_id"}), @ORM\Index(name="fk_acadgeral__situacao_acadperiodo__aluno_disciplina_acesso_idx", columns={"usuario_historico"}), @ORM\Index(name="fk_acadperiodo__aluno_disciplina_historico_acadperiodo__tur_idx", columns={"turma_id"})})
 * @ORM\Entity
 */
class AcadperiodoAlunoDisciplinaHistorico
{
    /**
     * @var integer
     *
     * @ORM\Column(name="situacaodisc_historico_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $situacaodiscHistoricoId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="situacaodisc_data", type="datetime", nullable=false)
     */
    private $situacaodiscData;

    /**
     * @var string
     *
     * @ORM\Column(name="situacaodisc_observacoes", type="text", length=65535, nullable=true)
     */
    private $situacaodiscObservacoes;

    /**
     * @var \Matricula\Entity\AcadgeralSituacao
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadgeralSituacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="situacao_id", referencedColumnName="situacao_id")
     * })
     */
    private $situacao;

    /**
     * @var \Matricula\Entity\AcadperiodoAlunoDisciplina
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoAlunoDisciplina")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="alunodisc_id", referencedColumnName="alunodisc_id")
     * })
     */
    private $alunodisc;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     *
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_historico", referencedColumnName="id")
     * })
     */
    private $usuarioHistorico;

    /**
     * @var \Matricula\Entity\AcadperiodoTurma
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoTurma")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="turma_id", referencedColumnName="turma_id")
     * })
     */
    private $turma;

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getSituacaodiscHistoricoId()
    {
        return $this->situacaodiscHistoricoId;
    }

    /**
     * @param int $situacaodiscHistoricoId
     * @return AcadperiodoAlunoDisciplinaHistorico
     */
    public function setSituacaodiscHistoricoId($situacaodiscHistoricoId)
    {
        $this->situacaodiscHistoricoId = $situacaodiscHistoricoId;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getSituacaodiscData()
    {
        return $this->situacaodiscData;
    }

    /**
     * @param \DateTime $situacaodiscData
     * @return AcadperiodoAlunoDisciplinaHistorico
     */
    public function setSituacaodiscData($situacaodiscData)
    {
        $this->situacaodiscData = $situacaodiscData;

        return $this;
    }

    /**
     * @return string
     */
    public function getSituacaodiscObservacoes()
    {
        return $this->situacaodiscObservacoes;
    }

    /**
     * @param string $situacaodiscObservacoes
     * @return AcadperiodoAlunoDisciplinaHistorico
     */
    public function setSituacaodiscObservacoes($situacaodiscObservacoes)
    {
        $this->situacaodiscObservacoes = $situacaodiscObservacoes;

        return $this;
    }

    /**
     * @return AcadgeralSituacao
     */
    public function getSituacao()
    {
        return $this->situacao;
    }

    /**
     * @param AcadgeralSituacao $situacao
     * @return AcadperiodoAlunoDisciplinaHistorico
     */
    public function setSituacao($situacao)
    {
        $this->situacao = $situacao;

        return $this;
    }

    /**
     * @return AcadperiodoAlunoDisciplina
     */
    public function getAlunodisc()
    {
        return $this->alunodisc;
    }

    /**
     * @param AcadperiodoAlunoDisciplina $alunodisc
     * @return AcadperiodoAlunoDisciplinaHistorico
     */
    public function setAlunodisc($alunodisc)
    {
        $this->alunodisc = $alunodisc;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuarioHistorico()
    {
        return $this->usuarioHistorico;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuarioHistorico
     * @return AcadperiodoAlunoDisciplinaHistorico
     */
    public function setUsuarioHistorico($usuarioHistorico)
    {
        $this->usuarioHistorico = $usuarioHistorico;

        return $this;
    }

   /**
    * @return \Matricula\Entity\AcadperiodoTurma
    */
    public function getTurma()
    {
        return $this->turma;
    }

    /**
     * @param AcadperiodoTurma $turma
     * @return AcadperiodoAlunoDisciplinaHistorico
     */
    public function setTurma($turma)
    {
        $this->turma = $turma;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new \VersaSpine\Stdlib\Hydrator())->extract($this);
    }
}