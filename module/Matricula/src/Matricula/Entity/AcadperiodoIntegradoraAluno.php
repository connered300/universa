<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoIntegradoraAluno
 *
 * @ORM\Table(name="acadperiodo__integradora_aluno", uniqueConstraints={@ORM\UniqueConstraint(name="unq_aluno_int", columns={"integradora_id", "alunodisc_id"})}, indexes={@ORM\Index(name="fk_acadperiodo__aluno_acadperiodo__integradora_acadperiodo__idx", columns={"integradora_id"}), @ORM\Index(name="fk_acadperiodo__aluno_integradora_acadperiodo__aluno_discip_idx", columns={"alunodisc_id"})})
 * @ORM\Entity
 */
class AcadperiodoIntegradoraAluno
{
    /**
     * @var integer
     *
     * @ORM\Column(name="alunointeg_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $alunointegId;

    /**
     * @var string
     *
     * @ORM\Column(name="alunointeg_respostas", type="string", length=255, nullable=false)
     */
    private $alunointegRespostas;

    /**
     * @var integer
     *
     * @ORM\Column(name="alunointeg_acertos", type="integer", nullable=false)
     */
    private $alunointegAcertos;

    /**
     * @var float
     *
     * @ORM\Column(name="alunointeg_nota_isolada", type="float", precision=10, scale=0, nullable=true)
     */
    private $alunointegNotaIsolada;

    /**
     * @var \AcadperiodoIntegradora
     *
     * @ORM\ManyToOne(targetEntity="AcadperiodoIntegradora")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="integradora_id", referencedColumnName="integradora_id")
     * })
     */
    private $integradora;

    /**
     * @var \AcadperiodoAlunoDisciplina
     *
     * @ORM\ManyToOne(targetEntity="AcadperiodoAlunoDisciplina")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="alunodisc_id", referencedColumnName="alunodisc_id")
     * })
     */
    private $alunodisc;

    /**
     * @return int
     */
    public function getAlunointegId()
    {
        return $this->alunointegId;
    }

    /**
     * @param int $alunointegId
     */
    public function setAlunointegId($alunointegId)
    {
        $this->alunointegId = $alunointegId;
    }

    /**
     * @return string
     */
    public function getAlunointegRespostas()
    {
        return $this->alunointegRespostas;
    }

    /**
     * @param string $alunointegRespostas
     */
    public function setAlunointegRespostas($alunointegRespostas)
    {
        $this->alunointegRespostas = $alunointegRespostas;
    }

    /**
     * @return int
     */
    public function getAlunointegAcertos()
    {
        return $this->alunointegAcertos;
    }

    /**
     * @param int $alunointegAcertos
     */
    public function setAlunointegAcertos($alunointegAcertos)
    {
        $this->alunointegAcertos = $alunointegAcertos;
    }

    /**
     * @return float
     */
    public function getAlunointegNotaIsolada()
    {
        return $this->alunointegNotaIsolada;
    }

    /**
     * @param float $alunointegNotaIsolada
     */
    public function setAlunointegNotaIsolada($alunointegNotaIsolada)
    {
        $this->alunointegNotaIsolada = $alunointegNotaIsolada;
    }

    /**
     * @return AcadperiodoIntegradora
     */
    public function getIntegradora()
    {
        return $this->integradora;
    }

    /**
     * @param AcadperiodoIntegradora $integradora
     */
    public function setIntegradora($integradora)
    {
        $this->integradora = $integradora;
    }

    /**
     * @return AcadperiodoAlunoDisciplina
     */
    public function getAlunodisc()
    {
        return $this->alunodisc;
    }

    /**
     * @param AcadperiodoAlunoDisciplina $alunodisc
     */
    public function setAlunodisc($alunodisc)
    {
        $this->alunodisc = $alunodisc;
    }
}
