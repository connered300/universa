<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AcadgeralAlunoFormacao
 *
 * @ORM\Table(name="acadgeral__aluno_formacao", indexes={@ORM\Index(name="fk_acadgeral__aluno_formacao_acadgeral__aluno_curso1_idx", columns={"aluno_id"})})
 * @ORM\Entity
 */
class AcadgeralAlunoFormacao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="formacao_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $formacaoId;

    /**
     * @var string
     *
     * @ORM\Column(name="formacao_curso", type="string", length=150, nullable=false)
     */
    private $formacaoCurso;

    /**
     * @var string
     *
     * @ORM\Column(name="formacao_instituicao", type="string", length=250, nullable=false)
     */
    private $formacaoInstituicao;

    /**
     * @var string
     *
     * @ORM\Column(name="formacao_cidade", type="string", length=100, nullable=true)
     */
    private $formacaoCidade;

    /**
     * @var string
     *
     * @ORM\Column(name="formacao_estado", type="string", length=50, nullable=true)
     */
    private $formacaoEstado;

    /**
     * @var string
     *
     * @ORM\Column(name="formacao_tipo_ensino", type="string", nullable=true)
     */
    private $formacaoTipoEnsino;

    /**
     * @var string
     *
     * @ORM\Column(name="formacao_tipo", type="string", nullable=false)
     */
    private $formacaoTipo;

    /**
     * @var integer
     *
     * @ORM\Column(name="formacao_ano", type="integer", nullable=true)
     */
    private $formacaoAno;

    /**
     * @var string
     *
     * @ORM\Column(name="formacao_regime", type="string", nullable=true)
     */
    private $formacaoRegime;

    /**
     * @var integer
     *
     * @ORM\Column(name="formacao_duracao", type="integer", nullable=true)
     */
    private $formacaoDuracao;

    /**
     * @var string
     *
     * @ORM\Column(name="formacao_concluido", type="string", nullable=false)
     */
    private $formacaoConcluido;

    /**
     * @var string
     *
     * @ORM\Column(name="formacao_obs", type="text", nullable=true)
     * @LG\Labels\Property(name="formacao_obs")
     * @LG\Labels\Attributes(text="observação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $formacaoObs;

    /**
     * @var \Matricula\Entity\AcadgeralAluno
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadgeralAluno")
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(name="aluno_id", referencedColumnName="aluno_id")
     * })
     */
    private $aluno;

    /**
     * @return integer
     */
    public function getFormacaoId()
    {
        return $this->formacaoId;
    }

    /**
     * @param integer $formacaoId
     * @return AcadgeralAlunoFormacao
     */
    public function setFormacaoId($formacaoId)
    {
        $this->formacaoId = $formacaoId;

        return $this;
    }

    /**
     * @return \Matricula\Entity\AcadgeralAluno
     */
    public function getAluno()
    {
        return $this->aluno;
    }

    /**
     * @param \Matricula\Entity\AcadgeralAluno $aluno
     * @return AcadgeralAlunoFormacao
     */
    public function setAluno($aluno)
    {
        $this->aluno = $aluno;

        return $this;
    }

    /**
     * @return string
     */
    public function getFormacaoInstituicao()
    {
        return $this->formacaoInstituicao;
    }

    /**
     * @param string $formacaoInstituicao
     * @return AcadgeralAlunoFormacao
     */
    public function setFormacaoInstituicao($formacaoInstituicao)
    {
        $this->formacaoInstituicao = $formacaoInstituicao;

        return $this;
    }

    /**
     * @return string
     */
    public function getFormacaoCidade()
    {
        return $this->formacaoCidade;
    }

    /**
     * @param string $formacaoCidade
     * @return AcadgeralAlunoFormacao
     */
    public function setFormacaoCidade($formacaoCidade)
    {
        $this->formacaoCidade = $formacaoCidade ? $formacaoCidade : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getFormacaoEstado()
    {
        return $this->formacaoEstado;
    }

    /**
     * @param string $formacaoEstado
     * @return AcadgeralAlunoFormacao
     */
    public function setFormacaoEstado($formacaoEstado)
    {
        $this->formacaoEstado = $formacaoEstado ? $formacaoEstado : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getFormacaoCurso()
    {
        return $this->formacaoCurso;
    }

    /**
     * @param string $formacaoCurso
     * @return AcadgeralAlunoFormacao
     */
    public function setFormacaoCurso($formacaoCurso)
    {
        $this->formacaoCurso = $formacaoCurso;

        return $this;
    }

    /**
     * @return string
     */
    public function getFormacaoTipoEnsino()
    {
        return $this->formacaoTipoEnsino;
    }

    /**
     * @param string $formacaoTipoEnsino
     * @return AcadgeralAlunoFormacao
     */
    public function setFormacaoTipoEnsino($formacaoTipoEnsino)
    {
        $this->formacaoTipoEnsino = $formacaoTipoEnsino ? $formacaoTipoEnsino : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getFormacaoTipo()
    {
        return $this->formacaoTipo;
    }

    /**
     * @param string $formacaoTipo
     * @return AcadgeralAlunoFormacao
     */
    public function setFormacaoTipo($formacaoTipo)
    {
        $this->formacaoTipo = $formacaoTipo ? $formacaoTipo : null;

        return $this;
    }

    /**
     * @return integer
     */
    public function getFormacaoAno()
    {
        return $this->formacaoAno;
    }

    /**
     * @param integer $formacaoAno
     * @return AcadgeralAlunoFormacao
     */
    public function setFormacaoAno($formacaoAno)
    {
        $this->formacaoAno = $formacaoAno ? $formacaoAno : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getFormacaoConcluido()
    {
        return $this->formacaoConcluido;
    }

    /**
     * @param string $formacaoConcluido
     * @return AcadgeralAlunoFormacao
     */
    public function setFormacaoConcluido($formacaoConcluido)
    {
        $this->formacaoConcluido = $formacaoConcluido ? $formacaoConcluido : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getFormacaoRegime()
    {
        return $this->formacaoRegime;
    }

    /**
     * @param string $formacaoRegime
     * @return AcadgeralAlunoFormacao
     */
    public function setFormacaoRegime($formacaoRegime)
    {
        $this->formacaoRegime = $formacaoRegime ? $formacaoRegime : null;

        return $this;
    }

    /**
     * @return integer
     */
    public function getFormacaoDuracao()
    {
        return $this->formacaoDuracao;
    }

    /**
     * @param integer $formacaoDuracao
     * @return AcadgeralAlunoFormacao
     */
    public function setFormacaoDuracao($formacaoDuracao)
    {
        $this->formacaoDuracao = $formacaoDuracao ? $formacaoDuracao : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getFormacaoObs()
    {
        return $this->formacaoObs;
    }

    /**
     * @param string $formacaoObs
     * @return AcadgeralAlunoFormacao
     */
    public function setFormacaoObs($formacaoObs)
    {
        $this->formacaoObs = $formacaoObs ? $formacaoObs : null;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'formacaoId'          => $this->getFormacaoId(),
            'aluno'               => $this->getAluno(),
            'formacaoInstituicao' => $this->getFormacaoInstituicao(),
            'formacaoCidade'      => $this->getFormacaoCidade(),
            'formacaoEstado'      => $this->getFormacaoEstado(),
            'formacaoCurso'       => $this->getFormacaoCurso(),
            'formacaoTipoEnsino'  => $this->getFormacaoTipoEnsino(),
            'formacaoTipo'        => $this->getFormacaoTipo(),
            'formacaoAno'         => $this->getFormacaoAno(),
            'formacaoConcluido'   => $this->getFormacaoConcluido(),
            'formacaoRegime'      => $this->getFormacaoRegime(),
            'formacaoDuracao'     => $this->getFormacaoDuracao(),
            'formacaoObs'         => $this->getFormacaoObs(),
        );

        $array['aluno'] = $this->getAluno() ? $this->getAluno()->getAlunoId() : null;

        return $array;
    }
}