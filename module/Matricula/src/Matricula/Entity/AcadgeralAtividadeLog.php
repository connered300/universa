<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\Service\AbstractService;

/**
 * Log
 *
 * @ORM\Table(name="acadgeral__atividade_log")
 * @ORM\Entity
 * @LG\LG(id="log_id",label="logId")
 */
class AcadgeralAtividadeLog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="log_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $logId;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="log_data", type="datetime", nullable=false)
     */
    private $logData;

    /**
     * @var string
     *
     * @ORM\Column(name="log_action", type="string", length=200, nullable=false)
     */
    private $logAction;

    /**
     * @var string
     *
     * @ORM\Column(name="log_user_agent", type="string", length=200, nullable=false)
     */
    private $logUserAgent;

    /**
     * @var string
     *
     * @ORM\Column(name="log_ip", type="string", length=45, nullable=false)
     */
    private $logIp;

    /**
     * @var \Matricula\Entity\AcadgeralAluno
     *
     * @ORM\ManyToOne(targetEntity="AcadgeralAluno")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="aluno_id", referencedColumnName="aluno_id")
     * })
     */
    private $aluno;

    /**
     * @var \Matricula\Entity\AcadgeralDisciplina
     *
     * @ORM\ManyToOne(targetEntity="AcadgeralDisciplina")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="disc_id", referencedColumnName="disc_id")
     * })
     */
    private $disciplina;

    /**
     * @var \Matricula\Entity\AcadCurso
     *
     * @ORM\ManyToOne(targetEntity="AcadCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="curso_id", referencedColumnName="curso_id")
     * })
     */
    private $curso;

    /**
     * @var string
     *
     * @ORM\Column(name="log_info", type="string", length=3000)
     */
    private $logInfo;

    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    public function getLogId()
    {
        return $this->logId;
    }

    public function getLogData()
    {
        return $this->logData;
    }

    public function getLogAction()
    {
        return $this->logAction;
    }

    public function getLogUserAgent()
    {
        return $this->logUserAgent;
    }

    public function getLogIp()
    {
        return $this->logIp;
    }

    public function getLogInfo()
    {
        return $this->logInfo;
    }

    public function setLogId($logId)
    {
        $this->logId = $logId;

        return $this;
    }

    public function setLogData($logData)
    {
        if ($logData) {
            if (is_string($logData)) {
                $logData = \VersaSpine\Service\AbstractService::formatDateAmericano($logData);
                $logData = new \Datetime($logData);
            }
        } else {
            $logData = null;
        }

        $this->logData = $logData;

        return $this;
    }

    public function setLogAction($logAction)
    {
        $this->logAction = $logAction;

        return $this;
    }

    public function setLogUserAgent($logUserAgent)
    {
        $this->logUserAgent = $logUserAgent;

        return $this;
    }

    public function setLogIp($logIp)
    {
        $this->logIp = $logIp;

        return $this;
    }

    public function setLogInfo($logInfo)
    {
        $this->logInfo = $logInfo;

        return $this;
    }

    /**
     * @return \Matricula\Entity\AcadgeralAluno
     */
    public function getAluno()
    {
        return $this->aluno;
    }

    /**
     * @return \Matricula\Entity\AcadgeralDisciplina
     */
    public function getDisciplina()
    {
        return $this->disciplina;
    }

    /**
     * @return \Matricula\Entity\AcadCurso
     */
    public function getCurso()
    {
        return $this->curso;
    }

    /**
     * @param $aluno \Matricula\Entity\AcadgeralAluno
     * @return \Matricula\Entity\AcadgeralAtividadeLog
     */
    public function setAluno($aluno)
    {
        $this->aluno = $aluno;

        return $this;
    }

    /**
     * @param $disciplina \Matricula\Entity\AcadgeralDisciplina
     * @return \Matricula\Entity\AcadgeralAtividadeLog
     */
    public function setDisciplina($disciplina)
    {
        $this->disciplina = $disciplina;

        return $this;
    }

    /**
     * @param $curso \Matricula\Entity\AcadCurso
     * @return \Matricula\Entity\AcadgeralAtividadeLog
     */
    public function setCurso($curso)
    {
        $this->curso = $curso;

        return $this;
    }

    public function toArray()
    {
        return array(
            'logId'        => $this->getLogId(),
            'logData'      => $this->getLogData(),
            'logAction'    => $this->getLogAction(),
            'logUserAgent' => $this->getLogUserAgent(),
            'logIp'        => $this->getLogIp(),
            'logInfo'      => $this->getLogInfo(),
            'disciplina'   => $this->getDisciplina(),
            'curso'        => $this->getCurso(),
            'aluno'        => $this->getAluno()
        );
    }
}