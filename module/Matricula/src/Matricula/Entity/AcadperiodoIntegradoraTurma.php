<?php

namespace Matricula\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoIntegradoraTurma
 *
 * @ORM\Table(name="acadperiodo__integradora_turma", indexes={@ORM\Index(name="fk_acadperiodo__integradora_turma_acadperiodo__integradora__idx", columns={"integavaliacao_id"}), @ORM\Index(name="fk_acadperiodo__integradora_turma_acadperiodo__turma1_idx", columns={"turma_id"})})
 * @ORM\Entity
 */
class AcadperiodoIntegradoraTurma
{
    /**
     * @var integer
     *
     * @ORM\Column(name="integturma_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $integturmaId;

    /**
     * @var AcadperiodoIntegradoraAvaliacao
     *
     * @ORM\ManyToOne(targetEntity="AcadperiodoIntegradoraAvaliacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="integavaliacao_id", referencedColumnName="integavaliacao_id")
     * })
     */
    private $integavaliacao;

    /**
     * @var AcadperiodoTurma
     *
     * @ORM\ManyToOne(targetEntity="AcadperiodoTurma")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="turma_id", referencedColumnName="turma_id")
     * })
     */
    private $turma;

    /**
     * @return int
     */
    public function getIntegturmaId()
    {
        return $this->integturmaId;
    }

    /**
     * @param int $integturmaId
     */
    public function setIntegturmaId($integturmaId)
    {
        $this->integturmaId = $integturmaId;
    }

    /**
     * @return AcadperiodoIntegradoraAvaliacao
     */
    public function getIntegavaliacao()
    {
        return $this->integavaliacao;
    }

    /**
     * @param AcadperiodoIntegradoraAvaliacao $integavaliacao
     */
    public function setIntegavaliacao($integavaliacao)
    {
        $this->integavaliacao = $integavaliacao;
    }

    /**
     * @return AcadperiodoTurma
     */
    public function getTurma()
    {
        return $this->turma;
    }

    /**
     * @param AcadperiodoTurma $turma
     */
    public function setTurma($turma)
    {
        $this->turma = $turma;
    }
}
