<?php

namespace Matricula;

return array(
    'router'                    => array(
        'routes' => array(
            'matricula' => array(
                'type'          => 'Literal',
                'options'       => array(
                    'route'    => '/matricula',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Matricula\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes'  => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'       => '/[:controller[/:action[/:id][/:id2]]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults'    => array(),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager'           => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases'            => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'doctrine'                  => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default'             => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            ),
        ),
    ),
    'controllers'               => array(
        'invokables' => array(
            'Matricula\Controller\AcadgeralAlunoFormacao'          => 'Matricula\Controller\AcadgeralAlunoFormacaoController',
            'Matricula\Controller\AcadgeralAluno'                  => 'Matricula\Controller\AcadgeralAlunoController',
            'Matricula\Controller\AcadgeralAlunoCaptacao'          => 'Matricula\Controller\AcadgeralAlunoCaptacaoController',
            'Matricula\Controller\AcadgeralAlunoCurso'             => 'Matricula\Controller\AcadgeralAlunoCursoController',
            'Matricula\Controller\AcadgeralDocente'             => 'Matricula\Controller\AcadgeralDocenteController',
            'Matricula\Controller\AcadperiodoTurma'                => 'Matricula\Controller\AcadperiodoTurmaController',
            'Matricula\Controller\AcadperiodoMatrizDisciplina'     => 'Matricula\Controller\AcadperiodoMatrizDisciplinaController',
            'Matricula\Controller\AcadgeralResponsavel'            => 'Matricula\Controller\AcadgeralResponsavelController',
            'Matricula\Controller\AcadperiodoAluno'                => 'Matricula\Controller\AcadperiodoAlunoController',
            'Matricula\Controller\AcadperiodoAlunoDisciplina'      => 'Matricula\Controller\AcadperiodoAlunoDisciplinaController',
            'Matricula\Controller\Relatorios'                      => 'Matricula\Controller\RelatoriosController',
            'Matricula\Controller\AcadperiodoIntegradoraCaderno'   => 'Matricula\Controller\AcadperiodoIntegradoraCadernoController',
            'Matricula\Controller\AcadperiodoIntegradoraAvaliacao' => 'Matricula\Controller\AcadperiodoIntegradoraAvaliacaoController',
            'Matricula\Controller\Aluno'                           => 'Matricula\Controller\AlunoController',
            'Matricula\Controller\Notas'                           => 'Matricula\Controller\NotasController',
            'Matricula\Controller\AcadperiodoDocenteDisciplina'    => 'Matricula\Controller\AcadperiodoDocenteDisciplinaController',
            'Matricula\Controller\AcadperiodoDocentes'             => 'Matricula\Controller\AcadperiodoDocentesController',
            'Matricula\Controller\AcadgeralDocente'                => 'Matricula\Controller\AcadgeralDocenteController',
            'Matricula\Controller\AcadgeralDisciplina'             => 'Matricula\Controller\AcadgeralDisciplinaController',
            'Matricula\Controller\AcadgeralDisciplinaCurso'        => 'Matricula\Controller\AcadgeralDisciplinaCursoController',
            'Matricula\Controller\AcadCurso'                       => 'Matricula\Controller\AcadCursoController',
            'Matricula\Controller\AcadgeralAreaConhecimento'       => 'Matricula\Controller\AcadgeralAreaConhecimentoController',
            'Matricula\Controller\AcadgeralCadastroOrigem'         => 'Matricula\Controller\AcadgeralCadastroOrigemController',
            'Matricula\Controller\CampusCurso'                     => 'Matricula\Controller\CampusCursoController',
            'Matricula\Controller\AcadperiodoLetivo'               => 'Matricula\Controller\AcadperiodoLetivoController',
            'Matricula\Controller\EtapaFinal'                      => 'Matricula\Controller\EtapaFinalController',
            'Matricula\Controller\GradeHoraria'                    => 'Matricula\Controller\GradeHorariaController',
            'Matricula\Controller\AcadgeralMotivoAlteracao'        => 'Matricula\Controller\AcadgeralMotivoAlteracaoController',
            'Matricula\Controller\AcadNivel'                       => 'Matricula\Controller\AcadNivelController',
            'Matricula\Controller\AcadModalidade'                  => 'Matricula\Controller\AcadModalidadeController',
            'Matricula\Controller\Index'                           => 'Matricula\Controller\IndexController',
        ),
    ),
    'view_manager'              => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console'                   => array(
        'router' => array(
            'routes' => array(),
        ),
    ),
    'funcionalidades_liberadas' => array(
        'controllers' => array(
            'Matricula\Controller\Index',
        ),
        'actions'     => array(
            'teste-numero-matricula',
            'captacao',
            'busca-turmas',
            'busca-disciplinas',
            'busca-dados-responveis',
            'busca-disciplinas-aluno-periodo',
            'requerimento-matricula', // Remover.
            'contrato-servico', //Remover.
            'busca-matricula',
            'form', // Remover -> AcadgeralAlunoController
            'resumo-aluno',
            'matriz',
            'busca-turmas-que-ofertam-disciplina',
            'turmas-origem-destino-sugestao',
            'formulario-integradora',
            'busca-cadernos-por-avaliacao',
            'correcao',
            'finalizacao',
            'adiciona-alunos-migracao-que-tem-alunocurso-mas-nao-tem-alunoperido',
            'finaliza-notas',
            'edita-notas',
            'busca-dados-pessoais-residenciais-contatos-aluno',
            'boletin-aluno-periodo-externo',
            'busca-disciplinas-pendentes',
            'vincula-fotos-usuarios',
            'pendencias-reprovacoes',
            'dados-aluno',
            'muda-arquivos-direotrio',
            'busca-turmas-por-turno',
            'desligamento',
            'verificacao-desligamento',
            'busca-turmas-por-periodo-letivo',
            'informacoes-disciplinas',
            'informacoes-prova-integradora',
            'ficha-inscricao',
            'captacao-inicio',
            'liberar-alunos'
        ),
    ),
    "layoutComponents"          => array(
        "App"           => array(
            "JsPath"   => "/js/",
            "CssPath"  => "/css/",
            "required" => array()
        ),
        "SmartAdmin"    => array(
            "JsPath"   => "/smart-admin/js/",
            "CssPath"  => "/smart-admin/css/",
            "required" => array(
                array('dataTables.min', array('layout' => 'SmartAdmin2', 'mode' => 'CSS')),
                array('dataTables.colVis', array('layout' => 'SmartAdmin2', 'mode' => 'CSS')),
                array('dataTables.tableTools', array('layout' => 'SmartAdmin2', 'mode' => 'CSS')),
                array('dataTables.bootstrap', array('layout' => 'SmartAdmin2', 'mode' => 'CSS')),
                array('dataTables.responsive', array('layout' => 'SmartAdmin2', 'mode' => 'CSS')),
                array('dataTables.min', array('layout' => 'SmartAdmin2', 'mode' => 'JS')),
                array('dataTables.colVis', array('layout' => 'SmartAdmin2', 'mode' => 'JS')),
                array('dataTables.tableTools', array('layout' => 'SmartAdmin2', 'mode' => 'JS')),
                array('dataTables.bootstrap', array('layout' => 'SmartAdmin2', 'mode' => 'JS')),
                array('dataTables.responsive', array('layout' => 'SmartAdmin2', 'mode' => 'JS')),
            )
        ),
        "SmartAdmin2"   => array(
            "JsPath"   => "/smart-admin/js/",
            "CssPath"  => "/smart-admin/css/",
            "required" => array(
                array('dataTables.min', array('layout' => 'SmartAdmin2', 'mode' => 'CSS')),
                array('dataTables.colVis', array('layout' => 'SmartAdmin2', 'mode' => 'CSS')),
                array('dataTables.tableTools', array('layout' => 'SmartAdmin2', 'mode' => 'CSS')),
                array('dataTables.bootstrap', array('layout' => 'SmartAdmin2', 'mode' => 'CSS')),
                array('dataTables.responsive', array('layout' => 'SmartAdmin2', 'mode' => 'CSS')),
                array('dataTables.min', array('layout' => 'SmartAdmin2', 'mode' => 'JS')),
                array('dataTables.colVis', array('layout' => 'SmartAdmin2', 'mode' => 'JS')),
                array('dataTables.tableTools', array('layout' => 'SmartAdmin2', 'mode' => 'JS')),
                array('dataTables.bootstrap', array('layout' => 'SmartAdmin2', 'mode' => 'JS')),
                array('dataTables.responsive', array('layout' => 'SmartAdmin2', 'mode' => 'JS')),
            )
        ),
        "MaterialAdmin" => array(
            "JsPath"   => "/material-admin/assets/js/",
            "CssPath"  => "/material-admin/assets/css/theme-default/",
            "required" => array(
                array('jquery.2.0.2', array('mode' => 'JS', 'JsLocation' => 'header')),
                array('jquery-migrate-1.2.1', array('mode' => 'JS', 'JsLocation' => 'header')),
                array('jquery-ui', array('JsCheck' => '.min')),
                array('autosize', array('JsCheck' => '.min', 'mode' => 'JS')),
                array('bootstrap', array('JsCheck' => '.min')),
                array('font-awesome', array('CssCheck' => '.min', 'mode' => 'CSS')),
                array('iconic-font', array('CssCheck' => '.min', 'mode' => 'CSS')),
                array('materialadmin', array('mode' => 'CSS')),
                array('dataTables'),
                array('dataTables.colVis'),
                array('dataTables.tableTools'),
                array('dataTables.bootstrap'),
                array('dataTables.responsive'),
                array('select2', array('layout' => 'MaterialAdmin', 'mode' => 'CSS')),
                array('dataTables.responsive'),
                //array('select2', array('JsCheck' => '.min', 'CssCheck' => '.min')),
                //array('versa.skeleton', array('layout' => 'VersaSkeleton', 'mode' => 'JS')),
                //array('bootstrap-tagsinput', array('JsCheck' => '.min', 'mode' => 'JS')),
                //array('multi-select', array('mode' => 'JS')),
                //array('bootstrap-rating', array('mode' => 'JS')),
                //array('inputmask', array('JsCheck' => '.bundle', 'mode' => 'JS')),
                //array('bootstrap-datepicker', array('mode' => 'JS')),
                //array('moment', array('mode' => 'JS')),
                //array('bootstrap-colorpicker', array('JsCheck' => '.min', 'mode' => 'JS')),
                //array('typeahead', array('JsCheck' => '.bundle', 'mode' => 'JS')),
                //array('dropzone', array('JsCheck' => '.min', 'mode' => 'JS')),
                //array('nanoscroller', array('JsCheck' => '.min', 'mode' => 'JS')),
                //array('jquery.validate', array('JsCheck' => '.min', 'mode' => 'JS')),
                //array('dataTables', array('JsCheck' => '.colVis|.tableTools', 'CssCheck' => '.colVis|.tableTools')),
                array('App', array('JsCheck' => '.min')),
            )
        ),
    ),
    'namesDictionary'           => array(
        'Matricula'                                  => 'Matrícula',
        'Matricula\AcadCurso'                        => 'Curso',
        'Matricula\AcadgeralDocente'                 => 'Docente',
        'Matricula\AcadgeralAluno'                   => 'Aluno',
        'Matricula\AcadgeralAlunoCaptacao'           => 'Captação de Alunos',
        'Matricula\AcadgeralAluno::captacao'         => 'Formulário de inscrição',
        'Matricula\AcadgeralAluno::captacao-inicio'  => 'Formulário de inscrição',
        'Matricula\AcadgeralAlunoCaptacao::captacao' => 'Formulário',
        'Matricula\AcadgeralAlunoCurso'              => 'Curso do aluno',
        'Matricula\AcadperiodoTurma'                 => 'Turma',
        'Matricula\AcadperiodoMatrizDisciplina'      => 'Matriz de disciplina',
        'Matricula\AcadgeralResponsavel'             => 'Responsável',
        'Matricula\AcadperiodoAluno'                 => 'Aluno',
        'Matricula\AcadperiodoAlunoDisciplina'       => 'Disciplina do aluno',
        'Matricula\Relatorios'                       => 'Relatórios',
        'Matricula\AcadperiodoIntegradoraCaderno'    => 'Caderno de avaliação integradora',
        'Matricula\AcadperiodoIntegradoraAvaliacao'  => 'Avaliação integradora',
        'Matricula\Notas'                            => 'Notas',
        'Matricula\AcadperiodoDocenteDisciplina'     => 'Docências',
        'Matricula\AcadgeralAreaConhecimento'        => 'Área de conhecimento',
        'Matricula\AcadgeralCadastroOrigem'          => 'Origem de cadastro',
        'Matricula\AcadgeralMotivoAlteracao'         => 'Motivo Alteração',
    )
);