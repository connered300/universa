<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Matricula;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getViewHelperConfig()
    {
        return array(
            'invokables' => array(),
            'factories'  => array(
                'sisConfig' => function ($sm, $param = false) {
                    $config    = $sm->getServiceLocator()->get("config");
                    $sisConfig = new \Sistema\ViewHelper\SisConfig(
                        $sm->getServiceLocator()->get('Doctrine\ORM\EntityManager'),
                        $config
                    );

                    return $sisConfig;
                }
            )
        );
    }

    public function getServiceConfig(){
        return array(
            'factories' => array(
                'Matricula\Service\AcadperiodoAluno' => function( $em ){
                    return new Service\AcadperiodoAluno( $em->get('Doctrine\ORM\EntityManager'));
                },
                'Matricula\Service\AcadgeralAlunoCurso' => function( $em ){
                    return new Service\AcadgeralAlunoCurso( $em->get('Doctrine\ORM\EntityManager'));
                },
                'Matricula\Service\AcadperiodoAlunoDisciplina' => function( $em ){
                    return new Service\AcadperiodoAlunoDisciplina( $em->get('Doctrine\ORM\EntityManager'));
                },
                'Matricula\Service\Aluno' => function( $em ){
                    return new Service\Aluno( $em->get('Doctrine\ORM\EntityManager'));
                },
                'Matricula\Service\AcadperiodoDocenteDisciplina' => function( $em ){
                    return new Service\AcadperiodoDocenteDisciplina( $em->get('Doctrine\ORM\EntityManager'));
                },
                'Matricula\Service\AcadperiodoLetivo' => function( $em ){
                    return new Service\AcadperiodoLetivo( $em->get('Doctrine\ORM\EntityManager'));
                },
            ),

        );
    }
}
