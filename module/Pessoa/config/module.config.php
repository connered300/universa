<?php

namespace Pessoa;

return array(
    'router'                    => array(
        'routes' => array(
            'pessoa' => array(
                'type'          => 'Literal',
                'options'       => array(
                    'route'    => '/pessoa',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Pessoa\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes'  => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'       => '/[:controller[/:action[/:id]]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id'         => '[0-9]+',
                            ),
                            'defaults'    => array(),
                        ),
                    ),
                    'paginator-pessoa-duplicada' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/pessoa-fisica/pessoas-duplicadas[/page/:page]',
                            'constraints' => array(
                                'page' => '\d+'
                            ),
                            'defaults'    => array(
                                'controller'    => 'Pessoa\Controller\PessoaFisica',
                                'action'        => 'pessoas-duplicadas',
                            ),
                        )
                    ),
                ),
            ),
        ),
    ),
    'controllers'               => array(
        'invokables' => array(
            'Pessoa\Controller\Pessoa'         => 'Pessoa\Controller\PessoaController',
            'Pessoa\Controller\PessoaFisica'   => 'Pessoa\Controller\PessoaFisicaController',
            'Pessoa\Controller\PessoaJuridica' => 'Pessoa\Controller\PessoaJuridicaController',
            'Pessoa\Controller\Endereco'       => 'Pessoa\Controller\EnderecoController',
            'Pessoa\Controller\Contato'        => 'Pessoa\Controller\ContatoController',
            'Pessoa\Controller\TipoContato'    => 'Pessoa\Controller\TipoContatoController',
            'Pessoa\Controller\Cidade'         => 'Pessoa\Controller\CidadeController',
        ),
    ),
    'view_helpers' => array(
        'invokables' => array(
            'formHelper' => 'Pessoa\ViewHelper\FormHelper',
        ),
    ),
    'doctrine'                  => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default'             => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            ),
        ),
    ),
    'view_manager'              => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'funcionalidades_liberadas' => array(
        'controllers' => array(),
        'actions'     => array(
            'valida-cpf',
            'valida-cnpj',
            'busca-info-cep',
            'busca-dados-pessoais',
            'ficha'
        )
    ),
    'namesDictionary'           => array(
        'Pessoa'                => 'Pessoa',
        'Pessoa\PessoaFisica'   => 'Pessoa física',
        'Pessoa\PessoaJuridica' => 'Pessoa jurídica',
    )
);
