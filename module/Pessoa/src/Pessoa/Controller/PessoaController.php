<?php

namespace Pessoa\Controller;

use Respect\Validation\Validator as v;
use VersaSpine\Controller\AbstractCoreController;
use Zend\View\Model\JsonModel;

/**
 * Class PessoaController
 * @package Pessoa\Controller
 */
class PessoaController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    /**
     * @return JsonModel
     */
    public function searchForJsonAction()
    {
        $request = $this->getRequest();
        $paramsGet = $request->getQuery()->toArray();
        $paramsPost = $request->getPost()->toArray();
        $param = array_merge($paramsGet, $paramsPost);

        $servicePessoa           = new \Pessoa\Service\Pessoa($this->getEntityManager());
        $serviceSelecaoInscricao = new \Vestibular\Service\SelecaoInscricao($this->getEntityManager());

        if ($param['pesquisaPFPJ']) {
            $arrDados = $servicePessoa->pesquisaForJsonPFPJ($param);
        } elseif ($param['pesquisaCompleta']) {
            $serviceAcadgeralAluno = new \Matricula\Service\AcadgeralAluno($this->getEntityManager());

            $arrDados = $serviceAcadgeralAluno->buscaAlunoPorCpf($param['pesCpf'], true);

            if (is_string($arrDados)) {
                return new \Zend\View\Model\JsonModel(array(
                    'error' => true,
                    'message' => $arrDados
                ));
            }

            if ($param['validarAprovacaoVestibular']) {
                $arrDados['aprovacaoProcessoSeletivo'] = $serviceSelecaoInscricao->verificaSeAprovado($arrDados['pesId']);
            }

            $arrDados['processoSeletivo'] = $servicePessoa->getInfoVestibular($arrDados['pesId']);

            if ($arrDados['processoSeletivo']) {
                $serviceCampusCurso = new \Matricula\Service\CampusCurso($this->getEntityManager());

                $serviceCampusCurso->getArrayCampusCursoDoProcessoSeletivo(
                    $arrDados['processoSeletivo']
                );
            }
        } else {
            $arrDados = $servicePessoa->pesquisaForJson($param);
        }

        if ($param['pesId'] && $param['dadosCompletos'] && $arrDados) {
            $arrDados = $arrDados[0];
            $arrDados = $servicePessoa->getArray($arrDados['pesId']);
        }

        return new \Zend\View\Model\JsonModel($arrDados ? $arrDados : array());
    }

    /**
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction()
    {
        return $this->getView();
    }

    /**
     * @return JsonModel
     */
    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $servicePessoa = new \Pessoa\Service\Pessoa($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result = $servicePessoa->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    /**
     * @return \Zend\Http\Response|JsonModel|\Zend\View\Model\ViewModel
     */
    public function editAction()
    {
        $pesNome = $this->params()->fromRoute("id", 0);

        if (!$pesNome) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($pesNome);
    }

    /**
     * @param bool|false $pesId
     * @return \Zend\Http\Response|\Zend\View\Model\ViewModel
     */
    public function addAction($pesId = false)
    {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $ajax = false;

        $arrDados     = array();
        $arrDadosPost = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getFiles()->toArray()
        );
        $servicePessoa = new \Pessoa\Service\Pessoa($em);

        if (isset($arrDadosPost['ajax']) && $arrDadosPost['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        if ($pesId) {
            $arrDados = $servicePessoa->getArray($pesId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        $arrDados = array_merge($arrDados,$arrDadosPost);
        $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);

        if ($request->isPost()) {
            $salvar = $servicePessoa->salvarPessoaComValidacao($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de pessoa salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($servicePessoa->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $servicePessoa->getLastError());
                }
            }
        }

        $servicePessoa->formataDadosPost($arrDados);

        $servicePessoa->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    /**
     * @return JsonModel
     */
    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $servicePessoa = new \Pessoa\Service\Pessoa($this->getEntityManager());

            $erro = false;
            $erroDescricao = '';
            $dataPost = $request->getPost()->toArray();
            $ok = $servicePessoa->remover($dataPost);

            if (empty($ok)) {
                $erro = true;
                $erroDescricao = $servicePessoa->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }

    /**
     * @return JsonModel
     */
    public function buscaPessoasJsonAction()
    {
        $json = new JsonModel();
        $request = $this->getRequest();
        $array = $this->getServiceLocator()->get("Doctrine\ORM\EntityManager")->getRepository(
            "Pessoa\Entity\Pessoa"
        )->buscaPessoasNome($request->getPost("string"));
        $json->setVariable("array", $array);

        return $json;
    }

    /**
     * @return JsonModel
     */
    public function testeAction()
    {
        $json = new JsonModel();
        $request = $this->getRequest();
        $array = $this->getServiceLocator()->get("Doctrine\ORM\EntityManager")->getRepository(
            "Pessoa\Entity\Pessoa"
        )->buscaPessoasNome($request->getPost("string"));
        $json->setVariable("array", $array);

        return $json;
    }

    /**
     * @return JsonModel
     */
    public function buscaDadosPessoaisAction()
    {
        $json = new JsonModel();

        $service      = $this->getServiceLocator();
        $em           = $service->get('Doctrine\ORM\EntityManager');
        $serviceAluno = new \Matricula\Service\AcadgeralAluno($this->getEntityManager());

        $data = $this->getRequest()->getPost()->toArray();

        if (v::cnpj()->validate($data['cnpjCpf'])) {
            $repository = $em->getRepository("Pessoa\Entity\PessoaJuridica");
            $pessoaJuridica = $repository->findOneBy(array('pesCnpj' => $data['cnpjCpf']));
            foreach ($data['atributos'] as $key => $atributo) {
                $get = "get" . $atributo['atributo'];
                if ($atributo['classe'] != "") {
                    $classe = "get" . $atributo['classe'];
                    $json->setVariable($key, $pessoaJuridica->$classe()->$get());
                } else {
                    $json->setVariable($key, $pessoaJuridica->$get());
                }
            }
        }
        if (v::cpf()->validate($data['cnpjCpf'])) {
            $repository = $em->getRepository("Pessoa\Entity\PessoaFisica");
            $pessoaFisica = $repository->findOneBy(array('pesCpf' => $data['cnpjCpf']));
            if ($pessoaFisica) {
                foreach ($data['atributos'] as $key => $atributo) {
                    if ($key == 'dadosAluno') {
                        continue;
                    }
                    $get = "get" . $atributo['atributo'];
                    if ($atributo['classe'] != "") {
                        $classe = "get" . $atributo['classe'];
                        $json->setVariable($key, $pessoaFisica->$classe()->$get());
                    } else {
                        $json->setVariable($key, $pessoaFisica->$get());
                    }
                }
                if ($data['endereco'] == true) {
                    $repositoryEndereco = $em->getRepository("Pessoa\Entity\Endereco");
                    $endereco = $repositoryEndereco->findOneBy(
                        array('pes' => $pessoaFisica->getPes()->getPesId())
                    );
                    if ($endereco) {
                        $json->setVariable('cep', $endereco->getEndCep());
                        $json->setVariable('logradouro', $endereco->getEndLogradouro());
                        $json->setVariable('logradouro', $endereco->getEndLogradouro());
                        $json->setVariable('numero', $endereco->getEndNumero());
                        $json->setVariable('cidade', $endereco->getEndCidade());
                        $json->setVariable('estado', $endereco->getEndEstado());
                        $json->setVariable('complemento', $endereco->getEndComplemento());
                        $json->setVariable('bairro', $endereco->getEndBairro());
                    }
                }
                if ($data['contato'] == true) {
                    $repositoryContato = $em->getRepository("Pessoa\Entity\Contato");
                    $email = $repositoryContato->findOneBy(
                        array('pessoa' => $pessoaFisica->getPes()->getPesId(), 'tipoContato' => '1')
                    );
                    if ($email) {
                        $json->setVariable('email', $email->getConContato());
                    }

                    $telefone = $repositoryContato->findOneBy(
                        array('pessoa' => $pessoaFisica->getPes()->getPesId(), 'tipoContato' => '2')
                    );
                    if ($telefone) {
                        $json->setVariable('telefone', $telefone->getConContato());
                    }
                    $telefone = $repositoryContato->findOneBy(
                        array('pessoa' => $pessoaFisica->getPes()->getPesId(), 'tipoContato' => '3')
                    );
                    if ($telefone) {
                        $json->setVariable('celular', $telefone->getConContato());
                    }
                }
            }

            if ($data['atributos']['dadosAluno'] && $pessoaFisica) {
                $json->setVariable('pesRgEmissao', $pessoaFisica->getPesRgEmissao());

                /** @var $objAluno \Matricula\Entity\AcadgeralAluno */
                $objAluno = $serviceAluno->getRepository()->findOneBy(['pes' => $pessoaFisica->getPes()->getPesId()]);

                if ($objAluno) {
                    $json->setVariable('alunoDados', $objAluno->toArray());
                }
            }
        }

        return $json;
    }

    public function alterarObservacoesAction()
    {
        $request = $this->getRequest();
        $paramsGet = $request->getQuery()->toArray();
        $paramsPost = $request->getPost()->toArray();
        $arrDados = array_merge($paramsGet, $paramsPost);

        $erro = true;
        $mensagem = 'Falha ao alterar observação: Parâmetros incorretos!';

        if ($arrDados['pesId']) {
            $servicePessoa = new \Pessoa\Service\Pessoa($this->getEntityManager());

            $resultadoOperacao = $servicePessoa->alterarObservacoes($arrDados);

            if (!$resultadoOperacao) {
                $erro = true;
                $mensagem = $servicePessoa->getLastError();
            } else {
                $erro = false;
                $mensagem = 'Observação alterada com sucesso!';
            }
        }

        $this->getJson()->setVariable('erro', $erro);
        $this->getJson()->setVariable('mensagem', $mensagem);

        return $this->getJson();
    }
}
