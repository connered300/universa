<?php

namespace Pessoa\Controller;

use VersaSpine\Controller\AbstractCoreController;
use Zend\View\Model\JsonModel;

/**
 * Description of PessoaJuridica
 *
 * @author Matheus
 */
class PessoaJuridicaController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    // metodo responsável por verificar se cnpj ja esta está registrado
    public function validaCnpjAction($cnpj)
    {
        $request = $this->getRequest()->getPost()->toArray();

        return $this->getJson()->setVariable(
            'cnpj_usado',
            $this->services()->getService($this->getService())->pesquisaPessoaPorCnpj($request['cnpj'])
        );
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {
        $request    = $this->getRequest();
        $paramsGet  = $request->getQuery()->toArray();
        $paramsPost = $request->getPost()->toArray();
        $param      = array_merge($paramsGet, $paramsPost);

        $servicePessoaJuridica = new \Pessoa\Service\PessoaJuridica($this->getEntityManager());

        $arrDados = $servicePessoaJuridica->pesquisaForJson($param);

        if ($param['pesId'] && $param['dadosCompletos'] && $arrDados) {
            $arrDados = $arrDados[0];
            $arrDados = $servicePessoaJuridica->getArray($arrDados['pesId']);
        }

        return new \Zend\View\Model\JsonModel($arrDados ? $arrDados : array());
    }

    public function editAction()
    {
        $identificador = $this->params()->fromRoute("id", 0);

        if (!$identificador) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($identificador);
    }

    public function addAction($pesId = false)
    {
        $request  = $this->getRequest();
        $arrDados = array();

        $servicePessoaJuridica = new \Pessoa\Service\PessoaJuridica($this->getEntityManager());
        $servicePessoa         = new \Pessoa\Service\Pessoa($this->getEntityManager());

        if ($pesId) {
            $arrDados = $servicePessoaJuridica->getArray($pesId);

            if (empty($arrDados)) {
                $this->flashMessenger()->addErrorMessage($servicePessoaJuridica->getLastError());

                return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge($arrDados, $request->getPost()->toArray());

            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $servicePessoaJuridica->salvarPessoaJuridicaComValidacao($arrDados);
            $ajax     = false;

            if (isset($arrDados['ajax']) && $arrDados['ajax']) {
                $this->setView(new JsonModel());
                $ajax = true;
            }

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de pessoa jurídica salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($servicePessoaJuridica->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $servicePessoaJuridica->getLastError());
                }
            }
        }

        $servicePessoaJuridica->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $servicePessoaJuridica = new \Pessoa\Service\PessoaJuridica($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $servicePessoaJuridica->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);

            return $this->getJson();
        }

        return $this->getJson();
    }

    public function indexAction()
    {
        $servicePessoaJuridica = new \Pessoa\Service\PessoaJuridica($this->getEntityManager());
        $servicePessoaJuridica->setarDependenciasView($this->getView());

        return $this->getView();
    }
}
