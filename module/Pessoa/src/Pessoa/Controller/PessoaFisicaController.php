<?php

namespace Pessoa\Controller;

use VersaSpine\Controller\AbstractCoreController;
use Zend\View\Model\JsonModel;

class PessoaFisicaController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    // metodo responsável por verificar se cpf ja esta está registrado
    public function validaCpfAction($cpf)
    {
        $request = $this->getRequest()->getPost()->toArray();

        return $this->getJson()->setVariable(
            'cpf_usado',
            $this->services()->getService($this->getService())->pesquisaPessoaPorCpf($request['cpf'])
        );
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {
        $request    = $this->getRequest();
        $paramsGet  = $request->getQuery()->toArray();
        $paramsPost = $request->getPost()->toArray();
        $param      = array_merge($paramsGet, $paramsPost);

        $servicePessoaFisica = new \Pessoa\Service\PessoaFisica($this->getEntityManager());

        $arrDados = $servicePessoaFisica->pesquisaForJson($param);

        if ($param['pesId'] && $param['dadosCompletos'] && $arrDados) {
            $arrDados = $arrDados[0];
            $arrDados = $servicePessoaFisica->getArray($arrDados['pesId']);
        }

        return new \Zend\View\Model\JsonModel($arrDados ? $arrDados : array());
    }

    public function editAction()
    {
        $identificador = $this->params()->fromRoute("id", 0);

        if (!$identificador) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($identificador);
    }

    public function addAction($pesId = false)
    {
        $request  = $this->getRequest();
        $arrDados = array();

        $servicePessoaFisica = new \Pessoa\Service\PessoaFisica($this->getEntityManager());

        if ($pesId) {
            $arrDados = $servicePessoaFisica->getArray($pesId);

            if (empty($arrDados)) {
                $this->flashMessenger()->addErrorMessage($servicePessoaFisica->getLastError());

                return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge($arrDados, $request->getPost()->toArray());

            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $servicePessoaFisica->salvarPessoaFisicaComValidacao($arrDados);
            $ajax     = false;

            if (isset($arrDados['ajax']) && $arrDados['ajax']) {
                $this->setView(new JsonModel());
                $ajax = true;
            }

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de pessoa física salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($servicePessoaFisica->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $servicePessoaFisica->getLastError());
                }
            }
        }

        $servicePessoaFisica->setarDependenciasView($this->getView());
        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $servicePessoaFisica = new \Pessoa\Service\PessoaFisica($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $servicePessoaFisica->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);

            return $this->getJson();
        }

        return $this->getJson();
    }

    public function indexAction()
    {
        $arrConfig            = $this->getServiceManager()->get('Config');
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEntityManager(), $arrConfig);

        $permissaoMesclar = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Pessoa\Controller\PessoaFisica',
            'pessoas-duplicadas'
        );

        $this->getView()->setVariable('permissaoMesclar', $permissaoMesclar);

        return $this->getView();
    }

    public function pessoasDuplicadasAction()
    {
        /** @var \Zend\Http\Request $request */
        $request = $this->getRequest();

        $srvPessoaFisica = new \Pessoa\Service\PessoaFisica($this->getEntityManager());

        $page = $this->params()->fromRoute('page', 1);

        if ($request->isPost()) {
            try {
                $result = $srvPessoaFisica->mergePessoas($request->getPost('pessoas'));

                $pessoas = "";

                /** @var \Pessoa\Entity\PessoaFisica $cadaResult */
                foreach ($result as $cadaResult) {
                    $pessoas .= "{$cadaResult->getPes()->getPesNome()},";
                }

                $mensagem = sprintf('Pessoa(s) %s mesclada(s) com sucesso!', $pessoas);

                $this->flashMessenger()->addSuccessMessage($mensagem);
                $this->getJson()->setVariable("success", $mensagem);
            } catch (\Exception $ex) {
                $mensagem = $ex->getMessage();

                $this->flashMessenger()->addErrorMessage($mensagem);
                $this->getJson()->setVariable("error", $mensagem);
            }

            if ($request->isXmlHttpRequest()) {
                $this->flashMessenger()->clearCurrentMessages();

                return $this->getJson()->setVariable("page", $page);
            }

            return $this->redirect()->toRoute("pessoa/paginator-pessoa-duplicada", ["page" => $page]);
        }

        //TODO: expandir buscas para nome + nome da mae
        //TODO: expandir buscas para nome + contato (telefone, telefone celular, email)
        //TODO: expandir buscas para nome + data nascimento
        //TODO: expandir buscas para nome + endereco
        $cpfDuplicados = $srvPessoaFisica->buscaCpfDuplicados();

        $paginatorPessoasDuplicadas = new \Zend\Paginator\Paginator(
            new \Zend\Paginator\Adapter\ArrayAdapter($cpfDuplicados ? $cpfDuplicados : [])
        );
        $paginatorPessoasDuplicadas
            ->setCurrentPageNumber($page)
            ->setDefaultItemCountPerPage(10);

        //ainda precisa ajustar a interface no javascript e anexar buscas e etc via interface

        return $this->getView()->setVariables(
            [
                "pessoasDuplicadas" => $paginatorPessoasDuplicadas,
                "page"              => $page,
            ]
        );
    }

    public function removerDuplicadosAction()
    {
        $srvPessoaFisica = new \Pessoa\Service\PessoaFisica($this->getEntityManager());
        $cpfDuplicados   = $srvPessoaFisica->removeDuplicados();

        echo "<pre>";
        print_r($cpfDuplicados);
        die();

        return false;
    }
}