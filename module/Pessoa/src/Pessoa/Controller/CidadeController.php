<?php

namespace Pessoa\Controller;

use VersaSpine\Controller\AbstractCoreController;
use Zend\View\Model\JsonModel;

/**
 * Description of CidadeController
 *
 * @author JoaoPaulo
 */
class CidadeController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function buscaInfoCepAction()
    {
        $json    = new JsonModel();
        $service = new \Pessoa\Service\Cidade($this->services()->getEm());
        $request = $this->getRequest();
        $cep     = $request->getPost("cep")?$request->getPost("cep"):null;
        $cidade  = $request->getPost("cidade")?$request->getPost("cidade"):null;
        $estado  = $request->getPost("estado")?$request->getPost("estado"):null;
        $dados   = array();

        if ($cep) {
            $dados = $service->buscaDadosCep($cep);
        } elseif ($cidade) {
            $dados = $service->buscaDadosCidade($cidade, $estado)->fetchAll();
        } elseif ($estado !== null) {
            $dados = $service->buscaDadosEstado($estado)->fetchAll();
        }

        $json->setVariable("dados", $dados);

        return $json;
    }

    public function searchForJsonAction()
    {
    }

}
