<?php

namespace Pessoa\Form;

use Zend\Form\Form,
    Zend\Form\Element;

/**
 * Description of Pessoa
 *
 * @author Matheus Ferreira
 */
class Pessoa extends Form {
    public function __construct($name = null)
    {
        parent::__construct($name);
        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');

        $pessoa = new Element\Hidden("pessoa");
        $this->add($pessoa);

        $pesNome = new Element\Text("pesNome");
        $pesNome->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Informe o Nome ou a Razão Social ',
            'label' => 'Nome/Razão Social:',
            'icon' => 'icon-prepend fa fa-user',
            'wrap' => true,
            'maxlength' => 255,
            'data-validations' => 'Required,String',
        ));
        $this->add($pesNome);

        $pesNacionalidade = new Element\Select("pesNacionalidade");
        $pesNacionalidade->setAttributes(array(
            'col' => 4,
            'label' => 'Nacionalidade',
            'wrap' => true,
            'data-validations' => 'Required',
        ));
        $pesNacionalidade->setValueOptions([
            'Brasileiro' => 'Brasileiro',
            'Estrangeiro' => 'Estrangeiro',
            'Naturalizado' => 'Naturalizado',
        ]);
        $pesNacionalidade->setValue('Brasileiro');
        $this->add($pesNacionalidade);
    }

}
