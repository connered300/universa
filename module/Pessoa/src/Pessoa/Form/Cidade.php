<?php

namespace Pessoa\Form;

use Zend\Form\Form,
    Zend\Form\Element;

class Cidade extends Form {
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name);
        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');

        $cidId = new Element\Text('cidId');
        $this->add($cidId);

        $cidNome = new Element\Text('cidNome');
        $cidNome->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Informe o nome da Cidade',
            'label' => 'Cidade:',
            'data-validations' => 'Required',
        ));
        $this->add($cidNome);

        $cidCep = new Element\Text('cidCep');
        $cidCep->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Informe o CEP',
            'label' => 'CEP:',
            'data-validations' => 'Required,Cep',
        ));
        $this->add($cidCep);

        $estado = new Element\Select('estado');
        $estado->setValueOptions($options);
        $estado->setAttributes(array(
            'col' => 4,
            'label' => 'Estado:',
            'data-validations' => 'Required'
        ));
        $this->add($estado);

        $enviar = new Element\Submit('enviar');
        $enviar->setValue('Concluir');
        $enviar->setAttribute('class', 'pull-left btn btn-primary');
        $this->add($enviar);
    }

}
