<?php

namespace Pessoa\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class Endereco extends Form {
    public function __construct(array $options_estados = array())
    {
        parent::__construct("Endereco");
        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');

        $endId = new Element\Hidden('endId[]');
        $this->add($endId);

        $pes = new Element\Hidden('pes');
        $this->add($pes);

        $tipoEndereco = new Element\Hidden('tipoEndereco');
        $this->add($tipoEndereco);

        $endPais = new Element\Text('endPais[]');
        $endPais->setAttributes(array(
            'placeholder' => 'Informe o País',
            'label' => 'País',
//            'data-validations' => 'Required',
        ));
        $this->add($endPais);

        $endEstado = new Element\Select('endEstado[]');
        $endEstado->setAttributes(array(
            'col' => 4,
            'label' => 'Estado',
            'data-validations' => 'Required',
        ));
        $endEstado->setValueOptions($options_estados);
        $this->add($endEstado);

        $endCidade = new Element\Text('endCidade[]');
        $endCidade->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Informe a Cidade',
            'label' => 'Cidade',
            'data-validations' => 'Required',
        ));
        $this->add($endCidade);

        $endBairro = new Element\Text('endBairro[]');
        $endBairro->setAttributes(array(
            'placeholder' => 'Informe o Bairro',
            'label' => 'Bairro',
//            'data-validations' => 'Required',
        ));
        $this->add($endBairro);

        $endLogradouro = new Element\Text('endLogradouro[]');
        $endLogradouro->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Informe o Logradouro',
            'label' => 'Logradouro',
//            'data-validations' => 'Required',
        ));
        $this->add($endLogradouro);

        $endTipoLogradouro = new Element\Text('endTipoLogradouro[]');
        $endTipoLogradouro->setAttributes(array(
            'col' => 4,
            'label' => 'Tipo:',
//            'data-validations' => 'Required',
        ));
        $this->add($endTipoLogradouro);

        $endCep = new Element\Text('endCep[]');
        $endCep->setAttributes(array(
            'col' => 2,
            'placeholder' => 'Informe o CEP',
            'label' => 'CEP:',
        ));
        $this->add($endCep);

        $endNumero = new Element\Text('endNumero[]');
        $endNumero->setAttributes(array(
            'label' => 'Número',
            'placeholder' => 'N°',
        ));
        $this->add($endNumero);

        $endComplemento = new Element\Text('endComplemento[]');
        $endComplemento->setAttributes(array(
            'label' => 'Complemento',
            'placeholder' => 'Complemento',
        ));
        $this->add($endComplemento);
        
        $tipoEndereco = new Element\Select('tipoEndereco[]');
        $tipoEndereco->setAttributes(array(
            'label' => 'Tipo de endereço',
            'data-validations' => 'Required',
        ));
        $this->add($tipoEndereco);
    }

}
