<?php

namespace Pessoa\Form;

use Zend\Form\Form,
    Zend\Form\Element;

class Estado extends Form {
    public function __construct($name = null, $uf_options = array(), $paises_options = array())
    {
        parent::__construct($name);
        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');

        $estId = new Element\Hidden('estId');
        $this->add($estId);

        $estNome = new Element\Text('estNome');
        $estNome->setAttributes(array(
            'col' => 4,
            'label' => 'Estado:',
            'placeholder' => 'Informe o nome do Estado',
            'wrap' => true,
            'data-validations' => 'Required,String',
        ));
        $this->add($estNome);

        $estUf = new Element\Select('estUf');
        $estUf->setValueOptions($uf_options);
        $estUf->setAttributes(array(
            'col' => 4,
            'label' => 'UF:',
            'data-validations' => 'Required',
        ));
        $this->add($estUf);

        $pais = new Element\Select('pais');
        $pais->setValueOptions($paises_options);
        $pais->setAttributes(array(
            'col' => 4,
            'label' => 'País:',
            'data-validations' => 'Required',
        ));
        $this->add($pais);

        $enviar = new Element\Submit('enviar');
        $enviar->setValue('Concluir');
        $enviar->setAttribute('class', 'pull-left btn btn-primary');
        $this->add($enviar);
    }

}
