<?php

namespace Pessoa\Form;

use Zend\Form\Form,
    Zend\Form\Element;

class TipoContato extends Form {
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name);
        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');

        $tipContatoId = new Element\Hidden('tipContatoId');
        $this->add($tipContatoId);

        $tipContatoNome = new Element\Text('tipContatoNome');
        $tipContatoNome->setAttributes(array(
            'col' => 4,
            'label' => 'Nome do tipo:',
            'placeholder' => 'Informe o nome do tipo de contato',
            'data-validations' => 'Required',
        ));
        $this->add($tipContatoNome);

        $tipContatoDescricao = new Element\Text('tipContatoDescricao');
        $tipContatoDescricao->setAttributes(array(
            'col' => 4,
            'label' => 'Descrição:',
            'placeholder' => 'Descrição do contato',
            'data-validations' => 'Required',
        ));
        $this->add($tipContatoDescricao);

        $enviar = new Element\Submit('enviar');
        $enviar->setValue('Concluir');
        $enviar->setAttribute('class', 'pull-left btn btn-primary');
        $this->add($enviar);
    }

}
