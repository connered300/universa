<?php

namespace Pessoa\Form;

use Zend\Form\Form,
    Zend\Form\Element;

class Contato extends Form {
    public function __construct($name = null)
    {
        parent::__construct($name);

        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');

        $conId = new Element\Hidden('conId');
        $this->add($conId);

        $conContato = new Element\Text('conContato');
        $conContato->setAttributes(array(
            'col' => 4,
            'label' => 'Contato:',
            'placeholder' => 'Descrição do Contato',
            'data-validations' => 'Required',
        ));
        $this->add($conContato);

        $pessoa = new Element\Hidden('pessoa');
        $this->add($pessoa);

        $tipoContato = new Element\Hidden('tipoContato');
        $this->add($tipoContato);

        $enviar = new Element\Submit('enviar');
        $enviar->setValue('Concluir');
        $enviar->setAttribute('class', 'pull-left btn btn-primary');
        $this->add($enviar);
    }

}
