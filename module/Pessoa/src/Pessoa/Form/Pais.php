<?php

namespace Pessoa\Form;

use Zend\Form\Form,
    Zend\Form\Element;

class Pais extends Form {
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name, $options);
        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');

        $paisId = new Element\Hidden('paisId');
        $this->add($paisId);

        $paisNome = new Element\Text('paisNome');
        $paisNome->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Informe o nome',
            'label' => 'Nome',
            'data-validations' => 'Required,String',
        ));
        $this->add($paisNome);

        $paisSigla = new Element\Text('paisSigla');
        $paisSigla->setAttributes(array(
            'col' => 4,
            'label' => 'Sigla',
            'placeholder' => 'Informe a sigla',
            'data-validations' => 'Required',
        ));
        $this->add($paisSigla);

        $enviar = new Element\Submit('enviar');
        $enviar->setValue('Concluir');
        $enviar->setAttribute('class', 'pull-left btn btn-primary');
        $this->add($enviar);
    }

}
