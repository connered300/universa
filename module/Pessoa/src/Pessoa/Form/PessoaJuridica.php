<?php

namespace Pessoa\Form;

use Zend\Form\Form,
    Zend\Form\Element;

class PessoaJuridica extends Form
{
    public function __construct($name = null)
    {
        parent::__construct($name);
        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');

        $pesId = new Element\Hidden('pes');
        $this->add($pesId);

        $pesNomeFantasia = new Element\Text('pesNomeFantasia');
        $pesNomeFantasia->setAttributes(
            array(
                'col'              => 4,
                'maxlength'        => 255,
                'wrap'             => true,
                'placeholder'      => 'Informe o Nome Fantasia',
                'label'            => 'Nome Fantasia',
                'data-validations' => 'Required,String',
            )
        );
        $this->add($pesNomeFantasia);

        $pesCnpj = new Element\Text('pesCnpj');
        $pesCnpj->setAttributes(
            array(
                'col'              => 4,
                'placeholder'      => 'Informe o CNPJ',
                'label'            => 'CNPJ:',
                'data-validations' => 'Required,Cnpj',
                'data-masked'      => 'Cnpj',
            )
        );
        $this->add($pesCnpj);

        $pesInscMunicipal = new Element\Text('pesInscMunicipal');
        $pesInscMunicipal->setAttributes(
            array(
                'col'              => 4,
                'placeholder'      => 'Informe a Incrição Municipal',
                'label'            => 'Inscrição Municipal',
                'data-validations' => 'Required,Integer',
            )
        );
        $this->add($pesInscMunicipal);

        $pesInscEstadual = new Element\Text('pesInscEstadual');
        $pesInscEstadual->setAttributes(
            array(
                'col'              => 4,
                'placeholder'      => 'Informe a Incrição Estadual',
                'label'            => 'Inscrição Estadual',
                'data-validations' => 'Required,Integer',
                'wrap'             => true,
            )
        );
        $this->add($pesInscEstadual);

        $pesDataInicio = new Element\Text('pesDataInicio');
        $pesDataInicio->setAttributes(
            array(
                'col'              => 4,
                'placeholder'      => 'Informe a data de abertura',
                'label'            => 'Data de Abertura',
                'icon'             => 'icon-prepend fa fa-calendar',
                'wrap'             => true,
                'data-validations' => 'Required,Data',
                'data-masked'      => 'Date',
            )
        );
        $this->add($pesDataInicio);

        $pesDataFim = new Element\Text('pesDataFim');
        $pesDataFim->setAttributes(
            array(
                'col'              => 4,
                'placeholder'      => 'Informe a data de encerramento',
                'label'            => 'Data de Finalização',
                'icon'             => 'icon-prepend fa fa-calendar',
                'wrap'             => true,
                'data-validations' => 'Data',
                'data-masked'      => 'Date',
            )
        );
        $this->add($pesDataFim);
    }

}
