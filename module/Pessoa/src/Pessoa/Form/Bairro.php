<?php

namespace Pessoa\Form;

use Zend\Form\Form,
    Zend\Form\Element;

class Bairro extends Form {
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name);

        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');

        $baiId = new Element\Hidden('baiId');
        $this->add($baiId);

        $baiNome = new Element\Text('baiNome');
        $baiNome->setAttributes(array(
            'col' => 4,
            'placeholder' => 'Informe o Bairro',
            'label' => 'Bairro:',
            'icon' => 'icon-prepend fa fa-user',
            'data-validations' => 'Required',
        ));
        $this->add($baiNome);

        $cidade = new Element\Select('cidade');
        $cidade->setValueOptions($options);
        $cidade->setAttributes(array(
            'col' => 4,
            'label' => 'Cidade:',
            'data-validations' => 'Required'
        ));
        $this->add($cidade);

        $enviar = new Element\Submit('enviar');
        $enviar->setValue('Concluir');
        $enviar->setAttribute('class', 'pull-left btn btn-primary');
        $this->add($enviar);
    }

}
