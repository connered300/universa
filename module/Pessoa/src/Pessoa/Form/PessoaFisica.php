<?php

namespace Pessoa\Form;

use Zend\Form\Form,
    Zend\Form\Element;

class PessoaFisica extends Pessoa
{
    public function __construct($necessidadesEspeciais = null, $options_estados = array())
    {
        parent::__construct("PessoaFisica");
        //        $this->setAttribute("class", 'smart-form');
        //        $this->setAttribute('data-validate', 'yes');

        $pesId = new Element\Hidden('pes');
        $this->add($pesId);

        $pesCpf = new Element\Text('pesCpf');
        $pesCpf->setAttributes(
            array(
                'col'              => 4,
                'placeholder'      => 'Informe o CPF',
                'label'            => 'CPF',
                'data-validations' => 'Required,Cpf',
                'data-masked'      => 'Cpf',
            )
        );
        $this->add($pesCpf);

        $pesCpfEmissao = new Element\Text('pesCpfEmissao');
        $pesCpfEmissao->setAttributes(
            array(
                'col'              => 4,
                'placeholder'      => 'Data de emissão',
                'label'            => 'Data de emissão CPF:',
                'data-validations' => 'Required,Data',
                'data-masked'      => 'Date',
            )
        );
        $this->add($pesCpfEmissao);

        $pesRg = new Element\Text('pesRg');
        $pesRg->setAttributes(
            array(
                'col'              => 4,
                'icon'             => 'icon-prepend fa fa-user',
                'placeholder'      => 'Informe o RG',
                'label'            => 'RG:',
                'data-validations' => 'Required',
            )
        );
        $this->add($pesRg);

        $pesRgEmissao = new Element\Text('pesRgEmissao');
        $pesRgEmissao->setAttributes(
            array(
                'col'              => 4,
                'placeholder'      => 'Data de emissão',
                'label'            => 'Data de emissão do RG',
                'data-validations' => 'Required,Data',
                'data-masked'      => 'Date',
            )
        );
        $this->add($pesRgEmissao);

        $pesSobrenome = new Element\Text('pesSobrenome');
        $pesSobrenome->setAttributes(
            array(
                'col'              => 4,
                'placeholder'      => 'Informe o seu Sobrenome',
                'label'            => 'Sobrenome',
                'data-validations' => 'Required,String'
            )
        );
        $this->add($pesSobrenome);

        $pesSexo = new Element\Radio('pesSexo');
        $pesSexo->setValueOptions(
            array(
                'Feminino'  => 'Feminino',
                'Masculino' => 'Masculino',
            )
        );
        $pesSexo->setAttributes(
            array(
                'col'              => 4,
                'label'            => 'Sexo:',
                'data-validations' => 'Required'
            )
        );
        $this->add($pesSexo);

        $pesDataNascimento = new Element\Text('pesDataNascimento');
        $pesDataNascimento->setAttributes(
            array(
                'col'              => 4,
                'label'            => 'Nascimento',
                'data-validations' => 'Required,Data',
                'placeholder'      => '00/00/0000',
                'data-masked'      => 'Date'
            )
        );
        $this->add($pesDataNascimento);

        $pesEstadoCivil = new Element\Select('pesEstadoCivil');
        $pesEstadoCivil->setValueOptions(
            array('Solteiro' => 'Solteiro(a)', 'Casado' => 'Casado(a)', 'União Estável' => 'União Estável')
        );
        $pesEstadoCivil->setAttributes(
            array(
                'col'   => 4,
                'label' => 'Selecione o Estado Civil'
            )
        );
        $this->add($pesEstadoCivil);

        $pesFilhos = new Element\Text('pesFilhos');
        $pesFilhos->setAttributes(
            array(
                'col'              => 4,
                'placeholder'      => 'Informe a quantidade filhos',
                'label'            => 'Quantidade de filhos',
                'data-validations' => 'Required,Integer'
            )
        );
        $this->add($pesFilhos);

        $pessoa = new Element\Hidden('pessoa');
        $this->add($pessoa);

        $endereco = new Element\Hidden('endereco');
        $this->add($endereco);

        $pesDocEstrangeiro = new Element\Text('pesDocEstrangeiro');
        $pesDocEstrangeiro->setAttributes(
            array(
                'col'              => 4,
                'placeholder'      => 'Informe o documento estrangeiro',
                'label'            => 'Documento Estrangeiro:',
                'data-validations' => 'Required',
                'icon'             => 'icon-prepend fa fa-credit-card',
            )
        );
        $this->add($pesDocEstrangeiro);

        $pesNaturalidade = new Element\Text('pesNaturalidade');
        $pesNaturalidade->setAttributes(
            array(
                'col'              => 4,
                'placeholder'      => 'Informe a cidade de Nascimento',
                'label'            => 'Cidade de Nascimento:',
                'data-validations' => 'Required',
            )
        );
        $this->add($pesNaturalidade);

        $pesNascUf = new Element\Select('pesNascUf');
        $pesNascUf->setAttributes(
            array(
                'col'              => 4,
                'placeholder'      => 'Informe o estado de nascimento',
                'label'            => 'Estado de Nascimento:',
                'data-validations' => 'Required',
            )
        );
        $this->add($pesNascUf);

        $pesFalecido = new Element\Select('pesFalecido');
        $pesFalecido->setAttributes(
            array(
                'col'              => 4,
                'placeholder'      => 'Pessoa falecida',
                'label'            => 'Falecido:',
                'data-validations' => 'Required'
            )
        );
        $pesFalecido->setValueOptions(array('Não' => 'Não', 'Sim' => 'Sim'));
        $this->add($pesFalecido);

        $pesEmissorRg = new Element\Text('pesEmissorRg');
        $pesEmissorRg->setAttributes(
            array(
                'col'              => 4,
                'placeholder'      => 'Informe o órgão expedidor do RG',
                'label'            => 'Oŕgão expedidor do RG:',
                'data-validations' => 'Required',
                'icon'             => 'icon-prepend fa fa-credit-card',
            )
        );
        $this->add($pesEmissorRg);

        $necessidades = new Element\MultiCheckBox('pesNecessidades');
        $necessidades->setAttributes(
            [
                'label' => 'Necessidades Especiais',
                'col'   => '4'
            ]
        );
        if ($necessidadesEspeciais) {
            $necessidades->setValueOptions($necessidadesEspeciais);
        }
        $this->add($necessidades);

        $enviar = new Element\Submit('enviar');
        $enviar->setValue('Concluir');
        $enviar->setAttribute('class', 'pull-left btn btn-primary');
        $this->add($enviar);
    }

}
