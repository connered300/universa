<?php


namespace Pessoa\Service;

use VersaSpine\Service\AbstractService;

class NecessidadesEspeciais extends AbstractService
{
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Pessoa\Entity\NecessidadesEspeciais');
    }

    public function pesquisaForJson($params)
    {
        $sql                  = 'SELECT * FROM necessidades_especiais WHERE';
        $necessidadeDescricao = false;
        $necessidadeId        = false;

        if ($params['q']) {
            $necessidadeDescricao = $params['q'];
        } elseif ($params['query']) {
            $necessidadeDescricao = $params['query'];
        }

        if ($params['necessidadeId']) {
            $necessidadeId = $params['necessidadeId'];
        }

        $parameters = array('necessidade_descricao' => "{$necessidadeDescricao}%");
        $sql .= ' necessidade_descricao LIKE :necessidade_descricao';

        if ($necessidadeId) {
            $parameters['necessidade_id'] = $necessidadeId;
            $sql .= ' AND necessidade_id <> :necessidade_id';
        }

        $sql .= " ORDER BY necessidade_descricao";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['necessidadeId']) {
                $objNecessidadesEspeciais = $this->getRepository()->find($arrDados['necessidadeId']);

                if (!$objNecessidadesEspeciais) {
                    $this->setLastError('Registro de necessidade especial não existe!');

                    return false;
                }
            } else {
                $objNecessidadesEspeciais = new \Pessoa\Entity\NecessidadesEspeciais();
            }

            $objNecessidadesEspeciais->setNecessidadeNome($arrDados['necessidadeNome']);
            $objNecessidadesEspeciais->setNecessidadeDescricao($arrDados['necessidadeDescricao']);

            $this->getEm()->persist($objNecessidadesEspeciais);
            $this->getEm()->flush($objNecessidadesEspeciais);

            $this->getEm()->commit();

            $arrDados['necessidadeId'] = $objNecessidadesEspeciais->getNecessidadeId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de necessidade especial!');
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['necessidadeNome']) {
            $errors[] = 'Por favor preencha o campo "nome"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM necessidades_especiais";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($necessidadeId)
    {
        $arrDados = $this->getRepository()->find($necessidadeId);

        try {
            $arrDados = $arrDados->toArray();
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('necessidadeId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Pessoa\Entity\NecessidadesEspeciais */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getNecessidadeId(),
                $params['value'] => $objEntity->getNecessidadeNome()
            );
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['necessidadeId']) {
            $this->setLastError(
                'Para remover um registro de necessidade especial é necessário especificar o código.'
            );

            return false;
        }

        try {
            $objNecessidadesEspeciais = $this->getRepository()->find($param['necessidadeId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objNecessidadesEspeciais);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de necessidade especial.');

            return false;
        }

        return true;
    }
}