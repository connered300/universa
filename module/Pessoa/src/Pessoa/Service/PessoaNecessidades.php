<?php
namespace Pessoa\Service;

use VersaSpine\Service\AbstractService;

class PessoaNecessidades extends AbstractService
{
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Pessoa\Entity\PessoaNecessidades');
    }

    public function pesquisaForJson($params)
    {
        $sql              = 'SELECT * FROM pessoa_necessidades NATURAL JOIN necessidades_especiais WHERE';
        $necessidadeNome  = false;
        $pesNecessidadeId = false;

        if ($params['q']) {
            $necessidadeNome = $params['q'];
        } elseif ($params['query']) {
            $necessidadeNome = $params['query'];
        }

        if ($params['pesNecessidadeId']) {
            $pesNecessidadeId = $params['pesNecessidadeId'];
        }

        $parameters = array('necessidade_nome' => "{$necessidadeNome}%");
        $sql .= ' necessidade_nome LIKE :necessidade_id';

        if ($pesNecessidadeId) {
            $parameters['pes_necessidade_id'] = $pesNecessidadeId;
            $sql .= ' AND pes_necessidade_id <> :pes_necessidade_id';
        }

        $sql .= " ORDER BY necessidade_nome";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function salvarNecessidadePessoa(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $servicePessoaFisica          = new \Pessoa\Service\PessoaFisica($this->getEm());
        $serviceNecessidadesEspeciais = new \Pessoa\Service\NecessidadesEspeciais($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['pesNecessidadeId']) {
                $objPessoaNecessidades = $this->getRepository()->find($arrDados['pesNecessidadeId']);

                if (!$objPessoaNecessidades) {
                    $this->setLastError('Registro de necessidade especial de pessoa não existe!');

                    return false;
                }
            } else {
                $objPessoaNecessidades = new \Pessoa\Entity\PessoaNecessidades();
            }

            if ($arrDados['pesId']) {
                $objPessoaFisica = $servicePessoaFisica->getRepository()->find($arrDados['pesId']);

                if (!$objPessoaFisica) {
                    $this->setLastError('Registro de pessoa não existe!');

                    return false;
                }

                $objPessoaNecessidades->setPes($objPessoaFisica);
            }

            if ($arrDados['necessidadeId']) {
                $objNecessidadesEspeciais = $serviceNecessidadesEspeciais->getRepository()->find(
                    $arrDados['necessidadeId']
                );

                if (!$objNecessidadesEspeciais) {
                    $this->setLastError('Registro de necessidades especial não existe!');

                    return false;
                }

                $objPessoaNecessidades->setNecessidade($objNecessidadesEspeciais);
            }

            $this->getEm()->persist($objPessoaNecessidades);
            $this->getEm()->flush($objPessoaNecessidades);

            $this->getEm()->commit();

            $arrDados['pesNecessidadeId'] = $objPessoaNecessidades->getPesNecessidadeId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de necessidade especial de pessoa!');
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['pesId']) {
            $errors[] = 'Por favor preencha o campo "pessoa"!';
        }

        if (!$arrParam['necessidadeId']) {
            $errors[] = 'Por favor preencha o campo "necessidade"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM pessoa_necessidades NATURAL JOIN necessidades_especiais";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($pesNecessidadeId)
    {
        $arrDados = array();
        /** @var $objPessoaNecessidades \Pessoa\Entity\PessoaNecessidades */
        $objPessoaNecessidades        = $this->getRepository()->find($pesNecessidadeId);
        $servicePessoaFisica          = new \Pessoa\Service\PessoaFisica($this->getEm());
        $serviceNecessidadesEspeciais = new \Pessoa\Service\NecessidadesEspeciais($this->getEm());

        try {
            $arrDados = $objPessoaNecessidades->toArray();
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('pesNecessidadeId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Pessoa\Entity\PessoaNecessidades */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getPesNecessidadeId(),
                $params['value'] => $objEntity->getNecessidade()->getNecessidadeNome()
            );
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['pesNecessidadeId']) {
            $this->setLastError(
                'Para remover um registro de necessidade especial de pessoa é necessário especificar o código.'
            );

            return false;
        }

        try {
            $objPessoaNecessidades = $this->getRepository()->find($param['pesNecessidadeId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objPessoaNecessidades);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de necessidade especial de pessoa.');

            return false;
        }

        return true;
    }

    public function adicionar($dados)
    {
        $necessidades = null;
        foreach ($dados['pesNecessidades'] as $necessidade) {
            $pessoaNecessidade = [
                'pesNecessidadeId' => '',
                'pes'              => $dados['pes'],
                'necessidade'      => $necessidade
            ];
            $necessidades[]    = parent::adicionar($pessoaNecessidade);
        }

        return $necessidades;
    }

    /**
     * A função de editar ela simplesmente irá vincular novas ou desvincular necessidades
     * por isso eu desvinculo todas as necessidades da pessoa e vinculo novas com base nos dados passados.
     * @author: Breno 13-05-2015.
     * */
    public function edita($dados)
    {
        $pes          = (is_object($dados['pes'])) ? $dados['pes']->getPesId() : $dados['pes'];
        $necessidades = $this->buscaNecessidadesPessoa($pes);
        if ($necessidades) {
            foreach ($necessidades as $id => $nome) {
                //                $this->exec("DELETE FROM pessoa_necessidades WHERE pes_id = {$pes} AND necessidade_id = {$id}");
            }
        }
        $necessidades = $this->adicionar($dados);

        return $necessidades;
    }

    /**
     * Retorna todos as necessidades especiais de uma pessoa em um array que contem o id e o nome da necessidade
     * Exemplo:
     * return array ( '1' => audiovisual);
     * pode ser utilizado para marcar os selects com as necessidades especiais de uma pessoa.
     * @return array
     */
    public function buscaNecessidadesPessoa($pesId)
    {
        $label = array();
        $query = "
        SELECT necessidade_id as id, necessidade_nome as label
        FROM pessoa_necessidades
        INNER JOIN necessidades_especiais USING (necessidade_id)
        WHERE pes_id = {$pesId}";
        $array = $this->executeQuery($query)->fetchAll();

        foreach ($array as $item) {
            $label[$item['id']] = $item['label'];
        }

        return $label;
    }

    public function getArrayPessoaNecessidades($pesId)
    {
        $arrPessoaNecessidadesRetorno = [];
        $arrPessoaNecessidades        = $this->getRepository()->findBy(['pes' => $pesId]);

        /* @var $objPessoaNecessidades \Pessoa\Entity\PessoaNecessidades */
        foreach ($arrPessoaNecessidades as $objPessoaNecessidades) {
            $objPessoa      = $objPessoaNecessidades->getPes()->getPes();
            $objNecessidade = $objPessoaNecessidades->getNecessidade();

            $arrPessoaNecessidadesRetorno[] = [
                'pesNecessidadeId' => $objPessoaNecessidades->getPesNecessidadeId(),
                'pesId'            => $objPessoa->getPesId(),
                'pesNome'          => $objPessoa->getPesNome(),
                'necessidadeId'    => $objNecessidade->getNecessidadeId(),
                'necessidadeNome'  => $objNecessidade->getNecessidadeNome(),
                'id'               => $objNecessidade->getNecessidadeId(),
                'text'             => $objNecessidade->getNecessidadeNome(),
            ];
        }

        return $arrPessoaNecessidadesRetorno;
    }

    public function salvarMultiplos(array &$arrParam, \Pessoa\Entity\PessoaFisica $objPessoaFisica)
    {
        try {
            $this->getEm()->beginTransaction();

            $arrPessoaNecessidades = $arrParam['pessoaNecessidades'];

            if (is_string($arrPessoaNecessidades) && $arrParam['pessoaNecessidades']) {
                $arrPessoaNecessidades = explode(',', $arrPessoaNecessidades);
            }

            $arrPessoaNecessidadesDB = $this->getRepository()->findBy(
                ['pes' => $objPessoaFisica->getPes()->getPesId()]
            );

            /* @var $objPessoaNecessidades \Pessoa\Entity\PessoaNecessidades */
            foreach ($arrPessoaNecessidadesDB as $objPessoaNecessidades) {
                $this->getEm()->remove($objPessoaNecessidades);
                $this->getEm()->flush();
            }

            foreach ($arrPessoaNecessidades as $necessidadeId) {
                $arrNecessidade = [
                    'pesId'         => $objPessoaFisica->getPes()->getPesId(),
                    'necessidadeId' => $necessidadeId
                ];

                if (!$this->salvarNecessidadePessoa($arrNecessidade)) {
                    throw new \Exception($this->getLastError());
                }
            }

            $this->getEm()->commit();

            return true;
        } catch (\Exception $e) {
            $this->getEm()->rollback();
            $this->setLastError('Não foi possível salvar deficiencias do aluno!' . '<br>' . $e->getMessage());
        }

        return false;
    }
}