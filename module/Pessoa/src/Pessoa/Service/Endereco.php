<?php

namespace Pessoa\Service;

use VersaSpine\Service\AbstractService;

/**
 * Description of Endereco
 *
 * @author Matheus Ferreira
 */
class Endereco extends AbstractService
{
    /**
     * @var null
     */
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Pessoa\Entity\Endereco');
    }

    public function excluir($id)
    {
        $entity = $this->getEm()->getReference($this->getEntity(), $id);
        try {
            $this->getEm()->remove($entity);
            $this->getEm()->flush();
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        return true;
    }

    protected function pesquisaForJson($criterios)
    {
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        $arrRequerido = array(
            'endLogradouro' => 'Logradouro',
            'endCidade'     => 'Cidade',
            'endEstado'     => 'Estado',
        );

        foreach ($arrRequerido as $req => $nome) {
            if (!$arrParam[$req]) {
                $errors[] = 'Por favor preencha o campo "' . $nome . '"!';
            }
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function adicionarMultiplos(array $dados)
    {
        $enderecos = array();
        if ($pos = array_search('NULL', $dados['endEstado'])) {
            unset($dados['endEstado'][$pos]);
        }
        for ($i = 0; $i < count($dados['endEstado']); $i++) {
            if (!$dados['endId'][$i]) {
                $new_endereco        = array();
                $new_endereco['pes'] = $dados['pes'];

                $new_endereco['endEstado']         = $dados['endEstado'][$i];
                $new_endereco['endCidade']         = $dados['endCidade'][$i];
                $new_endereco['tipoEndereco']      = $dados['tipoEndereco'][$i];
                $new_endereco['endData']           = new \DateTime('now');
                $new_endereco['endCep']            = ($dados['endCep'][$i] != "") ? $dados['endCep'][$i] : null;
                $new_endereco['endBairro']         = ($dados['endBairro'][$i] != "") ? $dados['endBairro'][$i] : null;
                $new_endereco['endLogradouro']     = ($dados['endLogradouro'][$i] != "") ? $dados['endLogradouro'][$i] : null;
                $new_endereco['endTipoLogradouro'] = ($dados['endTipoLogradouro'][$i] != "") ? $dados['endTipoLogradouro'][$i] : null;
                $new_endereco['endNumero']         = ($dados['endNumero'][$i] != "") ? $dados['endNumero'][$i] : null;
                $new_endereco['endComplemento']    = ($dados['endComplemento'][$i] != "") ? $dados['endComplemento'][$i] : null;

                try {
                    $endereco    = parent::adicionar($new_endereco);  // Registra um novo Endereço
                    $enderecos[] = $endereco;
                } catch (\Exception $ex) {
                    throw new \Exception($ex->getMessage());
                }
            }
        }

        return $enderecos;
    }

    public function editaMultiplos($dados)
    {
        $enderecos = array();
        for ($i = 0; $i < count($dados['endId']); $i++) {
            if ($dados['endId'][$i]) {
                $endereco = array();

                $endereco['pes']               = $dados['pes'];
                $endereco['endId']             = $dados['endId'][$i];
                $endereco['endEstado']         = $dados['endEstado'][$i];
                $endereco['endCidade']         = $dados['endCidade'][$i];
                $endereco['endCep']            = ($dados['endCep'][$i] != "") ? $dados['endCep'][$i] : null;
                $endereco['endBairro']         = ($dados['endBairro'][$i] != "") ? $dados['endBairro'][$i] : null;
                $endereco['endLogradouro']     = ($dados['endLogradouro'][$i] != "") ? $dados['endLogradouro'][$i] : null;
                $endereco['endTipoLogradouro'] = ($dados['endTipoLogradouro'][$i] != "") ? $dados['endTipoLogradouro'][$i] : null;
                $endereco['endNumero']         = ($dados['endNumero'][$i] != "") ? $dados['endNumero'][$i] : null;
                $endereco['endComplemento']    = ($dados['endComplemento'][$i] != "") ? $dados['endComplemento'][$i] : null;
                $endereco['tipoEndereco']      = $dados['tipoEndereco'][$i];

                try {
                    $endereco    = parent::edita($endereco);  // Registra um novo Endereço
                    $enderecos[] = $endereco;
                } catch (\Exception $ex) {
                    throw new \Exception($ex->getMessage());
                }
            } else {
                $endereco = array();

                $endereco['pes']               = $dados['pes'];
                $endereco['endEstado']         = $dados['endEstado'][$i];
                $endereco['endCidade']         = $dados['endCidade'][$i];
                $endereco['endCep']            = ($dados['endCep'][$i] != "") ? $dados['endCep'][$i] : null;
                $endereco['endBairro']         = ($dados['endBairro'][$i] != "") ? $dados['endBairro'][$i] : null;
                $endereco['endLogradouro']     = ($dados['endLogradouro'][$i] != "") ? $dados['endLogradouro'][$i] : null;
                $endereco['endTipoLogradouro'] = ($dados['endTipoLogradouro'][$i] != "") ? $dados['endTipoLogradouro'][$i] : null;
                $endereco['endNumero']         = ($dados['endNumero'][$i] != "") ? $dados['endNumero'][$i] : null;
                $endereco['endComplemento']    = ($dados['endComplemento'][$i] != "") ? $dados['endComplemento'][$i] : null;
                $endereco['tipoEndereco']      = $dados['tipoEndereco'][$i];
                $endereco['endData']           = new \DateTime();

                $enderecos = parent::adicionar($endereco);
            }
        }

        return $enderecos;
    }

    public function buscaEstados()
    {
        $array   = parent::executeQuery("SELECT * FROM estado ORDER BY est_uf ASC")->fetchAll();
        $estados = array();

        foreach ($array as $a) {
            $estados[$a['est_uf']] = $a['est_uf'] . " - " . $a['est_nome'];
        }

        return $estados;
    }

    public function buscaEndereco($idPes = null, $tipoEndereco = "Residencial", $multiplos = true)
    {
        if ($idPes != null) {
            $tipos = (new \Pessoa\Service\TipoEndereco($this->getEm()))->buscaTipoEnderecos();
            foreach ($tipos as $key => $tipo) {
                if ($tipo == $tipoEndereco) {
                    $tipoEndereco = $key;
                }
            }

            $endereco = $this->findOneBy(
                [
                    'pes' => $idPes,
                    //                'tipoEndereco' => $tipoEndereco
                ]
            );
            if ($endereco) {
                if ($multiplos) {
                    return $endereco->toArrayAux();
                }

                return $endereco->toArray();
            }
        }
    }

    /**
     * @param $pesId
     * @return array
     */
    public function getArrayEndereco($pesId)
    {
        try {
            /* @var $objEndereco \Pessoa\Entity\Endereco */
            $objEndereco = $this->getRepository()->findOneBy(array('pes' => $pesId));

            if (!$objEndereco) {
                //Retornar dados vazios
                $objEndereco = new \Pessoa\Entity\Endereco();
            }

            $arrData = array_merge(
                ($objEndereco ? $objEndereco->toArray() : array())
            );

            $serviceEstado = new \Pessoa\Service\Estado($this->getEm());

            if (is_numeric($arrData['endEstado'])) {
                $objEstado = $serviceEstado->getRepository()->find($arrData['endEstado']);

                if ($objEstado) {
                    $arrData['endEstado'] = $objEstado->getEstNome();
                }
            }

            $arrData['endEstadoUF'] = $serviceEstado->nomeUF($arrData['endEstado']);
        } catch (\Exception $e) {
            return array();
        }

        return $arrData;
    }

    /**
     * Pesquisa endereço pelo $endId caso não exista cria registro
     * @param $endId
     * @param $arrDados
     * @return \Pessoa\Entity\Endereco
     * @throws \Exception
     */
    public function salvarEndereco(&$arrDados = array())
    {
        $objEndereco = null;

        if (!$arrDados['endCidade'] || !$arrDados['endEstado']) {
            return $objEndereco;
        } elseif ($arrDados['endCidade'] && !$arrDados['endEstado']) {
            $arrDados['endEstado'] = '-';
        } elseif (!$arrDados['endCidade'] && $arrDados['endEstado']) {
            $arrDados['endCidade'] = '-';
        }

        if ($arrDados['endId']) {
            $objEndereco = $this->getRepository()->find($arrDados['endId']);
        }

        if (!$objEndereco) {
            $objEndereco     = new \Pessoa\Entity\Endereco(array());
            $objTipoEndereco = new \Pessoa\Service\TipoEndereco($this->getEm());

            $objTipoEndereco = $objTipoEndereco->getRepository()->find(1);

            $objEndereco->setTipoEndereco($objTipoEndereco);
            $objEndereco->setEndData('now');
        }

        $objEndereco->setEndCep($arrDados['endCep']);
        $objEndereco->setEndLogradouro($arrDados['endLogradouro']);
        $objEndereco->setEndNumero($arrDados['endNumero']);
        $objEndereco->setEndComplemento($arrDados['endComplemento']);
        $objEndereco->setEndBairro($arrDados['endBairro']);
        $objEndereco->setEndCidade($arrDados['endCidade']);
        $objEndereco->setEndEstado($arrDados['endEstado']);

        if (!$arrDados['pesId']) {
            throw new \Exception('Para registrar um endereço é necessário informar uma pessoa.');
        }

        $servicePessoa = new \Pessoa\Service\Pessoa($this->getEm());
        $objPessoa     = $servicePessoa->getRepository()->find($arrDados['pesId']);

        if (!$objPessoa) {
            throw new \Exception('Para registrar um endereço é necessário informar uma pessoa válida.');
        }

        $objEndereco->setPes($objPessoa);

        $this->getEm()->persist($objEndereco);
        $this->getEm()->flush($objEndereco);

        $arrDados['endId'] = $objEndereco->getEndId();

        return $objEndereco;
    }
}
