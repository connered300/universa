<?php

namespace Pessoa\Service;

use VersaSpine\Service\AbstractService;

/**
 * Description of TipoContato
 *
 * @author Matheus Ferreira
 */
class TipoContato extends AbstractService
{
    const TIPO_CONTATO_EMAIL = 1;
    const TIPO_CONTATO_TELEFONE = 2;
    const TIPO_CONTATO_CELULAR = 3;

    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Pessoa\Entity\TipoContato');
    }

    public function pesquisaForJson($params)
    {
        $sql                 = '
        SELECT a.tipContatoId as tip_contato_id, a.tipContatoNome as tip_contato_nome, a.tipContatoDescricao as tip_contato_descricao
        FROM Pessoa\Entity\TipoContato a
        WHERE';
        $tipContatoDescricao = false;
        $tipContatoId        = false;

        if ($params['q']) {
            $tipContatoDescricao = $params['q'];
        } elseif ($params['query']) {
            $tipContatoDescricao = $params['query'];
        }

        if ($params['tipContatoId']) {
            $tipContatoId = $params['tipContatoId'];
        }

        $parameters = array('tipContatoDescricao' => "{$tipContatoDescricao}%");
        $sql .= ' a.tipContatoDescricao LIKE :tipContatoDescricao';

        if ($tipContatoId) {
            $parameters['tipContatoId'] = explode(',', $tipContatoId);
            $parameters['tipContatoId'] = $tipContatoId;
            $sql .= ' AND a.tipContatoId NOT IN(:tipContatoId)';
        }

        $sql .= " ORDER BY a.tipContatoDescricao";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['tipContatoId']) {
                /** @var $objTipoContato \Pessoa\Entity\TipoContato */
                $objTipoContato = $this->getRepository()->find($arrDados['tipContatoId']);

                if (!$objTipoContato) {
                    $this->setLastError('Registro de tipo não existe!');

                    return false;
                }
            } else {
                $objTipoContato = new \Pessoa\Entity\TipoContato();
            }

            $objTipoContato->setTipContatoNome($arrDados['tipContatoNome']);
            $objTipoContato->setTipContatoDescricao($arrDados['tipContatoDescricao']);

            $this->getEm()->persist($objTipoContato);
            $this->getEm()->flush($objTipoContato);

            $this->getEm()->commit();

            $arrDados['tipContatoId'] = $objTipoContato->getTipContatoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de tipo!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['tipContatoNome']) {
            $errors[] = 'Por favor preencha o campo "nome contato"!';
        }

        if (!$arrParam['tipContatoDescricao']) {
            $errors[] = 'Por favor preencha o campo "descrição contato"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM tipo_contato";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($tipContatoId)
    {
        /** @var $objTipoContato \Pessoa\Entity\TipoContato */
        $objTipoContato = $this->getRepository()->find($tipContatoId);

        try {
            $arrDados = $objTipoContato->toArray();
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('tipContatoId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Pessoa\Entity\TipoContato */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getTipContatoId(),
                $params['value'] => $objEntity->getTipContatoDescricao()
            );
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['tipContatoId']) {
            $this->setLastError('Para remover um registro de tipo é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objTipoContato \Pessoa\Entity\TipoContato */
            $objTipoContato = $this->getRepository()->find($param['tipContatoId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objTipoContato);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de tipo.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
    }
}
