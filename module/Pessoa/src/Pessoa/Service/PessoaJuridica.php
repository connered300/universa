<?php

namespace Pessoa\Service;

use Respect\Validation\Validator;
use VersaSpine\Service\AbstractService;

/**
 * Description of PessoaJuridica
 *
 * @author Matheus
 */
class PessoaJuridica extends AbstractService
{
    /**
     * @var null
     */
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    public function getDataForDatatables($data)
    {
        $query = "
        SELECT
            pessoa.pes_id, pessoa.pes_nome, pj.pes_nome_fantasia, pj.pes_cnpj,
            en.end_estado, en.end_cidade,
            pessoa.con_contato_email,
            pessoa.con_contato_telefone,
            pessoa.con_contato_celular
        FROM pessoa_juridica pj
        INNER JOIN pessoa pessoa ON pessoa.pes_id = pj.pes_id
        LEFT JOIN endereco en ON en.pes_id=pessoa.pes_id";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function pesquisaForJson($params)
    {
        $sql   = '
        SELECT
            pessoa.pes_id, pessoa.pes_id AS pesId,
            pessoa.pes_nome, pessoa.pes_nome AS pesNome,
            pj.pes_nome_fantasia, pj.pes_nome_fantasia AS pesNomeFantasia,
            pj.pes_cnpj, pj.pes_cnpj AS pesCnpj,
            en.end_estado, en.end_estado AS endEstado,
            en.end_cidade, en.end_cidade AS endCidade
        FROM pessoa_juridica pj
        INNER JOIN pessoa pessoa ON pessoa.pes_id = pj.pes_id
        LEFT JOIN endereco en ON en.pes_id=pessoa.pes_id
        WHERE';
        $query = false;

        if ($params['query']) {
            $query = trim($params['query']);
        }

        if ($params['pesId']) {
            $parameters['pes_id'] = trim($params['pesId']);
            $sql .= ' pessoa.pes_id = :pes_id';
        } elseif ($params['pesCnpj']) {
            $parameters['pes_cnpj'] = trim($params['pesCnpj']);
            $sql .= ' pes_cnpj like :pes_cnpj';
        } else {
            $parameters = array(
                'pes_nome' => "{$query}%",
                'pes_id'   => ltrim($query, '0') . "%"
            );
            $sql .= '
            (
                pessoa.pes_id*1 LIKE :pes_id OR
                pes_nome COLLATE UTF8_GENERAL_CI LIKE :pes_nome OR
                pes_nome_fantasia COLLATE UTF8_GENERAL_CI LIKE :pes_nome
            )';
        }

        $sql .= " ORDER BY pes_nome";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function mountSelect()
    {
        $array = $this->getRepository()->findAll();
        $dados = array();

        /* @var $item \Pessoa\Entity\PessoaJuridica */
        foreach ($array as $item) {
            $dados[$item->getPes()->getPesId()] = $item->getPes()->getPesNome();
        }

        return $dados;
    }

    public function pesquisaPessoaPorCnpj($cnpj)
    {
        if ($cnpj == "") {
            return null;
        }

        $sql = "SELECT * FROM pessoa_juridica WHERE pes_cnpj = '$cnpj' ";
        $ret = parent::executeQuery($sql);

        if ($ret->rowCount() > 0) {
            return $ret->fetch();
        }

        return null;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors        = array();
        $servicePessoa = new \Pessoa\Service\Pessoa($this->getEm());

        if (!$servicePessoa->valida($arrParam)) {
            $errors[] = $servicePessoa->getLastError();
        }

        if (!$arrParam['pesNomeFantasia']) {
            $this->setLastError('Por favor informe o "fantasia"!');

            return false;
        }

        if ($arrParam['pesNomeFantasia'] && strlen($arrParam['pesNomeFantasia']) > 255) {
            $this->setLastError('O campo "fantasia" deve conter no máximo 255 caracteres!');

            return false;
        }

        $pesCnpj = (int)preg_replace('/[^0-9]/', '', $arrParam['pesCnpj']);

        if ($pesCnpj) {
            if (!Validator::cnpj()->validate($arrParam['pesCnpj'])) {
                $errors[] = "O CNPJ é inválido!";
            } elseif ($this->verificaSeCNPJJaExiste($pesCnpj, $arrParam['pesId'])) {
                $errors[] = "Já existe uma pessoa cadastrada com este CNPJ.";
            }
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    /**
     * Retorna true se o cnpj já existe na base
     * @param            $pesCnpj
     * @param bool|false $pesId
     * @return bool
     */
    public function verificaSeCNPJJaExiste($pesCnpj, $pesId = false)
    {
        $sql        = "
        SELECT count(*) AS qtd
        FROM pessoa_juridica
        WHERE
        TRIM(REPLACE(REPLACE(REPLACE(REPLACE(pes_cnpj,'.',''),',',''),'-',''),'/','')) LIKE :pesCnpj";
        $parameters = array('pesCnpj' => $pesCnpj);

        if ($pesId) {
            $sql .= ' AND pes_id <> :pesId';
            $parameters['pesId'] = $pesId;
        }

        $result = $this->executeQueryWithParam($sql, $parameters)->fetch();

        return $result['qtd'] > 0;
    }

    /**
     * @param int|null      $pesId
     * @param array         $arrDados
     * @param boolean|false $salvarEndereco
     * @param boolean|false $salvarContato
     * @return \Pessoa\Entity\PessoaJuridica
     */
    public function salvarPessoaJuridica(
        $pesId = null,
        &$arrDados = array(),
        $salvarEndereco = false,
        $salvarContato = false
    ) {
        $objPessoaJuridica = null;

        if ($pesId) {
            $objPessoaJuridica = $this->getRepository()->find($pesId);
        }

        if (!$objPessoaJuridica) {
            $objPessoaJuridica = new \Pessoa\Entity\PessoaJuridica($arrDados);

            $objPessoaJuridica->setPesDataInicio(date('Y-m-d'));
        }

        $servicePessoa = new \Pessoa\Service\Pessoa($this->getEm());

        $arrDados['pesTipo'] = $arrDados['pesTipo'] ? $arrDados['pesTipo'] : $servicePessoa::PES_TIPO_JURIDICA;

        $objPessoa = $servicePessoa->salvarPessoa($arrDados);

        $objPessoaJuridica->setPes($objPessoa);

        if ($arrDados['pesCnpj']) {
            $objPessoaJuridica->setPesCnpj($arrDados['pesCnpj']);
        }

        if ($arrDados['pesNomeFantasia']) {
            $arrDados['pesNomeFantasia'] = trim(preg_replace('/\s+/', ' ', $arrDados['pesNomeFantasia']));

            $objPessoaJuridica->setPesNomeFantasia($arrDados['pesNomeFantasia']);
        }

        if ($arrDados['pesInscEstadual']) {
            $objPessoaJuridica->setPesInscEstadual($arrDados['pesInscEstadual']);
        }

        if ($arrDados['pesDataInicio']) {
            $objPessoaJuridica->setPesDataInicio($arrDados['pesDataInicio']);
        }

        if ($arrDados['pesDataFim']) {
            $objPessoaJuridica->setPesDataFim($arrDados['pesDataFim']);
        }

        if ($arrDados['pesSigla']) {
            $objPessoaJuridica->setPesSigla($arrDados['pesSigla']);
        }

        if ($arrDados['pesNaturezaJuridica']) {
            $objPessoaJuridica->setPesNaturezaJuridica($arrDados['pesNaturezaJuridica']);
        }

        if ($salvarContato) {
            $this->salvarContatos($arrDados, $objPessoa);
        }

        if ($salvarEndereco) {
            $serviceEndereco = new \Pessoa\Service\Endereco($this->getEm());
            $serviceEndereco->salvarEndereco($arrDados);
        }

        $this->getEm()->persist($objPessoaJuridica);
        $this->getEm()->flush($objPessoaJuridica);

        return $objPessoaJuridica;
    }

    public function salvarContatos(&$arrDados, \Pessoa\Entity\Pessoa $objPessoa)
    {
        $serviceContato = new \Pessoa\Service\Contato($this->getEm());

        //Altera/Cria telefone de contato
        $objContatoTelefone = $serviceContato->salvarContato(
            $arrDados['conContatoTelefoneId'],
            \Pessoa\Service\TipoContato::TIPO_CONTATO_TELEFONE
        );
        $objContatoTelefone->setConContato($arrDados['conContatoTelefone']);
        $objContatoTelefone->setPessoa($objPessoa);

        if (!$arrDados['conContatoTelefone']) {
            if ($arrDados['conContatoTelefoneId']) {
                $this->getEm()->remove($objContatoTelefone);
                $this->getEm()->flush();
            }
        } else {
            $this->getEm()->persist($objContatoTelefone);
            $this->getEm()->flush($objContatoTelefone);
        }

        //Altera/Cria celular de contato
        $objContatoCelular = $serviceContato->salvarContato(
            $arrDados['conContatoCelularId'],
            \Pessoa\Service\TipoContato::TIPO_CONTATO_CELULAR
        );
        $objContatoCelular->setConContato($arrDados['conContatoCelular']);
        $objContatoCelular->setPessoa($objPessoa);

        if (!$arrDados['conContatoCelular']) {
            if ($arrDados['conContatoCelularId']) {
                $this->getEm()->remove($objContatoCelular);
                $this->getEm()->flush();
            }
        } else {
            $this->getEm()->persist($objContatoCelular);
            $this->getEm()->flush($objContatoCelular);
        }

        //Altera/Cria email de contato
        $objContatoEmail = $serviceContato->salvarContato(
            $arrDados['conContatoEmailId'],
            \Pessoa\Service\TipoContato::TIPO_CONTATO_EMAIL
        );
        $objContatoEmail->setConContato($arrDados['conContatoEmail']);
        $objContatoEmail->setPessoa($objPessoa);

        if (!$arrDados['conContatoEmail']) {
            if ($arrDados['conContatoEmailId']) {
                $this->getEm()->remove($objContatoEmail);
                $this->getEm()->flush();
            }
        } else {
            $this->getEm()->persist($objContatoEmail);
            $this->getEm()->flush($objContatoEmail);
        }
    }

    public function getArray($pesId)
    {
        /* @var $objPessoaJuridica \Pessoa\Entity\PessoaJuridica */
        $objPessoaJuridica = $this->getRepository()->find($pesId);

        if (!$objPessoaJuridica) {
            $this->setLastError('Pessoa não encontrada!');

            return array();
        }

        $serviceEndereco = new \Pessoa\Service\Endereco($this->getEm());
        $serviceContato  = new \Pessoa\Service\Contato($this->getEm());

        try {
            $arrDados    = $objPessoaJuridica->toArray();
            $arrEndereco = $serviceEndereco->getArrayEndereco($pesId);
            $arrContato  = $serviceContato->getArrayContato($pesId);

            $arrDados = array_merge($arrDados, array_filter($arrContato), array_filter($arrEndereco));
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    public function salvarPessoaJuridicaComValidacao(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $servicePessoaJuridica = new \Pessoa\Service\PessoaJuridica($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            //Altera/Cria Pessoa Jurídica
            $objPessoaJuridica = $servicePessoaJuridica->salvarPessoaJuridica(
                $arrDados['pesId'],
                $arrDados,
                true,
                true
            );

            $this->getEm()->commit();

            $arrDados['pesId'] = $objPessoaJuridica->getPes()->getPesId();

            return true;
        } catch (\Exception $e) {
            $this->getEm()->rollback();
            $this->setLastError('Não foi possível salvar o registro de pessoa jurídica!<br>' . $e->getMessage());
        }

        return false;
    }

    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, "Pessoa\Entity\PessoaJuridica");
    }

    public function adicionar(array $dados)
    {
        $this->begin();
        try {
            $SV_pessoa = new \Pessoa\Service\Pessoa($this->getEm());

            $dados['pesTipo'] = 'Juridica';
            $dados['pes']     = $SV_pessoa->adicionar($dados);
            $dados['pes']     = $dados['pes']->getPesId();

            $pessoaPJ = parent::adicionar($dados);

            $enderecos = (new \Pessoa\Service\Endereco($this->getEm()))->adicionarMultiplos($dados);
        } catch (\Exception $ex) {
            die("Erro ao registrar Pessoa Juridica:<br/><br/>" . $ex);
        }
        // regista os enderecos

        $this->commit();

        return $pessoaPJ;
    }

    public function edita(array $dados)
    {
        // edita campos da tabela pessoa
        parent::begin();
        try {
            (new \Pessoa\Service\Pessoa($this->getEm()))->edita($dados);
            $pessoaPJ = parent::edita($dados);
        } catch (\Exception $ex) {
            throw new \Exception("Erro ao editar registro de pessoa Pessoa Juridica:<br/><br/>" . $ex->getMessage());
        }
        $qtd_endId        = count($dados['endId']);
        $endereco_service = new \Pessoa\Service\Endereco($this->getEm());

        $endereco_service->editaMultiplos($dados);
        $endereco_service->adicionarMultiplos($dados);
        $ent = parent::edita($dados);
        parent::commit();

        return $ent;
    }

    public function excluir($endId)
    {
        // verifica se este endereço possui vinculo com uma Pessoa Juridica 
        $ends = $this->getRepository("Pessoa\Entity\PessoaJuridicaEnd")->findOneBy(array('enderecoEnd' => $endId));
        if (is_object($ends)) {
            // exclui o vinculo do endereco com Pessoa Juridica
            (new \Pessoa\Service\PessoaJuridicaEnd($this->getEm()))->excluir($ends->getPessEndId());
        }
        // exclui endereco

        $ret = (new \Pessoa\Service\Endereco($this->getEm()))->excluir($endId);

        if ($ret) {
            return array('retorno_end' => true);
        }

        return array('retorno_end' => false);
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $servicePessoa = new \Pessoa\Service\Pessoa($this->getEm());

        $servicePessoa->setarDependenciasView($view);
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        return $arrDados;
    }

    public function remover($pesId)
    {
        if (!$pesId) {
            $this->setLastError('Para remover um registro de pessoa é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objPessoaJuridica \Pessoa\Entity\PessoaJuridica */
            $objPessoaJuridica = $this->getRepository()->find($pesId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objPessoaJuridica);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError(
                'Falha ao remover registro de pessoa.<br>' .
                'Verifique se existe algum vínculo com o registro.'
            );

            return false;
        }

        return true;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('pes' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Pessoa\Entity\PessoaJuridica */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getPes()->getPesId(),
                $params['value'] => $objEntity->getPes()->getPesNome(),
            );
        }

        return $arrEntitiesArr;
    }
}