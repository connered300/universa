<?php
namespace Pessoa\Service;

use VersaSpine\Service\AbstractService;
/**
 * Description of TipoEndereco
 *
 * @author Matheus Ferreira
 */
class TipoEndereco extends AbstractService{
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Pessoa\Entity\TipoEndereco');
    }
   
    protected function pesquisaForJson($criterios)
    {
    }

    protected function valida($dados)
    {
    }
    
    public function buscaTipoEnderecos ()
    {
        $array   = $this->getRepository()->findAll();
        $options = array();
        
        foreach ($array as $a){
            $options[$a->getTipoEnderecoId()] = $a->getNome();
        }
        
        return $options;
    }

//    public function buscaTipoEndereco($tipoEndereco = "Residêncial")
//    {
//        $endereco = $this->findOneBy([
//            'endTipo' => $tipoEndereco
//        ]);
//
//        if($endereco){
//            return $endereco->tipoEnderecoId;
//        }
//    }
}
