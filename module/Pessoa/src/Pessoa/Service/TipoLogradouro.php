<?php

namespace Pessoa\Service;

use VersaSpine\Service\AbstractService;

/**
 * Description of TipoLogradouro
 * 
 * @author Matheus Ferreira
 */
class TipoLogradouro extends AbstractService {
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, "Pessoa\Entity\TipoLogradouro");
    }

    protected function pesquisaForJson($criterios)
    {
    }

    protected function valida($dados)
    {
    }

}
