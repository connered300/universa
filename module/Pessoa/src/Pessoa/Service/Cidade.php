<?php

namespace Pessoa\Service;

use VersaSpine\Service\AbstractService;

/**
 * Description of Cidade
 *
 * @author JoaoPaulo
 */
class Cidade extends AbstractService
{
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, "Pessoa\Entity\Cidade");
    }

    /**
     * @param     $url
     * @param int $timeout
     * @return mixed
     */
    private function getResponse($url, $timeout = 60, $dataPost = array())
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);

        if ($dataPost) {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($dataPost));
        }

        $output = curl_exec($curl);
        curl_close($curl);

        return $output;
    }

    private function buscaCepRepublicaVirtual($cep)
    {
        $url    = 'http://republicavirtual.com.br/web_cep.php?cep=' . urlencode($cep) . '&formato=json';
        $result = $this->getResponse($url);
        $result = json_decode($result, true);

        if (!$result || !$result['resultado']) {
            return false;
        }

        return array(
            'logr_nome'          => $result['logradouro'],
            'logradouro'         => $result['logradouro'],
            'logradouroComposto' => trim($result['tipo_logradouro'] . ' ' . $result['logradouro']),
            'tip_logr_nome'      => $result['tipo_logradouro'],
            'tipo_logradouro'    => $result['tipo_logradouro'],
            'bai_nome'           => $result['bairro'],
            'bairo'              => $result['bairro'],
            'cid_nome'           => $result['cidade'],
            'cidade'             => $result['cidade'],
            'cid_cep'            => $cep,
            'cep'                => $cep,
            'est_nome'           => $result['uf'],
            'estado'             => $result['uf'],
            'est_uf'             => $result['uf'],
            'uf'                 => $result['uf'],
        );
    }

    private function buscaCepViaCep($cep)
    {
        $result = $this->getResponse('http://viacep.com.br/ws/' . $cep . '/json/unicode');
        $result = json_decode($result, true);

        if (!$result || !$result['cep']) {
            return false;
        }

        return array(
            'logr_nome'          => $result['logradouro'],
            'logradouro'         => $result['logradouro'],
            'logradouroComposto' => $result['logradouro'],
            'tip_logr_nome'      => '',
            'tipo_logradouro'    => '',
            'bai_nome'           => $result['bairro'],
            'bairo'              => $result['bairro'],
            'cid_nome'           => $result['cidade'],
            'cidade'             => $result['cidade'],
            'cid_cep'            => $cep,
            'cep'                => $cep,
            'est_nome'           => $result['uf'],
            'estado'             => $result['uf'],
            'est_uf'             => $result['uf'],
            'uf'                 => $result['uf'],
        );
    }

    private function buscaCepCorreios($cep)
    {
        $data   = array('relaxation' => $cep, 'tipoCEP' => 'ALL', 'semelhante' => 'N', 'metodo' => 'buscarCep');
        $result = $this->getResponse(
            "http://www.buscacep.correios.com.br/sistemas/buscacep/resultadoBuscaCepEndereco.cfm",
            60,
            $data
        );
        $result = preg_replace("/[\n\r\t]/", "", $result);
        $result = mb_convert_encoding($result, 'UTF-8', 'ISO-8859-1');

        if (substr_count($result, 'DADOS ENCONTRADOS COM SUCESSO') == 0) {
            return false;
        }

        if (!preg_match(
            '@<table class="tmptabela">.*</table>@msi',
            $result,
            $dadosCep
        )
        ) {
            return false;
        }

        $dadosCep = strip_tags($dadosCep[0], '<td>');
        preg_match_all('@<td[^>]*>([^<]*)</td>@msi', $dadosCep, $celulas);

        $logradouro = trim(strip_tags($celulas[1][0]));
        $bairro     = trim(strip_tags($celulas[1][1]));
        $cidadeUF   = explode('/', trim(strip_tags($celulas[1][2])));
        $cidade     = $cidadeUF[0];
        $UF         = $cidadeUF[1];

        return array(
            'logr_nome'          => $logradouro,
            'logradouro'         => $logradouro,
            'logradouroComposto' => $logradouro,
            'tip_logr_nome'      => '',
            'tipo_logradouro'    => '',
            'bai_nome'           => $bairro,
            'bairo'              => $bairro,
            'cid_nome'           => $cidade,
            'cidade'             => $cidade,
            'cid_cep'            => $cep,
            'cep'                => $cep,
            'est_nome'           => $UF,
            'estado'             => $UF,
            'est_uf'             => $UF,
            'uf'                 => $UF,
        );
    }

    private function tratarDadosServicosCep($arrDados)
    {
        foreach ($arrDados as $pos => $valor) {
            $arrDados[$pos] = trim(str_replace('&nbsp;', ' ', $valor));
        }

        $serviceEstado = new \Pessoa\Service\Estado($this->getEm());

        if (strlen($arrDados['uf']) > 2) {
            foreach ($serviceEstado->getArrEstados() as $uf => $nome) {
                if ($arrDados['uf'] == $nome || $arrDados['uf'] == $uf) {
                    $arrDados['uf']       = $uf;
                    $arrDados['estado']   = $uf;
                    $arrDados['est_nome'] = $uf;
                    $arrDados['est_uf']   = $uf;
                    break;
                }
            }
        }

        return $arrDados;
    }

    public function buscaDadosCep($cep)
    {
        $cep = preg_replace('/[^0-9]/', '', $cep);
        $cep = str_pad($cep, 8, '0');

        $data = $this->buscaCepCorreios($cep);

        if (!$data) {
            $data = $this->buscaCepViaCep($cep);
        }

        if (!$data) {
            $data = $this->buscaCepRepublicaVirtual($cep);
        }

        if ($data) {
            $data = $this->tratarDadosServicosCep($data);

            return $data;
        }

        return array();
    }

    public function buscaDadosCidade($cidade, $estado = null)
    {
        $sql = "
        SELECT
            c.cid_id, c.cid_nome, c.cid_cep,
                e.est_id, e.est_nome, e.est_uf
        FROM cidade c
            INNER JOIN estado e ON e.est_id = c.estado
        WHERE cid_nome LIKE :cid_nome";

        if ($estado != null) {
            $sql .= ' AND e.est_uf = "' . $estado . '"';
        }

        $sql .= " ORDER BY length(cid_nome), cid_nome";
        $sql .= " LIMIT 0, 30";

        return parent::executeQueryWithParam($sql, array('cid_nome' => '%' . $cidade . '%'));
    }

    public function buscaDadosEstado($estado)
    {
        $sql = "
        SELECT e.est_id, e.est_nome, e.est_uf
        FROM estado e
        WHERE (est_nome LIKE :est_nome OR est_uf LIKE :est_nome)
        LIMIT 0, 30";

        return parent::executeQueryWithParam($sql, array('est_nome' => '%' . $estado . '%'));
    }

    public function getEstadoArrSelect2($params = array())
    {
        $params       = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);
        $parametroSql = [];

        $sql = "SELECT e.est_uf AS id, concat(e.est_uf, ' - ', e.est_nome) AS text FROM estado e";

        if ($params['id']) {
            $sql .= ' WHERE (est_nome LIKE :est_nome OR est_uf LIKE :est_nome)';
            $parametroSql['est_nome'] = $params['id'];
        } elseif ($params['text']) {
            $sql .= ' WHERE (est_nome LIKE :est_nome OR est_uf LIKE :est_nome)';
            $parametroSql['est_nome'] = $params['text'];
        }

        $arrDados = parent::executeQueryWithParam($sql, $parametroSql)->fetchAll();

        $arrItens = array();

        foreach ($arrDados as $arrItem) {
            $arrItens[] = array(
                $params['key']   => $arrItem['id'],
                $params['value'] => $arrItem['text']
            );
        }

        return $arrItens;
    }

    protected function pesquisaForJson($criterios)
    {
    }

    protected function valida($dados)
    {
    }

}
