<?php

namespace Pessoa\Service;

use VersaSpine\Service\AbstractService;

/**
 * Description of Contato
 *
 * @author Matheus Ferreira
 */
class Contato extends AbstractService
{
    const CONTATO_TELEFONE = 'Telefone';
    const CONTATO_EMAIL = 'E-mail';
    const CONTATO_CELULAR = 'Celular';

    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Pessoa\Entity\Contato');
    }

    public function pesquisaForJson($params)
    {
        $sql    = '
        SELECT
            a.conId as con_id,
            a.pessoa as pessoa,
            a.tipoContato as tipo_contato,
            a.conContato as con_contato
        FROM Pessoa\Entity\Contato a
        WHERE';
        $pessoa = false;
        $conId  = false;

        if ($params['q']) {
            $pessoa = $params['q'];
        } elseif ($params['query']) {
            $pessoa = $params['query'];
        }

        if ($params['conId']) {
            $conId = $params['conId'];
        }

        $parameters = array('pessoa' => "{$pessoa}%");
        $sql .= ' a.pessoa LIKE :pessoa';

        if ($conId) {
            $parameters['conId'] = explode(',', $conId);
            $parameters['conId'] = $conId;
            $sql .= ' AND a.conId NOT IN(:conId)';
        }

        $sql .= " ORDER BY a.pessoa";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $servicePessoa      = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceTipoContato = new \Pessoa\Service\TipoContato($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['conId']) {
                /** @var $objContato \Pessoa\Entity\Contato */
                $objContato = $this->getRepository()->find($arrDados['conId']);

                if (!$objContato) {
                    $this->setLastError('Registro de contato não existe!');

                    return false;
                }
            } else {
                $objContato = new \Pessoa\Entity\Contato();
            }

            if ($arrDados['pessoa']) {
                /** @var $objPessoa \Pessoa\Entity\Pessoa */
                $objPessoa = $servicePessoa->getRepository()->find($arrDados['pessoa']);

                if (!$objPessoa) {
                    $this->setLastError('Registro de pessoa não existe!');

                    return false;
                }

                $objContato->setPessoa($objPessoa);
            } else {
                $objContato->setPessoa(null);
            }

            if ($arrDados['tipoContato']) {
                /** @var $objTipoContato \Pessoa\Entity\TipoContato */
                $objTipoContato = $serviceTipoContato->getRepository()->find($arrDados['tipoContato']);

                if (!$objTipoContato) {
                    $this->setLastError('Registro de tipo não existe!');

                    return false;
                }

                $objContato->setTipoContato($objTipoContato);
            } else {
                $objContato->setTipoContato(null);
            }

            $objContato->setConContato($arrDados['conContato']);

            $this->getEm()->persist($objContato);
            $this->getEm()->flush($objContato);

            $this->getEm()->commit();

            $arrDados['conId'] = $objContato->getConId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de contato!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['pessoa']) {
            $errors[] = 'Por favor preencha o campo "pessoa"!';
        }

        if (!$arrParam['tipoContato']) {
            $errors[] = 'Por favor preencha o campo "tipo"!';
        }

        if (!$arrParam['conContato']) {
            $errors[] = 'Por favor preencha o campo "con"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "
        SELECT contato.*, pes_nome, tip_contato_nome FROM contato
        INNER JOIN pessoa on pessoa.pes_id = contato.pessoa
        INNER JOIN tipo_contato on tipo_contato.tip_contato_id=contato.tipo_contato
        ";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($conId)
    {
        /** @var $objContato \Pessoa\Entity\Contato */
        $objContato         = $this->getRepository()->find($conId);
        $servicePessoa      = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceTipoContato = new \Pessoa\Service\TipoContato($this->getEm());

        try {
            $arrDados = $objContato->toArray();

            if ($arrDados['pessoa']) {
                /** @var $objPessoa \Pessoa\Entity\Pessoa */
                $objPessoa = $servicePessoa->getRepository()->find($arrDados['pessoa']);

                if ($objPessoa) {
                    $arrDados['pessoa'] = array(
                        'id'   => $objPessoa->getPesId(),
                        'text' => $objPessoa->getPesNome()
                    );
                } else {
                    $arrDados['pessoa'] = null;
                }
            }

            if ($arrDados['tipoContato']) {
                /** @var $objTipoContato \Pessoa\Entity\TipoContato */
                $objTipoContato = $serviceTipoContato->getRepository()->find($arrDados['tipoContato']);

                if ($objTipoContato) {
                    $arrDados['tipoContato'] = array(
                        'id'   => $objTipoContato->getTipContatoId(),
                        'text' => $objTipoContato->getTipContatoNome()
                    );
                } else {
                    $arrDados['tipoContato'] = null;
                }
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $servicePessoa      = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceTipoContato = new \Pessoa\Service\TipoContato($this->getEm());

        if (is_array($arrDados['pessoa']) && !$arrDados['pessoa']['id']) {
            $arrDados['pessoa']['id']   = $arrDados['pessoa']['pesId'];
            $arrDados['pessoa']['text'] = $arrDados['pessoa']['pesNome'];
        } elseif ($arrDados['pessoa'] && is_string($arrDados['pessoa'])) {
            $arrDados['pessoa'] = $servicePessoa->getArrSelect2(
                array('id' => $arrDados['pessoa'])
            );
        }

        if (is_array($arrDados['tipoContato']) && !$arrDados['tipoContato']['id']) {
            $arrDados['tipoContato']['id']   = $arrDados['tipoContato']['tipContatoId'];
            $arrDados['tipoContato']['text'] = $arrDados['tipoContato']['tipContatoNome'];
        } elseif ($arrDados['tipoContato'] && is_string($arrDados['tipoContato'])) {
            $arrDados['tipoContato'] = $serviceTipoContato->getArrSelect2(
                array('id' => $arrDados['tipoContato'])
            );
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('conId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Pessoa\Entity\Contato */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getConId(),
                $params['value'] => (
                    $objEntity->getPessoa()->getPesNome() . ' / ' .
                    $objEntity->getTipoContato()->getTipContatoNome()
                )
            );
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['conId']) {
            $this->setLastError('Para remover um registro de contato é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objContato \Pessoa\Entity\Contato */
            $objContato = $this->getRepository()->find($param['conId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objContato);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de contato.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $servicePessoa      = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceTipoContato = new \Pessoa\Service\TipoContato($this->getEm());

        $servicePessoa->setarDependenciasView($view);
        $serviceTipoContato->setarDependenciasView($view);
    }

    public function adicionar($dados)
    {
        if (!array_key_exists('tipoContato', $dados)) {
            return false;
        }
        $repository = $this->getRepository("Pessoa\Entity\TipoContato");
        foreach ($dados['tipoContato'] as $key => $tipo) {
            $tipo    = $repository->findByTipoContato($tipo);
            $contato = array(
                'conId'       => '',
                'pessoa'      => $dados['pessoa'],
                'tipoContato' => $tipo['tipContatoId'],
                'conContato'  => $dados['conContato'][$key]

            );
            try {
                parent::adicionar($contato);
            } catch (\Exception $ex) {
                throw new \Exception($ex->getMessage());
            }
        }
    }

    public function adicionaNew(array $contato)
    {
        $contato['tipoContato'] = $this->getRepository("Pessoa\Entity\TipoContato")->findByTipoContato(
            $contato['tipoContato']
        )['tipContatoId'];

        return parent::adicionar($contato);
    }

    /**
     * @param $pesId
     * @return array
     */
    public function getArrayContato($pesId)
    {
        try {
            $objContatoTelefone = $this->getRepository()
                ->findOneBy(array('pessoa' => $pesId, 'tipoContato' => TipoContato::TIPO_CONTATO_TELEFONE));
            $objContatoCelular  = $this->getRepository()
                ->findOneBy(array('pessoa' => $pesId, 'tipoContato' => TipoContato::TIPO_CONTATO_CELULAR));
            $objContatoEmail    = $this->getRepository()
                ->findOneBy(array('pessoa' => $pesId, 'tipoContato' => TipoContato::TIPO_CONTATO_EMAIL));

            $arrData = array(
                'conContatoTelefoneId' => '',
                'conContatoTelefone'   => '',
                'conContatoCelularId'  => '',
                'conContatoCelular'    => '',
                'conContatoEmailId'    => '',
                'conContatoEmail'      => '',
            );

            if ($objContatoTelefone) {
                $arrData['conContatoTelefoneId'] = $objContatoTelefone->getConId();
                $arrData['conContatoTelefone']   = $objContatoTelefone->getConContato();
            }

            if ($objContatoCelular) {
                $arrData['conContatoCelularId'] = $objContatoCelular->getConId();
                $arrData['conContatoCelular']   = $objContatoCelular->getConContato();
            }

            if ($objContatoEmail) {
                $arrData['conContatoEmailId'] = $objContatoEmail->getConId();
                $arrData['conContatoEmail']   = $objContatoEmail->getConContato();
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrData;
    }

    /**
     * Pesquisa contato pelo $conId caso não exista cria registro
     * @param bool|false $conId
     * @param int        $contatoTipo
     * @return \Pessoa\Entity\Contato
     */
    public function salvarContato($conId = false, $contatoTipo = 1)
    {
        $objContato = null;

        if ($conId) {
            $objContato = $this->getRepository()->find($conId);
        }

        if (!$objContato) {
            $objContato     = new \Pessoa\Entity\Contato(array());
            $objTipoContato = new \Pessoa\Service\TipoContato($this->getEm());

            $objTipoContato = $objTipoContato->getRepository()->find($contatoTipo);

            $objContato->setTipoContato($objTipoContato);
        }

        return $objContato;
    }
}
