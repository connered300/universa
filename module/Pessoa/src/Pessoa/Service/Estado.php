<?php

namespace Pessoa\Service;

use VersaSpine\Service\AbstractService;

/**
 * Description of Endereco
 *
 * @author Matheus Ferreira
 */
class Estado extends AbstractService
{

    /**
     * @return array
     */
    public function getArrEstados()
    {
        $arrEstados = [
            'AC' => 'Acre',
            'AL' => 'Alagoas',
            'AP' => 'Amapá',
            'AM' => 'Amazonas',
            'BA' => 'Bahia',
            'DF' => 'Brasília',
            'CE' => 'Ceará',
            'ES' => 'Espirito Santo',
            'GO' => 'Goiás',
            'MA' => 'Maranhão',
            'MT' => 'Mato Grosso',
            'MS' => 'Mato Grosso do Sul',
            'MG' => 'Minas Gerais',
            'PA' => 'Pará',
            'PB' => 'Paraíba',
            'PR' => 'Paraná',
            'PE' => 'Pernambuco',
            'PI' => 'Piauí',
            'RJ' => 'Rio de Janeiro',
            'RN' => 'Rio Grande do Norte',
            'RS' => 'Rio Grande do Sul',
            'RO' => 'Rondônia',
            'RR' => 'Roraima',
            'SC' => 'Santa Catarina',
            'SP' => 'São Paulo',
            'SE' => 'Sergipe',
            'TO' => 'Tocantins',
        ];

        return $arrEstados;
    }

    public static function getArrSelect2Estados()
    {
        $arrDados = array();

        foreach (self::getArrEstados() as $key => $value) {
            $arrDados[] = [
                'id'   => $key,
                'text' => $key . ' - ' . $value
            ];
        }

        return $arrDados;
    }

    public static function nomeUF($estado)
    {
        foreach (self::getArrEstados() as $uf => $nome) {
            if ($uf == $estado || $nome == $estado) {
                return $uf;
            }
        }

        $arrEstado = explode(' ', ucwords($estado));
        $uf        = substr($arrEstado[0], 0, 1) . substr($arrEstado[1], 0, 1);

        return $uf;
    }

    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Pessoa\Entity\Estado');
    }

    protected function pesquisaForJson($criterios)
    {
    }

    protected function valida($dados)
    {
    }

}
