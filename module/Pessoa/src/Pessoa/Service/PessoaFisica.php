<?php

namespace Pessoa\Service;

use Respect\Validation\Validator;
use VersaSpine\Service\AbstractService;
use VersaSpine\ViewHelper\StringLib;

/**
 * Description of PessoaFisica
 *
 * @author joao
 */
class PessoaFisica extends AbstractService
{
    const PES_SEXO_MASCULINO             = 'Masculino';
    const PES_SEXO_FEMININO              = 'Feminino';
    const PES_ESTADO_CIVIL_SOLTEIRO      = 'Solteiro';
    const PES_ESTADO_CIVIL_CASADO        = 'Casado';
    const PES_ESTADO_CIVIL_UNIAO_ESTAVEL = 'União Estável';
    const PES_ESTADO_CIVIL_VIUVO         = 'Viúvo';
    const PES_ESTADO_CIVIL_DIVORCIADO    = 'Divorciado';
    const PES_FALECIDO_SIM               = 'Sim';
    const PES_FALECIDO_NAO               = 'Não';
    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Pessoa\Entity\PessoaFisica');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2PesSexo($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getPesSexo());
    }

    public static function getPesSexo()
    {
        return array(self::PES_SEXO_MASCULINO, self::PES_SEXO_FEMININO);
    }

    public function getArrSelect2PesEstadoCivil($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getPesEstadoCivil());
    }

    public static function getPesEstadoCivil()
    {
        return array(
            self::PES_ESTADO_CIVIL_SOLTEIRO,
            self::PES_ESTADO_CIVIL_CASADO,
            self::PES_ESTADO_CIVIL_UNIAO_ESTAVEL,
            self::PES_ESTADO_CIVIL_VIUVO,
            self::PES_ESTADO_CIVIL_DIVORCIADO
        );
    }

    public function getArrSelect2PesFalecido($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getPesFalecido());
    }

    public static function getPesFalecido()
    {
        return array(self::PES_FALECIDO_SIM, self::PES_FALECIDO_NAO);
    }

    public function getDataForDatatables($data)
    {
        $query = "
        SELECT
            pessoa.pes_id, pes_nome, pes_cpf,
            endereco.end_id, tipo_endereco, end_pais, end_estado, end_tipo_end, end_bairro,
            end_cidade, end_cep, end_logradouro, end_tipo_logradouro, end_numero, end_complemento,
            pessoa.con_contato_email,
            pessoa.con_contato_telefone,
            pessoa.con_contato_celular
        FROM pessoa_fisica
        INNER JOIN pessoa ON pessoa.pes_id=pessoa_fisica.pes_id
        LEFT JOIN endereco ON endereco.pes_id=pessoa.pes_id
        GROUP BY pessoa.pes_id";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function pesquisaForJson($params)
    {
        $sql   = '
    SELECT
        pessoa.pes_id, pessoa.pes_id pesId,
        pessoa.pes_nome, pessoa.pes_nome pesNome,
        pessoaFisica.pes_cpf, pessoaFisica.pes_cpf pesCpf,
        pessoaFisica.pes_doc_estrangeiro, pessoaFisica.pes_doc_estrangeiro pesDocEstrangeiro,
        pessoa.pes_nacionalidade, pessoa.pes_nacionalidade pesNacionalidade,
        pessoaFisica.pes_rg, pessoaFisica.pes_rg pesRg,
        pessoaFisica.pes_sexo, pessoaFisica.pes_sexo pesSexo,
        date_format(pessoaFisica.pes_data_nascimento, "%d/%m/%Y") pes_data_nascimento,
        date_format(pessoaFisica.pes_data_nascimento, "%d/%m/%Y") pesDataNascimento,
        endereco.end_estado, endereco.end_estado endEstado,
        endereco.end_cidade, endereco.end_cidade endCidade
    FROM pessoa
        INNER JOIN pessoa_fisica pessoaFisica USING (pes_id)
        LEFT JOIN endereco USING (pes_id)
    WHERE ';
        $query = false;

        if ($params['query']) {
            $query = trim($params['query']);
        }

        if ($params['pesId']) {
            $parameters['pes_id'] = trim($params['pesId']);
            $sql .= ' pessoa.pes_id = :pes_id';
        } elseif ($params['pesCpf']) {
            $parameters['pes_cpf'] = trim($params['pesCpf']);
            $sql .= ' pes_cpf like :pes_cpf';
        } elseif ($params['pesDocEstrangeiro']) {
            $parameters['pes_doc_estrangeiro'] = trim($params['pesDocEstrangeiro']);
            $sql .= ' pes_doc_estrangeiro = :pes_doc_estrangeiro';
        } else {
            $parameters = array(
                'pes_nome' => "{$query}%",
                'pes_id'   => ltrim($query, '0') . "%"
            );
            $sql .= ' (pessoa.pes_id*1 LIKE :pes_id OR pes_nome COLLATE UTF8_GENERAL_CI LIKE :pes_nome)';
        }

        $sql .= " ORDER BY pes_nome";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function adicionar(array $dados)
    {
        $this->begin();
        try {
            $SV_pessoa = new \Pessoa\Service\Pessoa($this->getEm());

            $dados['pesTipo'] = 'Fisica';
            $dados['pes']     = $SV_pessoa->adicionar($dados);
            $dados['pes']     = $dados['pes']->getPesId();

            $pessoaPF = parent::adicionar($dados);

            $enderecos = (new \Pessoa\Service\Endereco($this->getEm()))->adicionarMultiplos(
                $dados
            );  // Instancia de servico de Endereço
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
        }

        $this->commit();

        return $pessoaPF;
    }

    public function edita(array $dados)
    {
        try {
            (new \Pessoa\Service\Endereco($this->getEm()))->editaMultiplos($dados);
        } catch (\Exception $ex) {
            throw new \Exception('Erro ao editar Endereco de Pessoa Fisica<br/><br/>' . $ex->getMessage());
        }

        return parent::edita($dados);
    }

    public function mountSelect()
    {
        $array = $this->getRepository()->findAll();
        $dados = array();

        /* @var $item \Pessoa\Entity\PessoaFisica */
        foreach ($array as $item) {
            $dados[$item->getPes()->getPesId()] = $item->getPes()->getPesNome();
        }

        return $dados;
    }

    public function pesquisarPessoaPelosDados($arrDados, $priorizarBanco = false)
    {
        $pesId = false;

        if (!$pesId && $arrDados['pesId']) {
            $pesId = $arrDados['pesId'];
        }

        if (!$pesId && $arrDados['pesCpf']) {
            $pesCpf = preg_replace('/[^0-9]/', '', $arrDados['pesCpf']);
            $pesId  = $this->verificaSeCPFJaExiste($pesCpf, $arrDados['pesId']);
        }

        if (!$pesId && $arrDados['pesNome'] && $arrDados['conContatoEmail']) {
            $pesId = $this->pesquisaPessoaPorNomeEContato($arrDados['pesNome'], $arrDados['conContatoEmail']);
        }

        if (!$pesId && $arrDados['pesNome'] && $arrDados['conContatoCelular']) {
            $pesId = $this->pesquisaPessoaPorNomeEContato($arrDados['pesNome'], $arrDados['conContatoCelular'], true);
        }

        if (!$pesId && $arrDados['pesNome'] && $arrDados['conContatoTelefone']) {
            $pesId = $this->pesquisaPessoaPorNomeEContato($arrDados['pesNome'], $arrDados['conContatoTelefone'], true);
        }

        if (!$pesId && $arrDados['pesNome'] && $arrDados['endCidade']) {
            $pesId = $this->pesquisaPessoaPorNomeECidade($arrDados['pesNome'], $arrDados['endCidade']);
        }

        if (!$pesId && $arrDados['pesNome']) {
            $pesId = $this->pesquisaPessoaPorNomeSemContato($arrDados['pesNome']);
        }

        if ($pesId) {
            $arrDadosPessoa = $this->getArray($pesId);

            if ($priorizarBanco) {
                $arrDados = array_merge(array_filter($arrDados), array_filter($arrDadosPessoa));
            } else {
                $arrDados = array_merge(array_filter($arrDadosPessoa), array_filter($arrDados));
            }
        }

        return $arrDados;
    }

    public function pesquisaPessoaPorNomeEContato($pesNome, $conContato, $telefone = false)
    {
        $conContato = preg_replace('/[\., \)\(\-\/]/', '', $conContato);

        if ($telefone) {
            $conContato = preg_replace('/[^0-9]/', '', $conContato);
        }

        $stringLib  = new StringLib();
        $conContato = $stringLib->removeAcento(trim($conContato));
        $pesNome    = $stringLib->removeAcento(trim($pesNome));
        $pesNome    = preg_replace('/[ \-\.]/', '', $pesNome);

        if (!$pesNome || !$conContato) {
            return null;
        }

        $sql = "
        SELECT pessoa.pes_id
        FROM pessoa
        LEFT JOIN pessoa_fisica USING (pes_id)
        LEFT JOIN contato ON contato.pessoa=pessoa.pes_id
        WHERE
        REPLACE(
          REPLACE(
            REPLACE(pessoa.pes_nome, '.', ''),
            '-',''
          ),
          ' ',''
        ) LIKE :pesNome AND
        REPLACE(
          REPLACE(
            REPLACE(
              REPLACE(
                REPLACE(
                  REPLACE(
                    REPLACE(
                      REPLACE(contato.con_contato,'.',''),
                      ',',''
                    ),
                    ' ',''
                  ),
                  ')',''
                ),
                '(',''
              ),
              '-',''
            ),
            '/',''
          ),
          '-',''
        ) LIKE :conContato
        GROUP BY pessoa.pes_id";

        $parameters = array(
            'pesNome'    => $pesNome,
            'conContato' => $conContato
        );

        $result = $this->executeQueryWithParam($sql, $parameters)->fetch();

        return $result['pes_id'] ? $result['pes_id'] : false;
    }

    public function pesquisaPessoaPorNomeECidade($pesNome, $endCidade)
    {
        $stringLib = new StringLib();
        $endCidade = $stringLib->removeAcento(trim($endCidade));
        $pesNome   = $stringLib->removeAcento(trim($pesNome));
        $pesNome   = preg_replace('/[ \-\.]/', '', $pesNome);
        $endCidade = preg_replace('/[ \-\.]/', '', $endCidade);

        if (!$pesNome || !$endCidade) {
            return null;
        }

        $sql = "
        SELECT pessoa.pes_id
        FROM pessoa
        LEFT JOIN pessoa_fisica USING (pes_id)
        LEFT JOIN endereco ON endereco.pes_id=pessoa.pes_id
        WHERE
        REPLACE(
          REPLACE(
            REPLACE(pessoa.pes_nome, '.', ''),
            '-',''
          ),
          ' ',''
        ) LIKE :pesNome AND
        REPLACE(
          REPLACE(
            REPLACE(end_cidade, '.', ''),
            '-',''
          ),
          ' ',''
        ) LIKE :endCidade
        GROUP BY pessoa.pes_id";

        $parameters = array(
            'pesNome'   => $pesNome,
            'endCidade' => $endCidade
        );

        $result = $this->executeQueryWithParam($sql, $parameters)->fetch();

        return $result['pes_id'] ? $result['pes_id'] : false;
    }

    public function pesquisaPessoaPorNomeSemContato($pesNome)
    {
        $stringLib = new StringLib();
        $pesNome   = $stringLib->removeAcento(trim($pesNome));
        $pesNome   = preg_replace('/[ \-\.]/', '', $pesNome);

        if (!$pesNome) {
            return null;
        }

        $sql = "
        SELECT pessoa.pes_id
        FROM pessoa
        LEFT JOIN pessoa_fisica USING (pes_id)
        WHERE
        REPLACE(
          REPLACE(
            REPLACE(pessoa.pes_nome, '.', ''),
            '-',''
          ),
          ' ',''
        ) LIKE :pesNome AND pes_id NOT IN(
          SELECT pessoa FROM contato
        )";

        $parameters = array('pesNome' => $pesNome);

        $result = $this->executeQueryWithParam($sql, $parameters)->fetch();

        return $result['pes_id'] ? $result['pes_id'] : false;
    }

    public function pesquisaPessoaPorCpf($cpf)
    {
        if ($cpf == "") {
            return null;
        }

        $sql = "SELECT * FROM pessoa_fisica WHERE pes_cpf = '$cpf' ";
        $ret = parent::executeQuery($sql);

        if ($ret->rowCount() > 0) {
            return $ret->fetch();
        }

        return null;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors        = array();
        $servicePessoa = new \Pessoa\Service\Pessoa($this->getEm());

        if (!$servicePessoa->valida($arrParam)) {
            $errors[] = $servicePessoa->getLastError();
        }

        $arrRequerido = array(
            'pesSexo'           => 'Sexo',
            'pesDataNascimento' => 'Data de nascimento',
            //'pesNascUf'         => 'UF de nascimento',
            //'pesNaturalidade'   => 'Naturalidade',
        );

        foreach ($arrRequerido as $req => $nome) {
            if (!$arrParam[$req]) {
                $errors[] = 'Por favor informe o valor de "' . $nome . '"!';
            }
        }

        if ($arrParam['pesSexo'] && !in_array($arrParam['pesSexo'], self::getPesSexo())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "sexo"!';
        }

        if ($arrParam['pesEstadoCivil'] && !in_array($arrParam['pesEstadoCivil'], self::getPesEstadoCivil())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "estado cívil"!';
        }

        if ($arrParam['pesFalecido'] && !in_array($arrParam['pesFalecido'], self::getPesFalecido())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "pessoa falecida"!';
        }

        if (!$arrParam['desabilitarObrigatoriedadeCpf']) {
            if (empty($arrParam['pesCpf']) && empty($arrParam['pesDocEstrangeiro'])) {
                $errors[] = "Informe o CPF ou o documento estrangeiro da pessoa!";
            } elseif (!empty($arrParam['pesCpf'])) {
                $pesCpf = preg_replace('/[^0-9]/', '', $arrParam['pesCpf']);

                if (!Validator::cpf()->validate($arrParam['pesCpf'])) {
                    $errors[] = "O CPF é inválido!";
                } elseif ($this->verificaSeCPFJaExiste($pesCpf, $arrParam['pesId'])) {
                    $errors[] = "Já existe uma pessoa cadastrada com este CPF.";
                }
            } elseif (empty($arrParam['pesDocEstrangeiro'])) {
                $errors[] = "Informe o documento estrangeiro da pessoa!";
            }
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    /**
     * Retorna true se o cpf já existe na base
     * @param            $pesCpf
     * @param bool|false $pesId
     * @return bool|int
     */
    public function verificaSeCPFJaExiste($pesCpf, $pesId = false)
    {
        $sql        = "
        SELECT pes_id
        FROM pessoa_fisica
        WHERE
        TRIM(REPLACE(REPLACE(REPLACE(REPLACE(pes_cpf,'.',''),',',''),'-',''),'/','')) LIKE :pesCpf";
        $parameters = array('pesCpf' => $pesCpf);

        if ($pesId) {
            $sql .= ' AND pes_id <> :pesId';
            $parameters['pesId'] = $pesId;
        }

        $result = $this->executeQueryWithParam($sql, $parameters)->fetch();

        return $result['pes_id'] ? $result['pes_id'] : false;
    }

    /**
     * @param int|null      $pesId
     * @param array         $arrDados
     * @param boolean|false $salvarEndereco
     * @param boolean|false $salvarContato
     * @return \Pessoa\Entity\PessoaFisica
     */
    public function salvarPessoaFisica(
        $pesId = null,
        &$arrDados = array(),
        $salvarEndereco = false,
        $salvarContato = false,
        $salvarDeficiencias = false
    ) {
        $objPessoaFisica = null;

        if ($pesId) {
            $objPessoaFisica = $this->getRepository()->find($pesId);
        }

        if (!$objPessoaFisica) {
            $objPessoaFisica = new \Pessoa\Entity\PessoaFisica($arrDados);
        }

        $servicePessoa = new \Pessoa\Service\Pessoa($this->getEm());
        $objPessoa     = $servicePessoa->salvarPessoa($arrDados);

        $objPessoaFisica->setPes($objPessoa);

        if (isset($arrDados['pesCpf'])) {
            $objPessoaFisica->setPesCpf($arrDados['pesCpf']);
        }

        if ($arrDados['pesCpfEmissao']) {
            $objPessoaFisica->setPesCpfEmissao($arrDados['pesCpfEmissao']);
        }

        if ($arrDados['pesRg']) {
            $objPessoaFisica->setPesRg($arrDados['pesRg']);
        }

        if ($arrDados['pesRgEmissao']) {
            $objPessoaFisica->setPesRgEmissao($arrDados['pesRgEmissao']);
        }

        if ($arrDados['pesSobrenome']) {
            $objPessoaFisica->setPesSobrenome($arrDados['pesSobrenome']);
        }

        if ($arrDados['pesSexo']) {
            $objPessoaFisica->setPesSexo($arrDados['pesSexo']);
        }

        if ($arrDados['pesDataNascimento']) {
            $objPessoaFisica->setPesDataNascimento($arrDados['pesDataNascimento']);
        }

        if ($arrDados['pesEstadoCivil']) {
            $objPessoaFisica->setPesEstadoCivil($arrDados['pesEstadoCivil']);
        }

        if ($arrDados['pesFilhos']) {
            $objPessoaFisica->setPesFilhos($arrDados['pesFilhos']);
        }

        if ($arrDados['pecessidade']) {
            $objPessoaFisica->setNecessidade($arrDados['pecessidade']);
        }

        if ($arrDados['pesDocEstrangeiro']) {
            $objPessoaFisica->setPesDocEstrangeiro($arrDados['pesDocEstrangeiro']);
        }

        if ($arrDados['pesEmissorRg']) {
            $objPessoaFisica->setPesEmissorRg($arrDados['pesEmissorRg']);
        }

        if ($arrDados['pesNascUf']) {
            $objPessoaFisica->setPesNascUf($arrDados['pesNascUf']);
        }

        if ($arrDados['pesFalecido']) {
            $objPessoaFisica->setPesFalecido($arrDados['pesFalecido']);
        }

        if ($arrDados['pesNaturalidade']) {
            $objPessoaFisica->setPesNaturalidade($arrDados['pesNaturalidade']);
        }

        if ($arrDados['pesEmpresa']) {
            $objPessoaFisica->setPesEmpresa($arrDados['pesEmpresa']);
        }

        if ($arrDados['pesProfissao']) {
            $objPessoaFisica->setPesProfissao($arrDados['pesProfissao']);
        }

        if ($salvarContato) {
            $this->salvarContatos($arrDados, $objPessoa);
        }

        if ($salvarEndereco) {
            $serviceEndereco = new \Pessoa\Service\Endereco($this->getEm());
            $serviceEndereco->salvarEndereco($arrDados);
        }

        $this->getEm()->persist($objPessoaFisica);
        $this->getEm()->flush($objPessoaFisica);

        if ($salvarDeficiencias) {
            $servicePessoaNecessidades = new \Pessoa\Service\PessoaNecessidades($this->getEm());
            $servicePessoaNecessidades->salvarMultiplos($arrDados, $objPessoaFisica);
        }

        return $objPessoaFisica;
    }

    public function salvarContatos(&$arrDados, \Pessoa\Entity\Pessoa $objPessoa)
    {
        $serviceContato = new \Pessoa\Service\Contato($this->getEm());

        //Altera/Cria telefone de contato
        $objContatoTelefone = $serviceContato->salvarContato(
            $arrDados['conContatoTelefoneId'],
            \Pessoa\Service\TipoContato::TIPO_CONTATO_TELEFONE
        );
        $objContatoTelefone->setConContato($arrDados['conContatoTelefone']);
        $objContatoTelefone->setPessoa($objPessoa);

        if (!$arrDados['conContatoTelefone']) {
            if ($arrDados['conContatoTelefoneId']) {
                $this->getEm()->remove($objContatoTelefone);
                $this->getEm()->flush();
            }
        } else {
            $this->getEm()->persist($objContatoTelefone);
            $this->getEm()->flush($objContatoTelefone);
        }

        //Altera/Cria celular de contato
        $objContatoCelular = $serviceContato->salvarContato(
            $arrDados['conContatoCelularId'],
            \Pessoa\Service\TipoContato::TIPO_CONTATO_CELULAR
        );
        $objContatoCelular->setConContato($arrDados['conContatoCelular']);
        $objContatoCelular->setPessoa($objPessoa);

        if (!$arrDados['conContatoCelular']) {
            if ($arrDados['conContatoCelularId']) {
                $this->getEm()->remove($objContatoCelular);
                $this->getEm()->flush();
            }
        } else {
            $this->getEm()->persist($objContatoCelular);
            $this->getEm()->flush($objContatoCelular);
        }

        //Altera/Cria email de contato
        $objContatoEmail = $serviceContato->salvarContato(
            $arrDados['conContatoEmailId'],
            \Pessoa\Service\TipoContato::TIPO_CONTATO_EMAIL
        );
        $objContatoEmail->setConContato($arrDados['conContatoEmail']);
        $objContatoEmail->setPessoa($objPessoa);

        if (!$arrDados['conContatoEmail']) {
            if ($arrDados['conContatoEmailId']) {
                $this->getEm()->remove($objContatoEmail);
                $this->getEm()->flush();
            }
        } else {
            $this->getEm()->persist($objContatoEmail);
            $this->getEm()->flush($objContatoEmail);
        }
    }

    public function getArray($pesId)
    {
        /* @var $objPessoaFisica \Pessoa\Entity\PessoaFisica */
        $objPessoaFisica = $this->getRepository()->find($pesId);

        if (!$objPessoaFisica) {
            $this->setLastError('Pessoa não encontrada!');

            return array();
        }

        $serviceEndereco           = new \Pessoa\Service\Endereco($this->getEm());
        $serviceContato            = new \Pessoa\Service\Contato($this->getEm());
        $servicePessoaNecessidades = new \Pessoa\Service\PessoaNecessidades($this->getEm());

        try {
            $arrDados    = $objPessoaFisica->toArray();
            $arrEndereco = $serviceEndereco->getArrayEndereco($pesId);
            $arrContato  = $serviceContato->getArrayContato($pesId);

            $arrDados                       = array_merge(
                $arrDados,
                array_filter($arrContato),
                array_filter($arrEndereco)
            );
            $arrDados['pessoaNecessidades'] = $servicePessoaNecessidades->getArrayPessoaNecessidades($pesId);
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    public function salvarPessoaFisicaComValidacao(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $servicePessoaFisica = new \Pessoa\Service\PessoaFisica($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            //Altera/Cria Pessoa Fisica
            $objPessoaFisica = $servicePessoaFisica->salvarPessoaFisica(
                $arrDados['pesId'],
                $arrDados,
                true,
                true,
                true
            );

            $this->getEm()->commit();

            $arrDados['pesId'] = $objPessoaFisica->getPes()->getPesId();

            return true;
        } catch (\Exception $e) {
            $this->getEm()->rollback();
            $this->setLastError('Não foi possível salvar o registro de pessoa física!<br>' . $e->getMessage());
        }

        return false;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('pes' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Pessoa\Entity\PessoaFisica */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getPes()->getPesId(),
                $params['value'] => $objEntity->getPes()->getPesNome(),
            );
        }

        return $arrEntitiesArr;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $servicePessoa                = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceNecessidadesEspeciais = new \Pessoa\Service\NecessidadesEspeciais($this->getEm());
        $serviceCidade                = new \Pessoa\Service\Cidade($this->getEm());
        $servicePessoa->setarDependenciasView($view);

        $arrEstados = $serviceCidade->getEstadoArrSelect2();

        $view->setVariable("arrNecessidadesEspeciais", $serviceNecessidadesEspeciais->getArrSelect2());
        $view->setVariable("arrPesSexo", $this->getArrSelect2PesSexo());
        $view->setVariable("arrEstados", $arrEstados);
        $view->setVariable("arrPesEstadoCivil", $this->getArrSelect2PesEstadoCivil());
        $view->setVariable("arrPesFalecido", $this->getArrSelect2PesFalecido());
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceNecessidadesEspeciais = new \Pessoa\Service\NecessidadesEspeciais($this->getEm());

        if ($arrDados['pessoaNecessidades'] && is_string($arrDados['pessoaNecessidades'])) {
            $arrDados['pessoaNecessidades'] = explode(',', $arrDados['pessoaNecessidades']);
            $arrDados['pessoaNecessidades'] = $serviceNecessidadesEspeciais->getArrSelect2(
                array(
                    'id' => $arrDados['pessoaNecessidades']
                )
            );
        }

        return $arrDados;
    }

    public function remover($pesId)
    {
        if (!$pesId) {
            $this->setLastError('Para remover um registro de pessoa é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objPessoaFisica \Pessoa\Entity\PessoaFisica */
            $objPessoaFisica = $this->getRepository()->find($pesId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objPessoaFisica);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError(
                'Falha ao remover registro de pessoa.<br>' .
                'Verifique se existe algum vínculo com o registro.'
            );

            return false;
        }

        return true;
    }

    public function buscaCpfDuplicados()
    {
        $sql = "
        SELECT pes_id, pes_cpf, count(*) as qtd, group_concat(pes_id) as pes_id_duplicados
        FROM (
            SELECT pes_id, REPLACE(REPLACE(REPLACE(pes_cpf, '-', ''), '.', '' ), ' ', '') as pes_cpf
            FROM pessoa_fisica
        ) as pessoa
        WHERE CAST(pes_cpf as DECIMAL) > 0
        GROUP BY pes_cpf
        HAVING qtd > 1";

        $cpfDuplicados = $this->executeQueryWithParam($sql)->fetchAll();

        $result = [];

        foreach ($cpfDuplicados as $cadaCpf) {
            $sql = "
            SELECT * FROM pessoa_fisica
            INNER JOIN pessoa using (pes_id)
            WHERE pes_id in ({$cadaCpf['pes_id_duplicados']})
            ORDER BY pes_id";

            $result[$cadaCpf['pes_cpf']] = $this->executeQueryWithParam($sql)->fetchAll();
        }

        return $result;
    }

    public function removeDuplicados()
    {
        $totalCpfDuplicados = 0;
        $totalRemovidos     = 0;

        //faço um do while devido a possibilidade de haver mais de 2 pessoas com o msm cpf
        //como a lógica utilizada pra facilitar o desenvolvimento foi de 2 em 2 pessoas o do while foi a melhor solução
        //pra limpar a base mais rapidamente
        do {
            $cpfDuplicados = $this->buscaCpfDuplicados();

            $removidos          = 0;
            $totalCpfDuplicados = $totalCpfDuplicados ? $totalCpfDuplicados : sizeof($cpfDuplicados);

            foreach ($cpfDuplicados as $cadaCpf) {
                //tratando casos onde a quantidade de pessoas com cpf repetido seja maior que dois
                $arrAnterior = [];

                foreach ($cadaCpf as $key => $item) {
                    $dataNascimentoAtual    = $item['pes_data_nascimento'];
                    $dataNascimentoAnterior = $arrAnterior['pes_data_nascimento'];

                    $nascimentoIgual = ($dataNascimentoAtual && $dataNascimentoAnterior == $dataNascimentoAtual);
                    $nomeParecido    = $this->compareFirstAndLastName($arrAnterior["pes_nome"], $item["pes_nome"]);

                    if ($arrAnterior && ($nascimentoIgual || $nomeParecido)) {
                        try {
                            $this->mergePessoa($arrAnterior, $item);
                            $removidos++;
                        } catch (\Exception $ex) {
                        }
                    } else {
                        //nomes diferentes, sem data de nascimento ou com data de nascimento diferentes
                        if ($arrAnterior) {
                            $sql      = "
                            SELECT resp.* FROM acadgeral__responsavel resp
                            INNER JOIN acadgeral__aluno aluno using (aluno_id)
                            WHERE aluno.pes_id = :pes_id1 OR aluno.pes_id = :pes_id2";
                            $arrParam = ["pes_id1" => $item['pes_id'], "pes_id2" => $arrAnterior['pes_id']];

                            $alunoOuResponsavel = $this->executeQueryWithParam($sql, $arrParam)->fetchAll();

                            if ($alunoOuResponsavel) {
                                foreach ($alunoOuResponsavel as $cadaAlunoOuResponsavel) {
                                    $pesIdResponsavel = $cadaAlunoOuResponsavel['pes_id'];

                                    //a pessoa atual é o responsavel e o cpf é dela
                                    if ($pesIdResponsavel == $item['pes_id']) {
                                        $sql = "
                                        UPDATE pessoa_fisica
                                        SET pes_cpf = null
                                        WHERE pes_id = {$arrAnterior['pes_id']}";

                                        $this->executeQuery($sql);
                                    } else {
                                        //a pessoa atual é o aluno e o cpf não é dela
                                        if ($pesIdResponsavel == $arrAnterior['pes_id']) {
                                            $sql = "
                                            UPDATE pessoa_fisica
                                            SET pes_cpf = null
                                            WHERE pes_id = {$item['pes_id']}";

                                            $this->executeQuery($sql);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $arrAnterior = $item;
                }
            }

            $totalRemovidos += $removidos;
        } while ($removidos > 0);

        return array("totalCpfDuplicados" => $totalCpfDuplicados, "totalRemovidos" => $totalRemovidos);
    }

    public function mergePessoas($arrDados)
    {
        $sql    = "SELECT * FROM pessoa_fisica INNER JOIN pessoa USING(pes_id) WHERE pes_id = :pes_id";
        $result = [];

        foreach ($arrDados as $pesId => $cadaPessoa) {
            $arrPessoaAtual = $this->executeQueryWithParam($sql, ["pes_id" => $pesId])->fetch();

            foreach ($cadaPessoa as $pesIdMerge => $value) {
                if ($pesIdMerge != $pesId) {
                    $arrPessoaAnterior = $this->executeQueryWithParam($sql, ["pes_id" => $pesIdMerge])->fetch();

                    $result[$pesId] = $this->mergePessoa($arrPessoaAnterior, $arrPessoaAtual);
                }
            }
        }

        return $result;
    }

    private function mergePessoa($pessoaAnterior, $pessoaAtual)
    {
        $this->getEm()->getConnection()->connect();

        if (!$this->removerPessoaFisicaDuplicada($pessoaAnterior["pes_id"], $pessoaAtual["pes_id"])) {
            throw new \Exception(
                "Falha ao mesclar dados da pessoa " . $pessoaAtual['pes_nome'] . "." .
                "Verifique se a mesma possui registro duplicado de aluno, candidato, pessoa física."
            );
        }

        $pesNome = $pessoaAnterior['pes_nome'];

        if (strlen($pessoaAtual['pes_nome']) > strlen($pessoaAnterior['pes_nome'])) {
            $pesNome = $pessoaAtual['pes_nome'];
        }

        //não deixa substituir o nulo e manterá sempre a ultima informação, inclusive o pes_id
        $arrDiff   = array_diff($pessoaAtual, $pessoaAnterior);
        $arrUpdate = array_replace_recursive($pessoaAtual, $arrDiff);

        $arrUpdate['pes_nome'] = $pesNome;

        //contato e endereco fazer busca e criar um merge

        $arrDados = [
            "pesId"                     => $arrUpdate['pes_id'],
            "pesCpf"                    => $arrUpdate['pes_cpf'],
            "pesRg"                     => $arrUpdate['pes_rg'],
            "pesRgEmissao"              => $arrUpdate['pes_rg_emissao'],
            "pesSobrenome"              => $arrUpdate['pes_sobrenome'],
            "pesDataNascimento"         => $arrUpdate['pes_data_nascimento'],
            "pesEstadoCivil"            => $arrUpdate['pes_estado_civil'],
            "pesFilhos"                 => $arrUpdate['pes_filhos'],
            "pesNaturalidade"           => $arrUpdate['pes_naturalidade'],
            "pesDocEstrangeiro"         => $arrUpdate['pes_doc_estrangeiro'],
            "pesEmissorRg"              => $arrUpdate['pes_emissor_rg'],
            "pesFalecido"               => $arrUpdate['pes_falecido'],
            "pesProfissao"              => $arrUpdate['pes_profissao'],
            "pesEmpresa"                => $arrUpdate['pes_empresa'],
            "pesEtnia"                  => $arrUpdate['pes_etnia'],
            "pesUfEmissorRg"            => $arrUpdate['pes_uf_emissor_rg'],
            "pesCertidaoCivil"          => $arrUpdate['pes_certidao_civil'],
            "pesCertidaoTipo"           => $arrUpdate['pes_certidao_tipo'],
            "pesCertidaoTermo"          => $arrUpdate['pes_certidao_termo'],
            "pesCertidaoFolha"          => $arrUpdate['pes_certidao_folha'],
            "pesCertidaoLivro"          => $arrUpdate['pes_certidao_livro'],
            "pesCertidaoEmissao"        => $arrUpdate['pes_certidao_emissao'],
            "pesCertidaoCartorio"       => $arrUpdate['pes_certidao_cartorio'],
            "pesCertidaoCartorioCidade" => $arrUpdate['pes_certidao_cartorio_cidade'],
            "pesCertidaoCartorioUf"     => $arrUpdate['pes_certidao_cartorio_uf'],
            "pesCertidaoMatricula"      => $arrUpdate['pes_certidao_matricula'],
            "pesNome"                   => $arrUpdate['pes_nome'],
            "pesTipo"                   => $arrUpdate['pes_tipo'],
            "pesNacionalidade"          => $arrUpdate['pes_nacionalidade'],
            "pesObservacao"             => $arrUpdate['pes_observacao'],
            "pesContatoTelefone"        => $arrUpdate['con_contato_telefone'],
            "pesContatoCelular"         => $arrUpdate['con_contato_celular'],
            "pesContatoEmail"           => $arrUpdate['con_contato_email'],
        ];

        return $this->salvarPessoaFisica($arrDados['pesId'], $arrDados, false, false, true);
    }

    private function compareFirstAndLastName($name, $nameToCompare)
    {
        $nomeAtual    = explode(" ", $name);
        $nomeAnterior = explode(" ", $nameToCompare);

        return (!strcasecmp(array_pop($nomeAnterior), array_pop($nomeAtual)) && !strcasecmp(
                array_shift($nomeAnterior),
                array_shift($nomeAtual)
            ));
    }

    private function trataArrayMesclagem($arr1, $arr2, $tabela, $chave, $arrCondicoes = [], $arrIgnorar = [])
    {
        $idFica       = '';
        $idRemove     = '';
        $arrMesclagem = '';

        foreach ($arrCondicoes as $arrCondicao) {
            $tipo   = $arrCondicao['tipo'];
            $campo  = $arrCondicao['campo'];
            $padrao = $arrCondicao['padrao'];

            if ($tipo == "padrao") {
                if ($arr1[$campo] == $padrao && $arr2[$campo] != $padrao) {
                    $idFica       = $arr1[$chave];
                    $idRemove     = $arr2[$chave];
                    $arrMesclagem = array_merge(array_filter($arr2), array_filter($arr1));
                    break;
                } elseif ($arr2[$campo] == $padrao && $arr1[$campo] != $padrao) {
                    $idFica       = $arr2[$chave];
                    $idRemove     = $arr1[$chave];
                    $arrMesclagem = array_merge(array_filter($arr1), array_filter($arr2));
                    break;
                }
            } elseif ($tipo == "data") {
                $data1 = self::formatDateAmericano($arr1[$campo]);
                $data2 = self::formatDateAmericano($arr2[$campo]);
                $data1 = preg_replace('/[^0-9]/', '', $data1);
                $data2 = preg_replace('/[^0-9]/', '', $data2);

                if ($data1 < $data2) {
                    $idFica       = $arr1[$chave];
                    $idRemove     = $arr2[$chave];
                    $arrMesclagem = array_merge(array_filter($arr2), array_filter($arr1));
                    break;
                } elseif ($data1 > $data2) {
                    $idFica       = $arr2[$chave];
                    $idRemove     = $arr1[$chave];
                    $arrMesclagem = array_merge(array_filter($arr1), array_filter($arr2));
                    break;
                }
            } elseif ($tipo == "numero") {
                $numero1 = $arr1[$campo];
                $numero2 = $arr2[$campo];
                $numero1 = preg_match('/^[0-9]+/', $numero1, $matches) ? $matches[0] : 0;
                $numero2 = preg_match('/^[0-9]+/', $numero2, $matches) ? $matches[0] : 0;

                if ($numero1 < $numero2) {
                    $idFica       = $arr1[$chave];
                    $idRemove     = $arr2[$chave];
                    $arrMesclagem = array_merge(array_filter($arr2), array_filter($arr1));
                    break;
                } elseif ($numero1 > $numero2) {
                    $idFica       = $arr2[$chave];
                    $idRemove     = $arr1[$chave];
                    $arrMesclagem = array_merge(array_filter($arr1), array_filter($arr2));
                    break;
                }
            }
        }

        if (!$arrMesclagem) {
            $idFica       = $arr1[$chave];
            $idRemove     = $arr2[$chave];
            $arrMesclagem = array_merge(array_filter($arr2), array_filter($arr1));
        }

        foreach ($arrIgnorar as $del) {
            foreach ($arrMesclagem as $key => $value) {
                if ($key == $del && !is_numeric($del)) {
                    unset($arrMesclagem[$key]);
                }
            }
        }

        $chaves = implode(',', array_keys($arrMesclagem)) . ',';
        $chaves = preg_replace('/([^,]+)/i', '$1=:$1', $chaves);
        $chaves = trim($chaves, ',');

        $queryUpdate = "UPDATE " . $tabela . " SET " . $chaves . " WHERE " . $chave . " = $idFica;";

        return [$idFica, $idRemove, $queryUpdate, $arrMesclagem];
    }

    private function removerPessoaFisicaDuplicada($pesIdAnterior, $pesIdAtual)
    {
        $sql = "SELECT * FROM pessoa_fisica WHERE pes_id = :pes_id";

        $pessoaAnterior = $this->executeQueryWithParam($sql, ["pes_id" => $pesIdAnterior])->fetch();
        $pessoaAtual    = $this->executeQueryWithParam($sql, ["pes_id" => $pesIdAtual])->fetch();

        if (!$pessoaAnterior) {
            throw new \Exception("Não encontrou a pessoa $pesIdAnterior");
        }

        if (!$pessoaAtual) {
            throw new \Exception("Não encontrou a pessoa $pesIdAtual");
        }

        try {
            $this->getEm()->beginTransaction();

            $sql       = "SELECT * FROM acadgeral__aluno WHERE pes_id = :pes_id";
            $arrAluno1 = $this->executeQueryWithParam($sql, ["pes_id" => $pesIdAtual])->fetch();
            $arrAluno2 = $this->executeQueryWithParam($sql, ["pes_id" => $pesIdAnterior])->fetch();

            if ($arrAluno2 && $arrAluno1) {
                $arrTratado = $this->trataArrayMesclagem(
                    $arrAluno1,
                    $arrAluno2,
                    'acadgeral__aluno',
                    'aluno_id',
                    [
                        ['campo' => 'aluno_data_cadastro', 'tipo' => 'data'],
                        ['campo' => 'aluno_id', 'tipo' => 'numero'],
                    ],
                    ['pes_id']
                );

                $alunoIdFica           = $arrTratado[0];
                $alunoIdRemove         = $arrTratado[1];
                $sqlMesclagem          = $arrTratado[2];
                $arrInscricaoMesclagem = $arrTratado[3];

                $this->executeQueryWithParam($sqlMesclagem, $arrInscricaoMesclagem);

                $sql = "
                UPDATE acadgeral__responsavel SET aluno_id = $alunoIdFica WHERE aluno_id = $alunoIdRemove;
                UPDATE acadgeral__aluno_formacao SET aluno_id = $alunoIdFica WHERE aluno_id = $alunoIdRemove;
                UPDATE acadgeral__aluno_curso SET aluno_id = $alunoIdFica WHERE aluno_id = $alunoIdRemove;
                DELETE FROM acadgeral__aluno WHERE aluno_id = $alunoIdRemove;";

                $sql     = trim($sql);
                $sql     = preg_replace('/\s*--[^\n]*/', '', $sql);
                $queries = explode("\n", $sql);

                foreach ($queries as $sql) {
                    $this->executeQuery(trim($sql));
                }
            }

            $sql           = "SELECT * FROM selecao_inscricao WHERE pes_id = :pes_id";
            $arrInscricao1 = $this->executeQueryWithParam($sql, ["pes_id" => $pesIdAtual])->fetch();
            $arrInscricao2 = $this->executeQueryWithParam($sql, ["pes_id" => $pesIdAnterior])->fetch();

            if ($arrInscricao2 && $arrInscricao1) {
                $arrTratado = $this->trataArrayMesclagem(
                    $arrInscricao1,
                    $arrInscricao2,
                    'selecao_inscricao',
                    'inscricao_id',
                    [
                        ['campo' => 'inscricao_status', 'padrao' => 'Aceita', 'tipo' => 'padrao'],
                        ['campo' => 'inscricao_data', 'tipo' => 'data'],
                        ['campo' => 'inscricao_id', 'tipo' => 'numero']
                    ],
                    ['pes_id']
                );

                $inscricaoIdFica       = $arrTratado[0];
                $inscricaoIdRemove     = $arrTratado[1];
                $sqlMesclagem          = $arrTratado[2];
                $arrInscricaoMesclagem = $arrTratado[3];

                $this->executeQueryWithParam($sqlMesclagem, $arrInscricaoMesclagem);

                $sql = "
                UPDATE inscricao_cursos SET inscricao_id = $inscricaoIdFica WHERE inscricao_id = $inscricaoIdRemove;
                UPDATE prova_inscrito SET inscricao_id = $inscricaoIdFica WHERE inscricao_id = $inscricaoIdRemove;
                DELETE FROM selecao_inscricao WHERE inscricao_id = $inscricaoIdRemove;";

                $sql     = trim($sql);
                $sql     = preg_replace('/\s*--[^\n]*/', '', $sql);
                $queries = explode("\n", $sql);

                foreach ($queries as $sql) {
                    $this->executeQuery(trim($sql));
                }
            }

            $sql = "
            UPDATE acad_curso SET pes_id = $pesIdAtual WHERE pes_id = $pesIdAnterior;
            UPDATE acadgeral__aluno SET pes_id = $pesIdAtual WHERE pes_id = $pesIdAnterior;
            UPDATE acadgeral__aluno SET pes_id_agenciador = $pesIdAtual WHERE pes_id_agenciador = $pesIdAnterior;
            UPDATE acadgeral__aluno_curso SET pes_id_agente = $pesIdAtual WHERE pes_id_agente = $pesIdAnterior;
            UPDATE acadgeral__aluno_curso_historico SET pes_id_agente = $pesIdAtual WHERE pes_id_agente = $pesIdAnterior;
            UPDATE acadgeral__docente SET pes_id = $pesIdAtual WHERE pes_id = $pesIdAnterior;
            UPDATE acadgeral__responsavel SET pes_id = $pesIdAtual WHERE pes_id = $pesIdAnterior;
            UPDATE acesso_pessoas SET pes_fisica = $pesIdAtual WHERE pes_fisica = $pesIdAnterior;
            UPDATE acesso_pessoas SET pes_juridica = $pesIdAtual WHERE pes_juridica = $pesIdAnterior;
            UPDATE arquivo SET pessoa = $pesIdAtual WHERE pessoa = $pesIdAnterior;
            UPDATE atividadegeral__nucleo_responsavel SET pes_id = $pesIdAtual WHERE pes_id = $pesIdAnterior;
            UPDATE atividadegeral__processo__pessoas SET pes_id = $pesIdAtual WHERE pes_id = $pesIdAnterior;
            UPDATE atividadegeral__secretaria SET pes_id = $pesIdAtual WHERE pes_id = $pesIdAnterior;
            UPDATE avaliacao__questionario_respondente SET pes_id = $pesIdAtual WHERE pes_id = $pesIdAnterior;
            UPDATE biblioteca__aquisicao SET pes_id_doador = $pesIdAtual WHERE pes_id_doador = $pesIdAnterior;
            UPDATE biblioteca__aquisicao SET pes_id_fornecedor = $pesIdAtual WHERE pes_id_fornecedor = $pesIdAnterior;
            UPDATE biblioteca__colecao SET pes_id = $pesIdAtual WHERE pes_id = $pesIdAnterior;
            UPDATE biblioteca__empresa SET pes_id = $pesIdAtual WHERE pes_id = $pesIdAnterior;
            UPDATE biblioteca__emprestimo SET pes_id = $pesIdAtual WHERE pes_id = $pesIdAnterior;
            UPDATE biblioteca__pessoa SET pes_id = $pesIdAtual WHERE pes_id = $pesIdAnterior;
            UPDATE biblioteca__titulo SET pes_id_editora = $pesIdAtual WHERE pes_id_editora = $pesIdAnterior;
            UPDATE boleto_conf_conta SET pes_id = $pesIdAtual WHERE pes_id = $pesIdAnterior;
            UPDATE boleto_historico SET pes_id = $pesIdAtual WHERE pes_id = $pesIdAnterior;
            UPDATE contato SET pessoa = $pesIdAtual WHERE pessoa = $pesIdAnterior;
            UPDATE documento__emissao SET pes_id = $pesIdAtual WHERE pes_id = $pesIdAnterior;
            UPDATE documento__pessoa SET pes_id = $pesIdAtual WHERE pes_id = $pesIdAnterior;
            UPDATE endereco SET pes_id = $pesIdAtual WHERE pes_id = $pesIdAnterior;
            UPDATE financeiro__pagamento SET pes_id = $pesIdAtual WHERE pes_id = $pesIdAnterior;
            UPDATE financeiro__titulo SET pes_id = $pesIdAtual WHERE pes_id = $pesIdAnterior;
            UPDATE org__agente_educacional SET pes_id = $pesIdAtual WHERE pes_id = $pesIdAnterior;
            UPDATE org__agente_educacional SET pes_id_indicacao = $pesIdAtual WHERE pes_id_indicacao = $pesIdAnterior;
            UPDATE org__unidade_estudo SET pes_id = $pesIdAtual WHERE pes_id = $pesIdAnterior;
            UPDATE org_campus SET diretoria_pes_id = $pesIdAtual WHERE diretoria_pes_id = $pesIdAnterior;
            UPDATE org_campus SET secretaria_pes_id = $pesIdAtual WHERE secretaria_pes_id = $pesIdAnterior;
            UPDATE org_ies SET diretoria_pes_id = $pesIdAtual WHERE diretoria_pes_id = $pesIdAnterior;
            UPDATE org_ies SET pes_id = $pesIdAtual WHERE pes_id = $pesIdAnterior;
            UPDATE org_ies SET pes_id_mantenedora = $pesIdAtual WHERE pes_id_mantenedora = $pesIdAnterior;
            UPDATE org_ies SET secretaria_pes_id = $pesIdAtual WHERE secretaria_pes_id = $pesIdAnterior;
            -- UPDATE pessoa SET pes_id = $pesIdAtual WHERE pes_id = $pesIdAnterior;
            -- UPDATE pessoa_fisica SET pes_id = $pesIdAtual WHERE pes_id = $pesIdAnterior;
            UPDATE pessoa_juridica SET pes_id = $pesIdAtual WHERE pes_id = $pesIdAnterior;
            UPDATE pessoa_necessidades SET pes_id = $pesIdAtual WHERE pes_id = $pesIdAnterior;
            UPDATE protocolo SET protocolo_solicitante_pes_id = $pesIdAtual WHERE protocolo_solicitante_pes_id = $pesIdAnterior;
            UPDATE protocolo__setor SET pes_id = $pesIdAtual WHERE pes_id = $pesIdAnterior;
            UPDATE selecao_inscricao SET pes_id = $pesIdAtual WHERE pes_id = $pesIdAnterior;
            -- APAGA REGISTRO ANTIGO
            DELETE FROM pessoa_fisica WHERE pes_id = $pesIdAnterior;
            DELETE FROM endereco WHERE pes_id = $pesIdAnterior;
            DELETE FROM contato WHERE pessoa = $pesIdAnterior;
            DELETE FROM pessoa WHERE pes_id = $pesIdAnterior;";

            $sql     = trim($sql);
            $sql     = preg_replace('/\s*--[^\n]*/', '', $sql);
            $queries = explode("\n", $sql);

            foreach ($queries as $sql) {
                $this->executeQuery(trim($sql));
            }

            $this->getEm()->commit();

            return true;
        } catch (\Exception $ex) {
            $this->getEm()->rollback();

            return false;
        }
    }

}
