<?php

namespace Pessoa\Service;

use VersaSpine\Service\AbstractService;

/**
 * Description of Pessoa
 *
 * @author Matheus
 */
class Pessoa extends AbstractService
{
    const PES_TIPO_FISICA                = 'Fisica';
    const PES_TIPO_FISICA_CPF            = 'CPF';
    const PES_TIPO_JURIDICA              = 'Juridica';
    const PES_TIPO_JURIDICA_CNPJ         = 'CNPJ';
    const PES_TIPO_EQUIPARADA            = 'Equiparada';
    const PES_NACIONALIDADE_BRASILEIRO   = 'Brasileiro';
    const PES_NACIONALIDADE_ESTRANGEIRO  = 'Estrangeiro';
    const PES_NACIONALIDADE_NATURALIZADO = 'Naturalizado';

    /**
     * @var null
     */
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    public static function getPesTipo()
    {
        return array(self::PES_TIPO_FISICA, self::PES_TIPO_JURIDICA, self::PES_TIPO_EQUIPARADA);
    }

    public static function getPesNacionalidade()
    {
        return array(
            self::PES_NACIONALIDADE_BRASILEIRO,
            self::PES_NACIONALIDADE_ESTRANGEIRO,
            self::PES_NACIONALIDADE_NATURALIZADO
        );
    }

    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Pessoa\Entity\Pessoa');
    }

    public function pesquisaForJsonPFPJ($params)
    {
        $sql   = '
        SELECT
            -- dados Pessoa
            pessoa.pes_id,
            pessoa.pes_id AS pesId,
            pessoa.pes_tipo,
            pessoa.pes_tipo AS pesTipo,
            pessoa.pes_nome,
            pessoa.pes_nome AS pesNome,
            pessoa.pes_nacionalidade,
            pessoa.pes_nacionalidade pesNacionalidade,
            -- dados PF
            pessoaFisica.pes_cpf,
            pessoaFisica.pes_cpf pesCpf,
            pessoaFisica.pes_doc_estrangeiro,
            pessoaFisica.pes_doc_estrangeiro pesDocEstrangeiro,
            pessoaFisica.pes_rg,
            pessoaFisica.pes_rg pesRg,
            pessoaFisica.pes_sexo,
            pessoaFisica.pes_sexo pesSexo,
            date_format(pessoaFisica.pes_data_nascimento, "%d/%m/%Y") pes_data_nascimento,
            date_format(pessoaFisica.pes_data_nascimento, "%d/%m/%Y") pesDataNascimento,
            -- Dados PJ
            pj.pes_nome_fantasia,
            pj.pes_nome_fantasia AS pesNomeFantasia,
            pj.pes_cnpj,
            pj.pes_cnpj AS pesCnpj,
            -- cidade e estado
            en.end_estado,
            en.end_estado AS endEstado,
            en.end_cidade,
            en.end_cidade AS endCidade
        FROM pessoa pessoa
        LEFT JOIN pessoa_juridica pj USING (pes_id)
        LEFT JOIN pessoa_fisica pessoaFisica USING (pes_id)
        LEFT JOIN endereco en ON en.pes_id=pessoa.pes_id
        WHERE';
        $query = false;

        if ($params['query']) {
            $query = trim($params['query']);
        }

        if ($params['pesId']) {
            $parameters['pes_id'] = trim($params['pesId']);
            $sql .= ' pessoa.pes_id = :pes_id';
        } elseif ($params['pesCpf']) {
            $parameters['pes_cpf'] = trim($params['pesCpf']);
            $sql .= ' pes_cpf like :pes_cpf';
        } elseif ($params['pesCnpj']) {
            $parameters['pes_cnpj'] = trim($params['pesCnpj']);
            $sql .= ' pes_cnpj like :pes_cnpj';
        } elseif ($params['pesDocEstrangeiro']) {
            $parameters['pes_doc_estrangeiro'] = trim($params['pesDocEstrangeiro']);
            $sql .= ' pes_doc_estrangeiro = :pes_doc_estrangeiro';
        } else {
            $parameters = array(
                'pes_nome' => "{$query}%",
                'pes_id'   => ltrim($query, '0') . "%"
            );
            $sql .= '
            (
                pessoa.pes_id*1 LIKE :pes_id OR
                pes_nome LIKE :pes_nome OR
                pes_nome_fantasia LIKE :pes_nome
            )';
        }

        if ($params['groupBy']) {
            $sql .= ' GROUP BY pessoa.pes_id ';
        }

        $sql .= " ORDER BY pes_nome";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function pesquisaForJson($params)
    {
        $serviceAcadperiodoLetivo = new \Matricula\Service\AcadperiodoLetivo($this->getEm());
        $serviceAcessoPessoas     = new \Acesso\Service\AcessoPessoas($this->getEm());
        $objAcadperiodoLetivo     = $serviceAcadperiodoLetivo->buscaPeriodoCorrente();

        $pesNome = false;
        $pesId   = false;

        if ($params['q']) {
            $pesNome = $params['q'];
        } elseif ($params['query']) {
            $pesNome = trim($params['query']);
        }

        if ($params['pesId']) {
            $pesId = trim($params['pesId']);
        }

        $bloquearPesquisaPorPesId    = $params['bloquearPesquisaPorPesId'] ? true : false;
        $bloquearPesquisaPorCPF      = $params['bloquearPesquisaPorCPF'] ? true : false;
        $bloquearPesquisaPorCodAluno = $params['bloquearPesquisaPeloAlunoId'] ? true : false;

        $parameters = array(
            'pes_nome'      => "{$pesNome}%",
            'aluno_id'      => $bloquearPesquisaPorCodAluno ? " " : ltrim($pesNome, '0') . "%",
            'alunocurso_id' => ltrim($pesNome, '0') . "%",
            'pes_id'        => ltrim($pesNome, '0') . "%",
            'pes_cpf'       => preg_replace('/[^0-9]/', '', $pesNome) . "%",
        );

        if ($bloquearPesquisaPorPesId) {
            unset($parameters['pes_id']);
        }

        if ($parameters['pes_cpf'] == '%' || $bloquearPesquisaPorCPF) {
            unset($parameters['pes_cpf']);
        }

        $perId = $objAcadperiodoLetivo ? $objAcadperiodoLetivo->getPerId() : 0;

        $sql = "
        SELECT
            apa.alunoper_id,
            p.pes_id, p.pes_id AS pesId,
            p.pes_nome, p.pes_nome AS pesNome,
            p.pes_observacao AS pesObservacao,
            pf.pes_cpf, pf.pes_cpf AS pesCpf,
            pj.pes_cnpj, pj.pes_cnpj AS pesCnpj,
            aa.aluno_id, aa.aluno_id AS alunoId,
            aac.alunocurso_id, aac.alunocurso_id AS alunocursoId,
            aac.alunocurso_situacao, aac.alunocurso_situacao AS alunocursoSituacao,
            matsit_descricao,
            turma_nome,
            per_id,
            per_nome,
            aac.cursocampus_id, aac.cursocampus_id AS cursocampusId,
            c.curso_id, c.curso_id AS cursoId,
            c.curso_nome, c.curso_nome AS cursoNome,
            (
                SELECT group_concat(rv.pes_id) AS pes_id
                FROM acadgeral__responsavel rv
                WHERE aa.aluno_id = rv.aluno_id AND resp_nivel = 'Financeiro'
            ) responsavel_financeiro_por
            FROM pessoa p
            LEFT JOIN pessoa_fisica pf ON pf.pes_id = p.pes_id
            LEFT JOIN pessoa_juridica pj ON pj.pes_id = p.pes_id
            LEFT JOIN acadgeral__aluno aa ON aa.pes_id=p.pes_id
            LEFT JOIN acadgeral__aluno_curso aac ON aac.aluno_id=aa.aluno_id
            LEFT JOIN campus_curso cc ON cc.cursocampus_id=aac.cursocampus_id
            LEFT JOIN acad_curso c ON c.curso_id=cc.curso_id
            LEFT JOIN (
              SELECT matsit_descricao, alunocurso_id, apt.per_id,apl.per_nome, apt.turma_id, alunoper_id, turma_nome
              FROM acadperiodo__aluno a
              INNER JOIN acadperiodo__turma AS apt ON apt.turma_id=a.turma_id AND apt.per_id = " . $perId . "
              INNER JOIN acadgeral__situacao ON acadgeral__situacao.situacao_id=matsituacao_id
              INNER JOIN acadperiodo__letivo apl ON apl.per_id=apt.per_id
            ) AS apa ON apa.alunocurso_id=aac.alunocurso_id
        WHERE
        (
            " . ($params['filtrarSolicitante'] ? '
                pes_id_agente = :solicitantePesId or
                aac.usuario_cadastro = :usuario or
                pes_id_agenciador = :solicitantePesId AND' : '') . "
            TRIM(LEADING '0' FROM aa.aluno_id) LIKE :aluno_id OR
            TRIM(LEADING '0' FROM aac.alunocurso_id) LIKE :alunocurso_id OR
            " . ($bloquearPesquisaPorPesId ? "" : "TRIM(LEADING '0' FROM p.pes_id) LIKE :pes_id OR") . "
            " . (
            $parameters['pes_cpf'] ? "
            (
              TRIM(REPLACE(REPLACE(REPLACE(REPLACE(pes_cpf,'.',''),',',''),'-',''),'/','')) LIKE :pes_cpf AND
              TRIM(REPLACE(REPLACE(REPLACE(REPLACE(pes_cpf,'.',''),',',''),'-',''),'/','')) <> '' AND
              pes_cpf IS NOT NULL
            ) OR
            (
              TRIM(REPLACE(REPLACE(REPLACE(REPLACE(pes_cnpj,'.',''),',',''),'-',''),'/','')) LIKE :pes_cpf AND
              TRIM(REPLACE(REPLACE(REPLACE(REPLACE(pes_cnpj,'.',''),',',''),'-',''),'/','')) <> '' AND
              pes_cnpj IS NOT NULL
            ) OR" : ""
            ) . "
            pes_nome LIKE :pes_nome OR
            pes_nome_fantasia LIKE :pes_nome
        )";

        $arrCampusCursoPermitido = implode(",", $serviceAcessoPessoas->retornaCampusCursoDoUsuarioLogado(true));

        if ($arrCampusCursoPermitido) {
            $sql .= " AND (cc.cursocampus_id in (" . $arrCampusCursoPermitido . ") OR cc.cursocampus_id IS NULL)";
        }

        if ($pesId) {
            $parameters['pes_id'] = is_array($pesId) ? $pesId : explode(',', $pesId);
            $sql .= ' AND pes_id not in (:pes_id)';
        }

        if ($params['somentePesId']) {
            if (!is_array($params['somentePesId'])) {
                $params['somentePesId'] = [$params['somentePesId']];
            }

            $sql .= ' AND p.pes_id in(' . implode(',', $params['somentePesId']) . ')';
        }

        if (!$params['pesquisarPJ']) {
            $sql .= ' AND (pj.pes_id IS NULL AND pf.pes_id is NOT NULL)';
        }

        if ($params['pessoaOuResponsavel']) {
            $pessoaOuResponsavel = $params['pessoaOuResponsavel'];

            if (is_array($pessoaOuResponsavel)) {
                $pessoaOuResponsavel = implode(',', $pessoaOuResponsavel);
            }

            $sql .= ' AND
            (
                p.pes_id in(' . $pessoaOuResponsavel . ') OR
                p.pes_id in(
                    SELECT av.pes_id
                    FROM acadgeral__aluno av
                    LEFT JOIN acadgeral__responsavel rv USING (aluno_id)
                    WHERE rv.pes_id = p.pes_id and resp_nivel="Financeiro"
                )
            )';
        }

        if ($params['somenteAlunos']) {
            $sql .= ' AND aa.aluno_id IS NOT NULL';
        }

        if ($params['agruparAluno']) {
            $sql .= ' GROUP BY p.pes_id, aa.aluno_id ';
        }

        if ($params['filtrarSolicitante']) {
            $parameters['usuario']          = $serviceAcessoPessoas->retornaUsuarioLogado()->getId();
            $parameters['solicitantePesId'] = $serviceAcessoPessoas->retornaUsuarioLogado()->getPes()->getPes(
            )->getPesId();
        }

        $sql .= " ORDER BY
        IF(TRIM(LEADING '0' FROM aa.aluno_id) LIKE :aluno_id, 0, 1),
        IF(TRIM(LEADING '0' FROM aac.alunocurso_id) LIKE :alunocurso_id, 0, 1),
        " . ($bloquearPesquisaPorPesId ? "" : "IF(TRIM(LEADING '0' FROM p.pes_id) LIKE :pes_id, 0, 1),") . "
        IF(pes_nome COLLATE UTF8_GENERAL_CI LIKE :pes_nome, 0, 1),
        IF(pes_nome_fantasia COLLATE UTF8_GENERAL_CI LIKE :pes_nome, 0, 1),
        aa.aluno_id, aac.alunocurso_id, p.pes_id, pes_nome
        ";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function salvarPessoaComValidacao(array &$arrDados)
    {
        $servicePessoaFisica   = new \Pessoa\Service\PessoaFisica($this->getEm());
        $servicePessoaJuridica = new \Pessoa\Service\PessoaJuridica($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            $arrDadosVerificacao = array();

            if ($arrDados['pesId']) {
                $arrDadosVerificacao = $this->getArray($arrDados['pesId']);
            }

            if ($this->verificacaoPessoaFisica($arrDados['pesTipo'])) {
                if (!$servicePessoaFisica->salvarPessoaFisicaComValidacao($arrDados)) {
                    $this->setLastError($servicePessoaFisica->getLastError());

                    return false;
                }
            } elseif ($arrDadosVerificacao['pesCpf']) {
                if (!$servicePessoaFisica->remover($arrDados['pesId'])) {
                    $this->setLastError($servicePessoaFisica->getLastError());

                    return false;
                }
            }

            if ($this->verificacaoPessoaJuridica($arrDados['pesTipo'])) {
                if (!$servicePessoaJuridica->salvarPessoaJuridicaComValidacao($arrDados)) {
                    $this->setLastError($servicePessoaJuridica->getLastError());

                    return false;
                }
            } elseif ($arrDadosVerificacao['pesCnpj']) {
                if (!$servicePessoaJuridica->remover($arrDados['pesId'])) {
                    $this->setLastError($servicePessoaJuridica->getLastError());

                    return false;
                }
            }

            $this->getEm()->commit();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de pessoa!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        if (!$arrParam['pesNome']) {
            $this->setLastError('Por favor informe o "nome"!');

            return false;
        }

        if ($arrParam['pesNome'] && strlen($arrParam['pesNome']) > 255) {
            $this->setLastError('O campo "nome" deve conter no máximo 255 caracteres!');

            return false;
        }

        if ($arrParam['pesTipo'] && !in_array($arrParam['pesTipo'], self::getPesTipo())) {
            $errors[] = 'Por favor selecione um valor válido para o "tipo de pessoa"!';
        }

        if ($arrParam['pesNacionalidade'] && !in_array($arrParam['pesNacionalidade'], self::getPesNacionalidade())) {
            $errors[] = 'Por favor selecione um valor válido para a "nacionalidade"!';
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM pessoa";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($pesId)
    {
        /** @var \Pessoa\Entity\Pessoa $objPessoa */
        $objPessoa = $this->getRepository()->find($pesId);

        if (!$objPessoa) {
            $this->setLastError('Pessoa não encontrada!');

            return array();
        }

        $arrDados = array();

        $servicePessoaFisica   = new \Pessoa\Service\PessoaFisica($this->getEm());
        $servicePessoaJuridica = new \Pessoa\Service\PessoaJuridica($this->getEm());
        $serviceAcessoPessoas  = new \Acesso\Service\AcessoPessoas($this->getEm());

        try {
            $arrDados = $objPessoa->toArray();

            if ($this->verificacaoPessoaFisica($objPessoa->getPesTipo())) {
                $arrDados = array_merge($arrDados, $servicePessoaFisica->getArray($pesId));
            }

            if ($this->verificacaoPessoaJuridica($objPessoa->getPesTipo())) {
                $arrDados = array_merge($arrDados, $servicePessoaJuridica->getArray($pesId));
            }

            $arrAcesso          = $serviceAcessoPessoas->buscaUsuario($pesId);
            $arrAcesso          = $arrAcesso ? $arrAcesso[0] : array();
            $arrDados['acesso'] = $arrAcesso;

            /** INFORMAÇÕES PARA GERAÇÃO DE REMESSA */
            $arrDados['pesTipoNumero'] = $arrDados['pesTipo'] == "Juridica" ? 2 : 1;

            $arrDados['cpfCnpj'] = $arrDados['pesCnpj'] ? $arrDados['pesCnpj'] : $arrDados['pesCpf'];
            $arrDados['cpfCnpj'] = str_replace(['.', '/', '-'], '', $arrDados['cpfCnpj']);
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    public function remover($param)
    {
        if (!$param['pesId']) {
            $this->setLastError('Para remover um registro de pessoa é necessário informar o código . ');

            return false;
        }

        try {
            /** @var $objPessoa \Pessoa\Entity\Pessoa */
            $objPessoa = $this->getRepository()->find($param['pesId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objPessoa);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de pessoa . ');

            return false;
        }

        return true;
    }

    public function buscaContatoPessoa($id, $tipo)
    {
        $sql = "SELECT "
            . "contato.con_contato, tipo_contato.tip_contato_nome "
            . "FROM "
            . "contato INNER JOIN "
            . "tipo_contato "
            . "ON "
            . "contato.tipo_contato = tipo_contato.tip_contato_id "
            . "WHERE "
            . "contato.pessoa = '{
                $id}'";
        //                pre($sql);
        $result = $this->executeQuery($sql)->fetchAll();
        foreach ($result as $cont) {
            //            pre($cont['tip_contato_nome'].$tipo);
            if ($cont['tip_contato_nome'] == $tipo) {
                $contato[] = $cont['con_contato'];
            }
        }

        return $contato;
    }

    /**
     * Busca dados da pessoa, tanto física como jurídica
     * $pesTipo = 'f' para pessoas físicas e 'j' para juridicas
     * */
    public function buscaDadosPessoa($id, $pesTipo = 'f', $end = false, $cont = false)
    {
        $dadosPessoa = array();
        $pes         = $this->getRepository('Pessoa\Entity\Pessoa')->findOneBy(['pesId' => $id])->toArray();
        if ($pes) {
            if ($pesTipo = 'f' && $pes['pesTipo'] == 'Fisica') {
                $dadosPessoa                     = $this->getRepository("Pessoa\Entity\PessoaFisica")->findOneBy(
                    ['pes' => $id]
                )->toArray();
                $dadosPessoa['pesNome']          = $pes['pesNome'];
                $dadosPessoa['pesNacionalidade'] = $pes['pesNacionalidade'];
            } else {
                if ($pesTipo = 'j' && $pes['pesTipo'] == 'Juridica') {
                    $dadosPessoa                     = $this->getRepository("Pessoa\Entity\PessoaJuridica")->findOneBy(
                        ['pes' => $id]
                    )->toArray();
                    $dadosPessoa['pesNome']          = $pes['pesNome'];
                    $dadosPessoa['pesNacionalidade'] = $pes['pesNacionalidade'];
                }
            }

            if ($end == true) {
                $repositoryEndereco = $this->getEm()->getRepository('Pessoa\Entity\Endereco');
                /** @var \Pessoa\Entity\Endereco $endereco */
                $endereco = $repositoryEndereco->findOneBy(array('pes' => $dadosPessoa['pes']));

                if ($endereco) {
                    $dadosPessoa['endCep']         = $endereco->getEndCep();
                    $dadosPessoa['endLogradouro']  = $endereco->getEndLogradouro();
                    $dadosPessoa['endNumero']      = $endereco->getEndNumero();
                    $dadosPessoa['endCidade']      = $endereco->getEndCidade();
                    $dadosPessoa['endEstadoNome']  = $endereco->getEndEstado();
                    $dadosPessoa['endComplemento'] = $endereco->getEndComplemento();
                    $dadosPessoa['endBairro']      = $endereco->getEndBairro();
                }
            }

            if ($cont == true) {
                $repositoryContato = $this->getEm()->getRepository('Pessoa\Entity\Contato');
                $contatos          = $repositoryContato->findBy(['pessoa' => $dadosPessoa['pes']]);

                /** @var \Pessoa\Entity\Contato $contato */
                foreach ($contatos as $contato) {
                    $dadosPessoa[$contato->getTipoContato()->getTipContatoNome()][] = [
                        'conId'       => $contato->getConId(),
                        'conContato'  => $contato->getConContato(),
                        'tipoContato' => $contato->getTipoContato()->getTipContatoId(),
                    ];
                }
            }

            return $dadosPessoa;
        }

        return null;
    }

    /**
     * Função para enviar email
     * */
    public function enviaEmail($mensagemHtml, $email, $remetente = "", $assunto = "")
    {
        $message = new \Zend\Mail\Message();

        //TODO: inserir dados de contato no fim da frase dinamicamente
        $textoHtml = "<div style='width:820px; margin:10px 0 0 5px;padding:0 0 50px 0;background:#00689b;'><div><div style='width:120px;text-align:center;float:left;'><img style='width:100px;' src='img/email.png'></div><div style='width:120px;text-align:center;float:left;'><i class='fa fa-envelope fa-4'></i></div><div style='float:left;width:700px;'><h1 style='font-family:arial;color:white;font-size:18pt;text-align:center;margin:15px 0 0 0;'>$remetente</h1><h1 style='font-family:arial;color:white;font-size:12pt;text-align:center;margin:4px 0 15px 0;'>Chegou uma nova mensagem para você.</h1></div><span style='clear:both;height:0px;font-size:0px;display:block;'></span></div><div style='padding:10px;margin:1px;text-align:left;background:#FFF;-webkit-border-radius: 20px;-moz-border-radius: 20px;border-radius: 20px;'><br>$mensagemHtml<br><br>Qualquer dúvida entre em contato.</div></div>";

        $html       = new \Zend\Mime\Part($textoHtml);
        $html->type = "text/html";

        $body = new \Zend\Mime\Message;
        $body->setParts(array($html));

        $assunto = $assunto ? $assunto : '-';

        $message->setTo($email);
        $message->setFrom("uni@versatecnologia.com.br");
        $message->setBody($body);
        $message->setSubject("$assunto");
        $message->setEncoding("UTF-8");

        $transport = new \Zend\Mail\Transport\Smtp();
        $options   = new \Zend\Mail\Transport\SmtpOptions(
            array(
                'name'              => 'Universa',
                'host'              => 'mail.versatecnologia.com.br',
                'connection_class'  => 'login',
                'connection_config' => array(
                    'username' => 'uni@versatecnologia.com.br',
                    'password' => 'NXf4TW4C',
                    'port'     => 587,
                    'from'     => 'uni@versatecnologia.com.br'
                )
            )
        );
        $transport->setOptions($options);
        $transport->send($message);
    }

    /**
     * Pesquisa pessoa pelo $pesId caso não exista cria registro
     * @param array $arrDados
     * @return \Pessoa\Entity\Pessoa
     */
    public function salvarPessoa(&$arrDados = array())
    {
        $objPessoa = null;

        try {
            if ($arrDados['pesId']) {
                $objPessoa = $this->getRepository()->find($arrDados['pesId']);
            }

            if (!$objPessoa) {
                $objPessoa = new \Pessoa\Entity\Pessoa($arrDados);
            }

            $pesNacionalidade = self::PES_NACIONALIDADE_BRASILEIRO;
            $pesTipo          = self::PES_TIPO_FISICA;

            $pesNome = $objPessoa->getPesNome();

            if ($arrDados['pesNacionalidade']) {
                $pesNacionalidade = $arrDados['pesNacionalidade'];
            }

            if ($arrDados['pesTipo']) {
                $pesTipo = $arrDados['pesTipo'];
            }

            if ($arrDados['pesNome']) {
                $pesNome = $arrDados['pesNome'];
            }

            $pesNome = trim(preg_replace('/\s+/', ' ', $pesNome));

            $objPessoa
                ->setPesNacionalidade($pesNacionalidade)
                ->setPesTipo($pesTipo)
                ->setPesObservacao($arrDados['pesObservacao'])
                ->setConContatoEmail($arrDados['conContatoEmail'])
                ->setConContatoCelular($arrDados['conContatoCelular'])
                ->setConContatoTelefone($arrDados['conContatoTelefone'])
                ->setPesNome($pesNome);

            $this->getEm()->persist($objPessoa);
            $this->getEm()->flush($objPessoa);

            $arrDados['pesId'] = $objPessoa->getPesId();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao salvar registro de pessoa. ' . $ex->getMessage());

            return false;
        }

        return $objPessoa;
    }

    public function getArrSelect2PesTipo($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getPesTipo());
    }

    public function getArrSelect2PesNacionalidade($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getPesNacionalidade());
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        //Previne loop ao chamar dependencias de view de PF e PJ
        if ($view->getVariable("arrPesTipo")) {
            return false;
        }

        $servicePessoaFisica   = new \Pessoa\Service\PessoaFisica($this->getEm());
        $servicePessoaJuridica = new \Pessoa\Service\PessoaJuridica($this->getEm());

        $serviceCidade = new \Pessoa\Service\Cidade($this->getEm());

        $arrEstados = $serviceCidade->getEstadoArrSelect2();

        $view->setVariable("arrEstados", $arrEstados);
        $view->setVariable("arrPesTipo", $this->getArrSelect2PesTipo());
        $view->setVariable("arrPesNacionalidade", $this->getArrSelect2PesNacionalidade());

        $servicePessoaFisica->setarDependenciasView($view);
        $servicePessoaJuridica->setarDependenciasView($view);
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('pesId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Pessoa\Entity\Pessoa */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getPesId(),
                $params['value'] => $objEntity->getPesNome()
            );
        }

        return $arrEntitiesArr;
    }

    function verificacaoPessoaFisica($pesTipo)
    {
        return (!$pesTipo || $pesTipo == self::PES_TIPO_FISICA || $pesTipo == self::PES_TIPO_EQUIPARADA);
    }

    function verificacaoPessoaJuridica($pesTipo)
    {
        return (!$pesTipo || $pesTipo == self::PES_TIPO_JURIDICA || $pesTipo == self::PES_TIPO_EQUIPARADA);
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $servicePessoaFisica   = new \Pessoa\Service\PessoaFisica($this->getEm());
        $servicePessoaJuridica = new \Pessoa\Service\PessoaJuridica($this->getEm());

        $servicePessoaFisica->formataDadosPost($arrDados);
        $servicePessoaJuridica->formataDadosPost($arrDados);

        return $arrDados;
    }

    public function formatarEndereco($arrDados)
    {
        $endereco = implode(', ', $this->formatarEnderecoDuasLinhas($arrDados));

        return $endereco;
    }

    public function formatarEnderecoDuasLinhas($arrDados)
    {
        $endereco1 = '';
        $endereco2 = '';

        $endereco1 .= $arrDados['endLogradouro'];
        $endereco1 .= ', ' . $arrDados['endNumero'];
        $endereco1 .= ', ' . $arrDados['endComplemento'];
        $endereco1 .= ', ' . $arrDados['endBairro'];
        $endereco1 = preg_replace('/([\s,-] )([\s,-] ){1,}/', '$1', $endereco1);

        $endereco2 .= $arrDados['endCidade'];
        $endereco2 .= ' - ' . $arrDados['endEstado'];
        $endereco2 .= ' - ' . $arrDados['endCep'];
        $endereco2 = preg_replace('/([\s,-] )([\s,-] ){1,}/', '$1', $endereco2);

        return array($endereco1, $endereco2);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getDescricao($id)
    {
        if (!$id) {
            return false;
        }

        if (!is_array($id)) {
            $id = array($id);
        }

        $id = implode($id, ',');

        $sql = "
        SELECT
            group_concat(pes_nome SEPARATOR ', ') AS pessoa
        FROM pessoa
        WHERE pes_id IN(" . $id . ")";

        $result = $this->executeQueryWithParam($sql)->fetch();

        return $result['pessoa'];
    }

    public function alterarObservacoes($arrDados)
    {
        if (!$arrDados['pesId']) {
            $this->setLastError('Registro de pessoa não encontrado!');

            return false;
        }

        $objPessoa = $this->getRepository()->find($arrDados['pesId']);

        if (!$objPessoa) {
            $this->setLastError('Registro de pessoa não encontrado!');

            return false;
        }

        $arrPessoa                  = $objPessoa->toArray();
        $arrPessoa['pesObservacao'] = $arrDados['pesObservacao'];

        return $this->salvarPessoa($arrPessoa);
    }

    /**
     * Retorna pes_id se o cnpj ou cpf existir na base
     * @param            $pesCpfCnpj
     * @return int|null
     */
    public function buscaPessoaPorCPFOuCNPJ($pesCpfCnpj)
    {
        $sql = "
        SELECT pes_id
        FROM pessoa
        LEFT JOIN pessoa_juridica USING(pes_id)
        LEFT JOIN pessoa_fisica USING(pes_id)
        WHERE
        TRIM(REPLACE(REPLACE(REPLACE(REPLACE(coalesce(pes_cnpj),'.',''),',',''),'-',''),'/','')) LIKE :pesCpfCnpj OR
        TRIM(REPLACE(REPLACE(REPLACE(REPLACE(coalesce(pes_cpf),'.',''),',',''),'-',''),'/','')) LIKE :pesCpfCnpj
        ";

        $parameters = array('pesCpfCnpj' => preg_replace('/[^0-9]/', '', $pesCpfCnpj));

        $result = $this->executeQueryWithParam($sql, $parameters)->fetch();

        return $result['pes_id'];
    }

    public function getInfoVestibular($pesId)
    {
        if (!$pesId) {
            return false;
        }

        $params = array('pesId' => $pesId);
        $query  = "SELECT
                      *
                    FROM inscricao_cursos
                      LEFT JOIN
                      selecao_inscricao USING(inscricao_id)
                      LEFT JOIN
                      pessoa USING(pes_id)
                      LEFT JOIN
                      selecao_cursos USING(selcursos_id)
                      LEFT JOIN
                      selecao_tipo_edicao USING (seletipoedicao_id)
                      LEFT JOIN
                      selecao_tipo USING (tiposel_id)
                    WHERE
                      pes_id = :pesId";

        $result = $this->executeQueryWithParam($query, $params)->fetch();

        if ($result) {
            return $result;
        }

        return false;
    }

    public function retornaPessoaLogada($usuario = null)
    {
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());

        if ($objAcessoPessoas = $serviceAcessoPessoas->retornaUsuarioLogado($usuario)) {
            $arrPessoa  = $objAcessoPessoas->getPes()->toArray();
            $arrUsuario = $objAcessoPessoas->toArray();
            unset($arrUsuario['senha']);

            return array_merge($arrUsuario, $arrPessoa);
        }

        return array();
    }
}
