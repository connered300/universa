<?php

namespace Pessoa\ViewHelper;

class FormHelper extends \Zend\Form\View\Helper\Form
{

    /**
     * Retorna o próprio objeto
     * @return \Sistema\ViewHelper\SisConfig
     */
    public function __invoke()
    {
        return $this;
    }

    /**
     * Abre uma tag div de acordo com os atributos
     * @param array $atributos
     * @return string
     */
    public function openDiv($atributos = array()){
        //a funcao createattributes já faz a verificação de injection nela
        $html_atributos = $this->createAttributesString($atributos);
        return "<div $html_atributos>&#013;";
    }

    /**
     * Fecha uma tag div
     * @return string
     */
    public function closeDiv(){
        return "</div>";
    }

    public function criarCampo($name, $dadosCampo = array())
    {
        if (isset($dadosCampo['type'])) {
            $type = strtolower($dadosCampo['type']);

            switch ($type) {
                case "checkbox":
                    $element = new \Zend\Form\Element\Checkbox($name);
                    break;
                case "text":
                    $element = new \Zend\Form\Element\Text($name);
                    break;
                case "textarea":
                    $element = new \Zend\Form\Element\Textarea($name);
                    break;
                case "number":
                    $element = new \Zend\Form\Element\Number($name);
                    break;
                case "hidden":
                    $element = new \Zend\Form\Element\Hidden($name);
                    break;
                case "url":
                    $element = new \Zend\Form\Element\Url($name);
                    break;
                case "file":
                    $element = new \Zend\Form\Element\File($name);
                    break;
                case "csrf":
                    $element = new \Zend\Form\Element\Csrf($name);
                    break;
                case "password":
                    $element = new \Zend\Form\Element\Password($name);
                    break;
                case "button":
                    $element = new \Zend\Form\Element\Button($name);
                    break;
                default:
                    throw new \Zend\Form\Exception\InvalidElementException("Não existe o type informado $type.");
                    break;
            }
        } elseif (isset($dadosCampo["options"])) {
            $element = new \Zend\Form\Element\Select($name);
        } else {
            throw new \Zend\Form\Exception\InvalidElementException("Ocorreu um errro ao renderizar o campo $name.");
        }

        $element->setAttributes($dadosCampo);

        $label = $dadosCampo['label'];

        if (!isset($dadosCampo['label'])) {
            $label = ucfirst($name);
        }

        $fieldString = "";

        $element->setLabel($label);
        $element->setLabelAttributes(["for" => $label]);

        //não criar a tag de label, alguns elementos exigem ter o label, mas em alguns casos não preciso mostrá-los
        if(!isset($dadosCampo['no-label'])){
            $fieldString = $this->getView()->formLabel($element);
        }

        $fieldString .= $this->getView()->formElement($element);

        //garantindo que o campo não vá para o setatributte, tendo ou não
        unset($dadosCampo['label']);

        return  $fieldString;
    }




}
