<?php

namespace Pessoa\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * Zona
 *
 * @ORM\Table(name="zona")
 * @ORM\Entity
 * @LG\LG(id="zonaId",label="ZonaNome")
 * @Jarvis\Jarvis(title="Listagem de zona",icon="fa fa-table")
 */
class Zona
{
    /**
     * @var integer
     *
     * @ORM\Column(name="zona_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="zona_id")
     * @LG\Labels\Attributes(text="código")
     * @LG\Querys\Conditions(type="=")
     */
    private $zonaId;

    /**
     * @var \Pessoa\Entity\Estado
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\Estado")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="est_id", referencedColumnName="est_id")
     * })
     */
    private $est;

    /**
     * @var string
     *
     * @ORM\Column(name="zona_nome", type="string", nullable=true, length=80)
     * @LG\Labels\Property(name="zona_nome")
     * @LG\Labels\Attributes(text="nome")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $zonaNome;

    /**
     * @var string
     *
     * @ORM\Column(name="zona_ddd", type="string", nullable=true, length=3)
     * @LG\Labels\Property(name="zona_ddd")
     * @LG\Labels\Attributes(text="ddd")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $zonaDdd;

    /**
     * @return integer
     */
    public function getZonaId()
    {
        return $this->zonaId;
    }

    /**
     * @param integer $zonaId
     * @return Zona
     */
    public function setZonaId($zonaId)
    {
        $this->zonaId = $zonaId;

        return $this;
    }

    /**
     * @return \Pessoa\Entity\Estado
     */
    public function getEst()
    {
        return $this->est;
    }

    /**
     * @param \Pessoa\Entity\Estado $est
     * @return Zona
     */
    public function setEst($est)
    {
        $this->est = $est;

        return $this;
    }

    /**
     * @return string
     */
    public function getZonaNome()
    {
        return $this->zonaNome;
    }

    /**
     * @param string $zonaNome
     * @return Zona
     */
    public function setZonaNome($zonaNome)
    {
        $this->zonaNome = $zonaNome;

        return $this;
    }

    /**
     * @return string
     */
    public function getZonaDdd()
    {
        return $this->zonaDdd;
    }

    /**
     * @param string $zonaDdd
     * @return Zona
     */
    public function setZonaDdd($zonaDdd)
    {
        $this->zonaDdd = $zonaDdd;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'zonaId'   => $this->getZonaId(),
            'est'      => $this->getEst(),
            'zonaNome' => $this->getZonaNome(),
            'zonaDdd'  => $this->getZonaDdd(),
        );

        $array['est'] = $this->getEst() ? $this->getEst()->getEstId() : null;

        return $array;
    }
}
