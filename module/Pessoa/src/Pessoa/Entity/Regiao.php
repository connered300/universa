<?php

namespace Pessoa\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * Regiao
 *
 * @ORM\Table(name="regiao")
 * @ORM\Entity
 * @LG\LG(id="regiaoId",label="RegiaoNome")
 * @Jarvis\Jarvis(title="Listagem de região",icon="fa fa-table")
 */
class Regiao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="regiao_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="regiao_id")
     * @LG\Labels\Attributes(text="código")
     * @LG\Querys\Conditions(type="=")
     */
    private $regiaoId;

    /**
     * @var string
     *
     * @ORM\Column(name="regiao_nome", type="string", nullable=false, length=128)
     * @LG\Labels\Property(name="regiao_nome")
     * @LG\Labels\Attributes(text="nome")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $regiaoNome;

    /**
     * @return integer
     */
    public function getRegiaoId()
    {
        return $this->regiaoId;
    }

    /**
     * @param integer $regiaoId
     * @return Regiao
     */
    public function setRegiaoId($regiaoId)
    {
        $this->regiaoId = $regiaoId;

        return $this;
    }

    /**
     * @return string
     */
    public function getRegiaoNome()
    {
        return $this->regiaoNome;
    }

    /**
     * @param string $regiaoNome
     * @return Regiao
     */
    public function setRegiaoNome($regiaoNome)
    {
        $this->regiaoNome = $regiaoNome;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'regiaoId'   => $this->getRegiaoId(),
            'regiaoNome' => $this->getRegiaoNome(),
        );

        return $array;
    }
}
