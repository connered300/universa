<?php

namespace Pessoa\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * Endereco
 *
 * @ORM\Table(name="endereco")
 * @ORM\Entity
 * @LG\LG(id="endId",label="PesId")
 * @Jarvis\Jarvis(title="Listagem de endereço",icon="fa fa-table")
 */
class Endereco
{
    /**
     * @var integer
     *
     * @ORM\Column(name="end_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="end_id")
     * @LG\Labels\Attributes(text="código endereço")
     * @LG\Querys\Conditions(type="=")
     */
    private $endId;

    /**
     * @var \Pessoa\Entity\Pessoa
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     */
    private $pes;

    /**
     * @var \Pessoa\Entity\TipoEndereco
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\TipoEndereco")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipo_endereco", referencedColumnName="tipo_endereco_id")
     * })
     */
    private $tipoEndereco;

    /**
     * @var string
     *
     * @ORM\Column(name="end_pais", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="end_pais")
     * @LG\Labels\Attributes(text="país")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $endPais;

    /**
     * @var string
     *
     * @ORM\Column(name="end_estado", type="string", nullable=false, length=45)
     * @LG\Labels\Property(name="end_estado")
     * @LG\Labels\Attributes(text="estado")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $endEstado;

    /**
     * @var string
     *
     * @ORM\Column(name="end_tipo_end", type="string", nullable=true, length=11)
     * @LG\Labels\Property(name="end_tipo_end")
     * @LG\Labels\Attributes(text="tipo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $endTipoEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="end_bairro", type="string", nullable=true, length=255)
     * @LG\Labels\Property(name="end_bairro")
     * @LG\Labels\Attributes(text="bairro")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $endBairro;

    /**
     * @var string
     *
     * @ORM\Column(name="end_cidade", type="string", nullable=false, length=255)
     * @LG\Labels\Property(name="end_cidade")
     * @LG\Labels\Attributes(text="cidade")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $endCidade;

    /**
     * @var string
     *
     * @ORM\Column(name="end_cep", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="end_cep")
     * @LG\Labels\Attributes(text="CEP")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $endCep;

    /**
     * @var string
     *
     * @ORM\Column(name="end_logradouro", type="string", nullable=true, length=255)
     * @LG\Labels\Property(name="end_logradouro")
     * @LG\Labels\Attributes(text="logradouro")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $endLogradouro;

    /**
     * @var string
     *
     * @ORM\Column(name="end_tipo_logradouro", type="string", nullable=true, length=255)
     * @LG\Labels\Property(name="end_tipo_logradouro")
     * @LG\Labels\Attributes(text="logradouro tipo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $endTipoLogradouro;

    /**
     * @var string
     *
     * @ORM\Column(name="end_numero", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="end_numero")
     * @LG\Labels\Attributes(text="número")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $endNumero;

    /**
     * @var string
     *
     * @ORM\Column(name="end_complemento", type="string", nullable=true, length=128)
     * @LG\Labels\Property(name="end_complemento")
     * @LG\Labels\Attributes(text="complemento")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $endComplemento;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="end_data", type="datetime", nullable=false)
     * @LG\Labels\Property(name="end_data")
     * @LG\Labels\Attributes(text="data")
     * @LG\Querys\Conditions(type="=")
     */
    private $endData;

    /**
     * @return integer
     */
    public function getEndId()
    {
        return $this->endId;
    }

    /**
     * @param integer $endId
     * @return Endereco
     */
    public function setEndId($endId)
    {
        $this->endId = $endId;

        return $this;
    }

    /**
     * @return \Pessoa\Entity\Pessoa
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @param \Pessoa\Entity\Pessoa $pes
     * @return Endereco
     */
    public function setPes($pes)
    {
        $this->pes = $pes;

        return $this;
    }

    /**
     * @return \Pessoa\Entity\TipoEndereco
     */
    public function getTipoEndereco()
    {
        return $this->tipoEndereco;
    }

    /**
     * @param \Pessoa\Entity\TipoEndereco $tipoEndereco
     * @return Endereco
     */
    public function setTipoEndereco($tipoEndereco)
    {
        $this->tipoEndereco = $tipoEndereco;

        return $this;
    }

    /**
     * @return string
     */
    public function getEndPais()
    {
        return $this->endPais ? $this->endPais : '';
    }

    /**
     * @param string $endPais
     * @return Endereco
     */
    public function setEndPais($endPais)
    {
        $this->endPais = $endPais;

        return $this;
    }

    /**
     * @return string
     */
    public function getEndEstado()
    {
        return $this->endEstado ? $this->endEstado : '';
    }

    /**
     * @param string $endEstado
     * @return Endereco
     */
    public function setEndEstado($endEstado)
    {
        $this->endEstado = $endEstado;

        return $this;
    }

    /**
     * @return string
     */
    public function getEndTipoEnd()
    {
        return $this->endTipoEnd ? $this->endTipoEnd : '';
    }

    /**
     * @param string $endTipoEnd
     * @return Endereco
     */
    public function setEndTipoEnd($endTipoEnd)
    {
        $this->endTipoEnd = $endTipoEnd;

        return $this;
    }

    /**
     * @return string
     */
    public function getEndBairro()
    {
        return $this->endBairro ? $this->endBairro : '';
    }

    /**
     * @param string $endBairro
     * @return Endereco
     */
    public function setEndBairro($endBairro)
    {
        $this->endBairro = $endBairro;

        return $this;
    }

    /**
     * @return string
     */
    public function getEndCidade()
    {
        return $this->endCidade ? $this->endCidade : '';
    }

    /**
     * @param string $endCidade
     * @return Endereco
     */
    public function setEndCidade($endCidade)
    {
        $this->endCidade = $endCidade;

        return $this;
    }

    /**
     * @return string
     */
    public function getEndCep()
    {
        return $this->endCep ? $this->endCep : '';
    }

    /**
     * @param string $endCep
     * @return Endereco
     */
    public function setEndCep($endCep)
    {
        $this->endCep = $endCep;

        return $this;
    }

    /**
     * @return string
     */
    public function getEndLogradouro()
    {
        return $this->endLogradouro ? $this->endLogradouro : '';
    }

    /**
     * @param string $endLogradouro
     * @return Endereco
     */
    public function setEndLogradouro($endLogradouro)
    {
        $this->endLogradouro = $endLogradouro;

        return $this;
    }

    /**
     * @return string
     */
    public function getEndTipoLogradouro()
    {
        return $this->endTipoLogradouro ? $this->endTipoLogradouro : '';
    }

    /**
     * @param string $endTipoLogradouro
     * @return Endereco
     */
    public function setEndTipoLogradouro($endTipoLogradouro)
    {
        $this->endTipoLogradouro = $endTipoLogradouro;

        return $this;
    }

    /**
     * @return string
     */
    public function getEndNumero()
    {
        return $this->endNumero ? $this->endNumero : '';
    }

    /**
     * @param string $endNumero
     * @return Endereco
     */
    public function setEndNumero($endNumero)
    {
        $this->endNumero = $endNumero;

        return $this;
    }

    /**
     * @return string
     */
    public function getEndComplemento()
    {
        return $this->endComplemento ? $this->endComplemento : '';
    }

    /**
     * @param string $endComplemento
     * @return Endereco
     */
    public function setEndComplemento($endComplemento)
    {
        $this->endComplemento = $endComplemento;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getEndData($format = false)
    {
        $endData = $this->endData;

        if ($format && $endData) {
            $endData = $endData->format('d/m/Y H:i:s');
        }

        return $endData;
    }

    /**
     * @param \Datetime $endData
     * @return Endereco
     */
    public function setEndData($endData)
    {
        if ($endData) {
            if (is_string($endData)) {
                $endData = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $endData
                );
                $endData = new \Datetime($endData);
            }
        } else {
            $endData = null;
        }

        $this->endData = $endData;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'endId'             => $this->getEndId(),
            'pes'               => $this->getPes(),
            'tipoEndereco'      => $this->getTipoEndereco(),
            'endPais'           => $this->getEndPais(),
            'endEstado'         => $this->getEndEstado(),
            'endTipoEnd'        => $this->getEndTipoEnd(),
            'endBairro'         => $this->getEndBairro(),
            'endCidade'         => $this->getEndCidade(),
            'endCep'            => $this->getEndCep(),
            'endLogradouro'     => $this->getEndLogradouro(),
            'endTipoLogradouro' => $this->getEndTipoLogradouro(),
            'endNumero'         => $this->getEndNumero(),
            'endComplemento'    => $this->getEndComplemento(),
            'endData'           => $this->getEndData(true),
        );

        $array['pes']          = $this->getPes() ? $this->getPes()->getPesId() : null;
        $array['tipoEndereco'] = $this->getTipoEndereco() ? $this->getTipoEndereco()->getTipoEnderecoId() : null;

        return $array;
    }

    /**
     * @return array
     */
    public function toArrayAux()
    {
        return array(
            'endId[]'             => $this->getEndId(),
            'endPais[]'           => $this->getEndPais(),
            'endEstado[]'         => $this->getEndEstado(),
            'endCidade[]'         => $this->getEndCidade(),
            'endBairro[]'         => $this->getEndBairro(),
            'endLogradouro[]'     => $this->getEndLogradouro(),
            'endTipoLogradouro[]' => $this->getEndTipoLogradouro(),
            'endCep[]'            => $this->getEndCep(),
            'endNumero[]'         => $this->getEndNumero(),
            'endComplemento[]'    => $this->getEndComplemento(),
            'endData[]'           => $this->getEndData(),
            'pes[]'               => $this->getPes()->getPesId(),
            'tipoEndereco[]'      => $this->getTipoEndereco()->getTipoEnderecoId(),
        );
    }
}
