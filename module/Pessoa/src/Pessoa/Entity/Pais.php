<?php

namespace Pessoa\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * Pais
 *
 * @ORM\Table(name="pais")
 * @ORM\Entity
 * @LG\LG(id="paisId",label="PaisNome")
 * @Jarvis\Jarvis(title="Listagem de país",icon="fa fa-table")
 */
class Pais
{
    /**
     * @var integer
     *
     * @ORM\Column(name="pais_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="pais_id")
     * @LG\Labels\Attributes(text="código")
     * @LG\Querys\Conditions(type="=")
     */
    private $paisId;

    /**
     * @var string
     *
     * @ORM\Column(name="pais_nome", type="string", nullable=false, length=45)
     * @LG\Labels\Property(name="pais_nome")
     * @LG\Labels\Attributes(text="nome")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $paisNome;

    /**
     * @var string
     *
     * @ORM\Column(name="pais_sigla", type="string", nullable=false, length=5)
     * @LG\Labels\Property(name="pais_sigla")
     * @LG\Labels\Attributes(text="sigla")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $paisSigla;

    /**
     * @return integer
     */
    public function getPaisId()
    {
        return $this->paisId;
    }

    /**
     * @param integer $paisId
     * @return Pais
     */
    public function setPaisId($paisId)
    {
        $this->paisId = $paisId;

        return $this;
    }

    /**
     * @return string
     */
    public function getPaisNome()
    {
        return $this->paisNome;
    }

    /**
     * @param string $paisNome
     * @return Pais
     */
    public function setPaisNome($paisNome)
    {
        $this->paisNome = $paisNome;

        return $this;
    }

    /**
     * @return string
     */
    public function getPaisSigla()
    {
        return $this->paisSigla;
    }

    /**
     * @param string $paisSigla
     * @return Pais
     */
    public function setPaisSigla($paisSigla)
    {
        $this->paisSigla = $paisSigla;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'paisId'    => $this->getPaisId(),
            'paisNome'  => $this->getPaisNome(),
            'paisSigla' => $this->getPaisSigla(),
        );

        return $array;
    }
}
