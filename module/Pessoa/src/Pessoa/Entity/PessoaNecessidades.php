<?php

namespace Pessoa\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PessoaNecessidades
 *
 * @ORM\Table(name="pessoa_necessidades", uniqueConstraints={@ORM\UniqueConstraint(name="index4", columns={"pes_id", "necessidade_id"})}, indexes={@ORM\Index(name="fk_pessoa_fisica_necessidades_especiais_necessidades_especi_idx", columns={"necessidade_id"}), @ORM\Index(name="fk_pessoa_fisica_necessidades_especiais_pessoa_fisica1_idx", columns={"pes_id"})})
 * @ORM\Entity
 */
class PessoaNecessidades
{
    /**
     * @var integer
     *
     * @ORM\Column(name="pes_necessidade_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $pesNecessidadeId;

    /**
     * @var NecessidadesEspeciais
     *
     * @ORM\ManyToOne(targetEntity="NecessidadesEspeciais")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="necessidade_id", referencedColumnName="necessidade_id")
     * })
     */
    private $necessidade;

    /**
     * @var PessoaFisica
     *
     * @ORM\ManyToOne(targetEntity="PessoaFisica")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     */
    private $pes;

    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getPesNecessidadeId()
    {
        return $this->pesNecessidadeId;
    }

    /**
     * @param int $pesNecessidadeId
     */
    public function setPesNecessidadeId($pesNecessidadeId)
    {
        $this->pesNecessidadeId = $pesNecessidadeId;
    }

    /**
     * @return NecessidadesEspeciais
     */
    public function getNecessidade()
    {
        return $this->necessidade;
    }

    /**
     * @param NecessidadesEspeciais $necessidade
     */
    public function setNecessidade($necessidade)
    {
        $this->necessidade = $necessidade;
    }

    /**
     * @return PessoaFisica
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @param PessoaFisica $pes
     */
    public function setPes($pes)
    {
        $this->pes = $pes;
    }

    public function toArray()
    {
        return array_merge(
            $this->getNecessidade() ? $this->getNecessidade()->toArray() : array(),
            array(
                'pesNecessidadeId' => $this->getPesNecessidadeId(),
                'necessidade'      => $this->getNecessidade()->getNecessidadeId(),
                'pes'              => $this->getPes()->getPes()->getPesId()
            )
        );
    }
}
