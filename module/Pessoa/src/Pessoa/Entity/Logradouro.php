<?php

namespace Pessoa\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * Logradouro
 *
 * @ORM\Table(name="logradouro")
 * @ORM\Entity
 * @LG\LG(id="logrId",label="LogrNome")
 * @Jarvis\Jarvis(title="Listagem de logradouro",icon="fa fa-table")
 */
class Logradouro
{
    /**
     * @var integer
     *
     * @ORM\Column(name="logr_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="logr_id")
     * @LG\Labels\Attributes(text="código logradouro")
     * @LG\Querys\Conditions(type="=")
     */
    private $logrId;

    /**
     * @var \Pessoa\Entity\Bairro
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\Bairro")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="bai_id", referencedColumnName="bai_id")
     * })
     */
    private $bai;

    /**
     * @var \Pessoa\Entity\TipoLogradouro
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\TipoLogradouro")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tip_logr_id", referencedColumnName="tip_logr_id")
     * })
     */
    private $tipLogr;

    /**
     * @var string
     *
     * @ORM\Column(name="logr_nome", type="string", nullable=false, length=255)
     * @LG\Labels\Property(name="logr_nome")
     * @LG\Labels\Attributes(text="nome")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $logrNome;

    /**
     * @var string
     *
     * @ORM\Column(name="logr_cep", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="logr_cep")
     * @LG\Labels\Attributes(text="CEP")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $logrCep;

    /**
     * @return integer
     */
    public function getLogrId()
    {
        return $this->logrId;
    }

    /**
     * @param integer $logrId
     * @return Logradouro
     */
    public function setLogrId($logrId)
    {
        $this->logrId = $logrId;

        return $this;
    }

    /**
     * @return \Pessoa\Entity\Bairro
     */
    public function getBai()
    {
        return $this->bai;
    }

    /**
     * @param \Pessoa\Entity\Bairro $bai
     * @return Logradouro
     */
    public function setBai($bai)
    {
        $this->bai = $bai;

        return $this;
    }

    /**
     * @return \Pessoa\Entity\TipoLogradouro
     */
    public function getTipLogr()
    {
        return $this->tipLogr;
    }

    /**
     * @param \Pessoa\Entity\TipoLogradouro $tipLogr
     * @return Logradouro
     */
    public function setTipLogr($tipLogr)
    {
        $this->tipLogr = $tipLogr;

        return $this;
    }

    /**
     * @return string
     */
    public function getLogrNome()
    {
        return $this->logrNome;
    }

    /**
     * @param string $logrNome
     * @return Logradouro
     */
    public function setLogrNome($logrNome)
    {
        $this->logrNome = $logrNome;

        return $this;
    }

    /**
     * @return string
     */
    public function getLogrCep()
    {
        return $this->logrCep;
    }

    /**
     * @param string $logrCep
     * @return Logradouro
     */
    public function setLogrCep($logrCep)
    {
        $this->logrCep = $logrCep;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'logrId'   => $this->getLogrId(),
            'bai'      => $this->getBai(),
            'tipLogr'  => $this->getTipLogr(),
            'logrNome' => $this->getLogrNome(),
            'logrCep'  => $this->getLogrCep(),
        );

        $array['bai']     = $this->getBai() ? $this->getBai()->getBaiId() : null;
        $array['tipLogr'] = $this->getTipLogr() ? $this->getTipLogr()->getTipLogrId() : null;

        return $array;
    }
}
