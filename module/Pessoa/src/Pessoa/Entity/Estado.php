<?php

namespace Pessoa\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * Estado
 *
 * @ORM\Table(name="estado")
 * @ORM\Entity
 * @LG\LG(id="estId",label="EstNome")
 * @Jarvis\Jarvis(title="Listagem de estado",icon="fa fa-table")
 */
class Estado
{
    /**
     * @var integer
     *
     * @ORM\Column(name="est_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="est_id")
     * @LG\Labels\Attributes(text="código estado")
     * @LG\Querys\Conditions(type="=")
     */
    private $estId;

    /**
     * @var \Pessoa\Entity\Pais
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\Pais")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pais", referencedColumnName="pais_id")
     * })
     */
    private $pais;

    /**
     * @var \Pessoa\Entity\Regiao
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\Regiao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="regiao_id", referencedColumnName="regiao_id")
     * })
     */
    private $regiao;

    /**
     * @var string
     *
     * @ORM\Column(name="est_nome", type="string", nullable=false, length=45)
     * @LG\Labels\Property(name="est_nome")
     * @LG\Labels\Attributes(text="nome")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $estNome;

    /**
     * @var string
     *
     * @ORM\Column(name="est_uf", type="string", nullable=false, length=5)
     * @LG\Labels\Property(name="est_uf")
     * @LG\Labels\Attributes(text="UF")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $estUf;

    /**
     * @var string
     *
     * @ORM\Column(name="est_codigo_ibge", type="string", nullable=true, length=10)
     * @LG\Labels\Property(name="est_codigo_ibge")
     * @LG\Labels\Attributes(text="ibge codigo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $estCodigoIbge;

    /**
     * @return integer
     */
    public function getEstId()
    {
        return $this->estId;
    }

    /**
     * @param integer $estId
     * @return Estado
     */
    public function setEstId($estId)
    {
        $this->estId = $estId;

        return $this;
    }

    /**
     * @return \Pessoa\Entity\Pais
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * @param \Pessoa\Entity\Pais $pais
     * @return Estado
     */
    public function setPais($pais)
    {
        $this->pais = $pais;

        return $this;
    }

    /**
     * @return \Pessoa\Entity\Regiao
     */
    public function getRegiao()
    {
        return $this->regiao;
    }

    /**
     * @param \Pessoa\Entity\Regiao $regiao
     * @return Estado
     */
    public function setRegiao($regiao)
    {
        $this->regiao = $regiao;

        return $this;
    }

    /**
     * @return string
     */
    public function getEstNome()
    {
        return $this->estNome;
    }

    /**
     * @param string $estNome
     * @return Estado
     */
    public function setEstNome($estNome)
    {
        $this->estNome = $estNome;

        return $this;
    }

    /**
     * @return string
     */
    public function getEstUf()
    {
        return $this->estUf;
    }

    /**
     * @param string $estUf
     * @return Estado
     */
    public function setEstUf($estUf)
    {
        $this->estUf = $estUf;

        return $this;
    }

    /**
     * @return string
     */
    public function getEstCodigoIbge()
    {
        return $this->estCodigoIbge;
    }

    /**
     * @param string $estCodigoIbge
     * @return Estado
     */
    public function setEstCodigoIbge($estCodigoIbge)
    {
        $this->estCodigoIbge = $estCodigoIbge;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'estId'         => $this->getEstId(),
            'pais'          => $this->getPais(),
            'regiao'        => $this->getRegiao(),
            'estNome'       => $this->getEstNome(),
            'estUf'         => $this->getEstUf(),
            'estCodigoIbge' => $this->getEstCodigoIbge(),
        );

        $array['pais']   = $this->getPais() ? $this->getPais()->getPaisId() : null;
        $array['regiao'] = $this->getRegiao() ? $this->getRegiao()->getRegiaoId() : null;

        return $array;
    }
}
