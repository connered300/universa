<?php

namespace Pessoa\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;

/**
 * NecessidadesEspeciais
 *
 * @ORM\Table(name="necessidades_especiais")
 * @ORM\Entity
 * @LG\LG(id="necessidade_id",label="necessidade_nome")
 */
class NecessidadesEspeciais
{
    /**
     * @var integer
     *
     * @ORM\Column(name="necessidade_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $necessidadeId;

    /**
     * @var string
     *
     * @ORM\Column(name="necessidade_nome", type="string", length=45, nullable=false)
     */
    private $necessidadeNome;

    /**
     * @var string
     *
     * @ORM\Column(name="necessidade_descricao", type="text", nullable=true)
     */
    private $necessidadeDescricao;

    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getNecessidadeId()
    {
        return $this->necessidadeId;
    }

    /**
     * @param int $necessidadeId
     */
    public function setNecessidadeId($necessidadeId)
    {
        $this->necessidadeId = $necessidadeId;
    }

    /**
     * @return string
     */
    public function getNecessidadeNome()
    {
        return $this->necessidadeNome;
    }

    /**
     * @param string $necessidadeNome
     */
    public function setNecessidadeNome($necessidadeNome)
    {
        $this->necessidadeNome = $necessidadeNome;
    }

    /**
     * @return string
     */
    public function getNecessidadeDescricao()
    {
        return $this->necessidadeDescricao;
    }

    /**
     * @param string $necessidadeDescricao
     */
    public function setNecessidadeDescricao($necessidadeDescricao)
    {
        $this->necessidadeDescricao = $necessidadeDescricao;
    }

    public function toArray()
    {
        return array(
            'necessidadeId'        => $this->getNecessidadeId(),
            'necessidadeNome'      => $this->getNecessidadeNome(),
            'necessidadeDescricao' => $this->getNecessidadeDescricao()
        );
    }
}