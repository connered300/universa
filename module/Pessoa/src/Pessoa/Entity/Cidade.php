<?php

namespace Pessoa\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * Cidade
 *
 * @ORM\Table(name="cidade")
 * @ORM\Entity
 * @LG\LG(id="cidId",label="CidNome")
 * @Jarvis\Jarvis(title="Listagem de cidade",icon="fa fa-table")
 */
class Cidade
{
    /**
     * @var integer
     *
     * @ORM\Column(name="cid_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="cid_id")
     * @LG\Labels\Attributes(text="código cidade")
     * @LG\Querys\Conditions(type="=")
     */
    private $cidId;

    /**
     * @var \Pessoa\Entity\Estado
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\Estado")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="estado", referencedColumnName="est_id")
     * })
     */
    private $estado;

    /**
     * @var string
     *
     * @ORM\Column(name="cid_nome", type="string", nullable=false, length=128)
     * @LG\Labels\Property(name="cid_nome")
     * @LG\Labels\Attributes(text="nome")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $cidNome;

    /**
     * @var string
     *
     * @ORM\Column(name="cid_cep", type="string", nullable=true, length=10)
     * @LG\Labels\Property(name="cid_cep")
     * @LG\Labels\Attributes(text="CEP")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $cidCep;

    /**
     * @var string
     *
     * @ORM\Column(name="cid_codigo_ibge", type="string", nullable=true, length=10)
     * @LG\Labels\Property(name="cid_codigo_ibge")
     * @LG\Labels\Attributes(text="ibge codigo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $cidCodigoIbge;

    /**
     * @var string
     *
     * @ORM\Column(name="cid_ddd", type="string", nullable=true, length=3)
     * @LG\Labels\Property(name="cid_ddd")
     * @LG\Labels\Attributes(text="ddd")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $cidDdd;

    /**
     * @var string
     *
     * @ORM\Column(name="cid_lat", type="string", nullable=true, length=180)
     * @LG\Labels\Property(name="cid_lat")
     * @LG\Labels\Attributes(text="lat")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $cidLat;

    /**
     * @var string
     *
     * @ORM\Column(name="cid_lng", type="string", nullable=true, length=180)
     * @LG\Labels\Property(name="cid_lng")
     * @LG\Labels\Attributes(text="lng")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $cidLng;

    /**
     * @return integer
     */
    public function getCidId()
    {
        return $this->cidId;
    }

    /**
     * @param integer $cidId
     * @return Cidade
     */
    public function setCidId($cidId)
    {
        $this->cidId = $cidId;

        return $this;
    }

    /**
     * @return \Pessoa\Entity\Estado
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param \Pessoa\Entity\Estado $estado
     * @return Cidade
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * @return string
     */
    public function getCidNome()
    {
        return $this->cidNome;
    }

    /**
     * @param string $cidNome
     * @return Cidade
     */
    public function setCidNome($cidNome)
    {
        $this->cidNome = $cidNome;

        return $this;
    }

    /**
     * @return string
     */
    public function getCidCep()
    {
        return $this->cidCep;
    }

    /**
     * @param string $cidCep
     * @return Cidade
     */
    public function setCidCep($cidCep)
    {
        $this->cidCep = $cidCep;

        return $this;
    }

    /**
     * @return string
     */
    public function getCidCodigoIbge()
    {
        return $this->cidCodigoIbge;
    }

    /**
     * @param string $cidCodigoIbge
     * @return Cidade
     */
    public function setCidCodigoIbge($cidCodigoIbge)
    {
        $this->cidCodigoIbge = $cidCodigoIbge;

        return $this;
    }

    /**
     * @return string
     */
    public function getCidDdd()
    {
        return $this->cidDdd;
    }

    /**
     * @param string $cidDdd
     * @return Cidade
     */
    public function setCidDdd($cidDdd)
    {
        $this->cidDdd = $cidDdd;

        return $this;
    }

    /**
     * @return string
     */
    public function getCidLat()
    {
        return $this->cidLat;
    }

    /**
     * @param string $cidLat
     * @return Cidade
     */
    public function setCidLat($cidLat)
    {
        $this->cidLat = $cidLat;

        return $this;
    }

    /**
     * @return string
     */
    public function getCidLng()
    {
        return $this->cidLng;
    }

    /**
     * @param string $cidLng
     * @return Cidade
     */
    public function setCidLng($cidLng)
    {
        $this->cidLng = $cidLng;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'cidId'         => $this->getCidId(),
            'estado'        => $this->getEstado(),
            'cidNome'       => $this->getCidNome(),
            'cidCep'        => $this->getCidCep(),
            'cidCodigoIbge' => $this->getCidCodigoIbge(),
            'cidDdd'        => $this->getCidDdd(),
            'cidLat'        => $this->getCidLat(),
            'cidLng'        => $this->getCidLng(),
        );

        $array['estado'] = $this->getEstado() ? $this->getEstado()->getEstId() : null;

        return $array;
    }
}
