<?php

namespace Pessoa\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * Bairro
 *
 * @ORM\Table(name="bairro")
 * @ORM\Entity
 * @LG\LG(id="baiId",label="BaiNome")
 * @Jarvis\Jarvis(title="Listagem de bairro",icon="fa fa-table")
 */
class Bairro
{
    /**
     * @var integer
     *
     * @ORM\Column(name="bai_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="bai_id")
     * @LG\Labels\Attributes(text="código bai")
     * @LG\Querys\Conditions(type="=")
     */
    private $baiId;

    /**
     * @var \Pessoa\Entity\Cidade
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\Cidade")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cidade", referencedColumnName="cid_id")
     * })
     */
    private $cidade;

    /**
     * @var string
     *
     * @ORM\Column(name="bai_nome", type="string", nullable=false, length=128)
     * @LG\Labels\Property(name="bai_nome")
     * @LG\Labels\Attributes(text="nome bai")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $baiNome;

    /**
     * @return integer
     */
    public function getBaiId()
    {
        return $this->baiId;
    }

    /**
     * @param integer $baiId
     * @return Bairro
     */
    public function setBaiId($baiId)
    {
        $this->baiId = $baiId;

        return $this;
    }

    /**
     * @return \Pessoa\Entity\Cidade
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * @param \Pessoa\Entity\Cidade $cidade
     * @return Bairro
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;

        return $this;
    }

    /**
     * @return string
     */
    public function getBaiNome()
    {
        return $this->baiNome;
    }

    /**
     * @param string $baiNome
     * @return Bairro
     */
    public function setBaiNome($baiNome)
    {
        $this->baiNome = $baiNome;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'baiId'   => $this->getBaiId(),
            'cidade'  => $this->getCidade(),
            'baiNome' => $this->getBaiNome(),
        );

        $array['cidade'] = $this->getCidade() ? $this->getCidade()->getCidId() : null;

        return $array;
    }
}
