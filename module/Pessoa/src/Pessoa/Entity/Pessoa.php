<?php

namespace Pessoa\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;

/**
 * Pessoa
 *
 * @ORM\Table(name="pessoa")
 * @ORM\Entity
 * @ORM\Entity (repositoryClass="Pessoa\Entity\Repository\PessoaRepository")
 * @LG\LG(id="pesId",label="pesNome")
 */
class Pessoa
{
    /**
     * @var integer
     *
     * @ORM\Column(name="pes_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $pesId;

    /**
     * @var string
     *
     * @ORM\Column(name="pes_nome", type="string", length=45, nullable=false)
     */
    private $pesNome;

    /**
     * @var string
     *
     * @ORM\Column(name="pes_tipo", type="string", nullable=false)
     */
    private $pesTipo;

    /**
     * @var string
     *
     * @ORM\Column(name="pes_nacionalidade", type="string", nullable=false)
     */
    private $pesNacionalidade;

    /**
     * @var string
     *
     * @ORM\Column(name="pes_observacao", type="string", nullable=true, length=3000)
     * @LG\Labels\Property(name="pes_observacao")
     * @LG\Labels\Attributes(text="observação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $pesObservacao;

    /**
     * @var string
     *
     * @ORM\Column(name="con_contato_telefone", type="string", length=255, nullable=false)
     * @LG\Labels\Property(name="con_contato_telefone")
     * @LG\Labels\Attributes(text="telefone")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $conContatoTelefone;

    /**
     * @var string
     *
     * @ORM\Column(name="con_contato_celular", type="string", length=255, nullable=false)
     * @LG\Labels\Property(name="con_contato_celular")
     * @LG\Labels\Attributes(text="celular")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $conContatoCelular;

    /**
     * @var string
     *
     * @ORM\Column(name="con_contato_email", type="string", length=255, nullable=false)
     * @LG\Labels\Property(name="con_contato_email")
     * @LG\Labels\Attributes(text="email")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $conContatoEmail;

    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getPesId()
    {
        return $this->pesId;
    }

    /**
     * @param int $pesId
     * @return Pessoa
     */
    public function setPesId($pesId)
    {
        $this->pesId = $pesId;

        return $this;
    }

    /**
     * @return string
     */
    public function getPesNome()
    {
        return $this->pesNome;
    }

    /**
     * @param string $pesNome
     * @return Pessoa
     */
    public function setPesNome($pesNome)
    {
        if (strlen($pesNome) > 255) {
            $pesNome = substr($pesNome, 255);
        }

        $this->pesNome = $pesNome;

        return $this;
    }

    /**
     * @return string
     */
    public function getPesTipo()
    {
        return $this->pesTipo;
    }

    /**
     * @return string
     */
    public function getPesTipoNumero()
    {
        return $this->getPesTipo() == "Juridica" ? 2 : 1;
    }

    /**
     * @param string $pesTipo
     * @return Pessoa
     */
    public function setPesTipo($pesTipo)
    {
        $this->pesTipo = $pesTipo;

        return $this;
    }

    /**
     * @return string
     */
    public function getPesNacionalidade()
    {
        return $this->pesNacionalidade;
    }

    /**
     * @param string $pesNacionalidade
     * @return Pessoa
     */
    public function setPesNacionalidade($pesNacionalidade)
    {
        $this->pesNacionalidade = $pesNacionalidade;

        return $this;
    }

    /**
     * @return string
     */
    public function getPesObservacao()
    {
        return $this->pesObservacao;
    }

    /**
     * @param string $pesObservacao
     * @return Pessoa
     */
    public function setPesObservacao($pesObservacao)
    {
        $this->pesObservacao = $pesObservacao;

        return $this;
    }

    /**
     * @return string
     */
    public function getConContatoTelefone()
    {
        return $this->conContatoTelefone;
    }

    /**
     * @param string $conContatoTelefone
     * @return Pessoa
     */
    public function setConContatoTelefone($conContatoTelefone)
    {
        $this->conContatoTelefone = $conContatoTelefone;

        return $this;
    }

    /**
     * @return string
     */
    public function getConContatoCelular()
    {
        return $this->conContatoCelular;
    }

    /**
     * @param string $conContatoCelular
     * @return Pessoa
     */
    public function setConContatoCelular($conContatoCelular)
    {
        $this->conContatoCelular = $conContatoCelular;

        return $this;
    }

    /**
     * @return string
     */
    public function getConContatoEmail()
    {
        return $this->conContatoEmail;
    }

    /**
     * @param string $conContatoEmail
     * @return Pessoa
     */
    public function setConContatoEmail($conContatoEmail)
    {
        $this->conContatoEmail = $conContatoEmail;

        return $this;
    }

    public function toArray()
    {
        return array(
            'pesId'              => $this->getPesId(),
            'pesNome'            => $this->getPesNome(),
            'pesTipo'            => $this->getPesTipo(),
            'getPesTipoNumero'   => $this->getPesTipoNumero(),
            'pesNacionalidade'   => $this->getPesNacionalidade(),
            'pesObservacao'      => $this->getPesObservacao(),
            'conContatoTelefone' => $this->getConContatoTelefone(),
            'conContatoCelular'  => $this->getConContatoCelular(),
            'conContatoEmail'    => $this->getConContatoEmail(),
        );
    }

    public function __toString()
    {
        return (string)$this->getPesId();
    }
}