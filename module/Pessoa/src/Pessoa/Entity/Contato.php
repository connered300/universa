<?php

namespace Pessoa\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;

/**
 * Contato
 *
 * @ORM\Table(name="contato", indexes={@ORM\Index(name="fk_contato_pessoas1_idx", columns={"pessoa"}), @ORM\Index(name="fk_contato_tipo_contato1_idx", columns={"tipo_contato"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Pessoa\Entity\Repository\ContatoRepository")
 * @LG\LG(id="contId",label="conContato")
 */
class Contato
{
    /**
     * @var integer
     *
     * @ORM\Column(name="con_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $conId;

    /**
     * @var string
     *
     * @ORM\Column(name="con_contato", type="string", length=255, nullable=false)
     */
    private $conContato;

    /**
     * @var Pessoa
     *
     * @ORM\ManyToOne(targetEntity="Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pessoa", referencedColumnName="pes_id")
     * })
     */
    private $pessoa;

    /**
     * @var TipoContato
     *
     * @ORM\ManyToOne(targetEntity="TipoContato")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipo_contato", referencedColumnName="tip_contato_id")
     * })
     */
    private $tipoContato;

    public function __construct(array $data=array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    public function getConId()
    {
        return $this->conId;
    }

    public function getConContato()
    {
        return $this->conContato;
    }

    public function getPessoa()
    {
        return $this->pessoa;
    }

    public function getTipoContato()
    {
        return $this->tipoContato;
    }

    public function setConId($conId)
    {
        $this->conId = $conId;
    }

    public function setConContato($conContato)
    {
        $this->conContato = $conContato;
    }

    public function setPessoa($pessoa)
    {
        $this->pessoa = $pessoa;
    }

    public function setTipoContato($tipoContato)
    {
        $this->tipoContato = $tipoContato;
    }

    public function toArray()
    {
        return array(
            'conId'       => $this->getConId(),
            'conContato'  => $this->getConContato(),
            'pessoa'      => $this->getPessoa()->getPesId(),
            'tipoContato' => $this->getTipoContato()->getTipContatoId()
        );
    }

    public function toArrayAux()
    {
        return array(
            'conId'       => $this->getConId(),
            'conContato'  => $this->getConContato(),
            'pessoa'      => $this->getPessoa()->getPesId(),
            'tipoContato' => $this->getTipoContato()->getTipContatoNome(),
        );
    }

}
