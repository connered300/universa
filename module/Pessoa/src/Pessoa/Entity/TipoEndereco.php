<?php

namespace Pessoa\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
/**
 * TipoEndereco
 *
 * @ORM\Table(name="tipo_endereco")
 * @ORM\Entity
 * @LG\LG(id="id",label="nome")
 */
class TipoEndereco
{
    /**
     * @var integer
     *
     * @ORM\Column(name="tipo_endereco_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $tipoEnderecoId;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=45, nullable=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="string", length=255, nullable=true)
     */
    private $descricao;
    
    public function __construct(array $data)
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    public function getTipoEnderecoId()
    {
        return $this->tipoEnderecoId;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function getDescricao()
    {
        return $this->descricao;
    }

    public function setTipoEnderecoId($tipoEnderecoId)
    {
        $this->tipoEnderecoId = $tipoEnderecoId;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }
    
    public function toArray()
    {
        return array(
            'tipoEnderecoId' => $this->getTipoEnderecoId(),
            'nome'           => $this->getNome(),
            'descricao'      => $this->getDescricao(),
        );
    }

}
