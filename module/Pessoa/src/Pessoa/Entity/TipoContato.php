<?php

namespace Pessoa\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * TipoContato
 *
 * @ORM\Table(name="tipo_contato")
 * @ORM\Entity(repositoryClass="Pessoa\Entity\Repository\TipoContatoRepository")
 * @LG\LG(id="tipContatoId",label="tip_contato_nome")
 * @Jarvis\Jarvis(title="Lista de tipo contatos",icon="fa fa-table")
 */
class TipoContato {
    /**
     * @var integer
     *
     * @ORM\Column(name="tip_contato_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="tip_contato_id")
     * @LG\Labels\Attributes(text="Índice",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $tipContatoId;

    /**
     * @var string
     *
     * @ORM\Column(name="tip_contato_nome", type="string", length=45, nullable=false)
     *
     * @LG\Labels\Property(name="tip_contato_nome")
     * @LG\Labels\Attributes(text="Nome do Contato",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $tipContatoNome;

    /**
     * @var string
     *
     * @ORM\Column(name="tip_contato_descricao", type="string", length=45, nullable=false)
     *
     * @LG\Labels\Property(name="tip_contato_descricao")
     * @LG\Labels\Attributes(text="Descrição",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $tipContatoDescricao;


    public function __construct(array $data)
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    public function getTipContatoId()
    {
        return $this->tipContatoId;
    }

    public function getTipContatoNome()
    {
        return $this->tipContatoNome;
    }

    public function getTipContatoDescricao()
    {
        return $this->tipContatoDescricao;
    }

    public function setTipContatoId($tipContatoId)
    {
        $this->tipContatoId = $tipContatoId;
    }

    public function setTipContatoNome($tipContatoNome)
    {
        $this->tipContatoNome = $tipContatoNome;
    }

    public function setTipContatoDescricao($tipContatoDescricao)
    {
        $this->tipContatoDescricao = $tipContatoDescricao;
    }



    public function toArray()
    {
        return array(
            'tipContatoId' => $this->getTipContatoId(),
            'tipContatoNome' => $this->getTipContatoNome(),
            'tipContatoDescricao' => $this->getTipContatoDescricao(),
        );
    }

}
