<?php

namespace Pessoa\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class PessoaRepository extends EntityRepository {
    public function buscaPessoasNome( $query ){
//        $sql = "SELECT p.pessoa codigo,CONCAT(p.pes_nome,' | ',pf.pes_cpf) string FROM pessoa p INNER JOIN pessoa_fisica pf ON p.pessoa = pf.pes_id WHERE ";
        $sql = "SELECT p.pes_id codigo,CONCAT(p.pes_nome,' | ',pf.pes_cpf) string FROM pessoa p INNER JOIN pessoa_fisica pf ON p.pes_id = pf.pes_id WHERE ";
        if ( is_numeric($query) ){
            $sql .= "pf.pes_cpf LIKE '%$query%'";
        } else {
            $sql .= "p.pes_nome LIKE '%$query%'";
        }
        
        $sql = $this->getEntityManager()->getConnection()->prepare($sql);
        $sql->execute();
        $array = $sql->fetchAll();
        $pessoas = array();
        foreach ( $array as $a ){
            $pessoas[] = array('label'=>$a['string'],'codigo'=>$a['codigo']);
        }
        return $pessoas;
    }
}
