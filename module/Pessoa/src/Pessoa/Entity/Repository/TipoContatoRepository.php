<?php


namespace Pessoa\Entity\Repository;


use Doctrine\ORM\EntityRepository;

class TipoContatoRepository extends EntityRepository{
    public function findByTipoContato($tipo)
    {
        $tipo = $this->findOneBy(['tipContatoNome' => $tipo])->toArray();
        return ($tipo)? $tipo : null;
    }

}