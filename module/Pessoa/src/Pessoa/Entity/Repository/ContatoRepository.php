<?php

namespace Pessoa\Entity\Repository;

/**
 * Description of ContatoRepository
 *
 * @author Matheus Ferreira
 */
use Doctrine\ORM\EntityRepository;

class ContatoRepository extends EntityRepository {
    /**
     * Busca os contatos vinculados a pessoa,
     * separa os contatos por tipo de contato.
     * 
     * @param type $pessoa_id: id da pessoa
     * @return array de contatos com o índice 
     *         sendo o tipo de contato
     */
    public function buscaContatos($pessoa_id)
    {
        $array      = $this->findBy(array('pessoa' => $pessoa_id));
        $contatos   = array();

        foreach ($array as $cont){
            $contatos[$cont->getTipoContato()->getTipContatoNome()][] = $cont;
        }
        return $contatos;
    }

}
