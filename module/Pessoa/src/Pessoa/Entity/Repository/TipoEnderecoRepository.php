<?php

namespace Pessoa\Entity\Repository;

use Doctrine\ORM\EntityRepository;
/**
 * Description of TipoEndereco
 *
 * @author Matheus Ferreira
 */
class TipoEndereco extends EntityRepository {
    public function buscaTipoEndOptions()
    {
        $array      = $this->findAll();
        $options    = array();
        foreach ($array as $a){
            $options[$a->getTipoContato()][] = $a->getNome(); 
        }
        
        return $options;
    }

    public function buscaTipoEndereco($nome = null)
    {
        if( !($nome === null) ){
            $id = $this->findOneBy(['nome' => $nome])->getTipoEnderecoId();
            if( !empty($id) ){
                return $id;
            }
        }

        return false;
    }
}
