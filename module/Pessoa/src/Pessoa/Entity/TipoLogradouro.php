<?php

namespace Pessoa\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;

/**
 * TipoLogradouro
 *
 * @ORM\Table(name="tipo_logradouro")
 * @ORM\Entity
 */
class TipoLogradouro {
    /**
     * @var integer
     *
     * @ORM\Column(name="tip_logr_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $tipLogrId;

    /**
     * @var string
     *
     * @ORM\Column(name="tip_logr_nome", type="string", length=128, nullable=false)
     */
    private $tipLogrNome;

    public function __construct(array $data)
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    public function getTipLogrId()
    {
        return $this->tipLogrId;
    }

    public function getTipLogrNome()
    {
        return $this->tipLogrNome;
    }

    public function setTipLogrId($tipLogrId)
    {
        $this->tipLogrId = $tipLogrId;
    }

    public function setTipLogrNome($tipLogrNome)
    {
        $this->tipLogrNome = $tipLogrNome;
    }

    public function toArray()
    {
        return array(
            'tipLogrId' => $this->getTipLogrId(),
            'tipLogrNome' => $this->getTipLogrNome(),
        );
    }

}
