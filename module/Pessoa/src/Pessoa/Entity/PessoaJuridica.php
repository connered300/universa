<?php

namespace Pessoa\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * PessoaJuridica
 *
 * @ORM\Table(name="pessoa_juridica")
 * @ORM\Entity
 * @LG\LG(id="pesId",label="pesNomeFantasia")
 * @Jarvis\Jarvis(title="Listagem dos Parceiros",icon="fa fa-table")
 */
class PessoaJuridica
{
    /**
     * @var string
     *
     * @ORM\Column(name="pes_nome_fantasia", type="string", length=255, nullable=true)
     * @LG\Labels\Property(name="pes_nome_fantasia")
     * @LG\Labels\Attributes(text="Nome Fantasia",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $pesNomeFantasia;

    /**
     * @var string
     *
     * @ORM\Column(name="pes_cnpj", type="string", length=20, nullable=false)
     * @LG\Labels\Property(name="pes_cnpj")
     * @LG\Labels\Attributes(text="CNPJ do Parceiro",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $pesCnpj;

    /**
     * @var string
     *
     * @ORM\Column(name="pes_insc_municipal", type="string", length=45, nullable=true)
     *
     */
    private $pesInscMunicipal;

    /**
     * @var string
     *
     * @ORM\Column(name="pes_insc_estadual", type="string", length=45, nullable=true)
     */
    private $pesInscEstadual;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="pes_data_inicio", type="datetime", nullable=false)
     * @LG\Labels\Property(name="pes_data_inicio")
     * @LG\Labels\Attributes(text="Data de criação",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $pesDataInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="pes_data_fim", type="datetime", nullable=true)
     * @LG\Labels\Property(name="pes_data_fim")
     * @LG\Labels\Attributes(text="Data de Encerramento",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $pesDataFim;

    /**
     * @var string
     *
     * @ORM\Column(name="pes_sigla", type="string", length=45, nullable=true)
     */
    private $pesSigla;

    /**
     * @var string
     *
     * @ORM\Column(name="pes_natureza_juridica", type="string", length=45, nullable=true)
     */
    private $pesNaturezaJuridica;

    /**
     * @var Pessoa
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     * @LG\Labels\Property(name="pes")
     * @LG\Labels\Attributes(text="Índice",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */

    private $pes;

    /**
     * @param array $data
     */
    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return Pessoa
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @return string
     */
    public function getPesNomeFantasia()
    {
        return $this->pesNomeFantasia;
    }

    /**
     * @return string
     */
    public function getPesCnpj()
    {
        return $this->pesCnpj;
    }

    /**
     * @return string
     */
    public function getPesInscMunicipal()
    {
        return $this->pesInscMunicipal;
    }

    /**
     * @return string
     */
    public function getPesInscEstadual()
    {
        return $this->pesInscEstadual;
    }

    /**
     * @return string
     */
    public function getPesDataInicio()
    {
        return $this->pesDataInicio ? $this->pesDataInicio->format('d/m/Y') : null;
    }

    /**
     * @return null|string
     */
    public function getPesDataFim()
    {
        return ($this->pesDataFim != null) ? $this->pesDataFim->format('d/m/Y') : null;
    }

    /**
     * @return string
     */
    public function getPesSigla()
    {
        return $this->pesSigla;
    }

    /**
     * @return string
     */
    public function getPesNaturezaJuridica()
    {
        return $this->pesNaturezaJuridica;
    }

    /**
     * @param $pes
     */
    public function setPes($pes)
    {
        $this->pes = $pes;
    }

    /**
     * @param $pesNomeFantasia
     */
    public function setPesNomeFantasia($pesNomeFantasia)
    {
        if (strlen($pesNomeFantasia) > 255) {
            $pesNomeFantasia = substr($pesNomeFantasia, 255);
        }

        $this->pesNomeFantasia = $pesNomeFantasia;
    }

    /**
     * @param $pesCnpj
     */
    public function setPesCnpj($pesCnpj)
    {
        $this->pesCnpj = $pesCnpj;
    }

    /**
     * @param $pesInscMunicipal
     */
    public function setPesInscMunicipal($pesInscMunicipal)
    {
        $this->pesInscMunicipal = $pesInscMunicipal;
    }

    /**
     * @param $pesInscEstadual
     */
    public function setPesInscEstadual($pesInscEstadual)
    {
        $this->pesInscEstadual = $pesInscEstadual;
    }

    /**
     * @param $pesDataInicio
     */
    public function setPesDataInicio($pesDataInicio)
    {
        if ($pesDataInicio) {
            if (is_string($pesDataInicio)) {
                if (!preg_match('/\d{4}-\d{2}-\d{2}/', $pesDataInicio)) {
                    $pesDataInicio = \VersaSpine\Service\AbstractService::formatDateAmericano($pesDataInicio);
                }

                try {
                    $pesDataInicio = new \Datetime($pesDataInicio);
                } catch (\Exception $ex) {
                    $pesDataInicio = null;
                }
            }
        } else {
            $pesDataInicio = null;
        }

        $this->pesDataInicio = $pesDataInicio;

        return $this;
    }

    /**
     * @param $pesDataFim
     */
    public function setPesDataFim($pesDataFim)
    {
        if ($pesDataFim) {
            if (is_string($pesDataFim)) {
                if (!preg_match('/\d{4}-\d{2}-\d{2}/', $pesDataFim)) {
                    $pesDataFim = \VersaSpine\Service\AbstractService::formatDateAmericano($pesDataFim);
                }

                try {
                    $pesDataFim = new \Datetime($pesDataFim);
                } catch (\Exception $ex) {
                    $pesDataFim = null;
                }
            }
        } else {
            $pesDataFim = null;
        }

        $this->pesDataFim = $pesDataFim;

        return $this;
    }

    /**
     * @param $pesSigla
     */
    public function setPesSigla($pesSigla)
    {
        $this->pesSigla = $pesSigla;
    }

    /**
     * @param $pesNaturezaJuridica
     */
    public function setPesNaturezaJuridica($pesNaturezaJuridica)
    {
        $this->pesNaturezaJuridica = $pesNaturezaJuridica;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $pes = $this->getPes();

        $arrData = array(
            'pes'                 => $this->getPes()->getPesId(),
            'pesNome'             => $this->getPes()->getPesNome(),
            'pesNomeFantasia'     => $this->getPesNomeFantasia(),
            'pesCnpj'             => $this->getPesCnpj(),
            'pesInscMunicipal'    => $this->getPesInscMunicipal(),
            'pesInscEstadual'     => $this->getPesInscEstadual(),
            'pesDataInicio'       => $this->getPesDataInicio(),
            'pesDataFim'          => $this->getPesDataFim(),
            'pesSigla'            => $this->getPesSigla(),
            'pesNaturezaJuridica' => $this->getPesNaturezaJuridica(),
        );

        if (!$pes) {
            $pes = new \Pessoa\Entity\Pessoa(array());
        }

        return array_merge($pes->toArray(), $arrData);
    }

}
