<?php

namespace Pessoa\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;

/**
 * PessoaFisica
 *
 * @ORM\Table(name="pessoa_fisica")
 * @ORM\Entity
 * @LG\LG(id="pesId",label="pesCpf")
 */
class PessoaFisica
{
    /**
     * @var array
     */
    private $estados = [
        'AC' => 'Acre',
        'AL' => 'Alagoas',
        'AP' => 'Amapá',
        'AM' => 'Amazonas',
        'BA' => 'Bahia',
        'DF' => 'Brasília',
        'CE' => 'Ceará',
        'ES' => 'Espírito Santo',
        'GO' => 'Goiás',
        'MA' => 'Maranhão',
        'MT' => 'Mato Grosso',
        'MS' => 'Mato Grosso do Sul',
        'MG' => 'Minas Gerais',
        'PA' => 'Pará',
        'PB' => 'Paraíba',
        'PR' => 'Paraná',
        'PE' => 'Pernambuco',
        'PI' => 'Piauí',
        'RJ' => 'Rio de Janeiro',
        'RN' => 'Rio Grande do Norte',
        'RS' => 'Rio Grande do Sul',
        'RO' => 'Rondônia',
        'RR' => 'Roraima',
        'SC' => 'Santa Catarina',
        'SP' => 'São Paulo',
        'SE' => 'Sergipe',
        'TO' => 'Tocantins',
    ];
    /**
     * @var string
     *
     * @ORM\Column(name="pes_cpf", type="string", length=16, nullable=false)
     */
    private $pesCpf;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="pes_cpf_emissao", type="datetime", nullable=true)
     */
    private $pesCpfEmissao;

    /**
     * @var string
     *
     * @ORM\Column(name="pes_rg", type="string", length=20, nullable=true)
     */
    private $pesRg;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="pes_rg_emissao", type="datetime", nullable=true)
     */
    private $pesRgEmissao;

    /**
     * @var string
     *
     * @ORM\Column(name="pes_sobrenome", type="string", length=128, nullable=true)
     */
    private $pesSobrenome;

    /**
     * @var string
     *
     * @ORM\Column(name="pes_sexo", type="string", nullable=true)
     */
    private $pesSexo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="pes_data_nascimento", type="datetime", nullable=true)
     */
    private $pesDataNascimento;

    /**
     * @var string
     *
     * @ORM\Column(name="pes_estado_civil", type="string", nullable=true)
     */
    private $pesEstadoCivil;

    /**
     * @var integer
     *
     * @ORM\Column(name="pes_filhos", type="integer", nullable=true)
     */
    private $pesFilhos;

    /**
     * @var string
     *
     * @ORM\Column(name="pes_naturalidade", type="string", length=45, nullable=true)
     */
    private $pesNaturalidade;

    /**
     * @var string
     *
     * @ORM\Column(name="pes_doc_estrangeiro", type="string", length=20, nullable=true)
     */
    private $pesDocEstrangeiro;

    /**
     * @var string
     *
     * @ORM\Column(name="pes_emissor_rg", type="string", length=10, nullable=true)
     */
    private $pesEmissorRg;

    /**
     * @var string
     *
     * @ORM\Column(name="pes_nasc_uf", type="string", length=5, nullable=true)
     */
    private $pesNascUf;

    /**
     * @var string
     *
     * @ORM\Column(name="pes_falecido", type="string", length=3, nullable=true)
     */
    private $pesFalecido = 'Não';

    /**
     * @var string
     *
     * @ORM\Column(name="pes_profissao", type="string", length=200, nullable=true)
     */
    private $pesProfissao;

    /**
     * @var string
     *
     * @ORM\Column(name="pes_empresa", type="string", length=200, nullable=true)
     */
    private $pesEmpresa;

    /**
     * @var Pessoa
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     */
    private $pes;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="NecessidadesEspeciais", inversedBy="pes")
     * @ORM\JoinTable(name="pessoa_necessidades",
     *   joinColumns={
     *     @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="necessidade_id", referencedColumnName="necessidade_id")
     *   }
     * )
     */
    private $necessidade;

    /**
     * @param array $data
     */
    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return string
     */
    public function getPesCpf()
    {
        return $this->pesCpf;
    }

    /**
     * @return null|string
     */
    public function getPesCpfEmissao()
    {
        return ($this->pesCpfEmissao) ? $this->pesCpfEmissao->format('d/m/Y') : null;
    }

    /**
     * @return string
     */
    public function getPesRg()
    {
        return $this->pesRg;
    }

    /**
     * @return null|string
     */
    public function getPesRgEmissao()
    {
        return ($this->pesRgEmissao) ? $this->pesRgEmissao->format('d/m/Y') : null;
    }

    /**
     * @return string
     */
    public function getPesSobrenome()
    {
        return $this->pesSobrenome;
    }

    /**
     * @return string
     */
    public function getPesSexo()
    {
        return $this->pesSexo;
    }

    /**
     * @return null|string
     */
    public function getPesDataNascimento()
    {
        return ($this->pesDataNascimento != null) ? $this->pesDataNascimento->format('d/m/Y') : null;
    }

    /**
     * @return string
     */
    public function getPesEstadoCivil()
    {
        return $this->pesEstadoCivil;
    }

    /**
     * @return int
     */
    public function getPesFilhos()
    {
        return $this->pesFilhos;
    }

    /**
     * @return \Pessoa\Entity\Pessoa
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNecessidade()
    {
        return $this->necessidade;
    }

    /**
     * @param $pes
     * @return $this
     */
    public function setPes($pes)
    {
        $this->pes = $pes;

        return $this;
    }

    /**
     * @param $pesCpf
     * @return $this
     */
    public function setPesCpf($pesCpf)
    {
        $this->pesCpf = $pesCpf;

        return $this;
    }

    /**
     * @param $pesCpfEmissao
     * @return $this
     */
    public function setPesCpfEmissao($pesCpfEmissao)
    {
        if ($pesCpfEmissao) {
            if (is_string($pesCpfEmissao)) {
                if (!preg_match('/\d{4}-\d{2}-\d{2}/', $pesCpfEmissao)) {
                    $pesCpfEmissao = \VersaSpine\Service\AbstractService::formatDateAmericano($pesCpfEmissao);
                }
                $pesCpfEmissao = new \Datetime($pesCpfEmissao);
            }
        } else {
            $pesCpfEmissao = null;
        }

        $this->pesCpfEmissao = $pesCpfEmissao;

        return $this;
    }

    /**
     * @param $pesRg
     * @return $this
     */
    public function setPesRg($pesRg)
    {
        $this->pesRg = $pesRg;

        return $this;
    }

    /**
     * @param $pesRgEmissao
     * @return $this
     */
    public function setPesRgEmissao($pesRgEmissao)
    {
        if ($pesRgEmissao) {
            if (is_string($pesRgEmissao)) {
                if (!preg_match('/\d{4}-\d{2}-\d{2}/', $pesRgEmissao)) {
                    $pesRgEmissao = \VersaSpine\Service\AbstractService::formatDateAmericano($pesRgEmissao);
                }

                $pesRgEmissao = new \Datetime($pesRgEmissao);
            }
        } else {
            $pesRgEmissao = null;
        }

        $this->pesRgEmissao = $pesRgEmissao;

        return $this;
    }

    /**
     * @param $pesSobrenome
     * @return $this
     */
    public function setPesSobrenome($pesSobrenome)
    {
        $this->pesSobrenome = $pesSobrenome;

        return $this;
    }

    /**
     * @param $pesSexo
     * @return $this
     */
    public function setPesSexo($pesSexo)
    {
        $this->pesSexo = $pesSexo;

        return $this;
    }

    /**
     * @param $pesDataNascimento
     * @return $this
     */
    public function setPesDataNascimento($pesDataNascimento)
    {
        if ($pesDataNascimento) {
            if (is_string($pesDataNascimento)) {
                if (!preg_match('/\d{4}-\d{2}-\d{2}/', $pesDataNascimento)) {
                    $pesDataNascimento = \VersaSpine\Service\AbstractService::formatDateAmericano($pesDataNascimento);
                }

                $pesDataNascimento = new \Datetime($pesDataNascimento);
            }
        } else {
            $pesDataNascimento = null;
        }

        $this->pesDataNascimento = $pesDataNascimento;

        return $this;
    }

    /**
     * @param $pesEstadoCivil
     * @return $this
     */
    public function setPesEstadoCivil($pesEstadoCivil)
    {
        if ($pesEstadoCivil == 'NULL') {
            $pesEstadoCivil = $this->pesEstadoCivil;
        }

        $this->pesEstadoCivil = $pesEstadoCivil;

        return $this;
    }

    /**
     * @param $pesFilhos
     * @return $this
     */
    public function setPesFilhos($pesFilhos)
    {
        $this->pesFilhos = $pesFilhos;

        return $this;
    }

    /**
     * @param $necessidade
     * @return $this
     */
    public function setNecessidade($necessidade)
    {
        $this->necessidade = $necessidade;

        return $this;
    }

    /**
     * @return string
     */
    public function getPesDocEstrangeiro()
    {
        return $this->pesDocEstrangeiro;
    }

    /**
     * @param $pesDocEstrangeiro
     * @return $this
     */
    public function setPesDocEstrangeiro($pesDocEstrangeiro)
    {
        $this->pesDocEstrangeiro = $pesDocEstrangeiro;

        return $this;
    }

    /**
     * @return string
     */
    public function getPesEmissorRg()
    {
        return $this->pesEmissorRg;
    }

    /**
     * @param $pesEmissorRg
     * @return $this
     */
    public function setPesEmissorRg($pesEmissorRg)
    {
        $this->pesEmissorRg = $pesEmissorRg;

        return $this;
    }

    /**
     * @return string
     */
    public function getPesNascUf($extenso = false)
    {
        if ($extenso && $this->pesNascUf) {
            return $this->estados[$this->pesNascUf];
        }

        return $this->pesNascUf;
    }

    /**
     * @param $pesNascUf
     * @return $this
     */
    public function setPesNascUf($pesNascUf)
    {
        if (strlen($pesNascUf) > 2) {
            $uf = "";

            foreach ($this->estados as $sigla => $extenso) {
                if ($pesNascUf == $extenso) {
                    $uf = $sigla;
                    break;
                }
            }

            $pesNascUf = $uf;
        }

        $this->pesNascUf = $pesNascUf;

        return $this;
    }

    /**
     * @return string
     */
    public function getPesFalecido()
    {
        return $this->pesFalecido;
    }

    /**
     * @param $pesFalecido
     * @return $this
     */
    public function setPesFalecido($pesFalecido)
    {
        $this->pesFalecido = $pesFalecido;

        return $this;
    }

    /**
     * @return string
     */
    public function getPesProfissao()
    {
        return $this->pesProfissao;
    }

    /**
     * @param string $pesProfissao
     * @return PessoaFisica
     */
    public function setPesProfissao($pesProfissao)
    {
        $this->pesProfissao = $pesProfissao;

        return $this;
    }

    /**
     * @return string
     */
    public function getPesEmpresa()
    {
        return $this->pesEmpresa;
    }

    /**
     * @param string $pesEmpresa
     * @return PessoaFisica
     */
    public function setPesEmpresa($pesEmpresa)
    {
        $this->pesEmpresa = $pesEmpresa;

        return $this;
    }

    /**
     * @return string
     */
    public function getPesNaturalidade()
    {
        return $this->pesNaturalidade;
    }

    /**
     * @param $pesNaturalidade
     * @return $this
     */
    public function setPesNaturalidade($pesNaturalidade)
    {
        $this->pesNaturalidade = $pesNaturalidade;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $pes = $this->getPes();

        $arrData = array(
            'pes'               => $pes,
            'pesCpf'            => $this->getPesCpf(),
            'pesCpfEmissao'     => $this->getPesCpfEmissao(),
            'pesRg'             => $this->getPesRg(),
            'pesRgEmissao'      => $this->getPesRgEmissao(),
            'pesSobrenome'      => $this->getPesSobrenome(),
            'pesSexo'           => $this->getPesSexo(),
            'pesDataNascimento' => $this->getPesDataNascimento(),
            'pesEstadoCivil'    => $this->getPesEstadoCivil(),
            'pesFilhos'         => $this->getPesFilhos(),
            //            'necessidade'       => $this->getNecessidade(),
            'pesDocEstrangeiro' => $this->getPesDocEstrangeiro(),
            'pesEmissorRg'      => $this->getPesEmissorRg(),
            'pesNascUf'         => $this->getPesNascUf(),
            'pesFalecido'       => $this->getPesFalecido(),
            'pesProfissao'      => $this->getPesProfissao(),
            'pesEmpresa'        => $this->getPesEmpresa(),
            'pesNaturalidade'   => $this->getPesNaturalidade(),
        );

        if (!$pes) {
            $pes = new \Pessoa\Entity\Pessoa(array());
        }

        return array_merge($pes->toArray(), $arrData);
    }
}
