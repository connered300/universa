<?php

namespace Sistema;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $em       = $e->getApplication()->getServiceManager()->get('Doctrine\ORM\EntityManager');
        $platform = $em->getConnection()->getDatabasePlatform();
        $platform->registerDoctrineTypeMapping('enum', 'string');
        $platform->registerDoctrineTypeMapping('set', 'string');
    
        //Resolvendo problema de log quando faço alguma execução via terminal, pois não existe uri
        if(!\Zend\Console\Console::isConsole()){
            $this->recordDataLog($e);
        }
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getViewHelperConfig()
    {
        return array(
            'invokables' => array(),
            'factories'  => array(
                'sisConfig' => function ($sm, $param = false) {
                    $config    = $sm->getServiceLocator()->get("config");
                    $sisConfig = new \Sistema\ViewHelper\SisConfig(
                        $sm->getServiceLocator()->get('Doctrine\ORM\EntityManager'),
                        $config
                    );

                    return $sisConfig;
                }
            )
        );
    }

    public function recordDataLog(MvcEvent $e)
    {   
        $session      = new \Zend\Authentication\Storage\Session();
        $arrUsuario   = $session->read();
        $request      = $e->getApplication()->getRequest();
        $arrDados     = array_merge($_GET, $_POST);
        $dataAtual    = new \DateTime();
        $usuarioLogin = ($arrUsuario['login'] ? $arrUsuario['login'] : 'anonimo');

        $naoIgnorar = (
            substr_count($request->getRequestUri(), '/search') == 0 &&
            substr_count($request->getRequestUri(), '/index') == 0 &&
            substr_count($request->getRequestUri(), '/arquivo/thumb') == 0
        );

        if ($usuarioLogin && ($naoIgnorar)) {
            $text = (
                'Hora: ' . $dataAtual->format('H:i:s') .
                ' | ' .
                'IP: ' . $request->getServer()->get('REMOTE_ADDR') .
                ' | ' .
                'Requisicao: ' . $request->getRequestUri()
            );

            if (count($arrDados) > 0) {
                $arrSenhas = [
                    "senha",
                    "password",
                    "passwordCheck",
                    "pessoaSenha",
                    "pessoaSenhaConfirmacao",
                    "atualizarTituloSenha",
                    "novoTituloSenha",
                    "pagamentoSenha",
                    "renegociacaoSenha",
                    "senhaSupervisor",
                    "senhaTroca",
                    "confirmNovaSenha"
                ];

                foreach ($arrSenhas as $chaveSenha) {
                    if ($arrDados[$chaveSenha]) {
                        $arrDados[$chaveSenha] = 'REMOVIDO';
                    }
                }

                $text .= " | Dados: " . json_encode($arrDados);
            }

            $local = getcwd() . '/data/log/' . $dataAtual->format('Y-m-d');

            if (!is_dir($local)) {
                mkdir($local, 0777, true);
            }

            $arquivo = $local . '/' . $usuarioLogin . '.txt';

            @file_put_contents($arquivo, $text . PHP_EOL, FILE_APPEND | LOCK_EX);
        }
    }
}
