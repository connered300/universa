<?php

namespace Sistema;

return array(
    'router'                    => array(
        'routes' => array(
            'sistema' => array(
                'type'          => 'Literal',
                'options'       => array(
                    'route'    => '/sistema',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Sistema\Controller',
                        'controller'    => 'SisIntegracao',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes'  => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'       => '/[:controller[/:action[/:id]]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id'         => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults'    => array(
                                'action' => 'index'
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'controllers'               => array(
        'invokables' => array(
            'Sistema\Controller\SisFilaEmail'          => 'Sistema\Controller\SisFilaEmailController',
            'Sistema\Controller\SisFilaSms'            => 'Sistema\Controller\SisFilaSmsController',
            'Sistema\Controller\SisIntegracao'         => 'Sistema\Controller\SisIntegracaoController',
            'Sistema\Controller\SisFilaIntegracao'     => 'Sistema\Controller\SisFilaIntegracaoController',
            'Sistema\Controller\SisRobo'               => 'Sistema\Controller\SisRoboController',
            'Sistema\Controller\SisConfig'             => 'Sistema\Controller\SisConfigController',
            'Sistema\Controller\SisConfigPainel'       => 'Sistema\Controller\SisConfigPainelController',
            'Sistema\Controller\SisCampoEntidade'      => 'Sistema\Controller\SisCampoEntidadeController',
            'Sistema\Controller\SisCampoPersonalizado' => 'Sistema\Controller\SisCampoPersonalizadoController',
            'Sistema\Controller\SisCampoTipo'          => 'Sistema\Controller\SisCampoTipoController',
            'Sistema\Controller\SisCampoValor'         => 'Sistema\Controller\SisCampoValorController',
        ),
    ),
    'service_manager'           => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases'            => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'doctrine'                  => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default'             => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            ),
        ),
    ),
    'view_manager'              => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'console'                   => array(
        'router' => array(
            'routes' => array(),
        ),
    ),
    'funcionalidades_liberadas' => array(
        'controllers' => array(),
        'actions'     => array(
            'executar',
            'correcao-automatica',
        ),
    ),
    'namesDictionary'           => array(
        'Sistema\SisFilaIntegracao' => 'Fila de integração',
        'Sistema\SisFilaEmail'      => 'Fila de e-mail',
        'Sistema\SisFilaSms'        => 'Fila de SMS',
        'Sistema\SisIntegracao'     => 'Integrações',
        'Sistema\SisMaxipago'       => 'Maxipago',
        'Sistema\SisRobo'           => 'Robôs',
        'Sistema\SisConfig'         => 'Configurações',
        'Sistema\SisConfigPainel'   => 'Configurações do Sistema',
    )
);