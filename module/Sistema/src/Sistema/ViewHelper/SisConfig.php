<?php

namespace Sistema\ViewHelper;

use Zend\Form\View\Helper\FormElement;
use Zend\Form\View\Helper\FormLabel;
use Zend\View\Helper\AbstractHelper;

class SisConfig extends AbstractHelper
{
    /**
     *
     * @var \Sistema\Service\SisConfig $sisConfig
     */
    private $sisConfig;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, $config = array())
    {
        $this->sisConfig = new \Sistema\Service\SisConfig($em, $config);
    }

    /**
     * Retorna o próprio objeto
     * @return \Sistema\ViewHelper\SisConfig
     */
    public function __invoke()
    {
        return $this;
    }

    public function localizarChave($chave)
    {
        return $this->sisConfig->localizarChave($chave);
    }

    public function criarCampo($name, $dadosCampo = array())
    {
        if (isset($dadosCampo['type'])) {
            $type = strtolower($dadosCampo['type']);

            switch ($type) {
                case "text":
                    $element = new \Zend\Form\Element\Text($name);
                    break;
                case "textarea":
                    $element = new \Zend\Form\Element\Textarea($name);
                    break;
                case "number":
                    $element = new \Zend\Form\Element\Number($name);
                    break;
                case "hidden":
                    $element = new \Zend\Form\Element\Hidden($name);
                    break;
                case "url":
                    $element = new \Zend\Form\Element\Url($name);
                    break;
                case "file":
                    $element = new \Zend\Form\Element\File($name);
                    break;
                case "csrf":
                    $element = new \Zend\Form\Element\Csrf($name);
                    break;
                case "password":
                    $element = new \Zend\Form\Element\Password($name);
                    break;
                default:
                    throw new \Zend\Form\Exception\InvalidElementException("Não existe o type informado $type.");
                    break;
            }
        } elseif (isset($dadosCampo["options"])) {
            $element = new \Zend\Form\Element\Select($name);
        } else {
            throw new \Zend\Form\Exception\InvalidElementException("Ocorreu um errro ao renderizar o campo $name.");
        }

        $element->setAttributes($dadosCampo);

        $label = $dadosCampo['label'];

        if (!isset($dadosCampo['label'])) {
            $label = ucfirst($name);
        }

        $element->setLabel($label);
        $element->setLabelAttributes(["for" => $label]);

        return $this->getView()->formLabel($element) . $this->getView()->formElement($element);
    }

}
