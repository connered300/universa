<?php

namespace Sistema\Service;

use VersaSpine\Service\AbstractService;

class SisCampoTipo extends AbstractService
{

    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Sistema\Entity\SisCampoTipo');
    }

    public function pesquisaForJson($params)
    {
        $sql = '
        SELECT *
        FROM sis__campo_tipo
        WHERE
            tipo_descricao LIKE :tipo_descricao';

        $tipoDescricao = false;
        $tipoCampo     = false;

        if ($params['q']) {
            $tipoDescricao = $params['q'];
        } elseif ($params['query']) {
            $tipoDescricao = $params['query'];
        }

        if ($params['tipoCampo']) {
            $tipoCampo = $params['tipoCampo'];
        }

        $parameters = array('tipo_descricao' => "{$tipoDescricao}%");

        if ($tipoCampo) {
            $parameters['tipo_campo'] = $tipoCampo;
            $sql .= ' AND tipo_campo <> :tipo_campo';
        }

        $sql .= " ORDER BY tipo_descricao";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['tipoCampoDescOrigin'] && $arrDados['tipoCampoOrigin']) {
                /** @var $objSisCampoTipo \Sistema\Entity\SisCampoTipo */
                $objSisCampoTipo = $this->getRepository()->findOneBy(
                    ['tipoDescricao' => $arrDados['tipoCampoDescOrigin'], 'tipoCampo' => $arrDados['tipoCampoOrigin']]
                );
            }

            if (!$objSisCampoTipo) {
                $objSisCampoTipo = new \Sistema\Entity\SisCampoTipo();
            }

            $objSisCampoTipo->setTipoCampo($arrDados['tipoCampo']);
            $objSisCampoTipo->setTipoDescricao($arrDados['tipoDescricao']);

            $this->getEm()->persist($objSisCampoTipo);
            $this->getEm()->flush($objSisCampoTipo);

            $this->getEm()->commit();

            $arrDados['tipoCampo'] = $objSisCampoTipo->getTipoCampo();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de campo tipo!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['tipoDescricao']) {
            $errors[] = 'Por favor preencha o campo "Descrição"!';
        }
        if (!$arrParam['tipoCampo']) {
            $errors[] = 'Por favor preencha o campo "Tipo do campo"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM sis__campo_tipo";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($tipoCampo)
    {
        $arrDados = array();

        if (!$tipoCampo) {
            $this->setLastError('Campo tipo inválido!');

            return array();
        }

        /** @var $objSisCampoTipo \Sistema\Entity\SisCampoTipo */
        $objSisCampoTipo = $this->getRepository()->find($tipoCampo);

        if (!$objSisCampoTipo) {
            $this->setLastError('Campo tipo não existe!');

            return array();
        }

        try {
            $arrDados = $objSisCampoTipo->toArray();
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['tipoCampo'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['tipoDescricao' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Sistema\Entity\SisCampoTipo */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getTipoCampo();
            $arrEntity[$params['value']] = $objEntity->getTipoDescricao();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($tipoCampo)
    {
        if (!$tipoCampo) {
            $this->setLastError('Para remover um registro de campo tipo é necessário informar o código.');

            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            /** @var $objSisCampoTipo \Sistema\Entity\SisCampoTipo */
            $objSisCampoTipo = $this->getRepository()->find($tipoCampo);

            $this->getEm()->remove($objSisCampoTipo);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $ex->getMessage();

            return false;
        }

        return true;
    }
}
?>