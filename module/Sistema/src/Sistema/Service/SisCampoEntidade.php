<?php

namespace Sistema\Service;

use VersaSpine\Service\AbstractService;

class SisCampoEntidade extends AbstractService
{

    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Sistema\Entity\SisCampoEntidade');
    }

    public function pesquisaForJson($params)
    {
        $sql = '
        SELECT *
        FROM sis__campo_entidade
        WHERE
            entidade_descricao LIKE :entidade_descricao';

        $entidadeDescricao = false;
        $entidadeId        = false;

        if ($params['q']) {
            $entidadeDescricao = $params['q'];
        } elseif ($params['query']) {
            $entidadeDescricao = $params['query'];
        }

        if ($params['entidadeId']) {
            $entidadeId = $params['entidadeId'];
        }

        $parameters = array('entidade_descricao' => "{$entidadeDescricao}%");

        if ($entidadeId) {
            $parameters['entidade_id'] = $entidadeId;
            $sql .= ' AND entidade_id <> :entidade_id';
        }

        $sql .= " ORDER BY entidade_descricao";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['entidadeId']) {
                /** @var $objSisCampoEntidade \Sistema\Entity\SisCampoEntidade */
                $objSisCampoEntidade = $this->getRepository()->find($arrDados['entidadeId']);

                if (!$objSisCampoEntidade) {
                    $objSisCampoEntidade = new \Sistema\Entity\SisCampoEntidade();
                    $objSisCampoEntidade->setEntidadeId($arrDados['entidadeId']);
                }
            }

            $objSisCampoEntidade->setEntidadeDescricao($arrDados['entidadeDescricao']);
            $objSisCampoEntidade->setEntidadeTabela($arrDados['entidadeTabela']);
            $objSisCampoEntidade->setEntidadeChave($arrDados['entidadeChave']);

            $this->getEm()->persist($objSisCampoEntidade);
            $this->getEm()->flush($objSisCampoEntidade);

            $this->getEm()->commit();

            $arrDados['entidadeId'] = $objSisCampoEntidade->getEntidadeId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de campo entidade!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['entidadeId']) {
            $errors[] = 'Por favor preencha o campo "Entidade"!';
        }

        if (!$arrParam['entidadeDescricao']) {
            $errors[] = 'Por favor preencha o campo "descrição"!';
        }

        if (!$arrParam['entidadeTabela']) {
            $errors[] = 'Por favor preencha o campo "tabela"!';
        }

        if (!$arrParam['entidadeChave']) {
            $errors[] = 'Por favor preencha o campo "chave"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM sis__campo_entidade";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($entidadeId)
    {
        $arrDados = array();

        if (!$entidadeId) {
            $this->setLastError('Campo entidade inválido!');

            return array();
        }

        /** @var $objSisCampoEntidade \Sistema\Entity\SisCampoEntidade */
        $objSisCampoEntidade = $this->getRepository()->find($entidadeId);

        if (!$objSisCampoEntidade) {
            $this->setLastError('Campo entidade não existe!');

            return array();
        }

        try {
            $arrDados = $objSisCampoEntidade->toArray();
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['entidadeId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['entidadeDescricao' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Sistema\Entity\SisCampoEntidade */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getEntidadeId();
            $arrEntity[$params['value']] = $objEntity->getEntidadeDescricao();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($entidadeId)
    {
        $serviceSisCampo = new \Sistema\Service\SisCampoPersonalizado($this->getEm());

        if (!$entidadeId) {
            $this->setLastError('Para remover um registro de campo entidade é necessário informar o código.');

            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            /** @var $objSisCampoEntidade \Sistema\Entity\SisCampoEntidade */
            $objSisCampoEntidade = $this->getRepository()->findOneBy($entidadeId);

            /** @var $objSisCampo \Sistema\Entity\SisCampoPersonalizado */
            $objSisCampo = $serviceSisCampo->getRepository()->findOneBy(
                ['entidadeId' => $objSisCampoEntidade]
            );

            if ($objSisCampo) {
                $this->setLastError(
                    "Esta entidade possui vinculo com campo : " .
                    $objSisCampo->getCampoNome() . " ,remova e tenta novamente!"
                );

                return false;
            }

            $this->getEm()->remove($objSisCampoEntidade);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de campo entidade.');

            return false;
        }

        return true;
    }

    /**
     * @param $arrParam
     * @return bool|array
     */
    public function buscaRegistrosInternos($arrParam = array(), $select2 = true)
    {
        if (!$arrParam) {
            return false;
        }

        $retorno = $this->retornaDadosTable($arrParam, $select2);

        return array_filter($retorno);
    }

    /**
     * @param $arrParam
     * @return bool|array
     */
    protected function retornaDadosTable($arrParam = array(), $select2 = true)
    {
        $retorno    = array();
        $arrRetorno = array();

        if (empty($arrParam)) {
            $this->setLastError("Nenhum dado informado!");

            return false;
        }

        switch ($arrParam['filter']) {
            case 'tables':

                $retorno = $this->executeQuery("SHOW TABLES")->fetchAll(\PDO::FETCH_COLUMN);

                if ($select2 && !empty($retorno)) {
                    array_walk(
                        $retorno,
                        function (&$a) {
                            $a = ['id' => $a, 'text' => $a];
                        }
                    );
                }
                break;

            case 'columns':

                if (!$arrParam['table']) {
                    $this->setLastError("Nenhuma tabela informada");
                    break;
                }
                /*TODO: Analisar melhoria. não aceita executeQueryWithParam*/
                $retorno = $this->executeQuery("DESC " . $arrParam['table'])->fetchAll();

                if ($select2 && !empty($retorno)) {
                    array_walk(
                        $retorno,
                        function (&$a) {
                            $a = ['id' => $a['Field'], 'text' => $a['Field']];
                        }
                    );
                }

                break;

            default:
                return false;
        }

        return $retorno;
    }
}
?>