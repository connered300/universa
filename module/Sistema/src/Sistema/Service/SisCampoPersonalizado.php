<?php

namespace Sistema\Service;

use VersaSpine\Service\AbstractService;

class SisCampoPersonalizado extends AbstractService
{
    const CAMPO_OBRIGATORIO_SIM      = 'Sim';
    const CAMPO_OBRIGATORIO_NAO      = 'Não';
    const CAMPO_VISIVEL_PAINEL_SIM   = 'Sim';
    const CAMPO_VISIVEL_PAINEL_NAO   = 'Não';
    const CAMPO_EXIBICAO_EXTERNA_SIM = 'Sim';
    const CAMPO_EXIBICAO_EXTERNA_NAO = 'Não';
    const CAMPO_ATIVO_SIM            = 'Sim';
    const CAMPO_ATIVO_NAO            = 'Não';
    const CAMPO_MULTIPLO_SIM         = 'Sim';
    const CAMPO_MULTIPLO_NAO         = 'Não';
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2CampoObrigatorio($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getCampoObrigatorio());
    }

    public static function getCampoObrigatorio()
    {
        return array(self::CAMPO_OBRIGATORIO_SIM, self::CAMPO_OBRIGATORIO_NAO);
    }

    public function getArrSelect2CampoVisivelPainel($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getCampoVisivelPainel());
    }

    public static function getCampoVisivelPainel()
    {
        return array(self::CAMPO_VISIVEL_PAINEL_SIM, self::CAMPO_VISIVEL_PAINEL_NAO);
    }

    public function getArrSelect2CampoExibicaoExterna($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getCampoExibicaoExterna());
    }

    public static function getCampoExibicaoExterna()
    {
        return array(self::CAMPO_EXIBICAO_EXTERNA_SIM, self::CAMPO_EXIBICAO_EXTERNA_NAO);
    }

    public function getArrSelect2CampoAtivo($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getCampoAtivo());
    }

    public static function getCampoAtivo()
    {
        return array(self::CAMPO_ATIVO_SIM, self::CAMPO_ATIVO_NAO);
    }

    public function getArrSelect2CampoMultiplo($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getCampoMultiplo());
    }

    public static function getCampoMultiplo()
    {
        return array(self::CAMPO_MULTIPLO_SIM, self::CAMPO_MULTIPLO_NAO);
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Sistema\Entity\SisCampoPersonalizado');
    }

    public function pesquisaForJson($params, $limit = true)
    {
        $sql = '
        SELECT
         sis__campo_entidade.*,
         sis__campo_personalizado.*,
         tipo_campo as "type",
         campo_nome as "label",
         @class:="form-control" as  "class",
         campo_descricao as "placeholder",
         concat("campo-", campo_id) as id,
        if(tipo_campo="textarea", 12, 4) as tamanho,
        COALESCE(campo_valor, campo_valor_padrao) resposta,
        COALESCE(campo_valor, campo_valor_padrao) value
        FROM sis__campo_personalizado
        INNER JOIN sis__campo_entidade USING (entidade_id)
        LEFT JOIN sis__campo_valor USING(campo_id)
        WHERE campo_descricao LIKE :campo_descricao';

        $campoDescricao = false;
        $campoId        = false;

        if ($params['q']) {
            $campoDescricao = $params['q'];
        } elseif ($params['query']) {
            $campoDescricao = $params['query'];
        }

        if ($params['campoId']) {
            $campoId = $params['campoId'];
        }

        $parameters = array('campo_descricao' => "{$campoDescricao}%");

        if ($campoId) {
            $parameters['campo_id'] = $campoId;
            $sql .= ' AND campo_id IN ( :campo_id )';
        }

        if ($params['exibicaoExterna']) {
            $sql .= ' AND campo_exibicao_externa="' . self::CAMPO_EXIBICAO_EXTERNA_SIM . '"';
        }

        if ($params['campoAtivo']) {
            $sql .= ' AND campo_ativo="' . self::CAMPO_ATIVO_SIM . '"';
        }

        if ($params['entidade']) {
            $parameters['entidade'] = $params['entidade'];
            $sql .= ' AND entidade_tabela IN (:entidade) ';
        }

        if ($params['campoChave']) {
            $sql .= ' AND (campo_chave IN (' . $params['campoChave'] . ') OR sis__campo_valor.campo_id IS NULL)';
        }

        $sql .= " ORDER BY campo_descricao";

        if ($limit) {
            $sql .= " LIMIT 0,40";
        }

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        $serviceSisTipoCampo     = new \Sistema\Service\SisCampoTipo($this->getEm());
        $serviceSisCampoEntidade = new \Sistema\Service\SisCampoEntidade($this->getEm());

        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['campoId']) {
                /** @var $objSisCampoPersonalizado \Sistema\Entity\SisCampoPersonalizado */
                $objSisCampoPersonalizado = $this->getRepository()->find($arrDados['campoId']);

                if (!$objSisCampoPersonalizado) {
                    $this->setLastError('Registro de campo personalizado não existe!');

                    return false;
                }
            } else {
                $objSisCampoPersonalizado = new \Sistema\Entity\SisCampoPersonalizado();
            }

            /** @var $objSisTipoCampo \Sistema\Entity\SisCampoTipo */
            $objSisTipoCampo =
                $serviceSisTipoCampo->getRepository()->findOneBy(['tipoCampo' => $arrDados['campoTipo']]);

            if (!$objSisTipoCampo) {
                $this->setLastError("Campo tipo não localizado!");

                return false;
            }

            /** @var $objsisCampoEntidade \Sistema\Entity\SisCampoEntidade */
            $objsisCampoEntidade = $serviceSisCampoEntidade
                ->getRepository()->findOneBy(['entidadeId' => $arrDados['campoEntidade']]);

            if (!$objsisCampoEntidade) {
                $this->setLastError("Entidade não localizada!");

                return false;
            }

            $objSisCampoPersonalizado->setCampoNome($arrDados['campoNome']);
            $objSisCampoPersonalizado->setCampoDescricao($arrDados['campoDescricao']);
            $objSisCampoPersonalizado->setCampoTamanhoMin($arrDados['campoTamanhoMin']);
            $objSisCampoPersonalizado->setCampoTamanhoMax($arrDados['campoTamanhoMax']);
            $objSisCampoPersonalizado->setCampoExpressaoRegular($arrDados['campoExpressaoRegular']);
            $objSisCampoPersonalizado->setCampoValorPadrao($arrDados['campoValorPadrao']);
            $objSisCampoPersonalizado->setCampoObrigatorio($arrDados['campoObrigatorio']);
            $objSisCampoPersonalizado->setCampoVisivelPainel($arrDados['campoVisivelPainel']);
            $objSisCampoPersonalizado->setCampoExibicaoExterna($arrDados['campoExibicaoExterna']);
            $objSisCampoPersonalizado->setCampoAtivo($arrDados['campoAtivo']);
            $objSisCampoPersonalizado->setCampoMultiplo($arrDados['campoMultiplo']);
            $objSisCampoPersonalizado->setCampoValores($arrDados['campoValores']);
            $objSisCampoPersonalizado->setEntidade($objsisCampoEntidade);
            $objSisCampoPersonalizado->setTipoCampo($objSisTipoCampo);

            $this->getEm()->persist($objSisCampoPersonalizado);
            $this->getEm()->flush($objSisCampoPersonalizado);

            $this->getEm()->commit();

            $arrDados['campoId'] = $objSisCampoPersonalizado->getCampoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de campo personalizado!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['campoNome']) {
            $errors[] = 'Por favor preencha o campo "nome"!';
        }

        if (!in_array($arrParam['campoObrigatorio'], self::getCampoObrigatorio())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "obrigatorio"!';
        }

        if (!in_array($arrParam['campoVisivelPainel'], self::getCampoVisivelPainel())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "painel visível"!';
        }

        if (!in_array($arrParam['campoExibicaoExterna'], self::getCampoExibicaoExterna())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "externa exibição"!';
        }

        if (!in_array($arrParam['campoAtivo'], self::getCampoAtivo())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "ativo"!';
        }

        if (!in_array($arrParam['campoMultiplo'], self::getCampoMultiplo())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "multiplo"!';
        }

        if (!$arrParam['campoEntidade']) {
            $errors[] = 'Por favor selecione um valor válido para o campo "entidade"!';
        }

        if (!$arrParam['campoTipo']) {
            $errors[] = 'Por favor selecione um valor válido para o campo "campo tipo"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM sis__campo_personalizado";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($campoId)
    {
        $arrDados = array();

        if (!$campoId) {
            $this->setLastError('Campo personalizado inválido!');

            return array();
        }

        /** @var $objSisCampoPersonalizado \Sistema\Entity\SisCampoPersonalizado */
        $objSisCampoPersonalizado = $this->getRepository()->find($campoId);

        if (!$objSisCampoPersonalizado) {
            $this->setLastError('Campo personalizado não existe!');

            return array();
        }

        try {
            $arrDados = $objSisCampoPersonalizado->toArray();
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['campoId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['campoDescricao' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Sistema\Entity\SisCampoPersonalizado */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getCampoId();
            $arrEntity[$params['value']] = $objEntity->getCampoDescricao();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($campoId)
    {
        $serviceCampoValor = new \Sistema\Service\SisCampoValor($this->getEm());

        if (!$campoId) {
            $this->setLastError('Para remover um registro de campo personalizado é necessário informar o código.');

            return false;
        }

        /** @var $objSisCampoValor \Sistema\Entity\SisCampoValor */
        $objSisCampoValor =
            $serviceCampoValor->getRepository()->findOneBy(['campo' => $campoId]);

        if ($objSisCampoValor) {
            $this->setLastError(
                "Existe um registro no 'Campo Valor' vinculado a este registro, remova-o para continuar!"
            );

            return false;
        }

        try {
            /** @var $objSisCampoPersonalizado \Sistema\Entity\SisCampoPersonalizado */
            $objSisCampoPersonalizado = $this->getRepository()->find($campoId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objSisCampoPersonalizado);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de campo personalizado.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $view->setVariable("arrCampoObrigatorio", $this->getArrSelect2CampoObrigatorio());
        $view->setVariable("arrCampoVisivelPainel", $this->getArrSelect2CampoVisivelPainel());
        $view->setVariable("arrCampoExibicaoExterna", $this->getArrSelect2CampoExibicaoExterna());
        $view->setVariable("arrCampoAtivo", $this->getArrSelect2CampoAtivo());
        $view->setVariable("arrCampoMultiplo", $this->getArrSelect2CampoMultiplo());
    }
}
?>