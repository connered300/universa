<?php

namespace Sistema\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class SisIntegracaoCurso
 * @package Sistema\Service
 */
class SisIntegracaoCurso extends AbstractService
{
    /**
     * @var null
     */
    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Sistema\Entity\SisIntegracaoCurso');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @param \Sistema\Entity\SisIntegracao|int $integracao
     * @param \Matricula\Entity\AcadCurso|int   $curso
     * @return bool|string
     */
    public function pesquisarCodigoIntegracao($integracao, $curso)
    {
        /** @var $objSisIntegracaoCurso \Sistema\Entity\SisIntegracaoCurso */
        $objSisIntegracaoCurso = $this->getRepository()->findOneBy(
            ['integracao' => $integracao, 'curso' => $curso]
        );

        return $objSisIntegracaoCurso ? $objSisIntegracaoCurso->getCodigo() : false;
    }

    /**
     * @param $integracaoId
     * @param $cursoId
     * @param $codigo
     * @return bool
     */
    public function registrarIntegracaoCurso($integracaoId, $cursoId, $codigo)
    {
        if (!$integracaoId) {
            $this->setLastError('Informe o código da integração!');

            return false;
        }

        if (!$cursoId) {
            $this->setLastError('Informe o código do curso!');

            return false;
        }

        if (!$codigo) {
            $this->setLastError('Informe o código retornado pela integração!');

            return false;
        }

        try {
            $serviceSisIntegracao = new \Sistema\Service\SisIntegracao($this->getEm());
            $serviceAcadCurso     = new \Matricula\Service\AcadCurso($this->getEm());

            /** @var $objSisIntegracao \Sistema\Entity\SisIntegracao */
            $objSisIntegracao = $serviceSisIntegracao->getRepository()->find($integracaoId);
            /** @var $objAcadCurso \Matricula\Entity\AcadCurso */
            $objAcadCurso = $serviceAcadCurso->getRepository()->find($cursoId);

            $objSisIntegracaoCurso = new \Sistema\Entity\SisIntegracaoCurso();

            $objSisIntegracaoCurso
                ->setIntegracao($objSisIntegracao)
                ->setCurso($objAcadCurso)
                ->setCodigo($codigo);

            $this->getEm()->beginTransaction();
            $this->getEm()->persist($objSisIntegracaoCurso);
            $this->getEm()->flush($objSisIntegracaoCurso);
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao registrar integração de curso.');

            return false;
        }

        return true;
    }

    /**
     * @param $codigoCurso
     * @return \Matricula\Entity\CampusCurso | null
     */
    public function retornarCursoCamposPeloCodigoEIntegracao($codigoCurso, $integracao)
    {
        /** @var \Sistema\Entity\SisIntegracaoCurso $objSisIntegracaoCurso */
        $objSisIntegracaoCurso = $this->getRepository()->findOneBy(
            ['codigo' => $codigoCurso, 'integracao' => $integracao]
        );

        if ($objSisIntegracaoCurso) {
            $serviceCampusCurso         = new \Matricula\Service\CampusCurso($this->getEm());
            $serviceSisIntegracaoCampus = new \Sistema\Service\SisIntegracaoCampus($this->getEm());

            $arrCampus         = [];
            $arrObjCampusCurso = $serviceCampusCurso->retornaCampusCursoExistente(
                false,
                $objSisIntegracaoCurso->getCurso()
            );

            /** @var \Matricula\Entity\CampusCurso $objCampusCurso */
            foreach ($arrObjCampusCurso as $objCampusCurso) {
                $arrCampus[] = $objCampusCurso->getCamp()->getCampId();
            }

            $arrObjSisIntegracaoCampus = $serviceSisIntegracaoCampus->retornaIntegracaoCampusExistente(
                $arrCampus,
                $objSisIntegracaoCurso->getIntegracao()
            );

            if (!$arrObjSisIntegracaoCampus) {
                return null;
            }

            /** @var \Sistema\Entity\SisIntegracaoCampus $objSisIntegracaoCampus */
            $objSisIntegracaoCampus = array_shift($arrObjSisIntegracaoCampus);
            /* TODO: Corrigir retorno, pois caso o curso esteja vinculado a mais de um campus ocorrerá um erro */
            $arrObjCampusCurso = $serviceCampusCurso->retornaCampusCursoExistente(
                $objSisIntegracaoCampus->getCampus(),
                $objSisIntegracaoCurso->getCurso()
            );

            /** @var \Matricula\Entity\CampusCurso $objCampusCurso */
            $objCampusCurso = $arrObjCampusCurso ? $arrObjCampusCurso[0] : null;

            return $objCampusCurso;
        }

        return false;
    }

    /**
     * @param $dados
     */
    protected function valida($dados)
    {
    }

    /**
     * @param $criterios
     */
    protected function pesquisaForJson($criterios)
    {
    }
}
?>