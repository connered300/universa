<?php

namespace Sistema\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class SisIntegracaoAluno
 * @package Sistema\Service
 */
class SisIntegracaoAluno extends AbstractService
{
    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Sistema\Entity\SisIntegracaoAluno');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @param \Sistema\Entity\SisIntegracao|int    $integracao
     * @param \Matricula\Entity\AcadgeralAluno|int $aluno
     * @return bool|string
     */
    public function pesquisarCodigoIntegracao($integracao, $aluno)
    {
        /** @var $objSisIntegracaoAluno \Sistema\Entity\SisIntegracaoAluno */
        $objSisIntegracaoAluno = $this->getRepository()->findOneBy(
            ['integracao' => $integracao, 'aluno' => $aluno]
        );

        return $objSisIntegracaoAluno ? $objSisIntegracaoAluno->getCodigo() : false;
    }

    /**
     * @param \Sistema\Entity\SisIntegracao|int $integracao
     * @param int                               $codigo
     * @return bool|string
     */
    public function pesquisarAlunoCodigoIntegracao($integracao, $codigo)
    {
        /** @var $objSisIntegracaoAluno \Sistema\Entity\SisIntegracaoAluno */
        $objSisIntegracaoAluno = $this->getRepository()->findOneBy(
            ['integracao' => $integracao, 'codigo' => $codigo]
        );

        return $objSisIntegracaoAluno ? $objSisIntegracaoAluno->getAluno()->getAlunoId() : false;
    }

    /**
     * @param \Sistema\Entity\SisIntegracao|int $integracao
     * @param int                               $codigo
     * @return bool|\Matricula\Entity\AcadgeralAluno
     */
    public function pesquisarObjAlunoPeloCodigoIntegracao($integracao, $codigo)
    {
        /** @var $objSisIntegracaoAluno \Sistema\Entity\SisIntegracaoAluno */
        $objSisIntegracaoAluno = $this->getRepository()->findOneBy(
            ['integracao' => $integracao, 'codigo' => $codigo]
        );

        return $objSisIntegracaoAluno ? $objSisIntegracaoAluno->getAluno() : false;
    }

    /**
     * @param $codigoAluno
     * @return \Matricula\Entity\AcadgeralAluno
     */
    public function retornarAlunoPeloCodigoDeIntegracao($codigoAluno)
    {
        $alunoId = $this
            ->executeQuery("SELECT aluno_id FROM sis__integracao_aluno WHERE codigo = {$codigoAluno}")
            ->fetch();

        $alunoId = $alunoId['aluno_id'] ? $alunoId['aluno_id'] : null;

        $serviceAcadgeralAluno = new \Matricula\Service\AcadgeralAluno($this->getEm());

        return $serviceAcadgeralAluno->getArray($alunoId);
    }

    /**
     * @param $alunoId
     * @return array|null
     */
    public function retornaCodigoPorAluno($alunoId)
    {
        if (!$alunoId) {
            $this->setLastError('Informe um aluno para busca');

            return false;
        }

        $query    = "
        SELECT  i.codigo, a.integracao_descricao FROM sis__integracao_aluno i
        inner join sis__integracao a using(integracao_id)
        where i.aluno_id={$alunoId}";
        $arrDados = $this->executeQuery($query)->fetchAll();

        return $arrDados;
    }

    /**
     * @param $alunoId
     * @return string
     */
    public function retornaCodigoPorAlunoFormatado($alunoId)
    {
        $arrRetorno = array();
        $arrDados   = $this->retornaCodigoPorAluno($alunoId);

        foreach ($arrDados as $arrIntegracaoAluno) {
            $arrRetorno[] = $arrIntegracaoAluno['integracao_descricao'] . ": " . $arrIntegracaoAluno['codigo'];
        }

        return $arrRetorno;
    }

    public function retornaAlunoCursoIdIntegracao($integracaoId, $codigoAluno, $codigoCurso = null)
    {
        if (!$integracaoId) {
            $this->setLastError('Informe uma integracao para busca');

            return false;
        }

        if (!$codigoAluno) {
            $this->setLastError('Informe um aluno para busca');

            return false;
        }

        //TODO: ENCONTRAR MELHOR FORMA DE CHEGAR NO ALUNOCURSO ATRAVES DA INTEGRAÇÃO

        $query = "
        SELECT  a.codigo AS codigoAluno, c.codigo AS codigoCurso, a.aluno_id, c.curso_id, ac.alunocurso_id, cc.cursocampus_id, cc.camp_id, aa.pes_id
        FROM sis__integracao i
          INNER JOIN sis__integracao_aluno a USING(integracao_id)
          INNER JOIN acadgeral__aluno aa USING(aluno_id)
          LEFT JOIN acadgeral__aluno_curso ac USING(aluno_id)
          LEFT JOIN campus_curso cc USING(cursocampus_id)
          LEFT JOIN sis__integracao_curso c
              ON c.integracao_id=i.integracao_id AND c.curso_id=cc.curso_id
                 -- CONDICAO-CURSO --
        WHERE i.integracao_id=:integracao_id AND a.codigo=:codigoAluno
        ORDER BY if(c.codigo, 0, 1), ac.alunocurso_id";

        $param = [
            'integracao_id' => $integracaoId,
            'codigoAluno'   => $codigoAluno,
            'codigoCurso'   => $codigoCurso
        ];

        if (!$param['codigoCurso']) {
            unset($param['codigoCurso']);
        } else {
            $query = str_replace('-- CONDICAO-CURSO --', 'AND c.codigo=:codigoCurso', $query);
        }

        $arrDados = $this->executeQueryWithParam($query, $param)->fetch();

        return $arrDados;
    }

    /**
     * @param $integracaoId
     * @param $alunoId
     * @param $codigo
     * @return bool
     */
    public function registrarIntegracaoAluno($integracaoId, $alunoId, $codigo)
    {
        if (!$integracaoId) {
            $this->setLastError('Informe o código da integração!');

            return false;
        }

        if (!$alunoId) {
            $this->setLastError('Informe o código do aluno!');

            return false;
        }

        if (!$codigo) {
            $this->setLastError('Informe o código retornado pela integração!');

            return false;
        }

        try {
            $serviceSisIntegracao  = new \Sistema\Service\SisIntegracao($this->getEm());
            $serviceAcadgeralAluno = new \Matricula\Service\AcadgeralAluno($this->getEm());

            /** @var $objSisIntegracao \Sistema\Entity\SisIntegracao */
            $objSisIntegracao = $serviceSisIntegracao->getRepository()->find($integracaoId);
            /** @var $objAcadgeralAluno \Matricula\Entity\AcadgeralAluno */
            $objAcadgeralAluno = $serviceAcadgeralAluno->getRepository()->find($alunoId);

            $objSisIntegracaoAluno = new \Sistema\Entity\SisIntegracaoAluno();

            $objSisIntegracaoAluno
                ->setIntegracao($objSisIntegracao)
                ->setAluno($objAcadgeralAluno)
                ->setCodigo($codigo);

            $this->getEm()->beginTransaction();
            $this->getEm()->persist($objSisIntegracaoAluno);
            $this->getEm()->flush($objSisIntegracaoAluno);
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao registrar integração de aluno.');

            return false;
        }

        return true;
    }

    /**
     * @param $dados
     */
    protected function valida($dados)
    {
    }

    /**
     * @param $criterios
     */
    protected function pesquisaForJson($criterios)
    {
    }

    /**
     * @param string $chave
     * @return array | bool
     */
    public function retornaDadosDeTokenCriptografado($chave)
    {
        if (!$chave) {
            return false;
        }

        $params = array('chave' => $chave);

        $query = "
            SELECT
              a.aluno_id alunoId,
              integracao.integracao_token token,
              trim(LEADING 0 FROM integracao.integracao_id) integracaoId,
              sis.codigo AS codigoAluno
            FROM
              acadgeral__aluno a
              LEFT JOIN sis__integracao_aluno sis ON a.aluno_id = sis.aluno_id
              LEFT JOIN sis__integracao integracao ON integracao.integracao_id = sis.integracao_id
            WHERE md5(concat(integracao.integracao_token, trim(LEADING 0 FROM sis.codigo))) = :chave
        ";

        try {
            $result = $this->executeQueryWithParam($query, $params)->fetch();
        } catch (\Exception $e) {
            $result = false;
            $this->setLastError($e->getMessage());
        }

        return $result;
    }

    public function buscaCodigoAlunoEmIntegracoesAtivas($alunoId)
    {
        if (!$alunoId) {
            $this->setLastError("Informe um aluno para iniciar a busca");

            return false;
        }
        $service                 = new SisIntegracao($this->getEm());
        $arrObjIntegracoesAtivas = $service->buscaIntegracoesAtivas();

        /** @var \Sistema\Entity\SisIntegracao $objSisIntegracao */
        foreach ($arrObjIntegracoesAtivas as $objSisIntegracao) {
            $codigoIntegracaoAluno = $this->pesquisarCodigoIntegracao($objSisIntegracao->getIntegracaoId(), $alunoId);

            if ($codigoIntegracaoAluno) {
                return array(
                    'codigo'     => $codigoIntegracaoAluno,
                    'integracao' => $objSisIntegracao->getIntegracaoId()
                );
            }
        }

        return false;
    }

    public function buscaInformacoesAlunoPer($codigoIntegracao)
    {
        if (!$codigoIntegracao) {
            $this->setLastError("É necessário informar um código de integração!");

            return false;
        }

        $serviceMatriculaSituacao = new \Matricula\Service\AcadgeralSituacao($this->getEm());
        $preMatricula             = $serviceMatriculaSituacao::PRE_MATRICULA;

        $sql = "
        SELECT
            sia.aluno_id as alunoId,
            aac.alunocurso_id, aac.cursocampus_id,
            ac.curso_id, ac.curso_nome,
            aa.alunoper_id, aa.turma_id, turma.turma_nome, turma.turma_serie,
            turma.per_id, turma.mat_cur_id,
            al.per_matricula_inicio, al.per_matricula_fim
            
        FROM sis__integracao_aluno sia
        
        INNER JOIN acadgeral__aluno_curso aac ON aac.aluno_id=sia.aluno_id
        INNER JOIN acadperiodo__aluno aa ON aa.alunocurso_id=aac.alunocurso_id
        INNER JOIN campus_curso cc ON cc.cursocampus_id=aac.cursocampus_id
        INNER JOIN acad_curso ac ON ac.curso_id=cc.curso_id
        INNER JOIN acadperiodo__turma turma ON turma.turma_id=aa.turma_id
        INNER JOIN acadperiodo__letivo  al ON al.per_id=turma.per_id

        WHERE sia.codigo=:alunoCodigoAva AND aa.matsituacao_id=$preMatricula 
        AND (now() BETWEEN al.per_rematricula_online_inicio AND al.per_rematricula_online_fim)";

        $result = $this->executeQueryWithParam($sql, ['alunoCodigoAva' => $codigoIntegracao])->fetchAll();

        return $result;
    }
}
?>
