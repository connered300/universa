<?php

namespace Sistema\Service;

use VersaSpine\Service\AbstractService;

class SisCampoValor extends AbstractService
{

    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Sistema\Entity\SisCampoValor');
    }

    public function pesquisaForJson($params)
    {
        $sql = '
        SELECT *
        FROM sis__campo_valor ';

        $parameters = array();
        $sql .= " ORDER BY campo_id";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados, $formulario = false)
    {
        $serviceSisCampoPerso = new \Sistema\Service\SisCampoPersonalizado($this->getEm());

        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            /** @var  $objSisCampoPerso \Sistema\Entity\SisCampoPersonalizado */
            $objSisCampoPerso =
                $serviceSisCampoPerso->getRepository()->findOneBy(['campoId' => ($arrDados['campoId'] * 1)]);

            if (!$objSisCampoPerso) {
                $this->setLastError("Campo não localizada!");

                return false;
            }

            if ($arrDados['campoOrigin'] || $arrDados['chaveOrigin']) {
                /** @var $objSisCampoValor \Sistema\Entity\SisCampoValor */
                $objSisCampoValor = $this->getRepository()->findOneBy(
                    [
                        'campo'      => $arrDados['campoOrigin'],
                        'campoChave' => $arrDados['chaveOrigin']
                    ]
                );

                if (!$objSisCampoValor) {
                    $this->setLastError("Campo valor não encontrado!");

                    return false;
                }
            } else {

                $objSisCampoValor = $this->getRepository()->findOneBy(
                    [
                        'campo'      => $objSisCampoPerso,
                        'campoChave' => $arrDados['campoChave']
                    ]
                );

                if (!$formulario) {
                    /** @var $objSisCampoValor \Sistema\Entity\SisCampoValor */
                    if ($objSisCampoValor) {
                        $this->setLastError(
                            "Já existe um registro semelhante a este cod.: " . $objSisCampoValor->getCampo(
                            )->getCampoId()
                        );

                        return false;
                    }

                    $objSisCampoValor = new \Sistema\Entity\SisCampoValor();
                } elseif (!$objSisCampoValor) {
                    $objSisCampoValor = new \Sistema\Entity\SisCampoValor();
                }
            }

            $objSisCampoValor->setCampo($objSisCampoPerso);
            $objSisCampoValor->setCampoChave($arrDados['campoChave']);
            $objSisCampoValor->setCampoValor($arrDados['campoValor']);

            $this->getEm()->persist($objSisCampoValor);
            $this->getEm()->flush($objSisCampoValor);

            $this->getEm()->commit();

            $arrDados['campo'] = $objSisCampoValor->getCampo();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de campo valor !<br>');

            if ($e->getErrorCode() == 1062) {
                $this->setLastError("Já existe um registro com esse campo e esta chave!");
            }
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['campoChave']) {
            $errors[] = 'Por favor preencha o campo "Chave"!';
        }
        if (!$arrParam['campoId']) {
            $errors[] = 'Por favor preencha o campo "Campo"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM sis__campo_valor";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($campoValor)
    {
        $arrDados = array();

        if (!$campoValor) {
            $this->setLastError('Campo tipo inválido!');

            return array();
        }
        unset($campoValor['ajax']);

        /** @var $objSisCampoValor \Sistema\Entity\SisCampoValor */
        $objSisCampoValor = $this->getRepository()->findOneBy($campoValor);

        if (!$objSisCampoValor) {
            $this->setLastError('Campo tipo não existe!');

            return array();
        }

        try {
            $arrDados = $objSisCampoValor->toArray();
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    public function remover($campoValor)
    {
        if (!$campoValor) {
            $this->setLastError('Para remover um registro de campo valor  é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objSisCampoValor \Sistema\Entity\SisCampoValor */
            $objSisCampoValor = $this->getRepository()->findOneBy($campoValor);

            if (!$objSisCampoValor) {
                $this->setLastError("Campo valor não localizado!");

                return false;
            }

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objSisCampoValor);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError(
                $this->getLastError() ? $this->getLastError() :
                    'Falha ao remover registro de campo valor.'
            );

            return false;
        }

        return true;
    }

    public function retornaValorChave($arrDados, $chave)
    {
        $error = array();

        if (empty($arrDados)) {
            $error[] = "dados para consulta";
        }
        if (!$chave) {
            $error[] = "chave a ser buscada";
        }

        if (!empty($error)) {
            $this->setLastError(implode($error));

            return false;
        }

        $chave = $this->retornaCamelCase($chave);

        return isset($arrDados[$chave]) ? $arrDados[$chave] : null;
    }

    public function retornaCamelCase($string, $primeiraLetraMaiuscula = false)
    {

        $str = str_replace(' ', '', ucwords(str_replace(['-', '_'], ' ', $string)));

        if (!$primeiraLetraMaiuscula) {
            $str[0] = strtolower($str[0]);
        }

        return $str;
    }

    /* roolBack configura se é pra não salvar caso ocorra algum erro, no academico isso é para ser ingnorado por hora
        formulario, diz que esta vindo de um formulario e pode ser editado    */
    public function salvaMultiplosBuscandoChave($arrDados, $rollback = true, $formulario = false)
    {
        $serviceSisCampoPersonalizado = new \Sistema\Service\SisCampoPersonalizado($this->getEm());

        if (empty($arrDados)) {
            $this->setLastError("Nenhum dado foi informado para busca!");
        }

        if ($rollback) {
            $this->getEm()->beginTransaction();
        }

        try {
            foreach ($arrDados['camposPersonalizados'] as $index => $value) {

                /** @var $objCampoPersonalizado \Sistema\Entity\SisCampoPersonalizado */
                $objCampoPersonalizado =
                    $serviceSisCampoPersonalizado->getRepository()->findOneBy(['campoId' => $index]);

                $campoChave = $this->retornaValorChave(
                    $arrDados,
                    $objCampoPersonalizado->getEntidade()->getEntidadeChave()
                );

                if (!$objCampoPersonalizado || !$campoChave) {
                    continue;
                }

                $arrSave =
                    [
                        'campoId'    => $objCampoPersonalizado,
                        'campoChave' => $campoChave,
                        'campoValor' => $value
                    ];

                $this->save($arrSave, $formulario);
            }

            if ($rollback) {
                $this->getEm()->commit();
            }

            return true;
        } catch (\Exception $ex) {
            $this->setLastError($ex->getMessage());

            return false;
        }

        return false;
    }

    /*Retorna perguntans e resposta para formulários, a patir da chave (código do campo) e entidade (relação do campo com a tabela) */
    public function retornaPerguntasReposta($arrParam)
    {
        if (!isset($arrParam['entidadeTabela']) || !isset($arrParam['campoChave'])) {
            $this->setLastError("Favor informar a tabela e a chave");

            return false;
        }

        $sql = '
            SELECT campo_nome as pergunta,campo_valor as resposta from sis__campo_personalizado
            LEFT JOIN sis__campo_valor USING(campo_id)
            INNER JOIN sis__campo_entidade USING(entidade_id)
            WHERE 1=1
            ';

        $arrParameters = array();

        if ($arrParam['entidadeTabela']) {
            $sql .= ' AND entidade_tabela IN (:entidadeTabela)';
            $arrParameters['entidadeTabela'] = $arrParam['entidadeTabela'];
        }

        if ($arrParam['campoChave']) {
            $sql .= ' AND campo_chave IN (:campoChave)';
            $arrParameters['campoChave'] = $arrParam['campoChave'];
        }

        return $this->executeQueryWithParam($sql, $arrParameters)->fetchAll();
    }
}
?>