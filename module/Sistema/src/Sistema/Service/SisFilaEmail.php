<?php

namespace Sistema\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class SisFilaEmail
 * @package Sistema\Service
 */
class SisFilaEmail extends AbstractService
{
    const FILA_SITUACAO_PENDENTE    = 'Pendente';
    const FILA_SITUACAO_ENVIADO     = 'Enviado';
    const FILA_SITUACAO_PROCESSANDO = 'Processando';
    const FILA_SITUACAO_ERRO        = 'Erro';
    const FILA_SUCESSO              = 'Sucesso';

    private $__lastError = null;

    /** @var \Sistema\Service\SisConfig */
    private $config = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, $arrConfig = array())
    {
        parent::__construct($em, 'Sistema\Entity\SisFilaEmail');

        $this->setConfig(new \Sistema\Service\SisConfig($em, $arrConfig));
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @return SisConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param SisConfig $config
     * @return SisFilaSms
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function pesquisaForJson($params)
    {
        $sql                  = 'SELECT * FROM sis__fila_email WHERE';
        $filaDestinatarioNome = false;
        $filaId               = false;

        if ($params['q']) {
            $filaDestinatarioNome = $params['q'];
        } elseif ($params['query']) {
            $filaDestinatarioNome = $params['query'];
        }

        if ($params['filaId']) {
            $filaId = $params['filaId'];
        }

        $parameters = array('fila_destinatario_nome' => "{$filaDestinatarioNome}%");
        $sql .= ' fila_destinatario_nome LIKE :fila_destinatario_nome';

        if ($filaId) {
            $parameters['fila_id'] = $filaId;
            $sql .= ' AND fila_id <> :fila_id';
        }

        $sql .= " ORDER BY fila_destinatario_nome";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        if (!$arrDados['conta']) {
            $arrDados['conta'] = \Organizacao\Service\OrgEmailConta::CONTA_PADRAO;
        }

        if (!$arrDados['filaSituacao']) {
            $arrDados['filaSituacao'] = self::FILA_SITUACAO_PENDENTE;
        }

        $serviceOrgEmailConta   = new \Organizacao\Service\OrgEmailConta($this->getEm());
        $serviceComunicacaoTipo = new \Organizacao\Service\OrgComunicacaoTipo($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['filaId']) {
                /** @var $objSisFilaEmail \Sistema\Entity\SisFilaEmail */
                $objSisFilaEmail = $this->getRepository()->find($arrDados['filaId']);

                if (!$objSisFilaEmail) {
                    $this->setLastError('Registro de fila de e-mail não existe!');

                    return false;
                }
            } else {
                $objSisFilaEmail = new \Sistema\Entity\SisFilaEmail();

                $arrDados['filaDataCadastro'] = date('Y-m-d H:i:s');
            }

            if ($arrDados['conta']) {
                /** @var $objOrgEmailConta \Organizacao\Entity\OrgEmailConta */
                $objOrgEmailConta = $serviceOrgEmailConta->getRepository()->find($arrDados['conta']);

                if (!$objOrgEmailConta) {
                    $this->setLastError('Registro de conta de e-mail não existe!');

                    return false;
                }

                $objSisFilaEmail->setConta($objOrgEmailConta);
            } else {
                $objSisFilaEmail->setConta(null);
            }

            if ($arrDados['tipoId']) {
                /** @var \Organizacao\Entity\OrgComunicacaoTipo $objComunicacaoTipo */
                $objComunicacaoTipo = $serviceComunicacaoTipo->getRepository()->find($arrDados['tipoId']);

                if (!$objComunicacaoTipo) {
                    $this->setLastError('Registro de Contexto não existe');

                    return false;
                }

                $objSisFilaEmail->setTipo($objComunicacaoTipo);
            } else {
                $objSisFilaEmail->setTipo(null);
            }

            $objSisFilaEmail->setFilaAssunto($arrDados['filaAssunto']);
            $objSisFilaEmail->setFilaDestinatarioNome($arrDados['filaDestinatarioNome']);
            $objSisFilaEmail->setFilaDestinatarioEmail($arrDados['filaDestinatarioEmail']);
            $objSisFilaEmail->setFilaConteudo($arrDados['filaConteudo']);
            $objSisFilaEmail->setFilaRetorno($arrDados['filaRetorno']);
            $objSisFilaEmail->setFilaDataCadastro($arrDados['filaDataCadastro']);
            $objSisFilaEmail->setFilaDataProcessamento($arrDados['filaDataProcessamento']);
            $objSisFilaEmail->setFilaDataFinalizacao($arrDados['filaDataFinalizacao']);
            $objSisFilaEmail->setFilaSituacao($arrDados['filaSituacao']);

            $this->getEm()->persist($objSisFilaEmail);
            $this->getEm()->flush($objSisFilaEmail);

            $this->getEm()->commit();

            $arrDados['filaId'] = $objSisFilaEmail->getFilaId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de fila de e-mail!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['filaDestinatarioEmail']) {
            $errors[] = 'Por favor preencha o campo "destinatário"!';
        }

        if (!$arrParam['filaConteudo']) {
            $errors[] = 'Por favor preencha o campo "conteúdo"!';
        }

        if ($arrParam['filaSituacao'] && !in_array($arrParam['filaSituacao'], self::getFilaSituacao())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "situação"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public static function getFilaSituacao()
    {
        return array(
            self::FILA_SITUACAO_PENDENTE,
            self::FILA_SITUACAO_ENVIADO,
            self::FILA_SITUACAO_PROCESSANDO,
            self::FILA_SITUACAO_ERRO
        );
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "
        SELECT
        fila_id, fila_assunto, fila_destinatario_nome, fila_destinatario_email, fila_conteudo, fila_retorno,
        fila_data_cadastro, fila_data_processamento, fila_data_finalizacao,
        fila_situacao, usuario, conta_id, conta_nome
        FROM sis__fila_email
        INNER JOIN org__email_conta USING(conta_id)
        ";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $filaId
     * @return array
     */
    public function getArray($filaId)
    {
        /** @var $objSisFilaEmail \Sistema\Entity\SisFilaEmail */
        $objSisFilaEmail      = $this->getRepository()->find($filaId);
        $serviceOrgEmailConta = new \Organizacao\Service\OrgEmailConta($this->getEm());

        try {
            $arrDados = $objSisFilaEmail->toArray();

            if ($arrDados['conta']) {
                /** @var $objOrgEmailConta \Organizacao\Entity\OrgEmailConta */
                $objOrgEmailConta = $serviceOrgEmailConta->getRepository()->find($arrDados['conta']);

                if ($objOrgEmailConta) {
                    $arrDados['conta'] = array(
                        'id'   => $objOrgEmailConta->getContaId(),
                        'text' => $objOrgEmailConta->getContaNome()
                    );
                } else {
                    $arrDados['conta'] = null;
                }
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceOrgEmailConta = new \Organizacao\Service\OrgEmailConta($this->getEm());

        if (is_array($arrDados['conta']) && !$arrDados['conta']['id']) {
            $arrDados['conta']['id']   = $arrDados['conta']['contaId'];
            $arrDados['conta']['text'] = $arrDados['conta']['contaNome'];
        } elseif ($arrDados['conta'] && is_string($arrDados['conta'])) {
            $arrDados['conta'] = $serviceOrgEmailConta->getArrSelect2(
                array('id' => $arrDados['conta'])
            );
        }

        return $arrDados;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('filaId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Sistema\Entity\SisFilaEmail */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getFilaId(),
                $params['value'] => $objEntity->getFilaDestinatarioNome()
            );
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['filaId']) {
            $this->setLastError('Para remover um registro de fila de e-mail é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objSisFilaEmail \Sistema\Entity\SisFilaEmail */
            $objSisFilaEmail = $this->getRepository()->find($param['filaId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objSisFilaEmail);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de fila de e-mail.');

            return false;
        }

        return true;
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceOrgEmailConta = new \Organizacao\Service\OrgEmailConta($this->getEm());

        $serviceOrgEmailConta->setarDependenciasView($view);

        $view->setVariable("arrFilaSituacao", $this->getArrSelect2FilaSituacao());
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2FilaSituacao($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getFilaSituacao());
    }

    /**
     * @return array
     */
    public function executarEnvio($url = '')
    {
        $numEnvios  = 0;
        $numSucesso = 0;

        try {
            @ini_set('memory_limit', '-1');
            $waitTimeout        = $this->executeQuery('SHOW SESSION VARIABLES LIKE "wait_timeout"')->fetch();
            $interactiveTimeout = $this->executeQuery('SHOW SESSION VARIABLES LIKE "interactive_timeout"')->fetch();
            $waitTimeout        = $waitTimeout['Value'];
            $interactiveTimeout = $interactiveTimeout['Value'];

            $this->executeQuery('SET @@session.wait_timeout=' . (60 * 60));
            $this->executeQuery('SET @@session.interactive_timeout=' . (60 * 60));

            $sendGridAtivo = $this->getConfig()->localizarChave('SENDGRID_ATIVO', false);
            $sendGridChave = $this->getConfig()->localizarChave('SENDGRID_CHAVE', '');

            $tamanhoFila = $this->getConfig()->localizarChave('SIS_FILA_EMAIL_TAMANHO', 5);

            $arrObjSisFilaEmail = $this->getRepository()->findBy(
                ['filaSituacao' => self::FILA_SITUACAO_PENDENTE],
                ['filaDataCadastro' => 'ASC'],
                $tamanhoFila,
                0
            );

            /** @var $objSisFilaEmail \Sistema\Entity\SisFilaEmail */
            foreach ($arrObjSisFilaEmail as $objSisFilaEmail) {
                $objSisFilaEmail->setFilaSituacao(self::FILA_SITUACAO_PROCESSANDO);
                $this->getEm()->persist($objSisFilaEmail);
                $this->getEm()->flush($objSisFilaEmail);
            }

            /** @var $objSisFilaEmail \Sistema\Entity\SisFilaEmail */
            foreach ($arrObjSisFilaEmail as $objSisFilaEmail) {
                $objSisFilaEmail->setFilaDataProcessamento(new \DateTime());
                $this->getEm()->persist($objSisFilaEmail);
                $this->getEm()->flush($objSisFilaEmail);

                if (!$sendGridAtivo) {
                    $pluginEmail = new \Sistema\Plugins\Email();

                    $envio = $pluginEmail->sendEmail(
                        $objSisFilaEmail->getFilaAssunto(),
                        $objSisFilaEmail->getFilaConteudo(),
                        $objSisFilaEmail->getFilaDestinatarioNome(),
                        $objSisFilaEmail->getFilaDestinatarioEmail(),
                        $objSisFilaEmail->getConta()->toArray()
                    );
                } else {
                    $email = new \SendGrid\Mail\Mail();
                    $email->setFrom($objSisFilaEmail->getConta()->getContaUsuario());
                    $email->setSubject($objSisFilaEmail->getFilaAssunto());
                    $email->addTo(
                        $objSisFilaEmail->getFilaDestinatarioEmail(),
                        $objSisFilaEmail->getFilaDestinatarioNome()
                    );
                    $email->addContent("text/plain", strip_tags($objSisFilaEmail->getFilaConteudo()));
                    $email->addContent("text/html", $objSisFilaEmail->getFilaConteudo());
                    $sendgrid = new \SendGrid($sendGridChave);

                    try {
                        $envio = $sendgrid->send($email);
                        $envio = true;
                    } catch (\Exception $e) {
                        $envio = $e->getMessage();
                    }
                }

                if ($envio === true) {
                    $envio = self::FILA_SUCESSO;
                    $numSucesso++;
                }

                $objSisFilaEmail
                    ->setFilaSituacao(self::FILA_SITUACAO_ENVIADO)
                    ->setFilaDataFinalizacao(new \DateTime())
                    ->setFilaRetorno($envio);

                if ($envio != self::FILA_SUCESSO) {
                    $objSisFilaEmail->setFilaSituacao(self::FILA_SITUACAO_ERRO);
                }

                $this->getEm()->persist($objSisFilaEmail);
                $this->getEm()->flush($objSisFilaEmail);
                $numEnvios++;
            }

            $this->executeQuery('SET @@session.wait_timeout=' . $waitTimeout);
            $this->executeQuery('SET @@session.interactive_timeout=' . $interactiveTimeout);
        } catch (\Exception $ex) {
            return ['erro' => $ex->getTraceAsString()];
        }

        return ['envios' => $numEnvios, 'sucesso' => $numSucesso, 'erro' => $numEnvios - $numSucesso];
    }

    /**
     * @return array
     */
    public function correcaoAutomatica()
    {
        $arrObjSisFilaEmail = $this->getRepository()->findBy(
            ['filaSituacao' => [self::FILA_SITUACAO_ERRO, self::FILA_SITUACAO_PROCESSANDO]],
            ['filaDataCadastro' => 'asc']
        );

        /** @var $objSisFilaEmail \Sistema\Entity\SisFilaEmail */
        foreach ($arrObjSisFilaEmail as $objSisFilaEmail) {
            $objSisFilaEmail->setFilaSituacao(self::FILA_SITUACAO_PENDENTE);
            $this->getEm()->persist($objSisFilaEmail);
            $this->getEm()->flush($objSisFilaEmail);
        }

        return ['quantidade' => count($arrObjSisFilaEmail)];
    }
}
?>