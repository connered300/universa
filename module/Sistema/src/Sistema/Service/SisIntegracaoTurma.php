<?php

namespace Sistema\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class SisIntegracaoCurso
 * @package Sistema\Service
 */
class SisIntegracaoTurma extends AbstractService
{
    /**
     * @var null
     */
    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Sistema\Entity\SisIntegracaoTurma');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @param \Sistema\Entity\SisIntegracao|int      $integracao
     * @param \Matricula\Entity\AcadperiodoTurma|int $turma
     * @return bool|string
     */
    public function pesquisarCodigoIntegracao($integracao, $turma)
    {
        /** @var $objSisIntegracaoTurma \Sistema\Entity\SisIntegracaoTurma */
        $objSisIntegracaoTurma = $this->getRepository()->findOneBy(
            ['integracao' => $integracao, 'turma' => $turma]
        );

        return $objSisIntegracaoTurma ? $objSisIntegracaoTurma->getCodigo() : false;
    }

    /**
     * @param $integracaoId
     * @param $turmaId
     * @param $codigo
     * @return bool
     */
    public function registrarIntegracaoTurma($integracaoId, $turmaId, $codigo)
    {
        if (!$integracaoId) {
            $this->setLastError('Informe o código da integração!');

            return false;
        }

        if (!$turmaId) {
            $this->setLastError('Informe o código do turma!');

            return false;
        }

        if (!$codigo) {
            $this->setLastError('Informe o código retornado pela integração!');

            return false;
        }

        try {
            $serviceSisIntegracao = new \Sistema\Service\SisIntegracao($this->getEm());
            $serviceAcadTurma     = new \Matricula\Service\AcadperiodoTurma($this->getEm());

            /** @var $objSisIntegracao \Sistema\Entity\SisIntegracao */
            $objSisIntegracao = $serviceSisIntegracao->getRepository()->find($integracaoId);
            /** @var $objAcadTurma \Matricula\Entity\AcadperiodoTurma */
            $objAcadTurma = $serviceAcadTurma->getRepository()->find($turmaId);

            $objSisIntegracaoTurma = new \Sistema\Entity\SisIntegracaoTurma();

            $objSisIntegracaoTurma
                ->setIntegracao($objSisIntegracao)
                ->setTurma($objAcadTurma)
                ->setCodigo($codigo);

            $this->getEm()->beginTransaction();
            $this->getEm()->persist($objSisIntegracaoTurma);
            $this->getEm()->flush($objSisIntegracaoTurma);
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao registrar integração de turma.');

            return false;
        }

        return true;
    }

    /**
     * @param $dados
     */
    protected function valida($dados)
    {
    }

    /**
     * @param $criterios
     */
    protected function pesquisaForJson($criterios)
    {
    }

    public function buscaCodigoTurmaEmIntegracoesAtivas($turmaId)
    {
        if(!$turmaId){
            $this->setLastError("Necessário pelo menos um parâmetro para iniciar a busca");
            return false;
        }
        $service = new SisIntegracao($this->getEm());
        $arrObjIntegracoesAtivas = $service->buscaIntegracoesAtivas();

        /** @var \Sistema\Entity\SisIntegracao $objSisIntegracao */
        foreach($arrObjIntegracoesAtivas as $objSisIntegracao){
            $codigoIntegracaoTurma = $this->pesquisarCodigoIntegracao($objSisIntegracao->getIntegracaoId(),$turmaId);

            if($codigoIntegracaoTurma){
                return array(
                    'codigo' => $codigoIntegracaoTurma,
                    'integracao' => $objSisIntegracao->getIntegracaoId()
                );
            }
        }

        return false;
    }
}