<?php

namespace Sistema\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class SisIntegracaoDisciplina
 * @package Sistema\Service
 */
class SisIntegracaoDisciplina extends AbstractService
{
    /**
     * @var null
     */
    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Sistema\Entity\SisIntegracaoDisciplina');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @param \Sistema\Entity\SisIntegracao|int              $integracao
     * @param \Matricula\Entity\AcadgeralDisciplinaCurso|int $cursoDisc
     * @return bool|string
     */
    public function pesquisarCodigoIntegracao($integracao, $cursoDisc)
    {
        /** @var $objSisIntegracaoDisciplina \Sistema\Entity\SisIntegracaoDisciplina */
        $objSisIntegracaoDisciplina = $this->getRepository()->findOneBy(
            ['integracao' => $integracao, 'cursoDisc' => $cursoDisc]
        );

        return $objSisIntegracaoDisciplina ? $objSisIntegracaoDisciplina->getCodigo() : false;
    }

    /**
     * @param $integracaoId
     * @param $discId
     * @param $codigo
     * @return bool
     */
    public function registrarIntegracaoDisciplina($integracaoId, $discId, $codigo)
    {
        if (!$integracaoId) {
            $this->setLastError('Informe o código da integração!');

            return false;
        }

        if (!$discId) {
            $this->setLastError('Informe uma disciplina!');

            return false;
        }

        if (!$codigo) {
            $this->setLastError('Informe o código retornado pela integração!');

            return false;
        }

        try {
            $serviceSisIntegracao       = new \Sistema\Service\SisIntegracao($this->getEm());
            $serviceAcadDisciplinaCurso = new \Matricula\Service\AcadgeralDisciplinaCurso($this->getEm());

            /** @var $objSisIntegracao \Sistema\Entity\SisIntegracao */
            $objSisIntegracao = $serviceSisIntegracao->getRepository()->find($integracaoId);

            /** @var $objAcadDisciplinaCurso \Matricula\Entity\AcadgeralDisciplinaCurso */
            $objAcadDisciplinaCurso = $serviceAcadDisciplinaCurso->getRepository()->find($discId);

            /** @var \Sistema\Entity\SisIntegracaoDisciplina $objSisIntegracaoDisciplina */
            $objSisIntegracaoDisciplina = $this->getRepository()->findOneBy(
                [
                    'cursoDisc'  => $objAcadDisciplinaCurso->getDiscCursoId(),
                    'integracao' => $objSisIntegracao->getIntegracaoId(),
                    'codigo'     => $codigo
                ]
            );

            if (!$objSisIntegracaoDisciplina) {
                $objSisIntegracaoDisciplina = new \Sistema\Entity\SisIntegracaoDisciplina();
            }

            $objSisIntegracaoDisciplina
                ->setIntegracao($objSisIntegracao)
                ->setCursoDisc($objAcadDisciplinaCurso)
                ->setCodigo($codigo);

            $this->getEm()->beginTransaction();
            $this->getEm()->persist($objSisIntegracaoDisciplina);
            $this->getEm()->flush($objSisIntegracaoDisciplina);
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao registrar integração da disciplina.');

            return false;
        }

        return true;
    }

    /**
     * @param $codigo
     * @param $integracao
     * @return bool|null
     */
    public function retornarDisciplinaPeloCodigoEIntegracao($codigo, $integracao)
    {
        if (!$codigo || !$integracao) {
            $this->setLastError('É necessário informar uma diciplina e o codigo de integração!');

            return false;
        }
        /** @var \Sistema\Entity\SisIntegracaoDisciplina $objSisIntegracaoDisciplina */
        $objSisIntegracaoDisciplina = $this->getRepository()->findOneBy(
            ['codigo' => $codigo, 'integracao' => $integracao]
        );

        if ($objSisIntegracaoDisciplina) {
            return $objSisIntegracaoDisciplina->getCursoDisc();
        }

        return false;
    }

    /**
     * @param $dados
     */
    protected function valida($dados)
    {
    }

    /**
     * @param $parans
     */
    protected function pesquisaForJson($parans)
    {
    }

    public function retornaCodigoDisciplinasPorCurso($cursoId, $integracaoId = false, $alunoId = false) {
        if (!$cursoId) {
            $this->setLastError("É necessário informar o código do curso!");

            return false;
        }

        $query = "
        SELECT sid.codigo, adc.disc_curso_id, sid.curso_disc_id, adc.disc_id, 0 as vigente
        FROM sis__integracao_disciplina sid
        INNER JOIN acadgeral__disciplina_curso adc ON adc.disc_curso_id=sid.curso_disc_id
        ";

        if ($alunoId) {
            $query = "
            SELECT
                sid.codigo,
                adc.disc_curso_id,
                sid.curso_disc_id,
                adc.disc_id,
                IF(DATE(now()) BETWEEN DATE(per_data_inicio) AND DATE(per_data_fim), 1, 0) as vigente
            FROM sis__integracao_disciplina sid
            INNER JOIN acadgeral__disciplina_curso adc ON adc.disc_curso_id=sid.curso_disc_id
            INNER JOIN acadperiodo__aluno_disciplina alunodisc ON alunodisc.disc_id=adc.disc_id
            INNER JOIN acadperiodo__aluno alunoper ON alunoper.alunoper_id = alunodisc.alunoper_id
            INNER JOIN acadperiodo__turma turma ON turma.turma_id = alunoper.turma_id
            INNER JOIN acadperiodo__letivo periodo ON periodo.per_id = turma.per_id
            INNER JOIN acadgeral__aluno_curso alunocur ON alunocur.alunocurso_id = alunoper.alunocurso_id";
            $where[]           = "aluno_id = :aluno_id";
            $param["aluno_id"] = $alunoId;
        }


        $where[]          = "adc.curso_id= :cursoId";
        $param['cursoId'] = $cursoId;

        if ($integracaoId) {
            $where[]               = "sid.integracao_id = :integracaoId";
            $param['integracaoId'] = $integracaoId;
        }

        $query .= ($where ? " WHERE " . implode(" AND ", $where) : "");

        $result = $this->executeQueryWithParam($query, $param)->fetchAll();

        return $result;
    }
}
?>