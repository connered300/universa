<?php

namespace Sistema\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class SisRobo
 * @package Sistema\Service
 */
class SisRobo extends AbstractService
{
    const ROBO_SITUACAO_ATIVO       = 'Ativo';
    const ROBO_SITUACAO_INATIVO     = 'Inativo';
    const ROBO_SITUACAO_PROCESSANDO = 'Processando';
    private $__lastError = null;

    /** @var \Sistema\Service\SisConfig */
    private $config = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, $arrConfig = array())
    {
        parent::__construct($em, 'Sistema\Entity\SisRobo');

        $this->setConfig(new \Sistema\Service\SisConfig($em, $arrConfig));
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @return SisConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param SisConfig $config
     * @return SisFilaSms
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function pesquisaForJson($params)
    {
        $sql      = 'SELECT * FROM sis__robo WHERE';
        $roboNome = false;
        $roboId   = false;

        if ($params['q']) {
            $roboNome = $params['q'];
        } elseif ($params['query']) {
            $roboNome = $params['query'];
        }

        if ($params['roboId']) {
            $roboId = $params['roboId'];
        }

        $parameters = array('robo_nome' => "{$roboNome}%");
        $sql .= ' robo_nome LIKE :robo_nome';

        if ($roboId) {
            $parameters['robo_id'] = $roboId;
            $sql .= ' AND robo_id <> :robo_id';
        }

        $sql .= " ORDER BY robo_nome";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['roboId']) {
                /** @var $objSisRobo \Sistema\Entity\SisRobo */
                $objSisRobo = $this->getRepository()->find($arrDados['roboId']);

                if (!$objSisRobo) {
                    $this->setLastError('Registro de robô não existe!');

                    return false;
                }
            } else {
                $objSisRobo = new \Sistema\Entity\SisRobo();
            }

            $objSisRobo->setRoboNome($arrDados['roboNome']);
            $objSisRobo->setRoboUrl($arrDados['roboUrl']);
            $objSisRobo->setRoboTempoExecucao($arrDados['roboTempoExecucao']);
            $objSisRobo->setRoboUltimaExecucao($arrDados['roboUltimaExecucao']);
            $objSisRobo->setRoboSituacao($arrDados['roboSituacao']);

            $this->getEm()->persist($objSisRobo);
            $this->getEm()->flush($objSisRobo);

            $this->getEm()->commit();

            $arrDados['roboId'] = $objSisRobo->getRoboId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de robô!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['roboNome']) {
            $errors[] = 'Por favor preencha o campo "nome"!';
        }

        if (!$arrParam['roboUrl']) {
            $errors[] = 'Por favor preencha o campo "URL"!';
        }

        if (!$arrParam['roboTempoExecucao']) {
            $errors[] = 'Por favor preencha o campo "tempo de execução"!';
        }

        if (!$arrParam['roboSituacao']) {
            $errors[] = 'Por favor preencha o campo "situação"!';
        }

        if (!in_array($arrParam['roboSituacao'], self::getRoboSituacao())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "situação"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public static function getRoboSituacao()
    {
        return array(self::ROBO_SITUACAO_ATIVO, self::ROBO_SITUACAO_INATIVO, self::ROBO_SITUACAO_PROCESSANDO);
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM sis__robo";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $roboId
     * @return array
     */
    public function getArray($roboId)
    {
        /** @var $objSisRobo \Sistema\Entity\SisRobo */
        $objSisRobo = $this->getRepository()->find($roboId);

        try {
            $arrDados = $objSisRobo->toArray();
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        return $arrDados;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('roboId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Sistema\Entity\SisRobo */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getRoboId(),
                $params['value'] => $objEntity->getRoboNome()
            );
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['roboId']) {
            $this->setLastError('Para remover um registro de robô é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objSisRobo \Sistema\Entity\SisRobo */
            $objSisRobo = $this->getRepository()->find($param['roboId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objSisRobo);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de robô.');

            return false;
        }

        return true;
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $view->setVariable("arrRoboSituacao", $this->getArrSelect2RoboSituacao());
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2RoboSituacao($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getRoboSituacao());
    }

    public function correcaoRoboTravado()
    {
        $query = '
        SELECT GROUP_CONCAT(r.robo_id) roboId FROM sis__robo r
        WHERE r.robo_situacao = :roboSituacao AND
              (TIME_TO_SEC(TIMEDIFF(now(), r.robo_ultima_execucao))/60) >= :processamentoLimite';

        $processamentoLimite = $this->getConfig()->localizarChave('SIS_ROBO_PROCESSAMENTO_LIMITE');
        $processamentoLimite = $processamentoLimite ? $processamentoLimite : 60;

        $params = [
            'roboSituacao'        => self::ROBO_SITUACAO_PROCESSANDO,
            'processamentoLimite' => $processamentoLimite
        ];

        $arrSisRobo = $this->executeQueryWithParam($query, $params)->fetch();

        if (!$arrSisRobo || !$arrSisRobo['roboId']) {
            return;
        }

        $arrSisRobo = explode(',', $arrSisRobo['roboId']);

        $arrObjSisRobo = $this->getRepository()->findBy(['roboId' => $arrSisRobo]);

        /** @var $objSisRobo \Sistema\Entity\SisRobo */
        foreach ($arrObjSisRobo as $objSisRobo) {
            $objSisRobo->setRoboSituacao(self::ROBO_SITUACAO_ATIVO);
            $this->getEm()->persist($objSisRobo);
            $this->getEm()->flush($objSisRobo);
        }
    }

    /**
     * @param string $baseUrl
     * @return array
     */
    public function executarRobo($baseUrl = '')
    {
        try {
            @ini_set('memory_limit', '-1');
            $waitTimeout        = $this->executeQuery('SHOW SESSION VARIABLES LIKE "wait_timeout"')->fetch();
            $interactiveTimeout = $this->executeQuery('SHOW SESSION VARIABLES LIKE "interactive_timeout"')->fetch();
            $waitTimeout        = $waitTimeout['Value'];
            $interactiveTimeout = $interactiveTimeout['Value'];

            $this->executeQuery('SET @@session.wait_timeout=' . (60 * 60));
            $this->executeQuery('SET @@session.interactive_timeout=' . (60 * 60));

            $this->correcaoRoboTravado();

            $arrRobos = [];

            $query = 'SELECT r FROM \Sistema\Entity\SisRobo r
        WHERE r.roboSituacao = :roboSituacao AND
              (
                  (DATE_ADD(r.roboUltimaExecucao, r.roboTempoExecucao, \'second\') < :dataAgora) OR
                  r.roboUltimaExecucao IS NULL
              )
        ORDER BY r.roboUltimaExecucao ASC';

            $arrObjSisRobo = $this
                ->getEm()
                ->createQuery($query)
                ->setParameter('roboSituacao', self::ROBO_SITUACAO_ATIVO)
                ->setParameter('dataAgora', new \DateTime())
                ->setMaxResults(1)
                ->getResult();

            /** @var $objSisRobo \Sistema\Entity\SisRobo */
            foreach ($arrObjSisRobo as $objSisRobo) {
                $objSisRobo
                    ->setRoboSituacao(self::ROBO_SITUACAO_PROCESSANDO)
                    ->setRoboUltimaExecucao(new \DateTime());
                $this->getEm()->persist($objSisRobo);
                $this->getEm()->flush($objSisRobo);

                $url = $objSisRobo->getRoboUrl();

                if ($url[0] == '/') {
                    $url = $baseUrl . $url . (strpos($url, '?') ? '&' : '?') . 'chamadaDeRobo=1';
                }

                try {
                    $resposta = $this->getResponse($url);
                } catch (\Exception $ex) {
                }

                $objSisRobo
                    ->setRoboSituacao(self::ROBO_SITUACAO_ATIVO)
                    ->setRoboUltimaExecucao(new \DateTime());

                $this->getEm()->persist($objSisRobo);
                $this->getEm()->flush($objSisRobo);

                $arrRobos[] = $objSisRobo->getRoboNome();
            }

            $this->executeQuery('SET @@session.wait_timeout=' . $waitTimeout);
            $this->executeQuery('SET @@session.interactive_timeout=' . $interactiveTimeout);
        } catch (\Exception $ex) {
            return ['erro' => $ex->getTraceAsString()];
        }

        return ['robos' => $arrRobos];
    }

    /**
     * @param     $url
     * @param int $timeout
     * @return mixed
     */
    private function getResponse($url, $timeout = 6000)
    {
        set_time_limit(0);
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        $output = curl_exec($curl);
        curl_close($curl);

        return $output;
    }
}
?>