<?php

namespace Sistema\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class SisFilaSms
 * @package Sistema\Service
 */
class SisFilaSms extends AbstractService
{
    const FILA_PROVEDOR_LOCASMS    = 'LOCASMS';
    const FILA_PROVEDOR_SMSEMPRESA = 'SMSEMPRESA';

    const FILA_SITUACAO_PENDENTE    = 'Pendente';
    const FILA_SITUACAO_ENVIADO     = 'Enviado';
    const FILA_SITUACAO_PROCESSANDO = 'Processando';
    const FILA_SITUACAO_ERRO        = 'Erro';

    const FILA_SUCESSO = 'Sucesso';

    private $__lastError = null;
    private $__providers = array();

    /** @var \Sistema\Service\SisConfig */
    private $config = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, $arrConfig = array())
    {
        parent::__construct($em, 'Sistema\Entity\SisFilaSms');

        $this->setConfig(new \Sistema\Service\SisConfig($em, $arrConfig));
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @return SisConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param SisConfig $config
     * @return SisFilaSms
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function pesquisaForJson($params)
    {
        $sql              = 'SELECT * FROM sis__fila_sms WHERE';
        $filaDestinatario = false;
        $filaId           = false;

        if ($params['q']) {
            $filaDestinatario = $params['q'];
        } elseif ($params['query']) {
            $filaDestinatario = $params['query'];
        }

        if ($params['filaId']) {
            $filaId = $params['filaId'];
        }

        $parameters = array('fila_destinatario' => "{$filaDestinatario}%");
        $sql .= ' fila_destinatario LIKE :fila_destinatario';

        if ($filaId) {
            $parameters['fila_id'] = $filaId;
            $sql .= ' AND fila_id <> :fila_id';
        }

        $sql .= " ORDER BY fila_destinatario";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        if (!$arrDados['filaProvedor']) {
            $filaProvedor = $this->getConfig()->localizarChave('SMS_PROVEDOR', self::FILA_PROVEDOR_LOCASMS);

            $arrDados['filaProvedor'] = $filaProvedor;
        }

        if (!$this->valida($arrDados)) {
            return false;
        }

        if (!$arrDados['filaSituacao']) {
            $arrDados['filaSituacao'] = self::FILA_SITUACAO_PENDENTE;
        }

        $serviceComunicacaoTipo = new \Organizacao\Service\OrgComunicacaoTipo($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['filaId']) {
                /** @var $objSisFilaSms \Sistema\Entity\SisFilaSms */
                $objSisFilaSms = $this->getRepository()->find($arrDados['filaId']);

                if (!$objSisFilaSms) {
                    $this->setLastError('Registro de fila de SMS não existe!');

                    return false;
                }
            } else {
                $objSisFilaSms = new \Sistema\Entity\SisFilaSms();

                $arrDados['filaDataCadastro'] = date('Y-m-d H:i:s');
            }

            if ($arrDados['tipoId']) {
                /** @var \Organizacao\Entity\OrgComunicacaoTipo $objComunicacaoTipo */
                $objComunicacaoTipo = $serviceComunicacaoTipo->getRepository()->find($arrDados['tipoId']);

                if (!$objComunicacaoTipo) {
                    $this->setLastError('Registro de Contexto não existe');

                    return false;
                }

                $objSisFilaSms->setTipo($objComunicacaoTipo);
            } else {
                $objSisFilaSms->setTipo(null);
            }

            $objSisFilaSms->setFilaDestinatario($arrDados['filaDestinatario']);
            $objSisFilaSms->setFilaProvedor($arrDados['filaProvedor']);
            $objSisFilaSms->setFilaConteudo($arrDados['filaConteudo']);
            $objSisFilaSms->setFilaRetorno($arrDados['filaRetorno']);
            $objSisFilaSms->setFilaDataCadastro($arrDados['filaDataCadastro']);
            $objSisFilaSms->setFilaDataProcessamento($arrDados['filaDataProcessamento']);
            $objSisFilaSms->setFilaDataFinalizacao($arrDados['filaDataFinalizacao']);
            $objSisFilaSms->setFilaSituacao($arrDados['filaSituacao']);

            $this->getEm()->persist($objSisFilaSms);
            $this->getEm()->flush($objSisFilaSms);

            $this->getEm()->commit();

            $arrDados['filaId'] = $objSisFilaSms->getFilaId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de fila de SMS!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['filaDestinatario']) {
            $errors[] = 'Por favor preencha o campo "destinatário"!';
        } elseif (preg_replace('/[^0-9]/', '', $arrParam['filaDestinatario']) == 0) {
            $errors[] = 'Destinatário inválido!';
        }

        if (!in_array($arrParam['filaProvedor'], self::getFilaProvedor())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "provedor"!';
        }

        if (!$arrParam['filaConteudo']) {
            $errors[] = 'Por favor preencha o campo "conteúdo"!';
        }

        if ($arrParam['filaSituacao'] && !in_array($arrParam['filaSituacao'], self::getFilaSituacao())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "situação"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public static function getFilaProvedor()
    {
        return array(self::FILA_PROVEDOR_LOCASMS, self::FILA_PROVEDOR_SMSEMPRESA);
    }

    /**
     * @return array
     */
    public static function getFilaSituacao()
    {
        return array(
            self::FILA_SITUACAO_PENDENTE,
            self::FILA_SITUACAO_ENVIADO,
            self::FILA_SITUACAO_PROCESSANDO,
            self::FILA_SITUACAO_ERRO
        );
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "
        SELECT
        fila_id, fila_destinatario, fila_provedor, fila_conteudo, fila_retorno,
        fila_data_cadastro, fila_data_processamento, fila_data_finalizacao,
        fila_situacao
        FROM sis__fila_sms
        ";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $filaId
     * @return array
     */
    public function getArray($filaId)
    {
        /** @var $objSisFilaSms \Sistema\Entity\SisFilaSms */
        $objSisFilaSms = $this->getRepository()->find($filaId);

        try {
            $arrDados = $objSisFilaSms->toArray();
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        return $arrDados;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('filaId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Sistema\Entity\SisFilaSms */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getFilaId(),
                $params['value'] => $objEntity->getFilaDestinatario()
            );
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['filaId']) {
            $this->setLastError('Para remover um registro de fila de SMS é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objSisFilaSms \Sistema\Entity\SisFilaSms */
            $objSisFilaSms = $this->getRepository()->find($param['filaId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objSisFilaSms);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de fila de SMS.');

            return false;
        }

        return true;
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $view->setVariable("arrFilaProvedor", $this->getArrSelect2FilaProvedor());
        $view->setVariable("arrFilaSituacao", $this->getArrSelect2FilaSituacao());
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2FilaProvedor($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getFilaProvedor());
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2FilaSituacao($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getFilaSituacao());
    }

    /**
     * @return array
     */
    public function executarEnvio()
    {
        $numEnvios  = 0;
        $numSucesso = 0;

        try {
            @ini_set('memory_limit', '-1');
            $waitTimeout        = $this->executeQuery('SHOW SESSION VARIABLES LIKE "wait_timeout"')->fetch();
            $interactiveTimeout = $this->executeQuery('SHOW SESSION VARIABLES LIKE "interactive_timeout"')->fetch();
            $waitTimeout        = $waitTimeout['Value'];
            $interactiveTimeout = $interactiveTimeout['Value'];

            $this->executeQuery('SET @@session.wait_timeout=' . (60 * 60));
            $this->executeQuery('SET @@session.interactive_timeout=' . (60 * 60));

            $tamanhoFila = $this->getConfig()->localizarChave('SIS_FILA_SMS_TAMANHO', 15);
            $arrGateWay  = [];

            $arrObjSisFilaSms = $this->getRepository()->findBy(
                ['filaSituacao' => self::FILA_SITUACAO_PENDENTE],
                ['filaDataCadastro' => 'asc'],
                $tamanhoFila,
                0
            );

            /** @var $objSisFilaSms \Sistema\Entity\SisFilaSms */
            foreach ($arrObjSisFilaSms as $objSisFilaSms) {
                $objSisFilaSms->setFilaSituacao(self::FILA_SITUACAO_PROCESSANDO);
                $this->getEm()->persist($objSisFilaSms);
                $this->getEm()->flush($objSisFilaSms);
            }

            /** @var $objSisFilaSms \Sistema\Entity\SisFilaSms */
            foreach ($arrObjSisFilaSms as $objSisFilaSms) {
                $objSisFilaSms->setFilaDataProcessamento(new \DateTime());
                $this->getEm()->persist($objSisFilaSms);
                $this->getEm()->flush($objSisFilaSms);

                $gateway = $objSisFilaSms->getFilaProvedor();

                if (!$arrGateWay[$gateway]) {
                    if ($gateway == self::FILA_PROVEDOR_LOCASMS) {
                        $easySMSUser     = $this->getConfig()->localizarChave('LOCASMS_USER');
                        $easySMSPassword = $this->getConfig()->localizarChave('LOCASMS_PASSWORD');

                        if (!$easySMSUser || !$easySMSPassword) {
                            return ['envios' => $numEnvios, 'error' => true, 'message' => 'Credenciais Inválidas'];
                        }

                        /** @var \Sistema\Plugins\SMSEmpresa $gatewaySMS */
                        $gatewaySMS = new \Sistema\Plugins\LocaSMS();
                        $gatewaySMS->setUser($easySMSUser)->setPassword($easySMSPassword);
                    } else {
                        $smsEmpresaChave = $this->getConfig()->localizarChave('SMSEMPRESA_CHAVE');

                        if (!$smsEmpresaChave) {
                            return ['envios' => $numEnvios, 'error' => true, 'message' => 'Credenciais Inválidas'];
                        }
                        /** @var \Sistema\Plugins\SMSEmpresa $locaSMS */
                        $gatewaySMS = new \Sistema\Plugins\SMSEmpresa();
                        $gatewaySMS->setKey($smsEmpresaChave);
                    }

                    $arrGateWay[$gateway] = $gatewaySMS;
                }

                $gatewaySMS = $arrGateWay[$gateway];

                $retorno = $gatewaySMS
                    ->setRecipients($objSisFilaSms->getFilaDestinatario())
                    ->setMensagem($objSisFilaSms->getFilaConteudo())
                    ->sendSMS();

                $objSisFilaSms
                    ->setFilaSituacao(self::FILA_SITUACAO_ENVIADO)
                    ->setFilaDataFinalizacao(new \DateTime())
                    ->setFilaRetorno($retorno);

                if ($gatewaySMS->getResultError()) {
                    $objSisFilaSms->setFilaRetorno($gatewaySMS->getResultErrorMessage());
                    $objSisFilaSms->setFilaSituacao(self::FILA_SITUACAO_ERRO);
                } else {
                    $numSucesso++;
                }

                $this->getEm()->persist($objSisFilaSms);
                $this->getEm()->flush($objSisFilaSms);
                $numEnvios++;
            }

            $this->executeQuery('SET @@session.wait_timeout=' . $waitTimeout);
            $this->executeQuery('SET @@session.interactive_timeout=' . $interactiveTimeout);
        } catch (\Exception $ex) {
            return ['erro' => $ex->getTraceAsString()];
        }

        return ['envios' => $numEnvios, 'sucesso' => $numSucesso, 'erro' => $numEnvios - $numSucesso];
    }

    /**
     * @return array
     */
    public function correcaoAutomatica()
    {
        $arrObjSisFilaSms = $this->getRepository()->findBy(
            ['filaSituacao' => [self::FILA_SITUACAO_ERRO, self::FILA_SITUACAO_PROCESSANDO]],
            ['filaDataCadastro' => 'asc']
        );

        /** @var $objSisFilaSms \Sistema\Entity\SisFilaSms */
        foreach ($arrObjSisFilaSms as $objSisFilaSms) {
            $objSisFilaSms->setFilaSituacao(self::FILA_SITUACAO_PENDENTE);
            $this->getEm()->persist($objSisFilaSms);
            $this->getEm()->flush($objSisFilaSms);
        }

        return ['quantidade' => count($arrObjSisFilaSms)];
    }
}
?>