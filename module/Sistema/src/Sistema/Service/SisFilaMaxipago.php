<?php

namespace Sistema\Service;

use VersaSpine\Service\AbstractService;

class SisFilaMaxipago extends AbstractService
{
    const FILA_SITUACAO_PENDENTE    = 'Pendente';
    const FILA_SITUACAO_ENVIADO     = 'Enviado';
    const FILA_SITUACAO_PROCESSANDO = 'Processando';
    const FILA_SITUACAO_ERRO        = 'Erro';
    const FILA_SUCESSO              = 'Sucesso';
    const FALHA_PAGAMENTO           = 'Falha no Pagamento';
    const FILA_CANCELADO            = 'Cancelado';
    const FILA_ESTORNO              = 'Estorno';
    private $__lastError = null;
    private $config      = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @return SisConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param SisConfig $config
     * @return SisFilaSms
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    public function getArrSelect2FilaSituacao($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getFilaSituacao());
    }

    public static function getFilaSituacao()
    {
        return array(
            self::FILA_SITUACAO_PENDENTE,
            self::FILA_SITUACAO_ENVIADO,
            self::FILA_SITUACAO_PROCESSANDO,
            self::FILA_SITUACAO_ERRO
        );
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, $arrConfig = array())
    {
        parent::__construct($em, 'Sistema\Entity\SisFilaMaxipago');

        $this->setConfig(new \Sistema\Service\SisConfig($em, $arrConfig));
    }

    public function pesquisaForJson($params)
    {
        $serviceTitulo = new \Financeiro\Service\FinanceiroTitulo($this->getEm());

        $sql = '
        SELECT *
        FROM sis__fila_maxipago
        INNER JOIN financeiro__titulo USING(titulo_id)';

        $filaId     = false;
        $where      = array();
        $parameters = array();
        $tituloId   = '';

        if ($params['q']) {
            $tituloId = $params['q'];
        } elseif ($params['query']) {
            $tituloId = $params['query'];
        }

        if ($params['filaId']) {
            $filaId = $params['filaId'];
        }

        if ($params['titulo_id']) {
            $where[] = "titulo_id IN (:tituloId)";

            $parameters['tituloId'] = $params['titulo_id'] . ($tituloId ? ',' . $tituloId : '');
        }

        if ($filaId) {
            $where[] = 'fila_id <> :fila_id';

            $parameters['fila_id'] = $filaId;
        }

        if ($params['alunoCurso']) {
            $where[] = 'alunocurso_id = :alunoCurso';

            $parameters['alunoCurso'] = $params['alunoCurso'];
        }

        if ($params['titulosParaRecorrencia']) {
            $where[] = "titulo_estado = '" . $serviceTitulo::TITULO_ESTADO_ABERTO . "' AND fila_situacao = '" . $this::FALHA_PAGAMENTO . "'";
        }

        $sql .= " WHERE " . implode(" AND ", $where);
        $sql .= " ORDER BY titulo_id";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function retornaItensFila()
    {
        $tamanhoFila = $this->getConfig()->localizarChave('SIS_FILA_MAXIPAGO_TAMANHO', 5);

        $sql = '
        SELECT fila_id
        FROM sis__fila_maxipago
        WHERE fila_situacao = :filaSituacao AND date(fila_agendamento) = date(now())
        ORDER BY fila_agendamento ASC
        LIMIT 0,' . $tamanhoFila;

        $parameters = [
            'filaSituacao' => self::FILA_SITUACAO_PENDENTE
        ];

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll(\PDO::FETCH_COLUMN);

        if (!$result) {
            return array();
        }

        $arrSisFilaMaxipago = $this->getRepository()->findBy(['filaId' => $result]);

        return $arrSisFilaMaxipago;
    }

    public function retornarTitulosRecorrencia($alunocurso)
    {
        return $this->pesquisaForJson(['alunoCurso' => $alunocurso, 'titulosParaRecorrencia' => true]);
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceFianceiroCartao  = new \Financeiro\Service\FinanceiroCartao($this->getEm());
        $dataAtual               = new \DateTime();

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['filaId']) {
                /** @var $objSisFilaMaxipago \Sistema\Entity\SisFilaMaxipago */
                $objSisFilaMaxipago = $this->getRepository()->find($arrDados['filaId']);

                if (!$objSisFilaMaxipago) {
                    $this->setLastError('Registro de fila maxipago não existe!');

                    return false;
                }
            } else {
                $objSisFilaMaxipago = new \Sistema\Entity\SisFilaMaxipago();
                $objSisFilaMaxipago->setFilaDataCadastro($dataAtual);
            }

            if ($arrDados['titulo']) {
                /** @var $objFinanceiroTitulo \Financeiro\Entity\FinanceiroTitulo */
                $objFinanceiroTitulo = $serviceFinanceiroTitulo->getRepository()->findOneBy(
                    ['tituloId' => $arrDados['titulo']]
                );

                if (!$objFinanceiroTitulo) {
                    $this->setLastError('Registro de título não existe!');

                    return false;
                }

                $objSisFilaMaxipago->setTitulo($objFinanceiroTitulo);
            } else {
                $this->setLastError("Título não informado!");

                return false;
            }

            if ($arrDados['cartao']) {
                $objCartao = $serviceFianceiroCartao->getRepository()->findOneBy(['cartaoId' => $arrDados['cartao']]);

                if (!$objCartao) {
                    $this->setLastError("Cartão inválido!");

                    return false;
                }
            } else {
                $objCartao = $objSisFilaMaxipago->getCartao();
            }

            $objSisFilaMaxipago->setFilaConteudo($arrDados['filaConteudo']);
            $objSisFilaMaxipago->setFilaRetorno($arrDados['filaRetorno']);
            $objSisFilaMaxipago->setFilaDataProcessamento($arrDados['dataProcessamento']);
            $objSisFilaMaxipago->setFilaDataFinalizacao($arrDados['filaDataFinalizacao']);
            $objSisFilaMaxipago->setFilaSituacao($arrDados['filaSituacao']);
            $objSisFilaMaxipago->setFilaAgendamento($objFinanceiroTitulo->getTituloDataVencimento());
            $objSisFilaMaxipago->setCartao($objCartao);

            $this->getEm()->persist($objSisFilaMaxipago);
            $this->getEm()->flush($objSisFilaMaxipago);

            $this->getEm()->commit();

            $arrDados['filaId'] = $objSisFilaMaxipago->getFilaId();

            return $objSisFilaMaxipago;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de fila maxipago!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['titulo']) {
            $errors[] = 'Por favor preencha o campo "código título"!';
        }

        if (!$arrParam['filaConteudo']) {
            $errors[] = 'Por favor preencha o campo "conteúdo"!';
        }

        $arrParam['filaSituacao'] =
            !$arrParam['filaSituacao'] ? self::FILA_SITUACAO_PENDENTE : $arrParam['filaSituacao'];

        if (!in_array($arrParam['filaSituacao'], self::getFilaSituacao())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "situação"!';
        }

        if (!$arrParam['cartao']) {
            $errors[] = 'Cartão não informado!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM sis__fila_maxipago";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($filaId)
    {
        $arrDados = array();

        if (!$filaId) {
            $this->setLastError('Fila maxipago inválido!');

            return array();
        }

        /** @var $objSisFilaMaxipago \Sistema\Entity\SisFilaMaxipago */
        $objSisFilaMaxipago = $this->getRepository()->find($filaId);

        if (!$objSisFilaMaxipago) {
            $this->setLastError('Fila maxipago não existe!');

            return array();
        }

        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo($this->getEm());

        try {
            $arrDados = $objSisFilaMaxipago->toArray();

            if ($arrDados['titulo']) {
                $arrFinanceiroTitulo = $serviceFinanceiroTitulo->getArrSelect2(['id' => $arrDados['titulo']]);
                $arrDados['titulo']  = $arrFinanceiroTitulo ? $arrFinanceiroTitulo[0] : null;
            }
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo($this->getEm());

        if (is_array($arrDados['titulo']) && !$arrDados['titulo']['text']) {
            $arrDados['titulo'] = $arrDados['titulo']['tituloId'];
        }

        if ($arrDados['titulo'] && is_string($arrDados['titulo'])) {
            $arrDados['titulo'] = $serviceFinanceiroTitulo->getArrSelect2(array('id' => $arrDados['titulo']));
            $arrDados['titulo'] = $arrDados['titulo'] ? $arrDados['titulo'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['filaId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['tituloId' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Sistema\Entity\SisFilaMaxipago */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getFilaId();
            $arrEntity[$params['value']] = $objEntity->getTituloId();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($filaId)
    {
        if (!$filaId) {
            $this->setLastError('Para remover um registro de fila maxipago é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objSisFilaMaxipago \Sistema\Entity\SisFilaMaxipago */
            $objSisFilaMaxipago = $this->getRepository()->find($filaId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objSisFilaMaxipago);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de fila maxipago.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo($this->getEm());

        $serviceFinanceiroTitulo->setarDependenciasView($view);

        $view->setVariable("arrFilaSituacao", $this->getArrSelect2FilaSituacao());
    }

    /**
     * @return array
     */
    public function executarEnvio($urlHome = '')
    {
        $numEnvios  = 0;
        $numSucesso = 0;
        $arrTitulo  = array();

        $serviceFinanceiroCartao         = new \Financeiro\Service\FinanceiroCartao($this->getEm());
        $serviceOrgComunicacaoModelo     = new \Organizacao\Service\OrgComunicacaoModelo($this->getEm());
        $serviceOrgComunicacaoModeloTipo = new \Organizacao\Service\OrgComunicacaoTipo($this->getEm());

        try {
            @ini_set('memory_limit', '-1');
            $waitTimeout        = $this->executeQuery('SHOW SESSION VARIABLES LIKE "wait_timeout"')->fetch();
            $interactiveTimeout = $this->executeQuery('SHOW SESSION VARIABLES LIKE "interactive_timeout"')->fetch();
            $waitTimeout        = $waitTimeout['Value'];
            $interactiveTimeout = $interactiveTimeout['Value'];

            $this->executeQuery('SET @@session.wait_timeout=' . (60 * 60));
            $this->executeQuery('SET @@session.interactive_timeout=' . (60 * 60));

            $arrObjSisFilaMaxipago = $this->retornaItensFila();

            /** @var $objFilaMaxipago \Sistema\Entity\SisFilaMaxipago */
            foreach ($arrObjSisFilaMaxipago as $objFilaMaxipago) {
                $objFilaMaxipago
                    ->setFilaDataProcessamento(new \DateTime())
                    ->setFilaSituacao(self::FILA_SITUACAO_PROCESSANDO);
                $this->getEm()->persist($objFilaMaxipago);
                $this->getEm()->flush($objFilaMaxipago);

                $arrTitulo           = json_decode($objFilaMaxipago->getFilaConteudo(), true);
                $arrTitulo['filaId'] = $objFilaMaxipago->getFilaId();
                $arrTitulo['pedido'] = $objFilaMaxipago->getTitulo()->getTituloId();

                $envio = $serviceFinanceiroCartao->realizaPagamento($arrTitulo);
                $objFilaMaxipago->setFilaRetorno($envio);

                if ($envio) {
                    $envio = self::FILA_SUCESSO;
                    $numSucesso++;
                }

                $objFilaMaxipago
                    ->setFilaSituacao(self::FILA_SITUACAO_ENVIADO)
                    ->setFilaDataFinalizacao(new \DateTime());

                if (!$envio) {
                    $objFilaMaxipago->setFilaSituacao(self::FALHA_PAGAMENTO);

                    $enviarEmail = $this->getConfig()->localizarChave('FINANCEIRO_ENVIAR_EMAIL_RECORRENCIA_MAXIPAGO');
                    $enviarSMS   = (
                        $this->getConfig()->localizarChave('FINANCEIRO_ENVIAR_SMS_RECORRENCIA_MAXIPAGO') &&
                        $this->getConfig()->localizarChave('SMS_ATIVO')
                    );

                    if ($enviarEmail || $enviarSMS) {
                        /*TODO:Implementar metodo que envia email para o usuario escolher outro meio de pagamento*/
                        $aluno = $objFilaMaxipago->getTitulo()->getAlunocurso()->getAluno()->getAlunoId();

                        $arrInfo = $serviceOrgComunicacaoModelo->trabalharVariaveisPorContexto(
                            array('alunoId' => $aluno),
                            $serviceOrgComunicacaoModeloTipo::TIPO_CONTEXTO_ALUNO
                        );

                        $alunoCursoId     = $objFilaMaxipago->getTitulo()->getAlunocurso()->getAlunocursoId();
                        $tokenRecorrencia = $this->criptografar($alunoCursoId);

                        $arrInfo['tokenRecorrencia'] = $urlHome . '?token=' . $tokenRecorrencia;
                        $arrInfo['urlRecorrencia']   = $urlHome . '?token=' . $tokenRecorrencia;
                        $arrInfo['urlHome']          = $urlHome;

                        if ($enviarSMS) {
                            $serviceOrgComunicacaoModelo->registrarComunicacao(
                                $serviceOrgComunicacaoModeloTipo::TIPO_ENVIO_RECORRENCIA_MAXIPAGO,
                                $serviceOrgComunicacaoModelo::MODELO_TIPO_SMS,
                                $arrInfo
                            );
                        }

                        if ($enviarEmail) {
                            $serviceOrgComunicacaoModelo->registrarComunicacao(
                                $serviceOrgComunicacaoModeloTipo::TIPO_ENVIO_RECORRENCIA_MAXIPAGO,
                                $serviceOrgComunicacaoModelo::MODELO_TIPO_EMAIL,
                                $arrInfo
                            );
                        }
                    }
                }

                $this->getEm()->persist($objFilaMaxipago);
                $this->getEm()->flush($objFilaMaxipago);
                $numEnvios++;
            }

            $this->executeQuery('SET @@session.wait_timeout=' . $waitTimeout);
            $this->executeQuery('SET @@session.interactive_timeout=' . $interactiveTimeout);
        } catch (\Exception $ex) {
            return ['erro' => $ex->getTraceAsString()];
        }

        return ['envios' => $numEnvios, 'sucesso' => $numSucesso, 'erro' => $numEnvios - $numSucesso];
    }

    /**
     * @return array
     */
    public function correcaoAutomatica()
    {
        $arrObjSisFilaMaxipago = $this->getRepository()->findBy(
            ['filaSituacao' => [self::FILA_SITUACAO_ERRO, self::FILA_SITUACAO_PROCESSANDO]],
            ['filaDataCadastro' => 'asc']
        );

        /** @var $objSisFilaMaxipago \Sistema\Entity\SisFilaEmail */
        foreach ($arrObjSisFilaMaxipago as $objSisFilaMaxipago) {
            $objSisFilaMaxipago->setFilaSituacao(self::FILA_SITUACAO_PENDENTE);
            $this->getEm()->persist($objSisFilaMaxipago);
            $this->getEm()->flush($objSisFilaMaxipago);
        }

        return ['quantidade' => count($arrObjSisFilaMaxipago)];
    }

    public function cancelaRecorrencia($arrTitulos)
    {
        $arrTitulosId = $arrTitulos;

        if (is_array($arrTitulos)) {
            $arrTitulosId = array();
            foreach ($arrTitulos as $value) {
                $arrTitulosId[] = $value['titulo_id'];
            }
        }

        if (empty($arrTitulosId) || !is_array($arrTitulosId)) {
            $this->setLastError("Nenhum título informado!");

            return false;
        }

        try {
            $this->begin();

            $arrFilaMaxiPago = $this->getRepository()->findBy(
                ['titulo' => $arrTitulosId, 'filaSituacao' => $this::FALHA_PAGAMENTO]
            );

            /** @var $objFilaMaxiPago \Sistema\Entity\SisFilaMaxipago */
            foreach ($arrFilaMaxiPago as $objFilaMaxiPago) {
                if ($objFilaMaxiPago->getFilaSituacao() == $this::FILA_CANCELADO) {
                    continue;
                }

                $objFilaMaxiPagoFilho = $this->getRepository()->findOneBy(
                    [
                        'titulo'       => $objFilaMaxiPago->getTitulo()->getTituloId(),
                        'filaSituacao' => array(
                            $this::FILA_SITUACAO_PROCESSANDO,
                            $this::FILA_SUCESSO,
                            $this::FILA_SITUACAO_ENVIADO,
                            $this::FILA_SITUACAO_PENDENTE
                        )
                    ]
                );

                $objFilaMaxiPago
                    ->setFilaSituacao($this::FILA_CANCELADO)
                    ->setFilaRecorrencia($objFilaMaxiPagoFilho);

                $this->getEm()->persist($objFilaMaxiPago);
                $this->getEm()->flush($objFilaMaxiPago);
            }

            $this->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao cancelar recorrência');

            return false;
        }

        return true;
    }

    /*Retorna Data Table com os registros que falharam*/
    public function sisFilaMaxiPagoDataTable($data = array())
    {
        $serviceSisMaxiPago = new \Sistema\Service\SisFilaMaxipago($this->getEm());

        if (!$data['filter']['alunoCurso']) {
            $this->setLastError("Nenhum aluno informado para busca!");

            return false;
        }

        $sql = '
        SELECT
            titulo_id tituloId, alunocurso_id matricula,
            financeiro__titulo.titulo_valor tituloValor, titulo_descricao tituloDescricao
        FROM sis__fila_maxipago
        INNER JOIN financeiro__titulo USING(titulo_id)';

        $where = array();
        $param = array();

        if ($data['filter']['falhaPagamento']) {
            $situacaoFalha = "'" . $serviceSisMaxiPago::FALHA_PAGAMENTO . "'";

            if (!$situacaoFalha) {
                $this->setLastError("Configuração de pagamento, não localizada, contante o suporte!");

                return false;
            }

            $where[] = "fila_situacao=$situacaoFalha";
        }

        $result = array(
            "draw"         => $data['draw'],
            "recordsTotal" => $this->executeQuery($sql)->rowCount(),
        );

        // verifica se existe alguma busca a ser feita
        $vSearch = $data["search"]['value'];

        if (!empty($vSearch)) {
            $where[]          = '( titulo_id=:vSearch OR fila_situacao=:vSearch OR alunocurso_id=:vSearch )';
            $param['vSearch'] = $vSearch;
        }

        $where = ' WHERE ' . implode(' AND ', $where);

        $sql = $sql . $where;

        $result["recordsFiltered"] = $this->executeQueryWithParam($sql, $param)->rowCount();

        $orderBy = array();

        foreach ($data['order'] as $order) {
            $orderColumn    = $order["column"];
            $orderColumnDir = $order["dir"];
            $columnName     = $data['columns'][$orderColumn]["name"];

            $orderBy[] = "`{$columnName}` $orderColumnDir";
        }

        $orderBy = implode(" , ", $orderBy);

        if (!empty($orderBy)) {
            $orderBy = " ORDER BY " . $orderBy;
        }

        $sql .= $orderBy;

        if ($data['length'] > 0) {
            $sql .= " LIMIT {$data['length']} OFFSET {$data['start']}";
        }

        $result["data"] = $this->executeQueryWithParam($sql, $param)->fetchAll();

        return $result;
    }

    public function criptografar($valor)
    {
        if (!$valor) {
            $this->setLastError("Nenhum valor informada!");

            return false;
        }

        return sha1($valor);
    }

    public function desencriptografar($token)
    {
        if (!$token) {
            $this->setLastError("Nenhum token informado!");

            return false;
        }

        $query = '
        SELECT *, sha1(alunocurso_id) AS cript
        FROM acadgeral__aluno_curso
        WHERE sha1(alunocurso_id)=:cript';

        $decrypted = $this->executeQueryWithParam($query, ['cript' => $token])->fetch();

        return isset($decrypted['alunocurso_id']) ? $decrypted['alunocurso_id'] : null;
    }

    public function retornaAlunoToken($token)
    {
        return (integer)$this->desencriptografar($token);
    }
}
?>