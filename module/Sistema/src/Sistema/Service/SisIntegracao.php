<?php

namespace Sistema\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class SisIntegracao
 * @package Sistema\Service
 */
class SisIntegracao extends AbstractService
{
    const INTEGRACAO_TIPO_VERSA_AVA   = 'Versa AVA';
    const INTEGRACAO_SITUACAO_ATIVO   = 'Ativo';
    const INTEGRACAO_SITUACAO_INATIVO = 'Inativo';
    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Sistema\Entity\SisIntegracao');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function pesquisaForJson($params)
    {
        $sql                 = 'SELECT * FROM sis__integracao WHERE';
        $integracaoDescricao = false;
        $integracaoId        = false;

        if ($params['q']) {
            $integracaoDescricao = $params['q'];
        } elseif ($params['query']) {
            $integracaoDescricao = $params['query'];
        }

        if ($params['integracaoId']) {
            $integracaoId = $params['integracaoId'];
        }

        $parameters = array('integracao_descricao' => "{$integracaoDescricao}%");
        $sql .= ' integracao_descricao LIKE :integracao_descricao';

        if ($integracaoId) {
            $parameters['integracao_id'] = $integracaoId;
            $sql .= ' AND integracao_id <> :integracao_id';
        }

        $sql .= " ORDER BY integracao_descricao";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceAcessoPessoas    = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceIntegracaoCampus = new \Sistema\Service\SisIntegracaoCampus($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['integracaoId']) {
                /** @var $objSisIntegracao \Sistema\Entity\SisIntegracao */
                $objSisIntegracao = $this->getRepository()->find($arrDados['integracaoId']);

                if (!$objSisIntegracao) {
                    $this->setLastError('Registro de integração não existe!');

                    return false;
                }
            } else {
                $objSisIntegracao = new \Sistema\Entity\SisIntegracao();
            }

            $objAcessoPessoas = $objSisIntegracao->getUsuario();

            if (!$objAcessoPessoas) {
                $usuarioId = $arrDados['usuario'];

                if (!$usuarioId) {
                    $session    = new \Zend\Authentication\Storage\Session();
                    $arrUsuario = $session->read();

                    if (!$arrUsuario) {
                        $this->setLastError('Verifique se você está autenticado no sistema!');

                        return false;
                    }

                    $usuarioId = $arrUsuario['id'];
                }

                /* @var $objAcessoPessoas \Acesso\Entity\AcessoPessoas */
                $objAcessoPessoas = $serviceAcessoPessoas->getRepository()->find($usuarioId);

                if (!$objAcessoPessoas) {
                    $this->setLastError('Verifique se você está autenticado no sistema!');

                    return false;
                }

                $objSisIntegracao->setUsuario($objAcessoPessoas);
            }

            $integracaoToken = $arrDados['integracaoToken'];
            $integracaoToken = $integracaoToken ? $integracaoToken : self::generateToken();

            $objSisIntegracao->setIntegracaoDescricao($arrDados['integracaoDescricao']);
            $objSisIntegracao->setIntegracaoTipo($arrDados['integracaoTipo']);
            $objSisIntegracao->setIntegracaoToken($integracaoToken);
            $objSisIntegracao->setIntegracaoUrl($arrDados['integracaoUrl']);
            $objSisIntegracao->setIntegracaoCabecalhoEnvio($arrDados['integracaoCabecalhoEnvio']);
            $objSisIntegracao->setIntegracaoSituacao($arrDados['integracaoSituacao']);

            $this->getEm()->persist($objSisIntegracao);
            $this->getEm()->flush($objSisIntegracao);

            $serviceIntegracaoCampus->salvarMultiplos($arrDados, $objSisIntegracao);

            $this->getEm()->commit();

            $arrDados['integracaoId'] = $objSisIntegracao->getIntegracaoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de integração!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['integracaoDescricao']) {
            $errors[] = 'Por favor preencha o campo "descrição"!';
        }

        if (!$arrParam['campus']) {
            $errors[] = 'Por favor preencha o campo "campus"!';
        }

        if (!$arrParam['integracaoTipo']) {
            $errors[] = 'Por favor preencha o campo "tipo"!';
        }

        if (!in_array($arrParam['integracaoTipo'], self::getIntegracaoTipo())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "tipo"!';
        }

        if (!$arrParam['integracaoUrl']) {
            $errors[] = 'Por favor preencha o campo "URL"!';
        }

        if (!$arrParam['integracaoSituacao']) {
            $errors[] = 'Por favor preencha o campo "situação"!';
        }

        if (!in_array($arrParam['integracaoSituacao'], self::getIntegracaoSituacao())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "situação"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public static function getIntegracaoTipo()
    {
        return array(self::INTEGRACAO_TIPO_VERSA_AVA);
    }

    /**
     * @return array
     */
    public static function getIntegracaoSituacao()
    {
        return array(self::INTEGRACAO_SITUACAO_ATIVO, self::INTEGRACAO_SITUACAO_INATIVO);
    }

    /**
     * @return string
     */
    public static function generateToken()
    {
        return sha1(uniqid(time()));
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM sis__integracao";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $integracaoId
     * @return array
     */
    public function getArray($integracaoId)
    {
        $arrDados                = array();
        $serviceIntegracaoCampus = new \Sistema\Service\SisIntegracaoCampus($this->getEm());

        try {
            /** @var $objIntegracao \Sistema\Entity\SisIntegracao */
            $objIntegracao      = $this->getRepository()->find($integracaoId);
            $arrDados           = $objIntegracao->toArray();
            $arrDados['campus'] = $serviceIntegracaoCampus->pesquisaIntegracaoCampus($integracaoId, true);
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        if (!$arrDados['integracaoToken']) {
            $arrDados['integracaoToken'] = self::generateToken();
        }

        return $arrDados;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('integracaoId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Sistema\Entity\SisIntegracao */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getIntegracaoId(),
                $params['value'] => $objEntity->getIntegracaoDescricao()
            );
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['integracaoId']) {
            $this->setLastError('Para remover um registro de integração é necessário informar o código.');

            return false;
        }

        $serviceIntegracaoCampus = new \Sistema\Service\SisIntegracaoCampus($this->getEm());

        try {
            $this->getEm()->beginTransaction();
            /** @var $objSisIntegracao \Sistema\Entity\SisIntegracao */
            $objSisIntegracao = $this->getRepository()->find($param['integracaoId']);

            if (!$serviceIntegracaoCampus->removeIntegracaoCampusPelaIntegracao($objSisIntegracao->getIntegracaoId())) {
                $this->setLastError($serviceIntegracaoCampus->getLastError());

                return false;
            }

            $this->getEm()->remove($objSisIntegracao);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de integração.');

            return false;
        }

        return true;
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $view->setVariable("arrIntegracaoTipo", $this->getArrSelect2IntegracaoTipo());
        $view->setVariable("arrIntegracaoSituacao", $this->getArrSelect2IntegracaoSituacao());
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2IntegracaoTipo($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getIntegracaoTipo());
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2IntegracaoSituacao($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getIntegracaoSituacao());
    }

    /**
     * @return array
     */
    public function buscaIntegracoesAtivas()
    {
        $arrFind = ['integracaoSituacao' => self::INTEGRACAO_SITUACAO_ATIVO];

        $arrObjSisIntegracao = $this->getRepository()->findBy($arrFind);

        return $arrObjSisIntegracao;
    }

    public function validaCredenciais($token)
    {
        /** @var \Sistema\Entity\SisIntegracao $objSisIntegracao */
        $objSisIntegracao = $this->getRepository()->findOneBy(['integracaoToken' => $token]);

        if (!$objSisIntegracao || $objSisIntegracao->getIntegracaoSituacao() == self::INTEGRACAO_SITUACAO_INATIVO) {
            return false;
        }

        return $objSisIntegracao;
    }

    public function integracaoInteligenteParaAluno($obj)
    {
        if (!$obj) {
            $this->setLastError("Necessário passar pelo menos um parâmetro para continuar com a integração");

            return false;
        }

        $serviceSisFila = new SisFilaIntegracao($this->getEm());
        $entity         = get_class($obj);

        try {
            if (is_a($obj, "Matricula\\Entity\\AcadgeralAlunoCurso")) {
                /** @var \Matricula\Entity\AcadgeralAlunoCurso $obj */
                $objAluno = $obj->getAluno();

                $serviceSisFila->registrarAlunoNaFilaDeIntegracao($objAluno);
            } elseif (is_a($obj, "Matricula\\Entity\\AcadperiodoAluno")) {
                /** @var \Matricula\Entity\AcadperiodoAluno $obj */
                $objAlunoCurso = $obj->getAlunocurso();
                $objAluno      = $objAlunoCurso->getAluno();

                $serviceSisFila->registrarAlunoNaFilaDeIntegracao($objAluno);
            } elseif (is_a($obj, "Matricula\\Entity\\AcadgeralAluno")) {
                /** @var \Matricula\Entity\AcadgeralAluno $obj */

                $serviceSisFila->registrarAlunoNaFilaDeIntegracao($obj);
            } else {
                throw new \Exception('Entidade desconhecida!');
            }
        } catch (\Exception $e) {
            $this->setLastError("Não foi possível registrar integração de $entity." . $e->getMessage());

            return false;
        }

        return true;
    }

    /**
     * @param $url string
     * @return \Sistema\Entity\SisIntegracao | bool
     */
    public function retornaIntegracaoPorUrl($url)
    {
        $arrIntegracoesAtivas = $this->buscaIntegracoesAtivas();

        /** @var \Sistema\Entity\SisIntegracao $objIntegracao */
        foreach ($arrIntegracoesAtivas as $objIntegracao) {
            if (substr_count($objIntegracao->getIntegracaoUrl(), $url) > 0) {
                return $objIntegracao;
            }
        }

        return false;
    }
}

?>