<?php

namespace Sistema\Service;

use Acesso\Service\AcessoGrupo;
use Financeiro\Service\FinanceiroMeioPagamento;
use VersaSpine\Service\AbstractService;

/**
 * Class SisConfig
 * @package Sistema\Service
 */
class SisConfig extends AbstractService
{
    private $__lastError = null;
    private $config      = array();

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, $arrConfig = array())
    {
        $this->setConfig($arrConfig);
        parent::__construct($em, 'Sistema\Entity\SisConfig');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function pesquisaForJson($params)
    {
        $sql   = 'SELECT * FROM sis__config WHERE';
        $valor = false;
        $chave = false;

        if ($params['q']) {
            $valor = $params['q'];
        } elseif ($params['query']) {
            $valor = $params['query'];
        }

        if ($params['chave']) {
            $chave = $params['chave'];
        }

        $parameters = array('valor' => "{$valor}%");
        $sql .= ' valor LIKE :valor';

        if ($chave) {
            $parameters['chave'] = $chave;
            $sql .= ' AND chave <> :chave';
        }

        $sql .= " ORDER BY valor";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            $chave        = ($arrDados['chaveOld'] ? $arrDados['chaveOld'] : null);
            $chave        = (!$chave && $arrDados['chave'] ? $arrDados['chave'] : $chave);
            $objSisConfig = null;

            if ($chave) {
                /** @var $objSisConfig \Sistema\Entity\SisConfig */
                $objSisConfig = $this->getRepository()->find($chave);
            }

            if (!$objSisConfig) {
                $objSisConfig = new \Sistema\Entity\SisConfig();
            }

            $objSisConfig->setChave($arrDados['chave']);
            $objSisConfig->setValor(trim($arrDados['valor']));

            $this->getEm()->persist($objSisConfig);
            $this->getEm()->flush($objSisConfig);

            $this->getEm()->commit();

            $arrDados['chave'] = $objSisConfig->getChave();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro De configuração!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['chave']) {
            $errors[] = 'Informe a chave para registrar a configuração!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM sis__config";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        return $arrDados;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('chave' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Sistema\Entity\SisConfig */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getChave(),
                $params['value'] => $objEntity->getValor()
            );
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['chave']) {
            $this->setLastError('Para remover um registro De configuração é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objSisConfig \Sistema\Entity\SisConfig */
            $objSisConfig = $this->getRepository()->find($param['chave']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objSisConfig);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro De configuração.');

            return false;
        }

        return true;
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
    }

    /**
     * @param $chave
     * @return array
     */
    public function localizarChave($chave, $default = false)
    {
        $arrConfig = $this->getArray($chave);

        if ($arrConfig) {
            return $arrConfig['valor'];
        }

        return $this->getConfig($chave, $default);
    }

    /**
     * @param $chave
     * @return array
     */
    public function getArray($chave)
    {
        /** @var $objSisConfig \Sistema\Entity\SisConfig */
        $objSisConfig = $this->getRepository()->find($chave);

        if (!$objSisConfig) {
            $this->setLastError('Registro De configuração inválido.');

            return array();
        }

        try {
            $arrDados = $objSisConfig->toArray();
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param null $key
     * @return array
     */
    public function getConfig($key = null, $default = false)
    {
        if ($key) {
            return isset($this->config[$key]) ? $this->config[$key] : $default;
        }

        return $this->config;
    }

    /**
     * @param array $config
     * @return SisFilaSms
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    public function getArrConfigForm()
    {
        $serviceAcessoGrupo              = new \Acesso\Service\AcessoGrupo($this->getEm());
        $serviceAcadNivel                = new \Matricula\Service\AcadNivel($this->getEm());
        $serviceSelecaoTipo              = new \Vestibular\Service\SelecaoTipo($this->getEm());
        $serviceFinanceiroMeioPagamento  = new \Financeiro\Service\FinanceiroMeioPagamento($this->getEm());
        $serviceFinanceiroTituloTipo     = new \Financeiro\Service\FinanceiroTituloTipo($this->getEm());
        $serviceProtocoloMensagemPadrao  = new \Protocolo\Service\ProtocoloMensagemPadrao($this->getEm());
        $serviceFinanceiroFiltro         = new \Financeiro\Service\FinanceiroFiltro($this->getEm());
        $serviceFinanceiroCartao         = new \Financeiro\Service\FinanceiroCartao($this->getEm());
        $serviceFinanceiroDescontoTitulo = new \Financeiro\Service\FinanceiroDescontoTitulo($this->getEm());

        $arrGrupos               = $serviceAcessoGrupo->getArrSelect();
        $arrNivel                = $serviceAcadNivel->getArrSelect();
        $arrFormaIngresso        = $serviceSelecaoTipo->getArrSelect();
        $arrMeioPagamento        = $serviceFinanceiroMeioPagamento->getArrSelect();
        $arrTipoTitulo           = $serviceFinanceiroTituloTipo->getArrSelect();
        $arrMensagemPadrao       = $serviceProtocoloMensagemPadrao->getArrSelect();
        $arrFinanceiroFiltro     = $serviceFinanceiroFiltro->getArrSelect();
        $arrFormaCalculoDesconto = $serviceFinanceiroDescontoTitulo->getDescontoTituloModo();

        $arrFinanceiroFiltro['']     = '';
        $arrGrupos['']               = '';
        $arrFormaCalculoDesconto[''] = '';
        $arrMensagemPadrao['']       = '';
        $arrNivel['']                = '';
        $arrSimNao                   = array(0 => "Não", 1 => "Sim");
        $arrTipoTitulo['']           = '';

        $arrCamposCaptacao = [
            'pesNaturalidade'           => 'Naturalidade',
            'pesNascUf'                 => 'Estado de Nascimento',
            'alunoEtnia'                => 'Etnia',
            'pesRg'                     => 'RG',
            'pessoaNecessidades'        => 'Necessidades Especiais',
            'alunoMae'                  => 'Nome da Mãe',
            'alunoPai'                  => 'nome do Pai',
            'origem'                    => 'Origem do Cadastro',
            'alunocursoPessoaIndicacao' => 'Pessoa que Indicou o Curso',
            'formacao'                  => 'Formação Anterior'
        ];

        $arrPadroesMatricula = array(
            'AAAAAAAAAAAA' => 'Aluno(12)',
            'YYAAAAAAAAAA' => 'Ano(2) + Aluno(10)',
            'YYAAAAA'      => 'Ano(2) + Aluno(5)',
            'YYCCAAAA'     => 'Ano(2) + Curso(2) + Aluno(4)',
            'YYCCAAAAA'    => 'Ano(2) + Curso(2) + Aluno(5)',
            'YYCCCAAA'     => 'Ano(2) + Curso(3) + Aluno(3)',
            'YYCCCAAAA'    => 'Ano(2) + Curso(3) + Aluno(4)',
            'YYCCCAAAAA'   => 'Ano(2) + Curso(3) + Aluno(5)',
            'YYCCCCAAAAA'  => 'Ano(2) + Curso(4) + Aluno(5)',
            'YYCCCCCAAAAA' => 'Ano(2) + Curso(5) + Aluno(5)',
        );

        $arrDescricaoPadraoTitulo = array(
            '__DATA_PAGAMENTO__'          => '__DATA_PAGAMENTO__',
            '__DATA_PAGAMENTO_ANO__'      => '__DATA_PAGAMENTO_ANO__',
            '__DATA_PAGAMENTO_DIA__'      => '__DATA_PAGAMENTO_DIA__',
            '__DATA_PAGAMENTO_MES__'      => '__DATA_PAGAMENTO_MES__',
            '__DATA_PAGAMENTO_MES_NOME__' => '__DATA_PAGAMENTO_MES_NOME__',
            '__PARCELA__'                 => '__PARCELA__',
            '__TIPO__'                    => '__TIPO__',
            '__TOTAL_PARCELAS__'          => '__TOTAL_PARCELAS__'
        );

        $arrBandeirasCartao     = $serviceFinanceiroCartao->retornaBandeirasPossiveis();
        $arrAdquirentesMaxiPago = $serviceFinanceiroCartao->retornaAdquirentesMaxiPago();
        $arrAmbientesMaxiPago   = $serviceFinanceiroCartao->retornaAAmbientesMaxiPago();

        $arrConfigForm = array();

        $arrConfigForm['alunos'] = array(
            'descricao' => 'Alunos',
            'blocos'    => array(
                array(
                    "ALUNO_ACESSO_GRUPO"       => array(
                        'options'     => $arrGrupos,
                        'value'       => $this->localizarChave("ALUNO_ACESSO_GRUPO"),
                        'id'          => "alunoGrupo",
                        'label'       => "Grupo De Usuário:",
                        'class'       => "form-control",
                        'placeholder' => "Selecione",
                        'descricao'   => 'Diz qual grupo o aluno pertence!'
                    ),
                    "ALUNO_CRIAR_USUARIO"      => array(
                        'options'     => $arrSimNao,
                        'value'       => $this->localizarChave("ALUNO_CRIAR_USUARIO"),
                        'id'          => "alunoCriarUsuario",
                        'label'       => "Criar Usuário De Aluno Automaticamente:",
                        'class'       => "form-control",
                        'placeholder' => "Selecione",
                        'descricao'   => 'Após cadastrar o aluno no sistema, seu usuario será criado automaticamente!'
                    ),
                    "EFETUAR_INTEGRACAO_ALUNO" => array(
                        'options'     => $arrSimNao,
                        'value'       => $this->localizarChave("EFETUAR_INTEGRACAO_ALUNO"),
                        'id'          => "alunoEfetuarIntegracao",
                        'label'       => "Integrar Com Outros Sistemas:",
                        'class'       => "form-control",
                        'placeholder' => "Selecione",
                        'descricao'   => 'Se esta configuração estiver ativada, o aluno será integrado ao AVA!'
                    ),
                ),
                'Registro e Captação:'        => array(
                    "ALUNO_POSSUI_AGENTE_EDUCACIONAL"    => array(
                        'options'     => $arrSimNao,
                        'value'       => $this->localizarChave("ALUNO_POSSUI_AGENTE_EDUCACIONAL"),
                        'id'          => "alunoTemAgente",
                        'label'       => "Possui Agente Educacional:",
                        'class'       => "form-control",
                        'placeholder' => "Selecione",
                        'descricao'   => 'Configura a exibição do campo de agente educacionao no momento do cadastro do aluno!'
                    ),
                    "ALUNO_POSSUI_UNIDADE_DE_ESTUDO"     => array(
                        'options'     => $arrSimNao,
                        'value'       => $this->localizarChave("ALUNO_POSSUI_UNIDADE_DE_ESTUDO"),
                        'id'          => "alunoTemUnidadeEstudo",
                        'label'       => "Possui Unidade Estudo:",
                        'class'       => "form-control",
                        'placeholder' => "Selecione",
                        'descricao'   => 'Configura a exibição do campo de unidade de estudo no momento do cadastro do aluno!'
                    ),
                    "ALUNO_CAPTACAO_FINANCEIRO_MENSAGEM" => array(
                        'value'     => $this->localizarChave("ALUNO_CAPTACAO_FINANCEIRO_MENSAGEM"),
                        'id'        => "alunoCaptacaoFinanceiroMensagem",
                        'label'     => "Mensagem Relacionada ao Financeiro do Aluno Para Captação:",
                        'class'     => "form-control",
                        'columns'   => 12,
                        'type'      => "textarea",
                        'descricao' => 'Mensagem que sera exibida relacionada ao financeiro na captação!',
                    ),
                    "MATRICULA_PADRAO"                   => array(
                        'options'    => $arrPadroesMatricula,
                        'value'      => $this->localizarChave("MATRICULA_PADRAO"),
                        'data-valor' => json_encode(
                            explode(",", $this->localizarChave("MATRICULA_PADRAO"))
                        ),
                        'id'         => "matriculaPadrao",
                        'label'      => "Padrão Para Geração do Número de Matrícula:",
                        'class'      => "form-control"
                    ),
                    "ALUNO_FORMA_INGRESSO_PADRAO"        => array(
                        'options'     => $arrFormaIngresso,
                        'value'       => $this->localizarChave("ALUNO_FORMA_INGRESSO_PADRAO"),
                        'id'          => "alunoFormaIngresso",
                        'label'       => "Forma De Ingresso Padrão:",
                        'class'       => "form-control",
                        'placeholder' => "Selecione",
                        'descricao'   => 'Forma de ingresso padrão, no momento do cadastro casou houver!'
                    ),
                    "DESABILITAR_OBRIGATORIEDADE_CPF"        => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave("DESABILITAR_OBRIGATORIEDADE_CPF"),
                        'id'      => "desabilitarObrigatoriedadeCpf",
                        'label'   => "Desabilitar Obrigatoriedade de CPF:",
                        'class'   => "form-control",
                    ),
                    "ALUNO_CAMPOS_CAPTACAO"              => array(
                        'options'    => $arrCamposCaptacao,
                        'value'      => $this->localizarChave("ALUNO_CAMPOS_CAPTACAO"),
                        'data-valor' => json_encode(
                            explode(",", $this->localizarChave("ALUNO_CAMPOS_CAPTACAO"))
                        ),
                        'id'         => "alunoCamposCaptacao",
                        'label'      => "Campos Disponíveis no Formulário de Captação:",
                        'class'      => "form-control",
                        'columns'    => 12,
                        'multiple'   => true
                    ),
                ),
                'Campos Restritos:'           => array(
                    "GRUPO_EDITA_INDICACAO_ALUNO"            => array(
                        'options'    => $arrGrupos,
                        'value'      => $this->localizarChave("GRUPO_EDITA_INDICACAO_ALUNO"),
                        'data-valor' => json_encode(
                            explode(",", $this->localizarChave("GRUPO_EDITA_INDICACAO_ALUNO"))
                        ),
                        'id'         => "grupoEditaInidicaoAluno",
                        'label'      => "Grupos Que Podem Editar a Pessoa Indicação do Aluno:",
                        'class'      => "form-control",
                        'multiple'   => true,
                        'columns'    => 4,
                    ),
                    "GRUPO_EDITA_AGENTE_EDUCACIONAL_ALUNO"   => array(
                        'options'    => $arrGrupos,
                        'data-valor' => json_encode(
                            explode(",", $this->localizarChave("GRUPO_EDITA_AGENTE_EDUCACIONAL_ALUNO"))
                        ),
                        'value'      => $this->localizarChave("GRUPO_EDITA_AGENTE_EDUCACIONAL_ALUNO"),
                        'id'         => "grupoEditaAgenteEducacionalAluno",
                        'label'      => "Grupos Que Podem Editar o Agenciador Do Aluno:",
                        'class'      => "form-control",
                        'multiple'   => true,
                        'columns'    => 4,
                    ),
                    "GRUPO_EDITA_OBSERVACAO_DOCUMENTO_ALUNO" => array(
                        'options'    => $arrGrupos,
                        'data-valor' => json_encode(
                            explode(",", $this->localizarChave("GRUPO_EDITA_OBSERVACAO_DOCUMENTO_ALUNO"))
                        ),
                        'value'      => $this->localizarChave("GRUPO_EDITA_OBSERVACAO_DOCUMENTO_ALUNO"),
                        'id'         => "grupoEditaObservacaoDocumentoAluno",
                        'label'      => "Grupos Que Podem Editar a Observação do Documento do Aluno:",
                        'class'      => "form-control",
                        'multiple'   => true,
                        'columns'    => 4,
                    ),
                ),
                'Boas Vindas de Aluno:'       => array(
                    "ENVIAR_EMAIL_BOAS_VINDAS_ALUNO"                  => array(
                        'options'     => $arrSimNao,
                        'value'       => $this->localizarChave("ENVIAR_EMAIL_BOAS_VINDAS_ALUNO"),
                        'id'          => "alunoBoasVindasEmail",
                        'label'       => "Enviar E-mail Automático De Boas-vindas:",
                        'class'       => "form-control",
                        'placeholder' => "Selecione",
                        'descricao'   => 'Faz envio de e-mail automático de boas-vindas ao aluno quando cadastrado no sistema!'
                    ),
                    "ENVIAR_SMS_BOAS_VINDAS_ALUNO"                    => array(
                        'options'     => $arrSimNao,
                        'value'       => $this->localizarChave("ENVIAR_SMS_BOAS_VINDAS_ALUNO"),
                        'id'          => "alunoBoasVindasSMS",
                        'label'       => "Enviar Sms Automático De Boas-vindas:",
                        'class'       => "form-control",
                        'placeholder' => "Selecione",
                        'descricao'   => 'Faz envio de sms automático de boas-vindas ao aluno quando cadastrado no sistema!'
                    ),
                    "ENVIAR_EMAIL_COMUNICACAO_ALUNO_APOS_DEFERIMENTO" => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave("ENVIAR_EMAIL_COMUNICACAO_ALUNO_APOS_DEFERIMENTO"),
                        'id'      => "enviarEmailComunicacaoAlunoAposDeferimento",
                        'label'   => "Enviar E-mail Automático Após o Deferimento:",
                        'class'   => "form-control",
                    ),
                    "ENVIAR_SMS_COMUNICACAO_ALUNO_APOS_DEFERIMENTO"   => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave("ENVIAR_SMS_COMUNICACAO_ALUNO_APOS_DEFERIMENTO"),
                        'id'      => "enviarSmsComunicacaoAlunoAposDeferimento",
                        'label'   => "Enviar Sms Automático Após o Deferimento:",
                        'class'   => "form-control",
                    ),
                ),
                'Link Externo Personalizado:' => array(
                    'LINK_EXTERNO_ALUNO'          => array(
                        'options'     => $arrSimNao,
                        'value'       => $this->localizarChave('LINK_EXTERNO_ALUNO'),
                        'id'          => 'linkExternoAluno',
                        'label'       => 'Ativar Link Externo Personalizado:',
                        'class'       => "form-control",
                        'placeholder' => "Selecione",
                        'descricao'   => 'Ativa o Link Externo Personalizado'
                    ),
                    'LINK_EXTERNO_ALUNO_ENDERECO' => array(
                        'value'       => $this->localizarChave('LINK_EXTERNO_ALUNO_ENDERECO'),
                        'id'          => 'linkExternoAlunoEndereco',
                        'label'       => 'Endereço Do Link Externo Personalizado:',
                        'class'       => "form-control",
                        'type'        => "text",
                        'placeholder' => "Selecione",
                    ),
                    'LINK_EXTERNO_ALUNO_MSG'      => array(
                        'value'       => $this->localizarChave('LINK_EXTERNO_ALUNO_MSG'),
                        'id'          => 'linkExternoAlunoMsg',
                        'label'       => 'Mensagem Do Link Externo Personalizado:',
                        'class'       => "form-control",
                        'type'        => "textarea",
                        'placeholder' => "Selecione",
                    ),
                ),
                'Rematrícula:'                => array(
                    'VALIDAR_DOCUMENTOS_PEDENTES_REMATRICULA'   => array(
                        'options'     => $arrSimNao,
                        'value'       => $this->localizarChave('VALIDAR_DOCUMENTOS_PEDENTES_REMATRICULA'),
                        'id'          => 'validarDocumentosPendentesRematricula',
                        'label'       => 'Validar Documentos Pendentes na API de Rematrícula :',
                        'class'       => "form-control",
                        'placeholder' => "Selecione",
                    ),
                    'VALIDAR_PENDENCIAS_ACADEMICAS_REMATRICULA' => array(
                        'options'     => $arrSimNao,
                        'value'       => $this->localizarChave('VALIDAR_PENDENCIAS_ACADEMICAS_REMATRICULA'),
                        'id'          => 'validarPendenciasAcademicasRematricula',
                        'label'       => 'Validar Pendências Acadêmicas na API de Rematrícula :',
                        'class'       => "form-control",
                        'placeholder' => "Selecione",
                    ),
                    'VALIDAR_TITULOS_ABERTOS_REMATRICULA'       => array(
                        'options'     => $arrSimNao,
                        'value'       => $this->localizarChave('VALIDAR_TITULOS_ABERTOS_REMATRICULA'),
                        'id'          => 'validarTitulosAbertosRematricula',
                        'label'       => 'Validar Títulos Abertos na API de Rematrícula :',
                        'class'       => "form-control",
                        'placeholder' => "Selecione",
                    ),
                    'VALIDAR_TITULOS_VENCIDOS_REMATRICULA'      => array(
                        'options'     => $arrSimNao,
                        'value'       => $this->localizarChave('VALIDAR_TITULOS_VENCIDOS_REMATRICULA'),
                        'id'          => 'validarTitulosVencidosRematricula',
                        'label'       => 'Validar Títulos Vencidos na API de Rematrícula :',
                        'class'       => "form-control",
                        'placeholder' => "Selecione",
                    ),
                ),
                'Indicadores de Aluno'        => array(
                    'TITULOS_FINANCEIRO_VALIDACAO_DESLIGAMENTO_ALUNO' => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave('TITULOS_FINANCEIRO_VALIDACAO_DESLIGAMENTO_ALUNO'),
                        'id'      => 'titulosFinanceiroValidacaoDesligamentoAluno',
                        'label'   => 'Validar Existência de Títulos do Aluno em Aberto no Desligamento:',
                        'class'   => "form-control",
                    ),
                ),
                'Outros'                      => array(
                    'FINANCEIRO_INADIMPLENCIA_TIPO_TITULO' => array(
                        'options'    => $arrTipoTitulo,
                        'value'      => $this->localizarChave('FINANCEIRO_INADIMPLENCIA_TIPO_TITULO'),
                        'data-valor' => json_encode(
                            explode(",", $this->localizarChave("FINANCEIRO_INADIMPLENCIA_TIPO_TITULO"))
                        ),
                        'id'         => 'financeiroInadimplenciaTipoTitulo',
                        'label'      => 'Tipos de Títulos Financeiros que impossibilitam a progressão automática do aluno:',
                        'class'      => "form-control",
                        'multiple'   => true,
                    ),
                    'MATRICULA_GERAR_TITULO_MENSALIDADE' => array(
                        'options'    => $arrSimNao,
                        'value'      => $this->localizarChave('MATRICULA_GERAR_TITULO_MENSALIDADE'),
                        'id'         => 'matriculaGerarTituloMensalidade',
                        'label'      => 'Gerar título de Mensalidade Durante A Matrícula/Rematrícula:',
                        'class'      => "form-control",
                    ),
                    'MATRICULA_SUMARIZADOR_RELATORIO_ATIVO' => array(
                        'options'    => $arrSimNao,
                        'value'      => $this->localizarChave('MATRICULA_SUMARIZADOR_RELATORIO_ATIVO'),
                        'id'         => 'matriculaSumarizadorRelatorioAtivo',
                        'label'      => 'Configura Valor Padrão do Sumarizador:',
                        'class'      => "form-control",
                    ),
                ),
            )
        );

        $arrConfigForm['agentes'] = array(
            'descricao' => 'Agentes Educacionais',
            'blocos'    => array(
                array(
                    "AGENTE_EDUCACIONAL_ACESSO_GRUPO"  => array(
                        'options' => $arrGrupos,
                        'value'   => $this->localizarChave("AGENTE_EDUCACIONAL_ACESSO_GRUPO"),
                        'id'      => "agenteGrupo",
                        'label'   => "Grupo De Usuário:",
                        'class'   => "form-control",
                    ),
                    "AGENTE_EDUCACIONAL_CRIAR_USUARIO" => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave("AGENTE_EDUCACIONAL_CRIAR_USUARIO"),
                        'id'      => "agenteCriarUsuario",
                        'label'   => "Criar Usuário Automaticamente:",
                        'class'   => "form-control",
                    ),
                    "ACADEMICO_CAPTACAO_AGENTE_NIVEIS" => array(
                        'options'    => $arrNivel,
                        'value'      => $this->localizarChave("ACADEMICO_CAPTACAO_AGENTE_NIVEIS"),
                        'data-valor' => json_encode(
                            explode(",", $this->localizarChave("ACADEMICO_CAPTACAO_AGENTE_NIVEIS"))
                        ),
                        'id'         => "agenteCaptacaoNivel",
                        'label'      => "Nível De Ensino Permitido Na Captação De Alunos:",
                        'class'      => "form-control",
                        'multiple'   => true,
                    ),
                    "ENVIAR_EMAIL_BOAS_VINDAS_AGENTE"  => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave("ENVIAR_EMAIL_BOAS_VINDAS_AGENTE"),
                        'id'      => "agenteBoasVindasEmail",
                        'label'   => "Enviar E-mail De Boas-vindas:",
                        'class'   => "form-control",
                    ),
                    "ENVIAR_SMS_BOAS_VINDAS_AGENTE"    => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave("ENVIAR_SMS_BOAS_VINDAS_AGENTE"),
                        'id'      => "agenteBoasVindasSms",
                        'label'   => "Enviar SMS De Boas-vindas:",
                        'class'   => "form-control",
                    ),
                )
            )
        );

        $arrConfigForm['biblioteca'] = array(
            'descricao' => 'Biblioteca',
            'blocos'    => array(
                'Biblioteca Pearson:' => array(
                    "TOKEN_BIBLIOTECA_PEARSON"    => array(
                        'value' => $this->localizarChave("TOKEN_BIBLIOTECA_PEARSON"),
                        'id'    => "bibliotecaPearsonToken",
                        'label' => "Chave De Autenticação:",
                        'class' => "form-control",
                        'type'  => "text"
                    ),
                    "ENDERECO_BIBLIOTECA_PEARSON" => array(
                        'value' => $this->localizarChave("ENDERECO_BIBLIOTECA_PEARSON"),
                        'id'    => "bibliotecaPearsonEnderecoEletronico",
                        'label' => "Endereço Eletronico Para Acesso A Biblioteca Pearson:",
                        'class' => "form-control",
                        'type'  => "text"
                    ),
                ),
                'Renovação:'          => array(
                    "BIBLIOTECA_RENOVACAO_DIAS_LIMITES"     => array(
                        'value' => $this->localizarChave("BIBLIOTECA_RENOVACAO_DIAS_LIMITES"),
                        'id'    => "bibliotecaRenovacaoDiasLimites",
                        'label' => "Quantidade De Dias Antessores Ao Vencimento Para Renovação:",
                        'class' => "form-control",
                        'type'  => "text",
                    ),
                    "BIBLIOTECA_RENOVACAO_PELO_ALUNO_ATIVO" => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave("BIBLIOTECA_RENOVACAO_PELO_ALUNO_ATIVO"),
                        'id'      => "bibliotecaRenovacaoPeloAluno",
                        'label'   => "Possibilitar Que O Aluno Renove O Acervo:",
                        'class'   => "form-control",
                    ),
                    "BIBLIOTECA_RENOVACAO_CONSECUTIVAS"     => array(
                        'value' => $this->localizarChave("BIBLIOTECA_RENOVACAO_CONSECUTIVAS"),
                        'id'    => "bibliotecaRenovacaoConsecutivas",
                        'label' => "Quantidade Máxima de Renovações Consecutivas:",
                        'class' => "form-control",
                        'type'  => "text",
                    ),
                ),
                'Minha Biblioteca (Biblioteca Virtual):' => array(
                    "BIBLIOTECA_VIRTUAL_API_KEY"  => array(
                        'value' => $this->localizarChave("BIBLIOTECA_VIRTUAL_API_KEY"),
                        'id'    => "bibliotecaVirtualApiKey",
                        'label' => "Chave Para API da Biblioteca Virtual (Api Key):",
                        'class' => "form-control",
                        'type'  => "text",
                    ),
                    "BIBLIOTECA_VIRTUAL_PRODUCAO" => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave("BIBLIOTECA_VIRTUAL_PRODUCAO"),
                        'id'      => "bibliotecaVirtualProducao",
                        'label'   => "Ativar/Desativar o Ambiente de Produção:",
                        'class'   => "form-control",
                    )
                ),
            )
        );

        $arrConfigForm['crm'] = array(
            'descricao' => 'CRM',
            'blocos'    => array(
                array(
                    "EMAIL_ENTRADA_LEAD"      => array(
                        'type'    => "text",
                        'value'   => $this->localizarChave("EMAIL_ENTRADA_LEAD"),
                        'id'      => "crmEntradaLead",
                        'label'   => "Pasta De Leitura Dos Leads:",
                        'class'   => "form-control",
                        'columns' => 6,
                    ),
                    "EMAIL_ARQUIVO_LEAD"      => array(
                        'type'    => "text",
                        'value'   => $this->localizarChave("EMAIL_ARQUIVO_LEAD"),
                        'id'      => "crmArquivoLead",
                        'label'   => "Pasta Leads Processados:",
                        'class'   => "form-control",
                        'columns' => 6,
                    ),
                    "EMAIL_ARQUIVO_ERRO_LEAD" => array(
                        'type'    => "text",
                        'value'   => $this->localizarChave("EMAIL_ARQUIVO_ERRO_LEAD"),
                        'id'      => "crmEntradaLeadErro",
                        'label'   => "Pasta De Leitura Dos Leads Com Erro Ao Processar:",
                        'class'   => "form-control",
                        'columns' => 6,
                    ),
                    "CRM_QUORUM_MINIMO"       => array(
                        'type'    => "text",
                        'value'   => $this->localizarChave("CRM_QUORUM_MINIMO"),
                        'id'      => "crmQuorumMinimo",
                        'label'   => "Percentual de agentes presentes para a distribuíção:",
                        'class'   => "form-control",
                        'columns' => 6,
                    ),
                ),
            )
        );

        $arrConfigForm['cursos'] = array(
            'descricao' => 'Cursos',
            'blocos'    => array(
                array(
                    "EFETUAR_INTEGRACAO_CURSO" => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave("EFETUAR_INTEGRACAO_CURSO"),
                        'id'      => "cursosIntegrarSistemas",
                        'label'   => "Integrar Com Outros Sistemas:",
                        'class'   => "form-control",
                    ),
                ),
            )
        );

        $arrConfigForm['financeiro'] = array(
            'descricao' => 'Financeiro',
            'blocos'    => array(
                'Comportamento:'                                => array(
                    "FINANCEIRO_COMPROVANTE_IMPRESSAO_DIRETA" => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave("FINANCEIRO_COMPROVANTE_IMPRESSAO_DIRETA"),
                        'id'      => "financeiroModal",
                        'label'   => "Comprovante Impressão Direta:",
                        'class'   => "form-control",
                    ),
                    "FINANCEIRO_MEIO_PAGAMENTO_PADRAO"        => array(
                        'options' => $arrMeioPagamento,
                        'value'   => $this->localizarChave("FINANCEIRO_MEIO_PAGAMENTO_PADRAO"),
                        'id'      => "financeiroMeioPgto",
                        'label'   => "Meio De Pagamento Padrão:",
                        'class'   => "form-control",
                    ),
                    "FINANCEIRO_MAX_PARCELAS"                 => array(
                        'type'        => "number",
                        'value'       => $this->localizarChave("FINANCEIRO_MAX_PARCELAS"),
                        'id'          => "financeiroMaxParcelas",
                        'label'       => "Máximo De Parcelas Títulos:",
                        'placeholder' => "Ex.: 9",
                        'class'       => "form-control",
                    ),
                    "FINANCEIRO_MAX_PERCENTUAL_DESCONTO"      => array(
                        'type'        => "number",
                        'value'       => $this->localizarChave("FINANCEIRO_MAX_PERCENTUAL_DESCONTO"),
                        'id'          => "financeiroMaxPercentualDesconto",
                        'label'       => "Percentual Máximo De Desconto Nos Títulos:",
                        'placeholder' => "Ex.: 9",
                        'class'       => "form-control",
                    ),
                    "FINANCEIRO_EMISSAO_CARNE_ATIVADO"        => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave("FINANCEIRO_EMISSAO_CARNE_ATIVADO"),
                        'id'      => "financeiroEmitirCarne",
                        'label'   => "Ativar Emissão de Carnê:",
                        'class'   => "form-control",
                    ),
                    "FINANCEIRO_TITULO_DESCRICAO_FORMATO"     => array(
                        'value'        => $this->localizarChave("FINANCEIRO_TITULO_DESCRICAO_FORMATO"),
                        'id'           => "financeiroTituloDescricaoPadrao",
                        'label'        => "Descrição Padrão Do Título:",
                        'class'        => "form-control",
                        'autocomplete' => "off",
                        'title'        => "Exemplos De uso: " . implode(" ,", $arrDescricaoPadraoTitulo),
                        'data-toogle'  => "tootip-financeiroTituloDescricaoPadrao",
                        'type'         => 'text',
                    ),
                    "FINANCEIRO_SELECAO_MULTIPLA"             => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave("FINANCEIRO_SELECAO_MULTIPLA"),
                        'id'      => "financeiroSelecaoMultipla",
                        'label'   => "Autoriza Seleção De Múltiplas Pessoas No Painel:",
                        'class'   => "form-control",
                    ),
                    "FINANCEIRO_ATUALIZACAO_TITULO_ABERTO"    => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave("FINANCEIRO_ATUALIZACAO_TITULO_ABERTO"),
                        'id'      => "financeiroAtualizaTituloAberto",
                        'label'   => "Habilitar Atualização De Títulos Abertos:",
                        'class'   => "form-control",
                    ),
                    "FINANCEIRO_FILTRO_PADRAO"                => array(
                        'options' => $arrFinanceiroFiltro,
                        'value'   => $this->localizarChave("FINANCEIRO_FILTRO_PADRAO"),
                        'id'      => "financeiroFiltroPadrao",
                        'label'   => "Filtro Padrão Dos Títulos:",
                        'class'   => "form-control",
                    ),
                ),
                'Pagamento por Cartão MaxiPago'                 => array(
                    "FINANCEIRO_MAXI_PAGO_ATIVO"                      => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave("FINANCEIRO_MAXI_PAGO_ATIVO"),
                        'id'      => "financeiroMaxiPagoAtivo",
                        'label'   => "Possibilitar Realizar Pagamentos Usando o MaxiPago:",
                        'class'   => "form-control",
                    ),
                    "FINANCEIRO_CARTAO_AMBIENTE"                      => array(
                        'options' => $arrAmbientesMaxiPago,
                        'value'   => $this->localizarChave("FINANCEIRO_CARTAO_AMBIENTE"),
                        'id'      => "financeiroCartaoAmbiente",
                        'label'   => "Ambiente em uso:",
                        'class'   => "form-control",
                    ),
                    "FINANCEIRO_CARTAO_IDENTIFICACAO_ESTABELECIMENTO" => array(
                        'value'        =>
                            $this->localizarChave("FINANCEIRO_CARTAO_IDENTIFICACAO_ESTABELECIMENTO"),
                        'type'         => 'text',
                        'id'           => "financeiroCartaoIdentificacaoEstabelecimento",
                        'label'        => "Identificação da Loja/Estabelecimento:",
                        'class'        => "form-control",
                        'autocomplete' => "off",
                    ),
                    "FINANCEIRO_CARTAO_CHAVE_ESTABELECIMENTO"         => array(
                        'value'        => $this->localizarChave("FINANCEIRO_CARTAO_CHAVE_ESTABELECIMENTO"),
                        'type'         => 'text',
                        'id'           => "financeiroCartaoChaveEstabelecimento",
                        'label'        => "Chave da Loja/Estabelecimento:",
                        'class'        => "form-control",
                        'autocomplete' => "off",
                    ),
                    "FINANCEIRO_CARTAO_BANDEIRAS_PERMITIDAS"          => array(
                        'options'    => $arrBandeirasCartao,
                        'value'      => $this->localizarChave("FINANCEIRO_CARTAO_BANDEIRAS_PERMITIDAS"),
                        'data-valor' => json_encode(
                            explode(",", $this->localizarChave("FINANCEIRO_CARTAO_BANDEIRAS_PERMITIDAS"))
                        ),
                        'id'         => "financeiroCartaoBandeirasPermitidas",
                        'label'      => "Bandeiras Permitidas:",
                        'class'      => "form-control",
                        'multiple'   => true,
                    ),
                    "FINANCEIRO_CARTAO_CODIGO_ADQUIRENTE"             => array(
                        'options' => $arrAdquirentesMaxiPago,
                        'value'   => $this->localizarChave("FINANCEIRO_CARTAO_CODIGO_ADQUIRENTE"),
                        'id'      => "financeiroCartaoCodigoAdquirente",
                        'label'   => "Adquirente que irá Processar a Transação:",
                        'class'   => "form-control",
                    ),
                ),
                'Configuração de Pesquisa de Pessoas e Alunos:' => array(
                    "FINANCEIRO_EXIBICAO_CODIGO_PESSOA"      => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave("FINANCEIRO_EXIBICAO_CODIGO_PESSOA"),
                        'id'      => "financeiroExibeCodigoPessoa",
                        'label'   => "Exibir Código Da Pessoa:",
                        'class'   => "form-control",
                    ),
                    "FINANCEIRO_EXIBICAO_PESSOA_JURIDICA"    => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave("FINANCEIRO_EXIBICAO_PESSOA_JURIDICA"),
                        'id'      => "financeiroExibeJuridica",
                        'label'   => "Exibir Pessoas Jurídicas Na Busca:",
                        'class'   => "form-control",
                    ),
                    "FINANCEIRO_BUSCA_EXIBICAO_CPF_PESSOA"   => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave("FINANCEIRO_BUSCA_EXIBICAO_CPF_PESSOA"),
                        'id'      => "financeiroExibeCPF",
                        'label'   => "Exibir CPF Na Busca:",
                        'class'   => "form-control",
                    ),
                    "FINANCEIRO_BUSCA_EXIBICAO_CODIGO_ALUNO" => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave("FINANCEIRO_BUSCA_EXIBICAO_CODIGO_ALUNO"),
                        'id'      => "financeiroExibeCodigoAluno",
                        'label'   => "Exibir Código Do Aluno Na Busca:",
                        'class'   => "form-control",
                    ),
                    "FINANCEIRO_EXIBICAO_SITUACAO_ALUNO"     => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave("FINANCEIRO_EXIBICAO_SITUACAO_ALUNO"),
                        'id'      => "financeiroExibicaoSituacaoAluno",
                        'label'   => "Exibir Situacao Do Aluno:",
                        'class'   => "form-control",
                    ),
                    "FINANCEIRO_EXIBICAO_PERIODO_ALUNO"      => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave("FINANCEIRO_EXIBICAO_PERIODO_ALUNO"),
                        'id'      => "financeiroExibePeriodoAluno",
                        'label'   => "Exibir Último Período Letivo Do Aluno:",
                        'class'   => "form-control",
                    ),
                    "FINANCEIRO_EXIBICAO_TURMA_ALUNO"        => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave("FINANCEIRO_EXIBICAO_TURMA_ALUNO"),
                        'id'      => "financeiroExibeTurmaAluno",
                        'label'   => "Exibir Última Turma Do Aluno:",
                        'class'   => "form-control",
                    ),
                ),
                'Configurações de Boleto:'                      => array(
                    "BOLETO_ATIVO"           => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave("BOLETO_ATIVO"),
                        'id'      => "financeiroBoletoAtivo",
                        'label'   => "Ativar Boletos:",
                        'class'   => "form-control",
                        'columns' => 4
                    ),
                    "BOLETO_REMESSA_ATIVADA" => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave("BOLETO_REMESSA_ATIVADA"),
                        'id'      => "financeiroBoletoRemessa",
                        'label'   => "Libera Apenas Boleto Contidos Em Remessas:",
                        'class'   => "form-control",
                        'columns' => 4,
                    ),
                    "BOLETO_ATIVO_ALUNO"     => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave("BOLETO_ATIVO_ALUNO"),
                        'id'      => "boletoAtivoAluno",
                        'label'   => "Libera Boletos Para Alunos:",
                        'class'   => "form-control",
                        'columns' => 4,
                    ),
                ),
                'Desconto:'                                     => array(
                    "PORCETAGEM_DESCONTO_PAINEL_FINANCEIRO_ALUNOPERID"     => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave("PORCETAGEM_DESCONTO_PAINEL_FINANCEIRO_ALUNOPERID"),
                        'id'      => "porcetagemDescontoPainelFinanceiroAlunoperid",
                        'label'   => "Habilitar Indicador De Desconto Do Painel Por Período:",
                        'class'   => "form-control",
                    ),
                    "FINANCEIRO_TIPOS_TITULO_CALCULO_PORCENTAGEM_DESCONTO" => array(
                        'options'    => $arrTipoTitulo,
                        'value'      => $this->localizarChave("FINANCEIRO_TIPOS_TITULO_CALCULO_PORCENTAGEM_DESCONTO"),
                        'data-valor' => json_encode(
                            explode(",", $this->localizarChave("FINANCEIRO_TIPOS_TITULO_CALCULO_PORCENTAGEM_DESCONTO"))
                        ),
                        'id'         => "financeiroTiposTituloCalculoPorcentagemDesconto",
                        'label'      => "Tipos De Título Contabilizados No Indicador De Desconto Do Painel:",
                        'class'      => "form-control",
                        'multiple'   => true,
                    ),
                    "FINANCEIRO_CONFIG_CALCULO_DESCONTO_INCENTIVO"         => array(
                        'options' => $arrFormaCalculoDesconto,
                        'value'   => $this->localizarChave("FINANCEIRO_CONFIG_CALCULO_DESCONTO_INCENTIVO"),
                        'id'      => "financeiroConfigCalculoDescontoIncentivo",
                        'label'   => "Selecione Sobre Qual Valor Deve Ser Calculado o Desconto De Incentivo:",
                        'class'   => "form-control",
                    ),
                ),
                'Outros:'                                       => array(
                    "FINANCEIRO_FILTRO_GRUPO_ACESSO"                       => array(
                        'options'    => $arrGrupos,
                        'value'      => $this->localizarChave("FINANCEIRO_FILTRO_GRUPO_ACESSO"),
                        'data-valor' => json_encode(
                            explode(",", $this->localizarChave("FINANCEIRO_FILTRO_GRUPO_ACESSO"))
                        ),
                        'id'         => "financeiroGrupo",
                        'label'      => "Grupos De Usuário Visíveis Para Relatório Fechamento De Caixa:",
                        'class'      => "form-control",
                        'multiple'   => true,
                    ),
                    "GRUPO_FINANCEIRO_SUPERVISAO"                          => array(
                        'options'    => $arrGrupos,
                        'value'      => $this->localizarChave("GRUPO_FINANCEIRO_SUPERVISAO"),
                        'data-valor' => json_encode(
                            explode(",", $this->localizarChave("GRUPO_FINANCEIRO_SUPERVISAO"))
                        ),
                        'id'         => "financeiroGrupoSupervisao",
                        'label'      => "Grupos De Usuário de Supervisores Financeiros:",
                        'class'      => "form-control",
                        'multiple'   => true,
                    ),
                    "FINANCEIRO_ENVIAR_TITULOS_COPIA_AGENTE"               => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave("FINANCEIRO_ENVIAR_TITULOS_COPIA_AGENTE"),
                        'id'      => "financeiroEnviarTitulosCopiaAgente",
                        'label'   => "Habilitar Envio E-mail De Títulos De Aluno Com Cópia Para Agente:",
                        'class'   => "form-control",
                    ),
                    "FINANCEIRO_TIPOS_TITULO_ALTERACAO_SITUACAO_ACADEMICA" => array(
                        'options'    => $arrTipoTitulo,
                        'value'      => $this->localizarChave("FINANCEIRO_TIPOS_TITULO_ALTERACAO_SITUACAO_ACADEMICA"),
                        'data-valor' => json_encode(
                            explode(",", $this->localizarChave("FINANCEIRO_TIPOS_TITULO_ALTERACAO_SITUACAO_ACADEMICA"))
                        ),
                        'id'         => "financeiroTiposTituloAlteracaoSituacaoAcademica",
                        'label'      => "Tipos De Título Que Impactarão na Alteração da Situação Acadêmica:",
                        'class'      => "form-control",
                        'multiple'   => true,
                    ),
                    "FINANCEIRO_ENVIAR_EMAIL_COBRANCA_MAXIPAGO" => array(
                        'options'  => $arrSimNao,
                        'value'    => $this->localizarChave("FINANCEIRO_ENVIAR_EMAIL_COBRANCA_MAXIPAGO"),
                        'id'       => "financeiroEnviarEmailCobrancaMaxiPago",
                        'label'    => "Permitir Envio de Email para Cobrança MaxiPago:",
                        'class'    => "form-control",
                    ),
                    "FINANCEIRO_ENVIAR_SMS_COBRANCA_MAXIPAGO"   => array(
                        'options'  => $arrSimNao,
                        'value'    => $this->localizarChave("FINANCEIRO_ENVIAR_SMS_COBRANCA_MAXIPAGO"),
                        'id'       => "financeiroEnviarSmsCobrancaMaxiPago",
                        'label'    => "Permitir Envio de SMS para Cobrança MaxiPago:",
                        'class'    => "form-control",
                    ),
                    "FINANCEIRO_ENVIAR_EMAIL_RECORRENCIA_MAXIPAGO" => array(
                        'options'  => $arrSimNao,
                        'value'    => $this->localizarChave("FINANCEIRO_ENVIAR_EMAIL_RECORRENCIA_MAXIPAGO"),
                        'id'       => "financeiroEnviarEmailRecorrenciaMaxiPago",
                        'label'    => "Permitir Envio de Email Sobre Falhas De Recorrência MaxiPago:",
                        'class'    => "form-control",
                    ),
                    "FINANCEIRO_ENVIAR_SMS_RECORRENCIA_MAXIPAGO"   => array(
                        'options'  => $arrSimNao,
                        'value'    => $this->localizarChave("FINANCEIRO_ENVIAR_EMAIL_RECORRENCIA_MAXIPAGO"),
                        'id'       => "financeiroEnviarSmsRecorrenciaMaxiPago",
                        'label'    => "Permitir Envio de Email Sobre Falhas De Recorrência MaxiPago:",
                        'class'    => "form-control",
                    ),
                )
            )
        );

        $arrConfigForm['pagseguro'] = array(
            'descricao' => 'Pag Seguro',
            'blocos'    => array(
                array(
                    "PAGSEGURO_ATIVO"        => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave("PAGSEGURO_ATIVO"),
                        'id'      => "pagseguroAtivo",
                        'label'   => "Habilita Pagamento De Títulos Com PagSeguro:",
                        'class'   => "form-control",
                        'columns' => 4,
                    ),
                    "PAGSEGURO_STATUS_BAIXA" => array(
                        'options' => \Financeiro\Service\IntegracaoPagseguro::situacoesPagSeguro(),
                        'value'   => $this->localizarChave("PAGSEGURO_STATUS_BAIXA"),
                        'id'      => "pagseguroStatusBaixa",
                        'label'   => "Status Relacionado a Pagamento No PagSeguro:",
                        'class'   => "form-control",
                        'columns' => 4,
                    ),
                    "PAGSEGURO_AMBIENTE"     => array(
                        'options' => ['sandbox' => "Desenvolvimento", 'production' => "Produção"],
                        'value'   => $this->localizarChave("PAGSEGURO_AMBIENTE"),
                        'id'      => "pagseguroAmbiente",
                        'label'   => "Ambiente De Uso Do PagSeguro:",
                        'class'   => "form-control",
                        'columns' => 4,
                    ),
                ),
                array(
                    "PAGSEGURO_EMAIL" => array(
                        'value'       => $this->localizarChave("PAGSEGURO_EMAIL"),
                        'id'          => "pagseguroEmail",
                        'label'       => "E-mail PagSeguro:",
                        'class'       => "form-control",
                        "placeholder" => "exemplo@dominio.com",
                        "type"        => "text",
                    ),
                    "PAGSEGURO_TOKEN" => array(
                        'value'       => $this->localizarChave("PAGSEGURO_TOKEN"),
                        'id'          => "pagseguroToken",
                        'label'       => "Token Autenticação PagSeguro:",
                        'class'       => "form-control",
                        "placeholder" => "Token Autenticação",
                        "type"        => "text",
                    ),
                ),
                array(
                    "PAGSEGURO_EMAIL_SANDBOX" => array(
                        'value'       => $this->localizarChave("PAGSEGURO_EMAIL_SANDBOX"),
                        'id'          => "pagseguroEmailHomologacao",
                        'label'       => "E-mail de Homologação do PagSeguro:",
                        'class'       => "form-control",
                        "placeholder" => "exemplo@dominio.com",
                        "type"        => "text",
                    ),
                    "PAGSEGURO_TOKEN_SANDBOX" => array(
                        'value'       => $this->localizarChave("PAGSEGURO_TOKEN_SANDBOX"),
                        'id'          => "pagseguroTokenHomologacao",
                        'label'       => "Token De Autenticação De Homologação Pagseguro:",
                        'class'       => "form-control",
                        "placeholder" => "Token autenticação",
                        "type"        => "text",
                    ),
                ),
            )
        );

        $arrConfigForm['sistema'] = array(
            'descricao' => 'Sistema',
            'blocos'    => array(
                array(
                    "SIS_FILA_EMAIL_TAMANHO" => array(
                        'value'       => $this->localizarChave("SIS_FILA_EMAIL_TAMANHO"),
                        'id'          => "sistemaFilaEmail",
                        'label'       => "Tamanho Da Fila De E-mail:",
                        'class'       => "form-control",
                        "placeholder" => "Ex.: 9",
                        "type"        => "number",
                    ),
                    "SIS_FILA_SMS_TAMANHO"   => array(
                        'value'       => $this->localizarChave("SIS_FILA_SMS_TAMANHO"),
                        'id'          => "sistemaFilaSms",
                        'label'       => "Tamanho Da Fila De Sms:",
                        'class'       => "form-control",
                        "placeholder" => "Ex.: 9",
                        "type"        => "number",
                    ),
                    "SIS_FILA_MAXIPAGO_TAMANHO"   => array(
                        'value'       => $this->localizarChave("SIS_FILA_MAXIPAGO_TAMANHO"),
                        'id'          => "sistemaFilaMaxipago",
                        'label'       => "Tamanho Da Fila De Processamento Maxipago:",
                        'class'       => "form-control",
                        "placeholder" => "Ex.: 9",
                        "type"        => "number",
                    ),
                ),
                array(
                    "SIS_FILA_INTEGRACAO_TAMANHO"   => array(
                        'value'       => $this->localizarChave("SIS_FILA_INTEGRACAO_TAMANHO"),
                        'id'          => "sistemaFilaIntegracao",
                        'label'       => "Tamanho Da Fila De Integrações:",
                        'class'       => "form-control",
                        "placeholder" => "Ex.: 9",
                        "type"        => "number",
                    ),
                    "SIS_ROBO_PROCESSAMENTO_LIMITE" => array(
                        'value'       => $this->localizarChave("SIS_ROBO_PROCESSAMENTO_LIMITE"),
                        'id'          => "sistemaProcessamentoLimite",
                        'label'       => "Tempo Limite De Processamento:",
                        'class'       => "form-control",
                        "placeholder" => "Em minutos. Ex.: 15",
                        "type"        => "number",
                    ),
                ),
                array(
                    "BTN_INTEGRACAO_MANUAL_NOTA" => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave("BTN_INTEGRACAO_MANUAL_NOTA"),
                        'id'      => "sistemaIntegracaoManualNota",
                        'label'   => "Permitir A Visualização Do Botão De Integração De Nota:",
                        'class'   => "form-control",
                    ),

                ),
            )
        );

        $arrConfigForm['sms'] = array(
            'descricao' => 'SMS',
            'blocos'    => array(
                array(
                    "SMS_ATIVO"    => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave("SMS_ATIVO"),
                        'id'      => "smsAtivo",
                        'label'   => "Ativa O Envio De SMS:",
                        'class'   => "form-control",
                    ),
                    "SMS_PROVEDOR" => array(
                        'options' => ['LOCASMS' => 'Loca SMS', 'SMSEMPRESA' => 'SMS Empresa'],
                        'value'   => $this->localizarChave("SMS_PROVEDOR"),
                        'id'      => "smsProvedor",
                        'label'   => "Provedor de Envio De SMS:",
                        'class'   => "form-control",
                    ),
                ),
                'Loca SMS:'    => array(
                    "LOCASMS_USER"     => array(
                        'value'       => $this->localizarChave("LOCASMS_USER"),
                        'id'          => "smsUser",
                        'label'       => "Usuário Loca SMS:",
                        'class'       => "form-control",
                        'type'        => "text",
                        'placeholder' => "Login do usuário no Loca SMS",
                    ),
                    "LOCASMS_PASSWORD" => array(
                        'value'       => $this->localizarChave("LOCASMS_PASSWORD"),
                        'id'          => "smsPassword",
                        'label'       => "Senha Loca SMS:",
                        'class'       => "form-control",
                        'type'        => "text",
                        'placeholder' => "Autenticação sistema Loca SMS",
                    ),
                ),
                'SMS Empresa:' => array(
                    "SMSEMPRESA_CHAVE" => array(
                        'value'       => $this->localizarChave("SMSEMPRESA_CHAVE"),
                        'id'          => "smsEmpresaChave",
                        'label'       => "Chave API SMS Empresa:",
                        'class'       => "form-control",
                        'type'        => "text",
                        'placeholder' => "Chave API SMS Empresa",
                        'columns'     => 12,
                    ),
                ),
            )
        );

        $arrConfigForm['outros'] = array(
            'descricao' => 'Outros',
            'blocos'    => array(
                array(
                    "FRESHDESK_LINK"               => array(
                        'value'       => $this->localizarChave("FRESHDESK_LINK"),
                        'id'          => "outrosFreshDesk",
                        'label'       => "Url Freshdesk:",
                        'class'       => "form-control",
                        'type'        => "url",
                        'placeholder' => "Exemplo.: http://universa.freshdesk.com",
                        'columns'     => 12,
                    ),
                    "PROTOCOLO_MENSAGEM_PADRAO_ID" => array(
                        'options' => $arrMensagemPadrao,
                        'value'   => $this->localizarChave("PROTOCOLO_MENSAGEM_PADRAO_ID"),
                        'id'      => "outrosProtocoloMensagem",
                        'label'   => "Mensagem Padrão Do Protocolo:",
                        'class'   => "form-control",
                        'columns' => 12,
                    ),
                    "ALTURA_CABECALHO_TIMBRADO"    => array(
                        'value'   => $this->localizarChave("ALTURA_CABECALHO_TIMBRADO"),
                        'id'      => "alturaCabecalhoTimbrado",
                        'label'   => "Altura do Cabeçalho do Papel Timbrado:",
                        'class'   => "form-control",
                        'type'    => "text",
                        'columns' => 3,
                    ),
                    "ALTURA_RODAPE_TIMBRADO"       => array(
                        'value'   => $this->localizarChave("ALTURA_RODAPE_TIMBRADO"),
                        'id'      => "alturaRodapeTimbrado",
                        'label'   => "Altura do Rodapé do Papel Timbrado:",
                        'class'   => "form-control",
                        'type'    => "text",
                        'columns' => 3,
                    ),
                    'LINK_ACESSO_AVA'              => array(
                        'value'   => $this->localizarChave('LINK_ACESSO_AVA'),
                        'id'      => 'linkAcessoAva',
                        'label'   => 'Link De Acesso Ao AVA:',
                        'class'   => "form-control",
                        'type'    => "text",
                        'columns' => 6,
                    ),
                    "CARTEIRINHA_MENSAGEM"         => array(
                        'value'   => $this->localizarChave("CARTEIRINHA_MENSAGEM"),
                        'id'      => "outrosCarteirinhaMensagem",
                        'label'   => "Mensagem Contida Na Carteirinha:",
                        'class'   => "form-control col-md-10",
                        'type'    => "textarea",
                        'columns' => 12,
                    ),
                ),
                'Pesquisa Institucional Para Professor:' => array(
                    'PESQUISA_INSTITUCIONAL_PROFESSOR'          => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave('PESQUISA_INSTITUCIONAL_PROFESSOR'),
                        'id'      => 'pesquisaInstitucionalProfessor',
                        'label'   => 'Ativar Pesquisa Institucional Professor:',
                        'class'   => "form-control",
                    ),
                    'PESQUISA_INSTITUCIONAL_PROFESSOR_ENDERECO' => array(
                        'value' => $this->localizarChave('PESQUISA_INSTITUCIONAL_PROFESSOR_ENDERECO'),
                        'id'    => 'pesquisaInstitucionalProfessorEndereco',
                        'label' => 'Endereço Da Pesquisa Institucional Professor:',
                        'class' => "form-control",
                        'type'  => "text",
                    ),
                    'PESQUISA_INSTITUCIONAL_PROFESSOR_MSG'      => array(
                        'value'   => $this->localizarChave('PESQUISA_INSTITUCIONAL_PROFESSOR_MSG'),
                        'id'      => 'pesquisaInstitucionalProfessorMsg',
                        'label'   => 'Mensagem Da Pesquisa Institucional Professor:',
                        'class'   => "form-control",
                        'columns' => '12',
                        'type'    => "textarea",
                    ),
                ),
                'CPA:'                                   => array(
                    'AVALIACAO_EXIBICAO_GRADE' => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave('AVALIACAO_EXIBICAO_GRADE'),
                        'id'      => 'avaliacaoExibicaoGrade',
                        'label'   => 'Exibir Questões Em Grade:',
                        'class'   => "form-control",
                        'columns' => 4,
                    ),
                ),
                'API Click2Call Sonax:'                  => array(
                    'SONAX_CLICK2CALL_ATIVO' => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave('SONAX_CLICK2CALL_ATIVO'),
                        'id'      => 'sonaxClick2callAtivo',
                        'label'   => 'Ativar Click2Call:',
                        'class'   => "form-control",
                        'columns' => 4,
                    ),
                    'SONAX_CLICK2CALL_CONTA' => array(
                        'value'   => $this->localizarChave('SONAX_CLICK2CALL_CONTA'),
                        'id'      => 'sonaxClick2callConta',
                        'label'   => 'Conta Sonax:',
                        'class'   => "form-control",
                        'type'    => "text",
                        'columns' => 4,
                    ),
                    'SONAX_CLICK2CALL_TOKEN' => array(
                        'value'   => $this->localizarChave('SONAX_CLICK2CALL_TOKEN'),
                        'id'      => 'sonaxClick2callToken',
                        'label'   => 'Token da API:',
                        'class'   => "form-control",
                        'type'    => "text",
                        'columns' => 4,
                    ),
                ),
                'Envio de Emails via Sendgrid:'          => array(
                    'SENDGRID_ATIVO' => array(
                        'options' => $arrSimNao,
                        'value'   => $this->localizarChave('SENDGRID_ATIVO'),
                        'id'      => 'sendgridAtivo',
                        'label'   => 'Ativar Envio de Emails via Sendgrid:',
                        'class'   => "form-control",
                        'columns' => 6,
                    ),
                    'SENDGRID_CHAVE' => array(
                        'value'   => $this->localizarChave('SENDGRID_CHAVE'),
                        'id'      => 'sendgridChave',
                        'label'   => 'Chave da API:',
                        'class'   => "form-control",
                        'type'    => "text",
                        'columns' => 6,
                    ),
                ),
            )
        );

        return $arrConfigForm;
    }
}