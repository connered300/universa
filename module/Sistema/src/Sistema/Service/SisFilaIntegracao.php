<?php

namespace Sistema\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class SisFilaIntegracao
 * @package Sistema\Service
 */
class SisFilaIntegracao extends AbstractService
{
    const FILA_SITUACAO_PENDENTE    = 'Pendente';
    const FILA_SITUACAO_ENVIADO     = 'Enviado';
    const FILA_SITUACAO_PROCESSANDO = 'Processando';
    const FILA_SITUACAO_ERRO        = 'Erro';

    const FILA_SUCESSO = 'SUCESSO';

    private $__lastError = null;

    /** @var \Sistema\Service\SisConfig */
    private $config = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, $arrConfig = array())
    {
        parent::__construct($em, 'Sistema\Entity\SisFilaIntegracao');

        $this->setConfig(new \Sistema\Service\SisConfig($em, $arrConfig));
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @return SisConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param SisConfig $config
     * @return SisFilaSms
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function pesquisaForJson($params)
    {
        $sql          = 'SELECT * FROM sis__fila_integracao WHERE';
        $integracaoId = false;
        $filaId       = false;

        if ($params['q']) {
            $integracaoId = $params['q'];
        } elseif ($params['query']) {
            $integracaoId = $params['query'];
        }

        if ($params['filaId']) {
            $filaId = $params['filaId'];
        }

        $parameters = array('integracao_id' => "{$integracaoId}%");
        $sql .= ' integracao_id LIKE :integracao_id';

        if ($filaId) {
            $parameters['fila_id'] = $filaId;
            $sql .= ' AND fila_id <> :fila_id';
        }

        $sql .= " ORDER BY integracao_id";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "
        SELECT
        integracao_id, fila_id, fila_conteudo, fila_retorno,
        fila_data_cadastro, fila_data_processamento, fila_data_finalizacao,
        fila_chave, fila_situacao, integracao_descricao
        FROM sis__fila_integracao
        INNER JOIN sis__integracao USING(integracao_id)
        ";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $filaId
     * @return array
     */
    public function getArray($filaId)
    {
        /** @var $objSisFilaIntegracao \Sistema\Entity\SisFilaIntegracao */
        $objSisFilaIntegracao = $this->getRepository()->find($filaId);
        $serviceSisIntegracao = new \Sistema\Service\SisIntegracao($this->getEm());

        try {
            $arrDados = $objSisFilaIntegracao->toArray();

            if ($arrDados['integracao']) {
                /** @var $objSisIntegracao \Sistema\Entity\SisIntegracao */
                $objSisIntegracao = $serviceSisIntegracao->getRepository()->find($arrDados['integracao']);

                if ($objSisIntegracao) {
                    $arrDados['integracao'] = array(
                        'id'   => $objSisIntegracao->getIntegracaoId(),
                        'text' => $objSisIntegracao->getIntegracaoDescricao()
                    );
                } else {
                    $arrDados['integracao'] = null;
                }
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceSisIntegracao = new \Sistema\Service\SisIntegracao($this->getEm());

        if (is_array($arrDados['integracao']) && !$arrDados['integracao']['id']) {
            $arrDados['integracao']['id']   = $arrDados['integracao']['integracaoId'];
            $arrDados['integracao']['text'] = $arrDados['integracao']['integracaoDescricao'];
        } elseif ($arrDados['integracao'] && is_string($arrDados['integracao'])) {
            $arrDados['integracao'] = $serviceSisIntegracao->getArrSelect2(
                array('id' => $arrDados['integracao'])
            );
        }

        return $arrDados;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('filaId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Sistema\Entity\SisFilaIntegracao */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getFilaId(),
                $params['value'] => $objEntity->getIntegracao()->getIntegracaoDescricao()
            );
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['filaId']) {
            $this->setLastError('Para remover um registro de fila de integração é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objSisFilaIntegracao \Sistema\Entity\SisFilaIntegracao */
            $objSisFilaIntegracao = $this->getRepository()->find($param['filaId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objSisFilaIntegracao);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de fila de integração.');

            return false;
        }

        return true;
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceSisIntegracao = new \Sistema\Service\SisIntegracao($this->getEm());

        $serviceSisIntegracao->setarDependenciasView($view);

        $view->setVariable("arrFilaSituacao", $this->getArrSelect2FilaSituacao());
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2FilaSituacao($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getFilaSituacao());
    }

    /**
     * @return array
     */
    public static function getFilaSituacao()
    {
        return array(
            self::FILA_SITUACAO_PENDENTE,
            self::FILA_SITUACAO_ENVIADO,
            self::FILA_SITUACAO_PROCESSANDO,
            self::FILA_SITUACAO_ERRO
        );
    }

    /**
     * @return array
     */
    public function executarEnvio()
    {
        $numEnvios  = 0;
        $numSucesso = 0;

        try {
            @ini_set('memory_limit', '-1');
            $waitTimeout        = $this->executeQuery('SHOW SESSION VARIABLES LIKE "wait_timeout"')->fetch();
            $interactiveTimeout = $this->executeQuery('SHOW SESSION VARIABLES LIKE "interactive_timeout"')->fetch();
            $waitTimeout        = $waitTimeout['Value'];
            $interactiveTimeout = $interactiveTimeout['Value'];

            $this->executeQuery('SET @@session.wait_timeout=' . (60 * 60));
            $this->executeQuery('SET @@session.interactive_timeout=' . (60 * 60));

            $tamanhoFila = $this->getConfig()->localizarChave('SIS_FILA_INTEGRACAO_TAMANHO', 5);

            $arrObjSisFilaIntegracao = $this->getRepository()->findBy(
                ['filaSituacao' => self::FILA_SITUACAO_PENDENTE],
                ['filaDataCadastro' => 'asc'],
                $tamanhoFila,
                0
            );

            /** @var $objSisFilaIntegracao \Sistema\Entity\SisFilaIntegracao */
            foreach ($arrObjSisFilaIntegracao as $objSisFilaIntegracao) {
                $objSisFilaIntegracao->setFilaSituacao(self::FILA_SITUACAO_PROCESSANDO);
                $this->getEm()->persist($objSisFilaIntegracao);
                $this->getEm()->flush($objSisFilaIntegracao);
            }

            /** @var $objSisFilaIntegracao \Sistema\Entity\SisFilaIntegracao */
            foreach ($arrObjSisFilaIntegracao as $objSisFilaIntegracao) {
                $objSisFilaIntegracao->setFilaDataProcessamento(new \DateTime());
                $this->getEm()->persist($objSisFilaIntegracao);
                $this->getEm()->flush($objSisFilaIntegracao);

                $arrParams = $this->readConfigFromString($objSisFilaIntegracao->getFilaConteudo());

                $retorno = $this->integracao($arrParams, $objSisFilaIntegracao->getIntegracao());

                if (!$retorno) {
                    $this->setLastError("Erro ao executar fila de integração");

                    return false;
                }

                $numSucesso++;

                $objSisFilaIntegracao
                    ->setFilaSituacao(self::FILA_SITUACAO_ENVIADO)
                    ->setFilaDataFinalizacao(new \DateTime())
                    ->setFilaRetorno($retorno);

                if ($retorno != self::FILA_SUCESSO) {
                    $objSisFilaIntegracao->setFilaSituacao(self::FILA_SITUACAO_ERRO);
                }

                $this->getEm()->persist($objSisFilaIntegracao);
                $this->getEm()->flush($objSisFilaIntegracao);

                $numEnvios++;
            }

            $this->executeQuery('SET @@session.wait_timeout=' . $waitTimeout);
            $this->executeQuery('SET @@session.interactive_timeout=' . $interactiveTimeout);
        } catch (\Exception $ex) {
            return ['erro' => $ex->getTraceAsString()];
        }

        return ['envios' => $numEnvios, 'sucesso' => $numSucesso, 'erro' => $numEnvios - $numSucesso];
    }

    /**
     * @param \Matricula\Entity\AcadCurso|int $curso
     * @return bool
     */
    public function registrarCursoNaFilaDeIntegracao($curso)
    {
        $serviceCurso      = new \Matricula\Service\AcadCurso($this->getEm());
        $serviceModalidade = new \Matricula\Service\AcadModalidade($this->getEm());
        $serviceIntegracao = new \Sistema\Service\SisIntegracao($this->getEm());

        $arrDadosCurso = $serviceCurso->getArray($curso);

        if ($arrDadosCurso['modNome'] == $serviceModalidade::MODALIDADE_DESCRICAO_PRESENCIAL) {
            return false;
        }

        if ($arrDadosCurso['cursoSituacao'] != $serviceCurso::CURSO_SITUACAO_ATIVO) {
            return false;
        }

        $arrObjSisIntegracao = $serviceIntegracao->buscaIntegracoesAtivas();

        /** @var $objSisIntegracao \Sistema\Entity\SisIntegracao */
        foreach ($arrObjSisIntegracao as $objSisIntegracao) {
            $arrContent = [
                'method'     => 'integrarCurso',
                'nome'       => preg_replace("/[\n\r\t]/", "", $arrDadosCurso['cursoNome']),
                'descricao'  => preg_replace("/[\n\r\t]/", "", $arrDadosCurso['cursoInformacoes']),
                'area'       => '',
                'nivel'      => '',
                'modalidade' => '',
                'cursoId'    => $arrDadosCurso['cursoId']
            ];

            if ($arrDadosCurso['areaDescricao']) {
                $arrContent['area'] = $arrDadosCurso['areaDescricao'];
            }

            if ($arrDadosCurso['modNome']) {
                $arrContent['modalidade'] = $arrDadosCurso['modNome'];
            }

            if ($arrDadosCurso['nivelNome']) {
                $arrContent['nivel'] = $arrDadosCurso['nivelNome'];
            }

            $arrSisIntegracao = $serviceIntegracao->getArray($objSisIntegracao->getIntegracaoId());

            $arrParam = ['integracao' => $objSisIntegracao, 'filaConteudo' => $arrContent];

            foreach ($arrSisIntegracao['campus'] as $arrIntegracaoCampus) {
                foreach ($arrDadosCurso['campus'] as $arrCursoCampus) {
                    if ($arrCursoCampus['campId'] == $arrIntegracaoCampus['campId']) {
                        $arrParam = $this->tratarRegistroParaColocarNaFila($arrParam);

                        if (!$this->save($arrParam)) {
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }

    public function registrarAlunoNaFilaDeIntegracao($objAcadgeralAluno)
    {
        $serviceIntegracao = new \Sistema\Service\SisIntegracao($this->getEm());

        if (!$arrObjSisIntegracao = $serviceIntegracao->buscaIntegracoesAtivas()) {
            $this->setLastError("Nenhuma integração ativa");

            return false;
        }

        /** @var \Matricula\Entity\AcadgeralAluno $objAcadgeralAluno */
        $serviceAlunoCurso           = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
        $serviceDiscCurso            = new \Matricula\Service\AcadgeralDisciplinaCurso($this->getEm());
        $serviceCurso                = new \Matricula\Service\AcadCurso($this->getEm());
        $serviceAcadgeralDisciplina  = new \Matricula\Service\AcadgeralDisciplina($this->getEm());
        $serviceAcadgeralSituacao    = new \Matricula\Service\AcadgeralSituacao($this->getEm());
        $serviceAcadgeralAluno       = new \Matricula\Service\AcadgeralAluno($this->getEm());
        $serviceIntegracaoTurma      = new \Sistema\Service\SisIntegracaoTurma($this->getEm());
        $serviceIntegracaoCurso      = new \Sistema\Service\SisIntegracaoCurso($this->getEm());
        $serviceIntegracaoDisciplina = new \Sistema\Service\SisIntegracaoDisciplina($this->getEm());
        $serviceModalidade           = new \Matricula\Service\AcadModalidade($this->getEm());

        $arrDadosAluno = $serviceAcadgeralAluno->getArray($objAcadgeralAluno);

        $login = preg_replace("/[^0-9]/", "", $arrDadosAluno['pesCpf']);
        $senha = substr($login, 0, 5);

        $arrContent = [
            'method'      => 'integrarAluno',
            'alunoId'     => $arrDadosAluno['alunoId'],
            'nome'        => preg_replace("/[\n\r\t]/", "", $arrDadosAluno['pesNome']),
            'login'       => $login,
            'senha'       => $senha,
            'email'       => $arrDadosAluno['conContatoEmail'],
            'cursos'      => [],
            'turmas'      => [],
            'disciplinas' => []
        ];

        $ontem = strtotime('-1 day');

        /** @var $objSisIntegracao \Sistema\Entity\SisIntegracao */
        foreach ($arrObjSisIntegracao as $objSisIntegracao) {
            $arrIntegracao             = $serviceIntegracao->getArray($objSisIntegracao->getIntegracaoId());
            $arrContent['cursos']      = [];
            $arrContent['turmas']      = [];
            $arrContent['disciplinas'] = [];
            $arrDiscTurmas             = null;

            foreach ($arrIntegracao['campus'] as $campus) {
                foreach ($arrDadosAluno['alunoCurso'] as $arrDadosAlunoCurso) {
                    /** @var \Matricula\Entity\AcadCurso $objCursoAluno */
                    $objCursoAluno = $serviceCurso->getRepository()->find($arrDadosAlunoCurso['cursoId']);

                    if ($objCursoAluno->getMod()->getModNome() == $serviceModalidade::MODALIDADE_DESCRICAO_PRESENCIAL) {
                        continue;
                    }

                    $campusIgual   = ($campus['campId'] == $arrDadosAlunoCurso['campId']);
                    $situacaoAtiva = ($arrDadosAlunoCurso['cursocampus']['cursoSituacao'] == $serviceCurso::CURSO_SITUACAO_ATIVO);

                    if ($campusIgual && $situacaoAtiva) {
                        $arrDiscTurmas = $serviceAlunoCurso
                            ->retornaArrTurmaDisciplinaPoralunocursoId($arrDadosAlunoCurso['alunocursoId'], true);

                        foreach ($arrDiscTurmas as $arrDiscTurma) {
                            $integracaoId = $objSisIntegracao->getIntegracaoId();
                            /** @var \Matricula\Entity\AcadperiodoTurma $objTurma */
                            $objTurma        = $arrDiscTurma['turma'];
                            $situacaoRegular = in_array(
                                $arrDiscTurma['situacaoId'],
                                $serviceAcadgeralSituacao->situacoesAtividade()
                            );

                            $turmaId = $objTurma->getTurmaId();

                            $turmaIdIntegracao = $serviceIntegracaoTurma->pesquisarCodigoIntegracao(
                                $integracaoId,
                                $turmaId
                            );

                            if (!$objTurma->getPer()) {
                                continue;
                            }

                            $dataFim   = $objTurma->getPer()->getPerDataFim();
                            $expiracao = $dataFim ? $dataFim->getTimestamp() : 0;

                            if (!$situacaoRegular) {
                                $expiracao = $ontem;
                            }

                            $arrContent['turmas'][] = [
                                'turmaId'   => $turmaId,
                                'expiracao' => $expiracao,
                            ];

                            if (!$turmaIdIntegracao) {
                                $this->registrarTurmaNaFilaDeIntegracao($objTurma);
                            }

                            foreach ($arrDiscTurma['disciplinas'] as $arrDisc) {
                                $discId                    = $arrDisc['discId'];
                                $situacaoRegularDisciplina = in_array(
                                    $arrDisc['situacaoId'],
                                    $serviceAcadgeralSituacao->situacoesAtividade()
                                );

                                $expiracaoDisciplina = $objTurma->getPer()->getPerDataFim()->getTimestamp();

                                if (!$situacaoRegularDisciplina || !$situacaoRegular) {
                                    $expiracaoDisciplina = $ontem;
                                }

                                $discCurso = $serviceDiscCurso->retornaDiscCursoIdPelaDiscIdTurmaId(
                                    $discId,
                                    $turmaId
                                );

                                if (!$discCurso) {
                                    /** @var \Matricula\Entity\AcadgeralDisciplina $objDisciplina */
                                    $objDisciplina = $serviceAcadgeralDisciplina->getRepository()->find($discId);
                                    $this->setLastError(
                                        'A disciplina ' . $objDisciplina->getDiscNome() . '' .
                                        ' não possui vínculo com o curso ' .
                                        $objTurma->getCursocampus()->getCurso()->getCursoNome()
                                    );

                                    return false;
                                }

                                $discIdIntegracao = $serviceIntegracaoDisciplina->pesquisarCodigoIntegracao(
                                    $integracaoId,
                                    $discCurso
                                );

                                $arrContent['disciplinas'][] = [
                                    'discCursoId' => $discCurso,
                                    'expiracao'   => $expiracaoDisciplina,
                                ];

                                if (!$discIdIntegracao) {
                                    $this->registrarDisciplinaNaFilaDeIntegracao($discCurso);
                                }
                            }
                        }

                        $cursoId                 = $arrDadosAlunoCurso['cursoId'];
                        $alunocursoDataExpiracao = strtotime(
                            self::formatDateAmericano($arrDadosAlunoCurso['alunocursoDataExpiracao'])
                        );

                        /*
                         * Verifica se o curso já foi integrado,
                         * caso negativo registra a solitação
                         * antes de solicitar registro do aluno no curso
                         */
                        $cursoIdIntegracao = $serviceIntegracaoCurso->pesquisarCodigoIntegracao(
                            $objSisIntegracao,
                            $cursoId
                        );

                        if (!$cursoIdIntegracao) {
                            $this->registrarCursoNaFilaDeIntegracao($cursoId);
                        }

                        if ($arrDadosAlunoCurso['alunocursoSituacao'] != $serviceAlunoCurso::ALUNOCURSO_SITUACAO_DEFERIDO) {
                            $alunocursoDataExpiracao = strtotime('-1 day');
                        }

                        $arrContent['cursos'][] = array('cursoId' => $cursoId, 'expiracao' => $alunocursoDataExpiracao);
                    }
                }
            }

            if ($arrContent['cursos']) {
                $arrParam = [
                    'integracao'   => $objSisIntegracao,
                    'filaConteudo' => $arrContent
                ];

                $arrParam = $this->tratarRegistroParaColocarNaFila($arrParam);
                $this->save($arrParam);
            }
        }

        return true;
    }

    private function tratarRegistroParaColocarNaFila($arrParam)
    {
        if (!is_array($arrParam['filaConteudo'])) {
            $arrParam['filaConteudo'] = $this->readConfigFromString($arrParam['filaConteudo']);
        }

        $filaChave = $arrParam['filaConteudo']['method'];

        if (substr_count(strtolower($filaChave), 'aluno') > 0) {
            $filaChave .= '-' . $arrParam['filaConteudo']['alunoId'];
        } elseif (substr_count(strtolower($filaChave), 'curso') > 0) {
            $filaChave .= '-' . $arrParam['filaConteudo']['cursoId'];
        } elseif (substr_count(strtolower($filaChave), 'disciplina') > 0) {
            $filaChave .= '-' . $arrParam['filaConteudo']['discId'];
        } elseif (substr_count(strtolower($filaChave), 'turma') > 0) {
            $filaChave .= '-' . $arrParam['filaConteudo']['turmaId'];
        }

        if (is_object($arrParam['integracao'])) {
            $filaChave .= '-' . $arrParam['integracao']->getIntegracaoId();
        } else {
            $filaChave .= '-' . $arrParam['integracao'];
        }

        $arrParam['filaChave']    = $filaChave;
        $arrParam['filaConteudo'] = $this->writeConfigToString($arrParam['filaConteudo']);
        $arrParam['filaSituacao'] = self::FILA_SITUACAO_PENDENTE;

        if (!$arrParam['filaDataCadastro']) {
            $arrParam['filaDataCadastro'] = new \DateTime();
        }

        if (!$arrParam['filaId']) {
            $arrParam['filaId'] = $this->pesquisaRegistroAtivoPelaChave($filaChave);
        }

        return $arrParam;
    }

    public function registrarTurmaNaFilaDeIntegracao(\Matricula\Entity\AcadperiodoTurma $objAcadperiodoTurma)
    {
        try {
            $serviceIntegracao           = new \Sistema\Service\SisIntegracao($this->getEm());
            $servicePeriodo              = new \Matricula\Service\AcadperiodoLetivo($this->getEm());
            $serviceCampusCurso          = new \Matricula\Service\CampusCurso($this->getEm());
            $serviceDisciplinaCurso      = new \Matricula\Service\AcadgeralDisciplinaCurso($this->getEm());
            $serviceDisciplinaIntegracao = new \Sistema\Service\SisIntegracaoDisciplina($this->getEm());

            /** @var \Matricula\Entity\AcadperiodoLetivo $objAcadperiodo */
            $objAcadperiodo = $servicePeriodo->getRepository()->find($objAcadperiodoTurma->getPer());

            $arrDisciplnaCursoId = [];
            $arrObjSisIntegracao = $serviceIntegracao->buscaIntegracoesAtivas();

            /** @var $objSisIntegracao \Sistema\Entity\SisIntegracao */
            foreach ($arrObjSisIntegracao as $objSisIntegracao) {
                if (!$objAcadperiodoTurma->getMatCur()) {
                    continue;
                }

                $arrDisciplinas = $serviceDisciplinaCurso->retornaDisciplinaCursoPelaMatrizSerie(
                    $objAcadperiodoTurma->getMatCur()->getMatCurId(),
                    $objAcadperiodoTurma->getTurmaSerie(),
                    $objAcadperiodoTurma->getTurmaId()
                );

                /** @var $objcampusCurso \Matricula\Entity\CampusCurso */
                $objcampusCurso = $serviceCampusCurso->getRepository()->find(
                    $objAcadperiodoTurma->getCursocampus()->getCursocampusId()
                );

                $cursoId = $objcampusCurso->getCurso()->getCursoId();

                foreach ($arrDisciplinas as $disciplinaId) {
                    /** @var  $objDisciplinaCurso \Matricula\Entity\AcadgeralDisciplinaCurso */
                    $objDisciplinaCurso = $serviceDisciplinaCurso->getRepository()->findOneBy(
                        ['curso' => $cursoId, 'disc' => $disciplinaId]
                    );

                    if ($objDisciplinaCurso) {
                        $discursoId            = $objDisciplinaCurso->getDiscCursoId();
                        $arrDisciplnaCursoId[] = $discursoId;

                        /** @var $objDisciplinaIntegracao \Sistema\Entity\SisIntegracaoDisciplina */
                        $objDisciplinaIntegracao = $serviceDisciplinaIntegracao->getRepository()->findOneBy(
                            ['cursoDisc' => $discursoId]
                        );

                        if (!$objDisciplinaIntegracao) {
                            $this->registrarDisciplinaNaFilaDeIntegracao($discursoId);
                        }
                    }
                }

                $arrContent = [
                    'method'      => 'integrarTurma',
                    'turmaId'     => $objAcadperiodoTurma->getTurmaId(),
                    'nome'        => preg_replace("/[\n\r\t]/", "", $objAcadperiodoTurma->getTurmaNome()),
                    'serie'       => $objAcadperiodoTurma->getTurmaSerie(),
                    'polo'        => (
                    $objAcadperiodoTurma->getUnidade() ? $objAcadperiodoTurma->getUnidade()->getUnidadeNome() : ''
                    ),
                    'inicio'      => $objAcadperiodo->getPerDataInicio()->format('Y-m-d'),
                    'fim'         => $objAcadperiodo->getPerDataFim()->format('Y-m-d'),
                    'curso'       => $objcampusCurso->getCurso()->getCursoNome(),
                    'disciplinas' => $arrDisciplnaCursoId,
                ];

                $arrParam = [
                    'integracao'   => $objSisIntegracao,
                    'filaConteudo' => $arrContent
                ];

                $arrSisIntegracao = $serviceIntegracao->getArray($objSisIntegracao->getIntegracaoId());

                foreach ($arrSisIntegracao['campus'] as $campus) {
                    if ($campus['campId'] == $objcampusCurso->getCamp()->getCampId()) {
                        $arrParam = $this->tratarRegistroParaColocarNaFila($arrParam);

                        if (!$this->save($arrParam)) {
                            return false;
                        }
                    }
                }
            }

            return true;
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return false;
        }
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceSisIntegracao = new \Sistema\Service\SisIntegracao($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['filaId']) {
                /** @var $objSisFilaIntegracao \Sistema\Entity\SisFilaIntegracao */
                $objSisFilaIntegracao = $this->getRepository()->find($arrDados['filaId']);

                if (!$objSisFilaIntegracao) {
                    $this->setLastError('Registro de fila de integração não existe!');

                    return false;
                }
            } else {
                $objSisFilaIntegracao = new \Sistema\Entity\SisFilaIntegracao();

                $arrDados['filaDataCadastro'] = date('Y-m-d H:i:s');
            }

            if ($arrDados['integracao']) {
                /** @var $objSisIntegracao \Sistema\Entity\SisIntegracao */
                $objSisIntegracao = $serviceSisIntegracao->getRepository()->find($arrDados['integracao']);

                if (!$objSisIntegracao) {
                    $this->setLastError('Registro de integração não existe!');

                    return false;
                }

                $objSisFilaIntegracao->setIntegracao($objSisIntegracao);
            } else {
                $objSisFilaIntegracao->setIntegracao(null);
            }
            $objSisFilaIntegracao->setFilaConteudo($arrDados['filaConteudo']);
            $objSisFilaIntegracao->setFilaRetorno($arrDados['filaRetorno']);
            $objSisFilaIntegracao->setFilaDataCadastro($arrDados['filaDataCadastro']);
            $objSisFilaIntegracao->setFilaDataProcessamento($arrDados['filaDataProcessamento']);
            $objSisFilaIntegracao->setFilaDataFinalizacao($arrDados['filaDataFinalizacao']);
            $objSisFilaIntegracao->setFilaChave($arrDados['filaChave']);
            $objSisFilaIntegracao->setFilaSituacao($arrDados['filaSituacao']);

            $this->getEm()->persist($objSisFilaIntegracao);
            $this->getEm()->flush($objSisFilaIntegracao);

            $this->getEm()->commit();

            $arrDados['filaId'] = $objSisFilaIntegracao->getFilaId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de fila de integração!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['integracao']) {
            $errors[] = 'Por favor preencha o campo "integração"!';
        }

        if (!$arrParam['filaConteudo']) {
            $errors[] = 'Por favor preencha o campo "conteúdo"!';
        }

        if (!$arrParam['filaSituacao']) {
            $errors[] = 'Por favor preencha o campo "situação"!';
        }

        if (!in_array($arrParam['filaSituacao'], self::getFilaSituacao())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "situação"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public function correcaoAutomatica()
    {
        $arrObjSisFilaIntegracao = $this->getRepository()->findBy(
            [
                'filaSituacao' => [
                    self::FILA_SITUACAO_ERRO,
                    //self::FILA_SITUACAO_PROCESSANDO,
                    self::FILA_SITUACAO_PENDENTE
                ]
            ],
            ['filaDataCadastro' => 'asc']
        );

        /** @var $objSisFilaIntegracao \Sistema\Entity\SisFilaIntegracao */
        foreach ($arrObjSisFilaIntegracao as $objSisFilaIntegracao) {
            $arrParam = $this->tratarRegistroParaColocarNaFila($objSisFilaIntegracao->toArray());

            $objSisFilaIntegracao->setFilaSituacao(self::FILA_SITUACAO_PENDENTE);
            $objSisFilaIntegracao->setFilaConteudo($arrParam['filaConteudo']);
            $objSisFilaIntegracao->setFilaChave($arrParam['filaChave']);
            $objSisFilaIntegracao->setFilaRetorno('');
            $this->getEm()->persist($objSisFilaIntegracao);
            $this->getEm()->flush($objSisFilaIntegracao);
        }

        $this->colocarCursosQueAindaNaoForamIntegradosnaFila();

        return ['quantidade' => count($arrObjSisFilaIntegracao)];
    }

    private function writeConfigToString($arrParam)
    {
        $objZendConfig = new \Zend\Config\Writer\Json();

        return $objZendConfig->toString($arrParam);
    }

    private function readConfigFromString($str)
    {
        $objZendConfigJson = new \Zend\Config\Reader\Json();
        $objZendConfigIni  = new \Zend\Config\Reader\Ini();
        $objZendConfigXml  = new \Zend\Config\Reader\Xml();

        try {
            $arrParam = $objZendConfigJson->fromString($str);
        } catch (\Exception $e) {
            try {
                $arrParam = $objZendConfigXml->fromString($str);
            } catch (\Exception $e) {
                $arrParam = $objZendConfigIni->fromString($str);
            }
        }

        return $arrParam;
    }

    private function pesquisaRegistroAtivoPelaChave($chave)
    {
        /** @var $objSisFilaIntegracao \Sistema\Entity\SisFilaIntegracao */
        $objSisFilaIntegracao = $this->getRepository()->findOneBy(
            ['filaChave' => $chave, 'filaSituacao' => [self::FILA_SITUACAO_PENDENTE, self::FILA_SITUACAO_ERRO]]
        );

        if ($objSisFilaIntegracao) {
            return $objSisFilaIntegracao->getFilaId();
        }

        return false;
    }

    public function colocarCursosQueAindaNaoForamIntegradosnaFila()
    {
        $sql   = '
        SELECT curso
        FROM \Matricula\Entity\AcadCurso AS curso
        LEFT JOIN \Sistema\Entity\SisIntegracaoCurso AS integracao WITH integracao.curso=curso.cursoId
        WHERE integracao.curso IS NULL';
        $query = $this->getEm()->createQuery($sql);

        $arrAcadCurso = $query->getResult();

        /** @var \Matricula\Entity\AcadCurso $acadCurso */
        foreach ($arrAcadCurso as $acadCurso) {
            $this->registrarCursoNaFilaDeIntegracao($acadCurso);
        }

        return $arrAcadCurso;
    }

    public function registrarDisciplinaNaFilaDeIntegracao($discCurId)
    {
        $serviceDisciplinaCurso = new \Matricula\Service\AcadgeralDisciplinaCurso($this->getEm());
        $serviceCampusCurso     = new \Matricula\Service\CampusCurso($this->getEm());
        $serviceIntegracao      = new \Sistema\Service\SisIntegracao($this->getEm());

        /** @var $objDisciplinaCurso \Matricula\Entity\AcadgeralDisciplinaCurso */
        $objDisciplinaCurso = $serviceDisciplinaCurso->getRepository()->find($discCurId);

        if (!$objDisciplinaCurso) {
            $this->setLastError('É necessário informar uma disciplina para integrar!');

            return false;
        }

        $arrCampusCurso      = $serviceCampusCurso->getRepository()->findBy(
            ['curso' => $objDisciplinaCurso->getCurso()]
        );
        $arrDadosDisciplina  = $objDisciplinaCurso->getDisc()->toArray();
        $arrObjSisIntegracao = $serviceIntegracao->buscaIntegracoesAtivas();

        /* @var $objSisIntegracao \Sistema\Entity\SisIntegracao */
        foreach ($arrObjSisIntegracao as $objSisIntegracao) {
            $arrContent = [
                'method'    => 'integrarDisciplina',
                'nome'      => preg_replace("/[\n\r\t]/", "", $arrDadosDisciplina['discNome']),
                'descricao' => preg_replace("/[\n\r\t]/", "", $arrDadosDisciplina['discNome']),
                'area'      => '',
                'discId'    => $objDisciplinaCurso->getDiscCursoId()
            ];

            $arrContent['area'] = $objDisciplinaCurso->getCurso()->getCursoNome();

            $arrParam = ['integracao' => $objSisIntegracao, 'filaConteudo' => $arrContent];

            $arrSisIntegracao = $serviceIntegracao->getArray($objSisIntegracao->getIntegracaoId());

            foreach ($arrSisIntegracao['campus'] as $campus) {
                /** @var $objCampusCurso \Matricula\Entity\CampusCurso */
                foreach ($arrCampusCurso as $objCampusCurso) {
                    if ($campus['campId'] == $objCampusCurso->getCamp()->getCampId()) {
                        $arrParam = $this->tratarRegistroParaColocarNaFila($arrParam);

                        if (!$this->save($arrParam)) {
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }

    public function integracaoNotas($codigoAluno, $codigoCurso, $integracao, $periodoCompleto = 'Não')
    {
        $serviceSisIntegracao = new \Sistema\Service\SisIntegracao($this->getEm());
        /** @var \Sistema\Entity\SisIntegracao $objIntegracao */
        $objIntegracao = $serviceSisIntegracao->getRepository()->find($integracao);

        if (!$objIntegracao) {
            $this->setLastError("Código de integração inválda");

            return false;
        }

        if (!$codigoAluno) {
            $this->setLastError("É necessário informar o código do aluno!");

            return false;
        }

        $arrDadosIntegracao = [
            'method'           => 'integracaoNotasUniversa',
            'course_id'        => $codigoCurso,
            'user_id'          => $codigoAluno,
            'periodo_completo' => $periodoCompleto
        ];

        $retorno = $this->integracao($arrDadosIntegracao, $objIntegracao);

        return $retorno;
    }

    public function integracao($arrDadosIntegracao, $objIntegracao)
    {
        if ($objIntegracao && $arrDadosIntegracao) {
            try {
                $clientParams        = $objIntegracao->getIntegracaoCabecalhoEnvio();
                $clientParams        = $this->readConfigFromString($clientParams);
                $clientParams['url'] = $objIntegracao->getIntegracaoUrl();
                $clientParams['url'] .= '/xmlrpc.php';
                $clientParams['integracaoId'] = $objIntegracao->getIntegracaoId();

                $client  = new \Sistema\Plugins\VersaAVA($this->getEm(), $clientParams);
                $retorno = $client->proccessByArray($arrDadosIntegracao);
            } catch (\Zend\XmlRpc\Client\Exception\FaultException $e) {
                $retorno = 'XmlRpc Fault [' . $e->getCode() . ']: ' . $e->getMessage();
            } catch (\Zend\XmlRpc\Client\Exception\HttpException $e) {
                $retorno = 'Erro de HTTP [' . $e->getCode() . ']: ' . $e->getMessage();
            } catch (\Exception $e) {
                $retorno = 'Erro de Desconhecido [' . $e->getCode() . ']: ' . $e->getMessage();
            }

            return $retorno;
        }

        $this->setLastError("Falha ao executar envio!");

        return false;
    }
}
?>