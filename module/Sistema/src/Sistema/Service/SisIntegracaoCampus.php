<?php

namespace Sistema\Service;

use Respect\Validation\Exceptions\ExecutableException;
use VersaSpine\Service\AbstractService;
use Sistema\Service\OrgCampus;
use Doctrine\ORM\EntityManager;

/**
 * Class SisIntegracaoCampus
 * @package Sistema\Service
 */
class SisIntegracaoCampus extends AbstractService
{
    private $_lastError = null;

    /**
     * @return null
     */
    public function getLastError()
    {
        return $this->_lastError;
    }

    /**
     * @param null $lastError
     */
    public function setLastError($lastError)
    {
        $this->_lastError = $lastError;
    }

    public function __construct(EntityManager $em)
    {
        parent::__construct($em, 'Sistema\Entity\SisIntegracaoCampus');
    }

    protected function valida($dados)
    {
        $erros = [];

        if (!$dados['integracaoId']) {
            $erros[] = "Falta preencher o campo 'integracaoId'";
        }

        if (!$dados['campId']) {
            $erros[] = "Falta preencher o campo 'campId'";
        }

        if (!empty($erros)) {
            $this->setLastError(implode("\n", $erros));

            return false;
        }

        return true;
    }

    protected function pesquisaForJson($criterios)
    {
        // TODO: Implement pesquisaForJson() method.
    }

    /**
     * @param Array                         $arrParam
     * @param \Sistema\Entity\SisIntegracao $objIntegracao
     * @return boolean
     * @throws
     */
    public function  salvarMultiplos($arrParam = array(), &$objIntegracao)
    {
        $arrExcluir = array();
        $arrEditar  = array();

        $arrCampus = $arrParam['campus'];

        if (!is_array($arrCampus)) {
            $arrCampus = explode(',', $arrCampus);
        }

        $arrintegracaoCampusDB = $this->pesquisaIntegracaoCampus($objIntegracao->getIntegracaoId());
        /** @var $objIntegracaoCampusDB \Sistema\Entity\SisIntegracaoCampus */
        foreach ($arrintegracaoCampusDB as $objIntegracaoCampusDB) {
            $encontrado = false;
            $campusId   = $objIntegracaoCampusDB->getCampus()->getCampId();

            foreach ($arrCampus as $campus) {
                if ($campus == $campusId) {
                    $encontrado           = true;
                    $arrEditar[$campusId] = $objIntegracaoCampusDB;
                    break;
                }
            }

            if (!$encontrado) {
                $arrExcluir[$campusId] = $objIntegracaoCampusDB;
            }
        }

        if ($arrExcluir) {
            foreach ($arrExcluir as $objReg) {
                $this->getEm()->remove($objReg);
                $this->getEm()->flush();
            }
        }

        foreach ($arrCampus as $campus) {
            $serviceOrgCampus = new \Organizacao\Service\OrgCampus($this->getEm());
            /** @var \Organizacao\Entity\OrgCampus $objOrgCampus */
            $objOrgCampus = $serviceOrgCampus->getRepository()->find($campus);

            if (!isset($arrEditar[$campus])) {
                $objSisIntegracaoCampus = new \Sistema\Entity\SisIntegracaoCampus();

                $objSisIntegracaoCampus->setCampus($objOrgCampus);
                $objSisIntegracaoCampus->setIntegracao($objIntegracao);

                $this->getEm()->persist($objSisIntegracaoCampus);
                $this->getEm()->flush($objSisIntegracaoCampus);
            }
        }

        return true;
    }

    public function save($arrDados = array())
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceCampus     = new \Organizacao\Service\OrgCampus($this->getEm());
        $serviceIntegracao = new \Sistema\Service\SisIntegracao($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['campus']) {
                /** @var \Organizacao\Entity\orgCampus $objIntegracao */
                $objCampus = $serviceCampus->getRepository()->find($arrDados['campus']);

                if (!$objCampus) {
                    $this->setLastError('Campus não encontrado!');

                    return false;
                }
            }

            if ($arrDados['integracao']) {
                /** @var \Sistema\Entity\SisIntegracao $objIntegracao */
                $objIntegracao = $serviceIntegracao->getRepository()->find($arrDados['integracao']);

                if (!$objIntegracao) {
                    $this->setLastError('Integração não encontrado!');

                    return false;
                }
            }

            $objIntegracaoCampus = new \Sistema\Entity\SisIntegracaoCampus();

            $objIntegracaoCampus->setCampus($objCampus);
            $objIntegracaoCampus->setIntegracao($objIntegracao);

            $this->getEm()->persist($objIntegracaoCampus);
            $this->getEm()->flush($objIntegracaoCampus);
            $this->getEm()->commit();

            return true;
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());
        }

        return false;
    }

    public function pesquisaIntegracaoCampus($integracaoId, $toArray = false)
    {
        $arrCampus = array();
        /** @var \Sistema\Entity\SisIntegracaoCampus $objIntegracaoCampus */
        $arrObjIntegracaoCampus = $this->getRepository()->findBy(
            ['integracao' => $integracaoId]
        );

        if (!$toArray) {
            return $arrObjIntegracaoCampus;
        }

        if ($arrObjIntegracaoCampus) {
            foreach ($arrObjIntegracaoCampus as $objIntegracaoCampus) {
                $arrCampus[$objIntegracaoCampus->getCampus()->getCampId()] = $objIntegracaoCampus->toArray();
            }
        }

        return array_values($arrCampus);
    }

    public function retornaIntegracaoCampusExistente($campId = false, $integracaoId = false)
    {
        $arrParam = [];

        if ($campId) {
            $arrParam['campus'] = $campId;
        }

        if ($integracaoId) {
            $arrParam['integracao'] = $integracaoId;
        }

        if (!$arrParam) {
            return null;
        }

        $objIntegracaoCampus = $this->getRepository()->findBy($arrParam);

        return $objIntegracaoCampus;
    }

    public function getArray($id)
    {
        $integracaoCampusId = $id;
        $integracaoId       = $id;
        $campId             = $id;

        $arrDados = array();

        $obj = $this->getRepository()->findOneBy(
            [
                'integracaocampusId' => $integracaoCampusId
            ]
        );

        if (!$obj) {
            $obj = $this->getRepository()->findBy(
                [
                    'integracao' => $integracaoId
                ]
            );
        } elseif (!$obj) {
            $obj = $this->getRepository()->findBy(
                [
                    'campus' => $campId
                ]
            );
        } else {
            return false;
        }

        /** @var \Sistema\Entity\SisIntegracaoCampus $objIntegracaoCampus */
        foreach ($obj as $objIntegracaoCampus) {
            $arrDados[] = $objIntegracaoCampus->toArray();
        }

        return $arrDados;
    }

    public function remove($arrDados)
    {
        $objIntegracaoCampus = $this->retornaIntegracaoCampusExistente(
            $arrDados['campId'],
            $arrDados['integracaoId']
        );

        if (!$objIntegracaoCampus) {
            return false;
        } else {
            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objIntegracaoCampus);
            $this->getEm()->flush($objIntegracaoCampus);
            $this->getEm()->commit();

            return true;
        }
    }

    /**
     * @param $integracao
     * @return bool
     */
    public function removeIntegracaoCampusPelaIntegracao($integracao)
    {
        try {
            $arrObjIntegracaoCampus = $this->getRepository()->findBy(['integracao' => $integracao]);

            if ($arrObjIntegracaoCampus) {
                $this->getEm()->beginTransaction();

                /** @var $objIntegracaoCampus \Sistema\Entity\SisIntegracaoCampus */
                foreach ($arrObjIntegracaoCampus as $objIntegracaoCampus) {
                    $this->getEm()->remove($objIntegracaoCampus);
                    $this->getEm()->flush();
                }

                $this->getEm()->commit();
            }
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de campus de integração.');

            return false;
        }

        return true;
    }
}