<?php

namespace Sistema\Controller;

use VersaSpine\Controller\AbstractCoreController;

class SisCampoValorController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request              = $this->getRequest();
        $paramsGet            = $request->getQuery()->toArray();
        $paramsPost           = $request->getPost()->toArray();
        $serviceSisCampoValor = new \Sistema\Service\SisCampoValor($this->getEntityManager());

        $result = $serviceSisCampoValor->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceSisCampoValor = new \Sistema\Service\SisCampoValor($this->getEntityManager());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {

            $serviceSisCampoValor = new \Sistema\Service\SisCampoValor($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceSisCampoValor->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function addAction($campoValor = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados             = array();
        $serviceSisCampoValor = new \Sistema\Service\SisCampoValor($this->getEntityManager());

        if ($campoValor) {
            $arrDados = $serviceSisCampoValor->getArray($campoValor);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceSisCampoValor->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de campo tipo salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceSisCampoValor->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceSisCampoValor->getLastError());
                }
            }
        }

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $request = $this->getRequest();

        $paramsGet = $request->getQuery()->toArray();

        return $this->addAction($paramsGet);
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceSisCampoValor = new \Sistema\Service\SisCampoValor($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();

            if (!$serviceSisCampoValor->remover($dataPost)) {
                $erro          = true;
                $erroDescricao = $serviceSisCampoValor->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}
?>