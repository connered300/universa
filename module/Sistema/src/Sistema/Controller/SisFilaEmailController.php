<?php

namespace Sistema\Controller;

use VersaSpine\Controller\AbstractCoreController;

class SisFilaEmailController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function correcaoAutomaticaAction()
    {
        $serviceSisFilaEmail = new \Sistema\Service\SisFilaEmail(
            $this->getEntityManager(),
            $this->getServiceManager()->get('Config')
        );

        $result = $serviceSisFilaEmail->correcaoAutomatica();

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function executarAction()
    {
        $url = $this->getBasePath();

        try {
            $serviceSisFilaEmail = new \Sistema\Service\SisFilaEmail($this->getEntityManager());
            $result              = $serviceSisFilaEmail->executarEnvio($url);
        } catch (\Exception $ex) {
            $result = ['erro' => $ex->getMessage()];
        }

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function searchForJsonAction()
    {
        $request             = $this->getRequest();
        $paramsGet           = $request->getQuery()->toArray();
        $paramsPost          = $request->getPost()->toArray();
        $serviceSisFilaEmail = new \Sistema\Service\SisFilaEmail($this->getEntityManager());

        $result = $serviceSisFilaEmail->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceSisFilaEmail = new \Sistema\Service\SisFilaEmail($this->getEntityManager());
        $serviceSisFilaEmail->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceSisFilaEmail = new \Sistema\Service\SisFilaEmail($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceSisFilaEmail->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function editAction()
    {
        $filaId = $this->params()->fromRoute("id", 0);

        if (!$filaId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($filaId);
    }

    public function addAction($filaId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados            = array();
        $serviceSisFilaEmail = new \Sistema\Service\SisFilaEmail($this->getEntityManager());

        if ($filaId) {
            $arrDados = $serviceSisFilaEmail->getArray($filaId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceSisFilaEmail->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de fila de e-mail salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceSisFilaEmail->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceSisFilaEmail->getLastError());
                }
            }
        }

        $serviceSisFilaEmail->formataDadosPost($arrDados);
        $serviceSisFilaEmail->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceSisFilaEmail = new \Sistema\Service\SisFilaEmail($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceSisFilaEmail->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceSisFilaEmail->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}
?>