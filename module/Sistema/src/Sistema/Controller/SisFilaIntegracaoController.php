<?php

namespace Sistema\Controller;

use VersaSpine\Controller\AbstractCoreController;

class SisFilaIntegracaoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function executarAction()
    {
        $serviceSisFilaIntegracao = new \Sistema\Service\SisFilaIntegracao(
            $this->getEntityManager(),
            $this->getServiceManager()->get('Config')
        );

        $result = $serviceSisFilaIntegracao->executarEnvio();

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function correcaoAutomaticaAction()
    {
        $serviceSisFilaIntegracao = new \Sistema\Service\SisFilaIntegracao($this->getEntityManager());

        $result = $serviceSisFilaIntegracao->correcaoAutomatica();

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function searchForJsonAction()
    {
        $request                  = $this->getRequest();
        $paramsGet                = $request->getQuery()->toArray();
        $paramsPost               = $request->getPost()->toArray();
        $serviceSisFilaIntegracao = new \Sistema\Service\SisFilaIntegracao($this->getEntityManager());

        $result = $serviceSisFilaIntegracao->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceSisFilaIntegracao = new \Sistema\Service\SisFilaIntegracao($this->getEntityManager());
        $serviceSisFilaIntegracao->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceSisFilaIntegracao = new \Sistema\Service\SisFilaIntegracao($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceSisFilaIntegracao->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function editAction()
    {
        $filaId = $this->params()->fromRoute("id", 0);

        if (!$filaId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($filaId);
    }

    public function addAction($filaId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados                 = array();
        $serviceSisFilaIntegracao = new \Sistema\Service\SisFilaIntegracao($this->getEntityManager());

        if ($filaId) {
            $arrDados = $serviceSisFilaIntegracao->getArray($filaId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceSisFilaIntegracao->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de fila de integração salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceSisFilaIntegracao->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceSisFilaIntegracao->getLastError());
                }
            }
        }

        $serviceSisFilaIntegracao->formataDadosPost($arrDados);
        $serviceSisFilaIntegracao->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceSisFilaIntegracao = new \Sistema\Service\SisFilaIntegracao($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceSisFilaIntegracao->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceSisFilaIntegracao->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}
?>