<?php

namespace Sistema\Controller;

use VersaSpine\Controller\AbstractCoreController;

class SisCampoEntidadeController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request                 = $this->getRequest();
        $paramsGet               = $request->getQuery()->toArray();
        $paramsPost              = $request->getPost()->toArray();
        $serviceSisCampoEntidade = new \Sistema\Service\SisCampoEntidade($this->getEntityManager());

        $result = $serviceSisCampoEntidade->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceSisCampoEntidade = new \Sistema\Service\SisCampoEntidade($this->getEntityManager());

        $arrTabelas = $serviceSisCampoEntidade->buscaRegistrosInternos(['filter' => 'tables']);

        $this->getView()->setVariable('arrTabelas', $arrTabelas);

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {

            $serviceSisCampoEntidade = new \Sistema\Service\SisCampoEntidade($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceSisCampoEntidade->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function addAction($entidadeId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados                = array();
        $serviceSisCampoEntidade = new \Sistema\Service\SisCampoEntidade($this->getEntityManager());

        if ($entidadeId) {
            $arrDados = $serviceSisCampoEntidade->getArray($entidadeId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceSisCampoEntidade->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de campo entidade salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceSisCampoEntidade->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceSisCampoEntidade->getLastError());
                }
            }
        }

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $entidadeId = $this->params()->fromRoute("id", 0);

        if (!$entidadeId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($entidadeId);
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceSisCampoEntidade = new \Sistema\Service\SisCampoEntidade($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();

            if (!$serviceSisCampoEntidade->remover($dataPost)) {
                $erro          = true;
                $erroDescricao = $serviceSisCampoEntidade->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }

    public function buscaRegistrosInternosAction()
    {

        $serviceSisCampoEntity = new \Sistema\Service\SisCampoEntidade($this->getEntityManager());
        $request               = $this->getRequest();
        $paramsGet             = $request->getQuery()->toArray();
        $paramsPost            = $request->getPost()->toArray();

        $retorno = $serviceSisCampoEntity->buscaRegistrosInternos(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($retorno ? $retorno : array());
    }

}
?>

