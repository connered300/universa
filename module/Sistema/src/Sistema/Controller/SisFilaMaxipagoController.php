<?php

namespace Sistema\Controller;

use VersaSpine\Controller\AbstractCoreController;

class SisFilaMaxipagoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request                = $this->getRequest();
        $paramsGet              = $request->getQuery()->toArray();
        $paramsPost             = $request->getPost()->toArray();
        $serviceSisFilaMaxipago = new \Sistema\Service\SisFilaMaxipago($this->getEntityManager());

        $result = $serviceSisFilaMaxipago->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceSisFilaMaxipago = new \Sistema\Service\SisFilaMaxipago($this->getEntityManager());
        $serviceSisFilaMaxipago->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceSisFilaMaxipago = new \Sistema\Service\SisFilaMaxipago($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceSisFilaMaxipago->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function addAction($filaId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados               = array();
        $serviceSisFilaMaxipago = new \Sistema\Service\SisFilaMaxipago($this->getEntityManager());

        if ($filaId) {
            $arrDados = $serviceSisFilaMaxipago->getArray($filaId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceSisFilaMaxipago->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de fila maxipago salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceSisFilaMaxipago->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceSisFilaMaxipago->getLastError());
                }
            }
        }

        $serviceSisFilaMaxipago->formataDadosPost($arrDados);
        $serviceSisFilaMaxipago->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $filaId = $this->params()->fromRoute("id", 0);

        if (!$filaId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($filaId);
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceSisFilaMaxipago = new \Sistema\Service\SisFilaMaxipago($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceSisFilaMaxipago->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceSisFilaMaxipago->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }

    public function executarAction()
    {
        $urlHome = $this->getBasePath();
        $urlHome .= $this->url()->fromRoute(
            'financeiro/default',
            array('controller' => 'financeiro-pagamento', 'action' => 'pagamento-cartao-recorrencia')
        );

        try {
            $serviceSisFilaMaxipago = new \Sistema\Service\SisFilaMaxipago($this->getEntityManager());
            $result                 = $serviceSisFilaMaxipago->executarEnvio($urlHome);
        } catch (\Exception $ex) {
            $result = ['erro' => $ex->getMessage()];
        }

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }
}
?>