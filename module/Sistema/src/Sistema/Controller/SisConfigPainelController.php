<?php

namespace Sistema\Controller;

use VersaSpine\Controller\AbstractCoreController;

class SisConfigPainelController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function indexAction()
    {
        $serviceSisConfig = new \Sistema\Service\SisConfig(
            $this->getEntityManager(),
            $this->getServiceManager()->get('Config')
        );

        $view = $this->getView();
        $view->setVariable("arrCampos", $serviceSisConfig->getArrConfigForm());

        return $view;
    }

    public function editAction()
    {
        $request = $this->getRequest();

        //vendo se é uma requisição ajax
        if ($request->isXmlHttpRequest()) {
            $json      = $this->getJson();
            $dadosPost = $request->getPost()->toArray();

            $serviceSisConfig = new \Sistema\Service\SisConfig(
                $this->getEntityManager(),
                $this->getServiceManager()->get('Config')
            );

            $arrError = array();
            $success  = false;

            foreach ($dadosPost as $chave => $valor) {
                $arrDados = array(
                    'chave' => $chave,
                    'valor' => $valor,
                );

                $serviceSisConfig->save($arrDados);

                if ($lastError = $serviceSisConfig->getLastError()) {
                    $arrError[] = $lastError;
                }

                $success = true;
            }

            $json->setVariables(
                [
                    "erro"    => ['mensagem' => $arrError],
                    "success" => $success,
                ]
            );

            return $json;
        }

        return $this->getView();
    }

}