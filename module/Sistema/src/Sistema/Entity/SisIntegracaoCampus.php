<?php

namespace Sistema\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;
use Zend\Stdlib\Hydrator\ClassMethods;

/**
 * SisIntegracaoCampus
 *
 * @ORM\Table(name="sis__integracao_campus")
 * @ORM\Entity
 * @Jarvis\Jarvis(title="mXn integração e campus curso",icon="fa fa-table")
 */
class SisIntegracaoCampus
{
    /**
     * @var integer
     *
     * @ORM\Column(name="integracao_campus_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="integracao_campus_id")
     * @LG\Labels\Attributes(text="código integração com o campus")
     * @LG\Querys\Conditions(type="=")
     */
    private $integracaocampusId;

    /**
     * @var \Sistema\Entity\SisIntegracao
     *
     * @ORM\ManyToOne(targetEntity="Sistema\Entity\SisIntegracao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="integracao_id", referencedColumnName="integracao_id", onDelete="CASCADE")
     * })
     */
    private $integracao;

    /**
     * @var \Organizacao\Entity\OrgCampus
     *
     * @ORM\ManyToOne(targetEntity="Organizacao\Entity\OrgCampus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="camp_id", referencedColumnName="camp_id", onDelete="CASCADE")
     * })
     */
    private $campus;

    /**
     * @return int
     */
    public function getIntegracaocampusId()
    {
        return $this->integracaocampusId;
    }

    /**
     * @param int $integracaocampusId
     * @return SisIntegracaoCampus
     */
    public function setIntegracaocampusId($integracaocampusId)
    {
        $this->integracaocampusId = $integracaocampusId;

        return $this;
    }

    /**
     * @return SisIntegracao
     */
    public function getIntegracao()
    {
        return $this->integracao;
    }

    /**
     * @param SisIntegracao $integracao
     * @return SisIntegracaoCampus
     */
    public function setIntegracao($integracao)
    {
        $this->integracao = $integracao;

        return $this;
    }

    /**
     * @return \Organizacao\Entity\OrgCampus
     */
    public function getCampus()
    {
        return $this->campus;
    }

    /**
     * @param \Organizacao\Entity\OrgCampus $campus
     * @return SisIntegracaoCampus
     */
    public function setCampus($campus)
    {
        $this->campus = $campus;

        return $this;
    }

    public function __construct($data = array())
    {
        (new ClassMethods())->hydrate($data, $this);
    }

    public function toArray()
    {
        return array(
            'campId'              => $this->getCampus()->getCampId(),
            'campNome'            => $this->getCampus()->getCampNome(),
            'integracaoId'        => $this->getIntegracao()->getIntegracaoId(),
            'integracaoDescricao' => $this->getIntegracao()->getIntegracaoDescricao(),
            'id'                  => $this->getCampus()->getCampId(),
            'text'                => $this->getCampus()->getCampNome(),
        );
    }
}