<?php

namespace Sistema\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * SisIntegracaoCurso
 *
 * @ORM\Table(name="sis__integracao_curso")
 * @ORM\Entity
 * @LG\LG(id="cursoId",label="Codigo")
 * @Jarvis\Jarvis(title="Listagem de integração curso",icon="fa fa-table")
 */
class SisIntegracaoCurso
{
    /**
     * @var \Sistema\Entity\SisIntegracao
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="Sistema\Entity\SisIntegracao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="integracao_id", referencedColumnName="integracao_id")
     * })
     */
    private $integracao;
    /**
     * @var \Matricula\Entity\AcadCurso
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="curso_id", referencedColumnName="curso_id")
     * })
     */
    private $curso;
    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="codigo")
     * @LG\Labels\Attributes(text="codigo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $codigo;

    /**
     * @return \Sistema\Entity\SisIntegracao
     */
    public function getIntegracao()
    {
        return $this->integracao;
    }

    /**
     * @param \Sistema\Entity\SisIntegracao $integracao
     * @return SisIntegracaoCurso
     */
    public function setIntegracao($integracao)
    {
        $this->integracao = $integracao;

        return $this;
    }

    /**
     * @return \Matricula\Entity\AcadCurso
     */
    public function getCurso()
    {
        return $this->curso;
    }

    /**
     * @param \Matricula\Entity\AcadCurso $curso
     * @return SisIntegracaoCurso
     */
    public function setCurso($curso)
    {
        $this->curso = $curso;

        return $this;
    }

    /**
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * @param string $codigo
     * @return SisIntegracaoCurso
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'integracao' => $this->getIntegracao(),
            'curso'      => $this->getCurso(),
            'codigo'     => $this->getCodigo(),
        );

        $array['integracao'] = $this->getIntegracao() ? $this->getIntegracao()->getIntegracaoId() : null;
        $array['curso']      = $this->getCurso() ? $this->getCurso()->getCursoId() : null;

        return $array;
    }
}
