<?php

namespace Sistema\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;
use Zend\Stdlib\Hydrator\ClassMethods;

/**
 * SisIntegracaoDisciplina
 *
 * @ORM\Table(name="sis__integracao_disciplina")
 * @ORM\Entity
 * @Jarvis\Jarvis(title="mXn integração disciplina",icon="fa fa-table")
 */
class SisIntegracaoDisciplina
{
    /**
     * @var \Matricula\Entity\AcadgeralDisciplinaCurso
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadgeralDisciplinaCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="curso_disc_id", referencedColumnName="disc_curso_id", onDelete="CASCADE")
     * })
     */
    private $cursoDisc;

    /**
     * @var \Sistema\Entity\SisIntegracao
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Sistema\Entity\SisIntegracao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="integracao_id", referencedColumnName="integracao_id", onDelete="CASCADE")
     * })
     */
    private $integracao;

    /**
     * @var string
     * @ORM\Column(name="codigo", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="codigo")
     * @LG\Labels\Attributes(text="codigo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $codigo;

    /**
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * @param string $codigo
     * @return SisIntegracaoDisciplina
     */

    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * @return \Matricula\Entity\AcadgeralDisciplinaCurso $cursoDisc
     */
    public function getCursoDisc()
    {
        return $this->cursoDisc;
    }

    /**
     * @param \Matricula\Entity\AcadgeralDisciplinaCurso $cursoDisc
     * @return SisIntegracaoDisciplina
     */
    public function setCursoDisc($cursoDisc)
    {
        $this->cursoDisc = $cursoDisc;

        return $this;
    }

    /**
     * @return \Sistema\Entity\SisIntegracao
     */
    public function getIntegracao()
    {
        return $this->integracao;
    }

    /**
     * @param \Sistema\Entity\SisIntegracao $integracao
     * @return SisIntegracaoDisciplina
     */
    public function setIntegracao($integracao)
    {
        $this->integracao = $integracao;

        return $this;
    }

    public function __construct($data = array())
    {
        (new ClassMethods())->hydrate($data, $this);
    }

    public function toArray()
    {
        return array(
            'cursoDisc'  => $this->getCursoDisc(),
            'integracao' => $this->getIntegracao(),
            'codigo'     => $this->getCodigo(),
        );
    }
}