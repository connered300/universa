<?php

namespace Sistema\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * SisFilaSms
 *
 * @ORM\Table(name="sis__fila_sms")
 * @ORM\Entity
 * @LG\LG(id="filaId",label="FilaDestinatario")
 * @Jarvis\Jarvis(title="Listagem de fila de SMS",icon="fa fa-table")
 */
class SisFilaSms
{
    /**
     * @var integer
     *
     * @ORM\Column(name="fila_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="fila_id")
     * @LG\Labels\Attributes(text="código fila")
     * @LG\Querys\Conditions(type="=")
     */
    private $filaId;

    /**
     * @var string
     *
     * @ORM\Column(name="fila_destinatario", type="string", nullable=false, length=45)
     * @LG\Labels\Property(name="fila_destinatario")
     * @LG\Labels\Attributes(text="destinatário")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $filaDestinatario;

    /**
     * @var string
     *
     * @ORM\Column(name="fila_provedor", type="string", nullable=true, length=7)
     * @LG\Labels\Property(name="fila_provedor")
     * @LG\Labels\Attributes(text="provedor")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $filaProvedor;

    /**
     * @var string
     *
     * @ORM\Column(name="fila_conteudo", type="text", nullable=false)
     * @LG\Labels\Property(name="fila_conteudo")
     * @LG\Labels\Attributes(text="conteúdo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $filaConteudo;

    /**
     * @var string
     *
     * @ORM\Column(name="fila_retorno", type="text", nullable=true)
     * @LG\Labels\Property(name="fila_retorno")
     * @LG\Labels\Attributes(text="retorno")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $filaRetorno;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="fila_data_cadastro", type="datetime", nullable=false)
     * @LG\Labels\Property(name="fila_data_cadastro")
     * @LG\Labels\Attributes(text="data de cadastro")
     * @LG\Querys\Conditions(type="=")
     */
    private $filaDataCadastro;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="fila_data_processamento", type="datetime", nullable=true)
     * @LG\Labels\Property(name="fila_data_processamento")
     * @LG\Labels\Attributes(text="data de processamento")
     * @LG\Querys\Conditions(type="=")
     */
    private $filaDataProcessamento;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="fila_data_finalizacao", type="datetime", nullable=true)
     * @LG\Labels\Property(name="fila_data_finalizacao")
     * @LG\Labels\Attributes(text="data de finalização")
     * @LG\Querys\Conditions(type="=")
     */
    private $filaDataFinalizacao;

    /**
     * @var \Organizacao\Entity\OrgComunicacaoTipo
     * @ORM\ManyToOne(targetEntity="Organizacao\Entity\OrgComunicacaoTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipo_id", referencedColumnName="tipo_id")
     * })
     */
    private $tipo;

    /**
     * @var string
     *
     * @ORM\Column(name="fila_situacao", type="string", nullable=false, length=11)
     * @LG\Labels\Property(name="fila_situacao")
     * @LG\Labels\Attributes(text="situação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $filaSituacao;

    /**
     * @return integer
     */
    public function getFilaId()
    {
        return $this->filaId;
    }

    /**
     * @param integer $filaId
     * @return SisFilaSms
     */
    public function setFilaId($filaId)
    {
        $this->filaId = $filaId;

        return $this;
    }

    /**
     * @return string
     */
    public function getFilaDestinatario()
    {
        return $this->filaDestinatario;
    }

    /**
     * @param string $filaDestinatario
     * @return SisFilaSms
     */
    public function setFilaDestinatario($filaDestinatario)
    {
        $this->filaDestinatario = $filaDestinatario;

        return $this;
    }

    /**
     * @return string
     */
    public function getFilaProvedor()
    {
        return $this->filaProvedor;
    }

    /**
     * @param string $filaProvedor
     * @return SisFilaSms
     */
    public function setFilaProvedor($filaProvedor)
    {
        $this->filaProvedor = $filaProvedor;

        return $this;
    }

    /**
     * @return string
     */
    public function getFilaConteudo()
    {
        return $this->filaConteudo;
    }

    /**
     * @param string $filaConteudo
     * @return SisFilaSms
     */
    public function setFilaConteudo($filaConteudo)
    {
        $filaConteudo = strip_tags($filaConteudo);
        $filaConteudo = preg_replace("/[\t ]+/", ' ', $filaConteudo);
        $filaConteudo = preg_replace("/\n+/", "\n", $filaConteudo);
        $filaConteudo = trim($filaConteudo);

        $this->filaConteudo = strip_tags($filaConteudo);

        return $this;
    }

    /**
     * @return string
     */
    public function getFilaRetorno()
    {
        return $this->filaRetorno;
    }

    /**
     * @param string $filaRetorno
     * @return SisFilaSms
     */
    public function setFilaRetorno($filaRetorno)
    {
        if (is_array($filaRetorno) || is_object($filaRetorno)) {
            $filaRetorno = json_encode($filaRetorno);
        }

        $this->filaRetorno = $filaRetorno;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getFilaDataCadastro($format = false)
    {
        $filaDataCadastro = $this->filaDataCadastro;

        if ($format && $filaDataCadastro) {
            $filaDataCadastro = $filaDataCadastro->format('Y-m-d H:i:s');
        }

        return $filaDataCadastro;
    }

    /**
     * @param \Datetime $filaDataCadastro
     * @return SisFilaSms
     */
    public function setFilaDataCadastro($filaDataCadastro)
    {
        if ($filaDataCadastro) {
            if (is_string($filaDataCadastro)) {
                $filaDataCadastro = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $filaDataCadastro
                );
                $filaDataCadastro = new \Datetime($filaDataCadastro);
            }
        } else {
            $filaDataCadastro = null;
        }

        $this->filaDataCadastro = $filaDataCadastro;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getFilaDataProcessamento($format = false)
    {
        $filaDataProcessamento = $this->filaDataProcessamento;

        if ($format && $filaDataProcessamento) {
            $filaDataProcessamento = $filaDataProcessamento->format('Y-m-d H:i:s');
        }

        return $filaDataProcessamento;
    }

    /**
     * @param \Datetime $filaDataProcessamento
     * @return SisFilaSms
     */
    public function setFilaDataProcessamento($filaDataProcessamento)
    {
        if ($filaDataProcessamento) {
            if (is_string($filaDataProcessamento)) {
                $filaDataProcessamento = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $filaDataProcessamento
                );
                $filaDataProcessamento = new \Datetime($filaDataProcessamento);
            }
        } else {
            $filaDataProcessamento = null;
        }

        $this->filaDataProcessamento = $filaDataProcessamento;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getFilaDataFinalizacao($format = false)
    {
        $filaDataFinalizacao = $this->filaDataFinalizacao;

        if ($format && $filaDataFinalizacao) {
            $filaDataFinalizacao = $filaDataFinalizacao->format('Y-m-d H:i:s');
        }

        return $filaDataFinalizacao;
    }

    /**
     * @param \Datetime $filaDataFinalizacao
     * @return SisFilaSms
     */
    public function setFilaDataFinalizacao($filaDataFinalizacao)
    {
        if ($filaDataFinalizacao) {
            if (is_string($filaDataFinalizacao)) {
                $filaDataFinalizacao = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $filaDataFinalizacao
                );
                $filaDataFinalizacao = new \Datetime($filaDataFinalizacao);
            }
        } else {
            $filaDataFinalizacao = null;
        }

        $this->filaDataFinalizacao = $filaDataFinalizacao;

        return $this;
    }

    /**
     * @return \Organizacao\Entity\OrgComunicacaoTipo
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param \Organizacao\Entity\OrgComunicacaoTipo $tipo
     * @return SisFilaSms
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * @return string
     */
    public function getFilaSituacao()
    {
        return $this->filaSituacao;
    }

    /**
     * @param string $filaSituacao
     * @return SisFilaSms
     */
    public function setFilaSituacao($filaSituacao)
    {
        $this->filaSituacao = $filaSituacao;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'filaId'                => $this->getFilaId(),
            'tipo'                  => $this->getTipo(),
            'filaDestinatario'      => $this->getFilaDestinatario(),
            'filaProvedor'          => $this->getFilaProvedor(),
            'filaConteudo'          => $this->getFilaConteudo(),
            'filaRetorno'           => $this->getFilaRetorno(),
            'filaDataCadastro'      => $this->getFilaDataCadastro(true),
            'filaDataProcessamento' => $this->getFilaDataProcessamento(true),
            'filaDataFinalizacao'   => $this->getFilaDataFinalizacao(true),
            'filaSituacao'          => $this->getFilaSituacao(),
        );

        $array['tipoId'] = $this->getTipo() ? $this->getTipo()->getTipoId() : null;

        return $array;
    }
}
