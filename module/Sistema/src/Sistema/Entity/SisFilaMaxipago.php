<?php

namespace Sistema\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * SisFilaMaxipago
 *
 * @ORM\Table(name="sis__fila_maxipago")
 * @ORM\Entity
 * @LG\LG(id="filaId",label="TituloId")
 * @Jarvis\Jarvis(title="Listagem de fila maxipago",icon="fa fa-table")
 */
class SisFilaMaxipago
{
    /**
     * @var integer
     *
     * @ORM\Column(name="fila_id", type="integer", nullable=false, length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="fila_id")
     * @LG\Labels\Attributes(text="código fila")
     * @LG\Querys\Conditions(type="=")
     */
    private $filaId;

    /**
     * @var \Financeiro\Entity\FinanceiroTitulo
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\FinanceiroTitulo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="titulo_id", referencedColumnName="titulo_id")
     * })
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="fila_conteudo", type="text", nullable=false)
     * @LG\Labels\Property(name="fila_conteudo")
     * @LG\Labels\Attributes(text="conteúdo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $filaConteudo;

    /**
     * @var string
     *
     * @ORM\Column(name="fila_retorno", type="text", nullable=true)
     * @LG\Labels\Property(name="fila_retorno")
     * @LG\Labels\Attributes(text="retorno")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $filaRetorno;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="fila_data_cadastro", type="datetime", nullable=false)
     * @LG\Labels\Property(name="fila_data_cadastro")
     * @LG\Labels\Attributes(text="cadastro data")
     * @LG\Querys\Conditions(type="=")
     */
    private $filaDataCadastro;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="fila_data_processamento", type="datetime", nullable=true)
     * @LG\Labels\Property(name="fila_data_processamento")
     * @LG\Labels\Attributes(text="processamento data")
     * @LG\Querys\Conditions(type="=")
     */
    private $filaDataProcessamento;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="fila_data_finalizacao", type="datetime", nullable=true)
     * @LG\Labels\Property(name="fila_data_finalizacao")
     * @LG\Labels\Attributes(text="finalização data")
     * @LG\Querys\Conditions(type="=")
     */
    private $filaDataFinalizacao;

    /**
     * @var string
     *
     * @ORM\Column(name="fila_situacao", type="string", nullable=true, length=11)
     * @LG\Labels\Property(name="fila_situacao")
     * @LG\Labels\Attributes(text="situação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $filaSituacao;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="fila_agendamento", type="datetime", nullable=false)
     * @LG\Labels\Property(name="fila_agendamento")
     * @LG\Labels\Attributes(text="cadastro data")
     * @LG\Querys\Conditions(type="=")
     */
    private $filaAgendamento;

    /**
     * @var \Financeiro\Entity\FinanceiroCartao
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\FinanceiroCartao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cartao_id", referencedColumnName="cartao_id")
     * })
     */
    private $cartaoId;
    /**
     * @var \Sistema\Entity\SisFilaMaxipago
     * @ORM\ManyToOne(targetEntity="Sistema\Entity\SisFilaMaxipago")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fila_recorrencia_id", referencedColumnName="fila_id")
     * })
     */
    private $filaRecorrenciaId;

    /**
     * @return integer
     */
    public function getFilaId()
    {
        return $this->filaId;
    }

    /**
     * @param integer $filaId
     * @return SisFilaMaxipago
     */
    public function setFilaId($filaId)
    {
        $this->filaId = $filaId;

        return $this;
    }

    /**
     * @return \Financeiro\Entity\FinanceiroTitulo
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroTitulo $titulo
     * @return SisFilaMaxipago
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * @return string|array
     */
    public function getFilaConteudo($decode = false)
    {
        if ($decode) {
            return json_decode($this->filaConteudo,true);
        }

        return $this->filaConteudo;
    }

    /**
     * @param string $filaConteudo
     * @return SisFilaMaxipago
     */
    public function setFilaConteudo($filaConteudo)
    {
        $this->filaConteudo = $filaConteudo;

        return $this;
    }

    /**
     * @return string
     */
    public function getFilaRetorno()
    {
        return $this->filaRetorno;
    }

    /**
     * @param string $filaRetorno
     * @return SisFilaMaxipago
     */
    public function setFilaRetorno($filaRetorno)
    {
        $this->filaRetorno = $filaRetorno;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getFilaDataCadastro($format = false)
    {
        $filaDataCadastro = $this->filaDataCadastro;

        if ($format && $filaDataCadastro) {
            $filaDataCadastro = $filaDataCadastro->format('d/m/Y H:i:s');
        }

        return $filaDataCadastro;
    }

    /**
     * @param \Datetime $filaDataCadastro
     * @return SisFilaMaxipago
     */
    public function setFilaDataCadastro($filaDataCadastro)
    {
        if (is_string($filaDataCadastro)) {
            $filaDataCadastro = \VersaSpine\Service\AbstractService::formatDateAmericano($filaDataCadastro);
            $filaDataCadastro = new \Datetime($filaDataCadastro);
        } elseif (!is_object($filaDataCadastro)) {
            $filaDataCadastro = null;
        }

        $this->filaDataCadastro = $filaDataCadastro;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getFilaDataProcessamento($format = false)
    {
        $filaDataProcessamento = $this->filaDataProcessamento;

        if ($format && $filaDataProcessamento) {
            $filaDataProcessamento = $filaDataProcessamento->format('d/m/Y H:i:s');
        }

        return $filaDataProcessamento;
    }

    /**
     * @param \Datetime $filaDataProcessamento
     * @return SisFilaMaxipago
     */
    public function setFilaDataProcessamento($filaDataProcessamento)
    {
        if (is_string($filaDataProcessamento)) {
            $filaDataProcessamento = \VersaSpine\Service\AbstractService::formatDateAmericano($filaDataProcessamento);
            $filaDataProcessamento = new \Datetime($filaDataProcessamento);
        } elseif (!is_object($filaDataProcessamento)) {
            $filaDataProcessamento = null;
        }

        $this->filaDataProcessamento = $filaDataProcessamento;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getFilaDataFinalizacao($format = false)
    {
        $filaDataFinalizacao = $this->filaDataFinalizacao;

        if ($format && $filaDataFinalizacao) {
            $filaDataFinalizacao = $filaDataFinalizacao->format('d/m/Y H:i:s');
        }

        return $filaDataFinalizacao;
    }

    /**
     * @param \Datetime $filaDataFinalizacao
     * @return SisFilaMaxipago
     */
    public function setFilaDataFinalizacao($filaDataFinalizacao)
    {
        if (is_string($filaDataFinalizacao)) {
            $filaDataFinalizacao = \VersaSpine\Service\AbstractService::formatDateAmericano($filaDataFinalizacao);
            $filaDataFinalizacao = new \Datetime($filaDataFinalizacao);
        } elseif (!is_object($filaDataFinalizacao)) {
            $filaDataFinalizacao = null;
        }

        $this->filaDataFinalizacao = $filaDataFinalizacao;

        return $this;
    }

    /**
     * @return string
     */
    public function getFilaSituacao()
    {
        return $this->filaSituacao;
    }

    /**
     * @param string $filaSituacao
     * @return SisFilaMaxipago
     */
    public function setFilaSituacao($filaSituacao)
    {
        $this->filaSituacao = $filaSituacao;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getFilaAgendamento($format = false)
    {
        $filaAgendamento = $this->filaAgendamento;

        if ($format && $filaAgendamento) {
            $filaAgendamento = $filaAgendamento->format('d/m/Y');
        }

        return $filaAgendamento;
    }

    /**
     * @param \Datetime $filaDataCadastro
     * @return SisFilaMaxipago
     */
    public function setFilaAgendamento($filaAgendamento)
    {
        if (is_string($filaAgendamento)) {
            $filaAgendamento = \VersaSpine\Service\AbstractService::formatDateAmericano($filaAgendamento);
            $filaAgendamento = new \Datetime($filaAgendamento);
        } elseif (!is_object($filaAgendamento)) {
            $filaAgendamento = null;
        }

        $this->filaAgendamento = $filaAgendamento;

        return $this;
    }

    /**
     * @return \Financeiro\Entity\FinanceiroCartao
     */
    public function getCartao()
    {
        return $this->cartaoId;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroCartao $cartaoId
     * @return $this
     */
    public function setCartao($cartaoId)
    {
        $this->cartaoId = $cartaoId;

        return $this;
    }

    /**
     * @return SisFilaMaxipago
     */
    public function getFilaRecorrencia()
    {
        return $this->filaRecorrenciaId;
    }

    /**
     * @param SisFilaMaxipago $cartaoId
     * @return $this
     */
    public function setFilaRecorrencia($filaRecorrenciaId)
    {
        $this->filaRecorrenciaId = $filaRecorrenciaId;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'filaId'                => $this->getFilaId(),
            'titulo'                => $this->getTitulo(),
            'filaConteudo'          => $this->getFilaConteudo(),
            'filaRetorno'           => $this->getFilaRetorno(),
            'filaDataCadastro'      => $this->getFilaDataCadastro(true),
            'filaDataProcessamento' => $this->getFilaDataProcessamento(true),
            'filaDataFinalizacao'   => $this->getFilaDataFinalizacao(true),
            'filaSituacao'          => $this->getFilaSituacao(),
            'filaDataAgendamento'   => $this->getFilaAgendamento(true),
            'cartao'                => $this->getCartao(),
            'filaRecorrencia'       => $this->getFilaRecorrencia()
        );

        $array['titulo'] = $this->getTitulo() ? $this->getTitulo()->getTituloId() : null;

        return $array;
    }
}
