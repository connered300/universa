<?php

namespace Sistema\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * SisCampoEntidade
 *
 * @ORM\Table(name="sis__campo_entidade")
 * @ORM\Entity
 * @LG\LG(id="entidadeId",label="EntidadeDescricao")
 * @Jarvis\Jarvis(title="Listagem de campo entidade",icon="fa fa-table")
 */
class SisCampoEntidade
{
    /**
     * @var string
     *
     * @ORM\Column(name="entidade_id", type="string", nullable=false, length=255)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @LG\Labels\Property(name="entidade_id")
     * @LG\Labels\Attributes(text="código entidade")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $entidadeId;

    /**
     * @var string
     *
     * @ORM\Column(name="entidade_descricao", type="string", nullable=false, length=255)
     * @LG\Labels\Property(name="entidade_descricao")
     * @LG\Labels\Attributes(text="descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $entidadeDescricao;

    /**
     * @var string
     *
     * @ORM\Column(name="entidade_tabela", type="string", nullable=false, length=255)
     * @LG\Labels\Property(name="entidade_tabela")
     * @LG\Labels\Attributes(text="tabela")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $entidadeTabela;

    /**
     * @var string
     *
     * @ORM\Column(name="entidade_chave", type="string", nullable=false, length=255)
     * @LG\Labels\Property(name="entidade_chave")
     * @LG\Labels\Attributes(text="chave")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $entidadeChave;

    /**
     * @return string
     */
    public function getEntidadeId()
    {
        return $this->entidadeId;
    }

    /**
     * @param string $entidadeId
     * @return SisCampoEntidade
     */
    public function setEntidadeId($entidadeId)
    {
        $this->entidadeId = $entidadeId;

        return $this;
    }

    /**
     * @return string
     */
    public function getEntidadeDescricao()
    {
        return $this->entidadeDescricao;
    }

    /**
     * @param string $entidadeDescricao
     * @return SisCampoEntidade
     */
    public function setEntidadeDescricao($entidadeDescricao)
    {
        $this->entidadeDescricao = $entidadeDescricao;

        return $this;
    }

    /**
     * @return string
     */
    public function getEntidadeTabela()
    {
        return $this->entidadeTabela;
    }

    /**
     * @param string $entidadeTabela
     * @return SisCampoEntidade
     */
    public function setEntidadeTabela($entidadeTabela)
    {
        $this->entidadeTabela = $entidadeTabela;

        return $this;
    }

    /**
     * @return string
     */
    public function getEntidadeChave()
    {
        return $this->entidadeChave;
    }

    /**
     * @param string $entidadeChave
     * @return SisCampoEntidade
     */
    public function setEntidadeChave($entidadeChave)
    {
        $this->entidadeChave = $entidadeChave;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'entidadeId'        => $this->getEntidadeId(),
            'entidadeDescricao' => $this->getEntidadeDescricao(),
            'entidadeTabela'    => $this->getEntidadeTabela(),
            'entidadeChave'     => $this->getEntidadeChave(),
        );

        return $array;
    }
}
