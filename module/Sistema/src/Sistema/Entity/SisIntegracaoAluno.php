<?php

namespace Sistema\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * SisIntegracaoAluno
 *
 * @ORM\Table(name="sis__integracao_aluno")
 * @ORM\Entity
 * @LG\LG(id="alunoId",label="Codigo")
 * @Jarvis\Jarvis(title="Listagem de integração aluno",icon="fa fa-table")
 */
class SisIntegracaoAluno
{
    /**
     * @var \Sistema\Entity\SisIntegracao
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="Sistema\Entity\SisIntegracao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="integracao_id", referencedColumnName="integracao_id")
     * })
     */
    private $integracao;
    /**
     * @var \Matricula\Entity\AcadgeralAluno
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadgeralAluno")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="aluno_id", referencedColumnName="aluno_id")
     * })
     */
    private $aluno;
    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="codigo")
     * @LG\Labels\Attributes(text="codigo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $codigo;

    /**
     * @return \Sistema\Entity\SisIntegracao
     */
    public function getIntegracao()
    {
        return $this->integracao;
    }

    /**
     * @param \Sistema\Entity\SisIntegracao $integracao
     * @return SisIntegracaoAluno
     */
    public function setIntegracao($integracao)
    {
        $this->integracao = $integracao;

        return $this;
    }

    /**
     * @return \Matricula\Entity\AcadgeralAluno
     */
    public function getAluno()
    {
        return $this->aluno;
    }

    /**
     * @param \Matricula\Entity\AcadgeralAluno $aluno
     * @return SisIntegracaoAluno
     */
    public function setAluno($aluno)
    {
        $this->aluno = $aluno;

        return $this;
    }

    /**
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * @param string $codigo
     * @return SisIntegracaoAluno
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'integracao' => $this->getIntegracao(),
            'aluno'      => $this->getAluno(),
            'codigo'     => $this->getCodigo(),
        );

        $array['integracao'] = $this->getIntegracao() ? $this->getIntegracao()->getIntegracaoId() : null;
        $array['aluno']      = $this->getAluno() ? $this->getAluno()->getAlunoId() : null;

        return $array;
    }
}
