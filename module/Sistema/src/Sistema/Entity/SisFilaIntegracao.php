<?php

namespace Sistema\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * SisFilaIntegracao
 *
 * @ORM\Table(name="sis__fila_integracao")
 * @ORM\Entity
 * @LG\LG(id="filaId",label="IntegracaoId")
 * @Jarvis\Jarvis(title="Listagem de fila de integração",icon="fa fa-table")
 */
class SisFilaIntegracao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="fila_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="fila_id")
     * @LG\Labels\Attributes(text="código fila")
     * @LG\Querys\Conditions(type="=")
     */
    private $filaId;
    /**
     * @var \Sistema\Entity\SisIntegracao
     * @ORM\ManyToOne(targetEntity="Sistema\Entity\SisIntegracao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="integracao_id", referencedColumnName="integracao_id")
     * })
     */
    private $integracao;
    /**
     * @var string
     *
     * @ORM\Column(name="fila_conteudo", type="text", nullable=false)
     * @LG\Labels\Property(name="fila_conteudo")
     * @LG\Labels\Attributes(text="conteúdo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $filaConteudo;
    /**
     * @var string
     *
     * @ORM\Column(name="fila_retorno", type="text", nullable=true)
     * @LG\Labels\Property(name="fila_retorno")
     * @LG\Labels\Attributes(text="retorno")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $filaRetorno;
    /**
     * @var \Datetime
     *
     * @ORM\Column(name="fila_data_cadastro", type="datetime", nullable=false)
     * @LG\Labels\Property(name="fila_data_cadastro")
     * @LG\Labels\Attributes(text="data de cadastro")
     * @LG\Querys\Conditions(type="=")
     */
    private $filaDataCadastro;
    /**
     * @var \Datetime
     *
     * @ORM\Column(name="fila_data_processamento", type="datetime", nullable=true)
     * @LG\Labels\Property(name="fila_data_processamento")
     * @LG\Labels\Attributes(text="data de processamento")
     * @LG\Querys\Conditions(type="=")
     */
    private $filaDataProcessamento;
    /**
     * @var \Datetime
     *
     * @ORM\Column(name="fila_data_finalizacao", type="datetime", nullable=true)
     * @LG\Labels\Property(name="fila_data_finalizacao")
     * @LG\Labels\Attributes(text="data de finalização")
     * @LG\Querys\Conditions(type="=")
     */
    private $filaDataFinalizacao;
    /**
     * @var string
     *
     * @ORM\Column(name="fila_chave", type="string", nullable=true, length=32)
     * @LG\Labels\Property(name="fila_chave")
     * @LG\Labels\Attributes(text="chave")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $filaChave;
    /**
     * @var string
     *
     * @ORM\Column(name="fila_situacao", type="string", nullable=false, length=11)
     * @LG\Labels\Property(name="fila_situacao")
     * @LG\Labels\Attributes(text="situação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $filaSituacao;

    /**
     * @return integer
     */
    public function getFilaId()
    {
        return $this->filaId;
    }

    /**
     * @param integer $filaId
     * @return SisFilaIntegracao
     */
    public function setFilaId($filaId)
    {
        $this->filaId = $filaId;

        return $this;
    }

    /**
     * @return \Sistema\Entity\SisIntegracao
     */
    public function getIntegracao()
    {
        return $this->integracao;
    }

    /**
     * @param \Sistema\Entity\SisIntegracao $integracao
     * @return SisFilaIntegracao
     */
    public function setIntegracao($integracao)
    {
        $this->integracao = $integracao;

        return $this;
    }

    /**
     * @return string
     */
    public function getFilaConteudo()
    {
        return $this->filaConteudo;
    }

    /**
     * @param string $filaConteudo
     * @return SisFilaIntegracao
     */
    public function setFilaConteudo($filaConteudo)
    {
        $this->filaConteudo = $filaConteudo;

        return $this;
    }

    /**
     * @return string
     */
    public function getFilaRetorno()
    {
        return $this->filaRetorno;
    }

    /**
     * @param string $filaRetorno
     * @return SisFilaIntegracao
     */
    public function setFilaRetorno($filaRetorno)
    {
        if (is_array($filaRetorno) || is_object($filaRetorno)) {
            $filaRetorno = json_encode($filaRetorno);
        }

        $this->filaRetorno = $filaRetorno;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getFilaDataCadastro($format = false)
    {
        $filaDataCadastro = $this->filaDataCadastro;

        if ($format && $filaDataCadastro) {
            $filaDataCadastro = $filaDataCadastro->format('d/m/Y H:i:s');
        }

        return $filaDataCadastro;
    }

    /**
     * @param \Datetime $filaDataCadastro
     * @return SisFilaIntegracao
     */
    public function setFilaDataCadastro($filaDataCadastro)
    {
        if ($filaDataCadastro) {
            if (is_string($filaDataCadastro)) {
                $filaDataCadastro = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $filaDataCadastro
                );
                $filaDataCadastro = new \Datetime($filaDataCadastro);
            }
        } else {
            $filaDataCadastro = null;
        }

        $this->filaDataCadastro = $filaDataCadastro;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getFilaDataProcessamento($format = false)
    {
        $filaDataProcessamento = $this->filaDataProcessamento;

        if ($format && $filaDataProcessamento) {
            $filaDataProcessamento = $filaDataProcessamento->format('d/m/Y H:i:s');
        }

        return $filaDataProcessamento;
    }

    /**
     * @param \Datetime $filaDataProcessamento
     * @return SisFilaIntegracao
     */
    public function setFilaDataProcessamento($filaDataProcessamento)
    {
        if ($filaDataProcessamento) {
            if (is_string($filaDataProcessamento)) {
                $filaDataProcessamento = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $filaDataProcessamento
                );
                $filaDataProcessamento = new \Datetime($filaDataProcessamento);
            }
        } else {
            $filaDataProcessamento = null;
        }

        $this->filaDataProcessamento = $filaDataProcessamento;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getFilaDataFinalizacao($format = false)
    {
        $filaDataFinalizacao = $this->filaDataFinalizacao;

        if ($format && $filaDataFinalizacao) {
            $filaDataFinalizacao = $filaDataFinalizacao->format('d/m/Y H:i:s');
        }

        return $filaDataFinalizacao;
    }

    /**
     * @param \Datetime $filaDataFinalizacao
     * @return SisFilaIntegracao
     */
    public function setFilaDataFinalizacao($filaDataFinalizacao)
    {
        if ($filaDataFinalizacao) {
            if (is_string($filaDataFinalizacao)) {
                $filaDataFinalizacao = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $filaDataFinalizacao
                );
                $filaDataFinalizacao = new \Datetime($filaDataFinalizacao);
            }
        } else {
            $filaDataFinalizacao = null;
        }

        $this->filaDataFinalizacao = $filaDataFinalizacao;

        return $this;
    }

    /**
     * @return string
     */
    public function getFilaChave()
    {
        return $this->filaChave;
    }

    /**
     * @param string $filaChave
     * @return SisFilaIntegracao
     */
    public function setFilaChave($filaChave)
    {
        $this->filaChave = $filaChave;

        return $this;
    }

    /**
     * @return string
     */
    public function getFilaSituacao()
    {
        return $this->filaSituacao;
    }

    /**
     * @param string $filaSituacao
     * @return SisFilaIntegracao
     */
    public function setFilaSituacao($filaSituacao)
    {
        $this->filaSituacao = $filaSituacao;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'filaId'                => $this->getFilaId(),
            'integracao'            => $this->getIntegracao(),
            'filaConteudo'          => $this->getFilaConteudo(),
            'filaRetorno'           => $this->getFilaRetorno(),
            'filaDataCadastro'      => $this->getFilaDataCadastro(true),
            'filaDataProcessamento' => $this->getFilaDataProcessamento(true),
            'filaDataFinalizacao'   => $this->getFilaDataFinalizacao(true),
            'filaChave'             => $this->getFilaChave(),
            'filaSituacao'          => $this->getFilaSituacao(),
        );

        $array['integracao'] = $this->getIntegracao() ? $this->getIntegracao()->getIntegracaoId() : null;

        return $array;
    }
}
