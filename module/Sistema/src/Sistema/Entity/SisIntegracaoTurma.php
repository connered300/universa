<?php

namespace Sistema\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * SisIntegracaoTurma
 *
 * @ORM\Table(name="sis__integracao_turma")
 * @ORM\Entity
 * @LG\LG(id="cursoId",label="Codigo")
 * @Jarvis\Jarvis(title="Listagem de integração turma",icon="fa fa-table")
 */
class SisIntegracaoTurma
{
    /**
     * @var \Sistema\Entity\SisIntegracao
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="Sistema\Entity\SisIntegracao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="integracao_id", referencedColumnName="integracao_id")
     * })
     */
    private $integracao;
    /**
     * @var \Matricula\Entity\AcadperiodoTurma
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoTurma")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="turma_id", referencedColumnName="turma_id")
     * })
     */
    private $turma;
    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="codigo")
     * @LG\Labels\Attributes(text="codigo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $codigo;

    /**
     * @return SisIntegracao
     */
    public function getIntegracao()
    {
        return $this->integracao;
    }

    /**
     * @param SisIntegracao $integracao
     * @return SisIntegracaoTurma
     */
    public function setIntegracao($integracao)
    {
        $this->integracao = $integracao;

        return $this;
    }

    /**
     * @return \Matricula\Entity\AcadperiodoTurma
     */
    public function getTurma()
    {
        return $this->turma;
    }

    /**
     * @param \Matricula\Entity\AcadperiodoTurma $turma
     * @return SisIntegracaoTurma
     */
    public function setTurma($turma)
    {
        $this->turma = $turma;

        return $this;
    }

    /**
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * @param string $codigo
     * @return SisIntegracaoTurma
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'integracao' => $this->getIntegracao(),
            'turma'      => $this->getTurma(),
            'codigo'     => $this->getCodigo(),
        );

        $array['integracao'] = $this->getIntegracao() ? $this->getIntegracao()->getIntegracaoId() : null;
        $array['turma']      = $this->getTurma() ? $this->getTurma()->getTurmaId() : null;

        return $array;
    }
}