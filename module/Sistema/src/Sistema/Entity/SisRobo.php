<?php

namespace Sistema\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * SisRobo
 *
 * @ORM\Table(name="sis__robo")
 * @ORM\Entity
 * @LG\LG(id="roboId",label="RoboNome")
 * @Jarvis\Jarvis(title="Listagem de robô",icon="fa fa-table")
 */
class SisRobo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="robo_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="robo_id")
     * @LG\Labels\Attributes(text="código robô")
     * @LG\Querys\Conditions(type="=")
     */
    private $roboId;
    /**
     * @var string
     *
     * @ORM\Column(name="robo_nome", type="string", nullable=false, length=45)
     * @LG\Labels\Property(name="robo_nome")
     * @LG\Labels\Attributes(text="nome")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $roboNome;
    /**
     * @var string
     *
     * @ORM\Column(name="robo_url", type="text", nullable=false)
     * @LG\Labels\Property(name="robo_url")
     * @LG\Labels\Attributes(text="URL")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $roboUrl;
    /**
     * @var integer
     *
     * @ORM\Column(name="robo_tempo_execucao", type="integer", nullable=false, length=11)
     * @LG\Labels\Property(name="robo_tempo_execucao")
     * @LG\Labels\Attributes(text="execução tempo")
     * @LG\Querys\Conditions(type="=")
     */
    private $roboTempoExecucao;
    /**
     * @var \Datetime
     *
     * @ORM\Column(name="robo_ultima_execucao", type="datetime", nullable=true)
     * @LG\Labels\Property(name="robo_ultima_execucao")
     * @LG\Labels\Attributes(text="execução última")
     * @LG\Querys\Conditions(type="=")
     */
    private $roboUltimaExecucao;
    /**
     * @var string
     *
     * @ORM\Column(name="robo_situacao", type="string", nullable=false, length=11)
     * @LG\Labels\Property(name="robo_situacao")
     * @LG\Labels\Attributes(text="situação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $roboSituacao;

    /**
     * @return integer
     */
    public function getRoboId()
    {
        return $this->roboId;
    }

    /**
     * @param integer $roboId
     * @return SisRobo
     */
    public function setRoboId($roboId)
    {
        $this->roboId = $roboId;

        return $this;
    }

    /**
     * @return string
     */
    public function getRoboNome()
    {
        return $this->roboNome;
    }

    /**
     * @param string $roboNome
     * @return SisRobo
     */
    public function setRoboNome($roboNome)
    {
        $this->roboNome = $roboNome;

        return $this;
    }

    /**
     * @return string
     */
    public function getRoboUrl()
    {
        return $this->roboUrl;
    }

    /**
     * @param string $roboUrl
     * @return SisRobo
     */
    public function setRoboUrl($roboUrl)
    {
        $this->roboUrl = $roboUrl;

        return $this;
    }

    /**
     * @return integer
     */
    public function getRoboTempoExecucao()
    {
        return $this->roboTempoExecucao;
    }

    /**
     * @param integer $roboTempoExecucao
     * @return SisRobo
     */
    public function setRoboTempoExecucao($roboTempoExecucao)
    {
        $this->roboTempoExecucao = $roboTempoExecucao;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getRoboUltimaExecucao($format = false)
    {
        $roboUltimaExecucao = $this->roboUltimaExecucao;

        if ($format && $roboUltimaExecucao) {
            $roboUltimaExecucao = $roboUltimaExecucao->format('d/m/Y H:i:s');
        }

        return $roboUltimaExecucao;
    }

    /**
     * @param \Datetime $roboUltimaExecucao
     * @return SisRobo
     */
    public function setRoboUltimaExecucao($roboUltimaExecucao)
    {
        if ($roboUltimaExecucao) {
            if (is_string($roboUltimaExecucao)) {
                $roboUltimaExecucao = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $roboUltimaExecucao
                );
                $roboUltimaExecucao = new \Datetime($roboUltimaExecucao);
            }
        } else {
            $roboUltimaExecucao = null;
        }

        $this->roboUltimaExecucao = $roboUltimaExecucao;

        return $this;
    }

    /**
     * @return string
     */
    public function getRoboSituacao()
    {
        return $this->roboSituacao;
    }

    /**
     * @param string $roboSituacao
     * @return SisRobo
     */
    public function setRoboSituacao($roboSituacao)
    {
        $this->roboSituacao = $roboSituacao;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'roboId'             => $this->getRoboId(),
            'roboNome'           => $this->getRoboNome(),
            'roboUrl'            => $this->getRoboUrl(),
            'roboTempoExecucao'  => $this->getRoboTempoExecucao(),
            'roboUltimaExecucao' => $this->getRoboUltimaExecucao(true),
            'roboSituacao'       => $this->getRoboSituacao(),
        );

        return $array;
    }
}
