<?php

namespace Sistema\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * SisConfig
 *
 * @ORM\Table(name="sis__config")
 * @ORM\Entity
 * @LG\LG(id="chave",label="Valor")
 * @Jarvis\Jarvis(title="Listagem de configuração",icon="fa fa-table")
 */
class SisConfig
{
    /**
     * @var string
     *
     * @ORM\Column(name="chave", type="string", nullable=false, length=255)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @LG\Labels\Property(name="chave")
     * @LG\Labels\Attributes(text="chave")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $chave;
    /**
     * @var string
     *
     * @ORM\Column(name="valor", type="text", nullable=true)
     * @LG\Labels\Property(name="valor")
     * @LG\Labels\Attributes(text="valor")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $valor;

    /**
     * @return string
     */
    public function getChave()
    {
        return $this->chave;
    }

    /**
     * @param string $chave
     * @return SisConfig
     */
    public function setChave($chave)
    {
        $this->chave = $chave;

        return $this;
    }

    /**
     * @return string
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * @param string $valor
     * @return SisConfig
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'chave'    => $this->getChave(),
            'chaveOld' => $this->getChave(),
            'valor'    => $this->getValor(),
        );

        return $array;
    }
}
