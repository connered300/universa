<?php

namespace Sistema\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;

/**
 * SisCampoValor
 *
 * @ORM\Table(name="sis__campo_valor")
 * @ORM\Entity
 * @LG\LG(id="campo",label="CampoPersonalizado")
 */
class SisCampoValor
{
    /**
     * @var string
     *
     * @ORM\Column(name="campo_chave", type="string", length=255, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $campoChave = '';

    /**
     * @var string
     *
     * @ORM\Column(name="campo_valor", type="text", length=65535, nullable=true)
     */
    private $campoValor;

    /**
     * @var \Sistema\Entity\SisCampoPersonalizado
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="SisCampoPersonalizado")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="campo_id", referencedColumnName="campo_id")
     * })
     */
    private $campo;

    /**
     * @return \Sistema\Entity\SisCampoPersonalizado
     */
    public function getCampo()
    {
        return $this->campo;
    }

    /**
     * @param \Sistema\Entity\SisCampoPersonalizado $tipoCampo
     * @return SisCampoValor
     */
    public function setCampo($campo)
    {
        $this->campo = $campo;

        return $this;
    }

    /**
     * @return string
     */
    public function getCampoValor()
    {
        return $this->campoValor;
    }

    /**
     * @param string $valor
     * @return SisCampoValor
     */
    public function setCampoValor($valor)
    {
        $this->campoValor = $valor;

        return $this;
    }

    /**
     * @return string
     */
    public function getCampoChave()
    {
        return $this->campoChave;
    }

    /**
     * @param string $chave
     * @return SisCampoValor
     */
    public function setCampoChave($chave)
    {
        $this->campoChave = $chave;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'campoId'    => $this->getCampo()->getCampoId(),
            'campoNome'  => $this->getCampo()->getCampoNome(),
            'campoChave' => $this->getCampoChave(),
            'campoValor' => $this->getCampoValor()
        );

        return $array;
    }
}