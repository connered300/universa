<?php

namespace Sistema\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * SisIntegracao
 *
 * @ORM\Table(name="sis__integracao")
 * @ORM\Entity
 * @LG\LG(id="integracaoId",label="IntegracaoDescricao")
 * @Jarvis\Jarvis(title="Listagem de integração",icon="fa fa-table")
 */
class SisIntegracao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="integracao_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="integracao_id")
     * @LG\Labels\Attributes(text="código integração")
     * @LG\Querys\Conditions(type="=")
     */
    private $integracaoId;
    /**
     * @var \Acesso\Entity\AcessoPessoas
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario", referencedColumnName="id")
     * })
     */
    private $usuario;
    /**
     * @var string
     *
     * @ORM\Column(name="integracao_descricao", type="string", nullable=false, length=100)
     * @LG\Labels\Property(name="integracao_descricao")
     * @LG\Labels\Attributes(text="descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $integracaoDescricao;
    /**
     * @var string
     *
     * @ORM\Column(name="integracao_tipo", type="string", nullable=false, length=9)
     * @LG\Labels\Property(name="integracao_tipo")
     * @LG\Labels\Attributes(text="tipo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $integracaoTipo;
    /**
     * @var string
     *
     * @ORM\Column(name="integracao_token", type="string", nullable=false, length=50)
     * @LG\Labels\Property(name="integracao_token")
     * @LG\Labels\Attributes(text="token")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $integracaoToken;
    /**
     * @var string
     *
     * @ORM\Column(name="integracao_url", type="text", nullable=false)
     * @LG\Labels\Property(name="integracao_url")
     * @LG\Labels\Attributes(text="URL")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $integracaoUrl;
    /**
     * @var string
     *
     * @ORM\Column(name="integracao_cabecalho_envio", type="text", nullable=true)
     * @LG\Labels\Property(name="integracao_cabecalho_envio")
     * @LG\Labels\Attributes(text="cabeçalho de envio")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $integracaoCabecalhoEnvio;
    /**
     * @var string
     *
     * @ORM\Column(name="integracao_situacao", type="string", nullable=false, length=7)
     * @LG\Labels\Property(name="integracao_situacao")
     * @LG\Labels\Attributes(text="situação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $integracaoSituacao;

    /**
     * @return integer
     */
    public function getIntegracaoId()
    {
        return $this->integracaoId;
    }

    /**
     * @param integer $integracaoId
     * @return SisIntegracao
     */
    public function setIntegracaoId($integracaoId)
    {
        $this->integracaoId = $integracaoId;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuario
     * @return SisIntegracao
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * @return string
     */
    public function getIntegracaoDescricao()
    {
        return $this->integracaoDescricao;
    }

    /**
     * @param string $integracaoDescricao
     * @return SisIntegracao
     */
    public function setIntegracaoDescricao($integracaoDescricao)
    {
        $this->integracaoDescricao = $integracaoDescricao;

        return $this;
    }

    /**
     * @return string
     */
    public function getIntegracaoTipo()
    {
        return $this->integracaoTipo;
    }

    /**
     * @param string $integracaoTipo
     * @return SisIntegracao
     */
    public function setIntegracaoTipo($integracaoTipo)
    {
        $this->integracaoTipo = $integracaoTipo;

        return $this;
    }

    /**
     * @return string
     */
    public function getIntegracaoToken()
    {
        return $this->integracaoToken;
    }

    /**
     * @param string $integracaoToken
     * @return SisIntegracao
     */
    public function setIntegracaoToken($integracaoToken)
    {
        $this->integracaoToken = $integracaoToken;

        return $this;
    }

    /**
     * @return string
     */
    public function getIntegracaoUrl()
    {
        return $this->integracaoUrl;
    }

    /**
     * @param string $integracaoUrl
     * @return SisIntegracao
     */
    public function setIntegracaoUrl($integracaoUrl)
    {
        $this->integracaoUrl = $integracaoUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getIntegracaoCabecalhoEnvio()
    {
        return $this->integracaoCabecalhoEnvio;
    }

    /**
     * @param string $integracaoCabecalhoEnvio
     * @return SisIntegracao
     */
    public function setIntegracaoCabecalhoEnvio($integracaoCabecalhoEnvio)
    {
        $this->integracaoCabecalhoEnvio = $integracaoCabecalhoEnvio;

        return $this;
    }

    /**
     * @return string
     */
    public function getIntegracaoSituacao()
    {
        return $this->integracaoSituacao;
    }

    /**
     * @param string $integracaoSituacao
     * @return SisIntegracao
     */
    public function setIntegracaoSituacao($integracaoSituacao)
    {
        $this->integracaoSituacao = $integracaoSituacao;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'integracaoId'             => $this->getIntegracaoId(),
            'usuario'                  => $this->getUsuario(),
            'integracaoDescricao'      => $this->getIntegracaoDescricao(),
            'integracaoTipo'           => $this->getIntegracaoTipo(),
            'integracaoToken'          => $this->getIntegracaoToken(),
            'integracaoUrl'            => $this->getIntegracaoUrl(),
            'integracaoCabecalhoEnvio' => $this->getIntegracaoCabecalhoEnvio(),
            'integracaoSituacao'       => $this->getIntegracaoSituacao(),
        );

        $array['usuario'] = $this->getUsuario() ? $this->getUsuario()->getId() : null;

        return $array;
    }
}
