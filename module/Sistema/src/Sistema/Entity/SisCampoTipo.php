<?php

namespace Sistema\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * SisCampoTipo
 *
 * @ORM\Table(name="sis__campo_tipo")
 * @ORM\Entity
 * @LG\LG(id="tipoCampo",label="TipoDescricao")
 * @Jarvis\Jarvis(title="Listagem de campo tipo",icon="fa fa-table")
 */
class SisCampoTipo
{
    /**
     * @var string
     *
     * @ORM\Column(name="tipo_campo", type="string", nullable=false, length=255)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @LG\Labels\Property(name="tipo_campo")
     * @LG\Labels\Attributes(text="campo tipo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $tipoCampo;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo_descricao", type="string", nullable=false, length=255)
     * @LG\Labels\Property(name="tipo_descricao")
     * @LG\Labels\Attributes(text="descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $tipoDescricao;

    /**
     * @return string
     */
    public function getTipoCampo()
    {
        return $this->tipoCampo;
    }

    /**
     * @param string $tipoCampo
     * @return SisCampoTipo
     */
    public function setTipoCampo($tipoCampo)
    {
        $this->tipoCampo = $tipoCampo;

        return $this;
    }

    /**
     * @return string
     */
    public function getTipoDescricao()
    {
        return $this->tipoDescricao;
    }

    /**
     * @param string $tipoDescricao
     * @return SisCampoTipo
     */
    public function setTipoDescricao($tipoDescricao)
    {
        $this->tipoDescricao = $tipoDescricao;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'tipoCampo'     => $this->getTipoCampo(),
            'tipoDescricao' => $this->getTipoDescricao(),
        );

        return $array;
    }
}
