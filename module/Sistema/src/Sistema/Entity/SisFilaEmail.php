<?php

namespace Sistema\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * SisFilaEmail
 *
 * @ORM\Table(name="sis__fila_email")
 * @ORM\Entity
 * @LG\LG(id="filaId",label="FilaDestinatarioNome")
 * @Jarvis\Jarvis(title="Listagem de fila de e-mail",icon="fa fa-table")
 */
class SisFilaEmail
{
    /**
     * @var integer
     *
     * @ORM\Column(name="fila_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="fila_id")
     * @LG\Labels\Attributes(text="código fila")
     * @LG\Querys\Conditions(type="=")
     */
    private $filaId;

    /**
     * @var \Organizacao\Entity\OrgEmailConta
     * @ORM\ManyToOne(targetEntity="Organizacao\Entity\OrgEmailConta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="conta_id", referencedColumnName="conta_id")
     * })
     */
    private $conta;

    /**
     * @var string
     *
     * @ORM\Column(name="fila_assunto", type="string", nullable=true, length=250)
     * @LG\Labels\Property(name="fila_assunto")
     * @LG\Labels\Attributes(text="assunto")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $filaAssunto;

    /**
     * @var string
     *
     * @ORM\Column(name="fila_destinatario_nome", type="string", nullable=true, length=100)
     * @LG\Labels\Property(name="fila_destinatario_nome")
     * @LG\Labels\Attributes(text="nome destinatário")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $filaDestinatarioNome;

    /**
     * @var string
     *
     * @ORM\Column(name="fila_destinatario_email", type="string", nullable=false, length=100)
     * @LG\Labels\Property(name="fila_destinatario_email")
     * @LG\Labels\Attributes(text="destinatário")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $filaDestinatarioEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="fila_conteudo", type="text", nullable=false)
     * @LG\Labels\Property(name="fila_conteudo")
     * @LG\Labels\Attributes(text="conteúdo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $filaConteudo;

    /**
     * @var string
     *
     * @ORM\Column(name="fila_retorno", type="text", nullable=true)
     * @LG\Labels\Property(name="fila_retorno")
     * @LG\Labels\Attributes(text="retorno")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $filaRetorno;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="fila_data_cadastro", type="datetime", nullable=false)
     * @LG\Labels\Property(name="fila_data_cadastro")
     * @LG\Labels\Attributes(text="data de cadastro")
     * @LG\Querys\Conditions(type="=")
     */
    private $filaDataCadastro;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="fila_data_processamento", type="datetime", nullable=true)
     * @LG\Labels\Property(name="fila_data_processamento")
     * @LG\Labels\Attributes(text="data de processamento")
     * @LG\Querys\Conditions(type="=")
     */
    private $filaDataProcessamento;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="fila_data_finalizacao", type="datetime", nullable=true)
     * @LG\Labels\Property(name="fila_data_finalizacao")
     * @LG\Labels\Attributes(text="data de finalização")
     * @LG\Querys\Conditions(type="=")
     */
    private $filaDataFinalizacao;

    /**
     * @var string
     *
     * @ORM\Column(name="fila_situacao", type="string", nullable=false, length=11)
     * @LG\Labels\Property(name="fila_situacao")
     * @LG\Labels\Attributes(text="situação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $filaSituacao;

    /**
     * @var \Organizacao\Entity\OrgComunicacaoTipo
     * @ORM\ManyToOne(targetEntity="Organizacao\Entity\OrgComunicacaoTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipo_id", referencedColumnName="tipo_id")
     * })
     */
    private $tipo;

    /**
     * @return integer
     */
    public function getFilaId()
    {
        return $this->filaId;
    }

    /**
     * @param integer $filaId
     * @return SisFilaEmail
     */
    public function setFilaId($filaId)
    {
        $this->filaId = $filaId;

        return $this;
    }

    /**
     * @return \Organizacao\Entity\OrgEmailConta
     */
    public function getConta()
    {
        return $this->conta;
    }

    /**
     * @param \Organizacao\Entity\OrgEmailConta $conta
     * @return SisFilaEmail
     */
    public function setConta($conta)
    {
        $this->conta = $conta;

        return $this;
    }

    /**
     * @return string
     */
    public function getFilaAssunto()
    {
        return $this->filaAssunto;
    }

    /**
     * @param string $filaAssunto
     * @return SisFilaEmail
     */
    public function setFilaAssunto($filaAssunto)
    {
        $this->filaAssunto = $filaAssunto;

        return $this;
    }

    /**
     * @return string
     */
    public function getFilaDestinatarioNome()
    {
        return $this->filaDestinatarioNome;
    }

    /**
     * @param string $filaDestinatarioNome
     * @return SisFilaEmail
     */
    public function setFilaDestinatarioNome($filaDestinatarioNome)
    {
        $this->filaDestinatarioNome = $filaDestinatarioNome;

        return $this;
    }

    /**
     * @return string
     */
    public function getFilaDestinatarioEmail()
    {
        return $this->filaDestinatarioEmail;
    }

    /**
     * @param string $filaDestinatarioEmail
     * @return SisFilaEmail
     */
    public function setFilaDestinatarioEmail($filaDestinatarioEmail)
    {
        $this->filaDestinatarioEmail = $filaDestinatarioEmail;

        return $this;
    }

    /**
     * @return string
     */
    public function getFilaConteudo()
    {
        return $this->filaConteudo;
    }

    /**
     * @param string $filaConteudo
     * @return SisFilaEmail
     */
    public function setFilaConteudo($filaConteudo)
    {
        $this->filaConteudo = $filaConteudo;

        return $this;
    }

    /**
     * @return string
     */
    public function getFilaRetorno()
    {
        return $this->filaRetorno;
    }

    /**
     * @param string $filaRetorno
     * @return SisFilaEmail
     */
    public function setFilaRetorno($filaRetorno)
    {
        if (is_array($filaRetorno) || is_object($filaRetorno)) {
            $filaRetorno = json_encode($filaRetorno);
        }

        $this->filaRetorno = $filaRetorno;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getFilaDataCadastro($format = false)
    {
        $filaDataCadastro = $this->filaDataCadastro;

        if ($format && $filaDataCadastro) {
            $filaDataCadastro = $filaDataCadastro->format('d/m/Y H:i:s');
        }

        return $filaDataCadastro;
    }

    /**
     * @param \Datetime $filaDataCadastro
     * @return SisFilaEmail
     */
    public function setFilaDataCadastro($filaDataCadastro)
    {
        if ($filaDataCadastro) {
            if (is_string($filaDataCadastro)) {
                $filaDataCadastro = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $filaDataCadastro
                );
                $filaDataCadastro = new \Datetime($filaDataCadastro);
            }
        } else {
            $filaDataCadastro = null;
        }

        $this->filaDataCadastro = $filaDataCadastro;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getFilaDataProcessamento($format = false)
    {
        $filaDataProcessamento = $this->filaDataProcessamento;

        if ($format && $filaDataProcessamento) {
            $filaDataProcessamento = $filaDataProcessamento->format('d/m/Y H:i:s');
        }

        return $filaDataProcessamento;
    }

    /**
     * @param \Datetime $filaDataProcessamento
     * @return SisFilaEmail
     */
    public function setFilaDataProcessamento($filaDataProcessamento)
    {
        if ($filaDataProcessamento) {
            if (is_string($filaDataProcessamento)) {
                $filaDataProcessamento = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $filaDataProcessamento
                );
                $filaDataProcessamento = new \Datetime($filaDataProcessamento);
            }
        } else {
            $filaDataProcessamento = null;
        }

        $this->filaDataProcessamento = $filaDataProcessamento;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getFilaDataFinalizacao($format = false)
    {
        $filaDataFinalizacao = $this->filaDataFinalizacao;

        if ($format && $filaDataFinalizacao) {
            $filaDataFinalizacao = $filaDataFinalizacao->format('d/m/Y H:i:s');
        }

        return $filaDataFinalizacao;
    }

    /**
     * @param \Datetime $filaDataFinalizacao
     * @return SisFilaEmail
     */
    public function setFilaDataFinalizacao($filaDataFinalizacao)
    {
        if ($filaDataFinalizacao) {
            if (is_string($filaDataFinalizacao)) {
                $filaDataFinalizacao = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $filaDataFinalizacao
                );
                $filaDataFinalizacao = new \Datetime($filaDataFinalizacao);
            }
        } else {
            $filaDataFinalizacao = null;
        }

        $this->filaDataFinalizacao = $filaDataFinalizacao;

        return $this;
    }

    /**
     * @return string
     */
    public function getFilaSituacao()
    {
        return $this->filaSituacao;
    }

    /**
     * @param string $filaSituacao
     * @return SisFilaEmail
     */
    public function setFilaSituacao($filaSituacao)
    {
        $this->filaSituacao = $filaSituacao;

        return $this;
    }

    /**
     * @return \Organizacao\Entity\OrgComunicacaoTipo
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param \Organizacao\Entity\OrgComunicacaoTipo $tipo
     * @return SisFilaEmail
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'filaId'                => $this->getFilaId(),
            'conta'                 => $this->getConta(),
            'tipo'                 => $this->getTipo(),
            'filaAssunto'           => $this->getFilaAssunto(),
            'filaDestinatarioNome'  => $this->getFilaDestinatarioNome(),
            'filaDestinatarioEmail' => $this->getFilaDestinatarioEmail(),
            'filaConteudo'          => $this->getFilaConteudo(),
            'filaRetorno'           => $this->getFilaRetorno(),
            'filaDataCadastro'      => $this->getFilaDataCadastro(true),
            'filaDataProcessamento' => $this->getFilaDataProcessamento(true),
            'filaDataFinalizacao'   => $this->getFilaDataFinalizacao(true),
            'filaSituacao'          => $this->getFilaSituacao(),
        );

        $array['tipoId']  = $this->getTipo() ? $this->getTipo()->getTipoId() : null;

        return $array;
    }
}
