<?php

namespace Sistema\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * SisCampoPersonalizado
 *
 * @ORM\Table(name="sis__campo_personalizado")
 * @ORM\Entity
 * @LG\LG(id="campoId",label="CampoDescricao")
 * @Jarvis\Jarvis(title="Listagem de campo personalizado",icon="fa fa-table")
 */
class SisCampoPersonalizado
{
    /**
     * @var integer|string
     *
     * @ORM\Column(name="campo_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="campo_id")
     * @LG\Labels\Attributes(text="código campo")
     * @LG\Querys\Conditions(type="=")
     */
    private $campoId;

    /**
     * @var string
     *
     * @ORM\Column(name="campo_nome", type="string", nullable=false, length=64)
     * @LG\Labels\Property(name="campo_nome")
     * @LG\Labels\Attributes(text="nome")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $campoNome;

    /**
     * @var string
     *
     * @ORM\Column(name="campo_descricao", type="string", nullable=true, length=255)
     * @LG\Labels\Property(name="campo_descricao")
     * @LG\Labels\Attributes(text="descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $campoDescricao;

    /**
     * @var integer
     *
     * @ORM\Column(name="campo_tamanho_min", type="integer", nullable=true, length=2)
     * @LG\Labels\Property(name="campo_tamanho_min")
     * @LG\Labels\Attributes(text="mínimo tamanho")
     * @LG\Querys\Conditions(type="=")
     */
    private $campoTamanhoMin;

    /**
     * @var integer
     *
     * @ORM\Column(name="campo_tamanho_max", type="integer", nullable=true, length=10)
     * @LG\Labels\Property(name="campo_tamanho_max")
     * @LG\Labels\Attributes(text="máximo tamanho")
     * @LG\Querys\Conditions(type="=")
     */
    private $campoTamanhoMax;

    /**
     * @var string
     *
     * @ORM\Column(name="campo_expressao_regular", type="string", nullable=true, length=255)
     * @LG\Labels\Property(name="campo_expressao_regular")
     * @LG\Labels\Attributes(text="regular expressão")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $campoExpressaoRegular;

    /**
     * @var string
     *
     * @ORM\Column(name="campo_valor_padrao", type="text", nullable=true)
     * @LG\Labels\Property(name="campo_valor_padrao")
     * @LG\Labels\Attributes(text="padrão valor")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $campoValorPadrao;

    /**
     * @var string
     *
     * @ORM\Column(name="campo_obrigatorio", type="string", nullable=true, length=3)
     * @LG\Labels\Property(name="campo_obrigatorio")
     * @LG\Labels\Attributes(text="obrigatorio")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $campoObrigatorio;

    /**
     * @var string
     *
     * @ORM\Column(name="campo_visivel_painel", type="string", nullable=true, length=3)
     * @LG\Labels\Property(name="campo_visivel_painel")
     * @LG\Labels\Attributes(text="painel visível")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $campoVisivelPainel;

    /**
     * @var string
     *
     * @ORM\Column(name="campo_exibicao_externa", type="string", nullable=true, length=3)
     * @LG\Labels\Property(name="campo_exibicao_externa")
     * @LG\Labels\Attributes(text="externa exibição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $campoExibicaoExterna;

    /**
     * @var string
     *
     * @ORM\Column(name="campo_ativo", type="string", nullable=true, length=3)
     * @LG\Labels\Property(name="campo_ativo")
     * @LG\Labels\Attributes(text="ativo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $campoAtivo;

    /**
     * @var string
     *
     * @ORM\Column(name="campo_multiplo", type="string", nullable=true, length=3)
     * @LG\Labels\Property(name="campo_multiplo")
     * @LG\Labels\Attributes(text="multiplo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $campoMultiplo;

    /**
     * @var string
     *
     * @ORM\Column(name="campo_valores", type="text", nullable=true)
     * @LG\Labels\Property(name="campo_valores")
     * @LG\Labels\Attributes(text="valores")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $campoValores;

    /**
     * @var \Sistema\Entity\SisCampoTipo
     * @ORM\ManyToOne(targetEntity="Sistema\Entity\SisCampoTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipo_campo", referencedColumnName="tipo_campo")
     * })
     */
    private $tipoCampo;

    /**
     * @var \Sistema\Entity\SisCampoEntidade
     * @ORM\ManyToOne(targetEntity="Sistema\Entity\SisCampoEntidade")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="entidade_id", referencedColumnName="entidade_id")
     * })
     */
    private $entidadeId;

    /**
     * @return integer
     */
    public function getCampoId()
    {
        return $this->campoId;
    }

    /**
     * @param integer $campoId
     * @return SisCampoPersonalizado
     */
    public function setCampoId($campoId)
    {
        $this->campoId = $campoId;

        return $this;
    }

    /**
     * @return string
     */
    public function getCampoNome()
    {
        return $this->campoNome;
    }

    /**
     * @param string $campoNome
     * @return SisCampoPersonalizado
     */
    public function setCampoNome($campoNome)
    {
        $this->campoNome = $campoNome;

        return $this;
    }

    /**
     * @return string
     */
    public function getCampoDescricao()
    {
        return $this->campoDescricao;
    }

    /**
     * @param string $campoDescricao
     * @return SisCampoPersonalizado
     */
    public function setCampoDescricao($campoDescricao)
    {
        $this->campoDescricao = $campoDescricao;

        return $this;
    }

    /**
     * @return integer
     */
    public function getCampoTamanhoMin()
    {
        return $this->campoTamanhoMin;
    }

    /**
     * @param integer $campoTamanhoMin
     * @return SisCampoPersonalizado
     */
    public function setCampoTamanhoMin($campoTamanhoMin)
    {
        $this->campoTamanhoMin = $campoTamanhoMin;

        return $this;
    }

    /**
     * @return integer
     */
    public function getCampoTamanhoMax()
    {
        return $this->campoTamanhoMax;
    }

    /**
     * @param integer $campoTamanhoMax
     * @return SisCampoPersonalizado
     */
    public function setCampoTamanhoMax($campoTamanhoMax)
    {
        $this->campoTamanhoMax = $campoTamanhoMax;

        return $this;
    }

    /**
     * @return string
     */
    public function getCampoExpressaoRegular()
    {
        return $this->campoExpressaoRegular;
    }

    /**
     * @param string $campoExpressaoRegular
     * @return SisCampoPersonalizado
     */
    public function setCampoExpressaoRegular($campoExpressaoRegular)
    {
        $this->campoExpressaoRegular = $campoExpressaoRegular;

        return $this;
    }

    /**
     * @return string
     */
    public function getCampoValorPadrao()
    {
        return $this->campoValorPadrao;
    }

    /**
     * @param string $campoValorPadrao
     * @return SisCampoPersonalizado
     */
    public function setCampoValorPadrao($campoValorPadrao)
    {
        $this->campoValorPadrao = $campoValorPadrao;

        return $this;
    }

    /**
     * @return string
     */
    public function getCampoObrigatorio()
    {
        return $this->campoObrigatorio;
    }

    /**
     * @param string $campoObrigatorio
     * @return SisCampoPersonalizado
     */
    public function setCampoObrigatorio($campoObrigatorio)
    {
        $this->campoObrigatorio = $campoObrigatorio;

        return $this;
    }

    /**
     * @return string
     */
    public function getCampoVisivelPainel()
    {
        return $this->campoVisivelPainel;
    }

    /**
     * @param string $campoVisivelPainel
     * @return SisCampoPersonalizado
     */
    public function setCampoVisivelPainel($campoVisivelPainel)
    {
        $this->campoVisivelPainel = $campoVisivelPainel;

        return $this;
    }

    /**
     * @return string
     */
    public function getCampoExibicaoExterna()
    {
        return $this->campoExibicaoExterna;
    }

    /**
     * @param string $campoExibicaoExterna
     * @return SisCampoPersonalizado
     */
    public function setCampoExibicaoExterna($campoExibicaoExterna)
    {
        $this->campoExibicaoExterna = $campoExibicaoExterna;

        return $this;
    }

    /**
     * @return string
     */
    public function getCampoAtivo()
    {
        return $this->campoAtivo;
    }

    /**
     * @param string $campoAtivo
     * @return SisCampoPersonalizado
     */
    public function setCampoAtivo($campoAtivo)
    {
        $this->campoAtivo = $campoAtivo;

        return $this;
    }

    /**
     * @return string
     */
    public function getCampoMultiplo()
    {
        return $this->campoMultiplo;
    }

    /**
     * @param string $campoMultiplo
     * @return SisCampoPersonalizado
     */
    public function setCampoMultiplo($campoMultiplo)
    {
        $this->campoMultiplo = $campoMultiplo;

        return $this;
    }

    /**
     * @return string
     */
    public function getCampoValores()
    {
        return $this->campoValores;
    }

    /**
     * @param string $campoValores
     * @return SisCampoPersonalizado
     */
    public function setCampoValores($campoValores)
    {
        $this->campoValores = $campoValores;

        return $this;
    }

    /**
     * @return \Sistema\Entity\SisCampoEntidade
     */
    public function getEntidade()
    {
        return $this->entidadeId;
    }

    /**
     * @param \Sistema\Entity\SisCampoEntidade $entidade
     * @return SisCampoPersonalizado
     */
    public function setEntidade($entidade)
    {
        $this->entidadeId = $entidade;

        return $this;
    }

    /**
     * @return \Sistema\Entity\SisCampoTipo
     */
    public function getTipoCampo()
    {

        return $this->tipoCampo;
    }

    /**
     * @param \Sistema\Entity\SisCampoTipo $tipoCampo
     * @return SisCampoPersonalizado
     */
    public function setTipoCampo($tipoCampo)
    {
        $this->tipoCampo = $tipoCampo;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'campoId'               => $this->getCampoId(),
            'campoNome'             => $this->getCampoNome(),
            'campoDescricao'        => $this->getCampoDescricao(),
            'campoTamanhoMin'       => $this->getCampoTamanhoMin(),
            'campoTamanhoMax'       => $this->getCampoTamanhoMax(),
            'campoExpressaoRegular' => $this->getCampoExpressaoRegular(),
            'campoValorPadrao'      => $this->getCampoValorPadrao(),
            'campoObrigatorio'      => $this->getCampoObrigatorio(),
            'campoVisivelPainel'    => $this->getCampoVisivelPainel(),
            'campoExibicaoExterna'  => $this->getCampoExibicaoExterna(),
            'campoAtivo'            => $this->getCampoAtivo(),
            'campoMultiplo'         => $this->getCampoMultiplo(),
            'campoValores'          => $this->getCampoValores()
        );

        if ($this->getTipoCampo()->getTipoCampo()) {
            $array['campoTipoId']        = $this->getTipoCampo()->getTipoCampo();
            $array['campoTipoDescricao'] = $this->getTipoCampo()->getTipoDescricao();
        }

        if ($this->getEntidade()->getEntidadeId()) {
            $array['campoEntidadeId']        = $this->getEntidade()->getEntidadeId();
            $array['campoEntidadeDescricao'] = $this->getEntidade()->getEntidadeDescricao();
        }

        return $array;
    }
}
