<?php

namespace Sistema\Plugins;

/**
 * Class Email
 * @package Sistema\Plugins
 */
class Email
{
    /**
     * @param       $subject
     * @param       $content
     * @param       $name
     * @param       $email
     * @param array $options
     * @return bool|string
     */
    public function sendEmail($subject, $content, $name, $email, $options = array())
    {
        try {
            $mail = new \PHPMailer();

            $mail->isSMTP();
            $mail->Host     = $options['contaHost'];
            $mail->SMTPAuth = true;
            $mail->Username = $options['contaUsuario'];
            $mail->Password = $options['contaSenha'];

            if ($options['contaSsl']) {
                $mail->SMTPSecure = $options['contaSsl'];
            }

            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer'       => false,
                    'verify_peer_name'  => false,
                    'allow_self_signed' => true
                )
            );

            $mail->Port = $options['contaPorta'];

            $mail->setFrom($options['contaUsuario']);

            if ($mail->Host == 'smtp.office365.com') {
                $mail->addBCC($options['contaUsuario']);
            }

            $mail->addAddress($email, $name);
            $mail->isHTML(true);

            $mail->CharSet = 'utf-8';
            $mail->Subject = $subject;
            $mail->Body    = $content;
            $mail->AltBody = strip_tags($content);

            if (!$mail->send()) {
                return $mail->ErrorInfo;
            } else {
                return true;
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Baseado em http://stackoverflow.com/a/38280466/7146797
     * @param            $hostname
     * @param            $port
     * @param            $username
     * @param            $password
     * @param bool|false $protocol
     * @return bool|string
     */
    public function validSMTP($hostname, $port, $username, $password, $protocol = false)
    {
        $mail = new \PHPMailer();

        $mail->isSMTP();
        $mail->Host     = $hostname;
        $mail->SMTPAuth = true;
        $mail->Username = $username;
        $mail->Password = $password;

        if ($protocol) {
            $mail->SMTPSecure = $protocol;
        }

        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer'       => false,
                'verify_peer_name'  => false,
                'allow_self_signed' => true
            )
        );

        $mail->Port = $port;

        if (!$mail->smtpConnect()) {
            return ($mail->ErrorInfo ? $mail->ErrorInfo : 'Falha ao validar conta de e-mail');
        }

        return true;
    }
}