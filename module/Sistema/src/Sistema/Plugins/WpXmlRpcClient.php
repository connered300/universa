<?php
namespace Sistema\Plugins;

use Zend\XmlRpc\Value\Base64;

/**
 * Wp xmlrpc client
 *
 * https://github.com/elvisciotti/wp-xmlrpc-client/
 * http://codex.wordpress.org/XML-RPC_WordPress_API
 */
class WpXmlRpcClient
{
    const STATUS_DRAFT = 'draft';
    const STATUS_PENDING = 'pending';
    const STATUS_PRIVATE = 'private';
    const STATUS_PUBLISH = 'publish';

    const TAXONOMY_TAG = 'post_tag';
    const TAXONOMY_CATEGORY = 'category';

    const POST_TYPE_POST = 'post';
    const POST_TYPE_PAGE = 'page';

    /**
     * @var \Zend\XmlRpc\Client
     */
    private $client;

    private $options;

    /**
     * @param array $options array (url | username | password | blogId )
     */
    function __construct(array $options)
    {
        $this->options = $options;
        $this->client  = new \Zend\XmlRpc\Client($this->options['url']);
        $this->client->getHttpClient()->setOptions(array('timeout' => 60));
    }

    /**
     * @param $status
     * @return bool
     */
    public static function isValidStatus($status)
    {
        return $status == self::STATUS_DRAFT
        || $status == self::STATUS_PENDING
        || $status == self::STATUS_PRIVATE
        || $status == self::STATUS_PUBLISH;
    }

    /**
     * @param string $path
     * @param string $name
     *
     * @return array (id, file, url, type)
     */
    public function uploadFile($path)
    {
        if (!file_exists($path)) {
            return array();
        }

        preg_match('/\.([a-z]+)$/i', $path, $m);
        if (empty($m[1])) {
            throw new \InvalidArgumentException("$path must be the file extension or wp upload fill fail");
        }

        $name  = basename($path);
        $type  = mime_content_type($path);
        $bites = new Base64(file_get_contents($path), false);

        $data = array(
            'name'      => $name,
            'type'      => $type,
            'bits'      => $bites,
            'overwrite' => false //wp creates multiple copies with the same name
        );

        return $this->call('wp.uploadFile', array('data' => $data));
    }

    /**
     * Perform XMLRPC call and adds including blog id, username and pass to params
     *
     * @param string $action
     * @param array  $options
     * @return array response from wp xmlrpc
     */
    protected function call($action, $options = array())
    {
        $addOptions = array(
            'blog_id'  => $this->options['blog_id'],
            'username' => $this->options['username'],
            'password' => $this->options['password']
        );
        $options    = $addOptions + $options;

        $response = $this->client->call($action, $options);

        return $response;
    }

    /**
     * @return array catId=>catName
     */
    public function getCategories()
    {
        $ret      = array();
        $response = $this->call('wp.getTerms', array('taxonomy' => self::TAXONOMY_CATEGORY));

        foreach ($response as $row) {
            $ret[$row['term_id']] = $row['name'];
        }

        return $ret;
    }

    /**
     * http://codex.wordpress.org/XML-RPC_WordPress_API/Posts#wp.newPost
     *
     * @return int ID of saved post
     */
    public function newPost(
        $title,
        $content,
        array $categories = array(),
        array $postTags = array(),
        $status = self::STATUS_PUBLISH,
        $type = 'post',
        $otherOptions = array()
    ) {
        $content = array(
            'post_status'  => $status,
            'post_title'   => $title,
            'post_content' => $content,
            'post_type'    => $type,
        );

        $terms = array();

        if ($postTags) {
            $terms['post_tag'] = $this->addTermsAndReturnIds($postTags, self::TAXONOMY_TAG);
        }

        if ($categories) {
            $terms['category'] = $this->addTermsAndReturnIds($categories, self::TAXONOMY_CATEGORY);
        }

        if ($terms) {
            $content['terms'] = $terms;
        }

        $content = array_merge($content, $otherOptions);

        $response = $this->call(
            'wp.newPost',
            array('content' => $content)
        );

        return (int)$response;
    }

    /**
     * Add the terms (if not existing).
     * Return the IDs of all the terms.
     * To use with newPost
     *
     * @param array  $termNames
     * @param string $taxonomy
     *
     * @return array of IDs of the terms (both added or new)
     */
    protected function addTermsAndReturnIds(array $termNames, $taxonomy)
    {
        $ret = array();
        foreach ($termNames as $termNameToAdd) {
            if ($id = $this->getOrCreateTerm($termNameToAdd, $taxonomy)) {
                $ret[] = $id;
            }
        }

        return $ret;
    }

    /**
     * @param string $term
     * @param string $taxonomy
     *
     * @return int|null term_id of added term, null if error occurs (duplicate tag coz of encoding ?)
     */
    private function getOrCreateTerm($term, $taxonomy)
    {
        try {
            $term = htmlspecialchars($term); //convert &, but not áéíóú
            if ($existingId = $this->searchTerm($term, $taxonomy)) {
                return (int)$existingId;
            } else {
                $response = $this->call(
                    'wp.newTerm',
                    array('content' => array('taxonomy' => $taxonomy, 'name' => $term))
                );

                return (int)$response;
            }
        } catch (\BadMethodCallException $e) {
            // avoids A term with the name provided already exists.
        }

        return null;
    }

    /**
     * @return int|null
     */
    private function searchTerm($search, $taxonomy)
    {
        $foundTerms = $this->call(
            'wp.getTerms',
            array(
                'taxonomy' => $taxonomy,
                'filter'   => array(
                    'search'     => $search,
                    'hide_empty' => false
                )
            )
        );

        foreach ($foundTerms as $term) {
            if ($term['name'] == $search) { //maybe not needed ?
                return $term['term_id'];
            }
        }

        return null;
    }

    /**
     * @return array
     */
    public function getPosts($limit = 10)
    {
        $filter = array(
            'number' => $limit
        );

        return $this->call('wp.getPosts', array('filter' => $filter));
    }

    /**
     * @param array $postIds
     *
     * @return array key=postId, value=boolean result
     */
    public function deletePosts(array $postIds)
    {
        $ret = array();
        foreach ($postIds as $postId) {
            $ret[$postId] = $this->call('wp.deletePost', array('post_id' => $postId));
        }

        return $ret;
    }

    /**
     * @return array
     */
    public function getPostStatusList()
    {
        return $this->call('wp.getPostStatusList');
    }

    /**
     * @param $params
     * @return mixed|null
     */
    public function proccessByArray($params)
    {
        $methods = get_class_methods($this);
        $method  = $params['method'];

        if (in_array($method, $methods)) {
            $fnReflection = new \ReflectionMethod($this, $method);
            $fixedParams  = array();

            foreach ($fnReflection->getParameters() as $param) {
                if ($params[$param->name]) {
                    $fixedParams[$param->name] = $params[$param->name];
                } else {
                    $fixedParams[$param->name] = $param->getDefaultValue();
                }
            }

            return $fnReflection->invokeArgs($this, $fixedParams);
        }

        return null;
    }

    //
    //    public function getOptions()
    //    {
    //        return $this->call('wp.getOptions', array('options'=>array()));
    //    }

    /**
     * @param $termId
     * @param $taxonomy
     * @return array
     */
    private function deleteTaxonomy($termId, $taxonomy)
    {
        return $this->call('wp.deleteTerm', array('content' => array('term_id' => $termId, 'taxonomy' => $taxonomy)));
    }
}