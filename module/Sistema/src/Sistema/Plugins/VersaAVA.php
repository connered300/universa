<?php

namespace Sistema\Plugins;

/**
 * Class VersaAVA
 * @package Sistema\Plugins
 */
class VersaAVA extends WpXmlRpcClient
{
    const TAXONOMY_COURSE_CATEGORY = 'course-cat';
    const POST_TYPE_CURSO          = 'course';
    const POST_TYPE_TURMA          = 'class';
    private $taxonomiaNivel      = 'course-cat';
    private $taxonomiaModalidade = 'course-cat';
    private $taxonomiaArea       = 'course-cat';
    private $versao              = 2;

    /** @var \Doctrine\ORM\EntityManager $entityManager */
    private $entityManager;
    /** @var integer */
    private $integracaoId;

    /**
     * @param \Doctrine\ORM\EntityManager $entityManager
     * @param                             $options
     */
    public function __construct(\Doctrine\ORM\EntityManager $entityManager, $options)
    {
        $this->setEntityManager($entityManager);
        $this->setIntegracaoId($options['integracaoId']);

        if ($options['versao']) {
            $this->setVersao($options['versao']);
        }

        if ($options['taxonomia-nivel']) {
            $this->setTaxonomiaNivel($options['taxonomia-nivel']);
        }

        if ($options['taxonomia-area']) {
            $this->setTaxonomiaArea($options['taxonomia-area']);
        }

        if ($options['taxonomia-modalidade']) {
            $this->setTaxonomiaModalidade($options['taxonomia-modalidade']);
        }

        parent::__construct($options);
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @param \Doctrine\ORM\EntityManager $entityManager
     * @return VersaAVA
     */
    public function setEntityManager($entityManager)
    {
        $this->entityManager = $entityManager;

        return $this;
    }

    /**
     * @return int
     */
    public function getIntegracaoId()
    {
        return $this->integracaoId;
    }

    /**
     * @param int $integracaoId
     * @return VersaAVA
     */
    public function setIntegracaoId($integracaoId)
    {
        $this->integracaoId = $integracaoId;

        return $this;
    }

    /**
     * @return string
     */
    public function getTaxonomiaNivel()
    {
        return $this->taxonomiaNivel;
    }

    /**
     * @param string $taxonomiaNivel
     * @return VersaAVA
     */
    public function setTaxonomiaNivel($taxonomiaNivel)
    {
        $this->taxonomiaNivel = $taxonomiaNivel;

        return $this;
    }

    /**
     * @return string
     */
    public function getTaxonomiaModalidade()
    {
        return $this->taxonomiaModalidade;
    }

    /**
     * @param string $taxonomiaModalidade
     * @return VersaAVA
     */
    public function setTaxonomiaModalidade($taxonomiaModalidade)
    {
        $this->taxonomiaModalidade = $taxonomiaModalidade;

        return $this;
    }

    /**
     * @return string
     */
    public function getTaxonomiaArea()
    {
        return $this->taxonomiaArea;
    }

    /**
     * @param string $taxonomiaArea
     * @return VersaAVA
     */
    public function setTaxonomiaArea($taxonomiaArea)
    {
        $this->taxonomiaArea = $taxonomiaArea;

        return $this;
    }

    /**
     * @return int
     */
    public function getVersao()
    {
        return $this->versao;
    }

    /**
     * @param int $versao
     * @return VersaAVA
     */
    public function setVersao($versao)
    {
        $this->versao = $versao;

        return $this;
    }

    /**
     * @param            $cursoId
     * @param            $nome
     * @param            $descricao
     * @param bool|false $area
     * @return bool|string
     */
    public function criarCurso($cursoId, $nome, $descricao = "", $area = '', $nivel = '', $modalidade = '')
    {
        return $this->integrarCurso($cursoId, $nome, $descricao, $area, $nivel, $modalidade);
    }

    /**
     * @param            $cursoId
     * @param            $nome
     * @param            $descricao
     * @param bool|false $area
     * @return bool|string
     */
    public function editarCurso($cursoId, $nome, $descricao = "", $area = '', $nivel = '', $modalidade = '')
    {
        return $this->integrarCurso($cursoId, $nome, $descricao, $area, $nivel, $modalidade);
    }

    /**
     * @param            $cursoId
     * @param            $nome
     * @param            $descricao
     * @param bool|false $area
     * @return bool|string
     * @throws \Exception
     */
    public function integrarCurso($cursoId, $nome, $descricao = "", $area = '', $nivel = '', $modalidade = '')
    {
        $serviceIntegracao = new \Sistema\Service\SisIntegracaoCurso($this->getEntityManager());
        $serviceAcadCurso  = new \Matricula\Service\AcadCurso($this->getEntityManager());

        /** @var \Matricula\Entity\AcadCurso $objAcadCurso */
        $objAcadCurso = $serviceAcadCurso->getRepository()->findOneBy(['cursoId' => $cursoId]);

        if (!$objAcadCurso) {
            throw new \Exception('Curso Não existe no Universa!');
        }

        $nome      = trim(preg_replace("/[\n\r\t]/", "", $nome));
        $descricao = trim(preg_replace("/[\n\r\t]/", "", $descricao));
        $descricao = html_entity_decode($descricao ? $descricao : $nome);

        $content = array(
            'post_title'   => $nome,
            'post_content' => $descricao,
            'post_type'    => self::POST_TYPE_CURSO,
            'post_status'  => self::STATUS_PUBLISH,
        );

        $content['terms_names'] = array();

        if ($area) {
            $content['terms_names'][$this->getTaxonomiaArea()] = is_array($area) ? $area : [$area];
        }

        if ($modalidade) {
            $content['terms_names'][$this->getTaxonomiaModalidade()] = (
            is_array($modalidade) ? $modalidade : [$modalidade]
            );
        }

        if ($nivel) {
            $content['terms_names'][$this->getTaxonomiaNivel()] = is_array($nivel) ? $nivel : [$nivel];
        }

        $cursoIdIntegracao = $serviceIntegracao->pesquisarCodigoIntegracao($this->getIntegracaoId(), $cursoId);

        if (!$cursoIdIntegracao) {
            $retorno = $this->call('wp.newPost', array('content' => $content));

            if (is_numeric($retorno)) {
                $serviceIntegracao->registrarIntegracaoCurso($this->getIntegracaoId(), $cursoId, $retorno);
            }
        } else {
            $retorno = $this->call('wp.editPost', array('post_id' => $cursoIdIntegracao, 'content' => $content));
        }

        return $retorno ? 'SUCESSO' : false;
    }

    public function integracaoNotasUniversa($course_id, $user_id, $periodo_completo = 'Não')
    {
        $content = array(
            'course_id'        => $course_id,
            'user_id'          => $user_id,
            'periodo_completo' => $periodo_completo
        );

        $retorno = $this->call('ava.integracaoNotasUniversa', array('content' => $content));

        return $retorno;
    }

    private function corrigeArrayIntegracao($oldArr, $key)
    {
        usort(
            $oldArr,
            function ($a, $b) {
                $chave = 'expiracao';

                if ($a[$chave] == $b[$chave]) {
                    return 0;
                }

                return ($a[$chave] > $b[$chave]) ? +1 : -1;
            }
        );

        $newArray = [];

        foreach ($oldArr as $element) {
            $chave            = is_array($element) ? $element[$key] : $element;
            $newArray[$chave] = $element;
        }

        return $newArray;
    }

    /**
     * @param       $alunoId
     * @param       $nome
     * @param       $login
     * @param       $senha
     * @param       $email
     * @param array $cursos
     * @return bool|string
     * @throws \Exception
     */
    public function integrarAluno(
        $alunoId,
        $nome,
        $login,
        $senha,
        $email,
        $cursos = [],
        $turmas = [],
        $disciplinas = []
    ) {
        $serviceDiscCurso         = new \Matricula\Service\AcadgeralDisciplinaCurso($this->getEntityManager());
        $serviceIntegracaoDisc    = new \Sistema\Service\SisIntegracaoDisciplina($this->getEntityManager());
        $serviceIntegracaoCurso   = new \Sistema\Service\SisIntegracaoCurso($this->getEntityManager());
        $serviceIntegracaoAluno   = new \Sistema\Service\SisIntegracaoAluno($this->getEntityManager());
        $serviceIntegracaoTurma   = new \Sistema\Service\SisIntegracaoTurma($this->getEntityManager());
        $serviceAcadCurso         = new \Matricula\Service\AcadCurso($this->getEntityManager());
        $serviceAcadperiodoTurma  = new \Matricula\Service\AcadperiodoTurma($this->getEntityManager());
        $serviceAcadgeralAluno    = new \Matricula\Service\AcadgeralAluno($this->getEntityManager());
        $serviceSisFilaIntegracao = new \Sistema\Service\SisFilaIntegracao($this->getEntityManager());

        $alunoIdIntegracao = $serviceIntegracaoAluno->pesquisarCodigoIntegracao($this->getIntegracaoId(), $alunoId);

        $content = array(
            'display_name' => $nome,
            'nickname'     => $nome,
            'user_login'   => $login,
            'user_pass'    => $senha,
            'user_email'   => $email,
            'courses'      => array(),
            'disciplinas'  => array(),
            'turmas'       => array(),
        );

        if ($this->getVersao() == 1) {
            unset($content['disciplinas']);
            unset($content['turmas']);
        }

        $cursos      = $this->corrigeArrayIntegracao($cursos, 'cursoId');
        $turmas      = $this->corrigeArrayIntegracao($turmas, 'turmaId');
        $disciplinas = $this->corrigeArrayIntegracao($disciplinas, 'discCursoId');

        foreach ($cursos as $curso) {
            $cursoId           = $curso['cursoId'];
            $tempoExpiracao    = $curso['expiracao'];
            $cursoIdIntegracao = $serviceIntegracaoCurso->pesquisarCodigoIntegracao(
                $this->getIntegracaoId(),
                $cursoId
            );

            if ($this->getVersao() == 1) {
                $content['courses'][$cursoIdIntegracao] = $tempoExpiracao;
                continue;
            }

            /** @var \Matricula\Entity\AcadCurso $objAcadCurso */
            $objAcadCurso = $serviceAcadCurso->getRepository()->find($cursoId);

            if ($objAcadCurso->getCursoPossuiPeriodoLetivo() == $serviceAcadCurso::CURSO_POSSUI_PERIODO_LETIVO_SIM) {
                $cursoPossuiPeriodoLetivo = $objAcadCurso->getCursoUnidadeMedida();
            } else {
                $cursoPossuiPeriodoLetivo = false;
            }

            if ($cursoIdIntegracao) {
                $content['courses'][$cursoIdIntegracao] = array(
                    'curso'         => $cursoIdIntegracao,
                    'tempo'         => $tempoExpiracao,
                    'periodicidade' => $cursoPossuiPeriodoLetivo
                );
            } elseif (!$cursoIdIntegracao && $cursoPossuiPeriodoLetivo) {
                throw new \Exception("Curso não foi integrado");

                return false;
            }
        }

        if ($this->getVersao() != 1) {
            foreach ($turmas as $turma) {
                $turmaId           = $turma['turmaId'] ? $turma['turmaId'] : $turma;
                $tempoExpiracao    = $turma['expiracao'] ? $turma['expiracao'] : 1;
                $turmaIdIntegracao = $serviceIntegracaoTurma->pesquisarCodigoIntegracao(
                    $this->getIntegracaoId(),
                    $turmaId
                );

                /** @var \Matricula\Entity\AcadperiodoTurma $objTurma */
                $objTurma = $serviceAcadperiodoTurma->getRepository()->find($turmaId);

                if (!$turmaIdIntegracao) {
                    if (!$objTurma) {
                        throw new \Exception('Turma: ' . $turmaId . ' não localizada');

                        return false;
                    }

                    if (!$serviceSisFilaIntegracao->registrarTurmaNaFilaDeIntegracao($objTurma)) {
                        throw new \Exception(
                            "Turma não foi integrada! " .
                            $serviceAcadperiodoTurma->verificarErroDeVinculoDisciplinaCursoMatriz($objTurma)
                        );

                        return false;
                    }
                }

                /** @var \Matricula\Entity\AcadperiodoTurma $objTurma */
                $objTurma                 = $serviceAcadperiodoTurma->getRepository()->find($turmaId);
                $objCurso                 = $objTurma->getCursocampus()->getCurso();
                $cursoPossuiPeriodoLetivo = $objTurma->getCursocampus()->getCurso()->getCursoPossuiPeriodoLetivo();

                if ($cursoPossuiPeriodoLetivo == $serviceAcadCurso::CURSO_POSSUI_PERIODO_LETIVO_SIM) {
                    $cursoPossuiPeriodoLetivo = $objTurma->getCursocampus()->getCurso()->getCursoUnidadeMedida();
                } else {
                    $cursoPossuiPeriodoLetivo = false;
                }

                if ($cursoPossuiPeriodoLetivo) {
                    $content['turmas'][] = array(
                        'id_turma'      => $turmaIdIntegracao,
                        'tempo'         => $tempoExpiracao,
                        'periodicidade' => $cursoPossuiPeriodoLetivo
                    );
                }
            }

            foreach ($disciplinas as $disciplina) {
                $disccursoId      = $disciplina['discCursoId'] ? $disciplina['discCursoId'] : $disciplina;
                $tempoExpiracao   = $disciplina['expiracao'] ? $disciplina['expiracao'] : 1;
                $discIdIntegracao = $serviceIntegracaoDisc->pesquisarCodigoIntegracao(
                    $this->getIntegracaoId(),
                    $disccursoId
                );

                if (!$discIdIntegracao) {
                    throw new \Exception("Disciplina não foi integrada");

                    return false;
                }

                /** @var \Matricula\Entity\AcadgeralDisciplinaCurso $objDiscCurso */
                $objDiscCurso = $serviceDiscCurso->getRepository()->findOneBy(['discCursoId' => $disccursoId]);

                $cursoPossuiPeriodicidade = $objDiscCurso->getCurso()->getCursoPossuiPeriodoLetivo();

                if ($cursoPossuiPeriodicidade == $serviceAcadCurso::CURSO_POSSUI_PERIODO_LETIVO_SIM) {
                    $cursoPossuiPeriodoLetivo = $objDiscCurso->getCurso()->getCursoUnidadeMedida();
                } else {
                    $cursoPossuiPeriodoLetivo = false;
                }

                $situacaoAlunoDisciplina = $serviceAcadgeralAluno->situacaoAlunoNaDisciplina(
                    $alunoId,
                    $disccursoId
                );

                $desligado = $situacaoAlunoDisciplina ? "Sim" : "Não";

                if ($cursoPossuiPeriodoLetivo) {
                    $content['disciplinas'][] = array(
                        'id_disciplina' => $discIdIntegracao,
                        'tempo'         => $tempoExpiracao,
                        'periodicidade' => $cursoPossuiPeriodoLetivo,
                        'desligado'     => $desligado
                    );
                }
            }

            $content['disciplinas'] = $this->ordernarArrayDisciplinaPeloTempoExpiracao(
                $content['disciplinas'],
                'id_disciplina'
            );
        }

        if ($alunoIdIntegracao) {
            $content['user_id'] = $alunoIdIntegracao;
            unset($content['user_pass']);
        }

        $retorno = $this->call('ava.integracaoAluno', array('content' => $content));

        if (!$alunoIdIntegracao && is_numeric($retorno)) {
            $serviceIntegracaoAluno->registrarIntegracaoAluno($this->getIntegracaoId(), $alunoId, $retorno);
        } elseif (!is_numeric($retorno)) {
            throw new \Exception('Erro:' . json_encode($retorno));
        }

        return $retorno ? 'SUCESSO' : false;
    }

    /**
     * @param            $discId
     * @param            $nome
     * @param            $descricao
     * @return bool|string
     * @throws \Exception
     */
    public function integrarDisciplina($discId, $nome, $descricao = "", $area = '', $nivel = '', $modalidade = '')
    {
        $serviceIntegracao = new \Sistema\Service\SisIntegracaoDisciplina($this->getEntityManager());

        $nome      = trim(preg_replace("/[\n\r\t]/", "", $nome));
        $descricao = trim(preg_replace("/[\n\r\t]/", "", $descricao));
        $descricao = html_entity_decode($descricao ? $descricao : $nome);

        $content = array(
            'post_title'   => $nome,
            'post_content' => $descricao,
            'post_type'    => self::POST_TYPE_CURSO,
            'post_status'  => self::STATUS_PUBLISH,
        );

        $content['terms_names'] = array();

        if ($area) {
            $content['terms_names'][$this->getTaxonomiaArea()] = is_array($area) ? $area : [$area];
        }

        if ($modalidade) {
            $content['terms_names'][$this->getTaxonomiaModalidade()] = (
            is_array($modalidade) ? $modalidade : [$modalidade]
            );
        }

        if ($nivel) {
            $content['terms_names'][$this->getTaxonomiaNivel()] = is_array($nivel) ? $nivel : [$nivel];
        }

        $disciplinaIdIntegracao = $serviceIntegracao->pesquisarCodigoIntegracao($this->getIntegracaoId(), $discId);

        if (!$disciplinaIdIntegracao) {
            $retorno = $this->call('wp.newPost', array('content' => $content));

            if (is_numeric($retorno)) {
                $serviceIntegracao->registrarIntegracaoDisciplina($this->getIntegracaoId(), $discId, $retorno);
            }
        } else {
            $retorno = $this->call('wp.editPost', array('post_id' => $disciplinaIdIntegracao, 'content' => $content));
        }

        return $retorno ? 'SUCESSO' : false;
    }

    public function ordernarArrayDisciplinaPeloTempoExpiracao(&$array, $campo)
    {
        usort(
            $array,
            function ($a, $b) {
                if ($a['expiracao'] == $b['expiracao']) {
                    return 0;
                }

                return ($a['expiracao'] < $b['expiracao'] ? -1 : 1);
            }
        );

        $arrDadosTratados = [];

        foreach ($array as $arrDado) {
            $arrDadosTratados[$arrDado[$campo]] = $arrDado;
        }

        $array = array_values($arrDadosTratados);

        return $array;
    }

    /**
     * @param $turmaId
     * @param $nome
     * @param $serie
     * @param $polo
     * @param $inicio
     * @param $fim
     * @param $curso
     * @param $disciplinas
     * @return bool|string
     * @throws \Exception
     */
    public function integrarTurma($turmaId, $nome, $serie, $polo = "", $inicio, $fim, $curso, $disciplinas = array())
    {
        $serviceFilaIntegracao       = new \Sistema\Service\SisFilaIntegracao($this->getEntityManager());
        $serviceIntegracaoTurma      = new \Sistema\Service\SisIntegracaoTurma($this->getEntityManager());
        $serviceDisciplinaIntegracao = new \Sistema\Service\SisIntegracaoDisciplina($this->getEntityManager());
        $serviceAcadperiodoTurma     = new \Matricula\Service\AcadperiodoTurma($this->getEntityManager());

        $arrCodigoIntegracao = array();
        $valida              = null;

        /** @var \Matricula\Entity\AcadperiodoTurma $objAcadperiodoTurma */
        $objAcadperiodoTurma = $serviceAcadperiodoTurma->getRepository()->find($turmaId);

        foreach ($disciplinas as $disciplinaCursoId) {
            /** @var $objDisciplinaIntegracao \Sistema\Entity\SisIntegracaoDisciplina */
            $objDisciplinaIntegracao = $serviceDisciplinaIntegracao->getRepository()->findOneBy(
                ['cursoDisc' => $disciplinaCursoId, 'integracao' => $this->getIntegracaoId()]
            );

            if (!$objDisciplinaIntegracao) {
                $serviceFilaIntegracao->registrarDisciplinaNaFilaDeIntegracao($disciplinaCursoId);
                $valida = true;
            } else {
                $arrCodigoIntegracao[] = $objDisciplinaIntegracao->getCodigo();
            }
        }

        if ($valida) {
            throw new \Exception('Porfavor verifique se as diciplinas da turma já foram integradas!');

            return false;
        }

        $turmaIdIntegracao = $serviceIntegracaoTurma->pesquisarCodigoIntegracao($this->getIntegracaoId(), $turmaId);

        $content = array(
            'nome'        => $nome,
            'descricao'   => $nome,
            'serie'       => $serie,
            'data_inicio' => $inicio,
            'data_fim'    => $fim,
            'curso_nome'  => $curso,
            'disciplinas' => $arrCodigoIntegracao,
        );

        if ($turmaIdIntegracao) {
            $content['turma_id'] = $turmaIdIntegracao;
        }

        $retorno = $this->call('ava.integracaoTurma', array('content' => $content));

        if (!$turmaIdIntegracao && is_numeric($retorno)) {
            $serviceIntegracaoTurma->registrarIntegracaoTurma($this->getIntegracaoId(), $turmaId, $retorno);
        } elseif (!is_numeric($retorno)) {

            throw new \Exception($retorno);
        }

        return $retorno ? 'SUCESSO' : false;
    }
}
