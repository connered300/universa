<?php

namespace Sistema\Plugins;

/**
 * Class LocaSMS
 * @package Sistema\Plugins
 */
class LocaSMS extends GatewaySMS
{
    public function __construct()
    {
        $this->setServer('http://app.locasms.com.br/painel/api.ashx');
    }

    /**
     * @return $this
     */
    public function processRequest($action = '', $param = array())
    {
        $param = array_merge(
            $param,
            array(
                'action' => $action,
                'lgn'    => $this->getUser(),
                'pwd'    => $this->getPassword()
            )
        );

        $response = $this->getResponse($param, self::HTTP_GET);
        $this->setResult($response, self::TYPE_JSON);

        return $this;
    }

    /**
     * @return boolean
     */
    public function getResultError()
    {
        return $this->getResult('status') == 0;
    }

    /**
     * @return boolean
     */
    public function getResultErrorMessage()
    {
        return $this->getResult('msg');
    }

    /**
     * @return $this
     */
    public function getBalance()
    {
        $saldo = $this
            ->processRequest('getbalance')
            ->getResult('data');

        return $saldo ? $saldo : 0;
    }

    /**
     * @return $this
     */
    public function sendSMS()
    {
        $dateNow = new \DateTime();
        $dateNow = $dateNow->add(new \DateInterval('PT10M'));

        $data = array(
            'msg'     => $this->getMensagem(),
            'numbers' => $this->getRecipients(),
            'jobdate' => $dateNow->format('d/m/Y'),
            'jobtime' => $dateNow->format('H:i:s')
        );

        $retorno = $this
            ->processRequest('sendsms', $data)
            ->getResult('data');

        return $retorno;
    }
}