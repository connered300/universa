<?php

namespace Sistema\Plugins;

/**
 * Class LocaSMS
 * @package Sistema\Plugins
 */
class SMSEmpresa extends GatewaySMS
{
    public function __construct()
    {
        $this->setServer('http://api.smsempresa.com.br/');
    }

    /**
     * @return array
     */
    public function getResult($key = false)
    {
        $result = parent::getResult();
        $result = $result['retorno']['@attributes'] ? $result['retorno']['@attributes'] : array();

        if ($key) {
            $result = $result[$key] ? $result[$key] : null;
        }

        return $result;
    }

    /**
     * @param       $action
     * @param array $param
     * @return $this
     */
    public function processRequest($action = '', $param = array())
    {
        $param = array_merge(
            $param,
            array('key' => $this->getKey())
        );

        if ($action) {
            $param['action'] = $action;
        }

        $response = $this->getResponse($param, self::HTTP_POST);
        $this->setResult($response, self::TYPE_XML);

        return $this;
    }

    /**
     * @return boolean
     */
    public function getResultError()
    {
        return $this->getResult('situacao') != 'OK';
    }

    /**
     * @return boolean
     */
    public function getResultErrorMessage()
    {
        $result = parent::getResult();
        $result = $result['retorno']['@content'] ? $result['retorno']['@content'] : '';

        return $result;
    }

    /**
     * @return $this
     */
    public function getBalance()
    {
        $this
            ->setEndPoint('get')
            ->processRequest('saldo');
        $saldo = $this->getResult('saldo_longo') + $this->getResult('saldo_sms');

        return $saldo ? $saldo : 0;
    }

    /**
     * @return $this
     */
    public function sendSMS()
    {
        $dateNow = new \DateTime();
        $dateNow = $dateNow->add(new \DateInterval('PT10M'));

        $data = array(
            'type'   => '0',
            'number' => $this->getRecipients(),
            'msg'    => $this->getMensagem(),
            //'jobdate' => $dateNow->format('d/m/Y'),
            //'jobtime' => $dateNow->format('H:i'),
            //'refer'   => $this->getCampaignId()
        );

        $retorno = $this
            ->setEndPoint('send')
            ->processRequest('', $data)
            ->getResult('situacao');

        return $retorno;
    }
}