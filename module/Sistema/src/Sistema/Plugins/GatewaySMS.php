<?php

namespace Sistema\Plugins;

/**
 * Class LocaSMS
 * @package Sistema\Plugins
 */
abstract class GatewaySMS
{
    private $server     = '';
    private $user       = '';
    private $key        = '';
    private $password   = '';
    private $mensagem   = '';
    private $recipients = '';
    private $campaignId = null;
    private $endPoint   = '';
    private $result     = [];
    private $error      = null;

    const HTTP_POST = 'POST';
    const HTTP_GET  = 'GET';
    const TYPE_JSON = 'JSON';
    const TYPE_XML  = 'XML';

    /**
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param string $user
     * @return LocaSMS
     */
    public function setUser($user)
    {
        $this->user = $this->formatNumber($user);

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return LocaSMS
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getMensagem()
    {
        return $this->mensagem;
    }

    /**
     * @param string $mensagem
     * @return LocaSMS
     */
    public function setMensagem($mensagem)
    {
        $this->mensagem = $mensagem;

        return $this;
    }

    /**
     * @return string
     */
    public function getRecipients()
    {
        return $this->recipients;
    }

    /**
     * @param string $recipients
     * @return LocaSMS
     */
    public function setRecipients($recipients)
    {
        $this->recipients = $this->formatNumber($recipients);

        return $this;
    }

    /**
     * @return string
     */
    public function getServer()
    {
        $server = trim($this->server, '/');
        $server .= '/' . $this->getEndPoint();

        return trim($server, '/');
    }

    /**
     * @param $celNumber
     * @return mixed
     */
    private function formatNumber($celNumber)
    {
        $celNumber = preg_replace('/[^0-9,]/', '', $celNumber);

        return $celNumber;
    }

    /**
     * @param string $server
     * @return LocaSMS
     */
    public function setServer($server)
    {
        $this->server = $server;

        return $this;
    }

    /**
     * @return array
     */
    public function getResult($key = false)
    {
        $result = $this->result;

        if ($key) {
            $result = $result[$key] ? $result[$key] : null;
        }

        return $result;
    }

    /**
     * @param array $retorno
     * @return LocaSMS
     */
    public function setResult($retorno, $type = self::TYPE_JSON)
    {
        if ($type == self::TYPE_JSON) {
            $retorno = json_decode($retorno, true);
        } elseif ($type == self::TYPE_XML) {
            $ob      = $this->xmlstr_to_array($retorno);
            $json    = json_encode($ob);
            $retorno = json_decode($json, true);
        }

        $this->result = $retorno;

        return $this;
    }

    /**
     * @return null
     */
    public function getCampaignId()
    {
        return $this->campaignId;
    }

    /**
     * @param null $campaignId
     * @return LocaSMS
     */
    public function setCampaignId($campaignId)
    {
        $this->campaignId = $campaignId;

        return $this;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     * @return GatewaySMS
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * @return null
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param null $error
     * @return GatewaySMS
     */
    public function setError($error)
    {
        $this->error = $error;

        return $this;
    }

    /**
     * @return string
     */
    public function getEndPoint()
    {
        return $this->endPoint;
    }

    /**
     * @param string $endPoint
     * @return GatewaySMS
     */
    public function setEndPoint($endPoint)
    {
        $this->endPoint = $endPoint;

        return $this;
    }

    /**
     * convert xml string to php array - useful to get a serializable value
     *
     * @param string $xmlstr
     * @return array
     *
     * @author Adrien aka Gaarf & contributors
     * @see http://gaarf.info/2009/08/13/xml-string-to-php-array/
     */
    public function xmlstr_to_array($xmlstr)
    {
        $doc = new \DOMDocument();
        $doc->loadXML($xmlstr);
        $root            = $doc->documentElement;
        $output          = $this->domnode_to_array($root);
        $output['@root'] = $root->tagName;

        return $output;
    }

    public function domnode_to_array($node)
    {
        $output = array();
        switch ($node->nodeType) {
            case XML_CDATA_SECTION_NODE:
            case XML_TEXT_NODE:
                $output = trim($node->textContent);
                break;
            case XML_ELEMENT_NODE:
                for ($i = 0, $m = $node->childNodes->length; $i < $m; $i++) {
                    $child = $node->childNodes->item($i);
                    $v     = $this->domnode_to_array($child);
                    if (isset($child->tagName)) {
                        $t = $child->tagName;
                        if (!isset($output[$t])) {
                            $output[$t] = array();
                        }
                        $output[$t][] = $v;
                    } elseif ($v || $v === '0') {
                        $output = (string)$v;
                    }
                }
                if ($node->attributes->length && !is_array($output)) { //Has attributes but isn't an array
                    $output = array('@content' => $output); //Change output into an array.
                }
                if (is_array($output)) {
                    if ($node->attributes->length) {
                        $a = array();
                        foreach ($node->attributes as $attrName => $attrNode) {
                            $a[$attrName] = (string)$attrNode->value;
                        }
                        $output['@attributes'] = $a;
                    }
                    foreach ($output as $t => $v) {
                        if (is_array($v) && count($v) == 1 && $t != '@attributes') {
                            $output[$t] = $v[0];
                        }
                    }
                }
                break;
        }

        return $output;
    }

    /**
     * @param     $url
     * @param int $timeout
     * @return mixed
     */
    protected function getResponse($fields = array(), $method = self::HTTP_GET, $timeout = 60)
    {
        $url = $this->getServer();

        if ($method == self::HTTP_GET) {
            $url = $url . '?' . http_build_query($fields);
        }

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);

        if ($method != self::HTTP_GET) {
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $fields);
        }

        curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);

        $result = curl_exec($curl);
        $err    = curl_errno($curl);
        $errmsg = curl_error($curl);
        //$header = curl_getinfo($curl);

        if ($err) {
            $this->setError('Error ' . $err . ': ' . $errmsg);
        } else {
            $this->setError(null);
        }

        curl_close($curl);

        return $result;
    }

    /**
     * @return boolean
     */
    abstract function getResultError();

    /**
     * @param       $action
     * @param array $param
     * @return $this
     */
    abstract function processRequest($action = '', $param = array());

    /**
     * @return int $balance
     */
    abstract function getBalance();

    /**
     * @return $this
     */
    abstract function sendSMS();
}