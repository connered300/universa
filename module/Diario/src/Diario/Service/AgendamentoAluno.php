<?php

namespace Diario\Service;

use VersaSpine\Service\AbstractService;

class AgendamentoAluno extends AbstractService
{

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Diario\Entity\AgendamentoAluno');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @param $params
     * @return array
     */
    public function pesquisaForJson($params)
    {

    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array $arrDados, \Diario\Entity\Agendamento $agendamento, \Acesso\Entity\AcessoPessoas $userLogado)
    {
        $this->valida($arrDados);

        $notaAluno = number_format((float)$arrDados['agendaalunoNota'], 2);
        $agendamentoNota = $agendamento->getAgendaNota();
        if( $agendamentoNota && (float) $agendamentoNota < (float) $notaAluno){
            throw new \Exception("Nota do aluno não pode ser superior ao valor do agendamento!");
        }


        $objAgAluno = $this->getRepository()->findOneByAgendaalunoId($arrDados['agendaalunoId']);
        if( !$objAgAluno ){
            $objAgAluno = new \Diario\Entity\AgendamentoAluno();
        }
        $this->begin();

        $objAgAluno->setAgendaalunoNota( $notaAluno );
        $objAgAluno->setAgendaalunoDataEntrega($arrDados['agendaalunoDataEntrega']);
        $objAgAluno->setAgendaalunoDataAvaliacao(date("Y-m-d H:i:s"));
        $objAgAluno->setAgendaalunoEstado("Avaliado");
        $objAgAluno->setUsuarioAvaliador($userLogado);

        $alunoDataEntrega = $objAgAluno->getAgendaalunoDataEntrega()->getTimestamp();

        $dataInicioAg = $agendamento->getAgendaDataInicio()->getTimestamp();
        $dataFimAg    = $agendamento->getAgendaDataFim()->getTimestamp();

        if( $dataFimAg < $alunoDataEntrega ){
            throw new \Exception("Data de entrega do aluno não pode ser maior que a data do agendamento!");
        }


        $this->getEm()->persist($objAgAluno);
        $this->getEm()->flush($objAgAluno);

        $this->commit();

        return $objAgAluno;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrDados)
    {
        $errors = array();

        if( !isset($arrDados['agendaalunoNota']) ){
            $errors[] = 'Aluno sem informação de nota, por favor preencha o campo de valor!';
        }

        if( !$arrDados['agendaalunoDataEntrega'] ){
            $errors[] = 'Aluno sem data de entrega do agendamento, por favor preencha o campo de valor!';
        }

        if (!empty($errors)) {
            throw new \Exception(implode("<br>", $errors));
        }

        return true;
    }





}