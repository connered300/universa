<?php

namespace Diario\Service;

use VersaSpine\Service\AbstractService;

class Agendamento extends AbstractService
{

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Diario\Entity\Agendamento');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @param $params
     * @return array
     */
    public function pesquisaForJson($params)
    {

        //devido a utilização de vários parametros, achei melhor usar o query builder do pdo
        //para evitar tratamento desnecessário

        $qb = $this->getEm()->getConnection()->createQueryBuilder();
        $qb->select("*")
           ->from("acadperiodo__turma", "apt");

        if( $params['cursocampus_id'] ){
            $qb->andWhere("apt.cursocampus_id = :cursocampus_id")
               ->setParameter(":cursocampus_id", $params['cursocampus_id']);
        }

        if( !$params["per_id"] ){
            $serviceAcadPeriodo = new \Matricula\Service\AcadperiodoLetivo($this->getEm());
            $periodoLetivoAtual = $serviceAcadPeriodo->buscaUltimoPeriodo();

            $params["per_id"] = $periodoLetivoAtual->getPerId();
        }

        //como pode vim uma string vazia é mais fácil tratar assim
        if( isset($params["turma_nome"]) ){
            $qb->andWhere( "apt.turma_nome LIKE :turma_nome" )
               ->setParameter(":turma_nome", "{$params['turma_nome']}%")
               ->addOrderBy("turma_nome") ;
        }

        if( $params["turma_id"] ){
            $qb->andWhere( "apt.turma_id = :turma_id" )
               ->setParameter(":turma_id", $params['turma_id']);
        }

        //como pode vim uma string vazia é mais fácil tratar assim
        if( isset($params["disc_nome"]) ){
            $qb->innerJoin("apt", "acadperiodo__matriz_disciplina", "amcd", "amcd.mat_cur_id = apt.mat_cur_id AND per_disc_periodo*1 = apt.turma_serie*1")
               ->innerJoin("amcd", "acadgeral__disciplina", "agd", "amcd.disc_id = agd.disc_id")
               ->andWhere( "agd.disc_nome LIKE :disc_nome" )
               ->setParameter(":disc_nome", "{$params['disc_nome']}%")
               ->addOrderBy("disc_nome")
               ->addGroupBy("agd.disc_id");
        }

        $qb->andWhere("apt.per_id = :per_id")
           ->setParameter(":per_id", $params['per_id']);

        return $qb->execute()->fetchAll();
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array $arrDados)
    {
        $this->valida($arrDados);

        $dataAtual = date("Y-m-d H:i:s");

        $this->begin();

        $objAgendamento = new \Diario\Entity\Agendamento();


        if($arrDados['agendaId'] && !($objAgendamento = $this->getRepository()->findOneByAgendaId($arrDados['agendaId'])) ){
            throw new \Exception("Agendamento inexistente!");
        } else if(!$arrDados['agendaId']){
            $objAgendamento->setAgendaDataCadastro($dataAtual);
        }

        $serviceAgendamentoTipo = new \Diario\Service\AgendamentoTipo($this->getEm());

        $objAgTipo = $serviceAgendamentoTipo->getRepository()->findOneByAgendaNome($arrDados['agendaNome']);
        $objAgendamento->setAgendaNome($objAgTipo);

        $objAgendamento->setAgendaDataInicio($dataAtual);
        $objAgendamento->setAgendaTitulo($arrDados['agendaTitulo']);
        $objAgendamento->setAgendaDataFim($arrDados['agendaDataFim']);
        $objAgendamento->setAgendaAvaliativa($arrDados['agendaAvaliativa']);

        if($arrDados['agendaAvaliativa'] == "Não"){
            $arrDados['agendaNota'] = 0;
        }

        $arrDados['agendaNota'] = number_format((float)$arrDados['agendaNota'], 2);

        $objAgendamento->setAgendaNota($arrDados['agendaNota']);
        $objAgendamento->setAgendaEntregaTipo($arrDados['agendaEntregaTipo']);

        //Por enquanto não vai ter anexo, futuramente modificar aki
        $objAgendamento->setAgendaAnexos("Não");

        $this->getEm()->persist($objAgendamento);
        $this->getEm()->flush($objAgendamento);

        $servicoAgendamentoDiario = new \Diario\Service\AgendamentoDiario($this->getEm());

        if ( !$servicoAgendamentoDiario->save($arrDados, $objAgendamento) ){
            $this->rollback();
            return false;
        }

        $this->commit();

        return $objAgendamento;
    }

    public function remover($agendaId)
    {
        $objAgendamento = $this->getRepository()->findOneByAgendaId($agendaId);
        if (! $objAgendamento ){
            throw new \Exception('Não foi possível remover o agendamento, pois o mesmo é inexistente!');
        }

        $this->begin();

        $serviceDiarioAgendamento = new \Diario\Service\AgendamentoDiario($this->getEm());

        $serviceDiarioAgendamento->remover($objAgendamento);

        $this->getEm()->remove($objAgendamento);
        $this->getEm()->flush();

        $this->commit();

        return true;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrDados)
    {
        $errors = array();

        if (!$arrDados['agendaDataFim']) {
            $errors[] = 'Por favor informe a data de entrega do agendamento!';
        }

        if($arrDados['agendaAvaliativa'] == "Sim" && !$arrDados['agendaNota']){
            $errors[] = 'O agendamento é avaliativo, por favor preencha o campo de valor!';
        }

        if (!empty($errors)) {
            throw new \Exception(implode("<br>", $errors));
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $qb = $this->getEm()->getConnection()->createQueryBuilder();

        //devido a utilização de vários parametros, achei melhor usar o query builder do pdo
        //para evitar tratamento desnecessário

        $qb->select("ag.*, aad.agendadiario_id, etapa_descricao, aturma.turma_nome, ac.curso_nome, 
                            ad.disc_nome, amd.per_disc_avaliacao, per_nome,
                            DATE_FORMAT(ag.agenda_data_fim,'%d/%m/%Y') as agenda_data_fim_br")
           ->from("acadperiodo__agendamento", "ag")
           ->innerJoin("ag"  , "acadperiodo__agendamento_diario", "aad", "aad.agenda_id = ag.agenda_id")
           ->innerJoin("aad" , "acadperiodo__etapa_diario", "aed", "aad.diario_id = aed.diario_id")
           ->innerJoin("aed"  , "acadperiodo__docente_disciplina", "apdd", "aed.docdisc_id = apdd.docdisc_id")
           ->innerJoin("aed" , "acadperiodo__etapas", "ae", "aed.etapa_id = ae.etapa_id")
           ->innerJoin("apdd", "acadperiodo__turma", "aturma", "aturma.turma_id = apdd.turma_id")
           ->innerJoin("aturma", "acadperiodo__letivo", "al", "al.per_id = aturma.per_id")
           ->innerJoin("aturma", "campus_curso", "cc", "cc.cursocampus_id = aturma.cursocampus_id")
           ->innerJoin("cc", "acad_curso", "ac", "ac.curso_id = cc.curso_id")
           ->innerJoin("apdd", "acadgeral__disciplina", "ad", "ad.disc_id = apdd.disc_id")
           ->innerJoin("ad"  , "acadperiodo__matriz_disciplina", "amd", "aturma.mat_cur_id = amd.mat_cur_id AND ad.disc_id = amd.disc_id")
           ->andWhere("apdd.docdisc_data_fim IS NULL");

        if( $data['per_id'] ){
            $qb->andWhere("aturma.per_id = {$data['per_id'] }");
        }
        if( $data['cursocampus_id'] ){
            $qb->andWhere("aturma.cursocampus_id = {$data['cursocampus_id'] }");
        }
        if( $data['turma_id'] ){
            $qb->andWhere("apdd.turma_id = {$data['turma_id'] }");
        }
        if( $data['disc_id'] ){
            $qb->andWhere("apdd.disc_id = {$data['disc_id'] }");
        }
        if( $data['etapa_id'] ){
            $qb->andWhere("aed.etapa_id = {$data['etapa_id'] }");
        }

        $query = $qb->getSQL();

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($params){

        $sql = "SELECT *, DATE_FORMAT(ag.agenda_data_fim,'%d/%m/%Y') as agenda_data_fim_br
                    FROM acadperiodo__agendamento ag
                    INNER JOIN acadperiodo__agendamento_diario agd USING (agenda_id)
                    INNER JOIN acadperiodo__etapa_diario aed USING (diario_id)
                    LEFT OUTER JOIN acadperiodo__etapa_diario_extensao aede ON (aede.diario_id = aed.diario_id)
                    INNER JOIN acadperiodo__docente_disciplina apdd USING (docdisc_id)
                    INNER JOIN acadperiodo__etapas ae ON aed.etapa_id = ae.etapa_id
                    INNER JOIN acadperiodo__turma aturma ON aturma.turma_id = apdd.turma_id
                    INNER JOIN acadperiodo__letivo al ON aturma.per_id = al.per_id
                    INNER JOIN campus_curso cc ON cc.cursocampus_id = aturma.cursocampus_id
                    INNER JOIN acad_curso ac ON ac.curso_id = cc.curso_id
                    INNER JOIN acadperiodo__matriz_disciplina amd on (aturma.mat_cur_id = amd.mat_cur_id AND apdd.disc_id = amd.disc_id)
                    INNER JOIN acadgeral__disciplina ad ON ad.disc_id = amd.disc_id
                    WHERE apdd.docdisc_data_fim IS NULL";

        $parametros = [];

        if( $params['agenda_id'] ){
            $sql .= " AND ag.agenda_id = :agenda_id";

            $parametros['agenda_id'] = $params['agenda_id'];
        }

        return $this->executeQueryWithParam($sql, $parametros)->fetch();
    }


}