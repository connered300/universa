<?php

namespace Diario\Service;

use Doctrine\ORM\Query\Expr;
use Professor\Entity\AcadperiodoEtapaDiario;
use VersaSpine\Service\AbstractService;
use Zend\Db\Sql\Predicate\Expression;

class AgendamentoDiario extends AbstractService
{

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Diario\Entity\AgendamentoDiario');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @param $params
     * @return array
     */
    public function pesquisaForJson($params)
    {

    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save($arrDados, \Diario\Entity\Agendamento $agendamento)
    {

        $this->valida($arrDados);

        $docenteDisciplina = $this->getRepository("\Matricula\Entity\AcadperiodoDocenteDisciplina")->findOneBy(["disc"=>$arrDados['disc'], "turma"=>$arrDados['turma']]);

        if( !$docenteDisciplina ){
            throw new \Exception("Docente não vinculado a disciplina!");
        }

        $etapaDiario = $this->getRepository("\Professor\Entity\AcadperiodoEtapaDiario")->findOneBy(["etapa"=>$arrDados['etapa'], "docdisc"=>$docenteDisciplina->getDocdiscId()]);

        if(!$etapaDiario){
            throw new \Exception("Docente não possui diário na etapa!");
        }

        $this->validaAgendamentoDiario($etapaDiario, $agendamento);

        //vendo se o agendamento já está incluído no diário
        $objAgDiario = $this->getRepository()->findOneBy(['diario'=>$etapaDiario->getDiarioId(), "agenda"=>$agendamento->getAgendaId()]);

        if( !$objAgDiario ){

            $objAgDiario = new \Diario\Entity\AgendamentoDiario($this->getEm());

            $objAgDiario->setAgenda($agendamento);
            $objAgDiario->setDiario($etapaDiario);

            $this->getEm()->persist($objAgDiario);
            $this->getEm()->flush($objAgDiario);
        }


        return $objAgDiario;
    }


    public function remover( \Diario\Entity\Agendamento $objAgendamento)
    {
        $objDiarioAgendamento = $this->getRepository()->findByAgenda($objAgendamento->getAgendaId());
        if( !$objDiarioAgendamento ){
            throw new \Exception('Não foi possível remover o agendamento, pois o mesmo não existe no diário!');
        }

        $this->begin();

        foreach($objDiarioAgendamento as $cadaDiarioAg){
            $this->getEm()->remove($cadaDiarioAg);
            $this->getEm()->flush($cadaDiarioAg);
        }

        $this->commit();

        return true;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrDados)
    {
        $errors = array();

        if (!$arrDados['turma']) {
            $errors[] = 'Por favor informe a turma!';
        }

        if (!$arrDados['disc']) {
            $errors[] = 'Por favor informe a disciplina!';
        }

        if (!$arrDados['etapa']) {
            $errors[] = 'Por favor informe a etapa do período letivo do agendamento!';
        }

        if (!empty($errors)) {
            throw new \Exception(implode("<br>", $errors));
        }

        return true;
    }


    private function buscaConfigCurso($docdiscId){
        $sql = "SELECT *
                    FROM acadperiodo__docente_disciplina apdd 
                    INNER JOIN acadperiodo__turma aturma using(turma_id)
                    INNER JOIN acadperiodo__letivo operiodo using(per_id)
                    INNER JOIN campus_curso cc using(cursocampus_id)
                    INNER JOIN acad_curso_config ac ON (
                      cc.curso_id = ac.curso_id AND
                      (
                        (aturma.per_id BETWEEN per_ativ_id AND coalesce(per_desat_id,aturma.per_id)) OR
                        (operiodo.per_data_inicio BETWEEN cursoconfig_data_inicio and cursoconfig_data_fim)
                      )
                    )
                    INNER JOIN acadperiodo__matriz_disciplina amd ON (apdd.disc_id = amd.disc_id AND amd.mat_cur_id = aturma.mat_cur_id)
                    WHERE docdisc_id = :docdisc_id AND docdisc_data_fim IS NULL
                ";

        $params = [];

        if($docdiscId){
            $params["docdisc_id"] = $docdiscId;
        }

        return $this->executeQueryWithParam($sql, $params)->fetch();
    }


    private function validaAgendamentoDiario(\Professor\Entity\AcadperiodoEtapaDiario $etapaDiario, \Diario\Entity\Agendamento $agendamento){
        $configCurso = $this->buscaConfigCurso($etapaDiario->getDocdisc()->getDocdiscId());

        if(!$configCurso){
            throw new \Exception('Configuração do curso inexistente, entre em contato com o suporte para mais informações!');
        }

        if($configCurso['per_disc_avaliacao'] == "Valor Numérico" && is_numeric($agendamento->getAgendaNota())){

            $sql = "SELECT COALESCE( SUM(agenda_nota), 0) as totalAgendamento
                    FROM acadperiodo__agendamento_diario aad
                    INNER JOIN acadperiodo__agendamento ag using (agenda_id)
                    WHERE ag.agenda_avaliativa = 'Sim' AND aad.diario_id = :diario_id
                      AND ag.agenda_id NOT IN (:agenda_id)
                    ";
            $params = [
                "diario_id"     => $etapaDiario->getDiarioId(),
                "agenda_id"     => $agendamento->getAgendaId(),
            ];

            $totalDiario = $this->executeQueryWithParam($sql,$params)->fetch();

            $dataEnvioMax = $etapaDiario->getDiarioDataEnvioFinal();

            $serviceDiarioExtensao = new \Professor\Service\AcadperiodoEtapaDiarioExtensao($this->getEm());
            if( $dataDirExtensao = $serviceDiarioExtensao->getRepository()->findOneByDiario($etapaDiario->getDiarioId()) ){
                $dataEnvioMax = $dataDirExtensao->getEtapadiarioExtensaoData();
            }
            $dataEnvioMax = $dataEnvioMax ? $dataEnvioMax->getTimestamp() : null;

            if( $dataEnvioMax && $dataEnvioMax < strtotime('now') ){
                throw new \Exception("Agendamento não pode ser efetuado, pois a etapa se encontra bloqueada, entre em contato com o suporte para mais informações!");
            }

            $etapaValor =  $configCurso['cursoconfig_nota_max']*( $etapaDiario->getEtapa()->getEtapaPercentagem()/100);
            $somaAgEtapa = $totalDiario['totalAgendamento'] + $agendamento->getAgendaNota();

            if( $etapaValor < $somaAgEtapa  ){
                //pegando apenas o restante
                $tmpValor = abs($etapaValor-$totalDiario['totalAgendamento']);
                $msg = "Agendamento atual não pode possuir valor superior à $tmpValor!";
                if(!$tmpValor){
                    $msg = "Etapa selecionada não possui pontos disponíveis para novos agendamentos!";
                }
                throw new \Exception($msg);
            }

            $dataFimAg = $agendamento->getAgendaDataFim()->getTimestamp();
            $dataInicioAg = $agendamento->getAgendaDataInicio()->getTimestamp();
            $dataFimEtapa = $etapaDiario->getEtapa()->getEtapaDataFim()->getTimestamp();
            $dataInicioEtapa = $etapaDiario->getEtapa()->getEtapaDataInicio()->getTimestamp();

            if( $dataInicioAg > $dataFimEtapa || $dataInicioAg < $dataInicioEtapa || $dataFimAg > $dataFimEtapa || $dataFimAg < $dataInicioEtapa ){
                throw new \Exception("Agendamento não pode estar com a data fora da etapa!");
            }
        }
        else if($configCurso['per_disc_avaliacao'] == "Valor Numérico" ){
            throw new \Exception("Valor da nota do agendamento incoerente com o método de avaliação da disciplina selecionada.");
        }
        //tratar a ideia do conceito na nota
        else{
            // 1-verificar se é conceito e não numérico
            // 2 - a ideia de conceito sempre pode ser A e a pontuação ser média
            //a busca tem que vim pelo menos a etapa para fazer comparação posteriormente com a data
        }


        return true;
    }


    public function getArray($params)
    {

        $sql = "SELECT *, DATE_FORMAT(ag.agenda_data_fim,'%d/%m/%Y') as agenda_data_fim_br
                    FROM acadperiodo__agendamento ag
                    INNER JOIN acadperiodo__agendamento_diario agd USING (agenda_id)
                    INNER JOIN acadperiodo__etapa_diario aed USING (diario_id)
                    LEFT OUTER JOIN acadperiodo__etapa_diario_extensao aede ON (aede.diario_id = aed.diario_id)
                    INNER JOIN acadperiodo__docente_disciplina apdd USING (docdisc_id)
                    INNER JOIN acadperiodo__etapas ae ON aed.etapa_id = ae.etapa_id
                    INNER JOIN acadperiodo__turma aturma ON aturma.turma_id = apdd.turma_id
                    INNER JOIN acadperiodo__letivo al ON aturma.per_id = al.per_id
                    INNER JOIN campus_curso cc ON cc.cursocampus_id = aturma.cursocampus_id
                    INNER JOIN acad_curso ac ON ac.curso_id = cc.curso_id
                    INNER JOIN acadperiodo__matriz_disciplina amd on (aturma.mat_cur_id = amd.mat_cur_id AND apdd.disc_id = amd.disc_id)
                    INNER JOIN acadgeral__disciplina ad ON ad.disc_id = amd.disc_id
                    WHERE apdd.docdisc_data_fim IS NULL";

        $parametros = [];

        if( $params['agendadiario_id'] ){
            $sql .= " AND agd.agendadiario_id = :agendadiario_id";

            $parametros['agendadiario_id'] = $params['agendadiario_id'];
        }

        return $this->executeQueryWithParam($sql, $parametros)->fetch();
    }



}