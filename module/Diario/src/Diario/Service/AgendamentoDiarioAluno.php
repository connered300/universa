<?php

namespace Diario\Service;

use VersaSpine\Service\AbstractService;

class AgendamentoDiarioAluno extends AbstractService
{

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Diario\Entity\AgendamentoDiarioAluno');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @param $params
     * @return array
     */
    public function pesquisaForJson($params)
    {

    }


    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array $arrDados)
    {

        $this->valida($arrDados);

        $serviceAgendamentoDiario = new \Diario\Service\AgendamentoDiario($this->getEm());
        $objAgDiario = $serviceAgendamentoDiario->getRepository()->findOneByAgendadiarioId($arrDados['agendadiarioId']);

        if( !$objAgDiario ){
            throw new \Exception("Diário não encontrado para o agendamento!");
        }

        $serviceAlunoDisciplina  = new \Matricula\Service\AcadperiodoAlunoDisciplina($this->getEm());
        $serviceAgendamentoAluno = new \Diario\Service\AgendamentoAluno($this->getEm());
        $serviceAcessoUserPessoa = new \Acesso\Service\AcessoPessoas($this->getEm());

        $userLogado = $serviceAcessoUserPessoa->retornaUsuarioLogado();
        if(!$userLogado){
            //throw new \Exception($serviceAcessoUserPessoa->getLastError());
            $userLogado = $serviceAcessoUserPessoa->getRepository()->findOneBy(['id'=>1]);
        }

        $this->begin();

        $objAgendamento = $objAgDiario->getAgenda();

        //deletando todas as relações dos agendamentos ao diario para poder adicionar posteriormente
        $qb = $this->getRepository()->createQueryBuilder("a");
        $qb->delete()->andWhere("a.agendadiario = :agendadiario_id")
           ->setParameter(":agendadiario_id", $objAgDiario->getAgendadiarioId())
           ->getQuery()
           ->getResult();

        $objAgDiarioAluno = new \Diario\Entity\AgendamentoDiarioAluno();
        $objAgDiarioAluno->setAgendadiario($objAgDiario);

        //não é necessário ter algum aluno, apenas não vinculo a atividade dele no diario
        if($arrDados['alunodisc']){
            foreach($arrDados['alunodisc'] as $alunodiscId=>$cadaAlunoDisc){

                $objAlunoDisc = $serviceAlunoDisciplina->getRepository()->findOneByAlunodiscId($alunodiscId);

                if( !$objAlunoDisc ){
                    throw new \Exception("Aluno não vinculado na disciplina!");
                }

                $objAgDiarioAluno->setAgendaaluno($serviceAgendamentoAluno->save($cadaAlunoDisc, $objAgendamento , $userLogado));
                $objAgDiarioAluno->setAlunodisc($objAlunoDisc);

                $this->getEm()->persist($objAgDiarioAluno);
            }
        }

        $this->getEm()->flush($objAlunoDisc);

        $this->commit();

        return true;
    }

    /**
     * @param $dados
     * @return array|true|void
     */
    public function valida($dados)
    {
        $errors = [];
        if( !$dados['agendadiarioId'] ){
            $errors[] = "Diário não encontrado para o agendamento!";
        }

        if( !isset($dados['alunodisc']) || !is_array($dados['alunodisc']) ){
            $errors[] = "Aluno(s) não informado(s) na disciplina do agendamento!";
        }

        if (!empty($errors)) {
            throw new \Exception(implode("<br>", $errors));
        }

        return true;
    }


    public function getDataForDatatables($data, $agendadiarioId)
    {
        $servicoSituacao = new \Matricula\Service\AcadgeralSituacao($this->getEm());
        $arrSituacaoRegular = implode("','",$servicoSituacao->situacoesAtividade());

        $query = "SELECT ag.*, aed.*, aede.etapadiario_extensao_data, 
                         DATE_FORMAT(aaa.agendaaluno_data_entrega,'%d/%m/%Y') as agendaaluno_data_entrega_br, 
                         p.*, aad.*, aaa.*, aa.alunocurso_id,  
                    IF(
                      aad.situacao_id in('$arrSituacaoRegular') AND
                      aa.matsituacao_id in('$arrSituacaoRegular'), 'Regular', 'Desligado'
                    ) as situacao_aluno
                    FROM acadperiodo__agendamento_diario agd
                    INNER JOIN acadperiodo__agendamento ag USING (agenda_id)
                    INNER JOIN acadperiodo__etapa_diario aed USING (diario_id)
                    LEFT OUTER JOIN acadperiodo__etapa_diario_extensao aede ON (aede.diario_id = aed.diario_id)
                    INNER JOIN acadperiodo__docente_disciplina apdd USING (docdisc_id)
                    INNER JOIN acadperiodo__aluno_disciplina aad ON (aad.turma_id = apdd.turma_id AND aad.disc_id = apdd.disc_id)
                    INNER JOIN acadperiodo__aluno aa ON (aa.alunoper_id = aad.alunoper_id)
                    INNER JOIN acadgeral__aluno_curso aac ON (aa.alunocurso_id = aac.alunocurso_id)
                    INNER JOIN acadgeral__aluno aga ON (aga.aluno_id = aac.aluno_id)
                    INNER JOIN pessoa p ON (aga.pes_id = p.pes_id)
                    
                    LEFT OUTER JOIN acadperiodo__agendamento_diario_aluno aada ON (agd.agendadiario_id = aada.agendadiario_id AND aada.alunodisc_id = aad.alunodisc_id)
                    LEFT OUTER JOIN acadperiodo__agendamento_aluno aaa ON (aaa.agendaaluno_id = aada.agendaaluno_id)
                    WHERE agd.agendadiario_id = '$agendadiarioId' AND apdd.docdisc_data_fim IS NULL
               ";
        return parent::paginationDataTablesAjax($query, $data, null, false);
    }


}