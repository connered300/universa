<?php

namespace Diario\Service;

use VersaSpine\Service\AbstractService;

class AgendamentoTipo extends AbstractService
{

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Diario\Entity\AgendamentoTipo');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @param $params
     * @return array
     */
    public function pesquisaForJson($params)
    {
        $sql = "SELECT * acadgeral__agenda_tipo";

        $parameters = [];
        if($params['agendatipo_nome'] ){
            $sql .= " WHERE agendatipo_nome = :agendatipo_nome";
            $parameters["agendatipo_nome"] = $params['agendatipo_nome'];
        }

        $sql .= " ORDER BY agendatipo_nome";

        return $this->executeQueryWithParam($sql, $parameters)->fetchAll();
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array $arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }
        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();


        if (!$arrParam['']) {
            $errors[] = 'Por favor preencha o campo ""!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }


    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrFind = [];

        if ($params['id']) {
            $arrFind = array('perId' => $params['id']);
        }
        $arrEntities = $this->getRepository()->findBy($arrFind, ["agendaNome"=>"ASC"]);

        $arrEntitiesArr = array();
        /* @var $objEntity \Diario\Entity\AgendamentoTipo */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getAgendaNome(),
                $params['value'] => $objEntity->getAgendaNome()
            );
        }

        return $arrEntitiesArr;
    }

}