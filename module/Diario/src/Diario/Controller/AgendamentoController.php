<?php

namespace Diario\Controller;

use VersaSpine\Controller\AbstractCoreController;

class AgendamentoController extends AbstractCoreController
{

    public function __construct()
    {
        parent::__construct(__CLASS__);
    }


    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAgendamento = new \Diario\Service\Agendamento($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceAgendamento->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function searchForJsonAction(){
        $request            = $this->getRequest();
        $paramsGet          = $request->getQuery()->toArray();
        $paramsPost         = $request->getPost()->toArray();
        $serviceAgendamento = new \Diario\Service\Agendamento($this->getEntityManager());

        $arrDados = array_merge($paramsGet, $paramsPost);

        if( isset($arrDados['buscarEtapa']) && $arrDados['perId'] ){
            $serviceEtapa = new \Professor\Service\AcadperiodoEtapas($this->getEntityManager());

            return new \Zend\View\Model\JsonModel($serviceEtapa->pesquisaForJson($arrDados));
        }

        $result = $serviceAgendamento->pesquisaForJson($arrDados);

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {

        $serviceAgendamentoTipo = new \Diario\Service\AgendamentoTipo($this->getEntityManager());
        $serviceAcadPeriodo     = new \Matricula\Service\AcadperiodoLetivo($this->getEntityManager());

        $view = $this->getView();

        $view->setTemplate("/diario/agendamento/add");

        return $view->setVariables([
            "periodosLetivos"  => $serviceAcadPeriodo->getArrSelect2(),
            "tiposAgendamento" => $serviceAgendamentoTipo->getArrSelect2()
        ]);
    }


    public function addAction($agendaId = false)
    {
        $serviceAgendamento = new \Diario\Service\Agendamento($this->getEntityManager());

        $request = $this->getRequest();

        $view = $this->getView();

        $view->setVariable("erro", false);
        if($request->isPost()){

            $dados = $request->getPost()->toArray();

            $success = true;
            $error = "";

            try{
                $serviceAgendamento->save($dados);

                $this->flashMessenger()->addSuccessMessage(
                    'Registro de agendamento salvo!'
                );

            } catch (\Exception $ex){
                $success = false;
                $error = $ex->getMessage();

                $view->setVariable("erro", true);
                $this->flashMessenger()->addErrorMessage($error);
            }

            if($request->isXmlHttpRequest()){
                $json = $this->getJson();

                $json->setVariables([
                    "success" => $success,
                    "error"   => ['mensagem'=>$error],
                ]);

                //se for json apaga as mensagens para ela não ir para a próxima tela quando fizer um
                //redirect
                $this->flashMessenger()->clearCurrentMessages();

                return $json;
            }

        }
        return $this->redirect()->toRoute("diario/default", ["controller"=>"agendamento", "action"=> "add"]);
    }

    public function editAction()
    {
        $request = $this->getRequest();

        $serviceAgendamento = new \Diario\Service\Agendamento($this->getEntityManager());

        if( !$request->isPost() ){

            $paramsGet = $request->getQuery()->toArray();

            if( $request->isXmlHttpRequest() ){

                $json   = $this->getJson();

                return $json->setVariable("result", $serviceAgendamento->getArray($paramsGet));
            }
        }

        return addAction();
    }


    public function removeAction()
    {
        $request = $this->getRequest();

        $serviceAgendamento = new \Diario\Service\Agendamento($this->getEntityManager());

        $agendaId = $this->params()->fromRoute("id");

        $success = true;
        $error = "";

        try{
            $serviceAgendamento->remover($agendaId);

            $this->flashMessenger()->addSuccessMessage(
                'Registro de agendamento removido!'
            );

        } catch ( \Exception $exception ){
            $success = false;
            $error = $exception->getMessage();

            $this->getView()->setVariable("erro", !$success);
            $this->flashMessenger()->addErrorMessage($error);
        }

        if($request->isXmlHttpRequest()){
            $json = $this->getJson();

            $json->setVariables([
                "success" => $success,
                "error"   => ['mensagem'=>$error],
            ]);

            //se for json apaga as mensagens para ela não ir para a próxima tela quando fizer um
            //redirect
            $this->flashMessenger()->clearCurrentMessages();

            return $json;
        }

        return $this->redirect()->toRoute("diario/default", ["controller"=>"agendamento", "action"=> "add"]);
    }


}