<?php

namespace Diario\Controller;

use VersaSpine\Controller\AbstractCoreController;

class AgendamentoDiarioAlunoController extends AbstractCoreController
{

    public function __construct()
    {
        parent::__construct(__CLASS__);
    }


    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAgendamentoDiarioAluno = new \Diario\Service\AgendamentoDiarioAluno($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceAgendamentoDiarioAluno->getDataForDatatables($dataPost, $dataPost['agendadiario_id']);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function addAction()
    {
        $serviceAgendamentoDiarioAluno = new \Diario\Service\AgendamentoDiarioAluno($this->getEntityManager());


        $request = $this->getRequest();
        $view = $this->getView();


        if($request->isPost()){

            $dados = $request->getPost()->toArray();

            $success = true;
            $error = "";

            try{
                $serviceAgendamentoDiarioAluno->save($dados);
                $this->flashMessenger()->addSuccessMessage(
                    'Registro de agendamento salvo!'
                );
            } catch (\Exception $ex){
                $success = false;
                $error   = $ex->getMessage();

                $this->flashMessenger()->addErrorMessage($error);
                $view->setVariable("erro", true);
            }

            if($request->isXmlHttpRequest()){
                $json = $this->getJson();

                $json->setVariables([
                    "success" => $success,
                    "error"   => ['mensagem'=>$error],
                ]);

                return $json;
            }

            return $this->redirect()->toRoute("diario/default", ["controller"=>"agendamento", "action"=> "add"]);
        }

        $serviceAgendamentoDiario = new \Diario\Service\AgendamentoDiario($this->getEntityManager());
        $agendaDiarioId = $this->params()->fromRoute("id");

        $agendamento = $serviceAgendamentoDiario->getArray(['agendadiario_id'=>$agendaDiarioId]);
        if( !$agendaDiarioId ){

            $this->flashMessenger()->addErrorMessage("Agendamento inexistente no diário ou não informado!");
            return $this->redirect()->toRoute("diario/default", ["controller"=>"agendamento", "action"=> "add"]);
        }

        $view->setVariables([
            "erro"        => false,
            "agendamento" => $agendamento,
        ]);

        return $view;
    }

}
