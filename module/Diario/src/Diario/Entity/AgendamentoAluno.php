<?php

namespace Diario\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AgendamentoAluno
 *
 * @ORM\Table(name="acadperiodo__agendamento_aluno", indexes={@ORM\Index(name="fk_acadperiodo__agendamento_aluno_acesso_pessoas1_idx", columns={"usuario_avaliador"})})
 * @ORM\Entity
 */
class AgendamentoAluno
{
    /**
     * @var int
     *
     * @ORM\Column(name="agendaaluno_id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $agendaalunoId;

    /**
     * @var float|null
     *
     * @ORM\Column(name="agendaaluno_nota", type="float", precision=10, scale=0, nullable=true)
     */
    private $agendaalunoNota;

    /**
     * @var string|null
     *
     * @ORM\Column(name="agendaaluno_conceito", type="string", length=45, nullable=true)
     */
    private $agendaalunoConceito;

    /**
     * @var int|null
     *
     * @ORM\Column(name="agendaaluno_carga_horaria", type="integer", nullable=true)
     */
    private $agendaalunoCargaHoraria;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="agendaaluno_data_entrega", type="datetime", nullable=true)
     */
    private $agendaalunoDataEntrega;

    /**
     * @var string|null
     *
     * @ORM\Column(name="agendaaluno_origem_registro", type="string", nullable=true, options={"default"="Sistema"})
     */
    private $agendaalunoOrigemRegistro = 'Sistema';

    /**
     * @var string|null
     *
     * @ORM\Column(name="agendaaluno_estado", type="string", nullable=true, options={"default"="Pendente"})
     */
    private $agendaalunoEstado = 'Pendente';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="agendaaluno_data_avaliacao", type="datetime", nullable=true)
     */
    private $agendaalunoDataAvaliacao;

    /**
     * @var string|null
     *
     * @ORM\Column(name="agendaaluno_observacao", type="string", length=250, nullable=true)
     */
    private $agendaalunoObservacao;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     *
     * @ORM\ManyToOne(targetEntity="\Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_avaliador", referencedColumnName="id")
     * })
     */
    private $usuarioAvaliador;

    /**
     * @return int
     */
    public function getAgendaalunoId()
    {
        return $this->agendaalunoId;
    }

    /**
     * @param int $agendaalunoId
     */
    public function setAgendaalunoId($agendaalunoId)
    {
        $this->agendaalunoId = $agendaalunoId;
    }

    /**
     * @return float|null
     */
    public function getAgendaalunoNota()
    {
        return $this->agendaalunoNota;
    }

    /**
     * @param float|null $agendaalunoNota
     */
    public function setAgendaalunoNota($agendaalunoNota)
    {
        $this->agendaalunoNota = $agendaalunoNota;
    }

    /**
     * @return null|string
     */
    public function getAgendaalunoConceito()
    {
        return $this->agendaalunoConceito;
    }

    /**
     * @param null|string $agendaalunoConceito
     */
    public function setAgendaalunoConceito($agendaalunoConceito)
    {
        $this->agendaalunoConceito = $agendaalunoConceito;
    }

    /**
     * @return int|null
     */
    public function getAgendaalunoCargaHoraria()
    {
        return $this->agendaalunoCargaHoraria;
    }

    /**
     * @param int|null $agendaalunoCargaHoraria
     */
    public function setAgendaalunoCargaHoraria($agendaalunoCargaHoraria)
    {
        $this->agendaalunoCargaHoraria = $agendaalunoCargaHoraria;
    }

    /**
     * @return DateTime|null
     */
    public function getAgendaalunoDataEntrega($format = false)
    {
        $agendaalunoDataEntrega = $this->agendaalunoDataEntrega;

        if ($format && $agendaalunoDataEntrega) {
            $agendaalunoDataEntrega = $agendaalunoDataEntrega->format('d/m/Y H:i:s');
        }

        return $agendaalunoDataEntrega;
    }

    /**
     * @param DateTime|null $agendaalunoDataEntrega
     */
    public function setAgendaalunoDataEntrega($agendaalunoDataEntrega)
    {
        if ($agendaalunoDataEntrega && is_string($agendaalunoDataEntrega)) {
            $agendaalunoDataEntrega = \VersaSpine\Service\AbstractService::formatDateAmericano(
                $agendaalunoDataEntrega
            );
            $agendaalunoDataEntrega = new \Datetime($agendaalunoDataEntrega);
        }
        $this->agendaalunoDataEntrega = $agendaalunoDataEntrega;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getAgendaalunoOrigemRegistro()
    {
        return $this->agendaalunoOrigemRegistro;
    }

    /**
     * @param null|string $agendaalunoOrigemRegistro
     */
    public function setAgendaalunoOrigemRegistro($agendaalunoOrigemRegistro)
    {
        $this->agendaalunoOrigemRegistro = $agendaalunoOrigemRegistro;
    }

    /**
     * @return null|string
     */
    public function getAgendaalunoEstado()
    {
        return $this->agendaalunoEstado;
    }

    /**
     * @param null|string $agendaalunoEstado
     */
    public function setAgendaalunoEstado($agendaalunoEstado)
    {
        $this->agendaalunoEstado = $agendaalunoEstado;
    }

    /**
     * @return DateTime|null
     */
    public function getAgendaalunoDataAvaliacao($format = false)
    {
        $agendaalunoDataAvaliacao = $this->agendaalunoDataAvaliacao;

        if ($format && $agendaalunoDataAvaliacao) {
            $agendaalunoDataAvaliacao = $agendaalunoDataAvaliacao->format('d/m/Y H:i:s');
        }

        return $agendaalunoDataAvaliacao;
    }

    /**
     * @param DateTime|null $agendaalunoDataAvaliacao
     */
    public function setAgendaalunoDataAvaliacao($agendaalunoDataAvaliacao)
    {
        if ($agendaalunoDataAvaliacao && is_string($agendaalunoDataAvaliacao)) {
            $agendaalunoDataAvaliacao = \VersaSpine\Service\AbstractService::formatDateAmericano(
                $agendaalunoDataAvaliacao
            );
            $agendaalunoDataAvaliacao = new \Datetime($agendaalunoDataAvaliacao);
        }
        $this->agendaalunoDataAvaliacao = $agendaalunoDataAvaliacao;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getAgendaalunoObservacao()
    {
        return $this->agendaalunoObservacao;
    }

    /**
     * @param null|string $agendaalunoObservacao
     */
    public function setAgendaalunoObservacao($agendaalunoObservacao)
    {
        $this->agendaalunoObservacao = $agendaalunoObservacao;
    }

    /**
     * @return AcessoPessoas
     */
    public function getUsuarioAvaliador()
    {
        return $this->usuarioAvaliador;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuarioAvaliador
     */
    public function setUsuarioAvaliador($usuarioAvaliador)
    {
        $this->usuarioAvaliador = $usuarioAvaliador;
    }

}
