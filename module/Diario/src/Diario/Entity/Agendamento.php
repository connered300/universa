<?php

namespace Diario\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;
/**
 * Agendamento
 *
 * @ORM\Table(name="acadperiodo__agendamento", indexes={@ORM\Index(name="fk_acadperiodo__agendamento_acadgeral__agendamento_tipo1_idx", columns={"agenda_nome"})})
 * @ORM\Entity
 * @Jarvis\Jarvis(title="Listagem de agendamento",icon="fa fa-table")
 */
class Agendamento
{
    /**
     * @var int
     *
     * @ORM\Column(name="agenda_id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="agenda_id")
     * @LG\Labels\Attributes(text="código agendamento")
     * @LG\Querys\Conditions(type="=")
     */
    private $agendaId;

    /**
     * @var string
     *
     * @ORM\Column(name="agenda_titulo", type="string", length=45, nullable=false, options={})
     * @LG\Labels\Property(name="agenda_titulo")
     * @LG\Labels\Attributes(text="titulo do agendamento")
     * @LG\Querys\Conditions(type="=")
     */
    private $agendaTitulo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="agenda_descricao", type="text", length=65535, nullable=true, options={"comment"="Descrição detalhada da atividade."})
     * @LG\Labels\Property(name="agenda_descricao")
     * @LG\Labels\Attributes(text="descricao do agendamento")
     * @LG\Querys\Conditions(type="=")
     */
    private $agendaDescricao;

    /**
     * @var float|null
     *
     * @ORM\Column(name="agenda_nota", type="float", precision=10, scale=0, nullable=true, options={"comment"="Valor total da atividade."})
     * @LG\Labels\Property(name="agenda_nota")
     * @LG\Labels\Attributes(text="valor da nota do agendamento")
     * @LG\Querys\Conditions(type="=")
     */
    private $agendaNota;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="agenda_data_inicio", type="datetime", nullable=false, options={"comment"="Publicação da atividade."})
     * @LG\Labels\Property(name="agenda_data_inicio")
     * @LG\Labels\Attributes(text="data inicio do agendamento")
     * @LG\Querys\Conditions(type="=")
     */
    private $agendaDataInicio ;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="agenda_data_fim", type="datetime", nullable=false, options={"comment"="Data máxima de aceitabilidade da atividade."})
     * @LG\Labels\Property(name="agenda_data_fim")
     * @LG\Labels\Attributes(text="data fim do agendamento")
     * @LG\Querys\Conditions(type="=")
     */
    private $agendaDataFim;

    /**
     * @var string
     *
     * @ORM\Column(name="agenda_avaliativa", type="string", nullable=false)
     * @LG\Labels\Property(name="agenda_avaliativa")
     * @LG\Labels\Attributes(text="agendamento avaliativo")
     * @LG\Querys\Conditions(type="=")
     */
    private $agendaAvaliativa;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="agenda_data_cadastro", type="datetime", nullable=true)
     * @LG\Labels\Property(name="agenda_data_cadastro")
     * @LG\Labels\Attributes(text="data de cadastro do agendamento")
     * @LG\Querys\Conditions(type="=")
     */
    private $agendaDataCadastro;

    /**
     * @var string
     *
     * @ORM\Column(name="agenda_entrega_tipo", type="string", nullable=false)
     * @LG\Labels\Property(name="agenda_entrega_tipo")
     * @LG\Labels\Attributes(text="tipo da entrega do agendamento")
     * @LG\Querys\Conditions(type="=")
     */
    private $agendaEntregaTipo;

    /**
     * @var string
     *
     * @ORM\Column(name="agenda_anexos", type="string", nullable=false)
     * @LG\Labels\Property(name="agenda_anexos")
     * @LG\Labels\Attributes(text="agendamento contém anexo")
     * @LG\Querys\Conditions(type="=")
     */
    private $agendaAnexos;

    /**
     * @var \AgendamentoTipo
     *
     * @ORM\ManyToOne(targetEntity="AgendamentoTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="agenda_nome", referencedColumnName="agenda_nome")
     * })
     */
    private $agendaNome;

    public function __construct()
    {
        $this->agendaDataInicio = new \DateTime("now");
    }

    /**
     * @return int
     */
    public function getAgendaId()
    {
        return $this->agendaId;
    }

    /**
     * @param int $agendaId
     */
    public function setAgendaId($agendaId)
    {
        $this->agendaId = $agendaId;
    }

    /**
     * @return string
     */
    public function getAgendaTitulo()
    {
        return $this->agendaTitulo;
    }

    /**
     * @param string $agendaTitulo
     */
    public function setAgendaTitulo($agendaTitulo)
    {
        $this->agendaTitulo = $agendaTitulo;
    }

    /**
     * @return null|string
     */
    public function getAgendaDescricao()
    {
        return $this->agendaDescricao;
    }

    /**
     * @param null|string $agendaDescricao
     */
    public function setAgendaDescricao($agendaDescricao)
    {
        $this->agendaDescricao = $agendaDescricao;
    }

    /**
     * @return float|null
     */
    public function getAgendaNota()
    {
        return $this->agendaNota;
    }

    /**
     * @param float|null $agendaNota
     */
    public function setAgendaNota($agendaNota)
    {
        $this->agendaNota = $agendaNota;
    }

    /**
     * @return \DateTime
     */
    public function getAgendaDataInicio($format = false)
    {
        $agendaDataInicio = $this->agendaDataInicio;

        if ($format && $agendaDataInicio) {
            $agendaDataInicio = $agendaDataInicio->format('d/m/Y H:i:s');
        }

        return $agendaDataInicio;
    }

    /**
     * @param \DateTime $agendaDataInicio
     */
    public function setAgendaDataInicio($agendaDataInicio)
    {
        if ($agendaDataInicio) {
            if (is_string($agendaDataInicio)) {
                $agendaDataInicio = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $agendaDataInicio
                );
                $agendaDataInicio = new \Datetime($agendaDataInicio);
            }
        } else {
            $agendaDataInicio = null;
        }
        $this->agendaDataInicio = $agendaDataInicio;

        return $this;
    }

    /**
     * @return \DateTime $agendaDataFim
     */
    public function getAgendaDataFim($format = false)
    {
        $agendaDataFim = $this->agendaDataFim;

        if ($format && $agendaDataFim) {
            $agendaDataFim = $agendaDataFim->format('d/m/Y H:i:s');
        }

        return $agendaDataFim;
    }

    /**
     * @param \DateTime $agendaDataFim
     */
    public function setAgendaDataFim($agendaDataFim)
    {
        if ($agendaDataFim) {
            if (is_string($agendaDataFim)) {
                $agendaDataFim = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $agendaDataFim
                );
                $agendaDataFim = new \Datetime($agendaDataFim);
            }
        } else {
            $agendaDataFim = null;
        }
        $this->agendaDataFim = $agendaDataFim;

        return $this;
    }

    /**
     * @return string
     */
    public function getAgendaAvaliativa()
    {
        return $this->agendaAvaliativa;
    }

    /**
     * @param string $agendaAvaliativa
     */
    public function setAgendaAvaliativa($agendaAvaliativa)
    {
        $this->agendaAvaliativa = $agendaAvaliativa;
    }

    /**
     * @return \DateTime|null
     */
    public function getAgendaDataCadastro($format = false)
    {
        $agendaDataCadastro = $this->agendaDataCadastro;

        if ($format && $agendaDataCadastro) {
            $agendaDataCadastro = $agendaDataCadastro->format('d/m/Y H:i:s');
        }

        return $agendaDataCadastro;
    }

    /**
     * @param \DateTime|null $agendaDataCadastro
     */
    public function setAgendaDataCadastro($agendaDataCadastro)
    {
        if ($agendaDataCadastro) {
            if (is_string($agendaDataCadastro)) {
                $agendaDataCadastro = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $agendaDataCadastro
                );
                $agendaDataCadastro = new \Datetime($agendaDataCadastro);
            }
        } else {
            $agendaDataCadastro = null;
        }
        $this->agendaDataCadastro = $agendaDataCadastro;

        return $this;
    }

    /**
     * @return string
     */
    public function getAgendaEntregaTipo()
    {
        return $this->agendaEntregaTipo;
    }

    /**
     * @param string $agendaEntregaTipo
     */
    public function setAgendaEntregaTipo($agendaEntregaTipo)
    {
        $this->agendaEntregaTipo = $agendaEntregaTipo;
    }

    /**
     * @return string
     */
    public function getAgendaAnexos()
    {
        return $this->agendaAnexos;
    }

    /**
     * @param string $agendaAnexos
     */
    public function setAgendaAnexos($agendaAnexos)
    {
        $this->agendaAnexos = $agendaAnexos;
    }

    /**
     * @return \AgendamentoTipo
     */
    public function getAgendaNome()
    {
        return $this->agendaNome;
    }

    /**
     * @param \AgendamentoTipo $agendaNome
     */
    public function setAgendaNome($agendaNome)
    {
        $this->agendaNome = $agendaNome;
    }



}
