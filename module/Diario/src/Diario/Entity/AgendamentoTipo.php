<?php

namespace Diario\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AgendamentoTipo
 *
 * @ORM\Table(name="acadgeral__agendamento_tipo")
 * @ORM\Entity
 */
class AgendamentoTipo
{
    /**
     * @var string
     *
     * @ORM\Column(name="agenda_nome", type="string", length=45, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $agendaNome;

    /**
     * @var string
     *
     * @ORM\Column(name="agenda_descricao", type="string", length=255, nullable=false)
     */
    private $agendaDescricao;

    /**
     * @return string
     */
    public function getAgendaNome()
    {
        return $this->agendaNome;
    }

    /**
     * @param string $agendaNome
     */
    public function setAgendaNome($agendaNome)
    {
        $this->agendaNome = $agendaNome;
    }

    /**
     * @return string
     */
    public function getAgendaDescricao()
    {
        return $this->agendaDescricao;
    }

    /**
     * @param string $agendaDescricao
     */
    public function setAgendaDescricao($agendaDescricao)
    {
        $this->agendaDescricao = $agendaDescricao;
    }

}
