<?php

namespace Diario\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoAgendamentoDiario
 *
 * @ORM\Table(name="acadperiodo__agendamento_diario", uniqueConstraints={@ORM\UniqueConstraint(name="agenda_id_UNIQUE", columns={"agenda_id"}), @ORM\UniqueConstraint(name="agendadocente_id_UNIQUE", columns={"agendadiario_id"})}, indexes={@ORM\Index(name="fk_acadperiodo__docente_disciplina_acadperiodo__agendamento__idx", columns={"agenda_id"}), @ORM\Index(name="fk_acadperiodo__agend_docente_etapa_diario_idx", columns={"diario_id"})})
 * @ORM\Entity
 */
class AgendamentoDiario
{
    /**
     * @var int
     *
     * @ORM\Column(name="agendadiario_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $agendadiarioId;

    /**
     * @var \AcadperiodoEtapaDiario
     *
     * @ORM\ManyToOne(targetEntity="Professor\Entity\AcadperiodoEtapaDiario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="diario_id", referencedColumnName="diario_id")
     * })
     */
    private $diario;

    /**
     * @var \Agendamento
     *
     * @ORM\ManyToOne(targetEntity="Agendamento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="agenda_id", referencedColumnName="agenda_id")
     * })
     */
    private $agenda;

    /**
     * @return int
     */
    public function getAgendadiarioId()
    {
        return $this->agendadiarioId;
    }

    /**
     * @param int $agendadiarioId
     */
    public function setAgendadiarioId($agendadiarioId)
    {
        $this->agendadiarioId = $agendadiarioId;
    }

    /**
     * @return \AcadperiodoEtapaDiario
     */
    public function getDiario()
    {
        return $this->diario;
    }

    /**
     * @param \AcadperiodoEtapaDiario $diario
     */
    public function setDiario($diario)
    {
        $this->diario = $diario;
    }

    /**
     * @return \Agendamento
     */
    public function getAgenda()
    {
        return $this->agenda;
    }

    /**
     * @param \Agendamento $agenda
     */
    public function setAgenda($agenda)
    {
        $this->agenda = $agenda;
    }

}
