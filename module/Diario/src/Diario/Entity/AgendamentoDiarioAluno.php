<?php

namespace Diario\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AgendamentoDiarioAluno
 *
 * @ORM\Table(name="acadperiodo__agendamento_diario_aluno", indexes={@ORM\Index(name="fk_acadperiodo__agendamento_diario_acadperiodo__agen_alu_idx", columns={"agendaaluno_id"}), @ORM\Index(name="fk_acadperiodo__agendamento_diario_acadperiodo__agen_alu_idx1", columns={"agendadiario_id"}), @ORM\Index(name="fk_acadperiodo__agendamento_diario_aluno_acadper__aluno_d_idx", columns={"alunodisc_id"})})
 * @ORM\Entity
 */
class AgendamentoDiarioAluno
{
    /**
     * @var \AgendamentoDiario
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AgendamentoDiario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="agendadiario_id", referencedColumnName="agendadiario_id")
     * })
     */
    private $agendadiario;

    /**
     * @var \AgendamentoAluno
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AgendamentoAluno")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="agendaaluno_id", referencedColumnName="agendaaluno_id")
     * })
     */
    private $agendaaluno;

    /**
     * @var \Matricula\Entity\AcadperiodoAlunoDisciplina
     *
     * @ORM\ManyToOne(targetEntity="\Matricula\Entity\AcadperiodoAlunoDisciplina")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="alunodisc_id", referencedColumnName="alunodisc_id")
     * })
     */
    private $alunodisc;

    /**
     * @return \AgendamentoDiario
     */
    public function getAgendadiario()
    {
        return $this->agendadiario;
    }

    /**
     * @param \AgendamentoDiario $agendadiario
     */
    public function setAgendadiario($agendadiario)
    {
        $this->agendadiario = $agendadiario;
    }

    /**
     * @return \AgendamentoAluno
     */
    public function getAgendaaluno()
    {
        return $this->agendaaluno;
    }

    /**
     * @param \AgendamentoAluno $agendaaluno
     */
    public function setAgendaaluno($agendaaluno)
    {
        $this->agendaaluno = $agendaaluno;
    }

    /**
     * @return \AcadperiodoAlunoDisciplina
     */
    public function getAlunodisc()
    {
        return $this->alunodisc;
    }

    /**
     * @param \AcadperiodoAlunoDisciplina $alunodisc
     */
    public function setAlunodisc($alunodisc)
    {
        $this->alunodisc = $alunodisc;
    }

}
