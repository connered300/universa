<?php

namespace Boleto;

return array(
    'router'                    => array(
        'routes' => array(
            'boleto-bancario' => array(
                'type'          => 'Literal',
                'options'       => array(
                    'route'    => '/boleto-bancario',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Boleto\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes'  => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'       => '/[:controller[/:action[/:id][/:id2][/:id3][/:id4]]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults'    => array(),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager'           => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases'            => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'doctrine'                  => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default'             => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            ),
        ),
    ),
    'controllers'               => array(
        'invokables' => array(
            'Boleto\Controller\Boleto'               => 'Boleto\Controller\BoletoController',
            'Boleto\Controller\BoletoConfConta'      => 'Boleto\Controller\BoletoConfContaController',
            'Boleto\Controller\BoletoTipo'           => 'Boleto\Controller\BoletoTipoController',
            'Boleto\Controller\FinanceiroTitulo'     => 'Boleto\Controller\FinanceiroTituloController',
            'Boleto\Controller\FinanceiroDesconto'   => 'Boleto\Controller\FinanceiroDescontoController',
            'Boleto\Controller\Financeiro'           => 'Boleto\Controller\FinanceiroController',
            'Boleto\Controller\FinanceiroRelatorios' => 'Boleto\Controller\FinanceiroRelatoriosController',
            'Boleto\Controller\PagamentoLote'        => 'Boleto\Controller\PagamentoLoteController',
        ),
    ),
    'view_manager'              => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console'                   => array(
        'router' => array(
            'routes' => array(),
        ),
    ),
    'funcionalidades_liberadas' => array(
        'actions' => array(
            'busca-cnpj-cpf',
            'exibe',
            'carne',
            'comprovante-pagamento-mensalidade',
            'relatorio',
            'busca-dados-desconto-tipo',
            'insere-desconto-manual',
            'prototipo',
            'prototipo2',
            'listagem-alunos',
            //            'dados-aluno',
            'quita-mensalidade',
            'dados-titulo',
            'cria-taxa',
            'quita-taxa',
            'comprovante-pagamento',
            'dados-taxa',
            'financeiro-relatorios',
            'taxas',
            'mensalidades',
            'busca-turmas',
            'descontos',
            'quita-vestibular',
            'dados-cheque',
            'dados-cheque-euristica',
            'salva-observacoes',
            'listagem-taxas',
            'cheques',
            'exibe-banco-brasil',
            'exibe-multiplos-boletos'
        ),
    ),
    'namesDictionary'           => array(
        'Boleto\Boleto::exibe'                   => 'Exibir Boleto',
        'Boleto\Boleto::exibe-multiplos-boletos' => 'Exibir Boleto'
    )
);
