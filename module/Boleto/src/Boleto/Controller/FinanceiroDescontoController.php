<?php


namespace Boleto\Controller;

use VersaSpine\Controller\AbstractCoreController;

class FinanceiroDescontoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {
    }

    public function indexAction()
    {
        $service = $this->services()->getService($this->getService());

        $dados = $service->resultSearch(null, false, $_GET);

        if (!empty($dados['aluno'])) {
            if ($dados['aluno']->rowCount() > 0) {
                $alunoPerIdUltimo = $service->buscaAlunoUltmimoPeriodoLetivo($_GET['alunoper']);
                $serviceAcadperiodoAluno    = new \Matricula\Service\AcadperiodoAluno($this->getEntityManager());
                $objAcadperiodoAluno = $serviceAcadperiodoAluno->getRepository()->findOneBy(['alunoperId'=>$alunoPerIdUltimo]);

                $this->redirect()->toRoute(
                    $this->getRoute(),
                    array(
                        'controller' => $this->getController(),
                        'action'     => 'add',
                        'id'         => $objAcadperiodoAluno->getAlunocurso()->getAlunocursoId()
                    )
                );
            }
        }

        $viewHelperManager = $this->getServiceLocator()->get("viewhelpermanager");
        $viewUrl           = $viewHelperManager->get("url");
        /**
         * Atribui as variaveis que vao ser utilizadas pelo list generator
         */
        $this->config['table']['dados']      = $dados['dados'];
        $this->config['pagination']['count'] = $dados['count'];
        $this->config['table']['table_key']  = $this->services()->getService($this->getService())->getTablePkey();
        /**
         * Cria as variaveis para a view
         */
        $this->view->setVariable("dados", $dados['dados']);
        $this->view->setVariable("count", $dados['count']);
        $this->view->setVariable("config", $this->config);
        $this->view->setTemplate($this->getTemplateToRoute());

        return $this->view;
    }

    public function addAction()
    {
        $alunocursoId = $this->params()->fromRoute('id');
        $request    = $this->getRequest();
        $form       = $this->forms()->mountForm($this->getForm());
        $service    = new \Boleto\Service\FinanceiroDesconto($this->getEntityManager());

        $serviceAcadperiodoAluno = new \Matricula\Service\AcadperiodoAluno($this->getEntityManager());

        $alunoPerIdUltimo = $service->buscaAlunoUltmimoPeriodoLetivo($alunocursoId);

        $objAcadperiodoAluno = $serviceAcadperiodoAluno->getRepository()->findOneBy(['alunoperId'=>$alunoPerIdUltimo]);

        if ($serviceAcadperiodoAluno->verificaSeAlunoEstaCancelado($objAcadperiodoAluno)) {
            $this->flashMessenger()->addErrorMessage(
                "Não é possível vincular desconto para aluno em situação de desligamento!"
            );

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        if ($request->isPost()) {
            try {
                $dados            = array_merge($request->getPost()->toArray(), $request->getFiles()->toArray());
                $dados['alunoper'] = $objAcadperiodoAluno->getAlunoperId();
                $dados['usuario'] = $_SESSION['Zend_Auth']['storage']['id'];
                $ret              = $service->adicionar($dados);
                $this->flashMessenger()->addSuccessMessage("Descontro cadastrado com sucesso!");
                if (is_null($this->getRoute())) {
                    $this->setRoute($this->getEvent()->getRouteMatch()->getMatchedRouteName());
                }
            } catch (\Exception $ex) {
                $erro = $ex->getMessage();
                $form->setData($request->getPost()->toArray());
            }
            $is_json = $request->getPost("is_json");
            if (isset($erro)) {
                if ($is_json) {
                    $this->json->setVariable("erro", $erro);

                    return $this->json;
                }
                $this->flashMessenger()->addErrorMessage($erro);
            } else {
                if ($is_json) {
                    if (method_exists($ret, "toArray")) {
                        $dados = $ret->toArray();
                    } else {
                        $dados = "Os dados foram inseridos corretamente, mas um erro ocorreu, verifique se existe o método toArray na entidade {$this->getEntity()}!";
                    }
                    $this->json->setVariable("result", $dados);

                    return $this->json;
                }
            }
            if ($dados['voltaFinanceiro']) {
                return $this->redirect()->toRoute(
                    $this->getRoute(),
                    array('controller' => 'financeiro', 'action' => 'dados-aluno', 'id' => $dados['alunoper'])
                );
            }

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        } else {
            $param  =$alunoPerIdUltimo;
            $param2 = $this->params()->fromRoute('id2');

            if ($param2 == 'financeiro') {
                $this->view->setVariable("acadperiodoAluno", $param);
                $this->view->setVariable("voltaFinanceiro", true);
            } else {
                $this->view->setVariable("voltaFinanceiro", false);
            }

            if (empty($objAcadperiodoAluno)) {
                $this->flashMessenger()->addErrorMessage("Aluno não encontrado!");

                return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
            }
            $perLetivo = $objAcadperiodoAluno->getTurma()->getPer()->getPerId();
            $form->get('nome')->setValue($objAcadperiodoAluno->toArray()['pesNome']);
            $form->get('alunoper')->setValue($alunoPerIdUltimo);
            $valorMessalidade       = $this->services()->getService(
                'Boleto\Service\FinanceiroValores'
            )->buscaValorMensalidadeAno($perLetivo);
            $financeiroMensalidades = $this->services()->getService(
                'Boleto\Service\FinanceiroTituloMensalidade'
            )->siatuacaoMensalidades($param);
            $this->view->setVariable("financeiroMensalidades", $financeiroMensalidades);

            $form->get('valorMensalidade')->setValue($valorMessalidade);
        }

        $this->view->setVariable("form", $form);
        $this->view->setTemplate($this->getTemplateToRoute());

        return $this->view;
    }

    public function editAction()
    {
        $request = $this->getRequest();
        $form    = $this->forms()->mountForm($this->getForm());
        $service = $this->services()->getService($this->getService());

        if ($request->isPost()) {
            try {
                $a                = $request->getPost()->toArray();
                $dados            = array_merge($request->getPost()->toArray(), $request->getFiles()->toArray());
                $dados['usuario'] = $_SESSION['Zend_Auth']['storage']['id'];
                $ret              = $service->edita($dados);

                $this->flashMessenger()->addSuccessMessage("Desconto alterado com sucesso!");

                if (is_null($this->getRoute())) {
                    $this->setRoute($this->getEvent()->getRouteMatch()->getMatchedRouteName());
                }
            } catch (\Exception $ex) {
                $erro = $ex->getMessage();
                $form->setData($request->getPost()->toArray());
            }

            $is_json = $request->getPost("is_json");
            if (isset($erro)) {
                if ($is_json) {
                    $this->json->setVariable("erro", $erro);

                    return $this->json;
                }
                $this->flashMessenger()->addErrorMessage($erro);
            } else {
                if ($is_json) {
                    if (method_exists($ret, "toArray")) {
                        $dados = $ret->toArray();
                    } else {
                        $dados = "Os dados foram alterados corretamente, mas um erro ocorreu, verifique se existe o método toArray na entidade {$this->getEntity()}!";
                    }
                    $this->json->setVariable("result", $dados);

                    return $this->json;
                }
            }
            if ($dados['voltaFinanceiro']) {
                return $this->redirect()->toRoute(
                    $this->getRoute(),
                    array('controller' => 'financeiro', 'action' => 'dados-aluno', 'id' => $dados['alunoper'])
                );
            }

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        $id = $this->params()->fromRoute("id", 0);
        if (!$id) {
            throw new \Exception(
                "Para editar um registro é necessário informar o identificador do mesmo pela rota!", 1
            );
        }

        $param2 = $this->params()->fromRoute('id2');

        if ($param2 == 'financeiro') {
            $this->view->setVariable("acadperiodoAluno", $id);
            $this->view->setVariable("voltaFinanceiro", true);
        } else {
            $this->view->setVariable("voltaFinanceiro", false);
        }

        $reference = $service->getReference($id);

        if ($param2 == 'financeiro') {
            $this->view->setVariable("acadperiodoAluno", $reference->getAlunoper()->getAlunoperId());
            $this->view->setVariable("voltaFinanceiro", true);
        } else {
            $this->view->setVariable("voltaFinanceiro", false);
        }

        $perLetivo                    = $reference->getAlunoper()->getTurma()->getPer()->getPerId();
        $desconto                     = $reference->toArray();
        $desconto                     = array_merge(
            $desconto,
            $this->services()->getService('Boleto\Service\FinanceiroDescontoTipo')->buscaDadosTipo(
                $desconto['desctipo']
            )
        );
        $desconto['valorMensalidade'] = $valorMessalidade = $this->services()->getService(
            'Boleto\Service\FinanceiroValores'
        )->buscaValorMensalidadeAno($perLetivo);
        $desconto['desctipo']         = $desconto['desctipo']->getDesctipoId();
        $form->setData($desconto);
        $mensalidades = explode(",", $desconto['descontoMensalidades']);
        for ($i = 1; $i <= 6; $i++) {
            foreach ($mensalidades as $mensalidade) {
                if ($mensalidade == $i) {
                    $array[$i] = $mensalidade;
                    break;
                } else {
                    $array[$i] = "";
                }
            }
        }
        $financeiroMensalidades = $this->services()->getService(
            'Boleto\Service\FinanceiroTituloMensalidade'
        )->siatuacaoMensalidades($desconto['alunoper']);

        $this->view->setVariable("financeiroMensalidades", $financeiroMensalidades);
        $this->view->setVariable('mensalidades', $array);
        $this->view->setVariable('parcelas', $desconto['descontoMensalidades']);
        $this->view->setVariable("form", $form);
        $this->view->setTemplate($this->getTemplateToRoute());

        return $this->view;
    }

    public function removeAction()
    {
        $service = $this->services()->getService($this->getService());
        $id      = $this->params()->fromRoute('id', 0);
        $is_json = $this->getRequest()->getPost("is_json");
        if (!$id) {
            $id = $this->getRequest()->getPost("id");
            if (!$id) {
                if ($is_json) {
                    $this->json->setVariable(
                        "erro",
                        "Para excluir o registro envie o seu valor com a chave id, atraves desse campo o mesmo ira tentar excluir o registro!"
                    );

                    return $this->json;
                }
                throw new \Exception("Deve ser indicado pela rota o id do registro a ser excluído", 0);
            }
        }

        try {
            if (is_array($id)) {
                foreach ($id as $value) {
                    $service->cancelar($value);
                    $service->inativarDesconto($value);
                }
            } else {
                $service->cancelar($id);
                $service->inativarDesconto($id);
            }
            $this->flashMessenger()->addSuccessMessage("Desconto cancelado com sucesso!");
        } catch (\Exception $ex) {
            $xml  = new \Zend\Config\Reader\Xml();
            $erro = $xml->fromString($ex->getMessage());
        }

        if ($is_json) {
            if ($erro) {
                $this->json->setVariable("erro", $erro);
            } else {
                $this->json->setVariable("result", "Registro excluído com sucesso!");
            }

            return $this->json;
        } else {
            $this->flashMessenger()->addErrorMessage($erro);
        }

        return $this->redirect()->toRoute(
            $this->getRoute(),
            array('controller' => $this->getController(), 'action' => 'index')
        );
    }

    public function buscaDadosDescontoTipoAction()
    {
        $request = $this->getRequest();
        $service = $this->services()->getService('Boleto\Service\FinanceiroDescontoTipo');
        $result  = false;
        if ($request->isPost()) {
            $dados  = $request->getPost()->toArray();
            $result = $service->buscaDadosTipo($dados['id']);
        }
        $this->json->setVariable("result", $result);

        return $this->json;
    }

    public function showFormAction()
    {
        $this->view->setTerminal(true);
        $form = $this->getForm();
        if (!class_exists($form)) {
            $this->view->setVariable("erro", "Formulário de busca não foi encontrado!");

            return $this->view;
        }
        $form = new $form;
        $form->remove('desctipo')
            ->remove('desctipoValormax')
            ->remove('desctipoPercmax')
            ->remove('descontoValor')
            ->remove('descontoPercentual')
            ->remove('valorMensalidade')
            ->remove('descontoDiaLimite')
            ->remove('nome');

        $element = $form->get('alunoper');
        $element->setAttribute('readonly', false);
        $form->add($element);

        $form->setAttribute("method", 'get');
        $this->view->setVariable("config", $this->config);
        $this->view->setVariable("form", $form);

        return $this->view;
    }
}