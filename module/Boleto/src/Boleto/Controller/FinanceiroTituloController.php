<?php


namespace Boleto\Controller;

use VersaSpine\Controller\AbstractCoreController;
use Zend\View\Model\ViewModel;

class FinanceiroTituloController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {
    }

    public function indexAction()
    {
        return $this->view;
    }

    public function paginationAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $serviceInscricao = new \Boleto\Service\FinanceiroTitulo($this->getEntityManager());

            $data   = $request->getPost()->toArray();
            $result = $serviceInscricao->resultSearch($data);

            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function addAction()
    {
        $request = $this->getRequest();

        $form       = $this->forms()->mountForm($this->getForm());
        $formCheque = $this->forms()->mountForm('Boleto\Form\FinanceiroCheque');
        $service    = $this->services()->getService($this->getService());

        $tipos = $service->getRepository("Boleto\Entity\FinanceiroTituloTipo")->buscaTipos();
        $form->get("tipotitulo")->setValueOptions($tipos);
        $confContas = $service->getRepository('Boleto\Entity\BoletoConfConta')->buscaContas();
        $form->get("confcont")->setValueOptions($confContas);

        if ($request->isPost()) {
            try {
                $dados                 = array_merge($request->getPost()->toArray(), $request->getFiles()->toArray());
                $dados['usuarioBaixa'] = $_SESSION['Zend_Auth']['storage']['id'];
                $ret                   = $service->adicionaTaxa($dados);
                if ($ret->getTituloTipoPagamento() == "Boleto") {
                    $dados['titulo'] = $ret->getTituloId();
                    $boleto          = (new \Boleto\Service\Boleto($service->getEm()))->adicionar($dados);
                }
                if (is_null($this->getRoute())) {
                    $this->setRoute($this->getEvent()->getRouteMatch()->getMatchedRouteName());
                }
            } catch (\Exception $ex) {
                $erro = $ex->getMessage();
                $form->setData($request->getPost()->toArray());
            }
            $is_json = $request->getPost("is_json");
            if (isset($erro)) {
                if ($is_json) {
                    $this->json->setVariable("erro", $erro);

                    return $this->json;
                }
                $this->flashMessenger()->addErrorMessage($erro);
            } else {
                if ($is_json) {
                    if (method_exists($ret, "toArray")) {
                        $dados = $ret->toArray();
                    } else {
                        $dados = "Os dados foram inseridos corretamente, mas um erro ocorreu, verifique se existe o método toArray na entidade {$this->getEntity()}!";
                    }
                    $this->json->setVariable("result", $dados);

                    return $this->json;
                }
            }
            $this->flashMessenger()->addSuccessMessage(
                "Titulo para <strong>{$dados['pessoaNome']}</strong> foi pago com sucesso!</br><br>Imprima o recibo clicando <strong><a target='_blank' href='/boleto-bancario/financeiro-titulo/comprovante-pagamento-mensalidade?tituloId={$ret->getTituloId()}&usuarioBaixa={$dados['usuarioBaixa']}&vencimento={$dados['pes']}&vencimento={$dados['tituloDataVencimento']}&matricula={$dados['matricula']}&pessoa={$dados['pes']}&nome={$dados['pessoaNome']}&servico={$ret->getTipotitulo()->getTipotituloNome()}'>Aqui</a></strong>"
            );

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        $taxas = $service->buscaTaxas();

        $this->view->setVariable("taxas", $taxas);
        $this->view->setVariable("form", $form);
        $this->view->setVariable("formCheque", $formCheque);

        return $this->view;
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function editAction()
    {
        $request                         = $this->getRequest();
        $form                            = $this->forms()->mountForm($this->getForm());
        $formCheque                      = $this->forms()->mountForm('Boleto\Form\FinanceiroCheque');
        $servPessoa                      = new \Pessoa\Service\PessoaFisica($this->getEntityManager());
        $servFinanceiroTitulo            = new \Boleto\Service\FinanceiroTitulo($this->getEntityManager());
        $servFinanceiroTituloMensalidade = new \Boleto\Service\FinanceiroTituloMensalidade($this->getEntityManager());

        if ($request->isPost()) {
            $dados = array_merge($request->getPost()->toArray(), $request->getFiles()->toArray());

            try {
                $dados['usuarioBaixa'] = $_SESSION['Zend_Auth']['storage']['usuario'];
                $this->log()->writeLog(\Zend\Log\Logger::INFO, "Alterando tipo do título - " . $dados['tituloId']);
                $ret = $servFinanceiroTitulo->baixaManual($dados);

                if (is_null($this->getRoute())) {
                    $this->setRoute($this->getEvent()->getRouteMatch()->getMatchedRouteName());
                }
            } catch (\Exception $ex) {
                $erro = $ex->getMessage();
                throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
                $form->setData($request->getPost()->toArray());
            }

            $is_json = $request->getPost("is_json");

            if (isset($erro)) {
                if ($is_json) {
                    $this->json->setVariable("erro", $erro);

                    return $this->json;
                }

                $this->flashMessenger()->addErrorMessage($erro);
            } else {
                if ($is_json) {
                    if (method_exists($ret, "toArray")) {
                        $dados = $ret->toArray();
                    } else {
                        $dados = "Os dados foram alterados corretamente, mas um erro ocorreu, verifique se existe o método toArray na entidade {$this->getEntity()}!";
                    }

                    $this->json->setVariable("result", $dados);

                    return $this->json;
                }
            }

            if ($ret->getTituloEstado() == "Pago" && $dados['tituloEstado'] == 'Aberto') {
                $params = [
                    'tituloId'          => $dados['tituloId'],
                    'usuarioBaixa'      => $dados['usuarioBaixa'],
                    'pessoa'            => $dados['pes'],
                    'matricula'         => $dados['matricula'],
                    'valor'             => $dados['tituloValor'],
                    'parcela'           => $dados['parcela'],
                    'descManual'        => $dados['tituloDescontoManual'],
                    'mensalidade'       => $dados['pessoaNome'],
                    'vencimento'        => $dados['tituloDataVencimento'],
                    'juros'             => $dados['juros'],
                    'descontoCalculado' => $dados['tituloDescontoCalculado'],
                    'DescontoManual'    => $dados['tituloDescontoManual'],
                    'nome'              => $dados['pessoaNome'],
                    'servico'           => $ret->getTipotitulo()->getTipotituloNome()
                ];

                $link = (
                    '/boleto-bancario/financeiro-titulo/comprovante-pagamento-mensalidade?' .
                    http_build_query($params)
                );

                $this->flashMessenger()->addSuccessMessage(
                    "Titulo foi pago com sucesso!</br>" .
                    "<br>Imprima o recibo clicando <strong>" .
                    "<a id='abaImpressao' target='_blank' href='" . $link . "'>Aqui</a></strong>"
                );
            }

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        $id = $this->params()->fromRoute("id", 0);

        if (!$id) {
            $this->flashMessenger()->addErrorMessage(
                "Para editar um registro é necessário informar o identificador do mesmo pela rota!"
            );

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        $reference       = $servFinanceiroTitulo->getReference($id);
        $data            = $reference->toArray();
        $pessoareference = $servPessoa->getReference($data['pes']);
        $formasPagamento = $servFinanceiroTitulo->buscaInformacoesFormasDePagamento(
            $data['tituloId'],
            $data['tituloTipoPagamento']
        );

        $data['tituloDescontoManual'] = round($data['tituloDescontoManual'], 2);
        $data['cnpjCpf']              = $pessoareference->getPesCpf();

        if ($data['tipotituloNome'] == 'Vestibular') {
            $form->remove('matricula');
            $form->remove('parcela');
            $form->remove('tituloMulta');
            $form->remove('tituloJuros');
            $form->get('tituloTipoPagamento')->unsetValueOption('Boleto');
            $form->get('tituloTipoPagamento')->unsetValueOption('Cheque');
            $form->get('tituloId')->setAttribute('wrap', true);

            $this->getView()->setTemplate('boleto/financeiro-titulo/vestibular.phtml');
        } else {
            $mensalidade = $servFinanceiroTituloMensalidade->findOneBy(['titulo' => $data['tituloId']]);

            if (!$mensalidade) {
                $this->flashMessenger()->addErrorMessage(
                    "Título não está vinculado a nenhuma mensalidade!"
                );

                return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
            }

            $form->get('matricula')->setAttribute('value', $mensalidade->getAlunoper()->getAlunoPerId());
            $form->get('parcela')->setAttribute('value', $mensalidade->getMensalidadeParcela());

            $element = $form->get("tituloTipoPagamento");
            $options = $element->getValueOptions();

            unset($options['Boleto']);
            unset($options['Isento']);

            $element->setValueOptions($options);
            $form->add($element);

            $desconto = $servFinanceiroTitulo->buscaInformacoesDescontoTitulo($data['tituloId']);

            $data['tituloDescontoCalculado'] = $data['tituloValor'];

            if (empty($desconto)) {
                $desconto = $servFinanceiroTituloMensalidade->buscaDescontoPadrao($data['tituloId']);

                if (!empty($desconto)) {
                    $this->getView()->setVariable("desconto", $desconto);
                    $data['tituloDescontoCalculado'] -= $desconto[0]['valor'];
                } else {
                    $data['tituloDescontoCalculado'] = $data['tituloValor'];
                }
            } else {
                $this->getView()->setVariable("desconto", $desconto);

                for ($i = 0; $i < sizeof($desconto); $i++) {
                    $data['tituloDescontoCalculado'] -= $desconto[$i]['valor'];
                }
            }

            if ($mensalidade->getMensalidadeParcela() != 1) {
                $dataAtual      = (new \DateTime('now'))->setTime(0, 0, 0);
                $dataVencimento = $servFinanceiroTitulo->formatDateAmericano($data['tituloDataVencimento']);
                $dataVencimento = new \DateTime($dataVencimento);

                if ($dataAtual > $dataVencimento) {
                    $dias           = (int)$dataAtual->diff($dataVencimento)->format("%a");
                    $juros          = $mensalidade->getTituloconf()->getTituloconfJuros();
                    $multa          = $mensalidade->getTituloconf()->getTituloconfMulta();
                    $jurusCalculado = ($data['tituloValor'] / 100) * ($dias * $juros);
                    $multaCalculado = ($data['tituloValor'] / 100) * $multa;

                    $confTituloInadinplencia = array(
                        'dias'                    => $dias,
                        'jurus'                   => $juros,
                        'multa'                   => $multa,
                        'jurusCalculado'          => $jurusCalculado,
                        'multaCalculado'          => $multaCalculado,
                        'valor'                   => $jurusCalculado + $multaCalculado,
                        'tituloDescontoCalculado' => $jurusCalculado + $multaCalculado + $data['tituloValor']
                    );

                    $this->getView()->setVariable("confTituloInadinplencia", $confTituloInadinplencia);
                }
            }

            if (!empty($data['tituloDescontoManual'])) {
                $data['tituloDescontoCalculado'] = $data['tituloDescontoCalculado'] - $data['tituloDescontoManual'];
            }

            $data['tituloDescontoCalculado'] = round($data['tituloDescontoCalculado'], 0);

            $this->getView()->setTemplate($this->getTemplateToRoute());
        }

        if ($data['tituloTipoPagamento'] != "Boleto") {
            $form->remove('tituloMulta');
            $form->remove('tituloJuros');
            $form->remove('tituloDataPagamento');
        }

        $form->setData($data);

        $this->getView()->setVariable("dados", $data);
        $this->getView()->setVariable("form", $form);
        $this->getView()->setVariable("formasPagamento", $formasPagamento);
        $this->getView()->setVariable("formCheque", $formCheque);

        return $this->getView();
    }

    public function baixaAction()
    {
        $request = $this->getRequest();
        $service = $this->services()->getService($this->getService());
        $result  = false;
        $link    = "";
        if ($request->isPost()) {
            $em              = $service->getEm();
            $dados           = $request->getPost()->toArray();
            $tituloReference = $em->getReference($this->getEntity(), $dados['id']);
            if ($tituloReference->getTipotitulo()->getTipotituloNome() === 'Vestibular') {
                $titulo = $service->baixa($dados['id'], null, null, 'Dinheiro');
            } else {
                $titulo = $service->baixa($dados['id']);
            }
            if (!empty($titulo)) {
                $result           = true;
                $serviceInscricao = $this->services()->getService('Vestibular\Service\SelecaoInscricao');
                $link             = "/boleto-bancario/financeiro-titulo/comprovante/{$titulo->getTituloId()}";
            }
        }
        $this->json->setVariable("result", $result);
        $this->json->setVariable("link", $link);

        return $this->json;
    }

    public function insereDescontoManualAction()
    {
        $request = $this->getRequest();
        $service = $this->services()->getService($this->getService());
        $result  = false;
        if ($request->isPost()) {
            $dados                          = $request->getPost()->toArray();
            $titulo                         = $service->findOneBy(['tituloId' => $dados['tituloId']])->toArray();
            $titulo['tituloDescontoManual'] = $dados['tituloDescontoManual'];

            $result = $service->edita($titulo);
        }
        $this->json->setVariable("result", $result);

        return $this->json;
    }

    /**
     * Action para impressão do comprovante de pagamento
     * */
    public function comprovanteAction()
    {
        $tituloId = $this->params()->fromRoute('id');

        //Instanciando serviço
        $service = $this->services()->getService($this->getService());
        /* @var $serviceInscricao \Vestibular\Service\SelecaoInscricao */
        $serviceInscricao = $this->services()->getService('Vestibular\Service\SelecaoInscricao');
        $em               = $service->getEm();

        //Obtendo referencia do título
        /* @var $tituloReference \Boleto\Entity\FinanceiroTitulo */
        $tituloReference = $this->getEntityManager()->getReference('Boleto\Entity\FinanceiroTitulo', $tituloId);
        $edicaoReference = $serviceInscricao->buscaEdicaoAtual();
        /* @var $inscricaoReference \Vestibular\Entity\SelecaoInscricao */
        $inscricaoReference = $em->getRepository('Vestibular\Entity\SelecaoInscricao')->findOneBy(
            array("edicao" => $edicaoReference, 'pes' => $tituloReference->getPes()->getPesId())
        );

        if ($tituloReference) {
            $serviceInscricaoCursos = new \Vestibular\Service\InscricaoCursos($this->getEntityManager());
            $serviceOrgCampus       = new \Organizacao\Service\OrgCampus($this->getEntityManager());

            /* @var $objInscricaoCursos \Vestibular\Entity\InscricaoCursos */
            $objInscricaoCursos = $serviceInscricaoCursos->getRepository()->findOneBy(
                ['inscricao' => $inscricaoReference->getInscricaoId()]
            );

            $objOrgCampus = null;

            if ($objInscricaoCursos) {
                $objOrgCampus = $objInscricaoCursos->getSelcursos()->getCursocampus()->getCamp();
            }

            $this->view->setVariables($serviceOrgCampus->retornaDadosInstituicao($objOrgCampus));
            $this->view->setVariable('tituloReference', $tituloReference);
            $this->view->setVariable('edicaoReference', $edicaoReference);
            $this->view->setVariable('inscricaoReference', $inscricaoReference);
        } else {
            $this->flashMessenger()->addErrorMessage('Título financeiro não encontrado.');

            return $this->redirect()->toRoute($this->getRoute(), ['controller' => $this->getController()]);
        }

        $this->view->setTerminal(true);

        return $this->view;
    }

    public function comprovantePagamentoMensalidadeAction()
    {
        $data             = $_GET;
        $service          = new \Boleto\Service\FinanceiroTitulo($this->getEntityManager());
        $serviceAluno     = new \Matricula\Service\AcadperiodoAluno($this->getEntityManager());
        $serviceTurma     = new \Matricula\Service\AcadperiodoTurma($this->getEntityManager());
        $serviceOrgCampus = new \Organizacao\Service\OrgCampus($this->getEntityManager());

        $infoUser = $service->buscaUsuario($data['usuarioBaixa']);

        $turma = $serviceAluno->buscaTurmaPrincipalAluno($data['matricula']);
        /* @var $objTurma \Matricula\Entity\AcadperiodoTurma */
        $objTurma = $serviceTurma->getRepository()->find($turma[0]['turma_id']);

        if ($infoUser != false) {
            $usuario = $infoUser[0]['pes_nome'];
        }
        if (!is_null($turma)) {
            $data['turma'] = $turma[0]['turma_nome'];
            $data['serie'] = $turma[0]['turma_serie'];
        }

        $dataVencimento = explode("/", $data['vencimento']);
        //Obtendo referencia do título
        $tituloReference = $this->getEntityManager()->getReference('Boleto\Entity\FinanceiroTitulo', $data['tituloId']);

        if ($tituloReference) {
            $this->view->setVariable('tituloReference', $tituloReference);
            $this->view->setVariable('titulo', $data['servico']);
            $this->view->setVariable('matricula', $data['matricula']);
            $this->view->setVariable('nome', $data['nome']);
            $this->view->setVariable('parcela', $data['parcela']);
            $this->view->setVariable('mes', $dataVencimento[1]);
            $this->view->setVariable('ano', $dataVencimento[2]);
            $this->view->setVariable('info', $data);
            $this->view->setVariable('usuario', $usuario);
            $this->view->setVariables(
                $serviceOrgCampus->retornaDadosInstituicao($objTurma->getCursocampus()->getCamp())
            );
        } else {
            $this->flashMessenger()->addErrorMessage('Título financeiro não encontrado.');

            return $this->redirect()->toRoute($this->getRoute(), ['controller' => $this->getController()]);
        }

        $this->view->setTerminal(true);

        return $this->view;
    }

    public function showFormAction()
    {
        $param = $this->params()->fromRoute();
        $this->view->setTerminal(true);
        $form = $this->getForm();
        if (!class_exists($form)) {
            $this->view->setVariable("erro", "Formulário de busca não foi encontrado!");

            return $this->view;
        }
        $form = new $form;
        $form->remove('cnpjCpf')
            ->remove('tituloJuros')
            ->remove('tituloMulta')
            ->remove('tipotitulo')
            ->remove('tituloDataVencimento')
            ->remove('tituloValor')
            ->remove('tituloDataPagamento')
            ->remove('tituloTipoPagamento')
            ->remove('tituloObservacoes')
            ->remove('parcela')
            ->remove('valorFormaPagamento')
            ->remove('tituloDescontoManual')
            ->remove('tituloDataProcessamento')
            ->remove('confcont');
        $element = $form->get('tituloId');
        $element->setAttribute('readonly', false);
        $form->add($element);
        $element = $form->get('pessoaNome');
        $element->setAttribute('readonly', false);
        $form->add($element);
        $element = $form->get('matricula');
        $element->setAttribute('readonly', false);
        $form->add($element);
        $form->setAttribute("method", 'get');
        $this->view->setVariable("config", $this->config);
        $this->view->setVariable("form", $form);

        return $this->view;
    }

    public function relatorioAction()
    {
        $service    = $this->services()->getService($this->getService());
        $serviceRel = $this->services()->getService('Boleto\Service\FinanceiroRelatorios');
        $request    = $this->getRequest();

        if ($request->isPost()) {
            $serviceOrgCampus = new \Organizacao\Service\OrgCampus($this->getEntityManager());
            $this->view->setVariables($serviceOrgCampus->retornaDadosInstituicao());

            $dados = $request->getPost()->toArray();
            $res   = $serviceRel->geraRelatorioFinanceiroPeriodo(
                $dados['dataInicio'],
                $dados['dataFim'],
                $dados['tipoUsuario']
            );
            if (!is_null($res)) {
                foreach ($res as $formas) {
                    if ($formas['titulo_tipo_pagamento'] === 'Dinheiro' OR $formas['titulo_tipo_pagamento'] === 'Dinheiro,Cheque' OR $formas['titulo_tipo_pagamento'] === 'Dinheiro,Credito Fies') {
                        if (sizeof(
                                $titulo[($formas['tipotitulo_nome'] === 'Mensalidade'
                                    OR $formas['tipotitulo_nome'] === 'Dependência'
                                    OR $formas['tipotitulo_nome'] === "Adaptação"
                                    OR $formas['tipotitulo_nome'] === "Monografia") ? 'mensalidade' : 'taxas']['dinheiro']
                            ) === 0
                        ) {
                            $sizeof = 1;
                        } elseif (sizeof(
                                $titulo[($formas['tipotitulo_nome'] === 'Mensalidade'
                                    OR $formas['tipotitulo_nome'] === 'Dependência'
                                    OR $formas['tipotitulo_nome'] === "Adaptação"
                                    OR $formas['tipotitulo_nome'] === "Monografia") ? 'mensalidade' : 'taxas']['dinheiro']
                            ) === 2
                        ) {
                            $sizeof = 2;
                        } else {
                            $sizeof = sizeof(
                                $titulo[($formas['tipotitulo_nome'] === 'Mensalidade'
                                    OR $formas['tipotitulo_nome'] === 'Dependência'
                                    OR $formas['tipotitulo_nome'] === "Adaptação"
                                    OR $formas['tipotitulo_nome'] === "Monografia") ? 'mensalidade' : 'taxas']['dinheiro']
                            );
                        }

                        $titulo[($formas['tipotitulo_nome'] === 'Mensalidade'
                            OR $formas['tipotitulo_nome'] === 'Dependência'
                            OR $formas['tipotitulo_nome'] === "Adaptação"
                            OR $formas['tipotitulo_nome'] === "Monografia") ? 'mensalidade' : 'taxas']['dinheiro'][$sizeof] = [
                            'aluno'          => $formas['pes_nome'],
                            'matricula'      => $formas['alunocurso_id'],
                            'titulo'         => $formas['titulo_id'],
                            'vencimento'     => $formas['titulo_data_vencimento'],
                            'data-pagamento' => $formas['titulo_data_pagamento'],
                            'estado'         => $formas['titulo_estado'],
                            'acrescimo'      => $formas['acrescimo'],
                            'pagamento'      => $formas['pagamento'],
                            'valor-pago'     => $formas['titulo_valor_pago_dinheiro'],
                        ];
                        $titulo[($formas['tipotitulo_nome'] === 'Mensalidade'
                            OR $formas['tipotitulo_nome'] === 'Dependência'
                            OR $formas['tipotitulo_nome'] === "Adaptação"
                            OR $formas['tipotitulo_nome'] === "Monografia") ? 'mensalidade' : 'taxas']['dinheiro']['total'] += $formas['titulo_valor_pago_dinheiro'];
                    }
                    if ($formas['titulo_tipo_pagamento'] == 'Cheque' OR !is_null($formas['titulocheque_valor'])) {
                        if ($formas['pagamento'] == 'a vista') {
                            if (sizeof(
                                    $titulo[($formas['tipotitulo_nome'] === 'Mensalidade'
                                        OR $formas['tipotitulo_nome'] === 'Dependência'
                                        OR $formas['tipotitulo_nome'] === "Adaptação"
                                        OR $formas['tipotitulo_nome'] === "Monografia") ? 'mensalidade' : 'taxas']['cheque-avista']
                                ) === 0
                            ) {
                                $sizeof = 1;
                            } elseif (sizeof(
                                    $titulo[($formas['tipotitulo_nome'] === 'Mensalidade') ? 'mensalidade' : 'taxas']['cheque-avista']
                                ) === 2
                            ) {
                                $sizeof = 2;
                            } else {
                                $sizeof = sizeof(
                                    $titulo[($formas['tipotitulo_nome'] === 'Mensalidade') ? 'mensalidade' : 'taxas']['cheque-avista']
                                );
                            }
                            $titulo[($formas['tipotitulo_nome'] === 'Mensalidade'
                                OR $formas['tipotitulo_nome'] === 'Dependência'
                                OR $formas['tipotitulo_nome'] === "Adaptação"
                                OR $formas['tipotitulo_nome'] === "Monografia") ? 'mensalidade' : 'taxas']['cheque-avista'][$sizeof] = [
                                'aluno'          => $formas['pes_nome'],
                                'matricula'      => $formas['alunocurso_id'],
                                'titulo'         => $formas['financeiro__titulo.titulo_id'],
                                'vencimento'     => $formas['financeiro__titulo.titulo_data_vencimento'],
                                'data-pagamento' => $formas['titulo_data_pagamento'],
                                'estado'         => $formas['titulo_estado'],
                                'acrescimo'      => $formas['acrescimo'],
                                'pagamento'      => $formas['pagamento'],
                                'valor-pago'     => $formas['titulocheque_valor'],
                            ];
                            $titulo[($formas['tipotitulo_nome'] === 'Mensalidade'
                                OR $formas['tipotitulo_nome'] === 'Dependência'
                                OR $formas['tipotitulo_nome'] === "Adaptação"
                                OR $formas['tipotitulo_nome'] === "Monografia") ? 'mensalidade' : 'taxas']['cheque-avista']['total'] += $formas['titulocheque_valor'];
                        } else {
                            if (sizeof(
                                    $titulo[($formas['tipotitulo_nome'] === 'Mensalidade') ? 'mensalidade' : 'taxas']['cheque-pre']
                                ) === 0
                            ) {
                                $sizeof = 1;
                            } elseif (sizeof(
                                    $titulo[($formas['tipotitulo_nome'] === 'Mensalidade') ? 'mensalidade' : 'taxas']['cheque-pre']
                                ) === 2
                            ) {
                                $sizeof = 2;
                            } else {
                                $sizeof = sizeof(
                                    $titulo[($formas['tipotitulo_nome'] === 'Mensalidade') ? 'mensalidade' : 'taxas']['cheque-pre']
                                );
                            }
                            $titulo[($formas['tipotitulo_nome'] === 'Mensalidade') ? 'mensalidade' : 'taxas']['cheque-pre'][$sizeof] = [
                                'aluno'          => $formas['pes_nome'],
                                'matricula'      => $formas['alunocurso_id'],
                                'titulo'         => $formas['financeiro__titulo.titulo_id'],
                                'vencimento'     => $formas['financeiro__titulo.titulo_data_vencimento'],
                                'data-pagamento' => $formas['titulo_data_pagamento'],
                                'estado'         => $formas['titulo_estado'],
                                'acrescimo'      => $formas['acrescimo'],
                                'pagamento'      => $formas['pagamento'],
                                'valor-pago'     => $formas['titulocheque_valor'],
                            ];
                            $titulo[($formas['tipotitulo_nome'] === 'Mensalidade') ? 'mensalidade' : 'taxas']['cheque-pre']['total'] += $formas['titulocheque_valor'];
                        }
                    }
                    if ($formas['titulo_tipo_pagamento'] === 'Deposito') {
                        if (sizeof(
                                $titulo[($formas['tipotitulo_nome'] === 'Mensalidade'
                                    OR $formas['tipotitulo_nome'] === 'Dependência'
                                    OR $formas['tipotitulo_nome'] === "Adaptação"
                                    OR $formas['tipotitulo_nome'] === "Monografia") ? 'mensalidade' : 'taxas']['deposito']
                            ) === 0
                        ) {
                            $sizeof = 1;
                        } elseif (sizeof(
                                $titulo[($formas['tipotitulo_nome'] === 'Mensalidade'
                                    OR $formas['tipotitulo_nome'] === 'Dependência'
                                    OR $formas['tipotitulo_nome'] === "Adaptação"
                                    OR $formas['tipotitulo_nome'] === "Monografia") ? 'mensalidade' : 'taxas']['deposito']
                            ) === 2
                        ) {
                            $sizeof = 2;
                        } else {
                            $sizeof = sizeof(
                                $titulo[($formas['tipotitulo_nome'] === 'Mensalidade'
                                    OR $formas['tipotitulo_nome'] === 'Dependência'
                                    OR $formas['tipotitulo_nome'] === "Adaptação"
                                    OR $formas['tipotitulo_nome'] === "Monografia") ? 'mensalidade' : 'taxas']['deposito']
                            );
                        }
                        $titulo[($formas['tipotitulo_nome'] === 'Mensalidade'
                            OR $formas['tipotitulo_nome'] === 'Dependência'
                            OR $formas['tipotitulo_nome'] === "Adaptação"
                            OR $formas['tipotitulo_nome'] === "Monografia") ? 'mensalidade' : 'taxas']['deposito'][$sizeof] = [
                            'aluno'          => $formas['pes_nome'],
                            'matricula'      => $formas['alunocurso_id'],
                            'titulo'         => $formas['financeiro__titulo.titulo_id'],
                            'vencimento'     => $formas['financeiro__titulo.titulo_data_vencimento'],
                            'data-pagamento' => $formas['titulo_data_pagamento'],
                            'estado'         => $formas['titulo_estado'],
                            'acrescimo'      => $formas['acrescimo'],
                            'pagamento'      => $formas['pagamento'],
                            'valor-pago'     => $formas['titulo_valor_pago'],
                        ];
                        $titulo[($formas['tipotitulo_nome'] === 'Mensalidade'
                            OR $formas['tipotitulo_nome'] === 'Dependência'
                            OR $formas['tipotitulo_nome'] === "Adaptação"
                            OR $formas['tipotitulo_nome'] === "Monografia") ? 'mensalidade' : 'taxas']['deposito']['total'] += $formas['titulo_valor_pago'];
                    }
                    if ($formas['titulo_tipo_pagamento'] === 'Credito Fies' OR $formas['titulo_tipo_pagamento'] === 'Dinheiro,Credito Fies') {
                        if (sizeof(
                                $titulo[($formas['tipotitulo_nome'] === 'Mensalidade'
                                    OR $formas['tipotitulo_nome'] === 'Dependência'
                                    OR $formas['tipotitulo_nome'] === "Adaptação"
                                    OR $formas['tipotitulo_nome'] === "Monografia") ? 'mensalidade' : 'taxas']['Credito Fies']
                            ) === 0
                        ) {
                            $sizeof = 1;
                        } elseif (sizeof(
                                $titulo[($formas['tipotitulo_nome'] === 'Mensalidade'
                                    OR $formas['tipotitulo_nome'] === 'Dependência'
                                    OR $formas['tipotitulo_nome'] === "Adaptação"
                                    OR $formas['tipotitulo_nome'] === "Monografia") ? 'mensalidade' : 'taxas']['Credito Fies']
                            ) === 2
                        ) {
                            $sizeof = 2;
                        } else {
                            $sizeof = sizeof(
                                $titulo[($formas['tipotitulo_nome'] === 'Mensalidade'
                                    OR $formas['tipotitulo_nome'] === 'Dependência'
                                    OR $formas['tipotitulo_nome'] === "Adaptação"
                                    OR $formas['tipotitulo_nome'] === "Monografia") ? 'mensalidade' : 'taxas']['Credito Fies']
                            );
                        }
                        $titulo[($formas['tipotitulo_nome'] === 'Mensalidade'
                            OR $formas['tipotitulo_nome'] === 'Dependência'
                            OR $formas['tipotitulo_nome'] === "Adaptação"
                            OR $formas['tipotitulo_nome'] === "Monografia") ? 'mensalidade' : 'taxas']['Credito Fies'][$sizeof] = [
                            'aluno'          => $formas['pes_nome'],
                            'matricula'      => $formas['alunocurso_id'],
                            'titulo'         => $formas['financeiro__titulo.titulo_id'],
                            'vencimento'     => $formas['financeiro__titulo.titulo_data_vencimento'],
                            'data-pagamento' => $formas['titulo_data_pagamento'],
                            'estado'         => $formas['titulo_estado'],
                            'acrescimo'      => $formas['acrescimo'],
                            'pagamento'      => $formas['pagamento'],
                            'valor-pago'     => $formas['titulo_valor_pago'] - $formas['titulo_valor_pago_dinheiro'],
                        ];
                        $titulo[($formas['tipotitulo_nome'] === 'Mensalidade'
                            OR $formas['tipotitulo_nome'] === 'Dependência'
                            OR $formas['tipotitulo_nome'] === "Adaptação"
                            OR $formas['tipotitulo_nome'] === "Monografia") ? 'mensalidade' : 'taxas']['Credito Fies']['total'] += $formas['titulo_valor_pago'];
                    }
                }
            }
            if ($dados['tipoUsuario'] !== 'todos') {
                $usuario = $service->getRepository("Acesso\Entity\AcessoPessoas")->findOneBy(
                    ['id' => $dados['tipoUsuario']]
                );
            }
            if ($dados['tipoRelatorio'] === 'analitico') {
                $this->view->setTemplate('boleto/financeiro-titulo/relatorio-analitico');
            }
            $this->view->setVariables(
                [
                    'dataIni' => $service->formatDateBrasileiro($dados['dataInicio']),
                    'dataFim' => $service->formatDateBrasileiro($dados['dataFim']),
                    'usuario' => $usuario,
                    'res'     => $titulo,
                ]
            );
            $this->view->setTerminal(true);

            return $this->view;
        }
        $usuariosTesouraria = $service->buscaUsuariosTesouraria();
        $servicos           = $service->buscaServicosRelatorio();
        for ($i = 0; $i < count($servicos); $i++) {
            if ($servicos[$i] == 'Vestibular') {
                unset($servicos[$i]);
                break;
            }
        }

        $formRelatorio = new \Boleto\Form\Relatorio($usuariosTesouraria, $servicos);
        $this->view->setTemplate('boleto/financeiro-titulo/relatorio-form');
        $this->view->setVariables(
            [
                'formRelatorio' => $formRelatorio
            ]
        );

        return $this->view;
    }

    public function prototipoAction()
    {
        return new ViewModel();
    }

    public function prototipo2Action()
    {
        return new ViewModel();
    }

    // 10/09/2016 Ao chamar o metodo de Baixa foi passado os parametros que serviço solicita, o que então não ocorria.
    public function quitaVestibularAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $dados = $request->getPost()->toArray();

            if (!empty($dados)) {
                $serviceFinanceiro    = $this->getServiceLocator()->get('Boleto\Service\FinanceiroTitulo');
                $dados['tituloValor'] = floatval($dados['tituloValor']);
                $titulo               = $serviceFinanceiro->baixa(
                    $dados['tituloId'],
                    null,
                    $dados['tituloValor'],
                    $dados['tituloTipoPagamento']
                );
                if (!empty($titulo)) {
                    $serviceInscricao = $this->services()->getService('Vestibular\Service\SelecaoInscricao');
                    $this->flashMessenger()->addSuccessMessage(
                        "Título foi pago com sucesso!</br><br>Imprima o recibo clicando <strong><a id='abaImpressao' target='_blank' href='/boleto-bancario/financeiro-titulo/comprovante/{$dados['tituloId']}'>Aqui</a></strong>"
                    );

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }
            }
        }
    }
}
