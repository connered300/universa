<?php

namespace Boleto\Controller;

use PhpBoletoZf2\Model\BoletoBB;
use PhpBoletoZf2\Model\BoletoCaixa;
use PhpBoletoZf2\Model\BoletoBancoob;
use PhpBoletoZf2\Model\Cedente;
use PhpBoletoZf2\Model\Sacado;
use Sistema\Service\SisConfig;
use VersaSpine\Controller\AbstractCoreController;
use Zend\View\Model\ViewModel;

class BoletoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {
    }

    public function indexAction()
    {
        $dados  = array();
        $dados  = $this->services()->getService($this->getService())->pagination();
        $config = $this->getConfig();
        /**
         * Atribui as variaveis que vao ser utilizadas pelo list generator
         */
        $config['table']['dados']      = $dados['dados'];
        $config['pagination']['count'] = $dados['count'];
        $config['table']['table_key']  = $this->services()->getService($this->getService())->getTablePkey();

        /**
         * Cria as variaveis para a view
         */
        $this->view->setVariable("dados", $dados['dados']);
        $this->view->setVariable("count", $dados['count']);
        $this->view->setVariable("config", $config);
        $this->view->setTemplate($this->getTemplateToRoute());

        return $this->view;
    }

    private function retornaViewSimples($mensagem, $erro = false)
    {
        $this->getView()->setVariable('erro', $erro);
        $this->getView()->setVariable('mensagem', $mensagem);

        $this->layout('layout/simples');

        return $this->getView();
    }

    public function exibeAction()
    {
        $idBoleto = $this->params()->fromRoute("id");

        if (!$idBoleto) {
            return $this->retornaViewSimples("Erro: Nenhum Boleto foi encontrado!", true);
        }

        $arrParam = $this->getRequest()->getQuery()->toArray();
        $force    = $arrParam['force'] ? true : false;

        $serviceBoleto = new \Financeiro\Service\Boleto(
            $this->getEntityManager(), $this->getServiceManager()->get('Config')
        );

        /** @var \Financeiro\Entity\Boleto $boleto */
        $boleto = $serviceBoleto->getRepository()->find($idBoleto);

        if (!$boleto) {
            return $this->retornaViewSimples("Erro: Nenhum Boleto foi encontrado!", true);
        }

        if (
            $boleto->getTitulo()->getTituloEstado() != \Financeiro\Service\FinanceiroTitulo::TITULO_ESTADO_ABERTO &&
            !$force
        ) {
            return $this->retornaViewSimples("Erro: Título não se encontra aberto!", true);
        }

        $serviceSisConfig = new \Sistema\Service\SisConfig(
            $this->getEntityManager(),
            $this->getServiceManager()->get('config')
        );

        $boletoAtivado        = $serviceSisConfig->localizarChave('BOLETO_ATIVO');
        $boletoRemessaAtivada = $serviceSisConfig->localizarChave('BOLETO_REMESSA_ATIVADA');

        if (!$boletoAtivado && !$force) {
            return $this->retornaViewSimples("Erro: Meio de pagamento boleto encontra-se desativado!", true);
        }

        if ($boletoRemessaAtivada) {
            $serviceFinanceiroRemessaLote = new \Financeiro\Service\FinanceiroRemessaLote($this->getEntityManager());
            $estaNaRemessa                = $serviceFinanceiroRemessaLote
                ->getRepository()
                ->findOneBy(['bol' => $boleto]);

            if (!$estaNaRemessa && !$force) {
                return $this->retornaViewSimples("Erro: Boleto ainda não foi registrado!", true);
            }
        }

        $view = new ViewModel();
        $view->setTerminal(true);

        $arrDadosBoleto = $serviceBoleto->formatarDadosBoletoImpressao($boleto);

        if (isset($_GET['carteira'])) {
            $arrDadosBoleto['boleto']['carteira']  = $_GET['carteira'];
            $arrDadosBoleto['sacado']['carteira']  = $_GET['carteira'];
            $arrDadosBoleto['cedente']['carteira'] = $_GET['carteira'];
        }

        $bancCodigo = $boleto->getConfcont()->getBanc()->getBancCodigo();

        $sacado         = new \PhpBoletoZf2\Model\Sacado($arrDadosBoleto['sacado']);
        $cedente        = new \PhpBoletoZf2\Model\Cedente($arrDadosBoleto['cedente']);
        $arrDadosBoleto = $arrDadosBoleto['boleto'];

        /** @var \PhpBoletoZf2\Model\AbstractBoleto $boleto */
        /** @var \PhpBoletoZf2\Factory\AbstractBoletoFactory $objBoleto */
        if ($bancCodigo == \Financeiro\Service\BoletoBanco::BANCO_BRADESCO) {
            $boleto    = new \PhpBoletoZf2\Model\BoletoBradesco($arrDadosBoleto);
            $objBoleto = $this->getServiceLocator()->get('Boleto\Bradesco');
            $objBoleto->getBanco()->setAceite('N');
            $view->setTemplate("/php-boleto-zf2/bradesco/index");
        } elseif ($bancCodigo == \Financeiro\Service\BoletoBanco::BANCO_BRASIL) {
            //TODO: Analisar se o boleto do BB está funcionando corretamente
            $boleto    = new \PhpBoletoZf2\Model\BoletoBB($arrDadosBoleto);
            $objBoleto = $this->getServiceLocator()->get('Boleto\BB');
            $view->setTemplate("/php-boleto-zf2/bb/index");
        } elseif ($bancCodigo == \Financeiro\Service\BoletoBanco::BANCO_BANCOOB) {
            $boleto    = new \PhpBoletoZf2\Model\BoletoBancoob($arrDadosBoleto);
            $objBoleto = $this->getServiceLocator()->get('Boleto\Bancoob');
            $view->setTemplate("/php-boleto-zf2/bancoob/index");
        } elseif ($bancCodigo == \Financeiro\Service\BoletoBanco::BANCO_CAIXA) {
            $boleto    = new \PhpBoletoZf2\Model\BoletoCaixa($arrDadosBoleto);
            $objBoleto = $this->getServiceLocator()->get('Boleto\CaixaSigcb');
            $view->setTemplate("/php-boleto-zf2/caixa-sigcb/index");
        } else {
            throw new \Exception("Layout de Boleto não implementado!", 1);
        }

        $objBoleto->setSacado($sacado);
        $objBoleto->setCedente($cedente);
        $objBoleto->setBoleto($boleto);

        if ($cedente->getCarteira()) {
            $objBoleto->getBanco()->setCarteira($cedente->getCarteira());
        }

        if ($boleto->getAceite()) {
            $objBoleto->getBanco()->setAceite($boleto->getAceite());
        }

        if ($boleto->getEspecieDoc()) {
            $objBoleto->getBanco()->setEspecieDoc($boleto->getEspecieDoc());
        }

        $arrDados = $objBoleto->prepare();

        $view->setVariable("dados", $arrDados);

        return $view;
    }

    public function carneAction()
    {
        $serviceBoletoBanco = new \Financeiro\Service\BoletoBanco($this->getEntityManager());

        $arrBoletos = array();
        $arrDados   = array();
        $arrTitulos = array();
        /** @var \Zend\Http\Request $request */
        $request = $this->getRequest();
        $params  = $this->params()->fromRoute("id");

        $serviceBoleto = new \Financeiro\Service\Boleto(
            $this->getEntityManager(),
            $this->getServiceManager()->get('config')
        );

        if (preg_match_all('/[[:digit:]]+/', $params, $arrParams)) {
            $arrTitulos = $arrParams[0];
        } elseif ($request->isPost()) {
            $arrTitulos = $request->getPost()->toArray();
        } else {
            $view = new ViewModel();
            $view->setTemplate("/boleto/carne/index");

            return $view;
        }

        foreach ($arrTitulos as $titulo) {
            /** @var \Financeiro\Entity\Boleto $boleto */
            $boleto = $serviceBoleto->getRepository()->findOneBy(['titulo' => $titulo]);

            if (!is_null($boleto)) {
                if ($serviceBoletoBanco::BANCO_CAIXA == $boleto->getConfcont()->getBanc()->getBancCodigo()) {
                    $arrDados['caixa'][] = $serviceBoleto->formatarDadosBoletoImpressao($boleto);
                } elseif ($serviceBoletoBanco::BANCO_BANCOOB == $boleto->getConfcont()->getBanc()->getBancCodigo()) {
                    $arrDados['sicoob'][] = $serviceBoleto->formatarDadosBoletoImpressao($boleto);
                } else {
                    $this->flashMessenger()->addErrorMessage(
                        'Não é possivel emitir carne para este banco ' .
                        $boleto->getConfcont()->getBanc()->getBancNome() . '!'
                    );

                    return false;
                }
            }
        }

        return $this->carneBanco($arrDados);
    }

    public function baixaAction()
    {
        /** @var \Zend\Http\Request $request */
        $request = $this->getRequest();
        $form    = $this->forms()->mountForm($this->getForm());
        $service = $this->services()->getService($this->getService());

        if ($request->isPost()) {
            $diretorioBaixas = $service
                ->getRepository('GerenciadorArquivos\Entity\ArquivoDiretorios')
                ->findOneBy(array('arqDiretorioNome' => 'BaixasBancarias'));

            $file  = new \GerenciadorArquivos\Service\Arquivo(
                $this->getServiceLocator()->get('Doctrine\ORM\EntityManager'), $diretorioBaixas
            );
            $dados = array_merge($this->getRequest()->getPost()->toArray(), $this->params()->fromFiles());

            if (!empty($dados['file']['name'])) {
                if ($dados['file']['type'] == "application/octet-stream") {
                    $dadosArquivo = array(
                        'name'     => $dados['file']['name'],
                        'type'     => $dados['file']['type'],
                        'tmp_name' => $dados['file']['tmp_name'],
                        'error'    => $dados['file']['error'],
                        'size'     => $dados['file']['size'],
                    );

                    try {
                        $arquivo = $file->adicionar($dadosArquivo);
                    } catch (\Exception $ex) {
                        $this->flashMessenger()->addErrorMessage(
                            "Não foi armazer o arquivo no servidor! Favor entrar em contato com suporte."
                        );
                    }
                    $caminhoArq = $arquivo->getDiretorio()->getarqDiretorioEndereco() . "/" . $arquivo->getArqChave();
                    try {
                        //habilar quando estiver em produção
                        $cnab400 = \ArquivoRetorno\Service\RetornoFactory::getRetorno($caminhoArq, "linhaProcessada1");
                        //habilar quando estiver em teste
                    } catch (\Exception $ex) {
                        $this->flashMessenger()->addErrorMessage("Não foi possivel realizar a leitura o arquivo");
                    }
                    if ($cnab400) {
                        $retorno = new \ArquivoRetorno\Service\RetornoBanco($cnab400);
                        try {
                            $arqLinhas = $retorno->processar();
                        } catch (\Exception $ex) {
                            $this->flashMessenger()->addErrorMessage(
                                "Não foi possivel processar o arquivo! Favor entrar em contato com suporte."
                            );
                        }
                        try {
                            $numBoletos = $service->baixaArquivo($arqLinhas);
                            $this->flashMessenger()->addSuccessMessage("Foram baixados: " . $numBoletos . " boletos");
                        } catch (\Exception $ex) {
                            $this->flashMessenger()->addErrorMessage(
                                "Não foi possivel realizar a baixa dos boletos! Favor entrar em contato com suporte."
                            );
                        }

                        return $this->redirect()->toRoute(
                            $this->getRoute(),
                            array('controller' => $dados['controller'])
                        );
                    }
                }
                $this->flashMessenger()->addErrorMessage(
                    "Arquivo com formato invalido! Favor selecionar um arquivo valido e tentar novamente"
                );
            }
            $this->flashMessenger()->addErrorMessage(
                "Nenhum arquivo foi enviado! Favor selecionar um arquivo e tentar novamente."
            );

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $dados['controller']));
        }

        $this->view->setVariable("form", $form);
        $this->view->setTemplate($this->getTemplateToRoute());

        return $this->view;
    }

    public function showFormAction()
    {
        $this->view->setTerminal(true);
        $form = $this->getForm();
        if (!class_exists($form)) {
            $this->view->setVariable("erro", "Formulário de busca não foi encontrado!");

            return $this->view;
        }
        $form = new $form;
        $form->remove("bolValor");
        $form->remove("bolDataVencimento");
        $form->remove("bolDemostrativo");
        $form->remove("pessoaNome");
        $form->remove("cnpjCpf");
        $form->setAttribute("method", 'get');
        $this->view->setVariable("config", $this->config);
        $this->view->setVariable("form", $form);

        return $this->view;
    }

    public function searchAction()
    {
        /** @var \Zend\Http\Request $request */
        $request = $this->getRequest();

        if ($request->isPost()) {
            $arrData = $request->getPost()->toArray();
            $boleto  = new \Boleto\Service\Boleto($this->getEntityManager());

            $result = $boleto->search($arrData);

            $this->getJson()->setVariable("draw", $result['draw']);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function carneBanco(array $arrDados)
    {
        $arrVariables = array();
        $alunocursoId = array();

        $serviceBoletoBanco = new \Financeiro\Service\BoletoBanco($this->getEntityManager());

        foreach ($arrDados as $arrDados) {
            foreach ($arrDados as $indice => $dados) {
                $sacado  = new Sacado($dados['sacado']);
                $cedente = new Cedente($dados['cedente']);
                if ($serviceBoletoBanco::BANCO_CAIXA == $dados['boleto']['codigoBanco']) {
                    $boleto = new BoletoCaixa($dados['boleto']);

                    // chamando o serviço para criação do boleto
                    $banco = $this->getServiceLocator()
                        ->get('Boleto\CaixaSigcb')
                        ->setSacado($sacado)
                        ->setCedente($cedente)
                        ->setBoleto($boleto);
                } elseif ($serviceBoletoBanco::BANCO_BANCOOB === $dados['boleto']['codigoBanco']) {
                    $boleto = new BoletoBancoob($dados['boleto']);

                    // chamando o serviço para criação do boleto
                    $banco = $this->getServiceLocator()
                        ->get('Boleto\Bancoob')
                        ->setSacado($sacado)
                        ->setCedente($cedente)
                        ->setBoleto($boleto);
                }
                if ($cedente->getCarteira()) {
                    $banco->getBanco()->setCarteira($cedente->getCarteira());
                }
                $arrVariables[$dados['boleto']['codigoBanco']][] = clone($banco->prepare());
                $alunocursoId[$dados['boleto']['codigoBanco']][] = $dados['alunocursoId'] ? $dados['alunocursoId'] : ' - ';
            }
        }

        $model = new ViewModel(array("arrDados" => $arrVariables, "alunocursoId" => $alunocursoId));
        $model->setTemplate("/boleto/carne/index");
        $model->setTerminal(true);

        return $model;
    }

    /**
     * @return ViewModel
     */
    public function exibeMultiplosBoletosAction()
    {
        /** @var \Zend\Http\Request $request */
        $request                 = $this->getRequest();
        $config                  = $this->getServiceManager()->get('Config');
        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo($this->getEntityManager(), $config);
        $serviceBoleto           = new \Financeiro\Service\Boleto($this->getEntityManager(), $config);
        $serviceSisConfig        = new \Sistema\Service\SisConfig($this->getEntityManager(), $config);

        $arrRetorno                = [];
        $filtros                   = $request->getQuery()->toArray();

        if (empty($filtros)) {
            return $this->retornaViewSimples("Erro: Nenhum dado informado", true);
        } elseif (!isset($filtros['vencimento_final']) || !isset($filtros['vencimento_inicial'])) {
            return $this->retornaViewSimples("Erro: É necessário informar data inicial e final de vencimento!", true);
        }

        $force                     = $filtros['force'] ? true : false;
        $filtros['titulosAbertos'] = true;
        $arrTitulos                = $serviceFinanceiroTitulo->retornaTitulosAluno($filtros);
        $boletoAtivado             = $serviceSisConfig->localizarChave('BOLETO_ATIVO');
        $boletoRemessaAtivada      = $serviceSisConfig->localizarChave('BOLETO_REMESSA_ATIVADA');

        if (!$boletoAtivado && !$force) {
            return $this->retornaViewSimples("Erro: Meio de pagamento boleto encontra-se desativado!", true);
        }

        foreach ($arrTitulos as $value) {
            $idBoleto       = '';
            $boleto         = null;
            $arrDadosBoleto = [];
            $arrDados       = [];
            $objBoleto      = null;

            if ($idBoleto = $value['bol_id']) {
                /** @var \Financeiro\Entity\Boleto $boleto */
                $boleto = $serviceBoleto->getRepository()->find($idBoleto);

                if (!$boleto->getTitulo()->tituloEstaAberto() && !$force) {
                    $arrBoletosFechados[] = $idBoleto;
                    continue;
                }

                if ($boletoRemessaAtivada) {
                    $serviceRemessaLote = new \Financeiro\Service\FinanceiroRemessaLote($this->getEntityManager());
                    $estaNaRemessa      = $serviceRemessaLote->getRepository()->findOneBy(['bol' => $boleto]);

                    if (!$estaNaRemessa && !$force) {
                        $arrBoletoNaoRegistrado[] = $idBoleto;
                        continue;
                    }
                }

                $arrDadosBoleto = $serviceBoleto->formatarDadosBoletoImpressao($boleto);
                $bancCodigo     = $boleto->getConfcont()->getBanc()->getBancCodigo();
                $sacado         = new \PhpBoletoZf2\Model\Sacado($arrDadosBoleto['sacado']);
                $cedente        = new \PhpBoletoZf2\Model\Cedente($arrDadosBoleto['cedente']);
                $arrDadosBoleto = $arrDadosBoleto['boleto'];

                /** @var \PhpBoletoZf2\Model\AbstractBoleto $boleto */
                /** @var \PhpBoletoZf2\Factory\AbstractBoletoFactory $objBoleto */
                if ($bancCodigo == \Financeiro\Service\BoletoBanco::BANCO_BRADESCO) {
                    $boleto    = new BoletoBancoob($arrDadosBoleto);
                    $objBoleto = $this->getServiceLocator()->get('Boleto\Bradesco');
                    $objBoleto->getBanco()->setAceite('N');
                } elseif ($bancCodigo == \Financeiro\Service\BoletoBanco::BANCO_BRASIL) {
                    //TODO: Analisar se o boleto do BB está funcionando corretamente
                    $boleto    = new BoletoBB($arrDadosBoleto);
                    $objBoleto = $this->getServiceLocator()->get('Boleto\BB');
                } elseif ($bancCodigo == \Financeiro\Service\BoletoBanco::BANCO_BANCOOB) {
                    $boleto    = new BoletoBancoob($arrDadosBoleto);
                    $objBoleto = $this->getServiceLocator()->get('Boleto\Bancoob');
                } elseif ($bancCodigo == \Financeiro\Service\BoletoBanco::BANCO_CAIXA) {
                    $boleto    = new BoletoCaixa($arrDadosBoleto);
                    $objBoleto = $this->getServiceLocator()->get('Boleto\CaixaSigcb');
                } else {
                    $boletosSemLayout[] = $idBoleto;
                    continue;
                }

                $objBoleto->setSacado($sacado)->setCedente($cedente)->setBoleto($boleto);

                if ($cedente->getCarteira()) {
                    $objBoleto->getBanco()->setCarteira($cedente->getCarteira());
                }

                if ($boleto->getAceite()) {
                    $objBoleto->getBanco()->setAceite($boleto->getAceite());
                }

                if ($boleto->getEspecieDoc()) {
                    $objBoleto->getBanco()->setEspecieDoc($boleto->getEspecieDoc());
                }

                $arrDados = clone($objBoleto->prepare());
                if (!empty($arrDados)) {
                    $arrRetorno[] = $arrDados;
                }
            }
        }

        $arrErros = [];

        if (empty($arrRetorno)) {
            return $this->retornaViewSimples("Erro: Nenhum boleto valido encontrado!", true);
        }

        if (!empty($arrBoletosFechados)) {
            $arrErros[] = "Boletos fechados: " . implode(",", $arrBoletosFechados);
        }

        if (!empty($arrBoletoNaoRegistrado)) {
            $arrErros[] = "Boletos não registrados: " . implode(",", $arrBoletoNaoRegistrado);
        }

        if (!empty($boletosSemLayout)) {
            $arrErros[] = (
                "Boletos sem layout, favor entrar em contato com o suporte!: " .
                implode(",", $boletosSemLayout)
            );
        }

        $this->getView()->setTemplate("/boleto/boleto/multiplos");
        $this->getView()->setVariable('arrDados', $arrRetorno);
        $this->getView()->setVariable('arrErros', $arrErros);
        $this->getView()->setTerminal(true);

        return $this->getView();
    }
}
