<?php


namespace Boleto\Controller;

use VersaSpine\Controller\AbstractCoreController;

use Respect\Validation\Validator as v;

use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class BoletoConfContaController extends AbstractCoreController{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {

    }

    public function buscaCnpjCpfAction(){
        $service = $this->services()->getService($this->getService());

        $data = $this->getRequest()->getPost()->toArray();

        if(v::cnpj()->validate($data['cnpjCpf'])){
            $repository = $service->getRepository("Pessoa\Entity\PessoaJuridica");
            $pessoaJuridica = $repository->findOneBy(array('pesCnpj' => $data['cnpjCpf']));
            $this->json->setVariable("nome", $pessoaJuridica->getPes()->getPesNome() );
            $this->json->setVariable("id", $pessoaJuridica->getPes()->getPesId() );

        }
        if(v::cpf()->validate($data['cnpjCpf'])){
            $this->json->setVariable("msg", "to aqui" );
            $repository = $service->getRepository("Pessoa\Entity\PessoaFisica");
            $pessoaFisica = $repository->findOneBy(array('pesCpf' => $data['cnpjCpf']));
            $this->json->setVariable("nome", $pessoaFisica->getPes()->getPesNome() );
            $this->json->setVariable("id", $pessoaFisica->getPes()->getPesId() );
        }
        return $this->json;

    }

    public function addAction(){
        $request = $this->getRequest();
        $service = $this->services()->getService( $this->getService() );
        $form = $this->forms()->mountForm( $this->getForm() );

        $bancos = $service->getRepository('Boleto\Entity\BoletoBanco')->buscaBancos();

        $form->get("banc")->setValueOptions($bancos);

        if ( $request->isPost() ){
            $file = new \GerenciadorArquivos\Service\Arquivo($this->getServiceLocator()->get("Doctrine\ORM\EntityManager"));
            $dados = array_merge($this->getRequest()->getPost()->toArray(),  $this->params()->fromFiles());
            $dados['pessoa'] = $dados['pes'];

            if(!empty($dados['file']['name'])){
                $arquivo = array(
                    'name' => $dados['file']['name'],
                    'type' => $dados['file']['type'],
                    'tmp_name' => $dados['file']['tmp_name'],
                    'error' => $dados['file']['error'],
                    'size' => $dados['file']['size'],
                    'pessoa' =>  $dados['pessoa'],
                );

                try{
                    $arquivo2 = $file->adicionar($arquivo);
                } catch (\Exception $ex) {
                    $erro = $ex->getMessage();
                    $form->setData( $request->getPost()->toArray() );
                }
                $dados['arq'] = $arquivo2->getArqId();
            }

            try{
                $ret = $service->adicionar($dados);

                if ( is_null($this->getRoute()) ){
                    $this->setRoute($this->getEvent()->getRouteMatch()->getMatchedRouteName());
                }

            } catch (\Exception $ex) {
                $erro = $ex->getMessage();
                $form->setData( $request->getPost()->toArray() );
            }

            $is_json = $request->getPost("is_json");
            if ( isset($erro) ){
                if ( $is_json ){
                    $this->json->setVariable("erro", $erro);
                    return $this->json;
                }
                $this->flashMessenger()->addErrorMessage($erro);

            } else {
                if ( $is_json ){
                    if ( method_exists($ret, "toArray") ){
                        $dados = $ret->toArray();
                    } else{
                        $dados = "Os dados foram inseridos corretamente, mas um erro ocorreu, verifique se existe o método toArray na entidade {$this->getEntity()}!";
                    }
                    $this->json->setVariable("result", $dados);
                    return $this->json;
                }

            }
            return $this->redirect()->toRoute($this->getRoute(), array('controller'=>$this->getController()));
        }

        $this->view->setVariable("form", $form);
        $this->view->setTemplate( $this->getTemplateToRoute() );
        return $this->view;
    }

    public function editAction(){
        $request = $this->getRequest();
        $form    = $this->forms()->mountForm( $this->getForm() );
        $service = $this->services()->getService( $this->getService() );

        $bancos = $service->getRepository('Boleto\Entity\BoletoBanco')->buscaBancos();

        $form->get("banc")->setValueOptions($bancos);

        if ( $request->isPost() ){
            $file = new \GerenciadorArquivos\Service\Arquivo($this->getServiceLocator()->get("Doctrine\ORM\EntityManager"));
            $dados = array_merge($this->getRequest()->getPost()->toArray(),  $this->params()->fromFiles());
            $dados['pessoa'] = $dados['pes'];


            if(!empty($dados['file']['name'])){
                $arquivo = array(
                    'name' => $dados['file']['name'],
                    'type' => $dados['file']['type'],
                    'tmp_name' => $dados['file']['tmp_name'],
                    'error' => $dados['file']['error'],
                    'size' => $dados['file']['size'],
                    'pessoa' =>  $dados['pessoa'],
                );

                try{
                    $arquivo2 = $file->adicionar($arquivo);
                } catch (\Exception $ex) {
                    $erro = $ex->getMessage();
                    $form->setData( $request->getPost()->toArray() );
                }
                $dados['arq'] = $arquivo2->getArqId();
            }
            try{
                $ret = $service->edita($dados);
                if ( is_null($this->getRoute()) ){
                    $this->setRoute($this->getEvent()->getRouteMatch()->getMatchedRouteName());
                }

            } catch (\Exception $ex) {
                $erro = $ex->getMessage();
                $form->setData( $request->getPost()->toArray() );
            }

            $is_json = $request->getPost("is_json");
            if ( isset($erro) ){
                if ( $is_json ){
                    $this->json->setVariable("erro", $erro);
                    return $this->json;
                }
                $this->flashMessenger()->addErrorMessage($erro);

            } else {
                if ( $is_json ){
                    if ( method_exists($ret, "toArray") ){
                        $dados = $ret->toArray();
                    } else{
                        $dados = "Os dados foram alterados corretamente, mas um erro ocorreu, verifique se existe o método toArray na entidade {$this->getEntity()}!";
                    }
                    $this->json->setVariable("result", $dados);
                    return $this->json;
                }

            }
            return $this->redirect()->toRoute($this->getRoute(), array('controller'=>$this->getController()));
        }

        $id = $this->params()->fromRoute("id",0);
        if ( !$id ){
            throw new \Exception("Para editar um registro é necessário informar o identificador do mesmo pela rota!", 1);
        }

        $reference = $service->getReference( $id );
        $dados = $reference->toArray();
        if($reference->getArq()){
            $dados['arq'] = $reference->getArq()->getArqId();
            $dados['file'] = $reference->getArq()->getArqNome();
        }

        //pre("Pessoa\Entity\Pessoa".$dados['pesTipo']);
        $repository = $service->getRepository("Pessoa\Entity\Pessoa".$dados['pesTipo']);
        $pessoa = $repository->findOneBy(array('pes' => $dados['pes']));
        $dados['pesCnpjCpf'] = $dados['pesTipo'] == "Juridica" ? $pessoa->getPesCnpj() : $pessoa->getPesCpf();


        $form->setData( $dados );


        $this->view->setVariable("form", $form);
        $this->view->setTemplate( $this->getTemplateToRoute() );

        return $this->view;
    }

    public function showFormAction(){
        $this->view->setTerminal(true);
        $form = $this->getForm();
        if ( !class_exists($form) ){
            $this->view->setVariable("erro", "Formulário de busca não foi encontrado!");
            return $this->view;
        }
        $form = new $form;
        $form->remove('pesCnpjCpf')
            ->remove('banc')
            ->remove('banc')
            ->remove('file');
        $form->remove('pesNome');
        $form->setAttribute("method", 'get');
        $this->view->setVariable("config", $this->config);
        $this->view->setVariable("form", $form);
        return $this->view;
    }
}