<?php


namespace Boleto\Controller;

use DOMPDFModule\View\Model\PdfModel;
use VersaSpine\Controller\AbstractCoreController;
use VersaSpine\Service\AbstractService;
use Zend\Stdlib\DateTime;
use Zend\View\Model\JsonModel;

class FinanceiroRelatoriosController extends AbstractCoreController

{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {
        $request          = $this->getRequest();
        $paramsGet        = $request->getQuery()->toArray();
        $paramsPost       = $request->getPost()->toArray();
        $objServiceBoleto = new \Boleto\Service\Boleto($this->getEntityManager());

        $result = $objServiceBoleto->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        return $this->view;
    }

    public function taxasAction()
    {
        $this->view->setTerminal(true);

        return $this->view;
    }

    public function mensalidadesAction()
    {
        $servicePeriodo   = $this->services()->getService('Matricula\Service\AcadperiodoLetivo');
        $serviceOrgCampus = new \Organizacao\Service\OrgCampus($this->getEntityManager());

        $periodo = $servicePeriodo->buscaPeriodoCorrente();
        $turnos  = $servicePeriodo->buscaTurnosPeriodo($periodo->getperId());
        $request = $this->getRequest();

        if ($request->isPost()) {
            $dados = $request->getPost()->toArray();
            /** @var  $snow \SnowReportsZf2\Service\SnowReportsZf2 */
            $snowZf2 = $this->getServiceLocator()->get('SnowReportsZf2\Service\SnowReportsZf2');

            $arrDadosInstituicao = $serviceOrgCampus->retornaDadosInstituicao();
            $service = $this->services()->getService($this->getService());

            if ($dados['forma-pagamento'] === 'pagos') {
                if (!empty($dados['dinheiro']) and !empty($dados['cheque'])) {
                    $dados['formas'] = 'dinheiro' . ',' . 'cheque';
                } elseif (!empty($dados['cheque'])) {
                    $dados['formas'] = 'cheque';
                } else {
                    $dados['formas'] = 'dinheiro';
                }
            }

            $turmas = explode(',',$dados['turmas'][0]);

            if(count($turmas) > 0){
                $arrTurmaId = $service->buscaTurmaNome($dados['turmas'][0]);

                foreach($arrTurmaId as $turma){
                    $arrTurmaNome[] =  $turma['turma_nome'];
                }
                $turmasDescrição = implode(',',$arrTurmaNome);
            }


            $formas = $dados['formas'];

            $subrelatorio = __DIR__ . '/../Reports/';

            if ($dados['modelo-relatorio'] == 'sintetico') {
                $dados['data-fim']    = $service::formatDateAmericano($dados['data-fim']);
                $dados['data-inicio'] = $service::formatDateAmericano($dados['data-inicio']);

                if ($dados['forma-pagamento'] == 'abertos') {
                    $ret = $snowZf2->setJrxmlPath(__DIR__ . '/../Reports/')
                        ->setJrxmlFile('mensalidade-sintetico-inadimplente.jrxml')
                        ->setAttrbutes(
                            [
                                'relatorioinfo' => (
                                    'Período entre ' . (new \DateTime($dados['data-inicio']))->format(
                                        'd/m/Y'
                                    ) . ' até ' .
                                    (new \DateTime($dados['data-fim']))->format('d/m/Y')
                                ),
                                'subrelatorio'      => $subrelatorio,
                                'instituicao'       => $arrDadosInstituicao['ies'],
                                'mantenedora'       => $arrDadosInstituicao['mantenedora'],
                                'endereco'          => $arrDadosInstituicao['endereco'],
                                'logo'              => $arrDadosInstituicao['logo'],
                                'turma_parametros'  =>$dados['turmas'][0],
                                'titulo'            => 'RELATÓRIO SINTÉTICO DOS ALUNOS INADIMPLENTES',
                                'datafim'           => $dados['data-fim'],
                                'datainicio'        => $dados['data-inicio'],
                                'turmas'            => $turmasDescrição
                            ]
                        )
                        ->setOutputFile(
                            $dados['modelo-relatorio'] . $dados['forma-pagamento'],
                            $dados['forma-relatorio']
                        )
                        ->downloadHtmlPdf(true)
                        ->execute();

                } else {
                    $ret = $snowZf2->setJrxmlPath(__DIR__ . '/../Reports/')
                        ->setJrxmlFile('mensalidade-sintetico-adimplente.jrxml')
                        ->setAttrbutes(
                            [
                                'relatorioinfo' =>
                                    (
                                        'Período entre ' . (new \DateTime($dados['data-inicio']))->format(
                                            'd/m/Y'
                                        ) . ' até ' .
                                        (new \DateTime($dados['data-fim']))->format('d/m/Y')
                                    ) . ' | Forma de pagamento: ' . str_replace('"', '', $formas),
                                'formas'            => $formas,
                                'subrelatorio'      => $subrelatorio,
                                'instituicao'       => $arrDadosInstituicao['ies'],
                                'mantenedora'       => $arrDadosInstituicao['mantenedora'],
                                'endereco'          => $arrDadosInstituicao['endereco'],
                                'logo'              => $arrDadosInstituicao['logo'],
                                'turma_parametros'  =>$dados['turmas'][0],
                                'titulo'            => 'RELATÓRIO SINTÉTICO DOS ALUNOS ADIMPLENTES',
                                'datafim'           => $dados['data-fim'],
                                'datainicio'        => $dados['data-inicio'],
                                'turmas'            => $turmasDescrição

                            ]
                        )
                        ->setOutputFile($dados['forma-pagamento'], $dados['forma-relatorio'])
                        ->downloadHtmlPdf(true)
                        ->execute();
                }

            }elseif ($dados['modelo-relatorio'] == 'analitico') {
                if ($dados['forma-pagamento'] == 'abertos') {
                    $relatorioinfo = 'Período entre '.$dados['data-inicio'].' até '.$dados['data-fim'];
                    $ret = $snowZf2->setJrxmlPath(__DIR__ . '/../Reports/')
                        ->setJrxmlFile("mensalidade-analitico-inadimplentes.jrxml")
                        ->setAttrbutes(
                            [
                                'subrelatorio'      => $subrelatorio,
                                'instituicao'       => $arrDadosInstituicao['ies'],
                                'mantenedora'       => $arrDadosInstituicao['mantenedora'],
                                'titulo'            => 'RELATÓRIO ANALÍTICO DOS ALUNOS INADIMPLENTES',
                                'logo'              => $arrDadosInstituicao['logo'],
                                'relatorioinfo'     => $relatorioinfo,
                                'datainicio'        => $service::formatDateAmericano($dados['data-inicio']),
                                'datafim'           => $service::formatDateAmericano($dados['data-fim']),
                                'turma_parametros'  =>$dados['turmas'][0],
                                'formapagamento'    => $dados['formas'],
                                'sistema'           => 'Universa',
                                'endereco'          => $arrDadosInstituicao['endereco'],
                                'turmas'            => $turmasDescrição

                            ]
                        )
                        ->setOutputFile(
                            $dados['modelo-relatorio'] . $dados['forma-pagamento'],
                            $dados['forma-relatorio']
                        )
                        ->downloadHtmlPdf(true)
                        ->execute();
                }else {
                    $relatorioinfo = 'Período entre '.$dados['data-inicio'].' até '.$dados['data-fim'].' | '.'Forma de pagamento: '.str_replace(',',', ',$dados['formas']);

                    $ret = $snowZf2->setJrxmlPath(__DIR__ . '/../Reports/')
                        ->setJrxmlFile("mensalidade-analitico-adimplentes.jrxml")
                        ->setAttrbutes(
                            [
                                'subrelatorio'      => $subrelatorio,
                                'instituicao'       => $arrDadosInstituicao['ies'],
                                'mantenedora'       => $arrDadosInstituicao['mantenedora'],
                                'titulo'            => 'RELATÓRIO ANALÍTICO DOS ALUNOS ADIMPLENTES',
                                'logo'              => $arrDadosInstituicao['logo'],
                                'relatorioinfo'     => $relatorioinfo,
                                'datainicio'        => $service::formatDateAmericano($dados['data-inicio']),
                                'datafim'           => $service::formatDateAmericano($dados['data-fim']),
                                'formapagamento'    => $formas,
                                'sistema'           => 'Universa',
                                'endereco'          => $arrDadosInstituicao['endereco'],
                                'turmas'            => $turmasDescrição,
                                'turma_parametros'  =>$dados['turmas'][0]
                            ]
                        )
                        ->setOutputFile(
                            $dados['modelo-relatorio'] . $dados['forma-pagamento'],
                            $dados['forma-relatorio']
                        )
                        ->downloadHtmlPdf(true)
                        ->execute();
                }

            } else {
                $datainicio = AbstractService::formatDateAmericano($dados['data-inicio']);
                $datainicio = (new \DateTime($datainicio))->format('Ymd');

                $datafim = AbstractService::formatDateAmericano($dados['data-fim']);
                $datafim = (new \DateTime($datafim))->format('Ymd');

                $relatorioinfo = 'Período entre ' . (new \DateTime($datainicio))->format('d/m/Y');
                $relatorioinfo.= ' até '.(new \DateTime($datafim))->format('d/m/Y');

                $subreport = __DIR__ . '/../Reports/';

                $ret           = $snowZf2->setJrxmlPath(__DIR__ . '/../Reports/')
                    ->setJrxmlFile('mensalidade-integral.jrxml')
                    ->setAttrbutes(
                        [
                            'subrelatorio'      => $subreport,
                            'relatorioinfo'     => $relatorioinfo,
                            'instituicao'       => $arrDadosInstituicao['ies'],
                            'mantenedora'       => $arrDadosInstituicao['mantenedora'],
                            'endereco'          => $arrDadosInstituicao['endereco'],
                            'logo'              => $arrDadosInstituicao['logo'],
                            'titulo'            => 'PAGAMENTO INTEGRAL',
                            'datafim'           => $datafim,
                            'datainicio'        => $datainicio,
                            'turmas'            => $turmasDescrição,
                            'turma_parametros'  =>$dados['turmas'][0]

                        ]
                    )
                    ->setOutputFile(
                        $dados['modelo-relatorio'] . $dados['forma-pagamento'],
                        $dados['forma-relatorio']
                    )
                    ->downloadHtmlPdf(true)
                    ->execute();
            }

            if (!$ret) {
                pre($snowZf2->getErrors());
            }
        }

        $this->view->setTerminal(true);

        return $this->getView();
    }

    public function chequesAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $pdf              = new PdfModel();
            $dados            = $request->getPost()->toArray();
            $service          = $this->Services()->getService($this->getService());
            $serviceOrgCampus = new \Organizacao\Service\OrgCampus($this->getEntityManager());

            $pdf->setVariables($serviceOrgCampus->retornaDadosInstituicao());

            $cheques = $service->chequesPeriodo(
                (new \DateTime(str_replace("/", "-", $dados['data-inicio'])))->format("Y-m-d"),
                (new \DateTime(str_replace("/", "-", $dados['data-fim'])))->format("Y-m-d"),
                $dados['estado-cheque']
            );

            $pdf->setOption('paperOrientation', 'landscape'); // Defaults to "portrait"

            $pdf->setVariables(
                [
                    'dados'    => $cheques,
                    'dataBase' => (new \DateTime('now'))->format("d-m-Y H:i"),
                    'info'     => $dados

                ]
            );

            if ($dados['modelo-relatorio-cheque'] == "boleto/financeiro-relatorios/cheques-analitico" AND count(
                    $cheques
                ) > 0
            ) {
                $aluno[0]['aluno']      = [
                    'aluno'         => $cheques[0]['aluno'],
                    'alunocurso_id' => $cheques[0]['alunocurso_id']
                ];
                $aluno[0]['cheques'][0] = [
                    'cheque_emitente'     => $cheques[0]['cheque_emitente'],
                    'cheque_valor'        => $cheques[0]['cheque_valor'],
                    'cheque_vencimento'   => $cheques[0]['cheque_vencimento'],
                    'titulo_valor'        => $cheques[0]['titulo_valor'],
                    'titulocheque_estado' => $cheques[0]['titulocheque_estado'],
                    'titulochque_valor'   => $cheques[0]['titulocheque_valor'],
                    'mensalidade_parcela' => $cheques[0]['mensalidade_parcela'],
                ];
                $aluno[0]['totais']     = [
                    'total_cheque'   => $cheques[0]['cheque_valor'],
                    'total_recebido' => $cheques[0]['titulocheque_valor']
                ];

                for ($i = 1; $i < count($cheques); $i++) {
                    if ($cheques[$i]['alunocurso_id'] == $aluno[count($aluno) - 1]['aluno']['alunocurso_id']) {
                        $aluno[count($aluno) - 1]['cheques'][count($aluno[count($aluno) - 1]['cheques']) + 1] = [
                            'cheque_emitente'     => $cheques[$i]['cheque_emitente'],
                            'cheque_valor'        => $cheques[$i]['cheque_valor'],
                            'cheque_vencimento'   => $cheques[$i]['cheque_vencimento'],
                            'titulo_valor'        => $cheques[$i]['titulo_valor'],
                            'titulocheque_estado' => $cheques[$i]['titulocheque_estado'],
                            'titulochque_valor'   => $cheques[$i]['titulocheque_valor'],
                            'mensalidade_parcela' => $cheques[$i]['mensalidade_parcela'],
                        ];
                        $aluno[count($aluno) - 1]['totais']                                                   = [
                            'total_cheque'   => $aluno[count(
                                    $aluno
                                ) - 1]['totais']['total_cheque'] + $cheques[$i]['cheque_valor'],
                            'total_recebido' => $aluno[count(
                                    $aluno
                                ) - 1]['totais']['total_recebido'] + $cheques[$i]['titulocheque_valor']
                        ];
                    } else {
                        $pos                       = count($aluno);
                        $aluno[$pos]['aluno']      = [
                            'aluno'         => $cheques[$i]['aluno'],
                            'alunocurso_id' => $cheques[$i]['alunocurso_id']
                        ];
                        $aluno[$pos]['cheques'][0] = [
                            'cheque_emitente'     => $cheques[$i]['cheque_emitente'],
                            'cheque_valor'        => $cheques[$i]['cheque_valor'],
                            'cheque_vencimento'   => $cheques[$i]['cheque_vencimento'],
                            'titulo_valor'        => $cheques[$i]['titulo_valor'],
                            'titulocheque_estado' => $cheques[$i]['titulocheque_estado'],
                            'titulochque_valor'   => $cheques[$i]['titulocheque_valor'],
                            'mensalidade_parcela' => $cheques[$i]['mensalidade_parcela'],
                        ];
                        $aluno[$pos]['totais']     = [
                            'total_cheque'   => $cheques[$i]['cheque_valor'],
                            'total_recebido' => $cheques[$i]['titulocheque_valor']
                        ];
                    }
                }
            }

            $pdf->setTemplate($dados['modelo-relatorio-cheque']);

            return $pdf;
        }

        $this->view->setVariables(
            [
                'dataAtual' => (new \DateTime('now'))->format("d/m/Y"),
            ]
        );

        $this->view->setTerminal(true);

        return $this->view;
    }

    public function descontosAction()
    {
        $servicePeriodo   = $this->services()->getService('Matricula\Service\AcadperiodoLetivo');
        $service          = $this->Services()->getService($this->getService());
        $serviceOrgCampus = new \Organizacao\Service\OrgCampus($this->getEntityManager());

        $tiposDescontos = $service->buscaTipoDescontos();
        $periodo        = $servicePeriodo->buscaPeriodoAtual();
        $turnos         = $servicePeriodo->buscaTurnosPeriodo($periodo->getperId());
        $request        = $this->getRequest();

        if ($request->isPost()) {
            $dados = $request->getPost()->toArray();
            $pdf   = new PdfModel();

            $pdf->setVariables($serviceOrgCampus->retornaDadosInstituicao());

            $pagina = 'landscape';
            if ($dados['modelo-relatorio'] == "/boleto/financeiro-relatorios/descontos-analitico") {
                if ($dados['periodo-turmas'] == "/boleto/financeiro-relatorios/descontos-analitico-alunos") {
                    $descAlunos = $service->buscaDescontosAlunosPeriodoLetivo(
                        $periodo->getperId(),
                        $dados['turnos'],
                        $dados['tipo_desconto']
                    );

                    if (!empty($descAlunos) AND !is_null($descAlunos)) {
                        $aluno[0]['aluno']        = [
                            'matricula' => $descAlunos[0]['alunocurso_id'],
                            'nome'      => $descAlunos[0]['pes_nome'],
                            'turma'     => $descAlunos[0]['turma_nome'],
                            'serie'     => $descAlunos[0]['turma_serie'],
                            'turno'     => $descAlunos[0]['turma_turno'],
                        ];
                        $aluno[0]['descontos'][0] = [
                            'valor'       => '',
                            'mensalidade' => \Boleto\Service\FinanceiroRelatorios::mesReferencia(
                                $descAlunos[0]['parcela']
                            ),
                            'status'      => $descAlunos[0]['desconto_status'],
                            'valor'       => $descAlunos[0]['desconto_valor'],
                            'tipo'        => $descAlunos[0]['desctipo_modalidade'],
                            'descricao'   => $descAlunos[0]['desctipo_descricao'],

                        ];
                        if ($descAlunos[0]['descmensalidade_status'] === 'Ativo') {
                            $aluno[0]['descontosAtivos'] = [
                                'totalAtivo' => $descAlunos[0]['desconto_valor']
                            ];
                        } else {
                            $aluno[0]['descontosInativo'] = [
                                'totalInativo' => $descAlunos[0]['desconto_valor']
                            ];
                        }

                        for ($i = 1; $i < count($descAlunos); $i++) {
                            $contAlunos = count($aluno) - 1;
                            if ($descAlunos[$i]['alunocurso_id'] == $aluno[$contAlunos]['aluno']['matricula']) {
                                $aluno[$contAlunos]['descontos'][count($aluno[$contAlunos]['descontos'])] = [
                                    'valor'       => '',
                                    'mensalidade' => \Boleto\Service\FinanceiroRelatorios::mesReferencia(
                                        $descAlunos[$i]['parcela']
                                    ),
                                    'status'      => $descAlunos[$i]['desconto_status'],
                                    'valor'       => $descAlunos[$i]['desconto_valor'],
                                    'tipo'        => $descAlunos[$i]['desctipo_modalidade'],
                                    'descricao'   => $descAlunos[$i]['desctipo_descricao'],

                                ];
                                if ($descAlunos[$i]['descmensalidade_status'] === 'Ativo') {
                                    $aluno[$contAlunos]['descontosAtivos'] = [
                                        'totalAtivo' => $descAlunos[$i]['desconto_valor'] + $aluno[$contAlunos]['descontosAtivos']['totalAtivo']
                                    ];
                                } else {
                                    $aluno[$contAlunos]['descontosInativo'] = [
                                        'totalInativo' => $descAlunos[$i]['desconto_valor'] + $aluno[$contAlunos]['descontosInativo']['totalInativo']
                                    ];
                                }
                            } else {
                                $pos                         = count($aluno);
                                $aluno[$pos]['aluno']        = [
                                    'matricula' => $descAlunos[$i]['alunocurso_id'],
                                    'nome'      => $descAlunos[$i]['pes_nome'],
                                    'turma'     => $descAlunos[$i]['turma_nome'],
                                    'serie'     => $descAlunos[$i]['turma_serie'],
                                    'turno'     => $descAlunos[$i]['turma_turno'],
                                ];
                                $aluno[$pos]['descontos'][0] = [
                                    'valor'       => '',
                                    'mensalidade' => \Boleto\Service\FinanceiroRelatorios::mesReferencia(
                                        $descAlunos[$i]['parcela']
                                    ),
                                    'status'      => $descAlunos[$i]['desconto_status'],
                                    'valor'       => $descAlunos[$i]['desconto_valor'],
                                    'tipo'        => $descAlunos[$i]['desctipo_modalidade'],
                                    'descricao'   => $descAlunos[$i]['desctipo_descricao'],

                                ];
                                if ($descAlunos[$i]['descmensalidade_status'] === 'Ativo') {
                                    $aluno[$pos]['descontosAtivos'] = [
                                        'totalAtivo' => $descAlunos[$i]['desconto_valor']
                                    ];
                                } else {
                                    $aluno[$pos]['descontosInativo'] = [
                                        'totalInativo' => $descAlunos[$i]['desconto_valor']
                                    ];
                                }
                            }
                        }
                    }

                    $pdf->setVariables(
                        [
                            'descAlunos' => $aluno,
                            'dados'      => $dados,
                            'semestre'   => $periodo->getperNome(),
                            'ano'        => $periodo->getperAno(),
                            'dataAtual'  => (new \DateTime('now'))->format("d/m/Y H:i:s"),
                        ]
                    );
                    $pdf->setOption('paperOrientation', 'landscape'); // Defaults to "portrait"

                    $pdf->setTemplate($dados['periodo-turmas']);

                    return $pdf;
                } else {
                    $descontos = $service->buscaDescontosAnaliticos(
                        $periodo->getperId(),
                        $dados['turnos'],
                        $turma = 'Todos'
                    );
                }
            } else {
                if ($dados['modelo-relatorio-sintetico'] == 'descontos') {
                    $descontos   = $service->buscaDescontosSintetico($periodo->getperId());
                    $mensalidade = $service->getRepository('Boleto\Entity\FinanceiroValores')->findOneBy(
                        ['per' => 2, 'tipotitulo' => 2]
                    );
                } else {
                    $descontos                 = $service->buscaDescontoAlunoSintetico($periodo->getperId());
                    $dados['modelo-relatorio'] = 'boleto/financeiro-relatorios/descontos-sintetico-aluno';
                    $pagina                    = 'portrait';
                }
                if ($mensalidade) {
                    $mensalidadeValor = $mensalidade->getValoresPreco();
                }
            }

            $pdf->setVariables(
                [
                    'dados'            => $dados,
                    'semestre'         => $periodo->getperNome(),
                    'ano'              => $periodo->getperAno(),
                    'dataAtual'        => (new \DateTime('now'))->format("d/m/Y H:i:s"),
                    'descontos'        => $descontos,
                    'mensalidadeValor' => $mensalidadeValor,
                ]
            );

            $pdf->setOption('filename', str_replace('/', '_', $dados['modelo-relatorio']));

            $pdf->setOption('paperOrientation', $pagina); // Defaults to "portrait"

            $pdf->setTemplate($dados['modelo-relatorio']);

            return $pdf;
        }

        $this->view->setVariables(
            [
                'data'      => (new \DateTime('now'))->format("d/m/Y"),
                'descontos' => $tiposDescontos,
            ]
        );
        $this->view->setTerminal(true);

        return $this->view;
    }

    public function buscaTurmasAction()
    {
        $json           = new JsonModel();
        $servicePeriodo = $this->services()->getService('Matricula\Service\AcadperiodoLetivo');
        $perId          = $this->getRequest()->getPost("periodoId");
        $campusCursoId          = $this->getRequest()->getPost("campusCursosId");
        if (is_null($servicePeriodo->getRepository('Matricula\Entity\AcadperiodoTurma')->findAll())) {
            $periodo = $servicePeriodo->buscaPeriodoAtual();
            $json->setVariable(
                'dados',
                $servicePeriodo->buscaTurmasPeriodoTurno($this->getRequest()->getPost("turno"), $periodo->getperId())
            );
        } else {
            $turmas = $servicePeriodo->getRepository('Matricula\Entity\AcadperiodoTurma')->findBy(
                ['per' => $perId,'cursocampus'=>$campusCursoId],
                ['turmaSerie' => 'ASC']
            );
            foreach ($turmas as $cont => $array) {
                $turmas[$cont] = $array->toArray();
            }
            $json->setVariable('dados', $turmas);
        }

        return $json;
    }

    public function buscaAlunoAction()
    {
        $service = $this->Services()->getService($this->getService());
        $request = $this->getRequest();

        if ($request->isPost()) {
        }
    }

    public function dependenciaAdaptacaoAction()
    {
        $service          = new \Boleto\Service\FinanceiroRelatorios($this->getEntityManager());
        $objServiceCampus = new \Organizacao\Service\OrgCampus($this->getEntityManager());
        $serviceAcadPeriodoLetivo = new \Matricula\Service\AcadperiodoLetivo($this->getEntityManager());
        $snowZf2          = $this->getServiceLocator()->get('SnowReportsZf2\Service\SnowReportsZf2');
        $request          = $this->getRequest();

        if ($request->isPost()) {
            $dados = $request->getPost()->toArray();

            $dadosinstituicao = $objServiceCampus->retornaDadosInstituicao();

            $datainicial = AbstractService::formatDateAmericano($dados['dataInicial']);
            $datainicial = (new \DateTime($datainicial))->format("Ymd");

            $datafinal  = (new \DateTime(AbstractService::formatDateAmericano($dados['dataFinal'])))->format(
                "Ymd"
            );
            $tituloTipo = '"' . $dados['titulotipo'] . '"';
            $situacao   = '"' . implode($dados['situacao'], '","') . '"';
            $turmas     = implode($dados['turmas'], ",");

            try {
                $objPerioodoAutal=$serviceAcadPeriodoLetivo->buscaPeriodoCorrente();

                $semestre = $objPerioodoAutal->getPerNome();

                $ArrTurmaNome = $service->buscaTurmaNome($turmas);

                foreach($ArrTurmaNome as $nome=>$i) {
                    $turmaNome[$nome] = $i['turma_nome'];
                }

                $situacaoDesc = $situacao == '"27","26"' ?
                    "Dependência e Adaptação" :
                    ($situacao == '"27"' ? "Adaptação" : "Dependência");

                $relatorioinfo = (
                    $semestre.' | '.
                    'Período: ' . $dados['dataInicial'] . ' - ' . $dados['dataFinal'] . ' | ' .
                    'Situação: ' . str_replace('"', '', str_replace('","', ', ', $tituloTipo)) . ' | ' .
                    'Tipo de Título: ' . $situacaoDesc . ' | '
                );
                $relatorioinfo.="Turma: ";
                $relatorioinfo.=$turmaNome?implode($turmaNome,', '):'Todas';

                $subrelatorio = __DIR__ . '/../Reports/';

                $ret = $snowZf2->setJrxmlPath(__DIR__ . '/../Reports/')
                    ->setJrxmlFile("dependencia-adaptacao.jrxml")
                    ->setAttrbutes(
                        [
                            'subrelatorio'    => $subrelatorio,
                            'logo'            => $dadosinstituicao['logo'],
                            'mantenedora'     => $dadosinstituicao['mantenedora'],
                            'instituicao'     => $dadosinstituicao['ies'],
                            'datainicial'     => $datainicial,
                            'datafinal'       => $datafinal,
                            'relatorioinfo'   => $relatorioinfo,
                            'instituicaoinfo' => 'Relatório Financeiro',
                            'titulo'          => 'Dependências e Adaptações',
                            'endereco'        => 'Rua '.$dadosinstituicao['endereco'],
                            'sistema'         => 'Universa',
                            'titulotipo'      => $tituloTipo == '""'?null:$tituloTipo,
                            'situacao'        => $situacao == '""'?null:$situacao,
                            'turma'           => $turmas?$turmas:null
                        ]
                    )
                    ->setOutputFile(
                        $dados['modelo-relatorio'] . $dados['forma-pagamento'],
                        $dados['forma-relatorio']
                    )
                    ->execute();
            } catch (\Exception $e) {
                echo "Impossível imprimir relatório\nCausa-> " . $e->getLine();
            }
        }

        $this->getView()->setTerminal(true);

        return $this->getView();
    }

    public function multasJurosAction()
    {
        $servicePeriodo   = $this->services()->getService('Matricula\Service\AcadperiodoLetivo');
        $serviceOrgCampus = new \Organizacao\Service\OrgCampus($this->getEntityManager());

        $periodo = $servicePeriodo->buscaPeriodoAtual();
        $request = $this->getRequest();

        if ($request->isPost()) {
            $dados = $request->getPost()->toArray();
            /** @var  $snow \SnowReportsZf2\Service\SnowReportsZf2 */
            $snowZf2 = $this->getServiceLocator()->get('SnowReportsZf2\Service\SnowReportsZf2');

            $arrDadosInstituicao = $serviceOrgCampus->retornaDadosInstituicao();
            $service             = $this->services()->getService($this->getService());

            if ($dados['forma-pagamento'] == 'pagos') {
                if (!empty($dados['dinheiro']) and !empty($dados['cheque'])) {
                    $dados['formas'] = 'Dinheiro' . ',' . 'Cheque';
                } elseif (!empty($dados['cheque'])) {
                    $dados['formas'] = 'Cheque';
                } else {
                    $dados['formas'] = 'Dinheiro';
                }
            }

            $formas = '"' . $dados['formas'] . '"';

            if ($dados['modelo-relatorio'] == 'sintetico') {
                $relatorioinfo = 'Modelo: ' . ucfirst($dados['modelo-relatorio']) . ' | ';
                $relatorioinfo .= $dados['formas'] != null ?
                    'Forma de pagamento: ' . (str_replace(',', ', ', $dados['formas'])) . ' | ' : '';
                $relatorioinfo .= 'Período entre ' . $dados['data-inicio'] . ' até ' . $dados['data-fim'];

                $titulo = $dados['forma-pagamento'] == 'pagos' ?
                    'RELATÓRIO SINTÉTICO DOS ALUNOS ADIMPLENTES' : 'RELATÓRIO SINTÉTICO DOS ALUNOS INADIMPLENTES';

                $subrelatorio = __DIR__ . '/../Reports/';

                $ret = $snowZf2->setJrxmlPath(__DIR__ . '/../Reports/')
                    ->setJrxmlFile("multa-juros-sintetico.jrxml")
                    ->setAttrbutes(
                        [
                            'subrelatorio'  => $subrelatorio,
                            'instituicao'   => $arrDadosInstituicao['ies'],
                            'mantenedora'   => $arrDadosInstituicao['mantenedora'],
                            'titulo'        => $titulo,
                            'logo'          => $arrDadosInstituicao['logo'],
                            'relatorioinfo' => $relatorioinfo,
                            'estado'        => $dados['forma-pagamento'],
                            'tipo'          => $formas != '""' ? $formas : null,
                            'datainicio'    => $service::formatDateAmericano($dados['data-inicio']),
                            'datafim'       => $service::formatDateAmericano($dados['data-fim']),
                            'sistema'       => 'Universa',
                            'endereco'      => $arrDadosInstituicao['endereco'],
                        ]
                    )
                    ->setOutputFile(
                        $dados['modelo-relatorio'] . $dados['forma-pagamento'],
                        $dados['forma-relatorio']
                    )
                    ->execute();
            } else {
                $relatorioinfo = 'Modelo: ' . ucfirst($dados['modelo-relatorio']) . ' | ';
                $relatorioinfo .= 'Forma de pagamento: ' .
                $dados['formas'] != null ?
                    (str_replace(',', ', ', $dados['formas'])) . ' | ' : '';
                $relatorioinfo .= 'Período entre ' . $dados['data-inicio'] . ' até ' . $dados['data-fim'];

                $titulo = $dados['forma-pagamento'] == 'pagos' ?
                    'RELATÓRIO ANALÍTICO DOS ALUNOS ADIMPLENTES' : 'RELATÓRIO ANALÍTICO DOS ALUNOS INADIMPLENTES';

                $subrelatorio = __DIR__ . '/../Reports/';

                $ret = $snowZf2->setJrxmlPath(__DIR__ . '/../Reports/')
                    ->setJrxmlFile("multa-juros-analitico.jrxml")
                    ->setAttrbutes(
                        [
                            'subrelatorio'  => $subrelatorio,
                            'instituicao'   => $arrDadosInstituicao['ies'],
                            'mantenedora'   => $arrDadosInstituicao['mantenedora'],
                            'titulo'        => $titulo,
                            'logo'          => $arrDadosInstituicao['logo'],
                            'relatorioinfo' => $relatorioinfo,
                            'estado'        => $dados['forma-pagamento'],
                            'tipo'          => $formas != '""' ? $formas : null,
                            'datainicio'    => $service::formatDateAmericano($dados['data-inicio']),
                            'datafim'       => $service::formatDateAmericano($dados['data-fim']),
                            'sistema'       => 'Universa',
                            'endereco'      => $arrDadosInstituicao['endereco'],
                        ]
                    )
                    ->setOutputFile(
                        $dados['modelo-relatorio'] . $dados['forma-pagamento'],
                        $dados['forma-relatorio']
                    )
                    ->execute();
            }
        }

        $this->getView()->setTerminal(true);

        return $this->getView();
    }

}
