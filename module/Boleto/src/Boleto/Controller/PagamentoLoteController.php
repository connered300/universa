<?php


namespace Boleto\Controller;

use VersaSpine\Controller\AbstractCoreController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

use VersaSpine\Service\AbstractService;

class PagamentoLoteController extends AbstractCoreController
{
    public function __construct()
    {
        $this->view = new ViewModel();
        $this->json = new JsonModel();
    }

    public function indexAction()
    {
        $alunoperId = $this->params()->fromRoute('id');
        if ($alunoperId) {
            $this->view->setVariable('alunocursoId', $alunoperId);
        }

        return $this->view;
    }

    public function listagemAlunosAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $arrayData = $request->getPost()->toArray();

            $serviceFinanceiro = $this->getServiceLocator()->get('Boleto\Service\Financeiro');
            $result            = $serviceFinanceiro->paginationAjax($arrayData);

            $this->json->setVariable("draw", $result["draw"]);
            $this->json->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->json->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->json->setVariable("data", $result["data"]);
        }

        return $this->json;
    }

}
