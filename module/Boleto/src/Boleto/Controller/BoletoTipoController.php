<?php


namespace Boleto\Controller;

use VersaSpine\Controller\AbstractCoreController;

use Zend\View\Model\ViewModel;

class BoletoTipoController extends AbstractCoreController{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {

    }
}