<?php

namespace Boleto\Controller;

use VersaSpine\Controller\AbstractCoreController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Class FinanceiroController
 * @package Boleto\Controller
 */
class FinanceiroController extends AbstractCoreController
{
    public function __construct()
    {
        $this->view = new ViewModel();
        $this->json = new JsonModel();
    }

    /**
     * Tela inicial do financeiro
     * @return ViewModel
     */
    public function indexAction()
    {
        $alunoperId = $this->params()->fromRoute('id');
        if ($alunoperId) {
            $this->view->setVariable('alunocursoId', $alunoperId);
        }

        return $this->view;
    }

    /**
     * Retorna listagem de alunos para ser usado em datatables
     * @return JsonModel
     */
    public function listagemAlunosAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $arrayData = $request->getPost()->toArray();

            $serviceFinanceiro = $this->getServiceLocator()->get('Boleto\Service\Financeiro');
            $result            = $serviceFinanceiro->paginationAjax($arrayData);

            $this->json->setVariable("draw", $result["draw"]);
            $this->json->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->json->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->json->setVariable("data", $result["data"]);
        }

        return $this->json;
    }

    /**
     * Painel financeiro do aluno
     * @return ViewModel
     */
    public function dadosAlunoAction()
    {
        $alunoperId                 = $this->params()->fromRoute('id');
        $usuario                    = $_SESSION['Zend_Auth']['storage']['usuario'];
        $serviceAcadgeralAlunoCurso = new \Matricula\Service\AcadgeralAlunoCurso($this->getEntityManager());
        $serviceAcadPeriodoAluno    = new \Matricula\Service\AcadperiodoAluno($this->getEntityManager());
        $serviceFinanceiroTitulo    = new \Boleto\Service\FinanceiroTitulo($this->getEntityManager());
        $serviceFinanceiroDesconto  = new \Boleto\Service\FinanceiroDesconto($this->getEntityManager());
        $serviceBancos              = new \Boleto\Service\BoletoBanco($this->getEntityManager());
        $serviceFinanceiro          = new \Boleto\Service\Financeiro($this->getEntityManager());
        $servicePessoa              = new \Pessoa\Service\Pessoa($this->getEntityManager());

        $grupoTesouraria = $serviceFinanceiro->grupoTesousaria($usuario);

        try {
            $dadosAluno   = $serviceAcadPeriodoAluno->buscaAlunoPerido($alunoperId);
            $alunocursoId = $dadosAluno['alunoperCurso']->getAlunocursoId();
        } catch (\Exception $e) {
            $dadosAluno   = $serviceAcadgeralAlunoCurso->getAlunocursoArray($alunoperId);
            $alunocursoId = $alunoperId;
            $alunoperId   = '';
        }

        if ($dadosAluno) {
            $dadosPessoa  = $servicePessoa->buscaDadosPessoa($dadosAluno['pes'], 'f', true, true);
            $dadosAluno   = array_merge($dadosAluno, $dadosPessoa);
            $mensalidades = $serviceFinanceiroTitulo->buscaMensalidadesAtivasAluno($alunocursoId);

            if ($mensalidades) {
                $dadosAluno = array_merge($dadosAluno, $mensalidades);
            }

            $taxas = $serviceFinanceiroTitulo->buscaTaxasAtivasAluno(
                $alunocursoId,
                array(
                    'tipoTitulo' => "NOT IN ('Adaptação', 'Dependência', 'Monografia','Mensalidade')",
                    'estado'     => " = 'Aberto'"
                ),
                false
            );

            if ($taxas) {
                $dadosAluno = array_merge($dadosAluno, $taxas);
            }

            $listaTaxas['listaTaxas'] = $serviceFinanceiroTitulo->buscaTaxas();

            if ($listaTaxas) {
                $dadosAluno = array_merge($dadosAluno, $listaTaxas);
            }

            $dependencias = $serviceFinanceiroTitulo->buscaMensalidadesAtivasAluno(
                $alunocursoId,
                \Boleto\Service\FinanceiroTituloTipo::DEPENDENCIA
            );

            if ($dependencias) {
                $dadosAluno = array_merge($dadosAluno, $dependencias);
            }

            $adaptacoes = $serviceFinanceiroTitulo->buscaMensalidadesAtivasAluno(
                $alunocursoId,
                \Boleto\Service\FinanceiroTituloTipo::ADAPTACAO
            );

            if ($adaptacoes) {
                $dadosAluno = array_merge($dadosAluno, $adaptacoes);
            }

            $monografias = $serviceFinanceiroTitulo->buscaMensalidadesAtivasAluno(
                $alunocursoId,
                \Boleto\Service\FinanceiroTituloTipo::MONOGRAFICA
            );

            if ($monografias) {
                $dadosAluno = array_merge($dadosAluno, $monografias);
            }

            $formasPagamento['formasPagamento'] = \Boleto\Entity\FinanceiroTitulo::$formasPagamento;

            if ($formasPagamento) {
                $dadosAluno = array_merge($dadosAluno, $formasPagamento);
            }
            /*
             * Todo:Analisar impacto da remoção do trecho de abaixo.
            $historicoFinanceiro['historicoFinanceiro'] = $serviceFinanceiroTitulo->buscaTitulosPagosPes(
                $dadosAluno['alunoperId']
            );*/

            $historicoFinanceiro['historicoCancelamento'] = $serviceFinanceiroTitulo->buscaTitulosCanceladosAluno(
                $dadosAluno['alunoperId']
            );

            if ($historicoFinanceiro) {
                $dadosAluno = array_merge($dadosAluno, $historicoFinanceiro);
            }

            $objAlunoperiodos = $serviceAcadPeriodoAluno->getRepository()->findBy(
                ['alunocurso' => $dadosAluno['alunocurso']]
            );

            foreach ($objAlunoperiodos as $key => $alunoperiodo) {
                $descontos['descontos'][$key] = $serviceFinanceiroDesconto->buscaDescontosAluno(
                    $alunoperiodo->getAlunoperId()
                );
            }

            $descontosSemestreCorrente = $serviceFinanceiroDesconto->buscaDescontosAluno($dadosAluno['alunoperId']);

            if ($descontosSemestreCorrente) {
                $totalPercentualDesconto = 0;

                foreach ($descontosSemestreCorrente as $desconto) {
                    $totalPercentualDesconto += $desconto['descontoPercentual'];
                }

                $dadosAluno                            = array_merge($dadosAluno, $descontos);
                $dadosAluno['totalPercentualDesconto'] = $totalPercentualDesconto;
            }

            $cheques['cheques'] = $serviceFinanceiroTitulo->buscaChequesAtivosAluno(
                $alunocursoId
            );

            if ($cheques) {
                $dadosAluno = array_merge($dadosAluno, $cheques);
            }

            $bancos['bancos'] = $serviceBancos->getRepository('Boleto\Entity\BoletoBanco')->buscaBancosCodNome();

            if ($bancos) {
                $dadosAluno = array_merge($dadosAluno, $bancos);
            }

            $dadosAluno['periodosLetivos']  = $serviceAcadPeriodoAluno->periodosLetivosAluno($dadosAluno['alunocurso']);
            $dadosAluno['tiposMensalidade'] = $serviceFinanceiroTitulo->buscaTiposMensalidade();
        }

        $alunoDesligado = false;

        if ($alunoperId) {
            /* @var $objAcadperiodoAluno \Matricula\Entity\AcadperiodoAluno */
            $objAcadperiodoAluno = $serviceAcadPeriodoAluno->getRepository()->findOneBy(['alunoperId' => $alunoperId]);
            $alunoDesligado      = $serviceAcadPeriodoAluno->verificaSeAlunnoEstaCancelado($objAcadperiodoAluno);
        }

        return $this->getView()->setVariables(
            [
                'dadosAluno'      => $dadosAluno,
                'grupoTesouraria' => $grupoTesouraria,
                'alunoDesligado'  => $alunoDesligado
            ]
        );
    }

    /**
     * retorno informações do título
     * @return JsonModel
     */
    public function dadosTituloAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $arrayData         = $request->getPost()->toArray();
            $serviceFinanceiro = new \Boleto\Service\Financeiro($this->getEntityManager());

            try {
                $titulo            = $serviceFinanceiro->buscaTitulo($arrayData['tituloId']);
                $arrDescontoValido = \Boleto\Service\FinanceiroTitulo::validaDescontos($titulo);

                // atualiza valores de desconto do titulo e valor a receber
                $titulo['tituloDesconto'] -= $arrDescontoValido['vlrTotalInvalido'];
                $titulo['valorReceber'] += $arrDescontoValido['vlrTotalInvalido'];
                $titulo['descontos'] = $arrDescontoValido['descontosValidos'];

                $this->getJson()->setVariable('titulo', $titulo);
            } catch (\Exception $ex) {
                $this->getJson()->setVariable('error', $ex->getMessage());
            }
        } else {
            $this->getJson()->setVariable('error', 'Titulo não informado');
        }

        return $this->getJson();
    }

    /**
     * Retorna informações de um cheque e titulos vinculados a ele
     * @return JsonModel|ViewModel
     */
    public function dadosChequeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $arrayData               = $request->getPost()->toArray();
            $serviceFinanceiroCheque = $this->getServiceLocator()->get('Boleto\Service\FinanceiroCheque');
            $result                  = $serviceFinanceiroCheque->buscaDadosCheque($arrayData['chequeId']);

            if ($result) {
                $this->json->setVariable('cheque', $result['cheque']);
                $this->json->setVariable('titulos', $result['titulos']);
            }

            return $this->json;
        }

        return $this->json->setVariable('error', 'Titulo não informado');
    }

    /**
     * Busaca dados pela informação de emissão: banco, agencia, conta
     * @return JsonModel|ViewModel
     */
    public function dadosChequeEuristicaAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $arrayData               = $request->getPost()->toArray();
            $serviceFinanceiroCheque = $this->getServiceLocator()->get('Boleto\Service\FinanceiroCheque');
            $result                  = $serviceFinanceiroCheque->buscaDadosChequeEuristica($arrayData);

            if ($result) {
                if (!empty($result['msgError'])) {
                    $this->json->setVariable('msg', $result['msgError']);
                } else {
                    $this->json->setVariable('cheque', $result['cheque']);
                }
            }

            return $this->json;
        }

        return $this->json->setVariable('error', 'Titulo não informado');
    }

    /**
     * Quita mensalidade de um aluno
     * @return JsonModel
     */
    public function quitaMensalidadeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $arrayData = $request->getPost()->toArray();

            if (!empty($arrayData)) {
                $serviceFinanceiroTitulo = $this->getServiceLocator()->get('Boleto\Service\FinanceiroTitulo');
                $titulo                  = $serviceFinanceiroTitulo->quitaTitulo($arrayData);

                if (!empty($titulo)) {
                    $cheques = $serviceFinanceiroTitulo->buscaChequesTitulo($titulo->getTituloId());

                    if (!empty($cheques)) {
                        $this->json->setVariable('cheques', $cheques);
                    }

                    $this->json->setVariables(
                        [
                            'titulo' => array_merge(
                                $titulo->toArray(),
                                ['usuarioBaixa' => $titulo->getUsuarioBaixa()->getLogin()]
                            ),
                            'result' => true,
                            'msg'    => 'Baixa efetuada com sucesso!'
                        ]
                    );

                    return $this->json;
                }
            }
        }

        $this->json->setVariables(
            ['result' => false, 'msg' => 'Não foi possivel efetuar a baixa! Favor entrar em contato com suporte.']
        );

        return $this->json;
    }

    /**
     * Cria taxa
     * @return JsonModel
     * @throws \Exception
     */
    public function criaTaxaAction()
    {
        $request = $this->getRequest();

        $msg    = 'Não foi possivel efetuar a baixa! Favor entrar em contato com suporte.';
        $result = false;

        if ($request->isPost()) {
            $arrayData = $request->getPost()->toArray();

            if (!empty($arrayData)) {
                $serviceFinanceiroTitulo = new \Boleto\Service\FinanceiroTitulo($this->getEntityManager());
                $titulo                  = $serviceFinanceiroTitulo->criaTaxa($arrayData);

                if (!empty($titulo)) {
                    if ($arrayData['pagarAgora'] === 'true') {
                        $arrayData['tituloId'] = $titulo->getTituloId();
                        $titulo                = $serviceFinanceiroTitulo->quitaTitulo($arrayData);

                        if (!empty($titulo)) {
                            $cheques = $serviceFinanceiroTitulo->buscaChequesTitulo($titulo->getTituloId());

                            if (!empty($cheques)) {
                                $this->getJson()->setVariable('cheques', $cheques);
                            }

                            $msg = 'Taxa criada e quitada com sucesso!';

                            $this->getJson()->setVariable(
                                'titulo',
                                array_merge(
                                    $titulo->toArray(),
                                    ['usuarioBaixa' => $titulo->getUsuarioBaixa()->getLogin()]
                                )
                            );
                        }
                    } else {
                        $titulo = $titulo->toArray();

                        if (is_array($titulo['usuarioAutor'])) {
                            $titulo['usuarioAutor'] = $titulo['usuarioAutor']['login'];
                        } else {
                            $titulo['usuarioAutor'] = $titulo['usuarioAutor']->getLogin();
                        }

                        $msg = 'Taxa criada com sucesso!';

                        $this->getJson()->setVariable('titulo', $titulo);
                    }

                    $result = true;
                }
            }
        }

        $this->getJson()->setVariables(['result' => $result, 'msg' => $msg]);

        return $this->getJson();
    }

    /**
     * Cadastra títulos avulsos de alunos(Mensalidade, Monografia, Adaptação, Dependência)
     * @return JsonModel
     */
    public function novoTituloAction()
    {
        $request = $this->getRequest();
        $msg     = 'Não foi possível cadastrar título! Por favor entre em contato com suporte.';
        $result  = false;

        if ($request->isPost()) {
            $arrayData = $request->getPost()->toArray();

            if (!empty($arrayData)) {
                $serviceFinanceiroTitulo = new \Boleto\Service\FinanceiroTitulo($this->getEntityManager());
                $arrRet                  = $serviceFinanceiroTitulo->criarTitulo($arrayData);

                if ($arrRet) {
                    $arrTitulo = $arrRet['arrTitulo'];

                    if (is_array($arrTitulo['usuarioAutor'])) {
                        $arrTitulo['usuarioAutor'] = $arrTitulo['usuarioAutor']['login'];
                    } else {
                        $arrTitulo['usuarioAutor'] = $arrTitulo['usuarioAutor']->getLogin();
                    }

                    $msg = 'Título criada com sucesso!';

                    $this->getJson()->setVariable('titulo', $arrTitulo);

                    $result = true;
                } else {
                    $msg = $serviceFinanceiroTitulo->getLastError();
                }
            }
        }

        $this->getJson()->setVariables(['result' => $result, 'msg' => $msg]);

        return $this->getJson();
    }

    /**
     * Rota para quitação de taxas
     * @return JsonModel
     */
    public function quitaTaxaAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $arrayData = $request->getPost()->toArray();

            if (!empty($arrayData)) {
                $serviceFinanceiroTitulo = $this->getServiceLocator()->get('Boleto\Service\FinanceiroTitulo');
                $titulo                  = $serviceFinanceiroTitulo->quitaTitulo($arrayData);

                if (!empty($titulo)) {
                    $cheques = $serviceFinanceiroTitulo->buscaChequesTitulo($titulo->getTituloId());

                    if (!empty($cheques)) {
                        $this->json->setVariable('cheques', $cheques);
                    }

                    $this->json->setVariables(
                        [
                            'result' => true,
                            'msg'    => 'Baixa efetuada com sucesso!',
                            'titulo' => array_merge(
                                $titulo->toArray(),
                                ['usuarioBaixa' => $titulo->getUsuarioBaixa()->getLogin()]
                            )
                        ]
                    );

                    return $this->json;
                }
            }
        }

        $this->json->setVariables(
            ['result' => false, 'msg' => 'Não foi possivel efetuar a baixa! Favor entrar em contato com suporte.']
        );

        return $this->json;
    }

    /**
     * Estorno de títulos
     * @return JsonModel
     */
    public function estornoTituloAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $arrayData = $request->getPost()->toArray();

            if (!empty($arrayData)) {
                $serviceFinanceiroTitulo = $this->getServiceLocator()->get('Boleto\Service\FinanceiroTitulo');
                $this->getEntityManager()->beginTransaction();
                $titulo = $serviceFinanceiroTitulo->cancelaTitulo($arrayData, 'Estorno');
                $this->getEntityManager()->commit();

                if (!empty($titulo)) {
                    $cheques = $serviceFinanceiroTitulo->buscaChequesTitulo($titulo->getTituloId());

                    if (!empty($cheques)) {
                        $this->json->setVariable('cheques', $cheques);
                    }

                    $serviceFinanceiro = $this->getServiceLocator()->get('Boleto\Service\Financeiro');
                    $titulo            = $serviceFinanceiro->buscaTitulo($titulo->getTituloId());

                    $this->json->setVariables(
                        [
                            'result' => true,
                            'msg'    => 'Titulo estornado com sucesso!',
                            'titulo' =>
                                array_merge(
                                    $titulo,
                                    ['usuarioAutor' => $titulo['usuarioAutor']->getLogin()]
                                )
                        ]
                    );

                    return $this->json;
                }
            }
        }

        $this->json->setVariables(
            ['result' => false, 'msg' => 'Não foi possivel efetuar o estorno! Favor entrar em contato com suporte.']
        );

        return $this->json;
    }

    /**
     * Cancela o titulo pago e gera um novo título
     * @return JsonModel
     */
    public function cancelaPagamentoTituloAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $arrayData = $request->getPost()->toArray();

            if (!empty($arrayData)) {
                $serviceFinanceiroTitulo = $this->getServiceLocator()->get("Boleto\Service\FinanceiroTitulo");
                $titulo                  = $serviceFinanceiroTitulo->cancelaPagamentoTitulo($arrayData, 'Estorno');

                if (!empty($titulo)) {
                    $cheques = $serviceFinanceiroTitulo->buscaChequesTitulo($titulo->getTituloId());

                    if (!empty($cheques)) {
                        $this->json->setVariable('cheques', $cheques);
                    }

                    $serviceFinanceiro = $this->getServiceLocator()->get('Boleto\Service\Financeiro');
                    $titulo            = $serviceFinanceiro->buscaTitulo($titulo->getTituloId());

                    if (is_array($titulo['usuarioAutor'])) {
                        $titulo['usuarioAutor'] = $titulo['usuarioAutor']['login'];
                    } else {
                        $titulo['usuarioAutor'] = $titulo['usuarioAutor']->getLogin();
                    }

                    $this->json->setVariables(
                        [
                            'result' => true,
                            'msg'    => 'Titulo cancelado com sucesso!',
                            'titulo' => $titulo
                        ]
                    );

                    return $this->json;
                }
            }
        }

        $this->json->setVariables(
            [
                'result' => false,
                'msg'    => 'Não foi possivel efetuar o cancelamento! Favor entrar em contato com suporte.'
            ]
        );

        return $this->json;
    }

    /**
     * Cancela título
     * @return JsonModel
     */
    public function cancelaTituloAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $arrayData           = $request->getPost()->toArray();
            $arrayData['addObs'] = $arrayData['tituloObservacoes'];

            if (!empty($arrayData)) {
                $serviceFinanceiroTitulo = $this->getServiceLocator()->get('Boleto\Service\FinanceiroTitulo');
                $this->getEntityManager()->beginTransaction();
                $titulo = $serviceFinanceiroTitulo->cancelaTitulo($arrayData, 'Cancelado');
                $this->getEntityManager()->commit();

                if (!empty($titulo)) {
                    $this->json->setVariables(
                        [
                            'result' => true,
                            'msg'    => 'Titulo cancelado com sucesso!',
                            'titulo' => array_merge(
                                $titulo->toArray(),
                                ['usuarioBaixa' => $titulo->getUsuarioBaixa()->getLogin()]
                            )
                        ]
                    );

                    return $this->json;
                }
            }
        }

        $this->json->setVariables(
            [
                'result' => false,
                'msg'    => 'Não foi possivel efetuar o cancelamento! Favor entrar em contato com suporte.'
            ]
        );

        return $this->json;
    }

    /**
     * Retorna dados de um título com descontos e acrescimos
     * @return JsonModel|ViewModel
     */
    public function descontosAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $arrayData         = $request->getPost()->toArray();
            $serviceFinanceiro = $this->getServiceLocator()->get('Boleto\Service\Financeiro');
            $titulo            = $serviceFinanceiro->buscaTitulo($arrayData['tituloId']);

            $this->json->setVariable('titulo', $titulo);

            return $this->json;
        }

        return $this->json->setVariable('error', 'Titulo não informado');
    }

    /**
     * Salva observações
     * @return JsonModel
     */
    public function salvaObservacoesAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $arrayData = $request->getPost()->toArray();

            if (!empty($arrayData)) {
                $serviceAcadgeralAlunoCurso = $this->getServiceLocator()->get('Matricula\Service\AcadgeralAlunoCurso');
                $alunoCurso                 = $serviceAcadgeralAlunoCurso->salvaObservacoes($arrayData);

                if (!empty($alunoCurso)) {
                    $this->json->setVariables(['msg' => 'Observações salvas com sucesso']);

                    return $this->json;
                }
            }
        }

        $this->json->setVariables(
            ['msg' => 'Não foi possivel salvar está observação favor entrar em contato com suporte']
        );

        return $this->json;
    }

    /**
     * Comprovantes de pagamento
     * @return \Zend\Http\Response|ViewModel
     */
    public function comprovantePagamentoAction()
    {
        $params = $this->params()->fromRoute();

        if (!empty($params['id']) && !empty($params['id2']) && !empty($params['id3'])) {
            $view                     = new ViewModel();
            $serviceAluno             = new \Matricula\Service\AcadperiodoAluno($this->getEntityManager());
            $serviceTurma             = new \Matricula\Service\AcadperiodoTurma($this->getEntityManager());
            $serviceFinanceiro        = new \Boleto\Service\Financeiro($this->getEntityManager());
            $serviceTituloMensalidade = new \Boleto\Service\FinanceiroTituloMensalidade($this->getEntityManager());
            $serviceOrgCampus         = new \Organizacao\Service\OrgCampus($this->getEntityManager());

            $titulo = $serviceFinanceiro->buscaTitulo($params['id3']);

            if ($titulo) {
                $usuarioBaixa = $serviceFinanceiro->buscaUsuarioTitulo($titulo['usuarioBaixa']->getId());
                $turma        = $serviceAluno->buscaTurmaPrincipalAluno($params['id2']);

                /* @var $objTurma \Matricula\Entity\AcadperiodoTurma */
                $objTurma = $serviceTurma->getRepository()->find($turma[0]['turma_id']);

                if ($titulo['tipotituloNome'] === "Mensalidade"
                    OR $titulo['tipotituloNome'] === "Dependência"
                    OR $titulo['tipotituloNome'] === "Adaptação"
                    OR $titulo['tipotituloNome'] === "Monografia"
                ) {
                    $parcela = explode("/", $titulo['tituloDataVencimento']);

                    $view->setVariable('mensalidade', $parcela);

                    /* @var $objFinanceiroMensalidade \Boleto\Entity\FinanceiroTituloMensalidade */
                    $objFinanceiroMensalidade = $serviceTituloMensalidade->getRepository()->findOneBy(
                        ['titulo' => $params['id3']]
                    );

                    $objAcadperiodoAluno  = $objFinanceiroMensalidade->getAlunoPer();
                    $objAcadperiodoTurma  = $objAcadperiodoAluno->getTurma();
                    $objAcadgeralSituacao = $objAcadperiodoAluno->getMatsituacao();

                    if ($objAcadgeralSituacao->getSituacaoId() != \Matricula\Service\AcadgeralSituacao::MATRICULADO) {
                        $turma['turma'] = 'Matrícula em Transição';
                        $turma['serie'] = '';
                    } else {
                        $turma['turma'] = 'Turma Principal: ' . $objAcadperiodoTurma->getTurmaNome();
                        $turma['serie'] = ' - ' . $objAcadperiodoTurma->getTurmaSerie() . ' º Periodo';
                    }
                } else {
                    $turma['turma'] = 'Turma Principal: ' . $turma[0]['turma_nome'];
                    $turma['serie'] = ' - ' . $turma[0]['turma_serie'] . ' º Periodo';
                }

                $view->setVariables(
                    $serviceOrgCampus->retornaDadosInstituicao($objTurma->getCursocampus()->getCamp())
                );

                $view->setVariables(
                    [
                        'titulo'       => $titulo,
                        'matricula'    => $params['id'],
                        'turma'        => $turma,
                        'usuarioBaixa' => $usuarioBaixa,
                        'via'          => $params['id4'] == 1 ? $params['id4'] : null,
                    ]
                );
            } else {
                $this->flashMessenger()->addErrorMessage('Título financeiro não encontrado.');

                return $this->redirect()->toRoute($this->getRoute(), ['controller' => $this->getController()]);
            }

            $view->setTerminal(true);

            return $view;
        }
    }
}