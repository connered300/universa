<?php

namespace Boleto\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * FinanceiroDesconto
 *
 * @ORM\Table(name="financeiro__desconto", indexes={@ORM\Index(name="fk_financeiro__desconto_financeiro__desconto_tipo1_idx", columns={"desctipo_id"}), @ORM\Index(name="fk_financeiro__desconto_acadperiodo__aluno1_idx", columns={"alunoper_id"}), @ORM\Index(name="fk_financeiro__desconto_acesso_pessoas1_idx", columns={"usuario_id"})})
 * @ORM\Entity
 * @LG\LG(id="descontoId",label="desconto_id")
 * @Jarvis\Jarvis(title="Descontos",icon="fa fa-table")
 */
class FinanceiroDesconto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="desconto_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="desconto_id")
     * @LG\Labels\Attributes(text="N° Desconto",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $descontoId;

    /**
     * @var \AcadperiodoAluno
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoAluno")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="alunoper_id", referencedColumnName="alunoper_id")
     * })
     */
    private $alunoper;

    /**
     * @LG\Labels\Property(name="alunocurso_id")
     * @LG\Labels\Attributes(text="Matricula Aluno",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $alunocurso;

    /**
     * @LG\Labels\Property(name="nome")
     * @LG\Labels\Attributes(text="Aluno",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $pesNome;

    /**
     * @LG\Labels\Property(name="semestre_nome")
     * @LG\Labels\Attributes(text="Semestre Letivo",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $semestreNome;

    /**
     * @var \FinanceiroDescontoTipo
     *
     * @ORM\ManyToOne(targetEntity="FinanceiroDescontoTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="desctipo_id", referencedColumnName="desctipo_id")
     * })
     * @LG\Labels\Property(name="desctipo_id")
     * @LG\Labels\Attributes(text="Tipo Desconto",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $desctipo;

    /**
     * @var float
     *
     * @ORM\Column(name="desconto_valor", type="float", precision=10, scale=2, nullable=true)
     * @LG\Labels\Property(name="desconto_valor")
     * @LG\Labels\Attributes(text="Valor de Desconto",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $descontoValor;

    /**
     * @var float
     *
     * @ORM\Column(name="desconto_percentual", type="float", precision=6, scale=2, nullable=true)
     * @LG\Labels\Property(name="desconto_percentual")
     * @LG\Labels\Attributes(text="Percentual de Desconto",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $descontoPercentual;

    /**
     * @var string
     *
     * @ORM\Column(name="desconto_mensalidades", type="string", nullable=true)
     * @LG\Labels\Property(name="desconto_mensalidades")
     * @LG\Labels\Attributes(text="Mensalidades",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $descontoMensalidades;

    /**
     * @var string
     *
     * @ORM\Column(name="desconto_status", type="string", nullable=false)
     * @LG\Labels\Property(name="desconto_status")
     * @LG\Labels\Attributes(text="Status Desconto",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $descontoStatus = 'Ativo';

    /**
     * @var \AcessoPessoas
     *
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * })
     */
    private $usuario;

    public function __construct(array $data)
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getDescontoId()
    {
        return $this->descontoId;
    }

    /**
     * @param int $descontoId
     */
    public function setDescontoId($descontoId)
    {
        $this->descontoId = $descontoId;
    }

    /**
     * @return float
     */
    public function getDescontoValor()
    {
        return $this->descontoValor;
    }

    /**
     * @param float $descontoValor
     */
    public function setDescontoValor($descontoValor)
    {
        $this->descontoValor = $descontoValor;
    }

    /**
     * @return float
     */
    public function getDescontoPercentual()
    {
        return $this->descontoPercentual;
    }

    /**
     * @param float $descontoPercentual
     */
    public function setDescontoPercentual($descontoPercentual)
    {
        $this->descontoPercentual = $descontoPercentual;
    }

    /**
     * @return string
     */
    public function getDescontoMensalidades()
    {
        return $this->descontoMensalidades;
    }

    /**
     * @param string $descontoMensalidades
     */
    public function setDescontoMensalidades($descontoMensalidades)
    {
        $this->descontoMensalidades = $descontoMensalidades;
    }

    /**
     * @return string
     */
    public function getDescontoStatus()
    {
        return $this->descontoStatus;
    }

    /**
     * @param string $descontoStatus
     */
    public function setDescontoStatus($descontoStatus)
    {
        $this->descontoStatus = $descontoStatus;
    }

    /**
     * @return \AcadperiodoAluno
     */
    public function getAlunoper()
    {
        return $this->alunoper;
    }

    /**
     * @param \AcadperiodoAluno $alunoper
     */
    public function setAlunoper($alunoper)
    {
        $this->alunoper = $alunoper;
    }

    /**
     * @return \FinanceiroDescontoTipo
     */
    public function getDesctipo()
    {
        return $this->desctipo;
    }

    /**
     * @param \FinanceiroDescontoTipo $desctipo
     */
    public function setDesctipo($desctipo)
    {
        $this->desctipo = $desctipo;
    }

    /**
     * @return \AcessoPessoas
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param \AcessoPessoas $usuario
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }

    public function toArray()
    {
        return [
            'descontoId'                => $this->getDescontoId(),
            'descontoDescontoDiaLimite' => $this->getDescontoDiaLimite(),
            'alunoper'                  => $this->getAlunoper()->getAlunoperId(),
            'descontoMensalidades'      => $this->getDescontoMensalidades(),
            'descontoPercentual'        => $this->getDescontoPercentual(),
            'descontoValor'             => $this->getDescontoValor(),
            'descontoDiaLimite'         => $this->getDescontoDiaLimite(),
            'descontoStatus'            => $this->getDescontoStatus(),
            'desctipo'                  => $this->getDesctipo(),
            'desctipoModalidade'        => $this->getDesctipo()->getDesctipoModalidade(),
            'descNome'                  => $this->getDesctipo()->getDesctipoDescricao(),
            'nome'                      => $this->getAlunoper()->getAlunocurso()->getAluno()->getPes()->getPes(
            )->getPesNome(),
        ];
    }

}
