<?php

namespace Boleto\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FinanceiroTituloConfig
 *
 * @ORM\Table(name="financeiro__titulo_config", indexes={@ORM\Index(name="fk_financeiro__titulo_config_campus_curso1_idx", columns={"cursocampus_id"}), @ORM\Index(name="fk_financeiro__titulo_config_acadperiodo__letivo1_idx", columns={"per_id"})})
 * @ORM\Entity
 */
class FinanceiroTituloConfig
{
    /**
     * @var integer
     *
     * @ORM\Column(name="tituloconf_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $tituloconfId;

    /**
     * @var integer
     *
     * @ORM\Column(name="tituloconf_dia_venc", type="integer", nullable=false)
     */
    private $tituloconfDiaVenc;

    /**
     * @var integer
     *
     * @ORM\Column(name="tituloconf_dia_desc", type="integer", nullable=false)
     */
    private $tituloconfDiaDesc;

    /**
     * @var float
     *
     * @ORM\Column(name="tituloconf_valor_desc", type="float", precision=10, scale=0, nullable=true)
     */
    private $tituloconfValorDesc;

    /**
     * @var float
     *
     * @ORM\Column(name="tituloconf_percent_desc", type="float", precision=10, scale=0, nullable=true)
     */
    private $tituloconfPercentDesc;

    /**
     * @var float
     *
     * @ORM\Column(name="tituloconf_multa", type="float", precision=10, scale=2, nullable=true)
     */
    private $tituloconfMulta;

    /**
     * @var float
     *
     * @ORM\Column(name="tituloconf_juros", type="float", precision=10, scale=2, nullable=true)
     */
    private $tituloconfJuros;

    /**
     * @var \Matricula\Entity\AcadperiodoLetivo
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoLetivo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="per_id", referencedColumnName="per_id")
     * })
     */
    private $per;

    /**
     * @var \Matricula\Entity\CampusCurso
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\CampusCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cursocampus_id", referencedColumnName="cursocampus_id")
     * })
     */
    private $cursocampus;

    public function __construct(  array $data ) {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getTituloconfId()
    {
        return $this->tituloconfId;
    }

    /**
     * @param int $tituloconfId
     */
    public function setTituloconfId($tituloconfId)
    {
        $this->tituloconfId = $tituloconfId;
    }

    /**
     * @return int
     */
    public function getTituloconfDiaVenc()
    {
        return $this->tituloconfDiaVenc;
    }

    /**
     * @param int $tituloconfDiaVenc
     */
    public function setTituloconfDiaVenc($tituloconfDiaVenc)
    {
        $this->tituloconfDiaVenc = $tituloconfDiaVenc;
    }

    /**
     * @return int
     */
    public function getTituloconfDiaDesc()
    {
        return $this->tituloconfDiaDesc;
    }

    /**
     * @param int $tituloconfDiaDesc
     */
    public function setTituloconfDiaDesc($tituloconfDiaDesc)
    {
        $this->tituloconfDiaDesc = $tituloconfDiaDesc;
    }

    /**
     * @return float
     */
    public function getTituloconfValorDesc()
    {
        return $this->tituloconfValorDesc;
    }

    /**
     * @param float $tituloconfValorDesc
     */
    public function setTituloconfValorDesc($tituloconfValorDesc)
    {
        $this->tituloconfValorDesc = $tituloconfValorDesc;
    }

    /**
     * @return float
     */
    public function getTituloconfPercentDesc()
    {
        return $this->tituloconfPercentDesc;
    }

    /**
     * @param float $tituloconfPercentDesc
     */
    public function setTituloconfPercentDesc($tituloconfPercentDesc)
    {
        $this->tituloconfPercentDesc = $tituloconfPercentDesc;
    }

    /**
     * @return \Matricula\Entity\AcadperiodoLetivo
     */
    public function getPer()
    {
        return $this->per;
    }

    /**
     * @param \Matricula\Entity\AcadperiodoLetivo $per
     */
    public function setPer($per)
    {
        $this->per = $per;
    }

    /**
     * @return \Matricula\Entity\CampusCurso
     */
    public function getCursocampus()
    {
        return $this->cursocampus;
    }

    /**
     * @param \Matricula\Entity\CampusCurso $cursocampus
     */
    public function setCursocampus($cursocampus)
    {
        $this->cursocampus = $cursocampus;
    }

    /**
     * @return float
     */
    public function getTituloconfMulta()
    {
        return $this->tituloconfMulta;
    }

    /**
     * @param float $tituloconfMulta
     */
    public function setTituloconfMulta($tituloconfMulta)
    {
        $this->tituloconfMulta = $tituloconfMulta;
    }

    /**
     * @return float
     */
    public function getTituloconfJuros()
    {
        return $this->tituloconfJuros;
    }

    /**
     * @param float $tituloconfJuros
     */
    public function setTituloconfJuros($tituloconfJuros)
    {
        $this->tituloconfJuros = $tituloconfJuros;
    }
}
