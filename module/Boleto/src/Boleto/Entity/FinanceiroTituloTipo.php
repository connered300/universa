<?php

namespace Boleto\Entity;

use Doctrine\ORM\Mapping as ORM;

use VersaSpine\ListGenerator\Annotations\Entity as LG;

/**
 * FinanceiroTituloTipo
 *
 * @ORM\Table(name="financeiro__titulo_tipo")
 * @ORM\Entity(repositoryClass="Boleto\Entity\Repository\FinanceiroTituloTipo")
 * @LG\LG(id="tipotituloId",label="tipotitulo_nome")
 */
class FinanceiroTituloTipo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="tipotitulo_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $tipotituloId;

    /**
     * @var string
     *
     * @ORM\Column(name="tipotitulo_nome", type="string", length=45, nullable=true)
     */
    private $tipotituloNome;

    /**
     * @var string
     *
     * @ORM\Column(name="tipotitulo_descricao", type="string", length=255, nullable=true)
     */
    private $tipotituloDescricao;

    public function __construct(  array $data ) {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getTipotituloId()
    {
        return $this->tipotituloId;
    }

    /**
     * @param int $tipotituloId
     */
    public function setTipotituloId($tipotituloId)
    {
        $this->tipotituloId = $tipotituloId;
    }

    /**
     * @return string
     */
    public function getTipotituloNome()
    {
        return $this->tipotituloNome;
    }

    /**
     * @param string $tipotituloNome
     */
    public function setTipotituloNome($tipotituloNome)
    {
        $this->tipotituloNome = $tipotituloNome;
    }

    /**
     * @return string
     */
    public function getTipotituloDescricao()
    {
        return $this->tipotituloDescricao;
    }

    /**
     * @param string $tipotituloDescricao
     */
    public function setTipotituloDescricao($tipotituloDescricao)
    {
        $this->tipotituloDescricao = $tipotituloDescricao;
    }
}
