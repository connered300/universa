<?php

namespace Boleto\Entity;

use Doctrine\ORM\Mapping as ORM;

use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * BoletoBaixa
 *
 * @ORM\Table(name="boleto_baixa", indexes={@ORM\Index(name="fk_baixas_efetuadas_arquivo1_idx", columns={"arq_id"}), @ORM\Index(name="fk_boleto_baixas_acesso_pessoas1_idx", columns={"usuario_baixa"})})
 * @ORM\Entity
 */
class BoletoBaixa
{

    /**
     * @var integer
     *
     * @ORM\Column(name="baixa_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $baixaId;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     *
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_baixa", referencedColumnName="id")
     * })
     */
    private $usuarioBaixa;

    /**
     * @var \GerenciadorArquivos\Entity\Arquivo
     *
     * @ORM\ManyToOne(targetEntity="GerenciadorArquivos\Entity\Arquivo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="arq_id", referencedColumnName="arq_id")
     * })
     *
     */
    private $arq;

    /**
     * @var string
     *
     * @ORM\Column(name="baixa_data_submissao", type="string", nullable=false)
     *
     */
    private $baixaDataSubmissao;

    /**
     * @return mixed
     */
    public function getBaixaId()
    {
        return $this->baixaId;
    }

    /**
     * @param mixed $baixaId
     * @return BoletoBaixa
     */
    public function setBaixaId($baixaId)
    {
        $this->baixaId = $baixaId;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuarioBaixa()
    {
        return $this->usuarioBaixa;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuarioBaixa
     * @return BoletoBaixa
     */
    public function setUsuarioBaixa($usuarioBaixa)
    {
        $this->usuarioBaixa = $usuarioBaixa;

        return $this;
    }

    /**
     * @return \GerenciadorArquivos\Entity\Arquivo
     */
    public function getArq()
    {
        return $this->arq;
    }

    /**
     * @param \GerenciadorArquivos\Entity\Arquivo $arq
     * @return BoletoBaixa
     */
    public function setArq($arq)
    {
        $this->arq = $arq;

        return $this;
    }

    /**
     * @return \Datetime
     */
    public function getBaixaDataSubmissao()
    {
        return $this->baixaDataSubmissao;
    }

    /**
     * @param \Datetime $baixaDataSubmissao
     * @return BoletoBaixa
     */
    public function setBaixaDataSubmissao($baixaDataSubmissao)
    {
        $this->baixaDataSubmissao = $baixaDataSubmissao;

        return $this;
    }

    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    public function toArray()
    {
        return [
            'arquivo' => $this->getArq(),
            'usuario' => $this->getUsuarioBaixa(),
            'data'    => $this->getBaixaDataSubmissao(),
            'baixaId' => $this->getBaixaId()
        ];
    }
}