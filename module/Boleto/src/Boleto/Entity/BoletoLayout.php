<?php

namespace Boleto\Entity;

use Doctrine\ORM\Mapping as ORM;

use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * BoletoLayout
 *
 * @ORM\Table(name="boleto_layout")
 * @ORM\Entity
 * @LG\LG(id="layoutId",label="layout_versao")
 * @Jarvis\Jarvis(title="Configurações de Boletos",icon="fa fa-table")
 */
class BoletoLayout
{
    /**
     * @var integer
     *
     * @ORM\Column(name="layout_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $layoutId;

    /**
     * @var string
     *
     * @ORM\Column(name="layout_caminho", type="string", length=255, nullable=false)
     */
    private $layoutCaminho;

    /**
     * @var integer
     *
     * @ORM\Column(name="layout_versao", type="integer", nullable=false)
     */
    private $layoutVersao;

    /**
     * @var string
     *
     * @ORM\Column(name="layout_data", type="string", length=45, nullable=true)
     */
    private $layoutData;

    /**
     * @var string
     *
     * @ORM\Column(name="layout_estado", type="string", nullable=false)
     */
    private $layoutEstado;

    public function __contruct( array $data){
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getLayoutId()
    {
        return $this->layoutId;
    }

    /**
     * @param int $layoutId
     */
    public function setLayoutId($layoutId)
    {
        $this->layoutId = $layoutId;
    }

    /**
     * @return string
     */
    public function getLayoutCaminho()
    {
        return $this->layoutCaminho;
    }

    /**
     * @param string $layoutCaminho
     */
    public function setLayoutCaminho($layoutCaminho)
    {
        $this->layoutCaminho = $layoutCaminho;
    }

    /**
     * @return int
     */
    public function getLayoutVersao()
    {
        return $this->layoutVersao;
    }

    /**
     * @param int $layoutVersao
     */
    public function setLayoutVersao($layoutVersao)
    {
        $this->layoutVersao = $layoutVersao;
    }

    /**
     * @return string
     */
    public function getLayoutData()
    {
        return $this->layoutData;
    }

    /**
     * @param string $layoutData
     */
    public function setLayoutData($layoutData)
    {
        $this->layoutData = $layoutData;
    }

    /**
     * @return string
     */
    public function getLayoutEstado()
    {
        return $this->layoutEstado;
    }

    /**
     * @param string $layoutEstado
     */
    public function setLayoutEstado($layoutEstado)
    {
        $this->layoutEstado = $layoutEstado;
    }
}
