<?php

namespace Boleto\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FinanceiroTituloCheque
 *
 * @ORM\Table(name="financeiro__titulo_cheque", indexes={@ORM\Index(name="fk_financeiro__titulo_financeiro__cheque_financeiro__cheque_idx", columns={"cheque_id"}), @ORM\Index(name="fk_financeiro__titulo_financeiro__cheque_financeiro__titulo_idx", columns={"titulo_id"})})
 * @ORM\Entity
 */
class FinanceiroTituloCheque
{
    /**
     * @var integer
     *
     * @ORM\Column(name="titulocheque_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $titulochequeId;

    /**
     * @var float
     *
     * @ORM\Column(name="titulocheque_valor", type="float", precision=10, scale=2, nullable=true)
     */
    private $titulochequeValor;

    /**
     * @var string
     *
     * @ORM\Column(name="titulocheque_estado", type="string", nullable=false)
     */
    private $titulochequeEstado = 'Ativo';

    /**
     * @var \FinanceiroCheque
     *
     * @ORM\ManyToOne(targetEntity="FinanceiroCheque")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cheque_id", referencedColumnName="cheque_id")
     * })
     */
    private $cheque;

    /**
     * @var \FinanceiroTitulo
     *
     * @ORM\ManyToOne(targetEntity="FinanceiroTitulo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="titulo_id", referencedColumnName="titulo_id")
     * })
     */
    private $titulo;

    public function __construct(  array $data ) {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getTitulochequeId()
    {
        return $this->titulochequeId;
    }

    /**
     * @param int $titulochequeId
     */
    public function setTitulochequeId($titulochequeId)
    {
        $this->titulochequeId = $titulochequeId;
    }

    /**
     * @return float
     */
    public function getTitulochequeValor()
    {
        return $this->titulochequeValor;
    }

    /**
     * @param float $titulochequeValor
     */
    public function setTitulochequeValor($titulochequeValor)
    {
        $this->titulochequeValor = $titulochequeValor;
    }

    /**
     * @return string
     */
    public function getTitulochequeEstado()
    {
        return $this->titulochequeEstado;
    }

    /**
     * @param string $titulochequeEstado
     */
    public function setTitulochequeEstado($titulochequeEstado)
    {
        $this->titulochequeEstado = $titulochequeEstado;
    }

    /**
     * @return \FinanceiroCheque
     */
    public function getCheque()
    {
        return $this->cheque;
    }

    /**
     * @param \FinanceiroCheque $cheque
     */
    public function setCheque($cheque)
    {
        $this->cheque = $cheque;
    }

    /**
     * @return \FinanceiroTitulo
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param \FinanceiroTitulo $titulo
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

}
