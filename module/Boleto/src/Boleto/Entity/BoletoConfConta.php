<?php

namespace Boleto\Entity;

use Doctrine\ORM\Mapping as ORM;

use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * BoletoConfConta
 *
 * @ORM\Table(name="boleto_conf_conta", indexes={@ORM\Index(name="fk_configuracao_conta_pessoa1_idx", columns={"pes_id"}), @ORM\Index(name="fk_boleto_conf_conta_boleto_banco1_idx", columns={"banc_id"}), @ORM\Index(name="fk_boleto_conf_conta_arquivo1_idx", columns={"arq_id"})})
 * @ORM\Entity(repositoryClass="Boleto\Entity\Repository\BoletoConfConta")
 * @LG\LG(id="confcontId",label="confcont_conta_digito")
 * @Jarvis\Jarvis(title="Confiraguração de Contas Bancarias",icon="fa fa-table")
 */
class BoletoConfConta
{
    /**
     * @var integer
     *
     * @ORM\Column(name="confcont_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="confcont_id")
     * @LG\Labels\Attributes(text="Índice",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $confcontId;

    /**
     * @var string
     *
     * @ORM\Column(name="confcont_agencia", type="string", length=45, nullable=false)
     * @LG\Labels\Property(name="confcont_agencia")
     * @LG\Labels\Attributes(text="Agencia Conta",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $confcontAgencia;

    /**
     * @var string
     *
     * @ORM\Column(name="confcont_agencia_digito", type="string", length=45, nullable=false)
     * @LG\Labels\Property(name="confcont_agencia_digito")
     * @LG\Labels\Attributes(text="Agencia DV",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $confcontAgenciaDigito;

    /**
     * @var string
     *
     * @ORM\Column(name="confcont_conta", type="string", length=45, nullable=false)
     * @LG\Labels\Property(name="confcont_conta")
     * @LG\Labels\Attributes(text="Conta",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $confcontConta;

    /**
     * @var string
     *
     * @ORM\Column(name="confcont_conta_digito", type="string", length=45, nullable=true)
     * @LG\Labels\Property(name="confcont_conta_digito")
     * @LG\Labels\Attributes(text="Conta DV",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $confcontContaDigito;

    /**
     * @var string
     *
     * @ORM\Column(name="confcont_carteira", type="string", length=45, nullable=true)
     * @LG\Labels\Property(name="confcont_carteira")
     * @LG\Labels\Attributes(text="Carteira",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $confcontCarteira;

    /**
     * @var string
     *
     * @ORM\Column(name="confcont_variacao", type="string", length=45, nullable=true)
     * @LG\Labels\Property(name="confcont_variacao")
     * @LG\Labels\Attributes(text="Variacao",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $confcontVariacao;

    /**
     * @var string
     *
     * @ORM\Column(name="confcont_convenio", type="string", length=45, nullable=true)
     * @LG\Labels\Property(name="confcont_convenio")
     * @LG\Labels\Attributes(text="Variacao",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $confcontConvenio;

    /**
     * @var string
     *
     * @ORM\Column(name="confcont_contrato", type="string", length=45, nullable=true)
     * @LG\Labels\Property(name="confcont_contrato")
     * @LG\Labels\Attributes(text="Contrato",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $confcontContrato;

    /**
     * @var \Pessoa\Entity\Pessoa
     *
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     * @LG\Labels\Property(name="pes_id")
     * @LG\Labels\Attributes(text="Responsavel",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE%")
     * @LG\Querys\Joins(table="pessoa",joinType="inner",joinCampDest="pes_id",joinCampDestLabel="pes_nome")
     */
    private $pes;

    /**
     * @var BoletoBanco
     *
     * @ORM\ManyToOne(targetEntity="BoletoBanco")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="banc_id", referencedColumnName="banc_id")
     * })
     */
    private $banc;

    /**
     * @var \GerenciadorArquivos\Entity\Arquivo
     *
     * @ORM\ManyToOne(targetEntity="GerenciadorArquivos\Entity\Arquivo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="arq_id", referencedColumnName="arq_id")
     * })
     */
    private $arq;

    public function __construct(  array $data ) {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getConfcontId()
    {
        return $this->confcontId;
    }

    /**
     * @param int $confcontId
     */
    public function setConfcontId($confcontId)
    {
        $this->confcontId = $confcontId;
    }

    /**
     * @return string
     */
    public function getConfcontAgencia()
    {
        return $this->confcontAgencia;
    }

    /**
     * @param string $confcontAgencia
     */
    public function setConfcontAgencia($confcontAgencia)
    {
        $this->confcontAgencia = $confcontAgencia;
    }

    /**
     * @return string
     */
    public function getConfcontAgenciaDigito()
    {
        return $this->confcontAgenciaDigito;
    }

    /**
     * @param string $confcontAgenciaDigito
     */
    public function setConfcontAgenciaDigito($confcontAgenciaDigito)
    {
        $this->confcontAgenciaDigito = $confcontAgenciaDigito;
    }

    /**
     * @return string
     */
    public function getConfcontConta()
    {
        return $this->confcontConta;
    }

    /**
     * @param string $confcontConta
     */
    public function setConfcontConta($confcontConta)
    {
        $this->confcontConta = $confcontConta;
    }

    /**
     * @return string
     */
    public function getConfcontContaDigito()
    {
        return $this->confcontContaDigito;
    }

    /**
     * @param string $confcontContaDigito
     */
    public function setConfcontContaDigito($confcontContaDigito)
    {
        $this->confcontContaDigito = $confcontContaDigito;
    }

    /**
     * @return string
     */
    public function getConfcontCarteira()
    {
        return $this->confcontCarteira;
    }

    /**
     * @param string $confcontCarteira
     */
    public function setConfcontCarteira($confcontCarteira)
    {
        $this->confcontCarteira = $confcontCarteira;
    }

    /**
     * @return \Pessoa\Entity\Pessoa
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @param \Pessoa\Entity\Pessoa $pes
     */
    public function setPes($pes)
    {
        $this->pes = $pes;
    }

    /**
     * @return BoletoBanco
     */
    public function getBanc()
    {
        return $this->banc;
    }

    /**
     * @param BoletoBanco $banc
     */
    public function setBanc($banc)
    {
        $this->banc = $banc;
    }

    /**
     * @return \GerenciadorArquivos\Entity\Arquivo
     */
    public function getArq()
    {
        return $this->arq;
    }

    /**
     * @param \GerenciadorArquivos\Entity\Arquivo $arq
     */
    public function setArq($arq)
    {
        $this->arq = $arq;
    }

    /**
     * @return string
     */
    public function getConfcontVariacao()
    {
        return $this->confcontVariacao;
    }

    /**
     * @param string $confcontVariacao
     */
    public function setConfcontVariacao($confcontVariacao)
    {
        $this->confcontVariacao = $confcontVariacao;
    }

    /**
     * @return string
     */
    public function getConfcontConvenio()
    {
        return $this->confcontConvenio;
    }

    /**
     * @param string $confcontConvenio
     */
    public function setConfcontConvenio($confcontConvenio)
    {
        $this->confcontConvenio = $confcontConvenio;
    }

    /**
     * @return string
     */
    public function getConfcontContrato()
    {
        return $this->confcontContrato;
    }

    /**
     * @param string $confcontContrato
     */
    public function setConfcontContrato($confcontContrato)
    {
        $this->confcontContrato = $confcontContrato;
    }


    public function toArray()
    {
        return array(
            'confcontId'            => $this->getConfcontId(),
            'banc'                  => $this->getBanc(),
            'confcontAgencia'       => $this->getConfcontAgencia(),
            'confcontAgenciaDigito' => $this->getConfcontAgenciaDigito(),
            'confcontConta'         => $this->getConfcontConta(),
            'confcontContaDigito'   => $this->getConfcontContaDigito(),
            'confcontCarteira'      => $this->getConfcontCarteira(),
            'confcontVariacao'      => $this->getConfcontVariacao(),
            'confcontContrato'      => $this->getConfcontContrato(),
            'confcontConvenio'      => $this->getConfcontConvenio(),
            'pes'                   => $this->getPes()->getPesId(),
            'pesNome'               => $this->getPes()->getPesNome(),
            'pesTipo'               => $this->getPes()->getPesTipo(),
            'arq'                   => $this->getArq(),
        );
    }
}
