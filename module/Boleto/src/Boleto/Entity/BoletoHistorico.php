<?php
namespace Boleto\Entity;

use Doctrine\ORM\Mapping as ORM;
use Financeiro\Entity\BoletoConfConta;

/**
 * BoletoHistorico
 *
 * @ORM\Table(name="boleto_historico", indexes={@ORM\Index(name="fk_boleto_configuracao_boleto1_idx", columns={"confcont_id"}), @ORM\Index(name="fk_boleto_pessoa1_idx", columns={"pes_id"}), @ORM\Index(name="fk_boleto_historico_boleto1_idx", columns={"bol_id"})})
 * @ORM\Entity
 */
class BoletoHistorico
{
    /**
     * @var integer
     *
     * @ORM\Column(name="bolhist_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $bolhistId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="bolhist_data_hora_documento", type="datetime", nullable=false)
     */
    private $bolhistDataHoraDocumento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="bolhist_data_hora_vencimento", type="date", nullable=false)
     */
    private $bolhistDataHoraVencimento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="bolhist_data_processamento", type="date", nullable=true)
     */
    private $bolhistDataProcessamento;

    /**
     * @var integer
     *
     * @ORM\Column(name="bolhist_nosso_numero", type="bigint", nullable=false)
     */
    private $bolhistNossoNumero;

    /**
     * @var integer
     *
     * @ORM\Column(name="bolhist_numero_documento", type="bigint", nullable=false)
     */
    private $bolhistNumeroDocumento;

    /**
     * @var float
     *
     * @ORM\Column(name="bolhist_valor", type="float", precision=10, scale=0, nullable=false)
     */
    private $bolhistValor;

    /**
     * @var float
     *
     * @ORM\Column(name="bolhist_valor_unitario", type="float", precision=10, scale=0, nullable=true)
     */
    private $bolhistValorUnitario;

    /**
     * @var float
     *
     * @ORM\Column(name="bolhist_quantidade", type="float", precision=10, scale=0, nullable=true)
     */
    private $bolhistQuantidade;

    /**
     * @var string
     *
     * @ORM\Column(name="bolhist_demostrativo", type="text", nullable=true)
     */
    private $bolhistDemostrativo;

    /**
     * @var string
     *
     * @ORM\Column(name="bolhist_instrucoes", type="text", nullable=true)
     */
    private $bolhistInstrucoes;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="bolhist_data_hora_alteracao", type="datetime", nullable=true)
     */
    private $bolhistDataHoraAlteracao;

    /**
     * @var BoletoConfConta
     *
     * @ORM\ManyToOne(targetEntity="BoletoConfConta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="confcont_id", referencedColumnName="confcont_id")
     * })
     */
    private $confcont;

    /**
     * @var Boleto
     *
     * @ORM\ManyToOne(targetEntity="Boleto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="bol_id", referencedColumnName="bol_id")
     * })
     */
    private $bol;

    /**
     * @var \Pessoa\Entity\Pessoa
     *
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     */
    private $pes;

    public function __construct(array $data)
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getBolhistId()
    {
        return $this->bolhistId;
    }

    /**
     * @param int $bolhistId
     */
    public function setBolhistId($bolhistId)
    {
        $this->bolhistId = $bolhistId;
    }

    /**
     * @return \DateTime
     */
    public function getBolhistDataHoraDocumento()
    {
        return $this->bolhistDataHoraDocumento;
    }

    /**
     * @param \DateTime $bolhistDataHoraDocumento
     */
    public function setBolhistDataHoraDocumento($bolhistDataHoraDocumento)
    {
        $this->bolhistDataHoraDocumento = $bolhistDataHoraDocumento;
    }

    /**
     * @return \DateTime
     */
    public function getBolhistDataHoraVencimento()
    {
        return $this->bolhistDataHoraVencimento;
    }

    /**
     * @param \DateTime $bolhistDataHoraVencimento
     */
    public function setBolhistDataHoraVencimento($bolhistDataHoraVencimento)
    {
        $this->bolhistDataHoraVencimento = $bolhistDataHoraVencimento;
    }

    /**
     * @return \DateTime
     */
    public function getBolhistDataProcessamento()
    {
        return $this->bolhistDataProcessamento;
    }

    /**
     * @param \DateTime $bolhistDataProcessamento
     */
    public function setBolhistDataProcessamento($bolhistDataProcessamento)
    {
        $this->bolhistDataProcessamento = $bolhistDataProcessamento;
    }

    /**
     * @return int
     */
    public function getBolhistNossoNumero()
    {
        return $this->bolhistNossoNumero;
    }

    /**
     * @param int $bolhistNossoNumero
     */
    public function setBolhistNossoNumero($bolhistNossoNumero)
    {
        $this->bolhistNossoNumero = $bolhistNossoNumero;
    }

    /**
     * @return int
     */
    public function getBolhistNumeroDocumento()
    {
        return $this->bolhistNumeroDocumento;
    }

    /**
     * @param int $bolhistNumeroDocumento
     */
    public function setBolhistNumeroDocumento($bolhistNumeroDocumento)
    {
        $this->bolhistNumeroDocumento = $bolhistNumeroDocumento;
    }

    /**
     * @return float
     */
    public function getBolhistValor()
    {
        return $this->bolhistValor;
    }

    /**
     * @param float $bolhistValor
     */
    public function setBolhistValor($bolhistValor)
    {
        $this->bolhistValor = $bolhistValor;
    }

    /**
     * @return float
     */
    public function getBolhistValorUnitario()
    {
        return $this->bolhistValorUnitario;
    }

    /**
     * @param float $bolhistValorUnitario
     */
    public function setBolhistValorUnitario($bolhistValorUnitario)
    {
        $this->bolhistValorUnitario = $bolhistValorUnitario;
    }

    /**
     * @return float
     */
    public function getBolhistQuantidade()
    {
        return $this->bolhistQuantidade;
    }

    /**
     * @param float $bolhistQuantidade
     */
    public function setBolhistQuantidade($bolhistQuantidade)
    {
        $this->bolhistQuantidade = $bolhistQuantidade;
    }

    /**
     * @return string
     */
    public function getBolhistDemostrativo()
    {
        return $this->bolhistDemostrativo;
    }

    /**
     * @param string $bolhistDemostrativo
     */
    public function setBolhistDemostrativo($bolhistDemostrativo)
    {
        $this->bolhistDemostrativo = $bolhistDemostrativo;
    }

    /**
     * @return string
     */
    public function getBolhistInstrucoes()
    {
        return $this->bolhistInstrucoes;
    }

    /**
     * @param string $bolhistInstrucoes
     */
    public function setBolhistInstrucoes($bolhistInstrucoes)
    {
        $this->bolhistInstrucoes = $bolhistInstrucoes;
    }

    /**
     * @return \DateTime
     */
    public function getBolhistDataHoraAlteracao()
    {
        return $this->bolhistDataHoraAlteracao;
    }

    /**
     * @param \DateTime $bolhistDataHoraAlteracao
     */
    public function setBolhistDataHoraAlteracao($bolhistDataHoraAlteracao)
    {
        $this->bolhistDataHoraAlteracao = $bolhistDataHoraAlteracao;
    }

    /**
     * @return BoletoConfConta
     */
    public function getConfcont()
    {
        return $this->confcont;
    }

    /**
     * @param BoletoConfConta $confcont
     */
    public function setConfcont($confcont)
    {
        $this->confcont = $confcont;
    }

    /**
     * @return Boleto
     */
    public function getBol()
    {
        return $this->bol;
    }

    /**
     * @param Boleto $bol
     */
    public function setBol($bol)
    {
        $this->bol = $bol;
    }

    /**
     * @return \Pessoa\Entity\Pessoa
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @param \Pessoa\Entity\Pessoa $pes
     */
    public function setPes($pes)
    {
        $this->pes = $pes;
    }
}
