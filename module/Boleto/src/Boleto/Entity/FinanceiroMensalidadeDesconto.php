<?php

namespace Boleto\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FinanceiroMensalidadeDesconto
 *
 * @ORM\Table(name="financeiro__mensalidade_desconto", uniqueConstraints={@ORM\UniqueConstraint(name="unq_mensalidade_desconto", columns={"desconto_id", "financeiro_titulo_mensalidade_id"})}, indexes={@ORM\Index(name="fk_financeiro_titulo_mensalidade_financeiro__desconto_finan_idx", columns={"desconto_id"}), @ORM\Index(name="fk_financeiro__mensalidade_desconto_financeiro__titulo_mens_idx", columns={"financeiro_titulo_mensalidade_id"})})
 * @ORM\Entity
 */
class FinanceiroMensalidadeDesconto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="descmensalidade_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $descmensalidadeId;

    /**
     * @var string
     *
     * @ORM\Column(name="descmensalidade_status", type="string", nullable=false)
     */
    private $descmensalidadeStatus = 'Ativo';

    /**
     * @var \FinanceiroTituloMensalidade
     *
     * @ORM\ManyToOne(targetEntity="FinanceiroTituloMensalidade")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="financeiro_titulo_mensalidade_id", referencedColumnName="financeiro_titulo_mensalidade_id")
     * })
     */
    private $financeiroTituloMensalidade;

    /**
     * @var \FinanceiroDesconto
     *
     * @ORM\ManyToOne(targetEntity="FinanceiroDesconto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="desconto_id", referencedColumnName="desconto_id")
     * })
     */
    private $desconto;


    public function __construct(  array $data ) {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getDescmensalidadeId()
    {
        return $this->descmensalidadeId;
    }

    /**
     * @param int $descmensalidadeId
     */
    public function setDescmensalidadeId($descmensalidadeId)
    {
        $this->descmensalidadeId = $descmensalidadeId;
    }

    /**
     * @return string
     */
    public function getDescmensalidadeStatus()
    {
        return $this->descmensalidadeStatus;
    }

    /**
     * @param string $descmensalidadeStatus
     */
    public function setDescmensalidadeStatus($descmensalidadeStatus)
    {
        $this->descmensalidadeStatus = $descmensalidadeStatus;
    }

    /**
     * @return \FinanceiroTituloMensalidade
     */
    public function getFinanceiroTituloMensalidade()
    {
        return $this->financeiroTituloMensalidade;
    }

    /**
     * @param \FinanceiroTituloMensalidade $financeiroTituloMensalidade
     */
    public function setFinanceiroTituloMensalidade($financeiroTituloMensalidade)
    {
        $this->financeiroTituloMensalidade = $financeiroTituloMensalidade;
    }

    /**
     * @return \FinanceiroDesconto
     */
    public function getDesconto()
    {
        return $this->desconto;
    }

    /**
     * @param \FinanceiroDesconto $desconto
     */
    public function setDesconto($desconto)
    {
        $this->desconto = $desconto;
    }

    function toArray() {
        return [
            'descmensalidadeId'             => $this->getDescmensalidadeId(),
            'descmensalidadeStatus'         => $this->getDescmensalidadeStatus(),
            'desconto'                      => $this->getDesconto()->getDescontoId(),
            'financeiroTituloMensalidade'   => $this->getFinanceiroTituloMensalidade()->getFinanceiroTituloMensalidadeId(),
        ];
    }

}
