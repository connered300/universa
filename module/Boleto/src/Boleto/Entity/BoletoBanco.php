<?php

namespace Boleto\Entity;

use Doctrine\ORM\Mapping as ORM;

use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * BoletoBanco
 *
 * @ORM\Table(name="boleto_banco", indexes={@ORM\Index(name="fk_boleto_banco_boleto_layout1_idx", columns={"layout_id"})})
 * @ORM\Entity(repositoryClass="Boleto\Entity\Repository\BoletoBanco")
 * @LG\LG(id="bancId",label="banc_nome")
 * @Jarvis\Jarvis(title="Bancos",icon="fa fa-table")
 */
class BoletoBanco
{
    /**
     * @var integer
     *
     * @ORM\Column(name="banc_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     */
    private $bancId;

    /**
     * @var integer
     *
     * @ORM\Column(name="banc_codigo", type="integer", nullable=false)
     */
    private $bancCodigo;

    /**
     * @var string
     *
     * @ORM\Column(name="banc_nome", type="string", length=45, nullable=false)
     */
    private $bancNome;

    /**
     * @var integer
     *
     * @ORM\Column(name="layout_id", type="integer", nullable=true)
     */
    private $layoutId;

    public function __construct(  array $data ) {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getBancId()
    {
        return $this->bancId;
    }

    /**
     * @param int $bancId
     */
    public function setBancId($bancId)
    {
        $this->bancId = $bancId;
    }

    /**
     * @return int
     */
    public function getBancCodigo()
    {
        return $this->bancCodigo;
    }

    /**
     * @param int $bancCodigo
     */
    public function setBancCodigo($bancCodigo)
    {
        $this->bancCodigo = $bancCodigo;
    }

    /**
     * @return string
     */
    public function getBancNome()
    {
        return $this->bancNome;
    }

    /**
     * @param string $bancNome
     */
    public function setBancNome($bancNome)
    {
        $this->bancNome = $bancNome;
    }

    /**
     * @return int
     */
    public function getLayoutId()
    {
        return $this->layoutId;
    }

    /**
     * @param int $layoutId
     */
    public function setLayoutId($layoutId)
    {
        $this->layoutId = $layoutId;
    }
}
