<?php

namespace Boleto\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FinanceiroCheque
 *
 * @ORM\Table(name="financeiro__cheque")
 * @ORM\Entity
 */
class FinanceiroCheque
{
    /**
     * @var integer
     *
     * @ORM\Column(name="cheque_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $chequeId;

    /**
     * @var string
     *
     * @ORM\Column(name="cheque_emitente", type="string", length=255, nullable=false)
     */
    private $chequeEmitente;

    /**
     * @var string
     *
     * @ORM\Column(name="cheque_num", type="string", length=45, nullable=false)
     */
    private $chequeNum;

    /**
     * @var string
     *
     * @ORM\Column(name="cheque_banco", type="string", length=45, nullable=false)
     */
    private $chequeBanco;

    /**
     * @var string
     *
     * @ORM\Column(name="cheque_agencia", type="string", length=45, nullable=false)
     */
    private $chequeAgencia;

    /**
     * @var string
     *
     * @ORM\Column(name="cheque_praca", type="string", length=45, nullable=false)
     */
    private $chequePraca;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="cheque_emissao", type="datetime", nullable=false)
     */
    private $chequeEmissao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="cheque_vencimento", type="datetime", nullable=false)
     */
    private $chequeVencimento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="cheque_compensado", type="datetime", nullable=true)
     */
    private $chequeCompensado;

    /**
     * @var float
     *
     * @ORM\Column(name="cheque_valor", type="float", precision=10, scale=2, nullable=false)
     */
    private $chequeValor;


    /**
     * @var string
     *
     * @ORM\Column(name="cheque_conta", type="string", length=45, nullable=true)
     */
    private $chequeConta;

    public function __construct(  array $data ) {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getChequeId()
    {
        return $this->chequeId;
    }

    /**
     * @param int $chequeId
     */
    public function setChequeId($chequeId)
    {
        $this->chequeId = $chequeId;
    }

    /**
     * @return string
     */
    public function getChequeEmitente()
    {
        return $this->chequeEmitente;
    }

    /**
     * @param string $chequeEmitente
     */
    public function setChequeEmitente($chequeEmitente)
    {
        $this->chequeEmitente = $chequeEmitente;
    }

    /**
     * @return string
     */
    public function getChequeNum()
    {
        return $this->chequeNum;
    }

    /**
     * @param string $chequeNum
     */
    public function setChequeNum($chequeNum)
    {
        $this->chequeNum = $chequeNum;
    }

    /**
     * @return string
     */
    public function getChequeBanco()
    {
        return $this->chequeBanco;
    }

    /**
     * @param string $chequeBanco
     */
    public function setChequeBanco($chequeBanco)
    {
        $this->chequeBanco = $chequeBanco;
    }

    /**
     * @return string
     */
    public function getChequeAgencia()
    {
        return $this->chequeAgencia;
    }

    /**
     * @param string $chequeAgencia
     */
    public function setChequeAgencia($chequeAgencia)
    {
        $this->chequeAgencia = $chequeAgencia;
    }

    /**
     * @return string
     */
    public function getChequePraca()
    {
        return $this->chequePraca;
    }

    /**
     * @param string $chequePraca
     */
    public function setChequePraca($chequePraca)
    {
        $this->chequePraca = $chequePraca;
    }

    /**
     * @return \DateTime
     */
    public function getChequeEmissao()
    {
        return $this->chequeEmissao->format("d/m/Y");
    }

    /**
     * @param \DateTime $chequeEmissao
     */
    public function setChequeEmissao($chequeEmissao)
    {
        $this->chequeEmissao = $chequeEmissao;
    }

    /**
     * @return \DateTime
     */
    public function getChequeVencimento()
    {
        return $this->chequeVencimento->format("d/m/Y");
    }

    /**
     * @param \DateTime $chequeVencimento
     */
    public function setChequeVencimento($chequeVencimento)
    {
        $this->chequeVencimento = $chequeVencimento;
    }

    /**
     * @return \DateTime
     */
    public function getChequeCompensado()
    {
        return $this->chequeCompensado->format("d/m/Y");
    }

    /**
     * @param \DateTime $chequeCompensado
     */
    public function setChequeCompensado($chequeCompensado)
    {
        $this->chequeCompensado = $chequeCompensado;
    }

    /**
     * @return float
     */
    public function getChequeValor()
    {
        return $this->chequeValor;
    }

    public function getChequeValorMoeda()
    {
        return \Boleto\Service\Moeda::decToMoeda($this->chequeValor);
    }

    /**
     * @param float $chequeValor
     */
    public function setChequeValor($chequeValor)
    {
        $this->chequeValor = $chequeValor;
    }


    /**
     * @return string
     */
    public function getChequeConta()
    {
        return $this->chequeConta;
    }

    /**
     * @param string $chequeConta
     */
    public function setChequeConta($chequeConta)
    {
        $this->chequeConta = $chequeConta;
    }

    public function toArray()
    {
        return [
            'chequeId'          => $this->getChequeId(),
            'chequeEmitente'    => $this->getChequeEmitente(),
            'chequeNum'         => $this->getChequeNum(),
            'chequeBanco'       => $this->getChequeBanco(),
            'chequeAgencia'     => $this->getChequeAgencia(),
            'chequePraca'       => $this->getChequePraca(),
            'chequeEmissao'     => $this->getChequeEmissao(),
            'chequeVencimento'  => $this->getChequeVencimento(),
            'chequeCompensado'  => $this->getChequeCompensado(),
            'chequeValor'       => $this->getChequeValor(),
            'chequeValorMoeda'  => $this->getChequeValorMoeda(),
        ];
    }
}
