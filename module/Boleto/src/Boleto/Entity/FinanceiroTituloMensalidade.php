<?php

namespace Boleto\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FinanceiroTituloMensalidade
 *
 * @ORM\Table(name="financeiro__titulo_mensalidade", indexes={@ORM\Index(name="fk_financeiro_titulo_mensalidade_financeiro__titulo1_idx", columns={"titulo_id"}), @ORM\Index(name="fk_financeiro_titulo_mensalidade_financeiro__titulo_config1_idx", columns={"tituloconf_id"}), @ORM\Index(name="fk_financeiro_titulo_mensalidade_acadperiodo__aluno1_idx", columns={"alunoper_id"})})
 * @ORM\Entity
 */
class FinanceiroTituloMensalidade
{
    /**
     * @var integer
     *
     * @ORM\Column(name="financeiro_titulo_mensalidade_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $financeiroTituloMensalidadeId;

    /**
     * @var string
     *
     * @ORM\Column(name="mensalidade_parcela", type="string", nullable=false)
     */
    private $mensalidadeParcela;

    /**
     * @var \Matricula\Entity\AcadperiodoAluno
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoAluno")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="alunoper_id", referencedColumnName="alunoper_id")
     * })
     */
    private $alunoper;

    /**
     * @var FinanceiroTitulo
     *
     * @ORM\ManyToOne(targetEntity="FinanceiroTitulo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="titulo_id", referencedColumnName="titulo_id")
     * })
     */
    private $titulo;

    /**
     * @var FinanceiroTituloConfig
     *
     * @ORM\ManyToOne(targetEntity="FinanceiroTituloConfig")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tituloconf_id", referencedColumnName="tituloconf_id")
     * })
     */
    private $tituloconf;

    public function __construct(array $data)
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getFinanceiroTituloMensalidadeId()
    {
        return $this->financeiroTituloMensalidadeId;
    }

    /**
     * @param int $financeiroTituloMensalidadeId
     */
    public function setFinanceiroTituloMensalidadeId($financeiroTituloMensalidadeId)
    {
        $this->financeiroTituloMensalidadeId = $financeiroTituloMensalidadeId;
    }

    /**
     * @return string
     */
    public function getMensalidadeParcela()
    {
        return $this->mensalidadeParcela;
    }

    /**
     * @param string $mensalidadeParcela
     */
    public function setMensalidadeParcela($mensalidadeParcela)
    {
        $this->mensalidadeParcela = $mensalidadeParcela;
    }

    /**
     * @return \Matricula\Entity\AcadperiodoAluno
     */
    public function getAlunoper()
    {
        return $this->alunoper;
    }

    /**
     * @param \Matricula\Entity\AcadperiodoAluno $alunoper
     */
    public function setAlunoper($alunoper)
    {
        $this->alunoper = $alunoper;
    }

    /**
     * @return FinanceiroTitulo
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param FinanceiroTitulo $titulo
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    /**
     * @return FinanceiroTituloConfig
     */
    public function getTituloconf()
    {
        return $this->tituloconf;
    }

    /**
     * @param FinanceiroTituloConfig $tituloconf
     */
    public function setTituloconf($tituloconf)
    {
        $this->tituloconf = $tituloconf;
    }

    public function toArray()
    {
        return [
            'financeiroTituloMensalidadeId' => $this->getFinanceiroTituloMensalidadeId(),
            'mensalidadeParcela'            => $this->getMensalidadeParcela(),
            'alunoper'                      => $this->getAlunoper()->getAlunoperId(),
            'tituloEstado'                  => $this->getTitulo()->getTituloEstado(),
        ];
    }
}
