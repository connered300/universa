<?php

namespace Boleto\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FinanceiroValores
 *
 * @ORM\Table(name="financeiro__valores", indexes={@ORM\Index(name="fk_financeiro__mensalidade_campus_curso1_idx", columns={"cursocampus_id"}), @ORM\Index(name="fk_financeiro__mensalidade_acadperiodo__letivo1_idx", columns={"per_id"}), @ORM\Index(name="fk_financeiro__mensalidade_financeiro__titulo_tipo1_idx", columns={"tipotitulo_id"})})
 * @ORM\Entity
 */
class FinanceiroValores
{
    /**
     * @var integer
     *
     * @ORM\Column(name="valores_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $valoresId;

    /**
     * @var float
     *
     * @ORM\Column(name="valores_preco", type="float", precision=10, scale=2, nullable=true)
     */
    private $valoresPreco;

    /**
     * @var \Matricula\Entity\AcadperiodoLetivo
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoLetivo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="per_id", referencedColumnName="per_id")
     * })
     */
    private $per;

    /**
     * @var \Matricula\Entity\CampusCurso
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\CampusCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cursocampus_id", referencedColumnName="cursocampus_id")
     * })
     */
    private $cursocampus;

    /**
     * @var FinanceiroTituloTipo
     *
     * @ORM\ManyToOne(targetEntity="FinanceiroTituloTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipotitulo_id", referencedColumnName="tipotitulo_id")
     * })
     */
    private $tipotitulo;

    public function __construct(array $data)
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getValoresId()
    {
        return $this->valoresId;
    }

    /**
     * @param int $valoresId
     */
    public function setValoresId($valoresId)
    {
        $this->valoresId = $valoresId;
    }

    /**
     * @return float
     */
    public function getValoresPreco()
    {
        return $this->valoresPreco;
    }

    /**
     * @param float $valoresPreco
     */
    public function setValoresPreco($valoresPreco)
    {
        $this->valoresPreco = $valoresPreco;
    }

    /**
     * @return \Matricula\Entity\AcadperiodoLetivo
     */
    public function getPer()
    {
        return $this->per;
    }

    /**
     * @param \Matricula\Entity\AcadperiodoLetivo $per
     */
    public function setPer($per)
    {
        $this->per = $per;
    }

    /**
     * @return \Matricula\Entity\CampusCurso
     */
    public function getCursocampus()
    {
        return $this->cursocampus;
    }

    /**
     * @param \Matricula\Entity\CampusCurso $cursocampus
     */
    public function setCursocampus($cursocampus)
    {
        $this->cursocampus = $cursocampus;
    }

    /**
     * @return FinanceiroTituloTipo
     */
    public function getTipotitulo()
    {
        return $this->tipotitulo;
    }

    /**
     * @param FinanceiroTituloTipo $tipotitulo
     */
    public function setTipotitulo($tipotitulo)
    {
        $this->tipotitulo = $tipotitulo;
    }
}
