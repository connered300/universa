<?php


namespace Boleto\Entity\Repository;


use Doctrine\ORM\EntityRepository;

class FinanceiroTituloTipo extends EntityRepository{
    public function buscaTipos($tipo = null){
        $array = array();
        if(!is_null($tipo)){
            $data = $this->findBy(array('tipotituloNome' => $tipo));
        } else{
            $data = $this->findAll();
        }

        foreach ($data as $tipos) {
            $array[$tipos->getTipoTituloId()] = $tipos->getTipoTituloNome();
        }

        return $array;

    }
}