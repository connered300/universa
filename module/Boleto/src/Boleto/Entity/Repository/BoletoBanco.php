<?php


namespace Boleto\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class BoletoBanco extends EntityRepository{
    public function buscaBancos()
    {
        $bancos = $this->findAll();
        $array = array();
        foreach ($bancos as $banco) {
            $banc = str_pad($banco->getBancCodigo(), 3, "0",STR_PAD_LEFT)." | ".$banco->getBancNome();
            $array[$banco->getBancId()] = $banc;
        }
        return $array;

    }
    public function buscaBancosCodNome()
    {
        $bancos = $this->findAll();
        $array = array();
        foreach ($bancos as $banco) {
            $banc = str_pad($banco->getBancCodigo(), 3, "0",STR_PAD_LEFT)." | ".$banco->getBancNome();
            $array[] = [ 'codigo' => $banco->getBancNome(),'nome' => $banco->getBancNome() ];
        }
        return $array;

    }
}