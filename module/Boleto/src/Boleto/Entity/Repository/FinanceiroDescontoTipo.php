<?php


namespace Boleto\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class FinanceiroDescontoTipo extends EntityRepository{
    public function buscaTiposDesconto()
    {
        $descontos = $this->findAll();
        $array = array();
        foreach ($descontos as $desconto) {
            $array[$desconto->getDesctipoId()] = $desconto->getDesctipoDescricao();
        }
        return $array;
    }
}