<?php


namespace Boleto\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class BoletoConfConta extends EntityRepository{
    public function buscaContas()
    {
        $contas = $this->findAll();

        $array = array();

        foreach ($contas as $conta) {
            $array[$conta->getConfContId()] = $conta->getConfContAgencia()."-".$conta->getConfContAgenciaDigito()." | ".$conta->getConfContConta().'-'.$conta->getConfcontContaDigito()." | ".$conta->getBanc()->getBancNome();
        }
        return $array;

    }

}