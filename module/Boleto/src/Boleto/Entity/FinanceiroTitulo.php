<?php

namespace Boleto\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * FinanceiroTitulo
 *
 * @ORM\Table(name="financeiro__titulo", indexes={@ORM\Index(name="fk_titulo_pessoa1_idx", columns={"pes_id"}), @ORM\Index(name="fk_titulo_boleto_tipo1_idx", columns={"tipotitulo_id"}), @ORM\Index(name="fk_financeiro__titulo_acesso_pessoas1_idx", columns={"usuario_baixa"}), @ORM\Index(name="fk_financeiro__titulo_acesso_pessoas2_idx", columns={"usuario_autor"}), @ORM\Index(name="fk_financeiro__titulo_financeiro__titulo1_idx", columns={"titulo_novo"})})
 * @ORM\Entity
 * @LG\LG(id="tituloId",label="titulo_id")
 * @Jarvis\Jarvis(title="Titulos Financeiros",icon="fa fa-table")
 */
class FinanceiroTitulo
{
    static $formasPagamento = [
        'Dinheiro',
        'Cheque',
        'Deposito',
        'Crédito Fies'
    ];

    /**
     * @var integer
     *
     * @ORM\Column(name="titulo_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="titulo_id")
     * @LG\Labels\Attributes(text="Título",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloId;

    /**
     * @var \Matricula\Entity\AcadgeralAlunoCurso
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadgeralAlunoCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="alunocurso_id", referencedColumnName="alunocurso_id")
     * })
     * @LG\Querys\Conditions(type="LIKE%")
     */
    private $alunocurso;
    /**
     * @var \Pessoa\Entity\Pessoa
     *
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     * @LG\Labels\Property(name="pes_nome")
     * @LG\Labels\Attributes(text="Nome",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE%")
     */
    private $pes;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo_tipo_pagamento", type="string", nullable=false)
     * @LG\Labels\Property(name="titulo_tipo_pagamento")
     * @LG\Labels\Attributes(text="Tipo Pagamento",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloTipoPagamento;

    /**
     * @var float
     *
     * @ORM\Column(name="titulo_valor", type="float", precision=10, scale=2, nullable=false)
     * @LG\Labels\Property(name="titulo_valor")
     * @LG\Labels\Attributes(text="Valor",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloValor;

    /**
     * @var float
     *
     * @ORM\Column(name="titulo_valor_pago_dinheiro", type="float", precision=10, scale=2, nullable=true)
     */
    private $tituloValorPagoDinheiro;

    /**
     * @var float
     *
     * @ORM\Column(name="titulo_multa", type="float", precision=10, scale=2, nullable=true)
     */
    private $tituloMulta;

    /**
     * @var float
     *
     * @ORM\Column(name="titulo_juros", type="float", precision=10, scale=2, nullable=true)
     */
    private $tituloJuros;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo_estado", type="string", nullable=true)
     * @LG\Labels\Property(name="titulo_estado")
     * @LG\Labels\Attributes(text="Estado",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloEstado;
    /**
     * @var string
     *
     * @LG\Labels\Property(name="tipo")
     * @LG\Labels\Attributes(text="Tipo",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $tipo;

    /**
     * @var float
     *
     * @ORM\Column(name="titulo_valor_pago", type="float", precision=10, scale=2, nullable=true)
     */
    private $tituloValorPago;

    /**
     * @var float
     *
     * @ORM\Column(name="titulo_desconto", type="float", precision=10, scale=2, nullable=true)
     */
    private $tituloDesconto;

    /**
     * @var float
     *
     * @ORM\Column(name="titulo_desconto_manual", type="float", precision=10, scale=2, nullable=true)
     */
    private $tituloDescontoManual;

    /**
     * @var float
     *
     * @ORM\Column(name="titulo_acrescimo_manual", type="float", precision=10, scale=2, nullable=true)
     */
    private $tituloAcrescimoManual;

    /**
     * @var FinanceiroTituloTipo
     *
     * @ORM\ManyToOne(targetEntity="FinanceiroTituloTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipotitulo_id", referencedColumnName="tipotitulo_id")
     * })
     */
    private $tipotitulo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="titulo_data_processamento", type="datetime", nullable=false)
     * @LG\Labels\Property(name="titulo_data_processamento")
     * @LG\Labels\Attributes(text="Dt. Processamento",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloDataProcessamento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="titulo_data_vencimento", type="datetime", nullable=false)
     * @LG\Labels\Property(name="titulo_data_vencimento")
     * @LG\Labels\Attributes(text="Dt. Vencimento",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     *
     */
    private $tituloDataVencimento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="titulo_data_pagamento", type="datetime", nullable=true)
     */
    private $tituloDataPagamento;

    /**
     * @var integer
     *
     * @LG\Labels\Property(name="financeiro_baixa")
     * @LG\Labels\Attributes(text="Baixa",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $baixa;

    /**
     * @var integer
     *
     * @LG\Labels\Property(name="financeiro_boleto")
     * @LG\Labels\Attributes(text="Boleto",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $boleto;

    /**
     * @var string
     *
     * @LG\Labels\Property(name="parcela")
     * @LG\Labels\Attributes(text="Parcela",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $parcela;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo_observacoes", type="text", nullable=true)
     */
    private $tituloObservacoes;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     *
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_baixa", referencedColumnName="id")
     * })
     */
    private $usuarioBaixa;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     *
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_autor", referencedColumnName="id")
     * })
     */
    private $usuarioAutor;

    public function __construct(array $data)
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @var FinanceiroTitulo
     *
     * @ORM\ManyToOne(targetEntity="FinanceiroTitulo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="titulo_novo", referencedColumnName="titulo_id")
     * })
     */
    private $tituloNovo;

    /**
     * @return int
     */
    public function getTituloId()
    {
        return $this->tituloId;
    }

    /**
     * @param int $tituloId
     */
    public function setTituloId($tituloId)
    {
        $this->tituloId = $tituloId;
    }

    /**
     * @return \DateTime
     */
    public function getTituloDataProcessamento($format = true)
    {
        if ($format) {
            return $this->tituloDataProcessamento->format("d/m/Y");
        }

        return $this->tituloDataProcessamento;
    }

    /**
     * @param \DateTime $tituloDataProcessamento
     */
    public function setTituloDataProcessamento($tituloDataProcessamento)
    {
        $this->tituloDataProcessamento = $tituloDataProcessamento;
    }

    /**
     * @return \DateTime
     */
    public function getTituloDataVencimento($format = true)
    {
        if ($format) {
            return $this->tituloDataVencimento->format("d/m/Y");
        }

        return $this->tituloDataVencimento;
    }

    /**
     * @param \DateTime $tituloDataVencimento
     */
    public function setTituloDataVencimento($tituloDataVencimento)
    {
        $this->tituloDataVencimento = $tituloDataVencimento;
    }

    /**
     * @return string
     */
    public function getTituloTipoPagamento()
    {
        return $this->tituloTipoPagamento;
    }

    /**
     * @param string $tituloTipoPagamento
     */
    public function setTituloTipoPagamento($tituloTipoPagamento)
    {
        $this->tituloTipoPagamento = $tituloTipoPagamento;
    }

    /**
     * @return float
     */
    public function getTituloValor()
    {
        return $this->tituloValor;
    }

    /**
     * @param float $tituloValor
     */
    public function setTituloValor($tituloValor)
    {
        $this->tituloValor = $tituloValor;
    }

    /**
     * @return float
     */
    public function getTituloValorPagoDinheiro()
    {
        return $this->tituloValorPagoDinheiro;
    }

    /**
     * @param float $tituloValorPagoDinheiro
     */
    public function setTituloValorPagoDinheiro($tituloValorPagoDinheiro)
    {
        $this->tituloValorPagoDinheiro = $tituloValorPagoDinheiro;
    }

    /**
     * @return \String
     */
    public function getTituloDataPagamento()
    {
        if (is_null($this->tituloDataPagamento)) {
            return "";
        }

        return $this->tituloDataPagamento->format("d/m/Y");
    }

    /**
     * @return \DateTime
     */
    public function getTituloDataHoraPagamento()
    {
        return $this->tituloDataPagamento;
    }

    /**
     * @param \DateTime $tituloDataPagamento
     */
    public function setTituloDataPagamento($tituloDataPagamento)
    {
        $this->tituloDataPagamento = $tituloDataPagamento;
    }

    /**
     * @return float
     */
    public function getTituloMulta()
    {
        return $this->tituloMulta;
    }

    /**
     * @param float $tituloMulta
     */
    public function setTituloMulta($tituloMulta)
    {
        $this->tituloMulta = $tituloMulta;
    }

    /**
     * @return float
     */
    public function getTituloJuros()
    {
        return $this->tituloJuros;
    }

    /**
     * @param float $tituloJuros
     */
    public function setTituloJuros($tituloJuros)
    {
        $this->tituloJuros = $tituloJuros;
    }

    /**
     * @return string
     */
    public function getTituloEstado()
    {
        return $this->tituloEstado;
    }

    /**
     * @param string $tituloEstado
     */
    public function setTituloEstado($tituloEstado)
    {
        $this->tituloEstado = $tituloEstado;
    }

    /**
     * @return float
     */
    public function getTituloValorPago()
    {
        return $this->tituloValorPago;
    }

    /**
     * @param float $tituloValorPago
     */
    public function setTituloValorPago($tituloValorPago)
    {
        $this->tituloValorPago = $tituloValorPago;
    }

    /**
     * @return FinanceiroTituloTipo
     */
    public function getTipotitulo()
    {
        return $this->tipotitulo;
    }

    /**
     * @param FinanceiroTituloTipo $tipotitulo
     */
    public function setTipotitulo($tipotitulo)
    {
        $this->tipotitulo = $tipotitulo;
    }

    /**
     * @return \Pessoa\Entity\Pessoa
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @param \Pessoa\Entity\Pessoa $pes
     */
    public function setPes($pes)
    {
        $this->pes = $pes;
    }

    /**
     * @return float
     */
    public function getTituloDesconto()
    {
        return $this->tituloDesconto;
    }

    /**
     * @param float $tituloDesconto
     */
    public function setTituloDesconto($tituloDesconto)
    {
        $this->tituloDesconto = $tituloDesconto;
    }

    public function setParcela($parcela)
    {
        $this->parcela = $parcela;
    }

    public function getParcela()
    {
        return $this->parcela;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuarioBaixa()
    {
        return $this->usuarioBaixa;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuarioBaixa
     */
    public function setUsuarioBaixa($usuarioBaixa)
    {
        $this->usuarioBaixa = $usuarioBaixa;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuarioAutor()
    {
        return $this->usuarioAutor;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuarioAutor
     */
    public function setUsuarioAutor($usuarioAutor)
    {
        $this->usuarioAutor = $usuarioAutor;
    }

    /**
     * @return string
     */
    public function getTituloObservacoes()
    {
        return $this->tituloObservacoes;
    }

    /**
     * @param string $tituloObservacoes
     */
    public function setTituloObservacoes($tituloObservacoes)
    {
        $this->tituloObservacoes = $tituloObservacoes;
    }

    /**
     * @return float
     */
    public function getTituloDescontoManual()
    {
        return $this->tituloDescontoManual;
    }

    /**
     * @param float $tituloDescontoManual
     */
    public function setTituloDescontoManual($tituloDescontoManual)
    {
        $this->tituloDescontoManual = $tituloDescontoManual;
    }

    /**
     * @return float
     */
    public function getTituloAcrescimoManual()
    {
        return $this->tituloAcrescimoManual;
    }

    /**
     * @param float $tituloAcrescimoManual
     */
    public function setTituloAcrescimoManual($tituloAcrescimoManual)
    {
        $this->tituloAcrescimoManual = $tituloAcrescimoManual;
    }

    /**
     * @return FinanceiroTitulo
     */
    public function getTituloNovo()
    {
        return $this->tituloNovo;
    }

    /**
     * @param FinanceiroTitulo $tituloNovo
     */
    public function setTituloNovo($tituloNovo)
    {
        $this->tituloNovo = $tituloNovo;
    }

    /**
     * @return \DateTime
     */
    public function getTituloMesVencimento()
    {
        switch ($this->tituloDataVencimento->format("m")) {
            case "01":
                $mes = 'Janeiro';
                break;
            case "02":
                $mes = 'Fevereiro';
                break;
            case "03":
                $mes = 'Março';
                break;
            case "04":
                $mes = 'Abril';
                break;
            case "05":
                $mes = 'Maio';
                break;
            case "06":
                $mes = 'Junho';
                break;
            case "07":
                $mes = 'Julho';
                break;
            case "08":
                $mes = 'Agosto';
                break;
            case "09":
                $mes = 'Setembro';
                break;
            case "10":
                $mes = 'Outubro';
                break;
            case "11":
                $mes = 'Novembro';
                break;
            case "12":
                $mes = 'Dezembro';
                break;
        }

        return $mes;
    }

    /**
     * @return \Matricula\Entity\AcadgeralAlunoCurso
     */
    public function getAlunocurso()
    {
        return $this->alunocurso;
    }

    /**
     * @param \Matricula\Entity\AcadgeralAlunoCurso $alunocurso
     * @return FinanceiroTitulo
     */
    public function setAlunocurso($alunocurso)
    {
        $this->alunocurso = $alunocurso;

        return $this;
    }

    /**
     * @return int
     */
    public function getBoleto()
    {
        return $this->boleto;
    }

    /**
     * @param int $boleto
     * @return FinanceiroTitulo
     */
    public function setBoleto($boleto)
    {
        $this->boleto = $boleto;

        return $this;
    }

    public function toArray()
    {
        return [
            'tituloId'                => $this->getTituloId(),
            'pessoaNome'              => $this->getPes()->getPesNome(),
            'pes'                     => $this->getPes()->getPesId(),
            'tipotitulo'              => $this->getTipotitulo()->getTipotituloId(),
            'tituloTipoPagamento'     => $this->getTituloTipoPagamento(),
            'tituloDataProcessamento' => $this->getTituloDataProcessamento(),
            'tituloDataPagamento'     => $this->getTituloDataPagamento(),
            'tituloDataHoraPagamento' => $this->getTituloDataHoraPagamento(),
            'tituloDataVencimento'    => $this->getTituloDataVencimento(),
            'tituloEstado'            => $this->getTituloEstado(),
            'tituloJuros'             => $this->getTituloJuros(),
            'tituloMulta'             => $this->getTituloMulta(),
            'tituloValor'             => $this->getTituloValor(),
            'tituloValorPago'         => $this->getTituloValorPago(),
            'tituloValorPagoDinheiro' => $this->getTituloValorPagoDinheiro(),
            'tituloObservacoes'       => $this->getTituloObservacoes(),
            'tituloDesconto'          => $this->getTituloDesconto(),
            'tipotituloNome'          => $this->getTipotitulo()->getTipotituloNome(),
            'tituloDescontoManual'    => $this->getTituloDescontoManual(),
            'tituloAcrescimoManual'   => $this->getTituloAcrescimoManual(),
            'usuarioBaixa'            => $this->getUsuarioBaixa(),
            'usuarioAutor'            => $this->getUsuarioAutor(),
            'tituloMesVencimento'     => $this->getTituloMesVencimento(),
            'tituloNovo'              => $this->getTituloNovo(),
        ];
    }
}