<?php

namespace Boleto\Entity;

use Doctrine\ORM\Mapping as ORM;

use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * FinanceiroDescontoTipo
 *
 * @ORM\Table(name="financeiro__desconto_tipo")
 * @ORM\Entity(repositoryClass="Boleto\Entity\Repository\FinanceiroDescontoTipo")
 * @LG\LG(id="desctipoId",label="desctipo_descricao")
 * @Jarvis\Jarvis(title="Desconto Tipo",icon="fa fa-table")
 */
class FinanceiroDescontoTipo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="desctipo_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $desctipoId;

    /**
     * @var string
     *
     * @ORM\Column(name="desctipo_descricao", type="string", length=45, nullable=false)
     */
    private $desctipoDescricao;

    /**
     * @var float
     *
     * @ORM\Column(name="desctipo_percmax", type="float", precision=6, scale=2, nullable=true)
     */
    private $desctipoPercmax;

    /**
     * @var float
     *
     * @ORM\Column(name="desctipo_valormax", type="float", precision=10, scale=2, nullable=true)
     */
    private $desctipoValormax;

    /**
     * @var string
     *
     * @ORM\Column(name="desctipo_modalidade", type="string", nullable=true)
     */
    private $desctipoModalidade;

    /**
     * @var string
     *
     * @ORM\Column(name="desctipo_limita_vencimento", type="string", nullable=false)
     */
    private $desctipoLimitaVencimento;


    /**
     * @return int
     */
    public function getDesctipoId()
    {
        return $this->desctipoId;
    }

    /**
     * @param int $desctipoId
     */
    public function setDesctipoId($desctipoId)
    {
        $this->desctipoId = $desctipoId;
    }

    /**
     * @return string
     */
    public function getDesctipoDescricao()
    {
        return $this->desctipoDescricao;
    }

    /**
     * @param string $desctipoDescricao
     */
    public function setDesctipoDescricao($desctipoDescricao)
    {
        $this->desctipoDescricao = $desctipoDescricao;
    }

    /**
     * @return float
     */
    public function getDesctipoPercmax()
    {
        return $this->desctipoPercmax;
    }

    /**
     * @param float $desctipoPercmax
     */
    public function setDesctipoPercmax($desctipoPercmax)
    {
        $this->desctipoPercmax = $desctipoPercmax;
    }

    /**
     * @return float
     */
    public function getDesctipoValormax()
    {
        return $this->desctipoValormax;
    }

    /**
     * @param float $desctipoValormax
     */
    public function setDesctipoValormax($desctipoValormax)
    {
        $this->desctipoValormax = $desctipoValormax;
    }

    /**
     * @return string
     */
    public function getDesctipoModalidade()
    {
        return $this->desctipoModalidade;
    }

    /**
     * @param string $desctipoModalidade
     */
    public function setDesctipoModalidade($desctipoModalidade)
    {
        $this->desctipoModalidade = $desctipoModalidade;
    }

    /**
     * @return string
     */
    public function getDesctipoLimitaVencimento()
    {
        return $this->desctipoLimitaVencimento;
    }

    /**
     * @param string $desctipoLimitaVencimento
     */
    public function setDesctipoLimitaVencimento($desctipoLimitaVencimento)
    {
        $this->desctipoLimitaVencimento = $desctipoLimitaVencimento;
    }


    public function toArray()
    {
        return [
            'desctipoId'                        => $this->getDesctipoId(),
            'desctipoDescricao'                 => $this->getDesctipoDescricao(),
            'desctipoDescricaoLimitaVencimento' => $this->getDesctipoLimitaVencimento(),
            'desctipoModalidade'                => $this->getDesctipoModalidade(),
            'desctipoPercmax'                   => $this->getDesctipoPercmax(),
            'desctipoValormax'                  => $this->getDesctipoValormax(),
        ];
    }
}
