<?php

namespace Boleto\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * Boleto
 *
 * @ORM\Table(name="boleto", indexes={@ORM\Index(name="fk_boleto_configuracao_boleto1_idx", columns={"confcont_id"}), @ORM\Index(name="fk_boleto_titulo1_idx", columns={"titulo_id"}),@ORM\Index(name="fk_boleto_baixas_efetuadas1_idx", columns={"baixa_id"})})
 * @ORM\Entity
 * @LG\LG(id="bolId",label="bol_id")
 * @Jarvis\Jarvis(title="Boletos Bancarios",icon="fa fa-table")
 */
class Boleto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="bol_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="bol_id")
     * @LG\Labels\Attributes(text="N° Boleto",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $bolId;

    /**
     * @var integer
     *
     * @ORM\Column(name="bol_nosso_numero", type="bigint", nullable=false)
     */
    private $bolNossoNumero;

    /**
     * @var integer
     *
     * @ORM\Column(name="bol_numero_documento", type="bigint", nullable=false)
     * @LG\Labels\Property(name="bol_numero_documento")
     * @LG\Labels\Attributes(text="Número do Documento",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $bolNumeroDocumento;

    /**
     * @var float
     *
     * @ORM\Column(name="bol_valor_unitario", type="float", precision=10, scale=0, nullable=true)
     *
     *
     */
    private $bolValorUnitario;

    /**
     * @var float
     *
     * @ORM\Column(name="bol_quantidade", type="float", precision=10, scale=0, nullable=true)
     */
    private $bolQuantidade;

    /**
     * @var string
     *
     * @ORM\Column(name="bol_estado", type="string", nullable=false)
     * @LG\Labels\Property(name="bol_estado")
     * @LG\Labels\Attributes(text="Estado",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $bolEstado;

    /**
     * @var string
     *
     * @ORM\Column(name="bol_demostrativo", type="text", nullable=true)
     */
    private $bolDemostrativo;

    /**
     * @var string
     *
     * @ORM\Column(name="bol_instrucoes", type="text", nullable=true)
     */
    private $bolInstrucoes;

    /**
     * @var integer
     *
     * @ORM\Column(name="bol_via", type="integer", nullable=false)
     */
    private $bolVia = '1';

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="bol_desconto_data", type="datetime", nullable=true)
     * @LG\Labels\Property(name="bol_desconto_data")
     * @LG\Labels\Attributes(text="data desconto")
     * @LG\Querys\Conditions(type="=")
     */
    private $bolDescontoData;

    /**
     * @var float
     *
     * @ORM\Column(name="bol_desconto_valor", type="float", nullable=true)
     * @LG\Labels\Property(name="bol_desconto_valor")
     * @LG\Labels\Attributes(text="valor desconto")
     * @LG\Querys\Conditions(type="=")
     */
    private $bolDescontoValor;

    /**
     * @var float
     *
     * @ORM\Column(name="bol_multa", type="float", nullable=true, length=10)
     * @LG\Labels\Property(name="bol_multa")
     * @LG\Labels\Attributes(text="multa")
     * @LG\Querys\Conditions(type="=")
     */
    private $bolMulta;

    /**
     * @var float
     *
     * @ORM\Column(name="bol_juros", type="float", nullable=true, length=10)
     * @LG\Labels\Property(name="bol_juros")
     * @LG\Labels\Attributes(text="juros")
     * @LG\Querys\Conditions(type="=")
     */
    private $bolJuros;

    /**
     * @var float
     *
     * @ORM\Column(name="bol_acrescimos", type="float", nullable=true, length=10)
     * @LG\Labels\Property(name="bol_acrescimos")
     * @LG\Labels\Attributes(text="acrescimos")
     * @LG\Querys\Conditions(type="=")
     */
    private $bolAcrescimos;

    /**
     * @var float
     *
     * @ORM\Column(name="bol_valor_pago", type="float", nullable=true, length=10)
     * @LG\Labels\Property(name="bol_valor_pago")
     * @LG\Labels\Attributes(text="pago valor")
     * @LG\Querys\Conditions(type="=")
     */
    private $bolValorPago;

    /**
     * @var BoletoConfConta
     * @ORM\ManyToOne(targetEntity="BoletoConfConta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="confcont_id", referencedColumnName="confcont_id")
     * })
     */
    private $confcont;

    /**
     * @var FinanceiroTitulo
     *
     * @ORM\ManyToOne(targetEntity="FinanceiroTitulo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="titulo_id", referencedColumnName="titulo_id")
     * })
     */
    private $titulo;

    /**
     * @var BoletoBaixa
     *
     * @ORM\ManyToOne(targetEntity="BoletoBaixa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="baixa_id", referencedColumnName="baixa_id")
     * })
     */
    private $baixa;

    /**
     * @var string
     *
     * @ORM\Column(name="bol_carteira",type="string", nullable=false, length=2)
     * @LG\Labels\Property(name="bol_carteira")
     * @LG\Labels\Attributes(text="carteira")
     * @LG\Querys\Conditions(type="=")
     */
    private $bolCarteira = '24';

    public function __construct(array $data)
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getBolId()
    {
        return $this->bolId;
    }

    /**
     * @param int $bolId
     */
    public function setBolId($bolId)
    {
        $this->bolId = $bolId;
    }

    /**
     * @return int
     */
    public function getBolNossoNumero()
    {
        return $this->bolNossoNumero;
    }

    /**
     * @param int $bolNossoNumero
     */
    public function setBolNossoNumero($bolNossoNumero)
    {
        $this->bolNossoNumero = $bolNossoNumero;
    }

    /**
     * @return int
     */
    public function getBolNumeroDocumento()
    {
        return $this->bolNumeroDocumento;
    }

    /**
     * @param int $bolNumeroDocumento
     */
    public function setBolNumeroDocumento($bolNumeroDocumento)
    {
        $this->bolNumeroDocumento = $bolNumeroDocumento;
    }

    /**
     * @return float
     */
    public function getBolValorUnitario()
    {
        return $this->bolValorUnitario;
    }

    /**
     * @param float $bolValorUnitario
     */
    public function setBolValorUnitario($bolValorUnitario)
    {
        $this->bolValorUnitario = $bolValorUnitario;
    }

    /**
     * @return float
     */
    public function getBolQuantidade()
    {
        return $this->bolQuantidade;
    }

    /**
     * @param float $bolQuantidade
     */
    public function setBolQuantidade($bolQuantidade)
    {
        $this->bolQuantidade = $bolQuantidade;
    }

    /**
     * @return string
     */
    public function getBolDemostrativo()
    {
        return $this->bolDemostrativo;
    }

    /**
     * @param string $bolDemostrativo
     */
    public function setBolDemostrativo($bolDemostrativo)
    {
        $this->bolDemostrativo = $bolDemostrativo;
    }

    /**
     * @return string
     */
    public function getBolInstrucoes()
    {
        return $this->bolInstrucoes;
    }

    /**
     * @param string $bolInstrucoes
     */
    public function setBolInstrucoes($bolInstrucoes)
    {
        $this->bolInstrucoes = $bolInstrucoes;
    }

    /**
     * @return FinanceiroTitulo
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param FinanceiroTitulo $titulo
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    /**
     * @return string
     */
    public function getBolEstado()
    {
        return $this->bolEstado;
    }

    /**
     * @param string $bolEstado
     */
    public function setBolEstado($bolEstado)
    {
        $this->bolEstado = $bolEstado;
    }

    /**
     * @return int
     */
    public function getBolVia()
    {
        return $this->bolVia;
    }

    /**
     * @param int $bolVia
     */
    public function setBolVia($bolVia)
    {
        $this->bolVia = $bolVia;
    }

    /**
     * @return \Datetime
     */
    public function getBolDescontoData()
    {
        return $this->bolDescontoData;
    }

    /**
     * @param \Datetime $bolDescontoData
     * @return Boleto
     */
    public function setBolDescontoData($bolDescontoData)
    {
        $this->bolDescontoData = $bolDescontoData;

        return $this;
    }

    /**
     * @return float
     */
    public function getBolDescontoValor()
    {
        return $this->bolDescontoValor;
    }

    /**
     * @param float $bolDescontoValor
     * @return Boleto
     */
    public function setBolDescontoValor($bolDescontoValor)
    {
        $this->bolDescontoValor = $bolDescontoValor;

        return $this;
    }

    /**
     * @return float
     */
    public function getBolMulta()
    {
        return $this->bolMulta;
    }

    /**
     * @param float $bolMulta
     * @return Boleto
     */
    public function setBolMulta($bolMulta)
    {
        $this->bolMulta = $bolMulta;

        return $this;
    }

    /**
     * @return float
     */
    public function getBolJuros()
    {
        return $this->bolJuros;
    }

    /**
     * @param float $bolJuros
     * @return Boleto
     */
    public function setBolJuros($bolJuros)
    {
        $this->bolJuros = $bolJuros;

        return $this;
    }

    /**
     * @return float
     */
    public function getBolAcrescimos()
    {
        return $this->bolAcrescimos;
    }

    /**
     * @param float $bolAcrescimos
     * @return Boleto
     */
    public function setBolAcrescimos($bolAcrescimos)
    {
        $this->bolAcrescimos = $bolAcrescimos;

        return $this;
    }

    /**
     * @return float
     */
    public function getBolValorPago()
    {
        return $this->bolValorPago;
    }

    /**
     * @param float $bolValorPago
     * @return Boleto
     */
    public function setBolValorPago($bolValorPago)
    {
        $this->bolValorPago = $bolValorPago;

        return $this;
    }

    /**
     * @return BoletoConfConta
     */
    public function getConfcont()
    {
        return $this->confcont;
    }

    /**
     * @param BoletoConfConta $confcont
     * @return Boleto
     */
    public function setConfcont($confcont)
    {
        $this->confcont = $confcont;

        return $this;
    }

    public function toArray()
    {
        return array(
            'bolId'            => $this->getBolId(),
            'bolDemonstrativo' => $this->getBolDemostrativo(),
            'baixa'            => $this->getBaixa()
        );
    }

    /**
     * @return BoletoBaixa
     */
    public function getBaixa()
    {
        return $this->baixa;
    }

    /**
     * @param BoletoBaixa $baixa
     * @return Boleto
     */
    public function setBaixa($baixa)
    {
        $this->baixa = $baixa;

        return $this;
    }

    /**
     * @return integer
     */
    public function getBolCarteira()
    {
        return $this->bolCarteira;
    }

    /**
     * @param integer $bol_carteira
     * @return Boleto
     */
    public function setBolCarteira($bol_carteira)
    {
        $this->bolCarteira = $bol_carteira;

        return $this;
    }

}