<?php


namespace Boleto\Form;

use Zend\Form\Form,
    Zend\Form\Element;

class BoletoConfConta extends Form{
    public function __construct($name = null, $options = null)
    {
        parent::__construct($name);

        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', '');
        $this->setAttribute('role', 'form');
        $this->setAttribute('name', 'formBoleto');
        $this->setAttribute('enctype', 'multipart/form-data');

        $confcontId = new Element\Hidden('confcontId');
        $this->add($confcontId);

        //tratar relacionamento ao final
        $pes = new Element\Hidden('pes');
        $pes->setAttributes([
            'id'       => 'pesId',
        ]);
        $this->add($pes);

        $pesCnpjCpf = new Element\Text('pesCnpjCpf');
        $pesCnpjCpf->setAttributes([
            'col'      => '2',
            'wrap'     => false,
            'label'    => 'Cnpj/Cpf Responsavel: ',
            'data-validations' => 'Required',
            'id'       =>   'cnpjCpf',
            'placeholder'   => 'Cnpj/Cpf Responsavel',
            'data-buscacnpjcpf-url' => "/pessoa/pessoa-juridica/autocomplete-json",
        ]);
        $this->add($pesCnpjCpf);

        $pesNome = new Element\Text('pesNome');
        $pesNome->setAttributes([
            'col'      => '3',
            'wrap'     => true,
            'label'    => 'Nome Responsavel: ',
            'placeholder'   => 'Nome Responsavel',
            'data-validations' => 'Required',
            'readonly'   => 'readonly',
            'id'       => 'pessoaNome'
        ]);
        $this->add($pesNome);

        $banc = new Element\Select('banc');
        $banc->setAttributes([
            'col'      => '4',
            'wrap'     => false,
            'label'    => 'Selecione o Banco:',
            'data-validations' => 'Required',
        ]);
        $this->add($banc);

        $confcontCarteira = new Element\Text('confcontCarteira');
        $confcontCarteira->setAttributes([
            'col'      => '1',
            'wrap'     => true,
            'label'    => 'N° da Carteira: ',
            'placeholder'   => '',
            'data-validations' => 'Required',
        ]);
        $this->add($confcontCarteira);


        $confcontAgencia = new Element\Text('confcontAgencia');
        $confcontAgencia->setAttributes([
            'col'      => '1',
            'wrap'     => false,
            'label'    => 'N° da agencia: ',
            'placeholder'   => '',
            'data-validations' => 'Required,Integer',
        ]);
        $this->add($confcontAgencia);

        $confcontAgenciaDigito = new Element\Text('confcontAgenciaDigito');
        $confcontAgenciaDigito->setAttributes([
            'col'      => '1',
            'wrap'     => false,
            'label'    => 'Digito da agencia: ',
            'placeholder'   => '',
            'data-validations' => 'Required,Integer',
        ]);
        $this->add($confcontAgenciaDigito);

        $confcontConta = new Element\Text('confcontConta');
        $confcontConta->setAttributes([
            'col'      => '1',
            'wrap'     => false,
            'label'    => 'Numero da conta: ',
            'placeholder'   => '',
            'data-validations' => 'Required,Integer',
        ]);
        $this->add($confcontConta);

        $confcontContaDigito = new Element\Text('confcontContaDigito');
        $confcontContaDigito->setAttributes([
            'col'      => '1',
            'wrap'     => true,
            'label'    => 'Digito da conta: ',
            'placeholder'   => '',
            'data-validations' => 'Required,Integer',
        ]);
        $this->add($confcontContaDigito);

        $file = new Element\File("file");
        $file->setAttributes([
            'col'      => '4',
            'label'    => 'Logo do cedente: ',
            'placeholder'   => 'Logo para ser impressa no boleto'
        ]);
        $this->add($file);


        $arq = new Element\Hidden("arq");
        $arq->setAttributes([
        ]);
        $this->add($arq);

        $salvar = new Element\Submit('salvar');
        $salvar->setAttributes([
            'value'   => 'Salvar',
            'class'   => 'btn btn-primary pull-left',
        ]);
        $this->add($salvar);
    }

}