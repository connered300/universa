<?php


namespace Boleto\Form;


use Zend\Form\Form;
use Zend\Form\Element;

class FinanceiroDesconto extends Form
{
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name);

        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', '');
        $this->setAttribute('role', 'form');
        $this->setAttribute('name', 'financeiroDescontoForm');


        $descontoId = new Element\Hidden('descontoId');
        $this->add($descontoId);

        $usuario = new Element\Hidden('usuario');
        $this->add($usuario);

        $alunoper = new Element\Text('alunoper');
        $alunoper->setAttributes([
            'col'      => '1',
            'wrap'     => false,
            'label'    => 'Matricula: ',
            'required' => 'Required',
            'id'       => 'matricula',
            'readonly' => 'readonly'
        ]);
        $this->add($alunoper);

        $alunoper = new Element\Text('nome');
        $alunoper->setAttributes([
            'col'      => '2',
            'wrap'     => false,
            'label'    => 'Aluno: ',
            'id'       => 'nome',
            'readonly' => 'readonly'
        ]);
        $this->add($alunoper);


        $desctipo = new Element\Select('desctipo');
        $desctipo->setAttributes([
            'col'      => '2',
            'wrap'     => true,
            'label'    => 'Tipo',
            'id'        => 'desctipo',
            'onchange'   => 'buscaDadosDesconto(this)'
        ]);
        $this->add($desctipo);

        $desctipoValormax = new Element\Text('desctipoValormax');
        $desctipoValormax->setAttributes([
            'col'      => '1',
            'wrap'     => true,
            'label'    => 'Max. de desc. R$: ',
            'id'       => 'desctipoValormax',
            'readonly' => 'readonly'
        ]);
        $this->add($desctipoValormax);

        $desctipoPercmax = new Element\Text('desctipoPercmax');
        $desctipoPercmax->setAttributes([
            'col'      => '1',
            'wrap'     => false,
            'label'    => 'Max. de desc. %: ',
            'id'       => 'desctipoPercmax',
            'readonly' => 'readonly'
        ]);
        $this->add($desctipoPercmax);

        $descontoValor = new Element\Text('descontoValor');
        $descontoValor->setAttributes([
            'col'      => '1',
            'wrap'     => false,
            'label'    => 'Desc. concedido: R$',
            'id'       => 'descontoValor',
        ]);
        $this->add($descontoValor);

        $descontoPercentual = new Element\Text('descontoPercentual');
        $descontoPercentual->setAttributes([
            'col'      => '1',
            'wrap'     => false,
            'label'    => 'Desc. concedido: %',
            'id'       => 'descontoPercentual',
        ]);
        $this->add($descontoPercentual);

        $valorMensalidade = new Element\Text('valorMensalidade');
        $valorMensalidade->setAttributes([
            'col'      => '1',
            'wrap'     => false,
            'label'    => 'Val. Mensalide',
            'id'       => 'valorMensalidade',
            'readonly' => 'readonly',
            'data-prefix' => 'R$',
            'data-thousands' => '.',
            'data-decimal' => ','
        ]);
        $this->add($valorMensalidade);

        $descontoDiaLimite = new Element\Text('descontoDiaLimite');
        $descontoDiaLimite->setAttributes([
            'col'      => '1',
            'wrap'     => false,
            'label'    => 'Dia Vencimento',
            'id'       => 'descontoDiaLimite',
        ]);
        $this->add($descontoDiaLimite);

        $desctipoModalidade = new Element\Hidden('desctipoModalidade');
        $desctipoModalidade->setAttributes([
            'id'        => 'desctipoModalidade',
        ]);
        $this->add($desctipoModalidade);

        $desctipoLimitaVencimento = new Element\Hidden('desctipoLimitaVencimento');
        $desctipoLimitaVencimento->setAttributes([
            'id'        => 'desctipoLimitaVencimento',
        ]);
        $this->add($desctipoLimitaVencimento);


        $salvar = new Element\Submit('voltar');
        $salvar->setAttributes([
            'value'   => 'Voltar',
            'formaction' => '/boleto-bancario/financeiro-desconto',
            'class'   => 'btn btn-primary pull-left',
        ]);
        $this->add($salvar);

        $salvar = new Element\Submit('salvar');
        $salvar->setAttributes([
            'value'   => 'Salvar',
            'class'   => 'btn btn-primary pull-left',
            'onclick'   => ' return validaFormFinanceiroDesconto()'
        ]);
        $this->add($salvar);
    }
}