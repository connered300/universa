<?php



namespace Boleto\Form;


use Zend\Form\Form;
use Zend\Form\Element;

class FinanceiroRelatorios extends Form{
    public function __construct()
    {
        parent::__construct();
        $this->setAttribute("data-validate", "yes");

        $this->setAttribute("class", "smart-form");
        $this->setAttribute("data-validate", "yes");
        $this->setAttribute("action","");
        $this->setAttribute("target","_blank");

        $inicio = new Element\Text('dataInicio');
        $inicio->setAttributes([
            'col' => '4',
            'label' => 'Data de Início:',
            'icon'=>'icon-calendar',
            'placeholder'=>'00/00/0000',
            'data-masked' => 'Date',
            'data-validations' => 'Data,Required'
        ]);
        $this->add($inicio);

        $inicio = new Element\Text('dataFim');
        $inicio->setAttributes([
            'col' => '4',
            'label' => 'Data de Termino:',
            'icon'=>'icon-calendar',
            'placeholder'=>'00/00/0000',
            'data-masked' => 'Date',
            'wrap'     => true,
            'data-validations' => 'Data,Required'
        ]);
        $this->add($inicio);

        $tipoRelatorio = new Element\Radio('tipoRelatorio');
        $tipoRelatorio->setAttributes([
            'col'              => '4',
            'wrap'             => true,
            'data-validations' => 'Required',
            'label'            => 'Modelo de Relatório'
        ]);
        $tipoRelatorio->setValueOptions([
            'analitico' => 'Analítico',
            'sintetico' => 'Sintético'
        ]);
        $tipoRelatorio->setValue('sintetico');
        $this->add($tipoRelatorio);


    }
}