<?php


namespace Boleto\Form;

use Zend\Form\Form,
    Zend\Form\Element;

class FinanceiroCheque extends Form{
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name);

        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', '');
        $this->setAttribute('role', 'form');
        $this->setAttribute('name', 'financeiroChequeForm');


        $chequeId = new Element\Hidden('chequeId');
        $chequeId->setAttribute('id', 'chequeId');
        $this->add($chequeId);

        $titulo = new Element\Hidden('titulo');
        $this->add($titulo);

        $chequeEmitente = new Element\Text('chequeEmitente');
        $chequeEmitente->setAttributes([
            'col'      => '2',
            'wrap'     => false,
            'label'    => 'Nome Emitente: ',
            'id'       => 'chequeEmitente',
        ]);
        $this->add($chequeEmitente);

        $chequeNum = new Element\Text('chequeNum');
        $chequeNum->setAttributes([
            'col'      => '2',
            'wrap'     => false,
            'label'    => 'n° cheque: ',
            'id'       => 'chequeNum',
        ]);
        $this->add($chequeNum);

        $chequeBanco = new Element\Text('chequeBanco');
        $chequeBanco->setAttributes([
            'col'      => '2',
            'wrap'     => false,
            'label'    => 'Banco',
            'id'       => 'chequeBanco',
        ]);
        $this->add($chequeBanco);

        $chequeAgencia = new Element\Text('chequeAgencia');
        $chequeAgencia->setAttributes([
            'col'      => '2',
            'wrap'     => false,
            'label'    => 'Agencia',
            'id'       => 'chequeAgencia',
        ]);
        $this->add($chequeAgencia);

        $chequePraca = new Element\Text('chequePraca');
        $chequePraca->setAttributes([
            'col'      => '2',
            'wrap'     => false,
            'label'    => 'Praça',
            'id'       => 'chequePraca',
        ]);
        $this->add($chequePraca);

        $chequeEmissao = new Element\Text('chequeEmissao');
        $chequeEmissao->setAttributes([
            'col'      => '2',
            'wrap'     => false,
            'label'    => 'Data emissão',
            'id'       => 'chequeEmissao',
            'data-masked' =>  'Date',
            'class'     => 'dataAtual'
        ]);
        $this->add($chequeEmissao);

        $chequeVencimento = new Element\Text('chequeVencimento');
        $chequeVencimento->setAttributes([
            'col'      => '2',
            'wrap'     => false,
            'label'    => 'Data vencimento',
            'id'       => 'chequeVencimento',
            'data-masked' =>  'Date',
        ]);
        $this->add($chequeVencimento);

        $chequeCompensado = new Element\Text('chequeCompensado');
        $chequeCompensado->setAttributes([
            'col'      => '2',
            'wrap'     => false,
            'label'    => 'Data Compensamento',
            'id'       => 'chequeCompensado',
            'data-masked' =>  'Date',
        ]);
        $this->add($chequeCompensado);

        $chequeValor = new Element\Text('chequeValor');
        $chequeValor->setAttributes([
            'col'      => '2',
            'wrap'     => false,
            'label'    => 'Valor',
            'id'       => 'chequeValor',
        ]);
        $this->add($chequeValor);

        $salvar = new Element\Submit('Voltar');
        $salvar->setAttributes([
            'value'   => 'Voltar',
            'formaction' => '/boleto-bancario/financeiro-cheque',
            'class'   => 'btn btn-primary pull-left',
        ]);
        $this->add($salvar);

        $salvar = new Element\Submit('salvar');
        $salvar->setAttributes([
            'value'   => 'Salvar',
            'class'   => 'btn btn-primary pull-left',
        ]);
        $this->add($salvar);
    }

}