<?php


namespace Boleto\Form;

use Zend\Form\Form,
    Zend\Form\Element;

class Boleto extends Form{
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name);

        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', '');
        $this->setAttribute('role', 'form');

        $bolId = new Element\Hidden('bolId');
        $this->add($bolId);

        $confcont = new Element\Hidden('confcont');
        $this->add($confcont);

        $bolDataVencimento = new Element\Text('bolDataVencimento');
        $bolDataVencimento->setAttributes([
            'col'      => '1',
            'wrap'     => false,
            'label'    => 'Data de Vencimento: ',
            'required' => 'Required',
            'data-masked'=>  'Date',
        ]);
        $this->add($bolDataVencimento);

        $bolNossoNumero = new Element\Text('bolNossoNumero');
        $bolNossoNumero->setAttributes([
            'col'      => '2',
            'wrap'     => false,
            'label'    => 'Nosso Numero:',
            'required' => 'Required',
        ]);
        $this->add($bolNossoNumero);


        $bolNumeroDocumento = new Element\Text('bolNumeroDocumento');
        $bolNumeroDocumento->setAttributes([
            'col'      => '2',
            'wrap'     => false,
            'label'    => 'Numero do Documento: ',
            'required' => 'Required',
        ]);
        $this->add($bolNumeroDocumento);


        $bolValor = new Element\Text('bolValor');
        $bolValor->setAttributes([
            'col'      => '1',
            'wrap'     => false,
            'label'    => 'Valor: ',
            'required' => 'Required',
        ]);
        $this->add($bolValor);


        $pes = new Element\Hidden('titulo');
        $pes->setAttributes([
            'id'       => 'titulo',
        ]);
        $this->add($pes);

        $cnpjCpf = new Element\Text('cnpjCpf');
        $cnpjCpf->setAttributes([
            'col'      => '2',
            'wrap'     => false,
            'label'    => 'CNPJ/CPF Sacado: ',
            'required' => 'Required',
            'id'       => 'cnpjCpf',
        ]);
        $this->add($cnpjCpf);

        $pessoaNome = new Element\Text('pessoaNome');
        $pessoaNome->setAttributes([
            'col'      => '2',
            'wrap'     => false,
            'label'    => 'Nome: ',
            'required' => 'Required',
            'id'       => 'pessoaNome',
        ]);
        $this->add($pessoaNome);

        $bolDemostrativo = new Element\Textarea('bolDemostrativo');
        $bolDemostrativo->setAttributes([
            'col'      => '8',
            'wrap'     => true,
            'label'    => 'Demostrativo: ',
            'required' => 'Required',
        ]);
        $this->add($bolDemostrativo);


        $salvar = new Element\Submit('salvar');
        $salvar->setAttributes([
            'value'   => 'Salvar',
            'class'   => 'btn btn-primary pull-left',
        ]);
        $this->add($salvar);
    }

}