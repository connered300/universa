<?php


namespace Boleto\Form;

use Zend\Form\Form,
    Zend\Form\Element;

class FinanceiroTitulo extends Form{
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name);

        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', '');
        $this->setAttribute('role', 'form');
        $this->setAttribute('name', 'financeiroTituloForm');



        $tituloId = new Element\Hidden('tituloId');
        $tituloId->setAttributes([
            'id'    => 'tituloId'
        ]);
        $this->add($tituloId);

        $tituloNovo = new Element\Hidden('tituloNovo');

        $this->add($tituloNovo);


        $matricula = new Element\Text('matricula');
        $matricula->setAttributes([
            'label'    => 'N° matricula',
            'col'      => '1',
            'wrap'     => false,
            'id'       => 'matricula',
        ]);
        $this->add($matricula);

        $parcela = new Element\Text('parcela');
        $parcela->setAttributes([
            'label'    => 'N° parcela',
            'col'      => '1',
            'wrap'     => true,
            'readonly' => 'readonly'
        ]);
        $this->add($parcela);

        $pes = new Element\Hidden('pes');
        $pes->setAttribute('id', 'pesId');
        $this->add($pes);

        $usuarioAutor = new Element\Hidden('usuarioAutor');
        $this->add($usuarioAutor);

        $usuarioBaixa = new Element\Hidden('usuarioBaixa');
        $this->add($usuarioBaixa);

        $tituloEstado = new Element\Hidden('tituloEstado');
        $this->add($tituloEstado);

        $cursocampus = new Element\Hidden('cursocampus');
        $cursocampus->setAttribute('id', 'cursocampus');
        $this->add($cursocampus);

        $per = new Element\Hidden('per');
        $per->setAttribute('id', 'per');
        $this->add($per);

        $pessoaCnpj = new Element\Text('cnpjCpf');
        $pessoaCnpj->setAttributes([
            'col'      => '2',
            'wrap'     => false,
            'label'    => 'CNPJ/CPF Sacado: ',
            'required' => 'Required',
            'id'       => 'cnpjCpf',
            'readonly' => 'readonly'
        ]);
        $this->add($pessoaCnpj);

        $pessoaNome = new Element\Text('pessoaNome');
        $pessoaNome->setAttributes([
            'col'      => '2',
            'wrap'     => true,
            'label'    => 'Nome: ',
            'required' => 'Required',
            'id'       => 'pessoaNome',
        ]);
        $this->add($pessoaNome);

        $tipotitulo = new Element\Select('tipotitulo');
        $tipotitulo->setAttributes([
            'col'      => '2',
            'wrap'     => false,
            'label'    => 'Tipo',
            'disabled' => 'disabled',
        ]);
        $this->add($tipotitulo);

        $tituloTipoPagamento = new Element\Select('tituloTipoPagamento');
        $tituloTipoPagamento->setAttributes([
            'col'      => '2',
            'wrap'     => true,
            'label'    => 'Tipo Pagamento',
            'id'       => 'tituloTipoPagamento'
        ]);
        $tituloTipoPagamento->setValueOptions(array(
            'Dinheiro'  => 'Dinheiro',
            'Boleto'    => 'Boleto',
            'Isento'    => 'Isento',
            'Cheque'    => 'Cheque'
        ));
        $this->add($tituloTipoPagamento);

        $confcont = new Element\Select('confcont');
        $confcont->setAttributes([
            'required' => true,
            'col' => '1',
            'wrap' => true,
            'label' => 'Configuração Bancaria',
            'id'    => 'confcont'
        ]);
        $this->add($confcont);

        $tituloDataVencimento = new Element\Text('tituloDataVencimento');
        $tituloDataVencimento->setAttributes([
            'col'      => '2',
            'wrap'     => false,
            'label'    => 'Data de Vencimento',
            'data-masked' =>  'Date',
            'class'     => 'dataAtual'
        ]);
        $this->add($tituloDataVencimento);

        $tituloDataProcessamento = new Element\Text('tituloDataProcessamento');
        $tituloDataProcessamento->setAttributes([
            'col'      => '2',
            'wrap'     => false,
        ]);
        $this->add($tituloDataProcessamento);

        $tituloValor = new Element\Text('tituloValor');
        $tituloValor->setAttributes([
            'col'      => '2',
            'wrap'     => true,
            'label'    => 'Valor',
            'data-prefix' => "R$ ",
            'data-thousands'   => ".",
            'data-decimal'  => ",",
            'readonly' => 'readonly'
        ]);
        $this->add($tituloValor);

        $valorFormaPagamento = new Element\Text('valorFormaPagamento');
        $valorFormaPagamento->setAttributes([
            'col'               => '2',
            'label'             => 'Valor',
            'data-prefix'       => "R$ ",
            'data-thousands'    => ".",
            'data-decimal'      => ",",
            'id'                => 'valorFormaPagamento'
        ]);
        $this->add($valorFormaPagamento);

        $tituloObservacoes = new Element\Textarea('tituloObservacoes');
        $tituloObservacoes->setAttributes([
            'wrap'     => true,
            'col'      => 7,
            'rows'     => 5,
            'label'    => 'Observações:',
            'onfocus'   => "verificaFormaPagamento()"
        ]);
        $this->add($tituloObservacoes);

        $tituloDataPagamento = new Element\Text('tituloDataPagamento');
        $tituloDataPagamento->setAttributes([
            'col'      => '2',
            'wrap'     => false,
            'label'    => 'Data de Pagamento',
            'data-masked' =>  'Date',
        ]);
        $this->add($tituloDataPagamento);

        $tituloMulta = new Element\Text('tituloMulta');
        $tituloMulta->setAttributes([
            'col'      => '1',
            'wrap'     => false,
            'label'    => 'Multa',
            'data-masked'=>  'Percent',
            'disabled' => 'disabled',
        ]);
        $this->add($tituloMulta);

        $tituloJuros = new Element\Text('tituloJuros');
        $tituloJuros->setAttributes([
            'col'      => '1',
            'wrap'     => false,
            'label'    => 'Juros',
            'data-masked'=>  'Percent',
            'disabled' => 'disabled'
        ]);
        $this->add($tituloJuros);

        $tituloDescontoManual = new Element\Text('tituloDescontoManual');
        $tituloDescontoManual->setAttributes([
            'col'      => '1',
            'wrap'     => false,
            'data-prefix' => "R$ ",
            'data-thousands'   => ".",
            'data-decimal'  => ",",
            'label'     => 'Desconto Manual:',
            'id'        => 'tituloDescontoManual',
            'onchange'  => 'recalculaDescontos(this)'
        ]);
        $this->add($tituloDescontoManual);

//        $tituloEstado = new Element\Text('tituloEstado');
//        $tituloEstado->setAttributes([
//            'col'      => '2',
//            'wrap'     => false,
//            'label'    => 'Estado',
//        ]);
//        $this->add($tituloEstado);

        /*
         * Não utilizar este botao em telas que necessitem de navegação pelo enter
         */
        $voltar = new Element\Submit('Voltar');
        $voltar->setAttributes([
            'value'   => 'Voltar',
            'formaction' => '/boleto-bancario/financeiro-titulo',
            'class'   => 'btn btn-primary pull-left',
        ]);
        $this->add($voltar);

        $salvar = new Element\Submit('salvar');
        $salvar->setAttributes([
            'value'   => 'Salvar',
            'class'   => 'btn btn-primary pull-left',
            'onclick'   => 'return baixaTitulo()'
        ]);
        $this->add($salvar);
    }

}