<?php


namespace Boleto\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class Relatorio extends Form{
    public function __construct($usuarios = null, $servicos = null)
    {
        parent::__construct('relatorio');
        $this->setAttribute("data-validate", "yes");

        $this->setAttribute("class", "smart-form");
        $this->setAttribute("data-validate", "yes");
        $this->setAttribute("action","");
        $this->setAttribute("target","_blank");

        $inicio = new Element\Text('dataInicio');
        $inicio->setAttributes([
            'col' => '4',
            'label' => 'Data de Início:',
            'icon'=>'icon-calendar',
            'placeholder'=>'00/00/0000',
            'data-masked' => 'Date',
            'value' => date('d/m/Y'),
            'data-validations' => 'Data,Required'
        ]);
        $this->add($inicio);

        $inicio = new Element\Text('dataFim');
        $inicio->setAttributes([
            'col' => '4',
            'label' => 'Data de Termino:',
            'icon'=>'icon-calendar',
            'placeholder'=>'00/00/0000',
            'data-masked' => 'Date',
            'wrap'     => true,
            'value' => date('d/m/Y'),
            'data-validations' => 'Data,Required'
        ]);
        $this->add($inicio);

        $tipoRelatorio = new Element\Radio('tipoRelatorio');
        $tipoRelatorio->setAttributes([
            'col'              => '4',
            'wrap'             => true,
            'data-validations' => 'Required',
            'label'            => 'Modelo de Relatório'
        ]);
        $tipoRelatorio->setValueOptions([
            'analitico' => 'Analítico',
            'sintetico' => 'Sintético'
        ]);
        $tipoRelatorio->setValue('sintetico');
        $this->add($tipoRelatorio);

        $tipo = new Element\Select('tipoUsuario');
        $tipo->setAttributes([
            'col'              => '4',
            'data-validations' => 'Required',
            'label'            => 'Tipo de Usuário'
        ]);
        if($usuarios){
            $tipo->setValueOptions($usuarios);
        }
        $this->add($tipo);

        $servico = new Element\Select('servico');
        $servico->setAttributes([
            'col'              => '4',
            'data-validations' => 'Required',
            'label'            => 'Serviço',
            'wrap'             => true,
        ]);
        if($servicos){
            $servico->setValueOptions($servicos);
        }
        $this->add($servico);

//        $tipoPagamento = new Element\MultiCheckBox('tipoPagamento');
//        $tipoPagamento->setAttributes([
//            'col'     => '3',
//            'label'   => 'Tipos de pagamento:',
//            'data-validations' => 'Required',
//            'wrap'    => true
//        ]);
//        $tipoPagamento->setValueOptions([
//            'Dinheiro' => 'Dinheiro',
//            'Boleto'   => 'Boleto',
//            'Cheque'   => 'Cheque',
//        ]);
//        $this->add($tipoPagamento);

        $cadastrar = new Element\Submit('gerar');
        $cadastrar->setAttributes([
            'class' => 'btn btn-primary',
            'value' => 'Gerar Relatorio',
        ]);
        $this->add($cadastrar);
    }
}