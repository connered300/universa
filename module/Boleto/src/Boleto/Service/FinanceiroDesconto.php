<?php


namespace Boleto\Service;

use VersaSpine\Service\AbstractService;

class FinanceiroDesconto extends AbstractService
{

    protected $repositoryTituloMensalidade;

    protected $repositoryTitulo;

    protected $repositoryMensalidadeDesconto;

    public function __construct(\Doctrine\ORM\EntityManager $em)
    {

        parent::__construct($em, 'Boleto\Entity\FinanceiroDesconto');

        $this->repositoryMensalidadeDesconto = $this->getRepository('Boleto\Entity\FinanceiroMensalidadeDesconto');
        $this->repositoryTitulo              = $this->getRepository('Boleto\Entity\FinanceiroTitulo');
        $this->repositoryTituloMensalidade   = $this->getRepository('Boleto\Entity\FinanceiroTituloMensalidade');
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {
        // TODO: Implement valida() method.
    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {
        // TODO: Implement pesquisaForJson() method.
    }

    function adicionar(array $dados)
    {
        $dados['parcelas']             = $dados['descontoMensalidades'];
        $dados['descontoMensalidades'] = implode(',', $dados['descontoMensalidades']);

        if (empty($dados['descontoValor'])) {
            $dados['descontoValor'] = $dados['descontoPercentual'] * ($dados['valorMensalidade'] / 100);
        } else {
            $dados['descontoPercentual'] = null;
        }
        if (empty($dados['descontoDiaLimite'])) {
            $dados['descontoDiaLimite'] = null;
        }

        $this->begin();
        $desconto = parent::adicionar($dados);
        // adiciona disconto apenas para titulos de mensalidade
        $this->adicionaParcelas($dados, $desconto, \Boleto\Service\FinanceiroTituloTipo::MENSALIDADE);
        $this->commit();

        return $desconto;
    }

    public function edita(array $dados)
    {
        $dados['parcelas'] = $dados['descontoMensalidades'];

        $dados['descontoMensalidades'] = implode(',', $dados['descontoMensalidades']);
        $dados['parcelasCadastradas']  = explode(",", $dados['parcelasCadastradas']);
        $arrayDif                      = array_diff($dados['parcelasCadastradas'], $dados['parcelas']);
        $entity                        = $this->getReference($dados['descontoId']);
        if (!empty($arrayDif)) {
            $retCancelados = $this->cancelar($dados['descontoId'], $arrayDif);
        }
        if (!empty($dados['descontoPercentual'])) {
            $dados['descontoValor'] = $dados['descontoPercentual'] * ($dados['valorMensalidade'] / 100);
        }
        if (empty($dados['descontoDiaLimite'])) {
            $dados['descontoDiaLimite'] = null;
        }
        $arrayDif          = array_diff($dados['parcelas'], $dados['parcelasCadastradas']);
        $parcelas          = $dados['parcelas'];
        $dados['parcelas'] = $arrayDif;
        $this->begin();
        $desconto = parent::edita($dados);
        $this->adicionaParcelas($dados, $desconto);
        $dados['parcelas'] = $parcelas;
        $this->alteraValorParcelas($dados, $entity->getDescontoValor());
        $this->commit();

        return $desconto;
    }

    function alteraValorParcelas(array $dados, $valorAnterior)
    {
        $entity = $this->getReference($dados['descontoId']);
        $this->begin();
        foreach ($dados['parcelas'] as $parcela) {
            $retTituloMensalidade = $this->repositoryTituloMensalidade->findBy(
                [
                    'alunoper'           => $entity->getAlunoper()->getAlunoperId(),
                    'mensalidadeParcela' => $parcela,

                ],
                [
                    'financeiroTituloMensalidadeId' => 'DESC'

                ]
            )[0];

            $qb = $this->getEm()->createQueryBuilder();
            $q  = $qb->select(array('u'))
                ->from('Boleto\Entity\FinanceiroTitulo', 'u')
                ->where(
                    $qb->expr()->andX(
                        $qb->expr()->eq('u.tituloId', '?1'),
                        $qb->expr()->isNull('u.tituloDataPagamento')
                    )
                )
                ->setParameter(1, $retTituloMensalidade->getTitulo()->getTituloId())
                ->getQuery();

            $titulo = $q->execute()[0];

            if ($titulo) {

                //                $valorDescontoTitulo = $titulo->getTituloDesconto() == "" ? 0 : $titulo->getTituloDesconto();
                //                $diferencaDesconto   = $valorDescontoTitulo - $valorAnterior;
                //                if ($diferencaDesconto <= 0) {
                //                    $diferencaDesconto = $valorAnterior;
                //                }
                if ($titulo->getTituloValor() === $dados['descontoValor']) {
                    $titulo->setTituloEstado('Pago');
                } else {
                    $titulo->setTituloEstado('Aberto');
                }

                $titulo->setTituloDesconto(($dados['descontoValor']));
                $retTitulo = (new \Boleto\Service\FinanceiroTitulo($this->getEm()))->edita($titulo->toArray());
            }
        }
        $this->commit();

        return $dados;
    }

    function adicionaParcelas(array $dados, $entity, $tipoTitulo = null)
    {
        $this->begin();

        $querySql = <<<SQL
                SELECT tm 
                FROM \Boleto\Entity\FinanceiroTituloMensalidade tm 
                JOIN tm.titulo t
                WHERE 
                    tm.alunoper = :alunoper AND
                    tm.mensalidadeParcela = :parcela
SQL;
        $params   = [
            'alunoper' => $entity->getAlunoper()->getAlunoperId()
        ];

        if ($tipoTitulo) {
            $querySql .= " AND t.tipotitulo = :tipoTitulo";
            $params['tipoTitulo'] = $tipoTitulo;
        }

        $querySql .= " ORDER BY tm.financeiroTituloMensalidadeId DESC";
        $query = $this->getEm()->createQuery($querySql);

        foreach ($dados['parcelas'] as $parcela) {
            $params['parcela'] = $parcela;
            $query->setParameters($params);

            // pega o primeiro resultado da pesquisa
            $retTituloMensalidade = current($query->getResult());

            $retMensalidadeDesconto = $this->repositoryMensalidadeDesconto->findOneBy(
                [
                    'financeiroTituloMensalidade' => $retTituloMensalidade->getFinanceiroTituloMensalidadeId(),
                    'desconto'                    => $entity->getDescontoId()
                ]
            );
            if ($retMensalidadeDesconto) {
                $retMensalidadeDesconto->setDescmensalidadeStatus('Ativo');
                $retMensalidadeDesconto = (new \Boleto\Service\FinanceiroMensalidadeDesconto($this->getEm()))->edita(
                    $retMensalidadeDesconto->toArray()
                );
            } else {
                $mensalidadeDesconto                                = "";
                $mensalidadeDesconto['descmensalidadeStatus']       = 'Ativo';
                $mensalidadeDesconto['financeiroTituloMensalidade'] = $retTituloMensalidade->getFinanceiroTituloMensalidadeId(
                );
                $mensalidadeDesconto['desconto']                    = $entity->getDescontoId();
                $retMensalidadeDesconto                             = (
                new \Boleto\Service\FinanceiroMensalidadeDesconto(
                    $this->getEm()
                )
                )->adicionar($mensalidadeDesconto);
            }

            $titulo = $this->repositoryTitulo->findOneBy(
                [
                    'tituloId' => $retTituloMensalidade->getTitulo()->getTituloId(),
                ]
            );

            $totalDesconto = floatval($titulo->getTituloDesconto() + $dados['descontoValor']);

            $titulo->setTituloDesconto($totalDesconto);
            if ($titulo->getTituloDesconto() >= $titulo->getTituloValor()) {
                $titulo->setTituloEstado("Pago");
            }
            $tituloArray = $titulo->toArray();
            $retTitulo   = (new \Boleto\Service\FinanceiroTitulo($this->getEm()))->edita($tituloArray);
        }
        $this->commit();
    }

    public function cancelar($descontoId, array $parcelas = ['1', '2', '3', '4', '5', '6'])
    {
        $em = $this->getEm();

        $serviceFinanceiroTitulo              = new \Boleto\Service\FinanceiroTitulo($em);
        $serviceFinanceiroMensalidadeDesconto = new \Boleto\Service\FinanceiroMensalidadeDesconto($em);

        $entity = $this->getReference($descontoId);

        $this->begin();

        foreach ($parcelas as $parcela) {
            $retTituloMensalidade = $this->repositoryTituloMensalidade->findBy(
                [
                    'alunoper'           => $entity->getAlunoper()->getAlunoperId(),
                    'mensalidadeParcela' => $parcela,

                ],
                [
                    'financeiroTituloMensalidadeId' => 'DESC'
                ]
            )[0];

            $retMensalidadeDescontos = $this->repositoryMensalidadeDesconto->findBy(
                [
                    'financeiroTituloMensalidade' => $retTituloMensalidade->getFinanceiroTituloMensalidadeId(),
                    'desconto'                    => $entity->getDescontoId(),
                    'descmensalidadeStatus'       => 'Ativo'
                ]
            );

            if ($retMensalidadeDescontos) {
                foreach ($retMensalidadeDescontos as $retMensalidadeDesconto) {
                    $titulo = $retTituloMensalidade->getTitulo();

                    if (!$titulo->getTituloDataPagamento()) {
                        $valorDescontoTitulo = (float)$titulo->getTituloDesconto();
                        $titulo->setTituloDesconto(($valorDescontoTitulo - $entity->getDescontoValor()));

                        if ($titulo->getTituloDesconto() < $titulo->getTituloValor()) {
                            $titulo->setTituloEstado("Aberto");

                            $retMensalidadeDesconto->setDescmensalidadeStatus('Inativo');
                            $serviceFinanceiroMensalidadeDesconto->edita($retMensalidadeDesconto->toArray());
                        }

                        $retTitulo = (new \Boleto\Service\FinanceiroTitulo($this->getEm()))->edita($titulo->toArray());
                    }
                }
            }
        }

        if (!array_diff($parcelas, ['1', '2', '3', '4', '5', '6'])) {
            $entity->setDescontoMensalidades(null);
            $this->getEm()->persist($entity);
            $this->getEm()->flush();
        }

        $this->commit();

        return $parcelas;
    }

    public function resultSearch($page = 0, $is_json = false, $post = null)
    {
        $dados = array();

        $serviceAcadperiodoLetivo = new \Matricula\Service\AcadperiodoLetivo($this->getEm());
        $periodo                  = $serviceAcadperiodoLetivo->buscaUltimoPeriodo();

        if (!$page) {
            if ($is_json) {
                $limit = " LIMIT 10 OFFSET 0 ";
            } else {
                $limit = "";
            }
        } else {
            $offset = (($page - 1) * 10);
            $limit  = " LIMIT 10 OFFSET $offset ";
        }
        if ($post['alunoper'] != "") {
            $WHERE = " AND acadgeral__aluno_curso.alunocurso_id = '{$post['alunoper']}' AND per_id = {$periodo->getPerId()} ";
        }

        $sql = "
            SELECT
                acadperiodo__aluno.alunocurso_id,
                financeiro__desconto.desconto_id,
                financeiro__desconto.desconto_status,
                desconto_valor,
                desconto_percentual,
                desconto_mensalidades,
                desconto_dia_limite,
                desctipo_descricao as desctipo_id,
                pessoa.pes_nome as nome,
                CONCAT(acadperiodo__letivo.per_ano,'/', acadperiodo__letivo.per_semestre) as semestre_nome
            FROM
                financeiro__desconto
            NATURAL JOIN
                financeiro__desconto_tipo
            NATURAL JOIN
                acadperiodo__aluno
            INNER JOIN
            acadgeral__situacao
              ON
             acadgeral__situacao.situacao_id = acadperiodo__aluno.matsituacao_id
            NATURAL JOIN
              acadgeral__aluno_curso
            NATURAL JOIN
              acadgeral__aluno
            NATURAL JOIN
              acadperiodo__turma
            NATURAL JOIN
              pessoa_fisica
            NATURAL JOIN
              pessoa
            NATURAL JOIN
              acadperiodo__letivo
            WHERE 1
            {$WHERE}
            AND financeiro__desconto.desconto_status = 'Ativo'
            GROUP BY
              financeiro__desconto.desconto_id
            LIMIT 300
        ";

        $sql .= " $limit ";
        $dados['dados'] = $this->executeQuery($sql);
        $dados['count'] = $dados['dados']->rowCount();
        if ($dados['count'] < 1 && $post['alunoper'] != "") {
            $dados['aluno'] = $this->executeQuery(
                "SELECT alunoper_id FROM acadperiodo__aluno WHERE alunocurso_id = '{$post['alunoper']}'"
            );
        }
        if ($is_json) {
            $dados['dados'] = $this->paginationFormatJson($dados['dados']);
        }

        return $dados;
    }

    public function buscaDescontosAluno($alunoperId)
    {
        if ($alunoperId) {
            $repositoryDesconto = $this->getEm()->getRepository('Boleto\Entity\FinanceiroDesconto');
            $descontos          = $repositoryDesconto->findBy(
                [
                    'alunoper'       => $alunoperId,
                    'descontoStatus' => 'Ativo',
                ]
            );

            if ($descontos) {
                foreach ($descontos as $desconto) {
                    $descontoArray                       = $desconto->toArray();
                    $descontoArray['descontoValor']      = \Boleto\Service\Moeda::decToMoeda(
                        $descontoArray['descontoValor']
                    );
                    $descontoArray['descontoPercentual'] = $descontoArray['descontoPercentual'];
                    $descontoArray['periodoLetivo']      = $desconto->getAlunoPer()->getTurma()->getPer()->getPerAno(
                        ) . '/' . $desconto->getAlunoPer()->getTurma()->getPer()->getPerSemestre();
                    $descontoArray['usuário']            = $desconto->getUsuario()->getPesFisica(
                    ) != null ? $desconto->getUsuario()->getPesFisica()->getPes()->getPesNome() : '-';
                    $array[]                             = $descontoArray;
                }

                return $array;
            }

            return null;
        }

        return null;
    }

    public function inativarDesconto($idDesconto)
    {
        $update = $this->getEm()->createQuery(
            "UPDATE \Boleto\Entity\FinanceiroDesconto fn SET fn.descontoStatus = 'Inativo' WHERE fn.descontoId = :id"
        );
        $update->setParameter('id', $idDesconto);
        $result = $update->getResult();

        return $result;
    }

    /**
     * metodo busca financiamento do tipo fies para o aluno no periodo atual e retorna o mesmo se houver
     * @param $alunoperId
     * @return null|object
     */
    public function buscasFiesAlunoPeriodo($alunoperId)
    {
        if ($alunoperId) {
            $fies = null;
            $tipo = $this->getRepository('Boleto\Entity\FinanceiroDescontoTipo')->findOneBy(
                ['desctipoDescricao' => 'FIES']
            );

            if ($tipo) {
                $fies = $this->getRepository()->findOneBy(
                    ['alunoper' => $alunoperId, 'desctipo' => $tipo->getDesctipoId(), 'descontoStatus' => 'Ativo']
                );
            }

            return $fies;
        }
    }

    public function cancelaTodosDescontosAluno($alunoperId)
    {
        if ($alunoperId) {
            $descontos = $this->getRepository()->findBy(['alunoper' => $alunoperId]);

            if ($descontos) {
                $this->begin();

                foreach ($descontos as $desconto) {
                    $desconto->setDescontoStatus('Inativo');
                    try {
                        $this->getEm()->persist($desconto);
                        $this->getEm()->flush();
                    } catch (\Exception $ex) {
                        throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
                    }
                }
                $this->commit();
            }
        }

        return null;
    }

    public function buscaAlunoUltmimoPeriodoLetivo($alunocursoId)
    {
        $alunoPerId = false;

        $serviceAcadperiodoLetivo = new \Matricula\Service\AcadperiodoLetivo($this->getEm());
        $periodo                  = $serviceAcadperiodoLetivo->buscaUltimoPeriodo();

        $sql = "
                    SELECT
                *
            FROM
                acadperiodo__aluno
                    NATURAL JOIN
                acadperiodo__turma
            WHERE
                alunocurso_id = $alunocursoId AND per_id = {$periodo->getPerId()}";

        $ArrayalunoPerId = $this->executeQuery($sql)->fetch();

        $alunoPerId = $ArrayalunoPerId['alunoper_id'];

        return $alunoPerId;
    }

}