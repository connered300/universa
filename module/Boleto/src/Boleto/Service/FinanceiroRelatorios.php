<?php


namespace Boleto\Service;

use VersaSpine\Service\AbstractService;

class FinanceiroRelatorios extends AbstractService
{
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Boleto\Entity\FinanceiroTitulo');
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {
    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {
    }

    public function buscaInadiplentes($per, $turno, $turma, $dataInicio, $dataFim)
    {
        if (!empty($per)) {
            $per = " AND Periodo = '{$per}' ";
        }
        if ($turma !== 'Todos') {
            $turma = " AND  Turma = '{$turma}' ";
        } else {
            $turma = "";
        }
        if ($turno !== 'Todos') {
            $turno = " AND  Turno = '{$turno}' ";
        } else {
            $turno = "";
        }

        $query = ("SELECT
                        Matricula as matricula,
                        UPPER(Nome) as nome,
                        turma,
                        pes_cpf as cpf,
                        pessoa.con_contato_telefone telefone,
                        pessoa.con_contato_celular celular,
                        pessoa.con_contato_email email,
                        end_logradouro,
                        end_numero,
                        end_complemento,
                        end_bairro,
                        end_cidade,
                        end_estado,
                        end_cep,
                        parcela as parcela,
                        valor as valor,
                        vencimento as vencimento,
                        Desconto as desconto,
                        DATEDIFF(DATE(NOW()), vencimento)-5 'dias',
                        round(valor*0.021) multa,
                        round(valor * ((DATEDIFF(DATE(NOW()), vencimento)* 0.0003))-5) juros,
                        ROUND(valor * (1+0.021 +  ((((DATEDIFF(DATE(NOW()), vencimento)-5)* 0.0003))))) - valor as 'acrescimo'
                    FROM
                        view__mensalidades
                            INNER JOIN
                        pessoa_fisica ON pessoa_fisica.pes_id = view__mensalidades.pessoa
                            INNER JOIN
                        pessoa  ON pessoa.pes_id = view__mensalidades.pessoa
                            INNER JOIN
                        endereco ON endereco.pes_id = view__mensalidades.pessoa and endereco.tipo_endereco = 1
                    WHERE
                        vencimento
                         BETWEEN
                        '{$dataInicio}'
                        AND
                        '{$dataFim}'  {$turma}
                    AND estado NOT IN ('cancelado' , 'pago')
                    ORDER BY nome, Dias desc ");

        return $this->executeQuery($query)->fetchAll();
    }

    public function buscaAdiplentes($per, $turno, $turma, $dataInicio, $dataFim, $pagamentos)
    {
        if (!empty($per)) {
            $per = " AND Periodo = '{$per}' ";
        }
        if ($turma !== 'Todos') {
            $turma = " AND  Turma = '{$turma}' ";
        } else {
            $turma = '';
        }

        if ($turno !== 'Todos') {
            $turno = " AND  Turno = '{$turno}' ";
        } else {
            $turno = "";
        }

        $query = ("SELECT
                        Matricula as matricula,
                        UPPER(Nome) as nome,
                        turma,
                        pes_cpf as cpf,
                        pessoa.con_contato_telefone telefone,
                        pessoa.con_contato_celular celular,
                        pessoa.con_contato_email email,
                        end_logradouro,
                        end_numero,
                        end_complemento,
                        end_bairro,
                        end_cidade,
                        end_estado,
                        end_cep,
                        parcela as parcela,
                        valor as valor,
                        vencimento as vencimento,
                        TipoPagamento as forma,
                        ValorPago as pago,
                        Desconto as desconto,
                        Multa as multa,
                        Juros as juros,
                        Juros+ Multa as 'acrescimo',
                        Pagamento as 'pagamento'
                    FROM
                        view__mensalidades
                            INNER JOIN
                        pessoa_fisica ON pessoa_fisica.pes_id = view__mensalidades.pessoa
                            INNER JOIN
                        pessoa ON pessoa.pes_id = view__mensalidades.pessoa
                            INNER JOIN
                        endereco ON endereco.pes_id = view__mensalidades.pessoa
                    WHERE
                        vencimento
                         BETWEEN
                        '{$dataInicio}'
                        AND
                        '{$dataFim}' {$per} {$turno} {$turma} AND TipoPagamento in({$pagamentos})
                    AND estado  IN ('pago')
                    ORDER BY nome, parcela");

        return $this->executeQuery($query)->fetchAll();
    }

    public function buscaTipoDescontos()
    {
        $query = "SELECT * FROM  financeiro__desconto_tipo WHERE desctipo_modalidade != 'Financiamento'";

        return $this->executeQuery($query)->fetchAll();
    }

    public function buscaDescontosSintetico($periodo)
    {
        $descontos = $this->executeQuery("SELECT * FROM  financeiro__desconto_tipo WHERE 1")->fetchAll();

        $descontoPeriodo = $this->executeQuery(
            "SELECT * , char_length(replace(desconto_mensalidades,',','')) as mensalidades FROM view__descontos WHERE per_id = {$periodo} AND desconto_status = 'Ativo'"
        )->fetchAll();

        if (!is_null($descontoPeriodo)) {
            for ($i = 0; $i < count($descontos); $i++) {
                for ($x = 0; $x < count($descontoPeriodo); $x++) {
                    if ($descontos[$i]['desctipo_id'] === $descontoPeriodo[$x]['desctipo_id']) {
                        $valor = $descontoPeriodo[$x]['mensalidades'] * $descontoPeriodo[$x]['desconto_valor'];
                        $descontos[$i]['mensalidades'] += (substr_count(
                                $descontoPeriodo[$x]['desconto_mensalidades'],
                                ','
                            ) + 1);
                        $descontos[$i]['total'] += $valor;
                        $descontos[$i]['beneficiados']++;
                    }
                }
            }

            return $descontos;
        }

        return $descontos;
    }

    public function buscaDescontosAnaliticos($periodo, $turno, $turma)
    {
        if (!empty($per)) {
            $per = " AND Periodo = '{$per}' ";
        }
        if ($turma !== 'Todos') {
            $turma = " AND  Turma = '{$turma}' ";
        } else {
            $turma = '';
        }

        if ($turno !== 'Todos') {
            $turno = " AND  turma_turno = '{$turno}' ";
        } else {
            $turno = "";
        }

        $descontos = $this->executeQuery(
            "SELECT desctipo_descricao FROM view__descontos WHERE per_id = {$periodo} AND desconto_status = 'Ativo' GROUP BY desctipo_descricao"
        )->fetchAll();

        $descontoPeriodo = $this->executeQuery(
            "SELECT * , char_length(replace(desconto_mensalidades,',','')) as mensalidades FROM view__descontos WHERE per_id = {$periodo} {$turma} {$turno} AND desconto_status = 'Ativo'"
        )->fetchAll();

        for ($z = 0; $z < count($descontos); $z++) {
            $descontos[$z] = [
                '0'  => $descontos[$z]['desctipo_descricao'],
                '1'  => 0,
                '2'  => 0,
                '3'  => 0,
                '4'  => 0,
                '5'  => 0,
                '6'  => 0,
                '7'  => 0,
                '8'  => 0,
                '9'  => 0,
                '10' => 0,
            ];
        }
        if (!is_null($descontoPeriodo)) {
            for ($i = 0; $i < count($descontos); $i++) {
                for ($x = 0; $x < count($descontoPeriodo); $x++) {
                    if ($descontos[$i][0] === $descontoPeriodo[$x]['desctipo_descricao']) {
                        $descontos[$i][$descontoPeriodo[$x]['turma_serie']] += $descontoPeriodo[$x]['mensalidades'] * $descontoPeriodo[$x]['desconto_valor'];
                    }
                }
            }

            return $descontos;
        }

        return $descontos;
    }

    public function buscaDescontosAlunosPeriodoLetivo($periodo, $turno, $desconto)
    {
        if (!empty($periodo)) {
            $where = "WHERE acadperiodo__turma.per_id = '{$periodo}' ";
        } else {
            $where = "";
        }
        if ($turno !== 'Todos') {
            $where .= " AND  acadperiodo__turma.turma_turno = '{$turno}' ";
        } else {
            $where .= "";
        }

        $select = " SELECT *, month(financeiro__titulo.titulo_data_vencimento) as parcela
                        FROM
                            financeiro__desconto
                                NATURAL JOIN
                            financeiro__mensalidade_desconto
                                NATURAL JOIN
                            financeiro__titulo_mensalidade
                                NATURAL JOIN
                            acadperiodo__aluno
                                NATURAL JOIN
                            acadperiodo__turma
                            NATURAL JOIN
                            financeiro__desconto_tipo
                                NATURAL JOIN
                            financeiro__titulo
                                NATURAL JOIN
                            pessoa
                        {$where} and desctipo_descricao = '{$desconto}' ";

        return $this->executeQuery($select)->fetchAll();
    }

    /**
     * @param $mesNumber
     * @return string|null
     */
    public static function mesReferencia($mesNumber)
    {
        if (strlen($mesNumber) == 1) {
            $mesNumber = '0' . $mesNumber;
        }

        $mes = [
            '01' => 'Janeiro',
            '02' => 'Fevereiro',
            '03' => 'Marco',
            '04' => 'Abril',
            '05' => 'Maio',
            '06' => 'Junho',
            '07' => 'Julho',
            '08' => 'Agosto',
            '09' => 'Setembro',
            '10' => 'Outubro',
            '11' => 'Novembro',
            '12' => 'Dezembro',
        ];

        if (!empty($mes[$mesNumber])) {
            return $mes[$mesNumber];
        } else {
            return null;
        }
    }

    public function chequesPeriodo($dataInicio, $dataFim, $filtro)
    {
        if ($filtro == 'Todos') {
            $filtro = '';
        } elseif ($filtro == 'Compensação') {
            $filtro = "AND cheque_vencimento <= date(now()) AND titulocheque_estado = 'ativo' ";
        } elseif ($filtro == 'Aberto') {
            $filtro = "AND cheque_vencimento >= date(now()) AND titulocheque_estado = 'ativo'";
        } else {
            $filtro = "AND titulocheque_estado = 'inativo'";
        }

        $select = " SELECT alunocurso_id,titulo_id,pes_nome as aluno, mensalidade_parcela, titulo_valor  , DATE(titulo_data_pagamento) AS titulo_data_pagamento, cheque_emitente , cheque_valor , DATE(cheque_vencimento) AS cheque_vencimento , titulocheque_valor , titulocheque_estado
                      FROM financeiro__cheque
                        NATURAL JOIN
                          financeiro__titulo_cheque
                        NATURAL JOIN
                          financeiro__titulo
                        NATURAL JOIN
                          pessoa
                        NATURAL JOIN
                          financeiro__titulo_mensalidade
                        NATURAL JOIN
                          acadgeral__aluno
                        NATURAL JOIN
                          acadgeral__aluno_curso
                      WHERE
                      DATE(titulo_data_vencimento)
                      BETWEEN '{$dataInicio}' AND '{$dataFim}' {$filtro}
                      ORDER BY alunocurso_id";

        return $this->executeQuery($select)->fetchAll();
    }

    public function buscaTitulosPagosPes($param)
    {
        $and = '';

        if (!$param['pesId']) {
            return false;
        } else {
            $and .= " AND pes.pes_id =:pesId ";
        }

        $arrParam = ['pesId' => $param['pesId'], 'anoBase' => $param['anoBase']];

        if ($param['anoBase']) {
            $and .= "AND YEAR(titulo.titulo_data_pagamento) =:anoBase";
        } else {
            unset($param['anoBase']);
        }

        $select = "
                    SELECT pes.pes_nome, pes.pes_tipo, coalesce(pes_cpf,pes_cpf) AS  pes_cpf,
					COUNT(titulo_id) as titulos, YEAR (titulo.titulo_data_pagamento) AS ano,
                    SUM(titulo_valor_pago) as total,
                    format(SUM(titulo_valor_pago), 2, 'de_DE') as totalFormatado
                    FROM pessoa as pes
                    INNER JOIN financeiro__titulo titulo ON titulo.pes_id= pes.pes_id
                    LEFT JOIN pessoa_fisica pesFisica ON pesFisica.pes_id=pes.pes_id
                    LEFT JOIN pessoa_juridica pesJuridica ON pesJuridica.pes_id=pes.pes_id
                    WHERE  (titulo_tipo_pagamento != 'isento' OR titulo_tipo_pagamento IS NULL) AND titulo_estado = 'Pago'
                    {$and}
                    GROUP BY pes.pes_id;
        ";

        return $this->executeQueryWithParam($select, $arrParam)->fetchAll();
    }

    /*
     * geraRelatorioFinanceiroPeriodo
     * $dataInicial and $dataFinal: Intervalo de data para geracao do relatório.
     * $tipoPagamento: Array contento os tipo de pagamento que deseja filtrar.
     * $servicos: Array contento os serviços que deseja filtra. Ex: Mensalidade, Multa de Biblioteca.
     * $estado: Estado dos titulos que deseja-se filtrar. Ex: Pagos, Abertos, Cancelados.
     * */
    public function geraRelatorioFinanceiroPeriodo($dataInicio, $dataFim, $usuario = 'todos')
    {
        if ($usuario === 'todos') {
            $usuario = "";
        } else {
            $usuario = "AND usuario_baixa = {$usuario}";
        }
        $query = ("SELECT pes_nome,acadgeral__aluno_curso.alunocurso_id ,tipotitulo_nome,financeiro__titulo.titulo_id,titulo_tipo_pagamento,financeiro__titulo.titulo_data_vencimento,titulo_data_pagamento,round((titulo_multa + titulo_juros),2) as acrescimo,titulo_estado,titulo_valor_pago,titulo_valor_pago_dinheiro, titulo_desconto,financeiro__cheque.cheque_id,titulocheque_valor,cheque_valor,if(date(cheque_vencimento) <= date(titulo_data_pagamento) or cheque_vencimento is null,'a vista','pre') as pagamento
                    FROM
                      pessoa
                    LEFT JOIN
                      acadgeral__aluno USING (pes_id)
                    LEFT JOIN
                      acadgeral__aluno_curso USING (aluno_id)
                    LEFT JOIN
                      financeiro__titulo USING (alunocurso_id)
                    LEFT JOIN
                      financeiro__titulo_cheque ON financeiro__titulo.titulo_id = financeiro__titulo_cheque.titulo_id
                    LEFT JOIN
                      financeiro__cheque ON financeiro__cheque.cheque_id = financeiro__titulo_cheque.cheque_id
                    NATURAL JOIN
                      financeiro__titulo_tipo
                    WHERE
                    titulo_estado = 'pago'
                    AND
                    titulo_valor_pago is not null
                    AND
                    titulo_valor_pago > 0
                    {$usuario}
                    AND
                      tipotitulo_nome != 'Vestibular'
                    AND
                      DATE(financeiro__titulo.titulo_data_pagamento)
                    BETWEEN
                        DATE('{$this->formatDateAmericano($dataInicio,'Ymd')}') AND DATE('{$this->formatDateAmericano($dataFim,'Ymd')}')
                    ORDER BY titulo_tipo_pagamento,titulo_data_pagamento");

        return $this->executeQuery($query)->fetchAll();
        // Criação do array associativo por Tipo de Titulo e Forma de Pagamento.
        //        foreach($titulos as $formas){
        //            if($formas['titulo_tipo_pagamento'] === 'Dinheiro' OR $formas['titulo_tipo_pagamento'] === 'Dinheiro,Cheque' ) {
        //                if(sizeof($titulo[($formas['tipotitulo_nome'] === 'Mensalidade') ? 'mensalidade' : 'taxas']['dinheiro']) === 0){
        //                    $sizeof = 1;
        //                }elseif(sizeof($titulo[($formas['tipotitulo_nome'] === 'Mensalidade') ? 'mensalidade' : 'taxas']['dinheiro']) === 2){
        //                    $sizeof = 2;
        //                }else{
        //                    $sizeof = sizeof($titulo[($formas['tipotitulo_nome'] === 'Mensalidade') ? 'mensalidade' : 'taxas']['dinheiro']);
        //                }
        //
        //                $titulo[($formas['tipotitulo_nome'] === 'Mensalidade') ? 'mensalidade' : 'taxas']['dinheiro'][ $sizeof ]  = [
        //                    'aluno'     => $formas['pes_nome'],
        //                    'matricula' => $formas['alunocurso_id'],
        //                    'titulo'    => $formas['titulo_id'],
        //                    'vencimento'=> $formas['titulo_data_vencimento'],
        //                    'data-pagamento' => $formas['titulo_data_pagamento'],
        //                    'estado'    => $formas['titulo_estado'],
        //                    'acrescimo' => $formas['acrescimo'],
        //                    'pagamento' => $formas['pagamento'],
        //                    'valor-pago' =>number_format($formas['titulo_valor_pago_dinheiro'], 2, ',', '.'),
        //                ];
        //                $titulo[($formas['tipotitulo_nome'] === 'Mensalidade') ? 'mensalidade' : 'taxas']['dinheiro']['total'] += $formas['titulo_valor_pago_dinheiro'];
        //            }
        //                if($formas['titulo_tipo_pagamento'] == 'Cheque' OR  !is_null($formas['titulocheque_valor'])) {
        //                    if($formas['pagamento'] == 'a vista' ){
        //                        if(sizeof($titulo[($formas['tipotitulo_nome'] === 'Mensalidade') ? 'mensalidade' : 'taxas']['cheque-avista']) === 0){
        //                            $sizeof = 1;
        //                        }elseif(sizeof($titulo[($formas['tipotitulo_nome'] === 'Mensalidade') ? 'mensalidade' : 'taxas']['cheque-avista']) === 2){
        //                            $sizeof = 2;
        //                        }else{
        //                            $sizeof = sizeof($titulo[($formas['tipotitulo_nome'] === 'Mensalidade') ? 'mensalidade' : 'taxas']['cheque-avista']);
        //                        }
        //                        $titulo[($formas['tipotitulo_nome'] === 'Mensalidade') ? 'mensalidade' : 'taxas']['cheque-avista'][$sizeof] = [
        //                            'aluno'     => $formas['pes_nome'],
        //                            'matricula' => $formas['alunocurso_id'],
        //                            'titulo'    => $formas['financeiro__titulo.titulo_id'],
        //                            'vencimento'=> $formas['financeiro__titulo.titulo_data_vencimento'],
        //                            'data-pagamento' => $formas['titulo_data_pagamento'],
        //                            'estado'    => $formas['titulo_estado'],
        //                            'acrescimo' =>  number_format($formas['acrescimo'],2, ',', '.'),
        //                            'pagamento' => $formas['pagamento'],
        //                            'valor-pago'=> number_format($formas['titulocheque_valor'],2, ',', '.'),
        //                        ];
        //                        $titulo[($formas['tipotitulo_nome'] === 'Mensalidade') ? 'mensalidade' : 'taxas']['cheque-avista']['total'] +=  number_format($formas['titulocheque_valor'],2, ',', '.');
        //
        //                    }else{
        //                        if(sizeof($titulo[($formas['tipotitulo_nome'] === 'Mensalidade') ? 'mensalidade' : 'taxas']['cheque-avista']) === 0){
        //                            $sizeof = 1;
        //                        }elseif(sizeof($titulo[($formas['tipotitulo_nome'] === 'Mensalidade') ? 'mensalidade' : 'taxas']['cheque-avista']) === 2){
        //                            $sizeof = 2;
        //                        }else{
        //                            $sizeof = sizeof($titulo[($formas['tipotitulo_nome'] === 'Mensalidade') ? 'mensalidade' : 'taxas']['cheque-avista']);
        //                        }
        //                            $titulo[($formas['tipotitulo_nome'] === 'Mensalidade') ? 'mensalidade' : 'taxas']['cheque-pre'][$sizeof] = [
        //                                'aluno'     => $formas['pes_nome'],
        //                                'matricula' => $formas['alunocurso_id'],
        //                                'titulo'    => $formas['financeiro__titulo.titulo_id'],
        //                                'vencimento'=> $formas['financeiro__titulo.titulo_data_vencimento'],
        //                                'data-pagamento' => $formas['titulo_data_pagamento'],
        //                                'estado'    => $formas['titulo_estado'],
        //                                'acrescimo' =>  number_format($formas['acrescimo'],2, ',', '.'),
        //                                'pagamento' => $formas['pagamento'],
        //                                'valor-pago'=> number_format($formas['titulocheque_valor'],2, ',', '.'),
        //                            ];
        //                        $titulo[($formas['tipotitulo_nome'] === 'Mensalidade') ? 'mensalidade' : 'taxas']['cheque-pre']['total'] += $formas['titulocheque_valor'];
        //
        //                    }
        //
        //                }
        //                    if($formas['titulo_tipo_pagamento'] === 'Deposito'){
        //                        if(sizeof($titulo[($formas['tipotitulo_nome'] === 'Mensalidade') ? 'mensalidade' : 'taxas']['deposito']) === 0){
        //                            $sizeof = 1;
        //                        }elseif(sizeof($titulo[($formas['tipotitulo_nome'] === 'Mensalidade') ? 'mensalidade' : 'taxas']['deposito']) === 2){
        //                            $sizeof = 2;
        //                        }else{
        //                            $sizeof = sizeof($titulo[($formas['tipotitulo_nome'] === 'Mensalidade') ? 'mensalidade' : 'taxas']['deposito']);
        //                        }
        //                        $titulo[($formas['tipotitulo_nome'] === 'Mensalidade') ? 'mensalidade' : 'taxas']['deposito'][$sizeof] = [
        //                            'aluno'     => $formas['pes_nome'],
        //                            'matricula' => $formas['alunocurso_id'],
        //                            'titulo'    => $formas['financeiro__titulo.titulo_id'],
        //                            'vencimento'=> $formas['financeiro__titulo.titulo_data_vencimento'],
        //                            'data-pagamento' => $formas['titulo_data_pagamento'],
        //                            'estado'    => $formas['titulo_estado'],
        //                            'acrescimo' =>  number_format($formas['acrescimo'],2, ',', '.'),
        //                            'pagamento' => $formas['pagamento'],
        //                            'valor-pago'=> number_format($formas['titulo_valor_pago'],2, ',', '.'),
        //                        ];
        //                        $titulo[($formas['tipotitulo_nome'] === 'Mensalidade') ? 'mensalidade' : 'taxas']['deposito']['total'] += number_format($formas['titulo_valor_pago'],2, ',', '.');
        //
        //                    }
        //        }

        //        return $titulo;

    }

    public function ordenaMensalidadesAluno($mensalidades)
    {
        foreach ($mensalidades as $aluno) {
            if (empty($alunos[$aluno['matricula']])) {
                $alunos[$aluno['matricula']]['informacoes'] = [
                    'mat'         => $aluno['matricula'],
                    'nome'        => $aluno['nome'],
                    'turma'       => $aluno['Turma'],
                    'cpf'         => $aluno['cpf'],
                    'email'       => $aluno['email'],
                    'cep'         => $aluno['end_cep'],
                    'rua'         => $aluno['end_logradouro'],
                    'bairro'      => $aluno['end_bairro'],
                    'cidade'      => $aluno['end_cidade'],
                    'complemento' => $aluno['end_complemento'],
                    'email'       => $aluno['email'],
                    'telefone'    => $aluno['telefone']

                ];
                $sizeof                                     = 0;
            } elseif (sizeof($alunos[$aluno['matricula']]['mensalidade']) === 2) {
                $sizeof = 2;
            } else {
                $sizeof = sizeof($alunos[$aluno['matricula']]['mensalidade']);
            }
            $alunos[$aluno['matricula']]['mensalidade'][$sizeof] = [
                'vencimento'  => $aluno['vencimento'],
                'dias'        => $aluno['dias'],
                'parcela'     => $aluno['parcela'],
                'mes_parcela' => self::mesReferencia(explode('-', $aluno['vencimento'])[1]),
                'valor'       => $aluno['valor'],
                'descontos'   => $aluno['desconto'],
                'acrescimo'   => $aluno['acrescimo'],
                'valorFinal'  => $aluno['valor'] + $aluno['acrescimo'],
                'pagamento'   => $aluno['pagamento'],
                'valorPago'   => $aluno['pago']
            ];
            $alunos[$aluno['matricula']]['totalizador']['valor'] += $alunos[$aluno['matricula']]['mensalidade'][$sizeof]['valor'];
            $alunos[$aluno['matricula']]['totalizador']['desconto'] += $alunos[$aluno['matricula']]['mensalidade'][$sizeof]['desconto'];
            $alunos[$aluno['matricula']]['totalizador']['acrescimo'] += $alunos[$aluno['matricula']]['mensalidade'][$sizeof]['acrescimo'];
            if (is_null($alunos[$aluno['matricula']]['mensalidade'][$sizeof]['valorPago'])) {
                $alunos[$aluno['matricula']]['totalizador']['valorFinal'] += $alunos[$aluno['matricula']]['mensalidade'][$sizeof]['valorFinal'];
            } else {
                $alunos[$aluno['matricula']]['totalizador']['valorFinal'] += $alunos[$aluno['matricula']]['mensalidade'][$sizeof]['valorPago'];
            }
        }

        return $alunos;
    }

    public function valorPorExtenso($valor = 0)
    {
        $singular = array("centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
        $plural   = array("centavos", "reais", "mil", "milhões", "bilhões", "trilhões", "quatrilhões");

        $c   = array(
            "",
            "cem",
            "duzentos",
            "trezentos",
            "quatrocentos",
            "quinhentos",
            "seiscentos",
            "setecentos",
            "oitocentos",
            "novecentos"
        );
        $d   = array(
            "",
            "dez",
            "vinte",
            "trinta",
            "quarenta",
            "cinquenta",
            "sessenta",
            "setenta",
            "oitenta",
            "noventa"
        );
        $d10 = array(
            "dez",
            "onze",
            "doze",
            "treze",
            "quatorze",
            "quinze",
            "dezesseis",
            "dezessete",
            "dezoito",
            "dezenove"
        );
        $u   = array("", "um", "dois", "três", "quatro", "cinco", "seis", "sete", "oito", "nove");

        $z = 0;

        $valor   = number_format($valor, 2, ".", ".");
        $inteiro = explode(".", $valor);
        for ($i = 0; $i < count($inteiro); $i++) {
            for ($ii = strlen($inteiro[$i]); $ii < 3; $ii++) {
                $inteiro[$i] = "0" . $inteiro[$i];
            }
        }

        // $fim identifica onde que deve se dar junção de centenas por "e" ou por "," 😉
        $fim = count($inteiro) - ($inteiro[count($inteiro) - 1] > 0 ? 1 : 2);
        for ($i = 0; $i < count($inteiro); $i++) {
            $valor = $inteiro[$i];
            $rc    = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
            $rd    = ($valor[1] < 2) ? "" : $d[$valor[1]];
            $ru    = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";

            $r = $rc . (($rc && ($rd || $ru)) ? " e " : "") . $rd . (($rd && $ru) ? " e " : "") . $ru;
            $t = count($inteiro) - 1 - $i;
            $r .= $r ? " " . ($valor > 1 ? $plural[$t] : $singular[$t]) : "";
            if ($valor == "000") {
                $z++;
            } elseif ($z > 0) {
                $z--;
            }
            if (($t == 1) && ($z > 0) && ($inteiro[0] > 0)) {
                $r .= (($z > 1) ? " de " : "") . $plural[$t];
            }
            if ($r) {
                $rt = $rt . ((($i > 0) && ($i <= $fim) && ($inteiro[0] > 0) && ($z < 1)) ? (($i < $fim) ? ", " : " e ") : " ") . $r;
            }
        }

        return ($rt ? $rt : "zero");
    }

    public function buscaDescontoAlunoSintetico($periodo)
    {
        $query    = "SELECT
                       alunocurso_id AS matricula,
                       pes_nome,
                       turma_nome,
                       matsit_descricao Situacao,
                       desctipo_descricao AS desconto,
                       desconto_percentual AS percentual,
                       desctipo_modalidade,
                       desconto_status,
                       desconto_valor,
                       length(replace(desconto_mensalidades,',','')) * desconto_valor
                       FROM
                       view__descontos
                    WHERE
                       per_id = {$periodo}
                           AND desctipo_descricao NOT LIKE '%FIES%'
                           AND desconto_status = 'Ativo'
                           and matsit_descricao = 'Matriculado' and matsit_descricao not in ('Cancelada','Trancado','Transferencia')
                    ORDER BY pes_nome";
        $desconto = $this->executeQuery($query)->fetchAll();

        return $desconto;
    }

    public function buscaTurmaNome($id)
    {
        if ($id) {
            $query = "SELECT turma_nome from acadperiodo__turma where turma_id in ($id)";

            $result = $this->executeQuery($query)->fetchAll();

            return $result;
        } else {
            return null;
        }
    }

    public function buscaUsuariosTesouraria()
    {
        $usuarios = $this->executeQuery(
            "
            SELECT
              p.id AS usuario_id, pessoa.pes_nome AS pessoa_nome
            FROM
              acesso_pessoas p
            INNER JOIN
              acesso_usuarios u ON p.usuario = u.id
            INNER JOIN
              acesso_usuarios_grupos g ON g.usuario_id = u.id
            INNER JOIN
              acesso_grupo gru ON gru.id = g.grupo_id
            INNER JOIN
              pessoa_fisica ON pessoa_fisica.pes_id = p.pes_fisica
            INNER JOIN
              pessoa ON pessoa.pes_id = pessoa_fisica.pes_id
            WHERE
              grup_nome = 'Tesouraria'
        "
        )->fetchAll();

        $ret = array('todos' => 'Todos');

        foreach ($usuarios as $usuario) {
            $ret[$usuario['usuario_id']] = $usuario['pessoa_nome'];
        }

        return $ret;
    }

    /**
     * Retorna tipos de títulos
     * @return array
     */
    public function buscaServicosRelatorio()
    {
        $array = array('todos' => 'Todos');
        $tipos = $this->getRepository('Boleto\Entity\FinanceiroTituloTipo')->findAll();

        foreach ($tipos as $tipo) {
            $array[$tipo->getTipotituloId()] = $tipo->getTipotituloNome();
        }

        return $array;
    }
}

