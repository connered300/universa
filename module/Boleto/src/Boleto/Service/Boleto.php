<?php

namespace Boleto\Service;

use VersaSpine\Service\AbstractService;

class Boleto extends AbstractService
{
    const ESTADO_ABERTO    = 'Aberto';
    const ESTADO_PAGO      = 'Pago';
    const ESTADO_CANCELADO = 'Cancelado';

    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Boleto\Entity\Boleto');
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {
    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($data)
    {
    }

    public function buscaBoleto($id = null){
        if(!is_null($id)){
            $query =
                "SELECT
              boleto.*,
              boleto_banco.*,
              financeiro__titulo.*,
              boleto_conf_conta.*,
              cedente.pes_nome as cedente_nome,
              cedente_fisica.pes_cpf as cedente_cpf,
              cedente_jurica.pes_cnpj as cedente_cnpj,
              sacado.pes_nome as sacado_nome,
              sacado_fisica.pes_cpf as sacado_cpf,
              sacado_juridica.pes_cnpj as sacaca_cnpj,
              endereco.*,
              tipo_endereco.*
            FROM
              boleto
            INNER JOIN
              boleto_conf_conta
            ON
              boleto_conf_conta.confcont_id = boleto.confcont_id
            INNER JOIN
              boleto_banco ON boleto_banco.banc_id = boleto_conf_conta.banc_id
            INNER JOIN
              financeiro__titulo
            ON
              boleto.titulo_id = financeiro__titulo.titulo_id
            INNER JOIN
              pessoa as cedente
            ON
              cedente.pes_id = boleto_conf_conta.pes_id
            LEFT JOIN
              pessoa_juridica as cedente_jurica
            ON
              cedente_jurica.pes_id = boleto_conf_conta.pes_id
            LEFT JOIN
              pessoa_fisica as cedente_fisica
            ON
              cedente_fisica.pes_id = boleto_conf_conta.pes_id
            INNER JOIN
              pessoa as sacado
            ON
              sacado.pes_id = financeiro__titulo.pes_id
            LEFT JOIN
              pessoa_juridica as sacado_juridica
            ON
              sacado_juridica.pes_id = financeiro__titulo.pes_id
            LEFT JOIN
              pessoa_fisica as sacado_fisica
            ON
              sacado_fisica.pes_id = financeiro__titulo.pes_id
            LEFT JOIN
              endereco
            ON
              sacado.pes_id = endereco.pes_id
            LEFT JOIN
              tipo_endereco
            ON
              endereco.tipo_endereco = tipo_endereco.tipo_endereco_id
            AND
              tipo_endereco.nome = 'Comercial'
            WHERE
              boleto.bol_id = {$id}
        ";
        $sql = $this->executeQuery($query);
        $dados = $sql->fetch();

            return $dados;
        }
        return false;

    }

    public function formataDadosBoleto(array $dados)
    {
    }

    public function formataDadosParaBancoBrasil(array $dados)
    {
        $cedente = array(
            'documento' => $dados['cedente_cnpj'] != "" ? $dados['cedente_cnpj'] : $dados['cedente_cpf'],
            'identificacao' => '',
            'nomeCedente' =>  $dados['cedente_nome'],
            'endereco' => $dados['end_tipo_logradouro']." ".$dados['end_logradouro'].", ".$dados['end_numero']." - ".$dados['end_bairro'].", ".$dados['end_cidade']." - ".$dados['end_estado'],
            'cidade' => $dados['end_cidade'],
            'uf' => $dados['end_estado'],
            'logoCedente' => '',
            'agencia' => $dados['confcont_agencia'],
            'agenciaDv' => $dados['confcont_agencia_digito'],
            'contaCorrente' => $dados['confcont_conta'],
            'contaCorrenteDv' => $dados['confcont_conta_digito'],
            'formatacaoConvenio' => strlen($dados['confcont_convenio']), // necessario
            'convenio' => $dados['confcont_convenio'], // necessario
            'contrato' => '', // necessario
            'carteira' => $dados['confcont_carteira'],
            'variacaoCarteira' => $dados['confcont_variacao']
        );

        return $cedente;

    }

    public function formataDadosParaBancoBradesco(array $dados)
    {
        $cedente = array(
            'documento' => $dados['cedente_cnpj'] != "" ? $dados['cedente_cnpj'] : $dados['cedente_cpf'],
            'nomeCedente' => $dados['cedente_nome'],
            'logoCedente' => '',
            'agencia' => $dados['confcont_agencia'],
            'agenciaDv' => $dados['confcont_agencia_digito'],
            'contaCorrente' => $dados['confcont_conta'],
            'contaCorrenteDv' => $dados['confcont_conta_digito'],
            'endereco' => $dados['end_tipo_logradouro']." ".$dados['end_logradouro'].", ".$dados['end_numero']." - ".$dados['end_bairro'].", ".$dados['end_cidade']." - ".$dados['end_estado'],
            'cidade' => $dados['end_cidade'],
            'uf' => $dados['end_estado'],
            'contaCedente' => $dados['confcont_conta'],
            'contaCedenteDv' => $dados['confcont_conta_digito'],
            'formatacaoConvenio' => '',
            'convenio' => '',
            'contrato' => '',
            'carteira' => $dados['confcont_carteira'],
            'variacaoCarteira' => '',
            'agenciaCodigo' => '',
            'carteiraDescricao' => '',
            'codigocliente' => '',
            'pontodevenda' => '',
        );

        return $cedente;
    }

    public function adicionar(array $dados)
    {
        $ultimoBoleto = $this->executeQuery("SELECT MAX(bol_nosso_numero) bol_nosso_numero, MAX(bol_numero_documento) bol_numero_documento FROM boleto")->fetch();

        if(!empty($ultimoBoleto['bol_nosso_numero'])){
            $dados['bolNossoNumero'] = $ultimoBoleto['bol_nosso_numero'] + 1;
            $dados['bolNumeroDocumento'] = $ultimoBoleto['bol_numero_documento'] + 1;
        }else{
            $dados['bolNossoNumero'] = 1;
            $dados['bolNumeroDocumento'] =  1;
        }


        $dados['bolDemostrativo'] = "teste";
        $dados['bolId'] = "";
        $dados['bolEstado'] = 'Aberto';

        return parent::adicionar($dados);

    }

    public function baixaArquivo(array $dados){
        $tamanho = sizeof($dados);
        $count = 0;
        if($tamanho > 2){
            $tamanho--;
            for($i = 1; $i < $tamanho; $i++){
                //nosso numero formato banco do brasil
                if($dados[0]['num_banco'] == '001'){
                    $nossoNumero = substr(trim($dados[$i]['nosso_numero']), -10);
                }
                //nosso numero formato banco bradesco
                else if($dados[0]['num_banco'] == '237'){
                    $nossoNumero = $dados[$i]['nosso_numero'];
                    $nossoNumero = substr($nossoNumero, 0, -1);
                }


                $boleto = $this->findOneBy(array('bolNossoNumero' => $nossoNumero));
                if($boleto){
                    if($boleto->getBolEstado() == "Aberto"){
                        $boleto->setBolEstado("Pago");
                        $this->begin();
                        try{
                            $this->getEm()->persist($boleto);
                            $this->getEm()->flush();
                        } catch (\Exception $ex) {
                            throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
                        }
                        try{
                            $titulo = (new FinanceiroTitulo($this->getEm()))
                            ->baixa($boleto->getTitulo()->getTituloId(), $this->formatDateAmericano($dados[$i]['data_ocorrencia']), $dados[$i]['valor'] );
                        } catch (\Exception $ex) {
                            throw new \Exception("Erro ao dar baixa no titulo", $ex->getCode(), $ex->getPrevious());
                        }
                        if($titulo){
                            $this->commit();
                            $count++;
                        }
                    }
                }
            }
        }
        return $count;
    }

    public function buscaBoletoPorPessoa($idPes, $tipo = null){
        $where = "";
        if(!empty($idPes)){
            $where .= "financeiro__titulo.pes_id  = {$idPes}";
        }
        if(!empty($tipo)){
            $where .= " AND financeiro__titulo.titulo_tipo = $tipo";
        }

        $query = "
            SELECT
              bol_id, titulo_data_vencimento as data_vencimento, boleto.titulo_id, bol_via, titulo_estado
            FROM
              boleto
            INNER JOIN
              financeiro__titulo
            ON
              financeiro__titulo.titulo_id = boleto.titulo_id
            WHERE
              $where
        ";

        $boleto =  $this->executeQuery($query)->fetchAll();
        return $boleto;
    }

    public function buscaBoletoPorTitulo($tituloId)
    {
        $arrTitulo = $this->getRepository()->findBy(['titulo' => $tituloId]);

        return $arrTitulo;
    }

    public function search($arrData)
    {
        $query = "
            SELECT
                bol_nosso_numero nosso_numero,
                titulo.titulo_valor valor,
                titulo.titulo_data_vencimento data_vencimento,
                boleto.bol_id bol_id,
                pessoa.pes_nome sacado
            FROM
                boleto
            INNER JOIN financeiro__titulo titulo ON boleto.titulo_id = titulo.titulo_id
            INNER JOIN pessoa ON titulo.pes_id = pessoa.pes_id
            INNER JOIN boleto_conf_conta bc ON bc.confcont_id = boleto.confcont_id
            WHERE 1 ";

        $where = [];
        if ($arrData['confContaId']) {
            $where[] = "bc.confcont_id = {$arrData['confContaId']}";
        } 

        if ($arrData['dataPross']) {
            $dataPross = $arrData['dataPross'];

            if ($dataPross['dataInicial']) {
                $dataInicial = self::formatDateAmericano($dataPross['dataInicial']);
                $where[]    .= "titulo.titulo_data_processamento > '$dataInicial'";
            }

            if ($dataPross['dataFinal']) {
                $dataFinal = self::formatDateAmericano($dataPross['dataFinal']);
                $where[]  .= "titulo.titulo_data_processamento < '$dataFinal'";
            }
        }

        if ($arrData['dataVenc']) {
            $dataVenc = $arrData['dataVenc'];

            if ($dataVenc['dataInicial']) {
                $dataInicial = self::formatDateAmericano($dataVenc['dataInicial']);
                $where[]    .= "titulo.titulo_data_vencimento > '$dataInicial'";
            }

            if ($dataVenc['dataFinal']) {
                $dataFinal = self::formatDateAmericano($dataVenc['dataFinal']);
                $where[]  .= "titulo.titulo_data_vencimento < '$dataFinal'";
            }
        }

        if ($arrData['boletosNotInclused']) {
            $where[] = "boleto.bol_id NOT IN(".implode(',',$arrData['boletosNotInclused']).")";
        }

        $query.= implode(' AND ', $where);

        $result = $this->paginationDataTablesAjax($query, $arrData, null, false);

        return $result;
    }
}