<?php

namespace Boleto\Service;

use VersaSpine\Service\AbstractService;

class FinanceiroTituloMensalidade extends AbstractService 
{
	public function __construct(\Doctrine\ORM\EntityManager $em)
	{
		parent::__construct($em, 'Boleto\Entity\FinanceiroTituloMensalidade');
	}

	/**
	 * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
	 * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
	 * usar os mesmos no banco de dados
	 * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
	 * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
	 * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
	 * @return array | true
	 * @throws \Exception
	 */
	protected function valida($dados) 
	{

	}

	/**
	 * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
	 * este método pode ser utilzado para retornado buscas no formato json
	 */
	protected function pesquisaForJson($criterios) 
	{

	}

	public function buscaDescontoPadrao($tituloId) 
	{
		if (!empty($tituloId)) {
			$query = "
              SELECT
                tituloconf_dia_venc as dia_vencimento,
                tituloconf_dia_desc as dia_vencimento_desconto,
                tituloconf_valor_desc as valor,
                tituloconf_percent_desc
              FROM
                financeiro__titulo_mensalidade
              NATURAL JOIN
                financeiro__titulo
              NATURAL JOIN
                financeiro__titulo_config
              WHERE
                titulo_id = {$tituloId}
              AND
                DATE_FORMAT(NOW(),'%Y-%m-%d') <= DATE_FORMAT(CONCAT(DATE_FORMAT(financeiro__titulo.titulo_data_vencimento, '%Y%m'),if( LENGTH(tituloconf_dia_desc) <= 1, CONCAT('0',tituloconf_dia_desc), tituloconf_dia_desc )), '%Y-%m-%d')
            ";
			$result = $this->executeQuery($query);
			if ($result->rowCount() > 0) {
				$result = $result->fetchAll();
				$total = "";
				for ($i = 0; $i < sizeof($result); $i++) {
					$result[$i]['tituloconf_valor_desc'] = \Boleto\Service\Moeda::decToMoeda(
						$result[$i]['tituloconf_valor_desc']
					);
				}

				return $result;
			}

			return false;
		}

		return false;
	}

	/*
	 * funcionalidade para nova inteface
	 */
	public function buscaDescontoPadraoNew($tituloId) 
	{
		if (!empty($tituloId)) {
			$query = "
              SELECT
                'Desconto Padrão' as descNome,
                tituloconf_dia_desc as descontoDescontoDiaLimite,
                tituloconf_valor_desc as descontoValor
              FROM
                financeiro__titulo_mensalidade
              NATURAL JOIN
                financeiro__titulo
              NATURAL JOIN
                financeiro__titulo_config
              WHERE
                titulo_id = {$tituloId}
              AND
                financeiro__titulo_mensalidade.mensalidade_parcela != 1
              AND
                DATE_FORMAT(NOW(),'%Y-%m-%d') <= DATE_FORMAT(CONCAT(DATE_FORMAT(financeiro__titulo.titulo_data_vencimento, '%Y%m'),if( LENGTH(tituloconf_dia_desc) <= 1, CONCAT('0',tituloconf_dia_desc), tituloconf_dia_desc )), '%Y-%m-%d')
            ";
			$result = $this->executeQuery($query);
			if ($result->rowCount() > 0) {
				$result = $result->fetchAll();
				$total = "";
				for ($i = 0; $i < sizeof($result); $i++) {
					$result[$i]['tituloconf_valor_desc'] = \Boleto\Service\Moeda::decToMoeda(
						$result[$i]['tituloconf_valor_desc']
					);
				}

				return $result;
			}

			return false;
		}

		return false;
	}

	public function siatuacaoMensalidades($aluno) 
	{
		$query = "
          SELECT
            DISTINCT(financeiro__titulo_mensalidade.mensalidade_parcela) as parcela,
            financeiro__titulo.titulo_estado as estado,
            usuario_baixa
          FROM
            financeiro__titulo_mensalidade
          NATURAL JOIN
            financeiro__titulo
          WHERE
            financeiro__titulo_mensalidade.alunoper_id = {$aluno}
          AND
            (financeiro__titulo.titulo_estado != 'Cancelado' AND financeiro__titulo.titulo_estado != 'Estorno')
          ORDER BY parcela ASC
       ";

		$result = $this->executeQuery($query);
		if ($result->rowCount() > 0) {
			$result = $result->fetchAll();

			return $result;
		}

		return false;
	}

	public function mensalidadesAluno($alunoper) 
	{
		if ($alunoper) {
			$respositoryTituloMensalidade = $this->getRepository($this->getEntity());
			$mensalidades = $respositoryTituloMensalidade->findBy(
				['alunoper' => $alunoper]
			);

			usort(
				$mensalidades,
				function ($a, $b) {
					$d1 = $a->getTitulo()->getTituloDataVencimento();
					$d2 = $b->getTitulo()->getTituloDataVencimento();
					$d1 = (new \DateTime($this->formatDateAmericano($d1)))->format('Ymd');
					$d2 = (new \DateTime($this->formatDateAmericano($d2)))->format('Ymd');

					return $d1 < $d2 ? -1 : 1;
				}
			);

			if ($mensalidades) {
				return $mensalidades;
			}

			return null;
		}

		return null;
	}

	/*
	 * recebe como parametro uma instacia de Boleto\Entity\FinanceiroTitulo em forma de array
	*/
	public function buscaDescontosMensalidade(array $titulo) 
	{
			$repositoryMensalidadeDesconto = $this->getEm()->getRepository('Boleto\Entity\FinanceiroMensalidadeDesconto');
			$mensalidadeDescontos = $repositoryMensalidadeDesconto->findBy([
				'financeiroTituloMensalidade' => $titulo['financeiroTituloMensalidadeId'],
				'descmensalidadeStatus'       => 'Ativo'
			]);

			$descontoPadrao = array();
			if ($titulo['tipotituloNome'] == "Mensalidade") {
					$descontoPadrao = $this->buscaDescontoPadraoNew($titulo['tituloId']);
			} 

			if ($mensalidadeDescontos) {
					foreach ($mensalidadeDescontos as $mensalidadeDesconto) {
							$arrayMensalidadeDescontos[] = $mensalidadeDesconto->toArray();
					}

					$mensalidadeDescontos = array_column($arrayMensalidadeDescontos, 'desconto');
					$repositoryDesconto    = $this->getEm()->getRepository('Boleto\Entity\FinanceiroDesconto');
					$descontos             = $repositoryDesconto->findBy(['descontoId' => $mensalidadeDescontos]);

					if ($descontos) {
							foreach ($descontos as $desconto) {
									$descontoArray = $desconto->toArray();
									if ($descontoArray['desctipoModalidade'] === "Financiamento" && $descontoArray['descontoPercentual'] < 100) {
											if (!empty($descontoPadrao)) {
											//tratar aqui quando um desconto padrao incidir no financiamento, por exemplo o fies de 50%
										}
									}
									$arrayDescontos['descontos'][] = $descontoArray;
							}
							return $arrayDescontos;
					}
			}
			
			if (!empty($descontoPadrao)) {
					$arrayDescontos = ['descontos' => $descontoPadrao];

					return $arrayDescontos;
			}

			return array();
	}

	public function calculaAcrecimos($tituloInfo) {
		if (is_array($tituloInfo)) {
			if ($tituloInfo['tituloId']) {
				$tituloInfo = $this->getRepository()->findOneBy(array('titulo' => (int) $tituloInfo['tituloId']));
			}
		}

		$tituloMensalidade = $tituloInfo;

		$descontoPadrao = $this->buscaDescontoPadraoNew($tituloMensalidade->getTitulo()->getTituloId());

		$valorAPagar = (float) $tituloMensalidade->getTitulo()->getTituloValor() - (is_null($tituloMensalidade->getTitulo()->getTituloDesconto()) ? 0 : $tituloMensalidade->getTitulo()->getTituloDesconto() );


		if (empty($descontoPadrao)) {
			$dataAtual = (new \DateTime('now'))->setTime(0, 0, 0);

			$tituloDataVencimento = $this->formatDateAmericano(
				$tituloMensalidade->getTitulo()->getTituloDataVencimento()
			);

			$tituloDataVencimento = date('Y-m-d', strtotime($tituloDataVencimento . ' + 5 days'));

			$dataVencimento = (new \DateTime($tituloDataVencimento));

			if ($dataAtual > $dataVencimento) {
				$confTituloInadinplencia['acrescimos']['dias'] = (int) $dataAtual->diff($dataVencimento)->format("%a");

				$confTituloInadinplencia['acrescimos']['jurus'] = $tituloMensalidade->getTituloconf()->getTituloconfJuros();

				$confTituloInadinplencia['acrescimos']['multa'] = $tituloMensalidade->getTituloconf()->getTituloconfMulta();

				$confTituloInadinplencia['acrescimos']['jurusCalculado'] = ($valorAPagar / 100) * ($confTituloInadinplencia['acrescimos']['dias'] * $confTituloInadinplencia['acrescimos']['jurus']);


				$confTituloInadinplencia['acrescimos']['multaCalculado'] = ($valorAPagar / 100) * $confTituloInadinplencia['acrescimos']['multa'];

				$confTituloInadinplencia['acrescimos']['valorAcrescimos'] = round(
					$confTituloInadinplencia['acrescimos']['jurusCalculado'] + $confTituloInadinplencia['acrescimos']['multaCalculado'],
					0
				);

				return $confTituloInadinplencia;
			}
		}

		return array();
	}
}
