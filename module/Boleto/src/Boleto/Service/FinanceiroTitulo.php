<?php
namespace Boleto\Service;

use Acesso\Entity\AcessoPessoas;
use VersaSpine\Service\AbstractService;

/**
 * Class FinanceiroTitulo
 * @package Boleto\Service
 */
class FinanceiroTitulo extends AbstractService
{
    const ESTADO_ABERTO = 'ABERTO';
    const ESTADO_PAGO = 'PAGO';
    const ESTADO_CANCELADO = 'CANCELADO';
    const ESTADO_ESTORNO = 'ESTORNO';
    const ESTADO_ALTERACAO = 'ALTERACAO';

    /**
     * @var null
     */
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Boleto\Entity\FinanceiroTitulo');
    }

    /**
     * @param $dados
     */
    protected function valida($dados)
    {
    }

    /**
     * @param $criterios
     */
    protected function pesquisaForJson($criterios)
    {
    }

    /**
     * Efetua baixa do título, caso seja do vestibular aceita a inscrição
     * @param int|null    $id
     * @param string|null $dataPagamento
     * @param float|null  $valorPago
     * @param string|null $tipoPagamento
     * @return bool|\Boleto\Entity\FinanceiroTitulo
     * @throws \Exception
     */
    public function baixa($id = null, $dataPagamento = null, $valorPago = null, $tipoPagamento = null)
    {
        if (!is_null($id)) {
            $titulo = $this->getReference($id);

            if ($titulo) {
                $usuarioBaixa = ($this->getRepository('Acesso\Entity\AcessoPessoas', $this->getEm())->findOneBy(
                    ['id' => $_SESSION['Zend_Auth']['storage']['id']]
                ));
                $titulo->setUsuarioBaixa($usuarioBaixa);
                $titulo->setTituloEstado("Pago");
                $titulo->setTituloDataPagamento((new \DateTime()));

                if (!is_null($tipoPagamento)) {
                    $titulo->setTituloTipoPagamento($tipoPagamento);
                }

                if ($dataPagamento != null) {
                    $titulo->setTituloDataPagamento((new \DateTime($dataPagamento)));
                }

                $titulo->setTituloValorPago($titulo->getTituloValor());

                if ($valorPago != null) {
                    $titulo->setTituloValorPago($valorPago);
                }

                $this->begin();

                try {
                    $this->getEm()->persist($titulo);
                    $this->getEm()->flush();
                } catch (\Exception $ex) {
                    throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
                }

                try {
                    $edicao = (new \Vestibular\Service\SelecaoInscricao($this->getEm()))->buscaEdicaoAtual();
                } catch (\Exception $ex) {
                    throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
                }

                try {
                    $candidato = (new \Vestibular\Service\SelecaoInscricao($this->getEm()))->buscaInscricao(
                        $titulo->getPes()->getPesId(),
                        $edicao->getEdicaoId()
                    );
                    //
                } catch (\Exception $ex) {
                    throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
                }

                try {
                    if ($candidato->getInscricaoStatus() == 'Solicitada') {
                        $candidato->setInscricaoStatus("Aceita");
                        $this->getEm()->persist($candidato);
                        $this->getEm()->flush($candidato);
                    }
                } catch (\Exception $ex) {
                    throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
                }

                $this->commit();

                return $titulo;
            }
        }

        return false;
    }
    /*
     * Quando entrar em vigor novo vestibular essa parte tera e ser alterado (levada para o vestibular, para os devidos tratamentos)
     */
    //    public function adicionar(array $dados)
    //    {
    //
    //        $dados['tituloDataVencimento'] = $this->formatDateAmericano($dados['tituloDataVencimento']);
    //        $dados['tituloDataVencimento'] = (new \DateTime($dados['tituloDataVencimento']));
    //        if($dados['tituloTipoPagamento'] == "Boleto"){
    //            $dados['tituloDataVencimento']->modify("+2 day");
    //            unset($dados['tituloDataPagamento']);
    //
    //        }
    //        $dados['tituloDataProcessamento']  = (new \DateTime());
    //        return parent::adicionar($dados);
    //
    //    }

    /**
     * Retorna dados para listagem
     * @param int        $page
     * @param bool|false $is_json
     * @param null       $post
     * @param null       $tipo
     * @return array
     */
    public function pagination($page = 0, $is_json = false, $post = null, $tipo = null)
    {
        $dados          = array();
        $dados['count'] = $this
            ->executeQuery("SELECT COUNT(*) as row  FROM {$this->getMetadados()->table['name']}")
            ->fetch()['row'];
        $sql            = "";

        if (!$page) {
            if ($is_json) {
                $limit = " LIMIT 10 OFFSET 0 ";
            } else {
                $limit = "";
            }
        } else {
            $offset = (($page - 1) * 10);
            $limit  = " LIMIT 10 OFFSET $offset ";
        }

        $JOIN   = "";
        $WHERE  = "";
        $SELECT = "";

        if (!is_null($tipo)) {
            $WHERE .= " WHERE financeiro__titulo_tipo.tipotitulo_nome = '{$tipo}'";
            if ($tipo == "mensalidade") {
                $SELECT .= "
                    , financeiro__titulo_mensalidade.alunoper_id matricula,
                    financeiro__titulo_mensalidade.mensalidade_parcela parcela
                ";

                $JOIN .= "
                    INNER JOIN financeiro__titulo_mensalidade ON financeiro__titulo_mensalidade.titulo_id =  financeiro__titulo.titulo_id
                ";
            }
            if ($tipo == "vestibular") {
                $SELECT .= "
                    , financeiro__titulo.titulo_id as financeiro_baixa,
                    boleto.bol_id as financeiro_boleto
                ";
            }
        }

        $sql = "
            SELECT
              financeiro__titulo.titulo_id,
              pessoa.pes_nome pes_nome,
              DATE_FORMAT(financeiro__titulo.titulo_data_processamento,'%d/%m/%Y') as titulo_data_processamento,
              DATE_FORMAT(financeiro__titulo.titulo_data_vencimento,'%d/%m/%Y') as titulo_data_vencimento,
              financeiro__titulo.titulo_tipo_pagamento,
              financeiro__titulo.titulo_valor,
              financeiro__titulo.titulo_estado,
              financeiro__titulo_tipo.tipotitulo_nome tipo
              $SELECT
            FROM
              financeiro__titulo
            LEFT JOIN
              boleto
            ON
              financeiro__titulo.titulo_id = boleto.titulo_id
            INNER JOIN
              pessoa
            ON
              financeiro__titulo.pes_id = pessoa.pes_id
            INNER JOIN
              financeiro__titulo_tipo
            ON
              financeiro__titulo.tipotitulo_id = financeiro__titulo_tipo.tipotitulo_id
            {$JOIN}
            {$WHERE}
        ";

        $sql .= " $limit ";
        $dados['dados'] = $this->executeQuery($sql);

        if ($is_json) {
            $dados['dados'] = $this->paginationFormatJson($dados['dados']);
        }

        return $dados;
    }

    /**
     * Efetua baixa manual de um título
     * @param array $dados
     * @return \Boleto\Entity\FinanceiroTitulo
     * @throws \Exception
     */
    public function baixaManual(array $dados)
    {
        $serviceFinanceiroCheque = new \Boleto\Service\FinanceiroCheque($this->getEm());
        $tituloDataVencimento    = $this->formatDateAmericano($dados['tituloDataVencimento']);
        $tituloDataProcessamento = $this->formatDateAmericano($dados['tituloDataProcessamento']);
        $tituloDesconto          = $dados['tituloDesconto'];
        $tituloValorPago         = $dados['tituloValorPago'];
        $tituloTipoPagamento     = $dados['tituloTipoPagamento'];

        if ($tituloDesconto == "") {
            $tituloDesconto = $dados['tituloValor'];
            $tituloDesconto -= ($dados['tituloDescontoCalculado'] - $dados['inadinplenciaValor']);
            $tituloDesconto = round($tituloDesconto, 0);
        }

        if ($tituloValorPago == "") {
            $tituloValorPago = $dados['tituloDescontoCalculado'];
        }

        $dados['tituloDataVencimento']    = (new \DateTime($tituloDataVencimento));
        $dados['tituloDataProcessamento'] = (new \DateTime($tituloDataProcessamento));
        $dados['tituloEstado']            = 'Pago';
        $dados['tituloDesconto']          = $tituloDesconto;
        $dados['tituloDescontoCalculado'] = $tituloValorPago;
        $dados['tituloDataPagamento']     = (new \DateTime('now', new \DateTimeZone('America/Sao_Paulo')));
        $dados['tituloTipoPagamento']     = "";

        $countCheque = 0;

        $this->begin();

        for ($i = 0; $i < sizeof($dados['valorFormaPagamento']); $i++) {
            $existe                   = false;
            $tituloTipoPagamentoArray = explode(",", $dados['tituloTipoPagamento']);

            foreach ($tituloTipoPagamentoArray as $tituloTipo) {
                if ($tituloTipo == $tituloTipoPagamento[$i]) {
                    $existe = true;
                    break;
                }
            }

            if ($existe == false) {
                if (!$dados['tituloTipoPagamento'] != "") {
                    $dados['tituloTipoPagamento'] .= "{$tituloTipoPagamento[$i]}";
                }

                $dados['tituloTipoPagamento'] .= ",{$tituloTipoPagamento[$i]}";
            }

            if ($tituloTipoPagamento[$i] == 'Dinheiro') {
                $dados['tituloValorPagoDinheiro'] = $dados['valorFormaPagamento'][$i];
            }

            if ($tituloTipoPagamento[$i] == 'Cheque') {
                $cheque = $serviceFinanceiroCheque->adicionar($this->formataCheque($dados, $countCheque));
                $countCheque++;
            }
        }

        $entityTitulo = parent::edita($dados);

        $this->commit();

        return $entityTitulo;
    }

    /**
     * Quita título
     * @param array $dados
     * @return null|\Boleto\Entity\FinanceiroTitulo
     * @throws \Doctrine\ORM\ORMException
     * @throws \Exception
     */
    public function quitaTitulo(array $dados)
    {
        $titulo = $this->getReference($dados['tituloId']);

        if ($titulo) {
            $titulo->setTituloEstado('Pago');

            $desconto = \Boleto\Service\Moeda::moedaToDec($dados['tituloDesconto']);
            $titulo->setTituloDesconto($desconto + $dados['descontoManual']);
            $titulo->setTituloAcrescimoManual(floatval($dados['acrescimoManual']));

            $usuarioBaixa = ($this->getRepository('Acesso\Entity\AcessoPessoas', $this->getEm())->findOneBy(
                ['id' => $_SESSION['Zend_Auth']['storage']['id']]
            ));
            if ($usuarioBaixa instanceof \Acesso\Entity\AcessoPessoas) {
                $titulo->setUsuarioBaixa($usuarioBaixa);
            }

            if (!empty($dados['descontoManual'])) {
                $titulo->setTituloDescontoManual($dados['descontoManual']);
            }

            if (!empty($dados['valorReceber'])) { // quando for mensalidade
                $titulo->setTituloValorPago($dados['valorReceber']);
            } else { // quando for uma taxa
                $titulo->setTituloValorPago($titulo->getTituloValor());
            }
            if (!empty($dados['tituloMulta'])) {
                $titulo->setTituloMulta($dados['tituloMulta']);
            }
            if (!empty($dados['tituloJuros'])) {
                $titulo->setTituloJuros($dados['tituloJuros']);
            }
            if (!empty($dados['tituloObservacoes'])) {
                $titulo->setTituloObservacoes($dados['tituloObservacoes']);
            }

            $titulo->setTituloDataPagamento((new \DateTime('now', new \DateTimeZone('America/Sao_Paulo'))));
            $tituloTipoPagamento          = $dados['tituloTipoPagamento'];
            $dados['tituloTipoPagamento'] = "";
            $countCheque                  = 0;

            $this->begin();

            for ($i = 0; $i < sizeof($dados['valorFormaPagamento']); $i++) {
                $existe                   = false;
                $tituloTipoPagamentoArray = $tituloTipoPagamento;

                foreach ($tituloTipoPagamentoArray as $tituloTipo) {
                    if ($tituloTipo === $dados['tituloTipoPagamento'][$i]) {
                        $existe = true;
                        break;
                    }
                }

                if ($existe == false) {
                    if ($dados['tituloTipoPagamento'] == "") {
                        $dados['tituloTipoPagamento'] .= "{$tituloTipoPagamento[$i]}";
                    } else {
                        $dados['tituloTipoPagamento'] .= ",{$tituloTipoPagamento[$i]}";
                    }
                }

                if ($tituloTipoPagamento[$i] == 'Dinheiro') {
                    $titulo->setTituloValorPagoDinheiro($dados['valorFormaPagamento'][$i]);
                }

                if ($tituloTipoPagamento[$i] == 'Cheque') {
                    if (!empty($dados['chequeId'][$countCheque])) {
                        $chequesResult = $this->getEm()->getReference(
                            'Boleto\Entity\FinanceiroCheque',
                            $dados['chequeId'][$countCheque]
                        );

                        if ($chequesResult) {
                            $cheques[] = $chequesResult;
                        }
                    } else {
                        $cheques[] = (new \Boleto\Service\FinanceiroCheque($this->getEm()))->adicionar(
                            $this->formataCheque($dados, $countCheque)
                        );
                    }

                    $countCheque++;
                }
            }

            $titulo->setTituloTipoPagamento($dados['tituloTipoPagamento']);

            try {
                $this->getEm()->persist($titulo);
                $this->getEm()->flush();
            } catch (\Exception $ex) {
                throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
            }

            if ($cheques) {
                $sTituloCheque = new \Boleto\Service\FinanceiroTituloCheque($this->getEm());

                $sTituloCheque->vinculaChequesTitulo($titulo, $cheques);
            }

            $tituloMensalidade = $this->getRepository('Boleto\Entity\FinanceiroTituloMensalidade')->findOneBy(
                ['titulo' => $titulo->getTituloId()]
            );

            if ($tituloMensalidade) {
                if ($tituloMensalidade->getMensalidadeParcela() == 1 AND $titulo->getTipotitulo()->getTipotituloNome(
                    ) == 'Mensalidade'
                ) {
                    if (!empty($dados['alunoper'])) {
                        (new \Matricula\Service\AcadperiodoAluno($this->getEm()))->ativaMatriculaAluno(
                            $tituloMensalidade->getAlunoper()->getAlunoperId()
                        );
                    }
                }
            }

            $this->commit();

            return $titulo;
        }

        return null;
    }

    /**
     * Efetua cancelamento de pagamento de um título
     * @param array     $dados
     * @param string    $estado
     * @param bool|true $gerarEstorno
     * @return \Boleto\Entity\FinanceiroTitulo|bool|null
     * @throws \Exception
     */
    public function cancelaPagamentoTitulo(array $dados, $estado = 'Cancelado', $gerarEstorno = true)
    {
        $this->begin();

        $varDesconto                      = 0;
        $serviceBibliotecaMulta           = new \Biblioteca\Service\Multa($this->getEm());
        $serviceTituloMensalidade         = new \Boleto\Service\FinanceiroTituloMensalidade($this->getEm());
        $serviceTituloMensalidadeDesconto = new \Boleto\Service\FinanceiroMensalidadeDesconto($this->getEm());

        $tituloChequeRepository = $this->getRepository('Boleto\Entity\FinanceiroTituloCheque');
        $titulo                 = $this->getReference($dados['tituloId']);
        $tituloNovo             = false;

        if ($titulo) {
            $session      = new \Zend\Authentication\Storage\Session();
            $arrUsuario   = $session->read();
            $usuarioBaixa = $this
                ->getRepository('Acesso\Entity\AcessoPessoas', $this->getEm())
                ->findOneBy(['usuario' => $arrUsuario['usuario']]);

            if ($titulo->getTipoTitulo()->getTipoTituloNome() == 'Mensalidade') {
                $tituloMensalidade = $serviceTituloMensalidade->findOneBy(['titulo' => $titulo]);

                $mensalidadeDesconto = $serviceTituloMensalidadeDesconto->findOneBy(
                    [
                        'financeiroTituloMensalidade' => $tituloMensalidade->getFinanceiroTituloMensalidadeId()
                    ]
                );

                if ($mensalidadeDesconto && ($mensalidadeDesconto->getDescMensalidadeStatus() === 'Ativo')) {
                    $varDesconto = $titulo->getTituloDesconto() - $titulo->getTituloDescontoManual();
                }
            }

            if ($gerarEstorno) {
                $tituloDataVencimento = $this->formatDateAmericano($titulo->getTituloDataVencimento());
                $tituloNovo           = array(
                    'pes'                     => $titulo->getPes(),
                    'tituloDataVencimento'    => new \DateTime($tituloDataVencimento),
                    'tituloDataProcessamento' => new \DateTime('now', new \DateTimeZone('America/Sao_Paulo')),
                    'tipotitulo'              => $titulo->getTipotitulo(),
                    'usuarioAutor'            => $usuarioBaixa,
                    'tituloTipoPagamento'     => null,
                    'tituloEstado'            => 'Aberto',
                    'tituloValor'             => $titulo->getTituloValor(),
                    'tituloDesconto'          => $varDesconto,
                    'tituloObservacoes'       => $titulo->getTituloObservacoes()
                );

                $tituloNovo = new \Boleto\Entity\FinanceiroTitulo($tituloNovo);

                try {
                    $this->getEm()->persist($tituloNovo);
                    $this->getEm()->flush($tituloNovo);
                } catch (\Exception $e) {
                    throw new \Exception($e->getMessage());
                }

                $titulo->setTituloNovo($tituloNovo->getTituloId());
                $titulo->setTituloNovo($tituloNovo);

                if ($titulo->getTipoTitulo()->getTipoTituloId() == FinanceiroTituloTipo::MULTA_BIBLIOTECA) {
                    $serviceBibliotecaMulta->atualizaTituloMulta($titulo, $tituloNovo);
                }

                if ($tituloMensalidade) {
                    $tituloMensalidade = $serviceTituloMensalidade->findOneBy(['titulo' => $titulo]);

                    if ($tituloMensalidade) {
                        $tituloMensalidadeNovo['titulo']             = $tituloNovo;
                        $tituloMensalidadeNovo['mensalidadeParcela'] = $tituloMensalidade->getMensalidadeParcela();
                        $tituloMensalidadeNovo['alunoper']           = $tituloMensalidade->getAlunoper();
                        $tituloMensalidadeNovo['tituloconf']         = $tituloMensalidade->getTituloconf();

                        $tituloMensalidadeNovo = new \Boleto\Entity\FinanceiroTituloMensalidade($tituloMensalidadeNovo);

                        try {
                            $this->getEm()->persist($tituloMensalidadeNovo);
                            $this->getEm()->flush();
                        } catch (\Exception $e) {
                            throw new \Exception($e->getMessage());
                        }

                        if ($mensalidadeDesconto) {
                            $mensalidadeDescontoNovo = [
                                'descmensalidadeStatus'       => $mensalidadeDesconto->getDescmensalidadeStatus(),
                                'desconto'                    => $mensalidadeDesconto->getDesconto(),
                                'financeiroTituloMensalidade' => $tituloMensalidadeNovo
                            ];

                            $mensalidadeDescontoNovo = new \Boleto\Entity\FinanceiroMensalidadeDesconto(
                                $mensalidadeDescontoNovo
                            );

                            try {
                                $this->getEm()->persist($mensalidadeDescontoNovo);
                                $this->getEm()->flush();
                            } catch (\Exception $e) {
                                throw new \Exception($e->getMessage());
                            }
                        }
                    }
                }
            }

            $tituloCheques = $tituloChequeRepository->findBy(['titulo' => $titulo->getTituloId()]);

            foreach ($tituloCheques as $tituloCheque) {
                $tituloCheque->setTitulochequeEstado('Inativo');

                try {
                    $this->getEm()->persist($tituloCheque);
                } catch (\Exception $e) {
                    throw new \Exception($e->getMessage());
                }
            }

            if (!empty($dados['addObs'])) {
                $strObs = trim($titulo->getTituloObservacoes());
                $strObs .= "\n" . trim($dados['addObs']);

                $titulo->setTituloObservacoes(trim($strObs));
            }

            $titulo->setTituloEstado($estado);

            try {
                $this->getEm()->persist($titulo);
            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }

            $this->getEm()->flush();
            $this->commit();

            return $tituloNovo ? $tituloNovo : $titulo;
        }

        return null;
    }

    /**
     * Efetua cancelamento de um título
     * @param array  $dados
     * @param string $estado
     * @param string $origem
     * @return null|\Boleto\Entity\FinanceiroTitulo
     * @throws \Exception
     */
    public function cancelaTitulo(array $dados, $estado = 'Cancelado', $origem = 'financeiro')
    {
        $serviceTituloMensalidade         = new \Boleto\Service\FinanceiroTituloMensalidade($this->getEm());
        $serviceTituloMensalidadeDesconto = new \Boleto\Service\FinanceiroMensalidadeDesconto($this->getEm());

        $titulo = $this->getReference($dados['tituloId']);

        if ($titulo->getTipotitulo()->getTipotituloNome() == 'Multa de biblioteca' && $origem != 'biblioteca') {
            return null;
        }

        if ($titulo) {
            $session      = new \Zend\Authentication\Storage\Session();
            $arrUsuario   = $session->read();
            $usuarioBaixa = $this
                ->getRepository('Acesso\Entity\AcessoPessoas', $this->getEm())
                ->findOneBy(['usuario' => $arrUsuario['usuario']]);

            $titulo->setUsuarioBaixa($usuarioBaixa);
            $titulo->setTituloDataPagamento((new \DateTime('now', new \DateTimeZone('America/Sao_Paulo'))));

            if (!empty($dados['addObs'])) {
                $strObs = trim($titulo->getTituloObservacoes());
                $strObs .= "\n" . trim($dados['addObs']);

                $titulo->setTituloObservacoes(trim($strObs));
            }

            $titulo->setTituloEstado($estado);

            try {
                $this->getEm()->persist($titulo);
                $this->getEm()->flush();
            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }

            if ($titulo->getTipoTitulo()->getTipoTituloNome() == 'Mensalidade') {
                $tituloMensalidade = $serviceTituloMensalidade->findOneBy(['titulo' => $titulo]);

                if ($tituloMensalidade) {
                    $mensalidadeDesconto = $serviceTituloMensalidadeDesconto->findOneBy(
                        [
                            'financeiroTituloMensalidade' => $tituloMensalidade->getFinanceiroTituloMensalidadeId()
                        ]
                    );

                    if ($mensalidadeDesconto) {
                        $mensalidadeDesconto->setDescmensalidadeStatus('Inativo');

                        try {
                            $this->getEm()->persist($mensalidadeDesconto);
                            $this->getEm()->flush();
                        } catch (\Exception $e) {
                            throw new \Exception($e->getMessage());
                        }
                    }
                }
            }

            return $titulo;
        }

        return null;
    }

    /**
     * Cria registro de taxa
     * @param array $dados
     * @return \Boleto\Entity\FinanceiroTitulo
     * @throws \Exception
     */
    public function adicionaTaxa(array $dados)
    {
        $dados['tituloDataVencimento']    = (new \DateTime($this->formatDateAmericano($dados['tituloDataVencimento'])));
        $dados['tituloDataProcessamento'] = (new \DateTime($dados['tituloDataProcessamento']));
        $dados['tituloEstado']            = 'Pago';
        $dados['tituloValorPago']         = $dados['titulo_valor'];
        $dados['tituloDataPagamento']     = (new \DateTime('now', new \DateTimeZone('America/Sao_Paulo')));
        $tituloTipoPagamento              = $dados['tituloTipoPagamento'];
        $dados['tituloTipoPagamento']     = "";
        $countCheque                      = 0;

        for ($i = 0; $i < sizeof($dados['valorFormaPagamento']); $i++) {
            $existe                   = false;
            $tituloTipoPagamentoArray = explode(",", $dados['tituloTipoPagamento']);

            foreach ($tituloTipoPagamentoArray as $tituloTipo) {
                if ($tituloTipo == $tituloTipoPagamento[$i]) {
                    $existe = true;
                    break;
                }
            }

            if ($existe == false) {
                if (!$dados['tituloTipoPagamento'] != "") {
                    $dados['tituloTipoPagamento'] .= "{$tituloTipoPagamento[$i]}";
                }
                $dados['tituloTipoPagamento'] .= ",{$tituloTipoPagamento[$i]}";
            }

            if ($tituloTipoPagamento[$i] == 'Dinheiro') {
                $dados['tituloValorPagoDinheiro'] = $dados['valorFormaPagamento'][$i];
            }
        }

        $titulo            = parent::adicionar($dados);
        $dados['tituloId'] = $titulo->getTituloId();

        for ($i = 0; $i < sizeof($tituloTipoPagamento[$i]); $i++) {
            if ($tituloTipoPagamento[$i] == 'Cheque') {
                $cheque = (new \Boleto\Service\FinanceiroCheque($this->getEm()))
                    ->adicionar($this->formataCheque($dados, $countCheque));
                $countCheque++;
            }
        }

        return $titulo;
    }

    /**
     * Cria título avulso
     * @param $arrData
     * @return array|bool
     * @throws \Exception
     */
    public function criarTitulo($arrData)
    {
        if (!$arrData['periodoLetivo']) {
            $this->setLastError('Não é possível cadastrar título sem especificar o período letivo!');

            return false;
        } elseif (!$arrData['alunoperId']) {
            $this->setLastError('Não é possível cadastrar título sem especificar o aluno!');

            return false;
        } elseif (!$arrData['tipo']) {
            $this->setLastError('Não é possível cadastrar título sem especificar o tipo!');

            return false;
        } elseif (!$arrData['valor']) {
            $this->setLastError('Não é possível cadastrar título sem especificar o valor!');

            return false;
        } elseif (!$arrData['mesParcela']) {
            $this->setLastError('Não é possível cadastrar título sem especificar a parcela!');

            return false;
        } elseif (!$arrData['vencimento']) {
            $this->setLastError('Não é possível cadastrar título sem especificar o vencimento!');

            return false;
        } elseif (!trim($arrData['observacao'])) {
            $this->setLastError('Não é possível cadastrar título sem especificar uma observação!');

            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            $serviceTituloConfig      = new \Boleto\Service\FinanceiroTituloConfig($this->getEm());
            $serviceTituloTipo        = new \Boleto\Service\FinanceiroTituloTipo($this->getEm());
            $serviceTituloMensalidade = new \Boleto\Service\FinanceiroTituloMensalidade($this->getEm());
            $servicePeriodoLetivo     = new \Matricula\Service\AcadperiodoLetivo($this->getEm());
            $servicePeriodoAluno      = new \Matricula\Service\AcadperiodoAluno($this->getEm());
            $serviceAcessoPessoas     = new \Acesso\Service\AcessoPessoas($this->getEm());

            /* @var $objPeridoLetivo \Matricula\Entity\AcadperiodoLetivo */
            $objPeridoLetivo = $servicePeriodoLetivo->getRepository()->find($arrData['periodoLetivo']);

            if (!$objPeridoLetivo) {
                $this->setLastError('Período letivo não encontrado!');

                return false;
            }

            /* @var $objPeriodoAluno \Matricula\Entity\AcadperiodoAluno */
            $objPeriodoAluno = $servicePeriodoAluno->getRepository()->find($arrData['alunoperId']);

            if (!$objPeriodoAluno) {
                $this->setLastError('Aluno não encontrado!');

                return false;
            }

            /* @var $objTituloTipo \Boleto\Entity\FinanceiroTituloTipo */
            $objTituloTipo = $serviceTituloTipo->getRepository()->find($arrData['tipo']);

            if (!$objTituloTipo) {
                $this->setLastError('Tipo de título não encontrado!');

                return false;
            }

            $arrTiposPermitidos = array('Adaptação', 'Dependência', 'Monografia', 'Mensalidade');

            if (!in_array($objTituloTipo->getTipotituloNome(), $arrTiposPermitidos)) {
                $this->setLastError('Tipo de título não permitido!');

                return false;
            }

            $objTituloConf = $serviceTituloConfig->configuracaoPeriodo($objPeridoLetivo->getPerId());

            if (!$objTituloConf) {
                $this->setLastError('Configuração de título não encontrada!');

                return false;
            }

            $session    = new \Zend\Authentication\Storage\Session();
            $arrUsuario = $session->read();
            /* @var $usuarioAutor \Acesso\Entity\AcessoPessoas */
            $usuarioAutor = $serviceAcessoPessoas->getRepository()->findOneBy(['usuario' => $arrUsuario['usuario']]);

            $titulo = array(
                'tituloDataProcessamento' => (new \DateTime()),
                'tituloDataVencimento'    => (new \DateTime($this->formatDateAmericano($arrData['vencimento']))),
                'tituloTipoPagamento'     => 'Dinheiro',
                'tipotitulo'              => $objTituloTipo->getTipotituloId(),
                'tituloValor'             => str_replace(array('.', ','), array('', '.'), $arrData['valor']),
                'tituloEstado'            => 'Aberto',
                'tituloNovo'              => null,
                'usuarioBaixa'            => null,
                'usuarioAutor'            => $usuarioAutor->getId(),
                'pes'                     => $objPeriodoAluno->getAlunocurso()->getAluno()->getPes()->getPes(
                )->getPesId(),
                'cursocampus'             => $objPeriodoAluno->getAlunocurso()->getCursocampus()->getCursocampusId(),
                'per'                     => $objPeridoLetivo->getPerId(),
                'tituloObservacoes'       => $arrData['observacao']
            );

            /* @var $objTitulo \Boleto\Entity\FinanceiroTitulo */
            $objTitulo = $this->adicionar($titulo);

            $tituloMensalidade = array(
                'mensalidadeParcela' => ($arrData['mesParcela'] > 6 ? $arrData['mesParcela'] - 6 : $arrData['mesParcela']),
                'titulo'             => $objTitulo->getTituloId(),
                'alunoper'           => $objPeriodoAluno->getAlunoperId(),
                'tituloconf'         => $objTituloConf->getTituloconfId()
            );

            /* @var $objTituloMensalidade \Boleto\Entity\FinanceiroTituloMensalidade */
            $objTituloMensalidade = $serviceTituloMensalidade->adicionar($tituloMensalidade);

            $arrTitulo = array_merge(
                $objTitulo->toArray(),
                $serviceTituloMensalidade->calculaAcrecimos($objTituloMensalidade)
            );

            $tituloDesconto   = ($arrTitulo['tituloDesconto']) ? $arrTitulo['tituloDesconto'] : 0;
            $tituloAcrescimos = ($arrTitulo['acrescimos']) ? $arrTitulo['acrescimos']['valorAcrescimos'] : 0;

            $arrTitulo['valorReceber'] = $arrTitulo['tituloValor'] - $tituloDesconto + $tituloAcrescimos;

            $this->getEm()->commit();

            return [
                'titulo'      => $objTitulo,
                'arrTitulo'   => $arrTitulo,
                'mensalidade' => $objTituloMensalidade
            ];
        } catch (\Exception $ex) {
            $this->setLastError('Ouve uma falha ao cadastrar título. Entre em contato com o suporte.');
        }

        return false;
    }

    /**
     * Cria taxa
     * @param array $dados
     * @return \Boleto\Entity\FinanceiroTitulo
     * @throws \Exception
     */
    public function criaTaxa(array $dados)
    {
        $session    = new \Zend\Authentication\Storage\Session();
        $arrUsuario = $session->read();

        $tituloDataVencimento = $this->formatDateAmericano($dados['tituloDataVencimento']);

        $dados = array_merge(
            $dados,
            [
                'tituloDataVencimento'    => new \DateTime($tituloDataVencimento),
                'tituloDataProcessamento' => new \DateTime($dados['tituloDataProcessamento']),
                'tituloEstado'            => 'Aberto',
                'tituloTipoPagamento'     => 'Dinheiro',
                'tituloNovo'              => null,
                'usuarioAutor'            => $arrUsuario['id'],
                'usuarioBaixa'            => null
            ]
        );

        $titulo = parent::adicionar($dados);

        return $titulo;
    }

    /**
     * Formata infomrações de cheque
     * @param array $dados
     * @param       $i
     * @return array
     */
    public function formataCheque(array $dados, $i)
    {
        $cheque['chequeNum']        = $dados['chequeNum'][$i];
        $cheque['chequeEmitente']   = $dados['chequeEmitente'][$i];
        $cheque['chequeBanco']      = $dados['chequeBanco'][$i];
        $cheque['chequeAgencia']    = $dados['chequeAgencia'][$i];
        $cheque['chequePraca']      = $dados['chequePraca'][$i];
        $cheque['chequeEmissao']    = (new \DateTime($this->formatDateAmericano($dados['chequeEmissao'][$i])));
        $cheque['chequeVencimento'] = (new \DateTime($this->formatDateAmericano($dados['chequeVencimento'][$i])));
        $cheque['chequeValor']      = number_format($dados['chequeValor'][$i], 2, '.', '');
        $cheque['chequeCompensado'] = (new \DateTime($this->formatDateAmericano($dados['chequeCompensado'][$i])));
        $cheque['chequeConta']      = $dados['chequeConta'][$i];

        return $cheque;
    }

    /**
     * Edita dados de um título
     * @param array $dados
     * @return \Boleto\Entity\FinanceiroTitulo
     * @throws \Exception
     */
    public function edita(array $dados)
    {
        $dados['tituloDataVencimento']    = new \DateTime(
            $this->formatDateAmericano($dados['tituloDataVencimento'])
        );
        $dados['tituloDataProcessamento'] = new \DateTime(
            $this->formatDateAmericano($dados['tituloDataProcessamento'])
        );
        $dados['tituloDataPagamento']     = null;

        if ($dados['usuarioBaixa'] instanceof AcessoPessoas) {
            $dados['usuarioBaixa'] = $dados['usuarioBaixa']->getId();
        }

        if ($dados['usuarioAutor'] instanceof AcessoPessoas) {
            $dados['usuarioAutor'] = $dados['usuarioAutor']->getId();
        }

        return parent::edita($dados);
    }

    /**
     * Altera data de vencimento de um título
     * @param array $dados
     * @return \Boleto\Entity\FinanceiroTitulo
     * @throws \Exception
     */
    public function alteraDataVencimento(array $dados)
    {
        $boleto = (new \Boleto\Service\Boleto($this->getEm()))->buscaBoleto($dados['idBoleto']);

        if ($boleto) {
            $titulo = $this->getReference($boleto['titulo_id']);
            $titulo->setTituloDataVencimento((new \DateTime())->modify("+" . $dados['dias'] . " day"));

            $this->begin();

            try {
                $this->getEm()->persist($titulo);
                $this->getEm()->flush();
            } catch (\Exception $ex) {
                throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
            }

            $this->commit();

            return $titulo;
        }
    }

    /**
     * Retorna dados para listagem de títulos do vestibular usando datatables
     * @param array $data
     * @return array
     */
    public function resultSearch(array $data)
    {
        $edicaoAtual = (new \Vestibular\Service\SelecaoInscricao($this->getEm()))->buscaEdicaoAtual();

        $query  = <<<SQL
            SELECT
                financeiro__titulo.titulo_id,
                pessoa.pes_nome pes_nome,
                DATE_FORMAT(financeiro__titulo.titulo_data_processamento,'%d/%m/%Y') as data_processamento,
                DATE_FORMAT(financeiro__titulo.titulo_data_vencimento,'%d/%m/%Y') as data_vencimento,
                financeiro__titulo.titulo_tipo_pagamento tipo_pagamento,
                financeiro__titulo.titulo_valor,
                financeiro__titulo.titulo_estado,
                financeiro__titulo.titulo_id as titulo,
                financeiro__titulo_tipo.tipotitulo_nome tipo
            FROM
                financeiro__titulo
            LEFT JOIN
                boleto
            ON
                financeiro__titulo.titulo_id = boleto.titulo_id
            LEFT JOIN
                pessoa
            ON
                financeiro__titulo.pes_id = pessoa.pes_id
            INNER JOIN
                financeiro__titulo_tipo
            ON
                financeiro__titulo.tipotitulo_id = financeiro__titulo_tipo.tipotitulo_id
            INNER JOIN
                selecao_inscricao
            ON
                selecao_inscricao.pes_id = pessoa.pes_id
            INNER JOIN
                selecao_edicao
            ON
                selecao_edicao.edicao_id = selecao_inscricao.edicao_id
            WHERE
                financeiro__titulo_tipo.tipotitulo_nome = 'vestibular' AND
                selecao_edicao.edicao_id = {$edicaoAtual->getedicaoId()} AND
                titulo_data_processamento >= edicao_inicio_inscricao AND
                titulo_estado = 'aberto'
SQL;
        $result = $this->paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * Busca títulos de uma pessoa
     * @param int         $pesId
     * @param string|null $tituloTipo
     * @param string|null $pagamento
     * @return array
     */
    public function buscaTituloPessoa($pesId, $tituloTipo = null, $pagamento = null, $dataProcessamento = false)
    {
        $respository = $this->getRepository($this->getEntity());
        $param       = ['pes' => $pesId];

        if ($tituloTipo) {
            $tituloTipo = $this->getRepository('Boleto\Entity\FinanceiroTituloTipo')
                ->findOneBy(['tipotituloNome' => $tituloTipo]);

            $param['tipotitulo'] = $tituloTipo->getTipotituloId();
        }

        if ($pagamento) {
            $param['tituloTipoPagamento'] = $pagamento;
        }

        if (!$dataProcessamento || is_string($dataProcessamento)) {
            $param['tituloDataProcessamento'] = new \DateTime($dataProcessamento);
        }

        $titulosPessoa = $respository->findBy($param);

        return $titulosPessoa;
    }

    /**
     * Retora dados paginados de acordo comparâmetros do _GET
     * @return mixed
     */
    public function paginationAction()
    {
        $service = $this->services()->getService($this->getService());
        $post    = $this->getRequest()->getPost()->toArray();

        if (count($post) > 0) {
            $dados = $service->pagination($post['page'], true);
        } else {
            $dados = $service->pagination(array(), true);
        }

        return $this->json->setVariable("aaData", $dados['dados']);
    }

    /**
     * Retorna valor total de títulos para relatório
     * @param string   $dataIni d/m/Y
     * @param string   $dataFim d/m/Y
     * @param int|null $usuId
     * @param null     $servico
     * @param null     $formaPagamento
     * @return mixed
     */
    public function relatorioFinanceiroPeriodo(
        $dataIni,
        $dataFim,
        $usuId = null,
        $servico = null,
        $formaPagamento = null
    ) {
        $SELECT = "SUM(titulo_valor_pago) total";
        $WHERE  = "";
        $INNER  = "";

        if ($usuId !== 'todos' && $usuId !== null) {
            $WHERE .= " AND usuario_baixa = {$usuId}";
        }

        if ($servico !== 'todos' && $usuId !== null) {
            $WHERE .= " AND tipotitulo_id = {$servico}";
        }

        if (!empty($formaPagamento)) {
            foreach ($formaPagamento as $key => $forma) {
                if ($key > 0) {
                    $WHERE .= " OR titulo_tipo_pagamento = '{$forma}' ";
                } else {
                    $WHERE .= " AND ( titulo_tipo_pagamento = '{$forma}' ";
                }

                if ($forma == "Cheque") {
                    $INNER .= " LEFT JOIN financeiro__cheque ON financeiro__cheque.titulo_id = financeiro__titulo.titulo_id";
                }

                if ($forma == "Boleto") {
                    $INNER .= " LEFT JOIN boleto ON boleto.titulo_id = financeiro__titulo.titulo_id";
                }
            }

            $formasConcat = "";

            if (in_array("Dinheiro", $formaPagamento)) {
                $formasConcat = "if(titulo_valor_pago_dinheiro is null, 0, titulo_valor_pago_dinheiro)";
            }

            if (in_array("Cheque", $formaPagamento)) {
                if ($formasConcat === "") {
                    $formasConcat = "cheque_valor";
                } else {
                    $formasConcat .= " + if(cheque_valor is null, 0, cheque_valor)";
                }
            }

            $SELECT = "SUM(" . $formasConcat . ") as total";
            $formas = implode(',', array_values($formaPagamento));

            $WHERE .= " OR titulo_tipo_pagamento LIKE '%{$formas}%' ";
            $WHERE .= ' )';
        }

        $query = "
            SELECT
              {$SELECT}
            FROM
              financeiro__titulo
            {$INNER}
            WHERE
              DATE(titulo_data_pagamento) BETWEEN DATE('{$this->formatDateAmericano($dataIni)}')
            AND
              DATE('{$this->formatDateAmericano($dataFim)}')
            {$WHERE}
        ";

        $relatorio = $this->executeQuery($query)->fetchAll();

        return $relatorio[0]['total'];
    }

    /**
     * Retorna títulos emitidos dentro de um período
     * @param string   $dataIni d/m/Y
     * @param string   $dataFim d/m/Y
     * @param int|null $usuId
     * @param null     $servico
     * @param null     $formaPagamento
     * @return mixed
     */
    public function buscaTitulosPeriodo($dataIni, $dataFim, $usuId = null, $servico = null, $formaPagamento = null)
    {
        $SELECT = " titulo_valor_pago";
        $WHERE  = "";
        $INNER  = "";

        if ($usuId !== 'todos' && $usuId !== null) {
            $WHERE .= " AND usuario_baixa = {$usuId}";
        }

        if ($servico !== 'todos' && $servico !== null) {
            $WHERE .= " AND tipotitulo_id = {$servico}";
        }

        //        if ( ! empty($formaPagamento)) {
        //            foreach($formaPagamento as $key => $forma) {
        //                if($key > 0){
        //                    $WHERE .= " OR titulo_tipo_pagamento = '{$forma}' ";
        //                } else {
        //                    $WHERE .= " AND ( titulo_tipo_pagamento = '{$forma}' ";
        //                }
        //                if ( $forma == "Cheque" ) {
        //                    $SELECT = "  titulo_valor_pago as titulo_valor_pago";
        //                    $INNER .= " LEFT JOIN financeiro__cheque ON financeiro__cheque.titulo_id = financeiro__titulo.titulo_id";
        //                }
        //                if ( $forma == "Boleto" ) {
        //                    $INNER .= " LEFT JOIN boleto ON boleto.titulo_id = financeiro__titulo.titulo_id";
        //                }
        //            }
        //            for($i=0;$i<count($formaPagamento);$i++){
        //                if($formaPagamento[$i] === 'Boleto'){
        //                    unset($formaPagamento[$i]);
        //                }
        //            }
        //            if(count($formaPagamento) > 1){
        //                $formas = implode(',',array_values($formaPagamento));
        //                $WHERE .= " OR titulo_tipo_pagamento LIKE '%{$formas}%' ";
        //                $WHERE .= ' )';
        //            }else{
        //                if($formaPagamento[0] === 'Cheque'){
        //                    $SELECT = " if(cheque_valor IS NULL, titulo_valor_pago, cheque_valor )  as titulo_valor_pago";
        //                    $WHERE .= " OR titulo_tipo_pagamento LIKE '%{$formaPagamento[0]}%' ";
        //                    $WHERE .= ' )';
        //                }else{
        //                    $WHERE .= " OR titulo_tipo_pagamento LIKE '%{$formaPagamento[0]}%' ";
        //                    $WHERE .= ' )';
        //                }
        //            }
        //        }

        $query = "
          SELECT
            alunocurso_id,
            pes_nome,
            titulo_data_processamento,
            titulo_data_pagamento,
            titulo_valor_pago,
            titulo_valor_pago_dinheiro,
            titulo_tipo_pagamento
          FROM
            pessoa
          NATURAL JOIN
            acadgeral__aluno
          NATURAL JOIN
            acadgeral__aluno_curso
          NATURAL JOIN
            financeiro__titulo
          NATURAL JOIN
            financeiro__titulo_tipo
          NATURAL JOIN
            financeiro__titulo_mensalidade

          WHERE
            date(titulo_data_pagamento) BETWEEN Date('{$this->formatDateAmericano($dataIni)}')
          AND
            Date('{$this->formatDateAmericano($dataFim)}')
          AND titulo_valor_pago != 0
          {$WHERE}
          ORDER BY titulo_data_processamento
        ";

        return $this->executeQuery($query)->fetchAll();
    }

    /**
     * Retorna informações sobre formas de pagamento e valores de títulos
     * @param $tituloTipo
     * @param $tiposPagamento
     * @return bool|array
     */
    public function buscaInformacoesFormasDePagamento($tituloTipo, $tiposPagamento)
    {
        if (!(empty($tituloTipo) && empty($tiposPagamento))) {
            $tiposPagamento = explode(",", $tiposPagamento);
            $INNER          = "";

            foreach ($tiposPagamento as $tipo) {
                if ($tipo == "Cheque") {
                    $INNER = " NATURUAL JOIN financeiro__cheque";
                }

                if ($tipo == "Boleto") {
                    $INNER = " INNER JOIN boleto ON boleto.titulo_id = financeiro__titulo.titulo_id";
                }
            }

            $query = "
              SELECT
                financeiro__titulo.titulo_id,
                financeiro__titulo.titulo_tipo_pagamento,
                financeiro__titulo.titulo_valor
              FROM
                financeiro__titulo
              $INNER
              WHERE
                financeiro__titulo.titulo_id = $tituloTipo
            ";

            $result = $this->executeQuery($query);

            if ($result->rowCount() > 0) {
                $result = $result->fetchAll();

                for ($i = 0; $i < sizeof($result); $i++) {
                    $result[$i]['titulo_valor'] = \Boleto\Service\Moeda::decToMoeda($result[$i]['titulo_valor']);
                }

                return $result;
            }
        }

        return false;
    }

    /** Retorna informações de desconto de um título
     * @param int $tituloId
     * @return bool|array
     */
    public function buscaInformacoesDescontoTitulo($tituloId)
    {
        if (!empty($tituloId)) {
            $query = "
              SELECT
                desctipo_descricao as descricao,
                mensalidade_parcela as parcela ,
                desconto_dia_limite as dia_vencimento_desconto,
                desconto_valor as valor
              FROM
                financeiro__titulo
              NATURAL JOIN
                financeiro__titulo_mensalidade
              NATURAL JOIN
                financeiro__titulo_config
              LEFT JOIN
                financeiro__mensalidade_desconto
              ON
                financeiro__titulo_mensalidade.financeiro_titulo_mensalidade_id = financeiro__mensalidade_desconto.financeiro_titulo_mensalidade_id
              AND
                financeiro__mensalidade_desconto.descmensalidade_status = 'Ativo'
              INNER JOIN
                financeiro__desconto ON financeiro__mensalidade_desconto.desconto_id = financeiro__desconto.desconto_id
              NATURAL JOIN
                financeiro__desconto_tipo
              WHERE
                financeiro__titulo.titulo_id = {$tituloId}
            ";

            $result = $this->executeQuery($query);

            if ($result->rowCount() > 0) {
                return $result = $result->fetchAll();
            }

            return false;
        }

        return false;
    }

    /**
     * Busca informações sobre vencimento e valores de taxas
     * @return bool|array
     */
    public function buscaTaxas()
    {
        $query = "
        SELECT tipotitulo_id, tipotitulo_nome, valores_preco
        FROM financeiro__titulo_tipo
        NATURAL JOIN financeiro__valores
        WHERE tipotitulo_nome NOT IN ('Adaptação', 'Dependência', 'Monografia','Mensalidade')";

        $result = $this->executeQuery($query);

        if ($result->rowCount() > 0) {
            return $result->fetchAll();
        }

        return false;
    }

    /**
     * Retorna informações sobre data e valor de
     * "mensalidades" (Adaptação, Dependência, Monografia,Mensalidade) do aluno
     * @return array
     */
    public function buscaTiposMensalidade()
    {
        $query = "
        SELECT * FROM(
			SELECT *
			FROM financeiro__titulo_tipo
			NATURAL JOIN financeiro__valores
			WHERE tipotitulo_nome IN ('Adaptação', 'Dependência', 'Monografia','Mensalidade')
			ORDER BY tipotitulo_id ASC, per_id DESC
        ) AS tipos
        GROUP BY tipotitulo_id";

        $result = $this->executeQuery($query);

        if ($result->rowCount() > 0) {
            return $result->fetchAll();
        }

        return array();
    }

    /**
     * Verifica se usuário existe
     * @param int $user
     * @return bool
     */
    public function buscaUsuario($user)
    {
        $query  = ("
            SELECT
              *
            FROM
              acesso_pessoas
            INNER JOIN
              pessoa_fisica
            ON
              acesso_pessoas.pes_fisica = pessoa_fisica.pes_id
            INNER JOIN
              pessoa
            ON
              pessoa_fisica.pes_id = pessoa.pes_id
            WHERE
              acesso_pessoas.id = {$user}
        ");
        $result = $this->executeQuery($query);

        if ($result->rowCount() > 0) {
            return $result->fetchAll();
        }

        return false;
    }

    /**
     * Busca mensalidades ativas de um aluno
     * @param int $matricula
     * @param     $tipo
     * @return null|array
     */
    public function buscaMensalidadesAtivasAluno($matricula, $tipo = \Boleto\Service\FinanceiroTituloTipo::MENSALIDADE)
    {
        if (!empty($matricula)) {
            $respositoryTitulo        = $this->getRepository($this->getEntity());
            $respositoryTituloTipo    = $this->getRepository('Boleto\Entity\FinanceiroTituloTipo');
            $serviceTituloMensalidade = (new \Boleto\Service\FinanceiroTituloMensalidade($this->getEm()));
            $serviceAcadperiodoAluno  = (new \Matricula\Service\AcadperiodoAluno($this->getEm()));
            $objTipo                  = $respositoryTituloTipo->findOneBy(['tipotituloId' => $tipo]);

            if ($objTipo) {
                $alunoPeridos = $serviceAcadperiodoAluno->buscaAlunoPeriodos($matricula);

                if (!empty($alunoPeridos)) {
                    foreach ($alunoPeridos as $alunoper) {
                        $mensalidades = $serviceTituloMensalidade->mensalidadesAluno($alunoper->getAlunoperId());

                        foreach ($mensalidades as $mensalidade) {
                            $titulo = $mensalidade->getTitulo();

                            if (
                                $titulo->getTituloEstado() == "Aberto"
                                && $titulo->getTipotitulo()->getTipotituloId() == $objTipo->getTipotituloId()
                            ) {
                                $titulo = array_merge($titulo->toArray(), $mensalidade->toArray());

                                $serviceFinanceiroTituloMensalidade = new \Boleto\Service\FinanceiroTituloMensalidade(
                                    $this->getEm()
                                );

                                $descontos = $serviceFinanceiroTituloMensalidade->buscaDescontosMensalidade($titulo);

                                // adiciona desconto padrao 
                                foreach ($descontos['descontos'] as $desconto) {
                                    if ($titulo['tituloDesconto'] > 0) {
                                        if ($desconto['descNome'] === "Desconto Padrão") {
                                            $titulo['tituloDesconto'] = (float)bcadd(
                                                $titulo['tituloDesconto'],
                                                $desconto['descontoValor'],
                                                5
                                            );
                                        }
                                    }
                                }

                                $arrDescontoValido   = self::validaDescontos(array_merge($titulo, $descontos));
                                $titulo['descontos'] = $arrDescontoValido['descontosValidos'];
                                $titulo['tituloDesconto'] -= $arrDescontoValido['vlrTotalInvalido'];

                                // atualiza valores de desconto do titulo e valor a receber

                                $titulo = array_merge(
                                    $titulo,
                                    $serviceFinanceiroTituloMensalidade->calculaAcrecimos($mensalidade)
                                );

                                $tituloDesconto   = ($titulo['tituloDesconto']) ? $titulo['tituloDesconto'] : 0;
                                $tituloAcrescimos = ($titulo['acrescimos']) ? $titulo['acrescimos']['valorAcrescimos'] : 0;

                                $titulo['valorReceber']                    = $titulo['tituloValor'] - $tituloDesconto + $tituloAcrescimos;
                                $parcelas[$objTipo->getTipotituloNome()][] = $titulo;
                            }
                        }
                    }

                    return $parcelas;
                }
            }
        }

        return null;
    }

    /**
     * Valida desconto pela data de vencimento e dia de vencimento do desconto
     * @param array $titulo : array com informações do titulo, incluindo descontos
     *
     * @return array [
     *          vlrTotalInvalido: valor tatal de descontos invalidos
     *          descontosValidos: array de descontos validos
     *       ]
     */
    public static function validaDescontos(array $arrTitulo)
    {
        $dataAtual = new \DateTime(date_format(new \DateTime('now'), 'Y-m-d') . ' 0:00:00');

        // converte data para formato americano e cria um objeto DateTime
        $tituloDataVencimento = new \DateTime(self::formatDateAmericano($arrTitulo['tituloDataVencimento']));

        // valor de total de descontos invalidos  
        $vlrDescInvalido = 0;

        // verifica se boleto esta com status aberto e tem desconto
        if ($arrTitulo['tituloEstado'] == 'Aberto' && $arrTitulo['tituloDesconto'] > 0) {
            foreach ($arrTitulo['descontos'] as $desc => $desconto) {
                $dataVencDesconto = clone $tituloDataVencimento;

                if (!$desconto['desctipo'] || $desconto['desctipo']->getDesctipoLimitaVencimento() != 'Sim') {
                    continue;
                }

                /**
                 * se houver dia limite para o desconto, valida de acordo com o mesmo
                 * se não, valida de acordo com o dia de vencimento do titulo
                 */
                $diaLimiteDesconto = $desconto['descontoDescontoDiaLimite'] ? $desconto['descontoDescontoDiaLimite'] : $tituloDataVencimento->format(
                    'd'
                );

                $dataVencDesconto->setDate(
                    $tituloDataVencimento->format('Y'),
                    $tituloDataVencimento->format('m'),
                    $diaLimiteDesconto
                );

                $tituloDataVencimento->setTime(0, 0, 00);

                if ($dataVencDesconto <= $dataAtual) {
                    $vlrDescInvalido = (float)bcadd(
                        $vlrDescInvalido,
                        $desconto['descontoValor'],
                        5
                    );
                    unset($arrTitulo['descontos'][$desc]);
                }
            }
        }

        return [
            'vlrTotalInvalido' => $vlrDescInvalido,
            'descontosValidos' => $arrTitulo['descontos']
        ];
    }

    /**
     * Retorna taxas ativas de uma aluno
     * @param int|null  $alunocursoId
     * @param array     $whereParams
     * @param bool|true $calcAcrescimo
     * @return null|array
     */
    public function buscaTaxasAtivasAluno($alunocursoId = null, $whereParams = array(), $calcAcrescimo = true)
    {
        $serviceTituloMensalidade = new \Boleto\Service\FinanceiroTituloMensalidade($this->getEm());

        if (!empty($alunocursoId)) {
            $estado = ($whereParams['estado']) ? $whereParams['estado'] : "  = 'Aberto'";

            if ($whereParams['tipoTitulo']) {
                $tipoTitulo = " financeiro__titulo_tipo.tipotitulo_nome {$whereParams['tipoTitulo']} ";
            } else {
                $tipoTitulo = " financeiro__titulo_tipo.tipotitulo_nome <> 'mensalidade' AND financeiro__titulo_tipo.tipotitulo_nome <> 'Vestibular'";
            }

            $WHERE = " AND acadgeral__aluno_curso.alunocurso_id = {$alunocursoId}";

            $query = "
              SELECT
                financeiro__titulo.titulo_id as tituloId,
                DATE_FORMAT(financeiro__titulo.titulo_data_vencimento, '%d/%m/%Y') as tituloDataVencimento,
                financeiro__titulo.titulo_valor as tituloValor,
                financeiro__titulo.usuario_autor as usuario,
                acesso_pessoas.login as login,
                financeiro__titulo_tipo.tipotitulo_nome as titpotituloNome
              FROM
                financeiro__titulo
              NATURAL JOIN
                financeiro__titulo_tipo
              NATURAL JOIN
                pessoa
              NATURAL JOIN
                pessoa_fisica
              NATURAL JOIN
                acadgeral__aluno
              NATURAL JOIN
                acadgeral__aluno_curso
              INNER JOIN
                acesso_pessoas ON financeiro__titulo.usuario_autor = acesso_pessoas.id
              LEFT JOIN
                acadperiodo__aluno USING (alunocurso_id)
              WHERE
                $tipoTitulo
              AND
                financeiro__titulo.titulo_estado $estado
              $WHERE
            ";

            $result = $this->executeQuery($query);

            if ($result->rowCount() > 0) {
                $taxas['taxas'] = $result->fetchAll();

                if ($calcAcrescimo) {
                    foreach ($taxas['taxas'] as $pos => $taxa) {
                        $acrescimos = $serviceTituloMensalidade->calculaAcrecimos(
                            array('tituloId' => $taxa['tituloId'])
                        );

                        $juros = $acrescimos['acrescimos']['valorAcrescimos'];

                        $taxas['taxas'][$pos]['tituloValor'] += ($juros) ? (float)$juros : 0;
                        $taxas['taxas'][$pos] = array_merge($taxas['taxas'][$pos], $acrescimos);
                    }
                }

                return $taxas;
            }

            return null;
        }

        return null;
    }

    /**
     * Retorna títulos pagos do aluno
     * @param int|null $alunoper
     * @return null|array
     */
    public function buscaTitulosPagosAluno($alunoper = null)
    {
        if (!empty($alunoper)) {
            $query = "
              SELECT
                financeiro__titulo.titulo_id as tituloId,
                DATE_FORMAT(financeiro__titulo.titulo_data_pagamento, '%d/%m/%Y') as tituloDataPagamento,
                DATE_FORMAT(financeiro__titulo.titulo_data_vencimento, '%m') tituloMesVencimento,
                financeiro__titulo.titulo_valor as tituloValor,
                financeiro__titulo.titulo_valor_pago,
                COALESCE(financeiro__titulo.titulo_desconto + financeiro__titulo.titulo_desconto_manual,'0.00') as total_desconto,
                financeiro__titulo.titulo_acrescimo_manual,
                financeiro__titulo.usuario_baixa as usuario,
                acesso_pessoas.login as login,
                financeiro__titulo_tipo.tipotitulo_nome as titpotituloNome,
                financeiro__titulo.titulo_estado as estado
              FROM
                financeiro__titulo
              NATURAL JOIN
                financeiro__titulo_tipo
              NATURAL JOIN
                pessoa
              NATURAL JOIN
                pessoa_fisica
              NATURAL JOIN
                acadgeral__aluno
              LEFT JOIN
                acadgeral__aluno_curso ON acadgeral__aluno.aluno_id = acadgeral__aluno_curso.aluno_id
              NATURAL JOIN
                acadperiodo__aluno
              INNER JOIN
                acesso_pessoas ON financeiro__titulo.usuario_baixa = acesso_pessoas.id
              WHERE
                financeiro__titulo_tipo.tipotitulo_nome <> 'Vestibular'
               AND
                 financeiro__titulo.titulo_estado = 'Pago'
              AND acadperiodo__aluno.alunoper_id = $alunoper";

            $result = $this->executeQuery($query);

            if ($result->rowCount() > 0) {
                $taxas = $result->fetchAll();

                return $taxas;
            }

            return null;
        }

        return null;
    }

    /**
     * Retorna títulos cancelados do aluno
     * @param int|null $alunoper
     * @return null|array
     */
    public function buscaTitulosCanceladosAluno($alunoper = null)
    {
        if (!empty($alunoper)) {
            $query = "
            SELECT
                ft.titulo_id as tituloId,
                DATE_FORMAT(ft.titulo_data_pagamento, '%d/%m/%Y') as tituloDataPagamento,
                DATE_FORMAT(ft.titulo_data_processamento, '%d/%m/%Y') as tituloDataCancelamento,
                DATE_FORMAT(ft.titulo_data_vencimento, '%m') tituloMesVencimento,
                ft.titulo_valor as tituloValor,
                ft.titulo_valor_pago,
                COALESCE(ft.titulo_desconto + ft.titulo_desconto_manual,'0.00') as total_desconto,
                ft.titulo_acrescimo_manual,
                ft.usuario_baixa as usuario,
                ap.login as login,
                ftt.tipotitulo_nome as titpotituloNome,
                ft.titulo_estado as estado
            FROM financeiro__titulo ft
              INNER JOIN financeiro__titulo_tipo ftt on ftt.tipotitulo_id = ft.tipotitulo_id
              INNER JOIN pessoa p on p.pes_id = ft.pes_id
              INNER JOIN pessoa_fisica pf on pf.pes_id = p.pes_id
              INNER JOIN acadgeral__aluno aga on aga.pes_id = pf.pes_id
              INNER JOIN acadgeral__aluno_curso agac on agac.aluno_id = aga.aluno_id
              INNER JOIN acadperiodo__aluno apa on apa.alunocurso_id = agac.alunocurso_id
              INNER JOIN acesso_pessoas ap ON ft.usuario_baixa = ap.id
            WHERE
                ftt.tipotitulo_nome <> 'Vestibular'
                AND ft.titulo_estado not in ('Aberto','Pago')
                AND apa.alunoper_id = $alunoper";

            $result = $this->executeQuery($query);

            if ($result->rowCount() > 0) {
                $taxas = $result->fetchAll();

                return $taxas;
            }

            return null;
        }

        return null;
    }

    /**
     * Retorna cheques ativos do aluno
     * @param int|null $matricula
     * @return null|array
     */
    public function buscaChequesAtivosAluno($matricula = null)
    {
        if (!empty($matricula)) {
            $WHERE = " AND acadgeral__aluno_curso.alunocurso_id  = {$matricula}";

            $query = "
              SELECT
                cheque_id,
                cheque_emitente,
                cheque_num,
                cheque_banco,
                cheque_agencia,
                cheque_conta,
                cheque_valor
              FROM
                pessoa
              NATURAL JOIN
                pessoa_fisica
              NATURAL JOIN
                acadgeral__aluno
              natural JOIN
               acadgeral__aluno_curso
              NATURAL JOIN
                financeiro__titulo
              NATURAL JOIN
                financeiro__titulo_cheque
              NATURAL JOIN
                financeiro__cheque
              WHERE 1
              AND financeiro__titulo.titulo_estado = 'Pago'
              $WHERE
              GROUP BY cheque_id
            ";

            $result = $this->executeQuery($query);

            if ($result->rowCount() > 0) {
                $taxas = $result->fetchAll();

                return $taxas;
            }

            return null;
        }

        return null;
    }

    /**
     * Retorna cheques relacionados ao título
     * @param int|null $titulo
     * @return null|array
     */
    public function buscaChequesTitulo($titulo = null)
    {
        if (!empty($titulo)) {
            $WHERE = " AND financeiro__titulo.titulo_id  = {$titulo}";

            $query = "
              SELECT
                cheque_id,
                cheque_emitente,
                cheque_num,
                cheque_banco,
                cheque_agencia,
                cheque_conta,
                cheque_valor
              FROM
                pessoa
              NATURAL JOIN
                pessoa_fisica
              NATURAL JOIN
                acadgeral__aluno
              natural JOIN
               acadgeral__aluno_curso
              NATURAL JOIN
                  financeiro__titulo
              NATURAL JOIN
                financeiro__titulo_cheque
              NATURAL JOIN
                financeiro__cheque
              WHERE 1
              AND financeiro__titulo.titulo_estado = 'Pago'
              $WHERE
            ";

            $result = $this->executeQuery($query);

            if ($result->rowCount() > 0) {
                $taxas = $result->fetchAll();

                return $taxas;
            }

            return null;
        }

        return null;
    }

    /**
     * Retorna títulos abertos do aluno
     * @param int|null $alunoper
     * @return null|array
     */
    public function buscaTitulosAbertoAluno($alunoper = null)
    {
        if (!empty($alunoper)) {
            $query  = "
              SELECT financeiro__titulo.titulo_id as tituloId
              FROM financeiro__titulo
              NATURAL JOIN financeiro__titulo_tipo
              NATURAL JOIN pessoa
              NATURAL JOIN pessoa_fisica
              NATURAL JOIN acadgeral__aluno
              NATURAL JOIN acadgeral__aluno_curso
              NATURAL JOIN acadperiodo__aluno
              WHERE
                financeiro__titulo_tipo.tipotitulo_nome <> 'Vestibular' AND
                financeiro__titulo.titulo_estado = 'Aberto' AND
                financeiro__titulo.titulo_data_vencimento <= CURRENT_DATE() + 1 AND
                acadperiodo__aluno.alunoper_id = $alunoper";
            $result = $this->executeQuery($query);

            if ($result->rowCount() > 0) {
                $titulos = $result->fetchAll();

                return $titulos;
            }

            return null;
        }

        return null;
    }

    /**
     * metodo que cancela todos os titulos do aluno em um periodo letivo que possuem vinculo com financeiro__titulo_mensalidade
     * @param null $alunoper
     * @throws \Exception
     */
    public function cancelaTodosTitulosEmAbertoDeUmAlunoPeriodo($alunoper = null)
    {
        if (!empty($alunoper)) {
            $titulos = $this->getRepository('Boleto\Entity\FinanceiroTituloMensalidade')->findBy(
                ['alunoper' => $alunoper]
            );

            if ($titulos) {
                $this->begin();
                foreach ($titulos as $titulo) {
                    if ($titulo->getTitulo()->getTituloEstado() == 'Aberto') {
                        $titulo = $this->getRepository()->find($titulo->getTitulo()->getTituloId());
                        try {
                            $this->cancelaTitulo($titulo->toArray());
                        } catch (\Exception $ex) {
                            throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
                        }
                    }
                }

                $this->commit();
                $this->getEm()->clear();
            }
        }
    }

    /**
     * Retorna usuários cadastrados na tesouraria
     * @return array
     */
    public function buscaUsuariosTesouraria()
    {
        $usuarios = $this->executeQuery(
            "
            SELECT
              p.id AS usuario_id, pessoa.pes_nome AS pessoa_nome
            FROM
              acesso_pessoas p
            INNER JOIN
              acesso_usuarios u ON p.usuario = u.id
            INNER JOIN
              acesso_usuarios_grupos g ON g.usuario_id = u.id
            INNER JOIN
              acesso_grupo gru ON gru.id = g.grupo_id
            INNER JOIN
              pessoa_fisica ON pessoa_fisica.pes_id = p.pes_fisica
            INNER JOIN
              pessoa ON pessoa.pes_id = pessoa_fisica.pes_id
            WHERE
              grup_nome = 'Tesouraria'
        "
        )->fetchAll();

        $ret = array('todos' => 'Todos');

        foreach ($usuarios as $usuario) {
            $ret[$usuario['usuario_id']] = $usuario['pessoa_nome'];
        }

        return $ret;
    }

    /**
     * Retorna tipos de títulos
     * @return array
     */
    public function buscaServicosRelatorio()
    {
        $array = array('todos' => 'Todos');
        $tipos = $this->getRepository('Boleto\Entity\FinanceiroTituloTipo')->findAll();

        foreach ($tipos as $tipo) {
            $array[$tipo->getTipotituloId()] = $tipo->getTipotituloNome();
        }

        return $array;
    }
}
