<?php

namespace Boleto\Service;

use VersaSpine\Service\AbstractService;

class FinanceiroTituloTipo extends AbstractService
{
    const MULTA_BIBLIOTECA = 14;
    const MENSALIDADE      = 2;
    const DEPENDENCIA      = 26;
    const ADAPTACAO        = 27;
    const MONOGRAFICA      = 28;
    const VESTIBULAR       = 1;

    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Boleto\Entity\FinanceiroTituloTipo');
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {

    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {

    }

    public function getTiposTituloPrincipais ()
    {
        return [
            self::MULTA_BIBLIOTECA => 14,
            self::MENSALIDADE      => 2,
            self::DEPENDENCIA      => 26,
            self::ADAPTACAO        => 27,
            self::MONOGRAFICA      => 28,
            self::VESTIBULAR       => 1,
        ];
    }

}