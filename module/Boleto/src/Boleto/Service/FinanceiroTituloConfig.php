<?php
namespace Boleto\Service;

use Acesso\Entity\AcessoPessoas;

use VersaSpine\Service\AbstractService;

/**
 * Class FinanceiroTituloConfig
 * @package Boleto\Service
 */
class FinanceiroTituloConfig extends AbstractService
{
    /**
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Boleto\Entity\FinanceiroTituloConfig');
    }

    /**
     * @param $dados
     */
    protected function valida($dados)
    {
    }

    /**
     * @param $criterios
     */
    protected function pesquisaForJson($criterios)
    {
    }

    /**
     * Retorna ultima configuração cadastrada para o período
     * @param int      $perId
     * @param int|null $cursocamposId
     * @return \Boleto\Entity\FinanceiroTituloConfig
     */
    public function configuracaoPeriodo($perId, $cursocamposId = null)
    {
        $arrParams = array('per' => $perId);

        if ($cursocamposId) {
            $arrParams['cursocampos'] = $cursocamposId;
        }

        return $this->getRepository()->findOneBy($arrParams, array('tituloconfId' => 'desc'));
    }
}
