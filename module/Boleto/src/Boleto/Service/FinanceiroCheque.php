<?php


namespace Boleto\Service;

use VersaSpine\Service\AbstractService;

class FinanceiroCheque extends AbstractService{
    public function __construct(\Doctrine\ORM\EntityManager $em){
        parent::__construct($em, 'Boleto\Entity\FinanceiroCheque');
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {

    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {

    }

    public function buscaChequesAluno($matricula)
    {
    }

    public function buscaDadosCheque($chequeId = null){
        if(!empty($chequeId)){
            $cheque = $this->getEm()->getReference('Boleto\Entity\FinanceiroCheque', $chequeId);
            if($cheque){
                $chequeArray = $cheque->toArray();
                $result['titulos'] = $this->buscaTituloPagosComMesmoCheque($chequeId);
                $result['cheque'] = $chequeArray;
                return $result;
            }
        }

        return null;
    }

    public function buscaTituloPagosComMesmoCheque($chequeId = null)
    {
        if(! empty($chequeId)){
            $repositoryTituloCheque = $this->getRepository('Boleto\Entity\FinanceiroTituloCheque');
            $repositoryAcadgeralAluno = $this->getRepository('Matricula\Entity\AcadgeralAluno');
            $repositoryAcadgeralAlunoCurso = $this->getRepository('Matricula\Entity\AcadgeralAlunoCurso');
            $tituloscheques = $repositoryTituloCheque->findBy(['cheque' => $chequeId ]);
            if($tituloscheques){
                foreach ($tituloscheques as $tituloCheque) {
                    $aluno = $repositoryAcadgeralAluno->findOneBy(['pes' => $tituloCheque->getTitulo()->getPes() ]);
                    $alunoCurso = $repositoryAcadgeralAlunoCurso->findOneBy(['aluno' => $aluno->getAlunoId() ]);
                    $titulos[] = array_merge($tituloCheque->getTitulo()->toArray(), ['matricula' => $alunoCurso->getAlunocursoId(), 'valorUtilizado' => $tituloCheque->getTitulochequeValor() ]);
                }
                return $titulos;
            }
        }
        return null;
    }

    public function buscaDadosChequeEuristica(array $dadosCheque){
        if(!empty($dadosCheque)){
            $repositoryCheque = $this->getEm()->getRepository('Boleto\Entity\FinanceiroCheque');
            $cheque = $repositoryCheque->findOneBy([
                'chequeNum'     => $dadosCheque['chequeNum'],
                'chequeConta'   => $dadosCheque['chequeConta'],
                'chequeBanco'   => $dadosCheque['chequeBanco'],
            ]);
            if($cheque){
                $repositoryTituloCheque = $this->getRepository('Boleto\Entity\FinanceiroTituloCheque');
                $tituloscheques = $repositoryTituloCheque->findBy(['cheque' => $cheque->getChequeId() ]);
                if($tituloscheques){
                    $valorUtilizadoCheque = 0;
                    foreach ($tituloscheques as $tituloCheque) {
                        if($tituloCheque->getTitulochequeEstado() === 'Ativo'){
                            $valorUtilizadoCheque += $tituloCheque->getTitulochequeValor();
                        }
                    }
                    if($valorUtilizadoCheque >= $cheque->getChequeValor()){
                        $result['msgError'] = "Este cheque já foi lançado no sistema e totalmente utilizado! Favor inserir um cheque valido.";
                    }
                    $result['cheque'] = array_merge($cheque->toArray(), ['chequeValorUtilizado'=> number_format($valorUtilizadoCheque, 2, '.', ''), 'chequeValorRestante' => number_format(($cheque->getChequeValor() - $valorUtilizadoCheque), 2, '.', '')]);
                    return $result;
                }

            }
        }

        return null;
    }
}