<?php


namespace Boleto\Service;

use VersaSpine\Service\AbstractService;

class BoletoConfConta extends AbstractService{
    public function __construct(\Doctrine\ORM\EntityManager $em){
        parent::__construct($em, 'Boleto\Entity\BoletoConfConta');
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {

    }

    public function edita($dados){
        if(!$dados['arq']){
            unset($dados['arq']);
        }
        return parent::edita($dados);
    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {

    }


    public function resultSearch($post)
    {
        $conditions = (new \VersaSpine\ListGenerator\Annotations\ReaderQuerys($this->getEntity()))->getConditions();
        $metadados = $this->getMetadados();

        $tableOrig = $metadados->table['name'];
        $chaveOrig = $metadados->identifier[0];

        $sql = " SELECT t0.*, pessoa.pes_nome as pes_id FROM $tableOrig t0 ";
        $chaves = array_keys($post);
        $apelido = 0;
        $where = " WHERE ";
        $whereJoins = "";
        $joins = "";
        foreach ($chaves as $input)
        {
            // verifica se esta nos campos simples
            if (isset($metadados->fieldMappings[$input]) && !empty($post[$input]))
            {
                $e = $metadados->fieldMappings[$input];

                if ($post[$e['fieldName']])
                {
                    if ($conditions[$e['columnName']]['type'] == "LIKE")
                    {
                        $where .= " t0.{$e['columnName']} LIKE '{$post[$e['fieldName']]}' AND";
                    } else if ($conditions[$e['columnName']]['type'] == "LIKE%")
                    {
                        $where .= " t0.{$e['columnName']} LIKE '%{$post[$e['fieldName']]}%' AND";
                    } else if ($conditions[$e['columnName']]['type'] == "=")
                    {
                        $where .= " t0.{$e['columnName']} = '{$post[$e['fieldName']]}' AND";
                    }
                }
            } else if (isset($metadados->associationMappings[$input]) && !empty($post[$input]))
            {
                $fj = $metadados->associationMappings[$input];
                $apelido++;
                $entityTarget = $this->em->getClassMetadata($fj['targetEntity']);

                $s = str_replace("Entity", "Service", $fj['targetEntity']);
                $labelInnerJoin = (new $s($this->em))->getLabelEntity();
                $tableInnerJoin = $entityTarget->table['name'];

                $nomeCampoTable = $fj['joinColumns'][0]['name'];
                $chaveInnerJoin = $entityTarget->fieldMappings[$fj['sourceToTargetKeyColumns'][$nomeCampoTable]]['fieldName'];

                $labelPsr0 = $this->strToPsr0($labelInnerJoin);

                if (isset($entityTarget->fieldMappings[$labelPsr0]))
                {
                    $tipoLabelDestino = $entityTarget->fieldMappings[$labelPsr0]['type'];
                } else if (isset($entityTarget->associationMappings[$labelPsr0]))
                {
                    die('A desenvolver');
                }

                $t = "t$apelido";

                $joins .= " INNER JOIN $tableInnerJoin $t ON t0.$chaveOrig = $t.$chaveInnerJoin";

                if ($conditions[$nomeCampoTable]['type'] == "LIKE")
                {
                    $whereJoins .= " $t.$labelInnerJoin LIKE '{$post[$input]}' AND";
                } else if ($conditions[$nomeCampoTable]['type'] == "LIKE%")
                {
                    $whereJoins .= " $t.$labelInnerJoin LIKE '%{$post[$input]}%' AND";
                } else if ($conditions[$nomeCampoTable]['type'] == "=")
                {
                    $whereJoins .= " $t.$labelInnerJoin = '{$post[$input]}' AND";
                }
            }
        }
        $joins = "INNER JOIN pessoa ON t0.pes_id = pessoa.pes_id";
        $query = substr($sql . $joins . $where . $whereJoins, 0, -3);

        $dados = $this->executeQuery($query)->fetchAll();
        return array(
            'dados' => $dados,
            'count' => count($dados)
        );
    }

    public function buscaContas()
    {
        $array       = array();
        $confContas = $this->getEm()->getRepository('\Boleto\Entity\BoletoConfConta')->findAll();

        /* @var $conta \Boleto\Entity\BoletoConfConta */
        foreach ($confContas as $conta) {
            $array[$conta->getConfContId()] = $conta->getConfContAgencia()."-".$conta->getConfContAgenciaDigito()." | ".$conta->getConfContConta().'-'.$conta->getConfcontContaDigito()." | ".$conta->getBanc()->getBancNome();
        }

        return $array;
    }

}