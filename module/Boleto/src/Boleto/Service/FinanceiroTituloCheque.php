<?php


namespace Boleto\Service;

use VersaSpine\Service\AbstractService;

class FinanceiroTituloCheque extends AbstractService
{
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Boleto\Entity\FinanceiroTituloCheque');
    }

    protected function valida($dados)
    {
    }

    protected function pesquisaForJson($criterios)
    {
    }

    public function vinculaChequesTitulo($titulo, $cheque)
    {
        if (!(is_a($titulo, '\Boleto\Entity\FinanceiroTitulo') || is_subclass_of($titulo, '\Boleto\Entity\FinanceiroTitulo'))) {
            throw new \Exception('Classe inválida: a variável $titulo deve ser uma entidade FinanceiroTitulo');
        }

        $em = $this->getEm();
        $chequesInfo = array();

        if (!is_array($cheque)) {
            $cheque = array($cheque);
        }


        // percorre cheques em busca dos creditos restantes dos mesmos
        foreach ($cheque as $c) {
            $query = $em->createQuery("SELECT SUM(tc.titulochequeValor) AS total FROM \Boleto\Entity\FinanceiroTituloCheque tc WHERE tc.cheque = :cheque");
            $query->setParameter('cheque', $c->getChequeId());
            $result = $query->getResult();

            $gastos = $result[0]['total'] ? $result[0]['total'] : 0;

            $chequesInfo[] = array(
                'valor' => $c->getChequeValor(),
                'gastos' => floatval($gastos),
                'credito' => floatval($c->getChequeValor() - $gastos),
                'cheque' => $c,
            );
        }

        // ordena por credito menor
        usort($chequesInfo, function ($a, $b) {
            if ($a['credito'] == $b['credito']) {
                return 0;
            }

            return ($a['credito'] > $b['credito']) ? 1 : -1;

        });
        $titulosCheques = array();

        $this->begin();

        $valorAtualTitulo = $titulo->getTituloValorPago();

        foreach ($chequesInfo as $chequeInfo) {
            if ($chequeInfo['credito'] >= $valorAtualTitulo) {
                $chequeInfo['credito'] = $valorAtualTitulo;
                $valorAtualTitulo = 0;
            } else {
                $valorAtualTitulo = $valorAtualTitulo - $chequeInfo['credito'];
            }

            $tituloCheque = new \Boleto\Entity\FinanceiroTituloCheque([
                'titulo' => $titulo,
                'cheque' => $chequeInfo['cheque'],
                'titulochequeValor' => $chequeInfo['credito']
            ]);

            $em->persist($tituloCheque);

            $titulosCheques[] = $tituloCheque;

        }
        $em->flush();

        $this->commit();


        return $tituloCheque;

    }
}