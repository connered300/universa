<?php


namespace Boleto\Service;

use Doctrine\ORM\EntityManager;
use VersaSpine\Service\AbstractService;

class Financeiro extends AbstractService
{
    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Boleto\Entity\FinanceiroTitulo');
    }

    protected function valida($dados)
    {
    }

    protected function pesquisaForJson($criterios)
    {
    }

    /**
     * Este metodo foi criado para permitir a paginação via ajax usando o plugin dataTable,
     * @param $query string com a consulta que será executada,
     * @param $arrayData array com os dados enviados via ajax pelo plugin dataTables
     * @param $sqlAdditional null OPCIONAL: caso o desenvolvedor queira adicionar algum sql no final da consulta
     *         (ex: um 'GROUP BY ...')
     * @param $retDataTables true OPCIONAL: caso o desenvolvedor queira que o retorno dos dados já estejam no padrao
     *          que o DataTables usa para exibir os resultados (ex: 'array( 0 => value0, 1 => value1)' )
     *
     * @return array $result array de resultados da consulta final
     */
    protected function paginationDataTablesAjax($query, $arrayData, $sqlAdditional = null, $retDataTables = true)
    {
        $query = "select * from ($query) as newtable ";

        $conn = $this->getEm()->getConnection()->prepare($query . $sqlAdditional);
        $conn->execute();

        $result = array(
            "draw"         => $arrayData['draw'],
            "recordsTotal" => $conn->rowCount(),
        );

        // verifica se existe alguma busca a ser feita
        $vSearch = $arrayData["search"]['value'];
        if (!empty($vSearch)) {
            $params = array();
            $query .= " WHERE ";

            foreach ($arrayData['columns'] as $key => $value) {
                $slug = preg_replace('/[^0-9a-z]/i', '', $value['name']);

                $params[$slug] = "%$vSearch%";
                $query .= "`{$value['name']}` LIKE :{$slug} OR ";
            }

            $query = substr($query, 0, -3);
        }

        $query .= $sqlAdditional;

        $result["recordsFiltered"] = $this->executeQueryWithParam($query, $params)->rowCount();
        $orderBy                   = array();

        foreach ($arrayData['order'] as $order) {
            $orderColumn    = $order["column"];
            $orderColumnDir = $order["dir"];
            $columnName     = $arrayData['columns'][$orderColumn]["name"];

            $orderBy[] = "`{$columnName}` $orderColumnDir";
        }

        $orderBy = implode(" , ", $orderBy);

        if (!empty($orderBy)) {
            $orderBy = " ORDER BY " . $orderBy;
        }

        $query .= $orderBy . " LIMIT {$arrayData['length']} OFFSET {$arrayData['start']}";

        $result["data"] = $this->executeQueryWithParam($query, $params)->fetchAll();

        // verificando se os dados devem ser retornados no padrao DataTables
        if ($retDataTables === true) {
            // percorrendo os dados retornados e adequando ao padrao usado pelo dataTables para exibir os resultados
            foreach ($result["data"] as $key => $r) {
                $a = array();

                foreach ($r as $v) {
                    $a[] = $v;
                }

                $result["data"][$key] = $a;
            }
        }

        return $result;
    }

    public function paginationAjax(array $arrayData)
    {
        $query = "

           SELECT
               acadgeral__aluno_curso.alunocurso_id matricula,
                IFNULL(alunoMatriculado.alunoper_id,alunoPeriodo.alunoper_id) alunoper_id,
                pes_nome nome,
                turma_nome turma,
                acadgeral__situacao.matsit_descricao situacao,
                CONCAT(per_ano, '/', per_semestre) periodo
            FROM
                acadgeral__aluno
                    LEFT JOIN
                acadgeral__aluno_curso ON acadgeral__aluno.aluno_id = acadgeral__aluno_curso.aluno_id
                    LEFT JOIN
                (SELECT
                    acadperiodo__aluno.*
                FROM
                    acadperiodo__aluno
                INNER JOIN (SELECT
                    alunocurso_id, MAX(alunoper_id) AS alunoper_id
                FROM
                    acadperiodo__aluno
                INNER JOIN acadgeral__situacao ON acadgeral__situacao.situacao_id = acadperiodo__aluno.matsituacao_id
                WHERE
                  matsit_descricao != 'Pré Matricula'
                GROUP BY alunocurso_id) q1 ON q1.alunoper_id = acadperiodo__aluno.alunoper_id) AS alunoPeriodo ON acadgeral__aluno_curso.alunocurso_id = alunoPeriodo.alunoCurso_id
                 LEFT JOIN
                (SELECT
                    acadperiodo__aluno.*
                FROM
                    acadperiodo__aluno
                INNER JOIN (SELECT
                    alunocurso_id, MAX(alunoper_id) AS alunoper_id
                FROM
                    acadperiodo__aluno
                INNER JOIN acadgeral__situacao ON acadgeral__situacao.situacao_id = acadperiodo__aluno.matsituacao_id
                INNER JOIN acadperiodo__turma ON acadperiodo__turma.turma_id = acadperiodo__aluno.turma_id
                WHERE
                  matsit_descricao = 'Matriculado'
                GROUP BY alunocurso_id) q1 ON q1.alunoper_id = acadperiodo__aluno.alunoper_id) AS alunoMatriculado ON alunoMatriculado.alunocurso_id = acadgeral__aluno_curso.alunoCurso_id
                    LEFT JOIN
                acadgeral__situacao ON acadgeral__situacao.situacao_id = alunoPeriodo.matsituacao_id
                    INNER JOIN
                pessoa  ON pessoa.pes_id = acadgeral__aluno.pes_id
                    INNER JOIN
                acadperiodo__turma ON acadperiodo__turma.turma_id = alunoPeriodo.turma_id
                    LEFT JOIN
                acadperiodo__letivo ON acadperiodo__turma.per_id = acadperiodo__letivo.per_id
            WHERE
                1

        ";

        $result = $this->paginationDataTablesAjax($query, $arrayData, null, false);

        return $result;
    }

    function buscaTitulo($tituloId)
    {
        if ($tituloId) {
            $servFinanceiroTitulo            = new \Boleto\Service\FinanceiroTitulo($this->getEm());
            $servFinanceiroTituloMensalidade = new \Boleto\Service\FinanceiroTituloMensalidade($this->getEm());

            $titulo = $servFinanceiroTitulo->getReference($tituloId);

            if ($titulo) {
                $titulo = $titulo->toArray();

                if (in_array($titulo['tipotituloNome'], ['Mensalidade', 'Dependência', 'Adaptação', 'Monografia'])) {
                    $mensalidade = $servFinanceiroTituloMensalidade
                        ->getRepository()
                        ->findOneBy(['titulo' => $titulo['tituloId']]);

                    if (!$mensalidade) {
                        throw new \Exception(
                            'Título não está vinculado a nenhuma mensalidade! Entre em contato com o suporte.'
                        );
                    }

                    $titulo = array_merge($titulo, $mensalidade->toArray());

                    if ($titulo['tituloEstado'] != "Pago") {
                        // soma o desconto padrao
                        $descontos = $servFinanceiroTituloMensalidade->buscaDescontosMensalidade($titulo);

                        if (!empty($descontos['descontos'])) {
                            foreach ($descontos['descontos'] as $desconto) {
                                if ($desconto['descNome'] === "Desconto Padrão") {
                                    $titulo['tituloDesconto'] += $desconto['descontoValor'];
                                }
                            }
                        }

                        $titulo = array_merge($titulo, $descontos);
                    }

                    $titulo = array_merge($titulo, $servFinanceiroTituloMensalidade->calculaAcrecimos($mensalidade));

                    $tituloDesconto = (!empty($titulo['tituloDesconto']) ? $titulo['tituloDesconto'] : 0);
                    $acrescimos     = (!empty($titulo['acrescimos']) ? $titulo['acrescimos']['valorAcrescimos'] : 0);

                    $titulo['valorReceber'] = $titulo['tituloValor'] - $tituloDesconto + $acrescimos;

                    return $titulo;
                }

                $session = new \Zend\Session\Container('Zend_Auth');

                $titulo['usuarioAutor'] = array(
                    'usuario' => $session->storage['usuario'],
                    'login'   => $session->storage['login'],
                );

                return $titulo;
            }
        }

        return null;
    }

    public function buscaUsuarioTitulo($user)
    {
        $repositoryUsuarioPessoas = $this->getEm()->getRepository('Acesso\Entity\AcessoPessoas');
        $user                     = $repositoryUsuarioPessoas->findOneBy(['id' => $user]);

        if ($user) {
            return $user;
        }

        return null;
    }

    public function grupoTesousaria($usuario)
    {
        $servAcessoGrupo = new \Acesso\Service\AcessoGrupo($this->getEm());
        $usuarioGrupos   = $servAcessoGrupo->buscaGrupoUsr($usuario);

        foreach ($usuarioGrupos as $usuarioGrupo) {
            $grupo = $this->getEm()->getReference('Acesso\Entity\AcessoGrupo', $usuarioGrupo)->getGrupNome();

            if ($grupo === 'Financeiro') {
                return true;
            }
        }

        return false;
    }
}