<?php


namespace Boleto\Service;

use VersaSpine\Service\AbstractService;

class BoletoBanco extends AbstractService
{
    const BANCO_BRASIL          = 1;
    const BANCO_NORDESTE_BRASIL = 4;
    const BANCO_SANTANDER       = 33;
    const BANCO_BRADESCO_BDI    = 36;
    const BANCO_CAIXA           = 104;
    const BANCO_ITAU_BBA        = 184;
    const BANCO_BRADESCO        = 237;
    const BANCO_ACB_BRASIL      = 246;
    const BANCO_ITAU_UNIBANCO   = 341;
    const BANCO_MERCANTIL       = 389;
    const BANCO_HSBC            = 399;
    const BANCO_UNIBANCO        = 409;
    const BANCO_BANCOOB         = 756;
    const BANCO_UNICRED         = 136;

    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Boleto\Entity\BoletoBanco');
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {

    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {

    }
}