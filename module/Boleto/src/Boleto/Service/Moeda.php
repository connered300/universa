<?php

namespace Boleto\Service;

class Moeda
{
    public static function moedaToDec($val)
    {
        $val = str_replace(".", "", $val);
        $val = str_replace(",", ".", $val);

        //$valor = $valor/100;

        return number_format($val, 2, '.', '');
    }

    public static function decToMoeda($val)
    {
        return number_format($val, 2, ',', '.');
    }

    public static function unformatNumber($str)
    {
        return (string)preg_replace("/[^0-9]/", "", $str);
    }
}
