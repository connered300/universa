<?php


namespace Vestibular\ViewHelper;


use Zend\View\Helper\HelperInterface;
use Zend\View\Renderer\RendererInterface as Renderer;

class Boleto implements HelperInterface{
    public function __construct()
    {
    }

    /**
     * Set the View object
     *
     * @param  Renderer $view
     * @return HelperInterface
     */
    public function setView(Renderer $view)
    {

    }

    /**
     * Get the View object
     *
     * @return Renderer
     */
    public function getView()
    {

    }

    public function getButton($id)
    {
        if($id){
            return "<a href='/boleto-bancario/boleto/exibe/$id'><button class='btn btn-default' style='padding: 5px'> <i class='fa fa-check '></i> Visualizar Boleto </button></a>";
        }
        return "Boleto não gerado";
    }
}