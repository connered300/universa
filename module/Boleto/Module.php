<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Boleto;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;


class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig(){
        return array(
            'factories' => array(
                'Boleto\Service\Financeiro' => function( $em ){
                    return new Service\Financeiro( $em->get('Doctrine\ORM\EntityManager'));
                },
                'Boleto\Service\FinanceiroTitulo' => function( $em ){
                    return new Service\FinanceiroTitulo( $em->get('Doctrine\ORM\EntityManager'));
                },
                'Boleto\Service\FinanceiroDesconto' => function( $em ){
                    return new Service\FinanceiroDesconto( $em->get('Doctrine\ORM\EntityManager'));
                },
                'Boleto\Service\BoletoBanco' => function( $em ){
                    return new Service\BoletoBanco( $em->get('Doctrine\ORM\EntityManager'));
                },
                'Boleto\Service\FinanceiroCheque' => function( $em ){
                    return new Service\FinanceiroCheque( $em->get('Doctrine\ORM\EntityManager'));
                },
            ),
        );
    }
}
