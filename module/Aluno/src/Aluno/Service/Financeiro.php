<?php

namespace Aluno\Service;

use VersaSpine\Service\AbstractService;

class Financeiro extends AbstractService
{
    /**
     * @var null
     */
    private $__lastError = null;

    /** @var \Sistema\Service\SisConfig */
    private $config = null;

    /**
     * @return \Sistema\Service\SisConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param \Sistema\Service\SisConfig $config
     * @return FinanceiroTitulo
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     * @param array                       $arrConfig
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, $arrConfig = array())
    {
        parent::__construct($em, 'Financeiro\Entity\FinanceiroTitulo');

        $this->setConfig(new \Sistema\Service\SisConfig($em, $arrConfig));
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    protected function valida($dados)
    {
    }

    protected function pesquisaForJson($criterios)
    {
    }

    /**
     * @param $data array|null
     * @param $remessaAtivada boolean // quando o boleto tem de ter registro na remessa
     * @param $objPessoa \Pessoa\Entity\PessoaFisica|\Pessoa\Entity\PessoaJuridica
     * @return array|null
     */
    public function getDataForDatatables($data, $objPessoa)
    {
        if (!is_object($objPessoa)) {
            $this->setLastError("Parâmetro 'objPessoa' inválido");

            return null;
        }

        if (!$data) {
            $this->setLastError("Parâmetro 'data' inválido");

            return null;
        }

        $pesId = $objPessoa->getPes()->getPesId();

        $query = "
        SELECT
            *,
            if(datediff(date(now()), date(financeiro__titulo.titulo_data_vencimento)) <= 0, FALSE, TRUE) vencido,
            (select count(bol_id)>0 from financeiro__remessa_lote where bol_id=boleto.bol_id) as esta_na_remessa
        FROM financeiro__titulo
        LEFT JOIN boleto USING(titulo_id)
        -- JOINS --
        WHERE
          financeiro__titulo.titulo_estado IN ('Aberto', 'Pago') AND pes_id = " . $pesId . "
          -- CONDITIONS --
        ORDER BY financeiro__titulo.titulo_data_vencimento DESC";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }
}
?>