<?php

namespace Aluno\Service;


use VersaSpine\Service\AbstractService;

class Biblioteca extends AbstractService
{
    /**
     * @var null
     */
    private $__lastError = null;

    /** @var \Sistema\Service\SisConfig */
    private $config = null;


    /**
     * @return \Sistema\Service\SisConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param \Sistema\Service\SisConfig $config
     * @return Biblioteca
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     * @param array                       $arrConfig
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, $arrConfig = array())
    {
        parent::__construct($em, 'Biblioteca\Entity\Titulo');

        $this->setConfig(new \Sistema\Service\SisConfig($em, $arrConfig));
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    protected function valida($dados)
    {
        // TODO: Implement valida() method.
    }


    protected function pesquisaForJson($criterios)
    {
        // TODO: Implement pesquisaForJson() method.
    }


    public function getDataForDatatablesEmprestimos($pesId, $data)
    {
        if(!is_numeric($pesId)){
            $this->setLastError('Verifique se o usuário está correto!');

            return false;
        }

        $quantidadeRenovacaoConsecutivas = (int)$this->getConfig()->localizarChave("BIBLIOTECA_RENOVACAO_CONSECUTIVAS");

        $query = "
        SELECT
            bemp.emprestimo_id,
            (
                SELECT SUM(IF(biblioteca__exemplar.titulo_id = bex.titulo_id, 1, 0))
                FROM biblioteca__emprestimo
                INNER JOIN biblioteca__exemplar USING (exemplar_id)
                WHERE biblioteca__emprestimo.pes_id=bemp.pes_id
                ORDER BY emprestimo_data DESC
                LIMIT 0, $quantidadeRenovacaoConsecutivas
            ) as quantidadeDeEmprestimosConcedidos,
            emprestimo_data as emprestimo_data_us,
            titulo_titulo, grupo_bibliografico_nome, exemplar_codigo, '' as acoes,
            emprestimo_devolucao_data_previsao as emprestimo_devolucao_data_previsao_us,
            emprestimo_devolucao_data_efetuada as emprestimo_devolucao_data_efetuada_us,
            DATE_FORMAT(emprestimo_data,'%d/%m/%Y') as emprestimo_data,
            datediff( COALESCE(emprestimo_devolucao_data_efetuada, curdate()), emprestimo_devolucao_data_previsao) dias,
            DATE_FORMAT(emprestimo_devolucao_data_previsao,'%d/%m/%Y') as emprestimo_devolucao_data_previsao,
            DATE_FORMAT(emprestimo_devolucao_data_efetuada,'%d/%m/%Y') as emprestimo_devolucao_data_efetuada,
            (
                SELECT GROUP_CONCAT(DISTINCT autor_referencia SEPARATOR '/ ')
                FROM biblioteca__titulo_autor bta
                INNER JOIN biblioteca__autor ba USING (autor_id)
                WHERE bta.titulo_id=bt.titulo_id
            ) autor_referencia
        FROM biblioteca__emprestimo bemp
        INNER JOIN biblioteca__exemplar bex USING(exemplar_id)
        INNER JOIN biblioteca__titulo bt USING (titulo_id)
        INNER JOIN biblioteca__grupo_bibliografico bgb USING (grupo_bibliografico_id)
        INNER JOIN biblioteca__modalidade_emprestimo bme USING (modalidade_emprestimo_id)
        INNER JOIN biblioteca__pessoa bp USING (pes_id)
        WHERE bemp.pes_id = $pesId AND bex.exemplar_acesso <> 'kit Saraiva'";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);
        return $result;
    }
}