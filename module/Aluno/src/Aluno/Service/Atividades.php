<?php

namespace Aluno\Service;

use VersaSpine\Service\AbstractService;

class Atividades extends AbstractService
{

    /**
     * @var null
     */
    private $__lastError = null;

    /** @var \Sistema\Service\SisConfig */
    private $config = null;

    /**
     * @return \Sistema\Service\SisConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param \Sistema\Service\SisConfig $config
     * @return FinanceiroTitulo
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     * @param array                       $arrConfig
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, $arrConfig = array())
    {
        parent::__construct($em, 'Atividades\Entity\AtividadeperiodoAlunoAtividades');

        $this->setConfig(new \Sistema\Service\SisConfig($em, $arrConfig));
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    protected function valida($dados)
    {

    }

    public function pesquisaForJson($params)
    {
    }

    /**
     * Faz a busca se o usuário possui atividade vinculada a ele
     * @param $usuarioId
     * @return bool
     */
    public function buscaAtividadeUsuario($usuarioId){
        $query = "SELECT COUNT(*) as total
                       FROM atividadeperiodo__aluno
                       JOIN atividadeperiodo__evento using(evento_id)
                       JOIN acadperiodo__aluno using(alunoper_id)
                       JOIN acadgeral__aluno_curso using(alunocurso_id)
                       JOIN acadgeral__aluno using(aluno_id)
                       JOIN acesso_pessoas acesso ON (pes_fisica = pes_id)
                       WHERE acesso.usuario = :usuario";
        $result = $this->executeQueryWithParam($query, [ 'usuario' =>$usuarioId ])->fetch();
        return $result['total'] > 0;
    }


    /**
     * Busca tem por finalidade retornar se o usuário logado na interface do aluno possui informações de estágio
     * @param $usuarioId
     * @return bool
     */
    public function buscaAtividadesEstagioUsuario($usuarioId){
        if( !is_numeric($usuarioId) ){
            $this->setLastError('Verifique se o usuário está correto!');

            return false;
        }
        $perAtual  = (new \Matricula\Service\AcadperiodoLetivo($this->getEm()))->buscaPeriodoCorrente(true);
        $periodos = '';

        /** @var \Matricula\Entity\AcadperiodoLetivo $entidade */
        foreach ($perAtual as $entidade) {
            $periodos = $periodos ? $periodos . ',' . $entidade->getPerId() : $entidade->getPerId();
        }

        $estagio = $this->executeQuery(
            "
            SELECT COUNT(*) as total FROM acesso_pessoas ace
            INNER JOIN pessoa_fisica pf ON pf.pes_id = ace.pes_fisica
            INNER JOIN acadgeral__aluno aluno ON pf.pes_id = aluno.pes_id
            INNER JOIN acadgeral__aluno_curso acurso ON aluno.aluno_id = acurso.aluno_id
            INNER JOIN atividadegeral__configuracoes_curso confatividades ON confatividades.cursocampus_id = acurso.cursocampus_id
            INNER JOIN atividadegeral__configuracoes conf on confatividades.atividadeconf_portaria = conf.atividadeconf_portaria
            INNER JOIN acadperiodo__aluno aaluno ON acurso.alunocurso_id = aaluno.alunocurso_id 
            INNER JOIN acadperiodo__turma taluno ON taluno.turma_id =  aaluno.turma_id
            WHERE
                ace.usuario = {$usuarioId} AND taluno.per_id in($periodos)
                AND FIND_IN_SET(taluno.turma_serie, conf.atividadeconf_serie)
        "
        )->fetch();
        //retorna true para se usuario possui disciplina de estágio e false para não existe
        return $estagio['total'] > 0;
    }

    public function getDataForDatatablesEstagio($pesId, $data)
    {
        if(!is_numeric($pesId)){
            $this->setLastError('Verifique se o usuário está correto!');

            return false;
        }
        $query = "SELECT
                      atividade.alunoatividade_data_lancamento as alunoatividade_data_lancamento_us,
                      DATE_FORMAT(atividade.alunoatividade_data_lancamento,'%d/%m/%Y') alunoatividade_data_lancamento,
                      atividade.alunoatividade_horas,
                      IF(atividade.alunoatividade_descricao IS NULL or atividade.alunoatividade_descricao = '',
                          anucleo.nucleo_descricao,
                          atividade.alunoatividade_descricao
                      ) alunoatividade_descricao,
                      periodo.per_nome,
                      nucleo.alunoperiodo_status status,
                      atividade.atividadealuno_atividade_id
                      FROM atividadeperiodo__aluno_atividades atividade
                      JOIN atividadeperiodo__aluno_nucleo nucleo USING(alunonucleo_id)
                      JOIN atividadegeral__nucleo anucleo USING(nucleo_id)
                      JOIN acadperiodo__aluno aluno USING(alunoper_id)
                      JOIN acadperiodo__letivo periodo USING(per_id)
                      JOIN acadgeral__aluno_curso acurso USING(alunocurso_id)
                      JOIN acadgeral__aluno using(aluno_id)
                      JOIN pessoa_fisica using(pes_id)
                      where pes_id = '{$pesId}' ";
        $totalizacao = " sum(alunoatividade_horas) as total_horas";
        $result = parent::paginationDataTablesAjax($query, $data, null, false, false, false, $totalizacao);
        return $result;
    }

    public function getDataForDatatables($pesId, array $data)
    {

        if (isset($data['filter']['pes_id'])) {
            $pesId = $data['filter']['pes_id'];
        }
        if(!is_numeric($pesId)){
            $this->setLastError('Verifique se o aluno está correto!');

            return false;
        }

        $where = "";
        if($data['alunoatividade_data_lancamento']){
            $where .= "AND alunoatividade_data_lancamento = {$data['alunoatividade_data_lancamento']}";
        }
        if($data['aluno_atividade_horas_validas']){
            $where .= "AND aluno_atividade_horas_validas = {$data['aluno_atividade_horas_validas']}";
        }
        if($data['atividadeatividade_descricao']){
            $where .= "AND atividadeatividade_descricao = {$data['atividadeatividade_descricao']}";
        }

        $query = "SELECT aluno_atividade_data_realizacao as aluno_atividade_data_realizacao_us,
                         aluno_atividade_horas_validas,
                         evento_nome,
                         evento_responsavel,
                         atividadeatividade_descricao,
                         DATE_FORMAT(aluno_atividade_data_realizacao,'%d/%m/%Y') as aluno_atividade_data_realizacao
                         
                         -- periodo                         
                         -- contador
                         
                       FROM atividadeperiodo__aluno
                       JOIN atividadeperiodo__evento using(evento_id)
                       JOIN atividadegeral__atividades ON evento_tipo_atividade = atividadeatividade_id
                       JOIN acadperiodo__aluno using(alunoper_id)
                       JOIN acadgeral__aluno_curso using(alunocurso_id)
                       JOIN acadgeral__aluno using(aluno_id)
                       JOIN pessoa_fisica using(pes_id)
                       
                       -- join
                       
                       where pes_id = '{$pesId}' $where
                  ";

        if (isset($data['filter']['eventosAluno'])) {
            $query = str_replace('-- periodo ', " ,periodo.per_nome ", $query);
            $query = str_replace('-- join','LEFT JOIN acadperiodo__letivo periodo on periodo.per_id=atividadeperiodo__evento.evento_periodo_letivo',$query);
            $query .= " group by evento_id ";
        }

        $totalizacao = " sum(aluno_atividade_horas_validas) as total_horas";

        if (isset($data['filter']['totalHoras'])) {
            $query = str_replace("-- contador", ",SUM(aluno_atividade_horas_validas)total_horas", $query);
            $query.=' group by atividadeatividade_descricao ';
            $totalizacao = " sum(total_horas) as total_horas";
        }

        $result = parent::paginationDataTablesAjax($query, $data, null, false, false, false, $totalizacao);

        return $result;
    }


}