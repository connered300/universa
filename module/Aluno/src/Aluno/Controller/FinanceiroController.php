<?php
namespace Aluno\Controller;

use VersaSpine\Controller\AbstractCoreController;

class FinanceiroController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {
    }

    public function indexAction()
    {
        return $this->getView();
    }

    public function searchAction()
    {
        $serviceAcesso = new \Acesso\Service\AcessoPessoas($this->getEntityManager());
        $request       = $this->getRequest();
        $config        = $this->getServiceLocator()->get('config');
        $objUsuario    = $serviceAcesso->retornaUsuarioLogado();

        if ($request->isPost()) {
            $serviceSisConfig  = new \Sistema\Service\SisConfig($this->getEntityManager(), $config);
            $serviceFinanceiro = new \Aluno\Service\Financeiro($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();

            $boletoAtivado        = (bool)$serviceSisConfig->localizarChave('BOLETO_ATIVO');
            $boletoAtivoAluno     = (bool)$serviceSisConfig->localizarChave('BOLETO_ATIVO_ALUNO');
            $boletoRemessaAtivada = (bool)$serviceSisConfig->localizarChave('BOLETO_REMESSA_ATIVADA');
            $pagseguro            = (bool)$serviceSisConfig->localizarChave('PAGSEGURO_ATIVO');
            $boletoAtivado        = ($boletoAtivado && $boletoAtivoAluno);

            $result = $serviceFinanceiro->getDataForDatatables($dataPost, $objUsuario->getPes());

            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
            $this->getJson()->setVariable("pagseguro", $pagseguro);
            $this->getJson()->setVariable("boletoAtivo", $boletoAtivado);
            $this->getJson()->setVariable("boletoRemessaAtivada", $boletoRemessaAtivada);
        }

        return $this->getJson();
    }
}