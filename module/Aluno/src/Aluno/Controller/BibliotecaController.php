<?php

namespace Aluno\Controller;

use VersaSpine\Controller\AbstractCoreController;

class BibliotecaController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function indexAction()
    {
        $arrConfig        = $this->getServiceManager()->get('Config');
        $serviceSisConfig = new \Sistema\Service\SisConfig($this->getEntityManager(), $arrConfig);

        $quantidadeDiasRenovacao         = $serviceSisConfig->localizarChave("BIBLIOTECA_RENOVACAO_DIAS_LIMITES");
        $quantidadeRenovacaoConsecutivas = $serviceSisConfig->localizarChave("BIBLIOTECA_RENOVACAO_CONSECUTIVAS");
        $permiteAlunoRenovar             = $serviceSisConfig->localizarChave("BIBLIOTECA_RENOVACAO_PELO_ALUNO_ATIVO");

        $dataAtual = new \Datetime('now');
        $dataAtual = $dataAtual->format('Y-m-d');

        $this->getView()->setVariable('dataAtual', $dataAtual);
        $this->getView()->setVariable('diasParaRenovacao', $quantidadeDiasRenovacao);
        $this->getView()->setVariable('quantidadeDeRenovacoes', $quantidadeRenovacaoConsecutivas);
        $this->getView()->setVariable('permiteAlunoRenovar', $permiteAlunoRenovar);

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $serviceBiblioteca    = new \Aluno\Service\Biblioteca(
                $this->getEntityManager(),
                $this->getServiceManager()->get('Config')
            );
            $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEntityManager());

            $usuario = $serviceAcessoPessoas->retornaUsuarioLogado();

            $dataPost = $request->getPost()->toArray();

            $pesId = $usuario->getPes()->getPes() ? $usuario->getPes()->getPes()->getPesId() : null;

            $result = $serviceBiblioteca->getDataForDatatablesEmprestimos($pesId, $dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function consultarAcervoAction()
    {
        $objGrupoBibliografico  = new \Biblioteca\Service\GrupoBibliografico($this->getEntityManager());
        $jsonGrupoBibliografico = json_encode($objGrupoBibliografico->getArrSelect2());

        $this->getView()->setTemplate('/aluno/biblioteca/consultar-acervo');

        $this->getView()->setVariable('permiteVisualizarAcervo', 1);
        $this->getView()->setVariable('jsonGrupoBibliografico', $jsonGrupoBibliografico);

        return $this->getView();
    }
}