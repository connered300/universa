<?php

namespace Aluno\Controller;

use VersaSpine\Controller\AbstractCoreController;

class DisciplinasController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {
    }

    public function indexAction()
    {
        $id                    = $this->params()->fromRoute('id');
        $service               = new \Professor\Service\AcadperiodoAnotacao($this->getEntityManager());
        $arrConfig             = $this->getServiceManager()->get('Config');
        $serviceSisConfig      = new \Sistema\Service\SisConfig($this->getEntityManager(), $arrConfig);
        $arredondamentoDeNotas = (int)$serviceSisConfig->localizarChave('ARREDONDAMENTO_CALCULO_NOTA_SEMESTRAL');
        $alunoDisciplina       = $service
            ->getRepository('Matricula\Entity\AcadperiodoAlunoDisciplina')
            ->findOneBy(['alunodiscId' => $id]);

        $this->view->setVariable('alunodisc', $alunoDisciplina);
        $this->view->setVariable('arredondamentoDeNotas', $arredondamentoDeNotas);

        return $this->view;
    }

    public function anotacoesAction()
    {
        $id        = $this->params()->fromRoute('id');
        $service   = new \Professor\Service\AcadperiodoAnotacao($this->getEntityManager());
        $anotacoes = [];

        $alunoDisciplina = $service->getRepository('Matricula\Entity\AcadperiodoAlunoDisciplina')->findOneBy(
            ['alunodiscId' => $id]
        );

        if ($alunoDisciplina) {
            $docenteDisciplina = $service->getRepository('Matricula\Entity\AcadperiodoDocenteDisciplina')->findOneBy(
                ['turma' => $alunoDisciplina->getTurma(), 'disc' => $alunoDisciplina->getDisc()]
            );
            $anotacoes         = $service->getRepository('Professor\Entity\AcadperiodoAnotacao')->findBy(
                ['docdisc' => $docenteDisciplina],
                ['anotData' => 'DESC']
            );
        }

        $this->view->setVariables(['anotacoes' => $anotacoes]);

        $this->view->setTerminal(true);

        return $this->view;
    }

    public function faltaPorDisciplinaAction()
    {
        $alunoDiscId = $this->params()->fromRoute('id');
        $service     = new \Professor\Service\AcadperiodoFrequencia($this->getEntityManager());

        $faltas = $service->buscaFaltasAlunoPorDisicplina($alunoDiscId);

        $this->view->setVariables(
            [
                'faltas' => $faltas
            ]
        );
        $this->view->setTerminal(true);
        $this->view->setTemplate('/aluno/frequencia/index');

        return $this->view;
    }

    public function notasAction()
    {
        $arrConfig             = $this->getServiceManager()->get('Config');
        $serviceSisConfig      = new \Sistema\Service\SisConfig($this->getEntityManager(), $arrConfig);
        $arredondamentoDeNotas = (int)$serviceSisConfig->localizarChave('ARREDONDAMENTO_CALCULO_NOTA_SEMESTRAL');

        $serviceCursoConfig = new \Matricula\Service\AcadCursoConfig($this->getEntityManager());

        $id = $this->params()->fromRoute('id');

        $service = new \Professor\Service\AcadperiodoEtapaAluno($this->getEntityManager());

        /** @var \Matricula\Entity\AcadperiodoAlunoDisciplina $alunoDisc */
        $alunoDisc = $service->getRepository('Matricula\Entity\AcadperiodoAlunoDisciplina')->findOneBy(
            ['alunodiscId' => $id]
        );
        $etapas    = $service->getRepository('Professor\Entity\AcadperiodoEtapaAluno')->findBy(
            ['alunodisc' => $id],
            ['diario' => 'desc']
        );

        /** @var \Professor\Entity\AcadperiodoEtapaAluno $etapa */
        foreach ($etapas as $key => $etapa) {
            $media[$key] = $service->buscaMediaEtapa($etapa->getDiario()->getDiarioId());
        }

        $objCursoConfig = null;

        if ($alunoDisc) {
            /** @var \Matricula\Entity\AcadCursoConfig $objCursoConfig */
            $objCursoConfig = $serviceCursoConfig->retornaObjCursoConfig(
                $alunoDisc->getTurma()->getCursocampus()->getCurso()->getCursoId()
            );
        }

        $this->view->setVariables(
            [
                'arredondamentoDeNotas'          => $arredondamentoDeNotas,
                'etapas'                         => $etapas,
                'media'                          => $media,
                'constMetodoDesempenhoNotaMedia' => $serviceCursoConfig::METODO_DESEMPENHO_CALCULO_POR_MEDIA,
                'objCursoConfig'                 => $objCursoConfig
            ]
        );

        return $this->view;
    }

    public function notasResumoAction()
    {
        $request = $this->getRequest();
        $service = new \Professor\Service\AcadperiodoEtapaAluno($this->getEntityManager());

        if ($request->isPost()) {
            $dados                 = $request->getPost()->toArray();
            $etapaDisc             = [];
            $arrConfig             = $this->getServiceManager()->get('Config');
            $serviceSisConfig      = new \Sistema\Service\SisConfig($this->getEntityManager(), $arrConfig);
            $arredondamentoDeNotas = (int)$serviceSisConfig->localizarChave('ARREDONDAMENTO_CALCULO_NOTA_SEMESTRAL');

            foreach ($dados['alunodisc'] as $key => $disc) {
                $etapas = $service
                    ->getRepository('Professor\Entity\AcadperiodoEtapaAluno')
                    ->findBy(['alunodisc' => $disc['alunodisc_id']]);
                $media  = 0;
                $nota   = 0;

                foreach ($etapas as $etapa) {
                    $media += $service->buscaMediaEtapa($etapa->getDiario()->getDiarioId());
                    $nota += $etapa->getAlunoetapaNota();
                }

                $etapaDisc[$key] = [
                    'media' => $media,
                    'nota'  => $nota,
                    'disc'  => $disc
                ];
            }

            $this->view->setVariables(
                [
                    'arredondamentoDeNotas' => $arredondamentoDeNotas,
                    'etapaDisc'             => $etapaDisc
                ]
            );
        }

        return $this->view;
    }
}