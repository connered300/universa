<?php
namespace Aluno\Controller;

use VersaSpine\Controller\AbstractCoreController;

class AlunosController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {
    }

    public function indexAction()
    {
        $serviceAlunoDisciplina = new \Matricula\Service\AcadperiodoAlunoDisciplina($this->getEntityManager());
        $serviceGrade           = $this->services()->getService('Professor\Service\AcadperiodoGrade');
        $serviceFrequ           = $this->services()->getService('Professor\Service\AcadperiodoFrequencia');
        /** @var \Matricula\Service\AcadgeralAluno $serviceAluno */
        $serviceAluno      = $this->services()->getService('Matricula\Service\AcadgeralAluno');
        $serviceEtapaAluno = $this->services()->getService('Professor\Service\AcadperiodoEtapaAluno');
        /** @var \Matricula\Service\AcadperiodoAluno $serviceAlunoPeriodo */
        $serviceAlunoPeriodo = $this->services()->getService('Matricula\Service\AcadperiodoAluno');
        /** @var \Matricula\Service\AcadCursoConfig $serviceCursoConfig */
        $serviceCursoConfig = $this->services()->getService('Matricula\Service\AcadCursoConfig');

        $serviceAcessoPessoas         = new \Acesso\Service\AcessoPessoas($this->getEntityManager());
        $arrConfig                    = $this->getServiceManager()->get('Config');
        $serviceSisConfig             = new \Sistema\Service\SisConfig($this->getEntityManager(), $arrConfig);
        $serviceAlunocurso            = new \Matricula\Service\AcadgeralAlunoCurso($this->getEntityManager());
        $serviceBibliotecaVirtual     = new \Biblioteca\Service\BibliotecaVirtual($this->getEntityManager());
        $arredondamentoDeNotas        = (int)$serviceSisConfig->localizarChave('ARREDONDAMENTO_CALCULO_NOTA_SEMESTRAL');
        $tokenBibliotecaPearsonConfig = $serviceSisConfig->localizarChave('TOKEN_BIBLIOTECA_PEARSON');
        $enderecoBibliotacaPearson    = $serviceSisConfig->localizarChave('ENDERECO_BIBLIOTECA_PEARSON');
        $urlAcessoAva                 = $serviceSisConfig->localizarChave('LINK_ACESSO_AVA');

        $usuario = $serviceAcessoPessoas->retornaUsuarioLogado();

        /** @var $objAluno \Matricula\Entity\AcadgeralAluno */
        $objAluno = $serviceAluno->getRepository('Matricula\Entity\AcadgeralAluno')->findby(
            ['pes' => $usuario->getPes()]
        );

        $arrObjAlunocurso   = $serviceAlunocurso->getRepository()->findBy(['aluno' => $objAluno[0]->getAlunoId()]);
        $arrAlunoPeriodos   = array();
        $arrAlunoPeriodosId = array();
        $matricula          = null;

        foreach ($arrObjAlunocurso as $int => $objAlunocurso) {
            $arrObjAlunoPeriodo = $serviceAlunoPeriodo->getRepository('Matricula\Entity\AcadperiodoAluno')->findBy(
                ['alunocurso' => $objAlunocurso->getAlunocursoId()]
            );

            if ($objAlunocurso && !$matricula) {
                $matricula = $objAlunocurso->getAlunocursoId();
            }

            if (!$arrObjAlunoPeriodo) {
                continue;
            }

            foreach ($arrObjAlunoPeriodo as $chave => $objAlunoPeriodo) {
                $arrAlunoPeriodos[$chave] = [
                    'alunoPer'      => $objAlunoPeriodo->getAlunoPerId(),
                    'alunoSemestre' => (
                        $objAlunoPeriodo->getTurma()->getPer()->getPerSemestre() .
                        '° Semestre de ' .
                        $objAlunoPeriodo->getTurma()->getPer()->getPerAno()
                    ),
                    'periodoId'     => $objAlunoPeriodo->getTurma()->getPer()->getPerId()
                ];
                $arrAlunoPeriodosId[]     = $objAlunoPeriodo->getAlunoPerId();
            }
        }

        $alunoper = $serviceAluno->retornaAlunoperiodoValido($objAlunocurso->getAlunocursoId());

        /** @var \Matricula\Entity\AcadperiodoAluno $objAlunoperiodo */
        $objAlunoperiodo = $serviceAlunoPeriodo->getRepository()->findOneBy(['alunoperId' => $alunoper]);
        $objAlunocurso   = $objAlunoperiodo ? $objAlunoperiodo->getAlunocurso() : null;

        if ($alunoper) {
            $disciplinas = $serviceAlunoDisciplina->retornaDisciplinasAlunoPer(
                array('alunoPer' => $alunoper, 'situacoesValidas' => true)
            );
        }

        if ($objAlunocurso && !$matricula) {
            $matricula = $objAlunocurso->getAlunocursoId();
        }

        $rematriculaDisponivel = $serviceAlunocurso->verificaSeAlunoPossuiRematricula($matricula);

        $gradeAluno = $serviceGrade->buscaGrade($disciplinas);
        $gradeAluno = $serviceGrade->formataGradeHorariaAluno($gradeAluno);

        foreach ($disciplinas as $key => $disc) {
            $faltasDisc[] = $serviceFrequ->buscaFaltasDisciplina($disc['alunodisc_id']);
            /** @var $etapas  \Professor\Entity\AcadperiodoEtapaAluno */
            $etapas = $serviceEtapaAluno->getRepository('Professor\Entity\AcadperiodoEtapaAluno')->findBy(
                ['alunodisc' => $disc['alunodisc_id']]
            );
            //            $matricula = $_SESSION['Zend_Auth']['storage']['login'];

            $objAlunoResumo = $serviceEtapaAluno->getRepository('Matricula\Entity\AcadperiodoAlunoResumo')->findOneBy(
                ['alunoper' => $alunoper, 'discId' => $disc['disc'], 'alunocursoId' => $matricula]
            );

            //CONDIÇÃO ABAIXAO PARA DEFIR A SITUAÇÃO DO ALUNO NA DISCIPLINA CONFORME AS ATIVIDADE EXECUTADAS PELO PROFESSOR QUE PODEM SER: APROVADO, EXAME FINAL, REPROVADO OU CURSANDO CASO NÃO TENHA NENHUMA SITUAÇÃO DEFINIDA.
            // $etapaDisc[$key]['cor'] COR DA FONTE NA VIEW

            if (!is_null($objAlunoResumo)) {
                $etapaDisc[$key]['situacao'] = $objAlunoResumo->getResAlunoSituacao();
                $etapaDisc[$key]['cor']      = 'blue'; // COR DA FONTE NA VIEW

                if (strtoupper($objAlunoResumo->getResAlunoSituacao()) === 'REPROVADO') {
                    $etapaDisc[$key]['cor'] = 'red';
                    $alunoFinal             = $serviceEtapaAluno->getRepository(
                        'Matricula\Entity\AcadperiodoAlunoDisciplinaFinal'
                    )->findOneBy(['alunodisc' => $disc['alunodisc_id']]);

                    if ($alunoFinal) {
                        $objDocenteDisciplina = $serviceEtapaAluno->getRepository(
                            'Matricula\Entity\AcadperiodoDocenteDisciplina'
                        )->findOneBy(['turma' => $disc['turmaId'], 'disc' => $disc['disc']]);

                        if (
                            ($alunoFinal && is_null($alunoFinal->getAlunofinalNota())) ||
                            ($objDocenteDisciplina && is_null($objDocenteDisciplina->getDocdiscDataFechamentoFinal()))
                        ) {
                            $etapaDisc[$key]['situacao'] = 'EXAME FINAL';
                        } else {
                            $etapaDisc['nota'] = $objAlunoResumo->getResAlunoNota();
                        }
                    }
                }
            } else {
                $etapaDisc[$key]['situacao'] = 'CURSANDO';
            }

            $media     = 0;
            $nota      = 0;
            $numEtapas = 0;

            /** @var $etapa \Professor\Entity\AcadperiodoEtapaAluno */
            foreach ($etapas as $etapa) {
                $curso = $etapa->getAlunodisc() ? $etapa->getAlunodisc()->getTurma()->getMatCur()->getCurso() : false;

                /** @var $objcursoConfig  \Matricula\Entity\AcadCursoConfig */
                $objcursoConfig = $serviceCursoConfig->retornaObjCursoConfig($curso->getCursoId());

                if (!$objcursoConfig) {
                    $this->flashMessenger()->addErrorMessage($serviceCursoConfig->getLastError());

                    return $this->redirect()->toRoute(
                        $this->getRoute(),
                        array('controller' => $this->getController())
                    );
                }

                $media += $serviceEtapaAluno->buscaMediaEtapa($etapa->getDiario()->getDiarioId());
                $nota += $etapa->getAlunoetapaNota();
            }

            if ((!$etapa || !$objcursoConfig) && $objAlunocurso) {
                /** @var $objcursoConfig  \Matricula\Entity\AcadCursoConfig */
                $objcursoConfig = $serviceCursoConfig->retornaObjCursoConfig(
                    $objAlunocurso->getCursocampus()->getCurso()->getCursoId()
                );
            }

            if ($objcursoConfig) {
                $metodoCalculoDesempenhoAluno = (
                $objcursoConfig->getCursoconfigMetodo()
                    ?
                    $objcursoConfig->getCursoconfigMetodo()
                    :
                    $serviceCursoConfig::METODO_DESEMPENHO_CALCULO_POR_SOMA
                );

                if ($metodoCalculoDesempenhoAluno == $serviceCursoConfig::METODO_DESEMPENHO_CALCULO_POR_MEDIA) {
                    $numEtapas = $etapa ? $etapa->getAlunodisc()->getTurma()->getPer()->getPerEtapas() : 1;
                    $nota      = number_format(
                        ($nota / $numEtapas),
                        2,
                        '.',
                        ''
                    );
                }
            }

            $etapaDisc[$key]['nota']  = $nota;
            $etapaDisc[$key]['media'] = $numEtapas ? $media / $numEtapas : $media;
            $etapaDisc[$key]['disc']  = $disc;
        }

        $pesCpf = $objAluno ? preg_replace("/[^0-9]/", "", $objAluno[0]->getPes()->getPesCpf()) : null;

        $tokenBibliotecaPearson = '';
        $bibliotecaPerson       = false;

        if ($enderecoBibliotacaPearson && $pesCpf && $tokenBibliotecaPearsonConfig) {
            $tokenBibliotecaPearson = md5($pesCpf . $tokenBibliotecaPearsonConfig);
            $bibliotecaPerson       = true;
        }

        $serviceAtividadesAluno = new \Aluno\Service\Atividades($this->getEntityManager());
        $serviceAcessoPessoas   = new \Acesso\Service\AcessoPessoas($this->getEntityManager());

        $usuario = $serviceAcessoPessoas->retornaUsuarioLogado();

        $permissaoAtividades = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Aluno\Controller\Atividades',
            'index'
        );

        $permissaoEstagio = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Aluno\Controller\Atividades',
            'estagio'
        );

        $permissaoEmprestimoBiblioteca = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Aluno\Controller\Biblioteca',
            'index'
        );

        $permissaoBibliotecaVirtual = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Biblioteca\Controller\BibliotecaVirtual',
            'criar-url'
        );

        $estagio = false;

        if ($permissaoEstagio) {
            $estagio = $serviceAtividadesAluno->buscaAtividadesEstagioUsuario($usuario->getUsuario()->getId());
        }

        $linkExternoAlunoEndereco = "#";
        $linkExternoAlunoAtiva    = $serviceSisConfig->localizarChave('LINK_EXTERNO_ALUNO');
        $linkExternoAlunoMsg      = "";

        if ($linkExternoAlunoAtiva) {
            $linkExternoAlunoEndereco = $serviceSisConfig->localizarChave('LINK_EXTERNO_ALUNO_ENDERECO');
            $linkExternoAlunoMsg      = $serviceSisConfig->localizarChave('LINK_EXTERNO_ALUNO_MSG');

            $pesId = $usuario->getPes()->getPes() ? $usuario->getPes()->getPes()->getPesId() : null;
            $linkExternoAlunoEndereco .= $pesId;
        }

        $this->getView()->setVariables(
            [
                'arrCursoConfig'            => $objcursoConfig ? $objcursoConfig->toArray() : null,
                'numEtapas'                 => $numEtapas,
                'gradeAluno'                => $gradeAluno,
                'disciplinas'               => $disciplinas,
                'alunoper'                  => $alunoper,
                'faltasDisc'                => $faltasDisc,
                'etapaDisc'                 => $etapaDisc,
                'alunoPeriodos'             => $arrAlunoPeriodos,
                'arredondamentoDeNotas'     => $arredondamentoDeNotas,
                'pesCpf'                    => $pesCpf,
                'tokenBibliotecaPearson'    => $tokenBibliotecaPearson,
                'bibliotecaPerson'          => $bibliotecaPerson,
                'enderecoBibliotacaPearson' => $enderecoBibliotacaPearson,
                'urlAcessoAva'              => $urlAcessoAva ? $urlAcessoAva : false,
                'estagio'                   => $estagio,
                'atividades'                => $permissaoAtividades,
                'emprestimoBiblioteca'      => $permissaoEmprestimoBiblioteca,
                'acessoBibliotecaVirtual'   => $permissaoBibliotecaVirtual,
                'linkExternoAlunoAtivado'   => $linkExternoAlunoAtiva,
                'linkExternoAlunoEndereco'  => $linkExternoAlunoEndereco,
                'linkExternoAlunoMsg'       => $linkExternoAlunoMsg,
                'rematriculaDisponivel'     => $rematriculaDisponivel,
            ]
        );

        return $this->getView();
    }

    public function rematricularAlunoAction()
    {
        $request  = $this->getRequest();
        $erro     = true;
        $mensagem = '';

        if ($request->isPost()) {
            $serviceAlunoCurso = new \Matricula\Service\AcadgeralAlunoCurso(
                $this->getEntityManager(),
                $this->getServiceManager()->get('Config')
            );

            $arrDados = $request->getPost()->toArray();

            if ($arrInfo = $serviceAlunoCurso->ativaMatriculaRetornandoInfo($arrDados['alunocurso_id'])) {
                $mensagem = 'Rematrícula efetuada com sucesso!';
                $erro     = false;
            } else {
                $mensagem = $serviceAlunoCurso->getLastError();
            }
        }

        $this->getJson()->setVariable('erro', $erro);
        $this->getJson()->setVariable('mensagem', $mensagem);

        return $this->getJson();
    }
}
