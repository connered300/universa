<?php
namespace Aluno\Controller;

use VersaSpine\Controller\AbstractCoreController;

class AtividadesController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function indexAction()
    {
        return $this->getView();
    }

    public function estagioAction(){
        $portfolioAtividade = null;
        $serviceAtividadePortifolio = new \Atividades\Service\AtividadeperiodoAlunoPortfolio($this->getEntityManager());
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEntityManager());

        $usuario = $serviceAcessoPessoas->retornaUsuarioLogado();
        $pesId = null;
        try {
            $pesId = $usuario->getPes()->getPes()->getPesId();
            $this->view->setVariable( "portfolioAtividade" , $serviceAtividadePortifolio->buscaUltimoAlunoPortfolio( $pesId ) );
        } catch (\Exception $ex) {
            $this->flashMessenger()->addErrorMessage(
                "Não foi possivel verificar as atividades do estágio! Favor entrar em contato com suporte."
            );
        }
        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $serviceAtividades = new \Aluno\Service\Atividades($this->getEntityManager());
            $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEntityManager());

            $usuario = $serviceAcessoPessoas->retornaUsuarioLogado();

            $dataPost = $request->getPost()->toArray();

            $pesId = $usuario->getPes()->getPes() ? $usuario->getPes()->getPes()->getPesId() : null;

            $result   = $serviceAtividades->getDataForDatatables($pesId, $dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
            $this->getJson()->setVariable("totalHoras", $result["totalizacoes"]['total_horas']);
        }

        return $this->getJson();
    }

    public function searchEstagioAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $serviceAtividades = new \Aluno\Service\Atividades($this->getEntityManager());
            $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();

            $usuario = $serviceAcessoPessoas->retornaUsuarioLogado();
            $pesId = $usuario->getPes()->getPes() ? $usuario->getPes()->getPes()->getPesId() : null;

            $result   = $serviceAtividades->getDataForDatatablesEstagio($pesId, $dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
            $this->getJson()->setVariable("totalHoras", $result["totalizacoes"]['total_horas']);
        }

        return $this->getJson();
    }

}