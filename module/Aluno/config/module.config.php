<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Aluno;

return array(
    'router' => array(
        'routes' => array(
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'aluno' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/aluno',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Aluno\Controller',
                        'controller' => 'Alunos',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/[:controller[/:action[/:id]]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Aluno\Controller\Alunos' => 'Aluno\Controller\AlunosController',
            'Aluno\Controller\Atividades' => 'Aluno\Controller\AtividadesController',
            'Aluno\Controller\Biblioteca' => 'Aluno\Controller\BibliotecaController',
            'Aluno\Controller\Disciplinas' => 'Aluno\Controller\DisciplinasController',
            'Aluno\Controller\Financeiro' => 'Aluno\Controller\FinanceiroController',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
    'funcionalidades_liberadas' => array(
        'controllers' => array(
//            'Aluno\Controller\Disciplinas'
        ),
        'actions' => array(
//            'busca-frequencia-por-disciplina'
            'rematricular-aluno',
            'retornar-informacoes-rematricula'
        ),
    ),
);
