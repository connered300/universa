<?php

namespace Atividades;

return array(
    'router'                    => array(
        'routes' => array(
            'atividades' => array(
                'type'          => 'Literal',
                'options'       => array(
                    'route'    => '/atividades',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Atividades\Controller',
                        'controller'    => 'AlunoNucleo',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes'  => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'       => '/[:controller[/:action[/:id]]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults'    => array(),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager'           => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        )
    ),
    'doctrine'                  => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default'             => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            ),
        ),
    ),
    'controllers'               => array(
        'invokables' => array(
            'Atividades\Controller\AlunoAtividades'            => 'Atividades\Controller\AlunoAtividadesController',
            'Atividades\Controller\AlunoNucleo'                => 'Atividades\Controller\AlunoNucleoController',
            'Atividades\Controller\AlunoPortfolio'             => 'Atividades\Controller\AlunoPortfolioController',
            'Atividades\Controller\ConfiguracoesAtividades'    => 'Atividades\Controller\ConfiguracoesAtividadesController',
            'Atividades\Controller\AtividadePeriodoLetivo'     => 'Atividades\Controller\AtividadePeriodoLetivoController',
            'Atividades\Controller\Nucleo'                     => 'Atividades\Controller\NucleoController',
            'Atividades\Controller\AtividadePeriodoAluno'      => 'Atividades\Controller\AtividadePeriodoAlunoController',
            'Atividades\Controller\AtividadePeriodoEvento'     => 'Atividades\Controller\AtividadePeriodoEventoController',
            'Atividades\Controller\AtividadegeralSecretaria'   => 'Atividades\Controller\AtividadegeralSecretariaController',
            'Atividades\Controller\AtividadegeralTipo'         => 'Atividades\Controller\AtividadegeralTipoController',
            'Atividades\Controller\AtividadegeralProcessoTipo' => 'Atividades\Controller\AtividadegeralProcessoTipoController',
            'Atividades\Controller\NucleoResponsavel'          => 'Atividades\Controller\NucleoResponsavelController ',
            'Atividades\Controller\AtividadegeralAtividades'   => 'Atividades\Controller\AtividadegeralAtividadesController',
            'Atividades\Controller\AtividadesComplementares'   => 'Atividades\Controller\AtividadesComplementaresController',
        ),
    ),
    'view_manager'              => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'console'                   => array(
        'router' => array(
            'routes' => array(),
        ),
    ),
    'funcionalidades_liberadas' => array(
        'controllers' => array(),
        'actions'     => array(),
    ),
    'namesDictionary'           => array(
        'Atividades\AtividadePeriodoAluno'      => 'Atividades',
        'Atividades\AtividadesComplementares'   => 'Atividades Complementares',
        'Atividades\AtividadePeriodoEvento'     => 'Eventos',
        'Atividades\AlunoAtividades'            => 'Atividades de Estágio do Aluno',
        'Atividades\AlunoNucleo'                => 'Núcleos do Aluno',
        'Atividades\AlunoPortfolio'             => 'Portfólio do Aluno',
        'Atividades\ConfiguracoesAtividades'    => 'Configurações de Atividades e Estágio',
        'Atividades\AtividadePeriodoLetivo'     => 'AtividadePeriodoLetivo',
        'Atividades\Nucleo'                     => 'Núcleos de estágio',
        'Atividades\AtividadegeralSecretaria'   => 'Secretarias de Estágio e Atividades Complementares',
        'Atividades\AtividadegeralTipo'         => 'Tipos de Atividade',
        'Atividades\AtividadegeralProcessoTipo' => 'Tipos de Processo',
        'Atividades\NucleoResponsavel'          => 'Responsável pelo Núcleo',
        'Atividades\AtividadegeralAtividades'   => 'Atividades Complementares',
    )
);