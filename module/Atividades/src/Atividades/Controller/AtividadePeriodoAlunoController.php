<?php

namespace Atividades\Controller;

use VersaSpine\Controller\AbstractCoreController;

class AtividadeperiodoAlunoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request                      = $this->getRequest();
        $paramsGet                    = $request->getQuery()->toArray();
        $paramsPost                   = $request->getPost()->toArray();
        $serviceAtividadeperiodoAluno = new \Atividades\Service\AtividadeperiodoAluno($this->getEntityManager());

        $result = $serviceAtividadeperiodoAluno->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAtividadeperiodoAluno = new \Atividades\Service\AtividadeperiodoAluno($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceAtividadeperiodoAluno->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function addAction($atividadePeriodoId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados                     = array();
        $serviceAtividadeperiodoAluno = new \Atividades\Service\AtividadeperiodoAluno($this->getEntityManager());

        if ($atividadePeriodoId) {
            $arrDados = $serviceAtividadeperiodoAluno->getArray($atividadePeriodoId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);

            if ($atividadePeriodoId) {
                /** @var  $objAtividadePeriodo \Atividades\Entity\AtividadeperiodoAluno */
                $objAtividadePeriodo = $serviceAtividadeperiodoAluno->getRepository()->find($atividadePeriodoId);

                $edita = ($objAtividadePeriodo->getAlunoper()->getAlunoperId() != $arrDados['alunoper'] ||
                    $objAtividadePeriodo->getEvento()->getEventoId() != $arrDados['evento']);

                if ($arrDados && $edita) {
                    $this->getView()->setVariable("erro", true);
                    $mensagem = 'Não é possivel editar o aluno ou evento, de uma atividade cadastrada anteriomente!';

                    if (!$ajax) {
                        $this->flashMessenger()->addErrorMessage($mensagem);

                        return $this->redirect()->toRoute(
                            $this->getRoute(),
                            array('controller' => $this->getController())
                        );
                    } else {
                        $this->getView()->setVariable("mensagem", $mensagem);
                    }

                    return $this->getView();
                }
            }

            $salvar   = $serviceAtividadeperiodoAluno->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de atividade do aluno salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceAtividadeperiodoAluno->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceAtividadeperiodoAluno->getLastError());
                }
            }
        }

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $atividadePeriodoId = $this->params()->fromRoute("id", 0);

        if (!$atividadePeriodoId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($atividadePeriodoId);
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAtividadeperiodoAluno = new \Atividades\Service\AtividadeperiodoAluno($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceAtividadeperiodoAluno->remover($dataPost['atividade']);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceAtividadeperiodoAluno->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}
?>