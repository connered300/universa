<?php

namespace Atividades\Controller;

use VersaSpine\Controller\AbstractCoreController;

class NucleoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request                     = $this->getRequest();
        $paramsGet                   = $request->getQuery()->toArray();
        $paramsPost                  = $request->getPost()->toArray();
        $serviceAtividadegeralNucleo = new \Atividades\Service\AtividadegeralNucleo($this->getEntityManager());

        $result = $serviceAtividadegeralNucleo->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAtividadegeralNucleo = new \Atividades\Service\AtividadegeralNucleo($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceAtividadegeralNucleo->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function editAction()
    {
        $nucleoVagas = $this->params()->fromRoute("id", 0);

        if (!$nucleoVagas) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($nucleoVagas);
    }

    public function addAction($nucleoVagas = false)
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();
        $ajax    = false;

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new JsonModel());
            $ajax = true;
        }

        $arrDados                    = array();
        $serviceAtividadegeralNucleo = new \Atividades\Service\AtividadegeralNucleo($em);

        if ($nucleoVagas) {
            $arrDados = $serviceAtividadegeralNucleo->getArray($nucleoVagas);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = $request->getPost()->toArray();
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceAtividadegeralNucleo->save($dadosAtividadegeralNucleo);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage('Registro de atividadegeral__nucleo salvo!');

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }
            } else {
                $this->getView()->setVariable("erro", array($serviceAtividadegeralNucleo->getLastError()));
            }
        }

        $arrNucleoTipo = $serviceAtividadegeralNucleo->getArrSelect2NucleoTipo();

        $this->getView()->setVariable("arrNucleoTipo", $arrNucleoTipo);

        $arrNucleoDiasFuncionamento = $serviceAtividadegeralNucleo->getArrSelect2NucleoDiasFuncionamento();

        $this->getView()->setVariable("arrNucleoDiasFuncionamento", $arrNucleoDiasFuncionamento);

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAtividadegeralNucleo = new \Atividades\Service\AtividadegeralNucleo($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceAtividadegeralNucleo->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceAtividadegeralNucleo->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}
?>