<?php

namespace Atividades\Controller;

use VersaSpine\Controller\AbstractCoreController;

class AtividadegeralAtividadesController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request        = $this->getRequest();
        $paramsGet      = $request->getQuery()->toArray();
        $paramsPost     = $request->getPost()->toArray();
        $serviceAtividadegeralAtividades = new \Atividades\Service\AtividadegeralAtividades($this->getEntityManager());

        $result = $serviceAtividadegeralAtividades->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceAtividadegeralAtividades = new \Atividades\Service\AtividadegeralAtividades($this->getEntityManager());
        $serviceAtividadegeralAtividades->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAtividadegeralAtividades = new \Atividades\Service\AtividadegeralAtividades($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceAtividadegeralAtividades->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function addAction($atividadeatividadeId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados = array();
        $serviceAtividadegeralAtividades = new \Atividades\Service\AtividadegeralAtividades($this->getEntityManager());

        if ($atividadeatividadeId) {
            $arrDados = $serviceAtividadegeralAtividades->getArray($atividadeatividadeId);

            if (empty($arrDados) ) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar    = $serviceAtividadegeralAtividades->save($arrDados);


            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de atividade salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceAtividadegeralAtividades->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceAtividadegeralAtividades->getLastError());
                }
            }
        }

        $serviceAtividadegeralAtividades->formataDadosPost($arrDados);
        $serviceAtividadegeralAtividades->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $atividadeatividadeId = $this->params()->fromRoute("id", 0);

        if (!$atividadeatividadeId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($atividadeatividadeId);
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAtividadegeralAtividades = new \Atividades\Service\AtividadegeralAtividades($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceAtividadegeralAtividades->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceAtividadegeralAtividades->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}
?>