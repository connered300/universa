<?php

namespace Atividades\Controller;

use VersaSpine\Controller\AbstractCoreController;

class AlunoPortfolioController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request                               = $this->getRequest();
        $paramsGet                             = $request->getQuery()->toArray();
        $paramsPost                            = $request->getPost()->toArray();
        $serviceAtividadeperiodoAlunoPortfolio = new \Atividades\Service\AtividadeperiodoAlunoPortfolio(
            $this->getEntityManager()
        );

        $result = $serviceAtividadeperiodoAlunoPortfolio->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        $cont = count($result);

        if ($cont == 0) {
            $result = ['type' => 'error', 'message' => 'Nenhum documento lançado para avaliação'];
        } else {
            $result = current($result);
            $result = ['type' => 'success', 'message' => $result['portfolioaluno_situacao']];
        }

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAtividadeperiodoAlunoPortfolio = new \Atividades\Service\AtividadeperiodoAlunoPortfolio(
                $this->getEntityManager()
            );

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceAtividadeperiodoAlunoPortfolio->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function alunoPortfolioAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $arrayData = $request->getPost()->toArray();

            $serviceAlunoPortifolio = new \Atividades\Service\AtividadeperiodoAlunoPortfolio($this->getEntityManager());

            $result = $serviceAlunoPortifolio->getAlunoPortfolioInfo($arrayData);

            $this->json->setVariable("draw", $result["draw"]);
            $this->json->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->json->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->json->setVariable("data", $result["data"]);
        }

        return $this->json;
    }

    public function editAction()
    {
        $portfolioalunoSituacao = $this->params()->fromRoute("id", 0);

        if (!$portfolioalunoSituacao) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($portfolioalunoSituacao);
    }

    public function addAction()
    {
        $serviceAlunoPort  = new \Atividades\Service\AtividadeperiodoAlunoPortfolio($this->getEntityManager());
        $peridoLetivoAtual = (new \Matricula\Service\AcadperiodoLetivo($this->getEntityManager()))->buscaUltimoPeriodo(
        );

        $request = $this->getRequest();

        if ($request->isPost()) {
            $dados = $request->getPost()->toArray();

            if ($dados['pontos'] <= $dados['pt_max']) {
                if ($dados['pontos'] < $dados['pt_min']) {
                    $dados['situacao'] = 'Reprovado';
                } else {
                    $dados['situacao'] = 'Aprovado';
                }

                $dados['dataEntrega'] = new \DateTime(date("Ymd"));

                $dados['perId'] = $peridoLetivoAtual->getPerId();

                $dados['alunonucleoId'] = (int)$dados['alunonucleoId'];

                $result = $serviceAlunoPort->save($dados);

                if (!$result) {
                    $result = ['type' => 'error', 'message' => $result];
                } elseif (is_string($result)) {
                    $result = ['type' => 'warning', 'message' => $result];
                } else {
                    $result = ['type' => 'success', 'message' => 'Dados atualizados com Sucesso!'];
                }
            } else {
                $result = [
                    'type'    => 'error',
                    'message' => 'Pontuação lançada deve ser menor ou igual a: ' . $dados['pt_max']
                ];
            }
        }
        $this->json->setVariable('result', $result);

        return $this->json;
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAtividadeperiodoAlunoPortfolio = new \Atividades\Service\AtividadeperiodoAlunoPortfolio(
                $this->getEntityManager()
            );

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceAtividadeperiodoAlunoPortfolio->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceAtividadeperiodoAlunoPortfolio->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}
?>