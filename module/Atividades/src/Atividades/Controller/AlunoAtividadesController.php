<?php

namespace Atividades\Controller;

use VersaSpine\Controller\AbstractCoreController;
use Zend\Authentication\AuthenticationService;

class AlunoAtividadesController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request                = $this->getRequest();
        $paramsGet              = $request->getQuery()->toArray();
        $paramsPost             = $request->getPost()->toArray();
        $serviceAlunoAtividades = new \Atividades\Service\AtividadeperiodoAlunoAtividades($this->getEntityManager());

        $result = $serviceAlunoAtividades->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEntityManager());

        $possuiAcessoExclusao = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Atividades\Controller\AlunoAtividades',
            'remove'
        );

        $this->getView()->setVariable('possuiAcessoExclusao', $possuiAcessoExclusao);

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAlunoAtividades = new \Atividades\Service\AtividadeperiodoAlunoAtividades(
                $this->getEntityManager()
            );

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceAlunoAtividades->getDataForDatatables($dataPost);

            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function editAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $params                     = $request->getPost()->toArray();
            $service                    = new \Atividades\Service\AtividadeperiodoAlunoNucleo(
                $this->getEntityManager()
            );
            $sessao                     = (new AuthenticationService)->getIdentity();
            $params['usuarioAlteracao'] = $sessao['id'];

            $result = $service->editarHoras($params);

            if ($result) {
                $result = ['type' => 'success', 'message' => 'Edição concluída'];
                $this->getJson()->setVariables($result);
            } else {
                $result = ['type' => 'error', 'message' => 'Não foi possível concluir a edição'];
                $this->getJson()->setVariables($result);
            }
        }

        return $this->getJson();
    }

    public function addAction($alunoatividadeId = false)
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados               = array();
        $serviceAlunoAtividades = new \Atividades\Service\AtividadeperiodoAlunoAtividades($em);

        if ($alunoatividadeId) {
            $arrDados = $serviceAlunoAtividades->getArray($alunoatividadeId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = $request->getPost()->toArray();
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);

            $ajax   = $arrDados['ajax'];
            $salvar = $serviceAlunoAtividades->save($arrDados);

            if ($salvar) {
                if ($ajax) {
                    $this->getJson()->setVariable("erro", false);
                    $this->getJson()->setVariable('message', 'Atividades Lançadas com sucesso!');

                    return $this->getJson();
                }

                $this->getView()->setVariable("erro", false);
                $this->flashMessenger()->addSuccessMessage('Atividades Lançadas com sucesso!');

                return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
            }

            if ($ajax) {
                $this->getJson()->setVariable("erro", true);
                $this->getJson()->setVariable('message', array($serviceAlunoAtividades->getLastError()));

                return $this->getJson();
            }

            $this->getView()->setVariable("erro", true);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAlunoAtividades = new \Atividades\Service\AtividadeperiodoAlunoAtividades(
                $this->getEntityManager()
            );

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceAlunoAtividades->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceAlunoAtividades->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}
