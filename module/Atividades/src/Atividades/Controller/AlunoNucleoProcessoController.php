<?php

namespace Atividades\Controller;

use VersaSpine\Controller\AbstractCoreController;

class AlunoNucleoProcessoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request                                    = $this->getRequest();
        $paramsGet                                  = $request->getQuery()->toArray();
        $paramsPost                                 = $request->getPost()->toArray();
        $serviceAtividadeperiodoAlunoNucleoProcesso = new \Atividades\Service\AtividadeperiodoAlunoNucleoProcesso(
            $this->getEntityManager()
        );

        $result = $serviceAtividadeperiodoAlunoNucleoProcesso->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAtividadeperiodoAlunoNucleoProcesso = new \Atividades\Service\AtividadeperiodoAlunoNucleoProcesso(
                $this->getEntityManager()
            );

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceAtividadeperiodoAlunoNucleoProcesso->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function editAction()
    {
        $alunoprocessoObservacoes = $this->params()->fromRoute("id", 0);

        if (!$alunoprocessoObservacoes) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($alunoprocessoObservacoes);
    }

    public function addAction($alunoprocessoObservacoes = false)
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();
        $ajax    = false;

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new JsonModel());
            $ajax = true;
        }

        $arrDados                                   = array();
        $serviceAtividadeperiodoAlunoNucleoProcesso = new \Atividades\Service\AtividadeperiodoAlunoNucleoProcesso($em);

        if ($alunoprocessoObservacoes) {
            $arrDados = $serviceAtividadeperiodoAlunoNucleoProcesso->getArray($alunoprocessoObservacoes);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = $request->getPost()->toArray();
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceAtividadeperiodoAlunoNucleoProcesso->save($dadosAtividadeperiodoAlunoNucleoProcesso);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage(
                        'Registro de atividadeperiodo__aluno_nucleo_processo salvo!'
                    );

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }
            } else {
                $this->getView()->setVariable(
                    "erro",
                    array($serviceAtividadeperiodoAlunoNucleoProcesso->getLastError())
                );
            }
        }

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAtividadeperiodoAlunoNucleoProcesso = new \Atividades\Service\AtividadeperiodoAlunoNucleoProcesso(
                $this->getEntityManager()
            );

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceAtividadeperiodoAlunoNucleoProcesso->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceAtividadeperiodoAlunoNucleoProcesso->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}
?>