<?php

namespace Atividades\Controller;

use VersaSpine\Controller\AbstractCoreController;

class AtividadegeralNucleoResponsavelController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request        = $this->getRequest();
        $paramsGet      = $request->getQuery()->toArray();
        $paramsPost     = $request->getPost()->toArray();
        $serviceAtividadegeralNucleoResponsavel = new \Atividades\Service\AtividadegeralNucleoResponsavel($this->getEntityManager());

        $result = $serviceAtividadegeralNucleoResponsavel->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceAtividadegeralNucleoResponsavel = new \Atividades\Service\AtividadegeralNucleoResponsavel($this->getEntityManager());
        $serviceAtividadegeralNucleoResponsavel->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAtividadegeralNucleoResponsavel = new \Atividades\Service\AtividadegeralNucleoResponsavel($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceAtividadegeralNucleoResponsavel->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function addAction($nucleoresponsavelId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados = array();
        $serviceAtividadegeralNucleoResponsavel = new \Atividades\Service\AtividadegeralNucleoResponsavel($this->getEntityManager());

        if ($nucleoresponsavelId) {
            $arrDados = $serviceAtividadegeralNucleoResponsavel->getArray($nucleoresponsavelId);

            if (empty($arrDados) ) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar    = $serviceAtividadegeralNucleoResponsavel->save($arrDados);


            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de atividadegeral nucleo responsável salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceAtividadegeralNucleoResponsavel->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceAtividadegeralNucleoResponsavel->getLastError());
                }
            }
        }

        $serviceAtividadegeralNucleoResponsavel->formataDadosPost($arrDados);
        $serviceAtividadegeralNucleoResponsavel->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $nucleoresponsavelId = $this->params()->fromRoute("id", 0);

        if (!$nucleoresponsavelId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($nucleoresponsavelId);
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAtividadegeralNucleoResponsavel = new \Atividades\Service\AtividadegeralNucleoResponsavel($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceAtividadegeralNucleoResponsavel->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceAtividadegeralNucleoResponsavel->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}
?>