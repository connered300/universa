<?php

namespace Atividades\Controller;

use VersaSpine\Controller\AbstractCoreController;

class AtividadePeriodoLetivoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request                       = $this->getRequest();
        $paramsGet                     = $request->getQuery()->toArray();
        $paramsPost                    = $request->getPost()->toArray();
        $serviceAtividadeperiodoLetivo = new \Atividades\Service\AtividadeperiodoLetivo($this->getEntityManager());

        $result = $serviceAtividadeperiodoLetivo->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAtividadeperiodoLetivo = new \Atividades\Service\AtividadeperiodoLetivo($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceAtividadeperiodoLetivo->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function editAction()
    {
        $atividadeperiodoFuncionamentoFim = $this->params()->fromRoute("id", 0);

        if (!$atividadeperiodoFuncionamentoFim) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($atividadeperiodoFuncionamentoFim);
    }

    public function addAction($obj)
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $service              = new \Atividades\Service\AtividadeperiodoLetivo($this->getEntityManager());
            $servicePeriodoLetivo = new \Matricula\Service\AcadperiodoLetivo($this->getEntityManager());
            $arrayData            = $request->getPost()->toArray();
            $objPeriodo           = $servicePeriodoLetivo->getRepository()->find($arrayData['dados']['perId']);
            $perDataInicio        = (array)$objPeriodo->getPerDataInicio();
            $atividadeDataInicio  = $service->formatDateAmericano($arrayData['dados']['atividadeperiodoDataInicio']);

            if ($atividadeDataInicio >= $perDataInicio['date']) {
                $retorno = $service->save($arrayData['dados']);
            }

            if ($retorno) {
                $this->json->setVariables(['type' => 'success', 'message' => "Adicionado com Sucesso!"]);
            } else {
                $this->json->setVariables(['type' => 'error', 'message' => "Não foi possível salvar o registro!"]);
            }
        }

        return $this->json;
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAtividadeperiodoLetivo = new \Atividades\Service\AtividadeperiodoLetivo($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceAtividadeperiodoLetivo->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceAtividadeperiodoLetivo->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }

    public function searchForJsonPeriodoAction()
    {
        $request    = $this->getRequest();
        $paramsGet  = $request->getQuery()->toArray();
        $paramsPost = $request->getPost()->toArray();
        $service    = new \Matricula\Service\AcadperiodoLetivo($this->getEntityManager());

        $result = $service->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function searchForJsonSecretariaAction()
    {
        $request    = $this->getRequest();
        $paramsGet  = $request->getQuery()->toArray();
        $paramsPost = $request->getPost()->toArray();
        $service    = new \Atividades\Service\AtividadegeralSecretaria($this->getEntityManager());

        $result = $service->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function searchForJsonCampusAction()
    {
        $request    = $this->getRequest();
        $paramsGet  = $request->getQuery()->toArray();
        $paramsPost = $request->getPost()->toArray();
        $service    = new \Atividades\Service\AtividadeperiodoLetivo($this->getEntityManager());

        $result = $service->pesquisaForJsonCampus(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function searchForJsonCursosAction()
    {
        $request    = $this->getRequest();
        $paramsGet  = $request->getQuery()->toArray();
        $paramsPost = $request->getPost()->toArray();
        $service    = new \Atividades\Service\AtividadeperiodoLetivo($this->getEntityManager());

        $result = $service->pesquisaForJsonCursos(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function searchForJsonPortariaAction()
    {
        $request    = $this->getRequest();
        $paramsGet  = $request->getQuery()->toArray();
        $paramsPost = $request->getPost()->toArray();
        $service    = new \Atividades\Service\AtividadegeralConfiguracoesCurso($this->getEntityManager());

        $result = $service->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function searchForJsonNucleosAction()
    {
        $request    = $this->getRequest();
        $paramsGet  = $request->getQuery()->toArray();
        $paramsPost = $request->getPost()->toArray();
        $service    = new \Atividades\Service\AtividadegeralNucleo($this->getEntityManager());

        $result = $service->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function listagemPeriodoAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $arrayData = $request->getPost()->toArray();
            $service   = new \Atividades\Service\AtividadeperiodoLetivo($this->getEntityManager());

            $result = $service->paginationAjax($arrayData);

            $this->json->setVariable("draw", $result["draw"]);
            $this->json->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->json->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->json->setVariable("data", $result["data"]);
        }

        return $this->json;
    }

    public function editarDataAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $service = new \Atividades\Service\AtividadeperiodoLetivo($this->getEntityManager());

            $arrayData = $request->getPost()->toArray();

            /** @var \Atividades\Entity\AtividadeperiodoLetivo $objPeriodo */
            $objPeriodo = $service->getRepository()->find($arrayData['perId']);

            $arrayData['perDataInicio'] = (array)$objPeriodo->getAtividadeperiodoDataInicio();

            $retorno = $service->editarData($arrayData);

            if ($retorno) {
                $this->json->setVariables(['type' => 'success', 'message' => "Atualizado com Sucesso!"]);
            } elseif (is_string($retorno)) {
                $this->json->setVariables(['type' => 'error', 'message' => $retorno]);
            } else {
                $this->json->setVariables(
                    [
                        'type'    => 'error',
                        'message' => "A nova data deve ser menor que o dia atual e maior que a data de início do período geral"
                    ]
                );
            }
        }

        return $this->json;
    }
}
?>