<?php

namespace Atividades\Controller;

use VersaSpine\Controller\AbstractCoreController;

class AtividadeperiodoEventoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request                       = $this->getRequest();
        $paramsGet                     = $request->getQuery()->toArray();
        $paramsPost                    = $request->getPost()->toArray();
        $serviceAtividadeperiodoEvento = new \Atividades\Service\AtividadeperiodoEvento($this->getEntityManager());

        $result = $serviceAtividadeperiodoEvento->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAtividadeperiodoEvento = new \Atividades\Service\AtividadeperiodoEvento($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceAtividadeperiodoEvento->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function addAction($eventoId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados                      = array();
        $serviceAtividadeperiodoEvento = new \Atividades\Service\AtividadeperiodoEvento($this->getEntityManager());
        $serviceAtividadeSecretaria    = new \Atividades\Service\AtividadegeralSecretaria($this->getEntityManager());

        if ($eventoId) {
            $arrDados = $serviceAtividadeperiodoEvento->getArray($eventoId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceAtividadeperiodoEvento->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de evento salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                    $this->getView()->setVariable("arrDados", $salvar);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceAtividadeperiodoEvento->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceAtividadeperiodoEvento->getLastError());
                }
            }

            return $this->getView();
        }

        /** @var \Atividades\Entity\AtividadegeralSecretaria $objSecretaria */
        $objSecretaria = $serviceAtividadeSecretaria->getRepository()->find($arrDados['eventoSecretaria']);

        $arrSecretariaSelect =
            [
                'id'   => $objSecretaria->getSecretariaId(),
                'text' => $objSecretaria->getSecretariaNome()
            ];

        $arrAtividadeTipoSelect = [
            'id'   => $arrDados['atividadeatividade']['id'],
            'text' => $arrDados['atividadeatividade']['text']
        ];

        $arrTipo =
            ['id' => $arrDados['eventoTipo'], 'text' => $arrDados['eventoTipo']];

        $arrDados['arrSecretariaSelect']    = $arrSecretariaSelect;
        $arrDados['arrAtividadeTipoSelect'] = $arrAtividadeTipoSelect;
        $arrDados['arrTipo']                = $arrTipo;

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $eventoId = $this->params()->fromRoute("id", 0);

        if (!$eventoId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($eventoId);
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAtividadeperiodoEvento = new \Atividades\Service\AtividadeperiodoEvento($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceAtividadeperiodoEvento->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceAtividadeperiodoEvento->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}
?>