<?php

namespace Atividades\Controller;

use VersaSpine\Controller\AbstractCoreController;
use Zend\Authentication\AuthenticationService;

class AlunoNucleoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
        parent::setEntity('Atividades\Entity\AtividadeperiodoAlunoNucleo');
        parent::setService('Atividades\Service\AtividadeperiodoAlunoNucleo');
    }

    public function searchForJsonAction()
    {
        $request            = $this->getRequest();
        $paramsGet          = $request->getQuery()->toArray();
        $paramsPost         = $request->getPost()->toArray();
        $serviceAlunoNucleo = new \Atividades\Service\AtividadeperiodoAlunoNucleo($this->getEntityManager());

        $result = $serviceAlunoNucleo->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function searchForJsonNucleoAction()
    {
        $request            = $this->getRequest();
        $paramsGet          = $request->getQuery()->toArray();
        $paramsPost         = $request->getPost()->toArray();
        $serviceAlunoNucleo = new \Atividades\Service\AtividadeperiodoAlunoNucleo($this->getEntityManager());

        $result = $serviceAlunoNucleo->pesquisaForJsonNucleo(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function searchForJsonAtividadesAction()
    {
        $request            = $this->getRequest();
        $paramsGet          = $request->getQuery()->toArray();
        $paramsPost         = $request->getPost()->toArray();
        $serviceAlunoNucleo = new \Atividades\Service\AtividadeperiodoAlunoNucleo($this->getEntityManager());

        $result = $serviceAlunoNucleo->pesquisaForJsonAtividades(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $srvAtividadeperiodoAlunoNucleo = new \Atividades\Service\AtividadeperiodoAlunoNucleo(
                $this->getEntityManager()
            );

            $dataPost = $request->getPost()->toArray();
            $result   = $srvAtividadeperiodoAlunoNucleo->getDataForDatatables($dataPost);

            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function alunoNucleosAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $srvAlunoNucleo = new \Atividades\Service\AtividadeperiodoAlunoNucleo($this->getEntityManager());

            $dataRequest = $request->getPost()->toArray();
            $result      = $srvAlunoNucleo->getAlunoNucleosInfo($dataRequest);

            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function editAction()
    {
        $alunoperiodoObservacoes = $this->params()->fromRoute("id", 0);

        if (!$alunoperiodoObservacoes) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($alunoperiodoObservacoes);
    }

    public function trocaAlunoNucleoAction(){
        $request = $this->getRequest();

        if($request->isPost()){
            $dados = $request->getPost()->toArray();
            $service = new \Atividades\Service\AtividadeperiodoAlunoNucleo($this->getEntityManager());
            $sessao = (new AuthenticationService)->getIdentity();
            $dados['usuarioAlteracao'] = $sessao['usuario'];

            $result = $service->trocaAlunoNucleo($dados);

            if(!is_string($result) && $result == true){;
                $this->json->setVariables(['type'=>'success','message'=>'Troca realizada com sucesso!']);
            }else{
                $this->json->setVariables($result = ['type'=>'error','message'=>$result]);
            }
        }

        return $this->json;
    }

    public function addAction($alunoperiodoObservacoes = false)
    {
        $em                                 = $this->getEntityManager();
        $request                            = $this->getRequest();
        $serviceAtividadeperiodoAlunoNucleo = new \Atividades\Service\AtividadeperiodoAlunoNucleo($em);

        if ($request->isPost()) {
            $arrDados = $request->getPost()->toArray();
            $salvar   = $serviceAtividadeperiodoAlunoNucleo->save($arrDados);

            if ($salvar) {
                $result['type']    = 'success';
                $result['message'] = 'Aluno inscrito com Sucesso!';
            } else {
                $result['type']    = 'error';
                $result['message'] = 'Não foi possível Inscrever o aluno!';
            }

            $this->getJson()->setVariables($result);
        }

        return $this->getJson();
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAtividadeperiodoAlunoNucleo = new \Atividades\Service\AtividadeperiodoAlunoNucleo(
                $this->getEntityManager()
            );

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceAtividadeperiodoAlunoNucleo->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceAtividadeperiodoAlunoNucleo->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }

    public function desligaAlunoNucleoAction()
    {
        $request = $this->getRequest();

        if($request->isPost()) {
            $param = $request->getPost()->toArray();
            $service = new \Atividades\Service\AtividadeperiodoAlunoNucleo($this->getEntityManager());
            $sessao = (new AuthenticationService)->getIdentity();
            $param['usuario'] = $sessao['usuario'];

            if ($param['alunonucleoId']) {
                $result = $service->desligaAluno($param);

                if (is_string($result)) {
                    $this->json->setVariables(['type' => 'error', 'message' => $result]);
                } else {
                    $this->json->setVariables(
                        ['type' => 'success', 'message' => "Desligamento concluído com sucesso!"]
                    );
                }
            }
        }
        return $this->json;
    }
}