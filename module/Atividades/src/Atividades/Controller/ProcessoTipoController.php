<?php

namespace Atividades\Controller;

use VersaSpine\Controller\AbstractCoreController;

class ProcessoTipoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request                           = $this->getRequest();
        $paramsGet                         = $request->getQuery()->toArray();
        $paramsPost                        = $request->getPost()->toArray();
        $serviceAtividadegeralProcessoTipo = new \Atividades\Service\AtividadegeralProcessoTipo(
            $this->getEntityManager()
        );

        $result = $serviceAtividadegeralProcessoTipo->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAtividadegeralProcessoTipo = new \Atividades\Service\AtividadegeralProcessoTipo(
                $this->getEntityManager()
            );

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceAtividadegeralProcessoTipo->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function editAction()
    {
        $processoTipoObservacao = $this->params()->fromRoute("id", 0);

        if (!$processoTipoObservacao) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($processoTipoObservacao);
    }

    public function addAction($processoTipoObservacao = false)
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();
        $ajax    = false;

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new JsonModel());
            $ajax = true;
        }

        $arrDados                          = array();
        $serviceAtividadegeralProcessoTipo = new \Atividades\Service\AtividadegeralProcessoTipo($em);

        if ($processoTipoObservacao) {
            $arrDados = $serviceAtividadegeralProcessoTipo->getArray($processoTipoObservacao);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = $request->getPost()->toArray();
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceAtividadegeralProcessoTipo->save($dadosAtividadegeralProcessoTipo);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage('Registro de atividadegeral__processo_tipo salvo!');

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }
            } else {
                $this->getView()->setVariable("erro", array($serviceAtividadegeralProcessoTipo->getLastError()));
            }
        }

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAtividadegeralProcessoTipo = new \Atividades\Service\AtividadegeralProcessoTipo(
                $this->getEntityManager()
            );

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceAtividadegeralProcessoTipo->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceAtividadegeralProcessoTipo->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}
?>