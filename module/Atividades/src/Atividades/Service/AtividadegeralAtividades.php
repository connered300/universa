<?php

namespace Atividades\Service;

use VersaSpine\Service\AbstractService;

class AtividadegeralAtividades extends AbstractService
{
    const PORTFOLIO = 1;
    const ATIVIDADES_PRATICAS_SUPERVISIONADAS = 2;
    const ATIVIDADES_PRATICAS_SIMULADAS = 3;

    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Atividades\Entity\AtividadegeralAtividades');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function pesquisaForJson($params)
    {
        $sql                           = 'SELECT * FROM atividadegeral__atividades WHERE';
        $atividadeatividadeObservacoes = false;
        $atividadeatividadeId          = false;

        if ($params['q']) {
            $atividadeatividadeObservacoes = $params['q'];
        } elseif ($params['query']) {
            $atividadeatividadeObservacoes = $params['query'];
        }

        if ($params['atividadeatividadeId']) {
            $atividadeatividadeId = $params['atividadeatividadeId'];
        }

        $parameters = array('atividadeatividade_observacoes' => "{$atividadeatividadeObservacoes}%");
        $sql .= ' atividadeatividade_observacoes LIKE :atividadeatividade_observacoes';

        if ($atividadeatividadeId) {
            $parameters['atividadeatividade_id'] = $atividadeatividadeId;
            $sql .= ' AND atividadeatividade_id <> :atividadeatividade_id';
        }

        $sql .= " ORDER BY atividadeatividade_observacoes";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['atividadeatividadeId']) {
                $objAtividadegeralAtividades = $this->getRepository()->find($arrDados['atividadeatividadeId']);

                if (!$objAtividadegeralAtividades) {
                    $this->setLastError('Regitro de atividade não existe!');

                    return false;
                }
            } else {
                $objAtividadegeralAtividades = new \Atividades\Entity\AtividadegeralAtividades();
            }

            $objAtividadegeralAtividades->setAtividadeatividadeDescricao($arrDados['atividadeatividadeDescricao']);
            $objAtividadegeralAtividades->setAtividadeatividadeObservacoes($arrDados['atividadeatividadeObservacoes']);

            $this->getEm()->persist($objAtividadegeralAtividades);
            $this->getEm()->flush();

            $this->getEm()->commit();

            $arrDados['atividadeatividadeId'] = $objAtividadegeralAtividades->getAtividadeatividadeId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possíel salvar o registro de atividade!');
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['atividadeatividadeDescricao']) {
            $errors[] = 'Por favor preencha o campo "atividadeatividadeDescricao"!';
        }

        if (!$arrParam['atividadeatividadeObservacoes']) {
            $errors[] = 'Por favor preencha o campo "atividadeatividadeObservacoes"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM atividadegeral__atividades";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($atividadeatividadeId)
    {
        $arrDados = $this->getRepository()->find($atividadeatividadeId);

        try {
            $arrDados = $arrDados->toArray();
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('atividadeatividadeId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Atividades\Entity\AtividadegeralAtividades */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getAtividadeatividadeId(),
                $params['value'] => $objEntity->getAtividadeatividadeObservacoes()
            );
        }

        return $arrEntitiesArr;
    }

    public function  remover($param)
    {
        if (!$param['atividadeatividadeId']) {
            $this->setLastError(
                'Para remover um registro de atividade é necessário especificar o código.'
            );

            return false;
        }

        try {
            $objAtividadegeralAtividades = $this->getRepository()->find($param['atividadeatividadeId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAtividadegeralAtividades);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de atividade.');

            return false;
        }

        return true;
    }
}
?>