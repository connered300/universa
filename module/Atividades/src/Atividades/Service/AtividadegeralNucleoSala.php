<?php

namespace Atividades\Service;

use VersaSpine\Service\AbstractService;

class AtividadegeralNucleoSala extends AbstractService
{
    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Atividade\Entity\AtividadegeralNucleoSala');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function pesquisaForJson($params)
    {
        $sql                        = 'SELECT * FROM atividadegeral__nucleo__sala WHERE';
        $salaId                     = false;
        $atividadegeralSalaNucleoId = false;

        if ($params['q']) {
            $salaId = $params['q'];
        } elseif ($params['query']) {
            $salaId = $params['query'];
        }

        if ($params['atividadegeralSalaNucleoId']) {
            $atividadegeralSalaNucleoId = $params['atividadegeralSalaNucleoId'];
        }

        $parameters = array('sala_id' => "{$salaId}%");
        $sql .= ' sala_id LIKE :sala_id';

        if ($atividadegeralSalaNucleoId) {
            $parameters['atividadegeral_sala_nucleo_id'] = $atividadegeralSalaNucleoId;
            $sql .= ' AND atividadegeral_sala_nucleo_id <> :atividadegeral_sala_nucleo_id';
        }

        $sql .= " ORDER BY sala_id";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceAtividadegeralNucleo = new \Atividades\Service\AtividadegeralNucleo($this->getEm());
        $serviceInfraSala            = new \Infraestrutura\Service\InfraSala($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['atividadegeralSalaNucleoId']) {
                $objAtividadegeralNucleoSala = $this->getRepository()->find($arrDados['atividadegeralSalaNucleoId']);

                if (!$objAtividadegeralNucleoSala) {
                    $this->setLastError('Regitro de atividadegeral__nucleo__sala não existe!');

                    return false;
                }
            } else {
                $objAtividadegeralNucleoSala = new \Atividades\Entity\AtividadegeralNucleoSala();
            }

            if ($arrDados['nucleoId']) {
                $objAtividadegeralNucleo = $serviceAtividadegeralNucleo->getRepository()->find($arrDados['nucleoId']);

                if (!$objAtividadegeralNucleo) {
                    $this->setLastError('Registro de atividadegeral__nucleo não existe!');

                    return false;
                }

                $objAtividadegeralNucleoSala->setId($objAtividadegeralNucleo);
            }

            if ($arrDados['salaId']) {
                $objInfraSala = $serviceInfraSala->getRepository()->find($arrDados['salaId']);

                if (!$objInfraSala) {
                    $this->setLastError('Registro de sala não existe!');

                    return false;
                }

                $objAtividadegeralNucleoSala->setId($objInfraSala);
            }

            $this->getEm()->persist($objAtividadegeralNucleoSala);
            $this->getEm()->flush();

            $this->getEm()->commit();

            $arrDados['atividadegeralSalaNucleoId'] = $objAtividadegeralNucleoSala->getAtividadegeralSalaNucleoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possíel salvar o registro de atividadegeral__nucleo__sala!');
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['nucleoId']) {
            $errors[] = 'Por favor preencha o campo "nucleoId"!';
        }

        if (!$arrParam['salaId']) {
            $errors[] = 'Por favor preencha o campo "salaId"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM atividadegeral__nucleo__sala";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($atividadegeralSalaNucleoId)
    {
        $arrDados                    = $this->getRepository()->find($atividadegeralSalaNucleoId);
        $serviceAtividadegeralNucleo = new \Atividades\Service\AtividadegeralNucleo($this->getEm());
        $serviceInfraSala            = new \Infraestrutura\Service\InfraSala($this->getEm());

        try {
            $arrDados = $arrDados->toArray();

            if ($arrDados['nucleoId']) {
                $arrNucleoId          = [];
                $arrDados['nucleoId'] = is_array($arrDados['nucleoId']) ? $arrDados['nucleoId'] : explode(
                    ',',
                    $arrDados['nucleoId']
                );

                foreach ($arrDados['nucleoId'] as $nucleoId) {
                    $objAtividadegeralNucleo = $serviceAtividadegeralNucleo->getRepository()->find($nucleoId);

                    if ($objAtividadegeralNucleo) {
                        $arrNucleoId[] = array(
                            'id'   => $objAtividadegeralNucleo->getNucleoId(),
                            'text' => $objAtividadegeralNucleo->getNucleoId()
                        );
                    }
                }

                $arrDados['nucleoId'] = $arrNucleoId;
            }

            if ($arrDados['salaId']) {
                $arrSalaId          = [];
                $arrDados['salaId'] = is_array($arrDados['salaId']) ? $arrDados['salaId'] : explode(
                    ',',
                    $arrDados['salaId']
                );

                foreach ($arrDados['salaId'] as $salaId) {
                    $objInfraSala = $serviceInfraSala->getRepository()->find($salaId);

                    if ($objInfraSala) {
                        $arrSalaId[] = array(
                            'id'   => $objInfraSala->getSalaId(),
                            'text' => $objInfraSala->getSalaId()
                        );
                    }
                }

                $arrDados['salaId'] = $arrSalaId;
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('atividadegeralSalaNucleoId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Atividades\Entity\AtividadegeralNucleoSala */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getAtividadegeralSalaNucleoId(),
                $params['value'] => $objEntity->getSalaId()
            );
        }

        return $arrEntitiesArr;
    }

    public function  remover($param)
    {
        if (!$param['atividadegeralSalaNucleoId']) {
            $this->setLastError(
                'Para remover um registro de atividadegeral__nucleo__sala é necessário especificar o código.'
            );

            return false;
        }

        try {
            $objAtividadegeralNucleoSala = $this->getRepository()->find($param['atividadegeralSalaNucleoId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAtividadegeralNucleoSala);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de atividadegeral__nucleo__sala.');

            return false;
        }

        return true;
    }
}
?>