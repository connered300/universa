<?php

namespace Atividades\Service;

use VersaSpine\Service\AbstractService;

class AtividadegeralNucleo extends AbstractService
{
    const NUCLEO_TIPO_INTERNO = 'interno';
    const NUCLEO_TIPO_EXTERNO = 'externo';
    const NUCLEO_DIAS_FUNCIONAMENTO_SEGUNDA = 'Segunda';
    const NUCLEO_DIAS_FUNCIONAMENTO_TERCA = 'Terca';
    const NUCLEO_DIAS_FUNCIONAMENTO_QUARTA = 'Quarta';
    const NUCLEO_DIAS_FUNCIONAMENTO_QUINTA = 'Quinta';
    const NUCLEO_DIAS_FUNCIONAMENTO_SEXTA = 'Sexta';
    const NUCLEO_DIAS_FUNCIONAMENTO_SABADO = 'Sabado';
    const NUCLEO_DIAS_FUNCIONAMENTO_DOMINGO = 'Domingo';

    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Atividades\Entity\AtividadegeralNucleo');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2NucleoTipo($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getNucleoTipo());
    }

    public static function getNucleoTipo()
    {
        return array(self::NUCLEO_TIPO_INTERNO, self::NUCLEO_TIPO_EXTERNO);
    }

    public function getArrSelect2NucleoDiasFuncionamento($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getNucleoDiasFuncionamento());
    }

    public static function getNucleoDiasFuncionamento()
    {
        return array(
            self::NUCLEO_DIAS_FUNCIONAMENTO_SEGUNDA,
            self::NUCLEO_DIAS_FUNCIONAMENTO_TERCA,
            self::NUCLEO_DIAS_FUNCIONAMENTO_QUARTA,
            self::NUCLEO_DIAS_FUNCIONAMENTO_QUINTA,
            self::NUCLEO_DIAS_FUNCIONAMENTO_SEXTA,
            self::NUCLEO_DIAS_FUNCIONAMENTO_SABADO,
            self::NUCLEO_DIAS_FUNCIONAMENTO_DOMINGO
        );
    }

    public function pesquisaForJson($params)
    {
        $sql        = 'SELECT * FROM atividadegeral__nucleo LEFT JOIN atividadeperiodo__aluno_nucleo USING(nucleo_id) WHERE ';
        $parameters = array();

        if ($params['q']) {
            $nucleoDescricao = $params['q'];
        } elseif ($params['query']) {
            $nucleoDescricao = $params['query'];
        }

        $sql .= "nucleo_descricao like '%" . $nucleoDescricao . "%'";

        if ($params['nucleoId']) {
            $parameters['nucleo_id'] = $params['nucleoId'];
            $sql .= ' AND nucleo_id = :nucleo_id';
        }

        if ($params['alunonucleoId']) {
            $parameters['alunonucleoId'] = $params['alunonucleoId'];
            $sql .= ' AND alunonucleo_id = :alunonucleoId';
        }

        $sql .= " GROUP BY nucleo_id ";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['nucleoId']) {
                $objAtividadegeralNucleo = $this->getRepository()->find($arrDados['nucleoId']);

                if (!$objAtividadegeralNucleo) {
                    $this->setLastError('Regitro de atividadegeral__nucleo não existe!');

                    return false;
                }
            } else {
                $objAtividadegeralNucleo = new \Atividades\Entity\AtividadegeralNucleo();
            }

            $objAtividadegeralNucleo->setNucleoDescricao($arrDados['nucleoDescricao']);
            $objAtividadegeralNucleo->setNucleoCapacidadeTotal($arrDados['nucleoCapacidadeTotal']);
            $objAtividadegeralNucleo->setNucleoTipo($arrDados['nucleoTipo']);
            $objAtividadegeralNucleo->setNucleoEfetivacaoInicio($arrDados['nucleoEfetivacaoInicio']);
            $objAtividadegeralNucleo->setNucleoEfetivacaoFim($arrDados['nucleoEfetivacaoFim']);
            $objAtividadegeralNucleo->setNucleoDiasFuncionamento($arrDados['nucleoDiasFuncionamento']);
            $objAtividadegeralNucleo->setNucleoFuncionamentoAbertura($arrDados['nucleoFuncionamentoAbertura']);
            $objAtividadegeralNucleo->setNucleoFuncionamentoFechamento($arrDados['nucleoFuncionamentoFechamento']);
            $objAtividadegeralNucleo->setNucleoVagas($arrDados['nucleoVagas']);

            $this->getEm()->persist($objAtividadegeralNucleo);
            $this->getEm()->flush();

            $this->getEm()->commit();

            $arrDados['nucleoId'] = $objAtividadegeralNucleo->getNucleoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possíel salvar o registro de atividadegeral__nucleo!');
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!in_array($arrParam['nucleoTipo'], self::getNucleoTipo())) {
            $errors[] = 'Por favor selecione um valor v?ido para o campo "nucleoTipo"!';
        }

        if (!array_diff($arrParam['nucleoDiasFuncionamento'], self::getNucleoDiasFuncionamento())) {
            $errors[] = 'Por favor selecione valores v?idos para o campo "nucleoDiasFuncionamento"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM atividadegeral__nucleo";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($nucleoId)
    {
        $arrDados = $this->getRepository()->find($nucleoId);

        try {
            $arrDados = $arrDados->toArray();
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('nucleoId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Atividades\Entity\AtividadegeralNucleo */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getNucleoId(),
                $params['value'] => $objEntity->getNucleoVagas()
            );
        }

        return $arrEntitiesArr;
    }

    public function  remover($param)
    {
        if (!$param['nucleoId']) {
            $this->setLastError(
                'Para remover um registro de atividadegeral__nucleo é necessário especificar o código.'
            );

            return false;
        }

        try {
            $objAtividadegeralNucleo = $this->getRepository()->find($param['nucleoId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAtividadegeralNucleo);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de atividadegeral__nucleo.');

            return false;
        }

        return true;
    }
}
?>