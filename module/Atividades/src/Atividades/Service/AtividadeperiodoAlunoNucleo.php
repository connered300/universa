<?php

namespace Atividades\Service;

use VersaSpine\Service\AbstractService;

class AtividadeperiodoAlunoNucleo extends AbstractService
{
    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Atividades\Entity\AtividadeperiodoAlunoNucleo');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function pesquisaForJson($params)
    {
        $sql                     = 'SELECT * FROM atividadeperiodo__aluno_nucleo WHERE 1';
        $alunoperiodoObservacoes = false;
        $alunonucleoId           = false;

        if ($params['q']) {
            $alunoperiodoObservacoes = $params['q'];
        } elseif ($params['query']) {
            $alunoperiodoObservacoes = $params['query'];
        }

        if ($params['alunonucleoId']) {
            $alunonucleoId = $params['alunonucleoId'];
        }

        if ($alunoperiodoObservacoes) {
            $parameters = array('alunoperiodo_observacoes' => "{$alunoperiodoObservacoes}%");
            $sql .= ' AND alunoperiodo_observacoes LIKE :alunoperiodo_observacoes';
        }

        if ($alunonucleoId) {
            $parameters['alunonucleo_id'] = $alunonucleoId;
            $sql .= ' AND alunonucleo_id = :alunonucleo_id';
        }

        $sql .= " ORDER BY alunoperiodo_observacoes";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function pesquisaForJsonNucleo($params)
    {
        $parameters = array();
        $sql        = <<<SQL
        SELECT * FROM atividadegeral__nucleo WHERE 1
SQL;

        if ($params['q']) {
            $parameters['nucleo_descricao'] = "%" . $params['q'] . "%";
            $sql .= " AND nucleo_descricao like :nucleo_descricao";
        } elseif ($params['query']) {
            $parameters['nucleo_descricao'] = "%" . $params['query'] . "%";
            $sql .= " AND nucleo_descricao like :nucleo_descricao";
        }

        if ($params['nucleosValidos'] && $params['alunoNucleoId']) {
            $parameters['alunoNucleo'] = $params['alunoNucleoId'];
            $sql .= " AND nucleo_id NOT IN (
            SELECT nucleo_id FROM atividadeperiodo__aluno_nucleo WHERE alunonucleo_id=:alunoNucleo AND alunoperiodo_status='ATIVO' GROUP BY nucleo_id
            ) ";
        }

        if ($params['nucleosValidos'] && $params['alunoperId']) {
            $parameters['alunoperId'] = $params['alunoperId'];
            $sql .= " AND nucleo_id NOT IN (
            SELECT nucleo_id FROM atividadeperiodo__aluno_nucleo WHERE alunoper_id=:alunoperId AND alunoperiodo_status='ATIVO' GROUP BY nucleo_id
            ) ";
        }

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function pesquisaForJsonAtividades($params)
    {
        $sql = <<<SQL
        SELECT
        *
        FROM atividadeperiodo__aluno_nucleo AAN
        JOIN acadperiodo__letivo USING(per_id)
        JOIN atividadegeral__nucleo USING(nucleo_id)
        JOIN acadperiodo__aluno USING(alunoper_id)
        WHERE alunocurso_id = {$params['alunocursoId']}
        AND AAN.nucleo_id = {$params['nucleoId']}
        AND per_id = {$params['perId']}
SQL;

        $result = $this->executeQuery($sql)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        $serviceAtividadeperiodoLetivo = new \Atividades\Service\AtividadeperiodoLetivo($this->getEm());
        $serviceAtividadegeralNucleo   = new \Atividades\Service\AtividadegeralNucleo($this->getEm());
        $serviceAcadperiodoAluno       = new \Matricula\Service\AcadperiodoAluno($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['alunonucleoId'] AND $arrDados['alunonucleoId'] != "false") {
                /** @var $objAtividadeperiodoAlunoNucleo \Atividades\Entity\AtividadeperiodoAlunoNucleo */
                $objAtividadeperiodoAlunoNucleo = $this->getRepository()->find($arrDados['alunonucleoId']);

                if (!$objAtividadeperiodoAlunoNucleo) {
                    $this->setLastError('Regitro de atividadeperiodo__aluno_nucleo não existe!');

                    return false;
                }

                $validaEdicao = $objAtividadeperiodoAlunoNucleo->getAlunoperiodoStatus() == "DESLIGADO" &&
                    $objAtividadeperiodoAlunoNucleo->getAlunonucleoId() != $arrDados['alunonucleoId'];

                if ($validaEdicao) {
                    $objAtividadeperiodoAlunoNucleo = new \Atividades\Entity\AtividadeperiodoAlunoNucleo();
                }
            } else {
                $objAtividadeperiodoAlunoNucleo = new \Atividades\Entity\AtividadeperiodoAlunoNucleo();
            }

            if ($arrDados['perId']) {
                $objAtividadeperiodoLetivo = $serviceAtividadeperiodoLetivo->getRepository()->find($arrDados['perId']);

                if (!$objAtividadeperiodoLetivo) {
                    $this->setLastError('Registro de atividadeperiodo__letivo não existe!');

                    return false;
                }

                $objAtividadeperiodoAlunoNucleo->setPer($objAtividadeperiodoLetivo);
            }

            if ($arrDados['nucleoId']) {
                $objAtividadegeralNucleo = $serviceAtividadegeralNucleo->getRepository()->find($arrDados['nucleoId']);

                if (!$objAtividadegeralNucleo) {
                    $this->setLastError('Registro de atividadegeral__nucleo não existe!');

                    return false;
                }

                $objAtividadeperiodoAlunoNucleo->setNucleo($objAtividadegeralNucleo);
            }

            if ($arrDados['alunoperId']) {
                $objAcadperiodoAluno = $serviceAcadperiodoAluno->getRepository()->find($arrDados['alunoperId']);

                if (!$objAcadperiodoAluno) {
                    $this->setLastError('Registro de aluno não existe!');

                    return false;
                }

                $objAtividadeperiodoAlunoNucleo->setAlunoper($objAcadperiodoAluno);
            }

            $objAtividadeperiodoAlunoNucleo->setAlunoperiodoDataInscricao(new \DateTime());
            $objAtividadeperiodoAlunoNucleo->setAlunoperiodoStatus("ATIVO");
            $objAtividadeperiodoAlunoNucleo->setAlunoperiodoDataFechamento(null);

            $this->getEm()->persist($objAtividadeperiodoAlunoNucleo);
            $this->getEm()->flush();

            $this->getEm()->commit();

            $arrDados['alunonucleoId'] = $objAtividadeperiodoAlunoNucleo->getAlunonucleoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError(
                'Não foi possíel salvar o registro de atividadeperiodo__aluno_nucleo!<br>' . $e->getMessage()
            );
        }

        return false;
    }

    public function validaAluno($param)
    {
        $message = null;

        if ($param['nucleoId'] == null) {
            $message .= "NucleoId está vazio";
        } elseif ($param['alunonucleoId'] == 0) {
            $message .= "AlunonucleoId está vazio";
        }

        if ($message) {
            return $message;
        } else {
            return true;
        }
    }

    public function trocaAlunoNucleo($param)
    {
        if (is_string($this->validaAluno($param))) {
            return $this->validaAluno($param);
        }
        /** @var \Atividades\Entity\AtividadeperiodoAlunoNucleo $objAlunoNucleo */
        $objAlunoNucleo = $this->getRepository('Atividades\Entity\AtividadeperiodoAlunoNucleo')->find(
            $param['alunonucleoId']
        );

        try {
            $arrayAlunoNucleo               = $objAlunoNucleo->toArray();
            $arrayAlunoNucleo['nucleoId']   = $param['nucleoId'];
            $arrayAlunoNucleo['perId']      = $objAlunoNucleo->getPer()->getPer()->getPerId();
            $arrayAlunoNucleo['alunoperId'] = $objAlunoNucleo->getAlunoper()->getAlunoperId();

            return $this->save($arrayAlunoNucleo);
        } catch (\Exception $e) {
            return false;
        }
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['perId']) {
            $errors[] = 'Por favor preencha o campo "perId"!';
        }

        if (!$arrParam['nucleoId']) {
            $errors[] = 'Por favor preencha o campo "nucleoId"!';
        }

        if (!$arrParam['alunoperId']) {
            $errors[] = 'Por favor preencha o campo "alunoperId"!';
        }

        if (!$arrParam['alunoperiodoDataInscricao']) {
            $errors[] = 'Por favor preencha o campo "alunoperiodoDataInscricao"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $periodosCorrente = (new \Matricula\Service\AcadperiodoLetivo($this->getEm()))->buscaPeriodoCorrente(true, true);

        $query = "SELECT
                    @nucleos_descricao:= IFNULL((SELECT
                                GROUP_CONCAT(nucleo_descricao)
                            FROM
                                atividadeperiodo__aluno_nucleo
                            INNER JOIN atividadegeral__nucleo USING (nucleo_id)
                            WHERE
                                atividadeperiodo__aluno_nucleo.alunoper_id = acadperiodo__aluno.alunoper_id), 'Sem Inscrição') nucleos,
                         @nucleos_descricao nucleo_descricao,
                         aluno_n.alunonucleo_id,
                    TRIM(LEADING 0 FROM acadperiodo__aluno.alunocurso_id) alunocurso_id,
                    pes_nome,
                    IFNULL((
                        SELECT
                            portfolioaluno_situacao
                        FROM
                            atividadeperiodo__aluno_portfolio aluno_p
                        WHERE
                            aluno_p.alunonucleo_id = aluno_n.alunonucleo_id
                        ORDER BY portfolio_id DESC LIMIT 0, 1
                    ),'Sem Registro') as status_entrega_portifolio,
                    IFNULL((
                        SELECT
                            if(alunoatividade_situacao = 'Inconcluso','Não Concluido', alunoatividade_situacao) alunoatividade_situacao
                        FROM
                            atividadeperiodo__aluno_sumarizador aluno_atv_sum
                        WHERE
                            aluno_atv_sum.alunonucleo_id = aluno_n.alunonucleo_id
                        ORDER BY alunoatividade_id DESC LIMIT 0, 1
                    ),'Não Concluido') as status_disciplinaconclusao,
                    IFNULL((
                        SELECT
                            SUM(alunoatividade_horas)
                        FROM
                            acadperiodo__aluno a
                            INNER JOIN
                            atividadeperiodo__aluno_nucleo USING (alunoper_id)
                            INNER JOIN
                            atividadeperiodo__aluno_atividades aluno_a USING (alunonucleo_id)
                        WHERE
                            a.alunoper_id = acadperiodo__aluno.alunoper_id
                    ),0) total_horas,
                    TRIM(LEADING 0 FROM acadperiodo__aluno.alunoper_id) matricula,
                    acadgeral__situacao.matsit_descricao,
                    acadperiodo__turma.turma_nome,
                    CONCAT(p.per_ano,' / ',p.per_semestre) periodo,
                    p.per_id PP,
                    if(curdate() > apl.atividadeperiodo_data_fechamento,'not','yes') resultado,
                    acad_curso.curso_nome curso_nome
               FROM
                    acadperiodo__aluno
                INNER JOIN
                    acadperiodo__turma ON acadperiodo__aluno.turma_id = acadperiodo__turma.turma_id
                 INNER JOIN
                    acadgeral__aluno_curso ON acadperiodo__aluno.alunocurso_id = acadgeral__aluno_curso.alunocurso_id
				LEFT JOIN
                acadgeral__aluno on acadgeral__aluno.aluno_id = acadgeral__aluno_curso.aluno_id
                LEFT JOIN
                    acadperiodo__letivo p on p.per_id = acadperiodo__turma.per_id
                LEFT JOIN
                    atividadeperiodo__letivo apl on p.per_id = apl.per_id
                INNER JOIN
                    pessoa ON acadgeral__aluno.pes_id = pessoa.pes_id
                LEFT JOIN
                    atividadeperiodo__aluno_nucleo aluno_n ON acadperiodo__aluno.alunoper_id = aluno_n.alunoper_id
                LEFT JOIN
                    atividadegeral__nucleo nucleo ON aluno_n.nucleo_id = nucleo.nucleo_id
                INNER JOIN
					atividadegeral__configuracoes_curso  configuracoesCurso on configuracoesCurso.cursocampus_id =  acadgeral__aluno_curso.cursocampus_id
                INNER JOIN
                    acadgeral__situacao ON acadperiodo__aluno.matsituacao_id = acadgeral__situacao.situacao_id
                INNER JOIN
                    campus_curso ON campus_curso.cursocampus_id = acadgeral__aluno_curso.cursocampus_id
                INNER JOIN
                    acad_curso ON acad_curso.curso_id = campus_curso.curso_id
                WHERE
                    acadperiodo__aluno.matsituacao_id = 1
                        AND acadperiodo__turma.per_id IN ($periodosCorrente)
                        AND FIND_IN_SET(acadperiodo__turma.turma_serie, (SELECT
                            atividadeconf_serie
                        FROM
                            atividadegeral__configuracoes
                        JOIN atividadegeral__configuracoes_curso c USING (atividadeconf_portaria)
                        INNER JOIN atividadeperiodo__letivo ON atividadegeral__configuracoes.atividadeconf_portaria = c.atividadeconf_portaria
                            AND atividadeperiodo__letivo.atividadeconfigcurso_id = c.atividadeconfigcurso_id
                        WHERE
                            atividadeperiodo__letivo.per_id = acadperiodo__turma.per_id
                                AND c.cursocampus_id = acadperiodo__turma.cursocampus_id
                        GROUP BY atividadeconf_serie))
                GROUP BY acadperiodo__aluno.alunocurso_id";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getAlunoNucleosInfo($data)
    {
        $nuceloTipoInterno = \Atividades\Service\AtividadegeralNucleo::NUCLEO_TIPO_INTERNO;
        $nuceloTipoExterno = \Atividades\Service\AtividadegeralNucleo::NUCLEO_TIPO_EXTERNO;

        $query = <<<SQL
            (SELECT
               nucleo.nucleo_id,
               (SELECT
                       aluno_nucleo.alunonucleo_id
                   FROM
                     acadgeral__aluno_curso
                    INNER JOIN
                        acadperiodo__aluno aluno_periodo USING (alunocurso_id)
                    INNER JOIN
                       atividadeperiodo__aluno_nucleo aluno_nucleo ON aluno_periodo.alunoper_id = aluno_nucleo.alunoper_id
                           AND aluno_nucleo.alunoperiodo_status = 'ATIVO'
                           INNER JOIN
                       (SELECT
                           atividadeperiodo__letivo.*
                       FROM
                           atividadeperiodo__letivo
                       WHERE
                           atividadeperiodo_data_fechamento > NOW()) periodo ON periodo.per_id = aluno_nucleo.per_id
                            INNER JOIN
                        atividadegeral__configuracoes_curso ON atividadegeral__configuracoes_curso.cursocampus_id = acadgeral__aluno_curso.cursocampus_id and atividadegeral__configuracoes_curso.atividadeconfigcurso_id = periodo.atividadeconfigcurso_id
                     WHERE
                       aluno_periodo.alunocurso_id = {$data['matricula']}
                           AND aluno_nucleo.nucleo_id = nucleo.nucleo_id  and alunoperiodo_status = 'Ativo') alunonucleo_id,
               nucleo.nucleo_descricao nucleo,
               (SELECT
                       IFNULL(SUM(alunoatividade_horas), 0)
                   FROM
                       acadperiodo__aluno
                           LEFT JOIN
                       atividadeperiodo__aluno_nucleo ON atividadeperiodo__aluno_nucleo.alunoper_id = acadperiodo__aluno.alunoper_id
                           LEFT JOIN
                       atividadeperiodo__aluno_atividades ON atividadeperiodo__aluno_atividades.alunonucleo_id = atividadeperiodo__aluno_nucleo.alunonucleo_id
                   WHERE
                       nucleo.nucleo_id = atividadeperiodo__aluno_nucleo.nucleo_id
                           AND alunocurso_id = {$data['matricula']}) total_horas,
                 (SELECT
                               aluno_nucleo.alunoperiodo_status
                           FROM
                               acadperiodo__aluno aluno_periodo
                                   INNER JOIN
                               atividadeperiodo__aluno_nucleo aluno_nucleo ON aluno_periodo.alunoper_id = aluno_nucleo.alunoper_id
                                   LEFT JOIN
                               (SELECT
                per_id
            FROM
                atividadeperiodo__letivo
                    INNER JOIN
                acadperiodo__letivo USING (per_id)
            WHERE
                atividadeperiodo_data_fechamento > NOW()
                    AND atividadeperiodo_data_fechamento IS NOT NULL
                    AND per_data_fim > NOW()
                    AND per_data_inicio < NOW()) periodo ON periodo.per_id = aluno_nucleo.per_id
                           WHERE
                               aluno_periodo.alunocurso_id = {$data['matricula']}
                                   AND alunoperiodo_status = 'Ativo'
                                   and periodo.per_id IS NOT NULL
                                   AND aluno_nucleo.nucleo_id = nucleo.nucleo_id
                           LIMIT 1)situacao_periodo
            FROM
               atividadegeral__nucleo nucleo
                   CROSS JOIN
               atividadeperiodo__aluno_nucleo aluno_n
                   LEFT JOIN
               acadperiodo__aluno USING (alunoper_id)
                   LEFT JOIN
               (SELECT
                   MAX(per_id), atividadeperiodo__letivo.*
               FROM
                   atividadeperiodo__letivo
               WHERE
                   atividadeperiodo_data_fechamento > NOW()
                   ) periodo ON periodo.per_id = aluno_n.per_id
            WHERE
               alunocurso_id = {$data['matricula']}
                   AND nucleo.nucleo_tipo = 'interno'
            GROUP BY nucleo.nucleo_id)

            UNION

            (SELECT
            nucleo.nucleo_id,
            aluno_n.alunonucleo_id,
            'Atividades Externas e Convênios' nucleo,
            IFNULL(SUM(aluno_a.alunoatividade_horas), 0) total_horas,
            alunoperiodo_status situacao_periodo
            FROM
            atividadegeral__nucleo nucleo
            LEFT JOIN
            acadperiodo__aluno alunoper
            LEFT JOIN
            atividadeperiodo__aluno_nucleo aluno_n ON aluno_n.alunoper_id = alunoper.alunoper_id AND aluno_n.alunoperiodo_status = "ATIVO" ON nucleo.nucleo_id = aluno_n.nucleo_id and nucleo_tipo = 'externo'
            LEFT JOIN
            atividadeperiodo__aluno_atividades aluno_a ON aluno_n.alunonucleo_id = aluno_a.alunonucleo_id
            LEFT JOIN
              (SELECT
                    atividadeperiodo__letivo.*
                FROM
                    atividadeperiodo__letivo
                WHERE
                    (atividadeperiodo_data_fechamento > NOW()
                        OR atividadeperiodo_data_fechamento IS NOT NULL)
                ORDER BY per_id DESC
                LIMIT 1)periodo ON periodo.per_id = aluno_n.per_id WHERE alunoper.alunocurso_id = {$data['matricula']})

SQL;

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($alunonucleoId)
    {
        $arrDados                      = $this->getRepository()->find($alunonucleoId);
        $serviceAtividadeperiodoLetivo = new \Atividades\Service\AtividadeperiodoLetivo($this->getEm());
        $serviceAtividadegeralNucleo   = new \Atividades\Service\AtividadegeralNucleo($this->getEm());
        $serviceAcadperiodoAluno       = new \Matricula\Service\AcadperiodoAluno($this->getEm());

        try {
            $arrDados = $arrDados->toArray();

            if ($arrDados['perId']) {
                $arrPerId          = [];
                $arrDados['perId'] = is_array($arrDados['perId']) ? $arrDados['perId'] : explode(
                    ',',
                    $arrDados['perId']
                );

                foreach ($arrDados['perId'] as $perId) {
                    $objAtividadeperiodoLetivo = $serviceAtividadeperiodoLetivo->getRepository()->find($perId);

                    if ($objAtividadeperiodoLetivo) {
                        $arrPerId[] = array(
                            'id'   => $objAtividadeperiodoLetivo->getPerId(),
                            'text' => $objAtividadeperiodoLetivo->getPerId()
                        );
                    }
                }

                $arrDados['perId'] = $arrPerId;
            }

            if ($arrDados['nucleoId']) {
                $arrNucleoId          = [];
                $arrDados['nucleoId'] = is_array($arrDados['nucleoId']) ? $arrDados['nucleoId'] : explode(
                    ',',
                    $arrDados['nucleoId']
                );

                foreach ($arrDados['nucleoId'] as $nucleoId) {
                    $objAtividadegeralNucleo = $serviceAtividadegeralNucleo->getRepository()->find($nucleoId);

                    if ($objAtividadegeralNucleo) {
                        $arrNucleoId[] = array(
                            'id'   => $objAtividadegeralNucleo->getNucleoId(),
                            'text' => $objAtividadegeralNucleo->getNucleoId()
                        );
                    }
                }

                $arrDados['nucleoId'] = $arrNucleoId;
            }

            if ($arrDados['alunoperId']) {
                $arrAlunoperId          = [];
                $arrDados['alunoperId'] = is_array($arrDados['alunoperId']) ? $arrDados['alunoperId'] : explode(
                    ',',
                    $arrDados['alunoperId']
                );

                foreach ($arrDados['alunoperId'] as $alunoperId) {
                    $objAcadperiodoAluno = $serviceAcadperiodoAluno->getRepository()->find($alunoperId);

                    if ($objAcadperiodoAluno) {
                        $arrAlunoperId[] = array(
                            'id'   => $objAcadperiodoAluno->getAlunoperId(),
                            'text' => $objAcadperiodoAluno->getAlunoperId()
                        );
                    }
                }

                $arrDados['alunoperId'] = $arrAlunoperId;
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('alunonucleoId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Atividades\Entity\AtividadeperiodoAlunoNucleo */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getAlunonucleoId(),
                $params['value'] => $objEntity->getAlunoperiodoObservacoes()
            );
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['alunonucleoId']) {
            $this->setLastError(
                'Para remover um registro de atividadeperiodo__aluno_nucleo é necessário especificar o código.'
            );

            return false;
        }

        try {
            $objAtividadeperiodoAlunoNucleo = $this->getRepository()->find($param['alunonucleoId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAtividadeperiodoAlunoNucleo);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de atividadeperiodo__aluno_nucleo.');

            return false;
        }

        return true;
    }

    public function editarHoras($params)
    {
        $sql = <<<SQL
          UPDATE
            atividadeperiodo__aluno_atividades
          SET
            alunoatividade_horas = {$params['alunoatividade_horas']}
          WHERE
            atividadealuno_atividade_id = {$params['alunoatividadeId']}
SQL;
        try {
            $this->exec($sql);

            return true;
        } catch (\Exception $e) {
        }

        return false;
    }

    public function desligaAluno($param)
    {
        if (!$param['alunonucleoId']) {
            return "AlunonucleoId está vazio";
        }

        try {
            $this->exec(
                "UPDATE atividadeperiodo__aluno_nucleo SET alunoperiodo_status = 'DESLIGADO', alunoperiodo_data_fechamento = NOW(), usuario_alteracao = {$param['usuario']} where alunonucleo_id = {$param['alunonucleoId']}"
            );

            return true;
        } catch (\Exception $e) {
        }

        return false;
    }
}
?>