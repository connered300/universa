<?php

namespace Atividades\Service;

use VersaSpine\Service\AbstractService;

class AtividadegeralProcesso extends AbstractService
{
    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Atividades\Entity\AtividadegeralProcesso');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function pesquisaForJson($params)
    {
        $sql                      = 'SELECT * FROM atividadegeral__processo WHERE';
        $atividadeProcessoDataFim = false;
        $atividadeProcessoId      = false;

        if ($params['q']) {
            $atividadeProcessoDataFim = $params['q'];
        } elseif ($params['query']) {
            $atividadeProcessoDataFim = $params['query'];
        }

        if ($params['atividadeProcessoId']) {
            $atividadeProcessoId = $params['atividadeProcessoId'];
        }

        $parameters = array('atividade_processo_data_fim' => "{$atividadeProcessoDataFim}%");
        $sql .= ' atividade_processo_data_fim LIKE :atividade_processo_data_fim';

        if ($atividadeProcessoId) {
            $parameters['atividade_processo_id'] = $atividadeProcessoId;
            $sql .= ' AND atividade_processo_id <> :atividade_processo_id';
        }

        $sql .= " ORDER BY atividade_processo_data_fim";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceAtividadegeralNucleoProcesso = new \Atividades\Service\AtividadegeralNucleoProcesso($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['atividadeProcessoId']) {
                $objAtividadegeralProcesso = $this->getRepository()->find($arrDados['atividadeProcessoId']);

                if (!$objAtividadegeralProcesso) {
                    $this->setLastError('Regitro de atividadegeral__processo não existe!');

                    return false;
                }
            } else {
                $objAtividadegeralProcesso = new \Atividades\Entity\AtividadegeralProcesso();
            }

            if ($arrDados['nucleoProcessoId']) {
                $objAtividadegeralNucleoProcesso = $serviceAtividadegeralNucleoProcesso->getRepository()->find(
                    $arrDados['nucleoProcessoId']
                );

                if (!$objAtividadegeralNucleoProcesso) {
                    $this->setLastError('Registro de atividadegeral__nucleo_processo não existe!');

                    return false;
                }

                $objAtividadegeralProcesso->setId($objAtividadegeralNucleoProcesso);
            }
            $objAtividadegeralProcesso->setAtividadeProcessoDescricao($arrDados['atividadeProcessoDescricao']);
            $objAtividadegeralProcesso->setAtividadeProcessoDataInicio($arrDados['atividadeProcessoDataInicio']);
            $objAtividadegeralProcesso->setAtividadeProcessoDataFim($arrDados['atividadeProcessoDataFim']);

            $this->getEm()->persist($objAtividadegeralProcesso);
            $this->getEm()->flush();

            $this->getEm()->commit();

            $arrDados['atividadeProcessoId'] = $objAtividadegeralProcesso->getAtividadeProcessoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possíel salvar o registro de atividadegeral__processo!');
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['nucleoProcessoId']) {
            $errors[] = 'Por favor preencha o campo "nucleoProcessoId"!';
        }

        if (!$arrParam['atividadeProcessoDescricao']) {
            $errors[] = 'Por favor preencha o campo "atividadeProcessoDescricao"!';
        }

        if (!$arrParam['atividadeProcessoDataInicio']) {
            $errors[] = 'Por favor preencha o campo "atividadeProcessoDataInicio"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM atividadegeral__processo";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($atividadeProcessoId)
    {
        $arrDados                            = $this->getRepository()->find($atividadeProcessoId);
        $serviceAtividadegeralNucleoProcesso = new \Atividades\Service\AtividadegeralNucleoProcesso($this->getEm());

        try {
            $arrDados = $arrDados->toArray();

            if ($arrDados['nucleoProcessoId']) {
                $arrNucleoProcessoId          = [];
                $arrDados['nucleoProcessoId'] = is_array(
                    $arrDados['nucleoProcessoId']
                ) ? $arrDados['nucleoProcessoId'] : explode(',', $arrDados['nucleoProcessoId']);

                foreach ($arrDados['nucleoProcessoId'] as $nucleoProcessoId) {
                    $objAtividadegeralNucleoProcesso = $serviceAtividadegeralNucleoProcesso->getRepository()->find(
                        $nucleoProcessoId
                    );

                    if ($objAtividadegeralNucleoProcesso) {
                        $arrNucleoProcessoId[] = array(
                            'id'   => $objAtividadegeralNucleoProcesso->getNucleoProcessoId(),
                            'text' => $objAtividadegeralNucleoProcesso->getNucleoProcessoId()
                        );
                    }
                }

                $arrDados['nucleoProcessoId'] = $arrNucleoProcessoId;
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('atividadeProcessoId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Atividades\Entity\AtividadegeralProcesso */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getAtividadeProcessoId(),
                $params['value'] => $objEntity->getAtividadeProcessoDataFim()
            );
        }

        return $arrEntitiesArr;
    }

    public function  remover($param)
    {
        if (!$param['atividadeProcessoId']) {
            $this->setLastError(
                'Para remover um registro de atividadegeral__processo é necessário especificar o código.'
            );

            return false;
        }

        try {
            $objAtividadegeralProcesso = $this->getRepository()->find($param['atividadeProcessoId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAtividadegeralProcesso);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de atividadegeral__processo.');

            return false;
        }

        return true;
    }
}
?>