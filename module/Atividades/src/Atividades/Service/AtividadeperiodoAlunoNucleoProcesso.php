<?php

namespace Atividades\Service;

use VersaSpine\Service\AbstractService;

class AtividadeperiodoAlunoNucleoProcesso extends AbstractService
{
    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Atividade\Entity\AtividadeperiodoAlunoNucleoProcesso');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function pesquisaForJson($params)
    {
        $sql                      = 'SELECT * FROM atividadeperiodo__aluno_nucleo_processo WHERE';
        $alunoprocessoObservacoes = false;
        $alunoprocessoId          = false;

        if ($params['q']) {
            $alunoprocessoObservacoes = $params['q'];
        } elseif ($params['query']) {
            $alunoprocessoObservacoes = $params['query'];
        }

        if ($params['alunoprocessoId']) {
            $alunoprocessoId = $params['alunoprocessoId'];
        }

        $parameters = array('alunoprocesso_observacoes' => "{$alunoprocessoObservacoes}%");
        $sql .= ' alunoprocesso_observacoes LIKE :alunoprocesso_observacoes';

        if ($alunoprocessoId) {
            $parameters['alunoprocesso_id'] = $alunoprocessoId;
            $sql .= ' AND alunoprocesso_id <> :alunoprocesso_id';
        }

        $sql .= " ORDER BY alunoprocesso_observacoes";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceAtividadeperiodoAlunoNucleo = new \Atividades\Service\AtividadeperiodoAlunoNucleo($this->getEm());
        $serviceAtividadegeralProcesso      = new \Atividades\Service\AtividadegeralProcesso($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['alunoprocessoId']) {
                $objAtividadeperiodoAlunoNucleoProcesso = $this->getRepository()->find($arrDados['alunoprocessoId']);

                if (!$objAtividadeperiodoAlunoNucleoProcesso) {
                    $this->setLastError('Regitro de atividadeperiodo__aluno_nucleo_processo não existe!');

                    return false;
                }
            } else {
                $objAtividadeperiodoAlunoNucleoProcesso = new \Atividades\Entity\AtividadeperiodoAlunoNucleoProcesso();
            }

            if ($arrDados['alunonucleoId']) {
                $objAtividadeperiodoAlunoNucleo = $serviceAtividadeperiodoAlunoNucleo->getRepository()->find(
                    $arrDados['alunonucleoId']
                );

                if (!$objAtividadeperiodoAlunoNucleo) {
                    $this->setLastError('Registro de atividadeperiodo__aluno_nucleo não existe!');

                    return false;
                }

                $objAtividadeperiodoAlunoNucleoProcesso->setId($objAtividadeperiodoAlunoNucleo);
            }

            if ($arrDados['atividadeProcessoId']) {
                $objAtividadegeralProcesso = $serviceAtividadegeralProcesso->getRepository()->find(
                    $arrDados['atividadeProcessoId']
                );

                if (!$objAtividadegeralProcesso) {
                    $this->setLastError('Registro de atividadegeral__processo não existe!');

                    return false;
                }

                $objAtividadeperiodoAlunoNucleoProcesso->setId($objAtividadegeralProcesso);
            }
            $objAtividadeperiodoAlunoNucleoProcesso->setAlunoprocessoDataInicio($arrDados['alunoprocessoDataInicio']);
            $objAtividadeperiodoAlunoNucleoProcesso->setAlunoprocessoDataFim($arrDados['alunoprocessoDataFim']);
            $objAtividadeperiodoAlunoNucleoProcesso->setAlunoprocessoObservacoes($arrDados['alunoprocessoObservacoes']);

            $this->getEm()->persist($objAtividadeperiodoAlunoNucleoProcesso);
            $this->getEm()->flush();

            $this->getEm()->commit();

            $arrDados['alunoprocessoId'] = $objAtividadeperiodoAlunoNucleoProcesso->getAlunoprocessoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possíel salvar o registro de atividadeperiodo__aluno_nucleo_processo!');
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['alunonucleoId']) {
            $errors[] = 'Por favor preencha o campo "alunonucleoId"!';
        }

        if (!$arrParam['atividadeProcessoId']) {
            $errors[] = 'Por favor preencha o campo "atividadeProcessoId"!';
        }

        if (!$arrParam['alunoprocessoDataInicio']) {
            $errors[] = 'Por favor preencha o campo "alunoprocessoDataInicio"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM atividadeperiodo__aluno_nucleo_processo";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($alunoprocessoId)
    {
        $arrDados                           = $this->getRepository()->find($alunoprocessoId);
        $serviceAtividadeperiodoAlunoNucleo = new \Atividades\Service\AtividadeperiodoAlunoNucleo($this->getEm());
        $serviceAtividadegeralProcesso      = new \Atividades\Service\AtividadegeralProcesso($this->getEm());

        try {
            $arrDados = $arrDados->toArray();

            if ($arrDados['alunonucleoId']) {
                $arrAlunonucleoId          = [];
                $arrDados['alunonucleoId'] = is_array(
                    $arrDados['alunonucleoId']
                ) ? $arrDados['alunonucleoId'] : explode(',', $arrDados['alunonucleoId']);

                foreach ($arrDados['alunonucleoId'] as $alunonucleoId) {
                    $objAtividadeperiodoAlunoNucleo = $serviceAtividadeperiodoAlunoNucleo->getRepository()->find(
                        $alunonucleoId
                    );

                    if ($objAtividadeperiodoAlunoNucleo) {
                        $arrAlunonucleoId[] = array(
                            'id'   => $objAtividadeperiodoAlunoNucleo->getAlunonucleoId(),
                            'text' => $objAtividadeperiodoAlunoNucleo->getAlunonucleoId()
                        );
                    }
                }

                $arrDados['alunonucleoId'] = $arrAlunonucleoId;
            }

            if ($arrDados['atividadeProcessoId']) {
                $arrAtividadeProcessoId          = [];
                $arrDados['atividadeProcessoId'] = is_array(
                    $arrDados['atividadeProcessoId']
                ) ? $arrDados['atividadeProcessoId'] : explode(',', $arrDados['atividadeProcessoId']);

                foreach ($arrDados['atividadeProcessoId'] as $atividadeProcessoId) {
                    $objAtividadegeralProcesso = $serviceAtividadegeralProcesso->getRepository()->find(
                        $atividadeProcessoId
                    );

                    if ($objAtividadegeralProcesso) {
                        $arrAtividadeProcessoId[] = array(
                            'id'   => $objAtividadegeralProcesso->getAtividadeProcessoId(),
                            'text' => $objAtividadegeralProcesso->getAtividadeProcessoId()
                        );
                    }
                }

                $arrDados['atividadeProcessoId'] = $arrAtividadeProcessoId;
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('alunoprocessoId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Atividades\Entity\AtividadeperiodoAlunoNucleoProcesso */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getAlunoprocessoId(),
                $params['value'] => $objEntity->getAlunoprocessoObservacoes()
            );
        }

        return $arrEntitiesArr;
    }

    public function  remover($param)
    {
        if (!$param['alunoprocessoId']) {
            $this->setLastError(
                'Para remover um registro de atividadeperiodo__aluno_nucleo_processo é necessário especificar o código.'
            );

            return false;
        }

        try {
            $objAtividadeperiodoAlunoNucleoProcesso = $this->getRepository()->find($param['alunoprocessoId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAtividadeperiodoAlunoNucleoProcesso);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de atividadeperiodo__aluno_nucleo_processo.');

            return false;
        }

        return true;
    }
}
?>