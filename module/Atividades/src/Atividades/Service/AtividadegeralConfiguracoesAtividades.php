<?php

namespace Atividades\Service;

use VersaSpine\Service\AbstractService;

class AtividadegeralConfiguracoesAtividades extends AbstractService
{
    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Atividades\Entity\AtividadegeralConfiguracoesAtividades');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function pesquisaForJson($params)
    {
        $sql                       = 'SELECT * FROM atividadegeral__configuracoes_atividades WHERE 1';
        $atividadeconfPontuacaoMin = false;
        $atividadeatividadeId      = false;

        $atividadeconfPontuacaoMin = ($params['q']) ? $params['q'] : $params['query'];

        if ($params['atividadeatividadeId']) {
            $sql .= ' AND atividadeatividade_id = :atividadeatividade_id';
            $parameters['atividadeatividade_id'] = $params['atividadeatividadeId'];
        }

        if ($atividadeconfPontuacaoMin) {
            $sql .= ' AND atividadeconf_pontuacao_min LIKE :atividadeconf_pontuacao_min';
            $parameters['atividadeconf_pontuacao_min'] = "$atividadeconfPontuacaoMin%";
        }

        $sql .= " ORDER BY atividadeconf_pontuacao_min";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceAtividadegeralConfiguracoes = new \Atividades\Service\AtividadegeralConfiguracoes($this->getEm());
        $serviceAtividadegeralAtividades    = new \Atividades\Service\AtividadegeralAtividades($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['atividadeconfatividadeId']) {
                $objAtividadegeralConfiguracoesAtividades = $this->getRepository()->find(
                    $arrDados['atividadeconfatividadeId']
                );

                if (!$objAtividadegeralConfiguracoesAtividades) {
                    $this->setLastError('Regitro de atividadegeral__configuracoes_atividades não existe!');

                    return false;
                }
            } else {
                $objAtividadegeralConfiguracoesAtividades = new \Atividades\Entity\AtividadegeralConfiguracoesAtividades(
                );
            }

            if ($arrDados['atividadeconfPortaria']) {
                $objAtividadegeralConfiguracoes = $serviceAtividadegeralConfiguracoes->getRepository()->find(
                    $arrDados['atividadeconfPortaria']
                );

                if (!$objAtividadegeralConfiguracoes) {
                    $this->setLastError('Registro de atividadegeral__configuracoes não existe!');

                    return false;
                }

                $objAtividadegeralConfiguracoesAtividades->setAtividadeconfPortaria($objAtividadegeralConfiguracoes);
            }

            if ($arrDados['atividadeatividadeId']) {
                $objAtividadegeralAtividades = $serviceAtividadegeralAtividades->getRepository()->find(
                    $arrDados['atividadeatividadeId']
                );

                if (!$objAtividadegeralAtividades) {
                    $this->setLastError('Registro de atividadegeral__atividades não existe!');

                    return false;
                }

                $objAtividadegeralConfiguracoesAtividades->setId($objAtividadegeralAtividades);
            }
            $objAtividadegeralConfiguracoesAtividades->setAtividadeconfMinimoDisciplina(
                $arrDados['atividadeconfMinimoDisciplina']
            );
            $objAtividadegeralConfiguracoesAtividades->setAtividadegeralHorasPraticasMax(
                $arrDados['atividadegeralHorasPraticasMax']
            );
            $objAtividadegeralConfiguracoesAtividades->setAtividadeconfHorasPraticasMin(
                $arrDados['atividadeconfHorasPraticasMin']
            );
            $objAtividadegeralConfiguracoesAtividades->setAtividadeconfPontuacaoMax(
                $arrDados['atividadeconfPontuacaoMax']
            );
            $objAtividadegeralConfiguracoesAtividades->setAtividadeconfPontuacaoMin(
                $arrDados['atividadeconfPontuacaoMin']
            );

            $this->getEm()->persist($objAtividadegeralConfiguracoesAtividades);
            $this->getEm()->flush();

            $this->getEm()->commit();

            $arrDados['atividadeconfatividadeId'] = $objAtividadegeralConfiguracoesAtividades->getAtividadeconfatividadeId(
            );

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possíel salvar o registro de atividadegeral__configuracoes_atividades!');
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['atividadeconfPortaria']) {
            $errors[] = 'Por favor preencha o campo "atividadeconfPortaria"!';
        }

        if (!$arrParam['atividadeatividadeId']) {
            $errors[] = 'Por favor preencha o campo "atividadeatividadeId"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM atividadegeral__configuracoes_atividades";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($atividadeconfatividadeId)
    {
        $arrDados                           = $this->getRepository()->find($atividadeconfatividadeId);
        $serviceAtividadegeralConfiguracoes = new \Atividades\Service\AtividadegeralConfiguracoes($this->getEm());
        $serviceAtividadegeralAtividades    = new \Atividades\Service\AtividadegeralAtividades($this->getEm());

        try {
            $arrDados = $arrDados->toArray();

            if ($arrDados['atividadeconfPortaria']) {
                $arrAtividadeconfPortaria          = [];
                $arrDados['atividadeconfPortaria'] = is_array(
                    $arrDados['atividadeconfPortaria']
                ) ? $arrDados['atividadeconfPortaria'] : explode(',', $arrDados['atividadeconfPortaria']);

                foreach ($arrDados['atividadeconfPortaria'] as $atividadeconfPortaria) {
                    $objAtividadegeralConfiguracoes = $serviceAtividadegeralConfiguracoes->getRepository()->find(
                        $atividadeconfPortaria
                    );

                    if ($objAtividadegeralConfiguracoes) {
                        $arrAtividadeconfPortaria[] = array(
                            'id'   => $objAtividadegeralConfiguracoes->getAtividadeconfPortaria(),
                            'text' => $objAtividadegeralConfiguracoes->getAtividadeconfPortaria()
                        );
                    }
                }

                $arrDados['atividadeconfPortaria'] = $arrAtividadeconfPortaria;
            }

            if ($arrDados['atividadeatividadeId']) {
                $arrAtividadeatividadeId          = [];
                $arrDados['atividadeatividadeId'] = is_array(
                    $arrDados['atividadeatividadeId']
                ) ? $arrDados['atividadeatividadeId'] : explode(',', $arrDados['atividadeatividadeId']);

                foreach ($arrDados['atividadeatividadeId'] as $atividadeatividadeId) {
                    $objAtividadegeralAtividades = $serviceAtividadegeralAtividades->getRepository()->find(
                        $atividadeatividadeId
                    );

                    if ($objAtividadegeralAtividades) {
                        $arrAtividadeatividadeId[] = array(
                            'id'   => $objAtividadegeralAtividades->getAtividadeatividadeId(),
                            'text' => $objAtividadegeralAtividades->getAtividadeatividadeId()
                        );
                    }
                }

                $arrDados['atividadeatividadeId'] = $arrAtividadeatividadeId;
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('atividadeconfatividadeId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Atividades\Entity\AtividadegeralConfiguracoesAtividades */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getAtividadeconfatividadeId(),
                $params['value'] => $objEntity->getAtividadeconfPontuacaoMin()
            );
        }

        return $arrEntitiesArr;
    }

    public function  remover($param)
    {
        if (!$param['atividadeconfatividadeId']) {
            $this->setLastError(
                'Para remover um registro de atividadegeral__configuracoes_atividades é necessário especificar o código.'
            );

            return false;
        }

        try {
            $objAtividadegeralConfiguracoesAtividades = $this->getRepository()->find(
                $param['atividadeconfatividadeId']
            );

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAtividadegeralConfiguracoesAtividades);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de atividadegeral__configuracoes_atividades.');

            return false;
        }

        return true;
    }
}
?>