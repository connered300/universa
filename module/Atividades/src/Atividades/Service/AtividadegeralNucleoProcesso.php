<?php

namespace Atividades\Service;

use VersaSpine\Service\AbstractService;

class AtividadegeralNucleoProcesso extends AbstractService
{
    const NUCLEO_PROCESSO_VINCULACAO_ATIVO = 'ativo';
    const NUCLEO_PROCESSO_VINCULACAO_INATIVO = 'inativo';
    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Atividade\Entity\AtividadegeralNucleoProcesso');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2NucleoProcessoVinculacao($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getNucleoProcessoVinculacao());
    }

    public static function getNucleoProcessoVinculacao()
    {
        return array(self::NUCLEO_PROCESSO_VINCULACAO_ATIVO, self::NUCLEO_PROCESSO_VINCULACAO_INATIVO);
    }

    public function pesquisaForJson($params)
    {
        $sql                      = 'SELECT * FROM atividadegeral__nucleo_processo WHERE';
        $nucleoProcessoVinculacao = false;
        $nucleoProcessoId         = false;

        if ($params['q']) {
            $nucleoProcessoVinculacao = $params['q'];
        } elseif ($params['query']) {
            $nucleoProcessoVinculacao = $params['query'];
        }

        if ($params['nucleoProcessoId']) {
            $nucleoProcessoId = $params['nucleoProcessoId'];
        }

        $parameters = array('nucleo_processo_vinculacao' => "{$nucleoProcessoVinculacao}%");
        $sql .= ' nucleo_processo_vinculacao LIKE :nucleo_processo_vinculacao';

        if ($nucleoProcessoId) {
            $parameters['nucleo_processo_id'] = $nucleoProcessoId;
            $sql .= ' AND nucleo_processo_id <> :nucleo_processo_id';
        }

        $sql .= " ORDER BY nucleo_processo_vinculacao";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceAtividadegeralNucleo       = new \Atividades\Service\AtividadegeralNucleo($this->getEm());
        $serviceAtividadegeralProcessoTipo = new \Atividades\Service\AtividadegeralProcessoTipo($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['nucleoProcessoId']) {
                $objAtividadegeralNucleoProcesso = $this->getRepository()->find($arrDados['nucleoProcessoId']);

                if (!$objAtividadegeralNucleoProcesso) {
                    $this->setLastError('Regitro de atividadegeral__nucleo_processo não existe!');

                    return false;
                }
            } else {
                $objAtividadegeralNucleoProcesso = new \Atividades\Entity\AtividadegeralNucleoProcesso();
            }

            if ($arrDados['nucleoId']) {
                $objAtividadegeralNucleo = $serviceAtividadegeralNucleo->getRepository()->find($arrDados['nucleoId']);

                if (!$objAtividadegeralNucleo) {
                    $this->setLastError('Registro de atividadegeral__nucleo não existe!');

                    return false;
                }

                $objAtividadegeralNucleoProcesso->setId($objAtividadegeralNucleo);
            }

            if ($arrDados['processoTipoId']) {
                $objAtividadegeralProcessoTipo = $serviceAtividadegeralProcessoTipo->getRepository()->find(
                    $arrDados['processoTipoId']
                );

                if (!$objAtividadegeralProcessoTipo) {
                    $this->setLastError('Registro de atividadegeral__processo_tipo não existe!');

                    return false;
                }

                $objAtividadegeralNucleoProcesso->setId($objAtividadegeralProcessoTipo);
            }
            $objAtividadegeralNucleoProcesso->setNucleoProcessoVinculacao($arrDados['nucleoProcessoVinculacao']);

            $this->getEm()->persist($objAtividadegeralNucleoProcesso);
            $this->getEm()->flush();

            $this->getEm()->commit();

            $arrDados['nucleoProcessoId'] = $objAtividadegeralNucleoProcesso->getNucleoProcessoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possíel salvar o registro de atividadegeral__nucleo_processo!');
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['nucleoId']) {
            $errors[] = 'Por favor preencha o campo "nucleoId"!';
        }

        if (!$arrParam['processoTipoId']) {
            $errors[] = 'Por favor preencha o campo "processoTipoId"!';
        }

        if (!$arrParam['nucleoProcessoVinculacao']) {
            $errors[] = 'Por favor preencha o campo "nucleoProcessoVinculacao"!';
        }

        if (!in_array($arrParam['nucleoProcessoVinculacao'], self::getNucleoProcessoVinculacao())) {
            $errors[] = 'Por favor selecione um valor v?ido para o campo "nucleoProcessoVinculacao"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM atividadegeral__nucleo_processo";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($nucleoProcessoId)
    {
        $arrDados                          = $this->getRepository()->find($nucleoProcessoId);
        $serviceAtividadegeralNucleo       = new \Atividades\Service\AtividadegeralNucleo($this->getEm());
        $serviceAtividadegeralProcessoTipo = new \Atividades\Service\AtividadegeralProcessoTipo($this->getEm());

        try {
            $arrDados = $arrDados->toArray();

            if ($arrDados['nucleoId']) {
                $arrNucleoId          = [];
                $arrDados['nucleoId'] = is_array($arrDados['nucleoId']) ? $arrDados['nucleoId'] : explode(
                    ',',
                    $arrDados['nucleoId']
                );

                foreach ($arrDados['nucleoId'] as $nucleoId) {
                    $objAtividadegeralNucleo = $serviceAtividadegeralNucleo->getRepository()->find($nucleoId);

                    if ($objAtividadegeralNucleo) {
                        $arrNucleoId[] = array(
                            'id'   => $objAtividadegeralNucleo->getNucleoId(),
                            'text' => $objAtividadegeralNucleo->getNucleoId()
                        );
                    }
                }

                $arrDados['nucleoId'] = $arrNucleoId;
            }

            if ($arrDados['processoTipoId']) {
                $arrProcessoTipoId          = [];
                $arrDados['processoTipoId'] = is_array(
                    $arrDados['processoTipoId']
                ) ? $arrDados['processoTipoId'] : explode(',', $arrDados['processoTipoId']);

                foreach ($arrDados['processoTipoId'] as $processoTipoId) {
                    $objAtividadegeralProcessoTipo = $serviceAtividadegeralProcessoTipo->getRepository()->find(
                        $processoTipoId
                    );

                    if ($objAtividadegeralProcessoTipo) {
                        $arrProcessoTipoId[] = array(
                            'id'   => $objAtividadegeralProcessoTipo->getProcessoTipoId(),
                            'text' => $objAtividadegeralProcessoTipo->getProcessoTipoId()
                        );
                    }
                }

                $arrDados['processoTipoId'] = $arrProcessoTipoId;
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('nucleoProcessoId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Atividades\Entity\AtividadegeralNucleoProcesso */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getNucleoProcessoId(),
                $params['value'] => $objEntity->getNucleoProcessoVinculacao()
            );
        }

        return $arrEntitiesArr;
    }

    public function  remover($param)
    {
        if (!$param['nucleoProcessoId']) {
            $this->setLastError(
                'Para remover um registro de atividadegeral__nucleo_processo é necessário especificar o código.'
            );

            return false;
        }

        try {
            $objAtividadegeralNucleoProcesso = $this->getRepository()->find($param['nucleoProcessoId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAtividadegeralNucleoProcesso);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de atividadegeral__nucleo_processo.');

            return false;
        }

        return true;
    }
}
?>