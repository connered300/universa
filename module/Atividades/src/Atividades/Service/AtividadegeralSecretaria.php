<?php

namespace Atividades\Service;

use VersaSpine\Service\AbstractService;

class AtividadegeralSecretaria extends AbstractService
{
    function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Atividades\Entity\AtividadegeralSecretaria');
    }

    protected function valida($dados)
    {
        // TODO: Implement valida() method.
    }

    protected function pesquisaForJson($criterios)
    {
        $parameters = array();
        $query      = "select * from atividadegeral__secretaria
              left join atividadegeral__configuracoes_curso using (secretaria_id)
              left join campus_curso  using(cursocampus_id)
              left join acad_curso using(curso_id)
            where 1";

        if ($criterios['q']) {
            $parameters['secretaria_nome'] = "%" . $criterios['q'] . "%";
            $query .= " AND secretaria_nome like :secretaria_nome";
        } elseif ($criterios['query']) {
            $parameters['secretaria_nome'] = "%" . $criterios['query'] . "%";
            $query .= " AND secretaria_nome like :secretaria_nome";
        }

        if ($criterios['cursoId']) {
            $query .= " AND curso_id in (:cursoId)";
            $parameters['cursoId'] = $criterios['cursoId'];
        }

        $query .= " GROUP BY atividadegeral__secretaria.secretaria_id";
        $result = $this->executeQueryWithParam($query, $parameters)->fetchAll();

        return $result;
    }
}