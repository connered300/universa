<?php

namespace Atividades\Service;

use VersaSpine\Service\AbstractService;

class AtividadegeralConfiguracoes extends AbstractService
{
    const ATIVIDADECONF_SERIE_1 = '1';
    const ATIVIDADECONF_SERIE_2 = '2';
    const ATIVIDADECONF_SERIE_3 = '3';
    const ATIVIDADECONF_SERIE_4 = '4';
    const ATIVIDADECONF_SERIE_5 = '5';
    const ATIVIDADECONF_SERIE_6 = '6';
    const ATIVIDADECONF_SERIE_7 = '7';
    const ATIVIDADECONF_SERIE_8 = '8';
    const ATIVIDADECONF_SERIE_9 = '9';
    const ATIVIDADECONF_SERIE_10 = '10';
    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Atividades\Entity\AtividadegeralConfiguracoes');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2AtividadeconfSerie($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getAtividadeconfSerie());
    }


    public static function getAtividadeconfSerie()
    {
        return array(
            self::ATIVIDADECONF_SERIE_1,
            self::ATIVIDADECONF_SERIE_2,
            self::ATIVIDADECONF_SERIE_3,
            self::ATIVIDADECONF_SERIE_4,
            self::ATIVIDADECONF_SERIE_5,
            self::ATIVIDADECONF_SERIE_6,
            self::ATIVIDADECONF_SERIE_7,
            self::ATIVIDADECONF_SERIE_8,
            self::ATIVIDADECONF_SERIE_9,
            self::ATIVIDADECONF_SERIE_10
        );
    }

    public function pesquisaForJson($params)
    {
        $sql                   = 'SELECT * FROM atividadegeral__configuracoes WHERE';
        $atividadeconfSerie    = false;
        $atividadeconfPortaria = false;

        if ($params['q']) {
            $atividadeconfSerie = $params['q'];
        } elseif ($params['query']) {
            $atividadeconfSerie = $params['query'];
        }

        if ($params['atividadeconfPortaria']) {
            $atividadeconfPortaria = $params['atividadeconfPortaria'];
        }

        $parameters = array('atividadeconf_serie' => "{$atividadeconfSerie}%");
        $sql .= ' atividadeconf_serie LIKE :atividadeconf_serie';

        if ($atividadeconfPortaria) {
            $parameters['atividadeconf_portaria'] = $atividadeconfPortaria;
            $sql .= ' AND atividadeconf_portaria <> :atividadeconf_portaria';
        }

        $sql .= " ORDER BY atividadeconf_serie";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['atividadeconfPortaria']) {
                $objAtividadegeralConfiguracoes = $this->getRepository()->find($arrDados['atividadeconfPortaria']);

                if (!$objAtividadegeralConfiguracoes) {
                    $this->setLastError('Regitro de atividadegeral__configuracoes não existe!');

                    return false;
                }
            } else {
                $objAtividadegeralConfiguracoes = new \Atividades\Entity\AtividadegeralConfiguracoes();
            }

            $objAtividadegeralConfiguracoes->setAtividadeconfMaximoDuracao($arrDados['atividadeconfMaximoDuracao']);
            $objAtividadegeralConfiguracoes->setAtividadeconfNumeroDecreto($arrDados['atividadeconfNumeroDecreto']);
            $objAtividadegeralConfiguracoes->setAtividadeconfDataDecreto($arrDados['atividadeconfDataDecreto']);
            $objAtividadegeralConfiguracoes->setAtividadecargaTotalPontuacao($arrDados['atividadecargaTotalPontuacao']);
            $objAtividadegeralConfiguracoes->setAtividadeconfCargaTotalHoras($arrDados['atividadeconfCargaTotalHoras']);
            $objAtividadegeralConfiguracoes->setAtividadeconfSerie($arrDados['atividadeconfSerie']);

            $this->getEm()->persist($objAtividadegeralConfiguracoes);
            $this->getEm()->flush();

            $this->getEm()->commit();

            $arrDados['atividadeconfPortaria'] = $objAtividadegeralConfiguracoes->getAtividadeconfPortaria();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possíel salvar o registro de atividadegeral__configuracoes!');
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['atividadeconfMaximoDuracao']) {
            $errors[] = 'Por favor preencha o campo "atividadeconfMaximoDuracao"!';
        }

        if (!$arrParam['atividadeconfNumeroDecreto']) {
            $errors[] = 'Por favor preencha o campo "atividadeconfNumeroDecreto"!';
        }

        if (!$arrParam['atividadeconfDataDecreto']) {
            $errors[] = 'Por favor preencha o campo "atividadeconfDataDecreto"!';
        }

        if (!$arrParam['atividadecargaTotalPontuacao']) {
            $errors[] = 'Por favor preencha o campo "atividadecargaTotalPontuacao"!';
        }

        if (!$arrParam['atividadeconfCargaTotalHoras']) {
            $errors[] = 'Por favor preencha o campo "atividadeconfCargaTotalHoras"!';
        }

        if (!$arrParam['atividadeconfSerie']) {
            $errors[] = 'Por favor preencha o campo "atividadeconfSerie"!';
        }

        if (!array_diff($arrParam['atividadeconfSerie'], self::getAtividadeconfSerie())) {
            $errors[] = 'Por favor selecione valores v?idos para o campo "atividadeconfSerie"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM atividadegeral__configuracoes";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($atividadeconfPortaria)
    {
        $arrDados = $this->getRepository()->find($atividadeconfPortaria);

        try {
            $arrDados = $arrDados->toArray();
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('atividadeconfPortaria' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Atividades\Entity\AtividadegeralConfiguracoes */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getAtividadeconfPortaria(),
                $params['value'] => $objEntity->getAtividadeconfSerie()
            );
        }

        return $arrEntitiesArr;
    }

    public function  remover($param)
    {
        if (!$param['atividadeconfPortaria']) {
            $this->setLastError(
                'Para remover um registro de atividadegeral__configuracoes é necessário especificar o código.'
            );

            return false;
        }

        try {
            $objAtividadegeralConfiguracoes = $this->getRepository()->find($param['atividadeconfPortaria']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAtividadegeralConfiguracoes);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de atividadegeral__configuracoes.');

            return false;
        }

        return true;
    }
}
?>