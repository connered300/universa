<?php

namespace Atividades\Service;

use VersaSpine\Service\AbstractService;

class AtividadeperiodoAlunoAtividades extends AbstractService
{
    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Atividades\Entity\AtividadeperiodoAlunoAtividades');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function pesquisaForJson($params)
    {
        $sql        = <<<SQL
        SELECT * FROM
          atividadeperiodo__aluno_atividades join acesso_pessoas on acesso_pessoas.usuario = atividadeperiodo__aluno_atividades.usuario_lancamento
        JOIN
          atividadeperiodo__aluno_nucleo using(alunonucleo_id)
        LEFT JOIN
          pessoa on pes_fisica = pes_id
        WHERE
SQL;
        $parameters = array();

        if($params['query'] != null) {
            $sql .= " alunoatividade_descricao like '%" . $params['query'] . "%'";
        }else{
            $sql .= " 1 ";
        }

        if ($params['alunonucleoId'] || $params['alunonucleoId'] == 0) {
            $parameters['alunonucleo_id'] = $params['alunonucleoId'];
            $sql .= ' AND alunonucleo_id = :alunonucleo_id';
        }

        if ($params['nucleoId']) {
            $parameters['nucleo_id'] = $params['nucleoId'];
            $sql .= ' AND nucleo_id = :nucleo_id';
        }

        if ($params['perId']){
            $parameters['per_id'] = $params['perId'];
            $sql .= ' AND per_id = :per_id';
        }

        $sql .= " ORDER BY alunoatividades_observacao";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        $arrDados['alunoAtividadeHoras'] = $arrDados['horasPontos'];

        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceAcessoPessoas               = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceAtividadegeralProcesso      = new \Atividades\Service\AtividadegeralProcesso($this->getEm());
        $serviceAtividadegeralAtividades    = new \Atividades\Service\AtividadegeralAtividades($this->getEm());
        $serviceAtividadeperiodoAlunoNucleo = new \Atividades\Service\AtividadeperiodoAlunoNucleo($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['atividadealunoAtividadeId']) {
                $objAlunoAtividades = $this->getRepository()->find($arrDados['atividadealunoAtividadeId']);

                if (!$objAlunoAtividades) {
                    $this->setLastError('A atividade a ser vinculada não existe!');

                    return false;
                }
            } else {
                $objAlunoAtividades = new \Atividades\Entity\AtividadeperiodoAlunoAtividades();
            }

            $objAlunoNucleo = $serviceAtividadeperiodoAlunoNucleo->getRepository()->find($arrDados['alunonucleo']);

            if (!$objAlunoNucleo) {
                $this->setLastError('Aluno não se encontra matriculado no núcleo informado!');

                return false;
            }

            $objAlunoAtividades->setAlunonucleo($objAlunoNucleo);

            if ($arrDados['atividadeId']) {
                $objAtividadegeralAtividades = $serviceAtividadegeralAtividades->getRepository()->find(
                    $arrDados['atividadeId']
                );

                if (!$objAtividadegeralAtividades) {
                    $this->setLastError('Registro de atividade não existe!');

                    return false;
                }

                $objAlunoAtividades->setAtividadeatividade($objAtividadegeralAtividades);
            }

            if ($arrDados['usuarioLancamento']) {
                $objAcessoPessoas = $serviceAcessoPessoas->getRepository()->find($arrDados['usuarioLancamento']);
            } else {
                $session = new \Zend\Authentication\Storage\Session();
                $user    = $session->read();

                $objAcessoPessoas = $serviceAcessoPessoas->getRepository()->find($user['id']);
            }

            if (!$objAcessoPessoas) {
                $this->setLastError('Usuário de lançamento não encontrado!');

                return false;
            }

            $objAlunoAtividades->setUsuarioLancamento($objAcessoPessoas);

            $arrDados['alunoatividadeDataInicial'] = self::formatDateAmericano($arrDados['alunoatividadeDataInicial']);
            $objAlunoAtividades->setAlunoatividadeDataInicial(new \DateTime($arrDados['alunoatividadeDataInicial']));

            if ($arrDados['alunoatividadeDataFinal']) {
                $arrDados['alunoatividadeDataFinal'] = self::formatDateAmericano($arrDados['alunoatividadeDataFinal']);

                $objAlunoAtividades->setAlunoatividadeDataFinal(new \DateTime($arrDados['alunoatividadeDataFinal']));
            }

            $objAlunoAtividades->setAlunoatividadeDataLancamento(new \DateTime('now'));

            if ($arrDados['atividadeprocessoId']) {
                $objAtividadegeralProcesso = $serviceAtividadegeralProcesso->getRepository()->find(
                    $arrDados['atividadeprocessoId']
                );

                if (!$objAtividadegeralProcesso) {
                    $this->setLastError('Registro de atividadegeral__processo não existe!');

                    return false;
                }

                $objAlunoAtividades->setAtividadeprocesso($objAtividadegeralProcesso);
            }

            $objAlunoAtividades->setAlunoatividadePontos($arrDados['alunoatividadePontos']);
            $objAlunoAtividades->setAlunoatividadeHoras($arrDados['alunoAtividadeHoras']);
            $objAlunoAtividades->setAlunoatividadeDescricao($arrDados['alunoatividadeDescricao']);
            $objAlunoAtividades->setAlunoatividadesObservacao($arrDados['alunoatividadesObservacao']);

            $this->getEm()->persist($objAlunoAtividades);
            $this->getEm()->flush();

            $this->getEm()->commit();

            return true;
        } catch (\Exception $e) {
            $this->setLastError(
                'Ocorreu um erro ao vincular a atividade para o aluno, verifique as informações inseridas e tente novamente. Caso o erro persista contate o suporte!'
            );
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['alunonucleo']) {
            $errors[] = 'Informe a inscrição do aluno no núcleo!';
        }

        if (!$arrParam['atividadeId']) {
            $errors[] = 'Informe a <b>Atividade</b> pra qual deseja lançar horas/pontos!';
        }

        if (!$arrParam['alunoAtividadeHoras'] && !$arrParam['alunoAtividadePontos']) {
            $errors[] = 'Informe as <b>horas/pontos</b> à serem contabilizadas para a Atividade!';
        }

        if (!$arrParam['alunoatividadeDataInicial']) {
            $errors[] = 'Informe a <b>Data de Lançamento</b> da(s) Atividade(s)!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $where = $data['alunocursoId']?'WHERE alunocurso_id = '. $data['alunocursoId']:'';
        $query = "SELECT
                      DATE_FORMAT(atividade.alunoatividade_data_lancamento,'%d/%m/%Y') alunoatividade_data_lancamento,
                      atividade.alunoatividade_horas,
                      atividade.alunoatividade_descricao,
                      ifnull(pessoa.pes_nome,acesso.login) pes_nome,
                      periodo.per_nome,
                      nucleo.alunoperiodo_status status,
                      atividade.atividadealuno_atividade_id,
                      if(isnull(acesso.id),0,1) lancamentoManual
                      FROM atividadeperiodo__aluno_atividades atividade
                      JOIN atividadeperiodo__aluno_nucleo nucleo USING(alunonucleo_id)
                      LEFT JOIN acadperiodo__aluno aluno USING(alunoper_id)
                      LEFT JOIN acadperiodo__letivo periodo USING(per_id)
                      LEFT JOIN acesso_pessoas acesso on acesso.id = atividade.usuario_lancamento
                      LEFT JOIN pessoa on acesso.pes_fisica = pessoa.pes_id ". $where;

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($atividadealunoAtividadeId)
    {
        $arrDados                           = $this->getRepository()->find($atividadealunoAtividadeId);
        $serviceAtividadeperiodoAlunoNucleo = new \Atividades\Service\AtividadeperiodoAlunoNucleo($this->getEm());
        $serviceAtividadegeralAtividades    = new \Atividades\Service\AtividadegeralAtividades($this->getEm());
        $serviceAcessoPessoas               = new \Pessoa\Service\AcessoPessoas($this->getEm());
        $serviceAtividadegeralProcesso      = new \Atividades\Service\AtividadegeralProcesso($this->getEm());

        try {
            $arrDados = $arrDados->toArray();

            if ($arrDados['alunonucleoId']) {
                $arrAlunonucleoId          = [];
                $arrDados['alunonucleoId'] = is_array(
                    $arrDados['alunonucleoId']
                ) ? $arrDados['alunonucleoId'] : explode(',', $arrDados['alunonucleoId']);

                foreach ($arrDados['alunonucleoId'] as $alunonucleoId) {
                    $objAlunoNucleo = $serviceAtividadeperiodoAlunoNucleo->getRepository()->find($alunonucleoId);

                    if ($objAlunoNucleo) {
                        $arrAlunonucleoId[] = array(
                            'id'   => $objAlunoNucleo->getAlunonucleoId(),
                            'text' => $objAlunoNucleo->getAlunonucleoId()
                        );
                    }
                }

                $arrDados['alunonucleoId'] = $arrAlunonucleoId;
            }

            if ($arrDados['atividadeatividadeId']) {
                $arrAtividadeatividadeId          = [];
                $arrDados['atividadeatividadeId'] = is_array(
                    $arrDados['atividadeatividadeId']
                ) ? $arrDados['atividadeatividadeId'] : explode(',', $arrDados['atividadeatividadeId']);

                foreach ($arrDados['atividadeatividadeId'] as $atividadeatividadeId) {
                    $objAtividadegeralAtividades = $serviceAtividadegeralAtividades->getRepository()->find(
                        $atividadeatividadeId
                    );

                    if ($objAtividadegeralAtividades) {
                        $arrAtividadeatividadeId[] = array(
                            'id'   => $objAtividadegeralAtividades->getAtividadeatividadeId(),
                            'text' => $objAtividadegeralAtividades->getAtividadeatividadeId()
                        );
                    }
                }

                $arrDados['atividadeatividadeId'] = $arrAtividadeatividadeId;
            }

            if ($arrDados['usuarioLancamento']) {
                $arrUsuarioLancamento          = [];
                $arrDados['usuarioLancamento'] = is_array(
                    $arrDados['usuarioLancamento']
                ) ? $arrDados['usuarioLancamento'] : explode(',', $arrDados['usuarioLancamento']);

                foreach ($arrDados['usuarioLancamento'] as $usuarioLancamento) {
                    $objAcessoPessoas = $serviceAcessoPessoas->getRepository()->find($usuarioLancamento);

                    if ($objAcessoPessoas) {
                        $arrUsuarioLancamento[] = array(
                            'id'   => $objAcessoPessoas->getId(),
                            'text' => $objAcessoPessoas->getId()
                        );
                    }
                }

                $arrDados['usuarioLancamento'] = $arrUsuarioLancamento;
            }

            if ($arrDados['atividadeprocessoId']) {
                $arrAtividadeprocessoId          = [];
                $arrDados['atividadeprocessoId'] = is_array(
                    $arrDados['atividadeprocessoId']
                ) ? $arrDados['atividadeprocessoId'] : explode(',', $arrDados['atividadeprocessoId']);

                foreach ($arrDados['atividadeprocessoId'] as $atividadeprocessoId) {
                    $objAtividadegeralProcesso = $serviceAtividadegeralProcesso->getRepository()->find(
                        $atividadeprocessoId
                    );

                    if ($objAtividadegeralProcesso) {
                        $arrAtividadeprocessoId[] = array(
                            'id'   => $objAtividadegeralProcesso->getAtividadeProcessoId(),
                            'text' => $objAtividadegeralProcesso->getAtividadeProcessoId()
                        );
                    }
                }

                $arrDados['atividadeprocessoId'] = $arrAtividadeprocessoId;
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('atividadealunoAtividadeId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Atividades\Entity\AtividadeperiodoAlunoAtividades */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getAtividadealunoAtividadeId(),
                $params['value'] => $objEntity->getAlunoatividadesObservacao()
            );
        }

        return $arrEntitiesArr;
    }

    public function  remover($param)
    {
        if (!$param['atividadealunoAtividadeId']) {
            $this->setLastError(
                'Para remover um registro de atividadeperiodo__aluno_atividades é necessário especificar o código.'
            );

            return false;
        }

        try {
            $objAlunoAtividades = $this->getRepository()->find($param['atividadealunoAtividadeId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAlunoAtividades);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de atividadeperiodo__aluno_atividades.');

            return false;
        }

        return true;
    }
}