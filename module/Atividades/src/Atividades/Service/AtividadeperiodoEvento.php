<?php

namespace Atividades\Service;

use VersaSpine\Service\AbstractService;

class AtividadeperiodoEvento extends AbstractService
{
    const EVENTO_TIPO_INTERNO = 'interno';
    const EVENTO_TIPO_EXTERNO = 'externo';
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2EventoTipo($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getEventoTipo());
    }

    public static function getEventoTipo()
    {
        return array(self::EVENTO_TIPO_INTERNO, self::EVENTO_TIPO_EXTERNO);
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Atividades\Entity\AtividadeperiodoEvento');
    }

    public function pesquisaForJson($params)
    {
        $sql        = '
        SELECT ae.*,
        LEAST(
            coalesce(atividadegeral__horas_praticas_max, evento_horas_validas),
            coalesce(evento_horas_validas, atividadegeral__horas_praticas_max)
        ) AS hora_maxima
        FROM atividadeperiodo__evento ae
        INNER JOIN atividadegeral__secretaria asec ON asec.secretaria_id=ae.evento_secretaria
        LEFT JOIN atividadegeral__configuracoes_curso acc ON acc.secretaria_id=asec.secretaria_id
        LEFT JOIN atividadegeral__configuracoes_atividades ON ae.evento_tipo_atividade=atividadeatividade_id
        WHERE 1 GROUP BY ae.evento_id';
        $eventoNome = false;
        $parameters = array();

        if ($params['q']) {
            $eventoNome = $params['q'];
        } elseif ($params['query']) {
            $eventoNome = $params['query'];
        }

        if ($params['eventoId']) {
            $sql .= " AND evento_id in (:eventoId)";
            $parameters['eventoId'] = $params['eventoId'];
        }

        if ($eventoNome) {
            $sql .= " AND evento_nome LIKE :eventoNome";
            $parameters['eventoNome'] = "{$eventoNome}%";
        }

        if ($params['groupBy']) {
            $sql .= " GROUP BY evento_id ";
        }

        $order = " evento_nome";

        if ($params['ordenar']) {
            if ($params['ordenar']['dataEvento']) {
                $order = " evento_data desc ," . $order;
            }
        }

        $sql .= " ORDER BY " . $order;

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        $serviceAtividadegeralTipo = new \Atividades\Service\AtividadegeralAtividades($this->getEm());
        $serviceAcessoPessoas      = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceSecretaria         = new \Atividades\Service\AtividadegeralSecretaria($this->getEm());

        if ($arrDados['usuarioCadastro']) {
            $arrDados['usuarioCadastro'] = $serviceAcessoPessoas->getRepository()->find($arrDados['usuarioCadastro']);
        } else {
            $arrDados['usuarioCadastro'] = $serviceAcessoPessoas->retornaUsuarioLogado();
        }

        $arrDados['atividadeSecretariaNome'] = $serviceSecretaria->getRepository()->find(
            $arrDados['atividadeSecretariaNome']
        );

        $arrDados['tipoAtividadeEvento'] = $arrDados['tipoAtividadeEvento'] ? $arrDados['tipoAtividadeEvento'] : $arrDados['atividadeatividade']['id'];

        $arrDados['tipoAtividadeEvento'] = $serviceAtividadegeralTipo->getRepository()->find(
            $arrDados['tipoAtividadeEvento']
        );

        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['eventoId']) {
                /** @var $objAtividadeperiodoEvento \Atividades\Entity\AtividadeperiodoEvento */
                $objAtividadeperiodoEvento = $this->getRepository()->find($arrDados['eventoId']);

                if (!$objAtividadeperiodoEvento) {
                    $this->setLastError('Registro de evento não existe!');

                    return false;
                }
            } else {
                $objAtividadeperiodoEvento = new \Atividades\Entity\AtividadeperiodoEvento();
            }

            $objAtividadeperiodoEvento->setSecretaria($arrDados['atividadeSecretariaNome']);
            $objAtividadeperiodoEvento->setEventoNome($arrDados['eventoNome']);
            $objAtividadeperiodoEvento->setEventoHorasValidas($arrDados['eventoHorasValidas']);
            $objAtividadeperiodoEvento->setEventoLotacao($arrDados['eventoLotacao']);
            $objAtividadeperiodoEvento->setEventoResponsavel($arrDados['eventoResponsavel']);
            $objAtividadeperiodoEvento->setEventoTipoAtividade($arrDados['tipoAtividadeEvento']);
            $objAtividadeperiodoEvento->setEventoData($arrDados['eventoData']);
            $objAtividadeperiodoEvento->setEventoTipo($arrDados['eventoTipo']);
            $objAtividadeperiodoEvento->setEventoDescricao($arrDados['eventoDescricao']);
            $objAtividadeperiodoEvento->setUsuarioCadastro($arrDados['usuarioCadastro']);

            $this->getEm()->persist($objAtividadeperiodoEvento);
            $this->getEm()->flush($objAtividadeperiodoEvento);

            $this->getEm()->commit();

            $arrDados['eventoId']   = $objAtividadeperiodoEvento->getEventoId();
            $arrDados['horaMaxima'] = $this->retornaHoraMaximaEvento($objAtividadeperiodoEvento);

            return $arrDados;
        } catch (\Exception $e) {
            $this->setLastError(
                'Não foi possível salvar o registro de evento!<br>' . $e->getMessage()
            );
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['atividadeSecretariaNome']) {
            $errors[] = 'Por favor preencha o campo "código do tipo de atividade"!';
        }

        if (!$arrParam['usuarioCadastro']) {
            $errors[] = 'Por favor preencha o campo "cadastro usuário"!';
        }

        if (!$arrParam['eventoTipo']) {
            $errors[] = 'Por favor preencha o campo "tipo"!';
        }

        if (!$arrParam['tipoAtividadeEvento']) {
            $errors[] = 'Por favor preencha o campo "tipo de Atividade" !';
        }

        if (!$arrParam['eventoNome']) {
            $errors[] = 'Por favor preencha o campo "nome"!';
        }

        if (!$arrParam['eventoData']) {
            $errors[] = 'Por favor preencha o campo "data"!';
        }

        if (!$arrParam['eventoHorasValidas'] || $arrParam['eventoHorasValidas'] <= 0) {
            $errors[] = 'Por favor preencha o campo "horas válidas do evento"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM atividadeperiodo__evento";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($eventoId)
    {
        $arrDados = array();

        if (!$eventoId) {
            $this->setLastError('Evento inválido!');

            return array();
        }

        /** @var $objAtividadeperiodoEvento \Atividades\Entity\AtividadeperiodoEvento */
        $objAtividadeperiodoEvento = $this->getRepository()->find($eventoId);

        if (!$objAtividadeperiodoEvento) {
            $this->setLastError('Evento não existe!');

            return array();
        }

        $serviceAtividadegeralTipo = new \Atividades\Service\AtividadegeralAtividades($this->getEm());
        $serviceAcessoPessoas      = new \Acesso\Service\AcessoPessoas($this->getEm());

        try {
            $arrDados = $objAtividadeperiodoEvento->toArray();

            if ($arrDados['atividadeatividade']) {

                /** @var $objAtividadeAtividades  \Atividades\Entity\AtividadegeralAtividades */
                $objAtividadeAtividades = $serviceAtividadegeralTipo->getRepository()->find(
                    $arrDados['atividadeatividade']
                );

                $arrDados['atividadeatividade'] = [
                    'id'   => $objAtividadeAtividades->getAtividadeId(),
                    'text' => $objAtividadeAtividades->getAtividadeDescricao()
                ];
            }

            if ($arrDados['usuarioCadastro']) {
                $arrAcessoPessoas            = $serviceAcessoPessoas->getArrSelect2(
                    ['id' => $arrDados['usuarioCadastro']]
                );
                $arrDados['usuarioCadastro'] = $arrAcessoPessoas ? $arrAcessoPessoas[0] : null;
            }

            $arrDados['horaMaxima'] = $this->retornaHoraMaximaEvento($objAtividadeperiodoEvento);
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    public function retornaHoraMaximaEvento(\Atividades\Entity\AtividadeperiodoEvento $objAtividadeperiodoEvento)
    {
        $serviceConfiguracoesCurso      = new \Atividades\Service\AtividadegeralConfiguracoesCurso($this->getEm());
        $serviceConfiguracoesAtividades = new \Atividades\Service\AtividadegeralConfiguracoesAtividades($this->getEm());

        $eventoHorasValidas = $objAtividadeperiodoEvento->getEventoHorasValidas();
        $horasPraticasMax   = $eventoHorasValidas;

        /** @var \Atividades\Entity\AtividadegeralConfiguracoesCurso $objConfiguracoesCurso */
        $objConfiguracoesCurso = $serviceConfiguracoesCurso->getRepository()->findOneBy(
            ['secretaria' => $objAtividadeperiodoEvento->getSecretaria()->getSecretariaId()]
        );

        if ($objConfiguracoesCurso) {
            /** @var \Atividades\Entity\AtividadegeralConfiguracoesAtividades $objConfiguracoesAtividades */
            $objConfiguracoesAtividades = $serviceConfiguracoesAtividades->getRepository()->findOneBy(
                ['atividadeconfPortaria' => $objConfiguracoesCurso->getAtividadeconfPortaria()]
            );

            if ($objConfiguracoesAtividades) {
                $horasPraticasMax = $objConfiguracoesAtividades->getAtividadegeralHorasPraticasMax();
            }
        }

        return min($eventoHorasValidas, $horasPraticasMax);
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceAtividadegeralTipo = new \Atividades\Service\AtividadegeralTipo($this->getEm());
        $serviceArquivo            = new \GerenciadorArquivos\Service\Arquivo($this->getEm());
        $serviceAcessoPessoas      = new \Acesso\Service\AcessoPessoas($this->getEm());

        if (is_array($arrDados['atividadeatividade']) && !$arrDados['atividadeatividade']['text']) {
            $arrDados['atividadeatividade'] = $arrDados['atividadeatividade']['atividadeatividadeId'];
        }

        if ($arrDados['atividadeatividade'] && is_string($arrDados['atividadeatividade'])) {
            $arrDados['atividadeatividade'] = $serviceAtividadegeralTipo->getArrSelect2(
                array('id' => $arrDados['atividadeatividade'])
            );
            $arrDados['atividadeatividade'] = $arrDados['atividadeatividade'] ? $arrDados['atividadeatividade'][0] : null;
        }

        if (is_array($arrDados['arq']) && !$arrDados['arq']['text']) {
            $arrDados['arq'] = $arrDados['arq']['arqId'];
        }

        if ($arrDados['arq'] && is_string($arrDados['arq'])) {
            $arrDados['arq'] = $serviceArquivo->getArrSelect2(array('id' => $arrDados['arq']));
            $arrDados['arq'] = $arrDados['arq'] ? $arrDados['arq'][0] : null;
        }

        if (is_array($arrDados['usuarioCadastro']) && !$arrDados['usuarioCadastro']['text']) {
            $arrDados['usuarioCadastro'] = $arrDados['usuarioCadastro']['id'];
        }

        if ($arrDados['usuarioCadastro'] && is_string($arrDados['usuarioCadastro'])) {
            $arrDados['usuarioCadastro'] = $serviceAcessoPessoas->getArrSelect2(
                array('id' => $arrDados['usuarioCadastro'])
            );
            $arrDados['usuarioCadastro'] = $arrDados['usuarioCadastro'] ? $arrDados['usuarioCadastro'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['eventoId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['eventoDescricao' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Atividades\Entity\AtividadeperiodoEvento */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getEventoId();
            $arrEntity[$params['value']] = $objEntity->getEventoNome();
            $arrEntity['horaMaxima']     = $this->retornaHoraMaximaEvento($objEntity);

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($eventoId)
    {
        if (!$eventoId) {
            $this->setLastError('Para remover um registro de evento é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objAtividadeperiodoEvento \Atividades\Entity\AtividadeperiodoEvento */
            $objAtividadeperiodoEvento = $this->getRepository()->find($eventoId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAtividadeperiodoEvento);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de evento.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceAtividadegeralTipo = new \Atividades\Service\AtividadegeralTipo($this->getEm());
        //$serviceArquivo            = new \GerenciadorArquivos\Service\Arquivo($this->getEm());
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());

        $serviceAtividadegeralTipo->setarDependenciasView($view);
        //$serviceArquivo->setarDependenciasView($view);
        $serviceAcessoPessoas->setarDependenciasView($view);

        $view->setVariable("arrEventoTipo", $this->getArrSelect2EventoTipo());
    }

    public function getDataForDatatablesEventoAluno($alunocursoId, $data = [])
    {
        $query = "
            SELECT 
                aluno_atividade_id,
                date_format(aluno_atividade_data_realizacao, '%d/%m/%Y') aluno_atividade_data_realizacao_br,
                aluno_atividade_data_realizacao,
                aluno_atividade_horas_validas,
                evento_nome,
                per_nome,
                pessoa.pes_nome,
                aa.atividadeatividade_descricao
            FROM atividadeperiodo__aluno atva
            INNER JOIN atividadeperiodo__evento atvev using(evento_id)
            INNER JOIN (
                SELECT pes_nome, id usuario_id
                FROM acesso_pessoas
                LEFT JOIN pessoa ON pes_id = pes_fisica
            ) pessoa on atvev.usuario_cadastro = pessoa.usuario_id
            INNER JOIN atividadegeral__secretaria asec on evento_secretaria = secretaria_id
            INNER JOIN acadperiodo__aluno acada using (alunoper_id)
            INNER JOIN acadperiodo__turma acadturma using (turma_id)    
            INNER JOIN acadperiodo__letivo per using(per_id)     
            INNER JOIN atividadegeral__configuracoes_curso ativcc ON ( ativcc.cursocampus_id = acadturma.cursocampus_id and ativcc.secretaria_id = asec.secretaria_id)  
            INNER JOIN atividadegeral__configuracoes ativc using (atividadeconf_portaria)
            LEFT JOIN atividadegeral__atividades aa on aa.atividadeatividade_id=atvev.evento_tipo_atividade
            WHERE alunocurso_id = '$alunocursoId'
            ";

        return parent::paginationDataTablesAjax($query, $data, null, false);
    }

}