<?php

namespace Atividades\Service;

use VersaSpine\Service\AbstractService;

class AtividadeperiodoAlunoPortfolio extends AbstractService
{
    const PORTFOLIOALUNO_SITUACAO_APROVADO = 'Aprovado';
    const PORTFOLIOALUNO_SITUACAO_REPROVADO = 'Reprovado';

    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Atividades\Entity\AtividadeperiodoAlunoPortfolio');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2PortfolioalunoSituacao($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getPortfolioalunoSituacao());
    }

    public static function getPortfolioalunoSituacao()
    {
        return array(self::PORTFOLIOALUNO_SITUACAO_APROVADO, self::PORTFOLIOALUNO_SITUACAO_REPROVADO);
    }

    public function pesquisaForJson($params)
    {
        $sql = 'SELECT * FROM atividadeperiodo__aluno_portfolio WHERE';

        $portfolioalunoSituacao = ($params['q']) ? $params['q'] : $params['query'];

        if ($params['alunonucleoId']) {
            $alunonucleoId = $params['alunonucleoId'];
        }

        $parameters = array('portfolioaluno_situacao' => "{$portfolioalunoSituacao}%");
        $sql .= ' portfolioaluno_situacao LIKE :portfolioaluno_situacao';

        if ($params['portfolioId']) {
            $parameters['portfolio_id'] = $params['portfolioId'];
            $sql .= ' AND portfolio_id <> :portfolio_id';
        }

        if ($alunonucleoId) {
            $parameters['alunonucleo_id'] = $alunonucleoId;
            $sql .= ' AND alunonucleo_id = :alunonucleo_id';
        }

        $sql .= " ORDER BY portfolioaluno_situacao";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    /**
     * Busca o ultimo registro de atividade aluno portfólio da pessoa informada
     * @param $pes_id
     */
    public function buscaUltimoAlunoPortfolio($pes_id){
       $sql = "SELECT *
                FROM atividadeperiodo__aluno_portfolio portfolio 
                INNER JOIN atividadeperiodo__aluno_nucleo USING(alunonucleo_id)
                INNER JOIN acadperiodo__aluno USING(alunoper_id)
                INNER JOIN acadgeral__aluno_curso USING(alunocurso_id)
                INNER JOIN acadgeral__aluno  USING(aluno_id)
                WHERE pes_id = :pes_id
                ORDER BY portfolioaluno_data_entrega DESC, portfolio_id DESC";
       return $this->executeQueryWithParam($sql, ["pes_id"=>$pes_id])->fetch();
    }


    public function getAlunoPortfolioInfo($arrData)
    {
        $query = "
            SELECT
                ifnull(portfolioaluno_data_entrega,' - ') data_entrega,
                ifnull(portfolioaluno_data_avaliacao,' - ') data_avaliacao,
                ifnull(portfolioaluno_pontuacao,' - ') pontuacao,
                portfolio_aluno_retorno situacao,
                ifnull(portfolioaluno_situacao,' - ') retorno
            FROM
                atividadeperiodo__aluno_nucleo aluNucleo
            RIGHT JOIN
                acadperiodo__aluno aluno on aluno.alunoper_id = aluNucleo.alunoper_id
            RIGHT JOIN
                atividadeperiodo__aluno_portfolio portfolio on portfolio.alunonucleo_id = aluNucleo.alunonucleo_id
        ";

        if ($arrData['matricula']) {
            $query .= ' WHERE aluno.alunocurso_id = ' . $arrData['matricula'];
        }

        $arrData['order'] = 'nucleo';

        $result = $this->paginationDataTablesAjax($query, $arrData, null, false);

        return $result;
    }

    public function save($arrDados)
    {
        $result = '';

        if ($arrDados['alunonucleoId'] == null) {
            $result .= 'Aluno não cadastrado em nenhum núcleo!<br>';
        }

        if ($arrDados['dataEntrega'] == null) {
            $result .= 'Campo dataEntrega está vazio <br>'; //DATA QUE O ALUNO LANÇA AS NOTAS
        }

        if ($arrDados['pontos'] == null) {
            $result .= 'Campo pontos está vazio <br>';
        }

        if ($result) {
            return $result;
        }

        $serviceAtividadeperiodoAlunoNucleo = new \Atividades\Service\AtividadeperiodoAlunoNucleo($this->getEm());
        $serviceArquivo                     = new \GerenciadorArquivos\Service\Arquivo($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['portfolioId']) {
                $objAtividadeperiodoAlunoPortfolio = $this->getRepository()->find($arrDados['portfolioId']);

                if (!$objAtividadeperiodoAlunoPortfolio) {
                    $this->setLastError('Regitro de atividadeperiodo__aluno_portfolio não existe!');

                    return false;
                }
            } else {
                $objAtividadeperiodoAlunoPortfolio = new \Atividades\Entity\AtividadeperiodoAlunoPortfolio();
            }

            if ($arrDados['alunonucleoId']) {
                $objAtividadeperiodoAlunoNucleo = $serviceAtividadeperiodoAlunoNucleo->getRepository()->find(
                    $arrDados['alunonucleoId']
                );

                if (!$objAtividadeperiodoAlunoNucleo) {
                    $this->setLastError('Registro de atividadeperiodo__aluno_nucleo não existe!');

                    return false;
                }
            }
            $objAtividadeperiodoAlunoPortfolio->setPortfolioalunoDataEntrega($arrDados['dataEntrega']);

            if ($arrDados['arqId']) {
                $objArquivo = $serviceArquivo->getRepository()->find($arrDados['arqId']);

                if (!$objArquivo) {
                    $this->setLastError('Registro de arquivo não existe!');

                    return false;
                }

                $objAtividadeperiodoAlunoPortfolio->setId($objArquivo);
            }

            $objAtividadeperiodoAlunoPortfolio->setAlunonucleo($objAtividadeperiodoAlunoNucleo);
            $objAtividadeperiodoAlunoPortfolio->setPortfolioalunoPontuacao($arrDados['pontos']);
            $objAtividadeperiodoAlunoPortfolio->setPortfolioAlunoRetorno($arrDados['obs']);
            $objAtividadeperiodoAlunoPortfolio->setPortfolioalunoSituacao($arrDados['situacao']);

            $this->getEm()->persist($objAtividadeperiodoAlunoPortfolio);
            $this->getEm()->flush();

            $this->getEm()->commit();

            $arrDados['portfolioId'] = $objAtividadeperiodoAlunoPortfolio->getPortfolioId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possíel salvar o registro de atividadeperiodo__aluno_portfolio!');
        }

        return $result;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['alunonucleoId']) {
            $errors[] = 'Por favor preencha o campo "alunonucleoId"!';
        }

        if (!$arrParam['portfolioalunoDataEntrega']) {
            $errors[] = 'Por favor preencha o campo "portfolioalunoDataEntrega"!';
        }

        if (!in_array($arrParam['portfolioalunoSituacao'], self::getPortfolioalunoSituacao())) {
            $errors[] = 'Por favor selecione um valor v?ido para o campo "portfolioalunoSituacao"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM atividadeperiodo__aluno_portfolio";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($portfolioId)
    {
        $arrDados                           = $this->getRepository()->find($portfolioId);
        $serviceAtividadeperiodoAlunoNucleo = new \Atividades\Service\AtividadeperiodoAlunoNucleo($this->getEm());
        $serviceArquivo                     = new \GerenciadorArquivos\Service\Arquivo($this->getEm());

        try {
            $arrDados = $arrDados->toArray();

            if ($arrDados['alunonucleoId']) {
                $arrAlunonucleoId          = [];
                $arrDados['alunonucleoId'] = is_array(
                    $arrDados['alunonucleoId']
                ) ? $arrDados['alunonucleoId'] : explode(',', $arrDados['alunonucleoId']);

                foreach ($arrDados['alunonucleoId'] as $alunonucleoId) {
                    $objAtividadeperiodoAlunoNucleo = $serviceAtividadeperiodoAlunoNucleo->getRepository()->find(
                        $alunonucleoId
                    );

                    if ($objAtividadeperiodoAlunoNucleo) {
                        $arrAlunonucleoId[] = array(
                            'id'   => $objAtividadeperiodoAlunoNucleo->getAlunonucleoId(),
                            'text' => $objAtividadeperiodoAlunoNucleo->getAlunonucleoId()
                        );
                    }
                }

                $arrDados['alunonucleoId'] = $arrAlunonucleoId;
            }

            if ($arrDados['arqId']) {
                $arrArqId          = [];
                $arrDados['arqId'] = is_array($arrDados['arqId']) ? $arrDados['arqId'] : explode(
                    ',',
                    $arrDados['arqId']
                );

                foreach ($arrDados['arqId'] as $arqId) {
                    $objArquivo = $serviceArquivo->getRepository()->find($arqId);

                    if ($objArquivo) {
                        $arrArqId[] = array(
                            'id'   => $objArquivo->getArqId(),
                            'text' => $objArquivo->getArqId()
                        );
                    }
                }

                $arrDados['arqId'] = $arrArqId;
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('portfolioId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Atividades\Entity\AtividadeperiodoAlunoPortfolio */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getPortfolioId(),
                $params['value'] => $objEntity->getPortfolioalunoSituacao()
            );
        }

        return $arrEntitiesArr;
    }

    public function  remover($param)
    {
        if (!$param['portfolioId']) {
            $this->setLastError(
                'Para remover um registro de atividadeperiodo__aluno_portfolio é necessário especificar o código.'
            );

            return false;
        }

        try {
            $objAtividadeperiodoAlunoPortfolio = $this->getRepository()->find($param['portfolioId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAtividadeperiodoAlunoPortfolio);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de atividadeperiodo__aluno_portfolio.');

            return false;
        }

        return true;
    }
}