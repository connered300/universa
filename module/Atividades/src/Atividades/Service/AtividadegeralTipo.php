<?php

namespace Atividades\Service;

use VersaSpine\Service\AbstractService;

class AtividadegeralTipo extends AbstractService
{
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Atividades\Entity\AtividadegeralTipo');
    }

    public function pesquisaForJson($params)
    {
        $sql                         = '
        SELECT * FROM atividadegeral__tipo
        LEFT JOIN atividadegeral__configuracoes_atividades USING(atividadeatividade_id)
        LEFT JOIN atividadegeral__configuracoes USING(atividadeconf_portaria)
        LEFT JOIN atividadegeral__configuracoes_curso USING(atividadeconf_portaria)
        LEFT JOIN atividadegeral__secretaria USING(secretaria_id)
        WHERE 1
        ';
        $atividadeatividadeDescricao = false;
        $parameters                  = array();

        if ($params['q']) {
            $atividadeatividadeDescricao = $params['q'];
        } elseif ($params['query']) {
            $atividadeatividadeDescricao = $params['query'];
        }
        if ($params['secretariaId']) {
            $sql .= " AND secretaria_id in (:secretariaId) ";
            $parameters['secretariaId'] = $params['secretariaId'];
        }

        if ($params['atividadeatividadeId']) {
            $sql .= " AND atividadeatividade_id in (:atividadeatividadeId) ";
            $parameters['atividadeatividadeId'] = $params['atividadeatividadeId'];
        }

        if ($atividadeatividadeDescricao) {
            $sql .= " AND atividadeatividade_descricao LIKE :atividadeDescricao ";
            $parameters['atividadeDescricao'] = "{$atividadeatividadeDescricao}%";
        }

        $sql .= " ORDER BY atividadeatividade_descricao";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['atividadeatividadeId']) {
                /** @var $objAtividadegeralTipo \Atividades\Entity\AtividadegeralTipo */
                $objAtividadegeralTipo = $this->getRepository()->find($arrDados['atividadeatividadeId']);

                if (!$objAtividadegeralTipo) {
                    $this->setLastError('Registro de atividadegeral tipo não existe!');

                    return false;
                }
            } else {
                $objAtividadegeralTipo = new \Atividades\Entity\AtividadegeralTipo();
            }

            $objAtividadegeralTipo->setAtividadeatividadeDescricao($arrDados['atividadeatividadeDescricao']);
            $objAtividadegeralTipo->setAtividadeatividadeObservacoes($arrDados['atividadeatividadeObservacoes']);

            $this->getEm()->persist($objAtividadegeralTipo);
            $this->getEm()->flush($objAtividadegeralTipo);

            $this->getEm()->commit();

            $arrDados['atividadeatividadeId'] = $objAtividadegeralTipo->getAtividadeatividadeId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de atividadegeral tipo!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['atividadeatividadeDescricao']) {
            $errors[] = 'Por favor preencha o campo "descrição do tipo de atividade"!';
        }

        if (!$arrParam['atividadeatividadeObservacoes']) {
            $errors[] = 'Por favor preencha o campo "observacões do tipo de atividade"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM atividadegeral__tipo";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getDataForDatatablesTipoAtividadesAluno($alunocursoId, $data = [])
    {
        $query = "
            SELECT SUM(aluno_atividade_horas_validas) totalHorasAtividadeTipo, atividadeatividade_descricao
            FROM atividadeperiodo__aluno atva
            INNER JOIN atividadeperiodo__evento atvev using(evento_id)
            INNER JOIN atividadegeral__atividades atvatv on atvev.evento_tipo_atividade = atvatv.atividadeatividade_id
            INNER JOIN atividadegeral__secretaria asec on evento_secretaria = secretaria_id
            INNER JOIN acadperiodo__aluno acada using (alunoper_id)
            INNER JOIN acadperiodo__turma acadturma using (turma_id)            
            INNER JOIN atividadegeral__configuracoes_curso ativcc ON ( ativcc.cursocampus_id = acadturma.cursocampus_id and ativcc.secretaria_id = asec.secretaria_id)  
            INNER JOIN atividadegeral__configuracoes ativc using (atividadeconf_portaria)
            WHERE alunocurso_id = '$alunocursoId'
            GROUP BY evento_tipo_atividade, alunocurso_id
            ";

        $totalizador=" SUM(totalHorasAtividadeTipo) AS totalHorasEvento ";

        $result=parent::paginationDataTablesAjax($query, $data, null, false,false,"",$totalizador);

        return $result;
    }

    public function getArray($atividadeatividadeId)
    {
        $arrDados = array();

        if (!$atividadeatividadeId) {
            $this->setLastError('Atividadegeral tipo inválido!');

            return array();
        }

        /** @var $objAtividadegeralTipo \Atividades\Entity\AtividadegeralTipo */
        $objAtividadegeralTipo = $this->getRepository()->find($atividadeatividadeId);

        if (!$objAtividadegeralTipo) {
            $this->setLastError('Atividadegeral tipo não existe!');

            return array();
        }

        try {
            $arrDados = $objAtividadegeralTipo->toArray();
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['atividadeatividadeId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['atividadeatividadeDescricao' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Atividades\Entity\AtividadegeralTipo */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getAtividadeatividadeId();
            $arrEntity[$params['value']] = $objEntity->getAtividadeatividadeDescricao();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($atividadeatividadeId)
    {
        if (!$atividadeatividadeId) {
            $this->setLastError('Para remover um registro de atividadegeral tipo é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objAtividadegeralTipo \Atividades\Entity\AtividadegeralTipo */
            $objAtividadegeralTipo = $this->getRepository()->find($atividadeatividadeId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAtividadegeralTipo);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de atividadegeral tipo.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
    }
}
?>