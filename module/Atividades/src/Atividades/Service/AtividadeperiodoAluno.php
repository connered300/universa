<?php

namespace Atividades\Service;

use VersaSpine\Service\AbstractService;

class AtividadeperiodoAluno extends AbstractService
{
    const EVENTO_ORIGEM_CADASTRO_ARQUIVO = 'arquivo';
    const EVENTO_ORIGEM_CADASTRO_MANUAL = 'manual';
    const EVENTO_ORIGEM_CADASTRO_MIGRACAO = 'migracao';
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2EventoOrigemCadastro($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getEventoOrigemCadastro());
    }

    public static function getEventoOrigemCadastro()
    {
        return array(
            self::EVENTO_ORIGEM_CADASTRO_ARQUIVO,
            self::EVENTO_ORIGEM_CADASTRO_MANUAL,
            self::EVENTO_ORIGEM_CADASTRO_MIGRACAO
        );
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Atividades\Entity\AtividadeperiodoAluno');
    }

    public function pesquisaForJson($params)
    {
        $sql                  = '
        SELECT alunocurso_id, a.* 
        FROM atividadeperiodo__aluno a
        INNER JOIN acadperiodo__aluno USING(alunoper_id)
        WHERE';
        $alunoperId           = false;
        $eventoDataRealizacao = false;

        if ($params['q']) {
            $alunoperId = $params['q'];
        } elseif ($params['query']) {
            $alunoperId = $params['query'];
        }

        if ($params['eventoDataRealizacao']) {
            $eventoDataRealizacao = $params['eventoDataRealizacao'];
        }

        $parameters = array('alunoperId' => "{$alunoperId}%");
        $sql .= ' a.alunoperId LIKE :alunoperId';

        if ($eventoDataRealizacao) {
            $parameters['eventoDataRealizacao'] = explode(',', $eventoDataRealizacao);
            $parameters['eventoDataRealizacao'] = $eventoDataRealizacao;
            $sql .= ' AND a.eventoDataRealizacao NOT IN(:eventoDataRealizacao)';
        }

        $sql .= " ORDER BY a.alunoperId";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    public function save(array &$arrDados)
    {
        $serviceAcadperiodoAluno       = new \Matricula\Service\AcadperiodoAluno($this->getEm());
        $serviceAtividadeperiodoEvento = new \Atividades\Service\AtividadeperiodoEvento($this->getEm());
        $serviceAcessoPessoas          = new \Acesso\Service\AcessoPessoas($this->getEm());

        if (!$arrDados['usuarioCadastro']) {
            $arrDados['usuarioCadastro'] = $serviceAcessoPessoas->retornaUsuarioLogado();
        } else {
            $arrDados['usuarioCadastro'] = $serviceAcessoPessoas->getRepository()->find($arrDados['usuarioCadastro']);
        }
        if (!$arrDados['alunoAtividadeDataLancamento']) {
            $arrDados['alunoAtividadeDataLancamento'] = new \DateTime('now');
        }
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['alunoAtividadeId']) {

                /** @var $objAtividadeperiodoAluno \Atividades\Entity\AtividadeperiodoAluno */
                $objAtividadeperiodoAluno = $this->getRepository()->find($arrDados['alunoAtividadeId']);

                if (!$objAtividadeperiodoAluno) {
                    $this->setLastError('Registro de atividade do aluno não existe!');

                    return false;
                }
            } else {
                $objAtividadeperiodoAluno = new \Atividades\Entity\AtividadeperiodoAluno();
            }

            if ($arrDados['alunoper']) {

                /** @var $objAcadperiodoAluno \Matricula\Entity\AcadperiodoAluno */
                $objAcadperiodoAluno = $serviceAcadperiodoAluno->getRepository()->find($arrDados['alunoper']);

                if (!$objAcadperiodoAluno) {
                    $this->setLastError('Registro de aluno não existe!');

                    return false;
                }

                $objAtividadeperiodoAluno->setAlunoper($objAcadperiodoAluno);
            } else {
                $objAtividadeperiodoAluno->setAlunoper(null);
            }

            if ($arrDados['evento']) {
                /** @var $objAtividadeperiodoEvento \Atividades\Entity\AtividadeperiodoEvento */
                $objAtividadeperiodoEvento = $serviceAtividadeperiodoEvento->getRepository()->find($arrDados['evento']);

                if (!$objAtividadeperiodoEvento) {
                    $this->setLastError('Registro de evento não existe!');

                    return false;
                }

                $objAtividadeperiodoAluno->setEvento($objAtividadeperiodoEvento);
            } else {
                $objAtividadeperiodoAluno->setEvento(null);
            }

            if ($arrDados['usuarioCadastro']) {
                /** @var $objAcessoPessoas \Acesso\Entity\AcessoPessoas */
                $objAcessoPessoas = $serviceAcessoPessoas->getRepository()->find($arrDados['usuarioCadastro']);

                if (!$objAcessoPessoas) {
                    $this->setLastError('Registro de pessoas não existe!');

                    return false;
                }

                $objAtividadeperiodoAluno->setUsuarioCadastro($objAcessoPessoas);
            } else {
                $objAtividadeperiodoAluno->setUsuarioCadastro(null);
            }

            $objAtividadeperiodoAluno->setAlunoAtividadeHorasReais($arrDados['alunoAtividadeHorasReais']);
            $objAtividadeperiodoAluno->setAlunoAtividadeHorasValidas($arrDados['alunoAtividadeHorasValidas']);
            $objAtividadeperiodoAluno->setAlunoAtividadeObservacao($arrDados['alunoAtividadeObservacao']);
            $objAtividadeperiodoAluno->setAlunoAtividadeDataLancamento($arrDados['alunoAtividadeDataLancamento']);
            $objAtividadeperiodoAluno->setAlunoAtividadeDataRealizacao($arrDados['alunoAtividadeDataRealizacao']);

            $this->getEm()->persist($objAtividadeperiodoAluno);
            $this->getEm()->flush($objAtividadeperiodoAluno);

            $this->getEm()->commit();

            $arrDados['alunoAtividadeDataRealizacao'] = $objAtividadeperiodoAluno->getAlunoAtividadeDataRealizacao();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de atividade do aluno!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['evento']) {
            $errors[] = 'Por favor preencha o campo "código"!';
        }

        if (!$arrParam['usuarioCadastro']) {
            $errors[] = 'Por favor preencha o campo "cadastro usuário"!';
        }
        if (!$arrParam['alunoper']) {
            $errors[] = 'Por favor preencha o campo "Aluno"!';
        }

        if (!$arrParam['alunoAtividadeHorasReais']) {
            $errors[] = 'Por favor preencha o campo "horas reais da atividade"!';
        }

        if (!$arrParam['alunoAtividadeHorasValidas']) {
            $errors[] = 'Por favor preencha o campo "horas válidas da atividade"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT 
                        alunocurso_id,
                        atividadeperiodo__aluno.*,
                        pessoa.pes_nome,
                        atividadeperiodo__evento.evento_nome
                    FROM atividadeperiodo__aluno
                    LEFT JOIN atividadeperiodo__evento USING (evento_id)
                    LEFT JOIN acadperiodo__aluno USING (alunoper_id)
                    LEFT JOIN acadgeral__aluno_curso USING (alunocurso_id)
                    LEFT JOIN acadgeral__aluno USING(aluno_id)
                    LEFT JOIN pessoa USING(pes_id)";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($atividadePeriodo)
    {
        $arrDados = array();

        if (!$atividadePeriodo) {
            $this->setLastError('atividade do aluno inválido!');

            return array();
        }

        /** @var $objAtividadeperiodoAluno \Atividades\Entity\AtividadeperiodoAluno */
        $objAtividadeperiodoAluno = $this->getRepository()->find($atividadePeriodo);

        if (!$objAtividadeperiodoAluno) {
            $this->setLastError('atividade do aluno não existe!');

            return array();
        }

        $serviceAtividadeperiodoEvento = new \Atividades\Service\AtividadeperiodoEvento($this->getEm());
        $serviceAcessoPessoas          = new \Acesso\Service\AcessoPessoas($this->getEm());

        try {
            $arrDados = $objAtividadeperiodoAluno->toArray();

            if ($arrDados['alunoper']) {
                $arrDados['arrAcadPeriodoAluno'] = [
                    'id'            => $objAtividadeperiodoAluno->getAlunoper()->getAlunoperId(),
                    'text'          => $objAtividadeperiodoAluno->getAlunoper()->getAlunocurso()->getAluno()->getPes(
                    )->getPes()->getPesNome(),
                    'cursoNome'     => $objAtividadeperiodoAluno->getAlunoper()->getAlunocurso()->getCursocampus(
                    )->getCurso()->getCursoNome(),
                    'alunoSituacao' => $objAtividadeperiodoAluno->getAlunoper()->getMatsituacao()->getMatsitDescricao(),
                    'alunoCursoId'  => $objAtividadeperiodoAluno->getAlunoper()->getAlunocurso()->getAlunocursoId()
                ];
            }

            if ($arrDados['evento']) {
                $arrAtividadeperiodoEvento  = $serviceAtividadeperiodoEvento->getArrSelect2(
                    ['id' => $arrDados['evento']]
                );
                $arrDados['arrEventoDados'] = $arrAtividadeperiodoEvento ? $arrAtividadeperiodoEvento[0] : null;
            }

            if ($arrDados['usuarioCadastro']) {
                $arrAcessoPessoas            = $serviceAcessoPessoas->getArrSelect2(
                    ['id' => $arrDados['usuarioCadastro']]
                );
                $arrDados['usuarioCadastro'] = $arrAcessoPessoas ? $arrAcessoPessoas[0] : null;
            }
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceAcadperiodoAluno       = new \Matricula\Service\AcadperiodoAluno($this->getEm());
        $serviceAtividadeperiodoEvento = new \Atividades\Service\AtividadeperiodoEvento($this->getEm());
        $serviceAcessoPessoas          = new \Acesso\Service\AcessoPessoas($this->getEm());

        if (is_array($arrDados['alunoper']) && !$arrDados['alunoper']['text']) {
            $arrDados['alunoper'] = $arrDados['alunoper']['alunoperId'];
        }

        if ($arrDados['alunoper'] && is_string($arrDados['alunoper'])) {
            $arrDados['alunoper'] = $serviceAcadperiodoAluno->getArrSelect2(array('id' => $arrDados['alunoper']));
            $arrDados['alunoper'] = $arrDados['alunoper'] ? $arrDados['alunoper'][0] : null;
        }

        if (is_array($arrDados['evento']) && !$arrDados['evento']['text']) {
            $arrDados['evento'] = $arrDados['evento']['eventoId'];
        }

        if ($arrDados['evento'] && is_string($arrDados['evento'])) {
            $arrDados['evento'] = $serviceAtividadeperiodoEvento->getArrSelect2(array('id' => $arrDados['evento']));
            $arrDados['evento'] = $arrDados['evento'] ? $arrDados['evento'][0] : null;
        }

        if (is_array($arrDados['usuarioCadastro']) && !$arrDados['usuarioCadastro']['text']) {
            $arrDados['usuarioCadastro'] = $arrDados['usuarioCadastro']['id'];
        }

        if ($arrDados['usuarioCadastro'] && is_string($arrDados['usuarioCadastro'])) {
            $arrDados['usuarioCadastro'] = $serviceAcessoPessoas->getArrSelect2(
                array('id' => $arrDados['usuarioCadastro'])
            );
            $arrDados['usuarioCadastro'] = $arrDados['usuarioCadastro'] ? $arrDados['usuarioCadastro'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['eventoDataRealizacao'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['alunoperId' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Atividades\Entity\AtividadeperiodoAluno */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getEventoDataRealizacao();
            $arrEntity[$params['value']] = $objEntity->getAlunoperId();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($atividadeId)
    {
        if (!$atividadeId) {
            $this->setLastError('Para remover um registro de atividade do aluno é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objAtividadeperiodoAluno \Atividades\Entity\AtividadeperiodoAluno */
            $objAtividadeperiodoAluno = $this->getRepository()->find($atividadeId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAtividadeperiodoAluno);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de atividade do aluno.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceAcadperiodoAluno       = new \Matricula\Service\AcadperiodoAluno($this->getEm());
        $serviceAtividadeperiodoEvento = new \Atividades\Service\AtividadeperiodoEvento($this->getEm());
        $serviceAcessoPessoas          = new \Acesso\Service\AcessoPessoas($this->getEm());

        $serviceAcadperiodoAluno->setarDependenciasView($view);
        $serviceAtividadeperiodoEvento->setarDependenciasView($view);
        $serviceAcessoPessoas->setarDependenciasView($view);

        $view->setVariable("arrEventoOrigemCadastro", $this->getArrSelect2EventoOrigemCadastro());
    }

    /**
     * Busca o status da atividade complementar do aluno curso, se o msm concluiu ou não os créditos
     * @param $alunocursoId
     * @return array
     */
    public function buscarStatusAtividadesComplementares($alunocursoId)
    {
        $sql = "
        SELECT
            SUM(aluno_atividade_horas_validas) totalHoras,
            if(
                atividadeconf_carga_total_horas <= SUM(aluno_atividade_horas_validas),
                'Concluído',
                'Não Concluído'
            ) situacaoDescricao
        FROM atividadeperiodo__aluno atva
        INNER JOIN atividadeperiodo__evento atvev USING(evento_id)
        INNER JOIN atividadegeral__secretaria asec ON evento_secretaria = secretaria_id
        INNER JOIN acadperiodo__aluno acada USING (alunoper_id)
        INNER JOIN acadperiodo__turma acadturma USING (turma_id)
        INNER JOIN atividadegeral__configuracoes_curso ativcc ON ( ativcc.cursocampus_id = acadturma.cursocampus_id AND ativcc.secretaria_id = asec.secretaria_id)
        INNER JOIN atividadegeral__configuracoes ativc USING (atividadeconf_portaria)
        WHERE alunocurso_id = :alunocurso_id
        GROUP BY alunocurso_id";

        return $this->executeQueryWithParam($sql, ["alunocurso_id" => $alunocursoId])->fetch();
    }
}