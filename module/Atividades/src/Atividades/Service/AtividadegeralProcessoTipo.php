<?php

namespace Atividades\Service;

use VersaSpine\Service\AbstractService;

class AtividadegeralProcessoTipo extends AbstractService
{
    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Atividades\Entity\AtividadegeralProcessoTipo');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function pesquisaForJson($params)
    {
        $sql                    = 'SELECT * FROM atividadegeral__processo_tipo WHERE';
        $processoTipoObservacao = false;
        $processoTipoId         = false;

        if ($params['q']) {
            $processoTipoObservacao = $params['q'];
        } elseif ($params['query']) {
            $processoTipoObservacao = $params['query'];
        }

        if ($params['processoTipoId']) {
            $processoTipoId = $params['processoTipoId'];
        }

        $parameters = array('processo_tipo_observacao' => "{$processoTipoObservacao}%");
        $sql .= ' processo_tipo_observacao LIKE :processo_tipo_observacao';

        if ($processoTipoId) {
            $parameters['processo_tipo_id'] = $processoTipoId;
            $sql .= ' AND processo_tipo_id <> :processo_tipo_id';
        }

        $sql .= " ORDER BY processo_tipo_observacao";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['processoTipoId']) {
                $objAtividadegeralProcessoTipo = $this->getRepository()->find($arrDados['processoTipoId']);

                if (!$objAtividadegeralProcessoTipo) {
                    $this->setLastError('Regitro de tipo de processo não existe!');

                    return false;
                }
            } else {
                $objAtividadegeralProcessoTipo = new \Atividades\Entity\AtividadegeralProcessoTipo();
            }

            $objAtividadegeralProcessoTipo->setProcessoTipoDescricao($arrDados['processoTipoDescricao']);
            $objAtividadegeralProcessoTipo->setProcessoTipoObservacao($arrDados['processoTipoObservacao']);

            $this->getEm()->persist($objAtividadegeralProcessoTipo);
            $this->getEm()->flush();

            $this->getEm()->commit();

            $arrDados['processoTipoId'] = $objAtividadegeralProcessoTipo->getProcessoTipoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possíel salvar o registro de tipo de processo!');
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['processoTipoDescricao']) {
            $errors[] = 'Por favor preencha o campo "processoTipoDescricao"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM atividadegeral__processo_tipo";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($processoTipoId)
    {
        $arrDados = $this->getRepository()->find($processoTipoId);

        try {
            $arrDados = $arrDados->toArray();
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('processoTipoId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Atividades\Entity\AtividadegeralProcessoTipo */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getProcessoTipoId(),
                $params['value'] => $objEntity->getProcessoTipoObservacao()
            );
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['processoTipoId']) {
            $this->setLastError(
                'Para remover um registro de tipo de processo é necessário especificar o código.'
            );

            return false;
        }

        try {
            $objAtividadegeralProcessoTipo = $this->getRepository()->find($param['processoTipoId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAtividadegeralProcessoTipo);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de tipo de processo.');

            return false;
        }

        return true;
    }
}
?>