<?php

namespace Atividades\Service;

use VersaSpine\Service\AbstractService;

class AtividadeperiodoLetivo extends AbstractService
{
    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Atividades\Entity\AtividadeperiodoLetivo');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function pesquisaForJson($params)
    {
        $sql     = 'SELECT * FROM atividadeperiodo__letivo JOIN atividadegeral__configuracoes_curso USING(atividadeconfigcurso_id) JOIN campus_curso USING(cursocampus_id) WHERE 1';
        $perId   = false;
        $cursoId = false;

        if ($params['perId']) {
            $perId = $params['perId'];
        }

        if ($params['cursoId']) {
            $cursoId = $params['cursoId'];
        }

        if ($perId) {
            $parameters['per_id'] = $perId;
            $sql .= ' AND per_id = :per_id';
        }

        if ($cursoId) {
            $parameters['curso_id'] = $cursoId;
            $sql .= ' AND curso_id = :curso_id';
        }

        $sql .= " ORDER BY per_id";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function editarData($params)
    {
        $inicio    = strtotime($params['perDataInicio']['date']);
        $data      = strtotime($this->formatDateAmericano($params['data']));
        $valueData = str_replace("-", '', $this->formatDateAmericano($params['data']));
        $arrAtual  = (array)(new \DateTime());
        $dataAtual = strtotime($arrAtual['date']);

        if ($data >= $inicio && $dataAtual < $inicio) {
            $this->exec(
                "
                UPDATE
                  atividadeperiodo__letivo
                  join atividadegeral__configuracoes_curso USING(atividadeconfigcurso_id)
                  JOIN campus_curso USING(cursocampus_id)
                  SET atividadeperiodo_data_inicio = {$valueData} WHERE per_id = {$params['perId']} AND curso_id = {$params['cursoId']}
            "
            );

            return true;
        }

        return false;
    }

    public function pesquisaForJsonCursos($params)
    {
        $parameters = array();
        $sql        = 'select * from campus_curso JOIN org_campus USING(camp_id) JOIN acad_curso USING(curso_id) WHERE';

        if ($params['q']) {
            $campus = $params['q'];
        } elseif ($params['query']) {
            $campus = $params['query'];
        }

        if ($params['campId']) {
            $parameters['camp_id'] = $params['campId'];
            $sql .= ' camp_id = :camp_id AND';
        }

        $parameters['camp_nome'] = "%{$campus}%";

        $sql .= ' camp_nome LIKE :camp_nome';
        $sql .= " ORDER BY curso_nome";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function pesquisaForJsonCampus($params)
    {
        $sql = 'select * from campus_curso JOIN org_campus USING(camp_id) WHERE';

        if ($params['q']) {
            $campus = $params['q'];
        } elseif ($params['query']) {
            $campus = $params['query'];
        }

        $parameters = array('camp_nome' => "%{$campus}%");
        $sql .= ' camp_nome LIKE :camp_nome';

        $sql .= " ORDER BY camp_nome DESC";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function paginationAjax($arrayData)
    {
        $query = <<<SQL
        SELECT
          *
        FROM
          atividadeperiodo__letivo
          JOIN
          atividadegeral__configuracoes_curso acc USING(atividadeconfigcurso_id)
          JOIN
          campus_curso USING(cursocampus_id)
          JOIN
          acad_curso USING(curso_id)
          JOIN
          atividadegeral__nucleo
          JOIN
              (select secretaria_nome, secretaria_id from atividadegeral__secretaria)secretaria USING(secretaria_id)
          JOIN
          (
            SELECT
              count(*) qtd
            FROM
              atividadegeral__nucleo
          )Nucleos
          WHERE
          acc.cursocampus_id = {$arrayData['cursocampusId']} AND per_id = {$arrayData['perId']}
          GROUP BY curso_id
SQL;

        $result = $this->paginationDataTablesAjax($query, $arrayData, null, false);

        foreach ($result['data'] as $indice => $valor) {
            $result['data'][$indice]['atividadeperiodo_data_inicio']     = (
            new \DateTime(
                $valor['atividadeperiodo_data_inicio']
            )
            )->format("d/m/Y");
            $result['data'][$indice]['atividadeperiodo_data_fechamento'] = (
            new \DateTime(
                $valor['atividadeperiodo_data_fechamento']
            )
            )->format("d/m/Y");
        }

        return $result;
    }

    public function save(array $arrDados)
    {
        $serviceAcadperiodoLetivo = new \Matricula\Service\AcadperiodoLetivo($this->getEm());
        $serviceConfigCurso       = new \Atividades\Service\AtividadegeralConfiguracoesCurso($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['perId']) {
                $objAtividadeperiodoLetivo = $this->getRepository()->find($arrDados['perId']);
                $objAcadperiodoLetivo      = $serviceAcadperiodoLetivo->getRepository()->find($arrDados['perId']);

                if (!$objAtividadeperiodoLetivo) {
                    $objAtividadeperiodoLetivo = new \Atividades\Entity\AtividadeperiodoLetivo();
                }

                $objAtividadeperiodoLetivo->setPer($objAcadperiodoLetivo);
            }

            if ($arrDados['atividadeconfigcursoId']) {
                $objconfigcurso = $serviceConfigCurso->getRepository()->find($arrDados['atividadeconfigcursoId']);
                $objAtividadeperiodoLetivo->setAtividadeConfiguracoesCurso($objconfigcurso);
            } else {
                return false;
            }

            if ($arrDados['atividadeperiodoVagas']) {
                $objAtividadeperiodoLetivo->setAtividadeperiodoVagas($arrDados['atividadeperiodoVagas']);
            }

            $objAtividadeperiodoLetivo->setAtividadeperiodoDataInicio(
                new \DateTime($this->formatDateAmericano($arrDados['atividadeperiodoDataInicio']))
            );
            $objAtividadeperiodoLetivo->setAtividadeperiodoFuncionamentoInicio(
                $arrDados['atividadeperiodoFuncionamentoInicio']
            );
            $objAtividadeperiodoLetivo->setAtividadeperiodoFuncionamentoFim(
                $arrDados['atividadeperiodoFuncionamentoFim']
            );

            $this->getEm()->persist($objAtividadeperiodoLetivo);
            $this->getEm()->flush();
            $this->getEm()->commit();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possíel salvar o registro de atividadeperiodo__letivo!');
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['atividadeConfigCursoId']) {
            $errors[] = 'Por favor preencha o campo "atividadeConfigCursoId"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM atividadeperiodo__letivo";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($perId)
    {
        $arrDados                           = $this->getRepository()->find($perId);
        $serviceAcadperiodoLetivo           = new \Matricula\Service\AcadperiodoLetivo($this->getEm());
        $serviceAtividadegeralConfiguracoes = new \Atividades\Service\AtividadegeralConfiguracoes($this->getEm());

        try {
            $arrDados = $arrDados->toArray();

            if ($arrDados['perId']) {
                $arrPerId          = [];
                $arrDados['perId'] = is_array($arrDados['perId']) ? $arrDados['perId'] : explode(
                    ',',
                    $arrDados['perId']
                );

                foreach ($arrDados['perId'] as $perId) {
                    $objAcadperiodoLetivo = $serviceAcadperiodoLetivo->getRepository()->find($perId);

                    if ($objAcadperiodoLetivo) {
                        $arrPerId[] = array(
                            'id'   => $objAcadperiodoLetivo->getPerId(),
                            'text' => $objAcadperiodoLetivo->getPerId()
                        );
                    }
                }

                $arrDados['perId'] = $arrPerId;
            }

            if ($arrDados['atividadeconfPortaria']) {
                $arrAtividadeconfPortaria          = [];
                $arrDados['atividadeconfPortaria'] = is_array(
                    $arrDados['atividadeconfPortaria']
                ) ? $arrDados['atividadeconfPortaria'] : explode(',', $arrDados['atividadeconfPortaria']);

                foreach ($arrDados['atividadeconfPortaria'] as $atividadeconfPortaria) {
                    $objAtividadegeralConfiguracoes = $serviceAtividadegeralConfiguracoes->getRepository()->find(
                        $atividadeconfPortaria
                    );

                    if ($objAtividadegeralConfiguracoes) {
                        $arrAtividadeconfPortaria[] = array(
                            'id'   => $objAtividadegeralConfiguracoes->getAtividadeconfPortaria(),
                            'text' => $objAtividadegeralConfiguracoes->getAtividadeconfPortaria()
                        );
                    }
                }

                $arrDados['atividadeconfPortaria'] = $arrAtividadeconfPortaria;
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('perId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Atividades\Entity\AtividadeperiodoLetivo */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getPerId(),
                $params['value'] => $objEntity->getAtividadeperiodoFuncionamentoFim()
            );
        }

        return $arrEntitiesArr;
    }

    public function  remover($param)
    {
        if (!$param['perId']) {
            $this->setLastError(
                'Para remover um registro de atividadeperiodo__letivo é necessário especificar o código.'
            );

            return false;
        }

        try {
            $objAtividadeperiodoLetivo = $this->getRepository()->find($param['perId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAtividadeperiodoLetivo);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de atividadeperiodo__letivo.');

            return false;
        }

        return true;
    }
}
?>