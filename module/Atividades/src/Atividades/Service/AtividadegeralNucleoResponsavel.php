<?php

namespace Atividades\Service;

use VersaSpine\Service\AbstractService;

class AtividadegeralNucleoResponsavel extends AbstractService
{
    const NUCLEO_RESPONSAVEL_NIVEL_AUXILIAR = 'auxiliar';
    const NUCLEO_RESPONSAVEL_NIVEL_SUPERVISOR = 'supervisor';
    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Atividade\Entity\AtividadegeralNucleoResponsavel');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2NucleoResponsavelNivel($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getNucleoResponsavelNivel());
    }

    public static function getNucleoResponsavelNivel()
    {
        return array(self::NUCLEO_RESPONSAVEL_NIVEL_AUXILIAR, self::NUCLEO_RESPONSAVEL_NIVEL_SUPERVISOR);
    }

    public function pesquisaForJson($params)
    {
        $sql                    = 'SELECT * FROM atividadegeral__nucleo_responsavel WHERE';
        $nucleoResponsavelNivel = false;
        $nucleoresponsavelId    = false;

        if ($params['q']) {
            $nucleoResponsavelNivel = $params['q'];
        } elseif ($params['query']) {
            $nucleoResponsavelNivel = $params['query'];
        }

        if ($params['nucleoresponsavelId']) {
            $nucleoresponsavelId = $params['nucleoresponsavelId'];
        }

        $parameters = array('nucleo_responsavel_nivel' => "{$nucleoResponsavelNivel}%");
        $sql .= ' nucleo_responsavel_nivel LIKE :nucleo_responsavel_nivel';

        if ($nucleoresponsavelId) {
            $parameters['nucleoresponsavel_id'] = $nucleoresponsavelId;
            $sql .= ' AND nucleoresponsavel_id <> :nucleoresponsavel_id';
        }

        $sql .= " ORDER BY nucleo_responsavel_nivel";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $servicePessoa               = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceAtividadegeralNucleo = new \Atividades\Service\AtividadegeralNucleo($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['nucleoresponsavelId']) {
                $objAtividadegeralNucleoResponsavel = $this->getRepository()->find($arrDados['nucleoresponsavelId']);

                if (!$objAtividadegeralNucleoResponsavel) {
                    $this->setLastError('Regitro de responsável pelo núcleo não existe!');

                    return false;
                }
            } else {
                $objAtividadegeralNucleoResponsavel = new \Atividades\Entity\AtividadegeralNucleoResponsavel();
            }

            if ($arrDados['pesId']) {
                $objPessoa = $servicePessoa->getRepository()->find($arrDados['pesId']);

                if (!$objPessoa) {
                    $this->setLastError('Registro de pessoa não existe!');

                    return false;
                }

                $objAtividadegeralNucleoResponsavel->setId($objPessoa);
            }

            if ($arrDados['nucleoId']) {
                $objAtividadegeralNucleo = $serviceAtividadegeralNucleo->getRepository()->find($arrDados['nucleoId']);

                if (!$objAtividadegeralNucleo) {
                    $this->setLastError('Registro de núcleo não existe!');

                    return false;
                }

                $objAtividadegeralNucleoResponsavel->setId($objAtividadegeralNucleo);
            }
            $objAtividadegeralNucleoResponsavel->setNucleoresponsavelInicio($arrDados['nucleoresponsavelInicio']);
            $objAtividadegeralNucleoResponsavel->setNucleoresponsavelFim($arrDados['nucleoresponsavelFim']);
            $objAtividadegeralNucleoResponsavel->setNucleoresponsavelObservacoes(
                $arrDados['nucleoresponsavelObservacoes']
            );
            $objAtividadegeralNucleoResponsavel->setNucleoResponsavelNivel($arrDados['nucleoResponsavelNivel']);

            $this->getEm()->persist($objAtividadegeralNucleoResponsavel);
            $this->getEm()->flush();

            $this->getEm()->commit();

            $arrDados['nucleoresponsavelId'] = $objAtividadegeralNucleoResponsavel->getNucleoresponsavelId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possíel salvar o registro de responsável pelo núcleo!');
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['pesId']) {
            $errors[] = 'Por favor preencha o campo "pesId"!';
        }

        if (!$arrParam['nucleoId']) {
            $errors[] = 'Por favor preencha o campo "nucleoId"!';
        }

        if (!in_array($arrParam['nucleoResponsavelNivel'], self::getNucleoResponsavelNivel())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "nucleoResponsavelNivel"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM atividadegeral__nucleo_responsavel";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($nucleoresponsavelId)
    {
        $arrDados                    = $this->getRepository()->find($nucleoresponsavelId);
        $servicePessoa               = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceAtividadegeralNucleo = new \Atividades\Service\AtividadegeralNucleo($this->getEm());

        try {
            $arrDados = $arrDados->toArray();

            if ($arrDados['pesId']) {
                $arrPesId          = [];
                $arrDados['pesId'] = is_array($arrDados['pesId']) ? $arrDados['pesId'] : explode(
                    ',',
                    $arrDados['pesId']
                );

                foreach ($arrDados['pesId'] as $pesId) {
                    $objPessoa = $servicePessoa->getRepository()->find($pesId);

                    if ($objPessoa) {
                        $arrPesId[] = array(
                            'id'   => $objPessoa->getPesId(),
                            'text' => $objPessoa->getPesId()
                        );
                    }
                }

                $arrDados['pesId'] = $arrPesId;
            }

            if ($arrDados['nucleoId']) {
                $arrNucleoId          = [];
                $arrDados['nucleoId'] = is_array($arrDados['nucleoId']) ? $arrDados['nucleoId'] : explode(
                    ',',
                    $arrDados['nucleoId']
                );

                foreach ($arrDados['nucleoId'] as $nucleoId) {
                    $objAtividadegeralNucleo = $serviceAtividadegeralNucleo->getRepository()->find($nucleoId);

                    if ($objAtividadegeralNucleo) {
                        $arrNucleoId[] = array(
                            'id'   => $objAtividadegeralNucleo->getNucleoId(),
                            'text' => $objAtividadegeralNucleo->getNucleoId()
                        );
                    }
                }

                $arrDados['nucleoId'] = $arrNucleoId;
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('nucleoresponsavelId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Atividades\Entity\AtividadegeralNucleoResponsavel */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getNucleoresponsavelId(),
                $params['value'] => $objEntity->getNucleoResponsavelNivel()
            );
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['nucleoresponsavelId']) {
            $this->setLastError(
                'Para remover um registro de responsável pelo núcleo é necessário especificar o código.'
            );

            return false;
        }

        try {
            $objAtividadegeralNucleoResponsavel = $this->getRepository()->find($param['nucleoresponsavelId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAtividadegeralNucleoResponsavel);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de responsável pelo núcleo.');

            return false;
        }

        return true;
    }
}
?>