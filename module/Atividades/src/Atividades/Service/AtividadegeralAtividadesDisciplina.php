<?php

namespace Atividades\Service;

use VersaSpine\Service\AbstractService;

class AtividadegeralAtividadesDisciplina extends AbstractService
{
    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Atividade\Entity\AtividadegeralAtividadesDisciplina');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function pesquisaForJson($params)
    {
        $sql             = 'SELECT * FROM atividadegeral__atividades_disciplina WHERE';
        $discId          = false;
        $atividadediscId = false;

        if ($params['q']) {
            $discId = $params['q'];
        } elseif ($params['query']) {
            $discId = $params['query'];
        }

        if ($params['atividadediscId']) {
            $atividadediscId = $params['atividadediscId'];
        }

        $parameters = array('disc_id' => "{$discId}%");
        $sql .= ' disc_id LIKE :disc_id';

        if ($atividadediscId) {
            $parameters['atividadedisc_id'] = $atividadediscId;
            $sql .= ' AND atividadedisc_id <> :atividadedisc_id';
        }

        $sql .= " ORDER BY disc_id";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceAtividadegeralConfiguracoesAtividades = new \Atividades\Service\AtividadegeralConfiguracoesAtividades(
            $this->getEm()
        );
        $serviceAcadgeralDisciplina                   = new \Matricula\Service\AcadgeralDisciplina($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['atividadediscId']) {
                $objAtividadegeralAtividadesDisciplina = $this->getRepository()->find($arrDados['atividadediscId']);

                if (!$objAtividadegeralAtividadesDisciplina) {
                    $this->setLastError('Regitro de atividadegeral__atividades_disciplina não existe!');

                    return false;
                }
            } else {
                $objAtividadegeralAtividadesDisciplina = new \Atividades\Entity\AtividadegeralAtividadesDisciplina();
            }

            if ($arrDados['atividadeconfatividadeId']) {
                $objAtividadegeralConfiguracoesAtividades = $serviceAtividadegeralConfiguracoesAtividades->getRepository(
                )->find($arrDados['atividadeconfatividadeId']);

                if (!$objAtividadegeralConfiguracoesAtividades) {
                    $this->setLastError('Registro de atividadegeral__configuracoes_atividades não existe!');

                    return false;
                }

                $objAtividadegeralAtividadesDisciplina->setId($objAtividadegeralConfiguracoesAtividades);
            }

            if ($arrDados['discId']) {
                $objAcadgeralDisciplina = $serviceAcadgeralDisciplina->getRepository()->find($arrDados['discId']);

                if (!$objAcadgeralDisciplina) {
                    $this->setLastError('Registro de disciplina não existe!');

                    return false;
                }

                $objAtividadegeralAtividadesDisciplina->setId($objAcadgeralDisciplina);
            }

            $this->getEm()->persist($objAtividadegeralAtividadesDisciplina);
            $this->getEm()->flush();

            $this->getEm()->commit();

            $arrDados['atividadediscId'] = $objAtividadegeralAtividadesDisciplina->getAtividadediscId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possíel salvar o registro de atividadegeral__atividades_disciplina!');
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['atividadeconfatividadeId']) {
            $errors[] = 'Por favor preencha o campo "atividadeconfatividadeId"!';
        }

        if (!$arrParam['discId']) {
            $errors[] = 'Por favor preencha o campo "discId"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM atividadegeral__atividades_disciplina";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($atividadediscId)
    {
        $arrDados                                     = $this->getRepository()->find($atividadediscId);
        $serviceAtividadegeralConfiguracoesAtividades = new \Atividades\Service\AtividadegeralConfiguracoesAtividades(
            $this->getEm()
        );
        $serviceAcadgeralDisciplina                   = new \Matricula\Service\AcadgeralDisciplina($this->getEm());

        try {
            $arrDados = $arrDados->toArray();

            if ($arrDados['atividadeconfatividadeId']) {
                $arrAtividadeconfatividadeId          = [];
                $arrDados['atividadeconfatividadeId'] = is_array(
                    $arrDados['atividadeconfatividadeId']
                ) ? $arrDados['atividadeconfatividadeId'] : explode(',', $arrDados['atividadeconfatividadeId']);

                foreach ($arrDados['atividadeconfatividadeId'] as $atividadeconfatividadeId) {
                    $objAtividadegeralConfiguracoesAtividades = $serviceAtividadegeralConfiguracoesAtividades->getRepository(
                    )->find($atividadeconfatividadeId);

                    if ($objAtividadegeralConfiguracoesAtividades) {
                        $arrAtividadeconfatividadeId[] = array(
                            'id'   => $objAtividadegeralConfiguracoesAtividades->getAtividadeconfatividadeId(),
                            'text' => $objAtividadegeralConfiguracoesAtividades->getAtividadeconfatividadeId()
                        );
                    }
                }

                $arrDados['atividadeconfatividadeId'] = $arrAtividadeconfatividadeId;
            }

            if ($arrDados['discId']) {
                $arrDiscId          = [];
                $arrDados['discId'] = is_array($arrDados['discId']) ? $arrDados['discId'] : explode(
                    ',',
                    $arrDados['discId']
                );

                foreach ($arrDados['discId'] as $discId) {
                    $objAcadgeralDisciplina = $serviceAcadgeralDisciplina->getRepository()->find($discId);

                    if ($objAcadgeralDisciplina) {
                        $arrDiscId[] = array(
                            'id'   => $objAcadgeralDisciplina->getDiscId(),
                            'text' => $objAcadgeralDisciplina->getDiscId()
                        );
                    }
                }

                $arrDados['discId'] = $arrDiscId;
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('atividadediscId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Atividades\Entity\AtividadegeralAtividadesDisciplina */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getAtividadediscId(),
                $params['value'] => $objEntity->getDiscId()
            );
        }

        return $arrEntitiesArr;
    }

    public function  remover($param)
    {
        if (!$param['atividadediscId']) {
            $this->setLastError(
                'Para remover um registro de atividadegeral__atividades_disciplina é necessário especificar o código.'
            );

            return false;
        }

        try {
            $objAtividadegeralAtividadesDisciplina = $this->getRepository()->find($param['atividadediscId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAtividadegeralAtividadesDisciplina);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de atividadegeral__atividades_disciplina.');

            return false;
        }

        return true;
    }
}
?>