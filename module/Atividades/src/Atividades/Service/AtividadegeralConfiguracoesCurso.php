<?php

namespace Atividades\Service;

use VersaSpine\Service\AbstractService;

class AtividadegeralConfiguracoesCurso extends AbstractService
{
    function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Atividades\Entity\AtividadegeralConfiguracoesCurso');
    }

    protected function valida($dados)
    {
        // TODO: Implement valida() method.
    }

    protected function pesquisaForJson($criterios)
    {
        $parameters = array();
        $query      = <<<SQL
        SELECT * FROM atividadegeral__configuracoes_curso JOIN campus_curso USING(cursocampus_id) WHERE 1
SQL;
        if ($criterios['q']) {
            $parameters['atividadeconf_portaria'] = "%" . $criterios['q'] . "%";
            $query .= " AND atividadeconf_portaria like :atividadeconf_portaria";
        } elseif ($criterios['query']) {
            $parameters['atividadeconf_portaria'] = "%" . $criterios['query'] . "%";
            $query .= " AND atividadeconf_portaria like :atividadeconf_portaria";
        }

        if ($criterios['atividadeconfigcursoId']) {
            $parameters['atividadeconfigcurso_id'] = $criterios['atividadeconfigcurso_id'];
            $query .= " AND atividadeconfigcurso_id = :atividadeconfigcurso_id";
        }

        if ($criterios['cursocampusId']) {
            $parameters['cursocampus_id'] = $criterios['cursocampusId'];
            $query .= " AND cursocampus_id = :cursocampus_id";
        }

        if ($criterios['cursoId']) {
            $parameters['curso_id'] = $criterios['cursoId'];
            $query .= " AND curso_id = :curso_id";
        }

        $result = $this->executeQueryWithParam($query, $parameters)->fetchAll();

        return $result;
    }

}