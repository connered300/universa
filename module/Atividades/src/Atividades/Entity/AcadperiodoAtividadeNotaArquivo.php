<?php

namespace Atividades\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoAtividadeNotaArquivo
 *
 * @ORM\Table(name="acadperiodo__atividade_nota_arquivo", indexes={@ORM\Index(name="fk_acadperiodo__atividade_nota_arquivo_arquivo1_idx", columns={"arq_id"}), @ORM\Index(name="fk_acadperiodo__atividade_nota_arquivo_acadperiodo__ativida_idx", columns={"ativaluno_id"})})
 * @ORM\Entity
 */
class AcadperiodoAtividadeNotaArquivo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="arq_ativ_aluno_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $arqAtivAlunoId;

    /**
     * @var integer
     *
     * @ORM\Column(name="ativaluno_id", type="integer", nullable=false)
     */
    private $ativalunoId;

    /**
     * @var integer
     *
     * @ORM\Column(name="arq_id", type="integer", nullable=false)
     */
    private $arqId;

}

