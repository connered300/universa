<?php

namespace Atividades\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoAtividadesArquivo
 *
 * @ORM\Table(name="acadperiodo__atividades_arquivo", indexes={@ORM\Index(name="fk_acadperiodo__atividades_arquivo_arquivo1_idx", columns={"arq_id"}), @ORM\Index(name="fk_acadperiodo__atividades_arquivo_acadperiodo__atividades1_idx", columns={"ativ_id"})})
 * @ORM\Entity
 */
class AcadperiodoAtividadesArquivo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ativ_arq_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $ativArqId;

    /**
     * @var integer
     *
     * @ORM\Column(name="ativ_id", type="integer", nullable=false)
     */
    private $ativId;

    /**
     * @var integer
     *
     * @ORM\Column(name="arq_id", type="integer", nullable=false)
     */
    private $arqId;

}

