<?php

namespace Atividades\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AtividadegeralNucleoResponsavel
 *
 * @ORM\Table(name="atividadegeral__nucleo_responsavel", indexes={@ORM\Index(name="fk_pessoa_atividadegeral__nucleo_atividadegeral__nucleo1_idx", columns={"nucleo_id"}), @ORM\Index(name="fk_pessoa_atividadegeral__nucleo_pessoa1_idx", columns={"pes_id"})})
 * @ORM\Entity
 */
class AtividadegeralNucleoResponsavel
{
    /**
     * @var integer
     *
     * @ORM\Column(name="nucleoresponsavel_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $nucleoresponsavelId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="nucleoresponsavel_inicio", type="datetime", nullable=true)
     */
    private $nucleoresponsavelInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="nucleoresponsavel_fim", type="datetime", nullable=true)
     */
    private $nucleoresponsavelFim;

    /**
     * @var string
     *
     * @ORM\Column(name="nucleoresponsavel_observacoes", type="string", length=45, nullable=true)
     */
    private $nucleoresponsavelObservacoes;

    /**
     * @var string
     *
     * @ORM\Column(name="nucleo_responsavel_nivel", type="string", nullable=true)
     */
    private $nucleoResponsavelNivel;

    /**
     * @var \AtividadegeralNucleo
     *
     * @ORM\ManyToOne(targetEntity="AtividadegeralNucleo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="nucleo_id", referencedColumnName="nucleo_id")
     * })
     */
    private $nucleo;

    /**
     * @var \Pessoa
     *
     * @ORM\ManyToOne(targetEntity="Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     */
    private $pes;

}

