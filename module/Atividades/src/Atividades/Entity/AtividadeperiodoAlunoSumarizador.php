<?php

use Doctrine\ORM\Mapping as ORM;

/**
 * AtividadeperiodoAlunoSumarizador
 *
 * @ORM\Table(name="atividadeperiodo__aluno_sumarizador", indexes={@ORM\Index(name="fk_acadgeral__aluno_atividade_acadgeral__aluno_curso1_idx", columns={"alunocurso_id"}), @ORM\Index(name="fk_acadgeral__aluno_atividade_aluno_nucleo1_idx", columns={"alunonucleo_id"}), @ORM\Index(name="fk_acadgeral__aluno_atividade_acadperiodo__aluno_resumo1_idx", columns={"res_id"}), @ORM\Index(name="fk_acadperiodo__aluno_atividade_acesso_pessoas1_idx", columns={"usuario_criador"}), @ORM\Index(name="fk_acadperiodo__aluno_atividade_acadgeral__disciplina1_idx", columns={"disc_id"})})
 * @ORM\Entity
 */
class AtividadeperiodoAlunoSumarizador
{
    /**
     * @var integer
     *
     * @ORM\Column(name="alunoatividade_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $alunoatividadeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="alunoatividade_total_pratico", type="integer", nullable=true)
     */
    private $alunoatividadeTotalPratico;

    /**
     * @var float
     *
     * @ORM\Column(name="alunoatividade_pontuacao", type="float", precision=10, scale=0, nullable=true)
     */
    private $alunoatividadePontuacao;

    /**
     * @var integer
     *
     * @ORM\Column(name="alunoatividade_serie", type="integer", nullable=true)
     */
    private $alunoatividadeSerie;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="alunotividade_data_criacao", type="datetime", nullable=true)
     */
    private $alunotividadeDataCriacao = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="alunotividade_data_alteracao", type="datetime", nullable=true)
     */
    private $alunotividadeDataAlteracao = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="alunoatividade_situacao", type="string", nullable=false)
     */
    private $alunoatividadeSituacao = 'Inconcluso';

    /**
     * @var \AcadgeralAlunoCurso
     *
     * @ORM\ManyToOne(targetEntity="AcadgeralAlunoCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="alunocurso_id", referencedColumnName="alunocurso_id")
     * })
     */
    private $alunocurso;

    /**
     * @var \AcadperiodoAlunoResumo
     *
     * @ORM\ManyToOne(targetEntity="AcadperiodoAlunoResumo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="res_id", referencedColumnName="res_id")
     * })
     */
    private $res;

    /**
     * @var \AtividadeperiodoAlunoNucleo
     *
     * @ORM\ManyToOne(targetEntity="AtividadeperiodoAlunoNucleo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="alunonucleo_id", referencedColumnName="alunonucleo_id")
     * })
     */
    private $alunonucleo;

    /**
     * @var \AcadgeralDisciplina
     *
     * @ORM\ManyToOne(targetEntity="AcadgeralDisciplina")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="disc_id", referencedColumnName="disc_id")
     * })
     */
    private $disc;

    /**
     * @var \AcessoPessoas
     *
     * @ORM\ManyToOne(targetEntity="AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_criador", referencedColumnName="id")
     * })
     */
    private $usuarioCriador;

}

