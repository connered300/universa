<?php

namespace Atividades\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AtividadegeralProcessoTipo
 *
 * @ORM\Table(name="atividadegeral__processo_tipo", uniqueConstraints={@ORM\UniqueConstraint(name="tprocesso_descricao_UNIQUE", columns={"processo_tipo_descricao"}), @ORM\UniqueConstraint(name="tprocesso_id_UNIQUE", columns={"processo_tipo_id"})})
 * @ORM\Entity
 */
class AtividadegeralProcessoTipo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="processo_tipo_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $processoTipoId;

    /**
     * @var string
     *
     * @ORM\Column(name="processo_tipo_descricao", type="string", length=45, nullable=false)
     */
    private $processoTipoDescricao;

    /**
     * @var string
     *
     * @ORM\Column(name="processo_tipo_observacao", type="string", length=45, nullable=true)
     */
    private $processoTipoObservacao;

}

