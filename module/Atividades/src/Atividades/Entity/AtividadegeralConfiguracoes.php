<?php

namespace Atividades\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AtividadegeralConfiguracoes
 *
 * @ORM\Table(name="atividadegeral__configuracoes", uniqueConstraints={@ORM\UniqueConstraint(name="atividadeconf_portaria_UNIQUE", columns={"atividadeconf_portaria"})})
 * @ORM\Entity
 */
class AtividadegeralConfiguracoes
{
    /**
     * @var string
     *
     * @ORM\Column(name="atividadeconf_portaria", type="string", length=45, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $atividadeconfPortaria;

    /**
     * @var integer
     *
     * @ORM\Column(name="atividadeconf_maximo_duracao", type="integer", nullable=false)
     */
    private $atividadeconfMaximoDuracao;

    /**
     * @var string
     *
     * @ORM\Column(name="atividadeconf_numero_decreto", type="string", length=45, nullable=false)
     */
    private $atividadeconfNumeroDecreto;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="atividadeconf_data_decreto", type="datetime", nullable=false)
     */
    private $atividadeconfDataDecreto;

    /**
     * @var integer
     *
     * @ORM\Column(name="atividadecarga_total_pontuacao", type="integer", nullable=false)
     */
    private $atividadecargaTotalPontuacao;

    /**
     * @var integer
     *
     * @ORM\Column(name="atividadeconf_carga_total_horas", type="integer", nullable=false)
     */
    private $atividadeconfCargaTotalHoras;

    /**
     * @var string
     *
     * @ORM\Column(name="atividadeconf_serie", type="string", nullable=false)
     */
    private $atividadeconfSerie;

}

