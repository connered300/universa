<?php

namespace Atividades\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AtividadeperiodoAlunoNucleoProcesso
 *
 * @ORM\Table(name="atividadeperiodo__aluno_nucleo_processo", indexes={@ORM\Index(name="fk_atividadeperiodo__aluno_nucleo_processo__al_idx", columns={"alunonucleo_id"}), @ORM\Index(name="fk_atividadeperiodo__aluno_nucleo_processo__proc_idx", columns={"atividade_processo_id"})})
 * @ORM\Entity
 */
class AtividadeperiodoAlunoNucleoProcesso
{
    /**
     * @var integer
     *
     * @ORM\Column(name="alunoprocesso_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $alunoprocessoId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="alunoprocesso_data_inicio", type="datetime", nullable=false)
     */
    private $alunoprocessoDataInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="alunoprocesso_data_fim", type="datetime", nullable=true)
     */
    private $alunoprocessoDataFim;

    /**
     * @var string
     *
     * @ORM\Column(name="alunoprocesso_observacoes", type="string", length=45, nullable=true)
     */
    private $alunoprocessoObservacoes;

    /**
     * @var \AtividadeperiodoAlunoNucleo
     *
     * @ORM\ManyToOne(targetEntity="AtividadeperiodoAlunoNucleo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="alunonucleo_id", referencedColumnName="alunonucleo_id")
     * })
     */
    private $alunonucleo;

    /**
     * @var \AtividadegeralProcesso
     *
     * @ORM\ManyToOne(targetEntity="AtividadegeralProcesso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="atividade_processo_id", referencedColumnName="atividade_processo_id")
     * })
     */
    private $atividadeProcesso;

}

