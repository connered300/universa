<?php

namespace Atividades\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AtividadeperiodoAlunoPortfolio
 *
 * @ORM\Table(name="atividadeperiodo__aluno_portfolio", indexes={@ORM\Index(name="fk_atividadeperiodo__aluno_portfolio_arquivo1_idx", columns={"arq_id"}), @ORM\Index(name="fk_atividadeperiodo__aluno_portfolio_aluno_nu_idx", columns={"alunonucleo_id"})})
 * @ORM\Entity
 */
class AtividadeperiodoAlunoPortfolio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="portfolio_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $portfolioId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="portfolioaluno_data_entrega", type="datetime", nullable=false)
     */
    private $portfolioalunoDataEntrega;

    /**
     * @var integer
     *
     * @ORM\Column(name="portfolioaluno_pontuacao", type="integer", nullable=true)
     */
    private $portfolioalunoPontuacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="portfolioaluno_data_avaliacao", type="datetime", nullable=true)
     */
    private $portfolioalunoDataAvaliacao;

    /**
     * @var string
     *
     * @ORM\Column(name="portfolio_aluno_retorno", type="text", length=65535, nullable=true)
     */
    private $portfolioAlunoRetorno;

    /**
     * @var string
     *
     * @ORM\Column(name="portfolioaluno_situacao", type="string", nullable=true)
     */
    private $portfolioalunoSituacao;

    /**
     * @var \AtividadeperiodoAlunoNucleo
     *
     * @ORM\ManyToOne(targetEntity="AtividadeperiodoAlunoNucleo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="alunonucleo_id", referencedColumnName="alunonucleo_id")
     * })
     */
    private $alunonucleo;

    /**
     * @var \GerenciadorArquivos\Entity\Arquivo
     *
     * @ORM\ManyToOne(targetEntity="GerenciadorArquivos\Entity\Arquivo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="arq_id", referencedColumnName="arq_id")
     * })
     */
    private $arq;

    /**
     * @return int
     */
    public function getPortfolioId()
    {
        return $this->portfolioId;
    }

    /**
     * @param int $portfolioId
     * @return AtividadeperiodoAlunoPortfolio
     */
    public function setPortfolioId($portfolioId)
    {
        $this->portfolioId = $portfolioId;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPortfolioalunoDataEntrega()
    {
        return $this->portfolioalunoDataEntrega;
    }

    /**
     * @param \DateTime $portfolioalunoDataEntrega
     * @return AtividadeperiodoAlunoPortfolio
     */
    public function setPortfolioalunoDataEntrega($portfolioalunoDataEntrega)
    {
        $this->portfolioalunoDataEntrega = $portfolioalunoDataEntrega;

        return $this;
    }

    /**
     * @return int
     */
    public function getPortfolioalunoPontuacao()
    {
        return $this->portfolioalunoPontuacao;
    }

    /**
     * @param int $portfolioalunoPontuacao
     * @return AtividadeperiodoAlunoPortfolio
     */
    public function setPortfolioalunoPontuacao($portfolioalunoPontuacao)
    {
        $this->portfolioalunoPontuacao = $portfolioalunoPontuacao;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPortfolioalunoDataAvaliacao()
    {
        return $this->portfolioalunoDataAvaliacao;
    }

    /**
     * @param \DateTime $portfolioalunoDataAvaliacao
     * @return AtividadeperiodoAlunoPortfolio
     */
    public function setPortfolioalunoDataAvaliacao($portfolioalunoDataAvaliacao)
    {
        $this->portfolioalunoDataAvaliacao = $portfolioalunoDataAvaliacao;

        return $this;
    }

    /**
     * @return string
     */
    public function getPortfolioAlunoRetorno()
    {
        return $this->portfolioAlunoRetorno;
    }

    /**
     * @param string $portfolioAlunoRetorno
     * @return AtividadeperiodoAlunoPortfolio
     */
    public function setPortfolioAlunoRetorno($portfolioAlunoRetorno)
    {
        $this->portfolioAlunoRetorno = $portfolioAlunoRetorno;

        return $this;
    }

    /**
     * @return string
     */
    public function getPortfolioalunoSituacao()
    {
        return $this->portfolioalunoSituacao;
    }

    /**
     * @param string $portfolioalunoSituacao
     * @return AtividadeperiodoAlunoPortfolio
     */
    public function setPortfolioalunoSituacao($portfolioalunoSituacao)
    {
        $this->portfolioalunoSituacao = $portfolioalunoSituacao;

        return $this;
    }

    /**
     * @return \AtividadeperiodoAlunoNucleo
     */
    public function getAlunonucleo()
    {
        return $this->alunonucleo;
    }

    /**
     * @param \AtividadeperiodoAlunoNucleo $alunonucleo
     * @return AtividadeperiodoAlunoPortfolio
     */
    public function setAlunonucleo($alunonucleo)
    {
        $this->alunonucleo = $alunonucleo;

        return $this;
    }

    /**
     * @return \GerenciadorArquivos\Entity\Arquivo
     */
    public function getArq()
    {
        return $this->arq;
    }

    /**
     * @param \GerenciadorArquivos\Entity\Arquivo $arq
     * @return AtividadeperiodoAlunoPortfolio
     */
    public function setArq($arq)
    {
        $this->arq = $arq;

        return $this;
    }

}

