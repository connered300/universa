<?php

namespace Atividades\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoAtividadeNota
 *
 * @ORM\Table(name="acadperiodo__atividade_nota", indexes={@ORM\Index(name="fk_acadgeral__atividade_nota_acadgeral__atividades1_idx", columns={"ativ_id"}), @ORM\Index(name="fk_acadperiodo__atividade_nota_acadperiodo__aluno_disciplin_idx", columns={"alunodisc_id"})})
 * @ORM\Entity
 */
class AcadperiodoAtividadeNota
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ativaluno_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $ativalunoId;

    /**
     * @var integer
     *
     * @ORM\Column(name="ativ_id", type="integer", nullable=false)
     */
    private $ativId;

    /**
     * @var integer
     *
     * @ORM\Column(name="alunodisc_id", type="integer", nullable=false)
     */
    private $alunodiscId;

    /**
     * @var float
     *
     * @ORM\Column(name="ativaluno_nota", type="float", precision=10, scale=0, nullable=true)
     */
    private $ativalunoNota;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ativaluno_carga_horaria", type="time", nullable=true)
     */
    private $ativalunoCargaHoraria;

}

