<?php

namespace Atividades\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AtividadegeralConfiguracoesCurso
 *
 * @ORM\Table(name="atividadegeral__configuracoes_curso", indexes={@ORM\Index(name="fk_campus_curso_atividadegeral__configuracoes_atividadegera_idx", columns={"atividadeconf_portaria"}),@ORM\Index(name="fk_campus_curso_atividadegeral__configuracoes_campus_curso1_idx", columns={"cursocampus_id"}),@ORM\Index(name="fk_atividadadegeral__configuracoes_curso_atividadegeral__Se_idx", columns={"secretaria_id"})})
 * @ORM\Entity
 */
class AtividadegeralConfiguracoesCurso
{

    /** @var integer
     *
     * @ORM\Column(name="atividadeconfigcurso_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $atividadeconfigcursoId;

    /**
     * @var \Matricula\Entity\CampusCurso
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\CampusCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cursocampus_id", referencedColumnName="cursocampus_id")
     * })
     */
    private $cursocampus;

    /**
     * @var AtividadegeralSecretaria
     *
     * @ORM\ManyToOne(targetEntity="AtividadegeralSecretaria")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="secretaria_id", referencedColumnName="secretaria_id")
     * })
     */
    private $secretaria;

    /**
     * @var AtividadegeralConfiguracoes
     *
     * @ORM\ManyToOne(targetEntity="AtividadegeralConfiguracoes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="atividadeconf_portaria", referencedColumnName="atividadeconf_portaria")
     * })
     */
    private $atividadeconfPortaria;

    /** @var \Datetime
     * @ORM\Column(name="atividadeconfigcurso_data_inicio", type="datetime", nullable=false)
     */
    private $atividadeconfigcursoDataInicio;

    /** @var \Datetime
     * @ORM\Column(name="atividadeconfigcurso_data_fim", type="datetime", nullable=true)
     */
    private $atividadeconfigcursoDataFim;

    /**
     * @return int
     */
    public function getAtividadeconfigcursoId()
    {
        return $this->atividadeconfigcursoId;
    }

    /**
     * @param int $atividadeconfigcursoId
     * @return AtividadegeralConfiguracoesCurso
     */
    public function setAtividadeconfigcursoId($atividadeconfigcursoId)
    {
        $this->atividadeconfigcursoId = $atividadeconfigcursoId;

        return $this;
    }

    /**
     * @return \Datetime
     */
    public function getAtividadeconfigcursoDataFim()
    {
        return $this->atividadeconfigcursoDataFim;
    }

    /**
     * @param \Datetime $atividadeconfigcursoDataFim
     * @return AtividadegeralConfiguracoesCurso
     */
    public function setAtividadeconfigcursoDataFim($atividadeconfigcursoDataFim)
    {
        $this->atividadeconfigcursoDataFim = $atividadeconfigcursoDataFim;

        return $this;
    }

    /**
     * @return \Datetime
     */
    public function getAtividadeconfigcursoDataInicio()
    {
        return $this->atividadeconfigcursoDataInicio;
    }

    /**
     * @param \Datetime $atividadeconfigcursoDataInicio
     * @return AtividadegeralConfiguracoesCurso
     */
    public function setAtividadeconfigcursoDataInicio($atividadeconfigcursoDataInicio)
    {
        $this->atividadeconfigcursoDataInicio = $atividadeconfigcursoDataInicio;

        return $this;
    }

    /**
     * @return AtividadegeralConfiguracoes
     */
    public function getAtividadeconfPortaria()
    {
        return $this->atividadeconfPortaria;
    }

    /**
     * @param AtividadegeralConfiguracoes
     * @return AtividadegeralConfiguracoes
     */
    public function setAtividadeconfPortaria($atividadeconfPortaria)
    {
        $this->atividadeconfPortaria = $atividadeconfPortaria;

        return $this;
    }

    /**
     * @return AtividadegeralSecretaria
     */
    public function getSecretaria()
    {
        return $this->secretaria;
    }

    /**
     * @param AtividadegeralSecretaria $secretaria
     * @return AtividadegeralConfiguracoesCurso
     */
    public function setSecretaria($secretaria)
    {
        $this->secretaria = $secretaria;

        return $this;
    }

    /**
     * @return \Matricula\Entity\CampusCurso
     */
    public function getCursocampus()
    {
        return $this->cursocampus;
    }

    /**
     * @param \Matricula\Entity\CampusCurso $cursocampus
     * @return AtividadegeralConfiguracoesCurso
     */
    public function setCursocampus($cursocampus)
    {
        $this->cursocampus = $cursocampus;

        return $this;
    }
}