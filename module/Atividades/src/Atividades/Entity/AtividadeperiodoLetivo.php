<?php

namespace Atividades\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AtividadeperiodoLetivo
 *
 * @ORM\Table(name="atividadeperiodo__letivo", indexes={@ORM\Index(name="fk_atividade__periodo__letivo_acadperiodo__letivo1_idx", columns={"per_id"}), @ORM\Index(name="fk_atividadeperiodo__letivo_atividadegeral__configuracoes_c_idx", columns={"atividadeconfigcurso_id"})})
 * @ORM\Entity
 */
class AtividadeperiodoLetivo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="atividadeperiodo_vagas", type="integer", nullable=true)
     */
    private $atividadeperiodoVagas;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="atividadeperiodo_data_inicio", type="datetime", nullable=true)
     */
    private $atividadeperiodoDataInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="atividadeperiodo_data_fechamento", type="datetime", nullable=true)
     */
    private $atividadeperiodoDataFechamento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="atividadeperiodo_funcionamento_inicio", type="time", nullable=true)
     */
    private $atividadeperiodoFuncionamentoInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="atividadeperiodo_funcionamento_fim", type="time", nullable=true)
     */
    private $atividadeperiodoFuncionamentoFim;

    /**
     * @var \Matricula\Entity\AcadperiodoLetivo
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="\Matricula\Entity\AcadperiodoLetivo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="per_id", referencedColumnName="per_id")
     * })
     */
    private $per;

    /**
     * @var AtividadegeralConfiguracoesCurso
     *
     * @ORM\ManyToOne(targetEntity="AtividadegeralConfiguracoesCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="atividadeconfigcurso_id", referencedColumnName="atividadeconfigcurso_id")
     * })
     */
    private $atividadegeralConfiguracoesCurso;

    /**
     * @return AtividadegeralConfiguracoesCurso
     */
    public function getAtividadegeralConfiguracoesCurso()
    {
        return $this->atividadegeralConfiguracoesCurso;
    }

    /**
     * @param AtividadegeralConfiguracoesCurso $atividadegeralConfiguracoesCurso
     * @return atividadegeralConfiguracoesCurso
     */
    public function setAtividadeConfiguracoesCurso($atividadegeralConfiguracoesCurso)
    {
        $this->atividadegeralConfiguracoesCurso = $atividadegeralConfiguracoesCurso;

        return $this;
    }

    /**
     * @return \Matricula\Entity\AcadperiodoLetivo
     */
    public function getPer()
    {
        return $this->per;
    }

    /**
     * @param \Matricula\Entity\AcadperiodoLetivo $per
     * @return AtividadeperiodoLetivo
     */
    public function setPer($per)
    {
        $this->per = $per;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getAtividadeperiodoFuncionamentoFim()
    {
        return $this->atividadeperiodoFuncionamentoFim;
    }

    /**
     * @param \DateTime $atividadeperiodoFuncionamentoFim
     * @return AtividadeperiodoLetivo
     */
    public function setAtividadeperiodoFuncionamentoFim($atividadeperiodoFuncionamentoFim)
    {
        $this->atividadeperiodoFuncionamentoFim = $atividadeperiodoFuncionamentoFim;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getAtividadeperiodoFuncionamentoInicio()
    {
        return $this->atividadeperiodoFuncionamentoInicio;
    }

    /**
     * @param \DateTime $atividadeperiodoFuncionamentoInicio
     * @return AtividadeperiodoLetivo
     */
    public function setAtividadeperiodoFuncionamentoInicio($atividadeperiodoFuncionamentoInicio)
    {
        $this->atividadeperiodoFuncionamentoInicio = $atividadeperiodoFuncionamentoInicio;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getAtividadeperiodoDataFechamento()
    {
        return $this->atividadeperiodoDataFechamento;
    }

    /**
     * @param \DateTime $atividadeperiodoDataFechamento
     * @return AtividadeperiodoLetivo
     */
    public function setAtividadeperiodoDataFechamento($atividadeperiodoDataFechamento)
    {
        $this->atividadeperiodoDataFechamento = $atividadeperiodoDataFechamento;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getAtividadeperiodoDataInicio()
    {
        return $this->atividadeperiodoDataInicio;
    }

    /**
     * @param \DateTime $atividadeperiodoDataInicio
     * @return AtividadeperiodoLetivo
     */
    public function setAtividadeperiodoDataInicio($atividadeperiodoDataInicio)
    {
        $this->atividadeperiodoDataInicio = $atividadeperiodoDataInicio;

        return $this;
    }

    /**
     * @return int
     */
    public function getAtividadeperiodoVagas()
    {
        return $this->atividadeperiodoVagas;
    }

    /**
     * @param int $atividadeperiodoVagas
     * @return AtividadeperiodoLetivo
     */
    public function setAtividadeperiodoVagas($atividadeperiodoVagas)
    {
        $this->atividadeperiodoVagas = $atividadeperiodoVagas;

        return $this;
    }

}

