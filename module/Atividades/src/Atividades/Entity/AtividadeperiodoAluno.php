<?php

namespace Atividades\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AtividadeperiodoAluno
 *
 * @ORM\Table(name="atividadeperiodo__aluno")
 * @ORM\Entity
 * @LG\LG(id="aluno_atividade_id",label="aluno_atividade_id")
 * @Jarvis\Jarvis(title="Listagem de atividade do aluno",icon="fa fa-table")
 */
class AtividadeperiodoAluno
{
    /**
     * @var integer
     *
     * @ORM\Column(name="aluno_atividade_id", type="integer", nullable=false, length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="aluno_atividade_id")
     * @LG\Labels\Attributes(text="código da atividade aluno")
     * @LG\Querys\Conditions(type="=")
     */
    private $alunoAtividadeId;

    /**
     * @var \Matricula\Entity\AcadperiodoAluno
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoAluno")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="alunoper_id", referencedColumnName="alunoper_id")
     * })
     */
    private $alunoper;

    /**
     * @var \Atividades\Entity\AtividadeperiodoEvento
     * @ORM\ManyToOne(targetEntity="Atividades\Entity\AtividadeperiodoEvento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="evento_id", referencedColumnName="evento_id")
     * })
     */
    private $evento;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_cadastro", referencedColumnName="id")
     * })
     */
    private $usuarioCadastro;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="aluno_atividade_data_lancamento", type="datetime", nullable=false)
     * @LG\Labels\Property(name="aluno_atividade_data_lancamento")
     * @LG\Labels\Attributes(text="data de lancamento da atividade")
     * @LG\Querys\Conditions(type="=")
     */
    private $alunoAtividadeDataLancamento;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="aluno_atividade_data_realizacao", type="datetime", nullable=false)
     * @LG\Labels\Property(name="aluno_atividade_data_realizacao")
     * @LG\Labels\Attributes(text="data de realização da atividade")
     * @LG\Querys\Conditions(type="=")
     */
    private $alunoAtividadeDataRealizacao;

    /**
     * @var integer
     *
     * @ORM\Column(name="aluno_atividade_horas_reais", type="integer", nullable=false, length=10)
     * @LG\Labels\Property(name="aluno_atividade_horas_reais")
     * @LG\Labels\Attributes(text="horas reais do evento")
     * @LG\Querys\Conditions(type="=")
     */
    private $alunoAtividadeHorasReais;

    /**
     * @var integer
     *
     * @ORM\Column(name="aluno_atividade_horas_validas", type="integer", nullable=false, length=10)
     * @LG\Labels\Property(name="aluno_atividade_horas_validas")
     * @LG\Labels\Attributes(text="horas válidas do evento")
     * @LG\Querys\Conditions(type="=")
     */
    private $alunoAtividadeHorasValidas;

    /**
     * @var string
     *
     * @ORM\Column(name="aluno_atividade_observacao", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="aluno_atividade_observacao")
     * @LG\Labels\Attributes(text="observação da atividade")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $alunoAtividadeObservacao;

    /**
     * @return integer
     */
    public function getAlunoAtividadeId()
    {
        return $this->alunoAtividadeId;
    }

    /**
     * @param integer $alunoAtividadeId
     * @return AtividadeperiodoAluno
     */
    public function setAlunoAtividadeId($alunoAtividadeId)
    {
        $this->alunoAtividadeId = $alunoAtividadeId;

        return $this;
    }

    /**
     * @return \Matricula\Entity\AcadperiodoAluno
     */
    public function getAlunoper()
    {
        return $this->alunoper;
    }

    /**
     * @param \Matricula\Entity\AcadperiodoAluno $alunoper
     * @return AtividadeperiodoAluno
     */
    public function setAlunoper($alunoper)
    {
        $this->alunoper = $alunoper;

        return $this;
    }

    /**
     * @return \Atividades\Entity\AtividadeperiodoEvento
     */
    public function getEvento()
    {
        return $this->evento;
    }

    /**
     * @param \Atividades\Entity\AtividadeperiodoEvento $evento
     * @return AtividadeperiodoAluno
     */
    public function setEvento($evento)
    {
        $this->evento = $evento;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuarioCadastro()
    {
        return $this->usuarioCadastro;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuarioCadastro
     * @return AtividadeperiodoAluno
     */
    public function setUsuarioCadastro($usuarioCadastro)
    {
        $this->usuarioCadastro = $usuarioCadastro;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getAlunoAtividadeDataLancamento($format = false)
    {
        $alunoAtividadeDataLancamento = $this->alunoAtividadeDataLancamento;

        if ($format && $alunoAtividadeDataLancamento) {
            $alunoAtividadeDataLancamento = $alunoAtividadeDataLancamento->format('d/m/Y H:i:s');
        }

        return $alunoAtividadeDataLancamento;
    }

    /**
     * @param \Datetime $alunoAtividadeDataLancamento
     * @return AtividadeperiodoAluno
     */
    public function setAlunoAtividadeDataLancamento($alunoAtividadeDataLancamento)
    {
        if ($alunoAtividadeDataLancamento) {
            if (is_string($alunoAtividadeDataLancamento)) {
                $alunoAtividadeDataLancamento = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $alunoAtividadeDataLancamento
                );
                $alunoAtividadeDataLancamento = new \Datetime($alunoAtividadeDataLancamento);
            }
        } else {
            $alunoAtividadeDataLancamento = null;
        }
        $this->alunoAtividadeDataLancamento = $alunoAtividadeDataLancamento;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getAlunoAtividadeDataRealizacao($format = false)
    {
        $alunoAtividadeDataRealizacao = $this->alunoAtividadeDataRealizacao;

        if ($format && $alunoAtividadeDataRealizacao) {
            $alunoAtividadeDataRealizacao = $alunoAtividadeDataRealizacao->format('d/m/Y H:i:s');
        }

        return $alunoAtividadeDataRealizacao;
    }

    /**
     * @param \Datetime $alunoAtividadeDataRealizacao
     * @return AtividadeperiodoAluno
     */
    public function setAlunoAtividadeDataRealizacao($alunoAtividadeDataRealizacao)
    {
        if ($alunoAtividadeDataRealizacao) {
            if (is_string($alunoAtividadeDataRealizacao)) {
                $alunoAtividadeDataRealizacao = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $alunoAtividadeDataRealizacao
                );
                $alunoAtividadeDataRealizacao = new \Datetime($alunoAtividadeDataRealizacao);
            }
        } else {
            $alunoAtividadeDataRealizacao = null;
        }
        $this->alunoAtividadeDataRealizacao = $alunoAtividadeDataRealizacao;

        return $this;
    }

    /**
     * @return integer
     */
    public function getAlunoAtividadeHorasReais()
    {
        return $this->alunoAtividadeHorasReais;
    }

    /**
     * @param integer $alunoAtividadeHorasReais
     * @return AtividadeperiodoAluno
     */
    public function setAlunoAtividadeHorasReais($alunoAtividadeHorasReais)
    {
        $this->alunoAtividadeHorasReais = $alunoAtividadeHorasReais;

        return $this;
    }

    /**
     * @return integer
     */
    public function getAlunoAtividadeHorasValidas()
    {
        return $this->alunoAtividadeHorasValidas;
    }

    /**
     * @param integer $alunoAtividadeHorasValidas
     * @return AtividadeperiodoAluno
     */
    public function setAlunoAtividadeHorasValidas($alunoAtividadeHorasValidas)
    {
        $this->alunoAtividadeHorasValidas = $alunoAtividadeHorasValidas;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlunoAtividadeObservacao()
    {
        return $this->alunoAtividadeObservacao;
    }

    /**
     * @param string $alunoAtividadeObservacao
     * @return AtividadeperiodoAluno
     */
    public function setAlunoAtividadeObservacao($alunoAtividadeObservacao)
    {
        $this->alunoAtividadeObservacao = $alunoAtividadeObservacao;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'alunoAtividadeId'             => $this->getAlunoAtividadeId(),
            'alunoper'                     => $this->getAlunoper(),
            'evento'                       => $this->getEvento(),
            'usuarioCadastro'              => $this->getUsuarioCadastro(),
            'alunoAtividadeDataLancamento' => $this->getAlunoAtividadeDataLancamento(true),
            'alunoAtividadeDataRealizacao' => $this->getAlunoAtividadeDataRealizacao(true),
            'alunoAtividadeHorasReais'     => $this->getAlunoAtividadeHorasReais(),
            'alunoAtividadeHorasValidas'   => $this->getAlunoAtividadeHorasValidas(),
            'alunoAtividadeObservacao'     => $this->getAlunoAtividadeObservacao(),
        );

        $array['alunoper']        = $this->getAlunoper() ? $this->getAlunoper()->getAlunoperId() : null;
        $array['evento']          = $this->getEvento() ? $this->getEvento()->getEventoId() : null;
        $array['usuarioCadastro'] = $this->getUsuarioCadastro() ? $this->getUsuarioCadastro()->getId() : null;
        $array['perId']           = $this->getAlunoper()->getTurma()->getPer()? $this->getAlunoper()->getTurma()->getPer()->getPerId() : null;
        $array['perNome']         = $array['perId'] ? $this->getAlunoper()->getTurma()->getPer()->getPerNome() : null;

        return $array;
    }
}
