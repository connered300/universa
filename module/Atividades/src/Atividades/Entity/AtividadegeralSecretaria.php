<?php

namespace Atividades\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AtividadegeralSecretaria
 *
 * @ORM\Table(name="atividadegeral__secretaria", indexes={@ORM\Index(name="fk_atividadegeral__Secretaria_pessoa_fisica1_idx", columns={"per_id"})})
 * @ORM\Entity
 */
class AtividadegeralSecretaria
{

    /** @var integer
     *
     * @ORM\Column(name="secretaria_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $secretariaId;

    /** @var  string
     * @ORM\Column(name="secretaria_nome", type="string", nullable=false)
     */
    private $secretariaNome;

    /** @var string
     * @ORM\Column(name="secretaria_descricao", type="string", nullable=false)
     */
    private $secretariaDescricao;

    /** @var \Datetime
     * @ORM\Column(name="secretaria_data_funcionamento", type="datetime", nullable=true)
     */
    private $secretariaDataFuncionamento;

    /** @var \Datetime
     * @ORM\Column(name="secretaria__data_fechamento", type="datetime", nullable=true)
     */
    private $secretariaDataFechamento;

    /**
     * @var \Pessoa\Entity\Pessoa
     *
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     */
    private $pesId;

    /**
     * @return int
     */
    public function getSecretariaId()
    {
        return $this->secretariaId;
    }

    /**
     * @param int $secretariaId
     * @return AtividadegeralSecretaria
     */
    public function setSecretariaId($secretariaId)
    {
        $this->secretariaId = $secretariaId;

        return $this;
    }

    /**
     * @return \Pessoa\Entity\Pessoa
     */
    public function getPesId()
    {
        return $this->pesId;
    }

    /**
     * @param \Pessoa\Entity\Pessoa $pesId
     * @return AtividadegeralSecretaria
     */
    public function setPesId($pesId)
    {
        $this->pesId = $pesId;

        return $this;
    }

    /**
     * @return \Datetime
     */
    public function getSecretariaDataFechamento()
    {
        return $this->secretariaDataFechamento;
    }

    /**
     * @param \Datetime $secretariaDataFechamento
     * @return AtividadegeralSecretaria
     */
    public function setSecretariaDataFechamento($secretariaDataFechamento)
    {
        $this->secretariaDataFechamento = $secretariaDataFechamento;

        return $this;
    }

    /**
     * @return \Datetime
     */
    public function getSecretariaDataFuncionamento()
    {
        return $this->secretariaDataFuncionamento;
    }

    /**
     * @param \Datetime $secretariaDataFuncionamento
     * @return AtividadegeralSecretaria
     */
    public function setSecretariaDataFuncionamento($secretariaDataFuncionamento)
    {
        $this->secretariaDataFuncionamento = $secretariaDataFuncionamento;

        return $this;
    }

    /**
     * @return string
     */
    public function getSecretariaDescricao()
    {
        return $this->secretariaDescricao;
    }

    /**
     * @param string $secretariaDescricao
     * @return AtividadegeralSecretaria
     */
    public function setSecretariaDescricao($secretariaDescricao)
    {
        $this->secretariaDescricao = $secretariaDescricao;

        return $this;
    }

    /**
     * @return string
     */
    public function getSecretariaNome()
    {
        return $this->secretariaNome;
    }

    /**
     * @param string $secretariaNome
     * @return AtividadegeralSecretaria
     */
    public function setSecretariaNome($secretariaNome)
    {
        $this->secretariaNome = $secretariaNome;

        return $this;
    }

}