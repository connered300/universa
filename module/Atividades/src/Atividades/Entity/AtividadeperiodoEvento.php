<?php

namespace Atividades\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AtividadeperiodoEvento
 *
 * @ORM\Table(name="atividadeperiodo__evento")
 * @ORM\Entity
 * @LG\LG(id="eventoId",label="EventoDescricao")
 * @Jarvis\Jarvis(title="Listagem de evento",icon="fa fa-table")
 */
class AtividadeperiodoEvento
{
    /**
     * @var integer
     *
     * @ORM\Column(name="evento_id", type="integer", nullable=false, length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="evento_id")
     * @LG\Labels\Attributes(text="código")
     * @LG\Querys\Conditions(type="=")
     */
    private $eventoId;

    /**
     * @var \Atividades\Entity\AtividadegeralAtividades
     * @ORM\ManyToOne(targetEntity="Atividades\Entity\AtividadegeralAtividades")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="evento_tipo_atividade", referencedColumnName="atividadeatividade_id")
     * })
     */
    private $eventoTipoAtividade;
    /**
     * @var \Atividades\Entity\AtividadegeralSecretaria
     * @ORM\ManyToOne(targetEntity="Atividades\Entity\AtividadegeralSecretaria")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="evento_secretaria", referencedColumnName="secretaria_id")
     * })
     */
    private $eventoSecretaria;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_cadastro", referencedColumnName="id")
     * })
     */
    private $usuarioCadastro;

    /**
     * @var string
     *
     * @ORM\Column(name="evento_tipo", type="string", nullable=false, length=7)
     * @LG\Labels\Property(name="evento_tipo")
     * @LG\Labels\Attributes(text="tipo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $eventoTipo;

    /**
     * @var string
     *
     * @ORM\Column(name="evento_nome", type="string", nullable=false, length=45)
     * @LG\Labels\Property(name="evento_nome")
     * @LG\Labels\Attributes(text="nome")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $eventoNome;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="evento_data", type="datetime", nullable=true)
     * @LG\Labels\Property(name="evento_data")
     * @LG\Labels\Attributes(text="data")
     * @LG\Querys\Conditions(type="=")
     */
    private $eventoData;

    /**
     * @var integer
     *
     * @ORM\Column(name="evento_horas_validas", type="integer", nullable=false, length=10)
     * @LG\Labels\Property(name="evento_horas_validas")
     * @LG\Labels\Attributes(text="horas válidas do evento")
     * @LG\Querys\Conditions(type="=")
     */
    private $eventoHorasValidas;

    /**
     * @var string
     *
     * @ORM\Column(name="evento_responsavel", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="evento_responsavel")
     * @LG\Labels\Attributes(text="responsável")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $eventoResponsavel;

    /**
     * @var string
     *
     * @ORM\Column(name="evento_descricao", type="text", nullable=true)
     * @LG\Labels\Property(name="evento_descricao")
     * @LG\Labels\Attributes(text="descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $eventoDescricao;

    /**
     * @var integer
     *
     * @ORM\Column(name="evento_lotacao", type="integer", nullable=true, length=11)
     * @LG\Labels\Property(name="evento_lotacao")
     * @LG\Labels\Attributes(text="lotação")
     * @LG\Querys\Conditions(type="=")
     */
    private $eventoLotacao;

    /**
     * @return integer
     */
    public function getEventoId()
    {
        return $this->eventoId;
    }

    /**
     * @param integer $eventoId
     * @return AtividadeperiodoEvento
     */
    public function setEventoId($eventoId)
    {
        $this->eventoId = $eventoId;

        return $this;
    }

    /**
     * @return \Atividades\Entity\AtividadegeralSecretaria
     */
    public function getSecretaria()
    {
        return $this->eventoSecretaria;
    }

    /**
     * @param \Atividades\Entity\AtividadegeralSecretaria $objSecretaria
     * @return AtividadeperiodoEvento
     */
    public function setSecretaria($objSecretaria)
    {
        $this->eventoSecretaria = $objSecretaria;

        return $this;
    }

    /**
     * @return \Atividades\Entity\AtividadegeralTipo
     */
    public function getEventoTipoAtividade()
    {
        return $this->eventoTipoAtividade;
    }

    /**
     * @param \Atividades\Entity\AtividadegeralTipo $evento_tipo_atividade
     * @return AtividadeperiodoEvento
     */
    public function setEventoTipoAtividade($eventoTipoAtividade)
    {
        $this->eventoTipoAtividade = $eventoTipoAtividade;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuarioCadastro()
    {
        return $this->usuarioCadastro;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuarioCadastro
     * @return AtividadeperiodoEvento
     */
    public function setUsuarioCadastro($usuarioCadastro)
    {
        $this->usuarioCadastro = $usuarioCadastro;

        return $this;
    }

    /**
     * @return string
     */
    public function getEventoTipo()
    {
        return $this->eventoTipo;
    }

    /**
     * @param string $eventoTipo
     * @return AtividadeperiodoEvento
     */
    public function setEventoTipo($eventoTipo)
    {
        $this->eventoTipo = $eventoTipo;

        return $this;
    }

    /**
     * @return string
     */
    public function getEventoNome()
    {
        return $this->eventoNome;
    }

    /**
     * @param string $eventoNome
     * @return AtividadeperiodoEvento
     */
    public function setEventoNome($eventoNome)
    {
        $this->eventoNome = $eventoNome;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getEventoData($format = false)
    {
        $eventoData = $this->eventoData;

        if ($format && $eventoData) {
            $eventoData = $eventoData->format('d/m/Y H:i:s');
        }

        return $eventoData;
    }

    /**
     * @param \Datetime $eventoData
     * @return AtividadeperiodoEvento
     */
    public function setEventoData($eventoData)
    {
        if ($eventoData) {
            if (is_string($eventoData)) {
                $eventoData = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $eventoData
                );
                $eventoData = new \Datetime($eventoData);
            }
        } else {
            $eventoData = null;
        }
        $this->eventoData = $eventoData;

        return $this;
    }

    /**
     * @return string
     */
    public function getEventoHorasValidas()
    {
        return $this->eventoHorasValidas;
    }

    /**
     * @param string $eventoHorasValidas
     * @return AtividadeperiodoEvento
     */
    public function setEventoHorasValidas($eventoHorasValidas)
    {
        $this->eventoHorasValidas = $eventoHorasValidas;

        return $this;
    }

    /**
     * @return string
     */
    public function getEventoResponsavel()
    {
        return $this->eventoResponsavel;
    }

    /**
     * @param string $eventoResponsavel
     * @return AtividadeperiodoEvento
     */
    public function setEventoResponsavel($eventoResponsavel)
    {
        $this->eventoResponsavel = $eventoResponsavel;

        return $this;
    }

    /**
     * @return string
     */
    public function getEventoDescricao()
    {
        return $this->eventoDescricao;
    }

    /**
     * @param string $eventoDescricao
     * @return AtividadeperiodoEvento
     */
    public function setEventoDescricao($eventoDescricao)
    {
        $this->eventoDescricao = $eventoDescricao;

        return $this;
    }

    /**
     * @return integer
     */
    public function getEventoLotacao()
    {
        return $this->eventoLotacao;
    }

    /**
     * @param integer $eventoLotacao
     * @return AtividadeperiodoEvento
     */
    public function setEventoLotacao($eventoLotacao)
    {
        $this->eventoLotacao = $eventoLotacao;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'eventoId'              => $this->getEventoId(),
            'atividadeatividade'    => $this->getEventoTipoAtividade(),
            'tipoAtivividadeEvento' => $this->getEventoTipoAtividade(),
            'usuarioCadastro'       => $this->getUsuarioCadastro(),
            'eventoTipo'            => $this->getEventoTipo(),
            'eventoNome'            => $this->getEventoNome(),
            'eventoData'            => $this->getEventoData(true),
            'eventoHorasValidas'    => $this->getEventoHorasValidas(),
            'eventoResponsavel'     => $this->getEventoResponsavel(),
            'eventoDescricao'       => $this->getEventoDescricao(),
            'eventoLotacao'         => $this->getEventoLotacao(),
            'eventoSecretaria'      => $this->getSecretaria()
        );

        $array['usuarioCadastro'] = $this->getUsuarioCadastro() ? $this->getUsuarioCadastro()->getId() : null;

        return $array;
    }
}
