<?php

namespace Atividades\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AtividadeperiodoAlunoAtividades
 *
 * @ORM\Table(name="atividadeperiodo__aluno_atividades", indexes={@ORM\Index(name="fk_atividadeperiodo__aluno_atividades_acesso_pessoas1_idx", columns={"usuario_lancamento"}), @ORM\Index(name="fk_atividadeperiodo__aluno_atividades_processo1_idx", columns={"atividadeprocesso_id"}), @ORM\Index(name="fk_atividadeperiodo__aluno_atividades_atividade_idx", columns={"atividadeatividade_id"}), @ORM\Index(name="fk_atividadeperiodo__aluno_atividadeperiodo__aluno_n_idx", columns={"alunonucleo_id"}),
 * @ORM\Index(name="fk_atividadeperiodo__aluno_atividades_1_idx", columns={"usuario_alteracao"})})
 * @ORM\Entity
 */
class AtividadeperiodoAlunoAtividades
{
    /**
     * @var integer
     *
     * @ORM\Column(name="atividadealuno_atividade_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $atividadealunoAtividadeId;

    /**
     * @var string
     *
     * @ORM\Column(name="alunoatividade_descricao", type="text", length=65535, nullable=true)
     */
    private $alunoatividadeDescricao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="alunoatividade_data_inicial", type="datetime", nullable=false)
     */
    private $alunoatividadeDataInicial;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="alunoatividade_data_final", type="datetime", nullable=true)
     */
    private $alunoatividadeDataFinal;

    /**
     * @var integer
     *
     * @ORM\Column(name="alunoatividade_pontos", type="integer", nullable=true)
     */
    private $alunoatividadePontos;

    /**
     * @var integer
     *
     * @ORM\Column(name="alunoatividade_horas", type="integer", nullable=true)
     */
    private $alunoatividadeHoras;

    /**
     * @var string
     *
     * @ORM\Column(name="alunoatividades_observacao", type="string", length=45, nullable=true)
     */
    private $alunoatividadesObservacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="alunoatividade_data_lancamento", type="datetime", nullable=false)
     */
    private $alunoatividadeDataLancamento;

    /**
     * @var \AcessoPessoas
     *
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_lancamento", referencedColumnName="id")
     * })
     */
    private $usuarioLancamento;

    /**
     * @var \AtividadeperiodoAlunoNucleo
     *
     * @ORM\ManyToOne(targetEntity="AtividadeperiodoAlunoNucleo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="alunonucleo_id", referencedColumnName="alunonucleo_id")
     * })
     */
    private $alunonucleo;

    /**
     * @var \AtividadegeralProcesso
     *
     * @ORM\ManyToOne(targetEntity="AtividadegeralProcesso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="atividadeprocesso_id", referencedColumnName="atividade_processo_id")
     * })
     */
    private $atividadeprocesso;

    /**
     * @var \AtividadegeralAtividades
     *
     * @ORM\ManyToOne(targetEntity="AtividadegeralAtividades")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="atividadeatividade_id", referencedColumnName="atividadeatividade_id")
     * })
     */
    private $atividadeatividade;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     *
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_alteracao", referencedColumnName="id")
     * })
     */
    private $usuarioAlteracao;

    /**
     * @return AtividadegeralAtividades
     */
    public function getAtividadeatividade()
    {
        return $this->atividadeatividade;
    }

    /**
     * @param AtividadegeralAtividades $atividadeatividade
     * @return AtividadeperiodoAlunoAtividades
     */
    public function setAtividadeatividade($atividadeatividade)
    {
        $this->atividadeatividade = $atividadeatividade;

        return $this;
    }

    public function toArray()
    {
        return [
            'atividadealunoAtividadeId'    => $this->getAtividadealunoAtividadeId(),
            'alunoatividadeDescricao'      => $this->getAlunoatividadeDescricao(),
            'alunoatividadePontos'         => $this->getAlunoatividadePontos(),
            'alunoatividadeHoras'          => $this->getAlunoatividadeHoras(),
            'alunoatividadesObservacao'    => $this->getAlunoatividadesObservacao(),
            'usuarioLancamento'            => $this->getUsuarioLancamento(),
            'alunonucleo'                  => $this->getAlunonucleo(),
            'alunoatividadeDataInicial'    => $this->getAlunoatividadeDataInicial(),
            'alunoatividadeDataFinal'      => $this->getAlunoatividadeDataFinal(),
            'atividadeprocesso'            => $this->getAtividadeprocesso(),
            'alunoatividadeDataLancamento' => $this->getAlunoatividadeDataLancamento(),
        ];
    }

    /**
     * @return int
     */
    public function getAtividadealunoAtividadeId()
    {
        return $this->atividadealunoAtividadeId;
    }

    /**
     * @param int $atividadealunoAtividadeId
     * @return AtividadeperiodoAlunoAtividades
     */
    public function setAtividadealunoAtividadeId($atividadealunoAtividadeId)
    {
        $this->atividadealunoAtividadeId = $atividadealunoAtividadeId;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlunoatividadeDescricao()
    {
        return $this->alunoatividadeDescricao;
    }

    /**
     * @param string $alunoatividadeDescricao
     * @return AtividadeperiodoAlunoAtividades
     */
    public function setAlunoatividadeDescricao($alunoatividadeDescricao)
    {
        $this->alunoatividadeDescricao = $alunoatividadeDescricao;

        return $this;
    }

    /**
     * @return int
     */
    public function getAlunoatividadePontos()
    {
        return $this->alunoatividadePontos;
    }

    /**
     * @param int $alunoatividadePontos
     * @return AtividadeperiodoAlunoAtividades
     */
    public function setAlunoatividadePontos($alunoatividadePontos)
    {
        $this->alunoatividadePontos = $alunoatividadePontos;

        return $this;
    }

    /**
     * @return int
     */
    public function getAlunoatividadeHoras()
    {
        return $this->alunoatividadeHoras;
    }

    /**
     * @param int $alunoatividadeHoras
     * @return AtividadeperiodoAlunoAtividades
     */
    public function setAlunoatividadeHoras($alunoatividadeHoras)
    {
        $this->alunoatividadeHoras = $alunoatividadeHoras;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlunoatividadesObservacao()
    {
        return $this->alunoatividadesObservacao;
    }

    /**
     * @param string $alunoatividadesObservacao
     * @return AtividadeperiodoAlunoAtividades
     */
    public function setAlunoatividadesObservacao($alunoatividadesObservacao)
    {
        $this->alunoatividadesObservacao = $alunoatividadesObservacao;

        return $this;
    }

    /**
     * @return AcessoPessoas
     */
    public function getUsuarioLancamento()
    {
        return $this->usuarioLancamento;
    }

    /**
     * @param AcessoPessoas $usuarioLancamento
     * @return AtividadeperiodoAlunoAtividades
     */
    public function setUsuarioLancamento($usuarioLancamento)
    {
        $this->usuarioLancamento = $usuarioLancamento;

        return $this;
    }

    /**
     * @return AtividadeperiodoAlunoNucleo
     */
    public function getAlunonucleo()
    {
        return $this->alunonucleo;
    }

    /**
     * @param AtividadeperiodoAlunoNucleo $alunonucleo
     * @return AtividadeperiodoAlunoAtividades
     */
    public function setAlunonucleo($alunonucleo)
    {
        $this->alunonucleo = $alunonucleo;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getAlunoatividadeDataInicial()
    {
        return $this->alunoatividadeDataInicial;
    }

    /**
     * @param DateTime $alunoatividadeDataInicial
     * @return AtividadeperiodoAlunoAtividades
     */
    public function setAlunoatividadeDataInicial($alunoatividadeDataInicial)
    {
        $this->alunoatividadeDataInicial = $alunoatividadeDataInicial;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getAlunoatividadeDataFinal()
    {
        return $this->alunoatividadeDataFinal;
    }

    /**
     * @param DateTime $alunoatividadeDataFinal
     * @return AtividadeperiodoAlunoAtividades
     */
    public function setAlunoatividadeDataFinal($alunoatividadeDataFinal)
    {
        $this->alunoatividadeDataFinal = $alunoatividadeDataFinal;

        return $this;
    }

    /**
     * @return AtividadegeralProcesso
     */
    public function getAtividadeprocesso()
    {
        return $this->atividadeprocesso;
    }

    /**
     * @param AtividadegeralProcesso $atividadeprocesso
     * @return AtividadeperiodoAlunoAtividades
     */
    public function setAtividadeprocesso($atividadeprocesso)
    {
        $this->atividadeprocesso = $atividadeprocesso;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getAlunoatividadeDataLancamento()
    {
        return $this->alunoatividadeDataLancamento;
    }

    /**
     * @param DateTime $alunoatividadeDataLancamento
     * @return AtividadeperiodoAlunoAtividades
     */
    public function setAlunoatividadeDataLancamento($alunoatividadeDataLancamento)
    {
        $this->alunoatividadeDataLancamento = $alunoatividadeDataLancamento;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuarioAlteracao()
    {
        return $this->usuarioAlteracao;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuarioAlteracao
     * @return AtividadeperiodoAlunoAtividades
     */
    public function setUsuarioAlteracao($usuarioAlteracao)
    {
        $this->usuarioAlteracao = $usuarioAlteracao;

        return $this;
    }

}

