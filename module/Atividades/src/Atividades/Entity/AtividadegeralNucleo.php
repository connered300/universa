<?php

namespace Atividades\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AtividadegeralNucleo
 *
 * @ORM\Table(name="atividadegeral__nucleo")
 * @ORM\Entity
 */
class AtividadegeralNucleo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="nucleo_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $nucleoId;

    /**
     * @var string
     *
     * @ORM\Column(name="nucleo_descricao", type="string", length=45, nullable=true)
     */
    private $nucleoDescricao;

    /**
     * @var string
     *
     * @ORM\Column(name="nucleo_capacidade_total", type="string", length=45, nullable=true)
     */
    private $nucleoCapacidadeTotal;

    /**
     * @var string
     *
     * @ORM\Column(name="nucleo_tipo", type="string", nullable=true)
     */
    private $nucleoTipo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="nucleo_efetivacao_inicio", type="datetime", nullable=true)
     */
    private $nucleoEfetivacaoInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="nucleo_efetivacao_fim", type="datetime", nullable=true)
     */
    private $nucleoEfetivacaoFim;

    /**
     * @var string
     *
     * @ORM\Column(name="nucleo_dias_funcionamento", type="string", nullable=true)
     */
    private $nucleoDiasFuncionamento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="nucleo_funcionamento_abertura", type="time", nullable=true)
     */
    private $nucleoFuncionamentoAbertura;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="nucleo_funcionamento_fechamento", type="time", nullable=true)
     */
    private $nucleoFuncionamentoFechamento;

    /**
     * @var integer
     *
     * @ORM\Column(name="nucleo_vagas", type="integer", nullable=true)
     */
    private $nucleoVagas;

}

