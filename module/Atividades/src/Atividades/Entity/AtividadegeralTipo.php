<?php

namespace Atividades\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AtividadegeralTipo
 *
 * @ORM\Table(name="atividadegeral__tipo")
 * @ORM\Entity
 * @LG\LG(id="atividadeatividadeId",label="AtividadeatividadeDescricao")
 * @Jarvis\Jarvis(title="Listagem de atividadegeral tipo",icon="fa fa-table")
 */
class AtividadegeralTipo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="atividadeatividade_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="atividadeatividade_id")
     * @LG\Labels\Attributes(text="código do tipo de atividade")
     * @LG\Querys\Conditions(type="=")
     */
    private $atividadeatividadeId;

    /**
     * @var string
     *
     * @ORM\Column(name="atividadeatividade_descricao", type="string", nullable=false, length=45)
     * @LG\Labels\Property(name="atividadeatividade_descricao")
     * @LG\Labels\Attributes(text="descrição do tipo de atividade")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $atividadeatividadeDescricao;

    /**
     * @var string
     *
     * @ORM\Column(name="atividadeatividade_observacoes", type="text", nullable=false)
     * @LG\Labels\Property(name="atividadeatividade_observacoes")
     * @LG\Labels\Attributes(text="observacões do tipo de atividade")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $atividadeatividadeObservacoes;

    /**
     * @return integer
     */
    public function getAtividadeatividadeId()
    {
        return $this->atividadeatividadeId;
    }

    /**
     * @param integer $atividadeatividadeId
     * @return AtividadegeralTipo
     */
    public function setAtividadeatividadeId($atividadeatividadeId)
    {
        $this->atividadeatividadeId = $atividadeatividadeId;

        return $this;
    }

    /**
     * @return string
     */
    public function getAtividadeatividadeDescricao()
    {
        return $this->atividadeatividadeDescricao;
    }

    /**
     * @param string $atividadeatividadeDescricao
     * @return AtividadegeralTipo
     */
    public function setAtividadeatividadeDescricao($atividadeatividadeDescricao)
    {
        $this->atividadeatividadeDescricao = $atividadeatividadeDescricao;

        return $this;
    }

    /**
     * @return string
     */
    public function getAtividadeatividadeObservacoes()
    {
        return $this->atividadeatividadeObservacoes;
    }

    /**
     * @param string $atividadeatividadeObservacoes
     * @return AtividadegeralTipo
     */
    public function setAtividadeatividadeObservacoes($atividadeatividadeObservacoes)
    {
        $this->atividadeatividadeObservacoes = $atividadeatividadeObservacoes;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'atividadeatividadeId'          => $this->getAtividadeatividadeId(),
            'atividadeatividadeDescricao'   => $this->getAtividadeatividadeDescricao(),
            'atividadeatividadeObservacoes' => $this->getAtividadeatividadeObservacoes(),
        );

        return $array;
    }
}
