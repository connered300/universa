<?php

namespace Atividades\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AtividadegeralNucleoSala
 *
 * @ORM\Table(name="atividadegeral__nucleo__sala", uniqueConstraints={@ORM\UniqueConstraint(name="atividadegeral_sala_id_UNIQUE", columns={"atividadegeral_sala_nucleo_id"})}, indexes={@ORM\Index(name="fk_atividadegeral__nucleo__sala_atividadegeral__nucleo1_idx", columns={"nucleo_id"}), @ORM\Index(name="fk_atividadegeral__nucleo__sala_infra_sala1_idx", columns={"sala_id"})})
 * @ORM\Entity
 */
class AtividadegeralNucleoSala
{
    /**
     * @var integer
     *
     * @ORM\Column(name="atividadegeral_sala_nucleo_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $atividadegeralSalaNucleoId;

    /**
     * @var \AtividadegeralNucleo
     *
     * @ORM\ManyToOne(targetEntity="AtividadegeralNucleo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="nucleo_id", referencedColumnName="nucleo_id")
     * })
     */
    private $nucleo;

    /**
     * @var \InfraSala
     *
     * @ORM\ManyToOne(targetEntity="InfraSala")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sala_id", referencedColumnName="sala_id")
     * })
     */
    private $sala;

}

