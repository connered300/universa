<?php

namespace Atividades\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AtividadeperiodoAlunoNucleo
 *
 * @ORM\Table(name="atividadeperiodo__aluno_nucleo", indexes={@ORM\Index(name="fk_atividadeperiodo__aluno_nucleo_atividadegeral__nucleo1_idx", columns={"nucleo_id"}), @ORM\Index(name="fk_atividadeperiodo__aluno_nucleo_periodo__letivo1_idx", columns={"per_id"}), @ORM\Index(name="fk_atividadeperiodo__aluno_nucleo_acadperiodo__aluno1_idx", columns={"alunoper_id"}),
 * @ORM\Index(name="fk_atividadeperiodo__aluno_nucleo_1_idx", columns={"usuario_alteracao"})})
 * @ORM\Entity
 */
class AtividadeperiodoAlunoNucleo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="alunonucleo_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $alunonucleoId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="alunoperiodo_data_inscricao", type="datetime", nullable=false)
     */
    private $alunoperiodoDataInscricao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="alunoperiodo_data_fechamento", type="datetime", nullable=true)
     */
    private $alunoperiodoDataFechamento;

    /**
     * @var integer
     *
     * @ORM\Column(name="alunoperiodo_total_horas", type="integer", nullable=true)
     */
    private $alunoperiodoTotalHoras;

    /**
     * @var string
     *
     * @ORM\Column(name="alunoperiodo_observacoes", type="string", length=45, nullable=true)
     */
    private $alunoperiodoObservacoes;

    /**
     * @var \Matricula\Entity\AcadperiodoAluno
     *
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoAluno")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="alunoper_id", referencedColumnName="alunoper_id")
     * })
     */
    private $alunoper;

    /**
     * @var \Atividades\Entity\AtividadeperiodoLetivo
     *
     * @ORM\ManyToOne(targetEntity="\Atividades\Entity\AtividadeperiodoLetivo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="per_id", referencedColumnName="per_id")
     * })
     */
    private $per;

    /**
     * @var \AtividadegeralNucleo
     *
     * @ORM\ManyToOne(targetEntity="AtividadegeralNucleo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="nucleo_id", referencedColumnName="nucleo_id")
     * })
     */
    private $nucleo;

    /** @var string
     * @ORM\Column(name="alunoperiodo_status", type="string", nullable=false)
     */
    private $alunoperiodoStatus;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     *
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_alteracao", referencedColumnName="id")
     * })
     */
    private $usuarioAlteracao;

    /**
     * @return \AtividadegeralNucleo
     */
    public function getNucleo()
    {
        return $this->nucleo;
    }

    /**
     * @param \AtividadegeralNucleo $nucleo
     * @return AtividadeperiodoAlunoNucleo
     */
    public function setNucleo($nucleo)
    {
        $this->nucleo = $nucleo;

        return $this;
    }

    /**
     * @return \Atividades\Entity\AtividadeperiodoLetivo
     */
    public function getPer()
    {
        return $this->per;
    }

    /**
     * @param \Atividades\Entity\AtividadeperiodoLetivo $per
     * @return \Atividades\Entity\AtividadeperiodoLetivo
     */
    public function setPer($per)
    {
        $this->per = $per;

        return $this;
    }

    /**
     * @return \Matricula\Entity\AcadperiodoAluno
     */
    public function getAlunoper()
    {
        return $this->alunoper;
    }

    /**
     * @param \Matricula\Entity\AcadperiodoAluno $alunoper
     * @return AtividadeperiodoAlunoNucleo
     */
    public function setAlunoper($alunoper)
    {
        $this->alunoper = $alunoper;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlunoperiodoObservacoes()
    {
        return $this->alunoperiodoObservacoes;
    }

    /**
     * @param string $alunoperiodoObservacoes
     * @return AtividadeperiodoAlunoNucleo
     */
    public function setAlunoperiodoObservacoes($alunoperiodoObservacoes)
    {
        $this->alunoperiodoObservacoes = $alunoperiodoObservacoes;

        return $this;
    }

    /**
     * @return int
     */
    public function getAlunoperiodoTotalHoras()
    {
        return $this->alunoperiodoTotalHoras;
    }

    /**
     * @param int $alunoperiodoTotalHoras
     * @return AtividadeperiodoAlunoNucleo
     */
    public function setAlunoperiodoTotalHoras($alunoperiodoTotalHoras)
    {
        $this->alunoperiodoTotalHoras = $alunoperiodoTotalHoras;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getAlunoperiodoDataFechamento()
    {
        return $this->alunoperiodoDataFechamento;
    }

    /**
     * @param \DateTime $alunoperiodoDataFechamento
     * @return AtividadeperiodoAlunoNucleo
     */
    public function setAlunoperiodoDataFechamento($alunoperiodoDataFechamento)
    {
        $this->alunoperiodoDataFechamento = $alunoperiodoDataFechamento;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getAlunoperiodoDataInscricao()
    {
        return $this->alunoperiodoDataInscricao;
    }

    /**
     * @param \DateTime $alunoperiodoDataInscricao
     * @return AtividadeperiodoAlunoNucleo
     */
    public function setAlunoperiodoDataInscricao($alunoperiodoDataInscricao)
    {
        $this->alunoperiodoDataInscricao = $alunoperiodoDataInscricao;

        return $this;
    }

    /**
     * @return int
     */
    public function getAlunonucleoId()
    {
        return $this->alunonucleoId;
    }

    /**
     * @param int $alunonucleoId
     * @return AtividadeperiodoAlunoNucleo
     */
    public function setAlunonucleoId($alunonucleoId)
    {
        $this->alunonucleoId = $alunonucleoId;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlunoperiodoStatus()
    {
        return $this->alunoperiodoStatus;
    }

    /**
     * @param string $alunoperiodoStatus
     */
    public function setAlunoperiodoStatus($alunoperiodoStatus)
    {
        $this->alunoperiodoStatus = $alunoperiodoStatus;
    }

    public function toArray()
    {
        return [
            'alunoPer'                => $this->getAlunoper(),
            'dataFechamento'          => $this->getAlunoperiodoDataFechamento(),
            'dataInscricao'           => $this->getAlunoperiodoDataInscricao(),
            'alunoperiodoStatus'      => $this->getAlunoperiodoStatus(),
            'alunoperiodoTotalHoras'  => $this->getAlunoperiodoTotalHoras(),
            'alunoperiodoObservacoes' => $this->getAlunoperiodoObservacoes()
        ];
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuarioAlteracao()
    {
        return $this->usuarioAlteracao;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuarioAlteracao
     * @return AtividadeperiodoAlunoAtividades
     */
    public function setUsuarioAlteracao($usuarioAlteracao)
    {
        $this->usuarioAlteracao = $usuarioAlteracao;

        return $this;
    }

}

