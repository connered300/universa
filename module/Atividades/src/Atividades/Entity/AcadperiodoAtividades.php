<?php

namespace Atividades\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcadperiodoAtividades
 *
 * @ORM\Table(name="acadperiodo__atividades", indexes={@ORM\Index(name="fk_acadgeral__atividades_acadperiodo__docente_disciplina1_idx", columns={"docdisc_id"}), @ORM\Index(name="fk_acadperiodo__atividades_acadgeral__ativ_tipo1_idx", columns={"tativ_tipo"})})
 * @ORM\Entity
 */
class AcadperiodoAtividades
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ativ_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $ativId;

    /**
     * @var integer
     *
     * @ORM\Column(name="docdisc_id", type="integer", nullable=false)
     */
    private $docdiscId;

    /**
     * @var integer
     *
     * @ORM\Column(name="tativ_tipo", type="integer", nullable=false)
     */
    private $tativTipo;

    /**
     * @var string
     *
     * @ORM\Column(name="ativ_titulo", type="string", length=45, nullable=false)
     */
    private $ativTitulo;

    /**
     * @var string
     *
     * @ORM\Column(name="ativ_descricao", type="text", length=65535, nullable=true)
     */
    private $ativDescricao;

    /**
     * @var float
     *
     * @ORM\Column(name="ativ_nota", type="float", precision=10, scale=0, nullable=false)
     */
    private $ativNota;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ativ_data_inicio", type="datetime", nullable=false)
     */
    private $ativDataInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ativ_data_fim", type="datetime", nullable=false)
     */
    private $ativDataFim;

}

