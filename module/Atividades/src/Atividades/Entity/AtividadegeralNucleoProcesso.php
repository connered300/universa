<?php

namespace Atividades\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AtividadegeralNucleoProcesso
 *
 * @ORM\Table(name="atividadegeral__nucleo_processo", uniqueConstraints={@ORM\UniqueConstraint(name="nucleo_processo_id_UNIQUE", columns={"nucleo_processo_id"})}, indexes={@ORM\Index(name="fk_nucleo_id_acadgeral__atividade_tprocesso_nucleo_id1_idx", columns={"nucleo_id"}), @ORM\Index(name="fk_nucleo_processo_tip_idx", columns={"processo_tipo_id"})})
 * @ORM\Entity
 */
class AtividadegeralNucleoProcesso
{
    /**
     * @var integer
     *
     * @ORM\Column(name="nucleo_processo_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $nucleoProcessoId;

    /**
     * @var string
     *
     * @ORM\Column(name="nucleo_processo_vinculacao", type="string", nullable=false)
     */
    private $nucleoProcessoVinculacao;

    /**
     * @var \AtividadegeralProcessoTipo
     *
     * @ORM\ManyToOne(targetEntity="AtividadegeralProcessoTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="processo_tipo_id", referencedColumnName="processo_tipo_id")
     * })
     */
    private $processoTipo;

    /**
     * @var \AtividadegeralNucleo
     *
     * @ORM\ManyToOne(targetEntity="AtividadegeralNucleo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="nucleo_id", referencedColumnName="nucleo_id")
     * })
     */
    private $nucleo;

}

