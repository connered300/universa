<?php

namespace Atividades\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AtividadegeralAtividadesDisciplina
 *
 * @ORM\Table(name="atividadegeral__atividades_disciplina", indexes={@ORM\Index(name="fk_atividadegeral__atividades_disciplina_config_idx", columns={"atividadeconfatividade_id"}), @ORM\Index(name="fk_atividadegeral__atividades_disciplina__disciplina_idx", columns={"disc_id"})})
 * @ORM\Entity
 */
class AtividadegeralAtividadesDisciplina
{
    /**
     * @var integer
     *
     * @ORM\Column(name="atividadedisc_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $atividadediscId;

    /**
     * @var \AtividadegeralConfiguracoesAtividades
     *
     * @ORM\ManyToOne(targetEntity="AtividadegeralConfiguracoesAtividades")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="atividadeconfatividade_id", referencedColumnName="atividadeconfatividade_id")
     * })
     */
    private $atividadeconfatividade;

    /**
     * @var \AcadgeralDisciplina
     *
     * @ORM\ManyToOne(targetEntity="AcadgeralDisciplina")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="disc_id", referencedColumnName="disc_id")
     * })
     */
    private $disc;

}

