<?php

namespace Atividades\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AtividadegeralProcesso
 *
 * @ORM\Table(name="atividadegeral__processo", indexes={@ORM\Index(name="fk_atividadegeral__processo_atividadegeral__nucleo_processo1_idx", columns={"nucleo_processo_id"})})
 * @ORM\Entity
 */
class AtividadegeralProcesso
{
    /**
     * @var integer
     *
     * @ORM\Column(name="atividade_processo_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $atividadeProcessoId;

    /**
     * @var string
     *
     * @ORM\Column(name="atividade_processo_descricao", type="string", length=45, nullable=false)
     */
    private $atividadeProcessoDescricao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="atividade_processo_data_inicio", type="datetime", nullable=false)
     */
    private $atividadeProcessoDataInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="atividade_processo_data_fim", type="datetime", nullable=true)
     */
    private $atividadeProcessoDataFim;

    /**
     * @var \AtividadegeralNucleoProcesso
     *
     * @ORM\ManyToOne(targetEntity="AtividadegeralNucleoProcesso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="nucleo_processo_id", referencedColumnName="nucleo_processo_id")
     * })
     */
    private $nucleoProcesso;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="\Pessoa\Entity\Pessoa", mappedBy="atividadeprocesso")
     */
    private $pes;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pes = new \Doctrine\Common\Collections\ArrayCollection();
    }

}

