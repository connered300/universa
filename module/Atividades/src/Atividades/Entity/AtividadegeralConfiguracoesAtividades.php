<?php

namespace Atividades\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AtividadegeralConfiguracoesAtividades
 *
 * @ORM\Table(name="atividadegeral__configuracoes_atividades", indexes={@ORM\Index(name="fk_atividadegeral__configuracoes__atividades_est_idx", columns={"atividadeatividade_id"}), @ORM\Index(name="fk_atividadegeral__configuracoes__atividades_est_idx1", columns={"atividadeconf_portaria"})})
 * @ORM\Entity
 */
class AtividadegeralConfiguracoesAtividades
{
    /**
     * @var integer
     *
     * @ORM\Column(name="atividadeconfatividade_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $atividadeconfatividadeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="atividadeconf_minimo_disciplina", type="integer", nullable=true)
     */
    private $atividadeconfMinimoDisciplina;

    /**
     * @var integer
     *
     * @ORM\Column(name="atividadegeral__horas_praticas_max", type="integer", nullable=true)
     */
    private $atividadegeralHorasPraticasMax;

    /**
     * @var integer
     *
     * @ORM\Column(name="atividadeconf_horas_praticas_min", type="integer", nullable=true)
     */
    private $atividadeconfHorasPraticasMin;

    /**
     * @var integer
     *
     * @ORM\Column(name="atividadeconf_pontuacao_max", type="integer", nullable=true)
     */
    private $atividadeconfPontuacaoMax;

    /**
     * @var integer
     *
     * @ORM\Column(name="atividadeconf_pontuacao_min", type="integer", nullable=true)
     */
    private $atividadeconfPontuacaoMin;

    /**
     * @var AtividadegeralConfiguracoes
     *
     * @ORM\ManyToOne(targetEntity="AtividadegeralConfiguracoes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="atividadeconf_portaria", referencedColumnName="atividadeconf_portaria")
     * })
     */
    private $atividadeconfPortaria;

    /**
     * @var AtividadegeralAtividades
     *
     * @ORM\ManyToOne(targetEntity="AtividadegeralAtividades")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="atividadeatividade_id", referencedColumnName="atividadeatividade_id")
     * })
     */
    private $atividadeatividade;

    /**
     * @return int
     */
    public function getAtividadeconfatividadeId()
    {
        return $this->atividadeconfatividadeId;
    }

    /**
     * @param int $atividadeconfatividadeId
     * @return AtividadegeralConfiguracoesAtividades
     */
    public function setAtividadeconfatividadeId($atividadeconfatividadeId)
    {
        $this->atividadeconfatividadeId = $atividadeconfatividadeId;

        return $this;
    }

    /**
     * @return int
     */
    public function getAtividadeconfMinimoDisciplina()
    {
        return $this->atividadeconfMinimoDisciplina;
    }

    /**
     * @param int $atividadeconfMinimoDisciplina
     * @return AtividadegeralConfiguracoesAtividades
     */
    public function setAtividadeconfMinimoDisciplina($atividadeconfMinimoDisciplina)
    {
        $this->atividadeconfMinimoDisciplina = $atividadeconfMinimoDisciplina;

        return $this;
    }

    /**
     * @return int
     */
    public function getAtividadegeralHorasPraticasMax()
    {
        return $this->atividadegeralHorasPraticasMax;
    }

    /**
     * @param int $atividadegeralHorasPraticasMax
     * @return AtividadegeralConfiguracoesAtividades
     */
    public function setAtividadegeralHorasPraticasMax($atividadegeralHorasPraticasMax)
    {
        $this->atividadegeralHorasPraticasMax = $atividadegeralHorasPraticasMax;

        return $this;
    }

    /**
     * @return int
     */
    public function getAtividadeconfHorasPraticasMin()
    {
        return $this->atividadeconfHorasPraticasMin;
    }

    /**
     * @param int $atividadeconfHorasPraticasMin
     * @return AtividadegeralConfiguracoesAtividades
     */
    public function setAtividadeconfHorasPraticasMin($atividadeconfHorasPraticasMin)
    {
        $this->atividadeconfHorasPraticasMin = $atividadeconfHorasPraticasMin;

        return $this;
    }

    /**
     * @return int
     */
    public function getAtividadeconfPontuacaoMax()
    {
        return $this->atividadeconfPontuacaoMax;
    }

    /**
     * @param int $atividadeconfPontuacaoMax
     * @return AtividadegeralConfiguracoesAtividades
     */
    public function setAtividadeconfPontuacaoMax($atividadeconfPontuacaoMax)
    {
        $this->atividadeconfPontuacaoMax = $atividadeconfPontuacaoMax;

        return $this;
    }

    /**
     * @return int
     */
    public function getAtividadeconfPontuacaoMin()
    {
        return $this->atividadeconfPontuacaoMin;
    }

    /**
     * @param int $atividadeconfPontuacaoMin
     * @return AtividadegeralConfiguracoesAtividades
     */
    public function setAtividadeconfPontuacaoMin($atividadeconfPontuacaoMin)
    {
        $this->atividadeconfPontuacaoMin = $atividadeconfPontuacaoMin;

        return $this;
    }

    /**
     * @return AtividadegeralConfiguracoes
     */
    public function getAtividadeconfPortaria()
    {
        return $this->atividadeconfPortaria;
    }

    /**
     * @param AtividadegeralConfiguracoes $atividadeconfPortaria
     * @return AtividadegeralConfiguracoesAtividades
     */
    public function setAtividadeconfPortaria($atividadeconfPortaria)
    {
        $this->atividadeconfPortaria = $atividadeconfPortaria;

        return $this;
    }

    /**
     * @return AtividadegeralAtividades
     */
    public function getAtividadeatividade()
    {
        return $this->atividadeatividade;
    }

    /**
     * @param AtividadegeralAtividades $atividadeatividade
     * @return AtividadegeralConfiguracoesAtividades
     */
    public function setAtividadeatividade($atividadeatividade)
    {
        $this->atividadeatividade = $atividadeatividade;

        return $this;
    }
}

