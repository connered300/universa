<?php

namespace Atividades\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AtividadegeralAtividades
 *
 * @ORM\Table(name="atividadegeral__atividades")
 * @ORM\Entity
 */
class AtividadegeralAtividades
{
    /**
     * @var integer
     *
     * @ORM\Column(name="atividadeatividade_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $atividadeatividadeId;

    /**
     * @var string
     *
     * @ORM\Column(name="atividadeatividade_descricao", type="string", length=45, nullable=false)
     */
    private $atividadeatividadeDescricao;

    /**
     * @var string
     *
     * @ORM\Column(name="atividadeatividade_observacoes", type="text", length=65535, nullable=false)
     */
    private $atividadeatividadeObservacoes;

    public function getAtividadeId()
    {
        return $this->atividadeatividadeId;
    }

    public function getAtividadeDescricao()
    {
        return $this->atividadeatividadeDescricao;
    }
}

