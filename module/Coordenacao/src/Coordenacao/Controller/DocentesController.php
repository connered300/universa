<?php

namespace Coordenacao\Controller;

use VersaSpine\Controller\AbstractCoreController;

class DocentesController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
    }

    public function indexAction()
    {
        return $this->getView();
    }

    public function painelAction()
    {
        $docente = $this->params()->fromRoute('id');

        if ($docente) {
            $serviceDocente     = new \Coordenacao\Service\Docentes($this->getEntityManager());
            $serviceCursoConfig = new \Matricula\Service\AcadCursoConfig($this->getEntityManager());
            $serviceTurma       = new \Matricula\Service\AcadperiodoTurma($this->getEntityManager());

            $arrDocencias = $serviceDocente->getTurmasDocenciasDatatable($docente);
            $objDocente   = $serviceDocente->getRepository()->find($docente);

            $arrDocencia = [];

            if ($arrDocencias) {
                foreach ($arrDocencias as $value) {

                    /** @var  $objturma \Matricula\Entity\AcadperiodoTurma */
                    $objturma = $serviceTurma->getRepository()->find($value['turma_id']);

                    /** @var $objcursoConfig  \Matricula\Entity\AcadCursoConfig */
                    $objcursoConfig = $serviceCursoConfig->retornaObjCursoConfig(
                        $objturma->getMatCur()->getCurso()->getCursoId()
                    );

                    if (!$objcursoConfig) {
                        $error = $serviceCursoConfig->getLastError();
                        $error = $error ? $error : "Não foi possivel localizar a configuração para o curso informado!";
                        $this->flashMessenger()->addErrorMessage($error);

                        return $this->redirect()->toRoute(
                            $this->getRoute(),
                            array('controller' => $this->getController())
                        );
                    }

                    $value['metodoCalculoNotas'] = $objcursoConfig->getCursoconfigMetodo();
                    $arrDocencia[]               = $value;
                }
            }

            $this->getView()->setVariable('docencias', json_encode($arrDocencias));
            $this->getView()->setVariable('docente', $objDocente);
            $this->getView()->setVariable(
                'cursoConfigMetodo',
                $serviceCursoConfig::METODO_DESEMPENHO_CALCULO_POR_MEDIA
            );
        }

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceDocente = new \Coordenacao\Service\Docentes($this->getEntityManager());

            $data   = $request->getPost()->toArray();
            $result = $serviceDocente->search($data);

            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function docenciaInformacaoAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceDocente = new \Coordenacao\Service\Docentes($this->getEntityManager());

            $data        = $request->getPost()->toArray();
            $informacoes = $serviceDocente->getDocenciaInformacoes($data['docdiscId']);

            $this->getJson()->setVariables($informacoes);
        }

        return $this->getJson();
    }

    public function docenciaExtensaoAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceDocente = new \Coordenacao\Service\Docentes($this->getEntityManager());

            $data        = $request->getPost()->toArray();
            $informacoes = $serviceDocente->getInformacaoExtensaoDocencia($data['docdiscId']);

            $this->getJson()->setVariables($informacoes);
        }

        return $this->getJson();
    }

    public function docenciaExtensaoGravarAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceDocente = new \Coordenacao\Service\Docentes($this->getEntityManager());

            $data = $request->getPost()->toArray();

            $data = array(
                'docdiscId'                     => $data['docdisc'],
                'etapadiarioExtensaoId'         => $data['extensaoId'],
                'diario'                        => $data['diarioId'],
                'etapadiarioExtensaoData'       => $data['extensaoData'],
                'etapadiarioExtensaoObservacao' => $data['extensaoObservacao'],
            );

            $result = $serviceDocente->docenciaExtensaoGravar($data);

            $this->getJson()->setVariable('result', $result);
        }

        return $this->getJson();
    }

    public function docenciaAnotacoesAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAnotacao = new \Professor\Service\AcadperiodoAnotacao($this->getEntityManager());

            $data   = $request->getPost()->toArray();
            $result = $serviceAnotacao->buscaAnotacoesJson($data);

            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function docenciaAnotacaoAnexosAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAnotacaoArquivo = new \Professor\Service\AcadperiodoAnotacaoArquivo($this->getEntityManager());

            $data   = $request->getPost()->toArray();
            $anexos = $serviceAnotacaoArquivo->listaAnexosAnotacao($data['anotId']);

            $this->getJson()->setVariables(array('anexos' => $anexos));
        }

        return $this->getJson();
    }

    public function docenciaNotasAlunosEtapasAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceDocenteDisciplina = new \Matricula\Service\AcadperiodoDocenteDisciplina($this->getEntityManager());

            $situacoes = \Matricula\Service\AcadgeralSituacao::situacoesAtividade();
            $data      = $request->getPost()->toArray();
            $result    = $serviceDocenteDisciplina->notasAlunosDocenciaEtapaArray($data['docdiscId'], $situacoes);

            $this->getJson()->setVariable("data", $result);
        }

        return $this->getJson();
    }
}
