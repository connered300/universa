<?php

namespace Coordenacao\Service;

use VersaSpine\Service\AbstractService;

class Docentes extends AbstractService
{
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Professor\Entity\AcadgeralDocente');
    }

    protected function valida($dados)
    {
        $arrValidate = array();

        if (!$dados['diario']) {
            $arrValidate[] = "Não existe um registro de Diário para o Docente nessa Disciplina. Entre em contato com o suporte para averiguar tal situação";
        }

        if (!$dados['etapadiarioExtensaoData']) {
            $arrValidate[] = "Informe a data de extensão da entrega";
        }

        return $arrValidate;
    }

    protected function pesquisaForJson($criterios)
    {
    }

    public function search(array $data)
    {
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());
        $situacoes = \Matricula\Service\AcadgeralSituacao::situacoesAtividade();
        $situacoes = implode(', ', $situacoes);

        $campusCurso = "";
        $arrCampusCursoPermitido = implode(",", $serviceAcessoPessoas->retornaCampusCursoDoUsuarioLogado(true));

        if ($arrCampusCursoPermitido) {
            $campusCurso = " AND cc.cursocampus_id in (" . $arrCampusCursoPermitido . ")";
        }

        $query = <<<SQL
            SELECT
                DISTINCT docente.docente_id docente_id,
                pessoa.pes_nome docente_descricao,
                SUM(docente_resumo.qtd_disc) AS qtd_disc,
                SUM(docente_resumo.qtd_alunos) AS qtd_alunos,
                ROUND(SUM(docente_resumo.media_notas)/SUM(docente_resumo.qtd_disc)) AS media_notas,
                SUM(docente_resumo.qtd_anotacoes) AS qtd_anotacoes,
                SUM(docente_resumo.qtd_faltas)>0 AS qtd_faltas,
                IF(SUM(docente_resumo.qtd_faltas)>0, 'Sim', 'Não') AS faltas_lancadas
            FROM acadgeral__docente docente
            INNER JOIN pessoa ON docente.pes_id = pessoa.pes_id
            INNER JOIN (
            	SELECT
                    d_disc.docente_id,
                    d_disc.docdisc_id,
                    d_disc.disc_id,
                    COUNT(d_disc.disc_id) qtd_disc,
                    (
                        SELECT COUNT(acad_ad.alunodisc_id) FROM acadperiodo__aluno_disciplina AS acad_ad
                        WHERE acad_ad.disc_id = d_disc.disc_id AND
                              acad_ad.turma_id=d_disc.turma_id AND
                              acad_ad.situacao_id IN ($situacoes)
                    ) qtd_alunos,
                    media_notas.media_notas as media_notas,
                    (
                        SELECT COUNT(acad_an.anot_id) FROM acadperiodo__anotacao acad_an
                        WHERE acad_an.docdisc_id = d_disc.docdisc_id
                    ) qtd_anotacoes,
                    (
                        SELECT COUNT(acad_fr.frequencia_id) FROM acadperiodo__frequencia acad_fr
                        WHERE acad_fr.docdisc_id = d_disc.docdisc_id
                    ) qtd_faltas
                FROM acadperiodo__docente_disciplina d_disc
                INNER JOIN acadperiodo__turma acad_t ON acad_t.turma_id = d_disc.turma_id
                INNER JOIN campus_curso cc ON cc.cursocampus_id = acad_t.cursocampus_id {$campusCurso}
                INNER JOIN acadperiodo__letivo acad_l
                    ON acad_l.per_id = acad_t.per_id AND
                    CURDATE() between acad_l.per_data_inicio AND acad_l.per_data_fim
                LEFT JOIN (
                    SELECT
                        tnotas.disc_id, tnotas.turma_id,
                        SUM(COALESCE(tnotas.nota_aluno, 0))/COUNT(DISTINCT tnotas.alunodisc_id) AS media_notas
                    from
                    (
                        SELECT
                            SUM(COALESCE(acad_ea.alunoetapa_nota, 0))/COUNT(DISTINCT acad_ea.alunodisc_id) AS nota_aluno,
                            acad_ad.alunodisc_id, acad_ad.disc_id, acad_ad.turma_id
                        FROM acadperiodo__aluno_disciplina AS acad_ad
                        LEFT JOIN acadperiodo__etapa_aluno AS acad_ea ON acad_ea.alunodisc_id=acad_ad.alunodisc_id
                        WHERE acad_ad.situacao_id IN($situacoes) AND
                              acad_ea.alunoetapa_nota IS NOT NULL
                        GROUP BY acad_ad.alunodisc_id
                    ) AS tnotas
                    GROUP BY tnotas.disc_id, tnotas.turma_id
                ) AS media_notas ON media_notas.disc_id = d_disc.disc_id AND media_notas.turma_id=d_disc.turma_id
                WHERE
                    now() >= d_disc.docdisc_data_inicio AND
                    (now() <= d_disc.docdisc_data_fim || d_disc.docdisc_data_fim IS NULL)
                GROUP BY d_disc.docdisc_id
            ) AS docente_resumo ON docente_resumo.docente_id = docente.docente_id
            WHERE docente_ativo = 'Sim'
            GROUP BY docente.docente_id
SQL;

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getTurmasDocencias($docenteId)
    {
        $turmas             = array();
        $periodoLetivoAtual = (new \Matricula\Service\AcadperiodoLetivo($this->getEm()))->buscaPeriodoAtual();
        $dateInitLetivo     = (new \DateTime($periodoLetivoAtual->getPerDataInicio()))->format('Y-m-d');
        $dateFimLetivo      = (new \DateTime($periodoLetivoAtual->getPerDataFim()))->format('Y-m-d');

        $sql = '
            SELECT
                ddisc
            FROM
                \Matricula\Entity\AcadperiodoDocenteDisciplina ddisc
            WHERE
                ddisc.docente = :docente
                AND (ddisc.docdiscDataInicio >= :perDataInit AND ddisc.docdiscDataInicio <= :perDataFim)
                AND (ddisc.docdiscDataFim >= :currentDate OR ddisc.docdiscDataFim IS NULL)';

        $disciplinasDocente = $this
            ->getEm()
            ->createQuery($sql)
            ->setParameter('docente', $docenteId)
            ->setParameter('currentDate', (new \DateTime('now'))->format('Y-m-d'))
            ->setParameter('perDataInit', $dateInitLetivo)
            ->setParameter('perDataFim', $dateFimLetivo)
            ->getResult();

        // agrupa por turmas
        foreach ($disciplinasDocente as $disciplinaDocente) {
            $turma         = $disciplinaDocente->getTurma();
            $disciplina    = $disciplinaDocente->getDisc();
            $periodoLetivo = $turma->getPer();
            $discId        = $disciplina->getDiscId();

            if (!isset($turmas[$turma->getTurmaId()])) {
                $turmas[$turma->getTurmaId()] = [
                    'turmaId'     => $turma->getTurmaId(),
                    'turmaNome'   => $turma->getTurmaNome(),
                    'disciplinas' => [],
                ];
            }

            $turmas[$turma->getTurmaId()]['disciplinas'][$discId] = [
                'docdiscId'         => $disciplinaDocente->getDocdiscId(),
                'disc'              => $discId,
                'discNome'          => $disciplina->getDiscNome(),
                'periodoLetivoId'   => $periodoLetivo->getPerId(),
                'periodoLetivoNome' => $periodoLetivo->getPerNome(),
            ];
        }

        return $turmas;
    }

    public function getTurmasDocenciasDatatable($docenteId)
    {
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceAcadperiodoLetivo = new \Matricula\Service\AcadperiodoLetivo($this->getEm());

        $periodoLetivoAtual = $serviceAcadperiodoLetivo->buscaPeriodoAtual();

        $dateInitLetivo = $periodoLetivoAtual->getPerDataInicio()->format('Y-m-d');
        $dateFimLetivo  = $periodoLetivoAtual->getPerDataFim()->format('Y-m-d');
        $param = array(
            'docente'     => $docenteId,
            'currentDate' => (new \DateTime('now'))->format('Y-m-d'),
            'perDataInit' => $dateInitLetivo,
            'perDataFim'  => $dateFimLetivo,
        );
        $campusCurso = "";

        /** @var \Acesso\Entity\AcessoPessoas $objPessoa */
        $objAcessoPessoas = $serviceAcessoPessoas->retornaUsuarioLogado();
        $pesId          = $objAcessoPessoas ? $objAcessoPessoas->getPes()->getPes()->getPesId() : -1;
        $arrCampusCursoPermitido =  implode(',',$serviceAcessoPessoas->retornaCampusCursoDoUsuarioLogado(true));

        if ($arrCampusCursoPermitido) {
            $campusCurso = " AND apt.cursocampus_id in (" . $arrCampusCursoPermitido . ")";
        }

        $sql = "
        SELECT
            apt.turma_id, apt.turma_nome,
            apdd.docdisc_id,
            agd.disc_id, agd.disc_nome,
            apl.per_id, apl.per_nome
        FROM acadperiodo__docente_disciplina apdd
        INNER JOIN acadperiodo__turma apt ON apt.turma_id=apdd.turma_id
        INNER JOIN campus_curso cc ON cc.cursocampus_id = apt.cursocampus_id  " . $campusCurso . "
        INNER JOIN acad_curso c ON c.curso_id = cc.curso_id
        INNER JOIN acadgeral__disciplina agd ON agd.disc_id=apdd.disc_id
        INNER JOIN acadperiodo__letivo apl ON apl.per_id=apt.per_id
        WHERE
            apdd.docente_id = :docente AND
            (apdd.docdisc_data_inicio BETWEEN :perDataInit AND :perDataFim) AND
            (apdd.docdisc_data_fim >= :currentDate OR apdd.docdisc_data_fim IS NULL) ";

        $stmt         = $this->executeQueryWithParam($sql, $param);
        $arrDocencias = $stmt->fetchAll();

        return $arrDocencias;
    }

    public function getDocenciaInformacoes($docenciaId)
    {
        $situacoes = \Matricula\Service\AcadgeralSituacao::situacoesAtividade();
        $situacoes = implode(', ', $situacoes);

        $sql = '
        SELECT
            d_disc.docente_id,
            d_disc.docdisc_id,
            d_disc.disc_id,
            COUNT(d_disc.disc_id) qtd_disc,
            (
                SELECT COUNT(acad_ad.alunodisc_id) FROM acadperiodo__aluno_disciplina AS acad_ad
                WHERE acad_ad.disc_id = d_disc.disc_id AND
                      acad_ad.turma_id=d_disc.turma_id AND
                      acad_ad.situacao_id IN (' . $situacoes . ')
            ) qtd_alunos,
            (
                SELECT COUNT(acad_ea.alunodisc_id) FROM acadperiodo__aluno_disciplina AS acad_ad
                LEFT JOIN acadperiodo__etapa_aluno AS acad_ea ON acad_ea.alunodisc_id=acad_ad.alunodisc_id
                WHERE acad_ad.disc_id = d_disc.disc_id AND
                      acad_ad.turma_id=d_disc.turma_id AND
                      acad_ad.situacao_id IN(' . $situacoes . ') AND
                      acad_ea.alunoetapa_nota IS NOT NULL
            ) qtd_notas,
            ROUND(media_notas.media_notas) AS media_notas,
            (
                SELECT COUNT(acad_an.anot_id) FROM acadperiodo__anotacao acad_an
                WHERE acad_an.docdisc_id = d_disc.docdisc_id
            ) qtd_anotacoes,
            (
                SELECT COUNT(acad_fr.frequencia_id) FROM acadperiodo__frequencia acad_fr
                WHERE acad_fr.docdisc_id = d_disc.docdisc_id
            ) qtd_faltas
        FROM acadperiodo__docente_disciplina d_disc
        INNER JOIN acadperiodo__turma acad_t ON acad_t.turma_id = d_disc.turma_id
        INNER JOIN acadperiodo__letivo acad_l
            ON acad_l.per_id = acad_t.per_id AND
            CURDATE() BETWEEN acad_l.per_data_inicio AND acad_l.per_data_fim
        LEFT JOIN (
            SELECT
                tnotas.disc_id, tnotas.turma_id,
                SUM(COALESCE(tnotas.nota_aluno, 0))/COUNT(DISTINCT tnotas.alunodisc_id) AS media_notas
            FROM
            (
                SELECT
                    SUM(COALESCE(acad_ea.alunoetapa_nota, 0))/COUNT(DISTINCT acad_ea.alunodisc_id) AS nota_aluno,
                    acad_ad.alunodisc_id, acad_ad.disc_id, acad_ad.turma_id
                FROM acadperiodo__aluno_disciplina AS acad_ad
                LEFT JOIN acadperiodo__etapa_aluno AS acad_ea ON acad_ea.alunodisc_id=acad_ad.alunodisc_id
                WHERE acad_ad.situacao_id IN(' . $situacoes . ') AND
                      acad_ea.alunoetapa_nota IS NOT NULL
                GROUP BY acad_ad.alunodisc_id
            ) AS tnotas
            GROUP BY tnotas.disc_id, tnotas.turma_id
        ) AS media_notas ON media_notas.disc_id = d_disc.disc_id AND media_notas.turma_id=d_disc.turma_id
        WHERE
            d_disc.docdisc_id = :docdisc_id AND
            now() >= d_disc.docdisc_data_inicio AND
            (now() <= d_disc.docdisc_data_fim || d_disc.docdisc_data_fim IS NULL)
        GROUP BY d_disc.docdisc_id';

        $informacoes = $this->executeQueryWithParam($sql, array('docdisc_id' => $docenciaId));
        $informacoes = $informacoes->fetch();

        return $informacoes;
    }

    public function getInformacaoExtensaoDocencia($docenciaId)
    {
        $arrEtapas = array();

        $objDocenteDisciplina = $this
            ->getEm()
            ->getRepository('\Matricula\Entity\AcadperiodoDocenteDisciplina')
            ->find($docenciaId);

        $turma         = $objDocenteDisciplina->getTurma();
        $periodoLetivo = $turma->getPer();

        $etapas = $this
            ->getEm()
            ->getRepository('Matricula\Entity\AcadperiodoEtapas')
            ->findBy(array('per' => $periodoLetivo->getPerId()));

        foreach ($etapas as $etapa) {
            $etapaId             = $etapa->getEtapaId();
            $diarioEtapa         = "";
            $etapaDiarioExtensao = null;

            $etapaDiario = $this
                ->getEm()
                ->getRepository('Professor\Entity\AcadperiodoEtapaDiario')
                ->findOneBy(array('etapa' => $etapaId, 'docdisc' => $objDocenteDisciplina->getDocdiscId()));

            if ($etapaDiario) {
                $etapaDiarioExtensao = $this
                    ->getEm()
                    ->getRepository('Professor\Entity\AcadperiodoEtapaDiarioExtensao')
                    ->findOneBy(['diario' => $etapaDiario->getDiarioId()], ['etapadiarioExtensaoId' => 'DESC']);
                $diarioEtapa         = $etapaDiario->getDiarioId();
            }

            if ($etapaDiarioExtensao) {
                $arrayExtensao = array(
                    'extensaoId'            => $etapaDiarioExtensao->getEtapadiarioExtensaoId(),
                    'extensaoJustificativa' => $etapaDiarioExtensao->getEtapadiarioExtensaoJustificativa(),
                    'extensaoObservacao'    => $etapaDiarioExtensao->getEtapadiarioExtensaoObservacao(),
                    'extensaoDataFinal'     => $etapaDiarioExtensao->getEtapadiarioExtensaoData()->format('d/m/Y'),
                );
            } else {
                $arrayExtensao = array(
                    'extensaoId'            => '',
                    'extensaoJustificativa' => '',
                    'extensaoObservacao'    => '',
                    'extensaoDataFinal'     => '',
                );
            }

            $arrEtapas[$etapaId] = [
                'diario'                      => $diarioEtapa,
                'docdisc'                     => $objDocenteDisciplina->getDocdiscId(),
                'etapaId'                     => $etapaId,
                'etapaDescricao'              => $etapa->getEtapaDescricao(),
                'etapaDiarioDataFinalDefault' => $etapa->getEtapaDataFim()->format('d/m/Y'),
                'etapaExtensao'               => $arrayExtensao,
            ];
        }

        return $arrEtapas;
    }

    public function docenciaExtensaoGravar(array $data)
    {
        $docenciaId = $data['docdiscId'];
        $validate   = $this->valida($data);

        if (!empty($validate)) {
            $result = $validate;
        } else {
            $servEtapaDiarioExtensao = new \Professor\Service\AcadperiodoEtapaDiarioExtensao($this->getEm());
            $result                  = $servEtapaDiarioExtensao->save($data);

            if (is_object($result)) {
                return array(
                    'type'    => 'success',
                    'message' => 'Alteração realizada com Sucesso',
                    'etapas'  => $this->getInformacaoExtensaoDocencia($docenciaId)
                );
            }
        }

        return array('type' => 'warning', 'message' => $result, 'data' => '');
    }
}
