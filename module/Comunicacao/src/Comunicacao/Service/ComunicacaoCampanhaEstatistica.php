<?php

namespace Comunicacao\Service;

use VersaSpine\Service\AbstractService;

class ComunicacaoCampanhaEstatistica extends AbstractService
{
    const ESTATISTICA_ALVO_AGENTE       = 'Agente';
    const ESTATISTICA_ALVO_ALUNOS       = 'Alunos';
    const ESTATISTICA_ALVO_OPERADORES   = 'Operadores';
    const ESTATISTICA_ACAO_VISUALIZACAO = 'Visualização';
    const ESTATISTICA_ACAO_CLIQUE       = 'Clique';
    const ESTATISTICA_ACAO_CONVERSAO    = 'Conversão';
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2EstatisticaAlvo($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getEstatisticaAlvo());
    }

    public static function getEstatisticaAlvo()
    {
        return array(self::ESTATISTICA_ALVO_AGENTE, self::ESTATISTICA_ALVO_ALUNOS, self::ESTATISTICA_ALVO_OPERADORES);
    }

    public function getArrSelect2EstatisticaAcao($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getEstatisticaAcao());
    }

    public static function getEstatisticaAcao()
    {
        return array(
            self::ESTATISTICA_ACAO_VISUALIZACAO,
            self::ESTATISTICA_ACAO_CLIQUE,
            self::ESTATISTICA_ACAO_CONVERSAO
        );
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Comunicacao\Entity\ComunicacaoCampanhaEstatistica');
    }

    public function pesquisaForJson($params)
    {
        $sql = '
        SELECT *
        FROM comunicacao__campanha_estatistica
        WHERE
            campanha_id LIKE :campanha_id';

        $campanhaId    = false;
        $estatisticaId = false;

        if ($params['q']) {
            $campanhaId = $params['q'];
        } elseif ($params['query']) {
            $campanhaId = $params['query'];
        }

        if ($params['estatisticaId']) {
            $estatisticaId = $params['estatisticaId'];
        }

        $parameters = array('campanha_id' => "{$campanhaId}%");

        if ($estatisticaId) {
            $parameters['estatistica_id'] = $estatisticaId;
            $sql .= ' AND estatistica_id <> :estatistica_id';
        }

        $sql .= " ORDER BY campanha_id";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceComunicacaoCampanha = new \Comunicacao\Service\ComunicacaoCampanha($this->getEm());
        $serviceAcessoPessoas       = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceAcadgeralAluno      = new \Matricula\Service\AcadgeralAluno($this->getEm());
        $serviceAcadCurso           = new \Matricula\Service\AcadCurso($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['estatisticaId']) {
                /** @var $objComunicacaoCampanhaEstatistica \Comunicacao\Entity\ComunicacaoCampanhaEstatistica */
                $objComunicacaoCampanhaEstatistica = $this->getRepository()->find($arrDados['estatisticaId']);

                if (!$objComunicacaoCampanhaEstatistica) {
                    $this->setLastError('Registro de campanha estatistica não existe!');

                    return false;
                }
            } else {
                $objComunicacaoCampanhaEstatistica = new \Comunicacao\Entity\ComunicacaoCampanhaEstatistica();
            }

            if ($arrDados['campanha']) {
                /** @var $objComunicacaoCampanha \Comunicacao\Entity\ComunicacaoCampanha */
                $objComunicacaoCampanha = $serviceComunicacaoCampanha->getRepository()->find($arrDados['campanha']);

                if (!$objComunicacaoCampanha) {
                    $this->setLastError('Registro de campanha não existe!');

                    return false;
                }

                $objComunicacaoCampanhaEstatistica->setCampanha($objComunicacaoCampanha);
            } else {
                $objComunicacaoCampanhaEstatistica->setCampanha(null);
            }

            if ($arrDados['usuario']) {
                /** @var $objAcessoPessoas \Acesso\Entity\AcessoPessoas */
                $objAcessoPessoas = $serviceAcessoPessoas->getRepository()->find($arrDados['usuario']);

                if (!$objAcessoPessoas) {
                    $this->setLastError('Registro de pessoas não existe!');

                    return false;
                }

                $objComunicacaoCampanhaEstatistica->setUsuario($objAcessoPessoas);
            } else {
                $objComunicacaoCampanhaEstatistica->setUsuario(null);
            }

            if ($arrDados['aluno']) {
                /** @var $objAcadgeralAluno \Matricula\Entity\AcadgeralAluno */
                $objAcadgeralAluno = $serviceAcadgeralAluno->getRepository()->find($arrDados['aluno']);

                if (!$objAcadgeralAluno) {
                    $this->setLastError('Registro de aluno não existe!');

                    return false;
                }

                $objComunicacaoCampanhaEstatistica->setAluno($objAcadgeralAluno);
            } else {
                $objComunicacaoCampanhaEstatistica->setAluno(null);
            }

            if ($arrDados['curso']) {
                /** @var $objAcadCurso \Matricula\Entity\AcadCurso */
                $objAcadCurso = $serviceAcadCurso->getRepository()->find($arrDados['curso']);

                if (!$objAcadCurso) {
                    $this->setLastError('Registro de curso não existe!');

                    return false;
                }

                $objComunicacaoCampanhaEstatistica->setCurso($objAcadCurso);
            } else {
                $objComunicacaoCampanhaEstatistica->setCurso(null);
            }

            $objComunicacaoCampanhaEstatistica->setEstatisticaData($arrDados['estatisticaData']);
            $objComunicacaoCampanhaEstatistica->setEstatisticaAlvo($arrDados['estatisticaAlvo']);
            $objComunicacaoCampanhaEstatistica->setEstatisticaAcao($arrDados['estatisticaAcao']);

            $this->getEm()->persist($objComunicacaoCampanhaEstatistica);
            $this->getEm()->flush($objComunicacaoCampanhaEstatistica);

            $this->getEm()->commit();

            $arrDados['estatisticaId'] = $objComunicacaoCampanhaEstatistica->getEstatisticaId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de campanha estatistica!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['campanha']) {
            $errors[] = 'Por favor preencha o campo "código campanha"!';
        }

        if (!$arrParam['estatisticaData']) {
            $errors[] = 'Por favor preencha o campo "data"!';
        }

        if (!in_array($arrParam['estatisticaAlvo'], self::getEstatisticaAlvo())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "alvo"!';
        }

        if (!in_array($arrParam['estatisticaAcao'], self::getEstatisticaAcao())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "ação"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM comunicacao__campanha_estatistica";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($estatisticaId)
    {
        $arrDados = array();

        if (!$estatisticaId) {
            $this->setLastError('Campanha estatistica inválido!');

            return array();
        }

        /** @var $objComunicacaoCampanhaEstatistica \Comunicacao\Entity\ComunicacaoCampanhaEstatistica */
        $objComunicacaoCampanhaEstatistica = $this->getRepository()->find($estatisticaId);

        if (!$objComunicacaoCampanhaEstatistica) {
            $this->setLastError('Campanha estatistica não existe!');

            return array();
        }

        $serviceComunicacaoCampanha = new \Comunicacao\Service\ComunicacaoCampanha($this->getEm());
        $serviceAcessoPessoas       = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceAcadgeralAluno      = new \Matricula\Service\AcadgeralAluno($this->getEm());
        $serviceAcadCurso           = new \Matricula\Service\AcadCurso($this->getEm());

        try {
            $arrDados = $objComunicacaoCampanhaEstatistica->toArray();

            if ($arrDados['campanha']) {
                $arrComunicacaoCampanha = $serviceComunicacaoCampanha->getArrSelect2(['id' => $arrDados['campanha']]);
                $arrDados['campanha']   = $arrComunicacaoCampanha ? $arrComunicacaoCampanha[0] : null;
            }

            if ($arrDados['usuario']) {
                $arrAcessoPessoas    = $serviceAcessoPessoas->getArrSelect2(['id' => $arrDados['usuario']]);
                $arrDados['usuario'] = $arrAcessoPessoas ? $arrAcessoPessoas[0] : null;
            }

            if ($arrDados['aluno']) {
                $arrAcadgeralAluno = $serviceAcadgeralAluno->getArrSelect2(['id' => $arrDados['aluno']]);
                $arrDados['aluno'] = $arrAcadgeralAluno ? $arrAcadgeralAluno[0] : null;
            }

            if ($arrDados['curso']) {
                $arrAcadCurso      = $serviceAcadCurso->getArrSelect2(['id' => $arrDados['curso']]);
                $arrDados['curso'] = $arrAcadCurso ? $arrAcadCurso[0] : null;
            }
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceComunicacaoCampanha = new \Comunicacao\Service\ComunicacaoCampanha($this->getEm());
        $serviceAcessoPessoas       = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceAcadgeralAluno      = new \Matricula\Service\AcadgeralAluno($this->getEm());
        $serviceAcadCurso           = new \Matricula\Service\AcadCurso($this->getEm());

        if (is_array($arrDados['campanha']) && !$arrDados['campanha']['text']) {
            $arrDados['campanha'] = $arrDados['campanha']['campanhaId'];
        }

        if ($arrDados['campanha'] && is_string($arrDados['campanha'])) {
            $arrDados['campanha'] = $serviceComunicacaoCampanha->getArrSelect2(array('id' => $arrDados['campanha']));
            $arrDados['campanha'] = $arrDados['campanha'] ? $arrDados['campanha'][0] : null;
        }

        if (is_array($arrDados['usuario']) && !$arrDados['usuario']['text']) {
            $arrDados['usuario'] = $arrDados['usuario']['id'];
        }

        if ($arrDados['usuario'] && is_string($arrDados['usuario'])) {
            $arrDados['usuario'] = $serviceAcessoPessoas->getArrSelect2(array('id' => $arrDados['usuario']));
            $arrDados['usuario'] = $arrDados['usuario'] ? $arrDados['usuario'][0] : null;
        }

        if (is_array($arrDados['aluno']) && !$arrDados['aluno']['text']) {
            $arrDados['aluno'] = $arrDados['aluno']['alunoId'];
        }

        if ($arrDados['aluno'] && is_string($arrDados['aluno'])) {
            $arrDados['aluno'] = $serviceAcadgeralAluno->getArrSelect2(array('id' => $arrDados['aluno']));
            $arrDados['aluno'] = $arrDados['aluno'] ? $arrDados['aluno'][0] : null;
        }

        if (is_array($arrDados['curso']) && !$arrDados['curso']['text']) {
            $arrDados['curso'] = $arrDados['curso']['cursoId'];
        }

        if ($arrDados['curso'] && is_string($arrDados['curso'])) {
            $arrDados['curso'] = $serviceAcadCurso->getArrSelect2(array('id' => $arrDados['curso']));
            $arrDados['curso'] = $arrDados['curso'] ? $arrDados['curso'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['estatisticaId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['campanhaId' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Comunicacao\Entity\ComunicacaoCampanhaEstatistica */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getEstatisticaId();
            $arrEntity[$params['value']] = $objEntity->getCampanhaId();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($estatisticaId)
    {
        if (!$estatisticaId) {
            $this->setLastError('Para remover um registro de campanha estatistica é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objComunicacaoCampanhaEstatistica \Comunicacao\Entity\ComunicacaoCampanhaEstatistica */
            $objComunicacaoCampanhaEstatistica = $this->getRepository()->find($estatisticaId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objComunicacaoCampanhaEstatistica);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de campanha estatistica.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceComunicacaoCampanha = new \Comunicacao\Service\ComunicacaoCampanha($this->getEm());
        $serviceAcessoPessoas       = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceAcadgeralAluno      = new \Matricula\Service\AcadgeralAluno($this->getEm());
        $serviceAcadCurso           = new \Matricula\Service\AcadCurso($this->getEm());

        $serviceComunicacaoCampanha->setarDependenciasView($view);
        $serviceAcessoPessoas->setarDependenciasView($view);
        $serviceAcadgeralAluno->setarDependenciasView($view);
        $serviceAcadCurso->setarDependenciasView($view);

        $view->setVariable("arrEstatisticaAlvo", $this->getArrSelect2EstatisticaAlvo());

        $view->setVariable("arrEstatisticaAcao", $this->getArrSelect2EstatisticaAcao());
    }

    public function atualizarEstatistica($arrParam, $acao = self::ESTATISTICA_ACAO_CONVERSAO)
    {
        $serviceAcessoPessoa = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceCampusCurso  = new \Matricula\Service\CampusCurso($this->getEm());

        if ($_COOKIE['campanhaEstatistica']) {
            $arrEstatistica             = (array)json_decode($_COOKIE['campanhaEstatistica']);
            $arrDados['campanhaClique'] = $arrEstatistica['campanhaId'];
            $arrDados['visualizacoes']  = (array)$arrEstatistica['visualizacoes'];

            unset($_COOKIE['campanhaEstatistica']);
        }

        if ($objAcessoPessoas = $serviceAcessoPessoa->retornaUsuarioLogado()) {
            $arrDados['usuario'] = $objAcessoPessoas->getUsuario()->getId();
        }

        $arrDados['estatisticaData'] = new \DateTime();
        $arrDados['aluno']           = $arrParam['alunoId'];
        $arrDados['estatisticaAlvo'] = $arrParam['estatisticaAlvo'] ? $arrParam['estatisticaAlvo']
            : self::ESTATISTICA_ALVO_ALUNOS;

        foreach ($arrDados['visualizacoes'] as $campanhaId) {
            $arrDados['campanha']        = $campanhaId;
            $arrDados['estatisticaAcao'] = self::ESTATISTICA_ACAO_VISUALIZACAO;

            $this->save($arrDados);
        }

        $arrDados['campanha']        = $arrDados['campanhaClique'];
        $arrDados['estatisticaAcao'] = $arrParam['estatisticaAcao'] ? $arrParam['estatisticaAcao'] : $acao;

        $arrCampusCurso = $this->recuperaNovosCampusCurso($arrParam['alunoCurso']);

        foreach ($arrCampusCurso as $campusCursoId) {
            if ($arrCurso = $serviceCampusCurso->getArray($campusCursoId)) {
                $arrDados['curso'] = $arrCurso['cursoId']['id'];

                if ($this->save($arrDados)) {
                    $this->atualizarCookies($arrDados);
                }
            }
        }

        return true;
    }

    public function atualizarCookies($arrDados = array())
    {
        if (!$arrDados) {
            unset($_COOKIE);
        }

        if ($arrDados['pesCpf']) {
            setcookie('pesCpf', $arrDados['pesCpf']);
        }

        if ($arrDados['alunoId']) {
            setcookie('alunoId', $arrDados['alunoId']);
        }
    }

    public function recuperaNovosCampusCurso($arrDados)
    {
        $arrCampusCurso = array();

        foreach ($arrDados as $arrAlunoCurso) {
            if (!is_null($arrAlunoCurso['alunoCursoNovo'])) {
                $arrCampusCurso[] = $arrAlunoCurso['cursocampusId'];
            }
        }

        return $arrCampusCurso;
    }
}