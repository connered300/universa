<?php

namespace Comunicacao\Service;

use VersaSpine\Service\AbstractService;

class ComunicacaoCampanha extends AbstractService
{
    const campanhaAlvoAgente      = 'Agente';
    const CAMPANHA_ALVO_AGENTE      = 'Agente';
    const CAMPANHA_ALVO_ALUNOS      = 'Alunos';
    const CAMPANHA_ALVO_OPERADORES  = 'Operadores';
    const CAMPANHA_SITUACAO_ATIVA   = 'Ativa';
    const CAMPANHA_SITUACAO_INATIVA = 'Inativa';
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2CampanhaAlvo($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getCampanhaAlvo());
    }

    public static function getCampanhaAlvo()
    {
        return array(self::CAMPANHA_ALVO_AGENTE, self::CAMPANHA_ALVO_ALUNOS, self::CAMPANHA_ALVO_OPERADORES);
    }

    public function getArrSelect2CampanhaSituacao($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getCampanhaSituacao());
    }

    public static function getCampanhaSituacao()
    {
        return array(self::CAMPANHA_SITUACAO_ATIVA, self::CAMPANHA_SITUACAO_INATIVA);
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Comunicacao\Entity\ComunicacaoCampanha');
    }

    public function pesquisaForJson($params)
    {
        $sql = '
        SELECT *
        FROM comunicacao__campanha
        WHERE
            campanha_descricao LIKE :campanha_descricao';

        $campanhaDescricao = false;
        $campanhaId        = false;

        if ($params['q']) {
            $campanhaDescricao = $params['q'];
        } elseif ($params['query']) {
            $campanhaDescricao = $params['query'];
        }

        if ($params['campanhaId']) {
            $campanhaId = $params['campanhaId'];
        }

        $parameters = array('campanha_descricao' => "{$campanhaDescricao}%");

        if ($campanhaId) {
            $parameters['campanha_id'] = $campanhaId;
            $sql .= ' AND campanha_id <> :campanha_id';
        }

        $sql .= " ORDER BY campanha_descricao";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceAcessoPessoas     = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceArquivo           = new \GerenciadorArquivos\Service\Arquivo($this->getEm());
        $serviceArquivoDiretorios = new \GerenciadorArquivos\Service\ArquivoDiretorios($this->getEm());

        $objDiretorio = $serviceArquivoDiretorios->getRepository()->find(\Organizacao\Service\OrgIes::DIRETORIO_IMAGEM);
        $serviceArquivo->setDiretorio($objDiretorio);

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['campanhaId']) {
                /** @var $objComunicacaoCampanha \Comunicacao\Entity\ComunicacaoCampanha */
                $objComunicacaoCampanha = $this->getRepository()->find($arrDados['campanhaId']);

                if (!$objComunicacaoCampanha) {
                    $this->setLastError('Registro de campanha não existe!');

                    return false;
                }
            } else {
                $objComunicacaoCampanha = new \Comunicacao\Entity\ComunicacaoCampanha();
                $objComunicacaoCampanha
                    ->setcampanhaDataCadastro(new \DateTime())
                    ->setcampanhaDataAlteracao(new \DateTime())
                    ->setCampanhaUsuarioCadastro($serviceAcessoPessoas->retornaUsuarioLogado());
            }

            $imagemCampanha = $arrDados['vfile'][$arrDados['campanhaArquivo']];

            $objCampanhaImagem = null;
            $objArqAntiga      = null;

            if ($imagemCampanha) {
                if (empty($imagemCampanha['raw']) && $objComunicacaoCampanha->getCampanhaArquivo()) {
                    /* @var $objCampanhaImagem \GerenciadorArquivos\Entity\Arquivo */
                    $objArqAntiga = $objComunicacaoCampanha->getCampanhaArquivo();
                } else {
                    $arrDados['arq'] = ($imagemCampanha['raw']) ? $imagemCampanha['raw'] : null;

                    if ($arrDados['arq'] && is_string($arrDados['arq'])) {
                        $objCampanhaImagem = $serviceArquivo->getRepository()->find($arrDados['arq']);
                    }
                }

                if ($imagemCampanha['file']['error'] == 0 && $imagemCampanha['file']['size'] != 0) {
                    try {
                        $objCampanhaImagem = $serviceArquivo->adicionar($imagemCampanha['file']);
                    } catch (\Exception $ex) {
                        throw new \Exception('Falha ao registrar imagem de aluno!');
                    }
                }
            } else {
                $objCampanhaImagem = null;
            }

            $arrDados['campanhaArquivo'] = $objCampanhaImagem;

            $objComunicacaoCampanha
                ->setCampanhaArquivo($objCampanhaImagem)
                ->setCampanhaNome($arrDados['campanhaNome'])
                ->setCampanhaDescricao($arrDados['campanhaDescricao'])
                ->setCampanhaLink($arrDados['campanhaLink'])
                ->setCampanhaAlvo($arrDados['campanhaAlvo'])
                ->setCampanhaUsuarioAlteracao($serviceAcessoPessoas->retornaUsuarioLogado())
                ->setCampanhaDataAlteracao(new \DateTime())
                ->setCampanhaSituacao($arrDados['campanhaSituacao']);

            $this->getEm()->persist($objComunicacaoCampanha);
            $this->getEm()->flush($objComunicacaoCampanha);

            if (!empty($objArqAntiga)) {
                try {
                    $serviceArquivo->excluir($objArqAntiga->getArqId());
                } catch (\Exception $ex) {
                    throw new \Exception('Falha ao remover imagem antiga de aluno!');
                }
            }

            $this->getEm()->commit();

            $arrDados['campanhaId'] = $objComunicacaoCampanha->getCampanhaId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de campanha!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['campanhaNome']) {
            $errors[] = 'Por favor preencha o campo "nome"!';
        }

        if (array_diff($arrParam['campanhaAlvo'], self::getCampanhaAlvo())) {
            $errors[] = 'Por favor selecione valores válidos para o campo "alvo"!';
        }

        if (!in_array($arrParam['campanhaSituacao'], self::getCampanhaSituacao())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "situação"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $conditions = [];
        $query      = "
              SELECT
                campanha_id,
                -- cadastro.pes_nome as campanha_usuario_cadastro,
                -- alteracao.pes_nome as campanha_usuario_alteracao,
                campanha_nome,
                campanha_descricao,
                campanha_link,
                campanha_alvo,
                DATE_FORMAT(campanha_data_alteracao, '%d/%m/%Y') campanha_data_alteracao,
                DATE_FORMAT(campanha_data_cadastro, '%d/%m/%Y') campanha_data_cadastro,
                campanha_situacao,
                arquivo.arq_chave
                FROM comunicacao__campanha
                -- LEFT JOIN acesso_pessoas acessoCadastro on acessoCadastro.id = campanha_usuario_cadastro
                -- LEFT JOIN acesso_pessoas acessoAlteracao on acessoAlteracao.id = campanha_usuario_alteracao
                -- LEFT JOIN pessoa cadastro on acessoCadastro.pes_fisica = cadastro.pes_id
                -- LEFT JOIN pessoa alteracao on acessoCadastro.pes_fisica = alteracao.pes_id
                LEFT JOIN arquivo on campanha_arquivo = arq_id
        ";

        if ($data['carousel']) {
            $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());
            $serviceOrg           = new \Organizacao\Service\OrgAgenteEducacional($this->getEm());

            $agenteLogado  = $serviceOrg->getAgenteLogado();
            $usuarioLogado = $serviceAcessoPessoas->retornaUsuarioLogado();
            $conditions[]  = 'campanha_situacao="' . self::CAMPANHA_SITUACAO_ATIVA . '"';

            if ($usuarioLogado) {
                $conditions[] = "FIND_IN_SET('" . self::CAMPANHA_ALVO_OPERADORES . "', campanha_alvo) ";
            } elseif ($agenteLogado['id'] != '-1') {
                $conditions[] = "FIND_IN_SET('" . self::CAMPANHA_ALVO_AGENTE . "', campanha_alvo) ";
            } else {
                $conditions[] = "FIND_IN_SET('" . self::CAMPANHA_ALVO_ALUNOS . "', campanha_alvo) ";
            }
        }

        if ($conditions) {
            $query .= ' WHERE ' . implode(' AND ', $conditions);
        }

        $query .= "ORDER BY campanha_data_cadastro DESC";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($campanhaId)
    {
        $arrDados = array();

        if (!$campanhaId) {
            $this->setLastError('Campanha inválido!');

            return array();
        }

        /** @var $objComunicacaoCampanha \Comunicacao\Entity\ComunicacaoCampanha */
        $objComunicacaoCampanha = $this->getRepository()->find($campanhaId);

        if (!$objComunicacaoCampanha) {
            $this->setLastError('Campanha não existe!');

            return array();
        }

        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());

        try {
            $arrDados = $objComunicacaoCampanha->toArray();

            if ($arrDados['campanhaUsuarioCadastro']) {
                $arrAcessoPessoas                    = $serviceAcessoPessoas->getArrSelect2(
                    ['id' => $arrDados['campanhaUsuarioCadastro']]
                );
                $arrDados['campanhaUsuarioCadastro'] = $arrAcessoPessoas ? $arrAcessoPessoas[0] : null;
            }

            if ($arrDados['campanhaUsuarioAlteracao']) {
                $arrAcessoPessoas                     = $serviceAcessoPessoas->getArrSelect2(
                    ['id' => $arrDados['campanhaUsuarioAlteracao']]
                );
                $arrDados['campanhaUsuarioAlteracao'] = $arrAcessoPessoas ? $arrAcessoPessoas[0] : null;
            }

            if ($arrDados['campanhaArquivo']) {
                $serviceArquivo = new \GerenciadorArquivos\Service\Arquivo($this->getEm());
                /** @var \GerenciadorArquivos\Entity\Arquivo $objArquivo */
                $objArquivo = $serviceArquivo->getRepository()->find($arrDados['campanhaArquivo']);

                if ($objArquivo) {
                    $arrDados['campanhaArquivo'] = array(
                        'id'      => $objArquivo->getArqId(),
                        'nome'    => $objArquivo->getArqNome(),
                        'tipo'    => $objArquivo->getArqTipo(),
                        'tamanho' => $objArquivo->getArqTamanho(),
                        'chave'   => $objArquivo->getArqChave()
                    );
                }
            }
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $servicePessoa = new \Pessoa\Service\Pessoa($this->getEm());

        if (is_array($arrDados['campanhaUsuarioCadastro'])) {
            $arrDados['campanhaUsuarioCadastro'] =
                $servicePessoa->retornaPessoaLogada($arrDados['campanhaUsuarioCadastro']['id']);
        }

        if (is_array($arrDados['campanhaUsuarioAlteracao'])) {
            $arrDados['campanhaUsuarioAlteracao'] =
                $servicePessoa->retornaPessoaLogada($arrDados['campanhaUsuarioAlteracao']['id']);
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['campanhaId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['campanhaDescricao' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Comunicacao\Entity\ComunicacaoCampanha */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getCampanhaId();
            $arrEntity[$params['value']] = $objEntity->getCampanhaDescricao();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($campanhaId)
    {
        if (!$campanhaId) {
            $this->setLastError('Para remover um registro de campanha é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objComunicacaoCampanha \Comunicacao\Entity\ComunicacaoCampanha */
            $objComunicacaoCampanha = $this->getRepository()->find($campanhaId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objComunicacaoCampanha);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de campanha.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());

        $serviceAcessoPessoas->setarDependenciasView($view);
        $serviceAcessoPessoas->setarDependenciasView($view);

        $view->setVariable("arrCampanhaAlvo", $this->getArrSelect2CampanhaAlvo());

        $view->setVariable("arrCampanhaSituacao", $this->getArrSelect2CampanhaSituacao());
    }
}

?>