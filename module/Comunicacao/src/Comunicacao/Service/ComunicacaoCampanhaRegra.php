<?php

namespace Comunicacao\Service;

use VersaSpine\Service\AbstractService;

class ComunicacaoCampanhaRegra extends AbstractService
{
    const REGRA_CHAVE_NIVEL      = 'Nível';
    const REGRA_CHAVE_MODALIDADE = 'Modalidade';
    const REGRA_CHAVE_CURSO      = 'Curso';
    const REGRA_CHAVE_CIDADE     = 'Cidade';
    const REGRA_CHAVE_ESTADO     = 'Estado';
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2RegraChave($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getRegraChave());
    }

    public static function getRegraChave()
    {
        return array(
            self::REGRA_CHAVE_NIVEL,
            self::REGRA_CHAVE_MODALIDADE,
            self::REGRA_CHAVE_CURSO,
            self::REGRA_CHAVE_CIDADE,
            self::REGRA_CHAVE_ESTADO
        );
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Comunicacao\Entity\ComunicacaoCampanhaRegra');
    }

    public function pesquisaForJson($params)
    {
        $sql = '
        SELECT *
        FROM comunicacao__campanha_regra
        WHERE
            campanha_id LIKE :campanha_id';

        $campanhaId = false;
        $regraId    = false;

        if ($params['q']) {
            $campanhaId = $params['q'];
        } elseif ($params['query']) {
            $campanhaId = $params['query'];
        }

        if ($params['regraId']) {
            $regraId = $params['regraId'];
        }

        $parameters = array('campanha_id' => "{$campanhaId}%");

        if ($regraId) {
            $parameters['regra_id'] = $regraId;
            $sql .= ' AND regra_id <> :regra_id';
        }

        $sql .= " ORDER BY campanha_id";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceComunicacaoCampanha = new \Comunicacao\Service\ComunicacaoCampanha($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['regraId']) {
                /** @var $objComunicacaoCampanhaRegra \Comunicacao\Entity\ComunicacaoCampanhaRegra */
                $objComunicacaoCampanhaRegra = $this->getRepository()->find($arrDados['regraId']);

                if (!$objComunicacaoCampanhaRegra) {
                    $this->setLastError('Registro de campanha regra não existe!');

                    return false;
                }
            } else {
                $objComunicacaoCampanhaRegra = new \Comunicacao\Entity\ComunicacaoCampanhaRegra();
            }

            if ($arrDados['campanha']) {
                /** @var $objComunicacaoCampanha \Comunicacao\Entity\ComunicacaoCampanha */
                $objComunicacaoCampanha = $serviceComunicacaoCampanha->getRepository()->find($arrDados['campanha']);

                if (!$objComunicacaoCampanha) {
                    $this->setLastError('Registro de campanha não existe!');

                    return false;
                }

                $objComunicacaoCampanhaRegra->setCampanha($objComunicacaoCampanha);
            } else {
                $objComunicacaoCampanhaRegra->setCampanha(null);
            }

            $objComunicacaoCampanhaRegra->setRegraChave($arrDados['regraChave']);
            $objComunicacaoCampanhaRegra->setRegraValor($arrDados['regraValor']);

            $this->getEm()->persist($objComunicacaoCampanhaRegra);
            $this->getEm()->flush($objComunicacaoCampanhaRegra);

            $this->getEm()->commit();

            $arrDados['regraId'] = $objComunicacaoCampanhaRegra->getRegraId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de campanha regra!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['campanha']) {
            $errors[] = 'Por favor preencha o campo "código campanha"!';
        }

        if (!in_array($arrParam['regraChave'], self::getRegraChave())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "chave"!';
        }

        if (!$arrParam['regraValor']) {
            $errors[] = 'Por favor preencha o campo "valor"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM comunicacao__campanha_regra";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($regraId)
    {
        $arrDados = array();

        if (!$regraId) {
            $this->setLastError('Campanha regra inválido!');

            return array();
        }

        /** @var $objComunicacaoCampanhaRegra \Comunicacao\Entity\ComunicacaoCampanhaRegra */
        $objComunicacaoCampanhaRegra = $this->getRepository()->find($regraId);

        if (!$objComunicacaoCampanhaRegra) {
            $this->setLastError('Campanha regra não existe!');

            return array();
        }

        $serviceComunicacaoCampanha = new \Comunicacao\Service\ComunicacaoCampanha($this->getEm());

        try {
            $arrDados = $objComunicacaoCampanhaRegra->toArray();

            if ($arrDados['campanha']) {
                $arrComunicacaoCampanha = $serviceComunicacaoCampanha->getArrSelect2(['id' => $arrDados['campanha']]);
                $arrDados['campanha']   = $arrComunicacaoCampanha ? $arrComunicacaoCampanha[0] : null;
            }
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceComunicacaoCampanha = new \Comunicacao\Service\ComunicacaoCampanha($this->getEm());

        if (is_array($arrDados['campanha']) && !$arrDados['campanha']['text']) {
            $arrDados['campanha'] = $arrDados['campanha']['campanhaId'];
        }

        if ($arrDados['campanha'] && is_string($arrDados['campanha'])) {
            $arrDados['campanha'] = $serviceComunicacaoCampanha->getArrSelect2(array('id' => $arrDados['campanha']));
            $arrDados['campanha'] = $arrDados['campanha'] ? $arrDados['campanha'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['regraId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['campanhaId' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Comunicacao\Entity\ComunicacaoCampanhaRegra */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getRegraId();
            $arrEntity[$params['value']] = $objEntity->getCampanhaId();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($regraId)
    {
        if (!$regraId) {
            $this->setLastError('Para remover um registro de campanha regra é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objComunicacaoCampanhaRegra \Comunicacao\Entity\ComunicacaoCampanhaRegra */
            $objComunicacaoCampanhaRegra = $this->getRepository()->find($regraId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objComunicacaoCampanhaRegra);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de campanha regra.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceComunicacaoCampanha = new \Comunicacao\Service\ComunicacaoCampanha($this->getEm());

        $serviceComunicacaoCampanha->setarDependenciasView($view);

        $view->setVariable("arrRegraChave", $this->getArrSelect2RegraChave());
    }
}
?>