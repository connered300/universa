<?php

namespace Comunicacao\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * ComunicacaoCampanhaRegra
 *
 * @ORM\Table(name="comunicacao__campanha_regra")
 * @ORM\Entity
 * @LG\LG(id="regraId",label="CampanhaId")
 * @Jarvis\Jarvis(title="Listagem de campanha regra",icon="fa fa-table")
 */
class ComunicacaoCampanhaRegra
{
    /**
     * @var integer
     *
     * @ORM\Column(name="regra_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="regra_id")
     * @LG\Labels\Attributes(text="código regra")
     * @LG\Querys\Conditions(type="=")
     */
    private $regraId;

    /**
     * @var \Comunicacao\Entity\ComunicacaoCampanha
     * @ORM\ManyToOne(targetEntity="Comunicacao\Entity\ComunicacaoCampanha")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="campanha_id", referencedColumnName="campanha_id")
     * })
     */
    private $campanha;

    /**
     * @var string
     *
     * @ORM\Column(name="regra_chave", type="string", nullable=true, length=5)
     * @LG\Labels\Property(name="regra_chave")
     * @LG\Labels\Attributes(text="chave")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $regraChave;

    /**
     * @var string
     *
     * @ORM\Column(name="regra_valor", type="text", nullable=false)
     * @LG\Labels\Property(name="regra_valor")
     * @LG\Labels\Attributes(text="valor")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $regraValor;

    /**
     * @return integer
     */
    public function getRegraId()
    {
        return $this->regraId;
    }

    /**
     * @param integer $regraId
     * @return ComunicacaoCampanhaRegra
     */
    public function setRegraId($regraId)
    {
        $this->regraId = $regraId;

        return $this;
    }

    /**
     * @return \Comunicacao\Entity\ComunicacaoCampanha
     */
    public function getCampanha()
    {
        return $this->campanha;
    }

    /**
     * @param \Comunicacao\Entity\ComunicacaoCampanha $campanha
     * @return ComunicacaoCampanhaRegra
     */
    public function setCampanha($campanha)
    {
        $this->campanha = $campanha;

        return $this;
    }

    /**
     * @return string
     */
    public function getRegraChave()
    {
        return $this->regraChave;
    }

    /**
     * @param string $regraChave
     * @return ComunicacaoCampanhaRegra
     */
    public function setRegraChave($regraChave)
    {
        $this->regraChave = $regraChave;

        return $this;
    }

    /**
     * @return string
     */
    public function getRegraValor()
    {
        return $this->regraValor;
    }

    /**
     * @param string $regraValor
     * @return ComunicacaoCampanhaRegra
     */
    public function setRegraValor($regraValor)
    {
        $this->regraValor = $regraValor;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'regraId'    => $this->getRegraId(),
            'campanha'   => $this->getCampanha(),
            'regraChave' => $this->getRegraChave(),
            'regraValor' => $this->getRegraValor(),
        );

        $array['campanha'] = $this->getCampanha() ? $this->getCampanha()->getCampanhaId() : null;

        return $array;
    }
}
