<?php

namespace Comunicacao\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * ComunicacaoCampanhaEstatistica
 *
 * @ORM\Table(name="comunicacao__campanha_estatistica")
 * @ORM\Entity
 * @LG\LG(id="estatisticaId",label="CampanhaId")
 * @Jarvis\Jarvis(title="Listagem de campanha estatistica",icon="fa fa-table")
 */
class ComunicacaoCampanhaEstatistica
{
    /**
     * @var integer
     *
     * @ORM\Column(name="estatistica_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="estatistica_id")
     * @LG\Labels\Attributes(text="código estatistica")
     * @LG\Querys\Conditions(type="=")
     */
    private $estatisticaId;

    /**
     * @var \Comunicacao\Entity\ComunicacaoCampanha
     * @ORM\ManyToOne(targetEntity="Comunicacao\Entity\ComunicacaoCampanha")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="campanha_id", referencedColumnName="campanha_id")
     * })
     */
    private $campanha;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * })
     */
    private $usuario;

    /**
     * @var \Matricula\Entity\AcadgeralAluno
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadgeralAluno")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="aluno_id", referencedColumnName="aluno_id")
     * })
     */
    private $aluno;

    /**
     * @var \Matricula\Entity\AcadCurso
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="curso_id", referencedColumnName="curso_id")
     * })
     */
    private $curso;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="estatistica_data", type="datetime", nullable=false)
     * @LG\Labels\Property(name="estatistica_data")
     * @LG\Labels\Attributes(text="data")
     * @LG\Querys\Conditions(type="=")
     */
    private $estatisticaData;

    /**
     * @var string
     *
     * @ORM\Column(name="estatistica_alvo", type="string", nullable=true, length=10)
     * @LG\Labels\Property(name="estatistica_alvo")
     * @LG\Labels\Attributes(text="alvo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $estatisticaAlvo;

    /**
     * @var string
     *
     * @ORM\Column(name="estatistica_acao", type="string", nullable=true, length=12)
     * @LG\Labels\Property(name="estatistica_acao")
     * @LG\Labels\Attributes(text="ação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $estatisticaAcao;

    /**
     * @return integer
     */
    public function getEstatisticaId()
    {
        return $this->estatisticaId;
    }

    /**
     * @param integer $estatisticaId
     * @return ComunicacaoCampanhaEstatistica
     */
    public function setEstatisticaId($estatisticaId)
    {
        $this->estatisticaId = $estatisticaId;

        return $this;
    }

    /**
     * @return \Comunicacao\Entity\ComunicacaoCampanha
     */
    public function getCampanha()
    {
        return $this->campanha;
    }

    /**
     * @param \Comunicacao\Entity\ComunicacaoCampanha $campanha
     * @return ComunicacaoCampanhaEstatistica
     */
    public function setCampanha($campanha)
    {
        $this->campanha = $campanha;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuario
     * @return ComunicacaoCampanhaEstatistica
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * @return \Matricula\Entity\AcadgeralAluno
     */
    public function getAluno()
    {
        return $this->aluno;
    }

    /**
     * @param \Matricula\Entity\AcadgeralAluno $aluno
     * @return ComunicacaoCampanhaEstatistica
     */
    public function setAluno($aluno)
    {
        $this->aluno = $aluno;

        return $this;
    }

    /**
     * @return \Matricula\Entity\AcadCurso
     */
    public function getCurso()
    {
        return $this->curso;
    }

    /**
     * @param \Matricula\Entity\AcadCurso $curso
     * @return ComunicacaoCampanhaEstatistica
     */
    public function setCurso($curso)
    {
        $this->curso = $curso;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getEstatisticaData($format = false)
    {
        $estatisticaData = $this->estatisticaData;

        if ($format && $estatisticaData) {
            $estatisticaData = $estatisticaData->format('d/m/Y H:i:s');
        }

        return $estatisticaData;
    }

    /**
     * @param \Datetime $estatisticaData
     * @return ComunicacaoCampanhaEstatistica
     */
    public function setEstatisticaData($estatisticaData)
    {
        if ($estatisticaData) {
            if (is_string($estatisticaData)) {
                $estatisticaData = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $estatisticaData
                );
                $estatisticaData = new \Datetime($estatisticaData);
            }
        } else {
            $estatisticaData = null;
        }
        $this->estatisticaData = $estatisticaData;

        return $this;
    }

    /**
     * @return string
     */
    public function getEstatisticaAlvo()
    {
        return $this->estatisticaAlvo;
    }

    /**
     * @param string $estatisticaAlvo
     * @return ComunicacaoCampanhaEstatistica
     */
    public function setEstatisticaAlvo($estatisticaAlvo)
    {
        $this->estatisticaAlvo = $estatisticaAlvo;

        return $this;
    }

    /**
     * @return string
     */
    public function getEstatisticaAcao()
    {
        return $this->estatisticaAcao;
    }

    /**
     * @param string $estatisticaAcao
     * @return ComunicacaoCampanhaEstatistica
     */
    public function setEstatisticaAcao($estatisticaAcao)
    {
        $this->estatisticaAcao = $estatisticaAcao;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'estatisticaId'   => $this->getEstatisticaId(),
            'campanha'        => $this->getCampanha(),
            'usuario'         => $this->getUsuario(),
            'aluno'           => $this->getAluno(),
            'curso'           => $this->getCurso(),
            'estatisticaData' => $this->getEstatisticaData(true),
            'estatisticaAlvo' => $this->getEstatisticaAlvo(),
            'estatisticaAcao' => $this->getEstatisticaAcao(),
        );

        $array['campanha'] = $this->getCampanha() ? $this->getCampanha()->getCampanhaId() : null;
        $array['usuario']  = $this->getUsuario() ? $this->getUsuario()->getId() : null;
        $array['aluno']    = $this->getAluno() ? $this->getAluno()->getAlunoId() : null;
        $array['curso']    = $this->getCurso() ? $this->getCurso()->getCursoId() : null;

        return $array;
    }
}
