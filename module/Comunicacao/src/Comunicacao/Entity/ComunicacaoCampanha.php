<?php

namespace Comunicacao\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * ComunicacaoCampanha
 *
 * @ORM\Table(name="comunicacao__campanha")
 * @ORM\Entity
 * @LG\LG(id="campanhaId",label="CampanhaDescricao")
 * @Jarvis\Jarvis(title="Listagem de campanha",icon="fa fa-table")
 */
class ComunicacaoCampanha
{
    /**
     * @var integer
     *
     * @ORM\Column(name="campanha_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="campanha_id")
     * @LG\Labels\Attributes(text="código campanha")
     * @LG\Querys\Conditions(type="=")
     */
    private $campanhaId;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="campanha_usuario_cadastro", referencedColumnName="id")
     * })
     */
    private $campanhaUsuarioCadastro;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="campanha_usuario_alteracao", referencedColumnName="id")
     * })
     */
    private $campanhaUsuarioAlteracao;

    /**
     * @var string
     *
     * @ORM\Column(name="campanha_nome", type="string", nullable=false, length=255)
     * @LG\Labels\Property(name="campanha_nome")
     * @LG\Labels\Attributes(text="nome")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $campanhaNome;

    /**
     * @var string
     *
     * @ORM\Column(name="campanha_descricao", type="text", nullable=true)
     * @LG\Labels\Property(name="campanha_descricao")
     * @LG\Labels\Attributes(text="descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $campanhaDescricao;

    /**
     * @var string
     *
     * @ORM\Column(name="campanha_link", type="text", nullable=true)
     * @LG\Labels\Property(name="campanha_link")
     * @LG\Labels\Attributes(text="link")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $campanhaLink;

    /**
     * @var string
     *
     * @ORM\Column(name="campanha_alvo", type="string", nullable=true, length=10)
     * @LG\Labels\Property(name="campanha_alvo")
     * @LG\Labels\Attributes(text="alvo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $campanhaAlvo;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="campanha_data_alteracao", type="datetime", nullable=false)
     * @LG\Labels\Property(name="campanha_data_alteracao")
     * @LG\Labels\Attributes(text="alteração data")
     * @LG\Querys\Conditions(type="=")
     */
    private $campanhaDataAlteracao;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="campanha_data_cadastro", type="datetime", nullable=false)
     * @LG\Labels\Property(name="campanha_data_cadastro")
     * @LG\Labels\Attributes(text="cadastro data")
     * @LG\Querys\Conditions(type="=")
     */
    private $campanhaDataCadastro;

    /**
     * @var string
     *
     * @ORM\Column(name="campanha_situacao", type="string", nullable=true, length=7)
     * @LG\Labels\Property(name="campanha_situacao")
     * @LG\Labels\Attributes(text="situação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $campanhaSituacao;

    /**
     * @var \GerenciadorArquivos\Entity\Arquivo
     *
     * @ORM\ManyToOne(targetEntity="GerenciadorArquivos\Entity\Arquivo", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="campanha_arquivo", referencedColumnName="arq_id")
     * })
     */
    private $campanhaArquivo;

    /**
     * @return integer
     */
    public function getCampanhaId()
    {
        return $this->campanhaId;
    }

    /**
     * @param integer $campanhaId
     * @return ComunicacaoCampanha
     */
    public function setCampanhaId($campanhaId)
    {
        $this->campanhaId = $campanhaId;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getCampanhaUsuarioCadastro()
    {
        return $this->campanhaUsuarioCadastro;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $campanhaUsuarioCadastro
     * @return ComunicacaoCampanha
     */
    public function setCampanhaUsuarioCadastro($campanhaUsuarioCadastro)
    {
        $this->campanhaUsuarioCadastro = $campanhaUsuarioCadastro;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getCampanhaUsuarioAlteracao()
    {
        return $this->campanhaUsuarioAlteracao;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $campanhaUsuarioAlteracao
     * @return ComunicacaoCampanha
     */
    public function setCampanhaUsuarioAlteracao($campanhaUsuarioAlteracao)
    {
        $this->campanhaUsuarioAlteracao = $campanhaUsuarioAlteracao;

        return $this;
    }

    /**
     * @return string
     */
    public function getCampanhaNome()
    {
        return $this->campanhaNome;
    }

    /**
     * @param string $campanhaNome
     * @return ComunicacaoCampanha
     */
    public function setCampanhaNome($campanhaNome)
    {
        $this->campanhaNome = $campanhaNome;

        return $this;
    }

    /**
     * @return string
     */
    public function getCampanhaDescricao()
    {
        return $this->campanhaDescricao;
    }

    /**
     * @param string $campanhaDescricao
     * @return ComunicacaoCampanha
     */
    public function setCampanhaDescricao($campanhaDescricao)
    {
        $this->campanhaDescricao = $campanhaDescricao;

        return $this;
    }

    /**
     * @return string
     */
    public function getCampanhaLink()
    {
        return $this->campanhaLink;
    }

    /**
     * @param string $campanhaLink
     * @return ComunicacaoCampanha
     */
    public function setCampanhaLink($campanhaLink)
    {
        $this->campanhaLink = $campanhaLink;

        return $this;
    }

    /**
     * @return string
     */
    public function getCampanhaAlvo()
    {
        return $this->campanhaAlvo;
    }

    /**
     * @param string $campanhaAlvo
     * @return ComunicacaoCampanha
     */
    public function setCampanhaAlvo($campanhaAlvo)
    {
        $this->campanhaAlvo = $campanhaAlvo;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getCampanhaDataAlteracao($format = false)
    {
        $campanhaDataAlteracao = $this->campanhaDataAlteracao;

        if ($format && $campanhaDataAlteracao) {
            $campanhaDataAlteracao = $campanhaDataAlteracao->format('d/m/Y H:i:s');
        }

        return $campanhaDataAlteracao;
    }

    /**
     * @param \Datetime $campanhaDataAlteracao
     * @return ComunicacaoCampanha
     */
    public function setCampanhaDataAlteracao($campanhaDataAlteracao)
    {
        if ($campanhaDataAlteracao) {
            if (is_string($campanhaDataAlteracao)) {
                $campanhaDataAlteracao = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $campanhaDataAlteracao
                );
                $campanhaDataAlteracao = new \Datetime($campanhaDataAlteracao);
            }
        } else {
            $campanhaDataAlteracao = null;
        }
        $this->campanhaDataAlteracao = $campanhaDataAlteracao;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getCampanhaDataCadastro($format = false)
    {
        $campanhaDataCadastro = $this->campanhaDataCadastro;

        if ($format && $campanhaDataCadastro) {
            $campanhaDataCadastro = $campanhaDataCadastro->format('d/m/Y H:i:s');
        }

        return $campanhaDataCadastro;
    }

    /**
     * @param \Datetime $campanhaDataCadastro
     * @return ComunicacaoCampanha
     */
    public function setCampanhaDataCadastro($campanhaDataCadastro)
    {
        if ($campanhaDataCadastro) {
            if (is_string($campanhaDataCadastro)) {
                $campanhaDataCadastro = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $campanhaDataCadastro
                );
                $campanhaDataCadastro = new \Datetime($campanhaDataCadastro);
            }
        } else {
            $campanhaDataCadastro = null;
        }
        $this->campanhaDataCadastro = $campanhaDataCadastro;

        return $this;
    }

    /**
     * @return string
     */
    public function getCampanhaSituacao()
    {
        return $this->campanhaSituacao;
    }

    /**
     * @param string $campanhaSituacao
     * @return ComunicacaoCampanha
     */
    public function setCampanhaSituacao($campanhaSituacao)
    {
        $this->campanhaSituacao = $campanhaSituacao;

        return $this;
    }

    /**
     * @return \GerenciadorArquivos\Entity\Arquivo
     */
    public function getCampanhaArquivo()
    {
        return $this->campanhaArquivo;
    }

    /**
     * @param \GerenciadorArquivos\Entity\Arquivo $campanhaArquivo | null
     * @return ComunicacaoCampanha
     */
    public function setCampanhaArquivo($campanhaArquivo)
    {
        $this->campanhaArquivo = $campanhaArquivo;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'campanhaId'               => $this->getCampanhaId(),
            'campanhaUsuarioCadastro'  => $this->getCampanhaUsuarioCadastro(),
            'campanhaUsuarioAlteracao' => $this->getCampanhaUsuarioAlteracao(),
            'campanhaNome'             => $this->getCampanhaNome(),
            'campanhaDescricao'        => $this->getCampanhaDescricao(),
            'campanhaLink'             => $this->getCampanhaLink(),
            'campanhaAlvo'             => $this->getCampanhaAlvo(),
            'campanhaDataAlteracao'    => $this->getCampanhaDataAlteracao(true),
            'campanhaDataCadastro'     => $this->getCampanhaDataCadastro(true),
            'campanhaSituacao'         => $this->getCampanhaSituacao(),
            'campanhaArquivo'         => $this->getCampanhaArquivo(),
        );

        $array['campanhaUsuarioCadastro']  = $this->getCampanhaUsuarioCadastro() ? $this->getCampanhaUsuarioCadastro(
        )->getId() : null;
        $array['campanhaUsuarioAlteracao'] = $this->getCampanhaUsuarioAlteracao() ? $this->getCampanhaUsuarioAlteracao(
        )->getId() : null;

        return $array;
    }
}
