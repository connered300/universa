<?php

namespace Comunicacao\Controller;

use VersaSpine\Controller\AbstractCoreController;

class ComunicacaoCampanhaRegraController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request                         = $this->getRequest();
        $paramsGet                       = $request->getQuery()->toArray();
        $paramsPost                      = $request->getPost()->toArray();
        $serviceComunicacaoCampanhaRegra = new \Comunicacao\Service\ComunicacaoCampanhaRegra($this->getEntityManager());

        $result = $serviceComunicacaoCampanhaRegra->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceComunicacaoCampanhaRegra = new \Comunicacao\Service\ComunicacaoCampanhaRegra($this->getEntityManager());
        $serviceComunicacaoCampanhaRegra->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceComunicacaoCampanhaRegra = new \Comunicacao\Service\ComunicacaoCampanhaRegra(
                $this->getEntityManager()
            );

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceComunicacaoCampanhaRegra->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function addAction($regraId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados                        = array();
        $serviceComunicacaoCampanhaRegra = new \Comunicacao\Service\ComunicacaoCampanhaRegra($this->getEntityManager());

        if ($regraId) {
            $arrDados = $serviceComunicacaoCampanhaRegra->getArray($regraId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceComunicacaoCampanhaRegra->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de campanha regra salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceComunicacaoCampanhaRegra->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceComunicacaoCampanhaRegra->getLastError());
                }
            }
        }

        $serviceComunicacaoCampanhaRegra->formataDadosPost($arrDados);
        $serviceComunicacaoCampanhaRegra->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $regraId = $this->params()->fromRoute("id", 0);

        if (!$regraId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($regraId);
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceComunicacaoCampanhaRegra = new \Comunicacao\Service\ComunicacaoCampanhaRegra(
                $this->getEntityManager()
            );

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceComunicacaoCampanhaRegra->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceComunicacaoCampanhaRegra->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}
?>