<?php

namespace Comunicacao\Controller;

use VersaSpine\Controller\AbstractCoreController;
use Zend\Session\Container;

class ComunicacaoCampanhaController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request                    = $this->getRequest();
        $paramsGet                  = $request->getQuery()->toArray();
        $paramsPost                 = $request->getPost()->toArray();
        $serviceComunicacaoCampanha = new \Comunicacao\Service\ComunicacaoCampanha($this->getEntityManager());

        $result = $serviceComunicacaoCampanha->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceComunicacaoCampanha = new \Comunicacao\Service\ComunicacaoCampanha($this->getEntityManager());
        $serviceComunicacaoCampanha->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceComunicacaoCampanha = new \Comunicacao\Service\ComunicacaoCampanha($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceComunicacaoCampanha->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function addAction($campanhaId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if ($request->isxmlhttprequest()) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados                   = array();
        $serviceComunicacaoCampanha = new \Comunicacao\Service\ComunicacaoCampanha($this->getEntityManager());

        if ($campanhaId) {
            $arrDados = $serviceComunicacaoCampanha->getArray($campanhaId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceComunicacaoCampanha->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de campanha salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceComunicacaoCampanha->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceComunicacaoCampanha->getLastError());
                }
            }
        }

        $serviceComunicacaoCampanha->formataDadosPost($arrDados);
        $serviceComunicacaoCampanha->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $campanhaId = $this->params()->fromRoute("id", 0);

        if (!$campanhaId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($campanhaId);
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceComunicacaoCampanha = new \Comunicacao\Service\ComunicacaoCampanha($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceComunicacaoCampanha->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceComunicacaoCampanha->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }

    public function campanhaAction()
    {
        $this->getView()->setTemplate('comunicacao/comunicacao-campanha/campanha');

        return $this->getView();
    }
}

?>