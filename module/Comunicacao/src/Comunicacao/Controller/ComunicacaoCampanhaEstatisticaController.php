<?php

namespace Comunicacao\Controller;

use VersaSpine\Controller\AbstractCoreController;

class ComunicacaoCampanhaEstatisticaController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request                               = $this->getRequest();
        $paramsGet                             = $request->getQuery()->toArray();
        $paramsPost                            = $request->getPost()->toArray();
        $serviceComunicacaoCampanhaEstatistica = new \Comunicacao\Service\ComunicacaoCampanhaEstatistica(
            $this->getEntityManager()
        );

        $result = $serviceComunicacaoCampanhaEstatistica->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceComunicacaoCampanhaEstatistica = new \Comunicacao\Service\ComunicacaoCampanhaEstatistica(
            $this->getEntityManager()
        );
        $serviceComunicacaoCampanhaEstatistica->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceComunicacaoCampanhaEstatistica = new \Comunicacao\Service\ComunicacaoCampanhaEstatistica(
                $this->getEntityManager()
            );

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceComunicacaoCampanhaEstatistica->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function addAction($estatisticaId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados                              = array();
        $serviceComunicacaoCampanhaEstatistica = new \Comunicacao\Service\ComunicacaoCampanhaEstatistica(
            $this->getEntityManager()
        );

        if ($estatisticaId) {
            $arrDados = $serviceComunicacaoCampanhaEstatistica->getArray($estatisticaId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceComunicacaoCampanhaEstatistica->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de campanha estatistica salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceComunicacaoCampanhaEstatistica->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceComunicacaoCampanhaEstatistica->getLastError());
                }
            }
        }

        $serviceComunicacaoCampanhaEstatistica->formataDadosPost($arrDados);
        $serviceComunicacaoCampanhaEstatistica->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $estatisticaId = $this->params()->fromRoute("id", 0);

        if (!$estatisticaId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($estatisticaId);
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceComunicacaoCampanhaEstatistica = new \Comunicacao\Service\ComunicacaoCampanhaEstatistica(
                $this->getEntityManager()
            );

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceComunicacaoCampanhaEstatistica->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceComunicacaoCampanhaEstatistica->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }

    public function atualizaEstatisticaAction()
    {
        $serviceComunicacaoCampanha = new \Comunicacao\Service\ComunicacaoCampanhaEstatistica($this->getEntityManager());
        $request                    = $this->getRequest();

        if ($request->isPost()) {
            $arrParams = $request->getPost()->toArray();

            if ($serviceComunicacaoCampanha->atualizarEstatistica($arrParams)) {
                $this->getJson()->setVariables(
                    array(
                        'error'   => false,
                        'message' => 'Salvo com sucesso!'
                    ));
            }

            $this->getJson()->setVariables(
                array(
                    'error'   => true,
                    'message' => $serviceComunicacaoCampanha->getLastError()
                ));
        }

        return $this->getJson();
    }
}

?>