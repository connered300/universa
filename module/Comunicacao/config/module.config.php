<?php

namespace Comunicacao;

return array(
    'router'                    => array(
        'routes' => array(
            'comunicacao' => array(
                'type'          => 'Literal',
                'options'       => array(
                    'route'    => '/comunicacao',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Comunicacao\Controller',
                        'controller'    => 'ComunicacaoCampanha',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes'  => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'       => '/[:controller[/:action[/:id]]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id'         => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults'    => array(
                                'action' => 'index'
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'controllers'               => array(
        'invokables' => array(
            'Comunicacao\Controller\ComunicacaoCampanha'            => 'Comunicacao\Controller\ComunicacaoCampanhaController',
            'Comunicacao\Controller\ComunicacaoCampanhaEstatistica' => 'Comunicacao\Controller\ComunicacaoCampanhaEstatisticaController',
            'Comunicacao\Controller\ComunicacaoCampanhaRegra'       => 'Comunicacao\Controller\ComunicacaoCampanhaRegraController'
        ),
    ),
    'service_manager'           => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases'            => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'doctrine'                  => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default'             => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            ),
        ),
    ),
    'view_manager'              => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'console'                   => array(
        'router' => array(
            'routes' => array(),
        ),
    ),
    'funcionalidades_liberadas' => array(
        'controllers' => array(
            'Comunicacao\Controller\ComunicacaoCampanha::campanha',
            'Comunicacao\Controller\ComunicacaoCampanhaEstatistica::atualiza-estatistica'
        ),
        'actions'     => array(
            'campanha',
            'atualiza-estatistica'
        ),
    ),
    'namesDictionary'           => array(
        'Comunicacao\ComunicacaoCampanha'            => 'Campanha',
        'Comunicacao\ComunicacaoCampanhaEstatistica' => 'Esstatísticas de Campanha ',
        'Comunicacao\ComunicacaoCampanhaRegra'       => 'Regra de Campanha',
    ),
);