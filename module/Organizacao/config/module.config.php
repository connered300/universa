<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Organizacao;

return array(
    'router'                    => array(
        'routes' => array(
            'organizacao' => array(
                'type'          => 'Literal',
                'options'       => array(
                    'route'    => '/organizacao',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Organizacao\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes'  => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'       => '/[:controller[/:action[/:id]]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults'    => array(),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager'           => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases'            => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'doctrine'                  => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default'             => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            ),
        ),
    ),
    'controllers'               => array(
        'invokables' => array(
            'Organizacao\Controller\OrgIes'                       => 'Organizacao\Controller\OrgIesController',
            'Organizacao\Controller\OrgCampus'                    => 'Organizacao\Controller\OrgCampusController',
            'Organizacao\Controller\AcadgeralDisciplina'          => 'Organizacao\Controller\AcadgeralDisciplinaController',
            'Organizacao\Controller\AcadperiodoMatrizCurricular'  => 'Organizacao\Controller\AcadperiodoMatrizCurricularController',
            'Organizacao\Controller\AcadperiodoDocenteDisciplina' => 'Organizacao\Controller\AcadperiodoDocenteDisciplinaController',
            'Organizacao\Controller\OrgAgenteEducacional'         => 'Organizacao\Controller\OrgAgenteEducacionalController',
            'Organizacao\Controller\OrgEmailConta'                => 'Organizacao\Controller\OrgEmailContaController',
            'Organizacao\Controller\OrgComunicacaoTipo'           => 'Organizacao\Controller\OrgComunicacaoTipoController',
            'Organizacao\Controller\OrgComunicacaoModelo'         => 'Organizacao\Controller\OrgComunicacaoModeloController',
            'Organizacao\Controller\OrgUnidadeEstudo'             => 'Organizacao\Controller\OrgUnidadeEstudoController',
        ),
    ),
    'view_manager'              => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console'                   => array(
        'router' => array(
            'routes' => array(),
        ),
    ),
    'funcionalidades_liberadas' => array(
        'controllers' => array(),
        'actions'     => array(),
    ),
    'namesDictionary'           => array(
        'Organizacao'                             => 'Organização',
        'Organizacao\OrgIes'                      => 'Instituição',
        'Organizacao\OrgCampus'                   => 'Câmpus',
        'Organizacao\OrgUnidadeEstudo'            => 'Unidade de estudo',
        'Organizacao\OrgAgenteEducacional'        => 'Agente Educacional',
        'Organizacao\OrgEmailConta'               => 'Contas de e-mail',
        'Organizacao\OrgComunicacaoModelo'        => 'Modelo de comunicação',
        'Organizacao\OrgComunicacaoTipo'          => 'Tipo de comunicação',
        'Organizacao\AcadperiodoMatrizCurricular' => 'Matriz Curricular',
    )
);
