<?php


namespace Organizacao\Form;

use Zend\Form\Form,
    Zend\Form\Element;

class OrgCampus extends Form{
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name);

        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', '');
        $this->setAttribute('role', 'form');
        $this->setAttribute('id', 'formOrgCampus');

        $campId = new Element\Hidden('campId');
        $this->add($campId);

        $instituicao = new Element\Select("ies");
        $instituicao->setAttributes([
            'col'      => '8',
            'label'    => 'Instituição: ',
            'wrap'     => true,
            'required' => true,
        ]);
        if($options['ies']){
            $instituicao->setValueOptions($options['ies']);
        }
        $this->add($instituicao);

        $campNome = new Element\Text('campNome');
        $campNome->setAttributes([
            'col'      => '8',
            'wrap'     => true,
            'label'    => 'Nome do Câmpus: ',
            'required' => true,
            'placeholder' => 'Nome do Câmpus: '
        ]);
        $this->add($campNome);

        $email = new Element\Text('campEmail');
        $email->setAttributes([
            'col'      => '8',
            'wrap'     => true,
            'label'    => 'Email:',
            'placeholder' => 'Email'
        ]);
        $this->add($email);

        $telefone = new Element\Text('campTelefone');
        $telefone->setAttributes([
            'col'         => '8',
            'wrap'        => true,
            'label'       => 'Telefone:',
            'data-masked' => 'Fone',
            'placeholder' => 'Telefone'
        ]);
        $this->add($telefone);

        $site = new Element\Text('campSite');
        $site->setAttributes([
            'col'     => '8',
            'wrap'    => true,
            'label'   => 'Site:',
            'placeholder' => 'Site'
        ]);
        $this->add($site);

        $end = new Element\Hidden('end');
        $this->add($end);

        $cadastrar = new Element\Submit('cadastrar');
        $cadastrar->setAttributes([
            'value'   => 'Cadastrar',
            'class'   => 'btn btn-primary pull-right',
        ]);
        $this->add($cadastrar);

        $salvar = new Element\Submit('salvar');
        $salvar->setAttributes([
            'value'   => 'Salvar',
            'class'   => 'btn btn-primary pull-right',
        ]);
        $this->add($salvar);
    }
}