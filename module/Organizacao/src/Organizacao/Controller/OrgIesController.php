<?php

namespace Organizacao\Controller;

use VersaSpine\Controller\AbstractCoreController;

class OrgIesController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request       = $this->getRequest();
        $paramsGet     = $request->getQuery()->toArray();
        $paramsPost    = $request->getPost()->toArray();
        $serviceOrgIes = new \Organizacao\Service\OrgIes($this->getEntityManager());

        $result = $serviceOrgIes->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceOrgIes = new \Organizacao\Service\OrgIes($this->getEntityManager());

        $serviceOrgIes->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceOrgIes = new \Organizacao\Service\OrgIes($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceOrgIes->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);

            return $this->getJson();
        }

        return $this->getJson();
    }

    public function addAction($iesId = false)
    {
        $request  = $this->getRequest();
        $arrDados = array();

        $serviceOrgIes       = new \Organizacao\Service\OrgIes($this->getEntityManager());
        $servicePessoaFisica = new \Pessoa\Service\PessoaFisica($this->getEntityManager());

        if ($iesId) {
            $arrDados = $serviceOrgIes->getArray($iesId);

            if (empty($arrDados)) {
                $this->flashMessenger()->addErrorMessage($serviceOrgIes->getLastError());

                return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );

            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceOrgIes->salvar($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $this->flashMessenger()->addSuccessMessage('Registro de instituição salvo!');

                return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
            } else {
                $this->flashMessenger()->addErrorMessage($serviceOrgIes->getLastError());
                $this->getView()->setVariable("erro", array($serviceOrgIes->getLastError()));
            }
        }

        $serviceOrgIes->setarDependenciasView($this->getView());

        $serviceOrgIes->formataDadosPost($arrDados);
        $servicePessoaFisica->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $iesId = $this->params()->fromRoute("id", 0);

        if (!$iesId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($iesId);
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceOrgIes = new \Organizacao\Service\OrgIes($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceOrgIes->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceOrgIes->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);

            return $this->getJson();
        }

        return $this->getJson();
    }
}
?>