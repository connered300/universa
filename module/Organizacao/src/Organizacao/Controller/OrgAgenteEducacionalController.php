<?php

namespace Organizacao\Controller;

use VersaSpine\Controller\AbstractCoreController;

class OrgAgenteEducacionalController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request    = $this->getRequest();
        $paramsGet  = $request->getQuery()->toArray();
        $paramsPost = $request->getPost()->toArray();
        $param      = array_merge($paramsGet, $paramsPost);

        $serviceOrgAgenteEducacional = new \Organizacao\Service\OrgAgenteEducacional($this->getEntityManager());

        $arrDados = $serviceOrgAgenteEducacional->pesquisaForJson($param);

        if ($param['pesId'] && $param['dadosDeAgente'] && $arrDados) {
            $arrDados = $arrDados[0];

            $arrDados = $serviceOrgAgenteEducacional->getArrayFallback($arrDados['pesId']);
            $serviceOrgAgenteEducacional->formataDadosPost($arrDados);
        }

        return new \Zend\View\Model\JsonModel($arrDados ? $arrDados : array());
    }

    public function indexAction()
    {
        $serviceOrgAgenteEducacional = new \Organizacao\Service\OrgAgenteEducacional($this->getEntityManager());
        $serviceOrgAgenteEducacional->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceOrgAgenteEducacional = new \Organizacao\Service\OrgAgenteEducacional($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceOrgAgenteEducacional->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function editAction()
    {
        $pesId = $this->params()->fromRoute("id", 0);

        if (!$pesId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($pesId);
    }

    public function addAction($pesId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados                    = array();
        $serviceOrgAgenteEducacional = new \Organizacao\Service\OrgAgenteEducacional(
            $this->getEntityManager(),
            $this->getServiceManager()->get('Config')
        );

        if ($pesId) {
            $arrDados = $serviceOrgAgenteEducacional->getArray($pesId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );

            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);

            $salvar = $serviceOrgAgenteEducacional->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de agente educacional salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceOrgAgenteEducacional->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceOrgAgenteEducacional->getLastError());
                }
            }
        }

        $serviceOrgAgenteEducacional->formataDadosPost($arrDados);
        $serviceOrgAgenteEducacional->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceOrgAgenteEducacional = new \Organizacao\Service\OrgAgenteEducacional($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceOrgAgenteEducacional->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceOrgAgenteEducacional->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }

    public function buscaInformacoesAgenteEducacionalAction()
    {
        $service = new \Organizacao\Service\OrgAgenteEducacional($this->getEntityManager());

        $request    = $this->getRequest();
        $paramsGet  = $request->getQuery()->toArray();
        $paramsPost = $request->getPost()->toArray();
        $dados      = array_merge($paramsGet, $paramsPost);

        $result                     = $service->getArray($dados['pesId']);
        $result['origem']           = $result['origem']['text'];
        $result['login']            = $result['acesso']['login'];
        $result['pesNomeIndicacao'] = $result['pesIdIndicacao']['text'];

        if (is_array($result)) {
            $this->getJson()->setVariable('erro', false);
            $this->getJson()->setVariable('result', $result);
        } else {
            $this->getJson()->setVariable('erro', true);
        }

        return $this->getJson();
    }
}
?>