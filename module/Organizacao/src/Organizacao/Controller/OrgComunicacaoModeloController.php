<?php

namespace Organizacao\Controller;

use VersaSpine\Controller\AbstractCoreController;

class OrgComunicacaoModeloController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request                     = $this->getRequest();
        $paramsGet                   = $request->getQuery()->toArray();
        $paramsPost                  = $request->getPost()->toArray();
        $serviceOrgComunicacaoModelo = new \Organizacao\Service\OrgComunicacaoModelo($this->getEntityManager());

        $result = $serviceOrgComunicacaoModelo->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceOrgComunicacaoModelo = new \Organizacao\Service\OrgComunicacaoModelo($this->getEntityManager());
        $serviceOrgComunicacaoModelo->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceOrgComunicacaoModelo = new \Organizacao\Service\OrgComunicacaoModelo($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceOrgComunicacaoModelo->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function editAction()
    {
        $modeloId = $this->params()->fromRoute("id", 0);

        if (!$modeloId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($modeloId);
    }

    public function addAction($modeloId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados                    = array();
        $serviceOrgComunicacaoModelo = new \Organizacao\Service\OrgComunicacaoModelo($this->getEntityManager());

        if ($modeloId) {
            $arrDados = $serviceOrgComunicacaoModelo->getArray($modeloId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceOrgComunicacaoModelo->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de comunicação modelo salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceOrgComunicacaoModelo->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceOrgComunicacaoModelo->getLastError());
                }
            }
        }

        $serviceOrgComunicacaoModelo->formataDadosPost($arrDados);
        $serviceOrgComunicacaoModelo->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceOrgComunicacaoModelo = new \Organizacao\Service\OrgComunicacaoModelo($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceOrgComunicacaoModelo->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceOrgComunicacaoModelo->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }

    public function enviarComunicacaoAction()
    {
        $serviceOrgModelo = new \Organizacao\Service\OrgComunicacaoModelo($this->getEntityManager());

        $request   = $this->getRequest();
        $dadosGet  = $request->getQuery()->toArray();
        $dadosPost = $request->getPost()->toArray();
        $dados     = array_merge($dadosGet, $dadosPost);

        $envio = $serviceOrgModelo->registrarSmsOuEmailGenerico($dados);

        if ($envio) {
            $this->getJson()->setVariable('erro', false);
            $this->getJson()->setVariable('message', 'Registrado na fila de envio com Sucesso!');
        } else {
            $this->getJson()->setVariable('erro', true);
            $this->getJson()->setVariable('message', $serviceOrgModelo->getLastError());
        }

        return $this->getJson();
    }
}
?>