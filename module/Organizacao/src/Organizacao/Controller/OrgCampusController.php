<?php
namespace Organizacao\Controller;

use VersaSpine\Controller\AbstractCoreController;

class OrgCampusController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request          = $this->getRequest();
        $paramsGet        = $request->getQuery()->toArray();
        $paramsPost       = $request->getPost()->toArray();
        $serviceOrgCampus = new \Organizacao\Service\OrgCampus($this->getEntityManager());

        $result = $serviceOrgCampus->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceOrgCampus= new \Organizacao\Service\OrgCampus($this->getEntityManager());

        $serviceOrgCampus->setarDependenciasView($this->getView());
        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceOrgCampus = new \Organizacao\Service\OrgCampus($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceOrgCampus->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);

            return $this->getJson();
        }

        return $this->getJson();
    }

    public function addAction($campId = false)
    {
        $request  = $this->getRequest();
        $arrDados = array();

        $serviceOrgCampus    = new \Organizacao\Service\OrgCampus($this->getEntityManager());
        $servicePessoaFisica = new \Pessoa\Service\PessoaFisica($this->getEntityManager());
        $servicePredio       = new \Infraestrutura\Service\InfraPredio($this->getEntityManager());

        if ($campId) {
            $arrDados = $serviceOrgCampus->getArray($campId);

            if (empty($arrDados)) {
                $this->flashMessenger()->addErrorMessage($serviceOrgCampus->getLastError());

                return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge($arrDados,array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );

            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceOrgCampus->salvar($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);

                $this->flashMessenger()->addSuccessMessage('Registro de câmpus salvo!');

                return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
            }

            $this->flashMessenger()->addErrorMessage($serviceOrgCampus->getLastError());
        }

        $serviceOrgCampus->formataDadosPost($arrDados);

        $serviceOrgCampus->setarDependenciasView($this->getView());

        $servicePessoaFisica->setarDependenciasView($this->getView());

        $arrPredioTipo = $servicePredio->getArrSelect2PredioTipo();

        $this->getView()->setVariable("arrPredioTipo", $arrPredioTipo);
        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $campId = $this->params()->fromRoute("id", 0);

        if (!$campId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($campId);
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceOrgCampus = new \Organizacao\Service\OrgCampus($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceOrgCampus->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceOrgCampus->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }

}