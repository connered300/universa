<?php

namespace Organizacao\Controller;

use VersaSpine\Controller\AbstractCoreController;

class OrgComunicacaoTipoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request                   = $this->getRequest();
        $paramsGet                 = $request->getQuery()->toArray();
        $paramsPost                = $request->getPost()->toArray();
        $serviceOrgComunicacaoTipo = new \Organizacao\Service\OrgComunicacaoTipo($this->getEntityManager());

        $result = $serviceOrgComunicacaoTipo->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceOrgComunicacaoTipo = new \Organizacao\Service\OrgComunicacaoTipo($this->getEntityManager());
        $serviceOrgComunicacaoTipo->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceOrgComunicacaoTipo = new \Organizacao\Service\OrgComunicacaoTipo($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceOrgComunicacaoTipo->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function editAction()
    {
        $tipoId = $this->params()->fromRoute("id", 0);

        if (!$tipoId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($tipoId);
    }

    public function addAction($tipoId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados                  = array();
        $serviceOrgComunicacaoTipo = new \Organizacao\Service\OrgComunicacaoTipo($this->getEntityManager());

        if ($tipoId) {
            $arrDados = $serviceOrgComunicacaoTipo->getArray($tipoId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceOrgComunicacaoTipo->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de tipo de comunicação salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceOrgComunicacaoTipo->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceOrgComunicacaoTipo->getLastError());
                }
            }
        }

        $serviceOrgComunicacaoTipo->formataDadosPost($arrDados);
        $serviceOrgComunicacaoTipo->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceOrgComunicacaoTipo = new \Organizacao\Service\OrgComunicacaoTipo($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceOrgComunicacaoTipo->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceOrgComunicacaoTipo->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}
?>