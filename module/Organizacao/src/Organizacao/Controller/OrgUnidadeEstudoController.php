<?php

namespace Organizacao\Controller;

use VersaSpine\Controller\AbstractCoreController;

class OrgUnidadeEstudoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request                 = $this->getRequest();
        $paramsGet               = $request->getQuery()->toArray();
        $paramsPost              = $request->getPost()->toArray();
        $serviceOrgUnidadeEstudo = new \Organizacao\Service\OrgUnidadeEstudo($this->getEntityManager());

        $result = $serviceOrgUnidadeEstudo->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceOrgUnidadeEstudo= new \Organizacao\Service\OrgUnidadeEstudo($this->getEntityManager());

        $serviceOrgUnidadeEstudo->setarDependenciasView($this->getView());
        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceOrgUnidadeEstudo = new \Organizacao\Service\OrgUnidadeEstudo($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceOrgUnidadeEstudo->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function editAction()
    {
        $unidadeId = $this->params()->fromRoute("id", 0);

        if (!$unidadeId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($unidadeId);
    }

    public function addAction($unidadeId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados                = array();
        $serviceOrgUnidadeEstudo = new \Organizacao\Service\OrgUnidadeEstudo($this->getEntityManager());

        if ($unidadeId) {
            $arrDados = $serviceOrgUnidadeEstudo->getArray($unidadeId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceOrgUnidadeEstudo->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de unidade estudo salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceOrgUnidadeEstudo->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceOrgUnidadeEstudo->getLastError());
                }
            }
        }
        $serviceOrgUnidadeEstudo->setarDependenciasView($this->getView());
        $serviceOrgUnidadeEstudo->formataDadosPost($arrDados);

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceOrgUnidadeEstudo = new \Organizacao\Service\OrgUnidadeEstudo($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceOrgUnidadeEstudo->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceOrgUnidadeEstudo->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}
?>