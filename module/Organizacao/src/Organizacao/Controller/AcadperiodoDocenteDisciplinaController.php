<?php


namespace Organizacao\Controller;


use VersaSpine\Controller\AbstractCoreController;

class AcadperiodoDocenteDisciplinaController extends AbstractCoreController{
    public function __construct()
    {
        parent::__construct(__CLASS__);
        $this->setService('Matricula\Service\AcadperiodoDocenteDisciplina');
        $this->setEntity('Matricula\Entity\AcadperiodoDocenteDisciplina');
        $this->setForm('Matricula\Form\AcadperiodoDocenteDisciplina');
        $this->readAnnotationsConfig($this->getEntity());
    }

    public function indexAction()
    {
        return $this->getView();
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {

    }
}