<?php

namespace Organizacao\Controller;

use VersaSpine\Controller\AbstractCoreController;

class AcadperiodoMatrizCurricularController extends AbstractCoreController
{
    function __construct()
    {
        parent::__construct(__CLASS__);
        $this->setService('Matricula\Service\AcadperiodoMatrizCurricular');
        $this->setEntity('Matricula\Entity\AcadperiodoMatrizCurricular');
        $this->setForm('Matricula\Form\AcadperiodoMatrizCurricular');
        $this->readAnnotationsConfig($this->getEntity());
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {
        $request    = $this->getRequest();
        $paramsGet  = $request->getQuery()->toArray();
        $paramsPost = $request->getPost()->toArray();
        $param      = array_merge($paramsGet, $paramsPost);

        $serviceMatrizCurricular = new \Matricula\Service\AcadperiodoMatrizCurricular($this->getEntityManager());

        $arrDados = $serviceMatrizCurricular->pesquisaForJson($param);

        return new \Zend\View\Model\JsonModel($arrDados ? $arrDados : array());
    }

    public function addAction()
    {
        $request              = $this->getRequest();
        $service              = new \Matricula\Service\AcadperiodoMatrizCurricular($this->getEntityManager());
        $form                 = new \Matricula\Form\AcadperiodoMatrizCurricular();
        $formMatrizDisciplina = new \Matricula\Form\AcadperiodoMatrizDisciplina();

        $serviceAcadCurso = new \Matricula\Service\AcadCurso($this->getEntityManager());
        $form->get("curso")->setValueOptions($serviceAcadCurso->mountArrayCurso());

        $request = $this->getRequest();
        if ($request->isPost()) {
            $dados = $request->getPost()->toArray();
            try {
                $service->adicionar($dados);
                $this->flashMessenger()->addSuccessMessage('Matriz curricular cadastrada com sucesso.');

                return $this->redirect()->toRoute(
                    "organizacao/default",
                    ['controller' => 'acadperiodo-matriz-curricular', 'action' => 'index']
                );
            } catch (\Exception $ex) {
                $this->flashMessenger()->addErrorMessage('Erro ao cadastrar matriz curricular! Tente novamente.');

                return $this->redirect()->toRoute(
                    "organizacao/default",
                    ['controller' => 'acadperiodo-matriz-curricular', 'action' => 'index']
                );
            }
        }

        if ($request->getQuery('curso')) {
            $curso = $service->getRepository('Matricula\Entity\AcadCurso')->findOneBy(
                ['cursoId' => $request->getQuery('curso')]
            );
        } else {
            $curso = $service->getRepository('Matricula\Entity\AcadCurso')->findOneBy([], ['cursoId' => 'asc']);
        }

        $form->get("curso")->setValue($curso->getCursoId());

        $disciplinas = $service->getRepository('Matricula\Entity\AcadgeralDisciplina')->findBy(
            [],
            ['discNome' => 'ASC']
        );
        $this->view->setVariables(
            [
                'form'                 => $form,
                'curso'                => $curso,
                'disciplinas'          => $disciplinas,
                'formMatrizDisciplina' => $formMatrizDisciplina
            ]
        );
        $this->view->setTemplate($this->getTemplateToRoute());

        return $this->view;
    }

    public function editAction()
    {
        $request                 = $this->getRequest();
        $service                 = new \Matricula\Service\AcadperiodoMatrizCurricular($this->getEntityManager());
        $serviceMatrizDisciplina = $this->services()->getService('Matricula\Service\AcadperiodoMatrizDisciplina');
        $form                    = new \Matricula\Form\AcadperiodoMatrizCurricular();
        $formMatrizDisciplina    = new \Matricula\Form\AcadperiodoMatrizDisciplina();

        $serviceAcadCurso = new \Matricula\Service\AcadCurso($this->getEntityManager());
        $form->get("curso")->setValueOptions($serviceAcadCurso->mountArrayCurso());

        $id               = $this->params()->fromRoute('id');
        $matriz           = $service->getRepository($service->getEntity())->findOneBy(['matCurId' => $id]);
        $matrizDisciplina = $serviceMatrizDisciplina->getRepository($serviceMatrizDisciplina->getEntity())->findBy(
            ['matCur' => $matriz]
        );

        for ($i = 0; $i < count($matrizDisciplina); $i++) {
            if (!is_null($matrizDisciplina[$i]->getPerDiscChestagio())) {
                $matrizDisciplina[$i]->setPerDiscChestagio((string)$matrizDisciplina[$i]->getPerDiscChestagio());
            }
            if (!is_null($matrizDisciplina[$i]->getPerDiscChpratica())) {
                $matrizDisciplina[$i]->setPerDiscChpratica((string)$matrizDisciplina[$i]->getPerDiscChpratica());
            }
            if (!is_null($matrizDisciplina[$i]->getPerDiscChteorica())) {
                $matrizDisciplina[$i]->setPerDiscChteorica((string)$matrizDisciplina[$i]->getPerDiscChteorica());
            }
        }

        $matrizArray = $matriz->toArray();
        $form->setData($matriz->toArray());

        $request = $this->getRequest();

        if ($request->isPost()) {
            $dados = $request->getPost()->toArray();
            try {
                $service->edita($dados);
                $this->flashMessenger()->addSuccessMessage('Matriz curricular editada com sucesso.');

                return $this->redirect()->toRoute(
                    "organizacao/default",
                    ['controller' => 'AcadperiodoMatrizCurricular', 'action' => 'index']
                );
            } catch (\Exception $ex) {
                $this->flashMessenger()->addErrorMessage('Erro ao editar matriz curricular! Tente novamente.');

                return $this->redirect()->toRoute(
                    "organizacao/default",
                    ['controller' => 'AcadperiodoMatrizCurricular', 'action' => 'index']
                );
            }
        }

        $discSelecionadas = [];
        for ($i = 1; $i <= 10; $i++) {
            $discSelecionadas[$i] = $serviceMatrizDisciplina->buscaDiscPeriodo($i, $matriz->getMatCurId());
        }

        if ($matriz) {
            $curso = $matriz->getCurso();
        } else {
            $curso = $service->getRepository('Matricula\Entity\AcadCurso')->findOneBy([], ['cursoId' => 'asc']);
        }

        if ($request->getQuery('curso')) {
            $curso = $service->getRepository('Matricula\Entity\AcadCurso')->findOneBy(
                ['cursoId' => $request->getQuery('curso')]
            );
        }

        $form->get("curso")->setValue($curso->getCursoId());

        $disciplinas = $service->getRepository('Matricula\Entity\AcadgeralDisciplina')->findBy(
            [],
            ['discNome' => 'ASC']
        );

        $periodoTravado = $service->buscaPeriodoTravado($matriz->getMatCurId());

        $this->view->setVariables(
            [
                'form'                 => $form,
                'curso'                => $curso,
                'disciplinas'          => $disciplinas,
                'formMatrizDisciplina' => $formMatrizDisciplina,
                'matrizDisciplina'     => $matrizDisciplina,
                'discSelecionada'      => $discSelecionadas,
                'periodoTravado'       => $periodoTravado
            ]
        );
        $this->view->setTemplate('organizacao/acadperiodo-matriz-curricular/add');

        return $this->view;
    }

    public function clonarAction()
    {
        $request                 = $this->getRequest();
        $service                 = new \Matricula\Service\AcadperiodoMatrizCurricular($this->getEntityManager());
        $serviceMatrizDisciplina = $this->services()->getService('Matricula\Service\AcadperiodoMatrizDisciplina');
        $form                    = new \Matricula\Form\AcadperiodoMatrizCurricular();
        $formMatrizDisciplina    = new \Matricula\Form\AcadperiodoMatrizDisciplina();

        $serviceAcadCurso = new \Matricula\Service\AcadCurso($this->getEntityManager());
        $form->get("curso")->setValueOptions($serviceAcadCurso->mountArrayCurso());

        $id               = $this->params()->fromRoute('id');
        $matriz           = $service->getRepository($service->getEntity())->findOneBy(['matCurId' => $id]);
        $matrizDisciplina = $serviceMatrizDisciplina->getRepository($serviceMatrizDisciplina->getEntity())->findBy(
            ['matCur' => $matriz]
        );
        for ($i = 0; $i < count($matrizDisciplina); $i++) {
            if (!is_null($matrizDisciplina[$i]->getPerDiscChestagio())) {
                $matrizDisciplina[$i]->setPerDiscChestagio(
                    (string)$matrizDisciplina[$i]->getPerDiscChestagio() . ':00'
                );
            }
            if (!is_null($matrizDisciplina[$i]->getPerDiscChpratica())) {
                $matrizDisciplina[$i]->setPerDiscChpratica(
                    (string)$matrizDisciplina[$i]->getPerDiscChpratica() . ':00'
                );
            }
            if (!is_null($matrizDisciplina[$i]->getPerDiscChteorica())) {
                $matrizDisciplina[$i]->setPerDiscChteorica(
                    (string)$matrizDisciplina[$i]->getPerDiscChteorica() . ':00'
                );
            }
        }
        $matrizArray = $matriz->toArray();
        $form->setData($matriz->toArray());

        $request = $this->getRequest();
        if ($request->isPost()) {
            $dados = $request->getPost()->toArray();
            try {
                $service->edita($dados);
                $this->flashMessenger()->addSuccessMessage('Matriz curricular editada com sucesso.');

                return $this->redirect()->toRoute(
                    "organizacao/default",
                    ['controller' => 'AcadperiodoMatrizCurricular', 'action' => 'index']
                );
            } catch (\Exception $ex) {
                $this->flashMessenger()->addErrorMessage('Erro ao editar matriz curricular! Tente novamente.');

                return $this->redirect()->toRoute(
                    "organizacao/default",
                    ['controller' => 'AcadperiodoMatrizCurricular', 'action' => 'index']
                );
            }
        }
        $discSelecionadas = [];

        for ($i = 1; $i <= 10; $i++) {
            $discSelecionadas[$i] = $serviceMatrizDisciplina->buscaDiscPeriodo($i, $matriz->getMatCurId());
        }

        $curso = $service->getRepository('Matricula\Entity\AcadCurso')->findOneBy(
            ['cursoId' => $matriz->getCurso()->getCursoId()]
        );
        $form->get("curso")->setValue($curso->getCursoId());

        $disciplinas = $service->getRepository('Matricula\Entity\AcadgeralDisciplina')->findBy(
            [],
            ['discNome' => 'ASC']
        );
        /**
         * Limpando o id do formulário.
         * */
        $formId = $form->get('matCurId');
        $formId->setValue('');
        $form->add($formId);

        /**
         * Mudando a action do formulário para add.
         * */
        $form->setAttribute('action', '/organizacao/acadperiodo-matriz-curricular/add');

        $this->view->setVariables(
            [
                'form'                 => $form,
                'curso'                => $curso,
                'disciplinas'          => $disciplinas,
                'formMatrizDisciplina' => $formMatrizDisciplina,
                'matrizDisciplina'     => $matrizDisciplina,
                'discSelecionada'      => $discSelecionadas,
            ]
        );
        $this->view->setTemplate('organizacao/acadperiodo-matriz-curricular/add');
        $this->flashMessenger()->addInfoMessage('Edição do clone da matriz');

        return $this->view;
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAcadperiodoMatriz = new \Matricula\Service\AcadperiodoMatrizCurricular($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceAcadperiodoMatriz->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function indexAction()
    {
        return $this->getView();
    }
}