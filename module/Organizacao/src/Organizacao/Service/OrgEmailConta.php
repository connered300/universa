<?php

namespace Organizacao\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class OrgEmailConta
 * @package Organizacao\Service
 */
class OrgEmailConta extends AbstractService
{
    const CONTA_SSL_SSL = 'ssl';
    const CONTA_SSL_TLS = 'tls';
    const CONTA_IMAP_SSL_SSL = 'ssl';
    const CONTA_IMAP_SSL_TLS = 'tls';
    const CONTA_LEAD_SIM = 'Sim';
    const CONTA_LEAD_NAO = 'Não';
    const CONTA_ATIVADA_SIM = 'Sim';
    const CONTA_ATIVADA_NAO = 'Não';
    const CONTA_PADRAO = 1;

    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Organizacao\Entity\OrgEmailConta');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2ContaSsl($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getContaSsl());
    }

    /**
     * @return array
     */
    public static function getContaSsl()
    {
        return array(self::CONTA_SSL_SSL, self::CONTA_SSL_TLS);
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2ContaImapSsl($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getContaImapSsl());
    }

    /**
     * @return array
     */
    public static function getContaImapSsl()
    {
        return array(self::CONTA_IMAP_SSL_SSL, self::CONTA_IMAP_SSL_TLS);
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2ContaLead($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getContaLead());
    }

    /**
     * @return array
     */
    public static function getContaLead()
    {
        return array(self::CONTA_LEAD_SIM, self::CONTA_LEAD_NAO);
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2ContaAtivada($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getContaAtivada());
    }

    /**
     * @return array
     */
    public static function getContaAtivada()
    {
        return array(self::CONTA_ATIVADA_SIM, self::CONTA_ATIVADA_NAO);
    }

    /**
     * @param $params
     * @return mixed
     */
    public function pesquisaForJson($params)
    {
        $sql       = 'SELECT * FROM org__email_conta WHERE';
        $contaNome = false;
        $contaId   = false;

        if ($params['q']) {
            $contaNome = $params['q'];
        } elseif ($params['query']) {
            $contaNome = $params['query'];
        }

        if ($params['contaId']) {
            $contaId = $params['contaId'];
        }

        $parameters = array('conta_nome' => "{$contaNome}%");
        $sql .= ' conta_nome LIKE :conta_nome';

        if ($contaId) {
            $parameters['conta_id'] = $contaId;
            $sql .= ' AND conta_id <> :conta_id';
        }

        $sql .= " ORDER BY conta_nome";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceOrgEmailTipo  = new \Organizacao\Service\OrgEmailTipo($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['contaId']) {
                /** @var $objOrgEmailConta \Organizacao\Entity\OrgEmailConta */
                $objOrgEmailConta = $this->getRepository()->find($arrDados['contaId']);

                if (!$objOrgEmailConta) {
                    $this->setLastError('Registro de conta de e-mail não existe!');

                    return false;
                }
            } else {
                $objOrgEmailConta = new \Organizacao\Entity\OrgEmailConta();
            }

            $objAcessoPessoas = $objOrgEmailConta->getUsuario();

            if (!$objAcessoPessoas) {
                $usuarioId = $arrDados['usuario'];

                if (!$usuarioId) {
                    $session    = new \Zend\Authentication\Storage\Session();
                    $arrUsuario = $session->read();

                    if (!$arrUsuario) {
                        $this->setLastError('Verifique se você está autenticado no sistema!');

                        return false;
                    }

                    $usuarioId = $arrUsuario['id'];
                }

                /* @var $objAcessoPessoas \Acesso\Entity\AcessoPessoas */
                $objAcessoPessoas = $serviceAcessoPessoas->getRepository()->find($usuarioId);

                if (!$objAcessoPessoas) {
                    $this->setLastError('Verifique se você está autenticado no sistema!');

                    return false;
                }

                $objOrgEmailConta->setUsuario($objAcessoPessoas);
            }

            $objOrgEmailConta->setContaNome($arrDados['contaNome']);
            $objOrgEmailConta->setContaHost($arrDados['contaHost']);
            $objOrgEmailConta->setContaUsuario($arrDados['contaUsuario']);
            $objOrgEmailConta->setContaSenha($arrDados['contaSenha']);
            $objOrgEmailConta->setContaPorta($arrDados['contaPorta']);
            $objOrgEmailConta->setContaSsl($arrDados['contaSsl']);
            $objOrgEmailConta->setContaImapHost($arrDados['contaImapHost']);
            $objOrgEmailConta->setContaImapPorta($arrDados['contaImapPorta']);
            $objOrgEmailConta->setContaImapSsl($arrDados['contaImapSsl']);
            $objOrgEmailConta->setContaLead($arrDados['contaLead']);
            $objOrgEmailConta->setContaAtivada($arrDados['contaAtivada']);

            $this->getEm()->persist($objOrgEmailConta);
            $this->getEm()->flush($objOrgEmailConta);

            if ($arrDados['tipo']) {
                $arrDados['tipo'] = explode(',', $arrDados['tipo']);
            } else {
                $arrDados['tipo'] = array();
            }

            if (!$serviceOrgEmailTipo->salvarArray($arrDados['tipo'], $objOrgEmailConta)) {
                $this->setLastError('Falha ao víncular assuntos!');

                return false;
            }

            $this->getEm()->commit();

            $arrDados['contaId'] = $objOrgEmailConta->getContaId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de conta de e-mail!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['contaNome']) {
            $errors[] = 'Por favor preencha o campo "nome"!';
        }

        if (!$arrParam['contaHost']) {
            $errors[] = 'Por favor preencha o campo "endereço SMTP"!';
        }

        if (!$arrParam['contaUsuario']) {
            $errors[] = 'Por favor preencha o campo "usuário"!';
        }

        if (!$arrParam['contaSenha']) {
            $errors[] = 'Por favor preencha o campo "senha"!';
        }

        if (!$arrParam['contaPorta']) {
            $errors[] = 'Por favor preencha o campo "porta SMTP"!';
        }

        if ($arrParam['contaSsl'] && !in_array($arrParam['contaSsl'], self::getContaSsl())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "protocolo SMTP"!';
        }

        if ($arrParam['contaImapSsl'] && !in_array($arrParam['contaImapSsl'], self::getContaImapSsl())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "protocolo IMAP"!';
        }

        if (!$arrParam['contaLead']) {
            $errors[] = 'Por favor preencha o campo "lead"!';
        }

        if (!in_array($arrParam['contaLead'], self::getContaLead())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "lead"!';
        }

        if (!$arrParam['contaAtivada']) {
            $errors[] = 'Por favor preencha o campo "ativada"!';
        }

        if (!in_array($arrParam['contaAtivada'], self::getContaAtivada())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "ativada"!';
        }

        $pluginEmail = new \Sistema\Plugins\Email();

        $erro = $pluginEmail->validSMTP(
            $arrParam['contaHost'],
            $arrParam['contaPorta'],
            $arrParam['contaUsuario'],
            $arrParam['contaSenha'],
            $arrParam['contaSsl']
        );

        if ($erro !== true) {
            $errors[] = $erro;
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM org__email_conta";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $contaId
     * @return array
     */
    public function getArray($contaId)
    {
        /** @var $objOrgEmailConta \Organizacao\Entity\OrgEmailConta */
        $objOrgEmailConta    = $this->getRepository()->find($contaId);
        $serviceOrgEmailTipo = new \Organizacao\Service\OrgEmailTipo($this->getEm());

        try {
            $arrDados         = $objOrgEmailConta->toArray();
            $arrDados['tipo'] = $serviceOrgEmailTipo->retornaArrayPelaConta($contaId);
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        if ($arrDados['tipo']) {
            if (!is_array($arrDados['tipo'])) {
                $arrDados['tipo'] = explode(',', $arrDados['tipo']);
            }

            if (!is_array($arrDados['tipo'][0])) {
                $objAssunto = new \Organizacao\Service\OrgComunicacaoTipo($this->getEm());
                $arrSelect2 = $objAssunto->getArrSelect2(array('id' => $arrDados['tipo']));

                $arrDataNormalized['tipo'] = $arrSelect2;
            } elseif (is_array($arrDados['tipo'])) {
                $arrDataNormalized['tipo'] = $arrDados['tipo'];
            }
        }

        return $arrDados;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('contaId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Organizacao\Entity\OrgEmailConta */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getContaId(),
                $params['value'] => $objEntity->getContaNome()
            );
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['contaId']) {
            $this->setLastError('Para remover um registro de conta de e-mail é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objOrgEmailConta \Organizacao\Entity\OrgEmailConta */
            $objOrgEmailConta = $this->getRepository()->find($param['contaId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objOrgEmailConta);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de conta de e-mail.');

            return false;
        }

        return true;
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $view->setVariable("arrContaSsl", $this->getArrSelect2ContaSsl());
        $view->setVariable("arrContaImapSsl", $this->getArrSelect2ContaImapSsl());
        $view->setVariable("arrContaLead", $this->getArrSelect2ContaLead());
        $view->setVariable("arrContaAtivada", $this->getArrSelect2ContaAtivada());
    }

    public function retornaContasAtivas($imap = false, $lead = false)
    {
        $sql = "
        SELECT a
        FROM Organizacao\Entity\OrgEmailConta a
        WHERE a.contaAtivada = 'Sim'";

        if ($imap) {
            $sql .= ' AND a.contaImapHost IS NOT NULL AND a.contaImapPorta IS NOT NULL';
        }

        if ($lead) {
            $sql .= " AND a.contaLead = 'Sim'";
        }

        $query  = $this->getEm()->createQuery($sql);
        $result = $query->setFirstResult(0)->getResult();

        return $result;
    }
}
?>