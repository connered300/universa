<?php

namespace Organizacao\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class OrgIes
 * @package Organizacao\Service
 */
class OrgIes extends AbstractService
{
    const DIRETORIO_IMAGEM = 8;

    private $lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Organizacao\Entity\OrgIes');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function pesquisaForJson($params)
    {
        $sql = 'SELECT * FROM org_ies ies WHERE ';

        $IesNome = false;
        $iesId   = false;

        if ($params['q']) {
            $IesNome = $params['q'];
        } elseif ($params['query']) {
            $IesNome = $params['query'];
        }

        if ($params['iesId']) {
            $iesId = $params['iesId'];
        }

        $parameters = array('iesNome' => "{$IesNome}%");
        $sql .= '( ies_nome LIKE :iesNome OR ies_sigla LIKE :iesNome)';

        if ($iesId) {
            $parameters['ies_id'] = $iesId;
            $sql .= ' AND ies_id <> :ies_id';
        }

        $sql .= " ORDER BY ies_id";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function salvar(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $servicePessoaJuridica    = new \Pessoa\Service\PessoaJuridica($this->getEm());
        $serviceOrgIes            = new \Organizacao\Service\OrgIes($this->getEm());
        $servicePessoaFisica      = new \Pessoa\Service\PessoaFisica($this->getEm());
        $serviceArquivoDiretorios = new \GerenciadorArquivos\Service\ArquivoDiretorios($this->getEm());
        $serviceArquivo           = new \GerenciadorArquivos\Service\Arquivo($this->getEm());

        $objDiretorio = $serviceArquivoDiretorios->getRepository()->find(self::DIRETORIO_IMAGEM);
        $serviceArquivo->setDiretorio($objDiretorio);

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['iesId']) {
                $objOrgIes = $this->getRepository()->find($arrDados['iesId']);

                if (!$objOrgIes) {
                    $this->setLastError('Registro de ies não existe!');

                    return false;
                }
            } else {
                $objOrgIes = new \Organizacao\Entity\OrgIes();
            }

            if ($arrDados['pesMantenedora']) {
                $objPessoaJuridica = $servicePessoaJuridica->getRepository()->find($arrDados['pesMantenedora']);

                if (!$objPessoaJuridica) {
                    $this->setLastError('Empresa selecionada como mantenedora não existe!');

                    return false;
                }

                $objOrgIes->setPesMantenedora($objPessoaJuridica);
            }

            if ($arrDados['iesSede']) {
                $objOrgIesSede = $serviceOrgIes->getRepository()->find($arrDados['iesSede']);

                if (!$objOrgIesSede) {
                    $this->setLastError('Registro de instituição não existe!');

                    return false;
                }

                $objOrgIes->setIesSede($objOrgIesSede);
            }

            if ($arrDados['diretoriaPes']) {
                $objPessoaFisica = $servicePessoaFisica->getRepository()->find($arrDados['diretoriaPes']);

                if (!$objPessoaFisica) {
                    $this->setLastError('Pessoa selecionada para diretoria não existe!');

                    return false;
                }

                $objOrgIes->setDiretoriaPes($objPessoaFisica);
            }

            if ($arrDados['secretariaPes']) {
                $objPessoaFisica = $servicePessoaFisica->getRepository()->find($arrDados['secretariaPes']);

                if (!$objPessoaFisica) {
                    $this->setLastError('Pessoa selecionada para secretaria não existe!');

                    return false;
                }

                $objOrgIes->setSecretariaPes($objPessoaFisica);
            }

            $imagem            = $arrDados['vfile'][$arrDados['arq']];
            $imagemCarteirinha = $arrDados['vfile'][$arrDados['arqCarteirinha']];

            $objArq = null;

            if ($imagem) {
                if (empty($imagem['raw']) && $objOrgIes->getArq()) {
                    /* @var $objArq \GerenciadorArquivos\Entity\Arquivo */
                    $objArq = $objOrgIes->getArq();

                    if (!empty($objArq)) {
                        $objArqAntigo = $objArq->getArqId();
                    }

                    $objArq = null;
                } else {
                    $arrDados['arq'] = ($imagem['raw']) ? $imagem['raw'] : null;

                    if ($arrDados['arq'] && is_string($arrDados['arq'])) {
                        $objArq = $serviceArquivo->getRepository()->find($arrDados['arq']);
                    }
                }

                if ($imagem['file']['error'] == 0 && $imagem['file']['size'] != 0) {
                    try {
                        $objArq = $serviceArquivo->adicionar($imagem['file']);
                    } catch (\Exception $ex) {
                        throw new \Exception('Falha ao registrar imagem!');
                    }
                }
            } else {
                $objArq = null;
            }

            $arrDados['arq'] = $objArq;

            $objArqCarteirinha = null;

            if ($imagemCarteirinha) {
                if (empty($imagemCarteirinha['raw']) && $objOrgIes->getArqCarteirinha()) {
                    /* @var $objArqCarteirinha \GerenciadorArquivos\Entity\Arquivo */
                    $objArqCarteirinha = $objOrgIes->getArqCarteirinha();

                    if (!empty($objArqCarteirinha)) {
                        try {
                            $serviceArquivo->excluir($objArqCarteirinha);
                        } catch (\Exception $ex) {
                            throw new \Exception('Falha ao remover imagem antiga!');
                        }
                    }

                    $objArqCarteirinha = null;
                } else {
                    $arrDados['arqCarteirinha'] = ($imagemCarteirinha['raw']) ? $imagemCarteirinha['raw'] : null;

                    if ($arrDados['arqCarteirinha'] && is_string($arrDados['arqCarteirinha'])) {
                        $objArqCarteirinha = $serviceArquivo->getRepository()->find($arrDados['arqCarteirinha']);
                    }
                }

                if ($imagemCarteirinha['file']['error'] == 0 || $imagemCarteirinha['file']['size'] != 0) {
                    try {
                        $objArqCarteirinha = $serviceArquivo->adicionar($imagemCarteirinha['file']);
                    } catch (\Exception $ex) {
                        throw new \Exception('Falha ao registrar imagem!');
                    }
                }
            } else {
                $objArqCarteirinha = null;
            }

            $objOrgIes->setArq($objArq);
            $objOrgIes->setArqCarteirinha($objArqCarteirinha);
            $objOrgIes->setIesNome($arrDados['iesNome']);
            $objOrgIes->setIesCredenciamento($arrDados['iesCredenciamento']);
            $objOrgIes->setIesSigla($arrDados['iesSigla']);
            $objOrgIes->setIesEndNumero($arrDados['iesEndNumero']);
            $objOrgIes->setIesEndCep($arrDados['iesEndCep']);
            $objOrgIes->setIesEndBairro($arrDados['iesEndBairro']);
            $objOrgIes->setIesEndCidade($arrDados['iesEndCidade']);
            $objOrgIes->setIesEndEstado($arrDados['iesEndEstado']);
            $objOrgIes->setIesEndLogradouro($arrDados['iesEndLogradouro']);
            $objOrgIes->setIesTelefone($arrDados['iesTelefone']);
            $objOrgIes->setIesEmail($arrDados['iesEmail']);
            $objOrgIes->setIesNumeroMec($arrDados['iesNumeroMec']);
            $objOrgIes->setIesOrganizacaoAcademica($arrDados['iesOrganizacaoAcademica']);

            $this->getEm()->persist($objOrgIes);
            $this->getEm()->flush();

            if ($objArqAntigo) {
                try {
                    $serviceArquivo->excluir($objArqAntigo);
                } catch (\Exception $ex) {
                    throw new \Exception('Falha ao remover imagem antiga!');
                }
            }

            $this->getEm()->commit();

            $arrDados['iesId'] = $objOrgIes->getIesId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de instituição!' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['iesNome']) {
            $errors[] = 'Por favor preencha o campo "nome"!';
        }

        if (!$arrParam['pesMantenedora']) {
            $errors[] = 'Por favor selecione a mantenedora!';
        }

        if (!$arrParam['iesNumeroMec']) {
            $errors[] = 'Por favor preencha o campo "número MEC"!';
        }

        if (!$arrParam['iesOrganizacaoAcademica']) {
            $errors[] = 'Por favor preencha o campo "organização acadêmica"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "
        SELECT
            ies.*,
            coalesce(pjm.pes_nome_fantasia, pm.pes_nome) AS mantenedora,
            pd.pes_nome AS diretoria,
            ps.pes_nome AS secretaria
        FROM org_ies ies
        LEFT JOIN pessoa_juridica pjm ON pjm.pes_id=ies.pes_id_mantenedora
        LEFT JOIN pessoa pm ON pm.pes_id=pjm.pes_id
        LEFT JOIN pessoa pd ON pd.pes_id=ies.diretoria_pes_id
        LEFT JOIN pessoa ps ON ps.pes_id=ies.secretaria_pes_id";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $iesId
     * @return array
     */
    public function getArray($iesId)
    {
        /* @var $objOrgIes \Organizacao\Entity\OrgIes */
        $arrDados       = array();
        $objOrgIes      = $this->getRepository()->find($iesId);
        $serviceArquivo = new \GerenciadorArquivos\Service\Arquivo($this->getEm());

        try {
            $objPesMantenedora = $objOrgIes->getPesMantenedora();
            $objSecretariaPes  = $objOrgIes->getSecretariaPes();
            $objDiretoriaPes   = $objOrgIes->getDiretoriaPes();

            $arrDados = $objOrgIes->toArray();

            $arq    = $arrDados['arq'];
            $arrArq = null;

            if ($arq) {
                if (is_object($arq)) {
                    $objArquivo = $arq;
                } elseif (is_array($arq)) {
                    $objArquivo = $serviceArquivo->getRepository()->find($arq['id']);
                } else {
                    $objArquivo = $serviceArquivo->getRepository()->find($arq);
                }

                $arrArq = array(
                    'id'      => $objArquivo->getArqId(),
                    'nome'    => $objArquivo->getArqNome(),
                    'tipo'    => $objArquivo->getArqTipo(),
                    'tamanho' => $objArquivo->getArqTamanho(),
                    'chave'   => $objArquivo->getArqChave(),
                );
            }

            /** @var \GerenciadorArquivos\Entity\Arquivo $objCarteirinha */
            if ($objCarteirinha = $arrDados['arqCarteirinha']) {
                unset($arrDados['arqCarteirinha']);
                $arrDados['arqCarteirinha']['id']      = $objCarteirinha->getArqId();
                $arrDados['arqCarteirinha']['nome']    = $objCarteirinha->getArqNome();
                $arrDados['arqCarteirinha']['tipo']    = $objCarteirinha->getArqTipo();
                $arrDados['arqCarteirinha']['tamanho'] = $objCarteirinha->getArqTamanho();
                $arrDados['arqCarteirinha']['chave']   = $objCarteirinha->getArqChave();
            }

            $arrDados['arq']            = $arrArq;
            $arrDados['pesMantenedora'] = $objPesMantenedora ? $objPesMantenedora->toArray() : null;
            $arrDados['secretariaPes']  = $objSecretariaPes ? $objSecretariaPes->toArray() : null;
            $arrDados['diretoriaPes']   = $objDiretoriaPes ? $objDiretoriaPes->toArray() : null;
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('iesId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Organizacao\Entity\OrgIes */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getIesId(),
                $params['value'] => $objEntity->getIesNome()
            );
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['iesId']) {
            $this->setLastError('Para remover um registro de instituição é necessário especificar o código.');

            return false;
        }

        try {
            $objOrgIes = $this->getRepository()->find($param['iesId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objOrgIes);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de instituição.');

            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public function findIes()
    {
        $query  = "SELECT ies_id, ies_nome FROM org_ies AS ies";
        $ies    = $this->executeQuery($query);
        $option = array();

        foreach ($ies as $ie) {
            $option = [
                $ie['ies_id'] => $ie['ies_nome'],
            ];
        }

        return $option;
    }

    /**
     * Retorna informações sobre a IES e a mantenedora
     *
     * @param \Organizacao\Entity\OrgIes|null $objIes
     * @return array
     */
    public function retornaDadosInstituicao(\Organizacao\Entity\OrgIes $objIes = null)
    {
        if (!$objIes) {
            /* @var $objIes \Organizacao\Entity\OrgIes */
            $objIes = $this->getRepository()->findOneBy([], ['iesId' => 'asc']);
        }

        if (!$objIes) {
            return array();
        }

        $objPessoaJuridicaMantenedora = $objIes->getPesMantenedora();

        $logo = '';

        if ($objIes->getArq()) {
            $logo = getcwd();
            $logo .= DIRECTORY_SEPARATOR;
            $logo .= $objIes->getArq()->getDiretorio()->getArqDiretorioEndereco();
            $logo .= DIRECTORY_SEPARATOR;
            $logo .= $objIes->getArq()->getArqChave();
            $logo = realpath($logo);
        }

        if (!$logo) {
            $logo = getcwd() . DIRECTORY_SEPARATOR .
                'public' . DIRECTORY_SEPARATOR .
                'img' . DIRECTORY_SEPARATOR .
                'logo.png';
        }

        $secretaria = '';
        $diretoria  = '';

        try {
            $secretaria = $objIes->getSecretariaPes() ? $objIes->getSecretariaPes()->getPes()->getPesNome() : '';
        } catch (\Exception $e) {
        }

        try {
            $diretoria = $objIes->getDiretoriaPes() ? $objIes->getDiretoriaPes()->getPes()->getPesNome() : '';
        } catch (\Exception $e) {
        }

        $arrDados = $objIes->toArray();

        $arrPessoaJuridicaMantenedora = array();

        try {
            if ($objPessoaJuridicaMantenedora) {
                $arrPessoaJuridicaMantenedora = $objPessoaJuridicaMantenedora->toArray();
            }
        } catch (\Exception $e) {
            $arrPessoaJuridicaMantenedora = array();
        }

        $arrDados = array_merge(
            array_filter($arrPessoaJuridicaMantenedora),
            array_filter($arrDados),
            array_filter(
                [
                    'logo'                    => $logo,
                    'iesId'                   => $objIes->getIesId(),
                    'ies'                     => $objIes->getIesNome(),
                    'iesRazaoSocial'          => $objIes->getIesNome(),
                    'iesFantasia'             => $objIes->getIesSigla(),
                    'iesSigla'                => $objIes->getIesSigla(),
                    'iesNumeroMec'            => $objIes->getIesNumeroMec(),
                    'iesCredenciamento'       => $objIes->getIesCredenciamento(),
                    'iesOrganizacaoAcademica' => $objIes->getIesOrganizacaoAcademica(),
                    'secretaria'              => $secretaria,
                    'diretoria'               => $diretoria,
                    'cnpj'                    => $arrPessoaJuridicaMantenedora['pesCnpj'],
                    'inscricaoEstadual'       => $arrPessoaJuridicaMantenedora['pesInscEstadual'],
                    'inscricaoMunicipal'      => $arrPessoaJuridicaMantenedora['pesInscMunicipal'],
                    'mantenedora'             => $arrPessoaJuridicaMantenedora['pesNome'],
                ]
            )
        );

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceArquivo = new \GerenciadorArquivos\Service\Arquivo($this->getEm());

        $arq    = $arrDados['arq'];
        $arrArq = null;

        if ($arq) {
            if (is_object($arq)) {
                $objArquivo = $arq;
            } elseif (is_array($arq)) {
                $objArquivo = $serviceArquivo->getRepository()->find($arq['id']);
            } else {
                $objArquivo = $serviceArquivo->getRepository()->find($arq);
            }

            if ($objArquivo) {
                $arrArq = array(
                    'id'      => $objArquivo->getArqId(),
                    'nome'    => $objArquivo->getArqNome(),
                    'tipo'    => $objArquivo->getArqTipo(),
                    'tamanho' => $objArquivo->getArqTamanho(),
                    'chave'   => $objArquivo->getArqChave(),
                );
            }
        }

        $arrDados['arq'] = $arrArq;

        if ($arrDados['pesMantenedora']) {
            $arrDados['pesMantenedora']['id']   = $arrDados['pesMantenedora']['pesId'];
            $arrDados['pesMantenedora']['text'] = $arrDados['pesMantenedora']['pesNome'];
        }

        if ($arrDados['secretariaPes']) {
            $arrDados['secretariaPes']['id']   = $arrDados['secretariaPes']['pesId'];
            $arrDados['secretariaPes']['text'] = $arrDados['secretariaPes']['pesNome'];
        }

        if ($arrDados['diretoriaPes']) {
            $arrDados['diretoriaPes']['id']   = $arrDados['diretoriaPes']['pesId'];
            $arrDados['diretoriaPes']['text'] = $arrDados['diretoriaPes']['pesNome'];
        }

        return $arrDados;
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceCidade = new \Pessoa\Service\Cidade($this->getEm());

        $arrEstados = $serviceCidade->getEstadoArrSelect2();

        $view->setVariable("arrEstados", $arrEstados);
    }

}