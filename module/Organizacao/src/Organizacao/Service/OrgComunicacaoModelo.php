<?php

namespace Organizacao\Service;

use Doctrine\Common\Collections\ArrayCollection;
use VersaSpine\Service\AbstractService;

/**
 * Class OrgComunicacaoModelo
 * @package Organizacao\Service
 */
class OrgComunicacaoModelo extends AbstractService
{
    const MODELO_TIPO_EMAIL = 'Email';
    const MODELO_TIPO_SMS   = 'SMS';

    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Organizacao\Entity\OrgComunicacaoModelo');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function pesquisaForJson($params)
    {
        $service = new \Acesso\Service\AcessoGrupo($this->getEm());

        $sql        = '
        SELECT
            modelo_id, tipo_id, usuario, modelo_nome, modelo_tipo, tipo_descricao, tipo_contexto
        FROM org__comunicacao_modelo
        LEFT JOIN org__comunicacao_tipo USING (tipo_id)
        LEFT JOIN org__comunicacao_modelo_grupo_permissao USING (modelo_id)
        LEFT JOIN acesso_grupo
            ON acesso_grupo.id = org__comunicacao_modelo_grupo_permissao.grupo_id
        WHERE
            modelo_nome LIKE :modelo_nome';
        $modeloNome = false;
        $modeloId   = false;

        if ($params['q']) {
            $modeloNome = $params['q'];
        } elseif ($params['query']) {
            $modeloNome = $params['query'];
        }

        if ($params['modeloId']) {
            $modeloId = $params['modeloId'];
        }

        $grupos = $service->buscaGrupoUsr();

        $parameters = array('modelo_nome' => "{$modeloNome}%");

        if ($modeloId) {
            $parameters['modelo_id'] = $modeloId;
            $sql .= ' AND org__comunicacao_modelo.modelo_id <> :modelo_id';
        }

        if ($grupos) {
            $sql .= ' AND acesso_grupo.id in (' . implode(',', $grupos) . ')';
        }

        if ($params['tipoContexto']) {
            $arrTipoContexto = $params['tipoContexto'];
            $arrTipoContexto = !is_array($arrTipoContexto) ? explode(',', $arrTipoContexto) : $arrTipoContexto;

            $sqlContextoAdd = array();

            foreach ($arrTipoContexto as $c => $tipoContexto) {
                $parameters['tipoContexto' . $c] = $tipoContexto;
                $sqlContextoAdd[]                = '(tipo_contexto = :tipoContexto' . $c . ')';
            }

            $sql .= ' AND (' . implode(' OR ', $sqlContextoAdd) . ')';
        }

        $sql .= " GROUP BY org__comunicacao_modelo.modelo_id";
        $sql .= " ORDER BY modelo_nome";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        $arrDados['modeloAssunto']  = str_replace(
            array('&quot;', '&#39;'),
            array('"', "'"),
            $arrDados['modeloAssunto']
        );
        $arrDados['modeloConteudo'] = str_replace(
            array('&quot;', '&#39;'),
            array('"', "'"),
            $arrDados['modeloConteudo']
        );

        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceOrgComunicacaoTipo = new \Organizacao\Service\OrgComunicacaoTipo($this->getEm());
        $serviceAcessoPessoas      = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceAcessoGrupo        = new \Acesso\Service\AcessoGrupo($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['modeloId']) {
                /** @var $objOrgComunicacaoModelo \Organizacao\Entity\OrgComunicacaoModelo */
                $objOrgComunicacaoModelo = $this->getRepository()->find($arrDados['modeloId']);

                if (!$objOrgComunicacaoModelo) {
                    $this->setLastError('Registro de modelo de comunicação não existe!');

                    return false;
                }
            } else {
                $objOrgComunicacaoModelo = new \Organizacao\Entity\OrgComunicacaoModelo();
            }

            if ($arrDados['grupoId']) {
                $arrCollectionGrupo = new ArrayCollection(
                    $serviceAcessoGrupo->getArrayAcessoGrupoPeloId($arrDados['grupoId'])
                );

                $objOrgComunicacaoModelo->setGrupo($arrCollectionGrupo);
            }

            if ($arrDados['tipo']) {
                /** @var $objOrgComunicacaoTipo \Organizacao\Entity\OrgComunicacaoTipo */
                $objOrgComunicacaoTipo = $serviceOrgComunicacaoTipo->getRepository()->find($arrDados['tipo']);

                if (!$objOrgComunicacaoTipo) {
                    $this->setLastError('Registro de tipo de comunicação não existe!');

                    return false;
                }

                $objOrgComunicacaoModelo->setTipo($objOrgComunicacaoTipo);
            } else {
                $objOrgComunicacaoModelo->setTipo(null);
            }

            $objAcessoPessoas = $objOrgComunicacaoModelo->getUsuario();

            if (!$objAcessoPessoas) {
                $usuarioId = $arrDados['usuario'];

                if (!$usuarioId) {
                    $session    = new \Zend\Authentication\Storage\Session();
                    $arrUsuario = $session->read();

                    if (!$arrUsuario) {
                        $this->setLastError('Verifique se você está autenticado no sistema!');

                        return false;
                    }

                    $usuarioId = $arrUsuario['id'];
                }

                /* @var $objAcessoPessoas \Acesso\Entity\AcessoPessoas */
                $objAcessoPessoas = $serviceAcessoPessoas->getRepository()->find($usuarioId);

                if (!$objAcessoPessoas) {
                    $this->setLastError('Verifique se você está autenticado no sistema!');

                    return false;
                }

                $objOrgComunicacaoModelo->setUsuario($objAcessoPessoas);
            }

            $objOrgComunicacaoModelo->setModeloNome($arrDados['modeloNome']);
            $objOrgComunicacaoModelo->setModeloTipo($arrDados['modeloTipo']);
            $objOrgComunicacaoModelo->setModeloAssunto($arrDados['modeloAssunto']);
            $objOrgComunicacaoModelo->setModeloConteudo($arrDados['modeloConteudo']);
            $objOrgComunicacaoModelo->setModeloDataInicio($arrDados['modeloDataInicio']);
            $objOrgComunicacaoModelo->setModeloDataFim($arrDados['modeloDataFim']);

            $this->getEm()->persist($objOrgComunicacaoModelo);
            $this->getEm()->flush($objOrgComunicacaoModelo);

            $this->getEm()->commit();

            $arrDados['modeloId'] = $objOrgComunicacaoModelo->getModeloId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de comunicação modelo!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['tipo']) {
            $errors[] = 'Por favor preencha o campo "código tipo"!';
        }

        if (!$arrParam['modeloNome']) {
            $errors[] = 'Por favor preencha o campo "nome"!';
        }

        if (!$arrParam['grupoId']) {
            $errors[] = 'Por favor preencha o campo "Grupos"!';
        }

        if (!in_array($arrParam['modeloTipo'], self::getModeloTipo())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "tipo"!';
        }

        if (!$arrParam['modeloAssunto']) {
            $errors[] = 'Por favor preencha o campo "assunto"!';
        }

        if (!$arrParam['modeloConteudo']) {
            $errors[] = 'Por favor preencha o campo "conteúdo"!';
        } else {
            try {
                $loader    = new \Twig_Loader_Filesystem(array());
                $twig      = new \Twig_Environment($loader, array());
                $tplModelo = $twig->createTemplate($arrParam['modeloConteudo']);
                $tplModelo->render(array());
            } catch (\Exception $e) {
                $errors[] = 'Existe um ou mais erros no conteúdo do modelo!';
            }
        }

        if (!$arrParam['modeloDataInicio']) {
            $errors[] = 'Por favor preencha o campo "data de início"!';
        }

        if (!$arrParam['modeloDataFim']) {
            $errors[] = 'Por favor preencha o campo "data de término"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public static function getModeloTipo()
    {
        return array(self::MODELO_TIPO_EMAIL, self::MODELO_TIPO_SMS);
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "
        SELECT
            modelo_id, usuario, modelo_nome, modelo_tipo, modelo_assunto, modelo_conteudo,
            date_format(modelo_data_inicio, '%d/%m/%Y') AS modelo_data_inicio,
            date_format(modelo_data_fim, '%d/%m/%Y') AS modelo_data_fim,
            tipo_id, tipo_descricao
        FROM org__comunicacao_modelo
        INNER JOIN org__comunicacao_tipo USING (tipo_id)
        ";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $modeloId
     * @return array
     */
    public function getArray($modeloId)
    {
        /** @var $objOrgComunicacaoModelo \Organizacao\Entity\OrgComunicacaoModelo */
        $objOrgComunicacaoModelo   = $this->getRepository()->find($modeloId);
        $serviceOrgComunicacaoTipo = new \Organizacao\Service\OrgComunicacaoTipo($this->getEm());

        try {
            $arrDados = $objOrgComunicacaoModelo->toArray();

            if ($arrDados['tipo']) {
                /** @var $objOrgComunicacaoTipo \Organizacao\Entity\OrgComunicacaoTipo */
                $objOrgComunicacaoTipo = $serviceOrgComunicacaoTipo->getRepository()->find($arrDados['tipo']);

                if ($objOrgComunicacaoTipo) {
                    $arrDados['tipo'] = array(
                        'id'   => $objOrgComunicacaoTipo->getTipoId(),
                        'text' => $objOrgComunicacaoTipo->getTipoDescricao()
                    );
                } else {
                    $arrDados['tipo'] = null;
                }
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceOrgComunicacaoTipo = new \Organizacao\Service\OrgComunicacaoTipo($this->getEm());

        if (is_array($arrDados['tipo']) && !$arrDados['tipo']['id']) {
            $arrDados['tipo']['id']   = $arrDados['tipo']['tipoId'];
            $arrDados['tipo']['text'] = $arrDados['tipo']['tipoDescricao'];
        } elseif ($arrDados['tipo'] && is_string($arrDados['tipo'])) {
            $arrDados['tipo'] = $serviceOrgComunicacaoTipo->getArrSelect2(
                array('id' => $arrDados['tipo'])
            );
        }

        $arrDados['arrGrupo'] = $this->getArrSelect2ComunicacaoGrupo($arrDados);

        return $arrDados;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('modeloId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Organizacao\Entity\OrgComunicacaoModelo */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getModeloId(),
                $params['value'] => $objEntity->getModeloNome()
            );
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['modeloId']) {
            $this->setLastError('Para remover um registro de modelo de comunicação é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objOrgComunicacaoModelo \Organizacao\Entity\OrgComunicacaoModelo */
            $objOrgComunicacaoModelo = $this->getRepository()->find($param['modeloId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objOrgComunicacaoModelo);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de modelo de comunicação.');

            return false;
        }

        return true;
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceOrgComunicacaoTipo = new \Organizacao\Service\OrgComunicacaoTipo($this->getEm());

        $serviceOrgComunicacaoTipo->setarDependenciasView($view);

        $view->setVariable("arrModeloTipo", $this->getArrSelect2ModeloTipo());
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2ModeloTipo($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getModeloTipo());
    }

    /**
     * @param       $tipoComunicacao
     * @param       $tipoModelo
     * @param array $arrVariaveis
     * @return bool
     * @throws \Exception
     */
    public function registrarComunicacao($tipoComunicacao, $tipoModelo, $arrVariaveis = array())
    {
        if (!$arrVariaveis['nome'] && $arrVariaveis['pesNome']) {
            $arrVariaveis['nome'] = $arrVariaveis['pesNome'];
        }

        if (!$arrVariaveis['email'] && $arrVariaveis['conContatoEmail']) {
            $arrVariaveis['email'] = $arrVariaveis['conContatoEmail'];
        }

        if (!$arrVariaveis['telefone'] && $arrVariaveis['conContatoTelefone']) {
            $arrVariaveis['telefone'] = $arrVariaveis['conContatoTelefone'];
        }

        if (!$arrVariaveis['celular'] && $arrVariaveis['conContatoCelular']) {
            $arrVariaveis['celular'] = $arrVariaveis['conContatoCelular'];
        }

        $arrModelo = $this->retornaModelo($tipoComunicacao, $tipoModelo);

        $arrDadosComunicacao = $this->renderizarModelo($arrModelo, $arrVariaveis);

        if ($tipoModelo == \Organizacao\Service\OrgComunicacaoModelo::MODELO_TIPO_EMAIL) {
            $serviceSisFilaEmail = new \Sistema\Service\SisFilaEmail($this->getEm());
            $serviceOrgEmailTipo = new \Organizacao\Service\OrgEmailTipo($this->getEm());

            $arrDadosEmail = array(
                'filaAssunto'           => $arrDadosComunicacao['modeloAssunto'],
                'filaDestinatarioNome'  => $arrVariaveis['nome'],
                'filaDestinatarioEmail' => $arrVariaveis['email'],
                'conta'                 => $serviceOrgEmailTipo->pesquisaContaPorTipo($tipoComunicacao),
                'tipoId'                => $tipoComunicacao,
                'filaConteudo'          => $arrDadosComunicacao['modeloConteudo'],
            );

            if (!$serviceSisFilaEmail->save($arrDadosEmail)) {
                $this->setLastError($serviceSisFilaEmail->getLastError());

                return false;
            }

            if ($arrVariaveis['destinatarioCopia'] && $arrVariaveis['destinatarioCopia']['email']) {
                $arrDadosEmailCopia                          = $arrDadosEmail;
                $arrDadosEmailCopia['filaDestinatarioNome']  = $arrVariaveis['destinatarioCopia']['nome'];
                $arrDadosEmailCopia['filaDestinatarioEmail'] = $arrVariaveis['destinatarioCopia']['email'];

                if ($arrDadosEmailCopia['filaId']) {
                    unset($arrDadosEmailCopia['filaId']);
                }

                if (!$serviceSisFilaEmail->save($arrDadosEmailCopia)) {
                    $this->setLastError($serviceSisFilaEmail->getLastError());

                    return false;
                }
            }

            return true;
        } elseif ($tipoModelo == \Organizacao\Service\OrgComunicacaoModelo::MODELO_TIPO_SMS) {
            $arrSMS = array($arrDadosComunicacao['modeloConteudo']);

            if (strlen($arrDadosComunicacao['modeloConteudo']) > 160) {
                $arrSMS = $this->separarStringParaSMS($arrDadosComunicacao['modeloConteudo'], 140);

                foreach ($arrSMS as $pos => $sms) {
                    $arrSMS[$pos] = '[Parte ' . ($pos + 1) . ' de ' . (count($arrSMS)) . ']' . $sms;
                }
            }

            $serviceSisFilaSms = new \Sistema\Service\SisFilaSms($this->getEm());

            foreach ($arrSMS as $sms) {
                $arrDadosSMS = array(
                    'filaDestinatario' => $arrVariaveis['celular'],
                    'filaConteudo'     => $sms,
                    'tipoId'           => $tipoComunicacao,
                );

                if (!$serviceSisFilaSms->save($arrDadosSMS)) {
                    $this->setLastError($serviceSisFilaSms->getLastError());

                    return false;
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Separa um texto em várias linhas baseado em um tamanho
     * http://stackoverflow.com/a/31604637
     * @param     $bigStr
     * @param int $maxLineLength
     */
    private function separarStringParaSMS($bigStr, $maxLineLength = 160)
    {
        $arrayOutput = array();
        $arrayWords  = explode(' ', $bigStr);

        // Auxiliar counters, foreach will use them
        $currentLength = 0;
        $index         = 0;

        foreach ($arrayWords as $word) {
            // +1 because the word will receive back the space in the end that it loses in explode()
            $wordLength = strlen($word) + 1;

            if (($currentLength + $wordLength) <= $maxLineLength) {
                $arrayOutput[$index] .= $word . ' ';

                $currentLength += $wordLength;
            } else {
                $index += 1;

                $currentLength = $wordLength;

                $arrayOutput[$index] = $word;
            }
        }

        return $arrayOutput;
    }

    /**
     * @param $tipoComunicacao
     * @param $modeloTipo
     * @param $arrVariavies
     * @return array
     * @throws \Exception
     */
    private function renderizarModelo($arrModelo, $arrVariavies)
    {
        $loader     = new \Twig_Loader_Filesystem(array());
        $twig       = new \Twig_Environment($loader, array());
        $tplAssunto = $twig->createTemplate($arrModelo['modeloAssunto']);
        $tplModelo  = $twig->createTemplate($arrModelo['modeloConteudo']);

        try {
            $arrRetorno = array(
                'modeloAssunto'  => $tplAssunto->render($arrVariavies),
                'modeloConteudo' => $tplModelo->render($arrVariavies)
            );
        } catch (\Exception $e) {
            throw new \Exception('Falha ao renderizar modelo!');
        }

        return $arrRetorno;
    }

    /**
     * @param $tipoComunicacao
     * @param $modeloTipo
     * @return array
     * @throws \Exception
     */
    public function retornaModelo($tipoComunicacao, $modeloTipo)
    {
        $serviceOrgComunicacaoTipo = new \Organizacao\Service\OrgComunicacaoTipo($this->getEm());

        if (!in_array($tipoComunicacao, $serviceOrgComunicacaoTipo->getComunicacaoTipo())) {
            throw new \Exception('Tipo de comunicação não foi registrada no serviço!');
        }

        if (!in_array($modeloTipo, $this->getModeloTipo())) {
            throw new \Exception('Tipo de modelo inválido!');
        }

        $query = 'SELECT m FROM \Organizacao\Entity\OrgComunicacaoModelo m
        WHERE m.tipo = :tipo AND
              m.modeloTipo = :modeloTipo AND
              (:dataAtual >= m.modeloDataInicio AND :dataAtual <= m.modeloDataFim)
        ORDER BY m.modeloDataInicio ASC';

        $arrObjOrgComunicacaoModelo = $this
            ->getEm()
            ->createQuery($query)
            ->setParameter('tipo', $tipoComunicacao)
            ->setParameter('modeloTipo', $modeloTipo)
            ->setParameter('dataAtual', new \DateTime())
            ->setMaxResults(1)
            ->getResult();

        if ($arrObjOrgComunicacaoModelo) {
            /** @var $objOrgComunicacaoModelo \Organizacao\Entity\OrgComunicacaoModelo */
            $objOrgComunicacaoModelo = $arrObjOrgComunicacaoModelo[0];

            $assunto = $objOrgComunicacaoModelo->getModeloAssunto();
            $modelo  = $objOrgComunicacaoModelo->getModeloConteudo();
        } else {
            $arrModelo = $this->modeloPadraoComunicacaoTipo($tipoComunicacao, $modeloTipo);

            if (!$arrModelo) {
                throw new \Exception('Modelo não encontrado!');
            }

            $assunto = $arrModelo['modeloAssunto'];
            $modelo  = $arrModelo['modeloConteudo'];

            if ($modeloTipo == self::MODELO_TIPO_SMS) {
                $modelo = strip_tags($modelo);
            }

            $modelo = trim($modelo);
        }

        return array('modeloAssunto' => $assunto, 'modeloConteudo' => $modelo);
    }

    /**
     * @param $tipo
     * @return array|bool
     */
    private function modeloPadraoComunicacaoTipo($tipo, $modeloTipo)
    {
        $assunto = '';
        $modelo  = '';

        switch ($tipo) {
            case \Organizacao\Service\OrgComunicacaoTipo::TIPO_ACESSO_CADASTRO:
                $assunto = 'Dados de acesso';
                $modelo  = 'acesso-cadastro';
                break;
            case \Organizacao\Service\OrgComunicacaoTipo::TIPO_ACESSO_RECUPERAR_SENHA:
                $assunto = 'Novos dados de acesso';
                $modelo  = 'acesso-recuperar-senha';
                break;
            case \Organizacao\Service\OrgComunicacaoTipo::TIPO_VESTIBULAR_CADASTRO:
                $assunto = 'Informações sobre sua inscrição';
                $modelo  = 'vestibular-cadastro';
                break;
            case \Organizacao\Service\OrgComunicacaoTipo::TIPO_BOAS_VINDAS_ALUNO:
                $assunto = 'Bem-vindo!';
                $modelo  = 'boas-vindas-aluno';
                break;
            case \Organizacao\Service\OrgComunicacaoTipo::TIPO_BOAS_VINDAS_AGENTE:
                $assunto = 'Bem-vindo!';
                $modelo  = 'boas-vindas-agente';
                break;
            case \Organizacao\Service\OrgComunicacaoTipo::TIPO_ENVIO_BOLETO:
                $assunto = 'Títulos financeiros';
                $modelo  = 'boleto-envio';
                break;
        }

        if ($modelo) {
            $fileDir = __DIR__ . '/../../../view/organizacao/org-comunicacao-modelo/default/';
            $file    = $fileDir . $modelo . '-' . strtolower($modeloTipo) . '.twig';

            if ($conteudo = file_get_contents($file)) {
                return array('modeloAssunto' => $assunto, 'modeloConteudo' => $conteudo);
            }

            $file = $fileDir . $modelo . '.twig';

            if ($conteudo = file_get_contents($file)) {
                return array('modeloAssunto' => $assunto, 'modeloConteudo' => $conteudo);
            }
        }

        return false;
    }

    public function getArrSelect2ComunicacaoGrupo($params = array())
    {
        $array = array();

        try {

            /** @var $objOrgComunicacaoModelo \Organizacao\Entity\OrgComunicacaoModelo */
            $objOrgComunicacaoModelo = $this->getRepository()->find($params['modeloId']);
            $grupoCollection         = $objOrgComunicacaoModelo->getGrupo();
            $arrGrupo                = $grupoCollection->getValues();

            /** @var \Acesso\Entity\AcessoGrupo $grupo */
            foreach ($arrGrupo as $grupo) {
                $array[] = array('text' => $grupo->getGrupNome(), 'id' => $grupo->getId());
            }
        } catch (\Exception $e) {
        }

        return $array;
    }

    public function trabalharVariaveisPorContexto($arrVariavies, $contexto)
    {
        try {
            $arrDados = [];

            $serviceAlunoCurso          = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
            $serviceAluno               = new \Matricula\Service\AcadgeralAluno($this->getEm());
            $serviceOrgCampus           = new \Organizacao\Service\OrgCampus($this->getEm());
            $servicePessoa              = new \Pessoa\Service\Pessoa($this->getEm());
            $serviceComunicacaoTipo     = new \Organizacao\Service\OrgComunicacaoTipo($this->getEm());
            $serviceAgente              = new \Organizacao\Service\OrgAgenteEducacional($this->getEm());

            switch ($contexto) {
                case $serviceComunicacaoTipo::TIPO_CONTEXTO_ALUNO_CURSO:
                    if (!$arrVariavies['alunocursoId']) {
                        $this->setLastError(
                            'Para enviar uma comunicação para um curso de aluno é necessário informar a matrícula!'
                        );

                        return array();
                    }

                    $arrAlunoCurso          = $serviceAlunoCurso->getAlunocursoArray($arrVariavies['alunocursoId']);
                    $arrDados['alunoCurso'] = $arrAlunoCurso;
                    $arrDados['aluno']      = $arrDados['alunoCurso']['aluno'];
                    $arrDados['pessoa']     = $arrAlunoCurso;
                    $arrDados['campId']     = $arrDados['alunoCurso']['campId'];
                    break;
                case $serviceComunicacaoTipo::TIPO_CONTEXTO_ALUNO :
                    if (!$arrVariavies['alunoId']) {
                        $this->setLastError(
                            'Para enviar uma comunicação para um aluno é necessário informar o código!'
                        );

                        return array();
                    }

                    $arrAluno                  = $serviceAluno->getArray($arrVariavies['alunoId']);
                    $arrDados['aluno']         = $arrAluno;
                    $arrDados['alunoCurso']    = $arrDados['aluno']['alunoCurso'][0];
                    $arrDados['arrAlunoCurso'] = $arrDados['aluno']['alunoCurso'][0];
                    $arrDados['pessoa']        = $arrAluno;
                    break;
                case $serviceComunicacaoTipo::TIPO_CONTEXTO_AGENTE_EDUCACIONAL :
                    if (!$arrVariavies['pesId']) {
                        $this->setLastError(
                            'Para enviar uma comunicação para um agente é necessário informar o código!'
                        );

                        return array();
                    }

                    $arrPessoa          = $serviceAgente->getArray($arrVariavies['pesId']);
                    $arrDados['agente'] = $arrPessoa;
                    $arrDados['pessoa'] = $arrPessoa;
                    break;
                case $serviceComunicacaoTipo::TIPO_CONTEXTO_PESSOA :
                    if (!$arrVariavies['pesId']) {
                        $this->setLastError(
                            'Para enviar uma comunicação para a uma pessoa é necessário informar o código!'
                        );

                        return array();
                    }

                    $arrPessoa          = $servicePessoa->getArray($arrVariavies['pesId']);
                    $arrDados['pessoa'] = $arrPessoa;
                    break;
                default:
                    $this->setLastError('Para enviar uma comunicação é necessário informar o contexto!');

                    return array();
                    break;
            }

            $arrDados['contexto'] = $contexto;
            $arrDados['celular']  = $arrDados['pessoa']['conContatoCelular'];
            $arrDados['email']    = $arrDados['pessoa']['conContatoEmail'];
            $arrDados['telefone'] = $arrDados['pessoa']['conContatoTelefone'];
            $arrDados['nome']     = $arrDados['pessoa']['pesNome'];
            $arrDados['ies']      = $serviceOrgCampus->retornaDadosInstituicao($arrVariavies['campId']);

            return $arrDados;
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());
        }

        return false;
    }

    public function trabalharVariaveisDeComunicacaoPorContexto($arrVariavies)
    {
        if (!$arrVariavies['modeloId']) {
            $this->setLastError(
                'Para enviar uma comunicação é necessário informar o modelo e as variáveis requeridas.'
            );

            return array();
        }

        try {
            /** @var \Organizacao\Entity\OrgComunicacaoModelo $objModelo */
            $objModelo = $this->getRepository()->find($arrVariavies['modeloId']);

            if (!$objModelo) {
                $this->setLastError(
                    'Para enviar uma comunicação é necessário informar o modelo e as variáveis requeridas.'
                );

                return array();
            }

            $objComunicacaoTipo    = $objModelo->getTipo();
            $arrDados              = $this->trabalharVariaveisPorContexto(
                $arrVariavies,
                $objComunicacaoTipo->getTipoContexto()
            );
            $serviceOrgCampus      = new \Organizacao\Service\OrgCampus($this->getEm());
            $arrDados['modelo']    = $objModelo->toArray();
            $arrDados['tipoId']    = $objComunicacaoTipo->getTipoId();
            $arrDados['tipoEnvio'] = $objModelo->getModeloTipo();

            return $arrDados;
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());
        }

        return false;
    }

    public function registrarSmsOuEmailGenerico($arrParam)
    {
        $arrDados = $this->trabalharVariaveisDeComunicacaoPorContexto($arrParam);

        if (!$this->validaDadosEnvio($arrDados)) {
            return false;
        }

        $arrDadosComunicacao = $this->renderizarModelo($arrDados['modelo'], $arrDados);

        $serviceSisFilaEmail = new \Sistema\Service\SisFilaEmail($this->getEm());
        $serviceSisFilaSms   = new \Sistema\Service\SisFilaSms($this->getEm());
        $serviceOrgEmailTipo = new \Organizacao\Service\OrgEmailTipo($this->getEm());

        if ($arrDados['tipoEnvio'] == self::MODELO_TIPO_EMAIL) {
            $arrDadosEmail = array(
                'filaAssunto'           => $arrDadosComunicacao['modeloAssunto'],
                'filaDestinatarioNome'  => $arrDados['nome'],
                'filaDestinatarioEmail' => $arrDados['email'],
                'tipoId'                => $arrDados['tipoId'],
                'conta'                 => $serviceOrgEmailTipo->pesquisaContaPorTipo($arrDados['tipoId']),
                'filaConteudo'          => $arrDadosComunicacao['modeloConteudo'],
            );

            if (!$serviceSisFilaEmail->save($arrDadosEmail)) {
                $this->setLastError($serviceSisFilaEmail->getLastError());

                return false;
            }

            return true;
        } elseif ($arrDados['tipoEnvio'] == self::MODELO_TIPO_SMS) {
            $arrSMS = array($arrDadosComunicacao['modeloConteudo']);

            if (strlen($arrDadosComunicacao['modeloConteudo']) > 160) {
                $arrSMS = $this->separarStringParaSMS($arrDadosComunicacao['modeloConteudo'], 140);

                foreach ($arrSMS as $pos => $sms) {
                    $arrSMS[$pos] = '[Parte ' . ($pos + 1) . ' de ' . (count($arrSMS)) . ']' . $sms;
                }
            }

            foreach ($arrSMS as $sms) {
                $arrDadosSMS = array(
                    'filaDestinatario' => $arrDados['celular'],
                    'filaConteudo'     => $sms,
                    'tipoId'           => $arrDados['tipoId']
                );

                if (!$serviceSisFilaSms->save($arrDadosSMS)) {
                    $this->setLastError($serviceSisFilaSms->getLastError());

                    return false;
                }
            }

            return true;
        }

        return false;
    }

    public function validaDadosEnvio($arrDados)
    {
        $errors = [];

        if (!$arrDados['modelo']) {
            $errors[] = "Modelo de comunicação não encontrado!";
        }

        if ($arrDados['tipoEnvio'] == self::MODELO_TIPO_EMAIL && !$arrDados['email']) {
            $errors[] = "Sem email cadastrado no sistema";
        }

        if ($arrDados['tipoEnvio'] == self::MODELO_TIPO_SMS && !$arrDados['celular']) {
            $errors[] = "Sem celular cadastrado no sistema";
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }
}
?>