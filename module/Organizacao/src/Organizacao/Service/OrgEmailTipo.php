<?php

namespace Organizacao\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class OrgEmailTipo
 * @package Organizacao\Service
 */
class OrgEmailTipo extends AbstractService
{
    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Organizacao\Entity\OrgEmailTipo');
    }

    /**
     * @param $tipoId
     * @return bool|\Organizacao\Entity\OrgEmailConta
     */
    public function pesquisaContaPorTipo($tipoId)
    {
        /** @var $objOrgEmailTipo \Organizacao\Entity\OrgEmailTipo */
        $objOrgEmailTipo = $this->getRepository()->findOneBy(['tipo' => $tipoId]);

        if ($objOrgEmailTipo) {
            return $objOrgEmailTipo->getConta();
        }

        return false;
    }

    /**
     * @param $contaId
     * @return array
     */
    public function retornaOrgComunicacaoTipoIdPeloConta($contaId)
    {
        $arrOrgComunicacaoTipos = array();
        $arrObjOrgEmailTipo     = $this->pesquisaPelaConta($contaId, false);

        /** @var $objOrgEmailTipo \Organizacao\Entity\OrgEmailTipo */
        foreach ($arrObjOrgEmailTipo as $objOrgEmailTipo) {
            $arrOrgComunicacaoTipos[] = $objOrgEmailTipo->getTipo()->getTipoId();
        }

        return $arrOrgComunicacaoTipos;
    }

    /**
     * @param           $contaId
     * @param bool|true $toArray
     * @return array
     */
    public function pesquisaPelaConta($contaId, $toArray = true)
    {
        $arrResult = $this->getRepository()->findBy(array('conta' => $contaId));

        if ($toArray) {
            $arrRetorno = array();

            /** @var $objOrgEmailTipo \Organizacao\Entity\OrgEmailTipo */
            foreach ($arrResult as $objOrgEmailTipo) {
                $arrRetorno[] = $objOrgEmailTipo->toArray();
            }

            return $arrRetorno;
        }

        return $arrResult;
    }

    /**
     * @param                                   $arrOrgComunicacaoTiposConta
     * @param \Organizacao\Entity\OrgEmailConta $objConta
     * @return bool
     */
    public function salvarArray($arrOrgComunicacaoTiposConta, \Organizacao\Entity\OrgEmailConta $objConta)
    {
        $objEntityManager = $this->getEm();
        $arrExcluir       = array();
        $arrEditar        = array();

        if (!is_array($arrOrgComunicacaoTiposConta)) {
            $arrOrgComunicacaoTiposConta = explode(',', $arrOrgComunicacaoTiposConta);
        }

        $arrOrgEmailTiposDB = $this->pesquisaPelaConta($objConta->getContaId(), false);
        /** @var $objOrgEmailTipoDB \Organizacao\Entity\OrgEmailTipo */
        foreach ($arrOrgEmailTiposDB as $objOrgEmailTipoDB) {
            $encontrado = false;
            $tipoId     = $objOrgEmailTipoDB->getTipo()->getTipoId();

            foreach ($arrOrgComunicacaoTiposConta as $tipo) {
                if ($tipo == $tipoId) {
                    $encontrado         = true;
                    $arrEditar[$tipoId] = $objOrgEmailTipoDB;
                    break;
                }
            }

            if (!$encontrado) {
                $arrExcluir[$tipoId] = $objOrgEmailTipoDB;
            }
        }

        if ($arrExcluir) {
            foreach ($arrExcluir as $objReg) {
                $objEntityManager->remove($objReg);
                $objEntityManager->flush();
            }
        }

        foreach ($arrOrgComunicacaoTiposConta as $tipo) {
            $serviceOrgComunicacaoTipo = new \Organizacao\Service\OrgComunicacaoTipo($objEntityManager);
            $objOrgComunicacaoTipo     = $serviceOrgComunicacaoTipo->getRepository()->find($tipo);

            if (!isset($arrEditar[$tipo])) {
                $objOrgEmailTipo = new \Organizacao\Entity\OrgEmailTipo();

                $objOrgEmailTipo->setConta($objConta);
                $objOrgEmailTipo->setTipo($objOrgComunicacaoTipo);

                $objEntityManager->persist($objOrgEmailTipo);
                $objEntityManager->flush($objOrgEmailTipo);
            }
        }

        return true;
    }

    /**
     * @param $contaId
     * @return array
     */
    public function retornaArrayPelaConta($contaId)
    {
        $arrObjOrgEmailTipo = $this->pesquisaPelaConta($contaId, false);
        $arrOrgEmailTipo    = array();

        /** @var $objOrgEmailTipo \Organizacao\Entity\OrgEmailTipo */
        foreach ($arrObjOrgEmailTipo as $objOrgEmailTipo) {
            $arrItem = array(
                'id'   => $objOrgEmailTipo->getTipo()->getTipoId(),
                'text' => $objOrgEmailTipo->getTipo()->getTipoDescricao()
            );

            $arrOrgEmailTipo[] = $arrItem;
        }

        return $arrOrgEmailTipo;
    }

    /**
     * @param $dados
     */
    protected function valida($dados)
    {
    }

    /**
     * @param $criterios
     */
    protected function pesquisaForJson($criterios)
    {
    }
}
?>