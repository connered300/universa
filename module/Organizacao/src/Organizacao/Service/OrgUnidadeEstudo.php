<?php

namespace Organizacao\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class OrgUnidadeEstudo
 * @package Organizacao\Service
 */
class OrgUnidadeEstudo extends AbstractService
{
    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Organizacao\Entity\OrgUnidadeEstudo');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function pesquisaForJson($params)
    {
        $sql         = '
        SELECT *,
            unidade_end_logradouro endLogradouro ,
            unidade_end_numero endNumero,
            unidade_end_bairro endBairro ,
            unidade_end_cidade endCidade ,
            unidade_end_estado endEstado ,
            unidade_end_cep endCep,
            unidade_nome nome
        FROM org__unidade_estudo 
        INNER JOIN campus_curso USING(camp_id)
        LEFT JOIN org__unidade_agente agentes USING(unidade_id)
        WHERE ';
        $unidadeNome = false;
        $unidadeId   = false;
        $pesId       = false;

        if ($params['q']) {
            $unidadeNome = $params['q'];
        } elseif ($params['query']) {
            $unidadeNome = $params['query'];
        }

        if ($params['pesId']) {
            $pesId = $params['pesId'];
        }

        if ($params['pesIdAgente']) {
            $pesId = $params['pesIdAgente'];
        }

        if ($params['unidadeId']) {
            $unidadeId = $params['unidadeId'];
        }

        $parameters = array('unidade_nome' => "{$unidadeNome}%");
        $sql .= ' unidade_nome LIKE :unidade_nome';

        if ($unidadeId) {
            $parameters['unidade_id'] = $unidadeId;
            $sql .= ' AND unidade_id <> :unidade_id';
        }

        $limit = " LIMIT 0,40";

        if ($params['campuscursoId']) {
            $campuscursoId = $params['campuscursoId'];

            if (is_array($campuscursoId)) {
                if ($campuscursoId['ids']) {
                    $campuscursoId = $campuscursoId['ids'];
                }

                $limit = '';
            }

            $campuscursoId = is_array($campuscursoId) ? $campuscursoId : explode(',', $campuscursoId);

            $sql .= ' AND cursocampus_id IN (:campuscursoId)';
            $parameters['campuscursoId'] = $campuscursoId;
        }

        if ($params['campus']) {
            $sql .= " AND camp_id in ( :campId ) ";
            $parameters['campId'] = $params['campus'];
        }

        if ($pesId) {
            $parameters['pesId'] = $pesId;
            $sql .= ' AND (agentes.pes_id = :pesId OR agentes.pes_id IS NULL)';
        }

        $sql .= " GROUP BY unidade_id";
        $sql .= " ORDER BY unidade_nome";
        $sql .= $limit;

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function getDescricao($id)
    {
        if (!is_array($id)) {
            $id = array($id);
        }

        $id = implode($id, ',');

        $query = 'SELECT group_concat(unidade_nome SEPARATOR ", ") text FROM org__unidade_estudo WHERE org__unidade_estudo.unidade_id IN (' . $id . ')';

        return $this->executeQuery($query)->fetch();
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceOrgAgenteEducacional = new \Organizacao\Service\OrgAgenteEducacional($this->getEm());
        $serviceOrgCampus            = new \Organizacao\Service\OrgCampus($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['unidadeId']) {
                /** @var $objOrgUnidadeEstudo \Organizacao\Entity\OrgUnidadeEstudo */
                $objOrgUnidadeEstudo = $this->getRepository()->find($arrDados['unidadeId']);

                if (!$objOrgUnidadeEstudo) {
                    $this->setLastError('Registro de unidade estudo não existe!');

                    return false;
                }
            } else {
                $objOrgUnidadeEstudo = new \Organizacao\Entity\OrgUnidadeEstudo();
            }

            $arrCollectionGrupo = new \Doctrine\Common\Collections\ArrayCollection(
                $serviceOrgAgenteEducacional->getArrayEntidadePeloId($arrDados['agentes'])
            );

            $objOrgUnidadeEstudo->setAgentes($arrCollectionGrupo);

            if ($arrDados['camp']) {
                /** @var $objOrgCampus \Organizacao\Entity\OrgCampus */
                $objOrgCampus = $serviceOrgCampus->getRepository()->find($arrDados['camp']);

                if (!$objOrgCampus) {
                    $this->setLastError('Registro de câmpus não existe!');

                    return false;
                }

                $objOrgUnidadeEstudo->setCamp($objOrgCampus);
            } else {
                $objOrgUnidadeEstudo->setCamp(null);
            }

            $objOrgUnidadeEstudo->setUnidadeNome($arrDados['unidadeNome']);
            $objOrgUnidadeEstudo->setUnidadeEndNumero($arrDados['unidadeEndNumero']);
            $objOrgUnidadeEstudo->setUnidadeEndCep($arrDados['unidadeEndCep']);
            $objOrgUnidadeEstudo->setUnidadeEndBairro($arrDados['unidadeEndBairro']);
            $objOrgUnidadeEstudo->setUnidadeEndCidade($arrDados['unidadeEndCidade']);
            $objOrgUnidadeEstudo->setUnidadeEndEstado($arrDados['unidadeEndEstado']);
            $objOrgUnidadeEstudo->setUnidadeEndLogradouro($arrDados['unidadeEndLogradouro']);
            $objOrgUnidadeEstudo->setUnidadeTelefone($arrDados['unidadeTelefone']);
            $objOrgUnidadeEstudo->setUnidadeEmail($arrDados['unidadeEmail']);

            $this->getEm()->persist($objOrgUnidadeEstudo);
            $this->getEm()->flush($objOrgUnidadeEstudo);

            $this->getEm()->commit();

            $arrDados['unidadeId'] = $objOrgUnidadeEstudo->getUnidadeId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de unidade estudo!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['unidadeNome']) {
            $errors[] = 'Por favor preencha o campo "unidade"!';
        }

        if (!$arrParam['camp']) {
            $errors[] = 'Por favor preencha o campo "câmpus"!';
        }

        if ($this->verificaSeUnidadeEstaDuplicada($arrParam['unidadeNome'], $arrParam['unidadeId'])) {
            $errors[] = "Já existe uma unidade cadastrada com este mesma nome!";
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    public function verificaSeUnidadeEstaDuplicada($unidadeNome, $unidadeId = false)
    {
        $sql        = 'SELECT count(*) AS qtd FROM org__unidade_estudo WHERE unidade_nome LIKE :unidadeNome';
        $parameters = array('unidadeNome' => $unidadeNome);

        if ($unidadeId) {
            $sql .= ' AND unidade_id <> :unidadeId';
            $parameters['unidadeId'] = $unidadeId;
        }

        $result = $this->executeQueryWithParam($sql, $parameters)->fetch();

        return $result['qtd'] > 0;
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "
        SELECT
        unidade.unidade_id,
        unidade_nome,
        GROUP_CONCAT(pessoa.pes_id) as pes_id,
        GROUP_CONCAT(pessoa.pes_nome) as pes_nome,
        camp_id,
        unidade_end_numero,
        unidade_end_cep,
        unidade_end_bairro,
        unidade_end_cidade,
        unidade_end_estado,
        unidade_end_logradouro,
        unidade_telefone,
        unidade_email,
        org_campus.camp_nome
        FROM org__unidade_estudo unidade
        INNER JOIN org_campus USING (camp_id)
        LEFT JOIN org__unidade_agente agentes USING(unidade_id)
        LEFT JOIN pessoa ON pessoa.pes_id=agentes.pes_id
        GROUP BY unidade.unidade_id";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $unidadeId
     * @return array
     */
    public function getArray($unidadeId)
    {
        /** @var $objOrgUnidadeEstudo \Organizacao\Entity\OrgUnidadeEstudo */
        $objOrgUnidadeEstudo         = $this->getRepository()->find($unidadeId);
        $serviceOrgAgenteEducacional = new \Organizacao\Service\OrgAgenteEducacional($this->getEm());
        $serviceOrgCampus            = new \Organizacao\Service\OrgCampus($this->getEm());

        try {
            $arrDados = $objOrgUnidadeEstudo->toArray();

            if ($arrDados['camp']) {
                /** @var $objOrgCampus \Organizacao\Entity\OrgCampus */
                $objOrgCampus = $serviceOrgCampus->getRepository()->find($arrDados['camp']);

                if ($objOrgCampus) {
                    $arrDados['camp'] = array(
                        'id'   => $objOrgCampus->getCampId(),
                        'text' => $objOrgCampus->getCampNome()
                    );
                } else {
                    $arrDados['camp'] = null;
                }
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    public function getArrSelect2Agentes($params = array())
    {
        $array = array();

        if (!$params['unidadeId']) {
            return $array;
        }

        /** @var $objUnidade \Organizacao\Entity\OrgUnidadeEstudo */
        $objUnidade = $this->getRepository()->find($params['unidadeId']);

        if (!$objUnidade) {
            return $array;
        }

        $agentesCollection = $objUnidade->getAgentes();
        $arrAgentes        = $agentesCollection->count() > 0 ? $agentesCollection->getValues() : array();

        /** @var \Organizacao\Entity\OrgAgenteEducacional $objAgente */
        foreach ($arrAgentes as $objAgente) {
            $array[] = array(
                'text' => $objAgente->getPes()->getPesNome(),
                'id'   => $objAgente->getPes()->getPesId()
            );
        }

        return $array;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceOrgCampus            = new \Organizacao\Service\OrgCampus($this->getEm());

        $arrDados['agentes'] = $this->getArrSelect2Agentes($arrDados);

        if (is_array($arrDados['camp']) && !$arrDados['camp']['id']) {
            $arrDados['camp']['id']   = $arrDados['camp']['campId'];
            $arrDados['camp']['text'] = $arrDados['camp']['campNome'];
        } elseif ($arrDados['camp'] && is_string($arrDados['camp'])) {
            $arrDados['camp'] = $serviceOrgCampus->getArrSelect2(
                array('id' => $arrDados['camp'])
            );
            $arrDados['camp'] = $arrDados['camp'] ? $arrDados['camp'][0] : null;
        }

        return $arrDados;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('unidadeId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Organizacao\Entity\OrgUnidadeEstudo */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getUnidadeId(),
                $params['value'] => $objEntity->getUnidadeNome()
            );
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['unidadeId']) {
            $this->setLastError('Para remover um registro de unidade estudo é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objOrgUnidadeEstudo \Organizacao\Entity\OrgUnidadeEstudo */
            $objOrgUnidadeEstudo = $this->getRepository()->find($param['unidadeId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objOrgUnidadeEstudo);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de unidade estudo.');

            return false;
        }

        return true;
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceCidade = new \Pessoa\Service\Cidade($this->getEm());

        $arrEstados = $serviceCidade->getEstadoArrSelect2();

        $view->setVariable("arrEstados", $arrEstados);
    }

    /**
     * @param array $arrDados
     * @return array
     */
    public function retornaSelect2($arrDados, $informacoesExtra = false, $selecaoLocais = false)
    {
        if (!$arrDados) {
            $this->setLastError("Nenhum dado informado!");

            return [];
        }

        $arrRetorno = [];

        if ($informacoesExtra) {
            foreach ($arrDados as $index => $value) {
                if (isset($value['unidade_id'])) {
                    $arrRetorno[] = [
                        'id'   => $selecaoLocais ? $value['selelocais_id'] : $value['unidade_id'],
                        'text' => $value['unidade_nome'] . ' - Endereço: ' . $value['unidade_end_logradouro'] .
                            ($value['unidade_end_numero'] ? ',' . $value['unidade_end_numero'] : '') . ',' .
                            $value['unidade_end_cidade'] . ',' . $value['unidade_end_estado'] . ',' . $value['unidade_end_cep']
                    ];
                }
            }

            return $arrRetorno;
        }

        foreach ($arrDados as $index => $value) {
            if (isset($value['unidade_id'])) {
                $arrRetorno[] = ['id' => $value['unidade_id'], 'text' => $value['unidade_nome']];
            }
        }

        return $arrRetorno;
    }

}
?>