<?php

namespace Organizacao\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class OrgAgenteEducacional
 * @package Organizacao\Service
 */
class OrgAgenteEducacional extends AbstractService
{
    const AGENTE_CONTA_TIPO_POUPANCA = 'Poupança';
    const AGENTE_CONTA_TIPO_CORRENTE = 'Corrente';
    const AGENTE_SITUACAO_ATIVO      = 'Ativo';
    const AGENTE_SITUACAO_INATIVO    = 'Inativo';

    private $__lastError = null;

    /** @var \Sistema\Service\SisConfig */
    private $config = null;

    /**
     * @return \Sistema\Service\SisConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param \Sistema\Service\SisConfig $config
     * @return OrgAgenteEducacional
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, $arrConfig = array())
    {
        parent::__construct($em, 'Organizacao\Entity\OrgAgenteEducacional');

        $this->setConfig(new \Sistema\Service\SisConfig($em, $arrConfig));
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function pesquisaForJson($params)
    {
        $juncaoAgente = 'INNER JOIN org__agente_educacional agente USING (pes_id)';

        if ($params['todasPessoas']) {
            $juncaoAgente = 'LEFT JOIN org__agente_educacional agente USING (pes_id)';
        }

        $sql   = '
    SELECT
        pessoa.pes_id,
        pessoa.pes_id pesId,
        pessoa.pes_nome,
        pessoa.pes_nome pesNome,
        pessoaFisica.pes_cpf,
        pessoaFisica.pes_cpf pesCpf,
        pessoaJuridica.pes_cnpj,
        pessoaJuridica.pes_cnpj pesCnpj,
        pessoaFisica.pes_doc_estrangeiro,
        pessoaFisica.pes_doc_estrangeiro pesDocEstrangeiro,
        pessoa.pes_nacionalidade pesNacionalidade,
        pessoaFisica.pes_rg pesRg,
        pessoaFisica.pes_sexo pesSexo,
        date_format(pessoaFisica.pes_data_nascimento, "%d/%m/%Y") pesDataNascimento,
        endereco.end_estado endEstado,
        endereco.end_cidade endCidade
    FROM pessoa
        LEFT JOIN pessoa_fisica pessoaFisica USING (pes_id)
        LEFT JOIN pessoa_juridica pessoaJuridica USING (pes_id)
        ' . $juncaoAgente . '
        LEFT JOIN endereco USING (pes_id)
    WHERE';
        $query = false;

        if ($params['query']) {
            $query = trim($params['query']);
        }

        if ($params['pesId']) {
            $parameters['pes_id'] = trim($params['pesId']);
            $sql .= ' pes_id = :pes_id';
        } elseif ($params['pesCpf']) {
            $parameters['pes_cpf'] = trim($params['pesCpf']);
            $sql .= ' pes_cpf like :pes_cpf';
        } elseif ($params['pesCnpj']) {
            $parameters['pes_cnpj'] = trim($params['pesCnpj']);
            $sql .= ' pes_cnpj like :pes_cnpj';
        } elseif ($params['pesDocEstrangeiro']) {
            $parameters['pes_doc_estrangeiro'] = trim($params['pesDocEstrangeiro']);
            $sql .= ' pes_doc_estrangeiro = :pes_doc_estrangeiro';
        } else {
            $parameters = array(
                'pes_nome' => "{$query}%",
                'pes_id'   => ltrim($query, '0') . "%"
            );
            $sql .= ' (pessoa.pes_id*1 LIKE :pes_id OR pes_nome COLLATE UTF8_GENERAL_CI LIKE :pes_nome)';
        }

        if ($params['ignorarPesId']) {
            $ignorarPesId = preg_replace('/[^0-9,]/', '', $params['ignorarPesId']);
            $sql .= ' AND pes_id NOT IN(' . $ignorarPesId . ')';
        }

        $sql .= " ORDER BY pes_nome";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getDescricao($id)
    {
        if (!is_array($id)) {
            $id = array($id);
        }

        $id = implode($id, ',');

        $query = '
        SELECT group_concat(pes_nome SEPARATOR ", ") text
        FROM org__agente_educacional
        LEFT JOIN pessoa USING(pes_id)
        WHERE org__agente_educacional.pes_id IN ( ' . $id . ')';

        $result = $this->executeQuery($query)->fetch();

        return $result['text'];
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $novoAgente = false;

        $serviceAcessoPessoas           = new \Acesso\Service\AcessoPessoas($this->getEm());
        $servicePessoa                  = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceOrgAgenteEducacional    = new \Organizacao\Service\OrgAgenteEducacional($this->getEm());
        $serviceAcadgeralCadastroOrigem = new \Matricula\Service\AcadgeralCadastroOrigem($this->getEm());
        $serviceBoletoBanco             = new \Financeiro\Service\BoletoBanco($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            $objOrgAgenteEducacional = null;
            $objPessoa               = null;

            if ($arrDados['pesId']) {
                /** @var $objOrgAgenteEducacional \Organizacao\Entity\OrgAgenteEducacional */
                $objOrgAgenteEducacional = $this->getRepository()->find($arrDados['pesId']);

                /** @var \Pessoa\Entity\Pessoa $objPessoa */
                $objPessoa = $servicePessoa->getRepository()->find($arrDados['pesId']);
            }

            if (!$objOrgAgenteEducacional) {
                $objOrgAgenteEducacional = new \Organizacao\Entity\OrgAgenteEducacional();
                $novoAgente              = true;
            }

            //Troca a pessoa responsável pela agencia
            if ($arrDados['pesIdAgente']) {
                /** @var $objOrgAgenteEducacionalValidacao \Organizacao\Entity\OrgAgenteEducacional */
                $objOrgAgenteEducacionalValidacao = $this->getRepository()->find($arrDados['pesIdAgente']);

                if ($objOrgAgenteEducacionalValidacao && $objOrgAgenteEducacionalValidacao != $objOrgAgenteEducacional) {
                    $this->setLastError('A pessoa indicada já é um agente educacional!');

                    return false;
                }

                /** @var \Pessoa\Entity\Pessoa $objPessoa */
                $objPessoa = $servicePessoa->getRepository()->find($arrDados['pesIdAgente']);
            }

            $objOrgAgenteEducacional->setPes($objPessoa);

            if ($arrDados['pesIdIndicacao']) {
                /** @var $objOrgAgenteEducacionalInndicacao \Organizacao\Entity\OrgAgenteEducacional */
                $objOrgAgenteEducacionalInndicacao = $serviceOrgAgenteEducacional->getRepository()->find(
                    $arrDados['pesIdIndicacao']
                );

                if (!$objOrgAgenteEducacionalInndicacao) {
                    $this->setLastError('Registro de agente educacional não existe!');

                    return false;
                }

                $objOrgAgenteEducacional->setPesIdIndicacao($objOrgAgenteEducacionalInndicacao);
            } else {
                $objOrgAgenteEducacional->setPesIdIndicacao(null);
            }

            if ($arrDados['origem']) {
                /** @var $objAcadgeralCadastroOrigem \Matricula\Entity\AcadgeralCadastroOrigem */
                $objAcadgeralCadastroOrigem = $serviceAcadgeralCadastroOrigem->getRepository()->find(
                    $arrDados['origem']
                );

                if (!$objAcadgeralCadastroOrigem) {
                    $this->setLastError('Registro de origem de cadastro não existe!');

                    return false;
                }

                $objOrgAgenteEducacional->setOrigem($objAcadgeralCadastroOrigem);
            } else {
                $objOrgAgenteEducacional->setOrigem(null);
            }

            if ($arrDados['banc']) {
                /** @var $objBoletoBanco \Financeiro\Entity\BoletoBanco */
                $objBoletoBanco = $serviceBoletoBanco->getRepository()->find($arrDados['banc']);

                if (!$objBoletoBanco) {
                    $this->setLastError('Registro de banco não existe!');

                    return false;
                }

                $objOrgAgenteEducacional->setBanc($objBoletoBanco);
            } else {
                $objOrgAgenteEducacional->setBanc(null);
            }

            if ($arrDados['agenteComissaoPrimeiroNivel'] > 100 || $arrDados['agenteComissaoPrimeiroNivel'] < 0 ||
                $arrDados['agenteComissaoDemaisNiveis'] > 100 || $arrDados['agenteComissaoDemaisNiveis'] < 0
            ) {
                $this->setLastError('Valor informado da porcentagem esta incorreto!');

                return false;
            };

            $arrDados['agenteComissaoDemaisNiveis'] ?
                $arrDados['agenteComissaoDemaisNiveis'] = str_replace(
                    ",",
                    ".",
                    $arrDados['agenteComissaoDemaisNiveis']
                ) : $arrDados['agenteComissaoDemaisNiveis'] = 0.00;

            $arrDados['agenteComissaoPrimeiroNivel'] ? $arrDados['agenteComissaoPrimeiroNivel'] = str_replace(
                ",",
                ".",
                $arrDados['agenteComissaoPrimeiroNivel']
            ) : $arrDados['agenteComissaoPrimeiroNivel'] = 0.00;

            $dataCadastro = ($arrDados['agenteDataCadastro'] ? $arrDados['agenteDataCadastro'] : '');
            $dataCadastro = ($dataCadastro ? $dataCadastro : $objOrgAgenteEducacional->getAgenteDataCadastro());
            $dataCadastro = ($dataCadastro ? $dataCadastro : new \DateTime());
            $objOrgAgenteEducacional->setAgenteDataCadastro($dataCadastro);
            $objOrgAgenteEducacional->setAgenteDataAtualizacao(new \DateTime());
            $objOrgAgenteEducacional->setAgenteContaAgencia($arrDados['agenteContaAgencia']);
            $objOrgAgenteEducacional->setAgenteContaTipo($arrDados['agenteContaTipo']);
            $objOrgAgenteEducacional->setAgenteContaNumero($arrDados['agenteContaNumero']);
            $objOrgAgenteEducacional->setAgenteContaTitular($arrDados['agenteContaTitular']);
            $objOrgAgenteEducacional->setAgenteObservacao($arrDados['agenteObservacao']);
            $objOrgAgenteEducacional->setAgenteSituacao($arrDados['agenteSituacao']);
            $objOrgAgenteEducacional->setAgenteMaxNivel($arrDados['agenteMaxNivel']);
            $objOrgAgenteEducacional->setAgenteComissaoPrimeiroNivel($arrDados['agenteComissaoPrimeiroNivel']);
            $objOrgAgenteEducacional->setAgenteComissaoDemaisNiveis($arrDados['agenteComissaoDemaisNiveis']);

            $this->getEm()->persist($objOrgAgenteEducacional);
            $this->getEm()->flush($objOrgAgenteEducacional);

            $this->getEm()->commit();

            $arrDados['pesId'] = $objOrgAgenteEducacional->getPes()->getPesId();

            $criarUsuarioDeAgente = $this->getConfig()->localizarChave('AGENTE_EDUCACIONAL_CRIAR_USUARIO');
            $grupoAgente          = $this->getConfig()->localizarChave('AGENTE_EDUCACIONAL_ACESSO_GRUPO');

            if ($criarUsuarioDeAgente && $grupoAgente) {
                $serviceAcessoPessoas->registrarUsuarioPadraoEVinculaGrupo($arrDados['pesId'], $grupoAgente);
            }

            if ($objOrgAgenteEducacional->getAgenteSituacao() == self::AGENTE_SITUACAO_INATIVO) {
                $serviceAcessoPessoas->inativaUsuarioPessoa($objPessoa);
            }

            if ($novoAgente) {
                $serviceOrgComunicacaoModelo = new \Organizacao\Service\OrgComunicacaoModelo($this->getEm());

                $arrInfo = $serviceOrgComunicacaoModelo->trabalharVariaveisPorContexto(
                    array('pesId' => $arrDados['pesId']),
                    \Organizacao\Service\OrgComunicacaoTipo::TIPO_CONTEXTO_AGENTE_EDUCACIONAL
                );

                if (
                    $this->getConfig()->localizarChave('ENVIAR_SMS_BOAS_VINDAS_AGENTE') &&
                    $this->getConfig()->localizarChave('SMS_ATIVO')
                ) {
                    $serviceOrgComunicacaoModelo->registrarComunicacao(
                        \Organizacao\Service\OrgComunicacaoTipo::TIPO_BOAS_VINDAS_AGENTE,
                        \Organizacao\Service\OrgComunicacaoModelo::MODELO_TIPO_SMS,
                        $arrInfo
                    );
                }

                if ($this->getConfig()->localizarChave('ENVIAR_EMAIL_BOAS_VINDAS_AGENTE')) {
                    $serviceOrgComunicacaoModelo->registrarComunicacao(
                        \Organizacao\Service\OrgComunicacaoTipo::TIPO_BOAS_VINDAS_AGENTE,
                        \Organizacao\Service\OrgComunicacaoModelo::MODELO_TIPO_EMAIL,
                        $arrInfo
                    );
                }
            }

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de agente educacional!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if ($arrParam['pesIdAgente'] == "") {
            $errors[] = 'Por favor preencha o campo "Pessoa"!';
        }

        if ($arrParam['agenteMaxNivel'] == "") {
            $errors[] = 'Por favor preencha o campo "nível máximo agente"!';
        }

        if (!in_array($arrParam['agenteContaTipo'], self::getAgenteContaTipo())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "tipo conta"!';
        }

        if (!$arrParam['agenteSituacao']) {
            $errors[] = 'Por favor preencha o campo "situacão agente"!';
        }

        if (!in_array($arrParam['agenteSituacao'], self::getAgenteSituacao())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "situacão agente"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2AgenteContaTipo($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getAgenteContaTipo());
    }

    /**
     * @return array
     */
    public static function getAgenteContaTipo()
    {
        return array(self::AGENTE_CONTA_TIPO_POUPANCA, self::AGENTE_CONTA_TIPO_CORRENTE);
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2AgenteSituacao($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getAgenteSituacao());
    }

    /**
     * @return array
     */
    public static function getAgenteSituacao()
    {
        return array(self::AGENTE_SITUACAO_ATIVO, self::AGENTE_SITUACAO_INATIVO);
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $conditions = '';

        if ($data['filter']['cidadeAgente']) {
            $conditions .= " AND end_cidade IN('{$data['filter']['cidadeAgente']}')";
        }

        if ($data['filter']['estadoAgente']) {
            $conditions .= " AND (end_estado IN('{$data['filter']['estadoAgente']}') OR e.est_uf IN('{$data['filter']['estadoAgente']}'))";
        }

        if ($data['filter']['origemCadastro']) {
            $conditions .= " AND origem.origem_id IN({$data['filter']['origemCadastro']})";
        }

        if ($data['filter']['dataCadastroInicial']) {
            $conditions .= " AND date(agente_data_cadastro) >= " .
                "'" . self::formatDateAmericano($data['filter']['dataCadastroInicial']) . "'";
        }

        if ($data['filter']['dataCadastroFinal']) {
            $conditions .= " AND date(agente_data_atualizacao) <= " .
                "'" . self::formatDateAmericano($data['filter']['dataCadastroFinal']) . "'";
        }

        if ($data['filter']['dataAlteracaoInicial']) {
            $conditions .= " AND date(agente_data_atualizacao) >= " .
                "'" . self::formatDateAmericano($data['filter']['dataAlteracaoInicial']) . "'";
        }

        if ($data['filter']['dataAlteracaoFinal']) {
            $conditions .= " AND date(agente_data_atualizacao) <= " .
                "'" . self::formatDateAmericano($data['filter']['dataAlteracaoFinal']) . "'";
        }

        $query = "
       SELECT
            agente.*,
            format(agente.agente_comissao_primeiro_nivel,'2','de_DE') AS comissaoPrimeiroNivelFormatado,
            format(agente.agente_comissao_demais_niveis,'2','de_DE') AS comissaoDemaisNiveisFormatada,
            pes_nome,
            coalesce(pes_cpf,pes_cnpj) as pes_cpf_cnpj,
            endereco.end_id, tipo_endereco, end_pais, end_estado, end_tipo_end, end_bairro,
            end_cidade, end_cep, end_logradouro, end_tipo_logradouro, end_numero, end_complemento,
            origem.origem_nome, e.est_uf,
            pessoa.con_contato_email,
            pessoa.con_contato_telefone,
            pessoa.con_contato_celular
        FROM org__agente_educacional agente
        LEFT JOIN pessoa USING(pes_id)
        LEFT JOIN pessoa_fisica USING(pes_id)
        LEFT JOIN pessoa_juridica USING(pes_id)
        LEFT JOIN endereco USING(pes_id)
        LEFT JOIN acadgeral__cadastro_origem AS origem ON agente.origem_id=origem.origem_id
        LEFT JOIN estado as e on e.est_nome=end_estado
        WHERE 1=1 {$conditions}
        GROUP BY agente.pes_id
        ";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $pesId
     * @return array
     */
    public function getArrayFallback($pesId)
    {
        $arrDados = $this->getArray($pesId);

        if (!$arrDados) {
            $servicePessoa           = new \Pessoa\Service\Pessoa($this->getEm());
            $objOrgAgenteEducacional = new \Organizacao\Entity\OrgAgenteEducacional();

            $arrDados = $servicePessoa->getArray($pesId);
            $arrDados = array_merge($objOrgAgenteEducacional->toArray(), $arrDados);
        }

        return $arrDados;
    }

    /**
     * @param $pesId
     * @return array
     */
    public function getArray($pesId)
    {
        if (!$pesId) {
            return array();
        }

        /* @var $objOrgAgenteEducacional \Organizacao\Entity\OrgAgenteEducacional */
        $objOrgAgenteEducacional = $this->getRepository()->find($pesId);

        if (!$objOrgAgenteEducacional) {
            $this->setLastError('Agente não existe!');

            return array();
        }

        $servicePessoa                  = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceAcadgeralCadastroOrigem = new \Matricula\Service\AcadgeralCadastroOrigem($this->getEm());
        $serviceBoletoBanco             = new \Financeiro\Service\BoletoBanco($this->getEm());

        try {
            $arrDados = $objOrgAgenteEducacional->toArray();
            $pesId    = $objOrgAgenteEducacional->getPes()->getPesId();

            $arrDadosPessoa = $servicePessoa->getArray($pesId);

            $arrDados                = array_merge($arrDados, $arrDadosPessoa);
            $arrDados['pesIdAgente'] = $arrDados['pesId'];

            if ($arrDados['pesIdAgente']) {
                $arrDados['pesIdAgente'] = $servicePessoa->getArrSelect2(['id' => $arrDados['pesIdAgente']]);
                $arrDados['pesIdAgente'] = $arrDados['pesIdAgente'] ? $arrDados['pesIdAgente'][0] : null;
            }

            if ($arrDados['pesIdIndicacao']) {
                $arrDados['pesIdIndicacao'] = $this->getArrSelect2(['id' => $arrDados['pesIdIndicacao']]);
                $arrDados['pesIdIndicacao'] = $arrDados['pesIdIndicacao'] ? $arrDados['pesIdIndicacao'][0] : null;
            }

            if ($arrDados['origem']) {
                $arrDados['origem'] = $serviceAcadgeralCadastroOrigem->getArrSelect2(['id' => $arrDados['origem']]);
                $arrDados['origem'] = $arrDados['origem'] ? $arrDados['origem'][0] : null;
            }

            if ($arrDados['banc']) {
                $arrDados['banc'] = $serviceBoletoBanco->getArrSelect2(['id' => $arrDados['banc']]);
                $arrDados['banc'] = $arrDados['banc'] ? $arrDados['banc'][0] : null;
            }

            if ($arrDados['agenteComissaoPrimeiroNivel']) {
                $arrDados['agenteComissaoPrimeiroNivelFormatado'] = number_format(
                    $arrDados['agenteComissaoPrimeiroNivel'],
                    2,
                    ",",
                    "."
                );
            }

            if ($arrDados['agenteComissaoDemaisNiveis']) {
                $arrDados['agenteComissaoDemaisNiveisFormatado'] = number_format(
                    $arrDados['agenteComissaoDemaisNiveis'],
                    2,
                    ",",
                    "."
                );
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceAcadgeralCadastroOrigem = new \Matricula\Service\AcadgeralCadastroOrigem($this->getEm());
        $serviceBoletoBanco             = new \Financeiro\Service\BoletoBanco($this->getEm());
        $servicePessoa                  = new \Pessoa\Service\Pessoa($this->getEm());

        if (is_array($arrDados['pesIdAgente']) && !$arrDados['pesIdAgente']['text']) {
            $arrDados['pesIdAgente'] = $arrDados['pesIdAgente']['id'];
        }

        if ($arrDados['pesIdAgente'] && is_string($arrDados['pesIdAgente'])) {
            $arrDados['pesIdAgente'] = $servicePessoa->getArrSelect2(array('id' => $arrDados['pesIdAgente']));
            $arrDados['pesIdAgente'] = $arrDados['pesIdAgente'] ? $arrDados['pesIdAgente'][0] : null;
        }

        if (is_array($arrDados['pesIdIndicacao']) && !$arrDados['pesIdIndicacao']['text']) {
            $arrDados['pesIdIndicacao'] = $arrDados['pesIdIndicacao']['id'];
        }

        if ($arrDados['pesIdIndicacao'] && is_string($arrDados['pesIdIndicacao'])) {
            $arrDados['pesIdIndicacao'] = $this->getArrSelect2(array('id' => $arrDados['pesIdIndicacao']));
            $arrDados['pesIdIndicacao'] = $arrDados['pesIdIndicacao'] ? $arrDados['pesIdIndicacao'][0] : null;
        }

        if (is_array($arrDados['origem']) && !$arrDados['origem']['text']) {
            $arrDados['origem'] = $arrDados['origem']['id'];
        }

        if ($arrDados['origem'] && is_string($arrDados['origem'])) {
            $arrDados['origem'] = $serviceAcadgeralCadastroOrigem->getArrSelect2(array('id' => $arrDados['origem']));
            $arrDados['origem'] = $arrDados['origem'] ? $arrDados['origem'][0] : null;
        }

        if (is_array($arrDados['banc']) && !$arrDados['banc']['text']) {
            $arrDados['banc'] = $arrDados['banc']['id'];
        }

        if ($arrDados['banc'] && is_string($arrDados['banc'])) {
            $arrDados['banc'] = $serviceBoletoBanco->getArrSelect2(array('id' => $arrDados['banc']));
            $arrDados['banc'] = $arrDados['banc'] ? $arrDados['banc'][0] : null;
        }

        return $arrDados;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('pes' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Organizacao\Entity\OrgAgenteEducacional */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getPes()->getPesId(),
                $params['value'] => $objEntity->getPes()->getPesNome()
            );
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['pesId']) {
            $this->setLastError('Para remover um registro de agente educacional é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objOrgAgenteEducacional \Organizacao\Entity\OrgAgenteEducacional */
            $objOrgAgenteEducacional = $this->getRepository()->find($param['pesId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objOrgAgenteEducacional);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de agente educacional.');

            return false;
        }

        return true;
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $servicePessoa = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceCidade = new \Pessoa\Service\Cidade($this->getEm());

        $arrEstados = $serviceCidade->getEstadoArrSelect2();

        $servicePessoa->setarDependenciasView($view);

        $view->setVariable("arrEstados", $arrEstados);
        $view->setVariable("arrAgenteContaTipo", $this->getArrSelect2AgenteContaTipo());
        $view->setVariable("arrAgenteSituacao", $this->getArrSelect2AgenteSituacao());
    }

    /**
     * @return null
     */
    public function getAgenteLogado()
    {
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());
        $objAcessoPessoas     = $serviceAcessoPessoas->retornaUsuarioLogado();
        $arrItem              = [];

        if ($objAcessoPessoas) {
            if ($objAcessoPessoas->getPesFisica()) {
                $arrItem = $this->getArrSelect2(['id' => $objAcessoPessoas->getPesFisica()->getPes()->getPesId()]);

                if ($arrItem) {
                    return $arrItem[0];
                }
            } elseif ($objAcessoPessoas->getPesJuridica()) {
                $arrItem = $this->getArrSelect2(['id' => $objAcessoPessoas->getPesJuridica()->getPes()->getPesId()]);

                if ($arrItem) {
                    return $arrItem[0];
                }
            }
        }
        if (!$arrItem) {
            $arrItem = ['id' => '-1', 'text' => 'Indefinido'];
        }

        return $arrItem;
    }

    public function getArrayEntidadePeloId($id)
    {
        $array = array();
        $arrId = $id ? explode(',', $id) : null;

        foreach ($arrId as $id) {
            $array[] = $this->getRepository()->find($id);
        }

        return $array;
    }
}
?>