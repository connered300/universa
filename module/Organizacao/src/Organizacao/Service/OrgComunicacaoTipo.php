<?php

namespace Organizacao\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class OrgComunicacaoTipo
 * @package Organizacao\Service
 */
class OrgComunicacaoTipo extends AbstractService
{
    const TIPO_CONTEXTO_ALUNO              = 'Aluno';
    const TIPO_CONTEXTO_ALUNO_CURSO        = 'Aluno Curso';
    const TIPO_CONTEXTO_PESSOA             = 'Pessoa';
    const TIPO_CONTEXTO_AGENTE_EDUCACIONAL = 'Agente Educacional';

    const TIPO_ACESSO_CADASTRO                    = 1;
    const TIPO_ACESSO_RECUPERAR_SENHA             = 2;
    const TIPO_VESTIBULAR_CADASTRO                = 3;
    const TIPO_BOAS_VINDAS_ALUNO                  = 4;
    const TIPO_BOAS_VINDAS_AGENTE                 = 5;
    const TIPO_ENVIO_BOLETO                       = 6;
    const TIPO_BOAS_VINDAS_ALUNO_APOS_DEFERIMENTO = 7;
    const TIPO_ENVIO_RECORRENCIA_MAXIPAGO         = 8;

    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Organizacao\Entity\OrgComunicacaoTipo');
    }

    /**
     * @return array
     */
    public static function getComunicacaoTipo()
    {
        return array(
            self::TIPO_ACESSO_CADASTRO,
            self::TIPO_ACESSO_RECUPERAR_SENHA,
            self::TIPO_VESTIBULAR_CADASTRO,
            self::TIPO_BOAS_VINDAS_ALUNO,
            self::TIPO_BOAS_VINDAS_AGENTE,
            self::TIPO_ENVIO_BOLETO,
            self::TIPO_ENVIO_RECORRENCIA_MAXIPAGO,
        );
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @param $params
     * @return array
     */
    public function pesquisaForJson($params)
    {
        $sql           = '
        SELECT t.tipoId AS tipo_id,t.tipoDescricao AS tipo_descricao
        FROM \Organizacao\Entity\OrgComunicacaoTipo t
        WHERE';
        $tipoDescricao = false;
        $tipoId        = false;

        if ($params['q']) {
            $tipoDescricao = $params['q'];
        } elseif ($params['query']) {
            $tipoDescricao = $params['query'];
        }

        if ($params['tipoId']) {
            $tipoId = $params['tipoId'];
        }

        $parameters = array('tipoDescricao' => "{$tipoDescricao}%");
        $sql .= ' t.tipoDescricao LIKE :tipoDescricao';

        if ($tipoId) {
            $parameters['tipoId'] = explode(',', $tipoId);
            $sql .= ' AND t.tipoId NOT IN (:tipoId)';
        }

        $sql .= " ORDER BY t.tipoDescricao";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['tipoId']) {
                /** @var $objOrgComunicacaoTipo \Organizacao\Entity\OrgComunicacaoTipo */
                $objOrgComunicacaoTipo = $this->getRepository()->find($arrDados['tipoId']);

                if (!$objOrgComunicacaoTipo) {
                    $this->setLastError('Registro de tipo de comunicação não existe!');

                    return false;
                }
            } else {
                $objOrgComunicacaoTipo = new \Organizacao\Entity\OrgComunicacaoTipo();
            }

            $objOrgComunicacaoTipo->setTipoDescricao($arrDados['tipoDescricao']);
            $objOrgComunicacaoTipo->setTipoContexto($arrDados['tipoContexto']);

            $this->getEm()->persist($objOrgComunicacaoTipo);
            $this->getEm()->flush($objOrgComunicacaoTipo);

            $this->getEm()->commit();

            $arrDados['tipoId'] = $objOrgComunicacaoTipo->getTipoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de tipo de comunicação!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['tipoContexto']) {
            $errors[] = 'Por favor, preencha o campo "Contexto"';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM org__comunicacao_tipo";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $tipoId
     * @return array
     */
    public function getArray($tipoId)
    {
        /** @var $objOrgComunicacaoTipo \Organizacao\Entity\OrgComunicacaoTipo */
        $objOrgComunicacaoTipo = $this->getRepository()->find($tipoId);

        try {
            $arrDados = $objOrgComunicacaoTipo->toArray();
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        return $arrDados;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('tipoId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Organizacao\Entity\OrgComunicacaoTipo */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getTipoId(),
                $params['value'] => $objEntity->getTipoDescricao()
            );
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['tipoId']) {
            $this->setLastError('Para remover um registro de tipo de comunicação é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objOrgComunicacaoTipo \Organizacao\Entity\OrgComunicacaoTipo */
            $objOrgComunicacaoTipo = $this->getRepository()->find($param['tipoId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objOrgComunicacaoTipo);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de tipo de comunicação.');

            return false;
        }

        return true;
    }

    public static function getTipoContexto()
    {
        return array(
            self::TIPO_CONTEXTO_ALUNO,
            self::TIPO_CONTEXTO_ALUNO_CURSO,
            self::TIPO_CONTEXTO_PESSOA,
            self::TIPO_CONTEXTO_AGENTE_EDUCACIONAL
        );
    }

    public function getArrSelect2TipoContexto($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getTipoContexto());
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $view->setVariable("arrTipoContexto", $this->getArrSelect2TipoContexto());
    }
}
?>