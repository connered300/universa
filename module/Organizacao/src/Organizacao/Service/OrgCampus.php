<?php
namespace Organizacao\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class OrgCampus
 * @package Organizacao\Service
 */
class OrgCampus extends AbstractService
{
    /**
     * @var null
     */
    private $lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Organizacao\Entity\OrgCampus');
    }

    public function getDescricao($id)
    {
        if (!is_array($id)) {
            $id = explode(',', $id);
        }

        $query = '
        SELECT group_concat(DISTINCT camp_nome SEPARATOR ", ") text
        FROM org_campus
        WHERE camp_id IN (:id)';

        return $this->executeQueryWithParam($query, ['id' => $id])->fetch();
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "
        SELECT
            campus.*,
            ies_nome AS instituicao,
            pd.pes_nome AS diretoria,
            ps.pes_nome AS secretaria
        FROM org_campus campus
        INNER JOIN org_ies ies ON ies.ies_id=campus.ies_id
        LEFT JOIN pessoa pd ON pd.pes_id=campus.diretoria_pes_id
        LEFT JOIN pessoa ps ON ps.pes_id=campus.secretaria_pes_id";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $campusId
     * @return array
     */
    public function getArray($campusId)
    {
        /* @var $objOrgCampus \Organizacao\Entity\OrgCampus */
        $arrDados           = array();
        $objOrgCampus       = $this->getRepository()->find($campusId);
        $serviceArquivo     = new \GerenciadorArquivos\Service\Arquivo($this->getEm());
        $serviceOrgIes      = new \Organizacao\Service\OrgIes($this->getEm());
        $serviceInfraPredio = new \Infraestrutura\Service\InfraPredio($this->getEm());

        try {
            $objIes           = $objOrgCampus->getIes();
            $objSecretariaPes = $objOrgCampus->getSecretariaPes();
            $objDiretoriaPes  = $objOrgCampus->getDiretoriaPes();

            $arrDados = $objOrgCampus->toArray();

            $arq    = $arrDados['arq'];
            $arrArq = null;

            if ($arq) {
                if (is_object($arq)) {
                    $objArquivo = $arq;
                } elseif (is_array($arq)) {
                    $objArquivo = $serviceArquivo->getRepository()->find($arq['id']);
                } else {
                    $objArquivo = $serviceArquivo->getRepository()->find($arq);
                }

                $arrArq = array(
                    'id'      => $objArquivo->getArqId(),
                    'nome'    => $objArquivo->getArqNome(),
                    'tipo'    => $objArquivo->getArqTipo(),
                    'tamanho' => $objArquivo->getArqTamanho(),
                    'chave'   => $objArquivo->getArqChave(),
                );
            }

            /** @var \GerenciadorArquivos\Entity\Arquivo $objCarteirinha */
            if ($objCarteirinha = $arrDados['arqCarteirinha']) {
                unset($arrDados['arqCarteirinha']);
                $arrDados['arqCarteirinha']['id']      = $objCarteirinha->getArqId();
                $arrDados['arqCarteirinha']['nome']    = $objCarteirinha->getArqNome();
                $arrDados['arqCarteirinha']['tipo']    = $objCarteirinha->getArqTipo();
                $arrDados['arqCarteirinha']['tamanho'] = $objCarteirinha->getArqTamanho();
                $arrDados['arqCarteirinha']['chave']   = $objCarteirinha->getArqChave();
            }

            $arrDados['arq']           = $arrArq;
            $arrDados['ies']           = $objIes ? $serviceOrgIes->retornaDadosInstituicao($objIes) : null;
            $arrDados['secretariaPes'] = $objSecretariaPes ? $objSecretariaPes->toArray() : null;
            $arrDados['diretoriaPes']  = $objDiretoriaPes ? $objDiretoriaPes->toArray() : null;
            $arrDados['predios']       = $serviceInfraPredio->getArrayPredios($campusId);
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceArquivo = new \GerenciadorArquivos\Service\Arquivo($this->getEm());
        $servicePessoa  = new \Pessoa\Service\PessoaFisica($this->getEm());
        $serviceIes     = new \Organizacao\Service\OrgIes($this->getEm());

        $arq    = $arrDados['arq'];
        $arrArq = null;

        if ($arq) {
            if (is_object($arq)) {
                $objArquivo = $arq;
            } elseif (is_array($arq)) {
                $objArquivo = $serviceArquivo->getRepository()->find($arq['id']);
            } else {
                $objArquivo = $serviceArquivo->getRepository()->find($arq);
            }

            if ($objArquivo) {
                $arrArq = array(
                    'id'      => $objArquivo->getArqId(),
                    'nome'    => $objArquivo->getArqNome(),
                    'tipo'    => $objArquivo->getArqTipo(),
                    'tamanho' => $objArquivo->getArqTamanho(),
                    'chave'   => $objArquivo->getArqChave(),
                );
            }
        }

        $arrDados['arq'] = $arrArq;

        if ($arrDados['ies']) {
            if (is_array($arrDados['ies'])) {
                $arrDados['ies']['id']   = $arrDados['ies']['iesId'];
                $arrDados['ies']['text'] = $arrDados['ies']['iesNome'];
            } elseif ($arrDados['ies'] && is_string($arrDados['ies'])) {
                $arrDados['ies'] = $serviceIes->getArrSelect2(
                    array('id' => $arrDados['ies'])
                );
                $arrDados['ies'] = $arrDados['ies'][0];
            }
        }

        if ($arrDados['secretariaPes']) {
            if (is_array($arrDados['secretariaPes'])) {
                $arrDados['secretariaPes']['id']   = $arrDados['secretariaPes']['pesId'];
                $arrDados['secretariaPes']['text'] = $arrDados['secretariaPes']['pesNome'];
            } elseif ($arrDados['secretariaPes'] && is_string($arrDados['secretariaPes'])) {
                $arrDados['secretariaPes'] = $servicePessoa->getArrSelect2(
                    array('id' => $arrDados['secretariaPes'])
                );
                $arrDados['secretariaPes'] = $arrDados['secretariaPes'][0];
            }
        }

        if ($arrDados['diretoriaPes']) {
            if (is_array($arrDados['diretoriaPes'])) {
                $arrDados['diretoriaPes']['id']   = $arrDados['diretoriaPes']['pesId'];
                $arrDados['diretoriaPes']['text'] = $arrDados['diretoriaPes']['pesNome'];
            } elseif ($arrDados['diretoriaPes'] && is_string($arrDados['diretoriaPes'])) {
                $arrDados['diretoriaPes'] = $servicePessoa->getArrSelect2(
                    array('id' => $arrDados['diretoriaPes'])
                );
                $arrDados['diretoriaPes'] = $arrDados['diretoriaPes'][0];
            }
        }

        if ($arrDados['predios'] && is_string($arrDados['predios'])) {
            $arrDados['predios'] = json_decode($arrDados['predios'], true);
        }

        return $arrDados;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function salvar(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceInfraPredio       = new \Infraestrutura\Service\InfraPredio($this->getEm());
        $serviceOrgIes            = new \Organizacao\Service\OrgIes($this->getEm());
        $servicePessoaFisica      = new \Pessoa\Service\PessoaFisica($this->getEm());
        $serviceArquivoDiretorios = new \GerenciadorArquivos\Service\ArquivoDiretorios($this->getEm());
        $serviceArquivo           = new \GerenciadorArquivos\Service\Arquivo($this->getEm());

        $objDiretorio = $serviceArquivoDiretorios->getRepository()->find(\Organizacao\Service\OrgIes::DIRETORIO_IMAGEM);
        $serviceArquivo->setDiretorio($objDiretorio);

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['campId']) {
                $objOrgCampus = $this->getRepository()->find($arrDados['campId']);

                if (!$objOrgCampus) {
                    $this->setLastError('Registro de câmpus não existe!');

                    return false;
                }
            } else {
                $objOrgCampus = new \Organizacao\Entity\OrgCampus();
            }

            if ($arrDados['ies']) {
                $objOrgIes = $serviceOrgIes->getRepository()->find($arrDados['ies']);

                if (!$objOrgIes) {
                    $this->setLastError('Registro de instituicao não existe!');

                    return false;
                }

                $objOrgCampus->setIes($objOrgIes);
            }

            if ($arrDados['diretoriaPes']) {
                $objPessoaFisica = $servicePessoaFisica->getRepository()->find($arrDados['diretoriaPes']);

                if (!$objPessoaFisica) {
                    $this->setLastError('Pessoa selecionada para diretoria não existe!');

                    return false;
                }

                $objOrgCampus->setDiretoriaPes($objPessoaFisica);
            }

            if ($arrDados['secretariaPes']) {
                $objPessoaFisica = $servicePessoaFisica->getRepository()->find($arrDados['secretariaPes']);

                if (!$objPessoaFisica) {
                    $this->setLastError('Pessoa selecionada para secretaria não existe!');

                    return false;
                }

                $objOrgCampus->setSecretariaPes($objPessoaFisica);
            }

            $imagem            = $arrDados['vfile'][$arrDados['arq']];
            $imagemCarteirinha = $arrDados['vfile'][$arrDados['arqCarteirinha']];

            $objArq       = null;
            $objArqAntigo = null;

            if ($imagem) {
                if (empty($imagem['raw']) && $objOrgCampus->getArq()) {
                    /* @var $objArq \GerenciadorArquivos\Entity\Arquivo */
                    $objArq = $objOrgCampus->getArq();

                    if (!empty($objArq)) {
                        $objArqAntigo = $objArq->getArqId();
                    }

                    $objArq = null;
                } else {
                    $arrDados['arq'] = ($imagem['raw']) ? $imagem['raw'] : null;

                    if ($arrDados['arq'] && is_string($arrDados['arq'])) {
                        $objArq = $serviceArquivo->getRepository()->find($arrDados['arq']);
                    }
                }

                if ($imagem['file']['error'] == 0 && $imagem['file']['size'] != 0) {
                    try {
                        $objArq = $serviceArquivo->adicionar($imagem['file']);
                    } catch (\Exception $ex) {
                        throw new \Exception('Falha ao registrar imagem!');
                    }
                }
            } else {
                $objArq = null;
            }

            $arrDados['arq'] = $objArq;

            $objArqCarteirinha = null;

            if ($imagemCarteirinha) {
                if (empty($imagemCarteirinha['raw']) && $objOrgCampus->getArqCarteirinha()) {
                    /* @var $objArqCarteirinha \GerenciadorArquivos\Entity\Arquivo */
                    $objArqCarteirinha = $objOrgCampus->getArqCarteirinha();

                    if (!empty($objArqCarteirinha)) {
                        try {
                            $serviceArquivo->excluir($objArqCarteirinha);
                        } catch (\Exception $ex) {
                            throw new \Exception('Falha ao remover imagem antiga!');
                        }
                    }

                    $objArqCarteirinha = null;
                } else {
                    $arrDados['arqCarteirinha'] = ($imagemCarteirinha['raw']) ? $imagemCarteirinha['raw'] : null;

                    if ($arrDados['arqCarteirinha'] && is_string($arrDados['arqCarteirinha'])) {
                        $objArqCarteirinha = $serviceArquivo->getRepository()->find($arrDados['arqCarteirinha']);
                    }
                }

                if ($imagemCarteirinha['file']['error'] == 0 && $imagemCarteirinha['file']['size'] != 0) {
                    try {
                        $objArqCarteirinha = $serviceArquivo->adicionar($imagemCarteirinha['file']);
                    } catch (\Exception $ex) {
                        throw new \Exception('Falha ao registrar imagem!');
                    }
                }
            } else {
                $objArqCarteirinha = null;
            }

            $objOrgCampus->setArq($objArq);
            $objOrgCampus->setArqCarteirinha($objArqCarteirinha);
            $objOrgCampus->setCampNome($arrDados['campNome']);
            $objOrgCampus->setCampTelefone($arrDados['campTelefone']);
            $objOrgCampus->setCampEmail($arrDados['campEmail']);
            $objOrgCampus->setCampSite($arrDados['campSite']);

            $this->getEm()->persist($objOrgCampus);
            $this->getEm()->flush($objOrgCampus);

            if ($objArqAntigo) {
                try {
                    $serviceArquivo->excluir($objArqAntigo);
                } catch (\Exception $ex) {
                    throw new \Exception('Falha ao remover imagem antiga!');
                }
            }

            if (!$serviceInfraPredio->salvarMultiplos($arrDados, $objOrgCampus)) {
                throw new \Exception($serviceInfraPredio->getLastError());
            }

            $this->getEm()->commit();

            $arrDados['campId'] = $objOrgCampus->getCampId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de câmpus!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['ies']) {
            $errors[] = 'Por favor selecione a "instituição"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("\n", $errors));

            return false;
        }

        return true;
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * @param array $dados
     * @throws \Exception
     */
    public function adicionar(array $dados)
    {
        parent::begin();

        $dados['tipoEndereco'][0] = (new \Pessoa\Service\TipoEndereco($this->getEm()))->findOneBy(
            ['nome' => 'Comercial']
        )->getTipoEnderecoId();

        $endereco     = (new \Pessoa\Service\Endereco($this->getEm()))->adicionarMultiplos($dados);
        $dados['pes'] = $endereco[0]->getPes()->getPesId();
        parent::adicionar($dados);

        parent::commit();
    }

    /**
     * @param array $dados
     * @throws \Exception
     */
    public function edita(array $dados)
    {
        parent::begin();
        $dados['tipoEndereco'][0] = (new \Pessoa\Service\TipoEndereco($this->getEm()))->findOneBy(
            ['nome' => 'Comercial']
        )->getTipoEnderecoId();

        $endereco     = (new \Pessoa\Service\Endereco($this->getEm()))->editaMultiplos($dados);
        $dados['pes'] = $endereco[0]->getPes()->getPesId();
        parent::edita($dados);

        parent::commit();
    }

    /**
     * @return mixed
     */
    public function mountArrayCampi()
    {
        $campi = $this->em->getRepository("Organizacao\Entity\OrgCampus")->findAll();
        foreach ($campi as $campus) {
            $aux                         = $campus->toArray();
            $arrayCampi[$aux['camp_id']] = $aux['camp_nome'];
        }

        return $arrayCampi;
    }

    /**
     * @param array $query
     * @return array
     */
    public function autocomplete($query)
    {
        $result = array();
        $array  = $this->executeQuery(
            "SELECT
                                        camp_id, camp_nome
                                      FROM
                                        org_campus
                                      WHERE
                                        camp_nome
                                      LIKE
                                        '%{$query}%'"
        )->fetchAll();
        foreach ($array as $item) {
            $result[] = array(
                "value" => $item['camp_nome'],
                "data"  => $item['camp_id'],
            );
        }

        return $result;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function pesquisaForJson($params)
    {
        $sql      = 'SELECT * FROM org_campus WHERE';
        $campNome = false;
        $campId   = false;

        if ($params['q']) {
            $campNome = $params['q'];
        } elseif ($params['query']) {
            $campNome = $params['query'];
        }

        if ($params['campId']) {
            $campId = $params['campId'];
        }

        $parameters = array('camp_nome' => "{$campNome}%");
        $sql .= ' camp_nome LIKE :camp_nome';

        if ($campId) {
            $parameters['camp_id'] = $campId;
            $sql .= ' AND camp_id <> :camp_id';
        }

        $sql .= " ORDER BY camp_nome";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    /**
     * @return mixed
     */
    public function getMantenedoraNomes()
    {
        $query = "
        SELECT
            pes_nome instituicao,
            camp_nome campus
        FROM org_ies AS ies
        JOIN org_campus AS camp USING(ies_id)
        LEFT JOIN pessoa AS pIes USING(pes_id)";

        return $this->executeQuery($query)->fetch();
    }

    /**
     * Retorna informações sobre o câmpus, a IES e a mantenedora
     *
     * @param \Organizacao\Entity\OrgCampus|int|null $campus
     * @return array
     */
    public function retornaDadosInstituicao($campus = null)
    {
        $objCampus          = null;
        $serviceIes         = new \Organizacao\Service\OrgIes($this->getEm());
        $serviceInfraPredio = new \Infraestrutura\Service\InfraPredio($this->getEm());

        if (is_object($campus)) {
            $objCampus = $campus;
        } else {
            /* @var $objCampus \Organizacao\Entity\OrgCampus */
            $objCampus = $this->getRepository()->findOneBy(['campId' => $campus], ['campId' => 'asc']);
        }

        if (!$objCampus) {
            /* @var $objCampus \Organizacao\Entity\OrgCampus */
            $objCampus = $this->getRepository()->findOneBy([], ['campId' => 'asc']);
        }

        if (!$objCampus) {
            return array();
        }

        $logo = '';

        if ($objCampus->getArq()) {
            $logo = getcwd();
            $logo .= DIRECTORY_SEPARATOR;
            $logo .= $objCampus->getArq()->getDiretorio()->getArqDiretorioEndereco();
            $logo .= DIRECTORY_SEPARATOR;
            $logo .= $objCampus->getArq()->getArqChave();
            $logo = realpath($logo);
        }

        $arrPredio = [];
        $endereco  = '';

        $objInfraPredio = $serviceInfraPredio->getPredioPrincipalCampus($objCampus->getCampId());

        if ($objInfraPredio) {
            $endereco .= $objInfraPredio->getPredioLogradouro();
            $endereco .= ', ' . $objInfraPredio->getPredioNumero();
            $endereco .= ', ' . $objInfraPredio->getPredioBairro();
            $endereco .= ', ' . $objInfraPredio->getPredioCidade();
            $endereco .= ' - ' . $objInfraPredio->getPredioEstado();
            $endereco = preg_replace('/([\s,-] )([\s,-] ){1,}/', '$1', $endereco);

            $arrPredio = $objInfraPredio->toArray();

            $arrPredio['cidade'] = $objInfraPredio->getPredioCidade();
            $arrPredio['estado'] = $objInfraPredio->getPredioEstado();
        }

        $secretaria = $objCampus->getSecretariaPes() ? $objCampus->getSecretariaPes()->getPes()->getPesNome() : '';
        $diretoria  = $objCampus->getDiretoriaPes() ? $objCampus->getDiretoriaPes()->getPes()->getPesNome() : '';

        $dadosCampus = [
            'logo'       => $logo,
            'secretaria' => $secretaria,
            'diretoria'  => $diretoria,
            'campus'     => $objCampus->getCampNome(),
            'endereco'   => $endereco,
            'telefone'   => $objCampus->getCampTelefone(),
            'email'      => $objCampus->getCampEmail(),
            'site'       => $objCampus->getCampSite(),
        ];

        $dadosCampusIes = $serviceIes->retornaDadosInstituicao($objCampus->getIes());

        return array_merge(
            array_filter($arrPredio),
            array_filter($objCampus->toArray()),
            array_filter($dadosCampusIes),
            array_filter($dadosCampus)
        );
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('campId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Organizacao\Entity\OrgCampus */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getCampId(),
                $params['value'] => $objEntity->getCampNome()
            );
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['campId']) {
            $this->setLastError('Para remover um registro de câmpus é necessário especificar o código.');

            return false;
        }

        try {
            $objOrgCampus = $this->getRepository()->find($param['campId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objOrgCampus);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de campus.');

            return false;
        }

        return true;
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceCidade = new \Pessoa\Service\Cidade($this->getEm());

        $arrEstados = $serviceCidade->getEstadoArrSelect2();

        $view->setVariable("arrEstados", $arrEstados);
    }

    public function retornaCampus($param)
    {
        if (!$param) {
            $this->setLastError('Erro ao buscar o local, nenhum dado para referencia foi informado');

            return false;
        }

        $sql = "
                SELECT ies_nome instituto,oc.camp_nome  nome FROM selecao_locais sl
                INNER JOIN infra_predio ip ON ip.predio_id=sl.predio_id
                INNER JOIN org_campus oc ON oc.camp_id=ip.camp_id
                INNER JOIN org_ies o ON o.ies_id=oc.ies_id
                WHERE 1=1
        ";

        $where  = '';
        $parans = array();

        if ($param['local']) {
            $where = " and sl.selelocais_id={$param['local']} ";

            $parans['selecaolocal'] = $param['local'];
        }

        $sql .= $where;

        $result = $this->executeQueryWithParam($sql, $parans)->fetch();

        return $result;
    }

    public function retornarSelect2Campus()
    {
        $sql = '
            SELECT camp_id AS id,camp_nome "text" FROM org_campus';

        return $this->executeQuery($sql)->fetchAll();
    }
}