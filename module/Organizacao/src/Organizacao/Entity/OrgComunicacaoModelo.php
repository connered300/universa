<?php

namespace Organizacao\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * OrgComunicacaoModelo
 *
 * @ORM\Table(name="org__comunicacao_modelo")
 * @ORM\Entity
 * @LG\LG(id="modeloId",label="ModeloNome")
 * @Jarvis\Jarvis(title="Listagem de comunicação modelo",icon="fa fa-table")
 */
class OrgComunicacaoModelo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="modelo_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="modelo_id")
     * @LG\Labels\Attributes(text="código modelo")
     * @LG\Querys\Conditions(type="=")
     */
    private $modeloId;

    /**
     * @var \Organizacao\Entity\OrgComunicacaoTipo
     * @ORM\ManyToOne(targetEntity="Organizacao\Entity\OrgComunicacaoTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipo_id", referencedColumnName="tipo_id")
     * })
     */
    private $tipo;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario", referencedColumnName="id")
     * })
     */
    private $usuario;

    /**
     * @var string
     *
     * @ORM\Column(name="modelo_nome", type="string", nullable=false, length=45)
     * @LG\Labels\Property(name="modelo_nome")
     * @LG\Labels\Attributes(text="nome")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $modeloNome;

    /**
     * @var string
     *
     * @ORM\Column(name="modelo_tipo", type="string", nullable=true, length=3)
     * @LG\Labels\Property(name="modelo_tipo")
     * @LG\Labels\Attributes(text="tipo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $modeloTipo;

    /**
     * @var string
     *
     * @ORM\Column(name="modelo_assunto", type="string", nullable=false, length=250)
     * @LG\Labels\Property(name="modelo_assunto")
     * @LG\Labels\Attributes(text="assunto")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $modeloAssunto;

    /**
     * @var string
     *
     * @ORM\Column(name="modelo_conteudo", type="text", nullable=false)
     * @LG\Labels\Property(name="modelo_conteudo")
     * @LG\Labels\Attributes(text="conteúdo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $modeloConteudo;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="modelo_data_inicio", type="datetime", nullable=false)
     * @LG\Labels\Property(name="modelo_data_inicio")
     * @LG\Labels\Attributes(text="início data")
     * @LG\Querys\Conditions(type="=")
     */
    private $modeloDataInicio;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="modelo_data_fim", type="datetime", nullable=false)
     * @LG\Labels\Property(name="modelo_data_fim")
     * @LG\Labels\Attributes(text="fim data")
     * @LG\Querys\Conditions(type="=")
     */
    private $modeloDataFim;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Acesso\Entity\AcessoGrupo")
     * @ORM\JoinTable(name="org__comunicacao_modelo_grupo_permissao",
     *   joinColumns={
     *     @ORM\JoinColumn(name="modelo_id", referencedColumnName="modelo_id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="grupo_id", referencedColumnName="id")
     *   }
     * )
     */
    private $grupo;

    /**
     * @return integer
     */
    public function getModeloId()
    {
        return $this->modeloId;
    }

    /**
     * @param integer $modeloId
     * @return OrgComunicacaoModelo
     */
    public function setModeloId($modeloId)
    {
        $this->modeloId = $modeloId;

        return $this;
    }

    /**
     * @return \Organizacao\Entity\OrgComunicacaoTipo
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param \Organizacao\Entity\OrgComunicacaoTipo $tipo
     * @return OrgComunicacaoModelo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuario
     * @return OrgComunicacaoModelo
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * @return string
     */
    public function getModeloNome()
    {
        return $this->modeloNome;
    }

    /**
     * @param string $modeloNome
     * @return OrgComunicacaoModelo
     */
    public function setModeloNome($modeloNome)
    {
        $this->modeloNome = $modeloNome;

        return $this;
    }

    /**
     * @return string
     */
    public function getModeloTipo()
    {
        return $this->modeloTipo;
    }

    /**
     * @param string $modeloTipo
     * @return OrgComunicacaoModelo
     */
    public function setModeloTipo($modeloTipo)
    {
        $this->modeloTipo = $modeloTipo;

        return $this;
    }

    /**
     * @return string
     */
    public function getModeloAssunto()
    {
        return $this->modeloAssunto;
    }

    /**
     * @param string $modeloAssunto
     * @return OrgComunicacaoModelo
     */
    public function setModeloAssunto($modeloAssunto)
    {
        $this->modeloAssunto = $modeloAssunto;

        return $this;
    }

    /**
     * @return string
     */
    public function getModeloConteudo()
    {
        return $this->modeloConteudo;
    }

    /**
     * @param string $modeloConteudo
     * @return OrgComunicacaoModelo
     */
    public function setModeloConteudo($modeloConteudo)
    {
        $this->modeloConteudo = $modeloConteudo;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getModeloDataInicio($format = false)
    {
        $modeloDataInicio = $this->modeloDataInicio;

        if ($format && $modeloDataInicio) {
            $modeloDataInicio = $modeloDataInicio->format('d/m/Y H:i:s');
        }

        return $modeloDataInicio;
    }

    /**
     * @param \Datetime $modeloDataInicio
     * @return OrgComunicacaoModelo
     */
    public function setModeloDataInicio($modeloDataInicio)
    {
        if ($modeloDataInicio) {
            if (is_string($modeloDataInicio)) {
                $modeloDataInicio = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $modeloDataInicio
                );
                $modeloDataInicio = new \Datetime($modeloDataInicio);
            }
        } else {
            $modeloDataInicio = null;
        }
        $this->modeloDataInicio = $modeloDataInicio;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getModeloDataFim($format = false)
    {
        $modeloDataFim = $this->modeloDataFim;

        if ($format && $modeloDataFim) {
            $modeloDataFim = $modeloDataFim->format('d/m/Y H:i:s');
        }

        return $modeloDataFim;
    }

    /**
     * @param \Datetime $modeloDataFim
     * @return OrgComunicacaoModelo
     */
    public function setModeloDataFim($modeloDataFim)
    {
        if ($modeloDataFim) {
            if (is_string($modeloDataFim)) {
                $modeloDataFim = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $modeloDataFim
                );
                $modeloDataFim = new \Datetime($modeloDataFim);
            }
        } else {
            $modeloDataFim = null;
        }
        $this->modeloDataFim = $modeloDataFim;

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGrupo()
    {
        return $this->grupo;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $grupo
     * @return OrgComunicacaoModelo
     */
    public function setGrupo($grupo)
    {
        $this->grupo = $grupo;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'modeloId'         => $this->getModeloId(),
            'tipo'             => $this->getTipo(),
            'usuario'          => $this->getUsuario(),
            'modeloNome'       => $this->getModeloNome(),
            'modeloTipo'       => $this->getModeloTipo(),
            'modeloAssunto'    => $this->getModeloAssunto(),
            'modeloConteudo'   => $this->getModeloConteudo(),
            'modeloDataInicio' => $this->getModeloDataInicio(true),
            'modeloDataFim'    => $this->getModeloDataFim(true),
            'grupo'            => $this->getGrupo()->toArray()
        );

        $array['tipo']    = $this->getTipo() ? $this->getTipo()->getTipoId() : null;
        $array['usuario'] = $this->getUsuario() ? $this->getUsuario()->getId() : null;

        return $array;
    }
}
