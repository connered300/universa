<?php

    namespace Organizacao\Entity;

    use Doctrine\ORM\Mapping as ORM;
    use DoctrineORMModule\Proxy\__CG__\Pessoa\Entity\PessoaJuridica;
    use VersaSpine\ListGenerator\Annotations\Entity as LG;
    use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

    /**
     * OrgIes
     *
     * @ORM\Table(name="org_ies", indexes={@ORM\Index(name="fk_ies_pessoa_juridica1_idx", columns={"pes_id"}), @ORM\Index(name="fk_pessoa_ies_pessoa_ies1_idx", columns={"ies_id_sede"}), @ORM\Index(name="fk_pessoa_ies_pessoa1_idx", columns={"pes_id_mantenedora"})})
     * @ORM\Entity
     * @LG\LG(id="ies_id",label="ies_id")
     * @Jarvis\Jarvis(title="Lista de Instituições de ensino",icon="fa fa-table")
     */
    class OrgIes
    {
        /**
         * @var integer
         *
         * @ORM\Column(name="ies_id", type="integer", nullable=false)
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="IDENTITY")
         * @LG\Labels\Property(name="ies_id")
         * @LG\Labels\Attributes(text="Índice",icon="fa fa-user")
         * @LG\Querys\Conditions(type="=")
         */
        private $iesId;

        /**
         * @var \Pessoa\Entity\PessoaJuridica
         *
         * @ORM\GeneratedValue(strategy="NONE")
         * @ORM\ManyToOne(targetEntity="Pessoa\Entity\PessoaJuridica")
         * @ORM\JoinColumns({
         *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
         * })
         */
        private $pes;
        /**
         * @var string
         *
         * @ORM\Column(name="ies_nome", type="string", nullable=false, length=255)
         * @LG\Labels\Property(name="ies_nome")
         * @LG\Labels\Attributes(text="nome")
         * @LG\Querys\Conditions(type="LIKE")
         */
        private $iesNome;

        /**
         * @var string
         *
         * @ORM\Column(name="ies_credenciamento", type="string", nullable=true)
         * @LG\Labels\Property(name="ies_nome")
         * @LG\Labels\Attributes(text="credenciamento")
         * @LG\Querys\Conditions(type="LIKE")
         */
        private $iesCredenciamento;

        /**
         * @var string
         *
         * @ORM\Column(name="ies_sigla", type="string", nullable=true, length=50)
         * @LG\Labels\Property(name="ies_sigla")
         * @LG\Labels\Attributes(text="sigla")
         * @LG\Querys\Conditions(type="LIKE")
         */
        private $iesSigla;

        /**
         * @var integer
         *
         * @ORM\Column(name="ies_end_numero", type="integer", nullable=true, length=11)
         * @LG\Labels\Property(name="ies_end_numero")
         * @LG\Labels\Attributes(text="número endereço")
         * @LG\Querys\Conditions(type="=")
         */
        private $iesEndNumero;

        /**
         * @var string
         *
         * @ORM\Column(name="ies_end_cep", type="string", nullable=true, length=45)
         * @LG\Labels\Property(name="ies_end_cep")
         * @LG\Labels\Attributes(text="CEP endereço")
         * @LG\Querys\Conditions(type="LIKE")
         */
        private $iesEndCep;

        /**
         * @var string
         *
         * @ORM\Column(name="ies_end_bairro", type="string", nullable=true, length=45)
         * @LG\Labels\Property(name="ies_end_bairro")
         * @LG\Labels\Attributes(text="bairro endereço")
         * @LG\Querys\Conditions(type="LIKE")
         */
        private $iesEndBairro;

        /**
         * @var string
         *
         * @ORM\Column(name="ies_end_cidade", type="string", nullable=true, length=45)
         * @LG\Labels\Property(name="ies_end_cidade")
         * @LG\Labels\Attributes(text="cidade endereço")
         * @LG\Querys\Conditions(type="LIKE")
         */
        private $iesEndCidade;

        /**
         * @var string
         *
         * @ORM\Column(name="ies_end_estado", type="string", nullable=true, length=45)
         * @LG\Labels\Property(name="ies_end_estado")
         * @LG\Labels\Attributes(text="estado endereço")
         * @LG\Querys\Conditions(type="LIKE")
         */
        private $iesEndEstado;

        /**
         * @var string
         *
         * @ORM\Column(name="ies_end_logradouro", type="string", nullable=true, length=45)
         * @LG\Labels\Property(name="ies_end_logradouro")
         * @LG\Labels\Attributes(text="logradouro endereço")
         * @LG\Querys\Conditions(type="LIKE")
         */
        private $iesEndLogradouro;

        /**
         * @var string
         *
         * @ORM\Column(name="ies_telefone", type="string", nullable=true, length=15)
         * @LG\Labels\Property(name="ies_telefone")
         * @LG\Labels\Attributes(text="telefone")
         * @LG\Querys\Conditions(type="LIKE")
         */
        private $iesTelefone;

        /**
         * @var string
         *
         * @ORM\Column(name="ies_email", type="string", nullable=true, length=100)
         * @LG\Labels\Property(name="ies_email")
         * @LG\Labels\Attributes(text="e-mail")
         * @LG\Querys\Conditions(type="LIKE")
         */
        private $iesEmail;

        /**
         * @var string
         *
         * @ORM\Column(name="ies_numero_mec", type="string", length=45, nullable=false)
         * @LG\Labels\Property(name="ies_numero_mec")
         * @LG\Labels\Attributes(text="Número do MEC",icon="fa fa-user")
         * @LG\Querys\Conditions(type="=")
         */
        private $iesNumeroMec;

        /**
         * @var string
         *
         * @ORM\Column(name="ies_organizacao_academica", type="string", length=45, nullable=false)
         */
        private $iesOrganizacaoAcademica;

        /**
         * @var \Pessoa\Entity\PessoaJuridica
         *
         * @ORM\ManyToOne(targetEntity="Pessoa\Entity\PessoaJuridica")
         * @ORM\JoinColumns({
         *   @ORM\JoinColumn(name="pes_id_mantenedora", referencedColumnName="pes_id")
         * })
         */
        private $pesMantenedora;

        /**
         * @var OrgIes
         *
         * @ORM\ManyToOne(targetEntity="OrgIes")
         * @ORM\JoinColumns({
         *   @ORM\JoinColumn(name="ies_id_sede", referencedColumnName="ies_id")
         * })
         */
        private $iesSede;

        /**
         * @var \Pessoa\Entity\PessoaFisica
         *
         * @ORM\ManyToOne(targetEntity="Pessoa\Entity\PessoaFisica")
         * @ORM\JoinColumns({
         *   @ORM\JoinColumn(name="secretaria_pes_id", referencedColumnName="pes_id")
         * })
         */
        private $secretariaPes;

        /**
         * @var \Pessoa\Entity\PessoaFisica
         *
         * @ORM\ManyToOne(targetEntity="Pessoa\Entity\PessoaFisica")
         * @ORM\JoinColumns({
         *   @ORM\JoinColumn(name="diretoria_pes_id", referencedColumnName="pes_id")
         * })
         */
        private $diretoriaPes;

        /**
         * @var \GerenciadorArquivos\Entity\Arquivo
         *
         * @ORM\ManyToOne(targetEntity="GerenciadorArquivos\Entity\Arquivo",cascade={"persist"})
         * @ORM\JoinColumns({
         *   @ORM\JoinColumn(name="arq_id", referencedColumnName="arq_id")
         * })
         */
        private $arq;

        /**
         * @var \GerenciadorArquivos\Entity\Arquivo
         *
         * @ORM\ManyToOne(targetEntity="GerenciadorArquivos\Entity\Arquivo", cascade={"persist"})
         * @ORM\JoinColumns({
         *   @ORM\JoinColumn(name="arq_id_carteirinha", referencedColumnName="arq_id")
         * })
         */
        private $arqCarteirinha;

        public function __construct(array $data = array())
        {
            (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
        }

        /**
         * @return int
         */
        public function getIesId()
        {
            return $this->iesId;
        }

        /**
         * @param int $iesId
         * @return OrgIes
         */
        public function setIesId($iesId)
        {
            $this->iesId = $iesId;

            return $this;
        }

        /**
         * @return \Pessoa\Entity\Pessoa
         */
        public function getPes()
        {
            return $this->pes;
        }

        /**
         * @param \Pessoa\Entity\PessoaJuridica $pes
         * @return PessoaJuridica
         */
        public function setPes($pes)
        {
            $this->pes = $pes;

            return $this;
        }

        /**
         * @return string
         */
        public function getIesNome()
        {
            return $this->iesNome;
        }

        /**
         * @param string $iesNome
         * @return OrgIes
         */
        public function setIesNome($iesNome)
        {
            $this->iesNome = $iesNome;

            return $this;
        }

        /**
         * @return string
         */
        public function getIesSigla()
        {
            return $this->iesSigla;
        }

        /**
         * @param string $iesSigla
         * @return OrgIes
         */
        public function setIesSigla($iesSigla)
        {
            $this->iesSigla = $iesSigla;

            return $this;
        }

        /**
         * @return integer
         */
        public function getIesEndNumero()
        {
            return $this->iesEndNumero;
        }

        /**
         * @param integer $iesEndNumero
         * @return OrgIes
         */
        public function setIesEndNumero($iesEndNumero)
        {
            $this->iesEndNumero = $iesEndNumero;

            return $this;
        }

        /**
         * @return string
         */
        public function getIesEndCep()
        {
            return $this->iesEndCep;
        }

        /**
         * @param string $iesEndCep
         * @return OrgIes
         */
        public function setIesEndCep($iesEndCep)
        {
            $this->iesEndCep = $iesEndCep;

            return $this;
        }

        /**
         * @return string
         */
        public function getIesEndBairro()
        {
            return $this->iesEndBairro;
        }

        /**
         * @param string $iesEndBairro
         * @return OrgIes
         */
        public function setIesEndBairro($iesEndBairro)
        {
            $this->iesEndBairro = $iesEndBairro;

            return $this;
        }

        /**
         * @return string
         */
        public function getIesEndCidade()
        {
            return $this->iesEndCidade;
        }

        /**
         * @param string $iesEndCidade
         * @return OrgIes
         */
        public function setIesEndCidade($iesEndCidade)
        {
            $this->iesEndCidade = $iesEndCidade;

            return $this;
        }

        /**
         * @return string
         */
        public function getIesEndEstado()
        {
            return $this->iesEndEstado;
        }

        /**
         * @param string $iesEndEstado
         * @return OrgIes
         */
        public function setIesEndEstado($iesEndEstado)
        {
            $this->iesEndEstado = $iesEndEstado;

            return $this;
        }

        /**
         * @return string
         */
        public function getIesEndLogradouro()
        {
            return $this->iesEndLogradouro;
        }

        /**
         * @param string $iesEndLogradouro
         * @return OrgIes
         */
        public function setIesEndLogradouro($iesEndLogradouro)
        {
            $this->iesEndLogradouro = $iesEndLogradouro;

            return $this;
        }

        /**
         * @return string
         */
        public function getIesTelefone()
        {
            return $this->iesTelefone;
        }

        /**
         * @param string $iesTelefone
         * @return OrgIes
         */
        public function setIesTelefone($iesTelefone)
        {
            $this->iesTelefone = $iesTelefone;

            return $this;
        }

        /**
         * @return string
         */
        public function getIesEmail()
        {
            return $this->iesEmail;
        }

        /**
         * @param string $iesEmail
         * @return OrgIes
         */
        public function setIesEmail($iesEmail)
        {
            $this->iesEmail = $iesEmail;

            return $this;
        }

        /**
         * @return string
         */
        public function getIesNumeroMec()
        {
            return $this->iesNumeroMec;
        }

        /**
         * @param string $iesNumeroMec
         * @return OrgIes
         */
        public function setIesNumeroMec($iesNumeroMec)
        {
            $this->iesNumeroMec = $iesNumeroMec;

            return $this;
        }

        /**
         * @return string
         */
        public function getIesOrganizacaoAcademica()
        {
            return $this->iesOrganizacaoAcademica;
        }

        /**
         * @param string $iesOrganizacaoAcademica
         * @return OrgIes
         */
        public function setIesOrganizacaoAcademica($iesOrganizacaoAcademica)
        {
            $this->iesOrganizacaoAcademica = $iesOrganizacaoAcademica;

            return $this;
        }

        /**
         * @return \Pessoa\Entity\PessoaJuridica
         */
        public function getPesMantenedora()
        {
            return $this->pesMantenedora;
        }

        /**
         * @param \Pessoa\Entity\PessoaJuridica $pesMantenedora
         * @return OrgIes
         */
        public function setPesMantenedora($pesMantenedora)
        {
            $this->pesMantenedora = $pesMantenedora;

            return $this;
        }

        /**
         * @return OrgIes
         */
        public function getIesSede()
        {
            return $this->iesSede;
        }

        /**
         * @param OrgIes $iesSede
         * @return OrgIes
         */
        public function setIesSede($iesSede)
        {
            $this->iesSede = $iesSede;

            return $this;
        }

        /**
         * @return \Pessoa\Entity\PessoaFisica
         */
        public function getSecretariaPes()
        {
            return $this->secretariaPes;
        }

        /**
         * @param \Pessoa\Entity\PessoaFisica $secretariaPes
         * @return OrgIes
         */
        public function setSecretariaPes($secretariaPes)
        {
            $this->secretariaPes = $secretariaPes;

            return $this;
        }

        /**
         * @return \Pessoa\Entity\PessoaFisica
         */
        public function getDiretoriaPes()
        {
            return $this->diretoriaPes;
        }

        /**
         * @param \Pessoa\Entity\PessoaFisica $diretoriaPes
         * @return OrgIes
         */
        public function setDiretoriaPes($diretoriaPes)
        {
            $this->diretoriaPes = $diretoriaPes;

            return $this;
        }

        /**
         * @return \GerenciadorArquivos\Entity\Arquivo
         */
        public function getArq()
        {
            return $this->arq;
        }

        /**
         * @param \GerenciadorArquivos\Entity\Arquivo $arq
         * @return OrgIes
         */
        public function setArq($arq)
        {
            $this->arq = $arq;

            return $this;
        }

        /**
         * @return string
         */
        public function getIesCredenciamento()
        {
            return $this->iesCredenciamento;
        }

        /**
         * @param string $iesCredenciamento
         * @return OrgIes
         */
        public function setIesCredenciamento($iesCredenciamento)
        {
            $this->iesCredenciamento = $iesCredenciamento;

            return $this;
        }

        public function toArray()
        {
            return array(
                'iesSede'                 => $this->getIesSede(),
                'secretariaPes'           => $this->getSecretariaPes(),
                'iesNome'                 => $this->getIesNome(),
                'iesSigla'                => $this->getIesSigla(),
                'iesEndNumero'            => $this->getIesEndNumero(),
                'iesEndCep'               => $this->getIesEndCep(),
                'iesEndBairro'            => $this->getIesEndBairro(),
                'iesEndCidade'            => $this->getIesEndCidade(),
                'iesEndEstado'            => $this->getIesEndEstado(),
                'iesEndLogradouro'        => $this->getIesEndLogradouro(),
                'iesTelefone'             => $this->getIesTelefone(),
                'iesEmail'                => $this->getIesEmail(),
                'iesNumeroMec'            => $this->getIesNumeroMec(),
                'iesOrganizacaoAcademica' => $this->getIesOrganizacaoAcademica(),
                'iesId'                   => $this->getIesId(),
                'diretoriaPes'            => $this->getDiretoriaPes(),
                'pesMantenedora'          => $this->getPesMantenedora(),
                'arq'                     => $this->getArq(),
                'arqCarteirinha'          => $this->getArqCarteirinha(),
                'iesCredenciamento'       => $this->getIesCredenciamento()
            );
        }

        /**
         * @return \GerenciadorArquivos\Entity\Arquivo
         */
        public function getArqCarteirinha()
        {
            return $this->arqCarteirinha;
        }

        /**
         * @param \GerenciadorArquivos\Entity\Arquivo $arqCarteirinha
         * @return OrgIes
         */
        public function setArqCarteirinha($arqCarteirinha)
        {
            $this->arqCarteirinha = $arqCarteirinha;

            return $this;
        }
    }
