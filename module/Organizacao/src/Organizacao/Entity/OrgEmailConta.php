<?php

namespace Organizacao\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * OrgEmailConta
 *
 * @ORM\Table(name="org__email_conta")
 * @ORM\Entity
 * @LG\LG(id="contaId",label="ContaNome")
 * @Jarvis\Jarvis(title="Listagem de conta de e-mail",icon="fa fa-table")
 */
class OrgEmailConta
{
    /**
     * @var integer
     *
     * @ORM\Column(name="conta_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="conta_id")
     * @LG\Labels\Attributes(text="código conta")
     * @LG\Querys\Conditions(type="=")
     */
    private $contaId;
    /**
     * @var \Acesso\Entity\AcessoPessoas
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario", referencedColumnName="id")
     * })
     */
    private $usuario;
    /**
     * @var string
     *
     * @ORM\Column(name="conta_nome", type="string", nullable=false, length=100)
     * @LG\Labels\Property(name="conta_nome")
     * @LG\Labels\Attributes(text="nome")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $contaNome;
    /**
     * @var string
     *
     * @ORM\Column(name="conta_host", type="text", nullable=false)
     * @LG\Labels\Property(name="conta_host")
     * @LG\Labels\Attributes(text="endereço")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $contaHost;
    /**
     * @var string
     *
     * @ORM\Column(name="conta_usuario", type="string", nullable=false, length=100)
     * @LG\Labels\Property(name="conta_usuario")
     * @LG\Labels\Attributes(text="usuário")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $contaUsuario;
    /**
     * @var string
     *
     * @ORM\Column(name="conta_senha", type="string", nullable=false, length=100)
     * @LG\Labels\Property(name="conta_senha")
     * @LG\Labels\Attributes(text="senha")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $contaSenha;
    /**
     * @var integer
     *
     * @ORM\Column(name="conta_porta", type="integer", nullable=false, length=11)
     * @LG\Labels\Property(name="conta_porta")
     * @LG\Labels\Attributes(text="porta")
     * @LG\Querys\Conditions(type="=")
     */
    private $contaPorta;
    /**
     * @var string
     *
     * @ORM\Column(name="conta_ssl", type="string", nullable=true, length=3)
     * @LG\Labels\Property(name="conta_ssl")
     * @LG\Labels\Attributes(text="ssl")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $contaSsl;

    /**
     * @var string
     *
     * @ORM\Column(name="conta_imap_host", type="text", nullable=true)
     * @LG\Labels\Property(name="conta_imap_host")
     * @LG\Labels\Attributes(text="endereço imap")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $contaImapHost;

    /**
     * @var integer
     *
     * @ORM\Column(name="conta_imap_porta", type="integer", nullable=true, length=11)
     * @LG\Labels\Property(name="conta_imap_porta")
     * @LG\Labels\Attributes(text="porta imap")
     * @LG\Querys\Conditions(type="=")
     */
    private $contaImapPorta;

    /**
     * @var string
     *
     * @ORM\Column(name="conta_imap_ssl", type="string", nullable=true, length=3)
     * @LG\Labels\Property(name="conta_imap_ssl")
     * @LG\Labels\Attributes(text="ssl imap")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $contaImapSsl;

    /**
     * @var string
     *
     * @ORM\Column(name="conta_lead", type="string", nullable=false, length=3)
     * @LG\Labels\Property(name="conta_lead")
     * @LG\Labels\Attributes(text="lead")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $contaLead;

    /**
     * @var string
     *
     * @ORM\Column(name="conta_ativada", type="string", nullable=false, length=3)
     * @LG\Labels\Property(name="conta_ativada")
     * @LG\Labels\Attributes(text="ativada")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $contaAtivada;

    /**
     * @return integer
     */
    public function getContaId()
    {
        return $this->contaId;
    }

    /**
     * @param integer $contaId
     * @return OrgEmailConta
     */
    public function setContaId($contaId)
    {
        $this->contaId = $contaId;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuario
     * @return OrgEmailConta
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * @return string
     */
    public function getContaNome()
    {
        return $this->contaNome;
    }

    /**
     * @param string $contaNome
     * @return OrgEmailConta
     */
    public function setContaNome($contaNome)
    {
        $this->contaNome = $contaNome;

        return $this;
    }

    /**
     * @return string
     */
    public function getContaHost()
    {
        return $this->contaHost;
    }

    /**
     * @param string $contaHost
     * @return OrgEmailConta
     */
    public function setContaHost($contaHost)
    {
        $this->contaHost = $contaHost;

        return $this;
    }

    /**
     * @return string
     */
    public function getContaUsuario()
    {
        return $this->contaUsuario;
    }

    /**
     * @param string $contaUsuario
     * @return OrgEmailConta
     */
    public function setContaUsuario($contaUsuario)
    {
        $this->contaUsuario = $contaUsuario;

        return $this;
    }

    /**
     * @return string
     */
    public function getContaSenha()
    {
        return $this->contaSenha;
    }

    /**
     * @param string $contaSenha
     * @return OrgEmailConta
     */
    public function setContaSenha($contaSenha)
    {
        $this->contaSenha = $contaSenha;

        return $this;
    }

    /**
     * @return integer
     */
    public function getContaPorta()
    {
        return $this->contaPorta;
    }

    /**
     * @param integer $contaPorta
     * @return OrgEmailConta
     */
    public function setContaPorta($contaPorta)
    {
        $this->contaPorta = $contaPorta;

        return $this;
    }

    /**
     * @return string
     */
    public function getContaSsl()
    {
        return $this->contaSsl;
    }

    /**
     * @param string $contaSsl
     * @return OrgEmailConta
     */
    public function setContaSsl($contaSsl)
    {
        $this->contaSsl = $contaSsl ? $contaSsl : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getContaImapHost()
    {
        return $this->contaImapHost;
    }

    /**
     * @param string $contaImapHost
     * @return OrgEmailConta
     */
    public function setContaImapHost($contaImapHost)
    {
        $this->contaImapHost = $contaImapHost;

        return $this;
    }

    /**
     * @return integer
     */
    public function getContaImapPorta()
    {
        return $this->contaImapPorta;
    }

    /**
     * @param integer $contaImapPorta
     * @return OrgEmailConta
     */
    public function setContaImapPorta($contaImapPorta)
    {
        $this->contaImapPorta = $contaImapPorta;

        return $this;
    }

    /**
     * @return string
     */
    public function getContaImapSsl()
    {
        return $this->contaImapSsl;
    }

    /**
     * @param string $contaImapSsl
     * @return OrgEmailConta
     */
    public function setContaImapSsl($contaImapSsl)
    {
        $this->contaImapSsl = $contaImapSsl;

        return $this;
    }

    /**
     * @return string
     */
    public function getContaLead()
    {
        return $this->contaLead;
    }

    /**
     * @param string $contaLead
     * @return OrgEmailConta
     */
    public function setContaLead($contaLead)
    {
        $this->contaLead = $contaLead;

        return $this;
    }

    /**
     * @return string
     */
    public function getContaAtivada()
    {
        return $this->contaAtivada;
    }

    /**
     * @param string $contaAtivada
     * @return OrgEmailConta
     */
    public function setContaAtivada($contaAtivada)
    {
        $this->contaAtivada = $contaAtivada;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'contaId'        => $this->getContaId(),
            'usuario'        => $this->getUsuario(),
            'contaNome'      => $this->getContaNome(),
            'contaHost'      => $this->getContaHost(),
            'contaUsuario'   => $this->getContaUsuario(),
            'contaSenha'     => $this->getContaSenha(),
            'contaPorta'     => $this->getContaPorta(),
            'contaSsl'       => $this->getContaSsl(),
            'contaImapHost'  => $this->getContaImapHost(),
            'contaImapPorta' => $this->getContaImapPorta(),
            'contaImapSsl'   => $this->getContaImapSsl(),
            'contaLead'      => $this->getContaLead(),
            'contaAtivada'   => $this->getContaAtivada(),
        );

        $array['usuario'] = $this->getUsuario() ? $this->getUsuario()->getId() : null;

        return $array;
    }
}
