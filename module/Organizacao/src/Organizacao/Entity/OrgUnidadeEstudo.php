<?php

namespace Organizacao\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * OrgUnidadeEstudo
 *
 * @ORM\Table(name="org__unidade_estudo")
 * @ORM\Entity
 * @LG\LG(id="unidade_id",label="unidade_nome")
 * @Jarvis\Jarvis(title="Lista de unidade estudo",icon="fa fa-table")
 */
class OrgUnidadeEstudo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="unidade_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="unidade_id")
     * @LG\Labels\Attributes(text="código unidade")
     * @LG\Querys\Conditions(type="=")
     */
    private $unidadeId;
    /**
     * @var string
     *
     * @ORM\Column(name="unidade_nome", type="string", nullable=false, length=45)
     * @LG\Labels\Property(name="unidade_nome")
     * @LG\Labels\Attributes(text="nome unidade")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $unidadeNome;
    /**
     * @var \Organizacao\Entity\OrgCampus
     *
     * @ORM\ManyToOne(targetEntity="Organizacao\Entity\OrgCampus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="camp_id", referencedColumnName="camp_id")
     * })
     */
    private $camp;
    /**
     * @var integer
     *
     * @ORM\Column(name="unidade_end_numero", type="integer", nullable=true, length=11)
     * @LG\Labels\Property(name="unidade_end_numero")
     * @LG\Labels\Attributes(text="número endereço unidade")
     * @LG\Querys\Conditions(type="=")
     */
    private $unidadeEndNumero;
    /**
     * @var string
     *
     * @ORM\Column(name="unidade_end_cep", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="unidade_end_cep")
     * @LG\Labels\Attributes(text="CEP endereço unidade")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $unidadeEndCep;
    /**
     * @var string
     *
     * @ORM\Column(name="unidade_end_bairro", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="unidade_end_bairro")
     * @LG\Labels\Attributes(text="bairro endereço unidade")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $unidadeEndBairro;
    /**
     * @var string
     *
     * @ORM\Column(name="unidade_end_cidade", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="unidade_end_cidade")
     * @LG\Labels\Attributes(text="cidade endereço unidade")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $unidadeEndCidade;
    /**
     * @var string
     *
     * @ORM\Column(name="unidade_end_estado", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="unidade_end_estado")
     * @LG\Labels\Attributes(text="estado endereço unidade")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $unidadeEndEstado;
    /**
     * @var string
     *
     * @ORM\Column(name="unidade_end_logradouro", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="unidade_end_logradouro")
     * @LG\Labels\Attributes(text="logradouro endereço unidade")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $unidadeEndLogradouro;
    /**
     * @var string
     *
     * @ORM\Column(name="unidade_telefone", type="string", nullable=true, length=15)
     * @LG\Labels\Property(name="unidade_telefone")
     * @LG\Labels\Attributes(text="telefone unidade")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $unidadeTelefone;
    /**
     * @var string
     *
     * @ORM\Column(name="unidade_email", type="string", nullable=true, length=100)
     * @LG\Labels\Property(name="unidade_email")
     * @LG\Labels\Attributes(text="e-mail unidade")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $unidadeEmail;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Organizacao\Entity\OrgAgenteEducacional", inversedBy="unidades")
     * @ORM\JoinTable(name="org__unidade_agente",
     *   joinColumns={
     *     @ORM\JoinColumn(name="unidade_id", referencedColumnName="unidade_id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     *   }
     * )
     */
    private $agentes;

    public function getAgentes()
    {
        return $this->agentes;
    }

    public function setAgentes(\Doctrine\Common\Collections\Collection $agentes)
    {
        $this->agentes = $agentes;

        return $this;
    }

    /**
     * @return integer
     */
    public function getUnidadeId()
    {
        return $this->unidadeId;
    }

    /**
     * @param integer $unidadeId
     * @return OrgUnidadeEstudo
     */
    public function setUnidadeId($unidadeId)
    {
        $this->unidadeId = $unidadeId;

        return $this;
    }

    /**
     * @return string
     */
    public function getUnidadeNome()
    {
        return $this->unidadeNome;
    }

    /**
     * @param string $unidadeNome
     * @return OrgUnidadeEstudo
     */
    public function setUnidadeNome($unidadeNome)
    {
        $this->unidadeNome = $unidadeNome;

        return $this;
    }

    /**
     * @return \Organizacao\Entity\OrgCampus
     */
    public function getCamp()
    {
        return $this->camp;
    }

    /**
     * @param \Organizacao\Entity\OrgCampus $camp
     * @return OrgUnidadeEstudo
     */
    public function setCamp($camp)
    {
        $this->camp = $camp;

        return $this;
    }

    /**
     * @return integer
     */
    public function getUnidadeEndNumero()
    {
        return $this->unidadeEndNumero;
    }

    /**
     * @param integer $unidadeEndNumero
     * @return OrgUnidadeEstudo
     */
    public function setUnidadeEndNumero($unidadeEndNumero)
    {
        $this->unidadeEndNumero = $unidadeEndNumero ? $unidadeEndNumero : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getUnidadeEndCep()
    {
        return $this->unidadeEndCep;
    }

    /**
     * @param string $unidadeEndCep
     * @return OrgUnidadeEstudo
     */
    public function setUnidadeEndCep($unidadeEndCep)
    {
        $this->unidadeEndCep = $unidadeEndCep;

        return $this;
    }

    /**
     * @return string
     */
    public function getUnidadeEndBairro()
    {
        return $this->unidadeEndBairro;
    }

    /**
     * @param string $unidadeEndBairro
     * @return OrgUnidadeEstudo
     */
    public function setUnidadeEndBairro($unidadeEndBairro)
    {
        $this->unidadeEndBairro = $unidadeEndBairro;

        return $this;
    }

    /**
     * @return string
     */
    public function getUnidadeEndCidade()
    {
        return $this->unidadeEndCidade;
    }

    /**
     * @param string $unidadeEndCidade
     * @return OrgUnidadeEstudo
     */
    public function setUnidadeEndCidade($unidadeEndCidade)
    {
        $this->unidadeEndCidade = $unidadeEndCidade;

        return $this;
    }

    /**
     * @return string
     */
    public function getUnidadeEndEstado()
    {
        return $this->unidadeEndEstado;
    }

    /**
     * @param string $unidadeEndEstado
     * @return OrgUnidadeEstudo
     */
    public function setUnidadeEndEstado($unidadeEndEstado)
    {
        $this->unidadeEndEstado = $unidadeEndEstado;

        return $this;
    }

    /**
     * @return string
     */
    public function getUnidadeEndLogradouro()
    {
        return $this->unidadeEndLogradouro;
    }

    /**
     * @param string $unidadeEndLogradouro
     * @return OrgUnidadeEstudo
     */
    public function setUnidadeEndLogradouro($unidadeEndLogradouro)
    {
        $this->unidadeEndLogradouro = $unidadeEndLogradouro;

        return $this;
    }

    /**
     * @return string
     */
    public function getUnidadeTelefone()
    {
        return $this->unidadeTelefone;
    }

    /**
     * @param string $unidadeTelefone
     * @return OrgUnidadeEstudo
     */
    public function setUnidadeTelefone($unidadeTelefone)
    {
        $this->unidadeTelefone = $unidadeTelefone;

        return $this;
    }

    /**
     * @return string
     */
    public function getUnidadeEmail()
    {
        return $this->unidadeEmail;
    }

    /**
     * @param string $unidadeEmail
     * @return OrgUnidadeEstudo
     */
    public function setUnidadeEmail($unidadeEmail)
    {
        $this->unidadeEmail = $unidadeEmail;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'unidadeId'            => $this->getUnidadeId(),
            'unidadeNome'          => $this->getUnidadeNome(),
            'camp'                 => $this->getCamp(),
            'unidadeEndNumero'     => $this->getUnidadeEndNumero(),
            'unidadeEndCep'        => $this->getUnidadeEndCep(),
            'unidadeEndBairro'     => $this->getUnidadeEndBairro(),
            'unidadeEndCidade'     => $this->getUnidadeEndCidade(),
            'unidadeEndEstado'     => $this->getUnidadeEndEstado(),
            'unidadeEndLogradouro' => $this->getUnidadeEndLogradouro(),
            'unidadeTelefone'      => $this->getUnidadeTelefone(),
            'unidadeEmail'         => $this->getUnidadeEmail(),
            'agentes'              => $this->getAgentes(),
        );

        $array['camp'] = $this->getCamp() ? $this->getCamp()->getCampId() : null;

        return $array;
    }
}
