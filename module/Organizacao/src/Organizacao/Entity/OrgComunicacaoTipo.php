<?php

namespace Organizacao\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * OrgComunicacaoTipo
 *
 * @ORM\Table(name="org__comunicacao_tipo")
 * @ORM\Entity
 * @LG\LG(id="tipoId",label="TipoDescricao")
 * @Jarvis\Jarvis(title="Listagem de tipo de comunicação",icon="fa fa-table")
 */
class OrgComunicacaoTipo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="tipo_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="tipo_id")
     * @LG\Labels\Attributes(text="código tipo")
     * @LG\Querys\Conditions(type="=")
     */
    private $tipoId;
    /**
     * @var string
     *
     * @ORM\Column(name="tipo_descricao", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="tipo_descricao")
     * @LG\Labels\Attributes(text="descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $tipoDescricao;
    /**
     * @var string
     *
     * @ORM\Column(name="tipo_contexto", type="string", nullable=false, length=45)
     * @LG\Labels\Property(name="tipo_descricao")
     * @LG\Labels\Attributes(text="descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $tipoContexto;

    /**
     * @return integer
     */
    public function getTipoId()
    {
        return $this->tipoId;
    }

    /**
     * @param integer $tipoId
     * @return OrgComunicacaoTipo
     */
    public function setTipoId($tipoId)
    {
        $this->tipoId = $tipoId;

        return $this;
    }

    /**
     * @return string
     */
    public function getTipoDescricao()
    {
        return $this->tipoDescricao;
    }

    /**
     * @param string $tipoDescricao
     * @return OrgComunicacaoTipo
     */
    public function setTipoDescricao($tipoDescricao)
    {
        $this->tipoDescricao = $tipoDescricao;

        return $this;
    }

    /**
     * @return string
     */
    public function getTipoContexto()
    {
        return $this->tipoContexto;
    }

    /**
     * @param string $tipoContexto
     */
    public function setTipoContexto($tipoContexto)
    {
        $this->tipoContexto = $tipoContexto;
    }


    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'tipoId'        => $this->getTipoId(),
            'tipoDescricao' => $this->getTipoDescricao(),
            'tipoContexto' => $this->getTipoContexto()
        );

        return $array;
    }
}
