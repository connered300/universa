<?php

namespace Organizacao\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * OrgEmailTipo
 *
 * @ORM\Table(name="org__email_tipo")
 * @ORM\Entity
 * @LG\LG(id="tipoId",label="TipoId")
 * @Jarvis\Jarvis(title="Listagem de e-mail tipo",icon="fa fa-table")
 */
class OrgEmailTipo
{
    /**
     * @var \Organizacao\Entity\OrgEmailConta
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="Organizacao\Entity\OrgEmailConta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="conta_id", referencedColumnName="conta_id")
     * })
     */
    private $conta;
    /**
     * @var \Organizacao\Entity\OrgComunicacaoTipo
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="Organizacao\Entity\OrgComunicacaoTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipo_id", referencedColumnName="tipo_id")
     * })
     */
    private $tipo;

    /**
     * @return \Organizacao\Entity\OrgEmailConta
     */
    public function getConta()
    {
        return $this->conta;
    }

    /**
     * @param \Organizacao\Entity\OrgEmailConta $conta
     * @return OrgEmailTipo
     */
    public function setConta($conta)
    {
        $this->conta = $conta;

        return $this;
    }

    /**
     * @return \Organizacao\Entity\OrgComunicacaoTipo
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param \Organizacao\Entity\OrgComunicacaoTipo $tipo
     * @return OrgEmailTipo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'conta'         => $this->getConta(),
            'contaNome'     => $this->getConta()->getContaNome(),
            'tipo'          => $this->getTipo(),
            'tipoDescricao' => $this->getTipo()->getTipoDescricao(),
        );

        $array['conta'] = $this->getConta() ? $this->getConta()->getContaId() : null;
        $array['tipo']  = $this->getTipo() ? $this->getTipo()->getTipoId() : null;

        return $array;
    }
}
