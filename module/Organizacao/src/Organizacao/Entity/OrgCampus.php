<?php

namespace Organizacao\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * OrgCampus
 *
 * @ORM\Table(name="org_campus", indexes={@ORM\Index(name="fk_org_campus_pessoa_ies1_idx", columns={"ies_id"})})
 * @ORM\Entity
 * @LG\LG(id="camp_id",label="camp_id")
 * @Jarvis\Jarvis(title="Lista de Câmpus",icon="fa fa-table")
 */
class OrgCampus
{
    /**
     * @var integer
     *
     * @ORM\Column(name="camp_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="camp_id")
     * @LG\Labels\Attributes(text="Índice",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $campId;

    /**
     * @var string
     *
     * @ORM\Column(name="camp_nome", type="string", length=45, nullable=true)
     * @LG\Labels\Property(name="camp_nome")
     * @LG\Labels\Attributes(text="Câmpus Nome",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE%")
     */
    private $campNome;

    /**
     * @var string
     *
     * @ORM\Column(name="camp_telefone", type="string", length=45, nullable=true)
     * @LG\Labels\Property(name="camp_telefone")
     * @LG\Labels\Attributes(text="Câmpus Telefone",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE%")
     */
    private $campTelefone;

    /**
     * @var string
     *
     * @ORM\Column(name="camp_email", type="string", length=45, nullable=true)
     * @LG\Labels\Property(name="camp_email")
     * @LG\Labels\Attributes(text="E-mail do Câmpus",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE%")
     */
    private $campEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="camp_site", type="string", length=45, nullable=true)
     * @LG\Labels\Property(name="camp_site")
     * @LG\Labels\Attributes(text="Site",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE%")
     */
    private $campSite;

    /**
     * @var \Pessoa\Entity\PessoaFisica
     *
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\PessoaFisica")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="secretaria_pes_id", referencedColumnName="pes_id")
     * })
     */
    private $secretariaPes;

    /**
     * @var \Pessoa\Entity\PessoaFisica
     *
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\PessoaFisica")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="diretoria_pes_id", referencedColumnName="pes_id")
     * })
     */
    private $diretoriaPes;

    /**
     * @var \GerenciadorArquivos\Entity\Arquivo
     *
     * @ORM\ManyToOne(targetEntity="GerenciadorArquivos\Entity\Arquivo", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="arq_id", referencedColumnName="arq_id")
     * })
     */
    private $arq;

    /**
     * @var \GerenciadorArquivos\Entity\Arquivo
     *
     * @ORM\ManyToOne(targetEntity="GerenciadorArquivos\Entity\Arquivo", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="arq_id_carteirinha", referencedColumnName="arq_id")
     * })
     */
    private $arqCarteirinha;

    /**
     * @var OrgIes
     *
     * @ORM\ManyToOne(targetEntity="OrgIes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ies_id", referencedColumnName="ies_id")
     * })
     */
    private $ies;

    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    public function toArray()
    {
        return [
            'campId'         => $this->getCampId(),
            'campNome'       => $this->getCampNome(),
            'campTelefone'   => $this->getCampTelefone(),
            'campEmail'      => $this->getCampEmail(),
            'campSite'       => $this->getCampSite(),
            'ies'            => $this->getIes(),
            'diretoriaPes'   => $this->getDiretoriaPes(),
            'secretariaPes'  => $this->getSecretariaPes(),
            'arq'            => $this->getArq(),
            'arqCarteirinha' => $this->getArqCarteirinha()
        ];
    }

    /**
     * @return int
     */
    public function getCampId()
    {
        return $this->campId;
    }

    /**
     * @param int $campId
     * @return OrgCampus
     */
    public function setCampId($campId)
    {
        $this->campId = $campId;

        return $this;
    }

    /**
     * @return string
     */
    public function getCampNome()
    {
        return $this->campNome;
    }

    /**
     * @param string $campNome
     * @return OrgCampus
     */
    public function setCampNome($campNome)
    {
        $this->campNome = $campNome;

        return $this;
    }

    /**
     * @return string
     */
    public function getCampTelefone()
    {
        return $this->campTelefone;
    }

    /**
     * @param string $campTelefone
     * @return OrgCampus
     */
    public function setCampTelefone($campTelefone)
    {
        $this->campTelefone = $campTelefone;

        return $this;
    }

    /**
     * @return string
     */
    public function getCampEmail()
    {
        return $this->campEmail;
    }

    /**
     * @param string $campEmail
     * @return OrgCampus
     */
    public function setCampEmail($campEmail)
    {
        $this->campEmail = $campEmail;

        return $this;
    }

    /**
     * @return string
     */
    public function getCampSite()
    {
        return $this->campSite;
    }

    /**
     * @param string $campSite
     * @return OrgCampus
     */
    public function setCampSite($campSite)
    {
        $this->campSite = $campSite;

        return $this;
    }

    /**
     * @return \Pessoa\Entity\PessoaFisica
     */
    public function getSecretariaPes()
    {
        return $this->secretariaPes;
    }

    /**
     * @param \Pessoa\Entity\PessoaFisica $secretariaPes
     * @return OrgCampus
     */
    public function setSecretariaPes($secretariaPes)
    {
        $this->secretariaPes = $secretariaPes;

        return $this;
    }

    /**
     * @return \Pessoa\Entity\PessoaFisica
     */
    public function getDiretoriaPes()
    {
        return $this->diretoriaPes;
    }

    /**
     * @param \Pessoa\Entity\PessoaFisica $diretoriaPes
     * @return OrgCampus
     */
    public function setDiretoriaPes($diretoriaPes)
    {
        $this->diretoriaPes = $diretoriaPes;

        return $this;
    }

    /**
     * @return OrgIes
     */
    public function getIes()
    {
        return $this->ies;
    }

    /**
     * @param OrgIes $ies
     * @return OrgCampus
     */
    public function setIes($ies)
    {
        $this->ies = $ies;

        return $this;
    }

    /**
     * @return \GerenciadorArquivos\Entity\Arquivo
     */
    public function getArq()
    {
        return $this->arq;
    }

    /**
     * @param \GerenciadorArquivos\Entity\Arquivo $arq
     * @return OrgCampus
     */
    public function setArq($arq)
    {
        $this->arq = $arq;

        return $this;
    }

    /**
     * @return \GerenciadorArquivos\Entity\Arquivo
     */
    public function getArqCarteirinha()
    {
        return $this->arqCarteirinha;
    }

    /**
     * @param \GerenciadorArquivos\Entity\Arquivo $arqCarteirinha
     * @return OrgCampus
     */
    public function setArqCarteirinha($arqCarteirinha)
    {
        $this->arqCarteirinha = $arqCarteirinha;

        return $this;
    }
}
