<?php

namespace Organizacao\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * OrgAgenteEducacional
 *
 * @ORM\Table(name="org__agente_educacional")
 * @ORM\Entity
 * @LG\LG(id="pesId",label="PesIdIndicacao")
 * @Jarvis\Jarvis(title="Lista de agente educacional",icon="fa fa-table")
 */
class OrgAgenteEducacional
{
    /**
     * @var \Pessoa\Entity\Pessoa
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     */
    private $pes;
    /**
     * @var \Organizacao\Entity\OrgAgenteEducacional
     * @ORM\ManyToOne(targetEntity="Organizacao\Entity\OrgAgenteEducacional")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id_indicacao", referencedColumnName="pes_id")
     * })
     */
    private $pesIdIndicacao;
    /**
     * @var \Matricula\Entity\AcadgeralCadastroOrigem
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadgeralCadastroOrigem")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="origem_id", referencedColumnName="origem_id")
     * })
     */
    private $origem;
    /**
     * @var integer
     *
     * @ORM\Column(name="agente_max_nivel", type="integer", nullable=false, length=1)
     * @LG\Labels\Property(name="agente_max_nivel")
     * @LG\Labels\Attributes(text="nível máximo")
     * @LG\Querys\Conditions(type="=")
     */
    private $agenteMaxNivel;
    /**
     * @var float
     *
     * @ORM\Column(name="agente_comissao_primeiro_nivel", type="float", nullable=false)
     * @LG\Labels\Property(name="agente_comissao_primeiro_nivel")
     * @LG\Labels\Attributes(text="comissão")
     * @LG\Querys\Conditions(type="=")
     */
    private $agenteComissaoPrimeiroNivel;
    /**
     * @var float
     *
     * @ORM\Column(name="agente_comissao_demais_niveis", type="float", nullable=false)
     * @LG\Labels\Property(name="agente_comissao_demais_niveis")
     * @LG\Labels\Attributes(text="comissão")
     * @LG\Querys\Conditions(type="=")
     */
    private $agenteComissaoDemaisNiveis;
    /**
     * @var \Financeiro\Entity\BoletoBanco
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\BoletoBanco")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="banc_id", referencedColumnName="banc_id")
     * })
     */
    private $banc;
    /**
     * @var string
     *
     * @ORM\Column(name="agente_conta_agencia", type="string", nullable=true, length=20)
     * @LG\Labels\Property(name="agente_conta_agencia")
     * @LG\Labels\Attributes(text="agência conta")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $agenteContaAgencia;
    /**
     * @var string
     *
     * @ORM\Column(name="agente_conta_tipo", type="string", nullable=true, length=8)
     * @LG\Labels\Property(name="agente_conta_tipo")
     * @LG\Labels\Attributes(text="tipo conta")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $agenteContaTipo;
    /**
     * @var string
     *
     * @ORM\Column(name="agente_conta_numero", type="string", nullable=true, length=20)
     * @LG\Labels\Property(name="agente_conta_numero")
     * @LG\Labels\Attributes(text="número conta")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $agenteContaNumero;
    /**
     * @var string
     *
     * @ORM\Column(name="agente_conta_titular", type="string", nullable=true, length=100)
     * @LG\Labels\Property(name="agente_conta_titular")
     * @LG\Labels\Attributes(text="titular conta")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $agenteContaTitular;
    /**
     * @var string
     *
     * @ORM\Column(name="agente_observacao", type="text", nullable=true)
     * @LG\Labels\Property(name="agente_observacao")
     * @LG\Labels\Attributes(text="observação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $agenteObservacao;
    /**
     * @var string
     *
     * @ORM\Column(name="agente_situacao", type="string", nullable=false, length=7)
     * @LG\Labels\Property(name="agente_situacao")
     * @LG\Labels\Attributes(text="situação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $agenteSituacao;
    /**
     * @var \Datetime
     *
     * @ORM\Column(name="agente_data_cadastro", type="datetime", nullable=true)
     * @LG\Labels\Property(name="agente_data_cadastro")
     * @LG\Labels\Attributes(text="cadastro data")
     * @LG\Querys\Conditions(type="=")
     */
    private $agenteDataCadastro;
    /**
     * @var \Datetime
     *
     * @ORM\Column(name="agente_data_atualizacao", type="datetime", nullable=true)
     * @LG\Labels\Property(name="agente_data_atualizacao")
     * @LG\Labels\Attributes(text="atualização data")
     * @LG\Querys\Conditions(type="=")
     */
    private $agenteDataAtualizacao;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Organizacao\Entity\OrgUnidadeEstudo", inversedBy="agentes")
     * @ORM\JoinTable(name="org__unidade_agente",
     *   joinColumns={
     *     @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="unidade_id", referencedColumnName="unidade_id")
     *   }
     * )
     */
    private $unidades;

    public function getUnidades()
    {
        return $this->unidades;
    }

    public function setUnidades(\Doctrine\Common\Collections\Collection $unidades)
    {
        $this->unidades = $unidades;

        return $this;
    }

    /**
     * @return \Pessoa\Entity\Pessoa
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @param \Pessoa\Entity\Pessoa $pes
     * @return OrgAgenteEducacional
     */
    public function setPes($pes)
    {
        $this->pes = $pes;

        return $this;
    }

    /**
     * @return \Organizacao\Entity\OrgAgenteEducacional
     */
    public function getPesIdIndicacao()
    {
        return $this->pesIdIndicacao;
    }

    /**
     * @param \Organizacao\Entity\OrgAgenteEducacional $pesIdIndicacao
     * @return OrgAgenteEducacional
     */
    public function setPesIdIndicacao($pesIdIndicacao)
    {
        $this->pesIdIndicacao = $pesIdIndicacao;

        return $this;
    }

    /**
     * @return \Matricula\Entity\AcadgeralCadastroOrigem
     */
    public function getOrigem()
    {
        return $this->origem;
    }

    /**
     * @param \Matricula\Entity\AcadgeralCadastroOrigem $origem
     * @return OrgAgenteEducacional
     */
    public function setOrigem($origem)
    {
        $this->origem = $origem;

        return $this;
    }

    /**
     * @return integer
     */
    public function getAgenteMaxNivel()
    {
        return $this->agenteMaxNivel;
    }

    /**
     * @param integer $agenteMaxNivel
     * @return OrgAgenteEducacional
     */
    public function setAgenteMaxNivel($agenteMaxNivel)
    {
        $this->agenteMaxNivel = $agenteMaxNivel;

        return $this;
    }

    /**
     * @return float
     */
    public function getAgenteComissaoPrimeiroNivel()
    {
        return $this->agenteComissaoPrimeiroNivel;
    }

    /**
     * @param float $agenteComissaoPrimeiroNivel
     * @return OrgAgenteEducacional
     */
    public function setAgenteComissaoPrimeiroNivel($agenteComissaoPrimeiroNivel)
    {
        $this->agenteComissaoPrimeiroNivel = $agenteComissaoPrimeiroNivel;

        return $this;
    }

    /**
     * @return float
     */
    public function getAgenteComissaoDemaisNiveis()
    {
        return $this->agenteComissaoDemaisNiveis;
    }

    /**
     * @param float $agenteComissaoDemaisNiveis
     * @return OrgAgenteEducacional
     */
    public function setAgenteComissaoDemaisNiveis($agenteComissaoDemaisNiveis)
    {
        $this->agenteComissaoDemaisNiveis = $agenteComissaoDemaisNiveis;

        return $this;
    }

    /**
     * @return \Financeiro\Entity\BoletoBanco
     */
    public function getBanc()
    {
        return $this->banc;
    }

    /**
     * @param \Financeiro\Entity\BoletoBanco $banc
     * @return OrgAgenteEducacional
     */
    public function setBanc($banc)
    {
        $this->banc = $banc;

        return $this;
    }

    /**
     * @return string
     */
    public function getAgenteContaAgencia()
    {
        return $this->agenteContaAgencia;
    }

    /**
     * @param string $agenteContaAgencia
     * @return OrgAgenteEducacional
     */
    public function setAgenteContaAgencia($agenteContaAgencia)
    {
        $this->agenteContaAgencia = $agenteContaAgencia;

        return $this;
    }

    /**
     * @return string
     */
    public function getAgenteContaTipo()
    {
        return $this->agenteContaTipo;
    }

    /**
     * @param string $agenteContaTipo
     * @return OrgAgenteEducacional
     */
    public function setAgenteContaTipo($agenteContaTipo)
    {
        $this->agenteContaTipo = $agenteContaTipo;

        return $this;
    }

    /**
     * @return string
     */
    public function getAgenteContaNumero()
    {
        return $this->agenteContaNumero;
    }

    /**
     * @param string $agenteContaNumero
     * @return OrgAgenteEducacional
     */
    public function setAgenteContaNumero($agenteContaNumero)
    {
        $this->agenteContaNumero = $agenteContaNumero;

        return $this;
    }

    /**
     * @return string
     */
    public function getAgenteContaTitular()
    {
        return $this->agenteContaTitular;
    }

    /**
     * @param string $agenteContaTitular
     * @return OrgAgenteEducacional
     */
    public function setAgenteContaTitular($agenteContaTitular)
    {
        $this->agenteContaTitular = $agenteContaTitular;

        return $this;
    }

    /**
     * @return string
     */
    public function getAgenteObservacao()
    {
        return $this->agenteObservacao;
    }

    /**
     * @param string $agenteObservacao
     * @return OrgAgenteEducacional
     */
    public function setAgenteObservacao($agenteObservacao)
    {
        $this->agenteObservacao = $agenteObservacao;

        return $this;
    }

    /**
     * @return string
     */
    public function getAgenteSituacao()
    {
        return $this->agenteSituacao;
    }

    /**
     * @param string $agenteSituacao
     * @return OrgAgenteEducacional
     */
    public function setAgenteSituacao($agenteSituacao)
    {
        $this->agenteSituacao = $agenteSituacao;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getAgenteDataCadastro($format = false)
    {
        $agenteDataCadastro = $this->agenteDataCadastro;

        if ($format && $agenteDataCadastro) {
            $agenteDataCadastro = $agenteDataCadastro->format('d/m/Y H:i:s');
        }

        return $agenteDataCadastro;
    }

    /**
     * @param \Datetime $agenteDataCadastro
     * @return OrgAgenteEducacional
     */
    public function setAgenteDataCadastro($agenteDataCadastro)
    {
        if ($agenteDataCadastro) {
            if (is_string($agenteDataCadastro)) {
                $agenteDataCadastro = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $agenteDataCadastro
                );
                $agenteDataCadastro = new \Datetime($agenteDataCadastro);
            }
        } else {
            $agenteDataCadastro = null;
        }
        $this->agenteDataCadastro = $agenteDataCadastro;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getAgenteDataAtualizacao($format = false)
    {
        $agenteDataAtualizacao = $this->agenteDataAtualizacao;

        if ($format && $agenteDataAtualizacao) {
            $agenteDataAtualizacao = $agenteDataAtualizacao->format('d/m/Y H:i:s');
        }

        return $agenteDataAtualizacao;
    }

    /**
     * @param \Datetime $agenteDataAtualizacao
     * @return OrgAgenteEducacional
     */
    public function setAgenteDataAtualizacao($agenteDataAtualizacao)
    {
        if ($agenteDataAtualizacao) {
            if (is_string($agenteDataAtualizacao)) {
                $agenteDataAtualizacao = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $agenteDataAtualizacao
                );
                $agenteDataAtualizacao = new \Datetime($agenteDataAtualizacao);
            }
        } else {
            $agenteDataAtualizacao = null;
        }
        $this->agenteDataAtualizacao = $agenteDataAtualizacao;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'pes'                         => $this->getPes(),
            'pesIdIndicacao'              => $this->getPesIdIndicacao(),
            'origem'                      => $this->getOrigem(),
            'agenteMaxNivel'              => $this->getAgenteMaxNivel(),
            'agenteComissaoPrimeiroNivel' => $this->getAgenteComissaoPrimeiroNivel(),
            'agenteComissaoDemaisNiveis'  => $this->getAgenteComissaoDemaisNiveis(),
            'banc'                        => $this->getBanc(),
            'agenteContaAgencia'          => $this->getAgenteContaAgencia(),
            'agenteContaTipo'             => $this->getAgenteContaTipo(),
            'agenteContaNumero'           => $this->getAgenteContaNumero(),
            'agenteContaTitular'          => $this->getAgenteContaTitular(),
            'agenteObservacao'            => $this->getAgenteObservacao(),
            'agenteSituacao'              => $this->getAgenteSituacao(),
            'agenteDataCadastro'          => $this->getAgenteDataCadastro(true),
            'agenteDataAtualizacao'       => $this->getAgenteDataAtualizacao(true),
            'unidades'                    => $this->getUnidades(),
        );

        $array['pes']            = $this->getPes() ? $this->getPes()->getPesId() : null;
        $array['pesIdIndicacao'] = (
        $this->getPesIdIndicacao() ? $this->getPesIdIndicacao()->getPes()->getPesId() : null
        );
        $array['origem']         = $this->getOrigem() ? $this->getOrigem()->getOrigemId() : null;
        $array['banc']           = $this->getBanc() ? $this->getBanc()->getBancId() : null;

        return $array;
    }
}
