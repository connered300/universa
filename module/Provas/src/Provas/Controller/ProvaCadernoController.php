<?php

namespace Provas\Controller;

use Provas\Form\Correcao;
use Provas\Form\ProvaCaderno;
use Provas\Form\ProvaEdicao;
use VersaSpine\Controller\AbstractCoreController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class ProvaCadernoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function addAction()
    {
        $view    = new ViewModel();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $dados = $request->getPost()->toArray();

            $provaCaderno = $this->services()->getService('Provas\Service\ProvaCaderno');

            $provaCaderno->adicionar($dados);

            return $this->redirect()->toRoute('provas/default', ['controller' => 'prova-caderno', 'action' => 'index']);
        }
        $selecaoEdicao = $this->services()->getService($this->getService())->getRepository(
            "Vestibular\Entity\SelecaoEdicao"
        )->findAll();
        if (!$selecaoEdicao) {
            $this->flashMessenger()->addWarningMessage("Não existe nenhuma edição de vestibular cadastrada");

            return $this->redirect()->toRoute("provas/default", ['controller' => 'prova-caderno']);
        }
        foreach ($selecaoEdicao as $edicao) {
            $option['edicao'][$edicao->getEdicaoId()] = $edicao->getEdicaoAno() . ' - ' . $edicao->getEdicaoSemestre();
        }

        $provas = $this->services()->getService($this->getService())->getRepository("Provas\Entity\Prova")->findAll();
        foreach ($provas as $key => $prova) {
            if ($prova->getProvaEstrangeira() !== 'Sim') {
                $option['provas'][$prova->getProvaId()] = $prova->getProvaDescricao();
            } else {
                $option['provasEstrangeiras'][$prova->getProvaId()] = $prova->getProvaDescricao();
            }
        }
        $cadernoForm     = new ProvaCaderno('ProvaCaderno');
        $provaEdicaoForm = new ProvaEdicao('ProvaEdicao', $option);
        $view->setVariables(
            [
                'cadernoForm'     => $cadernoForm,
                'provaEdicaoForm' => $provaEdicaoForm,
            ]
        );

        return $view;
    }

    public function editAction()
    {
        $srvProvaCarderno = new \Provas\Service\ProvaCaderno($this->getEntityManager());

        $view    = new ViewModel();
        $request = $this->getRequest();

        if ($request->isPost()) {
            $dados = $request->getPost()->toArray();
            try {
                $result = $srvProvaCarderno->edita($dados);
                $this->flashMessenger()->addSuccessMessage("Dados da prova alterados com sucesso!");
            } catch (\Exception $ex) {
                $this->flashMessenger()->addErrorMessage("Não foi possivel alterar os dados da prova!");
            }

            return $this->redirect()->toRoute('provas/default', ['controller' => 'prova-caderno', 'action' => 'index']);
        }

        $cadernoId     = $this->params()->fromRoute('id');
        $selecaoEdicao = $srvProvaCarderno->getRepository("Vestibular\Entity\SelecaoEdicao")->findAll();

        foreach ($selecaoEdicao as $edicao) {
            $option['edicao'][$edicao->getEdicaoId()] = $edicao->getEdicaoAno() . ' - ' . $edicao->getEdicaoSemestre(
                ) . ' Semestre';
        }

        $provas = $srvProvaCarderno->getRepository("Provas\Entity\Prova")->findAll();
        foreach ($provas as $prova) {
            if ($prova->getProvaEstrangeira() === 'Sim') {
                $option['provasEstrangeiras'][$prova->getProvaId()] = $prova->getProvaDescricao();
                continue;
            }
            $option['provas'][$prova->getProvaId()] = $prova->getProvaDescricao();
        }

        /*
         * Buscando as informações para edição.
         * */
        $caderno = $srvProvaCarderno->getRepository('Provas\Entity\ProvaCaderno')->findOneBy(
            ['cadId' => $cadernoId]
        )->toArray();

        $informacoes = $srvProvaCarderno->buscaInfoForm($cadernoId);
        foreach ($informacoes as $key => $info) {
            $arrayCursos[] = $info['selcursos'];
        }

        foreach ($informacoes as $key => $value) {
            $prova = $this->services()->getService("Provas\Service\ProvaEdicao")->getReference($value['proedicaoId']);
            if ($prova->getProva()->getProvaEstrangeira() == 'Sim') {
                $arrayProvasEstrangeiras[] = $value['prova'];
                $estrangeiras[]            = [
                    'id'   => $value['prova'],
                    'desc' => $prova->getProva()->getProvaDescricao()
                ];
                $estrangeiraQuest          = $prova->getProedicaoQtquestoes();
                $estrangeiraOrdem          = $prova->getProedicaoOrdem();
            } else {
                $arrayProvas[$key] = $value['prova'];
            }
            $informacoes[$key]['provaId'] = $value['prova'];
        }
        $arrayProvas                        = implode(',', $arrayProvas);
        $informacoes[0]['prova']            = $arrayProvas;
        $informacoes[0]['provaEstrangeira'] = implode(',', $arrayProvasEstrangeiras);

        /*
         * Buscando pelos gabaritos
         * */
        foreach ($informacoes as $info) {
            $arrayGabarito[(int)$info['provaId']] = $srvProvaCarderno->buscaGabarito($info['procadedicaoId']);
        }
        $cadernoForm = new ProvaCaderno('ProvaCaderno');
        $cadernoForm->setData($caderno);
        $provaEdicaoForm = new ProvaEdicao('ProvaEdicao', $option);
        $provaEdicaoForm->setData($informacoes[0]);
        $view->setVariables(
            [
                'cadernoForm'       => $cadernoForm,
                'provaEdicaoForm'   => $provaEdicaoForm,
                'arrayCursos'       => \Zend\Json\Json::encode($arrayCursos),
                'informacoes'       => $informacoes,
                'arrayGabarito'     => \Zend\Json\Json::encode($arrayGabarito),
                'arrayEstrangeiras' => $estrangeiras,
                'estrangeiraQuest'  => \Zend\json\Json::encode($estrangeiraQuest),
                'estrangeiraOrdem'  => \Zend\json\Json::encode($estrangeiraOrdem),
            ]
        );

        return $view;
    }

    public function buscaSelecaoCursosAction()
    {
        $processoSeletivoId = $this->getRequest()->getPost()->toArray()['string'];

        $selcursos     = $this->services()->getService($this->getService())
            ->executeQuery(
                "SELECT
                              curso_nome, selcursos_id
                            FROM
                              selecao_cursos
                            INNER JOIN
                              campus_curso ON campus_curso.cursocampus_id = selecao_cursos.cursocampus_id
                            INNER JOIN
                              acad_curso ON acad_curso.curso_id = campus_curso.curso_id
                            WHERE
                              edicao_id = '{$processoSeletivoId}'"
            );
        $selcursosJson = array();
        foreach ($selcursos as $key => $curso) {
            $selcursosJson[$key] = [
                'selcursosId' => $curso['selcursos_id'],
                'cursoNome'   => $curso['curso_nome']
            ];
        }
        $view = new JsonModel();
        $view->setVariable('json', $selcursosJson);

        return $view;
    }

    /**
     * Este método deve retornar os dados no formato json
     */
    public function searchForJsonAction()
    {
    }

    public function correcaoAction()
    {
        //        pre($this->getServiceLocator()->get("config"));
        $request              = $this->getRequest();
        $serviceInscricao     = $this->services()->getService('Vestibular\Service\SelecaoInscricao');
        $serviceProvaInscrito = $this->services()->getService('Provas\Service\ProvaInscrito');
        $serviceProvaCaderno  = $this->services()->getService($this->getService());
        if ($request->isPost()) {
            $dados = $request->getPost()->toArray();
            $dados = array_merge($dados, $this->params()->fromFiles());

            try {
                /**
                 * Adicionando o arquivo de correção
                 * */
                $arquivo['gabarito'] = $serviceProvaCaderno->adicionarCorrecao($dados['arquivoCorrecao']);
                $arquivo['redacao']  = $serviceProvaCaderno->adicionarCorrecao($dados['arquivoCorrecaoRedacao']);

                /**
                 * Contando a quantidade de questões do caderno.
                 **/
                $provas             = $serviceProvaCaderno->buscaProvasCaderno($dados['caderno']);
                $estrangeiraContada = 0;
                $quantQuestoes      = 0;

                foreach ($provas as $pro) {
                    if ($pro['prova_estrangeira'] == 'Sim' && $estrangeiraContada == 0) {
                        $estrangeiraContada = 1;
                        $quantQuestoes += $pro['proedicao_qtquestoes'];
                    } else {
                        if ($pro['prova_estrangeira'] != 'Sim') {
                            $quantQuestoes += $pro['proedicao_qtquestoes'];
                        }
                    }
                }
                $arquivo['gabarito'] = "./data/uploads/" . $arquivo['gabarito']->getArqChave();
                $arquivo['redacao']  = "./data/uploads/" . $arquivo['redacao']->getArqChave();

                $correcaoProva = new \CorrecaoProva\Core\Core(
                    $this->getServiceLocator()->get('config')['correcao_prova'], $arquivo['gabarito'], $quantQuestoes
                );
                $dadosCorrecao = $correcaoProva->getOutput()->getArray();

                $redacoes = file($arquivo['redacao']);
                foreach ($redacoes as $redacao) {
                    $redacaoAluno[trim(substr(explode(' ', $redacao)[9], 0, 5))] = trim(
                        substr(explode(' ', $redacao)[9], 5, 6)
                    );
                }

                $notas = [];
                // mescla as respostas de gabarito e nota de redação dos alunos
                foreach ($dadosCorrecao as $inscricao => $value) {
                    $a         = str_split($value);
                    $respostas = [
                        'gabarito'    => $value,
                        'notaRedacao' => $redacaoAluno[$inscricao],
                    ];

                    $notas[$inscricao] = $respostas;
                }

                $i           = 0;
                $edicaoAtual = $serviceInscricao->buscaEdicaoAtual()->getEdicaoId();

                try {
                    foreach ($notas as $inscricao => $value) {
                        $value['grabarito'] = strtolower($value['gabarito']);
                        if ($inscricao != '' || $value['gabarito'] != '') {
                            $i++;
                            $serviceProvaCaderno->adicionaGabaritoCandidato($value['gabarito'], $inscricao);
                            $estrangeiraCandidato = $serviceProvaCaderno->buscaEstrangeiraCandidato(
                                $inscricao,
                                $dados['caderno']
                            );
                            $serviceProvaCaderno->adicionaProvasCandidato($provas, $inscricao, $estrangeiraCandidato);
                            $serviceProvaCaderno->corrigeProva(
                                $inscricao,
                                $quantQuestoes,
                                $value['notaRedacao'],
                                $i,
                                $edicaoAtual
                            );
                            $serviceProvaCaderno->marcaReprovado($inscricao);
                            $serviceInscricao->Classificar($edicaoAtual);
                        }
                    }

                    $this->flashMessenger()->addSuccessMessage(
                        'Notas atualizadas com sucesso. É preciso encerrar o vestibular para que as alterações sejam aplicadas'
                    );
                } catch (\Exception $e) {
                    //tem um erro mas é relativo a view que não está mais sendo usada
                    $this->flashMessenger()->addSuccessMessage(
                        'Notas atualizadas com sucesso. É preciso encerrar o vestibular para que as alterações sejam aplicadas'
                    );
                }

                return $this->redirect()->toRoute(
                    'provas/default',
                    ['controller' => 'prova-caderno', 'action' => 'index']
                );
            } catch (\Exception $ex) {
                $this->flashMessenger()->addErrorMessage($ex->getMessage());
                $this->redirect()->toRoute('provas/default', ['controller' => 'prova-caderno', 'action' => 'index']);
            }
        }
        $edicaoAtual = $serviceInscricao->buscaEdicaoAtual()->getEdicaoId();
        $cadernos    = $serviceProvaCaderno->buscaCadernosEdicao($edicaoAtual);
        if (!$cadernos) {
            $this->flashMessenger()->addWarningMessage("Nao possui cadernos cadastrados para o vestibular vigente!.");

            return $this->redirect()->toRoute('provas/default', ['controller' => 'prova-caderno', 'action' => 'index']);
        }
        $option = [];
        foreach ($cadernos as $cad) {
            $option['caderno'][$cad['cad_id']] = $cad['cad_descricao'];
        }
        $formCorrecao = new Correcao($option);
        $this->view->setVariable('formCorrecao', $formCorrecao);

        return $this->view;
    }

    public function correcaoManualAction()
    {
        $service             = $this->services()->getService($this->getService());
        $serviceProvaCaderno = $this->services()->getService("Provas\Service\ProvaCaderno");
        $request             = $this->getRequest();
        if ($request->isPost()) {
            $dados                = $request->getPost()->toArray();
            $serviceProvaInscrito = $this->services()->getService('Provas\Service\ProvaInscrito');
            $serviceInscricao     = $this->services()->getService('Vestibular\Service\SelecaoInscricao');
            $pontuacaoTotal       = 0;
            try {
                foreach ($dados['provainscId'] as $key => $provainsc) {
                    $provaInscrito['provainscId']        = $provainsc;
                    $provaInscrito['proedicao']          = $dados['proedicao'][$key];
                    $provaInscrito['provainscPontuacao'] = $dados['provainscPontuacao'][$key];
                    $provaInscrito['inscricao']          = $dados['inscricao'][$key];
                    $pontuacaoTotal += $provaInscrito['provainscPontuacao'];
                    $serviceProvaInscrito->edita($provaInscrito);
                }
                $service->marcaReprovado($provaInscrito['inscricao']);
                $serviceInscricao->atualizaNotaInscricao($provaInscrito['inscricao'], $pontuacaoTotal);
                $serviceProvaCaderno->corrigeProvasCandidatosEnem($serviceInscricao->buscaEdicaoAtual()->getEdicaoId());
                $serviceInscricao->mudaClassificacao(
                    $provaInscrito['inscricao'],
                    $serviceInscricao->buscaEdicaoAtual()->getEdicaoId()
                );
            } catch (\Exception $ex) {
                $this->flashMessenger()->addErrorMessage('Ocorreu um erro ao atualizar as notas! Tente novamente.');

                return $this->redirect()->toRoute(
                    'provas/default',
                    ['controller' => 'prova-caderno', 'action' => 'correcao-manual']
                );
            }
            $this->flashMessenger()->addSuccessMessage(
                'Notas atualizadas com sucesso. É preciso encerrar o vestibular para que as alterações sejam aplicadas'
            );

            return $this->redirect()->toRoute(
                'provas/default',
                ['controller' => 'prova-caderno', 'action' => 'correcao-manual']
            );
        }
        $provaInscritoForm = new \Provas\Form\ProvaInscrito();
        $this->view->setVariable('provaInscritoForm', $provaInscritoForm);

        return $this->view;
    }

    public function buscaProvasCandidatoAction()
    {
        $request = $this->getRequest();
        $this->view->setTerminal(true);
        if ($request->isPost()) {
            $dados           = $request->getPost()->toArray();
            $service         = $this->services()->getService('Provas\Service\ProvaInscrito');
            $provasCandidato = $service->buscaProvasInscrito($dados['inscricao']);
            $this->view->setVariable('provaInscritoForm', new \Provas\Form\ProvaInscrito());
            $this->view->setVariable('provasCandidato', $provasCandidato);

            return $this->view;
        } else {
            $this->view->setVariable('provasCandidato', null);

            return $this->view;
        }
    }
}