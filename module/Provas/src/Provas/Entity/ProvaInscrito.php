<?php

namespace Provas\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProvaInscrito
 *
 * @ORM\Table(name="prova_inscrito", indexes={@ORM\Index(name="fk_prova_inscrito_selecao_inscricao1_idx", columns={"inscricao_id"}), @ORM\Index(name="fk_prova_inscrito_prova_edicao1_idx", columns={"proedicao_id"})})
 * @ORM\Entity
 */
class ProvaInscrito
{
    /**
     * @var integer
     *
     * @ORM\Column(name="provainsc_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $provainscId;

    /**
     * @var integer
     *
     * @ORM\Column(name="provainsc_acertos", type="integer", nullable=false)
     */
    private $provainscAcertos;

    /**
     * @var float
     *
     * @ORM\Column(name="provainsc_pontuacao", type="float", precision=10, scale=0, nullable=true)
     */
    private $provainscPontuacao;

    /**
     * @var string
     *
     * @ORM\Column(name="provainsc_percentual", type="string", length=45, nullable=false)
     */
    private $provainscPercentual;

    /**
     * @var \ProvaEdicao
     *
     * @ORM\ManyToOne(targetEntity="ProvaEdicao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="proedicao_id", referencedColumnName="proedicao_id")
     * })
     */
    private $proedicao;

    /**
     * @var \SelecaoInscricao
     *
     * @ORM\ManyToOne(targetEntity="Vestibular\Entity\SelecaoInscricao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="inscricao_id", referencedColumnName="inscricao_id")
     * })
     */
    private $inscricao;

    public function __construct($data) {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getProvainscId()
    {
        return $this->provainscId;
    }

    /**
     * @param int $provaiscId
     */
    public function setProvainscId($provaiscId)
    {
        $this->provainscId = $provaiscId;
    }

    /**
     * @return int
     */
    public function getProvainscAcertos()
    {
        return $this->provainscAcertos;
    }

    /**
     * @param int $provainscAcertos
     */
    public function setProvainscAcertos($provainscAcertos)
    {
        $this->provainscAcertos = $provainscAcertos;
    }

    /**
     * @return float
     */
    public function getProvainscPontuacao()
    {
        return $this->provainscPontuacao;
    }

    /**
     * @param float $provainscPontuacao
     */
    public function setProvainscPontuacao($provainscPontuacao)
    {
        $this->provainscPontuacao = $provainscPontuacao;
    }

    /**
     * @return string
     */
    public function getProvainscPercentual()
    {
        return $this->provainscPercentual;
    }

    /**
     * @param string $provainscPercentual
     */
    public function setProvainscPercentual($provainscPercentual)
    {
        $this->provainscPercentual = $provainscPercentual;
    }

    /**
     * @return \ProvaEdicao
     */
    public function getProedicao()
    {
        return $this->proedicao;
    }

    /**
     * @param \ProvaEdicao $proedicao
     */
    public function setProedicao($proedicao)
    {
        $this->proedicao = $proedicao;
    }

    /**
     * @return \SelecaoInscricao
     */
    public function getInscricao()
    {
        return $this->inscricao;
    }

    /**
     * @param \SelecaoInscricao $inscricao
     */
    public function setInscricao($inscricao)
    {
        $this->inscricao = $inscricao;
    }

    public function toArray()
    {
        return [
            'provainscId' => $this->getProvainscId(),
            'provainscAcertos' => $this->getProvainscAcertos(),
            'provainscPontuacao' => $this->getProvainscPontuacao(),
            'provainscPercentual' => $this->getProvainscPercentual(),
            'proedicao' => $this->getProedicao()->getProedicaoId(),
            'inscricao' => $this->getInscricao()->getInscricaoId()
        ];
    }
}
