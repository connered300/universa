<?php
namespace Provas\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProvaEdicao
 *
 * @ORM\Table(name="prova_edicao", indexes={@ORM\Index(name="fk_prova_edicao_prova1_idx", columns={"prova_id"}), @ORM\Index(name="fk_prova_edicao_selecao_edicao1_idx", columns={"edicao_id"}), @ORM\Index(name="fk_prova_edicao_selecao_cursos1_idx", columns={"selcursos_id"})})
 * @ORM\Entity
 */
class ProvaEdicao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="proedicao_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $proedicaoId;

    /**
     * @var integer
     *
     * @ORM\Column(name="proedicao_qtquestoes", type="integer", nullable=true)
     */
    private $proedicaoQtquestoes;

    /**
     * @var integer
     *
     * @ORM\Column(name="proedicao_ordem", type="integer", nullable=true)
     */
    private $proedicaoOrdem;

    /**
     * @var Prova
     *
     * @ORM\ManyToOne(targetEntity="Prova")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="prova_id", referencedColumnName="prova_id")
     * })
     */
    private $prova;

    /**
     * @var \Vestibular\Entity\SelecaoCursos
     *
     * @ORM\ManyToOne(targetEntity="Vestibular\Entity\SelecaoCursos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="selcursos_id", referencedColumnName="selcursos_id")
     * })
     */
    private $selcursos;

    /**
     * @var \Vestibular\Entity\SelecaoEdicao
     *
     * @ORM\ManyToOne(targetEntity="Vestibular\Entity\SelecaoEdicao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="edicao_id", referencedColumnName="edicao_id")
     * })
     */
    private $edicao;

    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    function getProedicaoId()
    {
        return $this->proedicaoId;
    }

    function getProedicaoQtquestoes()
    {
        return $this->proedicaoQtquestoes;
    }

    function getProedicaoOrdem()
    {
        return $this->proedicaoOrdem;
    }

    function getProva()
    {
        return $this->prova;
    }

    function getSelcursos()
    {
        return $this->selcursos;
    }

    function getEdicao()
    {
        return $this->edicao;
    }

    function setProedicaoId($proedicaoId)
    {
        $this->proedicaoId = $proedicaoId;
    }

    function setProedicaoQtquestoes($proedicaoQtquestoes)
    {
        $this->proedicaoQtquestoes = (int)$proedicaoQtquestoes;
    }

    function setProedicaoOrdem($proedicaoOrdem)
    {
        $this->proedicaoOrdem = $proedicaoOrdem;
    }

    function setProva($prova)
    {
        $this->prova = $prova;
    }

    function setSelcursos($selcursos)
    {
        $this->selcursos = $selcursos;
    }

    function setEdicao($edicao)
    {
        $this->edicao = $edicao;
    }

    public function toArray()
    {
        return [
            'proedicaoId'         => $this->getProedicaoId(),
            'proedicaoQtquestoes' => $this->getProedicaoQtquestoes(),
            'proedicaoOrdem'      => $this->getProedicaoOrdem(),
            'prova'               => $this->getProva()->getProvaId(),
            'selcursos'           => $this->getSelcursos()->getSelcursosId(),
            'edicao'              => $this->getEdicao()->getEdicaoId()
        ];
    }
}
