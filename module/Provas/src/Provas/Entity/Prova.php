<?php

namespace Provas\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Prova
 *
 * @ORM\Table(name="prova")
 * @ORM\Entity
 */
class Prova
{
    /**
     * @var integer
     *
     * @ORM\Column(name="prova_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $provaId;

    /**
     * @var string
     *
     * @ORM\Column(name="prova_descricao", type="string", length=45, nullable=false)
     */
    private $provaDescricao;

    /**
     * @var string
     *
     * @ORM\Column(name="prova_tipo", type="string", nullable=false)
     */
    private $provaTipo;

    /**
     * @var string
     *
     * @ORM\Column(name="prova_estrangeira", type="string", nullable=false)
     */
    private $provaEstrangeira = 'Não';

    public function __construct($data) {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }
    function getProvaId() {
        return $this->provaId;
    }

    function getProvaDescricao() {
        return $this->provaDescricao;
    }

    function getProvaTipo() {
        return $this->provaTipo;
    }

    function getProvaEstrangeira() {
        return $this->provaEstrangeira;
    }

    function setProvaId($provaId) {
        $this->provaId = $provaId;
    }

    function setProvaDescricao($provaDescricao) {
        $this->provaDescricao = $provaDescricao;
    }

    function setProvaTipo($provaTipo) {
        $this->provaTipo = $provaTipo;
    }

    function setProvaEstrangeira($provaEstrangeira) {
        $this->provaEstrangeira = $provaEstrangeira;
    }

    public function toArray()
    {
        return [
            'provaId'          => $this->getProvaId(),
            'provaDescricao'   => $this->getProvaDescricao(),
            'provaTipo'        => $this->getProvaTipo(),
            'provaEstrangeira' => $this->getProvaEstrangeira()
        ];
    }

}
