<?php

namespace Provas\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProvaEdicaoCaderno
 *
 * @ORM\Table(name="prova_edicao_caderno", indexes={@ORM\Index(name="fk_prova_edicao_prova_gabarito_edicao_prova_gabarito_edicao_idx", columns={"cad_id"}), @ORM\Index(name="fk_prova_edicao_prova_gabarito_edicao_prova_edicao1_idx", columns={"proedicao_id"})})
 * @ORM\Entity
 */
class ProvaEdicaoCaderno
{
    /**
     * @var string
     *
     * @ORM\Column(name="procadedicao_id", type="string", length=45, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $procadedicaoId;

    /**
     * @var \ProvaEdicao
     *
     * @ORM\ManyToOne(targetEntity="ProvaEdicao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="proedicao_id", referencedColumnName="proedicao_id")
     * })
     */
    private $proedicao;

    /**
     * @var \ProvaCaderno
     *
     * @ORM\ManyToOne(targetEntity="ProvaCaderno")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cad_id", referencedColumnName="cad_id")
     * })
     */
    private $cad;

    public function __construct($data) {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return string
     */
    public function getProcadedicaoId()
    {
        return $this->procadedicaoId;
    }

    /**
     * @param string $procadedicaoId
     */
    public function setProcadedicaoId($procadedicaoId)
    {
        $this->procadedicaoId = $procadedicaoId;
    }

    /**
     * @return \ProvaEdicao
     */
    public function getProedicao()
    {
        return $this->proedicao;
    }

    /**
     * @param \ProvaEdicao $proedicao
     */
    public function setProedicao($proedicao)
    {
        $this->proedicao = $proedicao;
    }

    /**
     * @return \ProvaCaderno
     */
    public function getCad()
    {
        return $this->cad;
    }

    /**
     * @param \ProvaCaderno $cad
     */
    public function setCad($cad)
    {
        $this->cad = $cad;
    }

    public function toArray()
    {
        return [
            'procadedicaoId' => $this->getProcadedicaoId(),
            'proedicao'      => $this->getProedicao()->getProedicaoId(),
            'cad'            => $this->getCad()->getCadId()
        ];
    }

}
