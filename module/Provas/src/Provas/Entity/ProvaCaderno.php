<?php

namespace Provas\Entity;

use Doctrine\ORM\Mapping as ORM;

use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * ProvaCaderno
 *
 * @ORM\Table(name="prova_caderno")
 * @ORM\Entity
 * @LG\LG(id="cad_id",label="cad_descricao")
 * @Jarvis\Jarvis(title="Lista de Cadernos",icon="fa fa-table")
 */
class ProvaCaderno
{
    /**
     * @var integer
     *
     * @ORM\Column(name="cad_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="cad_id")
     * @LG\Labels\Attributes(text="Índice",icon="fa fa-user")
     * @LG\Querys\Conditions(type="=")
     */
    private $cadId;

    /**
     * @var string
     *
     * @ORM\Column(name="cad_descricao", type="string", length=45, nullable=true)
     * @LG\Labels\Property(name="cad_descricao")
     * @LG\Labels\Attributes(text="Descricao do Caderno",icon="fa fa-user")
     * @LG\Querys\Conditions(type="LIKE%")
     */
    private $cadDescricao;

    public function __construct($data) {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getCadId()
    {
        return $this->cadId;
    }

    /**
     * @param int $cadId
     */
    public function setCadId($cadId)
    {
        $this->cadId = $cadId;
    }

    /**
     * @return string
     */
    public function getcadDescricao()
    {
        return $this->cadDescricao;
    }

    /**
     * @param string $cadDescricao
     */
    public function setcadDescricao($cadDescricao)
    {
        $this->cadDescricao = $cadDescricao;
    }

    public function toArray()
    {
        return [
            'cadId'        => $this->getCadId(),
            'cadDescricao' => $this->getcadDescricao()
        ];
    }

}
