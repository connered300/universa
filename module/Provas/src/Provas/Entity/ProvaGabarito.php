<?php

namespace Provas\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProvaGabarito
 *
 * @ORM\Table(name="prova_gabarito", indexes={@ORM\Index(name="fk_prova_gabarito_prova_1_idx", columns={"procadedicao_id"})})
 * @ORM\Entity
 */
class ProvaGabarito
{
    /**
     * @var integer
     *
     * @ORM\Column(name="gab_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $gabId;

    /**
     * @var string
     *
     * @ORM\Column(name="gab_resposta", type="string", length=1, nullable=false)
     */
    private $gabResposta;

    /**
     * @var integer
     *
     * @ORM\Column(name="gab_ordem", type="integer", nullable=false)
     */
    private $gabOrdem;

    /**
     * @var integer
     *
     * @ORM\Column(name="gab_peso", type="integer", nullable=false)
     */
    private $gabPeso = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="gab_valida", type="string", nullable=false)
     */
    private $gabValida = 'valida';

    /**
     * @var \ProvaEdicaoCaderno
     *
     * @ORM\ManyToOne(targetEntity="ProvaEdicaoCaderno")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="procadedicao_id", referencedColumnName="procadedicao_id")
     * })
     */
    private $procadedicao;

    public function __construct($data) {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getGabId()
    {
        return $this->gabId;
    }

    /**
     * @param int $gabId
     */
    public function setGabId($gabId)
    {
        $this->gabId = $gabId;
    }

    /**
     * @return string
     */
    public function getGabResposta()
    {
        return $this->gabResposta;
    }

    /**
     * @param string $gabResposta
     */
    public function setGabResposta($gabResposta)
    {
        $this->gabResposta = $gabResposta;
    }

    /**
     * @return int
     */
    public function getGabOrdem()
    {
        return $this->gabOrdem;
    }

    /**
     * @param int $gabOrdem
     */
    public function setGabOrdem($gabOrdem)
    {
        $this->gabOrdem = $gabOrdem;
    }

    /**
     * @return int
     */
    public function getGabPeso()
    {
        return $this->gabPeso;
    }

    /**
     * @param int $gabPeso
     */
    public function setGabPeso($gabPeso)
    {
        $this->gabPeso = $gabPeso;
    }

    /**
     * @return string
     */
    public function getGabValida()
    {
        return $this->gabValida;
    }

    /**
     * @param string $gabValida
     */
    public function setGabValida($gabValida)
    {
        $this->gabValida = $gabValida;
    }

    /**
     * @return \ProvaEdicaoCaderno
     */
    public function getProcadedicao()
    {
        return $this->procadedicao;
    }

    /**
     * @param \ProvaEdicaoCaderno $procadedicao
     */
    public function setProcadedicao($procadedicao)
    {
        $this->procadedicao = $procadedicao;
    }


    public function toArray()
    {
        return [
            'gabId'        => $this->getGabId(),
            'gabResposta'  => $this->getGabResposta(),
            'gabOrdem'     => $this->getGabOrdem(),
            'gabPeso'      => $this->getGabPeso(),
            'gabValida'    => $this->getGabValida(),
            'procadedicao' => $this->getProcadedicao()
        ];
    }

}
