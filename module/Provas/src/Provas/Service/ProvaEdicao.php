<?php


namespace Provas\Service;

use VersaSpine\Service\AbstractService;

class ProvaEdicao extends AbstractService
{
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Provas\Entity\ProvaEdicao');
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {

    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {

    }


    //Busca prova lingua estrangeira da edicao para serem exibidas no seletor do candidato;
    //Parametros: Id da edicao atual ou a que deseja busca ou zero para todas cadastradas.
    public function buscaProvaEstrangeiraNaEdicao($edicaoId)
    {
        if ($edicaoId === 0) {
            $provas = $this->getRepository()->findAll();
        } else {
            $provas = $this->getRepository()->findBy(array('edicao' => $edicaoId));
        }

        $lingua = array();

        /** @var $obj \Provas\Entity\ProvaEdicao */
        foreach ($provas as $obj) {
            if ($obj->getProva()->getProvaEstrangeira() == 'Sim') {
                $lingua[$obj->getProva()->getProvaDescricao()] = $obj->getProva()->getProvaDescricao();
            }
        }

        return $lingua;
    }
}