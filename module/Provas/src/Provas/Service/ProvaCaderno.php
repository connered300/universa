<?php
namespace Provas\Service;

use VersaSpine\Service\AbstractService;

class ProvaCaderno extends AbstractService
{
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Provas\Entity\ProvaCaderno');
    }

    public function adicionar(array $dados)
    {
        foreach ($dados['estrangeirasId'] as $estrangeiraId) {
            $dados['procadedicaoId'][] = $estrangeiraId;
        }
        foreach ($dados['provaEstrangeira'] as $provaEstrangeira) {
            $dados['prova'][] = $provaEstrangeira;
        }
        parent::begin();
        try {
            $provaCadernoAdicionado = parent::adicionar($dados);
            $dados['cadId']         = $provaCadernoAdicionado->getCadId();
            $this->adicionaDependencias($dados);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
        }
        parent::commit();

        return $provaCadernoAdicionado;
    }

    public function edita($dados)
    {
        foreach ($dados['estrangeirasId'] as $estrangeiraId) {
            $dados['procadedicaoId'][] = $estrangeiraId;
        }
        foreach ($dados['provaEstrangeira'] as $provaEstrangeira) {
            $dados['prova'][] = $provaEstrangeira;
        }
        parent::begin();
        try {
            $provaCaderno = parent::edita($dados);
            $this->editaDependencias($dados);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
        }
        parent::commit();

        return $provaCaderno;
    }

    /**
     * Essa Função edita nas demais tabelas.
     * */
    public function editaDependencias($dados)
    {
        $erro = "";
        $ret  = $this->executeQuery(
            "
                        SELECT
                          pe.prova_id
                        FROM
                          prova_edicao pe
                        INNER JOIN
                          prova_edicao_caderno pec
                        ON
                          pe.proedicao_id = pec.proedicao_id
                        INNER JOIN
                          prova_caderno pc
                        ON
                          pc.cad_id = pec.cad_id
                        WHERE
                          pc.cad_id = '{$dados['cadId']}'
                        "
        );
        foreach ($ret as $pro) {
            $provasRegistradas[] = (int)$pro['prova_id'];
        }
        foreach ($dados['idDaProva'] as $pro) {
            $dados['provaVest'][] = (int)$pro;
        }

        /**
         * Verificação para deletar provas que foram desmarcadas.
         * */
        $dif = array_diff($provasRegistradas, $dados['provaVest']);
        if ($dif) {
            foreach ($dif as $remove) {
                try {
                    $proedicao = $this->executeQuery(
                        "
                        SELECT
                          pe.proedicao_id, pec.procadedicao_id
                        FROM
                          prova_edicao pe
                        INNER JOIN
                          prova_edicao_caderno pec
                        ON
                          pe.proedicao_id = pec.proedicao_id
                        INNER JOIN
                          prova_caderno pc
                        ON
                          pc.cad_id = pec.cad_id
                        WHERE
                          pc.cad_id = '{$dados['cadId']}'
                          AND
                          pe.prova_id = '{$remove}'
                    "
                    )->fetchAll();
                    $this->exec(
                        "DELETE FROM prova_gabarito WHERE procadedicao_id = '{$proedicao[0]['procadedicao_id']}'"
                    );
                    $this->exec(
                        "DELETE FROM prova_edicao_caderno WHERE procadedicao_id = '{$proedicao[0]['procadedicao_id']}'"
                    );
                    $this->exec("DELETE FROM prova_edicao WHERE proedicao_id = '{$proedicao[0]['proedicao_id']}'");
                } catch (\Exception $ex) {
                    $erro .= "Impossível remover a prova {$proedicao['proedicao_id']} do caderno. Existem inscrições do vestibular vinculadas à ela.<br>";
                }
            }
        }
        /**
         * Editando ou adicionando novos prova_edicao.
         * */
        $serviceProvaEdicao        = new \Provas\Service\ProvaEdicao($this->getEm());
        $provaEdicaoCadernoService = new \Provas\Service\ProvaEdicaoCaderno($this->getEm());
        foreach ($dados['idDaProva'] as $key => $prova) {
            $provaEdicaoId = $this->executeQuery(
                "
                        SELECT
                          pe.proedicao_id
                        FROM
                          prova_edicao pe
                        INNER JOIN
                          prova_edicao_caderno pec
                        ON
                          pe.proedicao_id = pec.proedicao_id
                        INNER JOIN
                          prova_caderno pc
                        ON
                          pc.cad_id = pec.cad_id
                        WHERE
                          pc.cad_id = '{$dados['cadId']}'
                          AND
                          pe.prova_id = '{$prova}'
                        "
            )->fetchAll();
            $provaEdicao   = [
                'proedicaoId'         => '',
                'prova'               => $prova,
                'edicao'              => $dados['edicao'],
                'proedicaoQtquestoes' => $dados['quantQuestoesGeral'][$key],
                'proedicaoOrdem'      => $dados['ordemGeral'][$key],
                'selcursos'           => $dados['selcursos'][0]
            ];
            if ($provaEdicaoId) {
                $provaEdicaoEditar          = $serviceProvaEdicao->getRepository(
                    $serviceProvaEdicao->getEntity()
                )->findOneBy(
                    ['proedicaoId' => $provaEdicaoId[0]]
                )->toArray();
                $provaEdicao['proedicaoId'] = $provaEdicaoEditar['proedicaoId'];
                $provaEdicaoAdicionada      = $serviceProvaEdicao->edita($provaEdicao);
            } else {
                $provaEdicaoAdicionada = $serviceProvaEdicao->adicionar($provaEdicao);
            }
            $provaEdicaoAdicionada = $provaEdicaoAdicionada->toArray();
            $provaEdicaoCaderno    = $provaEdicaoCadernoService->getRepository(
                $provaEdicaoCadernoService->getEntity()
            )->findOneBy(['proedicao' => $provaEdicaoAdicionada['proedicaoId'], 'cad' => $dados['cadId']]);
            if ($provaEdicaoCaderno) {
                $provaCadernoEdicao           = [
                    'procadedicaoId' => $provaEdicaoCaderno->getProcadedicaoId(),
                    'proedicao'      => $provaEdicaoAdicionada['proedicaoId'],
                    'cad'            => $dados['cadId'],
                ];
                $provaCadernoEdicaoAdicionado = $provaEdicaoCadernoService->edita($provaCadernoEdicao);
            } else {
                $provaCadernoEdicao           = [
                    'procadedicaoId' => '',
                    'proedicao'      => $provaEdicaoAdicionada['proedicaoId'],
                    'cad'            => $dados['cadId'],
                ];
                $provaCadernoEdicaoAdicionado = $provaEdicaoCadernoService->adicionar($provaCadernoEdicao);
            }
            $this->executeQuery(
                "DELETE FROM prova_gabarito WHERE procadedicao_id = '{$provaCadernoEdicaoAdicionado->getProcadedicaoId()}'"
            );
            foreach ($dados['ordemQuestao'] as $key => $ordemQuestao) {
                if ($provaEdicaoAdicionada['prova'] == $dados['provaQuestId'][$key]) {
                    $gabarito           = [
                        'gabId'        => '',
                        'gabResposta'  => $dados['resposta'][$key],
                        'gabOrdem'     => $ordemQuestao,
                        'gabPeso'      => $dados['peso'][$key],
                        'gabValida'    => ($dados['valida'][$key] == 1) ? 'valida' : 'anulada',
                        'procadedicao' => $provaCadernoEdicaoAdicionado->getProcadedicaoId()
                    ];
                    $gabaritoAdicionado = (new \Provas\Service\ProvaGabarito($this->getEm()))->adicionar($gabarito);
                }
            }
        }
    }

    public function adicionaDependencias($dados)
    {
        foreach ($dados['selcursos'] as $selcursos) {
            foreach ($dados['prova'] as $key => $provas) {
                $prova                        = $this->executeQuery(
                    "SELECT * FROM prova WHERE prova_id = '{$dados['prova'][$key]}'"
                )->fetchAll();
                $prova                        = $prova[0];
                $provaEdicao                  = [
                    'proedicaoId'         => '',
                    'prova'               => $dados['prova'][$key],
                    'edicao'              => $dados['edicao'],
                    'proedicaoQtquestoes' => ($prova['prova_estrangeira'] == 'Sim') ? $dados['quantQuestoesEstrangeiras'][0] : $dados['quantQuestoes'][$key],
                    'proedicaoOrdem'      => ($prova['prova_estrangeira'] == 'Sim') ? $dados['ordemEstrangeiras'][0] : $dados['ordem'][$key],
                    'selcursos'           => $selcursos
                ];
                $provaEdicaoAdicionada        = (new \Provas\Service\ProvaEdicao($this->getEm()))->adicionar(
                    $provaEdicao
                );
                $provaEdicaoAdicionada        = $provaEdicaoAdicionada->toArray();
                $provaCadernoEdicao           = [
                    'procadedicaoId' => '',
                    'proedicao'      => $provaEdicaoAdicionada['proedicaoId'],
                    'cad'            => $dados['cadId'],
                ];
                $provaCadernoEdicaoAdicionado = (new \Provas\Service\ProvaEdicaoCaderno($this->getEm()))->adicionar(
                    $provaCadernoEdicao
                );
                $provaCadernoEdicaoAdicionado = $provaCadernoEdicaoAdicionado->getProcadedicaoId();

                foreach ($dados['ordemQuestao'] as $key => $ordemQuestao) {
                    if ($provaEdicaoAdicionada['prova'] == $dados['procadId'][$key]) {
                        $gabarito           = [
                            'gabId'        => '',
                            'gabResposta'  => $dados['resposta'][$key],
                            'gabOrdem'     => $ordemQuestao,
                            'gabPeso'      => $dados['peso'][$key],
                            'gabValida'    => ($dados['valida'][$key] == 1) ? 'valida' : 'anulada',
                            'procadedicao' => $provaCadernoEdicaoAdicionado
                        ];
                        $gabaritoAdicionado = (new \Provas\Service\ProvaGabarito($this->getEm()))->adicionar($gabarito);
                    }
                }
            }
        }
    }

    /**
     * Busca todas as informações para um determinado caderno de provas.
     * **/
    public function buscaInfoForm($id)
    {
        $info = $this->executeQuery(
            "SELECT
                                        prova_edicao.proedicao_id,
                                        prova_edicao.prova_id,
                                        selcursos_id,
                                        edicao_id,
                                        prova_descricao,
                                        proedicao_qtquestoes,
                                        proedicao_ordem,
                                        procadedicao_id,
                                        prova_edicao_caderno.procadedicao_id
                                     FROM
                                      prova_edicao_caderno
                                     INNER JOIN
                                      prova_edicao ON prova_edicao.proedicao_id = prova_edicao_caderno.proedicao_id
                                     INNER JOIN
                                      prova ON prova.prova_id = prova_edicao.prova_id
                                     WHERE
                                      prova_edicao_caderno.cad_id = '{$id}'"
        );

        foreach ($info as $key => $consultaProvas) {
            $array[$key] = [
                'proedicaoId'         => $consultaProvas['proedicao_id'],
                'prova'               => $consultaProvas['prova_id'],
                'selcursos'           => $consultaProvas['selcursos_id'],
                'edicao'              => $consultaProvas['edicao_id'],
                'provaDescricao'      => $consultaProvas['prova_descricao'],
                'proedicaoQtquestoes' => $consultaProvas['proedicao_qtquestoes'],
                'proedicaoOrdem'      => $consultaProvas['proedicao_ordem'],
                'procadedicaoId'      => $consultaProvas['procadedicao_id'],
            ];
        }

        return $array;
    }

    public function buscaGabarito($procadedicaoId)
    {
        $result = $this->executeQuery(
            "SELECT
                                        gab_id, gab_resposta, gab_ordem, gab_peso, gab_valida, gab.procadedicao_id, prova_id
                                       FROM
                                        prova_gabarito as gab
                                       INNER JOIN
                                        prova_edicao_caderno as caded ON gab.procadedicao_id = caded.procadedicao_id
                                       INNER JOIN
                                        prova_edicao ON caded.proedicao_id = prova_edicao.proedicao_id
                                       WHERE
                                        gab.procadedicao_id = '{$procadedicaoId}'"
        );
        foreach ($result as $key => $value) {
            $arrayGabarito[$key] = $value;
        }

        return $arrayGabarito;
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {
    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {
    }

    public function pagination($page = 0, $is_json = false)
    {
        $dados                   = array();
        $selecaoInscricaoService = new \Vestibular\Service\SelecaoInscricao($this->getEm());
        $edicaoAtual             = $selecaoInscricaoService->buscaEdicaoAtual();
        if (!$edicaoAtual) {
            return '';
        }
        $dados['dados'] = $this->executeQuery(
            "SELECT DISTINCT pc.cad_id, cad_descricao FROM prova_caderno as pc
                                                            INNER JOIN
                                                             prova_edicao_caderno as pec
                                                              ON pec.cad_id = pc.cad_id
                                                            INNER JOIN
                                                             prova_edicao as pe
                                                              ON pe.proedicao_id = pec.proedicao_id
                                                            WHERE pe.edicao_id = '{$edicaoAtual->getEdicaoId()}'"
        );
        $dados['count'] = $dados['dados']->rowCount();
        if ($is_json) {
            $dados['dados'] = $this->paginationFormatJson($dados['dados']);
        }

        return $dados;
    }

    public function buscaCadernosEdicao($id)
    {
        $cadernos = $this->executeQuery(
            "SELECT DISTINCT
                                          cad.cad_id, cad.cad_descricao
                                         FROM
                                          prova_caderno as cad
                                         INNER JOIN
                                          prova_edicao_caderno as pcad ON pcad.cad_id = cad.cad_id
                                         INNER JOIN
                                          prova_edicao as ped ON ped.proedicao_id = pcad.proedicao_id
                                         WHERE
                                          ped.edicao_id = '{$id}'"
        )->fetchAll();
        if ($cadernos) {
            return $cadernos;
        } else {
            return false;
        }
    }

    public function adicionarCorrecao($dados)
    {
        $arquivo = new \GerenciadorArquivos\Service\Arquivo($this->getEm());
        try {
            $arquivoAdd = $arquivo->adicionar($dados);

            return $arquivoAdd;
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
        }
    }

    public function buscaProvasCaderno($cadId)
    {
        $provas = $this->executeQuery(
            "
        SELECT
          edi.proedicao_id,
          prova.prova_estrangeira,
          edi.proedicao_qtquestoes,
          edi.proedicao_ordem,
          prova_descricao
        FROM
          prova_edicao as edi
        INNER JOIN
          prova_edicao_caderno as pcad ON pcad.proedicao_id = edi.proedicao_id
        INNER JOIN
          prova_caderno as cad ON cad.cad_id = pcad.cad_id
        INNER JOIN
          prova ON edi.prova_id = prova.prova_id
        WHERE
          cad.cad_id = '{$cadId}'"
        )->fetchAll();
        if ($provas) {
            return $provas;
        } else {
            return false;
        }
    }

    /**
     * Busca a prova de lingua estrangeira do candidato com base no caderno de provas.
     * */
    public function buscaEstrangeiraCandidato($candidatoId, $cadernoId)
    {
        $estrangeiraCandidato = $this->executeQuery(
            "SELECT * FROM selecao_inscricao WHERE inscricao_id = '{$candidatoId}'"
        )->fetch();

        $estrangeira = $this->executeQuery(
            "SELECT
              ped.proedicao_id FROM prova_caderno as cad
             INNER JOIN
              prova_edicao_caderno as pcad
             ON
              pcad.cad_id = cad.cad_id
             INNER JOIN
              prova_edicao as ped
             ON
              ped.proedicao_id = pcad.proedicao_id
             INNER JOIN
              prova
             ON
              ped.prova_id = prova.prova_id
             WHERE
              cad.cad_id = '{$cadernoId}' AND prova.prova_descricao LIKE '{$estrangeiraCandidato['inscricao_lingua_estrangeira']}'"
        )->fetch();

        return $estrangeira['proedicao_id'];
    }

    /**
     * Insere as provas do candidato com base no caderno de provas que ele utilizou no dia do exame.
     * */
    public function adicionaProvasCandidato($provas, $inscricao, $estrangeira)
    {
        $inscricao               = (int)$inscricao;
        $provaInscritoRepository = $this->getRepository('Provas\Entity\ProvaInscrito');
        $this->begin();
        foreach ($provas as $prova) {
            $provaInscrito = $provaInscritoRepository->findOneBy(
                ['inscricao' => $inscricao, 'proedicao' => $prova['proedicao_id']]
            );
            if (!$provaInscrito) {
                if (!$provaInscrito) {
                    if ($prova['prova_estrangeira'] != 'Sim' || $prova['proedicao_id'] == $estrangeira) {
                        $provaInscrito['provainscAcertos']    = 0;
                        $provaInscrito['provainscPontuacao']  = null;
                        $provaInscrito['provainscPercentual'] = 0;
                        $provaInscrito['proedicao']           = $this->getReference(
                            $prova['proedicao_id'],
                            'Provas\Entity\ProvaEdicao'
                        );
                        $provaInscrito['inscricao']           = $this->getReference(
                            $inscricao,
                            'Vestibular\Entity\SelecaoInscricao'
                        );
                        $provaInscrito                        = new \Provas\Entity\ProvaInscrito($provaInscrito);
                        try {
                            $this->getEm()->persist($provaInscrito);
                        } catch (\Exception $ex) {
                            throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
                        }
                    }
                }
            }
        }
        $this->getEm()->flush();
        $this->getEm()->clear();
        $this->commit();
    }

    public function adicionaGabaritoCandidato($gabarito, $inscricaoId)
    {
        if ($this->executeQuery("SELECT * FROM selecao_inscricao WHERE inscricao_id = '{$inscricaoId}'")->fetch()) {
            $this->exec(
                "UPDATE selecao_inscricao SET inscricao_gabarito = '{$gabarito}' WHERE inscricao_id = '{$inscricaoId}'"
            );
        }
    }

    /**
     * Funçao que monta o gabarito para uma prova do candidato.
     * */
    public function montaGabaritoProvaCandidato($provainscId)
    {
        return $this->executeQuery(
            "
            SELECT
              gab_resposta, gab_ordem, gab_peso, gab_valida, ped.proedicao_id
            FROM
              prova_gabarito as gab
            INNER JOIN
              prova_edicao_caderno as pced ON pced.procadedicao_id = gab.procadedicao_id
            INNER JOIN
              prova_edicao as ped ON ped.proedicao_id = pced.proedicao_id
            INNER JOIN
              prova_inscrito as pinsc ON pinsc.proedicao_id = ped.proedicao_id
            WHERE
              pinsc.provainsc_id = '{$provainscId}'
            ORDER BY gab_ordem
        "
        )->fetchAll();
    }

    //    public function corrigeProva($inscricao, $quantQuestoes)
    //    {
    //        $inscricao = $this->getRepository("Vestibular\Entity\SelecaoInscricao")->findOneBy(['inscricaoId' => $inscricao]);
    //        if(!$inscricao){
    //            return;
    //        }
    //        $gabaritoCandidato = $inscricao->getInscricaoGabarito();
    //
    //        $pcad = $this->getRepository('Provas\Entity\ProvaInscrito')->findBy(['inscricao' => $inscricao]);
    //        $provasCandidato = $pcad;
    //        $acertoTotal = 0;
    //        $nota = 0;
    //        parent::begin();
    //        foreach ($provasCandidato as $provaCand) {
    //            $gabarito = $this->montaGabaritoProvaCandidato($provaCand->getProvainscId());
    //            $acertos = 0;
    //            $pontuacao = 0; // A pontuação é a somatória do número de acertos vezes o peso da questão.
    //            $questoes = count($gabarito); // Armazenará o número de questões que uma determinada prova possui.
    //            foreach ($gabarito as $questao) {
    //                if ($questao['gab_valida'] == 'valida') {
    ////                    echo ($questao['gab_ordem'])." => ".$questao['gab_resposta']." => ".$gabaritoCandidato[$questao['gab_ordem']-1]."<br>";
    //                    if ($questao['gab_resposta'] == $gabaritoCandidato[$questao['gab_ordem']-1]) {
    //                        $acertos++;
    //                        $pontuacao += 1 * $questao['gab_peso'];
    //                    }
    //                } else {
    //                    $acertos++;
    //                    $pontuacao += 1 * $questao['gab_peso'];
    //                }
    //            }
    //            $nota += $pontuacao;
    //            $percentual = ($acertos*100)/$questoes;
    //            $result = $this->executeQuery("
    //            UPDATE
    //              prova_inscrito
    //            SET
    //              provainsc_acertos = '{$acertos}', provainsc_pontuacao = '{$pontuacao}', provainsc_percentual = '{$percentual}'
    //            WHERE
    //              provainsc_id = {$provaCand->getProvainscId()}
    //            ");
    //            $acertoTotal += $acertos;
    //        }
    //        $percentualTotal = ($acertoTotal*100)/$quantQuestoes;
    //        $result =  $this->executeQuery("
    //            UPDATE
    //              selecao_inscricao
    //            SET
    //              selecao_inscricao_acertos = '{$percentualTotal}',
    //              inscricao_nota = '{$nota}'
    //            WHERE
    //              inscricao_id = '{$inscricao->getInscricaoId()}'
    //              ");
    //        parent::commit();
    //    }

    public function corrigeProva(
        $inscricao,
        $quantQuestoes,
        $notaRedacao = 0,
        $classificacao = null,
        $edicaoAtual = null
    ) {
        $inscricao = $this->executeQuery("SELECT * FROM selecao_inscricao WHERE inscricao_id = $inscricao")->fetch();
        if (!$inscricao) {
            return;
        }
        $gabaritoCandidato = $inscricao['inscricao_gabarito'];

        $provasCandidato = $this->executeQuery(
            "SELECT * FROM prova_inscrito INNER JOIN prova_edicao ON prova_edicao.proedicao_id = prova_inscrito.proedicao_id INNER JOIN prova ON prova.prova_id = prova_edicao.prova_id WHERE prova_inscrito.inscricao_id = {$inscricao['inscricao_id']}"
        )->fetchAll();
        $acertoTotal     = 0;
        $nota            = 0;
        parent::begin();
        foreach ($provasCandidato as $provaCand) {
            if ($provaCand['prova_descricao'] != 'Redação') {
                $gabarito  = $this->montaGabaritoProvaCandidato($provaCand['provainsc_id']);
                $acertos   = 0;
                $pontuacao = 0; // A pontuação é a somatória do número de acertos vezes o peso da questão.
                $questoes  = count($gabarito); // Armazenará o número de questões que uma determinada prova possui.
                foreach ($gabarito as $questao) {
                    if ($questao['gab_valida'] == 'valida') {
                        //                    echo ($questao['gab_ordem'])." => ".$questao['gab_resposta']." => ".$gabaritoCandidato[$questao['gab_ordem']-1]."<br>";
                        $questao['gab_resposta'] = strtolower($questao['gab_resposta']);
                        $questaoCandidato        = strtolower($gabaritoCandidato[$questao['gab_ordem'] - 1]);
                        if ($questao['gab_resposta'] == $questaoCandidato) {
                            $acertos++;
                            $pontuacao += 1 * $questao['gab_peso'];
                        }
                    } else {
                        $acertos++;
                        $pontuacao += 1 * $questao['gab_peso'];
                    }
                }
                $nota += $pontuacao;
                $percentual      = ($acertos * 100) / $questoes;
                $provaInscResult = $this->executeQuery(
                    "
                    UPDATE
                      prova_inscrito
                    SET
                      provainsc_acertos = '{$acertos}', provainsc_pontuacao = '{$pontuacao}', provainsc_percentual = '{$percentual}'
                    WHERE
                      provainsc_id = {$provaCand['provainsc_id']}
                "
                );
                $acertoTotal += $acertos;
            } else {
                $nota += $provaCand['provainsc_pontuacao'];

                $prova = $this->getReference($provaCand['provainsc_id'], 'Provas\Entity\ProvaInscrito');
                if ($prova) {
                    $prova->setProvainscPontuacao($notaRedacao);
                    try {
                        $this->getEm()->persist($prova);
                        $this->getEm()->flush();
                        $this->getEm()->clear();
                    } catch (\Exception $ex) {
                        throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
                    }
                }
            }
        }
        $percentualTotal = ($acertoTotal * 100) / $quantQuestoes;

        $this->executeQuery(
            "
            UPDATE
              selecao_inscricao
              INNER JOIN inscricao_cursos USING (inscricao_id)
            SET
              selecao_inscricao_acertos = '{$percentualTotal}',
              inscricao_nota = '{$nota}',
              inscricao_resultado = 'aprovado'
            WHERE
              inscricao_id = '{$inscricao['inscricao_id']}'
              "
        );

        parent::commit();
    }

    public function marcaReprovado($inscricao)
    {
        $result      = $this->executeQuery(
            "SELECT provainsc_pontuacao, prova_descricao FROM prova_inscrito insc INNER JOIN prova_edicao edi ON edi.proedicao_id = insc.proedicao_id INNER JOIN prova ON prova.prova_id = edi.prova_id WHERE inscricao_id = {$inscricao}"
        )->fetchAll();
        $provaZerada = false;
        foreach ($result as $provaInscrito) {
            if ($provaInscrito['provainsc_pontuacao'] < 5 && $provaInscrito['prova_descricao'] == 'Redação') {
                $provaZerada = true;
            }
        }
        if ($provaZerada) {
            $this->executeQuery(
                "
                    UPDATE
                      selecao_inscricao
                      LEFT JOIN inscricao_cursos USING(inscricao_id)

                    SET
                      inscricao_resultado = 'desclassificado'
                    WHERE
                      inscricao_id = '{$inscricao}'
                "
            );
        } else {
            $this->executeQuery(
                "
                    UPDATE
                      selecao_inscricao
                    LEFT JOIN inscricao_cursos USING(inscricao_id)

                    SET
                      inscricao_resultado = 'aprovado'
                    WHERE
                      inscricao_id = '{$inscricao}'
                "
            );
        }
    }

    public function corrigeProvasCandidatosEnem($edicaoId)
    {
        $inscricoesEnem = $this->executeQuery(
            "
            SELECT
                insc.*
            FROM
                selecao_inscricao insc
            INNER JOIN
                selecao_tipo_edicao seletipo
            ON
                insc.seletipoedicao_id = seletipo.seletipoedicao_id
            INNER JOIN
                selecao_tipo tipo
            ON
                tipo.tiposel_id = seletipo.tiposel_id
            WHERE
                tiposel_nome LIKE '%Enem%'
                AND
                insc.edicao_id = '{$edicaoId}'
                AND
                insc.inscricao_status = 'Aceita'
                AND
                inscricao_entrega_doc = 'Sim'
        "
        )->fetchAll();
        foreach ($inscricoesEnem as $insc) {
            $pontuacaoTotal = 0;
            /**
             * Calculo da redação.
             * */
            if ($insc['inscricao_nota_enem_redacao'] > 1000) {
                $insc['inscricao_nota_enem_redacao'] /= 10;
            }
            $redacao = (25 * (float)$insc['inscricao_nota_enem_redacao']) / 1000;
            $redacao = ceil($redacao);
            /**
             * Calculando a nota de português.
             * */
            if ($insc['inscricao_nota_enem_portugues'] > 1000) {
                $insc['inscricao_nota_enem_portugues'] /= 10;
            }
            $portugues = (50 * (float)$insc['inscricao_nota_enem_portugues']) / 1000;
            $portugues = round($portugues);
            while ($portugues % 2 !== 0) {
                $portugues += 1;
            }
            /**
             * Calculando o restante das notas
             *  */
            if ($insc['inscricao_nota_enem_cienc_humanas'] > 1000) {
                $insc['inscricao_nota_enem_cienc_humanas'] /= 10;
            }
            if ($insc['inscricao_nota_enem_cienc_natureza'] > 1000) {
                $insc['inscricao_nota_enem_cienc_natureza'] /= 10;
            }
            if ($insc['inscricao_nota_enem_matematica'] > 1000) {
                $insc['inscricao_nota_enem_matematica'] /= 10;
            }
            $restante = ($insc['inscricao_nota_enem_cienc_humanas'] + $insc['inscricao_nota_enem_cienc_natureza'] + $insc['inscricao_nota_enem_matematica']) / 3;
            $restante = (25 * $restante) / 1000;

            $pontuacaoTotal = $redacao + $portugues + $restante;
            try {
                $inscricaoId = $this->executeQuery(
                    "SELECT * FROM selecao_inscricao WHERE inscricao_id = '{$insc['inscricao_id']}'"
                )->fetchAll();
                if ($inscricaoId[0]['inscricao_nota'] < $pontuacaoTotal) {
                    $this->exec(
                        "UPDATE selecao_inscricao SET inscricao_nota = {$pontuacaoTotal} WHERE inscricao_id = {$inscricaoId[0]['inscricao_id']}"
                    );
                }
            } catch (\Exception $ex) {
                throw new \Exception (
                    "Erro ao atualizar a inscrição do candidato: {$insc['inscricao_id']}. Se o problema persistir contate o suporte!"
                );
            }
        }
    }
}