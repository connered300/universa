<?php


namespace Provas\Form;

use Zend\Form\Form,
    Zend\Form\Element;

class ProvaGabarito extends Form {
    public function __construct($name = null, array $option = null)
    {
        parent::__construct($name);

        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', '');
        $this->setAttribute('role', 'form');

        $gabId = new Element\Hidden('gabId');
        $this->add($gabId);

        $gabResposta = new Element\Text('gabResposta');
        $gabResposta->setAttributes([
            'col'      => '2',
            'label'    => 'Resposta',
            'required' => 'Required',
        ]);
        $this->add($gabResposta);

        $gabOrdem = new Element\Text('gabOrdem');
        $gabOrdem->setAttributes([
            'col'              => '2',
            'label'            => 'Ordem',
            'required'         => 'Required',
            'data-validations' => 'Integer'
        ]);
        $this->add($gabOrdem);

        $gabPeso = new Element\Text('gabPeso');
        $gabPeso->setAttributes([
            'col'              => '2',
            'label'            => 'Peso',
            'required'         => 'Required',
            'data-validations' => 'Integer'
        ]);
        $this->add($gabPeso);

        $gabValida = new Element\Text('gabValida');
        $gabValida->setAttributes([
            'col'              => '2',
            'label'            => 'Valida',
            'required'         => 'Required',
            'data-validations' => 'Integer'
        ]);
        $this->add($gabValida);

        $procadedicao = new Element\Hidden('procadedicao');
        $this->add($procadedicao);
    }

}