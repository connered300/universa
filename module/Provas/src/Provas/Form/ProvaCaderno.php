<?php


namespace Provas\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class ProvaCaderno extends Form{
    public function __construct($name = null, array $option = null)
    {
        parent::__construct($name);

        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', '');
        $this->setAttribute('role', 'form');
        $this->setAttribute('id', 'formProvaCaderno');

        $cadId = new Element\Hidden('cadId');
        $this->add($cadId);

        $descricao = new Element\Text('cadDescricao');
        $descricao->setAttributes([
            'placeholder' => 'Nome ou descrição do caderno',
            'label'       => 'Nome/Descrição',
            'col'         => '4',
            'required'    => 'Required'
        ]);
        $this->add($descricao);

    }

}