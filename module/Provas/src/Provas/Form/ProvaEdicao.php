<?php


namespace Provas\Form;

use Zend\Form\Form,
    Zend\Form\Element;

class ProvaEdicao extends Form{
    public function __construct($name = null, array $option = null)
    {
        parent::__construct($name);

        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', '');
        $this->setAttribute('role', 'form');

        $proedicaoId = new Element\Hidden('proedicaoId');
        $this->add($proedicaoId);

        $prova = new Element\MultiCheckbox('prova');
        $prova->setAttributes([
            'col'   => '6',
            'label' => 'Selecione as provas',
        ]);
        $prova->setValueOptions($option['provas']);
        $this->add($prova);

        $provaEstrangeira = new Element\MultiCheckbox('provaEstrangeira');
        $provaEstrangeira->setAttributes([
            'col'   => '6',
            'label' => 'Selecione as provas de lingua estrangeira',
        ]);
        $provaEstrangeira->setValueOptions($option['provasEstrangeiras']);
        $this->add($provaEstrangeira);

        $edicao = new Element\Select('edicao');
        $edicao->setAttributes([
            'col'   => '4',
            'label' => 'Edição do Vestibular',
            'wrap'  => true,
        ]);
        if($option){
            $edicao->setValueOptions($option['edicao']);
        }
        $this->add($edicao);

        $quantidadeQuestoes = new Element\Text('proedicaoQtquestoes');
        $quantidadeQuestoes->setAttributes([
            'col'              => '3',
            'label'            => 'Quantidade de questões',
            'data-validations' => 'Integer',
            'required'         => 'Required',
        ]);
        $this->add($quantidadeQuestoes);

        $ordem = new Element\Text('proedicaoOrdem');
        $ordem->setAttributes([
            'col'              => '2',
            'label'            => 'Ordem',
            'data-validations' => 'Integer',
            'required'         => 'Required',
        ]);
        $this->add($ordem);

        $curso = new Element\MultiCheckbox('selcursos');
        if ($option['selcursos']) {
            $curso->setValueOptions($option['selcursos']);
        }
        $curso->setAttributes([
            'label'    => 'Cursos',
            'col'      => '4',
            'required' => 'Required'
        ]);
        $this->add($curso);

        $cadastrar = new Element\Submit('Cadastrar');
        $cadastrar->setAttributes([
            'class' => 'btn btn-primary',
            'value' => 'Salvar',

        ]);
        $this->add($cadastrar);

    }
}