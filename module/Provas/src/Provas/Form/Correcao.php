<?php


namespace Provas\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class Correcao extends Form{
    public function __construct(array $option = null)
    {
        parent::__construct();
        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', '');
        $this->setAttribute('role', 'form');
        $this->setAttribute("enctype", "multipart/form-data");

        $caderno = new Element\Select('caderno');
        $caderno->setAttributes([
            'label'            => 'Caderno',
            'col'              => '5',
            'data-validations' => 'Required'
        ]);
        if ($option['caderno']){
            $caderno->setValueOptions($option['caderno']);
        }
        $this->add($caderno);

        $arquivo = new Element\File('arquivoCorrecao');
        $arquivo->setAttributes([
            'label'            => 'Arquivo de gabaritos',
            'col'              => '5',
            'data-validations' => 'Required',
        ]);
        $this->add($arquivo);

        $arquivo = new Element\File('arquivoCorrecaoRedacao');
        $arquivo->setAttributes([
            'label'            => 'Arquivo de Redação',
            'col'              => '5',
            'data-validations' => 'Required'
        ]);
        $this->add($arquivo);

        $correcao = new Element\Submit('iniciar');
        $correcao->setAttributes([
            'value' => 'Iniciar Correção',
            'class' => 'btn btn-primary pull-left'
        ]);
        $this->add($correcao);
    }
}