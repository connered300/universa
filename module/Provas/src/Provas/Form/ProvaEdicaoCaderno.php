<?php


namespace Provas\Form;

use Zend\Form\Form,
    Zend\Form\Element;

class ProvaEdicaoCaderno extends Form{
    public function __construct($nome = null, array $option = null)
    {
        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', '');
        $this->setAttribute('role', 'form');

        $procadedicaoId = new Element\Hidden('procadedicaoId');
        $this->add($procadedicaoId);

        $proedicao = new Element\Hidden('proedicao');
        $this->add($proedicao);

        $cad = new Element\Hidden('cad');
        $this->add($cad);
    }

}