<?php


namespace Provas\Form;

use Zend\Form\Form,
    Zend\Form\Element;

class ProvaInscrito extends Form{
    public function __construct($name = null, array $option = null)
    {
        parent::__construct($name);

        $this->setAttribute('class', 'smart-form');
        $this->setAttribute('data-validate', 'yes');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', '');
        $this->setAttribute('role', 'form');

        $id = new Element\Hidden('provainscId');
        $this->add($id);

        $provainscAcertos = new Element\Text('provainscAcertos');
        $provainscAcertos->setAttributes([
            'col'              => '1',
            'data-validations' => 'Integer',
            'label'            => 'Acertos',
            'required'         => 'Required'
        ]);
        $this->add($provainscAcertos);

        $provainscPontuacao = new Element\Text('provainscPontuacao');
        $provainscPontuacao->setAttributes([
            'col'              => '1',
            'label'            => 'Pontuacao',
            'required'         => 'Required'
        ]);
        $this->add($provainscPontuacao);

        $provainscPercentual = new Element\Text('provainscPercentual');
        $provainscPercentual->setAttributes([
            'col'   => '1',
            'label' => 'Percentual de Acerto'
        ]);
        $this->add($provainscPercentual);

        $inscricao = new Element\Text('inscricao');
        $inscricao->setAttributes([
            'label'            => 'Informe o número de inscrição:',
            'col'              => '4',
            'placeholder'      => 'Digite o número de inscrição',
            'data-validations' => 'Integer'
        ]);
        $this->add($inscricao);
        $provaNome = new Element\Text('provaNome');
        $provaNome->setAttributes([
            'label'            => 'Nome da Prova:',
            'col'              => '4',
            'disabled'         => 'disabled'
        ]);
        $this->add($provaNome);

        $proedicao = new Element\Hidden('proedicao');
        $this->add($proedicao);

        $correcao = new Element\Submit('salvar');
        $correcao->setAttributes([
            'value' => 'Salvar',
            'class' => 'btn btn-primary pull-left'
        ]);
        $this->add($correcao);
    }
}