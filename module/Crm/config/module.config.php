<?php

namespace Crm;

return array(
    'router'                    => array(
        'routes' => array(
            'crm' => array(
                'type'          => 'Literal',
                'options'       => array(
                    'route'    => '/crm',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Crm\Controller',
                        'controller'    => 'SisIntegracao',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes'  => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'       => '/[:controller[/:action[/:id]]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id'         => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults'    => array(
                                'action' => 'index'
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'controllers'               => array(
        'invokables' => array(
            'Crm\Controller\CrmMediador'        => 'Crm\Controller\CrmMediadorController',
            'Crm\Controller\CrmCampanha'        => 'Crm\Controller\CrmCampanhaController',
            'Crm\Controller\CrmFonte'           => 'Crm\Controller\CrmFonteController',
            'Crm\Controller\CrmInteresse'       => 'Crm\Controller\CrmInteresseController',
            'Crm\Controller\CrmAtividadeTipo'   => 'Crm\Controller\CrmAtividadeTipoController',
            'Crm\Controller\CrmOperador'        => 'Crm\Controller\CrmOperadorController',
            'Crm\Controller\CrmTime'            => 'Crm\Controller\CrmTimeController',
            'Crm\Controller\CrmLead'            => 'Crm\Controller\CrmLeadController',
            'Crm\Controller\CrmLeadAtendimento' => 'Crm\Controller\CrmLeadAtendimentoController',
            'Crm\Controller\CrmLeadAtividade'   => 'Crm\Controller\CrmLeadAtividadeController',
        ),
    ),
    'service_manager'           => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases'            => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'doctrine'                  => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default'             => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            ),
        ),
    ),
    'view_manager'              => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'console'                   => array(
        'router' => array(
            'routes' => array(),
        ),
    ),
    'funcionalidades_liberadas' => array(
        'controllers' => array(),
        'actions'     => array(
            'captura-leads-email',
            'distribuir-novos-leads',
            'efetuar-chamada',
            'importacao-csv'
        ),
    ),
    'namesDictionary'           => array(
        'Crm\CrmMediador'        => 'Mediadores',
        'Crm\CrmCampanha'        => 'Campanhas',
        'Crm\CrmFonte'           => 'Fontes',
        'Crm\CrmInteresse'       => 'Interesses',
        'Crm\CrmAtividadeTipo'   => 'Tipos de Atividade',
        'Crm\CrmOperador'        => 'Operadores',
        'Crm\CrmTime'            => 'Times',
        'Crm\CrmLead'            => 'Leads',
        'Crm\CrmLead::captacao'  => '',
        'Crm\CrmLeadAtendimento' => 'Atendimento de Leads',
        'Crm\CrmLeadAtividade'   => 'Atividades de Leads',
    )
);