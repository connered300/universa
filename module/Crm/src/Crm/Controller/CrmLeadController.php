<?php

namespace Crm\Controller;

use VersaSpine\Controller\AbstractCoreController;

class CrmLeadController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request        = $this->getRequest();
        $paramsGet      = $request->getQuery()->toArray();
        $paramsPost     = $request->getPost()->toArray();
        $param          = array_merge($paramsGet, $paramsPost);
        $serviceCrmLead = new \Crm\Service\CrmLead($this->getEntityManager());

        $arrDados = $serviceCrmLead->pesquisaForJson($param);

        if ($param['pesId'] && $param['dadosCompletos'] && $arrDados) {
            $arrDados = $arrDados[0];
            $arrDados = $serviceCrmLead->getArrayFallback($arrDados['pesId']);
        }

        return new \Zend\View\Model\JsonModel($arrDados ? $arrDados : array());
    }

    public function indexAction($atendimento = false)
    {
        $serviceCrmOperador = new \Crm\Service\CrmOperador($this->getEntityManager());
        $serviceCrmLead     = new \Crm\Service\CrmLead($this->getEntityManager());

        $serviceCrmLead->setarDependenciasView($this->getView());

        $this->getView()->setTemplate($this->getTemplateToRoute('index', 'crm-lead'));

        if ($atendimento) {
            $this
                ->getView()
                ->setVariable('atendimento', true)
                ->setVariable('operador', $serviceCrmOperador->retornaOperadorLogadoSelect2());
        }

        $click2CallAtivo = $serviceCrmOperador->getConfig()->localizarChave('SONAX_CLICK2CALL_ATIVO', false);

        $this->getView()->setVariable('click2CallAtivo', $click2CallAtivo);

        return $this->getView();
    }

    public function capturaLeadsEmailAction()
    {
        $request        = $this->getRequest();
        $paramsGet      = $request->getQuery()->toArray();
        $serviceCrmLead = new \Crm\Service\CrmLead(
            $this->getEntityManager(),
            $this->getServiceManager()->get('Config')
        );

        $chamadaDeRobo = $paramsGet['chamadaDeRobo'] ? true : false;

        $this->getJson()->setVariables($serviceCrmLead->importarLeadsEmail($chamadaDeRobo));

        return $this->getJson();
    }

    public function importacaoCsvAction()
    {
        $serviceCrmLead = new \Crm\Service\CrmLead(
            $this->getEntityManager(),
            $this->getServiceManager()->get('Config')
        );

        $request   = $this->getRequest();
        $dataPost  = $request->getPost()->toArray();
        $dataFiles = $request->getFiles()->toArray();

        $fileKey = $dataPost['arq'];

        $file = '';

        if ($dataFiles['vfile'] && $dataFiles['vfile'][$fileKey]) {
            $name = $dataFiles['vfile'][$fileKey]['file']['name'];
            $file = $dataFiles['vfile'][$fileKey]['file']['tmp_name'];

            if (!preg_match('/^.*.csv$/i', $name)) {
                $file = '';
                $this->getView()->setVariable("erro", true);
                $this->getView()->setVariable("mensagem", 'Arquivo Inválido!');
            }
        }

        if ($file) {
            $this->getJson()->setVariables($serviceCrmLead->importarLeadsArquivoCSV($file));
        }

        return $this->getJson();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceCrmLead = new \Crm\Service\CrmLead($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceCrmLead->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function atendimentoAction($pesId)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive($request->getPost()->toArray(), $request->getQuery()->toArray());

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados       = array();
        $serviceCrmLead = new \Crm\Service\CrmLead(
            $this->getEntityManager(),
            $this->getServiceManager()->get('Config')
        );

        $arrDados = $serviceCrmLead->getArray($pesId);

        if (empty($arrDados)) {
            $this->getView()->setVariable("erro", false);

            if (!$ajax) {
                return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
            }

            return $this->getView();
        }

        $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);

        $serviceCrmLead->formataDadosPost($arrDados);
        $serviceCrmLead->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);

        return $this->getView();
    }

    public function captacaoAction()
    {
        $view = $this->addAction(false, true);

        $this->getView()->setTemplate($this->getTemplateToRoute('captacao'));

        return $view;
    }

    public function addAction($pesId = false, $captacao = false)
    {
        $request     = $this->getRequest();
        $ajax        = false;
        $atendimento = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        if ($arrDados['atualizarInfomacoes']) {
            $pesId = $arrDados['pesId'];
        }

        $atendimento = $arrDados['atendimento'] ? true : false;

        $arrDados       = array();
        $serviceCrmLead = new \Crm\Service\CrmLead(
            $this->getEntityManager(),
            $this->getServiceManager()->get('Config')
        );

        if ($pesId) {
            $serviceCrmLead->colocarLeadEmProgresso($pesId);

            $arrDados = $serviceCrmLead->getArray($pesId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", true);
                $this->getView()->setVariable("mensagem", $serviceCrmLead->getLastError());

                if (!$ajax) {
                    $param = [
                        'controller' => $this->getController()
                    ];

                    if ($captacao) {
                        $param['action'] = 'captacao';
                    }

                    return $this->redirect()->toRoute($this->getRoute(), $param);
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceCrmLead->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de lead salvo!';

                if (!$ajax) {
                    $param = [
                        'controller' => $this->getController()
                    ];

                    if ($captacao) {
                        $param['action'] = 'captacao';
                    }

                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), $param);
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceCrmLead->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceCrmLead->getLastError());
                }
            }
        }

        $serviceCrmLead->formataDadosPost($arrDados);
        $serviceCrmLead->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $pesId = $this->params()->fromRoute("id", 0);

        if (!$pesId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($pesId);
    }

    public function atualizarAction()
    {
        return $this->addAction();
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceCrmLead = new \Crm\Service\CrmLead(
                $this->getEntityManager(),
                $this->getServiceManager()->get('Config')
            );

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceCrmLead->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceCrmLead->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }

    public function distribuirAction($dataPost = array())
    {
        $request   = $this->getRequest();
        $paramsGet = $request->getQuery()->toArray();

        if ($request->isPost() || $dataPost) {
            $serviceCrmLead = new \Crm\Service\CrmLead(
                $this->getEntityManager(),
                $this->getServiceManager()->get('Config')
            );

            $erro     = false;
            $mensagem = '';

            if (!$dataPost) {
                $dataPost = $request->getPost()->toArray();
            }

            $chamadaDeRobo = $paramsGet['chamadaDeRobo'] ? true : false;

            $arrRetorno = $serviceCrmLead->distribuirLeads($dataPost, $chamadaDeRobo);

            if (!$arrRetorno) {
                $erro     = true;
                $mensagem = $serviceCrmLead->getLastError();
            } else {
                $mensagem = (
                    $arrRetorno['leads'] . ' leads distribuídos entre ' . $arrRetorno['operadores'] . ' operadores!'
                );
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('mensagem', $mensagem);
        }

        return $this->getJson();
    }

    public function distribuirNovosLeadsAction()
    {
        $serviceCrmLead = new \Crm\Service\CrmLead(
            $this->getEntityManager(),
            $this->getServiceManager()->get('Config')
        );

        $dataPost = ['leadStatus' => $serviceCrmLead::LEAD_STATUS_NOVO];

        return $this->distribuirAction($dataPost);
    }

    public function converterAction()
    {
        $pesId = $this->params()->fromRoute("id", 0);

        if (!$pesId) {
            $erro = "Para converter um registro é necessário informar o identificador!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        $request        = $this->getRequest();
        $serviceCrmLead = new \Crm\Service\CrmLead(
            $this->getEntityManager(),
            $this->getServiceManager()->get('Config')
        );

        $arrRetorno = $serviceCrmLead->converterLead($pesId);

        if ($arrRetorno) {
            $erro = "Lead convertido em aluno com sucesso!";
            $this->flashMessenger()->addSuccessMessage($erro);

            return $this->redirect()->toRoute(
                'matricula/default',
                array('controller' => 'acadgeral-aluno', 'action' => 'edit', 'id' => $arrRetorno['alunoId'])
            );
        }

        $erro = "Falha ao converter lead em aluno!\n" . $serviceCrmLead->getLastError();
        $this->flashMessenger()->addErrorMessage($erro);

        return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
    }
}
?>