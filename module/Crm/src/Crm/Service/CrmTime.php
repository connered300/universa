<?php

namespace Crm\Service;

use VersaSpine\Service\AbstractService;

class CrmTime extends AbstractService
{
    const TIME_SITUACAO_ATIVO = 'Ativo';
    const TIME_SITUACAO_INATIVO = 'Inativo';
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2TimeSituacao($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getTimeSituacao());
    }

    public static function getTimeSituacao()
    {
        return array(self::TIME_SITUACAO_ATIVO, self::TIME_SITUACAO_INATIVO);
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Crm\Entity\CrmTime');
    }

    public function pesquisaForJson($params)
    {
        $sql      = '
        SELECT
        a.timeId as time_id,
        a.timeNome as time_nome,
        a.timeLocalizacao as time_localizacao,
        a.timeSituacao as time_situacao
        FROM Crm\Entity\CrmTime a
        WHERE';
        $timeNome = false;
        $timeId   = false;

        if ($params['q']) {
            $timeNome = $params['q'];
        } elseif ($params['query']) {
            $timeNome = $params['query'];
        }

        if ($params['timeId']) {
            $timeId = $params['timeId'];
        }

        $parameters = array('timeNome' => "{$timeNome}%");
        $sql .= ' a.timeNome LIKE :timeNome';

        if ($timeId) {
            $parameters['timeId'] = explode(',', $timeId);
            $parameters['timeId'] = $timeId;
            $sql .= ' AND a.timeId NOT IN(:timeId)';
        }

        $sql .= " ORDER BY a.timeNome";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['timeId']) {
                /** @var $objCrmTime \Crm\Entity\CrmTime */
                $objCrmTime = $this->getRepository()->find($arrDados['timeId']);

                if (!$objCrmTime) {
                    $this->setLastError('Registro de time não existe!');

                    return false;
                }
            } else {
                $objCrmTime = new \Crm\Entity\CrmTime();
            }

            $objCrmTime->setTimeNome($arrDados['timeNome']);
            $objCrmTime->setTimeLocalizacao($arrDados['timeLocalizacao']);
            $objCrmTime->setTimeSituacao($arrDados['timeSituacao']);

            $this->getEm()->persist($objCrmTime);
            $this->getEm()->flush($objCrmTime);

            $this->getEm()->commit();

            $arrDados['timeId'] = $objCrmTime->getTimeId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de time!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!in_array($arrParam['timeSituacao'], self::getTimeSituacao())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "situação"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM crm__time";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($timeId)
    {
        /** @var $objCrmTime \Crm\Entity\CrmTime */
        $objCrmTime = $this->getRepository()->find($timeId);

        try {
            $arrDados = $objCrmTime->toArray();
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['timeId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['timeNome' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Crm\Entity\CrmTime */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getTimeId();
            $arrEntity[$params['value']] = $objEntity->getTimeNome();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['timeId']) {
            $this->setLastError('Para remover um registro de time é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objCrmTime \Crm\Entity\CrmTime */
            $objCrmTime = $this->getRepository()->find($param['timeId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objCrmTime);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de time.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $view->setVariable("arrTimeSituacao", $this->getArrSelect2TimeSituacao());
    }
}
?>