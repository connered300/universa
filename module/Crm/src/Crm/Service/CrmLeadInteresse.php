<?php

namespace Crm\Service;

use VersaSpine\Service\AbstractService;

class CrmLeadInteresse extends AbstractService
{
    private $lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Crm\Entity\CrmLeadInteresse');
    }

    public function pesquisaForJson($a)
    {
        return array();
    }

    public function pesquisaPeloLead($leadId, $toArray = true)
    {
        $objCrmLeadInteresse = $this->getRepository();
        $arrResult           = $objCrmLeadInteresse->findBy(array('pes' => $leadId));

        if ($toArray) {
            $arrRetorno = array();

            foreach ($arrResult as $objCrmLeadInteresse) {
                $arrRetorno[] = $objCrmLeadInteresse->toArray();
            }

            return $arrRetorno;
        }

        return $arrResult;
    }

    public function retornaInteresseIdPeloLead($leadId)
    {
        $arrInteresses          = array();
        $arrObjCrmLeadInteresse = $this->pesquisaPeloLead($leadId, false);

        foreach ($arrObjCrmLeadInteresse as $objCrmLeadInteresse) {
            $arrInteresses[] = $objCrmLeadInteresse->getInteresse()->getInteresseId();
        }

        return $arrInteresses;
    }

    public function salvarArray($arrInteressesLead, \Crm\Entity\CrmLead $objLead)
    {
        $objEntityManager = $this->getEm();
        $arrExcluir       = array();
        $arrEditar        = array();

        if (is_string($arrInteressesLead)) {
            $arrInteressesLead = explode(',', $arrInteressesLead);
        }

        $arrCrmLeadInteressesDB = $this->pesquisaPeloLead($objLead->getPes()->getPes()->getPesId(), false);

        /** @var \Crm\Entity\CrmLeadInteresse $arrCrmLeadInteresseDB */
        foreach ($arrCrmLeadInteressesDB as $arrCrmLeadInteresseDB) {
            $encontrado         = false;
            $interesseId        = $arrCrmLeadInteresseDB->getInteresse()->getInteresseId();
            $interesseDescricao = $arrCrmLeadInteresseDB->getInteresse()->getInteresseDescricao();

            foreach ($arrInteressesLead as $leadInteresse) {
                if (
                    ($leadInteresse['interesseId'] && $leadInteresse['interesseId'] == $interesseId) ||
                    $leadInteresse == $interesseDescricao
                ) {
                    $encontrado                     = true;
                    $arrEditar[$interesseDescricao] = $arrCrmLeadInteresseDB;
                    break;
                }
            }

            if (!$encontrado) {
                $arrExcluir[$interesseId] = $arrCrmLeadInteresseDB;
            }
        }

        if ($arrExcluir) {
            foreach ($arrExcluir as $objReg) {
                $objEntityManager->remove($objReg);
                $objEntityManager->flush();
            }
        }

        foreach ($arrInteressesLead as $leadInteresse) {
            $objServiceInteresse = new \Crm\Service\CrmInteresse($objEntityManager);
            $interesseDescricao  = is_array($leadInteresse) ? $leadInteresse['interesseDescricao'] : $leadInteresse;

            if (!isset($arrEditar[$interesseDescricao]) && !is_numeric($leadInteresse)) {
                $arrInteresse = array('interesseDescricao' => $leadInteresse);

                if ($objServiceInteresse->save($arrInteresse)) {
                    $leadInteresse = $arrInteresse['interesseId'];
                } else {
                    /** @var  \Crm\Entity\CrmInteresse $objInteresse */
                    $objInteresse = $objServiceInteresse->getRepository()->findOneBy($arrInteresse);
                    $leadInteresse = $objInteresse ? $objInteresse->getInteresseId() : null;
                }
            } elseif (isset($arrEditar[$interesseDescricao])) {
                /** @var  \Crm\Entity\CrmLeadInteresse $objLeadInteresse */
                $objLeadInteresse = $arrEditar[$interesseDescricao];
                $leadInteresse    = $objLeadInteresse->getInteresse()->getInteresseId();
            }

            if (is_numeric($leadInteresse)) {
                /** @var \Crm\Entity\CrmInteresse $objInteresse */
                $objInteresse = $objServiceInteresse->getRepository()->find($leadInteresse);
            } else {
                continue;
            }

            if (!isset($arrEditar[$interesseDescricao])) {
                $objCrmLeadInteresse = new \Crm\Entity\CrmLeadInteresse();
            } else {
                $objCrmLeadInteresse = $arrEditar[$interesseDescricao];
            }

            $objCrmLeadInteresse->setPes($objLead);
            $objCrmLeadInteresse->setInteresse($objInteresse);

            $objEntityManager->persist($objCrmLeadInteresse);
            $objEntityManager->flush($objCrmLeadInteresse);
        }

        return true;
    }

    public function retornaArrayPeloLead($pesId)
    {
        $arrObjCrmLeadInteresse = $this->pesquisaPeloLead($pesId, false);
        $arrCrmLeadInteresse    = array();

        /** @var \Crm\Entity\CrmLeadInteresse $objCrmLeadInteresse */
        foreach ($arrObjCrmLeadInteresse as $objCrmLeadInteresse) {
            $arrItem = $objCrmLeadInteresse->toArray();
            $arrItem = array(
                'leadInteresseId'    => $arrItem['leadInteresseId'],
                'lead'               => $arrItem['pesId'],
                'interesse'          => $arrItem['interesseId'],
                'id'                 => $arrItem['interesseId'],
                'text'               => $arrItem['interesseDescricao'],
                'interesseId'        => $arrItem['interesseId'],
                'interesseDescricao' => $arrItem['interesseDescricao']
            );

            $arrCrmLeadInteresse[] = $arrItem;
        }

        return $arrCrmLeadInteresse;
    }

    protected function valida($dados)
    {
    }
}