<?php

namespace Crm\Service;

use VersaSpine\Service\AbstractService;

class CrmInteresse extends AbstractService
{
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Crm\Entity\CrmInteresse');
    }

    public function pesquisaForJson($params)
    {
        $sql                = '
        SELECT
        a.interesseId as interesse_id,
        a.interesseDescricao as interesse_descricao,
        a.interesseDataCadastro as interesse_data_cadastro
        FROM Crm\Entity\CrmInteresse a
        WHERE';
        $interesseDescricao = false;
        $interesseId        = false;

        if ($params['q']) {
            $interesseDescricao = $params['q'];
        } elseif ($params['query']) {
            $interesseDescricao = $params['query'];
        }

        if ($params['interesseId']) {
            $interesseId = $params['interesseId'];
        }

        $parameters = array('interesseDescricao' => "{$interesseDescricao}%");
        $sql .= ' a.interesseDescricao LIKE :interesseDescricao';

        if ($interesseId) {
            $parameters['interesseId'] = explode(',', $interesseId);
            $parameters['interesseId'] = $interesseId;
            $sql .= ' AND a.interesseId NOT IN(:interesseId)';
        }

        $sql .= " ORDER BY a.interesseDescricao";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['interesseId']) {
                /** @var $objCrmInteresse \Crm\Entity\CrmInteresse */
                $objCrmInteresse = $this->getRepository()->find($arrDados['interesseId']);

                if (!$objCrmInteresse) {
                    $this->setLastError('Registro de interesse não existe!');

                    return false;
                }
            } else {
                $objCrmInteresse = new \Crm\Entity\CrmInteresse();
                $objCrmInteresse->setInteresseDataCadastro(new \Datetime());
            }

            $objCrmInteresse->setInteresseDescricao($arrDados['interesseDescricao']);

            $this->getEm()->persist($objCrmInteresse);
            $this->getEm()->flush($objCrmInteresse);

            $this->getEm()->commit();

            $arrDados['interesseId'] = $objCrmInteresse->getInteresseId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de interesse!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['interesseDescricao']) {
            $errors[] = 'Por favor preencha o campo "descrição"!';
        }

        if ($this->verificaSeRegistroEstaDuplicado($arrParam['interesseDescricao'], $arrParam['interesseId'])) {
            $errors[] = "Já existe um interesse cadastrado com este mesmo nome!";
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function verificaSeRegistroEstaDuplicado($interesseDescricao, $interesseId = false)
    {
        $sql        = 'SELECT count(*) as qtd FROM crm__interesse WHERE interesse_descricao like :interesseDescricao';
        $parameters = array('interesseDescricao' => $interesseDescricao);

        if ($interesseId) {
            $sql .= ' AND interesse_id<>:interesseId';
            $parameters['interesseId'] = $interesseId;
        }

        $result = $this->executeQueryWithParam($sql, $parameters)->fetch();

        return $result['qtd'] > 0;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM crm__interesse";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($interesseId)
    {
        /** @var $objCrmInteresse \Crm\Entity\CrmInteresse */
        $objCrmInteresse = $this->getRepository()->find($interesseId);

        try {
            $arrDados = $objCrmInteresse->toArray();
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['interesseId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['interesseDescricao' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Crm\Entity\CrmInteresse */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getInteresseId();
            $arrEntity[$params['value']] = $objEntity->getInteresseDescricao();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['interesseId']) {
            $this->setLastError('Para remover um registro de interesse é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objCrmInteresse \Crm\Entity\CrmInteresse */
            $objCrmInteresse = $this->getRepository()->find($param['interesseId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objCrmInteresse);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de interesse.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
    }
}
?>