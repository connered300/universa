<?php

namespace Crm\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class CrmMediador
 * @package Crm\Service
 */
class CrmMediador extends AbstractService
{
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Crm\Entity\CrmMediador');
    }

    /**
     * @param $params
     * @return array
     */
    public function pesquisaForJson($params)
    {
        $sql          = '
        SELECT
        a.mediadorId AS mediador_id,
        a.mediadorNome AS mediador_nome,
        a.mediadorEmail AS mediador_email
        FROM Crm\Entity\CrmMediador a
        WHERE';
        $mediadorNome = false;
        $mediadorId   = false;

        if ($params['q']) {
            $mediadorNome = $params['q'];
        } elseif ($params['query']) {
            $mediadorNome = $params['query'];
        }

        if ($params['mediadorId']) {
            $mediadorId = $params['mediadorId'];
        }

        $parameters = array('mediadorNome' => "{$mediadorNome}%");
        $sql .= ' a.mediadorNome LIKE :mediadorNome';

        if ($mediadorId) {
            $parameters['mediadorId'] = explode(',', $mediadorId);
            $parameters['mediadorId'] = $mediadorId;
            $sql .= ' AND a.mediadorId NOT IN(:mediadorId)';
        }

        $sql .= " ORDER BY a.mediadorNome";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    public function retornaMediadoresComEmailCadastrado()
    {
        $sql = '
        SELECT a
        FROM Crm\Entity\CrmMediador a
        WHERE a.mediadorEmail IS NOT NULL
        ORDER BY a.mediadorNome';

        $query = $this->getEm()->createQuery($sql);

        $arrCrmMediador = $query->getResult();

        $arrEmailsRemetenteLead = [];

        /** @var \Crm\Entity\CrmMediador $objMediador */
        foreach ($arrCrmMediador as $objMediador) {
            $arrEmails = explode(",", $objMediador->getMediadorEmail());

            foreach ($arrEmails as $email) {
                $arrEmailsRemetenteLead[$email] = $objMediador->getMediadorId();
            }
        }

        return $arrEmailsRemetenteLead;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['mediadorId']) {
                /** @var $objCrmMediador \Crm\Entity\CrmMediador */
                $objCrmMediador = $this->getRepository()->find($arrDados['mediadorId']);

                if (!$objCrmMediador) {
                    $this->setLastError('Registro de mediador não existe!');

                    return false;
                }
            } else {
                $objCrmMediador = new \Crm\Entity\CrmMediador();
            }

            $objCrmMediador->setMediadorNome($arrDados['mediadorNome']);
            $objCrmMediador->setMediadorEmail($arrDados['mediadorEmail']);

            $this->getEm()->persist($objCrmMediador);
            $this->getEm()->flush($objCrmMediador);

            $this->getEm()->commit();

            $arrDados['mediadorId'] = $objCrmMediador->getMediadorId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de mediador!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['mediadorNome']) {
            $errors[] = 'Por favor preencha o campo "nome"!';
        }

        if ($this->verificaSeRegistroEstaDuplicado($arrParam['mediadorNome'], $arrParam['mediadorId'])) {
            $errors[] = "Já existe um mediador cadastrado com este mesmo nome!";
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    /**
     * @param            $mediadorNome
     * @param bool|false $mediadorId
     * @return bool
     */
    public function verificaSeRegistroEstaDuplicado($mediadorNome, $mediadorId = false)
    {
        $sql        = 'SELECT count(*) AS qtd FROM crm__mediador WHERE mediador_nome LIKE :mediadorNome';
        $parameters = array('mediadorNome' => $mediadorNome);

        if ($mediadorId) {
            $sql .= ' AND mediador_id<>:mediadorId';
            $parameters['mediadorId'] = $mediadorId;
        }

        $result = $this->executeQueryWithParam($sql, $parameters)->fetch();

        return $result['qtd'] > 0;
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM crm__mediador";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $mediadorId
     * @return array
     */
    public function getArray($mediadorId)
    {
        /** @var $objCrmMediador \Crm\Entity\CrmMediador */
        $objCrmMediador = $this->getRepository()->find($mediadorId);

        try {
            $arrDados = $objCrmMediador->toArray();
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {

        return $arrDados;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['mediadorId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['mediadorNome' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Crm\Entity\CrmMediador */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getMediadorId();
            $arrEntity[$params['value']] = $objEntity->getMediadorNome();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $mediador
     * @return \Crm\Entity\CrmMediador|null
     */
    public function getMediadorObj($mediador)
    {
        $arrParam = array();

        if (!$mediador) {
            return null;
        }

        if (is_numeric($mediador)) {
            $arrParam['mediadorId'] = $mediador;
        } elseif (is_string($mediador)) {
            $arrParam['mediadorNome'] = $mediador;
        }

        /** @var \Crm\Entity\CrmMediador $objEntity */
        $objEntity = $this->getRepository()->findOneBy($arrParam, ['mediadorId' => 'asc']);

        if (!$objEntity && is_string($mediador)) {
            if ($this->save($arrParam)) {
                $objEntity = $this->getRepository()->find($arrParam['mediadorId']);
            }
        }

        return $objEntity;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['mediadorId']) {
            $this->setLastError('Para remover um registro de mediador é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objCrmMediador \Crm\Entity\CrmMediador */
            $objCrmMediador = $this->getRepository()->find($param['mediadorId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objCrmMediador);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de mediador.');

            return false;
        }

        return true;
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
    }
}
?>