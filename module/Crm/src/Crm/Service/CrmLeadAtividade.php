<?php

namespace Crm\Service;

use VersaSpine\Service\AbstractService;

class CrmLeadAtividade extends AbstractService
{
    const ATIVIDADE_FORMA_CONTATO_EMAIL = 'Email';
    const ATIVIDADE_FORMA_CONTATO_SMS = 'Sms';
    const ATIVIDADE_FORMA_CONTATO_TELEFONE = 'Telefone';
    const ATIVIDADE_FORMA_CONTATO_WHATSAPP = 'Whatsapp';
    const ATIVIDADE_FORMA_CONTATO_OUTRO = 'Outro';

    const ATIVIDADE_FORMA_CONTATO_AGENDAMENTO_EMAIL = 'Email';
    const ATIVIDADE_FORMA_CONTATO_AGENDAMENTO_SMS = 'Sms';
    const ATIVIDADE_FORMA_CONTATO_AGENDAMENTO_TELEFONE = 'Telefone';
    const ATIVIDADE_FORMA_CONTATO_AGENDAMENTO_WHATSAPP = 'Whatsapp';
    const ATIVIDADE_FORMA_CONTATO_AGENDAMENTO_OUTRO = 'Outro';

    const ATIVIDADE_DESISTENCIA_SIM = 'Sim';
    const ATIVIDADE_DESISTENCIA_NAO = 'Não';
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2AtividadeFormaContato($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getAtividadeFormaContato());
    }

    public static function getAtividadeFormaContato()
    {
        return array(
            self::ATIVIDADE_FORMA_CONTATO_EMAIL,
            self::ATIVIDADE_FORMA_CONTATO_SMS,
            self::ATIVIDADE_FORMA_CONTATO_TELEFONE,
            self::ATIVIDADE_FORMA_CONTATO_WHATSAPP,
            self::ATIVIDADE_FORMA_CONTATO_OUTRO
        );
    }

    public function getArrSelect2AtividadeFormaContatoAgendamento($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getAtividadeFormaContatoAgendamento());
    }

    public static function getAtividadeFormaContatoAgendamento()
    {
        return array(
            self::ATIVIDADE_FORMA_CONTATO_AGENDAMENTO_EMAIL,
            self::ATIVIDADE_FORMA_CONTATO_AGENDAMENTO_SMS,
            self::ATIVIDADE_FORMA_CONTATO_AGENDAMENTO_TELEFONE,
            self::ATIVIDADE_FORMA_CONTATO_AGENDAMENTO_WHATSAPP,
            self::ATIVIDADE_FORMA_CONTATO_AGENDAMENTO_OUTRO
        );
    }

    public function getArrSelect2AtividadeDesistencia($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getAtividadeDesistencia());
    }

    public static function getAtividadeDesistencia()
    {
        return array(self::ATIVIDADE_DESISTENCIA_SIM, self::ATIVIDADE_DESISTENCIA_NAO);
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Crm\Entity\CrmLeadAtividade');
    }

    public function pesquisaForJson($params)
    {
        $sql         = '
        SELECT
        a.atividadeId AS atividade_id,
        a.pesId AS pes_id,
        a.atividadeDataCadastro AS atividade_data_cadastro,
        a.operadorId AS operador_id,
        a.atividadeTipoId AS atividade_tipo_id,
        a.atividadeAgendamento AS atividade_agendamento,
        a.atividadeFormaContato AS atividade_forma_contato,
        a.atividadeFormaContatoAgendamento AS atividade_forma_contato_agendamento,
        a.atividadeComentario AS atividade_comentario,
        a.atividadeDesistencia AS atividade_desistencia
        FROM Crm\Entity\CrmLeadAtividade a
        WHERE';
        $pesId       = false;
        $atividadeId = false;

        if ($params['q']) {
            $pesId = $params['q'];
        } elseif ($params['query']) {
            $pesId = $params['query'];
        }

        if ($params['atividadeId']) {
            $atividadeId = $params['atividadeId'];
        }

        $parameters = array('pesId' => "{$pesId}%");
        $sql .= ' a.pesId LIKE :pesId';

        if ($atividadeId) {
            $parameters['atividadeId'] = explode(',', $atividadeId);
            $parameters['atividadeId'] = $atividadeId;
            $sql .= ' AND a.atividadeId NOT IN(:atividadeId)';
        }

        $sql .= " ORDER BY a.pesId";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$arrDados['pes'] && $arrDados['pesId']) {
            $arrDados['pes'] = $arrDados['pesId'];
        }

        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceCrmLead          = new \Crm\Service\CrmLead($this->getEm());
        $serviceCrmOperador      = new \Crm\Service\CrmOperador($this->getEm());
        $serviceCrmAtividadeTipo = new \Crm\Service\CrmAtividadeTipo($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['atividadeId']) {
                /** @var $objCrmLeadAtividade \Crm\Entity\CrmLeadAtividade */
                $objCrmLeadAtividade = $this->getRepository()->find($arrDados['atividadeId']);

                if (!$objCrmLeadAtividade) {
                    $this->setLastError('Registro de atividade de lead  não existe!');

                    return false;
                }
            } else {
                $objCrmLeadAtividade = new \Crm\Entity\CrmLeadAtividade();
            }

            if ($arrDados['pes']) {
                /** @var $objCrmLead \Crm\Entity\CrmLead */
                $objCrmLead = $serviceCrmLead->getRepository()->find($arrDados['pes']);

                if (!$objCrmLead) {
                    $this->setLastError('Registro de lead não existe!');

                    return false;
                }

                $objCrmLeadAtividade->setPes($objCrmLead);
            } else {
                $objCrmLeadAtividade->setPes(null);
            }

            /** @var $objCrmOperador \Crm\Entity\CrmOperador */
            $objCrmOperador = $serviceCrmOperador->retornaOperadorLogado($arrDados['operador']);

            if (!$objCrmOperador) {
                $this->setLastError('Registro de operador inválido!');

                return false;
            }

            $objCrmLeadAtividade->setOperador($objCrmOperador);

            if ($arrDados['atividadeTipo']) {
                /** @var $objCrmAtividadeTipo \Crm\Entity\CrmAtividadeTipo */
                $objCrmAtividadeTipo = $serviceCrmAtividadeTipo->getRepository()->find($arrDados['atividadeTipo']);

                if (!$objCrmAtividadeTipo) {
                    $this->setLastError('Registro de tipo atividade não existe!');

                    return false;
                }

                $objCrmLeadAtividade->setAtividadeTipo($objCrmAtividadeTipo);
            } else {
                $objCrmLeadAtividade->setAtividadeTipo(null);
            }

            $objCrmLeadAtividade->setAtividadeDataCadastro(
                $arrDados['atividadeDataCadastro'] ? $arrDados['atividadeDataCadastro'] : new \DateTime()
            );
            $objCrmLeadAtividade->setAtividadeAgendamento($arrDados['atividadeAgendamento']);
            $objCrmLeadAtividade->setAtividadeFormaContato($arrDados['atividadeFormaContato']);
            $objCrmLeadAtividade->setAtividadeFormaContatoAgendamento($arrDados['atividadeFormaContatoAgendamento']);
            $objCrmLeadAtividade->setAtividadeComentario($arrDados['atividadeComentario']);
            $objCrmLeadAtividade->setAtividadeDesistencia($arrDados['atividadeDesistencia']);

            if ($objCrmAtividadeTipo->getAtividadeLeadStatus()) {
                $objCrmLead->setLeadStatus($objCrmAtividadeTipo->getAtividadeLeadStatus());
            }

            if ($arrDados['atividadeDesistencia'] == self::ATIVIDADE_DESISTENCIA_SIM) {
                $objCrmLead->setLeadStatus($serviceCrmLead::LEAD_STATUS_LIXO);
            }

            $this->getEm()->persist($objCrmLead);
            $this->getEm()->flush($objCrmLead);

            $this->getEm()->persist($objCrmLeadAtividade);
            $this->getEm()->flush($objCrmLeadAtividade);

            $this->getEm()->commit();

            $arrDados['atividadeId'] = $objCrmLeadAtividade->getAtividadeId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de atividade de lead !<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['pes']) {
            $errors[] = 'Por favor preencha o campo "código pessoa"!';
        }

        if (!$arrParam['atividadeTipo']) {
            $errors[] = 'Por favor preencha o campo "código tipo atividade"!';
        }

        if (!in_array($arrParam['atividadeFormaContato'], self::getAtividadeFormaContato())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "contato forma"!';
        }

        if (
            $arrParam['atividadeFormaContatoAgendamento'] &&
            !in_array($arrParam['atividadeFormaContatoAgendamento'], self::getAtividadeFormaContatoAgendamento())
        ) {
            $errors[] = 'Por favor selecione um valor válido para o campo "agendamento contato forma"!';
        }

        if (
            $arrParam['atividadeDesistencia'] &&
            !in_array($arrParam['atividadeDesistencia'], self::getAtividadeDesistencia())
        ) {
            $errors[] = 'Por favor selecione um valor válido para o campo "desistencia"!';
        }

        if ($arrParam['atividadeAgendamento']) {
            $atividadeAgendamento = new \Datetime(self::formatDateAmericano($arrParam['atividadeAgendamento']));

            if ($atividadeAgendamento < new \Datetime()) {
                $errors[] = 'Por favor selecione uma data válida para o agendamento!';
            }
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $where = null;

        $where = null;

        if ($data['filter']['mediador']) {
            $where .= "AND lead.mediador_id in ({$data['filter']['mediador']})";
        }

        if ($data['filter']['campanha']) {
            $where .= "AND lead.campanha_id in ({$data['filter']['campanha']})";
        }

        if ($data['filter']['fonte']) {
            $where .= "AND lead.fonte_id in ({$data['filter']['fonte']})";
        }

        if ($data['filter']['atividadeTipo']) {
            $where .= "AND atividade.atividade_tipo_id in ({$data['filter']['atividadeTipo']})";
        }

        if ($data['filter']['operador']) {
            $where .= "AND crm__operador.operador_id in ({$data['filter']['operador']})";
        }

        if ($data['filter']['pes']) {
            $where .= "AND crm__lead_atividade.pes_id in ({$data['filter']['pes']})";
        }

        if ($data['filter']['leadStatus']) {
            $turno = $data['filter']['leadStatus'];
            $turno = is_array($turno) ? implode(',', $turno) : $turno;
            $where .= "AND FIND_IN_SET(lead.lead_status, '{$turno}')";
        }

        if ($data['filter']['dataCadastroInicial']) {
            $dataCadastroInicial = self::formatDateAmericano($data['filter']['dataCadastroInicial']);
            $where .= " AND date(lead.lead_data_cadastro) >= " . "'" . $dataCadastroInicial . "'";
        }

        if ($data['filter']['dataCadastroFinal']) {
            $dataCadastroFinal = self::formatDateAmericano($data['filter']['dataCadastroFinal']);
            $where .= " AND date(lead.lead_data_cadastro) <= " . "'" . $dataCadastroFinal . "'";
        }

        if ($data['filter']['agendamentoInicial']) {
            $agendamentoInicial = self::formatDateAmericano($data['filter']['agendamentoInicial']);
            $where .= " AND date(crm__lead_atividade.atividade_agendamento) >= " . "'" . $agendamentoInicial . "'";
        }

        if ($data['filter']['agendamentoFinal']) {
            $agendamentoFinal = self::formatDateAmericano($data['filter']['agendamentoFinal']);
            $where .= " AND date(crm__lead_atividade.atividade_agendamento) <= " . "'" . $agendamentoFinal . "'";
        }

        if ($data['filter']['dataAtividadeInicial']) {
            $dataAtividadeInicial = self::formatDateAmericano($data['filter']['dataAtividadeInicial']);
            $where .= " AND date(atividade_data_cadastro) >= " . "'" . $dataAtividadeInicial . "'";
        }

        if ($data['filter']['dataAtividadeFinal']) {
            $dataAtividadeFinal = self::formatDateAmericano($data['filter']['dataAtividadeFinal']);
            $where .= " AND date(atividade_data_cadastro) <= " . "'" . $dataAtividadeFinal . "'";
        }

        $query = "
        SELECT
            crm__lead_atividade.atividade_id as atividadeId,
            crm__lead_atividade.*,
            lead.lead_data_cadastro,
            pes_nome,
            pes_cpf,
            login,
            time_nome,
            concat(login, ' / ', time_nome) AS operador_nome,
            atividade_tipo_nome
        FROM
            crm__lead_atividade
            LEFT JOIN crm__lead AS lead ON lead.pes_id = crm__lead_atividade.pes_id
            LEFT JOIN view__crm_atividade_tipo AS atividade
                ON atividade.atividade_tipo_id = crm__lead_atividade.atividade_tipo_id
            LEFT JOIN pessoa ON pessoa.pes_id = lead.pes_id
            LEFT JOIN pessoa_fisica ON pessoa_fisica.pes_id = pessoa.pes_id
            LEFT JOIN crm__operador ON crm__operador.operador_id = crm__lead_atividade.operador_id
            LEFT JOIN acesso_pessoas ON acesso_pessoas.id = crm__operador.operador_id
            LEFT JOIN crm__time ON crm__time.time_id = crm__operador.time_id
        WHERE 1 " . $where;

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($atividadeId)
    {
        $arrDados = array();

        if (!$atividadeId) {
            $this->setLastError('Atividade de lead  inválido!');

            return array();
        }

        /** @var $objCrmLeadAtividade \Crm\Entity\CrmLeadAtividade */
        $objCrmLeadAtividade = $this->getRepository()->find($atividadeId);

        if (!$objCrmLeadAtividade) {
            $this->setLastError('Atividade de lead  não existe!');

            return array();
        }

        $serviceCrmLead          = new \Crm\Service\CrmLead($this->getEm());
        $serviceCrmOperador      = new \Crm\Service\CrmOperador($this->getEm());
        $serviceCrmAtividadeTipo = new \Crm\Service\CrmAtividadeTipo($this->getEm());

        try {
            $arrDados = $objCrmLeadAtividade->toArray();

            if ($arrDados['pes']) {
                $arrCrmLead      = $serviceCrmLead->getArrSelect2(['id' => $arrDados['pes']]);
                $arrDados['pes'] = $arrCrmLead ? $arrCrmLead[0] : null;
            }

            if ($arrDados['operador']) {
                $arrCrmOperador       = $serviceCrmOperador->getArrSelect2(['id' => $arrDados['operador']]);
                $arrDados['operador'] = $arrCrmOperador ? $arrCrmOperador[0] : null;
            }

            if ($arrDados['atividadeTipo']) {
                $arrCrmAtividadeTipo       = $serviceCrmAtividadeTipo->getArrSelect2(
                    ['id' => $arrDados['atividadeTipo']]
                );
                $arrDados['atividadeTipo'] = $arrCrmAtividadeTipo ? $arrCrmAtividadeTipo[0] : null;
            }
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceCrmLead          = new \Crm\Service\CrmLead($this->getEm());
        $serviceCrmOperador      = new \Crm\Service\CrmOperador($this->getEm());
        $serviceCrmAtividadeTipo = new \Crm\Service\CrmAtividadeTipo($this->getEm());

        if (is_array($arrDados['pes']) && !$arrDados['pes']['text']) {
            $arrDados['pes'] = $arrDados['pes']['pesId'];
        }

        if ($arrDados['pes'] && is_string($arrDados['pes'])) {
            $arrDados['pes'] = $serviceCrmLead->getArrSelect2(array('id' => $arrDados['pes']));
            $arrDados['pes'] = $arrDados['pes'] ? $arrDados['pes'][0] : null;
        }

        if (is_array($arrDados['operador']) && !$arrDados['operador']['text']) {
            $arrDados['operador'] = $arrDados['operador']['operadorId'];
        }

        if ($arrDados['operador'] && is_string($arrDados['operador'])) {
            $arrDados['operador'] = $serviceCrmOperador->getArrSelect2(array('id' => $arrDados['operador']));
            $arrDados['operador'] = $arrDados['operador'] ? $arrDados['operador'][0] : null;
        }

        if (is_array($arrDados['atividadeTipo']) && !$arrDados['atividadeTipo']['text']) {
            $arrDados['atividadeTipo'] = $arrDados['atividadeTipo']['atividadeTipoId'];
        }

        if ($arrDados['atividadeTipo'] && is_string($arrDados['atividadeTipo'])) {
            $arrDados['atividadeTipo'] = $serviceCrmAtividadeTipo->getArrSelect2(
                array('id' => $arrDados['atividadeTipo'])
            );
            $arrDados['atividadeTipo'] = $arrDados['atividadeTipo'] ? $arrDados['atividadeTipo'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['atividadeId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['pesId' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Crm\Entity\CrmLeadAtividade */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getAtividadeId();
            $arrEntity[$params['value']] = $objEntity->getPes()->getPes()->getPes()->getPesId();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($atividadeId)
    {
        if (!$atividadeId) {
            $this->setLastError('Para remover um registro de atividade de lead  é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objCrmLeadAtividade \Crm\Entity\CrmLeadAtividade */
            $objCrmLeadAtividade = $this->getRepository()->find($atividadeId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objCrmLeadAtividade);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de atividade de lead .');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $view->setVariables(
            [
                "arrAtividadeFormaContato"            => $this->getArrSelect2AtividadeFormaContato(),
                "arrAtividadeFormaContatoAgendamento" => $this->getArrSelect2AtividadeFormaContatoAgendamento(),
                "arrAtividadeDesistencia"             => $this->getArrSelect2AtividadeDesistencia()
            ]
        );
    }
}
?>