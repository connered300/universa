<?php

namespace Crm\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class CrmCampanha
 * @package Crm\Service
 */
class CrmCampanha extends AbstractService
{
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Crm\Entity\CrmCampanha');
    }

    /**
     * @param $params
     * @return array
     */
    public function pesquisaForJson($params)
    {
        $sql          = '
        SELECT
        a.campanhaId as campanha_id,
        a.campanhaNome as campanha_nome
        FROM Crm\Entity\CrmCampanha a
        WHERE';
        $campanhaNome = false;
        $campanhaId   = false;

        if ($params['q']) {
            $campanhaNome = $params['q'];
        } elseif ($params['query']) {
            $campanhaNome = $params['query'];
        }

        if ($params['campanhaId']) {
            $campanhaId = $params['campanhaId'];
        }

        $parameters = array('campanhaNome' => "{$campanhaNome}%");
        $sql .= ' a.campanhaNome LIKE :campanhaNome';

        if ($campanhaId) {
            $parameters['campanhaId'] = explode(',', $campanhaId);
            $parameters['campanhaId'] = $campanhaId;
            $sql .= ' AND a.campanhaId NOT IN(:campanhaId)';
        }

        $sql .= " ORDER BY a.campanhaNome";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['campanhaId']) {
                /** @var $objCrmCampanha \Crm\Entity\CrmCampanha */
                $objCrmCampanha = $this->getRepository()->find($arrDados['campanhaId']);

                if (!$objCrmCampanha) {
                    $this->setLastError('Registro de campanha não existe!');

                    return false;
                }
            } else {
                $objCrmCampanha = new \Crm\Entity\CrmCampanha();
            }

            $objCrmCampanha->setCampanhaNome($arrDados['campanhaNome']);

            $this->getEm()->persist($objCrmCampanha);
            $this->getEm()->flush($objCrmCampanha);

            $this->getEm()->commit();

            $arrDados['campanhaId'] = $objCrmCampanha->getCampanhaId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de campanha!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['campanhaNome']) {
            $errors[] = 'Por favor preencha o campo "nome"!';
        }

        if ($this->verificaSeRegistroEstaDuplicado($arrParam['campanhaNome'], $arrParam['campanhaId'])) {
            $errors[] = "Já existe uma campanha cadastrada com este mesmo nome!";
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    /**
     * @param $campanha
     * @return \Crm\Entity\CrmCampanha|null
     */
    public function getCampanhaObj($campanha)
    {
        $arrParam = array();

        if (!$campanha) {
            return null;
        }

        if (is_numeric($campanha)) {
            $arrParam['campanhaId'] = $campanha;
        } elseif (is_string($campanha)) {
            $arrParam['campanhaNome'] = $campanha;
        }

        /** @var \Crm\Entity\CrmCampanha $objEntity */
        $objEntity = $this->getRepository()->findOneBy($arrParam, ['campanhaId' => 'asc']);

        if (!$objEntity && is_string($campanha)) {
            if ($this->save($arrParam)) {
                $objEntity = $this->getRepository()->find($arrParam['campanhaId']);
            }
        }

        return $objEntity;
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM crm__campanha";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $campanhaId
     * @return array
     */
    public function getArray($campanhaId)
    {
        /** @var $objCrmCampanha \Crm\Entity\CrmCampanha */
        $objCrmCampanha = $this->getRepository()->find($campanhaId);

        try {
            $arrDados = $objCrmCampanha->toArray();
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {

        return $arrDados;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['campanhaId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['campanhaNome' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Crm\Entity\CrmCampanha */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getCampanhaId();
            $arrEntity[$params['value']] = $objEntity->getCampanhaNome();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['campanhaId']) {
            $this->setLastError('Para remover um registro de campanha é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objCrmCampanha \Crm\Entity\CrmCampanha */
            $objCrmCampanha = $this->getRepository()->find($param['campanhaId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objCrmCampanha);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de campanha.');

            return false;
        }

        return true;
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
    }

    /**
     * @param            $campanhaNome
     * @param bool|false $campanhaId
     * @return bool
     */
    public function verificaSeRegistroEstaDuplicado($campanhaNome, $campanhaId = false)
    {
        $sql        = 'SELECT count(*) as qtd FROM crm__campanha WHERE campanha_nome like :campanhaNome';
        $parameters = array('campanhaNome' => $campanhaNome);

        if ($campanhaId) {
            $sql .= ' AND campanha_id<>:campanhaId';
            $parameters['campanhaId'] = $campanhaId;
        }

        $result = $this->executeQueryWithParam($sql, $parameters)->fetch();

        return $result['qtd'] > 0;
    }
}
?>