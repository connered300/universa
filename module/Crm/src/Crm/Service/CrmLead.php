<?php

namespace Crm\Service;

use VersaSpine\Service\AbstractService;

class CrmLead extends AbstractService
{
    const LEAD_STATUS_NOVO        = 'Novo';
    const LEAD_STATUS_ATRIBUIDO   = 'Atribuído';
    const LEAD_STATUS_EM_PROCESSO = 'Em processo';
    const LEAD_STATUS_CONVERTIDO  = 'Convertido';
    const LEAD_STATUS_RECICLADO   = 'Reciclado';
    const LEAD_STATUS_LIXO        = 'Lixo';

    const LEAD_DISTRIBUINDO_SIM = 'Sim';
    const LEAD_DISTRIBUINDO_NAO = 'Nao';

    private $__lastError = null;
    /** @var \Sistema\Service\SisConfig */
    private $config = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2LeadStatus($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getLeadStatus());
    }

    public static function getLeadStatus()
    {
        return array(
            self::LEAD_STATUS_NOVO,
            self::LEAD_STATUS_ATRIBUIDO,
            self::LEAD_STATUS_EM_PROCESSO,
            self::LEAD_STATUS_CONVERTIDO,
            self::LEAD_STATUS_RECICLADO,
            self::LEAD_STATUS_LIXO
        );
    }

    /**
     * @return \Sistema\Service\SisConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param \Sistema\Service\SisConfig $config
     * @return CrmLead
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     * @param array                       $arrConfig
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, $arrConfig = array())
    {
        parent::__construct($em, 'Crm\Entity\CrmLead');
        $this->setConfig(new \Sistema\Service\SisConfig($em, $arrConfig));
    }

    public function pesquisaForJson($params)
    {
        $sql   = '
    SELECT
        pessoa.pes_id, pessoa.pes_id pesId,
        pessoa.pes_nome, pessoa.pes_nome pesNome,
        pessoaFisica.pes_cpf, pessoaFisica.pes_cpf pesCpf,
        pessoaFisica.pes_doc_estrangeiro, pessoaFisica.pes_doc_estrangeiro pesDocEstrangeiro,
        pessoa.pes_nacionalidade, pessoa.pes_nacionalidade pesNacionalidade,
        pessoaFisica.pes_rg, pessoaFisica.pes_rg pesRg,
        pessoaFisica.pes_sexo, pessoaFisica.pes_sexo pesSexo,
        date_format(pessoaFisica.pes_data_nascimento, "%d/%m/%Y") pes_data_nascimento,
        date_format(pessoaFisica.pes_data_nascimento, "%d/%m/%Y") pesDataNascimento,
        endereco.end_estado, endereco.end_estado endEstado,
        endereco.end_cidade, endereco.end_cidade endCidade,
        fonte_id,
        mediador_id,
        campanha_id,
        operador_id,
        lead_obs,
        lead_data_cadastro,
        lead_data_alteracao,
        lead_status
    FROM pessoa
        LEFT JOIN pessoa_fisica pessoaFisica USING (pes_id)
        LEFT JOIN crm__lead lead USING (pes_id)
        LEFT JOIN endereco USING (pes_id)
    WHERE';
        $query = false;

        if ($params['query']) {
            $query = trim($params['query']);
        }

        if ($params['pesId']) {
            $parameters['pes_id'] = trim($params['pesId']);
            $sql .= ' pessoa.pes_id = :pes_id';
        } elseif ($params['pesCpf']) {
            $parameters['pes_cpf'] = trim($params['pesCpf']);
            $sql .= ' pes_cpf like :pes_cpf';
        } elseif ($params['pesDocEstrangeiro']) {
            $parameters['pes_doc_estrangeiro'] = trim($params['pesDocEstrangeiro']);
            $sql .= ' pes_doc_estrangeiro = :pes_doc_estrangeiro';
        } else {
            $parameters = array(
                'pes_nome' => "{$query}%",
                'pes_id'   => ltrim($query, '0') . "%"
            );
            $sql .= ' (pessoa.pes_id*1 LIKE :pes_id OR pes_nome COLLATE UTF8_GENERAL_CI LIKE :pes_nome)';
        }

        $sql .= " ORDER BY pes_nome";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function pesquisarPessoaPelosDados($arrDados)
    {
        $servicePessoaFisica = new \Pessoa\Service\PessoaFisica($this->getEm());

        $arrDadosPessoa = $servicePessoaFisica->pesquisarPessoaPelosDados($arrDados, true);
        $arrDadosLead   = $this->getArray($arrDadosPessoa['pesId']);

        if ($arrDados['importacao']) {
            $arrDados['interesse']  = array_merge_recursive($arrDados['interesse'], $arrDadosLead['interesse']);
            $arrDados['operador']   = $arrDadosLead['operador'] ? $arrDadosLead['operador'] : $arrDados['operador'];
            $arrDados['leadStatus'] = $arrDadosLead['leadStatus'] ? $arrDadosLead['leadStatus']
                : $arrDados['leadStatus'];
            $arrDados['fonte']      = $arrDadosLead['fonte'] ? $arrDadosLead['fonte'] : $arrDados['fonte'];
            $arrDados['campanha']   = $arrDadosLead['campanha'] ? $arrDadosLead['campanha'] : $arrDados['campanha'];
            $arrDados['mediador']   = $arrDadosLead['mediador'] ? $arrDadosLead['mediador'] : $arrDados['mediador'];
            $arrDados['leadObs']    = trim(
                str_replace($arrDados['leadObs'], '', $arrDadosLead['leadObs']) .
                "\n" .
                $arrDados['leadObs']
            );
        }

        $arrDados = array_merge(
            array_filter($arrDadosPessoa),
            array_filter($arrDadosLead),
            array_filter($arrDados)
        );

        return $arrDados;
    }

    public function save(array &$arrDados)
    {
        $novoLead = false;

        if (!$arrDados['leadStatus']) {
            $arrDados['leadStatus'] = self::LEAD_STATUS_NOVO;
        }

        if (!$this->valida($arrDados)) {
            return false;
        }

        $servicePessoaFisica     = new \Pessoa\Service\PessoaFisica($this->getEm());
        $serviceCrmFonte         = new \Crm\Service\CrmFonte($this->getEm());
        $serviceCrmMediador      = new \Crm\Service\CrmMediador($this->getEm());
        $serviceCrmCampanha      = new \Crm\Service\CrmCampanha($this->getEm());
        $serviceCrmOperador      = new \Crm\Service\CrmOperador($this->getEm());
        $serviceCrmLeadInteresse = new \Crm\Service\CrmLeadInteresse($this->getEm());
        $serviceCrmLeadEvento    = new \Crm\Service\CrmLeadEvento($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            $objCrmLead = null;
            $arrDados   = $this->pesquisarPessoaPelosDados($arrDados);

            if ($arrDados['pesId']) {
                /** @var $objCrmLead \Crm\Entity\CrmLead */
                $objCrmLead = $this->getRepository()->find($arrDados['pesId']);
            }

            if (!$objCrmLead) {
                $objCrmLead = new \Crm\Entity\CrmLead();
                $objCrmLead->setLeadDataCadastro(new \DateTime());
                $novoLead = true;
            }

            //Altera/Cria Pessoa Fisica
            $objPessoaFisica = $servicePessoaFisica->salvarPessoaFisica(
                $arrDados['pesId'],
                $arrDados,
                true,
                true,
                true
            );

            $objCrmLead->setPes($objPessoaFisica);

            $arrDadosLead = $objCrmLead->toArray();

            /** @var $objCrmFonte \Crm\Entity\CrmFonte */
            $objCrmFonte = $serviceCrmFonte->getFonteObj($arrDados['fonte']);
            /** @var $objCrmMediador \Crm\Entity\CrmMediador */
            $objCrmMediador = $serviceCrmMediador->getMediadorObj($arrDados['mediador']);
            /** @var $objCrmCampanha \Crm\Entity\CrmCampanha */
            $objCrmCampanha = $serviceCrmCampanha->getCampanhaObj($arrDados['campanha']);

            $objCrmLead->setFonte($objCrmFonte);
            $objCrmLead->setMediador($objCrmMediador);
            $objCrmLead->setCampanha($objCrmCampanha);

            if ($arrDados['operador']) {
                /** @var $objCrmOperador \Crm\Entity\CrmOperador */
                $objCrmOperador = $serviceCrmOperador->getRepository()->find($arrDados['operador']);

                if (!$objCrmOperador) {
                    $this->setLastError('Registro de operador não existe!');

                    return false;
                }

                $objCrmLead->setOperador($objCrmOperador);
            } else {
                $objCrmLead->setOperador(null);
            }

            $objCrmLead->setLeadObs($arrDados['leadObs']);
            $objCrmLead->setLeadStatus($arrDados['leadStatus']);

            $arrDadosLeadNovo = $objCrmLead->toArray();

            $arrDiff = array_diff_assoc($arrDadosLead, $arrDadosLeadNovo);

            if ($arrDiff && !$novoLead) {
                if (count(array_intersect(array_keys($arrDiff), ['mediador', 'fonte', 'campanha'])) > 0) {
                    $serviceCrmLeadEvento->save($arrDados);
                }
            }

            if ($arrDiff || $novoLead) {
                $objCrmLead->setLeadDataAlteracao(new \DateTime());
            }

            if (in_array('operador', array_keys($arrDiff))) {
                $objCrmLead->setLeadDataAtribuicao(new \DateTime());
            }

            $arrInteresse = array();

            if ($arrDados['leadInteresse']) {
                $arrInteresse = $arrDados['leadInteresse'];
            }

            if ($arrDados['interesse']) {
                $arrInteresse = $arrDados['interesse'];
            }

            $this->getEm()->persist($objCrmLead);
            $this->getEm()->flush($objCrmLead);

            $serviceCrmLeadInteresse->salvarArray($arrInteresse, $objCrmLead);

            $this->getEm()->commit();

            $arrDados['pesId'] = $objCrmLead->getPes()->getPes()->getPesId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de lead!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['leadStatus']) {
            $errors[] = 'Por favor preencha o campo "status"!';
        }

        if (!$arrParam['conContatoEmail'] && !$arrParam['conContatoEmail'] && !$arrParam['conContatoCelular']) {
            $errors[] = 'Por favor informe ao menos uma informação de contato!';
        }

        if (!in_array($arrParam['leadStatus'], self::getLeadStatus())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "status"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $where = $this->retornaFiltrosLeads($data['filter']);
        $query = "
        SELECT
            lead.pes_id,
            lead.lead_data_alteracao,
            lead.lead_data_cadastro,
            lead.lead_data_atribuicao,
            lead.lead_status,
            pes_nome, pes_cpf,
            endereco.end_id, tipo_endereco, end_pais, end_estado, end_tipo_end, end_bairro,
            end_cidade, end_cep, end_logradouro, end_tipo_logradouro, end_numero, end_complemento,
            pessoa.con_contato_email,
            pessoa.con_contato_telefone,
            pessoa.con_contato_celular,
            fonte_nome,
            mediador_nome,
            campanha_nome,
            login,
            time_nome,
            concat(login, ' / ', time_nome) AS operador_nome,
            atividade.atividade_tipo_id,
            atividade.atividade_tipo_nome,
            atividade.atividade_data_cadastro,
            atividade.atividade_agendamento
        FROM crm__lead AS lead
        LEFT JOIN pessoa ON pessoa.pes_id=lead.pes_id
        LEFT JOIN pessoa_fisica ON pessoa_fisica.pes_id=pessoa.pes_id
        LEFT JOIN endereco ON endereco.pes_id=pessoa.pes_id
        LEFT JOIN crm__fonte ON crm__fonte.fonte_id=lead.fonte_id
        LEFT JOIN crm__mediador ON crm__mediador.mediador_id=lead.mediador_id
        LEFT JOIN crm__campanha ON crm__campanha.campanha_id=lead.campanha_id
        LEFT JOIN crm__operador ON crm__operador.operador_id=lead.operador_id
        LEFT JOIN acesso_pessoas ON acesso_pessoas.id=crm__operador.operador_id
        LEFT JOIN crm__time ON crm__time.time_id=crm__operador.time_id
        LEFT JOIN (
            SELECT * FROM (
                SELECT
                    crm__lead_atividade.*,
                    atividade_tipo_nome,
                    @c := @c + 1 AS contador
                FROM crm__lead_atividade
                    INNER JOIN view__crm_atividade_tipo
                    ON view__crm_atividade_tipo.atividade_tipo_id = crm__lead_atividade.atividade_tipo_id
                    JOIN (SELECT @c := 0) r
                ORDER BY atividade_data_cadastro DESC
            ) AS ultima_atividade GROUP BY pes_id
        ) AS atividade  ON atividade.pes_id=lead.pes_id
        WHERE 1 " . $where;

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function retornaFiltrosLeads($filtro)
    {
        $where = null;

        if ($filtro['operador']) {
            $where .= "AND lead.operador_id in ({$filtro['operador']})";
        }

        if ($filtro['mediador']) {
            $where .= "AND lead.mediador_id in ({$filtro['mediador']})";
        }

        if ($filtro['campanha']) {
            $where .= "AND lead.campanha_id in ({$filtro['campanha']})";
        }

        if ($filtro['fonte']) {
            $where .= "AND lead.fonte_id in ({$filtro['fonte']})";
        }

        if ($filtro['ultimaAtividade']) {
            $where .= "AND atividade.atividade_tipo_id in ({$filtro['ultimaAtividade']})";
        }

        if ($filtro['leadStatus']) {
            $turno = $filtro['leadStatus'];
            $turno = is_array($turno) ? implode(',', $turno) : $turno;
            $where .= "AND FIND_IN_SET(lead.lead_status, '{$turno}')";
        }

        if ($filtro['dataCadastroInicial']) {
            $dataCadastroInicial = self::formatDateAmericano($filtro['dataCadastroInicial']);
            $where .= " AND date(lead.lead_data_cadastro) >= " . "'" . $dataCadastroInicial . "'";
        }

        if ($filtro['dataCadastroFinal']) {
            $dataCadastroFinal = self::formatDateAmericano($filtro['dataCadastroFinal']);
            $where .= " AND date(lead.lead_data_cadastro) <= " . "'" . $dataCadastroFinal . "'";
        }

        if ($filtro['agendamentoInicial']) {
            $agendamentoInicial = self::formatDateAmericano($filtro['agendamentoInicial']);
            $where .= " AND date(atividade.atividade_agendamento) >= " . "'" . $agendamentoInicial . "'";
        }

        if ($filtro['agendamentoFinal']) {
            $agendamentoFinal = self::formatDateAmericano($filtro['agendamentoFinal']);
            $where .= " AND date(atividade.atividade_agendamento) <= " . "'" . $agendamentoFinal . "'";
        }

        if ($filtro['dataAtividadeInicial']) {
            $dataAtividadeInicial = self::formatDateAmericano($filtro['dataAtividadeInicial']);
            $where .= " AND date(atividade_data_cadastro) >= " . "'" . $dataAtividadeInicial . "'";
        }

        if ($filtro['dataAtividadeFinal']) {
            $dataAtividadeFinal = self::formatDateAmericano($filtro['dataAtividadeFinal']);
            $where .= " AND date(atividade_data_cadastro) <= " . "'" . $dataAtividadeFinal . "'";
        }

        if ($filtro['dataAtribuicaoInicial']) {
            $dataAtribuicaoInicial = self::formatDateAmericano($filtro['dataAtribuicaoInicial']);
            $where .= " AND date(lead_data_atribuicao) >= " . "'" . $dataAtribuicaoInicial . "'";
        }

        if ($filtro['dataAtribuicaoFinal']) {
            $dataAtribuicaoFinal = self::formatDateAmericano($filtro['dataAtribuicaoFinal']);
            $where .= " AND date(lead_data_atribuicao) <= " . "'" . $dataAtribuicaoFinal . "'";
        }

        if ($filtro['atividadeTipo']) {
            $where .= "AND atividade.atividade_tipo_id in ({$filtro['atividadeTipo']})";
        }

        return $where;
    }

    public function retornaLeadsFiltrados($filtro)
    {
        $where = $this->retornaFiltrosLeads($filtro);

        $query = "
        SELECT lead.pes_id AS pesId
        FROM crm__lead lead
        LEFT JOIN (
            SELECT * FROM (
                SELECT
                    crm__lead_atividade.*,
                    atividade_tipo_nome,
                    @c := @c + 1 AS contador
                FROM crm__lead_atividade
                    INNER JOIN view__crm_atividade_tipo
                    ON view__crm_atividade_tipo.atividade_tipo_id = crm__lead_atividade.atividade_tipo_id
                    JOIN (SELECT @c := 0) r
                ORDER BY atividade_data_cadastro DESC
            ) AS ultima_atividade GROUP BY pes_id
        ) AS atividade  ON atividade.pes_id=lead.pes_id
        WHERE lead_distribuindo<>'Sim' " . $where;

        $arrLeads = $this->executeQueryWithParam($query)->fetchAll();

        return $arrLeads;
    }

    public function colocarLeadEmProgresso($pesId)
    {
        if (!$pesId) {
            $this->setLastError('Lead não encontrado!');

            return false;
        }

        /** @var $objCrmLead \Crm\Entity\CrmLead */
        $objCrmLead = $this->getRepository()->find($pesId);

        if (!$objCrmLead) {
            $this->setLastError('Lead não encontrado!');

            return array();
        }

        if ($objCrmLead->getLeadStatus() == self::LEAD_STATUS_ATRIBUIDO) {
            try {
                $this->getEm()->beginTransaction();
                $objCrmLead->setLeadStatus(self::LEAD_STATUS_EM_PROCESSO);
                $this->getEm()->persist($objCrmLead);
                $this->getEm()->flush($objCrmLead);
                $this->getEm()->commit();
            } catch (\Exception $e) {
                $this->setLastError('Falha ao alterar situação do lead.' . $e->getMessage());
            }
        }

        return false;
    }

    public function getArray($pesId)
    {
        if (!$pesId) {
            $this->setLastError('Lead não encontrada!');

            return array();
        }

        /** @var $objCrmLead \Crm\Entity\CrmLead */
        $objCrmLead = $this->getRepository()->find($pesId);

        if (!$objCrmLead) {
            $this->setLastError('Lead não encontrada!');

            return array();
        }

        $servicePessoa           = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceAcadgeralAluno   = new \Matricula\Service\AcadgeralAluno($this->getEm());
        $serviceCrmLeadInteresse = new \Crm\Service\CrmLeadInteresse($this->getEm());

        try {
            $arrDados  = $objCrmLead->toArray();
            $arrPessoa = $servicePessoa->getArray($pesId);
            $arrDados  = array_merge($arrDados, $arrPessoa);

            $objAcadgeralAluno   = $serviceAcadgeralAluno->retornaAlunoPeloPesId($arrDados['pesId']);
            $arrDados['alunoId'] = $objAcadgeralAluno ? $objAcadgeralAluno->getAlunoId() : null;

            $arrDados['interesse'] = $serviceCrmLeadInteresse->retornaArrayPeloLead($arrDados['pesId']);
        } catch (\Exception $e) {
            $this->setLastError('Falha ao buscar dados de lead.' . $e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $pesId
     * @return array
     */
    public function getArrayFallback($pesId)
    {
        $arrDados = $this->getArray($pesId);

        if (!$arrDados) {
            $servicePessoaFisica = new \Pessoa\Service\PessoaFisica($this->getEm());
            $objCrmLead          = new \Crm\Entity\CrmLead();

            $arrDados = $servicePessoaFisica->getArray($pesId);
            $arrDados = array_merge($objCrmLead->toArray(), $arrDados);
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $servicePessoa      = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceCrmFonte    = new \Crm\Service\CrmFonte($this->getEm());
        $serviceCrmMediador = new \Crm\Service\CrmMediador($this->getEm());
        $serviceCrmCampanha = new \Crm\Service\CrmCampanha($this->getEm());
        $serviceCrmOperador = new \Crm\Service\CrmOperador($this->getEm());

        if (is_array($arrDados['pes']) && !$arrDados['pes']['text']) {
            $arrDados['pes'] = $arrDados['pes']['pesId'];
        }

        if ($arrDados['pes'] && is_numeric($arrDados['pes'])) {
            $arrDados['pes'] = $servicePessoa->getArrSelect2(array('id' => $arrDados['pes']));
            $arrDados['pes'] = $arrDados['pes'] ? $arrDados['pes'][0] : null;
        }

        if (is_array($arrDados['fonte']) && !$arrDados['fonte']['text']) {
            $arrDados['fonte'] = $arrDados['fonte']['fonteId'];
        }

        if ($arrDados['fonte'] && is_numeric($arrDados['fonte'])) {
            $arrDados['fonte'] = $serviceCrmFonte->getArrSelect2(array('id' => $arrDados['fonte']));
            $arrDados['fonte'] = $arrDados['fonte'] ? $arrDados['fonte'][0] : null;
        }

        if (is_array($arrDados['mediador']) && !$arrDados['mediador']['text']) {
            $arrDados['mediador'] = $arrDados['mediador']['mediadorId'];
        }

        if ($arrDados['mediador'] && is_numeric($arrDados['mediador'])) {
            $arrDados['mediador'] = $serviceCrmMediador->getArrSelect2(array('id' => $arrDados['mediador']));
            $arrDados['mediador'] = $arrDados['mediador'] ? $arrDados['mediador'][0] : null;
        }

        if (is_array($arrDados['campanha']) && !$arrDados['campanha']['text']) {
            $arrDados['campanha'] = $arrDados['campanha']['campanhaId'];
        }

        if ($arrDados['campanha'] && is_numeric($arrDados['campanha'])) {
            $arrDados['campanha'] = $serviceCrmCampanha->getArrSelect2(array('id' => $arrDados['campanha']));
            $arrDados['campanha'] = $arrDados['campanha'] ? $arrDados['campanha'][0] : null;
        }

        if (is_array($arrDados['operador']) && !$arrDados['operador']['text']) {
            $arrDados['operador'] = $arrDados['operador']['operadorId'];
        }

        if ($arrDados['operador'] && is_numeric($arrDados['operador'])) {
            $arrDados['operador'] = $serviceCrmOperador->getArrSelect2(array('id' => $arrDados['operador']));
            $arrDados['operador'] = $arrDados['operador'] ? $arrDados['operador'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['pes'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam);

        $arrEntitiesArr = array();
        /* @var $objEntity \Crm\Entity\CrmLead */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getPes()->getPes()->getPesId();
            $arrEntity[$params['value']] = $objEntity->getPes()->getPes()->getPesNome();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['pesId']) {
            $this->setLastError('Para remover um registro de lead é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objCrmLead \Crm\Entity\CrmLead */
            $objCrmLead = $this->getRepository()->find($param['pesId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objCrmLead);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de lead.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $servicePessoa           = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceCrmFonte         = new \Crm\Service\CrmFonte($this->getEm());
        $serviceCrmMediador      = new \Crm\Service\CrmMediador($this->getEm());
        $serviceCrmCampanha      = new \Crm\Service\CrmCampanha($this->getEm());
        $serviceCrmOperador      = new \Crm\Service\CrmOperador($this->getEm());
        $serviceCrmLeadAtividade = new \Crm\Service\CrmLeadAtividade($this->getEm());

        $servicePessoa->setarDependenciasView($view);
        $serviceCrmFonte->setarDependenciasView($view);
        $serviceCrmMediador->setarDependenciasView($view);
        $serviceCrmCampanha->setarDependenciasView($view);
        $serviceCrmOperador->setarDependenciasView($view);
        $serviceCrmLeadAtividade->setarDependenciasView($view);

        $view->setVariable("arrLeadStatus", $this->getArrSelect2LeadStatus());
    }

    public function importarLeadsEmail($chamadaDeRobo = false)
    {
        $emails = [
            'emails' => 0,
            'leads'  => 0,
            'errors' => []
        ];

        try {
            @ini_set('memory_limit', '-1');
            $waitTimeout        = $this->executeQuery('SHOW SESSION VARIABLES LIKE "wait_timeout"')->fetch();
            $interactiveTimeout = $this->executeQuery('SHOW SESSION VARIABLES LIKE "interactive_timeout"')->fetch();
            $waitTimeout        = $waitTimeout['Value'];
            $interactiveTimeout = $interactiveTimeout['Value'];

            $this->executeQuery('SET @@session.wait_timeout=' . (60 * 600));
            $this->executeQuery('SET @@session.interactive_timeout=' . (60 * 600));

            $serviceSisRobo = new \Sistema\Service\SisRobo($this->getEm());

            $paramRobo = [
                'roboUrl' => '/crm/crm-lead/captura-leads-email',
            ];

            if (!$chamadaDeRobo) {
                //$paramRobo['roboSituacao'] = $serviceSisRobo::ROBO_SITUACAO_ATIVO;
            }

            /** @var \Sistema\Entity\SisRobo $objSisRobo */
            $objSisRobo = $serviceSisRobo->getRepository()->findOneBy($paramRobo);

            if (!$objSisRobo) {
                $this->setLastError('Robô de capturas está processando ou não está registrado!');

                return $emails;
            }

            $objSisRobo->setRoboSituacao($serviceSisRobo::ROBO_SITUACAO_PROCESSANDO);

            $this->getEm()->persist($objSisRobo);
            $this->getEm()->flush($objSisRobo);

            $pastaEntradaLead     = $this->getConfig()->localizarChave('EMAIL_ENTRADA_LEAD');
            $pastaArquivoLead     = $this->getConfig()->localizarChave('EMAIL_ARQUIVO_LEAD');
            $pastaArquivoErroLead = $this->getConfig()->localizarChave('EMAIL_ARQUIVO_ERRO_LEAD');

            $pastaArquivoLeadTratada     = '"' . trim($pastaArquivoLead, '"') . '"';
            $pastaArquivoErroLeadTratada = '"' . trim($pastaArquivoErroLead, '"') . '"';

            $serviceCrmMediador = new \Crm\Service\CrmMediador($this->getEm());
            $orgEmailConta      = new \Organizacao\Service\OrgEmailConta($this->getEm());

            $arrOrgEmailConta               = $orgEmailConta->retornaContasAtivas(true, true);
            $arrEmailsRemetenteLeadMediador = $serviceCrmMediador->retornaMediadoresComEmailCadastrado();

            $arrEmailsMediador = [];

            foreach (array_keys($arrEmailsRemetenteLeadMediador) as $email) {
                $arrEmailsMediador[] = ['UNSEEN', 'FROM "' . trim($email) . '"'];
            }

            /** @var \Organizacao\Entity\OrgEmailConta $objOrgEmailConta */
            foreach ($arrOrgEmailConta as $objOrgEmailConta) {
                try {
                    $objImap = new \Eden\Mail\Imap(
                        $objOrgEmailConta->getContaImapHost(),
                        $objOrgEmailConta->getContaUsuario(),
                        $objOrgEmailConta->getContaSenha(),
                        $objOrgEmailConta->getContaImapPorta(),
                        in_array($objOrgEmailConta->getContaImapSsl(), ['ssl', 'tls']),
                        $objOrgEmailConta->getContaImapSsl() == 'tls'
                    );

                    $arrCaixasDeEmail = $objImap->getMailboxes();

                    if ($pastaEntradaLead && in_array($pastaEntradaLead, $arrCaixasDeEmail)) {
                        $objImap->setActiveMailbox($pastaEntradaLead);
                    } else {
                        $objImap->setActiveMailbox('INBOX');
                    }

                    $arrEmails = [];

                    foreach ($arrEmailsMediador as $condicoes) {
                        $arrEmailsMediador = $objImap->search($condicoes, 0, 20);

                        if ($arrEmailsMediador['id']) {
                            $arrEmailsMediador = [$arrEmailsMediador];
                        }

                        $arrEmails = array_merge($arrEmails, $arrEmailsMediador);
                    }

                    foreach ($arrEmails as $emailData) {
                        $email = $objImap->getUniqueEmails($emailData['uid'], true);

                        if (!$mediadorId = $arrEmailsRemetenteLeadMediador[$emailData['from']['email']]) {
                            continue;
                        }

                        $htmlLead = $email['body']['text/html'];
                        $htmlLead = $htmlLead ? $email['body']['text/html'] : $email['body']['text/plain'];

                        $arrDadosLead = $this->tratarLeadHtml($htmlLead);

                        $arrDadosLead['fonte']      = 'E-mail';
                        $arrDadosLead['mediador']   = $mediadorId;
                        $arrDadosLead['importacao'] = true;

                        $emails['emails'] += 1;

                        if ($this->save($arrDadosLead)) {
                            $emails['leads'] += 1;

                            if ($pastaArquivoLead && in_array($pastaArquivoLead, $arrCaixasDeEmail)) {
                                //$objImap->move($emailData['uid'], $pastaArquivoLeadTratada);
                                $objImap->move($emailData['uid'], $pastaArquivoLead);
                            }
                        } else {
                            if ($pastaArquivoErroLead && in_array($pastaArquivoErroLead, $arrCaixasDeEmail)) {
                                //$objImap->move($emailData['uid'], $pastaArquivoErroLead);
                                $objImap->move($emailData['uid'], $pastaArquivoErroLead);
                            }
                        }
                    }

                    $objImap->disconnect();
                } catch (\Exception $ex) {
                    $emails['errors'][] = [
                        $objOrgEmailConta->getContaUsuario(),
                        $ex->getFile(),
                        $ex->getLine(),
                        $ex->getMessage()
                    ];
                }
            }

            $objSisRobo->setRoboSituacao($serviceSisRobo::ROBO_SITUACAO_ATIVO);

            $this->getEm()->persist($objSisRobo);
            $this->getEm()->flush($objSisRobo);
            $this->executeQuery('SET @@session.wait_timeout=' . $waitTimeout);
            $this->executeQuery('SET @@session.interactive_timeout=' . $interactiveTimeout);
        } catch (\Exception $ex) {
            return ['erro' => $ex->getTraceAsString()];
        }

        return $emails;
    }

    private function tratarLeadHtml($html)
    {
        $attrLead = [];

        $html = strip_tags(str_replace('><', ">\n<", $html));
        $html = preg_replace('/\n+/', "\n", $html);
        $html = \ForceUTF8\Encoding::fixUTF8($html);
        $html = preg_replace('/^[\t ]*/m', "", $html);

        $leadDoInfoJobs = substr_count($html, 'Infojobs') > 0 ? true : false;

        $arrStrSeparadorCabecalho = [
            'Um usuário solicitou informações sobre o seguinte curso:',
            'recebeu um Lead para o curso:',
            'Um novo lead foi gerado pela Leadsolution!',
            'Novo Lead da Vendedoor! Para melhor desempenho, acesse diretamente no sistema.',
            'Formulario gerado via website',
        ];

        $arrStrSeparadorRodape = [
            'Recomendações da Educaedu Brasil para otimizar a gestão das solicitações de informação:',
            'Contestar',
            'Lead enviado por Weach',
            'Ver dados de contato',
            'Lead enviado por Vendedoor',
        ];

        foreach ($arrStrSeparadorRodape as $strSeparadorRodape) {
            while (($posicao = strpos($html, $strSeparadorRodape)) !== false) {
                $html = substr($html, 0, $posicao);
            }
        }

        foreach ($arrStrSeparadorCabecalho as $strSeparadorCabecalho) {
            while (($posicao = strpos($html, $strSeparadorCabecalho)) != false) {
                $html = substr($html, $posicao + strlen($strSeparadorCabecalho));
            }
        }

        $html = preg_replace('/\s*\n/', "\n", $html);
        $html = preg_replace('/^ +(.*)/', "$1", $html);

        if ($leadDoInfoJobs) {
            $dados = explode("\n", $html);

            $attrLead['interesse']         = trim($dados[0]);
            $attrLead['pesNome']           = trim($dados[1]);
            $attrLead['conContatoCelular'] = trim($dados[2]);
            $attrLead['conContatoEmail']   = trim($dados[3]);
            $attrLead['endEstado']         = trim($dados[4]);
        }

        $arrAtributosLead = [
            ['html' => 'Pessoa de Contato', 'attr' => 'pesNome'],
            ['html' => 'Nome da Pessoa', 'attr' => 'pesNome'],
            ['html' => 'Nome da Empresa', 'attr' => 'pesNome'],
            ['html' => 'Nome Completo', 'attr' => 'pesNome'],
            ['html' => 'Nome do... indicado...', 'attr' => 'pesNome'],
            ['html' => 'Nome', 'attr' => 'pesNome'],
            ['html' => 'Sobrenome', 'attr' => 'pesNome'],
            ['html' => 'Apelido', 'attr' => 'pesNome'],
            ['html' => 'Movil', 'attr' => 'conContatoCelular'],
            ['html' => 'Celular', 'attr' => 'conContatoCelular'],
            ['html' => 'DDD.Celular', 'attr' => 'celularDDD'],
            ['html' => 'Nível de escolaridade', 'attr' => 'nivelEscolar'],
            ['html' => 'Graduacao.Completa', 'attr' => 'graduacaoCompleta'],
            ['html' => 'Graduacao', 'attr' => 'graduacaoCompleta'],
            ['html' => 'Possui Gradua..o', 'attr' => 'graduacaoCompleta'],
            ['html' => 'Possui Forma..o Superior', 'attr' => 'graduacaoCompleta'],
            ['html' => 'Curso de interesse', 'attr' => 'interesse'],
            ['html' => '.rea de interesse', 'attr' => 'interesse'],
            ['html' => '[^\s]+rea de interesse', 'attr' => 'interesse'],
            ['html' => 'Nome do curso', 'attr' => 'interesse'],
            ['html' => 'Nome do mestrado', 'attr' => 'interesse'],
            ['html' => 'Nome do curso/mestrado', 'attr' => 'interesse'],
            ['html' => 'Nome do curso.mestrado', 'attr' => 'interesse'],
            ['html' => 'Produto', 'attr' => 'interesse'],
            ['html' => 'Nome do curso', 'attr' => 'interesse'],
            ['html' => 'Curso', 'attr' => 'interesse'],
            ['html' => 'Interesse', 'attr' => 'interesse'],
            ['html' => 'Informa..es.adicionais', 'attr' => 'leadObs'],
            ['html' => 'Data', 'attr' => 'dataLead'],
            ['html' => 'Criado em', 'attr' => 'dataLead'],
            ['html' => 'IP', 'attr' => 'ipLead'],
            ['html' => 'Origem', 'attr' => 'origemLead'],
            ['html' => 'Concelho', 'attr' => 'endCidade'],
            ['html' => 'Distrito', 'attr' => 'endEstado'],
            ['html' => 'Cidade', 'attr' => 'endCidade'],
            ['html' => 'Estado', 'attr' => 'endEstado'],
            ['html' => 'Telefone Celular', 'attr' => 'conContatoCelular'],
            ['html' => 'Telefone do... indicado...', 'attr' => 'conContatoCelular'],
            ['html' => 'Telefone de Contato', 'attr' => 'conContatoTelefone'],
            ['html' => 'Telefone Fixo', 'attr' => 'conContatoTelefone'],
            ['html' => 'Telefone', 'attr' => 'conContatoTelefone'],
            ['html' => 'Fone', 'attr' => 'conContatoTelefone'],
            ['html' => 'Fixo', 'attr' => 'conContatoTelefone'],
            ['html' => 'Email', 'attr' => 'conContatoEmail'],
            ['html' => 'Email lead', 'attr' => 'conContatoEmail'],
            ['html' => 'E-mail', 'attr' => 'conContatoEmail'],
            ['html' => 'Meu nome', 'attr' => 'pesNomeIndicacao'],
            ['html' => 'Meu telefone', 'attr' => 'conContatoTelefoneIndicacao'],
        ];

        foreach ($arrAtributosLead as $search) {
            $strAtributosLead = "/^(?:" . $search['html'] . "(:|\s*\n+))(\s*.*)$/im";

            if (preg_match($strAtributosLead, $html, $arrMatch)) {
                if ('interesse' == $search['attr']) {
                    $attr  = ($attrLead[$search['attr']] ? $attrLead[$search['attr']] : array());
                    $attr2 = trim($arrMatch[count($arrMatch) - 1]);
                    $attr2 = preg_replace($strAtributosLead, "$2", $attr2);

                    if ($attr2) {
                        $attr[]                    = $attr2;
                        $attrLead[$search['attr']] = array_unique($attr);
                    }
                } else {
                    $attr  = ($attrLead[$search['attr']] ? $attrLead[$search['attr']] : "");
                    $attr2 = trim($arrMatch[count($arrMatch) - 1]);
                    $attr2 = preg_replace($strAtributosLead, "$2", $attr2);

                    $attr = trim($attr . " " . $attr2);

                    $attrLead[$search['attr']] = $attr;
                }
            }
        }

        if ($attrLead['celularDDD']) {
            $attrLead['conContatoCelular'] = '(' . $attrLead['celularDDD'] . ') ' . $attrLead['conContatoCelular'];
            unset($attrLead['celularDDD']);
        }

        $conContatoCelular  = $this->formatarTelefone($attrLead['conContatoCelular']);
        $conContatoTelefone = $this->formatarTelefone($attrLead['conContatoTelefone']);

        $attrLead['conContatoCelular']  = $conContatoCelular;
        $attrLead['conContatoTelefone'] = $conContatoTelefone;

        $attrLead['leadObs'] = $attrLead['leadObs'] ? $attrLead['leadObs'] : '';

        if ($attrLead['graduacaoCompleta']) {
            $attrLead['leadObs'] .= "\nGraduação Completa: " . $attrLead['graduacaoCompleta'];
            unset($attrLead['graduacaoCompleta']);
        }

        if ($attrLead['ipLead']) {
            $attrLead['leadObs'] .= "\nIP: " . $attrLead['ipLead'];
            unset($attrLead['ipLead']);
        }

        if ($attrLead['origemLead']) {
            $attrLead['leadObs'] .= "\nOrigem: " . $attrLead['origemLead'];
            unset($attrLead['origemLead']);
        }

        if ($attrLead['nivelEscolar']) {
            $attrLead['leadObs'] .= "\nNível Escolar: " . $attrLead['nivelEscolar'];
            unset($attrLead['nivelEscolar']);
        }

        if ($attrLead['dataLead']) {
            $attrLead['leadObs'] .= "\nData de recebmento do Lead: " . $attrLead['dataLead'];
            unset($attrLead['dataLead']);
        }

        if ($attrLead['pesNomeIndicacao']) {
            $attrLead['leadObs'] .= "\nPessoa que indicou: " . $attrLead['pesNomeIndicacao'];
            unset($attrLead['pesNomeIndicacao']);

            if ($attrLead['conContatoTelefoneIndicacao']) {
                $attrLead['leadObs'] .= "(" . $attrLead['conContatoTelefoneIndicacao'].")";
                unset($attrLead['conContatoTelefoneIndicacao']);
            }
        }


        $attrLead['leadObs'] = trim($attrLead['leadObs']);

        return $attrLead;
    }

    public function importarLeadsArquivoCSV($arquivo)
    {
        $arrProcessamento = [
            'contatos' => 0,
            'leads'    => 0
        ];

        $contents        = [];
        $outlook         = false;
        $csvSimplificado = false;
        $facebook        = false;
        $fonte           = false;

        if (is_file($arquivo)) {
            $content = file_get_contents($arquivo);

            if (substr_count($content, ";adset_id;") > 0) {
                $facebook = true;
                $content = utf8_encode($content);
                $content  = str_replace(";", ',', $content);
                file_put_contents($arquivo, $content);
            } elseif (preg_match('/((ID|EMAIL|NOME|TELEFONE|ÁREA);)+/i', $content) > 0) {
                $csvSimplificado = true;
                $content         = utf8_encode($content);
                $content         = str_replace(";", ',', $content);
                $content         = str_replace(
                    array('ID', 'EMAIL', 'NOME', 'TELEFONE', 'ÁREA', 'área'),
                    array('id', 'email', 'nome', 'telefone', 'area', 'area'),
                    $content
                );

                file_put_contents($arquivo, $content);
            } elseif (preg_match('/((ID|EMAIL|NOME|TELEFONE|ÁREA),)+/i', $content) > 0) {
                $csvSimplificado = true;
                $content         = utf8_encode($content);
                $content         = str_replace(
                    array('ID', 'EMAIL', 'NOME', 'TELEFONE', 'ÁREA', 'área'),
                    array('id', 'email', 'nome', 'telefone', 'area', 'area'),
                    $content
                );

                file_put_contents($arquivo, $content);
            } elseif (substr_count($content, ',Company,') > 0) {
                $outlook = true;
                $content = utf8_encode($content);
                file_put_contents($arquivo, $content);
            } else {
                $content = mb_convert_encoding($content, 'UTF-8', 'UTF-16');

                if (substr_count($content, "\tadset_id\t") > 0) {
                    $facebook = true;
                    $content  = str_replace(",", ' ', $content);
                    $content  = str_replace("\t", ',', $content);
                    //$content  = preg_replace("/(\r|)\n/", "\"\r\n\"", $content);
                    //$content  = '"' . trim($content) . '"';
                    file_put_contents($arquivo, $content);
                } else {
                    return $arrProcessamento;
                }
            }

            $objFile = new \SplFileObject($arquivo);

            $objFile->setFlags(
                \SplFileObject::READ_CSV | \SplFileObject::READ_AHEAD | \SplFileObject::SKIP_EMPTY | \SplFileObject::DROP_NEW_LINE
            );

            $objFile->setCsvControl(',', '""', '\\');

            $contents = [];
            $header   = $objFile->current();

            foreach ($objFile as $row) {
                if ($row != $header && count($row) >= count($header)) {
                    $contents[] = array_combine($header, array_slice($row, 0, count($header)));
                }
            }
        }

        if ($csvSimplificado) {
            $fonte    = 'Arquivo CSV Simplificado';
            $contents = $this->processarArrCsvSimplificado($contents);
        } elseif ($outlook) {
            $fonte    = 'Arquivo CSV';
            $contents = $this->processarArrCsvOutlook($contents);
        } elseif ($facebook) {
            $fonte    = 'Arquivo CSV Facebook';
            $contents = $this->processarArrCsvFacebook($contents);
        }

        foreach ($contents as $arrLead) {
            $arrLead['fonte']      = $fonte;
            $arrLead['importacao'] = true;

            $arrLead['leadObs'] = $arrLead['leadObs'] ? $arrLead['leadObs'] : '';

            if ($arrLead['nivelDeEscolaridade']) {
                $arrLead['leadObs'] .= "\nNível de Escolaridade: " . $arrLead['nivel_de_escolaridade'];
            }

            if ($arrLead['graduacaoCompleta']) {
                $arrLead['leadObs'] .= "\nGraduação Completa: " . $arrLead['graduacaoCompleta'];
            }

            if ($arrLead['dataLead']) {
                $arrLead['leadObs'] .= "\nData de recebmento do Lead: " . $arrLead['dataLead'];
            }

            $arrLead['leadObs'] = trim($arrLead['leadObs']);

            $arrProcessamento['contatos'] += 1;

            if ($this->save($arrLead)) {
                $arrProcessamento['leads'] += 1;
            }
        }

        return $arrProcessamento;
    }

    private function setarLeadsComoEmDistribuicao($arrLeads, $emDistribuicao = 'Sim')
    {
        if (!$arrLeads) {
            return false;
        }

        $arrPesId = [];

        foreach ($arrLeads as $arrLead) {
            $arrPesId[] = $arrLead['pesId'];
        }

        $query = "
        UPDATE crm__lead lead
        SET lead_distribuindo='" . $emDistribuicao . "'
        WHERE pes_id IN(" . implode(',', $arrPesId) . ")";

        return $this->executeQuery($query);
    }

    public function distribuirLeads($arrFiltrosLeads, $chamadaDeRobo = false)
    {
        $serviceCrmOperador = new \Crm\Service\CrmOperador($this->getEm());
        $serviceCrmOperador->setConfig($this->getConfig());

        try {
            $serviceSisRobo = new \Sistema\Service\SisRobo($this->getEm());

            $paramRobo = [
                'roboUrl' => '/crm/crm-lead/distribuir-novos-leads',
            ];

            if (!$chamadaDeRobo) {
                $paramRobo['roboSituacao'] = $serviceSisRobo::ROBO_SITUACAO_ATIVO;
            }

            /** @var \Sistema\Entity\SisRobo $objSisRobo */
            $objSisRobo = $serviceSisRobo->getRepository()->findOneBy($paramRobo);

            if (!$objSisRobo) {
                $this->setLastError('Robô de distribuição está processando ou não está registrado!');

                return false;
            }

            $objSisRobo->setRoboSituacao($serviceSisRobo::ROBO_SITUACAO_PROCESSANDO);

            $this->getEm()->persist($objSisRobo);
            $this->getEm()->flush($objSisRobo);

            $this->getEm()->beginTransaction();

            $arrLeads      = $this->retornaLeadsFiltrados($arrFiltrosLeads);
            $arrOperadores = $serviceCrmOperador->retornaOperadoresParaDistribuicaoLead(count($arrLeads));
            $this->setarLeadsComoEmDistribuicao($arrLeads);

            if ($arrOperadores) {
                $ultimoOperador = -1;
                $operadorAtual  = -1;

                $proximoOperador = function () use (&$arrOperadores, &$operadorAtual, &$ultimoOperador) {
                    $operadorAtual++;

                    if ($operadorAtual > count($arrOperadores) - 1) {
                        $operadorAtual = 0;
                    }

                    $arrOperador = $arrOperadores[$operadorAtual];

                    if ($arrOperador['operadorId'] == $ultimoOperador && count($arrOperadores) > 1) {
                        return -1;
                    }

                    if ($arrOperador['totalLeadsOperador'] <= 0) {
                        return 0;
                    }

                    $arrOperadores[$operadorAtual]['totalLeadsOperador'] -= 1;

                    return $arrOperador;
                };

                $sair = false;

                foreach ($arrLeads as $lead) {
                    $procurarOperador = true;
                    $arrOperador      = array();

                    do {
                        $arrOperador = $proximoOperador();

                        if ($arrOperador>0) {
                            $procurarOperador = false;
                        } elseif ($arrOperador == -1) {
                            $sair             = true;
                            $procurarOperador = false;
                        }
                    } while ($procurarOperador);

                    if ($sair || $arrOperador == false) {
                        break;
                    }

                    /** @var \Crm\Entity\CrmOperador $objOperador */
                    $objOperador = $serviceCrmOperador->getRepository()->find($arrOperador['operadorId']);

                    /** @var \Crm\Entity\CrmLead $objLead */
                    $objLead = $this->getRepository()->find($lead['pesId']);

                    $objOperador->setOperadorDataUltimoRecebimento(new \Datetime());

                    $this->getEm()->persist($objOperador);
                    $this->getEm()->flush($objOperador);

                    $objLead
                        ->setLeadStatus(self::LEAD_STATUS_ATRIBUIDO)
                        ->setLeadDistribuindo(self::LEAD_DISTRIBUINDO_NAO)
                        ->setOperador($objOperador)
                        ->setLeadDataAtribuicao(new \Datetime())
                        ->setLeadDataAlteracao(new \Datetime());

                    $this->getEm()->persist($objLead);
                    $this->getEm()->flush($objLead);

                    $ultimoOperador = $arrOperador['operadorId'];
                }
            } else {
                $this->setarLeadsComoEmDistribuicao($arrLeads, 'Nao');
            }

            $this->getEm()->commit();

            $objSisRobo->setRoboSituacao($serviceSisRobo::ROBO_SITUACAO_ATIVO);

            $this->getEm()->persist($objSisRobo);
            $this->getEm()->flush($objSisRobo);

            return array(
                'leads'      => count($arrLeads),
                'operadores' => count($arrOperadores),
            );
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao distribuir registros de lead.' . $ex->getMessage());

            return false;
        }

        return true;
    }

    public function converterLead($pesId)
    {
        if (!$pesId) {
            $this->setLastError('Para converter um registro de lead é necessário informar o código.');

            return false;
        }

        $serviceAcadGeralAluno = new \Matricula\Service\AcadgeralAluno($this->getEm());

        try {
            /** @var $objCrmLead \Crm\Entity\CrmLead */
            $objCrmLead = $this->getRepository()->find($pesId);

            $this->getEm()->beginTransaction();

            $objCrmLead->setLeadStatus(self::LEAD_STATUS_CONVERTIDO);

            $arrDados = $this->getArray($pesId);

            $arrDados['alunoMediador'] = 'conversao-lead';
            $arrDados['alunoMae']      = '';
            $arrDados['alunoEtnia']    = $serviceAcadGeralAluno::ALUNO_ETNIA_NAO_DECLARADO;

            if (!$serviceAcadGeralAluno->salvarAluno($arrDados)) {
                $this->setLastError($serviceAcadGeralAluno->getLastError());

                return null;
            }

            $this->getEm()->persist($objCrmLead);
            $this->getEm()->flush($objCrmLead);
            $this->getEm()->commit();

            return $arrDados;
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao converter registro de lead.');
        }

        return null;
    }

    public function processarArrCsvFacebook($arrLinhas)
    {
        $arrLinhasProcessadas = array();

        foreach ($arrLinhas as $arrLinha) {
            $arrNovaLinha = [];

            foreach ($arrLinha as $chave => $valor) {
                $valor = trim(str_replace('_', ' ', $valor));
                switch ($chave) {
                    case 'city':
                        $arrNovaLinha['endCidade'] = $valor;
                        break;
                    case 'company_name':
                        $arrNovaLinha['pesEmpresa'] = $valor;
                        break;
                    case 'country':
                        $arrNovaLinha['endEstado'] = $valor;
                        break;
                    case 'created_time':
                        $arrNovaLinha['dataLead'] = self::formatDateBrasileiro($valor);
                        break;
                    case 'date_of_birth':
                        $arrNovaLinha['pesDataNascimento'] = self::formatDateBrasileiro($valor);
                        break;
                    case 'email':
                        $arrNovaLinha['conContatoEmail'] = $valor;
                        break;
                    case 'full_name':
                        $arrNovaLinha['pesNome'] = $valor;
                        break;
                    case 'gender':
                        $arrNovaLinha['pesSexo'] = $valor;
                        break;
                    case 'job_title':
                        $arrNovaLinha['pesProfissao'] = $valor;
                        break;
                    case 'marital_status':
                        $arrNovaLinha['pesEstadoCivil'] = $valor;
                        break;
                    case 'phone_number':
                        $arrNovaLinha['conContatoTelefone'] = $this->formatarTelefone($valor);
                        break;
                    case 'course_of_interest':
                        $arrNovaLinha['interesse'] = $arrNovaLinha['interesse'] ? $arrNovaLinha['interesse'] : [];
                        $arrNovaLinha['interesse'] = $valor;
                        break;
                    case 'state':
                        $arrNovaLinha['endUf'] = $valor;
                        break;
                    case 'street_address':
                        $arrNovaLinha['endLogradouro'] = $valor;
                        break;
                    case 'zip_code':
                        $arrNovaLinha['endCep'] = $valor;
                        break;
                    case 'nível_de_escolaridade':
                    case 'nivel_de_escolaridade':
                    case 'escolaridade':
                        $arrNovaLinha['nivelDeEscolaridade'] = $valor;
                        break;
                }
            }

            $arrLinhasProcessadas[] = array_filter($arrNovaLinha);
        }

        return $arrLinhasProcessadas;
    }

    public function processarArrCsvOutlook($arrLinhas)
    {
        $arrLinhasProcessadas = array();

        foreach ($arrLinhas as $arrContato) {
            $arrLead                        = [];
            $arrLead['pesNome']             = $arrContato['First Name'] . ' ' . $arrContato['Middle Name'] . ' ' . $arrContato['Last Name'];
            $arrLead['pesNome']             = trim(preg_replace('/\s+/', ' ', $arrLead['pesNome']));
            $arrLead['pesDataNascimento']   = $arrContato['Birthday'];
            $arrLead['pesCidadeNascimento'] = $arrContato['Location'];

            if (trim($arrContato['Home Address'])) {
                $arrLead['endLogradouro'] = trim($arrContato['Home Address']);
                $arrLead['endCidade']     = trim($arrContato['Home City']);
                $arrLead['endEstado']     = trim($arrContato['Home State']);

                $arrLead['endLogradouro'] = $arrLead['endLogradouro'] ? $arrLead['endLogradouro'] : '-';
                $arrLead['endCidade']     = $arrLead['endCidade'] ? $arrLead['endCidade'] : '-';
                $arrLead['endEstado']     = $arrLead['endEstado'] ? $arrLead['endEstado'] : '-';
            }

            $arrLead['pesProfissao'] = $arrContato['Job Title'];
            $arrLead['pesEmpresa']   = $arrContato['Company'];

            $emails    = [];
            $telefones = [];

            foreach ($arrContato as $key => $value) {
                if (substr_count(strtolower($key), 'mail') > 0 && $value) {
                    $emails[] = $value;
                }

                if (substr_count(strtolower($key), 'phone') > 0 && $value) {
                    $telefones[] = $value;
                }
            }

            asort($telefones);

            $arrLead['conContatoEmail']    = array_shift($emails);
            $arrLead['conContatoTelefone'] = $this->formatarTelefone(array_shift($telefones));
            $arrLead['conContatoCelular']  = $this->formatarTelefone(array_shift($telefones));

            $arrLinhasProcessadas[] = array_filter($arrLead);
        }

        return $arrLinhasProcessadas;
    }

    public function processarArrCsvSimplificado($arrLinhas)
    {
        $arrLinhasProcessadas = array();

        foreach ($arrLinhas as $arrContato) {
            $arrLead                      = [];
            $arrLead['pesNome']           = $arrContato['nome'];
            $arrLead['pesNome']           = trim(preg_replace('/\s+/', ' ', $arrLead['pesNome']));
            $arrLead['conContatoEmail']   = $arrContato['email'];
            $arrLead['conContatoCelular'] = $this->formatarTelefone($arrContato['telefone']);
            $arrLead['interesse']         = $arrContato['area'];

            $arrLinhasProcessadas[] = array_filter($arrLead);
        }

        return $arrLinhasProcessadas;
    }

    public function formatarTelefone($numero)
    {
        $numero = preg_replace('/\+55/', '', $numero);
        $numero = preg_replace('/[^0-9]/', '', $numero);

        //Formatação DDD
        if (strlen($numero) <= 9) {
            $numero = str_pad($numero, ($numero[0] == 9 ? 11 : 10), '0', STR_PAD_LEFT);
        }

        if ($numero != "" && $numero != 0) {
            $size   = strlen($numero) > 11 ? 11 : strlen($numero);
            $numero = substr($numero, (-1) * $size);

            if (strlen($numero) < 8) {
                $numero = "";
            } else {
                $numero = substr(str_pad($numero, 10, '0', STR_PAD_LEFT), 0, 11);
                $numero = preg_replace(
                    '/([0-9]{2})([0-9]{4,5})([0-9]{4})/',
                    '($1) $2-$3',
                    $numero
                );
            }
        } else {
            $numero = "";
        }

        return $numero;
    }
}
?>