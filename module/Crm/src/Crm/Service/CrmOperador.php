<?php

namespace Crm\Service;

use VersaSpine\Service\AbstractService;

class CrmOperador extends AbstractService
{
    const OPERADOR_SITUACAO_ATIVO   = 'Ativo';
    const OPERADOR_SITUACAO_INATIVO = 'Inativo';
    private $__lastError = null;
    /** @var \Sistema\Service\SisConfig */
    private $config = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @return \Sistema\Service\SisConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param \Sistema\Service\SisConfig $config
     * @return CrmLead
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    public function getArrSelect2OperadorSituacao($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getOperadorSituacao());
    }

    public static function getOperadorSituacao()
    {
        return array(self::OPERADOR_SITUACAO_ATIVO, self::OPERADOR_SITUACAO_INATIVO);
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     * @param array                       $arrConfig
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, $arrConfig = array())
    {
        parent::__construct($em, 'Crm\Entity\CrmOperador');
        $this->setConfig(new \Sistema\Service\SisConfig($em, $arrConfig));
    }

    public function pesquisaForJson($params)
    {
        $sql        = '
        SELECT
        b.id AS operador_id,
        b.login AS login,
        a.operadorDataUltimoRecebimento AS operador_data_ultimo_recebimento,
        a.operadorSituacao AS operador_situacao,
        a.operadorPercentual AS operador_percentual,
        t.timeId AS time_id,
        t.timeNome AS time_nome
        FROM Crm\Entity\CrmOperador a
        INNER JOIN Acesso\Entity\AcessoPessoas b WITH b.id = a.operador
        INNER JOIN Crm\Entity\CrmTime t WITH t.timeId = a.time
        WHERE';
        $login      = false;
        $operadorId = false;

        if ($params['q']) {
            $login = $params['q'];
        } elseif ($params['query']) {
            $login = $params['query'];
        }

        if ($params['operadorId']) {
            $operadorId = $params['operadorId'];
        }

        $parameters = array('login' => "{$login}%");
        $sql .= ' b.login LIKE :login';

        if ($operadorId) {
            $parameters['operadorId'] = explode(',', $operadorId);
            $parameters['operadorId'] = $operadorId;
            $sql .= ' AND b.id NOT IN(:operadorId)';
        }

        $sql .= " ORDER BY b.login";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceCrmTime       = new \Crm\Service\CrmTime($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            /** @var $objCrmOperador \Crm\Entity\CrmOperador */
            $objCrmOperador = $this->getRepository()->find($arrDados['operador']);

            if (!$objCrmOperador) {
                $objCrmOperador = new \Crm\Entity\CrmOperador();
            }

            if ($arrDados['operador']) {
                /** @var $objAcessoPessoas \Acesso\Entity\AcessoPessoas */
                $objAcessoPessoas = $serviceAcessoPessoas->getRepository()->find($arrDados['operador']);

                if (!$objAcessoPessoas) {
                    $this->setLastError('Registro de usuário não existe!');

                    return false;
                }

                $objCrmOperador->setOperador($objAcessoPessoas);
            } else {
                $objCrmOperador->setOperador(null);
            }

            if ($arrDados['time']) {
                /** @var $objCrmTime \Crm\Entity\CrmTime */
                $objCrmTime = $serviceCrmTime->getRepository()->find($arrDados['time']);

                if (!$objCrmTime) {
                    $this->setLastError('Registro de time não existe!');

                    return false;
                }

                $objCrmOperador->setTime($objCrmTime);
            } else {
                $objCrmOperador->setTime(null);
            }

            $objCrmOperador->setOperadorDataUltimoRecebimento($arrDados['operadorDataUltimoRecebimento']);
            $objCrmOperador->setOperadorSituacao($arrDados['operadorSituacao']);
            $objCrmOperador->setOperadorPercentual(self::convertNumberFromBRToUS($arrDados['operadorPercentual']));
            $objCrmOperador->setOperadorEntrada1($arrDados['operadorEntrada1']);
            $objCrmOperador->setOperadorSaida1($arrDados['operadorSaida1']);
            $objCrmOperador->setOperadorEntrada2($arrDados['operadorEntrada2']);
            $objCrmOperador->setOperadorSaida2($arrDados['operadorSaida2']);
            $objCrmOperador->setOperadorRamal($arrDados['operadorRamal']);

            $this->getEm()->persist($objCrmOperador);
            $this->getEm()->flush($objCrmOperador);

            $this->getEm()->commit();

            $arrDados['operador'] = $objCrmOperador->getOperador()->getId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de operador!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['operador']) {
            $errors[] = 'Por favor preencha o campo "usuário"!';
        }

        if (!$arrParam['time']) {
            $errors[] = 'Por favor preencha o campo "time"!';
        }

        if (!$arrParam['operadorSituacao']) {
            $errors[] = 'Por favor preencha o campo "situação"!';
        }

        if (!$arrParam['operadorPercentual']) {
            $errors[] = 'Por favor preencha o campo "percentual"!';
        }

        if (!in_array($arrParam['operadorSituacao'], self::getOperadorSituacao())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "situação"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "
        SELECT
            o.*,
            a.login,
            t.time_nome
        FROM crm__operador o
        INNER JOIN acesso_pessoas a ON a.id=o.operador_id
        INNER JOIN crm__time t ON t.time_id=o.time_id
        ";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($operadorId)
    {
        /** @var $objCrmOperador \Crm\Entity\CrmOperador */
        $objCrmOperador       = $this->getRepository()->find($operadorId);
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceCrmTime       = new \Crm\Service\CrmTime($this->getEm());

        try {
            $arrDados = $objCrmOperador->toArray();

            if ($arrDados['operador']) {
                $arrAcessoPessoas     = $serviceAcessoPessoas->getArrSelect2(['id' => $arrDados['operador']]);
                $arrDados['operador'] = $arrAcessoPessoas ? $arrAcessoPessoas[0] : null;
            }

            if ($arrDados['time']) {
                $arrCrmTime       = $serviceCrmTime->getArrSelect2(['id' => $arrDados['time']]);
                $arrDados['time'] = $arrCrmTime ? $arrCrmTime[0] : null;
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceCrmTime       = new \Crm\Service\CrmTime($this->getEm());

        if (is_array($arrDados['operador']) && !$arrDados['operador']['text']) {
            $arrDados['operador'] = $arrDados['operador']['id'];
        }

        if ($arrDados['operador'] && is_string($arrDados['operador'])) {
            $arrDados['operador'] = $serviceAcessoPessoas->getArrSelect2(array('id' => $arrDados['operador']));
            $arrDados['operador'] = $arrDados['operador'] ? $arrDados['operador'][0] : null;
        }

        if (is_array($arrDados['time']) && !$arrDados['time']['text']) {
            $arrDados['time'] = $arrDados['time']['timeId'];
        }

        if ($arrDados['time'] && is_string($arrDados['time'])) {
            $arrDados['time'] = $serviceCrmTime->getArrSelect2(array('id' => $arrDados['time']));
            $arrDados['time'] = $arrDados['time'] ? $arrDados['time'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['operador'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam);

        $arrEntitiesArr = array();
        /* @var $objEntity \Crm\Entity\CrmOperador */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getOperador()->getId();
            $arrEntity[$params['value']] = $objEntity->getOperador()->getLogin();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['operadorId']) {
            $this->setLastError('Para remover um registro de operador é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objCrmOperador \Crm\Entity\CrmOperador */
            $objCrmOperador = $this->getRepository()->find($param['operadorId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objCrmOperador);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de operador.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceCrmTime = new \Crm\Service\CrmTime($this->getEm());
        $serviceCrmTime->setarDependenciasView($view);

        $view->setVariable("arrOperadorSituacao", $this->getArrSelect2OperadorSituacao());
    }

    public function retornaOperadoresParaDistribuicaoLead($numeroDeLeads, $horarioDeTrabalho = true)
    {
        $querySomenteAgentesLogados = ' AND date(now()) = date(acesso_pessoas.data_ultimo_login)';
        $queryPeriodoTrabalho       = ' AND
       (
         (time(now()) >= crm__operador.operador_entrada1 AND time(now()) <= crm__operador.operador_saida1) OR
         (time(now()) >= crm__operador.operador_entrada2 AND time(now()) <= crm__operador.operador_saida2)
       )';

        $query = "
        SELECT
          operador_id AS operadorId,
          login AS operadorLogin,
          crm__time.time_id AS timeId,
          operador_percentual AS operadorPercentual,
          time_nome AS timeNome,
          operador_data_ultimo_recebimento AS operadorDataUltimoRecebimento,
          0 AS totalLeadsOperador
        FROM crm__operador
          INNER JOIN crm__time
            ON crm__time.time_id = crm__operador.time_id AND
            time_situacao = 'Ativo'
          INNER JOIN acesso_pessoas
            ON acesso_pessoas.id = crm__operador.operador_id
            " . ($horarioDeTrabalho ? $queryPeriodoTrabalho : '') . "
        WHERE operador_situacao = 'Ativo'
        ORDER BY operador_data_ultimo_recebimento ASC";

        $arrOperadores = $this->executeQueryWithParam($query)->fetchAll();
        $ativos        = $this->retornaNumeroDeOperadoresAtivos();
        $quorumAtual   = ceil((count($arrOperadores) * 100) / $ativos);
        $quorumMinimo  = $this->getConfig()->localizarChave('CRM_QUORUM_MINIMO', 30);

        if ($quorumAtual < $quorumMinimo) {
            return [];
        }

        do {
            $numeroDeLeadsDistribuidos = 0;
            $totalLeadsPorOperador     = ceil($numeroDeLeads / count($arrOperadores));

            foreach ($arrOperadores as $pos => $arrOperador) {
                $totalLeadsOperador = ceil(($totalLeadsPorOperador * $arrOperador['operadorPercentual']) / 100);

                $numeroDeLeadsDistribuidos += $totalLeadsOperador;
                $arrOperadores[$pos]['totalLeadsOperador'] += $totalLeadsOperador;
            }

            $numeroDeLeads = $numeroDeLeads - $numeroDeLeadsDistribuidos;

            if ($numeroDeLeads < 0) {
                $numeroDeLeads = 0;
            }
        } while ($numeroDeLeads > 0 && count($arrOperadores) > 0);

        return $arrOperadores;
    }

    public function retornaNumeroDeOperadoresAtivos()
    {
        $query = "
        SELECT
          count(operador_id) AS ativos
        FROM crm__operador
        INNER JOIN crm__time ON
            crm__time.time_id = crm__operador.time_id AND
            time_situacao = 'Ativo'
        WHERE operador_situacao = 'Ativo'";

        $ativos = $this->executeQueryWithParam($query)->fetch(\PDO::FETCH_COLUMN);

        return $ativos * 1;
    }

    public function retornaOperadorLogado($operadorFallBack = null)
    {
        $objCrmOperador       = null;
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());

        if ($operadorFallBack) {
            /** @var \Crm\Entity\CrmOperador $objCrmOperador */
            $objCrmOperador = $this->getRepository()->find($operadorFallBack);
        }

        if (!$objCrmOperador) {
            /** @var \Acesso\Entity\AcessoPessoas $objPessoas */
            $objPessoas = $serviceAcessoPessoas->retornaUsuarioLogado($operadorFallBack);

            if (!$objPessoas) {
                return null;
            }

            /** @var \Crm\Entity\CrmOperador $objCrmOperador */
            $objCrmOperador = $this->getRepository()->find($objPessoas->getId());
        }

        return $objCrmOperador;
    }

    /**
     * @return null
     */
    public function retornaOperadorLogadoSelect2()
    {
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());
        $objAcessoPessoas     = $serviceAcessoPessoas->retornaUsuarioLogado();
        $arrItem              = [];

        if ($objAcessoPessoas) {
            $arrItem = $this->getArrSelect2(['id' => $objAcessoPessoas->getId()]);

            if ($arrItem) {
                return $arrItem[0];
            }
        }

        if (!$arrItem) {
            $arrItem = ['id' => '-1', 'text' => 'Indefinido'];
        }

        return $arrItem;
    }

    public function efetuarChamada($numero = '')
    {
        $ativo = $this->getConfig()->localizarChave('SONAX_CLICK2CALL_ATIVO');

        if (!$ativo) {
            $this->setLastError('Click2Call desativado!');

            return false;
        }

        $objOperador = $this->retornaOperadorLogado();

        if (!$objOperador) {
            $this->setLastError('Operador não está logado!');

            return false;
        }

        $ramal = $objOperador->getOperadorRamal();

        if (!$ramal) {
            $this->setLastError('Ramal não definido para operador!');

            return false;
        }

        if (!$numero) {
            $this->setLastError('Número não definido!');

            return false;
        }

        $url   = 'http://sip.sonax.net.br/sonax-click2call.php?';
        $conta = $this->getConfig()->localizarChave('SONAX_CLICK2CALL_CONTA');
        $token = $this->getConfig()->localizarChave('SONAX_CLICK2CALL_TOKEN');

        if (!$conta) {
            $this->setLastError('Conta Click2Call não definida!');

            return false;
        }

        if (!$token) {
            $this->setLastError('Token Click2Call não definido!');

            return '';
        }

        $params = [
            'account' => $conta,
            'ramal'   => $ramal,
            'token'   => $token,
            'numero'  => $numero
        ];

        $url      = $url . http_build_query($params);
        $resposta = $this->getResponse($url);

        if (substr_count($resposta, 'Sucesso') == 0) {
            $this->setLastError(trim(strip_tags($resposta)));

            return false;
        }

        return true;
    }

    /**
     * @param     $url
     * @param int $timeout
     * @return mixed
     */
    private function getResponse($url, $timeout = 6000)
    {
        set_time_limit(15);
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        $output = curl_exec($curl);
        curl_close($curl);

        return $output;
    }
}
?>