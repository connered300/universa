<?php

namespace Crm\Service;

use VersaSpine\Service\AbstractService;

class CrmLeadEvento extends AbstractService
{
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Crm\Entity\CrmLeadEvento');
    }

    public function pesquisaForJson($params)
    {
        $sql        = '
        SELECT
        a.eventoId AS evento_id,
        a.eventoData AS evento_data,
        a.pesId AS pes_id,
        a.mediadorId AS mediador_id,
        a.campanhaId AS campanha_id,
        a.fonteId AS fonte_id,
        a.eventoOutraInformacao AS evento_outra_informacao
        FROM Crm\Entity\CrmLeadEvento a
        WHERE';
        $eventoData = false;
        $eventoId   = false;

        if ($params['q']) {
            $eventoData = $params['q'];
        } elseif ($params['query']) {
            $eventoData = $params['query'];
        }

        if ($params['eventoId']) {
            $eventoId = $params['eventoId'];
        }

        $parameters = array('eventoData' => "{$eventoData}%");
        $sql .= ' a.eventoData LIKE :eventoData';

        if ($eventoId) {
            $parameters['eventoId'] = explode(',', $eventoId);
            $parameters['eventoId'] = $eventoId;
            $sql .= ' AND a.eventoId NOT IN(:eventoId)';
        }

        $sql .= " ORDER BY a.eventoData";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceCrmLead     = new \Crm\Service\CrmLead($this->getEm());
        $serviceCrmMediador = new \Crm\Service\CrmMediador($this->getEm());
        $serviceCrmCampanha = new \Crm\Service\CrmCampanha($this->getEm());
        $serviceCrmFonte    = new \Crm\Service\CrmFonte($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['eventoId']) {
                /** @var $objCrmLeadEvento \Crm\Entity\CrmLeadEvento */
                $objCrmLeadEvento = $this->getRepository()->find($arrDados['eventoId']);

                if (!$objCrmLeadEvento) {
                    $this->setLastError('Registro de lead evento não existe!');

                    return false;
                }
            } else {
                $objCrmLeadEvento = new \Crm\Entity\CrmLeadEvento();
            }

            if ($arrDados['pes']) {
                /** @var $objCrmLead \Crm\Entity\CrmLead */
                $objCrmLead = $serviceCrmLead->getRepository()->find($arrDados['pes']);

                if (!$objCrmLead) {
                    $this->setLastError('Registro de lead não existe!');

                    return false;
                }

                $objCrmLeadEvento->setPes($objCrmLead);
            } else {
                $objCrmLeadEvento->setPes(null);
            }

            if ($arrDados['mediador']) {
                /** @var $objCrmMediador \Crm\Entity\CrmMediador */
                $objCrmMediador = $serviceCrmMediador->getRepository()->find($arrDados['mediador']);

                if (!$objCrmMediador) {
                    $this->setLastError('Registro de mediador não existe!');

                    return false;
                }

                $objCrmLeadEvento->setMediador($objCrmMediador);
            } else {
                $objCrmLeadEvento->setMediador(null);
            }

            if ($arrDados['campanha']) {
                /** @var $objCrmCampanha \Crm\Entity\CrmCampanha */
                $objCrmCampanha = $serviceCrmCampanha->getRepository()->find($arrDados['campanha']);

                if (!$objCrmCampanha) {
                    $this->setLastError('Registro de campanha não existe!');

                    return false;
                }

                $objCrmLeadEvento->setCampanha($objCrmCampanha);
            } else {
                $objCrmLeadEvento->setCampanha(null);
            }

            if ($arrDados['fonte']) {
                /** @var $objCrmFonte \Crm\Entity\CrmFonte */
                $objCrmFonte = $serviceCrmFonte->getRepository()->find($arrDados['fonte']);

                if (!$objCrmFonte) {
                    $this->setLastError('Registro de fonte não existe!');

                    return false;
                }

                $objCrmLeadEvento->setFonte($objCrmFonte);
            } else {
                $objCrmLeadEvento->setFonte(null);
            }

            $objCrmLeadEvento->setEventoData(new \Datetime());
            $objCrmLeadEvento->setEventoOutraInformacao($arrDados['eventoOutraInformacao']);

            $this->getEm()->persist($objCrmLeadEvento);
            $this->getEm()->flush($objCrmLeadEvento);

            $this->getEm()->commit();

            $arrDados['eventoId'] = $objCrmLeadEvento->getEventoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de lead evento!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['pes']) {
            $errors[] = 'Por favor preencha o campo "código pessoa"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM crm__lead_evento";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($eventoId)
    {
        $arrDados = array();

        if (!$eventoId) {
            $this->setLastError('Lead evento inválido!');

            return array();
        }

        /** @var $objCrmLeadEvento \Crm\Entity\CrmLeadEvento */
        $objCrmLeadEvento = $this->getRepository()->find($eventoId);

        if (!$objCrmLeadEvento) {
            $this->setLastError('Lead evento não existe!');

            return array();
        }

        $serviceCrmLead     = new \Crm\Service\CrmLead($this->getEm());
        $serviceCrmMediador = new \Crm\Service\CrmMediador($this->getEm());
        $serviceCrmCampanha = new \Crm\Service\CrmCampanha($this->getEm());
        $serviceCrmFonte    = new \Crm\Service\CrmFonte($this->getEm());

        try {
            $arrDados = $objCrmLeadEvento->toArray();

            if ($arrDados['pes']) {
                $arrCrmLead      = $serviceCrmLead->getArrSelect2(['id' => $arrDados['pes']]);
                $arrDados['pes'] = $arrCrmLead ? $arrCrmLead[0] : null;
            }

            if ($arrDados['mediador']) {
                $arrCrmMediador       = $serviceCrmMediador->getArrSelect2(['id' => $arrDados['mediador']]);
                $arrDados['mediador'] = $arrCrmMediador ? $arrCrmMediador[0] : null;
            }

            if ($arrDados['campanha']) {
                $arrCrmCampanha       = $serviceCrmCampanha->getArrSelect2(['id' => $arrDados['campanha']]);
                $arrDados['campanha'] = $arrCrmCampanha ? $arrCrmCampanha[0] : null;
            }

            if ($arrDados['fonte']) {
                $arrCrmFonte       = $serviceCrmFonte->getArrSelect2(['id' => $arrDados['fonte']]);
                $arrDados['fonte'] = $arrCrmFonte ? $arrCrmFonte[0] : null;
            }
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceCrmLead     = new \Crm\Service\CrmLead($this->getEm());
        $serviceCrmMediador = new \Crm\Service\CrmMediador($this->getEm());
        $serviceCrmCampanha = new \Crm\Service\CrmCampanha($this->getEm());
        $serviceCrmFonte    = new \Crm\Service\CrmFonte($this->getEm());

        if (is_array($arrDados['pes']) && !$arrDados['pes']['text']) {
            $arrDados['pes'] = $arrDados['pes']['pesId'];
        }

        if ($arrDados['pes'] && is_string($arrDados['pes'])) {
            $arrDados['pes'] = $serviceCrmLead->getArrSelect2(array('id' => $arrDados['pes']));
            $arrDados['pes'] = $arrDados['pes'] ? $arrDados['pes'][0] : null;
        }

        if (is_array($arrDados['mediador']) && !$arrDados['mediador']['text']) {
            $arrDados['mediador'] = $arrDados['mediador']['mediadorId'];
        }

        if ($arrDados['mediador'] && is_string($arrDados['mediador'])) {
            $arrDados['mediador'] = $serviceCrmMediador->getArrSelect2(array('id' => $arrDados['mediador']));
            $arrDados['mediador'] = $arrDados['mediador'] ? $arrDados['mediador'][0] : null;
        }

        if (is_array($arrDados['campanha']) && !$arrDados['campanha']['text']) {
            $arrDados['campanha'] = $arrDados['campanha']['campanhaId'];
        }

        if ($arrDados['campanha'] && is_string($arrDados['campanha'])) {
            $arrDados['campanha'] = $serviceCrmCampanha->getArrSelect2(array('id' => $arrDados['campanha']));
            $arrDados['campanha'] = $arrDados['campanha'] ? $arrDados['campanha'][0] : null;
        }

        if (is_array($arrDados['fonte']) && !$arrDados['fonte']['text']) {
            $arrDados['fonte'] = $arrDados['fonte']['fonteId'];
        }

        if ($arrDados['fonte'] && is_string($arrDados['fonte'])) {
            $arrDados['fonte'] = $serviceCrmFonte->getArrSelect2(array('id' => $arrDados['fonte']));
            $arrDados['fonte'] = $arrDados['fonte'] ? $arrDados['fonte'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['eventoId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['eventoData' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Crm\Entity\CrmLeadEvento */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getEventoId();
            $arrEntity[$params['value']] = $objEntity->getEventoData();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($eventoId)
    {
        if (!$eventoId) {
            $this->setLastError('Para remover um registro de lead evento é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objCrmLeadEvento \Crm\Entity\CrmLeadEvento */
            $objCrmLeadEvento = $this->getRepository()->find($eventoId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objCrmLeadEvento);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de lead evento.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceCrmLead     = new \Crm\Service\CrmLead($this->getEm());
        $serviceCrmMediador = new \Crm\Service\CrmMediador($this->getEm());
        $serviceCrmCampanha = new \Crm\Service\CrmCampanha($this->getEm());
        $serviceCrmFonte    = new \Crm\Service\CrmFonte($this->getEm());

        $serviceCrmLead->setarDependenciasView($view);
        $serviceCrmMediador->setarDependenciasView($view);
        $serviceCrmCampanha->setarDependenciasView($view);
        $serviceCrmFonte->setarDependenciasView($view);
    }
}
?>