<?php

namespace Crm\Service;

use VersaSpine\Service\AbstractService;

class CrmAtividadeTipo extends AbstractService
{
    const INCIDENCIA_TIPO_POSITIVA = 'Positiva';
    const INCIDENCIA_TIPO_NEGATIVA = 'Negativa';
    const INCIDENCIA_TIPO_NEUTRA = 'Neutra';
    const INCIDENCIA_COMENTARIO_OBRIGATORIO_SIM = 'Sim';
    const INCIDENCIA_COMENTARIO_OBRIGATORIO_NAO = 'Não';
    const INCIDENCIA_CARACTERIZA_DESISTENCIA_SIM = 'Sim';
    const INCIDENCIA_CARACTERIZA_DESISTENCIA_NAO = 'Não';
    const INCIDENCIA_CARACTERIZA_DESISTENCIA_OPCIONAL = 'Opcional';
    const INCIDENCIA_AGENDAMENTO_OPCIONAL = 'Opcional';
    const INCIDENCIA_AGENDAMENTO_OBRIGATORIO = 'Obrigatório';
    const INCIDENCIA_AGENDAMENTO_INDISPONIVEL = 'Indisponível';
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2AtividadeTipo($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getAtividadeTipo());
    }

    public static function getAtividadeTipo()
    {
        return array(self::INCIDENCIA_TIPO_POSITIVA, self::INCIDENCIA_TIPO_NEGATIVA, self::INCIDENCIA_TIPO_NEUTRA);
    }

    public function getArrSelect2AtividadeComentarioObrigatorio($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getAtividadeComentarioObrigatorio());
    }

    public static function getAtividadeComentarioObrigatorio()
    {
        return array(self::INCIDENCIA_COMENTARIO_OBRIGATORIO_SIM, self::INCIDENCIA_COMENTARIO_OBRIGATORIO_NAO);
    }

    public function getArrSelect2AtividadeCaracterizaDesistencia($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getAtividadeCaracterizaDesistencia());
    }

    public static function getAtividadeCaracterizaDesistencia()
    {
        return array(
            self::INCIDENCIA_CARACTERIZA_DESISTENCIA_SIM,
            self::INCIDENCIA_CARACTERIZA_DESISTENCIA_NAO,
            self::INCIDENCIA_CARACTERIZA_DESISTENCIA_OPCIONAL
        );
    }

    public function getArrSelect2AtividadeAgendamento($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getAtividadeAgendamento());
    }

    public static function getAtividadeAgendamento()
    {
        return array(
            self::INCIDENCIA_AGENDAMENTO_OPCIONAL,
            self::INCIDENCIA_AGENDAMENTO_INDISPONIVEL,
            self::INCIDENCIA_AGENDAMENTO_OBRIGATORIO
        );
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Crm\Entity\CrmAtividadeTipo');
    }

    public function pesquisaForJson($params)
    {
        $sql = "SELECT * FROM view__crm_atividade_tipo";

        $atividadeTipoNome = false;

        if ($params['q']) {
            $atividadeTipoNome = $params['q'];
        } elseif ($params['query']) {
            $atividadeTipoNome = $params['query'];
        }

        $where = [];

        $parameters = array('atividadeTipoNome' => "{$atividadeTipoNome}%");
        $where[]    = 'atividade_tipo_nome LIKE :atividadeTipoNome';

        if ($params['atividadeTipoId']) {
            $parameters['atividadeTipoId'] = is_array($params['atividadeTipoId']) ? $params['atividadeTipoId']:  explode(',', $params['atividadeTipoId']);
            $where[] = 'atividade_tipo_id NOT IN(:atividadeTipoId)';
        }

        if ($params['apenasFolhas']) {
            $where[] = 'numero_atividades_flhas = 0';
        }

        if ($params['apenasNivel1']) {
            $where[] = 'atividade_pai IS NULL';
        }

        $sql .= (count($where) > 0 ? ' WHERE ' . implode(' AND ', $where) : '');

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceCrmAtividadeTipo = new \Crm\Service\CrmAtividadeTipo($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['atividadeTipoId']) {
                /** @var $objCrmAtividadeTipo \Crm\Entity\CrmAtividadeTipo */
                $objCrmAtividadeTipo = $this->getRepository()->find($arrDados['atividadeTipoId']);

                if (!$objCrmAtividadeTipo) {
                    $this->setLastError('Registro de tipo atividade não existe!');

                    return false;
                }
            } else {
                $objCrmAtividadeTipo = new \Crm\Entity\CrmAtividadeTipo();
            }

            $objCrmAtividadeTipo->setAtividadeTipoNome($arrDados['atividadeTipoNome']);

            if ($arrDados['atividadePai']) {
                /** @var $objCrmAtividadePai \Crm\Entity\CrmAtividadeTipo */
                $objCrmAtividadePai = $serviceCrmAtividadeTipo->getRepository()->find($arrDados['atividadePai']);

                if (!$objCrmAtividadePai) {
                    $this->setLastError('Registro de tipo atividade não existe!');

                    return false;
                }

                $objCrmAtividadeTipo->setAtividadePai($objCrmAtividadePai);
            } else {
                $objCrmAtividadeTipo->setAtividadePai(null);
            }

            $objCrmAtividadeTipo->setAtividadeTipo($arrDados['atividadeTipo']);
            $objCrmAtividadeTipo->setAtividadeComentarioObrigatorio($arrDados['atividadeComentarioObrigatorio']);
            $objCrmAtividadeTipo->setAtividadeCaracterizaDesistencia($arrDados['atividadeCaracterizaDesistencia']);
            $objCrmAtividadeTipo->setAtividadeAgendamento($arrDados['atividadeAgendamento']);
            $objCrmAtividadeTipo->setAtividadeLeadStatus($arrDados['atividadeLeadStatus']);

            $this->getEm()->persist($objCrmAtividadeTipo);
            $this->getEm()->flush($objCrmAtividadeTipo);

            $this->getEm()->commit();

            $arrDados['atividadeTipoId'] = $objCrmAtividadeTipo->getAtividadeTipoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de tipo atividade!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['atividadeTipo']) {
            $errors[] = 'Por favor preencha o campo "tipo de atividade"!';
        }

        if (!in_array($arrParam['atividadeTipo'], self::getAtividadeTipo())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "tipo de atividade"!';
        }

        if (!$arrParam['atividadeComentarioObrigatorio']) {
            $errors[] = 'Por favor preencha o campo "obrigatorio comentario"!';
        }

        if (!in_array($arrParam['atividadeComentarioObrigatorio'], self::getAtividadeComentarioObrigatorio())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "obrigatorio comentario"!';
        }

        if (!$arrParam['atividadeCaracterizaDesistencia']) {
            $errors[] = 'Por favor preencha o campo "desistencia caracteriza"!';
        }

        if (!in_array($arrParam['atividadeCaracterizaDesistencia'], self::getAtividadeCaracterizaDesistencia())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "desistencia caracteriza"!';
        }

        if (!$arrParam['atividadeAgendamento']) {
            $errors[] = 'Por favor preencha o campo "agendamento"!';
        }

        if (!in_array($arrParam['atividadeAgendamento'], self::getAtividadeAgendamento())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "agendamento"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM view__crm_atividade_tipo";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($atividadeTipoId)
    {
        /** @var $objCrmAtividadeTipo \Crm\Entity\CrmAtividadeTipo */
        $objCrmAtividadeTipo     = $this->getRepository()->find($atividadeTipoId);
        $serviceCrmAtividadeTipo = new \Crm\Service\CrmAtividadeTipo($this->getEm());

        try {
            $arrDados = $objCrmAtividadeTipo->toArray();

            if ($arrDados['atividadePai']) {
                $arrCrmAtividadeTipo      = $serviceCrmAtividadeTipo->getArrSelect2(
                    ['id' => $arrDados['atividadePai']]
                );
                $arrDados['atividadePai'] = $arrCrmAtividadeTipo ? $arrCrmAtividadeTipo[0] : null;
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceCrmAtividadeTipo = new \Crm\Service\CrmAtividadeTipo($this->getEm());

        if (is_array($arrDados['atividadePai']) && !$arrDados['atividadePai']['text']) {
            $arrDados['atividadePai'] = $arrDados['atividadePai']['atividadeTipoId'];
        }

        if ($arrDados['atividadePai'] && is_string($arrDados['atividadePai'])) {
            $arrDados['atividadePai'] = $serviceCrmAtividadeTipo->getArrSelect2(
                array('id' => $arrDados['atividadePai'])
            );
            $arrDados['atividadePai'] = $arrDados['atividadePai'] ? $arrDados['atividadePai'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['atividadeTipoId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['atividadeTipoNome' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Crm\Entity\CrmAtividadeTipo */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getAtividadeTipoId();
            $arrEntity[$params['value']] = $objEntity->getAtividadeTipoNome();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['atividadeTipoId']) {
            $this->setLastError('Para remover um registro de tipo atividade é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objCrmAtividadeTipo \Crm\Entity\CrmAtividadeTipo */
            $objCrmAtividadeTipo = $this->getRepository()->find($param['atividadeTipoId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objCrmAtividadeTipo);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de tipo atividade.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceCrmLead = new \Crm\Service\CrmLead($this->getEm());

        $view->setVariables(
            [
                "arrAtividadeTipo"                   => $this->getArrSelect2AtividadeTipo(),
                "arrAtividadeComentarioObrigatorio"  => $this->getArrSelect2AtividadeComentarioObrigatorio(),
                "arrAtividadeCaracterizaDesistencia" => $this->getArrSelect2AtividadeCaracterizaDesistencia(),
                "arrAtividadeAgendamento"            => $this->getArrSelect2AtividadeAgendamento(),
                "arrLeadStatus"                      => $serviceCrmLead->getArrSelect2LeadStatus()
            ]
        );
    }
}
?>