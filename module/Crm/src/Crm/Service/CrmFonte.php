<?php

namespace Crm\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class CrmFonte
 * @package Crm\Service
 */
class CrmFonte extends AbstractService
{
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Crm\Entity\CrmFonte');
    }

    /**
     * @param $params
     * @return array
     */
    public function pesquisaForJson($params)
    {
        $sql       = '
        SELECT
        a.fonteId as fonte_id,
        a.fonteNome as fonte_nome
        FROM Crm\Entity\CrmFonte a
        WHERE';
        $fonteNome = false;
        $fonteId   = false;

        if ($params['q']) {
            $fonteNome = $params['q'];
        } elseif ($params['query']) {
            $fonteNome = $params['query'];
        }

        if ($params['fonteId']) {
            $fonteId = $params['fonteId'];
        }

        $parameters = array('fonteNome' => "{$fonteNome}%");
        $sql .= ' a.fonteNome LIKE :fonteNome';

        if ($fonteId) {
            $parameters['fonteId'] = explode(',', $fonteId);
            $parameters['fonteId'] = $fonteId;
            $sql .= ' AND a.fonteId NOT IN(:fonteId)';
        }

        $sql .= " ORDER BY a.fonteNome";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['fonteId']) {
                /** @var $objCrmFonte \Crm\Entity\CrmFonte */
                $objCrmFonte = $this->getRepository()->find($arrDados['fonteId']);

                if (!$objCrmFonte) {
                    $this->setLastError('Registro de fonte não existe!');

                    return false;
                }
            } else {
                $objCrmFonte = new \Crm\Entity\CrmFonte();
            }

            $objCrmFonte->setFonteNome($arrDados['fonteNome']);

            $this->getEm()->persist($objCrmFonte);
            $this->getEm()->flush($objCrmFonte);

            $this->getEm()->commit();

            $arrDados['fonteId'] = $objCrmFonte->getFonteId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de fonte!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['fonteNome']) {
            $errors[] = 'Por favor preencha o campo "nome"!';
        }

        if ($this->verificaSeRegistroEstaDuplicado($arrParam['fonteNome'], $arrParam['fonteId'])) {
            $errors[] = "Já existe uma fonte cadastrada com este mesmo nome!";
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    /**
     * @param            $fonteNome
     * @param bool|false $fonteId
     * @return bool
     */
    public function verificaSeRegistroEstaDuplicado($fonteNome, $fonteId = false)
    {
        $sql        = 'SELECT count(*) as qtd FROM crm__fonte WHERE fonte_nome like :fonteNome';
        $parameters = array('fonteNome' => $fonteNome);

        if ($fonteId) {
            $sql .= ' AND fonte_id<>:fonteId';
            $parameters['fonteId'] = $fonteId;
        }

        $result = $this->executeQueryWithParam($sql, $parameters)->fetch();

        return $result['qtd'] > 0;
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM crm__fonte";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $fonteId
     * @return array
     */
    public function getArray($fonteId)
    {
        /** @var $objCrmFonte \Crm\Entity\CrmFonte */
        $objCrmFonte = $this->getRepository()->find($fonteId);

        try {
            $arrDados = $objCrmFonte->toArray();
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {

        return $arrDados;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['fonteId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['fonteNome' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Crm\Entity\CrmFonte */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getFonteId();
            $arrEntity[$params['value']] = $objEntity->getFonteNome();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $fonte
     * @return \Crm\Entity\CrmFonte|null
     */
    public function getFonteObj($fonte)
    {
        $arrParam = array();

        if (!$fonte) {
            return null;
        }

        if (is_numeric($fonte)) {
            $arrParam['fonteId'] = $fonte;
        } elseif (is_string($fonte)) {
            $arrParam['fonteNome'] = $fonte;
        }

        /** @var \Crm\Entity\CrmFonte $objEntity */
        $objEntity = $this->getRepository()->findOneBy($arrParam, ['fonteId' => 'asc']);

        if (!$objEntity && is_string($fonte)) {
            if ($this->save($arrParam)) {
                $objEntity = $this->getRepository()->find($arrParam['fonteId']);
            }
        }

        return $objEntity;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['fonteId']) {
            $this->setLastError('Para remover um registro de fonte é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objCrmFonte \Crm\Entity\CrmFonte */
            $objCrmFonte = $this->getRepository()->find($param['fonteId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objCrmFonte);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de fonte.');

            return false;
        }

        return true;
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
    }
}
?>