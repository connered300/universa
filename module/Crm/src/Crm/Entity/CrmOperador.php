<?php

namespace Crm\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * CrmOperador
 *
 * @ORM\Table(name="crm__operador")
 * @ORM\Entity
 * @LG\LG(id="operadorId",label="TimeId")
 * @Jarvis\Jarvis(title="Listagem de operador",icon="fa fa-table")
 */
class CrmOperador
{
    /**
     * @var \Acesso\Entity\AcessoPessoas
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="operador_id", referencedColumnName="id")
     * })
     */
    private $operador;

    /**
     * @var \Crm\Entity\CrmTime
     * @ORM\ManyToOne(targetEntity="Crm\Entity\CrmTime")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="time_id", referencedColumnName="time_id")
     * })
     */
    private $time;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="operador_data_ultimo_recebimento", type="datetime", nullable=true)
     * @LG\Labels\Property(name="operador_data_ultimo_recebimento")
     * @LG\Labels\Attributes(text="recebimento último data")
     * @LG\Querys\Conditions(type="=")
     */
    private $operadorDataUltimoRecebimento;

    /**
     * @var string
     *
     * @ORM\Column(name="operador_situacao", type="string", nullable=false, length=7)
     * @LG\Labels\Property(name="operador_situacao")
     * @LG\Labels\Attributes(text="situação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $operadorSituacao;

    /**
     * @var float
     *
     * @ORM\Column(name="operador_percentual", type="float", nullable=true)
     * @LG\Labels\Property(name="operador_percentual")
     * @LG\Labels\Attributes(text="percentual")
     * @LG\Querys\Conditions(type="=")
     */
    private $operadorPercentual;

    /**
     * @var string
     *
     * @ORM\Column(name="operador_entrada1", type="string", nullable=true)
     * @LG\Labels\Property(name="operador_entrada1")
     * @LG\Labels\Attributes(text="entrada1")
     * @LG\Querys\Conditions(type="=")
     */
    private $operadorEntrada1;

    /**
     * @var string
     *
     * @ORM\Column(name="operador_saida1", type="string", nullable=true)
     * @LG\Labels\Property(name="operador_saida1")
     * @LG\Labels\Attributes(text="saida1")
     * @LG\Querys\Conditions(type="=")
     */
    private $operadorSaida1;

    /**
     * @var string
     *
     * @ORM\Column(name="operador_entrada2", type="string", nullable=true)
     * @LG\Labels\Property(name="operador_entrada2")
     * @LG\Labels\Attributes(text="entrada2")
     * @LG\Querys\Conditions(type="=")
     */
    private $operadorEntrada2;

    /**
     * @var string
     *
     * @ORM\Column(name="operador_saida2", type="string", nullable=true)
     * @LG\Labels\Property(name="operador_saida2")
     * @LG\Labels\Attributes(text="saida2")
     * @LG\Querys\Conditions(type="=")
     */
    private $operadorSaida2;

    /**
     * @var int
     *
     * @ORM\Column(name="operador_ramal", type="integer", nullable=true)
     * @LG\Labels\Property(name="operador_ramal")
     * @LG\Labels\Attributes(text="ramal")
     * @LG\Querys\Conditions(type="=")
     */
    private $operadorRamal;

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getOperador()
    {
        return $this->operador;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $operador
     * @return CrmOperador
     */
    public function setOperador($operador)
    {
        $this->operador = $operador;

        return $this;
    }

    /**
     * @return \Crm\Entity\CrmTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param \Crm\Entity\CrmTime $time
     * @return CrmOperador
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getOperadorDataUltimoRecebimento($format = false)
    {
        $operadorDataUltimoRecebimento = $this->operadorDataUltimoRecebimento;

        if ($format && $operadorDataUltimoRecebimento) {
            $operadorDataUltimoRecebimento = $operadorDataUltimoRecebimento->format('d/m/Y H:i:s');
        }

        return $operadorDataUltimoRecebimento;
    }

    /**
     * @param \Datetime $operadorDataUltimoRecebimento
     * @return CrmOperador
     */
    public function setOperadorDataUltimoRecebimento($operadorDataUltimoRecebimento)
    {
        if ($operadorDataUltimoRecebimento) {
            if (is_string($operadorDataUltimoRecebimento)) {
                $operadorDataUltimoRecebimento = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $operadorDataUltimoRecebimento
                );
                $operadorDataUltimoRecebimento = new \Datetime($operadorDataUltimoRecebimento);
            }
        } else {
            $operadorDataUltimoRecebimento = null;
        }
        $this->operadorDataUltimoRecebimento = $operadorDataUltimoRecebimento;

        return $this;
    }

    /**
     * @return string
     */
    public function getOperadorSituacao()
    {
        return $this->operadorSituacao;
    }

    /**
     * @param string $operadorSituacao
     * @return CrmOperador
     */
    public function setOperadorSituacao($operadorSituacao)
    {
        $this->operadorSituacao = $operadorSituacao;

        return $this;
    }

    /**
     * @return float
     */
    public function getOperadorPercentual()
    {
        return $this->operadorPercentual;
    }

    /**
     * @param float $operadorPercentual
     * @return CrmOperador
     */
    public function setOperadorPercentual($operadorPercentual)
    {
        $this->operadorPercentual = $operadorPercentual;

        return $this;
    }

    /**
     * @return string
     */
    public function getOperadorEntrada1()
    {
        $operadorEntrada1 = $this->operadorEntrada1;

        if ($operadorEntrada1 && strlen($operadorEntrada1) > 5) {
            $operadorEntrada1 = substr($operadorEntrada1, 0, 5);
        }

        return $operadorEntrada1;
    }

    /**
     * @param string $operadorEntrada1
     * @return CrmOperador
     */
    public function setOperadorEntrada1($operadorEntrada1)
    {
        if ($operadorEntrada1 && strlen($operadorEntrada1) == 5) {
            $operadorEntrada1 = $operadorEntrada1 . ':00';
        }

        $this->operadorEntrada1 = $operadorEntrada1;

        return $this;
    }

    /**
     * @return string
     */
    public function getOperadorSaida1()
    {
        $operadorSaida1 = $this->operadorSaida1;

        if ($operadorSaida1 && strlen($operadorSaida1) > 5) {
            $operadorSaida1 = substr($operadorSaida1, 0, 5);
        }

        return $operadorSaida1;
    }

    /**
     * @param string $operadorSaida1
     * @return CrmOperador
     */
    public function setOperadorSaida1($operadorSaida1)
    {
        if ($operadorSaida1 && strlen($operadorSaida1) == 5) {
            $operadorSaida1 = $operadorSaida1 . ':00';
        }

        $this->operadorSaida1 = $operadorSaida1;

        return $this;
    }

    /**
     * @return string
     */
    public function getOperadorEntrada2()
    {
        $operadorEntrada2 = $this->operadorEntrada2;

        if ($operadorEntrada2 && strlen($operadorEntrada2) > 5) {
            $operadorEntrada2 = substr($operadorEntrada2, 0, 5);
        }

        return $operadorEntrada2;
    }

    /**
     * @param string $operadorEntrada2
     * @return CrmOperador
     */
    public function setOperadorEntrada2($operadorEntrada2)
    {
        if ($operadorEntrada2 && strlen($operadorEntrada2) == 5) {
            $operadorEntrada2 = $operadorEntrada2 . ':00';
        }

        $this->operadorEntrada2 = $operadorEntrada2;

        return $this;
    }

    /**
     * @return string
     */
    public function getOperadorSaida2()
    {
        $operadorSaida2 = $this->operadorSaida2;

        if ($operadorSaida2 && strlen($operadorSaida2) > 5) {
            $operadorSaida2 = substr($operadorSaida2, 0, 5);
        }

        return $operadorSaida2;
    }

    /**
     * @param string $operadorSaida2
     * @return CrmOperador
     */
    public function setOperadorSaida2($operadorSaida2)
    {
        if ($operadorSaida2 && strlen($operadorSaida2) == 5) {
            $operadorSaida2 = $operadorSaida2 . ':00';
        }

        $this->operadorSaida2 = $operadorSaida2;

        return $this;
    }

    /**
     * @return int
     */
    public function getOperadorRamal()
    {
        return $this->operadorRamal;
    }

    /**
     * @param int $operadorRamal
     * @return CrmOperador
     */
    public function setOperadorRamal($operadorRamal)
    {
        $this->operadorRamal = $operadorRamal;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'operadorId'                    => '',
            'operador'                      => '',
            'login'                         => '',
            'time'                          => '',
            'operadorDataUltimoRecebimento' => $this->getOperadorDataUltimoRecebimento(true),
            'operadorSituacao'              => $this->getOperadorSituacao(),
            'operadorPercentual'            => $this->getOperadorPercentual(),
            'operadorEntrada1'              => $this->getOperadorEntrada1(),
            'operadorSaida1'                => $this->getOperadorSaida1(),
            'operadorEntrada2'              => $this->getOperadorEntrada2(),
            'operadorSaida2'                => $this->getOperadorSaida2(),
            'operadorRamal'                 => $this->getOperadorRamal(),
        );

        if ($this->getOperador()) {
            $array['operador']   = $this->getOperador()->getId();
            $array['operadorId'] = $this->getOperador()->getId();
        }

        if ($this->getTime()) {
            $array['time']          = $this->getTime()->getTimeId();
            $array['timeDescricao'] = $this->getTime()->getTimeNome();
        }

        return $array;
    }
}
