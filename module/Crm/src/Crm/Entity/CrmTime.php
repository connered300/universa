<?php

namespace Crm\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * CrmTime
 *
 * @ORM\Table(name="crm__time")
 * @ORM\Entity
 * @LG\LG(id="timeId",label="timeNome")
 * @Jarvis\Jarvis(title="Listagem de time",icon="fa fa-table")
 */
class CrmTime
{
    /**
     * @var integer
     *
     * @ORM\Column(name="time_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="time_id")
     * @LG\Labels\Attributes(text="código time")
     * @LG\Querys\Conditions(type="=")
     */
    private $timeId;

    /**
     * @var string
     *
     * @ORM\Column(name="time_nome", type="string", nullable=true, length=70)
     * @LG\Labels\Property(name="time_nome")
     * @LG\Labels\Attributes(text="nome")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $timeNome;

    /**
     * @var string
     *
     * @ORM\Column(name="time_localizacao", type="string", nullable=true, length=200)
     * @LG\Labels\Property(name="time_localizacao")
     * @LG\Labels\Attributes(text="localização")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $timeLocalizacao;

    /**
     * @var string
     *
     * @ORM\Column(name="time_situacao", type="string", nullable=true, length=7)
     * @LG\Labels\Property(name="time_situacao")
     * @LG\Labels\Attributes(text="situação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $timeSituacao;

    /**
     * @return integer
     */
    public function getTimeId()
    {
        return $this->timeId;
    }

    /**
     * @param integer $timeId
     * @return CrmTime
     */
    public function setTimeId($timeId)
    {
        $this->timeId = $timeId;

        return $this;
    }

    /**
     * @return string
     */
    public function getTimeNome()
    {
        return $this->timeNome;
    }

    /**
     * @param string $timeNome
     * @return CrmTime
     */
    public function setTimeNome($timeNome)
    {
        $this->timeNome = $timeNome;

        return $this;
    }

    /**
     * @return string
     */
    public function getTimeLocalizacao()
    {
        return $this->timeLocalizacao;
    }

    /**
     * @param string $timeLocalizacao
     * @return CrmTime
     */
    public function setTimeLocalizacao($timeLocalizacao)
    {
        $this->timeLocalizacao = $timeLocalizacao;

        return $this;
    }

    /**
     * @return string
     */
    public function getTimeSituacao()
    {
        return $this->timeSituacao;
    }

    /**
     * @param string $timeSituacao
     * @return CrmTime
     */
    public function setTimeSituacao($timeSituacao)
    {
        $this->timeSituacao = $timeSituacao;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'timeId'          => $this->getTimeId(),
            'timeNome'        => $this->getTimeNome(),
            'timeLocalizacao' => $this->getTimeLocalizacao(),
            'timeSituacao'    => $this->getTimeSituacao(),
        );

        return $array;
    }
}
