<?php

namespace Crm\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * CrmLead
 *
 * @ORM\Table(name="crm__lead")
 * @ORM\Entity
 * @LG\LG(id="pesId",label="FonteId")
 * @Jarvis\Jarvis(title="Listagem de lead",icon="fa fa-table")
 */
class CrmLead
{
    /**
     * @var \Pessoa\Entity\PessoaFisica
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\PessoaFisica")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     */
    private $pes;

    /**
     * @var \Crm\Entity\CrmFonte
     * @ORM\ManyToOne(targetEntity="Crm\Entity\CrmFonte")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fonte_id", referencedColumnName="fonte_id")
     * })
     */
    private $fonte;

    /**
     * @var \Crm\Entity\CrmMediador
     * @ORM\ManyToOne(targetEntity="Crm\Entity\CrmMediador")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mediador_id", referencedColumnName="mediador_id")
     * })
     */
    private $mediador;

    /**
     * @var \Crm\Entity\CrmCampanha
     * @ORM\ManyToOne(targetEntity="Crm\Entity\CrmCampanha")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="campanha_id", referencedColumnName="campanha_id")
     * })
     */
    private $campanha;

    /**
     * @var \Crm\Entity\CrmOperador
     * @ORM\ManyToOne(targetEntity="Crm\Entity\CrmOperador")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="operador_id", referencedColumnName="operador_id")
     * })
     */
    private $operador;

    /**
     * @var string
     *
     * @ORM\Column(name="lead_obs", type="text", nullable=true)
     * @LG\Labels\Property(name="lead_obs")
     * @LG\Labels\Attributes(text="observação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $leadObs;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="lead_data_cadastro", type="datetime", nullable=false)
     * @LG\Labels\Property(name="lead_data_cadastro")
     * @LG\Labels\Attributes(text="cadastro data")
     * @LG\Querys\Conditions(type="=")
     */
    private $leadDataCadastro;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="lead_data_atribuicao", type="datetime", nullable=true)
     * @LG\Labels\Property(name="lead_data_atribuicao")
     * @LG\Labels\Attributes(text="Atribuição data")
     * @LG\Querys\Conditions(type="=")
     */
    private $leadDataAtribuicao;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="lead_data_alteracao", type="datetime", nullable=false)
     * @LG\Labels\Property(name="lead_data_alteracao")
     * @LG\Labels\Attributes(text="alteração data")
     * @LG\Querys\Conditions(type="=")
     */
    private $leadDataAlteracao;

    /**
     * @var string
     *
     * @ORM\Column(name="lead_status", type="string", nullable=false, length=9)
     * @LG\Labels\Property(name="lead_status")
     * @LG\Labels\Attributes(text="status")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $leadStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="lead_distribuindo", type="string", nullable=false, length=3)
     * @LG\Labels\Property(name="lead_distribuindo")
     * @LG\Labels\Attributes(text="distribuindo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $leadDistribuindo = 'Nao';

    /**
     * @return \Pessoa\Entity\PessoaFisica
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @param \Pessoa\Entity\PessoaFisica $pes
     * @return CrmLead
     */
    public function setPes($pes)
    {
        $this->pes = $pes;

        return $this;
    }

    /**
     * @return \Crm\Entity\CrmFonte
     */
    public function getFonte()
    {
        return $this->fonte;
    }

    /**
     * @param \Crm\Entity\CrmFonte $fonte
     * @return CrmLead
     */
    public function setFonte($fonte)
    {
        $this->fonte = $fonte;

        return $this;
    }

    /**
     * @return \Crm\Entity\CrmMediador
     */
    public function getMediador()
    {
        return $this->mediador;
    }

    /**
     * @param \Crm\Entity\CrmMediador $mediador
     * @return CrmLead
     */
    public function setMediador($mediador)
    {
        $this->mediador = $mediador;

        return $this;
    }

    /**
     * @return \Crm\Entity\CrmCampanha
     */
    public function getCampanha()
    {
        return $this->campanha;
    }

    /**
     * @param \Crm\Entity\CrmCampanha $campanha
     * @return CrmLead
     */
    public function setCampanha($campanha)
    {
        $this->campanha = $campanha;

        return $this;
    }

    /**
     * @return \Crm\Entity\CrmOperador
     */
    public function getOperador()
    {
        return $this->operador;
    }

    /**
     * @param \Crm\Entity\CrmOperador $operador
     * @return CrmLead
     */
    public function setOperador($operador)
    {
        $this->operador = $operador;

        return $this;
    }

    /**
     * @return string
     */
    public function getLeadObs()
    {
        return $this->leadObs;
    }

    /**
     * @param string $leadObs
     * @return CrmLead
     */
    public function setLeadObs($leadObs)
    {
        $this->leadObs = $leadObs;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getLeadDataCadastro($format = false)
    {
        $leadDataCadastro = $this->leadDataCadastro;

        if ($format && $leadDataCadastro) {
            $leadDataCadastro = $leadDataCadastro->format('d/m/Y H:i:s');
        }

        return $leadDataCadastro;
    }

    /**
     * @param \Datetime $leadDataCadastro
     * @return CrmLead
     */
    public function setLeadDataCadastro($leadDataCadastro)
    {
        if ($leadDataCadastro) {
            if (is_string($leadDataCadastro)) {
                $leadDataCadastro = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $leadDataCadastro
                );
                $leadDataCadastro = new \Datetime($leadDataCadastro);
            }
        } else {
            $leadDataCadastro = null;
        }

        $this->leadDataCadastro = $leadDataCadastro;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getLeadDataAlteracao($format = false)
    {
        $leadDataAlteracao = $this->leadDataAlteracao;

        if ($format && $leadDataAlteracao) {
            $leadDataAlteracao = $leadDataAlteracao->format('d/m/Y H:i:s');
        }

        return $leadDataAlteracao;
    }

    /**
     * @param \Datetime $leadDataAlteracao
     * @return CrmLead
     */
    public function setLeadDataAlteracao($leadDataAlteracao)
    {
        if ($leadDataAlteracao) {
            if (is_string($leadDataAlteracao)) {
                $leadDataAlteracao = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $leadDataAlteracao
                );
                $leadDataAlteracao = new \Datetime($leadDataAlteracao);
            }
        } else {
            $leadDataAlteracao = null;
        }

        $this->leadDataAlteracao = $leadDataAlteracao;

        return $this;
    }

    /**
     * @return string
     */
    public function getLeadStatus()
    {
        return $this->leadStatus;
    }

    /**
     * @param string $leadStatus
     * @return CrmLead
     */
    public function setLeadStatus($leadStatus)
    {
        $this->leadStatus = $leadStatus;

        return $this;
    }

    /**
     * @return string
     */
    public function getLeadDistribuindo()
    {
        return $this->leadDistribuindo;
    }

    /**
     * @param string $leadDistribuindo
     * @return CrmLead
     */
    public function setLeadDistribuindo($leadDistribuindo)
    {
        $this->leadDistribuindo = $leadDistribuindo;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getLeadDataAtribuicao($format = false)
    {
        $leadDataAtribuicao = $this->leadDataAtribuicao;

        if ($format && $leadDataAtribuicao) {
            $leadDataAtribuicao = $leadDataAtribuicao->format('d/m/Y H:i:s');
        }

        return $leadDataAtribuicao;
    }

    /**
     * @param \Datetime $leadDataAtribuicao
     * @return CrmLead
     */
    public function setLeadDataAtribuicao($leadDataAtribuicao)
    {
        if ($leadDataAtribuicao) {
            if (is_string($leadDataAtribuicao)) {
                $leadDataAtribuicao = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $leadDataAtribuicao
                );
                $leadDataAtribuicao = new \Datetime($leadDataAtribuicao);
            }
        } else {
            $leadDataAtribuicao = null;
        }

        $this->leadDataAtribuicao = $leadDataAtribuicao;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'pes'                => $this->getPes(),
            'fonte'              => $this->getFonte(),
            'mediador'           => $this->getMediador(),
            'campanha'           => $this->getCampanha(),
            'operador'           => $this->getOperador(),
            'leadObs'            => $this->getLeadObs(),
            'leadDataCadastro'   => $this->getLeadDataCadastro(true),
            'leadDataAlteracao'  => $this->getLeadDataAlteracao(true),
            'leadDataAtribuicao' => $this->getLeadDataAtribuicao(true),
            'leadStatus'         => $this->getLeadStatus(),
            'leadDistribuindo'   => $this->getLeadDistribuindo(),
        );

        if ($this->getPes()) {
            $array['pes']     = $this->getPes()->getPes()->getPesId();
            $array['pesId']   = $this->getPes()->getPes()->getPesId();
            $array['pesNome'] = $this->getPes()->getPes()->getPesNome();
        }

        if ($this->getFonte()) {
            $array['fonte']     = $this->getFonte()->getFonteId();
            $array['fonteId']   = $this->getFonte()->getFonteId();
            $array['fonteNome'] = $this->getFonte()->getFonteNome();
        }

        if ($this->getMediador()) {
            $array['mediador']     = $this->getMediador()->getMediadorId();
            $array['mediadorId']   = $this->getMediador()->getMediadorId();
            $array['mediadorNome'] = $this->getMediador()->getMediadorNome();
        }

        if ($this->getCampanha()) {
            $array['campanha']     = $this->getCampanha()->getCampanhaId();
            $array['campanhaId']   = $this->getCampanha()->getCampanhaId();
            $array['campanhaNome'] = $this->getCampanha()->getCampanhaNome();
        }

        if ($this->getOperador()) {
            $array['operador']     = $this->getOperador()->getOperador()->getId();
            $array['operadorId']   = $this->getOperador()->getOperador()->getId();
            $array['operadorNome'] = $this->getOperador()->getOperador()->getLogin();
        }

        return $array;
    }
}
