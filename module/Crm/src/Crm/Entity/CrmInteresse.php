<?php

namespace Crm\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * CrmInteresse
 *
 * @ORM\Table(name="crm__interesse")
 * @ORM\Entity
 * @LG\LG(id="interesseId",label="interesseDescricao")
 * @Jarvis\Jarvis(title="Listagem de interesse",icon="fa fa-table")
 */
class CrmInteresse
{
    /**
     * @var integer
     *
     * @ORM\Column(name="interesse_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="interesse_id")
     * @LG\Labels\Attributes(text="código interesse")
     * @LG\Querys\Conditions(type="=")
     */
    private $interesseId;

    /**
     * @var string
     *
     * @ORM\Column(name="interesse_descricao", type="string", nullable=false, length=200)
     * @LG\Labels\Property(name="interesse_descricao")
     * @LG\Labels\Attributes(text="descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $interesseDescricao;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="interesse_data_cadastro", type="datetime", nullable=false)
     * @LG\Labels\Property(name="interesse_data_cadastro")
     * @LG\Labels\Attributes(text="cadastro data")
     * @LG\Querys\Conditions(type="=")
     */
    private $interesseDataCadastro;

    /**
     * @return integer
     */
    public function getInteresseId()
    {
        return $this->interesseId;
    }

    /**
     * @param integer $interesseId
     * @return CrmInteresse
     */
    public function setInteresseId($interesseId)
    {
        $this->interesseId = $interesseId;

        return $this;
    }

    /**
     * @return string
     */
    public function getInteresseDescricao()
    {
        return $this->interesseDescricao;
    }

    /**
     * @param string $interesseDescricao
     * @return CrmInteresse
     */
    public function setInteresseDescricao($interesseDescricao)
    {
        $this->interesseDescricao = $interesseDescricao;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getInteresseDataCadastro($format = false)
    {
        $interesseDataCadastro = $this->interesseDataCadastro;

        if ($format && $interesseDataCadastro) {
            $interesseDataCadastro = $interesseDataCadastro->format('d/m/Y H:i:s');
        }

        return $interesseDataCadastro;
    }

    /**
     * @param \Datetime $interesseDataCadastro
     * @return CrmInteresse
     */
    public function setInteresseDataCadastro($interesseDataCadastro)
    {
        if ($interesseDataCadastro) {
            if (is_string($interesseDataCadastro)) {
                $interesseDataCadastro = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $interesseDataCadastro
                );
                $interesseDataCadastro = new \Datetime($interesseDataCadastro);
            }
        } else {
            $interesseDataCadastro = null;
        }
        $this->interesseDataCadastro = $interesseDataCadastro;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'interesseId'           => $this->getInteresseId(),
            'interesseDescricao'    => $this->getInteresseDescricao(),
            'interesseDataCadastro' => $this->getInteresseDataCadastro(true),
        );

        return $array;
    }
}
