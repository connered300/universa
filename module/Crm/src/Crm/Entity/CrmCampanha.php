<?php

namespace Crm\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * CrmCampanha
 *
 * @ORM\Table(name="crm__campanha")
 * @ORM\Entity
 * @LG\LG(id="campanhaId",label="campanhaNome")
 * @Jarvis\Jarvis(title="Listagem de campanha",icon="fa fa-table")
 */
class CrmCampanha
{
    /**
     * @var integer
     *
     * @ORM\Column(name="campanha_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="campanha_id")
     * @LG\Labels\Attributes(text="código campanha")
     * @LG\Querys\Conditions(type="=")
     */
    private $campanhaId;

    /**
     * @var string
     *
     * @ORM\Column(name="campanha_nome", type="string", nullable=true, length=70)
     * @LG\Labels\Property(name="campanha_nome")
     * @LG\Labels\Attributes(text="nome")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $campanhaNome;

    /**
     * @return integer
     */
    public function getCampanhaId()
    {
        return $this->campanhaId;
    }

    /**
     * @param integer $campanhaId
     * @return CrmCampanha
     */
    public function setCampanhaId($campanhaId)
    {
        $this->campanhaId = $campanhaId;

        return $this;
    }

    /**
     * @return string
     */
    public function getCampanhaNome()
    {
        return $this->campanhaNome;
    }

    /**
     * @param string $campanhaNome
     * @return CrmCampanha
     */
    public function setCampanhaNome($campanhaNome)
    {
        $this->campanhaNome = $campanhaNome;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'campanhaId'   => $this->getCampanhaId(),
            'campanhaNome' => $this->getCampanhaNome(),
        );

        return $array;
    }
}
