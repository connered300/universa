<?php

namespace Crm\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * CrmAtividadeTipo
 *
 * @ORM\Table(name="crm__atividade_tipo")
 * @ORM\Entity
 * @LG\LG(id="atividadeTipoId",label="AtividadeTipoNome")
 * @Jarvis\Jarvis(title="Listagem de tipo atividade",icon="fa fa-table")
 */
class CrmAtividadeTipo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="atividade_tipo_id", type="integer", nullable=false, length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="atividade_tipo_id")
     * @LG\Labels\Attributes(text="código tipo de atividade")
     * @LG\Querys\Conditions(type="=")
     */
    private $atividadeTipoId;

    /**
     * @var string
     *
     * @ORM\Column(name="atividade_tipo_nome", type="string", nullable=true, length=255)
     * @LG\Labels\Property(name="atividade_tipo_nome")
     * @LG\Labels\Attributes(text="nome")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $atividadeTipoNome;

    /**
     * @var \Crm\Entity\CrmAtividadeTipo
     * @ORM\ManyToOne(targetEntity="Crm\Entity\CrmAtividadeTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="atividade_pai", referencedColumnName="atividade_tipo_id")
     * })
     */
    private $atividadePai;

    /**
     * @var string
     *
     * @ORM\Column(name="atividade_tipo", type="string", nullable=false, length=8)
     * @LG\Labels\Property(name="atividade_tipo")
     * @LG\Labels\Attributes(text="tipo de atividade")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $atividadeTipo;

    /**
     * @var string
     *
     * @ORM\Column(name="atividade_comentario_obrigatorio", type="string", nullable=false, length=3)
     * @LG\Labels\Property(name="atividade_comentario_obrigatorio")
     * @LG\Labels\Attributes(text="obrigatorio comentario")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $atividadeComentarioObrigatorio;

    /**
     * @var string
     *
     * @ORM\Column(name="atividade_caracteriza_desistencia", type="string", nullable=false, length=3)
     * @LG\Labels\Property(name="atividade_caracteriza_desistencia")
     * @LG\Labels\Attributes(text="desistencia caracteriza")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $atividadeCaracterizaDesistencia;

    /**
     * @var string
     *
     * @ORM\Column(name="atividade_agendamento", type="string", nullable=false, length=8)
     * @LG\Labels\Property(name="atividade_agendamento")
     * @LG\Labels\Attributes(text="agendamento")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $atividadeAgendamento;

    /**
     * @var string
     *
     * @ORM\Column(name="atividade_lead_status", type="string", nullable=true, length=4)
     * @LG\Labels\Property(name="atividade_lead_status")
     * @LG\Labels\Attributes(text="status lead")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $atividadeLeadStatus;

    /**
     * @return integer
     */
    public function getAtividadeTipoId()
    {
        return $this->atividadeTipoId;
    }

    /**
     * @param integer $atividadeTipoId
     * @return CrmAtividadeTipo
     */
    public function setAtividadeTipoId($atividadeTipoId)
    {
        $this->atividadeTipoId = $atividadeTipoId;

        return $this;
    }

    /**
     * @return string
     */
    public function getAtividadeTipoNome()
    {
        return $this->atividadeTipoNome;
    }

    /**
     * @param string $atividadeTipoNome
     * @return CrmAtividadeTipo
     */
    public function setAtividadeTipoNome($atividadeTipoNome)
    {
        $this->atividadeTipoNome = $atividadeTipoNome;

        return $this;
    }

    /**
     * @return \Crm\Entity\CrmAtividadeTipo
     */
    public function getAtividadePai()
    {
        return $this->atividadePai;
    }

    /**
     * @param \Crm\Entity\CrmAtividadeTipo $atividadePai
     * @return CrmAtividadeTipo
     */
    public function setAtividadePai($atividadePai)
    {
        $this->atividadePai = $atividadePai;

        return $this;
    }

    /**
     * @return string
     */
    public function getAtividadeTipo()
    {
        return $this->atividadeTipo;
    }

    /**
     * @param string $atividadeTipo
     * @return CrmAtividadeTipo
     */
    public function setAtividadeTipo($atividadeTipo)
    {
        $this->atividadeTipo = $atividadeTipo;

        return $this;
    }

    /**
     * @return string
     */
    public function getAtividadeComentarioObrigatorio()
    {
        return $this->atividadeComentarioObrigatorio;
    }

    /**
     * @param string $atividadeComentarioObrigatorio
     * @return CrmAtividadeTipo
     */
    public function setAtividadeComentarioObrigatorio($atividadeComentarioObrigatorio)
    {
        $this->atividadeComentarioObrigatorio = $atividadeComentarioObrigatorio;

        return $this;
    }

    /**
     * @return string
     */
    public function getAtividadeCaracterizaDesistencia()
    {
        return $this->atividadeCaracterizaDesistencia;
    }

    /**
     * @param string $atividadeCaracterizaDesistencia
     * @return CrmAtividadeTipo
     */
    public function setAtividadeCaracterizaDesistencia($atividadeCaracterizaDesistencia)
    {
        $this->atividadeCaracterizaDesistencia = $atividadeCaracterizaDesistencia;

        return $this;
    }

    /**
     * @return string
     */
    public function getAtividadeAgendamento()
    {
        return $this->atividadeAgendamento;
    }

    /**
     * @param string $atividadeAgendamento
     * @return CrmAtividadeTipo
     */
    public function setAtividadeAgendamento($atividadeAgendamento)
    {
        $this->atividadeAgendamento = $atividadeAgendamento;

        return $this;
    }

    /**
     * @return string
     */
    public function getAtividadeLeadStatus()
    {
        return $this->atividadeLeadStatus;
    }

    /**
     * @param string $atividadeLeadStatus
     * @return CrmAtividadeTipo
     */
    public function setAtividadeLeadStatus($atividadeLeadStatus)
    {
        $this->atividadeLeadStatus = $atividadeLeadStatus;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'atividadeTipoId'                 => $this->getAtividadeTipoId(),
            'atividadeTipoNome'               => $this->getAtividadeTipoNome(),
            'atividadePai'                    => $this->getAtividadePai(),
            'atividadeTipo'                   => $this->getAtividadeTipo(),
            'atividadeComentarioObrigatorio'  => $this->getAtividadeComentarioObrigatorio(),
            'atividadeCaracterizaDesistencia' => $this->getAtividadeCaracterizaDesistencia(),
            'atividadeAgendamento'            => $this->getAtividadeAgendamento(),
            'atividadeLeadStatus'             => $this->getAtividadeLeadStatus(),
        );

        $array['atividadePai'] = $this->getAtividadePai() ? $this->getAtividadePai()->getAtividadeTipoId() : null;

        return $array;
    }
}
