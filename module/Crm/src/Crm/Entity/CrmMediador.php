<?php

namespace Crm\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * CrmMediador
 *
 * @ORM\Table(name="crm__mediador")
 * @ORM\Entity
 * @LG\LG(id="mediadorId",label="mediadorNome")
 * @Jarvis\Jarvis(title="Listagem de mediador",icon="fa fa-table")
 */
class CrmMediador
{
    /**
     * @var integer
     *
     * @ORM\Column(name="mediador_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="mediador_id")
     * @LG\Labels\Attributes(text="código mediador")
     * @LG\Querys\Conditions(type="=")
     */
    private $mediadorId;

    /**
     * @var string
     *
     * @ORM\Column(name="mediador_nome", type="string", nullable=true, length=70)
     * @LG\Labels\Property(name="mediador_nome")
     * @LG\Labels\Attributes(text="nome")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $mediadorNome;

    /**
     * @var string
     *
     * @ORM\Column(name="mediador_email", type="string", nullable=true)
     * @LG\Labels\Property(name="mediador_email")
     * @LG\Labels\Attributes(text="email")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $mediadorEmail;

    /**
     * @return integer
     */
    public function getMediadorId()
    {
        return $this->mediadorId;
    }

    /**
     * @param integer $mediadorId
     * @return CrmMediador
     */
    public function setMediadorId($mediadorId)
    {
        $this->mediadorId = $mediadorId;

        return $this;
    }

    /**
     * @return string
     */
    public function getMediadorNome()
    {
        return $this->mediadorNome;
    }

    /**
     * @param string $mediadorNome
     * @return CrmMediador
     */
    public function setMediadorNome($mediadorNome)
    {
        $this->mediadorNome = $mediadorNome;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return string
     */
    public function getMediadorEmail()
    {
        return $this->mediadorEmail;
    }

    /**
     * @param string $mediadorEmail
     * @return CrmMediador
     */
    public function setMediadorEmail($mediadorEmail)
    {
        $this->mediadorEmail = $mediadorEmail;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'mediadorId'    => $this->getMediadorId(),
            'mediadorNome'  => $this->getMediadorNome(),
            'mediadorEmail' => $this->getMediadorEmail(),
        );

        return $array;
    }
}
