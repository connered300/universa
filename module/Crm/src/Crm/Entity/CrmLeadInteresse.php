<?php

namespace Crm\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * CrmLeadInteresse
 *
 * @ORM\Table(name="crm__lead_interesse")
 * @ORM\Entity
 * @LG\LG(id="leadInteresseId",label="PesId")
 * @Jarvis\Jarvis(title="Listagem de lead interesse",icon="fa fa-table")
 */
class CrmLeadInteresse
{
    /**
     * @var integer
     *
     * @ORM\Column(name="lead_interesse_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="lead_interesse_id")
     * @LG\Labels\Attributes(text="código interesse lead")
     * @LG\Querys\Conditions(type="=")
     */
    private $leadInteresseId;

    /**
     * @var \Crm\Entity\CrmLead
     * @ORM\ManyToOne(targetEntity="Crm\Entity\CrmLead")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     */
    private $pes;

    /**
     * @var \Crm\Entity\CrmInteresse
     * @ORM\ManyToOne(targetEntity="Crm\Entity\CrmInteresse")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="interesse_id", referencedColumnName="interesse_id")
     * })
     */
    private $interesse;

    /**
     * @return integer
     */
    public function getLeadInteresseId()
    {
        return $this->leadInteresseId;
    }

    /**
     * @param integer $leadInteresseId
     * @return CrmLeadInteresse
     */
    public function setLeadInteresseId($leadInteresseId)
    {
        $this->leadInteresseId = $leadInteresseId;

        return $this;
    }

    /**
     * @return \Crm\Entity\CrmLead
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @param \Crm\Entity\CrmLead $pes
     * @return CrmLeadInteresse
     */
    public function setPes($pes)
    {
        $this->pes = $pes;

        return $this;
    }

    /**
     * @return \Crm\Entity\CrmInteresse
     */
    public function getInteresse()
    {
        return $this->interesse;
    }

    /**
     * @param \Crm\Entity\CrmInteresse $interesse
     * @return CrmLeadInteresse
     */
    public function setInteresse($interesse)
    {
        $this->interesse = $interesse;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'leadInteresseId'    => $this->getLeadInteresseId(),
            'pes'                => '',
            'pesId'              => '',
            'interesse'          => '',
            'interesseId'        => '',
            'interesseDescricao' => '',
        );

        if ($this->getPes()) {
            $array['pes']   = $this->getPes()->getPes()->getPes()->getPesId();
            $array['pesId'] = $array['pes'];
        }

        if ($this->getInteresse()) {
            $array['interesse']          = $this->getInteresse()->getInteresseId();
            $array['interesseId']        = $this->getInteresse()->getInteresseId();
            $array['interesseDescricao'] = $this->getInteresse()->getInteresseDescricao();
        }

        return $array;
    }
}
