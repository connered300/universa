<?php

namespace Crm\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * CrmFonte
 *
 * @ORM\Table(name="crm__fonte")
 * @ORM\Entity
 * @LG\LG(id="fonteId",label="fonteNome")
 * @Jarvis\Jarvis(title="Listagem de fonte",icon="fa fa-table")
 */
class CrmFonte
{
    /**
     * @var integer
     *
     * @ORM\Column(name="fonte_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="fonte_id")
     * @LG\Labels\Attributes(text="Código")
     * @LG\Querys\Conditions(type="=")
     */
    private $fonteId;

    /**
     * @var string
     *
     * @ORM\Column(name="fonte_nome", type="string", nullable=true, length=70)
     * @LG\Labels\Property(name="fonte_nome")
     * @LG\Labels\Attributes(text="nome")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $fonteNome;

    /**
     * @return integer
     */
    public function getFonteId()
    {
        return $this->fonteId;
    }

    /**
     * @param integer $fonteId
     * @return CrmFonte
     */
    public function setFonteId($fonteId)
    {
        $this->fonteId = $fonteId;

        return $this;
    }

    /**
     * @return string
     */
    public function getFonteNome()
    {
        return $this->fonteNome;
    }

    /**
     * @param string $fonteNome
     * @return CrmFonte
     */
    public function setFonteNome($fonteNome)
    {
        $this->fonteNome = $fonteNome;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'fonteId'   => $this->getFonteId(),
            'fonteNome' => $this->getFonteNome(),
        );

        return $array;
    }
}
