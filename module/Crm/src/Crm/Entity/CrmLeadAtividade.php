<?php

namespace Crm\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * CrmLeadAtividade
 *
 * @ORM\Table(name="crm__lead_atividade")
 * @ORM\Entity
 * @LG\LG(id="atividadeId",label="PesId")
 * @Jarvis\Jarvis(title="Listagem de atividade de lead ",icon="fa fa-table")
 */
class CrmLeadAtividade
{
    /**
     * @var integer
     *
     * @ORM\Column(name="atividade_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="atividade_id")
     * @LG\Labels\Attributes(text="código da atividade")
     * @LG\Querys\Conditions(type="=")
     */
    private $atividadeId;

    /**
     * @var \Crm\Entity\CrmLead
     * @ORM\ManyToOne(targetEntity="Crm\Entity\CrmLead")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     */
    private $pes;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="atividade_data_cadastro", type="datetime", nullable=false)
     * @LG\Labels\Property(name="atividade_data_cadastro")
     * @LG\Labels\Attributes(text="cadastro data")
     * @LG\Querys\Conditions(type="=")
     */
    private $atividadeDataCadastro;

    /**
     * @var \Crm\Entity\CrmOperador
     * @ORM\ManyToOne(targetEntity="Crm\Entity\CrmOperador")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="operador_id", referencedColumnName="operador_id")
     * })
     */
    private $operador;

    /**
     * @var \Crm\Entity\CrmAtividadeTipo
     * @ORM\ManyToOne(targetEntity="Crm\Entity\CrmAtividadeTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="atividade_tipo_id", referencedColumnName="atividade_tipo_id")
     * })
     */
    private $atividadeTipo;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="atividade_agendamento", type="datetime", nullable=true)
     * @LG\Labels\Property(name="atividade_agendamento")
     * @LG\Labels\Attributes(text="agendamento")
     * @LG\Querys\Conditions(type="=")
     */
    private $atividadeAgendamento;

    /**
     * @var string
     *
     * @ORM\Column(name="atividade_forma_contato", type="string", nullable=true, length=8)
     * @LG\Labels\Property(name="atividade_forma_contato")
     * @LG\Labels\Attributes(text="contato forma")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $atividadeFormaContato;

    /**
     * @var string
     *
     * @ORM\Column(name="atividade_forma_contato_agendamento", type="string", nullable=true, length=8)
     * @LG\Labels\Property(name="atividade_forma_contato_agendamento")
     * @LG\Labels\Attributes(text="agendamento contato forma")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $atividadeFormaContatoAgendamento;

    /**
     * @var string
     *
     * @ORM\Column(name="atividade_comentario", type="text", nullable=true)
     * @LG\Labels\Property(name="atividade_comentario")
     * @LG\Labels\Attributes(text="comentario atividate")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $atividadeComentario;

    /**
     * @var string
     *
     * @ORM\Column(name="atividade_desistencia", type="string", nullable=true, length=3)
     * @LG\Labels\Property(name="atividade_desistencia")
     * @LG\Labels\Attributes(text="desistencia")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $atividadeDesistencia;

    /**
     * @return integer
     */
    public function getAtividadeId()
    {
        return $this->atividadeId;
    }

    /**
     * @param integer $atividadeId
     * @return CrmLeadAtividade
     */
    public function setAtividadeId($atividadeId)
    {
        $this->atividadeId = $atividadeId;

        return $this;
    }

    /**
     * @return \Crm\Entity\CrmLead
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @param \Crm\Entity\CrmLead $pes
     * @return CrmLeadAtividade
     */
    public function setPes($pes)
    {
        $this->pes = $pes;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getAtividadeDataCadastro($format = false)
    {
        $atividadeDataCadastro = $this->atividadeDataCadastro;

        if ($format && $atividadeDataCadastro) {
            $atividadeDataCadastro = $atividadeDataCadastro->format('d/m/Y H:i:s');
        }

        return $atividadeDataCadastro;
    }

    /**
     * @param \Datetime $atividadeDataCadastro
     * @return CrmLeadAtividade
     */
    public function setAtividadeDataCadastro($atividadeDataCadastro)
    {
        if ($atividadeDataCadastro) {
            if (is_string($atividadeDataCadastro)) {
                $atividadeDataCadastro = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $atividadeDataCadastro
                );
                $atividadeDataCadastro = new \Datetime($atividadeDataCadastro);
            }
        } else {
            $atividadeDataCadastro = null;
        }
        $this->atividadeDataCadastro = $atividadeDataCadastro;

        return $this;
    }

    /**
     * @return \Crm\Entity\CrmOperador
     */
    public function getOperador()
    {
        return $this->operador;
    }

    /**
     * @param \Crm\Entity\CrmOperador $operador
     * @return CrmLeadAtividade
     */
    public function setOperador($operador)
    {
        $this->operador = $operador;

        return $this;
    }

    /**
     * @return \Crm\Entity\CrmAtividadeTipo
     */
    public function getAtividadeTipo()
    {
        return $this->atividadeTipo;
    }

    /**
     * @param \Crm\Entity\CrmAtividadeTipo $atividadeTipo
     * @return CrmLeadAtividade
     */
    public function setAtividadeTipo($atividadeTipo)
    {
        $this->atividadeTipo = $atividadeTipo;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getAtividadeAgendamento($format = false)
    {
        $atividadeAgendamento = $this->atividadeAgendamento;

        if ($format && $atividadeAgendamento) {
            $atividadeAgendamento = $atividadeAgendamento->format('d/m/Y H:i:s');
        }

        return $atividadeAgendamento;
    }

    /**
     * @param \Datetime $atividadeAgendamento
     * @return CrmLeadAtividade
     */
    public function setAtividadeAgendamento($atividadeAgendamento)
    {
        if ($atividadeAgendamento) {
            if (is_string($atividadeAgendamento)) {
                $atividadeAgendamento = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $atividadeAgendamento
                );
                $atividadeAgendamento = new \Datetime($atividadeAgendamento);
            }
        } else {
            $atividadeAgendamento = null;
        }
        $this->atividadeAgendamento = $atividadeAgendamento;

        return $this;
    }

    /**
     * @return string
     */
    public function getAtividadeFormaContato()
    {
        return $this->atividadeFormaContato;
    }

    /**
     * @param string $atividadeFormaContato
     * @return CrmLeadAtividade
     */
    public function setAtividadeFormaContato($atividadeFormaContato)
    {
        $this->atividadeFormaContato = $atividadeFormaContato;

        return $this;
    }

    /**
     * @return string
     */
    public function getAtividadeFormaContatoAgendamento()
    {
        return $this->atividadeFormaContatoAgendamento;
    }

    /**
     * @param string $atividadeFormaContatoAgendamento
     * @return CrmLeadAtividade
     */
    public function setAtividadeFormaContatoAgendamento($atividadeFormaContatoAgendamento)
    {
        $this->atividadeFormaContatoAgendamento = $atividadeFormaContatoAgendamento;

        return $this;
    }

    /**
     * @return string
     */
    public function getAtividadeComentario()
    {
        return $this->atividadeComentario;
    }

    /**
     * @param string $atividadeComentario
     * @return CrmLeadAtividade
     */
    public function setAtividadeComentario($atividadeComentario)
    {
        $this->atividadeComentario = $atividadeComentario;

        return $this;
    }

    /**
     * @return string
     */
    public function getAtividadeDesistencia()
    {
        return $this->atividadeDesistencia;
    }

    /**
     * @param string $atividadeDesistencia
     * @return CrmLeadAtividade
     */
    public function setAtividadeDesistencia($atividadeDesistencia)
    {
        $this->atividadeDesistencia = $atividadeDesistencia;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'atividadeId'                      => $this->getAtividadeId(),
            'pes'                              => $this->getPes(),
            'atividadeDataCadastro'            => $this->getAtividadeDataCadastro(true),
            'operador'                         => $this->getOperador(),
            'atividadeTipo'                   => $this->getAtividadeTipo(),
            'atividadeAgendamento'             => $this->getAtividadeAgendamento(true),
            'atividadeFormaContato'            => $this->getAtividadeFormaContato(),
            'atividadeFormaContatoAgendamento' => $this->getAtividadeFormaContatoAgendamento(),
            'atividadeComentario'              => $this->getAtividadeComentario(),
            'atividadeDesistencia'             => $this->getAtividadeDesistencia(),
        );

        $array['pes']            = $this->getPes() ? $this->getPes()->getPes()->getPes()->getPesId() : null;
        $array['operador']       = $this->getOperador() ? $this->getOperador()->getOperador()->getId() : null;
        $array['atividadeTipo'] = $this->getAtividadeTipo() ? $this->getAtividadeTipo()->getAtividadeTipoId(
        ) : null;

        return $array;
    }
}
