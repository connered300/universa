<?php

namespace Crm\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * CrmLeadEvento
 *
 * @ORM\Table(name="crm__lead_evento")
 * @ORM\Entity
 * @LG\LG(id="eventoId",label="EventoData")
 * @Jarvis\Jarvis(title="Listagem de lead evento",icon="fa fa-table")
 */
class CrmLeadEvento
{
    /**
     * @var integer
     *
     * @ORM\Column(name="evento_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="evento_id")
     * @LG\Labels\Attributes(text="código")
     * @LG\Querys\Conditions(type="=")
     */
    private $eventoId;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="evento_data", type="datetime", nullable=false)
     * @LG\Labels\Property(name="evento_data")
     * @LG\Labels\Attributes(text="data")
     * @LG\Querys\Conditions(type="=")
     */
    private $eventoData;

    /**
     * @var \Crm\Entity\CrmLead
     * @ORM\ManyToOne(targetEntity="Crm\Entity\CrmLead")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     */
    private $pes;

    /**
     * @var \Crm\Entity\CrmMediador
     * @ORM\ManyToOne(targetEntity="Crm\Entity\CrmMediador")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mediador_id", referencedColumnName="mediador_id")
     * })
     */
    private $mediador;

    /**
     * @var \Crm\Entity\CrmCampanha
     * @ORM\ManyToOne(targetEntity="Crm\Entity\CrmCampanha")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="campanha_id", referencedColumnName="campanha_id")
     * })
     */
    private $campanha;

    /**
     * @var \Crm\Entity\CrmFonte
     * @ORM\ManyToOne(targetEntity="Crm\Entity\CrmFonte")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fonte_id", referencedColumnName="fonte_id")
     * })
     */
    private $fonte;

    /**
     * @var string
     *
     * @ORM\Column(name="evento_outra_informacao", type="text", nullable=true)
     * @LG\Labels\Property(name="evento_outra_informacao")
     * @LG\Labels\Attributes(text="informação outra")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $eventoOutraInformacao;

    /**
     * @return integer
     */
    public function getEventoId()
    {
        return $this->eventoId;
    }

    /**
     * @param integer $eventoId
     * @return CrmLeadEvento
     */
    public function setEventoId($eventoId)
    {
        $this->eventoId = $eventoId;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getEventoData($format = false)
    {
        $eventoData = $this->eventoData;

        if ($format && $eventoData) {
            $eventoData = $eventoData->format('d/m/Y H:i:s');
        }

        return $eventoData;
    }

    /**
     * @param \Datetime $eventoData
     * @return CrmLeadEvento
     */
    public function setEventoData($eventoData)
    {
        if ($eventoData) {
            if (is_string($eventoData)) {
                $eventoData = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $eventoData
                );
                $eventoData = new \Datetime($eventoData);
            }
        } else {
            $eventoData = null;
        }
        $this->eventoData = $eventoData;

        return $this;
    }

    /**
     * @return \Crm\Entity\CrmLead
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @param \Crm\Entity\CrmLead $pes
     * @return CrmLeadEvento
     */
    public function setPes($pes)
    {
        $this->pes = $pes;

        return $this;
    }

    /**
     * @return \Crm\Entity\CrmMediador
     */
    public function getMediador()
    {
        return $this->mediador;
    }

    /**
     * @param \Crm\Entity\CrmMediador $mediador
     * @return CrmLeadEvento
     */
    public function setMediador($mediador)
    {
        $this->mediador = $mediador;

        return $this;
    }

    /**
     * @return \Crm\Entity\CrmCampanha
     */
    public function getCampanha()
    {
        return $this->campanha;
    }

    /**
     * @param \Crm\Entity\CrmCampanha $campanha
     * @return CrmLeadEvento
     */
    public function setCampanha($campanha)
    {
        $this->campanha = $campanha;

        return $this;
    }

    /**
     * @return \Crm\Entity\CrmFonte
     */
    public function getFonte()
    {
        return $this->fonte;
    }

    /**
     * @param \Crm\Entity\CrmFonte $fonte
     * @return CrmLeadEvento
     */
    public function setFonte($fonte)
    {
        $this->fonte = $fonte;

        return $this;
    }

    /**
     * @return string
     */
    public function getEventoOutraInformacao()
    {
        return $this->eventoOutraInformacao;
    }

    /**
     * @param string $eventoOutraInformacao
     * @return CrmLeadEvento
     */
    public function setEventoOutraInformacao($eventoOutraInformacao)
    {
        $this->eventoOutraInformacao = $eventoOutraInformacao;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'eventoId'              => $this->getEventoId(),
            'eventoData'            => $this->getEventoData(true),
            'pes'                   => $this->getPes(),
            'mediador'              => $this->getMediador(),
            'campanha'              => $this->getCampanha(),
            'fonte'                 => $this->getFonte(),
            'eventoOutraInformacao' => $this->getEventoOutraInformacao(),
        );

        $array['pes']      = $this->getPes() ? $this->getPes()->getPesId() : null;
        $array['mediador'] = $this->getMediador() ? $this->getMediador()->getMediadorId() : null;
        $array['campanha'] = $this->getCampanha() ? $this->getCampanha()->getCampanhaId() : null;
        $array['fonte']    = $this->getFonte() ? $this->getFonte()->getFonteId() : null;

        return $array;
    }
}
