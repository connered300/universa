<?php

namespace Financeiro;

return array(
    'router'                    => array(
        'routes' => array(
            'financeiro' => array(
                'type'          => 'Literal',
                'options'       => array(
                    'route'    => '/financeiro',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Financeiro\Controller',
                        'controller'    => 'Painel',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes'  => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'       => '/[:controller[/:action[/:id]]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id'         => '[a-zA-Z0-9_,-]+',
                            ),
                            'defaults'    => array(
                                'action' => 'index'
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'controllers'               => array(
        'invokables' => array(
            'Financeiro\Controller\Painel'                         => 'Financeiro\Controller\PainelController',
            'Financeiro\Controller\BoletoBanco'                    => 'Financeiro\Controller\BoletoBancoController',
            'Financeiro\Controller\BoletoConfConta'                => 'Financeiro\Controller\BoletoConfContaController',
            'Financeiro\Controller\Boleto'                         => 'Financeiro\Controller\BoletoController',
            'Financeiro\Controller\BoletoLayout'                   => 'Financeiro\Controller\BoletoLayoutController',
            'Financeiro\Controller\FinanceiroAlunoConfigPgtoCurso' => 'Financeiro\Controller\FinanceiroAlunoConfigPgtoCursoController',
            'Financeiro\Controller\FinanceiroCheque'               => 'Financeiro\Controller\FinanceiroChequeController',
            'Financeiro\Controller\FinanceiroDesconto'             => 'Financeiro\Controller\FinanceiroDescontoController',
            'Financeiro\Controller\FinanceiroDescontoTitulo'       => 'Financeiro\Controller\FinanceiroDescontoTituloController',
            'Financeiro\Controller\FinanceiroDescontoTipo'         => 'Financeiro\Controller\FinanceiroDescontoTipoController',
            'Financeiro\Controller\FinanceiroMeioPagamento'        => 'Financeiro\Controller\FinanceiroMeioPagamentoController',
            'Financeiro\Controller\FinanceiroMensalidadeDesconto'  => 'Financeiro\Controller\FinanceiroMensalidadeDescontoController',
            'Financeiro\Controller\FinanceiroPagamento'            => 'Financeiro\Controller\FinanceiroPagamentoController',
            'Financeiro\Controller\FinanceiroRemessa'              => 'Financeiro\Controller\FinanceiroRemessaController',
            'Financeiro\Controller\FinanceiroRetorno'              => 'Financeiro\Controller\FinanceiroRetornoController',
            //            'Financeiro\Controller\FinanceiroRemessaLote'          => 'Financeiro\Controller\FinanceiroRemessaLoteController',
            //            'Financeiro\Controller\FinanceiroRemessaLoteRegistro'  => 'Financeiro\Controller\FinanceiroRemessaLoteRegistroController',
            'Financeiro\Controller\FinanceiroFiltro'               => 'Financeiro\Controller\FinanceiroFiltroController',
            'Financeiro\Controller\FinanceiroTituloCheque'         => 'Financeiro\Controller\FinanceiroTituloChequeController',
            'Financeiro\Controller\FinanceiroTituloConfig'         => 'Financeiro\Controller\FinanceiroTituloConfigController',
            'Financeiro\Controller\FinanceiroTitulo'               => 'Financeiro\Controller\FinanceiroTituloController',
            'Financeiro\Controller\FinanceiroTituloMensalidade'    => 'Financeiro\Controller\FinanceiroTituloMensalidadeController',
            'Financeiro\Controller\FinanceiroTituloTipo'           => 'Financeiro\Controller\FinanceiroTituloTipoController',
            'Financeiro\Controller\FinanceiroValores'              => 'Financeiro\Controller\FinanceiroValoresController',
            'Financeiro\Controller\Relatorio'                      => 'Financeiro\Controller\RelatorioController',
            'Financeiro\Controller\FinanceiroPessoa'               => 'Financeiro\Controller\FinanceiroPessoaController',
        ),
    ),
    'service_manager'           => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases'            => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'doctrine'                  => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default'             => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            ),
        ),
    ),
    'view_manager'              => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'console'                   => array(
        'router' => array(
            'routes' => array(),
        ),
    ),
    'funcionalidades_liberadas' => array(
        'controllers' => array(
            'Financeiro\Controller\Index'
        ),
        'actions'     => array(
            'pagseguro-cobranca',
            'pagseguro-notificacao',
            'pagseguro-retorno',
            'executar',
            'titulos',
            'processa-arquivo',
            'processa-detalhes',
            'valida-cartao',
            'pagamento-cartao-recorrencia'
        ),
    ),
    'namesDictionary'           => array(
        'Financeiro\Painel'                                            => 'Painel',
        'Financeiro\Painel::index'                                     => 'Painel Financeiro',
        'Financeiro\FinanceiroTitulo::pagseguro-cobranca'              => 'Efetuar pagamento com Pagseguro',
        'Financeiro\FinanceiroTitulo::pagseguro-retorno'               => 'Obrigado!',
        'Financeiro\BoletoBanco'                                       => 'Bancos',
        'Financeiro\BoletoConfConta'                                   => 'Conta Bancária',
        'Financeiro\Boleto'                                            => 'Boleto',
        //'Financeiro\BoletoHistorico'                => 'BoletoHistorico',
        'Financeiro\FinanceiroRetorno'                                 => 'Retorno',
        'Financeiro\BoletoLayout'                                      => 'BoletoLayout',
        'Financeiro\FinanceiroAlunoConfigPgtoCurso'                    => 'FinanceiroAlunoConfigPgtoCurso',
        'Financeiro\FinanceiroCheque'                                  => 'Cheque',
        'Financeiro\FinanceiroDesconto'                                => 'Desconto',
        'Financeiro\FinanceiroDescontoTitulo'                          => 'Descontos de Título',
        'Financeiro\FinanceiroDescontoTipo'                            => 'Tipo de Desconto',
        'Financeiro\FinanceiroMeioPagamento'                           => 'Meio de Pagamento',
        'Financeiro\FinanceiroMensalidadeDesconto'                     => 'FinanceiroMensalidadeDesconto',
        'Financeiro\FinanceiroPagamento'                               => 'Pagamento',
        'Financeiro\FinanceiroRemessa'                                 => 'Remessa',
        //'Financeiro\FinanceiroRemessaLote'          => 'FinanceiroRemessaLote',
        //'Financeiro\FinanceiroRemessaLoteRegistro'  => 'FinanceiroRemessaLoteRegistro',
        //'Financeiro\FinanceiroTituloAlteracao'      => 'FinanceiroTituloAlteracao',
        'Financeiro\FinanceiroTituloCheque'                            => 'Cheque',
        'Financeiro\FinanceiroTituloConfig'                            => 'Configuração de título',
        'Financeiro\FinanceiroTitulo'                                  => 'Título',
        'Financeiro\FinanceiroTituloMensalidade'                       => 'Mensalidade',
        'Financeiro\FinanceiroTituloTipo'                              => 'Tipo de Título',
        'Financeiro\FinanceiroValores'                                 => 'Valores',
        'Financeiro\FinanceiroFiltro'                                  => 'Filtros',
        'Financeiro\Relatorio'                                         => 'Relatório',
        'Financeiro\Relatorio::sintetico'                              => 'Relação Financeiro Sintético',
        'Financeiro\Relatorio::boletos-aluno'                          => 'Impressão de Boletos',
        'Financeiro\Painel::titulos'                                   => '',//Consulta de Títulos Financeiros
        'Financeiro\FinanceiroPessoa'                                  => 'Dados Financeiros',
        'Financeiro\FinanceiroPagamento::pagamento-cartao-recorrencia' => 'Pagamento de Títulos Com Cartão'
    )
);
