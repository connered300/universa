<?php

namespace Financeiro\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * FinanceiroCheque
 *
 * @ORM\Table(name="financeiro__cheque")
 * @ORM\Entity
 * @LG\LG(id="chequeId",label="UsuarioId")
 * @Jarvis\Jarvis(title="Listagem de cheque",icon="fa fa-table")
 */
class FinanceiroCheque
{
    /**
     * @var integer
     *
     * @ORM\Column(name="cheque_id", type="integer", nullable=false, length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="cheque_id")
     * @LG\Labels\Attributes(text="código cheque")
     * @LG\Querys\Conditions(type="=")
     */
    private $chequeId;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * })
     */
    private $usuario;

    /**
     * @var \Pessoa\Entity\Pessoa
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     */
    private $pes;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="cheque_data_cadastro", type="datetime", nullable=true)
     * @LG\Labels\Property(name="cheque_data_cadastro")
     * @LG\Labels\Attributes(text="cadastro data")
     * @LG\Querys\Conditions(type="=")
     */
    private $chequeDataCadastro;

    /**
     * @var string
     *
     * @ORM\Column(name="cheque_emitente", type="string", nullable=false, length=255)
     * @LG\Labels\Property(name="cheque_emitente")
     * @LG\Labels\Attributes(text="emitente")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $chequeEmitente;

    /**
     * @var string
     *
     * @ORM\Column(name="cheque_num", type="string", nullable=false, length=45)
     * @LG\Labels\Property(name="cheque_num")
     * @LG\Labels\Attributes(text="número")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $chequeNum;

    /**
     * @var string
     *
     * @ORM\Column(name="cheque_banco", type="string", nullable=false, length=45)
     * @LG\Labels\Property(name="cheque_banco")
     * @LG\Labels\Attributes(text="banco")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $chequeBanco;

    /**
     * @var string
     *
     * @ORM\Column(name="cheque_agencia", type="string", nullable=false, length=45)
     * @LG\Labels\Property(name="cheque_agencia")
     * @LG\Labels\Attributes(text="agência")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $chequeAgencia;

    /**
     * @var string
     *
     * @ORM\Column(name="cheque_praca", type="string", nullable=false, length=45)
     * @LG\Labels\Property(name="cheque_praca")
     * @LG\Labels\Attributes(text="praça")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $chequePraca;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="cheque_emissao", type="datetime", nullable=false)
     * @LG\Labels\Property(name="cheque_emissao")
     * @LG\Labels\Attributes(text="emissão")
     * @LG\Querys\Conditions(type="=")
     */
    private $chequeEmissao;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="cheque_vencimento", type="datetime", nullable=false)
     * @LG\Labels\Property(name="cheque_vencimento")
     * @LG\Labels\Attributes(text="vencimento")
     * @LG\Querys\Conditions(type="=")
     */
    private $chequeVencimento;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="cheque_compensado", type="datetime", nullable=true)
     * @LG\Labels\Property(name="cheque_compensado")
     * @LG\Labels\Attributes(text="compensado")
     * @LG\Querys\Conditions(type="=")
     */
    private $chequeCompensado;

    /**
     * @var float
     *
     * @ORM\Column(name="cheque_valor", type="float", nullable=false, length=10)
     * @LG\Labels\Property(name="cheque_valor")
     * @LG\Labels\Attributes(text="valor")
     * @LG\Querys\Conditions(type="=")
     */
    private $chequeValor;

    /**
     * @var string
     *
     * @ORM\Column(name="cheque_conta", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="cheque_conta")
     * @LG\Labels\Attributes(text="conta")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $chequeConta;

    /**
     * @var string
     *
     * @ORM\Column(name="cheque_observacao", type="string", nullable=true)
     * @LG\Labels\Property(name="cheque_observacao")
     * @LG\Labels\Attributes(text="observação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $chequeObservacao;

    /**
     * @return integer
     */
    public function getChequeId()
    {
        return $this->chequeId;
    }

    /**
     * @param integer $chequeId
     * @return FinanceiroCheque
     */
    public function setChequeId($chequeId)
    {
        $this->chequeId = $chequeId;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuario
     * @return FinanceiroCheque
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getChequeDataCadastro($format = false)
    {
        $chequeDataCadastro = $this->chequeDataCadastro;

        if ($format && $chequeDataCadastro) {
            $chequeDataCadastro = $chequeDataCadastro->format('d/m/Y H:i:s');
        }

        return $chequeDataCadastro;
    }

    /**
     * @param \Datetime $chequeDataCadastro
     * @return FinanceiroCheque
     */
    public function setChequeDataCadastro($chequeDataCadastro)
    {
        if ($chequeDataCadastro) {
            if (is_string($chequeDataCadastro)) {
                $chequeDataCadastro = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $chequeDataCadastro
                );
                $chequeDataCadastro = new \Datetime($chequeDataCadastro);
            }
        } else {
            $chequeDataCadastro = null;
        }

        $this->chequeDataCadastro = $chequeDataCadastro;

        return $this;
    }

    /**
     * @return string
     */
    public function getChequeEmitente()
    {
        return $this->chequeEmitente;
    }

    /**
     * @param string $chequeEmitente
     * @return FinanceiroCheque
     */
    public function setChequeEmitente($chequeEmitente)
    {
        $this->chequeEmitente = $chequeEmitente;

        return $this;
    }

    /**
     * @return string
     */
    public function getChequeNum()
    {
        return $this->chequeNum;
    }

    /**
     * @param string $chequeNum
     * @return FinanceiroCheque
     */
    public function setChequeNum($chequeNum)
    {
        $this->chequeNum = $chequeNum;

        return $this;
    }

    /**
     * @return string
     */
    public function getChequeBanco()
    {
        return $this->chequeBanco;
    }

    /**
     * @param string $chequeBanco
     * @return FinanceiroCheque
     */
    public function setChequeBanco($chequeBanco)
    {
        $this->chequeBanco = $chequeBanco;

        return $this;
    }

    /**
     * @return string
     */
    public function getChequeAgencia()
    {
        return $this->chequeAgencia;
    }

    /**
     * @param string $chequeAgencia
     * @return FinanceiroCheque
     */
    public function setChequeAgencia($chequeAgencia)
    {
        $this->chequeAgencia = $chequeAgencia;

        return $this;
    }

    /**
     * @return string
     */
    public function getChequePraca()
    {
        return $this->chequePraca;
    }

    /**
     * @param string $chequePraca
     * @return FinanceiroCheque
     */
    public function setChequePraca($chequePraca)
    {
        $this->chequePraca = $chequePraca;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getChequeEmissao($format = false)
    {
        $chequeEmissao = $this->chequeEmissao;

        if ($format && $chequeEmissao) {
            $chequeEmissao = $chequeEmissao->format('d/m/Y H:i:s');
        }

        return $chequeEmissao;
    }

    /**
     * @param \Datetime $chequeEmissao
     * @return FinanceiroCheque
     */
    public function setChequeEmissao($chequeEmissao)
    {
        if ($chequeEmissao) {
            if (is_string($chequeEmissao)) {
                $chequeEmissao = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $chequeEmissao
                );
                $chequeEmissao = new \Datetime($chequeEmissao);
            }
        } else {
            $chequeEmissao = null;
        }

        $this->chequeEmissao = $chequeEmissao;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getChequeVencimento($format = false)
    {
        $chequeVencimento = $this->chequeVencimento;

        if ($format && $chequeVencimento) {
            $chequeVencimento = $chequeVencimento->format('d/m/Y H:i:s');
        }

        return $chequeVencimento;
    }

    /**
     * @param \Datetime $chequeVencimento
     * @return FinanceiroCheque
     */
    public function setChequeVencimento($chequeVencimento)
    {
        if ($chequeVencimento) {
            if (is_string($chequeVencimento)) {
                $chequeVencimento = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $chequeVencimento
                );
                $chequeVencimento = new \Datetime($chequeVencimento);
            }
        } else {
            $chequeVencimento = null;
        }

        $this->chequeVencimento = $chequeVencimento;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getChequeCompensado($format = false)
    {
        $chequeCompensado = $this->chequeCompensado;

        if ($format && $chequeCompensado) {
            $chequeCompensado = $chequeCompensado->format('d/m/Y H:i:s');
        }

        return $chequeCompensado;
    }

    /**
     * @param \Datetime $chequeCompensado
     * @return FinanceiroCheque
     */
    public function setChequeCompensado($chequeCompensado)
    {
        if ($chequeCompensado) {
            if (is_string($chequeCompensado)) {
                $chequeCompensado = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $chequeCompensado
                );
                $chequeCompensado = new \Datetime($chequeCompensado);
            }
        } else {
            $chequeCompensado = null;
        }

        $this->chequeCompensado = $chequeCompensado;

        return $this;
    }

    /**
     * @return float
     */
    public function getChequeValor()
    {
        return $this->chequeValor;
    }

    /**
     * @param float $chequeValor
     * @return FinanceiroCheque
     */
    public function setChequeValor($chequeValor)
    {
        $chequeValor = \VersaSpine\Service\AbstractService::convertNumberFromBRToUS($chequeValor);

        $this->chequeValor = $chequeValor;

        return $this;
    }

    /**
     * @return string
     */
    public function getChequeConta()
    {
        return $this->chequeConta;
    }

    /**
     * @param string $chequeConta
     * @return FinanceiroCheque
     */
    public function setChequeConta($chequeConta)
    {
        $this->chequeConta = $chequeConta;

        return $this;
    }

    /**
     * @return \Pessoa\Entity\Pessoa
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @param \Pessoa\Entity\Pessoa $pes
     * @return FinanceiroCheque
     */
    public function setPes($pes)
    {
        $this->pes = $pes;

        return $this;
    }

    /**
     * @return string
     */
    public function getChequeObservacao()
    {
        return $this->chequeObservacao;
    }

    /**
     * @param string $chequeObservacao
     * @return FinanceiroCheque
     */
    public function setChequeObservacao($chequeObservacao)
    {
        $this->chequeObservacao = $chequeObservacao;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'chequeId'           => $this->getChequeId(),
            'usuario'            => $this->getUsuario(),
            'pes'                => $this->getPes(),
            'chequeDataCadastro' => $this->getChequeDataCadastro(true),
            'chequeEmitente'     => $this->getChequeEmitente(),
            'chequeNum'          => $this->getChequeNum(),
            'chequeBanco'        => $this->getChequeBanco(),
            'chequeAgencia'      => $this->getChequeAgencia(),
            'chequePraca'        => $this->getChequePraca(),
            'chequeEmissao'      => $this->getChequeEmissao(true),
            'chequeVencimento'   => $this->getChequeVencimento(true),
            'chequeCompensado'   => $this->getChequeCompensado(true),
            'chequeValor'        => $this->getChequeValor(),
            'chequeConta'        => $this->getChequeConta(),
            'chequeObservacao'   => $this->getChequeObservacao(),
        );

        $array['usuario'] = $this->getUsuario() ? $this->getUsuario()->getId() : null;
        $array['pes']     = $this->getPes() ? $this->getPes()->getPesId() : null;

        return $array;
    }
}
