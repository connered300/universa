<?php

namespace Financeiro\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * FinanceiroFiltro
 *
 * @ORM\Table(name="financeiro__filtro")
 * @ORM\Entity
 * @LG\LG(id="filtroId",label="FiltroNome")
 * @Jarvis\Jarvis(title="Listagem de filtro",icon="fa fa-table")
 */
class FinanceiroFiltro
{
    /**
     * @var integer
     *
     * @ORM\Column(name="filtro_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="filtro_id")
     * @LG\Labels\Attributes(text="código filtro")
     * @LG\Querys\Conditions(type="=")
     */
    private $filtroId;

    /**
     * @var string
     *
     * @ORM\Column(name="filtro_nome", type="string", nullable=false, length=60)
     * @LG\Labels\Property(name="filtro_nome")
     * @LG\Labels\Attributes(text="nome")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $filtroNome;

    /**
     * @var string
     *
     * @ORM\Column(name="filtro_estado", type="string", nullable=true, length=29)
     * @LG\Labels\Property(name="filtro_estado")
     * @LG\Labels\Attributes(text="estado")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $filtroEstado;

    /**
     * @var string
     *
     * @ORM\Column(name="filtro_situacao", type="string", nullable=false, length=7)
     * @LG\Labels\Property(name="filtro_situacao")
     * @LG\Labels\Attributes(text="situação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $filtroSituacao;

    /**
     * @return integer
     */
    public function getFiltroId()
    {
        return $this->filtroId;
    }

    /**
     * @param integer $filtroId
     * @return FinanceiroFiltro
     */
    public function setFiltroId($filtroId)
    {
        $this->filtroId = $filtroId;

        return $this;
    }

    /**
     * @return string
     */
    public function getFiltroNome()
    {
        return $this->filtroNome;
    }

    /**
     * @param string $filtroNome
     * @return FinanceiroFiltro
     */
    public function setFiltroNome($filtroNome)
    {
        $this->filtroNome = $filtroNome;

        return $this;
    }

    /**
     * @return string
     */
    public function getFiltroEstado()
    {
        return explode(',', $this->filtroEstado);
    }

    /**
     * @param string $filtroEstado
     * @return FinanceiroFiltro
     */
    public function setFiltroEstado($filtroEstado)
    {
        if (is_array($filtroEstado)) {
            $filtroEstado = implode(',', $filtroEstado);
        }

        $this->filtroEstado = $filtroEstado;

        return $this;
    }

    /**
     * @return string
     */
    public function getFiltroSituacao()
    {
        return $this->filtroSituacao;
    }

    /**
     * @param string $filtroSituacao
     * @return FinanceiroFiltro
     */
    public function setFiltroSituacao($filtroSituacao)
    {
        $this->filtroSituacao = $filtroSituacao;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'filtroId'       => $this->getFiltroId(),
            'filtroNome'     => $this->getFiltroNome(),
            'filtroEstado'   => $this->getFiltroEstado(),
            'filtroSituacao' => $this->getFiltroSituacao(),
        );

        return $array;
    }
}
