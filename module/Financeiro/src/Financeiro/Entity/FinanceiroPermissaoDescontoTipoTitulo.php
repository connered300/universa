<?php

namespace Financeiro\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * FinanceiroPermissaoDescontoTipoTitulo
 *
 * @ORM\Table(name="financeiro__permissao_desconto_tipo_titulo")
 * @ORM\Entity
 * @LG\LG(id="desctipoId",label="DesctipoId")
 * @Jarvis\Jarvis(title="Listagem de permissão desconto tipo título",icon="fa fa-table")
 */
class FinanceiroPermissaoDescontoTipoTitulo
{
    /**
     * @var \Financeiro\Entity\FinanceiroTituloTipo
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\FinanceiroTituloTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipotitulo_id", referencedColumnName="tipotitulo_id")
     * })
     */
    private $tipotitulo;
    /**
     * @var \Financeiro\Entity\FinanceiroDescontoTipo
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\FinanceiroDescontoTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="desctipo_id", referencedColumnName="desctipo_id")
     * })
     */
    private $desctipo;

    /**
     * @return \Financeiro\Entity\FinanceiroTituloTipo
     */
    public function getTipotitulo()
    {
        return $this->tipotitulo;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroTituloTipo $tipotitulo
     * @return FinanceiroPermissaoDescontoTipoTitulo
     */
    public function setTipotitulo($tipotitulo)
    {
        $this->tipotitulo = $tipotitulo;

        return $this;
    }

    /**
     * @return \Financeiro\Entity\FinanceiroDescontoTipo
     */
    public function getDesctipo()
    {
        return $this->desctipo;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroDescontoTipo $desctipo
     * @return FinanceiroPermissaoDescontoTipoTitulo
     */
    public function setDesctipo($desctipo)
    {
        $this->desctipo = $desctipo;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'tipotitulo' => $this->getTipotitulo(),
            'desctipo'   => $this->getDesctipo(),
        );

        $array['tipotitulo'] = $this->getTipotitulo() ? $this->getTipotitulo()->getTipotituloId() : null;
        $array['desctipo']   = $this->getDesctipo() ? $this->getDesctipo()->getDesctipoId() : null;

        return $array;
    }
}
