<?php

namespace Financeiro\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * BoletoBanco
 *
 * @ORM\Table(name="boleto_banco")
 * @ORM\Entity
 * @LG\LG(id="bancId",label="BancNome")
 * @Jarvis\Jarvis(title="Listagem de banco",icon="fa fa-table")
 */
class BoletoBanco
{
    /**
     * @var integer
     *
     * @ORM\Column(name="banc_id", type="integer", nullable=false, length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="banc_id")
     * @LG\Labels\Attributes(text="código banco")
     * @LG\Querys\Conditions(type="=")
     */
    private $bancId;
    /**
     * @var integer
     *
     * @ORM\Column(name="banc_codigo", type="integer", nullable=false, length=11)
     * @LG\Labels\Property(name="banc_codigo")
     * @LG\Labels\Attributes(text="codigo")
     * @LG\Querys\Conditions(type="=")
     */
    private $bancCodigo;
    /**
     * @var string
     *
     * @ORM\Column(name="banc_nome", type="string", nullable=false, length=128)
     * @LG\Labels\Property(name="banc_nome")
     * @LG\Labels\Attributes(text="nome")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $bancNome;
    /**
     * @var \Financeiro\Entity\BoletoLayout
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\BoletoLayout")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="layout_id", referencedColumnName="layout_id")
     * })
     */
    private $layout;

    /**
     * @return integer
     */
    public function getBancId()
    {
        return $this->bancId;
    }

    /**
     * @param integer $bancId
     * @return BoletoBanco
     */
    public function setBancId($bancId)
    {
        $this->bancId = $bancId;

        return $this;
    }

    /**
     * @return integer
     */
    public function getBancCodigo()
    {
        return $this->bancCodigo;
    }

    /**
     * @param integer $bancCodigo
     * @return BoletoBanco
     */
    public function setBancCodigo($bancCodigo)
    {
        $this->bancCodigo = $bancCodigo;

        return $this;
    }

    /**
     * @return string
     */
    public function getBancNome()
    {
        return $this->bancNome;
    }

    /**
     * @param string $bancNome
     * @return BoletoBanco
     */
    public function setBancNome($bancNome)
    {
        $this->bancNome = $bancNome;

        return $this;
    }

    /**
     * @return \Financeiro\Entity\BoletoLayout
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * @param \Financeiro\Entity\BoletoLayout $layout
     * @return BoletoBanco
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'bancId'     => $this->getBancId(),
            'bancCodigo' => $this->getBancCodigo(),
            'bancNome'   => $this->getBancNome(),
            'layout'     => $this->getLayout(),
        );

        $array['layout'] = $this->getLayout() ? $this->getLayout()->getLayoutId() : null;

        return $array;
    }
}
