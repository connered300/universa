<?php

namespace Financeiro\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * FinanceiroDescontoTipo
 *
 * @ORM\Table(name="financeiro__desconto_tipo")
 * @ORM\Entity
 * @LG\LG(id="desctipoId",label="DesctipoDescricao")
 * @Jarvis\Jarvis(title="Listagem de desconto tipo",icon="fa fa-table")
 */
class FinanceiroDescontoTipo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="desctipo_id", type="integer", nullable=false, length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="desctipo_id")
     * @LG\Labels\Attributes(text="código tipo de desconto")
     * @LG\Querys\Conditions(type="=")
     */
    private $desctipoId;
    /**
     * @var string
     *
     * @ORM\Column(name="desctipo_descricao", type="string", nullable=false, length=45)
     * @LG\Labels\Property(name="desctipo_descricao")
     * @LG\Labels\Attributes(text="descrição de")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $desctipoDescricao;
    /**
     * @var float
     *
     * @ORM\Column(name="desctipo_percmax", type="float", nullable=true, length=6)
     * @LG\Labels\Property(name="desctipo_percmax")
     * @LG\Labels\Attributes(text="percentual máximo de")
     * @LG\Querys\Conditions(type="=")
     */
    private $desctipoPercmax;
    /**
     * @var float
     *
     * @ORM\Column(name="desctipo_valormax", type="float", nullable=true, length=10)
     * @LG\Labels\Property(name="desctipo_valormax")
     * @LG\Labels\Attributes(text="valor máximo de")
     * @LG\Querys\Conditions(type="=")
     */
    private $desctipoValormax;
    /**
     * @var string
     *
     * @ORM\Column(name="desctipo_modalidade", type="string", nullable=true, length=13)
     * @LG\Labels\Property(name="desctipo_modalidade")
     * @LG\Labels\Attributes(text="modalidade de")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $desctipoModalidade;
    /**
     * @var string
     *
     * @ORM\Column(name="desctipo_limita_vencimento", type="string", nullable=false, length=3)
     * @LG\Labels\Property(name="desctipo_limita_vencimento")
     * @LG\Labels\Attributes(text="vencimento limita de")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $desctipoLimitaVencimento;

    /**
     * @var string
     * @ORM\Column(name="desctipo_desconto_acumulativo", type="string", nullable=false, length=3)
     */
    private $desctipoDescontoAcumulativo;

    /**
     * @return integer
     */
    public function getDesctipoId()
    {
        return $this->desctipoId;
    }

    /**
     * @param integer $desctipoId
     * @return FinanceiroDescontoTipo
     */
    public function setDesctipoId($desctipoId)
    {
        $this->desctipoId = $desctipoId;

        return $this;
    }

    /**
     * @return string
     */
    public function getDesctipoDescricao()
    {
        return $this->desctipoDescricao;
    }

    /**
     * @param string $desctipoDescricao
     * @return FinanceiroDescontoTipo
     */
    public function setDesctipoDescricao($desctipoDescricao)
    {
        $this->desctipoDescricao = $desctipoDescricao;

        return $this;
    }

    /**
     * @return float
     */
    public function getDesctipoPercmax()
    {
        return $this->desctipoPercmax;
    }

    /**
     * @param float $desctipoPercmax
     * @return FinanceiroDescontoTipo
     */
    public function setDesctipoPercmax($desctipoPercmax)
    {
        $this->desctipoPercmax = $desctipoPercmax ? $desctipoPercmax : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getDesctipoValormax()
    {
        return $this->desctipoValormax;
    }

    /**
     * @param float $desctipoValormax
     * @return FinanceiroDescontoTipo
     */
    public function setDesctipoValormax($desctipoValormax)
    {
        $this->desctipoValormax = $desctipoValormax ? $desctipoValormax : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getDesctipoModalidade()
    {
        return $this->desctipoModalidade;
    }

    /**
     * @param string $desctipoModalidade
     * @return FinanceiroDescontoTipo
     */
    public function setDesctipoModalidade($desctipoModalidade)
    {
        $this->desctipoModalidade = $desctipoModalidade;

        return $this;
    }

    /**
     * @return string
     */
    public function getDesctipoLimitaVencimento()
    {
        return $this->desctipoLimitaVencimento;
    }

    /**
     * @param string $desctipoLimitaVencimento
     * @return FinanceiroDescontoTipo
     */
    public function setDesctipoLimitaVencimento($desctipoLimitaVencimento)
    {
        $this->desctipoLimitaVencimento = $desctipoLimitaVencimento;

        return $this;
    }

    /**
     * @return string
     */
    public function getDesctipoDescontoAcumulativo()
    {
        return $this->desctipoDescontoAcumulativo;
    }

    /**
     * @param  string $desctipoDescontoAcumulativo
     * @return FinanceiroDescontoTipo
     */
    public function setDesctipoDescontoAcumulativo($desctipoDescontoAcumulativo)
    {
        $this->desctipoDescontoAcumulativo = $desctipoDescontoAcumulativo;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'desctipoId'                  => $this->getDesctipoId(),
            'desctipoDescricao'           => $this->getDesctipoDescricao(),
            'desctipoPercmax'             => $this->getDesctipoPercmax(),
            'desctipoValormax'            => $this->getDesctipoValormax(),
            'desctipoModalidade'          => $this->getDesctipoModalidade(),
            'desctipoLimitaVencimento'    => $this->getDesctipoLimitaVencimento(),
            'desctipoDescontoAcumulativo' => $this->getDesctipoDescontoAcumulativo(),
        );

        $array['desctipoPercmax']  = $array['desctipoPercmax'] ? $array['desctipoPercmax'] : '';
        $array['desctipoValormax'] = $array['desctipoValormax'] ? $array['desctipoValormax'] : '';

        return $array;
    }
}
