<?php

namespace Financeiro\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * FinanceiroTitulo
 *
 * @ORM\Table(name="financeiro__titulo")
 * @ORM\Entity
 * @LG\LG(id="tituloId",label="TituloDescontoManual")
 * @Jarvis\Jarvis(title="Listagem de título",icon="fa fa-table")
 */
class FinanceiroTitulo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="titulo_id", type="integer", nullable=false, length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="titulo_id")
     * @LG\Labels\Attributes(text="código título")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloId;

    /**
     * @var \Pessoa\Entity\Pessoa
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     */
    private $pes;

    /**
     * @var \Financeiro\Entity\FinanceiroTituloTipo
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\FinanceiroTituloTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipotitulo_id", referencedColumnName="tipotitulo_id")
     * })
     */
    private $tipotitulo;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_baixa", referencedColumnName="id")
     * })
     */
    private $usuarioBaixa;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_autor", referencedColumnName="id")
     * })
     */
    private $usuarioAutor;

    /**
     * @var \Financeiro\Entity\FinanceiroTitulo
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\FinanceiroTitulo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="titulo_novo", referencedColumnName="titulo_id")
     * })
     */
    private $tituloNovo;

    /**
     * @var \Matricula\Entity\AcadgeralAlunoCurso
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadgeralAlunoCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="alunocurso_id", referencedColumnName="alunocurso_id")
     * })
     */
    private $alunocurso;

    /**
     * @var \Matricula\Entity\AcadperiodoAluno
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoAluno")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="alunoper_id", referencedColumnName="alunoper_id")
     * })
     */
    private $alunoPer;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo_descricao", type="string", nullable=false, length=255)
     * @LG\Labels\Property(name="titulo_descricao")
     * @LG\Labels\Attributes(text="descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $tituloDescricao;

    /**
     * @var integer
     *
     * @ORM\Column(name="titulo_parcela", type="integer", nullable=false, length=4)
     * @LG\Labels\Property(name="titulo_parcela")
     * @LG\Labels\Attributes(text="parcela")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloParcela;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="titulo_data_processamento", type="datetime", nullable=false)
     * @LG\Labels\Property(name="titulo_data_processamento")
     * @LG\Labels\Attributes(text="processamento data")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloDataProcessamento;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="titulo_data_baixa", type="datetime", nullable=false)
     * @LG\Labels\Property(name="titulo_data_baixa")
     * @LG\Labels\Attributes(text="Data da baixa do titulo")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloDataBaixa;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="titulo_data_vencimento", type="datetime", nullable=false)
     * @LG\Labels\Property(name="titulo_data_vencimento")
     * @LG\Labels\Attributes(text="vencimento data")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloDataVencimento;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo_tipo_pagamento", type="string", nullable=true, length=6)
     * @LG\Labels\Property(name="titulo_tipo_pagamento")
     * @LG\Labels\Attributes(text="pagamento tipo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $tituloTipoPagamento;

    /**
     * @var float
     *
     * @ORM\Column(name="titulo_valor", type="float", nullable=false, length=10)
     * @LG\Labels\Property(name="titulo_valor")
     * @LG\Labels\Attributes(text="valor")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloValor;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="titulo_data_pagamento", type="datetime", nullable=true)
     * @LG\Labels\Property(name="titulo_data_pagamento")
     * @LG\Labels\Attributes(text="pagamento data")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloDataPagamento;

    /**
     * @var float
     *
     * @ORM\Column(name="titulo_multa", type="float", nullable=true, length=10)
     * @LG\Labels\Property(name="titulo_multa")
     * @LG\Labels\Attributes(text="multa")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloMulta;

    /**
     * @var float
     *
     * @ORM\Column(name="titulo_juros", type="float", nullable=true, length=10)
     * @LG\Labels\Property(name="titulo_juros")
     * @LG\Labels\Attributes(text="juros")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloJuros;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo_estado", type="string", nullable=true, length=4)
     * @LG\Labels\Property(name="titulo_estado")
     * @LG\Labels\Attributes(text="estado")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $tituloEstado;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo_pagseguro_link", type="string", nullable=true, length=50)
     * @LG\Labels\Property(name="titulo_pagseguro_link")
     * @LG\Labels\Attributes(text="link pagseguro")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $tituloPagseguroLink;

    /**
     * @var float
     *
     * @ORM\Column(name="titulo_valor_pago", type="float", nullable=true, length=10)
     * @LG\Labels\Property(name="titulo_valor_pago")
     * @LG\Labels\Attributes(text="pago valor")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloValorPago;

    /**
     * @var float
     *
     * @ORM\Column(name="titulo_valor_pago_dinheiro", type="float", nullable=true, length=10)
     * @LG\Labels\Property(name="titulo_valor_pago_dinheiro")
     * @LG\Labels\Attributes(text="dinheiro pago valor")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloValorPagoDinheiro;

    /**
     * @var float
     *
     * @ORM\Column(name="titulo_desconto", type="float", nullable=true, length=10)
     * @LG\Labels\Property(name="titulo_desconto")
     * @LG\Labels\Attributes(text="desconto")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloDesconto;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo_observacoes", type="text", nullable=true)
     * @LG\Labels\Property(name="titulo_observacoes")
     * @LG\Labels\Attributes(text="observacões")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $tituloObservacoes;

    /**
     * @var float
     *
     * @ORM\Column(name="titulo_desconto_manual", type="float", nullable=true, length=10)
     * @LG\Labels\Property(name="titulo_desconto_manual")
     * @LG\Labels\Attributes(text="manual desconto")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloDescontoManual;

    /**
     * @var float
     *
     * @ORM\Column(name="titulo_acrescimo_manual", type="float", nullable=true, length=10)
     * @LG\Labels\Property(name="titulo_acrescimo_manual")
     * @LG\Labels\Attributes(text="manual acréscimo")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloAcrescimoManual;

    /**
     * @var integer
     *
     * @ORM\Column(name="titulo_comprovante_via", type="integer", nullable=true, length=10)
     * @LG\Labels\Property(name="titulo_comprovante_via")
     * @LG\Labels\Attributes(text="Número de vias impressas do comprovante")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloComprovanteVia = 0;

    /**
     * @var \Financeiro\Entity\FinanceiroTituloConfig
     * @ORM\ManyToOne(targetEntity="\Financeiro\Entity\FinanceiroTituloConfig")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tituloconf_id", referencedColumnName="tituloconf_id")
     * })
     */
    private $tituloconf;
    /**
     * @var float
     *
     * @ORM\Column(name="financeiro_desconto_incentivo", type="float", nullable=true)
     * @LG\Labels\Property(name="financeiro_desconto_incentivo")
     * @LG\Labels\Attributes(text="Valor do desconto de incentivo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $financeiroDescontoIncentivo = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo_incluir_remessa", type="string", nullable=false, length=8)
     * @LG\Labels\Property(name="titulo_incluir_remessa")
     * @LG\Labels\Attributes(text="Se é para incluir o título na remessa")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $tituloIncluirRemessa = "Sim";

    /**
     * @return integer
     */
    public function getTituloId()
    {
        return $this->tituloId;
    }

    /**
     * @param integer $tituloId
     * @return FinanceiroTitulo
     */
    public function setTituloId($tituloId)
    {
        $this->tituloId = $tituloId;

        return $this;
    }

    /**
     * @return \Pessoa\Entity\Pessoa
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @param \Pessoa\Entity\Pessoa $pes
     * @return FinanceiroTitulo
     */
    public function setPes($pes)
    {
        $this->pes = $pes;

        return $this;
    }

    /**
     * @return \Financeiro\Entity\FinanceiroTituloTipo
     */
    public function getTipotitulo()
    {
        return $this->tipotitulo;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroTituloTipo $tipotitulo
     * @return FinanceiroTitulo
     */
    public function setTipotitulo($tipotitulo)
    {
        $this->tipotitulo = $tipotitulo;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuarioBaixa()
    {
        return $this->usuarioBaixa;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuarioBaixa
     * @return FinanceiroTitulo
     */
    public function setUsuarioBaixa($usuarioBaixa)
    {
        $this->usuarioBaixa = $usuarioBaixa;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuarioAutor()
    {
        return $this->usuarioAutor;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuarioAutor
     * @return FinanceiroTitulo
     */
    public function setUsuarioAutor($usuarioAutor)
    {
        $this->usuarioAutor = $usuarioAutor;

        return $this;
    }

    /**
     * @return \Financeiro\Entity\FinanceiroTitulo
     */
    public function getTituloNovo()
    {
        return $this->tituloNovo;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroTitulo $tituloNovo
     * @return FinanceiroTitulo
     */
    public function setTituloNovo($tituloNovo)
    {
        $this->tituloNovo = $tituloNovo;

        return $this;
    }

    /**
     * @return \Matricula\Entity\AcadgeralAlunoCurso
     */
    public function getAlunocurso()
    {
        return $this->alunocurso;
    }

    /**
     * @param \Matricula\Entity\AcadgeralAlunoCurso $alunocurso
     * @return FinanceiroTitulo
     */
    public function setAlunocurso($alunocurso)
    {
        $this->alunocurso = $alunocurso;

        return $this;
    }

    /**
     * @return \Matricula\Entity\AcadperiodoAluno
     */
    public function getAlunoPer()
    {
        return $this->alunoPer;
    }

    /**
     * @param \Matricula\Entity\AcadperiodoAluno $alunoPer
     * @return FinanceiroTitulo
     */
    public function setAlunoPer($alunoPer)
    {
        $this->alunoPer = $alunoPer;

        return $this;
    }

    /**
     * @return string
     */
    public function getTituloDescricao()
    {
        return $this->tituloDescricao;
    }

    /**
     * @param string $tituloDescricao
     * @return FinanceiroTitulo
     */
    public function setTituloDescricao($tituloDescricao)
    {
        $this->tituloDescricao = $tituloDescricao;

        return $this;
    }

    /**
     * @return integer
     */
    public function getTituloParcela()
    {
        return $this->tituloParcela;
    }

    /**
     * @param integer $tituloParcela
     * @return FinanceiroTitulo
     */
    public function setTituloParcela($tituloParcela)
    {
        $this->tituloParcela = $tituloParcela;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getTituloDataProcessamento($format = false)
    {
        $tituloDataProcessamento = $this->tituloDataProcessamento;

        if ($format && $tituloDataProcessamento) {
            $tituloDataProcessamento = $tituloDataProcessamento->format('d/m/Y H:i:s');
        }

        return $tituloDataProcessamento;
    }

    /**
     * @param \Datetime $tituloDataProcessamento
     * @return FinanceiroTitulo
     */
    public function setTituloDataProcessamento($tituloDataProcessamento)
    {
        if ($tituloDataProcessamento) {
            if (is_string($tituloDataProcessamento)) {
                $tituloDataProcessamento = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $tituloDataProcessamento
                );
                $tituloDataProcessamento = new \Datetime($tituloDataProcessamento);
            }

            if ($tituloDataProcessamento->format("His") == 0) {
                $dataAtual = new \Datetime();
                $tituloDataProcessamento->setTime(
                    $dataAtual->format("H"),
                    $dataAtual->format("i"),
                    $dataAtual->format("s")
                );
            }
        } else {
            $tituloDataProcessamento = null;
        }
        $this->tituloDataProcessamento = $tituloDataProcessamento;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getTituloDataVencimento($format = false)
    {
        $tituloDataVencimento = $this->tituloDataVencimento;

        if ($tituloDataVencimento->format('Y') <= 0) {
            $tituloDataVencimento = $this->getTituloDataProcessamento();
        }

        if ($format && $tituloDataVencimento) {
            $tituloDataVencimento = $tituloDataVencimento->format('d/m/Y');
        }

        return $tituloDataVencimento;
    }

    /**
     * @param \Datetime $tituloDataVencimento
     * @return FinanceiroTitulo
     */
    public function setTituloDataVencimento($tituloDataVencimento)
    {
        if ($tituloDataVencimento) {
            if (is_string($tituloDataVencimento)) {
                $tituloDataVencimento = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $tituloDataVencimento
                );
                $tituloDataVencimento = new \Datetime($tituloDataVencimento);
            }
        } else {
            $tituloDataVencimento = null;
        }
        $this->tituloDataVencimento = $tituloDataVencimento;

        return $this;
    }

    /**
     * @return string
     */
    public function getTituloTipoPagamento()
    {
        return $this->tituloTipoPagamento;
    }

    /**
     * @param string $tituloTipoPagamento
     * @return FinanceiroTitulo
     */
    public function setTituloTipoPagamento($tituloTipoPagamento)
    {
        $this->tituloTipoPagamento = $tituloTipoPagamento;

        return $this;
    }

    /**
     * @return float
     */
    public function getTituloValor()
    {
        return $this->tituloValor;
    }

    /**
     * @param float $tituloValor
     * @return FinanceiroTitulo
     */
    public function setTituloValor($tituloValor)
    {
        $this->tituloValor = $tituloValor;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getTituloDataPagamento($format = false)
    {
        $tituloDataPagamento = $this->tituloDataPagamento;

        if ($format && $tituloDataPagamento) {
            $tituloDataPagamento = $tituloDataPagamento->format('d/m/Y H:i:s');
        }

        return $tituloDataPagamento;
    }

    /**
     * @param \Datetime $tituloDataPagamento
     * @return FinanceiroTitulo
     */
    public function setTituloDataPagamento($tituloDataPagamento)
    {
        if ($tituloDataPagamento) {
            if (is_string($tituloDataPagamento)) {
                $tituloDataPagamento = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $tituloDataPagamento
                );
                $tituloDataPagamento = new \Datetime($tituloDataPagamento);
            }

            if ($tituloDataPagamento->format("His") == 0) {
                $dataAtual = new \Datetime();
                $tituloDataPagamento->setTime(
                    $dataAtual->format("H"),
                    $dataAtual->format("i"),
                    $dataAtual->format("s")
                );
            }
        } else {
            $tituloDataPagamento = null;
        }
        $this->tituloDataPagamento = $tituloDataPagamento;

        return $this;
    }

    /**
     * @return float
     */
    public function getTituloMulta()
    {
        return $this->tituloMulta;
    }

    /**
     * @param float $tituloMulta
     * @return FinanceiroTitulo
     */
    public function setTituloMulta($tituloMulta)
    {
        $this->tituloMulta = $tituloMulta;

        return $this;
    }

    /**
     * @return float
     */
    public function getTituloJuros()
    {
        return $this->tituloJuros;
    }

    /**
     * @param float $tituloJuros
     * @return FinanceiroTitulo
     */
    public function setTituloJuros($tituloJuros)
    {
        $this->tituloJuros = $tituloJuros;

        return $this;
    }

    /**
     * @return string
     */
    public function getTituloEstado()
    {
        return $this->tituloEstado;
    }

    /**
     * @return boolean
     */
    public function tituloEstaAberto()
    {
        return $this->tituloEstado == \Financeiro\Service\FinanceiroTitulo::TITULO_ESTADO_ABERTO;
    }

    /**
     * @param string $tituloEstado
     * @return FinanceiroTitulo
     */
    public function setTituloEstado($tituloEstado)
    {
        $this->tituloEstado = $tituloEstado;

        return $this;
    }

    /**
     * @return string
     */
    public function getTituloPagseguroLink()
    {
        return $this->tituloPagseguroLink;
    }

    /**
     * @param string $tituloPagseguroLink
     * @return FinanceiroTitulo
     */
    public function setTituloPagseguroLink($tituloPagseguroLink)
    {
        $this->tituloPagseguroLink = $tituloPagseguroLink;

        return $this;
    }

    /**
     * @return float
     */
    public function getTituloValorPago()
    {
        return $this->tituloValorPago;
    }

    /**
     * @param float $tituloValorPago
     * @return FinanceiroTitulo
     */
    public function setTituloValorPago($tituloValorPago)
    {
        $this->tituloValorPago = $tituloValorPago;

        return $this;
    }

    /**
     * @return float
     */
    public function getTituloValorPagoDinheiro()
    {
        return $this->tituloValorPagoDinheiro;
    }

    /**
     * @param float $tituloValorPagoDinheiro
     * @return FinanceiroTitulo
     */
    public function setTituloValorPagoDinheiro($tituloValorPagoDinheiro)
    {
        $this->tituloValorPagoDinheiro = $tituloValorPagoDinheiro;

        return $this;
    }

    /**
     * @return float
     */
    public function getTituloDesconto()
    {
        return $this->tituloDesconto;
    }

    /**
     * @param float $tituloDesconto
     * @return FinanceiroTitulo
     */
    public function setTituloDesconto($tituloDesconto)
    {
        $this->tituloDesconto = $tituloDesconto;

        return $this;
    }

    /**
     * @return string
     */
    public function getTituloObservacoes()
    {
        return $this->tituloObservacoes;
    }

    /**
     * @param string $tituloObservacoes
     * @return FinanceiroTitulo
     */
    public function setTituloObservacoes($tituloObservacoes)
    {
        $this->tituloObservacoes = $tituloObservacoes;

        return $this;
    }

    /**
     * @return float
     */
    public function getTituloDescontoManual()
    {
        return $this->tituloDescontoManual;
    }

    /**
     * @param float $tituloDescontoManual
     * @return FinanceiroTitulo
     */
    public function setTituloDescontoManual($tituloDescontoManual)
    {
        $this->tituloDescontoManual = $tituloDescontoManual;

        return $this;
    }

    /**
     * @return float
     */
    public function getTituloAcrescimoManual()
    {
        return $this->tituloAcrescimoManual;
    }

    /**
     * @param float $tituloAcrescimoManual
     * @return FinanceiroTitulo
     */
    public function setTituloAcrescimoManual($tituloAcrescimoManual)
    {
        $this->tituloAcrescimoManual = $tituloAcrescimoManual;

        return $this;
    }

    public function getTituloDataBaixa($format = false)
    {
        $tituloDataBaixa = $this->tituloDataBaixa;

        if ($format && $tituloDataBaixa) {
            $tituloDataBaixa = $tituloDataBaixa->format('d/m/Y H:i:s');
        }

        return $tituloDataBaixa;
    }

    /**
     * @return \Financeiro\Entity\FinanceiroTituloConfig
     */
    public function getTituloconf()
    {
        return $this->tituloconf;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroTituloConfig $tituloconf
     * @return FinanceiroTitulo
     */
    public function setTituloconf($tituloconf)
    {
        $this->tituloconf = $tituloconf;

        return $this;
    }

    /**
     * @param \Datetime $tituloDataBaixa
     * @return FinanceiroTitulo
     */
    public function setTituloDataBaixa($tituloDataBaixa)
    {
        if ($tituloDataBaixa) {
            if (is_string($tituloDataBaixa)) {
                $tituloDataBaixa = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $tituloDataBaixa
                );
                $tituloDataBaixa = new \Datetime($tituloDataBaixa);
            }

            if ($tituloDataBaixa->format("His") == 0) {
                $dataAtual = new \Datetime();
                $tituloDataBaixa->setTime($dataAtual->format("H"), $dataAtual->format("i"), $dataAtual->format("s"));
            }
        } else {
            $tituloDataBaixa = null;
        }

        $this->tituloDataBaixa = $tituloDataBaixa;

        return $this;
    }

    /**
     * @return int
     */
    public function getTituloComprovanteVia()
    {
        return $this->tituloComprovanteVia;
    }

    /**
     * @param int $tituloComprovanteVia
     * @return FinanceiroTitulo
     */
    public function setTituloComprovanteVia($tituloComprovanteVia = 0)
    {
        $this->tituloComprovanteVia = $tituloComprovanteVia ? $tituloComprovanteVia : 0;

        return $this;
    }

    /**
     * @return float
     */
    public function getPagamentoDescontoIncentivo()
    {
        return $this->financeiroDescontoIncentivo;
    }

    /**
     * @param float $valor
     * @return FinanceiroTitulo
     */
    public function setPagamentoDescontoIncentivo($valor)
    {
        $this->financeiroDescontoIncentivo = $valor;

        return $this;
    }

    /**
     * @return string
     */
    public function getTituloIncluirRemessa()
    {
        return $this->tituloIncluirRemessa;
    }

    /**
     * @param string $tituloIncluirRemessa
     * @return FinanceiroTitulo
     */
    public function setTituloIncluirRemessa($tituloIncluirRemessa)
    {
        $this->tituloIncluirRemessa = $tituloIncluirRemessa;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'tituloId'                => $this->getTituloId(),
            'pes'                     => $this->getPes(),
            'tipotitulo'              => $this->getTipotitulo(),
            'usuarioBaixa'            => $this->getUsuarioBaixa(),
            'usuarioAutor'            => $this->getUsuarioAutor(),
            'tituloNovo'              => $this->getTituloNovo(),
            'alunocurso'              => $this->getAlunocurso(),
            'alunoPer'                => $this->getAlunoPer(),
            'tituloDescricao'         => $this->getTituloDescricao(),
            'tituloParcela'           => $this->getTituloParcela(),
            'tituloDataProcessamento' => $this->getTituloDataProcessamento(true),
            'tituloDataVencimento'    => $this->getTituloDataVencimento(true),
            'tituloTipoPagamento'     => $this->getTituloTipoPagamento(),
            'tituloValor'             => $this->getTituloValor(),
            'tituloDataPagamento'     => $this->getTituloDataPagamento(true),
            'tituloMulta'             => $this->getTituloMulta(),
            'tituloJuros'             => $this->getTituloJuros(),
            'tituloEstado'            => $this->getTituloEstado(),
            'tituloValorPago'         => $this->getTituloValorPago(),
            'tituloValorPagoDinheiro' => $this->getTituloValorPagoDinheiro(),
            'tituloDesconto'          => $this->getTituloDesconto(),
            'tituloObservacoes'       => $this->getTituloObservacoes(),
            'tituloDescontoManual'    => $this->getTituloDescontoManual(),
            'tituloAcrescimoManual'   => $this->getTituloAcrescimoManual(),
            'tituloDataBaixa'         => $this->getTituloDataBaixa(true),
            'tituloPagseguroLink'     => $this->getTituloPagseguroLink(),
            'tituloComprovanteVia'    => $this->getTituloComprovanteVia(),
            'tituloconf'              => $this->getTituloconf(),
            'tituloDescontoIncentivo' => $this->getPagamentoDescontoIncentivo(),
            'tituloIncluirRemessa'    => $this->getTituloIncluirRemessa()
        );

        $array['pes']          = $this->getPes() ? $this->getPes()->getPesId() : null;
        $array['tipotitulo']   = $this->getTipotitulo() ? $this->getTipotitulo()->getTipotituloId() : null;
        $array['usuarioBaixa'] = $this->getUsuarioBaixa() ? $this->getUsuarioBaixa()->getId() : null;
        $array['usuarioAutor'] = $this->getUsuarioAutor() ? $this->getUsuarioAutor()->getId() : null;
        $array['tituloNovo']   = $this->getTituloNovo() ? $this->getTituloNovo()->getTituloId() : null;
        $array['alunocurso']   = $this->getAlunocurso() ? $this->getAlunocurso()->getAlunocursoId() : null;
        $array['alunoPer']     = $this->getAlunoPer() ? $this->getAlunoPer()->getAlunoperId() : null;

        return $array;
    }
}
