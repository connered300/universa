<?php

namespace Financeiro\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * FinanceiroPagamento
 *
 * @ORM\Table(name="financeiro__pagamento", indexes={@ORM\Index(name="fk_financeiro__pagamento_acesso_pessoas1_idx", columns={"pagamento_usuario"}), @ORM\Index(name="fk_financeiro__pagamento_financeiro__meio_pagamento1_idx", columns={"meio_pagamento_id"}),@ORM\Index(name="fk_financeiro__pagamento_financeiro__titulo1_idx", columns={"titulo_id"}), @ORM\Index(name="fk_financeiro__pagamento_pessoa_fisica1_idx", columns={"pes_id"})})
 * @ORM\Entity
 * @LG\LG(id="pagamentoId",label="PagamentoDescontoManual")
 * @Jarvis\Jarvis(title="Listagem de pagamento",icon="fa fa-table")
 */
class FinanceiroPagamento
{
    /**
     * @var integer
     *
     * @ORM\Column(name="pagamento_id", type="integer", nullable=false, length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="pagamento_id")
     * @LG\Labels\Attributes(text="código")
     * @LG\Querys\Conditions(type="=")
     */
    private $pagamentoId;

    /**
     * @var \Financeiro\Entity\FinanceiroTitulo
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\FinanceiroTitulo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="titulo_id", referencedColumnName="titulo_id")
     * })
     */
    private $titulo;
    /**
     * @var \Financeiro\Entity\FinanceiroMeioPagamento
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\FinanceiroMeioPagamento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="meio_pagamento_id", referencedColumnName="meio_pagamento_id")
     * })
     */
    private $meioPagamento;
    /**
     * @var \Acesso\Entity\AcessoPessoas
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pagamento_usuario", referencedColumnName="id")
     * })
     */
    private $pagamentoUsuario;
    /**
     * @var \Pessoa\Entity\Pessoa
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     */
    private $pes;
    /**
     * @var float
     *
     * @ORM\Column(name="pagamento_valor_bruto", type="float", nullable=false, length=10)
     * @LG\Labels\Property(name="pagamento_valor_bruto")
     * @LG\Labels\Attributes(text="bruto valor")
     * @LG\Querys\Conditions(type="=")
     */
    private $pagamentoValorBruto;
    /**
     * @var float
     *
     * @ORM\Column(name="pagamento_valor_juros", type="float", nullable=false, length=10)
     * @LG\Labels\Property(name="pagamento_valor_juros")
     * @LG\Labels\Attributes(text="juros valor")
     * @LG\Querys\Conditions(type="=")
     */
    private $pagamentoValorJuros;
    /**
     * @var float
     *
     * @ORM\Column(name="pagamento_valor_multa", type="float", nullable=false, length=10)
     * @LG\Labels\Property(name="pagamento_valor_multa")
     * @LG\Labels\Attributes(text="multa valor")
     * @LG\Querys\Conditions(type="=")
     */
    private $pagamentoValorMulta;
    /**
     * @var float
     *
     * @ORM\Column(name="pagamento_acrescimos", type="float", nullable=false, length=10)
     * @LG\Labels\Property(name="pagamento_acrescimos")
     * @LG\Labels\Attributes(text="acrescimos")
     * @LG\Querys\Conditions(type="=")
     */
    private $pagamentoAcrescimos;
    /**
     * @var float
     *
     * @ORM\Column(name="pagamento_valor_descontos", type="float", nullable=false, length=10)
     * @LG\Labels\Property(name="pagamento_valor_descontos")
     * @LG\Labels\Attributes(text="descontos valor")
     * @LG\Querys\Conditions(type="=")
     */
    private $pagamentoValorDescontos;
    /**
     * @var float
     *
     * @ORM\Column(name="pagamento_valor_final", type="float", nullable=false, length=10)
     * @LG\Labels\Property(name="pagamento_valor_final")
     * @LG\Labels\Attributes(text="final valor")
     * @LG\Querys\Conditions(type="=")
     */
    private $pagamentoValorFinal;
    /**
     * @var float
     *
     * @ORM\Column(name="pagamento_desconto_manual", type="float", nullable=false, length=10)
     * @LG\Labels\Property(name="pagamento_desconto_manual")
     * @LG\Labels\Attributes(text="manual desconto")
     * @LG\Querys\Conditions(type="=")
     */
    private $pagamentoDescontoManual;
    /**
     * @var \Datetime
     *
     * @ORM\Column(name="pagamento_data_baixa", type="datetime", nullable=true)
     * @LG\Labels\Property(name="pagamento_data_baixa")
     * @LG\Labels\Attributes(text="baixa data")
     * @LG\Querys\Conditions(type="=")
     */
    private $pagamentoDataBaixa;
    /**
     * @var string
     *
     * @ORM\Column(name="pagamento_observacoes", type="text", nullable=true)
     * @LG\Labels\Property(name="pagamento_observacoes")
     * @LG\Labels\Attributes(text="observacões")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $pagamentoObservacoes;
    /**
     * @var string
     *
     * @ORM\Column(name="pagamento_retorno_api", type="text", nullable=true)
     * @LG\Labels\Property(name="pagamento_retorno_api")
     * @LG\Labels\Attributes(text="retorno da api")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $pagamentoRetornoApi;
    /**
     * @var \Financeiro\Entity\FinanceiroCartao
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\FinanceiroCartao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cartao_id", referencedColumnName="cartao_id")
     * })
     */
    private $cartaoId;

    /**
     * @return integer
     */
    public function getPagamentoId()
    {
        return $this->pagamentoId;
    }

    /**
     * @param integer $pagamentoId
     * @return FinanceiroPagamento
     */
    public function setPagamentoId($pagamentoId)
    {
        $this->pagamentoId = $pagamentoId;

        return $this;
    }

    /**
     * @return \Financeiro\Entity\FinanceiroTitulo
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroTitulo $titulo
     * @return FinanceiroPagamento
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * @return \Financeiro\Entity\FinanceiroMeioPagamento
     */
    public function getMeioPagamento()
    {
        return $this->meioPagamento;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroMeioPagamento $meioPagamento
     * @return FinanceiroPagamento
     */
    public function setMeioPagamento($meioPagamento)
    {
        $this->meioPagamento = $meioPagamento;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getPagamentoUsuario()
    {
        return $this->pagamentoUsuario;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $pagamentoUsuario
     * @return FinanceiroPagamento
     */
    public function setPagamentoUsuario($pagamentoUsuario)
    {
        $this->pagamentoUsuario = $pagamentoUsuario;

        return $this;
    }

    /**
     * @return \Pessoa\Entity\Pessoa
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @param \Pessoa\Entity\Pessoa $pes
     * @return FinanceiroPagamento
     */
    public function setPes($pes)
    {
        $this->pes = $pes;

        return $this;
    }

    /**
     * @return float
     */
    public function getPagamentoValorBruto()
    {
        return $this->pagamentoValorBruto;
    }

    /**
     * @param float $pagamentoValorBruto
     * @return FinanceiroPagamento
     */
    public function setPagamentoValorBruto($pagamentoValorBruto)
    {
        $this->pagamentoValorBruto = $pagamentoValorBruto;

        return $this;
    }

    /**
     * @return float
     */
    public function getPagamentoValorJuros()
    {
        return $this->pagamentoValorJuros;
    }

    /**
     * @param float $pagamentoValorJuros
     * @return FinanceiroPagamento
     */
    public function setPagamentoValorJuros($pagamentoValorJuros)
    {
        $this->pagamentoValorJuros = (float)$pagamentoValorJuros;

        return $this;
    }

    /**
     * @return float
     */
    public function getPagamentoValorMulta()
    {
        return $this->pagamentoValorMulta;
    }

    /**
     * @param float $pagamentoValorMulta
     * @return FinanceiroPagamento
     */
    public function setPagamentoValorMulta($pagamentoValorMulta)
    {
        $this->pagamentoValorMulta = (float)$pagamentoValorMulta;

        return $this;
    }

    /**
     * @return float
     */
    public function getPagamentoAcrescimos()
    {
        return $this->pagamentoAcrescimos;
    }

    /**
     * @param float $pagamentoAcrescimos
     * @return FinanceiroPagamento
     */
    public function setPagamentoAcrescimos($pagamentoAcrescimos)
    {
        $this->pagamentoAcrescimos = (float)$pagamentoAcrescimos;

        return $this;
    }

    /**
     * @return float
     */
    public function getPagamentoValorDescontos()
    {
        return $this->pagamentoValorDescontos;
    }

    /**
     * @param float $pagamentoValorDescontos
     * @return FinanceiroPagamento
     */
    public function setPagamentoValorDescontos($pagamentoValorDescontos)
    {
        $this->pagamentoValorDescontos = (float)$pagamentoValorDescontos;

        return $this;
    }

    /**
     * @return float
     */
    public function getPagamentoValorFinal()
    {
        return $this->pagamentoValorFinal;
    }

    /**
     * @param float $pagamentoValorFinal
     * @return FinanceiroPagamento
     */
    public function setPagamentoValorFinal($pagamentoValorFinal)
    {
        $this->pagamentoValorFinal = $pagamentoValorFinal;

        return $this;
    }

    /**
     * @return float
     */
    public function getPagamentoDescontoManual()
    {
        return $this->pagamentoDescontoManual;
    }

    /**
     * @param float $pagamentoDescontoManual
     * @return FinanceiroPagamento
     */
    public function setPagamentoDescontoManual($pagamentoDescontoManual)
    {
        $this->pagamentoDescontoManual = (float)$pagamentoDescontoManual;

        return $this;
    }

    /**
     * @param bool|false $meiot
     * @return \Datetime|string
     */
    public function getPagamentoDataBaixa($meiot = false)
    {
        $pagamentoDataBaixa = $this->pagamentoDataBaixa;

        if ($meiot && $pagamentoDataBaixa) {
            $pagamentoDataBaixa = $pagamentoDataBaixa->meiot('d/m/Y H:i:s');
        }

        return $pagamentoDataBaixa;
    }

    /**
     * @param \Datetime $pagamentoDataBaixa
     * @return FinanceiroPagamento
     */
    public function setPagamentoDataBaixa($pagamentoDataBaixa)
    {
        if ($pagamentoDataBaixa) {
            if (is_string($pagamentoDataBaixa)) {
                $pagamentoDataBaixa = \VersaSpine\Service\AbstractService::meiotDateAmericano(
                    $pagamentoDataBaixa
                );
                $pagamentoDataBaixa = new \Datetime($pagamentoDataBaixa);
            }
        } else {
            $pagamentoDataBaixa = null;
        }
        $this->pagamentoDataBaixa = $pagamentoDataBaixa;

        return $this;
    }

    /**
     * @return string
     */
    public function getPagamentoObservacoes()
    {
        return $this->pagamentoObservacoes;
    }

    /**
     * @param string $pagamentoObservacoes
     * @return FinanceiroPagamento
     */
    public function setPagamentoObservacoes($pagamentoObservacoes)
    {
        $this->pagamentoObservacoes = $pagamentoObservacoes;

        return $this;
    }
    /**
     * @return string
     */
    public function getPagamentoRetornoApi()
    {
        return $this->pagamentoRetornoApi;
    }

    /**
     * @param string $pagamentoObservacoes
     * @return FinanceiroPagamento
     */
    public function setPagamentoRetornoApi($pagamentoRetornoApi)
    {
        $this->pagamentoRetornoApi = $pagamentoRetornoApi;

        return $this;
    }

    /**
     * @return \Financeiro\Entity\FinanceiroCartao
     */
    public function getCartao()
    {
        return $this->cartaoId;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroCartao $cartaoId
     * @return FinanceiroPagamento
     */
    public function setCartao($cartaoId)
    {
        $this->cartaoId = $cartaoId;

        return $this;
    }



     /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'pagamentoId'             => $this->getPagamentoId(),
            'titulo'                  => $this->getTitulo(),
            'meioPagamento'           => $this->getMeioPagamento(),
            'pagamentoUsuario'        => $this->getPagamentoUsuario(),
            'pes'                     => $this->getPes(),
            'pagamentoValorBruto'     => $this->getPagamentoValorBruto(),
            'pagamentoValorJuros'     => $this->getPagamentoValorJuros(),
            'pagamentoValorMulta'     => $this->getPagamentoValorMulta(),
            'pagamentoAcrescimos'     => $this->getPagamentoAcrescimos(),
            'pagamentoValorDescontos' => $this->getPagamentoValorDescontos(),
            'pagamentoValorFinal'     => $this->getPagamentoValorFinal(),
            'pagamentoDescontoManual' => $this->getPagamentoDescontoManual(),
            'pagamentoDataBaixa'      => $this->getPagamentoDataBaixa(true),
            'pagamentoObservacoes'    => $this->getPagamentoObservacoes(),
            'pagamentoRetornoApi'     => $this->getPagamentoRetornoApi(),
            'cartaoId'                => $this->getCartao()
        );

        $array['titulo']           = $this->getTitulo() ? $this->getTitulo()->getTituloId() : null;
        $array['meioPagamento']    = $this->getMeioPagamento() ? $this->getMeioPagamento()->getMeioPagamentoId() : null;
        $array['pagamentoUsuario'] = $this->getPagamentoUsuario() ? $this->getPagamentoUsuario()->getId() : null;
        $array['pes']              = $this->getPes() ? $this->getPes()->getPesId() : null;

        return $array;
    }
}
