<?php

namespace Financeiro\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * FinanceiroTituloTipo
 *
 * @ORM\Table(name="financeiro__titulo_tipo")
 * @ORM\Entity
 * @LG\LG(id="tipotituloId",label="TipotituloDescricao")
 * @Jarvis\Jarvis(title="Listagem de tipo de título",icon="fa fa-table")
 */
class FinanceiroTituloTipo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="tipotitulo_id", type="integer", nullable=false, length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="tipotitulo_id")
     * @LG\Labels\Attributes(text="código")
     * @LG\Querys\Conditions(type="=")
     */
    private $tipotituloId;

    /**
     * @var string
     *
     * @ORM\Column(name="tipotitulo_nome", type="string", nullable=false, length=45)
     * @LG\Labels\Property(name="tipotitulo_nome")
     * @LG\Labels\Attributes(text="nome")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $tipotituloNome;

    /**
     * @var string
     *
     * @ORM\Column(name="tipotitulo_descricao", type="string", nullable=true, length=255)
     * @LG\Labels\Property(name="tipotitulo_descricao")
     * @LG\Labels\Attributes(text="descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $tipotituloDescricao;

    /**
     * @var string
     *
     * @ORM\Column(name="tipotitulo_tempo_vencimento_tipo", type="string", nullable=true, length=3)
     * @LG\Labels\Property(name="tipotitulo_tempo_vencimento_tipo")
     * @LG\Labels\Attributes(text="Tipo de Tempo de Vencimento")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $tipotituloTempoVencimentoTipo;

    /**
     * @var integer
     *
     * @ORM\Column(name="tipotitulo_tempo_vencimento", type="integer", nullable=true, length=11)
     * @LG\Labels\Property(name="tipotitulo_tempo_vencimento")
     * @LG\Labels\Attributes(text="tempo de vencimento")
     * @LG\Querys\Conditions(type="=")
     */
    private $tipotituloTempoVencimento;

    /**
     * @var string
     *
     * @ORM\Column(name="tipotitulo_matricula_emissao", type="string", nullable=true, length=3)
     * @LG\Labels\Property(name="tipotitulo_matricula_emissao")
     * @LG\Labels\Attributes(text="emissão de título de matrícula")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $tipotituloMatriculaEmissao;

    /**
     * @var float
     * @ORM\Column(name="tipotitulo_desconto_percentual_maximo", type="float", nullable=false)
     */
    private $tipotituloDescontoPercentualMaximo;

    /**
     * @var integer
     * @ORM\Column(name="tipotitulo_numero_maximo_parcelas", type="integer", nullable=false)
     */
    private $tipotituloNumeroMaximoParcelas;

    /**
     * @var string
     * @ORM\Column(name="tipotitulo_grupo", type="string", nullable=false)
     */
    private $tipotituloGrupo;


    /**
     * @var integer
     * @ORM\Column(name="tipotitulo_prioridade", type="integer", nullable=false)
     */
    private $tipotituloPrioridade;

    /**
     * @return integer
     */
    public function getTipotituloId()
    {
        return $this->tipotituloId;
    }

    /**
     * @param integer $tipotituloId
     * @return FinanceiroTituloTipo
     */
    public function setTipotituloId($tipotituloId)
    {
        $this->tipotituloId = $tipotituloId;

        return $this;
    }

    /**
     * @return string
     */
    public function getTipotituloNome()
    {
        return $this->tipotituloNome;
    }

    /**
     * @return string
     */
    public function verificaSeEdeVertibular()
    {
        return $this->tipotituloNome == \Financeiro\Service\FinanceiroTituloTipo::TIPOTITULOVESTIBULAR;
    }

    /**
     * @param string $tipotituloNome
     * @return FinanceiroTituloTipo
     */
    public function setTipotituloNome($tipotituloNome)
    {
        $this->tipotituloNome = $tipotituloNome;

        return $this;
    }

    /**
     * @return string
     */
    public function getTipotituloDescricao()
    {
        return $this->tipotituloDescricao;
    }

    /**
     * @param string $tipotituloDescricao
     * @return FinanceiroTituloTipo
     */
    public function setTipotituloDescricao($tipotituloDescricao)
    {
        $this->tipotituloDescricao = $tipotituloDescricao;

        return $this;
    }

    /**
     * @return string
     */
    public function getTipotituloTempoVencimentoTipo()
    {
        return $this->tipotituloTempoVencimentoTipo;
    }

    /**
     * @param string $tipotituloTempoVencimentoTipo
     * @return FinanceiroTituloTipo
     */
    public function setTipotituloTempoVencimentoTipo($tipotituloTempoVencimentoTipo)
    {
        $this->tipotituloTempoVencimentoTipo = $tipotituloTempoVencimentoTipo;

        return $this;
    }

    /**
     * @return integer
     */
    public function getTipotituloTempoVencimento()
    {
        return $this->tipotituloTempoVencimento;
    }

    /**
     * @param integer $tipotituloTempoVencimento
     * @return FinanceiroTituloTipo
     */
    public function setTipotituloTempoVencimento($tipotituloTempoVencimento)
    {
        $this->tipotituloTempoVencimento = $tipotituloTempoVencimento ? $tipotituloTempoVencimento : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getTipotituloMatriculaEmissao()
    {
        return $this->tipotituloMatriculaEmissao;
    }

    /**
     * @return string
     */
    public function verificaEmissaoOpcional()
    {
        return $this->tipotituloMatriculaEmissao == \Financeiro\Service\FinanceiroTituloTipo::TIPOTITULO_MATRICULA_EMISSAO_OPCIONAL;
    }

    /**
     * @param string $tipotituloMatriculaEmissao
     * @return FinanceiroTituloTipo
     */
    public function setTipotituloMatriculaEmissao($tipotituloMatriculaEmissao)
    {
        $this->tipotituloMatriculaEmissao = $tipotituloMatriculaEmissao;

        return $this;
    }

    /**
     * @return float
     */
    public function getTipotituloDescontoPercentualMaximo()
    {
        return $this->tipotituloDescontoPercentualMaximo;
    }

    /**
     * @param float $tipotituloDescontoPercentualMaximo
     * @return FinanceiroTituloTipo
     */
    public function setTipotituloDescontoPercentualMaximo($tipotituloDescontoPercentualMaximo)
    {
        $this->tipotituloDescontoPercentualMaximo = $tipotituloDescontoPercentualMaximo;

        return $this;
    }

    /**
     * @return int
     */
    public function getTipotituloNumeroMaximoParcelas()
    {
        return $this->tipotituloNumeroMaximoParcelas;
    }

    /**
     * @param int $tipotituloNumeroMaximoParcelas
     * @return FinanceiroTituloTipo
     */
    public function setTipotituloNumeroMaximoParcelas($tipotituloNumeroMaximoParcelas)
    {
        $this->tipotituloNumeroMaximoParcelas = $tipotituloNumeroMaximoParcelas;

        return $this;
    }

    /**
     * @return integer
     */
    public function getTipotituloPrioridade()
    {

        return $this->tipotituloPrioridade;
    }

    /**
     * @param integer $tipotituloPrioridade
     * @return FinanceiroTituloTipo
     */
    public function setTipotituloPrioridade($tipotituloPrioridade)
    {
        $this->tipotituloPrioridade = $tipotituloPrioridade;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return Enum
     */
    public function getTipotituloGrupo()
    {
        return $this->tipotituloGrupo;
    }

    /**
     * @param string $tipotituloGrupo
     */
    public function setTipotituloGrupo($tipotituloGrupo)
    {
        $this->tipotituloGrupo = $tipotituloGrupo;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'tipotituloId'                       => $this->getTipotituloId(),
            'tipotituloNome'                     => $this->getTipotituloNome(),
            'tipotituloDescricao'                => $this->getTipotituloDescricao(),
            'tipotituloTempoVencimento'          => $this->getTipotituloTempoVencimento(),
            'tipotituloTempoVencimentoTipo'      => $this->getTipotituloTempoVencimentoTipo(),
            'tipotituloMatriculaEmissao'         => $this->getTipotituloMatriculaEmissao(),
            'tipotituloDescontoPercentualMaximo' => $this->getTipotituloDescontoPercentualMaximo(),
            'tipotituloNumeroMaximoParcelas'     => $this->getTipotituloNumeroMaximoParcelas(),
            'tipotituloGrupo'                    => $this->getTipotituloGrupo(),
            'tipotituloPrioridade'               => $this->getTipotituloPrioridade()
        );

        return $array;
    }
}
