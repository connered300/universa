<?php

namespace Financeiro\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * FinanceiroFiltroTipo
 *
 * @ORM\Table(name="financeiro__filtro_tipo")
 * @ORM\Entity
 * @LG\LG(id="tipotituloId",label="TipotituloId")
 * @Jarvis\Jarvis(title="Listagem de filtro tipo",icon="fa fa-table")
 */
class FinanceiroFiltroTipo
{
    /**
     * @var \Financeiro\Entity\FinanceiroFiltro
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\FinanceiroFiltro")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="filtro_id", referencedColumnName="filtro_id")
     * })
     */
    private $filtro;

    /**
     * @var \Financeiro\Entity\FinanceiroTituloTipo
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\FinanceiroTituloTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipotitulo_id", referencedColumnName="tipotitulo_id")
     * })
     */
    private $tipotitulo;

    /**
     * @return \Financeiro\Entity\FinanceiroFiltro
     */
    public function getFiltro()
    {
        return $this->filtro;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroFiltro $filtro
     * @return FinanceiroFiltroTipo
     */
    public function setFiltro($filtro)
    {
        $this->filtro = $filtro;

        return $this;
    }

    /**
     * @return \Financeiro\Entity\FinanceiroTituloTipo
     */
    public function getTipotitulo()
    {
        return $this->tipotitulo;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroTituloTipo $tipotitulo
     * @return FinanceiroFiltroTipo
     */
    public function setTipotitulo($tipotitulo)
    {
        $this->tipotitulo = $tipotitulo;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'filtro'     => $this->getFiltro(),
            'tipotitulo' => $this->getTipotitulo(),
        );

        $array['filtro']     = $this->getFiltro() ? $this->getFiltro()->getFiltroId() : null;
        $array['tipotitulo'] = $this->getTipotitulo() ? $this->getTipotitulo()->getTipotituloId() : null;

        return $array;
    }
}
