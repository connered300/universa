<?php

namespace Financeiro\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * BoletoConfConta
 *
 * @ORM\Table(name="boleto_conf_conta")
 * @ORM\Entity
 * @LG\LG(id="confcontId",label="PesId")
 * @Jarvis\Jarvis(title="Listagem de configuração de conta",icon="fa fa-table")
 */
class BoletoConfConta
{
    /**
     * @var integer
     *
     * @ORM\Column(name="confcont_id", type="integer", nullable=false, length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="confcont_id")
     * @LG\Labels\Attributes(text="código configuração de conta bancária")
     * @LG\Querys\Conditions(type="=")
     */
    private $confcontId;
    /**
     * @var \Pessoa\Entity\Pessoa
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     */
    private $pes;
    /**
     * @var \Financeiro\Entity\BoletoBanco
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\BoletoBanco")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="banc_id", referencedColumnName="banc_id")
     * })
     */
    private $banc;
    /**
     * @var \GerenciadorArquivos\Entity\Arquivo
     * @ORM\ManyToOne(targetEntity="GerenciadorArquivos\Entity\Arquivo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="arq_id", referencedColumnName="arq_id")
     * })
     */
    private $arq;
    /**
     * @var string
     *
     * @ORM\Column(name="confcont_agencia", type="string", nullable=false, length=45)
     * @LG\Labels\Property(name="confcont_agencia")
     * @LG\Labels\Attributes(text="agência")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $confcontAgencia;
    /**
     * @var string
     *
     * @ORM\Column(name="confcont_agencia_digito", type="string", nullable=false, length=45)
     * @LG\Labels\Property(name="confcont_agencia_digito")
     * @LG\Labels\Attributes(text="dígito agência")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $confcontAgenciaDigito;
    /**
     * @var string
     *
     * @ORM\Column(name="confcont_conta", type="string", nullable=false, length=45)
     * @LG\Labels\Property(name="confcont_conta")
     * @LG\Labels\Attributes(text="conta")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $confcontConta;
    /**
     * @var string
     *
     * @ORM\Column(name="confcont_conta_digito", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="confcont_conta_digito")
     * @LG\Labels\Attributes(text="dígito conta")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $confcontContaDigito;
    /**
     * @var string
     *
     * @ORM\Column(name="confcont_carteira", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="confcont_carteira")
     * @LG\Labels\Attributes(text="carteira conta")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $confcontCarteira;
    /**
     * @var string
     *
     * @ORM\Column(name="confcont_variacao", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="confcont_variacao")
     * @LG\Labels\Attributes(text="variação conta")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $confcontVariacao;
    /**
     * @var string
     *
     * @ORM\Column(name="confcont_convenio", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="confcont_convenio")
     * @LG\Labels\Attributes(text="convênio conta")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $confcontConvenio;
    /**
     * @var string
     *
     * @ORM\Column(name="confcont_contrato", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="confcont_contrato")
     * @LG\Labels\Attributes(text="contrato conta")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $confcontContrato;
    /**
     * @var string
     *
     * @ORM\Column(name="confcont_instrucoes", type="text", nullable=true)
     * @LG\Labels\Property(name="confcont_instrucoes")
     * @LG\Labels\Attributes(text="instrucões de bancária")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $confcontInstrucoes;
    /**
     * @var integer
     *
     * @ORM\Column(name="confcont_nosso_numero_inicial", type="integer", nullable=false, length=11)
     * @LG\Labels\Property(name="confcont_nosso_numero_inicial")
     * @LG\Labels\Attributes(text="número nosso inicial")
     * @LG\Querys\Conditions(type="=")
     */
    private $confcontNossoNumeroInicial;
    /**
     * @var integer
     *
     * @ORM\Column(name="confcont_remessa_sequencial", type="integer", nullable=false, length=5)
     * @LG\Labels\Property(name="confcont_remessa_sequencial")
     * @LG\Labels\Attributes(text="número sequencial manual")
     * @LG\Querys\Conditions(type="=")
     */
    private $confcontRemessaSequencial;

    /**
     * @return integer
     */
    public function getConfcontId()
    {
        return $this->confcontId;
    }

    /**
     * @param integer $confcontId
     * @return BoletoConfConta
     */
    public function setConfcontId($confcontId)
    {
        $this->confcontId = $confcontId;

        return $this;
    }

    /**
     * @return \Pessoa\Entity\Pessoa
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @param \Pessoa\Entity\Pessoa $pes
     * @return BoletoConfConta
     */
    public function setPes($pes)
    {
        $this->pes = $pes;

        return $this;
    }

    /**
     * @return \Financeiro\Entity\BoletoBanco
     */
    public function getBanc()
    {
        return $this->banc;
    }

    /**
     * @param \Financeiro\Entity\BoletoBanco $banc
     * @return BoletoConfConta
     */
    public function setBanc($banc)
    {
        $this->banc = $banc;

        return $this;
    }

    /**
     * @return \GerenciadorArquivos\Entity\Arquivo
     */
    public function getArq()
    {
        return $this->arq;
    }

    /**
     * @param \GerenciadorArquivos\Entity\Arquivo $arq
     * @return BoletoConfConta
     */
    public function setArq($arq)
    {
        $this->arq = $arq;

        return $this;
    }

    /**
     * @return string
     */
    public function getConfcontAgencia()
    {
        return $this->confcontAgencia;
    }

    /**
     * @param string $confcontAgencia
     * @return BoletoConfConta
     */
    public function setConfcontAgencia($confcontAgencia)
    {
        $this->confcontAgencia = $confcontAgencia;

        return $this;
    }

    /**
     * @return string
     */
    public function getConfcontAgenciaDigito()
    {
        return $this->confcontAgenciaDigito;
    }

    /**
     * @param string $confcontAgenciaDigito
     * @return BoletoConfConta
     */
    public function setConfcontAgenciaDigito($confcontAgenciaDigito)
    {
        $this->confcontAgenciaDigito = $confcontAgenciaDigito ? $confcontAgenciaDigito : '';

        return $this;
    }

    /**
     * @return string
     */
    public function getConfcontConta()
    {
        return $this->confcontConta;
    }

    /**
     * @param string $confcontConta
     * @return BoletoConfConta
     */
    public function setConfcontConta($confcontConta)
    {
        $this->confcontConta = $confcontConta;

        return $this;
    }

    /**
     * @return string
     */
    public function getConfcontContaDigito()
    {
        return $this->confcontContaDigito;
    }

    /**
     * @param string $confcontContaDigito
     * @return BoletoConfConta
     */
    public function setConfcontContaDigito($confcontContaDigito)
    {
        $this->confcontContaDigito = $confcontContaDigito;

        return $this;
    }

    /**
     * @return string
     */
    public function getConfcontCarteira()
    {
        return $this->confcontCarteira;
    }

    /**
     * @param string $confcontCarteira
     * @return BoletoConfConta
     */
    public function setConfcontCarteira($confcontCarteira)
    {
        $this->confcontCarteira = $confcontCarteira;

        return $this;
    }

    /**
     * @return string
     */
    public function getConfcontVariacao()
    {
        return $this->confcontVariacao;
    }

    /**
     * @param string $confcontVariacao
     * @return BoletoConfConta
     */
    public function setConfcontVariacao($confcontVariacao)
    {
        $this->confcontVariacao = $confcontVariacao;

        return $this;
    }

    /**
     * @return string
     */
    public function getConfcontConvenio()
    {
        return $this->confcontConvenio;
    }

    /**
     * @param string $confcontConvenio
     * @return BoletoConfConta
     */
    public function setConfcontConvenio($confcontConvenio)
    {
        $this->confcontConvenio = $confcontConvenio;

        return $this;
    }

    /**
     * @return string
     */
    public function getConfcontContrato()
    {
        return $this->confcontContrato;
    }

    /**
     * @param string $confcontContrato
     * @return BoletoConfConta
     */
    public function setConfcontContrato($confcontContrato)
    {
        $this->confcontContrato = $confcontContrato;

        return $this;
    }

    /**
     * @return string
     */
    public function getConfcontInstrucoes()
    {
        return $this->confcontInstrucoes;
    }

    /**
     * @param string $confcontInstrucoes
     * @return BoletoConfConta
     */
    public function setConfcontInstrucoes($confcontInstrucoes)
    {
        $this->confcontInstrucoes = $confcontInstrucoes;

        return $this;
    }

    /**
     * @return integer
     */
    public function getConfcontNossoNumeroInicial()
    {
        return $this->confcontNossoNumeroInicial;
    }

    /**
     * @param integer $confcontNossoNumeroInicial
     * @return BoletoConfConta
     */
    public function setConfcontNossoNumeroInicial($confcontNossoNumeroInicial)
    {
        $this->confcontNossoNumeroInicial = $confcontNossoNumeroInicial;

        return $this;
    }

    /**
     * @return integer
     */
    public function getConfcontRemessaSequencial()
    {
        return $this->confcontRemessaSequencial;
    }

    /**
     * @param $confcontRemessaSequencial
     * @return BoletoConfConta
     */
    public function setConfcontRemessaSequencial($confcontRemessaSequencial)
    {
        $this->confcontRemessaSequencial = $confcontRemessaSequencial;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'confcontId'                 => $this->getConfcontId(),
            'pes'                        => $this->getPes(),
            'banc'                       => $this->getBanc(),
            'arq'                        => $this->getArq(),
            'confcontAgencia'            => $this->getConfcontAgencia(),
            'confcontAgenciaDigito'      => $this->getConfcontAgenciaDigito(),
            'confcontConta'              => $this->getConfcontConta(),
            'confcontContaDigito'        => $this->getConfcontContaDigito(),
            'confcontCarteira'           => $this->getConfcontCarteira(),
            'confcontVariacao'           => $this->getConfcontVariacao(),
            'confcontConvenio'           => $this->getConfcontConvenio(),
            'confcontContrato'           => $this->getConfcontContrato(),
            'confcontInstrucoes'         => $this->getConfcontInstrucoes(),
            'confcontNossoNumeroInicial' => $this->getConfcontNossoNumeroInicial(),
            'confcontRemessaSequencial'  => $this->getConfcontRemessaSequencial(),
        );

        $array['pes']  = $this->getPes() ? $this->getPes()->getPesId() : null;
        $array['banc'] = $this->getBanc() ? $this->getBanc()->getBancId() : null;
        $array['arq']  = $this->getArq() ? $this->getArq()->getArqId() : null;

        return $array;
    }
}
