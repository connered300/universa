<?php

namespace Financeiro\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * FinanceiroRetorno
 *
 * @ORM\Table(name="financeiro__retorno")
 * @ORM\Entity
 * @LG\LG(id="retornoId",label="ArqId")
 * @Jarvis\Jarvis(title="Listagem de retorno",icon="fa fa-table")
 */
class FinanceiroRetorno
{
    /**
     * @var integer
     *
     * @ORM\Column(name="retorno_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="retorno_id")
     * @LG\Labels\Attributes(text="código retorno")
     * @LG\Querys\Conditions(type="=")
     */
    private $retornoId;

    /**
     * @var \GerenciadorArquivos\Entity\Arquivo
     * @ORM\ManyToOne(targetEntity="GerenciadorArquivos\Entity\Arquivo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="arq_id", referencedColumnName="arq_id")
     * })
     */
    private $arq;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * })
     */
    private $usuario;

    /**
     * @var integer
     *
     * @ORM\Column(name="retorno_banco", type="integer", nullable=false, length=11)
     * @LG\Labels\Property(name="retorno_banco")
     * @LG\Labels\Attributes(text="banco")
     * @LG\Querys\Conditions(type="=")
     */
    private $retornoBanco;

    /**
     * @var integer
     *
     * @ORM\Column(name="retorno_conta", type="integer", nullable=true, length=11)
     * @LG\Labels\Property(name="retorno_conta")
     * @LG\Labels\Attributes(text="conta")
     * @LG\Querys\Conditions(type="=")
     */
    private $retornoConta;

    /**
     * @var integer
     *
     * @ORM\Column(name="retorno_convenio", type="integer", nullable=true, length=11)
     * @LG\Labels\Property(name="retorno_convenio")
     * @LG\Labels\Attributes(text="convenio")
     * @LG\Querys\Conditions(type="=")
     */
    private $retornoConvenio;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="retorno_data", type="datetime", nullable=false)
     * @LG\Labels\Property(name="retorno_data")
     * @LG\Labels\Attributes(text="data")
     * @LG\Querys\Conditions(type="=")
     */
    private $retornoData;

    /**
     * @var string
     *
     * @ORM\Column(name="retorno_status", type="text", nullable=false)
     * @LG\Labels\Property(name="retorno_status")
     * @LG\Labels\Attributes(text="Situação do arquivo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $retornoStatus = 'Finalizado';

    /**
     * @var string
     *
     * @ORM\Column(name="retorno_log", type="text", nullable=true)
     * @LG\Labels\Property(name="retorno_log")
     * @LG\Labels\Attributes(text="Situação do processamento do retorno")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $retornoLog;

    /**
     * @return integer
     */
    public function getRetornoId()
    {
        return $this->retornoId;
    }

    /**
     * @param integer $retornoId
     * @return FinanceiroRetorno
     */
    public function setRetornoId($retornoId)
    {
        $this->retornoId = $retornoId;

        return $this;
    }

    /**
     * @return \GerenciadorArquivos\Entity\Arquivo
     */
    public function getArq()
    {
        return $this->arq;
    }

    /**
     * @param \GerenciadorArquivos\Entity\Arquivo $arq
     * @return FinanceiroRetorno
     */
    public function setArq($arq)
    {
        $this->arq = $arq;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuario
     * @return FinanceiroRetorno
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * @return integer
     */
    public function getRetornoBanco()
    {
        return $this->retornoBanco;
    }

    /**
     * @param integer $retornoBanco
     * @return FinanceiroRetorno
     */
    public function setRetornoBanco($retornoBanco)
    {
        $this->retornoBanco = $retornoBanco;

        return $this;
    }

    /**
     * @return integer
     */
    public function getRetornoConta()
    {
        return $this->retornoConta;
    }

    /**
     * @param integer $retornoConta
     * @return FinanceiroRetorno
     */
    public function setRetornoConta($retornoConta)
    {
        $this->retornoConta = $retornoConta;

        return $this;
    }

    /**
     * @return int
     */
    public function getRetornoConvenio()
    {
        return $this->retornoConvenio;
    }

    /**
     * @param int $retornoConvenio
     * @return FinanceiroRetorno
     */
    public function setRetornoConvenio($retornoConvenio)
    {
        $this->retornoConvenio = $retornoConvenio;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getRetornoData($format = false)
    {
        $retornoData = $this->retornoData;

        if ($format && $retornoData) {
            $retornoData = $retornoData->format('d/m/Y H:i:s');
        }

        return $retornoData;
    }

    /**
     * @param \Datetime $retornoData
     * @return FinanceiroRetorno
     */
    public function setRetornoData($retornoData)
    {
        if ($retornoData) {
            if (is_string($retornoData)) {
                $retornoData = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $retornoData
                );
                $retornoData = new \Datetime($retornoData);
            }
        } else {
            $retornoData = null;
        }
        $this->retornoData = $retornoData;

        return $this;
    }

    /**
     * @return string
     */
    public function getRetornoStatus()
    {
        return $this->retornoStatus;
    }

    /**
     * @param $retornoStatus string
     * @return \Financeiro\Entity\FinanceiroRetorno
     */
    public function setRetornoStatus($retornoStatus)
    {
        $this->retornoStatus = $retornoStatus;

        return $this;
    }

    /**
     * @return string
     */
    public function getRetornoLog()
    {
        return $this->retornoLog;
    }

    /**
     * @param $retornoLog string
     * @return \Financeiro\Entity\FinanceiroRetorno
     */
    public function setRetornoLog($retornoLog)
    {
        $this->retornoLog = $retornoLog;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'retornoId'       => $this->getRetornoId(),
            'arq'             => $this->getArq(),
            'usuario'         => $this->getUsuario(),
            'retornoBanco'    => $this->getRetornoBanco(),
            'retornoConta'    => $this->getRetornoConta(),
            'retornoConvenio' => $this->getRetornoConvenio(),
            'retornoData'     => $this->getRetornoData(true),
            'retornoStatus'   => $this->getRetornoStatus(),
            'retornoLog'      => $this->getRetornoLog()
        );

        $array['arq']     = $this->getArq() ? $this->getArq()->getArqId() : null;
        $array['usuario'] = $this->getUsuario() ? $this->getUsuario()->getId() : null;

        return $array;
    }
}
