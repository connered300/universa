<?php

namespace Financeiro\Entity;

use \Financeiro\Service\FinanceiroTituloConfig as ServiceTituloConfig;
use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * FinanceiroTituloConfig
 *
 * @ORM\Table(name="financeiro__titulo_config")
 * @ORM\Entity
 * @LG\LG(id="tituloconfId",label="TituloconfPercentDesc")
 * @Jarvis\Jarvis(title="Listagem de configuração de título",icon="fa fa-table")
 */
class FinanceiroTituloConfig
{
    /**
     * @var integer
     *
     * @ORM\Column(name="tituloconf_id", type="integer", nullable=false, length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="tituloconf_id")
     * @LG\Labels\Attributes(text="código")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloconfId;

    /**
     * @var \Matricula\Entity\CampusCurso
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\CampusCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cursocampus_id", referencedColumnName="cursocampus_id")
     * })
     */
    private $cursocampus;

    /**
     * @var string
     * @ORM\Column(name="tituloconf_nome", type="string",nullable=false)
     */
    private $confignome;

    /**
     * @var \Matricula\Entity\AcadperiodoLetivo
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoLetivo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="per_id", referencedColumnName="per_id")
     * })
     */
    private $per;

    /**
     * @var \Financeiro\Entity\FinanceiroTituloTipo
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\FinanceiroTituloTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipotitulo_id", referencedColumnName="tipotitulo_id")
     * })
     */
    private $tipotitulo;

    /**
     * @var \Financeiro\Entity\BoletoConfConta
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\BoletoConfConta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="confcont_id", referencedColumnName="confcont_id")
     * })
     */
    private $confcont;

    /**
     * @var integer
     *
     * @ORM\Column(name="tituloconf_dia_venc", type="integer", nullable=false, length=11)
     * @LG\Labels\Property(name="tituloconf_dia_venc")
     * @LG\Labels\Attributes(text="vencimento dia de")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloconfDiaVenc;

    /**
     * @var integer
     *
     * @ORM\Column(name="tituloconf_dia_desc", type="integer", nullable=false, length=11)
     * @LG\Labels\Property(name="tituloconf_dia_desc")
     * @LG\Labels\Attributes(text="descrição dia de")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloconfDiaDesc;

    /**
     * @var float
     *
     * @ORM\Column(name="tituloconf_valor_desc", type="float", nullable=true)
     * @LG\Labels\Property(name="tituloconf_valor_desc")
     * @LG\Labels\Attributes(text="descrição valor de")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloconfValorDesc;

    /**
     * @var float
     *
     * @ORM\Column(name="tituloconf_percent_desc", type="float", nullable=true)
     * @LG\Labels\Property(name="tituloconf_percent_desc")
     * @LG\Labels\Attributes(text="descrição percentual de")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloconfPercentDesc;

    /**
     * @var float
     *
     * @ORM\Column(name="tituloconf_multa", type="float", nullable=true, length=10)
     * @LG\Labels\Property(name="tituloconf_multa")
     * @LG\Labels\Attributes(text="multa de")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloconfMulta;

    /**
     * @var float
     *
     * @ORM\Column(name="tituloconf_juros", type="float", nullable=true, length=10, precision=5)
     * @LG\Labels\Property(name="tituloconf_juros")
     * @LG\Labels\Attributes(text="juros de")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloconfJuros;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="tituloconf_data_inicio", type="date", nullable=true)
     * @LG\Labels\Property(name="tituloconf_data_inicio")
     * @LG\Labels\Attributes(text="início data de")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloconfDataInicio;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="tituloconf_data_fim", type="date", nullable=true)
     * @LG\Labels\Property(name="tituloconf_data_fim")
     * @LG\Labels\Attributes(text="fim data de")
     * @LG\Querys\Conditions(type="=")
     */
    private $tituloconfDataFim;

    /**
     * @var integer
     *
     * @ORM\Column(name="tituloconf_dia_juros", type="integer", nullable=true)
     */
    private $tituloconfDiaJuros;

    /**
     * @var string
     *
     * @ORM\Column(name="tituloconf_instrucao_boleto_automatica", type="string",nullable=true)
     */
    private $titutloconfInstrucaoAutomatica;

    /**
     * @var string
     *
     * @ORM\Column(name="tituloconf_desconto_incentivo_acumulativo", type="string", nullable=true, length=4)
     */
    private $tituloconfDescontoIncentivoAcumulativo;

    /**
     * @var integer
     * @ORM\Column(name="tituloconf_dia_desc2", type="integer", nullable=true)
     */
    private $tituloconfDiaDesc2;

    /**
     * @var float
     *
     * @ORM\Column(name="tituloconf_valor_desc2", type="float", nullable=true)
     */
    private $tituloconfValordesc2;

    /**
     * @var float
     * @ORM\Column(name="tituloconf_percent_desc2", type="float", nullable=true)
     */
    private $tituloconfPercentDesc2;

    /**
     * @var string
     *
     * @ORM\Column(name="tituloconf_vencimento_fixo", type="string", nullable=true, length=4)
     */
    private $tituloconfVencimentoFixo;

    /**
     * @var string
     *
     * @ORM\Column(name="tituloconf_vencimento_alteravel", type="string", nullable=true, length=4)
     */
    private $tituloconfVencimentoAlteravel;

    /**
     * @return integer
     *
     */
    public function getTituloconfId()
    {
        return $this->tituloconfId;
    }

    /**
     * @param integer $tituloconfId
     * @return FinanceiroTituloConfig
     */
    public function setTituloconfId($tituloconfId)
    {
        $this->tituloconfId = $tituloconfId;

        return $this;
    }

    /**
     * @return \Matricula\Entity\CampusCurso
     */
    public function getCursocampus()
    {
        return $this->cursocampus;
    }

    /**
     * @param \Matricula\Entity\CampusCurso $cursocampus
     * @return FinanceiroTituloConfig
     */
    public function setCursocampus($cursocampus)
    {
        $this->cursocampus = $cursocampus;

        return $this;
    }

    /**
     * @return \Matricula\Entity\AcadperiodoLetivo
     */
    public function getPer()
    {
        return $this->per;
    }

    /**
     * @param \Matricula\Entity\AcadperiodoLetivo $per
     * @return FinanceiroTituloConfig
     */
    public function setPer($per)
    {
        $this->per = $per;

        return $this;
    }

    /**
     * @return \Financeiro\Entity\FinanceiroTituloTipo
     */
    public function getTipotitulo()
    {
        return $this->tipotitulo;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroTituloTipo $tipotitulo
     * @return FinanceiroTituloConfig
     */
    public function setTipotitulo($tipotitulo)
    {
        $this->tipotitulo = $tipotitulo;

        return $this;
    }

    /**
     * @return \Financeiro\Entity\BoletoConfConta
     */
    public function getConfcont()
    {
        return $this->confcont;
    }

    /**
     * @param \Financeiro\Entity\BoletoConfConta $confcont
     * @return FinanceiroTituloConfig
     */
    public function setConfcont($confcont)
    {
        $this->confcont = $confcont;

        return $this;
    }

    /**
     * @param bool|false $mes
     * @param bool|false $ano
     * @return bool|string
     */
    private static function ultimoDiaMesCorrente($mes = false, $ano = false)
    {
        if (!$mes) {
            $mes = date('m');
        }

        if (!$ano) {
            $ano = date('Y');
        }

        $lastday = date('t', strtotime($ano . '-' . $mes . '-01'));

        return $lastday;
    }

    /**
     * @return integer
     */
    public function getTituloconfDiaVenc($formatado = false)
    {
        $diaVencimento = $this->tituloconfDiaVenc;

        //Se o vencimento for fixo e os dia fixo estiver fora do intervalo de dias do mês,
        //coloca o vencimento para o dia o dia mais próximo
        if ($this->verificarSeVencimentoFixo()) {
            $ultimoDia = self::ultimoDiaMesCorrente();

            if ($diaVencimento < 1) {
                $diaVencimento = 1;
            } elseif ($diaVencimento > $ultimoDia) {
                $diaVencimento = $ultimoDia;
            }
        }

        if ($formatado) {
            $diaVencimento = str_pad($diaVencimento, 2, '0', STR_PAD_LEFT);
        }

        return $diaVencimento;
    }

    /**
     * @param integer $tituloconfDiaVenc
     * @return FinanceiroTituloConfig
     */
    public function setTituloconfDiaVenc($tituloconfDiaVenc)
    {
        $this->tituloconfDiaVenc = (int)$tituloconfDiaVenc;

        return $this;
    }

    /**
     * @return integer
     */
    public function getTituloconfDiaDesc()
    {
        return $this->tituloconfDiaDesc;
    }

    /**
     * @param integer $tituloconfDiaDesc
     * @return FinanceiroTituloConfig
     */
    public function setTituloconfDiaDesc($tituloconfDiaDesc)
    {
        $this->tituloconfDiaDesc = (int)$tituloconfDiaDesc;

        return $this;
    }

    /**
     * @return float
     */
    public function getTituloconfValorDesc()
    {
        return $this->tituloconfValorDesc;
    }

    /**
     * @param float $tituloconfValorDesc
     * @return FinanceiroTituloConfig
     */
    public function setTituloconfValorDesc($tituloconfValorDesc)
    {
        $this->tituloconfValorDesc = (float)$tituloconfValorDesc;

        return $this;
    }

    /**
     * @return float
     */
    public function getTituloconfPercentDesc()
    {
        return $this->tituloconfPercentDesc;
    }

    /**
     * @param float $tituloconfPercentDesc
     * @return FinanceiroTituloConfig
     */
    public function setTituloconfPercentDesc($tituloconfPercentDesc)
    {
        $this->tituloconfPercentDesc = (float)$tituloconfPercentDesc;

        return $this;
    }

    /**
     * @return float
     */
    public function getTituloconfMulta()
    {
        return $this->tituloconfMulta;
    }

    /**
     * @param float $tituloconfMulta
     * @return FinanceiroTituloConfig
     */
    public function setTituloconfMulta($tituloconfMulta)
    {
        $this->tituloconfMulta = (float)$tituloconfMulta;

        return $this;
    }

    /**
     * @return float
     */
    public function getTituloconfJuros()
    {
        return $this->tituloconfJuros;
    }

    /**
     * @param float $tituloconfJuros
     * @return FinanceiroTituloConfig
     */
    public function setTituloconfJuros($tituloconfJuros)
    {
        $this->tituloconfJuros = (float)$tituloconfJuros;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getTituloconfDataInicio($format = false)
    {
        $tituloconfDataInicio = $this->tituloconfDataInicio;

        if ($format && $tituloconfDataInicio) {
            $tituloconfDataInicio = $tituloconfDataInicio->format('d/m/Y');
        }

        return $tituloconfDataInicio;
    }

    /**
     * @param \Datetime $tituloconfDataInicio
     * @return FinanceiroTituloConfig
     */
    public function setTituloconfDataInicio($tituloconfDataInicio)
    {
        if ($tituloconfDataInicio) {
            if (is_string($tituloconfDataInicio)) {
                $tituloconfDataInicio = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $tituloconfDataInicio
                );
                $tituloconfDataInicio = new \Datetime($tituloconfDataInicio);
            }
        } else {
            $tituloconfDataInicio = null;
        }

        $this->tituloconfDataInicio = $tituloconfDataInicio;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getTituloconfDataFim($format = false)
    {
        $tituloconfDataFim = $this->tituloconfDataFim;

        if ($format && $tituloconfDataFim) {
            $tituloconfDataFim = $tituloconfDataFim->format('d/m/Y');
        }

        return $tituloconfDataFim;
    }

    /**
     * @param \Datetime $tituloconfDataFim
     * @return FinanceiroTituloConfig
     */
    public function setTituloconfDataFim($tituloconfDataFim)
    {
        if ($tituloconfDataFim) {
            if (is_string($tituloconfDataFim)) {
                $tituloconfDataFim = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $tituloconfDataFim
                );
                $tituloconfDataFim = new \Datetime($tituloconfDataFim);
            }
        } else {
            $tituloconfDataFim = null;
        }

        $this->tituloconfDataFim = $tituloconfDataFim;

        return $this;
    }

    /** @return string */
    public function getTituloconfDescontoIncentivoAcumulativo()
    {
        return $this->tituloconfDescontoIncentivoAcumulativo;
    }

    /** @param string $tituloconfDescontoIncentivoAcumulativo
     * @return FinanceiroTituloConfig
     */
    public function setTituloconfDescontoIncentivoAcumulativo($tituloconfDescontoIncentivoAcumulativo)
    {
        $this->tituloconfDescontoIncentivoAcumulativo = $tituloconfDescontoIncentivoAcumulativo;

        return $this;
    }

    /** @return string */
    public function getTituloconfNome()
    {
        return $this->confignome;
    }

    /**
     * @param $cursoNome
     * @return $this
     */
    public function setTituloconfNome($cursoNome)
    {
        $this->confignome = $cursoNome;

        return $this;
    }

    /** @return integer */
    public function getTituloconfDiaDesc2()
    {
        return $this->tituloconfDiaDesc2;
    }

    /**
     * @param $diaDesc
     * @return $this
     */
    public function setTituloconfDiaDesc2($diaDesc)
    {
        $this->tituloconfDiaDesc2 = $diaDesc;

        return $this;
    }

    /** @return float */
    public function getTituloconfValorDesc2()
    {
        return $this->tituloconfValordesc2;
    }

    /**
     * @param $valorDesconto
     * @return $this
     */
    public function setTituloconfValorDesc2($valorDesconto)
    {
        $this->tituloconfValordesc2 = $valorDesconto;

        return $this;
    }

    /** @return float */
    public function getTituloconfPercentDesc2()
    {
        return $this->tituloconfPercentDesc2;
    }

    /**
     * @param $percentDesc
     * @return $this
     */
    public function setTituloconfPercentDesc2($percentDesc)
    {
        $this->tituloconfPercentDesc2 = $percentDesc;

        return $this;
    }

    /**
     * @return string
     */
    public function getTituloconfVencimentoFixo()
    {
        return $this->tituloconfVencimentoFixo;
    }

    /**
     * @return string
     */
    public function verificarSeVencimentoFixo()
    {
        return $this->tituloconfVencimentoFixo == ServiceTituloConfig::TITULOCONF_VENCIMENTO_FIXO_SIM;
    }

    /**
     * @param $valor
     * @return $this
     */
    public function setTituloconfVencimentoFixo($valor)
    {
        $this->tituloconfVencimentoFixo = $valor;

        return $this;
    }

    /**
     * @return  string
     */
    public function getTituloconfVencimentoAlteravel()
    {
        return $this->tituloconfVencimentoAlteravel;
    }

    /**
     * @return bool
     */
    public function verificaSeVencimentoAlteravel()
    {
        return $this->tituloconfVencimentoAlteravel == ServiceTituloConfig::TITULOCONF_VENCIMENTO_ALTERAVEL_SIM;
    }

    /**
     * @param $valor
     * @return $this
     */
    public function setTituloconfVencimentoAlteravel($valor)
    {
        $this->tituloconfVencimentoAlteravel = $valor;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'tituloconfId'                           => $this->getTituloconfId(),
            'tituloconfNome'                         => $this->getTituloconfNome(),
            'cursocampus'                            => $this->getCursocampus(),
            'per'                                    => $this->getPer(),
            'tipotitulo'                             => $this->getTipotitulo(),
            'confcont'                               => $this->getConfcont(),
            'tituloconfDiaVenc'                      => $this->getTituloconfDiaVenc(),
            'tituloconfDiaDesc'                      => $this->getTituloconfDiaDesc(),
            'tituloconfValorDesc'                    => $this->getTituloconfValorDesc(),
            'tituloconfPercentDesc'                  => $this->getTituloconfPercentDesc(),
            'tituloconfMulta'                        => $this->getTituloconfMulta(),
            'tituloconfJuros'                        => $this->getTituloconfJuros(),
            'tituloconfDataInicio'                   => $this->getTituloconfDataInicio(true),
            'tituloconfDataFim'                      => $this->getTituloconfDataFim(true),
            'tituloconfDiaJuros'                     => $this->getTituloconfDiaJuros(),
            'titutloconfInstrucaoAutomatica'         => $this->getTitutloconfInstrucaoAutomatica(),
            'tituloconfDescontoIncentivoAcumulativo' => $this->getTituloconfDescontoIncentivoAcumulativo(),
            'tituloconfDiaDesc2'                     => $this->getTituloconfDiaDesc2(),
            'tituloconfValordesc2'                   => $this->getTituloconfValorDesc2(),
            'tituloconfPercentDesc2'                 => $this->getTituloconfPercentDesc2(),
            'tituloconfVencimentoFixo'               => $this->getTituloconfVencimentoFixo(),
            'tituloconfVencimentoAlteravel'          => $this->getTituloconfVencimentoAlteravel()

        );

        $array['cursocampus'] = $this->getCursocampus() ? $this->getCursocampus()->getCursocampusId() : null;
        $array['per']         = $this->getPer() ? $this->getPer()->getPerId() : null;
        $array['tipotitulo']  = $this->getTipotitulo() ? $this->getTipotitulo()->getTipotituloId() : null;
        $array['confcont']    = $this->getConfcont() ? $this->getConfcont()->getConfcontId() : null;

        return $array;
    }

    /**
     * @return int
     */
    public function getTituloconfDiaJuros()
    {
        return $this->tituloconfDiaJuros;
    }

    /**
     * @param $tituloconfDiaJuros
     * @return $this
     */
    public function setTituloconfDiaJuros($tituloconfDiaJuros)
    {
        $this->tituloconfDiaJuros = $tituloconfDiaJuros;

        return $this;
    }

    /**
     * @return string
     *
     */
    public function getTitutloconfInstrucaoAutomatica()
    {
        return $this->titutloconfInstrucaoAutomatica;
    }

    /**
     * @param $titutloconfInstrucaoAutomatica
     * @return $this
     */
    public function setTitutloconfInstrucaoAutomatica($titutloconfInstrucaoAutomatica)
    {
        $this->titutloconfInstrucaoAutomatica = $titutloconfInstrucaoAutomatica;

        return $this;
    }
}
