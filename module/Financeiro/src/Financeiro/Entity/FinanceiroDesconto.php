<?php

namespace Financeiro\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * FinanceiroDesconto
 *
 * @ORM\Table(name="financeiro__desconto")
 * @ORM\Entity
 * @LG\LG(id="descontoId",label="DesctipoId")
 * @Jarvis\Jarvis(title="Listagem de desconto",icon="fa fa-table")
 */
class FinanceiroDesconto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="desconto_id", type="integer", nullable=false, length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="desconto_id")
     * @LG\Labels\Attributes(text="código desconto")
     * @LG\Querys\Conditions(type="=")
     */
    private $descontoId;
    /**
     * @var \Financeiro\Entity\FinanceiroDescontoTipo
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\FinanceiroDescontoTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="desctipo_id", referencedColumnName="desctipo_id")
     * })
     */
    private $desctipo;
    /**
     * @var \Acesso\Entity\AcessoPessoas
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * })
     */
    private $usuario;
    /**
     * @var float
     *
     * @ORM\Column(name="desconto_valor", type="float", nullable=true, length=10)
     * @LG\Labels\Property(name="desconto_valor")
     * @LG\Labels\Attributes(text="valor")
     * @LG\Querys\Conditions(type="=")
     */
    private $descontoValor;
    /**
     * @var float
     *
     * @ORM\Column(name="desconto_percentual", type="float", nullable=true, length=6)
     * @LG\Labels\Property(name="desconto_percentual")
     * @LG\Labels\Attributes(text="percentual")
     * @LG\Querys\Conditions(type="=")
     */
    private $descontoPercentual;
    /**
     * @var string
     *
     * @ORM\Column(name="desconto_status", type="string", nullable=false, length=7)
     * @LG\Labels\Property(name="desconto_status")
     * @LG\Labels\Attributes(text="status")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $descontoStatus;
    /**
     * @var \Matricula\Entity\AcadgeralAluno
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadgeralAluno")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="aluno_id", referencedColumnName="aluno_id")
     * })
     */
    private $aluno;
    /**
     * @var string
     *
     * @ORM\Column(name="desconto_observacao", type="string", nullable=false)
     * @LG\Labels\Property(name="desconto_observacao")
     * @LG\Labels\Attributes(text="observacao")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $descontoObservacao;
    /**
     * @return integer
     */
    public function getDescontoId()
    {
        return $this->descontoId;
    }

    /**
     * @param integer $descontoId
     * @return FinanceiroDesconto
     */
    public function setDescontoId($descontoId)
    {
        $this->descontoId = $descontoId;

        return $this;
    }

    /**
     * @return \Financeiro\Entity\FinanceiroDescontoTipo
     */
    public function getDesctipo()
    {
        return $this->desctipo;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroDescontoTipo $desctipo
     * @return FinanceiroDesconto
     */
    public function setDesctipo($desctipo)
    {
        $this->desctipo = $desctipo;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuario
     * @return FinanceiroDesconto
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * @return float
     */
    public function getDescontoValor()
    {
        return $this->descontoValor;
    }

    /**
     * @param float $descontoValor
     * @return FinanceiroDesconto
     */
    public function setDescontoValor($descontoValor)
    {
        $this->descontoValor = $descontoValor ? $descontoValor : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getDescontoPercentual()
    {
        return $this->descontoPercentual;
    }

    /**
     * @param float $descontoPercentual
     * @return FinanceiroDesconto
     */
    public function setDescontoPercentual($descontoPercentual)
    {
        $this->descontoPercentual = $descontoPercentual ? $descontoPercentual : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescontoStatus()
    {
        return $this->descontoStatus;
    }

    /**
     * @param string $descontoStatus
     * @return FinanceiroDesconto
     */
    public function setDescontoStatus($descontoStatus)
    {
        $this->descontoStatus = $descontoStatus;

        return $this;
    }

    /**
     * @return \Matricula\Entity\AcadgeralAluno
     */
    public function getAlunocurso()
    {
        return $this->aluno;
    }

    /**
     * @param \Matricula\Entity\AcadgeralAluno $aluno
     * @return FinanceiroDesconto
     */
    public function setAlunocurso($aluno)
    {
        $this->aluno = $aluno;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescontoObservacao()
    {
        return $this->descontoObservacao;
    }

    /**
     * @param string $descontoObservacao
     */
    public function setDescontoObservacao($descontoObservacao)
    {
        $this->descontoObservacao = $descontoObservacao;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'descontoId'         => $this->getDescontoId(),
            'desctipo'           => $this->getDesctipo(),
            'usuario'            => $this->getUsuario(),
            'descontoValor'      => (float)$this->getDescontoValor(),
            'descontoPercentual' => (float)$this->getDescontoPercentual(),
            'descontoStatus'     => $this->getDescontoStatus(),
            'aluno'              => $this->getAlunocurso(),
            'descontoObservacao' => $this->getDescontoObservacao(),
        );

        $array['desctipo'] = $this->getDesctipo() ? $this->getDesctipo()->getDesctipoId() : null;
        $array['usuario']  = $this->getUsuario() ? $this->getUsuario()->getId() : null;
        $array['aluno']    = $this->getAlunocurso() ? $this->getAlunocurso()->getAlunoId() : null;

        return $array;
    }
}
