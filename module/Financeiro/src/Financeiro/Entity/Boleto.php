<?php

namespace Financeiro\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * Boleto
 *
 * @ORM\Table(name="boleto", indexes={@ORM\Index(name="fk_boleto_configuracao_boleto1_idx", columns={"confcont_id"}),@ORM\Index(name="bol_nossonumero_confcontid_unicidade", columns={"confcont_id","bol_nosso_numero"}), @ORM\Index(name="fk_boleto_titulo1_idx", columns={"titulo_id"}),@ORM\Index(name="fk_boleto_baixas_efetuadas1_idx", columns={"baixa_id"}), @ORM\Index(name="fk_boleto_financeiro__pagamento1_idx", columns={"pagamento_id"})})
 * @ORM\Entity
 * @LG\LG(id="bolId",label="BolDescontoValor")
 * @Jarvis\Jarvis(title="Listagem de boleto",icon="fa fa-table")
 */
class Boleto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="bol_id", type="integer", nullable=false, length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="bol_id")
     * @LG\Labels\Attributes(text="código boleto")
     * @LG\Querys\Conditions(type="=")
     */
    private $bolId;

    /**
     * @var \Financeiro\Entity\BoletoConfConta
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\BoletoConfConta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="confcont_id", referencedColumnName="confcont_id")
     * })
     */
    private $confcont;

    /**
     * @var \Financeiro\Entity\FinanceiroTitulo
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\FinanceiroTitulo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="titulo_id", referencedColumnName="titulo_id")
     * })
     */
    private $titulo;

    /**
     * @var FinanceiroPagamento
     * @ORM\ManyToOne(targetEntity="FinanceiroPagamento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pagamento_id", referencedColumnName="pagamento_id")
     * })
     */
    private $pagamento;

    /**
     * @var string
     *
     * @ORM\Column(name="bol_nosso_numero", type="string", nullable=false, length=20)
     * @LG\Labels\Property(name="bol_nosso_numero")
     * @LG\Labels\Attributes(text="número nosso")
     * @LG\Querys\Conditions(type="=")
     */
    private $bolNossoNumero;

    /**
     * @var integer
     *
     * @ORM\Column(name="bol_numero_documento", type="integer", nullable=false, length=20)
     * @LG\Labels\Property(name="bol_numero_documento")
     * @LG\Labels\Attributes(text="documento número")
     * @LG\Querys\Conditions(type="=")
     */
    private $bolNumeroDocumento;

    /**
     * @var string
     *
     * @ORM\Column(name="bol_estado", type="string", nullable=false, length=4)
     * @LG\Labels\Property(name="bol_estado")
     * @LG\Labels\Attributes(text="estado")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $bolEstado;

    /**
     * @var float
     *
     * @ORM\Column(name="bol_valor_unitario", type="float", nullable=true)
     * @LG\Labels\Property(name="bol_valor_unitario")
     * @LG\Labels\Attributes(text="unitário valor")
     * @LG\Querys\Conditions(type="=")
     */
    private $bolValorUnitario;

    /**
     * @var float
     *
     * @ORM\Column(name="bol_quantidade", type="float", nullable=true)
     * @LG\Labels\Property(name="bol_quantidade")
     * @LG\Labels\Attributes(text="quantidade")
     * @LG\Querys\Conditions(type="=")
     */
    private $bolQuantidade;

    /**
     * @var string
     *
     * @ORM\Column(name="bol_demostrativo", type="text", nullable=true)
     * @LG\Labels\Property(name="bol_demostrativo")
     * @LG\Labels\Attributes(text="demostrativo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $bolDemostrativo;

    /**
     * @var string
     *
     * @ORM\Column(name="bol_instrucoes", type="text", nullable=true)
     * @LG\Labels\Property(name="bol_instrucoes")
     * @LG\Labels\Attributes(text="instrucões")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $bolInstrucoes;

    /**
     * @var integer
     *
     * @ORM\Column(name="bol_via", type="integer", nullable=false, length=11)
     * @LG\Labels\Property(name="bol_via")
     * @LG\Labels\Attributes(text="via")
     * @LG\Querys\Conditions(type="=")
     */
    private $bolVia;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="bol_desconto_data", type="datetime", nullable=true)
     * @LG\Labels\Property(name="bol_desconto_data")
     * @LG\Labels\Attributes(text="data desconto")
     * @LG\Querys\Conditions(type="=")
     */
    private $bolDescontoData;

    /**
     * @var float
     *
     * @ORM\Column(name="bol_desconto_valor", type="float", nullable=true)
     * @LG\Labels\Property(name="bol_desconto_valor")
     * @LG\Labels\Attributes(text="valor desconto")
     * @LG\Querys\Conditions(type="=")
     */
    private $bolDescontoValor;

    /**
     * @var float
     *
     * @ORM\Column(name="bol_multa", type="float", nullable=true, length=10)
     * @LG\Labels\Property(name="bol_multa")
     * @LG\Labels\Attributes(text="multa")
     * @LG\Querys\Conditions(type="=")
     */
    private $bolMulta;

    /**
     * @var float
     *
     * @ORM\Column(name="bol_juros", type="float", nullable=true, length=10)
     * @LG\Labels\Property(name="bol_juros")
     * @LG\Labels\Attributes(text="juros")
     * @LG\Querys\Conditions(type="=")
     */
    private $bolJuros;

    // TODO: UNIFICAR CAMPOS DE VALORES DE ACRESCIMOS
    /**
     * @var float
     *
     * @ORM\Column(name="bol_acrescimos", type="float", nullable=true, length=10)
     * @LG\Labels\Property(name="bol_acrescimos")
     * @LG\Labels\Attributes(text="acrescimos")
     * @LG\Querys\Conditions(type="=")
     */
    private $bolAcrescimos;

    /**
     * @var float
     *
     * @ORM\Column(name="bol_valor_pago", type="float", nullable=true, length=10)
     * @LG\Labels\Property(name="bol_valor_pago")
     * @LG\Labels\Attributes(text="pago valor")
     * @LG\Querys\Conditions(type="=")
     */
    private $bolValorPago;

    /**
     * @var string
     *
     * @ORM\Column(name="bol_carteira",type="string", nullable=false, length=2)
     * @LG\Labels\Property(name="bol_carteira")
     * @LG\Labels\Attributes(text="carteira")
     * @LG\Querys\Conditions(type="=")
     */
    private $bolCarteira = '24';

    //    /**
    //     * @var \Boleto\Entity\BoletoBaixa
    //     *
    //     * @ORM\ManyToOne(targetEntity="Boleto\Entity\BoletoBaixa")
    //     * @ORM\JoinColumns({
    //     *   @ORM\JoinColumn(name="baixa_id", referencedColumnName="baixa_id")
    //     * })
    //     */
    //    private $baixa;

    /**
     * @return integer
     */
    public function getBolId()
    {
        return $this->bolId;
    }

    /**
     * @param integer $bolId
     * @return Boleto
     */
    public function setBolId($bolId)
    {
        $this->bolId = $bolId;

        return $this;
    }

    /**
     * @return \Financeiro\Entity\BoletoConfConta
     */
    public function getConfcont()
    {
        return $this->confcont;
    }

    /**
     * @param \Financeiro\Entity\BoletoConfConta $confcont
     * @return Boleto
     */
    public function setConfcont($confcont)
    {
        $this->confcont = $confcont;

        return $this;
    }

    /**
     * @return \Financeiro\Entity\FinanceiroTitulo
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroTitulo $titulo
     * @return Boleto
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * @return FinanceiroPagamento
     */
    public function getPagamento()
    {
        return $this->pagamento;
    }

    /**
     * @param FinanceiroPagamento $pagamento
     * @return Boleto
     */
    public function setPagamento($pagamento)
    {
        $this->pagamento = $pagamento;

        return $this;
    }

    /**
     * @return string
     */
    public function getBolNossoNumero()
    {
        return $this->bolNossoNumero;
    }

    /**
     * @param string $bolNossoNumero
     * @return Boleto
     */
    public function setBolNossoNumero($bolNossoNumero)
    {
        $this->bolNossoNumero = $bolNossoNumero;

        return $this;
    }

    /**
     * @return integer
     */
    public function getBolNumeroDocumento()
    {
        return $this->bolNumeroDocumento;
    }

    /**
     * @param integer $bolNumeroDocumento
     * @return Boleto
     */
    public function setBolNumeroDocumento($bolNumeroDocumento)
    {
        $this->bolNumeroDocumento = $bolNumeroDocumento;

        return $this;
    }

    /**
     * @return string
     */
    public function getBolEstado()
    {
        return $this->bolEstado;
    }

    /**
     * @param string $bolEstado
     * @return Boleto
     */
    public function setBolEstado($bolEstado)
    {
        $this->bolEstado = $bolEstado;

        return $this;
    }

    /**
     * @return float
     */
    public function getBolValorUnitario()
    {
        return $this->bolValorUnitario;
    }

    /**
     * @param float $bolValorUnitario
     * @return Boleto
     */
    public function setBolValorUnitario($bolValorUnitario)
    {
        $this->bolValorUnitario = $bolValorUnitario;

        return $this;
    }

    /**
     * @return float
     */
    public function getBolQuantidade()
    {
        return $this->bolQuantidade;
    }

    /**
     * @param float $bolQuantidade
     * @return Boleto
     */
    public function setBolQuantidade($bolQuantidade)
    {
        $this->bolQuantidade = $bolQuantidade;

        return $this;
    }

    /**
     * @return string
     */
    public function getBolDemostrativo()
    {
        return $this->bolDemostrativo;
    }

    /**
     * @param string $bolDemostrativo
     * @return Boleto
     */
    public function setBolDemostrativo($bolDemostrativo)
    {
        $this->bolDemostrativo = $bolDemostrativo;

        return $this;
    }

    /**
     * @return string
     */
    public function getBolInstrucoes()
    {
        return $this->bolInstrucoes;
    }

    /**
     * @param string $bolInstrucoes
     * @return Boleto
     */
    public function setBolInstrucoes($bolInstrucoes)
    {
        $this->bolInstrucoes = $bolInstrucoes;

        return $this;
    }

    /**
     * @return integer
     */
    public function getBolVia()
    {
        return $this->bolVia;
    }

    /**
     * @param integer $bolVia
     * @return Boleto
     */
    public function setBolVia($bolVia)
    {
        $this->bolVia = $bolVia;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getBolDescontoData($format = false)
    {
        $bolDescontoData = $this->bolDescontoData;

        if ($format && $bolDescontoData) {
            $bolDescontoData = $bolDescontoData->format('d/m/Y H:i:s');
        }

        return $bolDescontoData;
    }

    /**
     * @param \Datetime $bolDescontoData
     * @return Boleto
     */
    public function setBolDescontoData($bolDescontoData)
    {
        if ($bolDescontoData) {
            if (is_string($bolDescontoData)) {
                $bolDescontoData = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $bolDescontoData
                );
                $bolDescontoData = new \Datetime($bolDescontoData);
            }
        } else {
            $bolDescontoData = null;
        }
        $this->bolDescontoData = $bolDescontoData;

        return $this;
    }

    /**
     * @return float
     */
    public function getBolDescontoValor()
    {
        return $this->bolDescontoValor;
    }

    /**
     * @param float $bolDescontoValor
     * @return Boleto
     */
    public function setBolDescontoValor($bolDescontoValor)
    {
        $this->bolDescontoValor = $bolDescontoValor;

        return $this;
    }

    /**
     * @return float
     */
    public function getBolMulta()
    {
        return $this->bolMulta;
    }

    /**
     * @param float $bolMulta
     * @return Boleto
     */
    public function setBolMulta($bolMulta)
    {
        $this->bolMulta = $bolMulta;

        return $this;
    }

    /**
     * @return float
     */
    public function getBolJuros()
    {
        return $this->bolJuros;
    }

    /**
     * @param float $bolJuros
     * @return Boleto
     */
    public function setBolJuros($bolJuros)
    {
        $this->bolJuros = $bolJuros;

        return $this;
    }

    /**
     * @return float
     */
    public function getBolAcrescimos()
    {
        return $this->bolAcrescimos;
    }

    /**
     * @param float $bolAcrescimos
     * @return Boleto
     */
    public function setBolAcrescimos($bolAcrescimos)
    {
        $this->bolAcrescimos = $bolAcrescimos;

        return $this;
    }

    /**
     * @return float
     */
    public function getBolValorPago()
    {
        return $this->bolValorPago;
    }

    /**
     * @param float $bolValorPago
     * @return Boleto
     */
    public function setBolValorPago($bolValorPago)
    {
        $this->bolValorPago = $bolValorPago;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'bolId'              => $this->getBolId(),
            'confcont'           => $this->getConfcont(),
            'titulo'             => $this->getTitulo(),
            'pagamento'          => $this->getPagamento(),
            'bolNossoNumero'     => $this->getBolNossoNumero(),
            'bolNumeroDocumento' => $this->getBolNumeroDocumento(),
            'bolEstado'          => $this->getBolEstado(),
            'bolValorUnitario'   => $this->getBolValorUnitario(),
            'bolQuantidade'      => $this->getBolQuantidade(),
            'bolDemostrativo'    => $this->getBolDemostrativo(),
            'bolInstrucoes'      => $this->getBolInstrucoes(),
            'bolVia'             => $this->getBolVia(),
            'bolDescontoData'    => $this->getBolDescontoData(true),
            'bolDescontoValor'   => $this->getBolDescontoValor(),
            'bolMulta'           => $this->getBolMulta(),
            'bolJuros'           => $this->getBolJuros(),
            'bolAcrescimos'      => $this->getBolAcrescimos(),
            'bolValorPago'       => $this->getBolValorPago(),
        );

        $array['confcont']  = $this->getConfcont() ? $this->getConfcont()->getConfcontId() : null;
        $array['titulo']    = $this->getTitulo() ? $this->getTitulo()->getTituloId() : null;
        $array['pagamento'] = $this->getPagamento() ? $this->getPagamento()->getPagamentoId() : null;

        return $array;
    }

    /**
     * @return \Boleto\Entity\BoletoBaixa
     */
    public function getBaixa()
    {
        return $this->baixa;
    }

    /**
     * @param \Boleto\Entity\BoletoBaixa $baixa
     * @return Boleto
     */
    public function setBaixa($baixa)
    {
        $this->baixa = $baixa;

        return $this;
    }

    /**
     * @return integer
     */
    public function getBolCarteira()
    {
        return $this->bolCarteira;
    }

    /**
     * @param integer $bolCarteira
     * @return Boleto
     */
    public function setBolCarteira($bolCarteira)
    {
        $this->bolCarteira = $bolCarteira;

        return $this;
    }
}
