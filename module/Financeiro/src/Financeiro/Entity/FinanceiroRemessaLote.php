<?php

namespace Financeiro\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FinanceiroRemessaLote
 *
 * @ORM\Table(name="financeiro__remessa_lote", uniqueConstraints={@ORM\UniqueConstraint(name="uniq", columns={"bol_id", "remessa_id", "lote_codigo"})}, indexes={@ORM\Index(name="fk_boleto_financeiro__remessa_financeiro__remessa1_idx", columns={"remessa_id"}), @ORM\Index(name="fk_boleto_financeiro__remessa_boleto1_idx", columns={"bol_id"})})
 * @ORM\Entity
 */
class FinanceiroRemessaLote
{
    /**
     * @var integer
     *
     * @ORM\Column(name="remessa_lote_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $remessaLoteId;

    /**
     * @var integer
     *
     * @ORM\Column(name="lote_codigo", type="integer", nullable=false)
     */
    private $loteCodigo;

    /**
     * @var integer
     *
     * @ORM\Column(name="lote_movimento", type="integer", nullable=false)
     */
    private $loteMovimento = 1;

    /**
     * @var \Financeiro\Entity\Boleto
     *
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\Boleto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="bol_id", referencedColumnName="bol_id")
     * })
     */
    private $bol;

    /**
     * @var \Financeiro\Entity\FinanceiroRemessa
     *
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\FinanceiroRemessa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="remessa_id", referencedColumnName="remessa_id")
     * })
     */
    private $remessa;

    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    public function setRemessaLoteId($remessaLoteId)
    {
        $this->remessaLoteId = $remessaLoteId;

        return $this;
    }

    public function getRemessaLoteId()
    {
        return $this->remessaLoteId;
    }

    public function setLoteCodigo($loteCodigo)
    {
        $this->loteCodigo = $loteCodigo;

        return $this;
    }

    public function getLoteCodigo()
    {
        return $this->loteCodigo;
    }

    /**
     * @return int
     */
    public function getLoteMovimento()
    {
        return $this->loteMovimento;
    }

    /**
     * @param int $loteMovimento
     * @return FinanceiroRemessaLote
     */
    public function setLoteMovimento($loteMovimento)
    {
        $this->loteMovimento = $loteMovimento;

        return $this;
    }

    public function setBol($bol)
    {
        $this->bol = $bol;

        return $this;
    }

    public function getBol()
    {
        return $this->bol;
    }

    public function setRemessa($remessa)
    {
        $this->remessa = $remessa;

        return $this;
    }

    public function getRemessa()
    {
        return $this->remessa;
    }

    public function toArray()
    {
        return [
            'remessaLoteId' => $this->getRemessaLoteId(),
            'loteCodigo'    => $this->getLoteCodigo(),
            'loteMovimento' => $this->getLoteMovimento(),
            'bol'           => $this->getBol(),
            'remessa'       => $this->getRemessa(),
        ];
    }
}

