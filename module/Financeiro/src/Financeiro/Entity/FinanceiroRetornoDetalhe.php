<?php

namespace Financeiro\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * FinanceiroRetornoDetalhe
 *
 * @ORM\Table(name="financeiro__retorno_detalhe")
 * @ORM\Entity
 * @LG\LG(id="detalheId",label="DetalheValorDesconto")
 * @Jarvis\Jarvis(title="Listagem de retorno detalhe",icon="fa fa-table")
 */
class FinanceiroRetornoDetalhe
{
    /**
     * @var integer
     *
     * @ORM\Column(name="detalhe_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="detalhe_id")
     * @LG\Labels\Attributes(text="código detalhe")
     * @LG\Querys\Conditions(type="=")
     */
    private $detalheId;

    /**
     * @var \Financeiro\Entity\FinanceiroRetorno
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\FinanceiroRetorno")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="retorno_id", referencedColumnName="retorno_id")
     * })
     */
    private $retorno;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="detalhe_data", type="datetime", nullable=true)
     * @LG\Labels\Property(name="detalhe_data")
     * @LG\Labels\Attributes(text="data")
     * @LG\Querys\Conditions(type="=")
     */
    private $detalheData;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="detalhe_data_vencimento", type="datetime", nullable=true)
     * @LG\Labels\Property(name="detalhe_data_vencimento")
     * @LG\Labels\Attributes(text="vencimento data valor")
     * @LG\Querys\Conditions(type="=")
     */
    private $detalheDataVencimento;

    /**
     * @var string
     *
     * @ORM\Column(name="detalhe_nosso_numero", type="string", nullable=true, length=40)
     * @LG\Labels\Property(name="detalhe_nosso_numero")
     * @LG\Labels\Attributes(text="número nosso")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $detalheNossoNumero;

    /**
     * @var string
     *
     * @ORM\Column(name="detalhe_numero_documento", type="string", nullable=true, length=40)
     * @LG\Labels\Property(name="detalhe_numero_documento")
     * @LG\Labels\Attributes(text="documento número")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $detalheNumeroDocumento;

    /**
     * @var float
     *
     * @ORM\Column(name="detalhe_valor_titulo", type="float", nullable=true)
     * @LG\Labels\Property(name="detalhe_valor_titulo")
     * @LG\Labels\Attributes(text="título valor")
     * @LG\Querys\Conditions(type="=")
     */
    private $detalheValorTitulo;

    /**
     * @var float
     *
     * @ORM\Column(name="detalhe_valor_pago", type="float", nullable=true)
     * @LG\Labels\Property(name="detalhe_valor_pago")
     * @LG\Labels\Attributes(text="pago valor")
     * @LG\Querys\Conditions(type="=")
     */
    private $detalheValorPago;

    /**
     * @var float
     *
     * @ORM\Column(name="detalhe_valor_mora", type="float", nullable=true)
     * @LG\Labels\Property(name="detalhe_valor_mora")
     * @LG\Labels\Attributes(text="mora valor")
     * @LG\Querys\Conditions(type="=")
     */
    private $detalheValorMora;

    /**
     * @var float
     *
     * @ORM\Column(name="detalhe_valor_desconto", type="float", nullable=true)
     * @LG\Labels\Property(name="detalhe_valor_desconto")
     * @LG\Labels\Attributes(text="desconto valor")
     * @LG\Querys\Conditions(type="=")
     */
    private $detalheValorDesconto;

    /**
     * @var string
     *
     * @ORM\Column(name="detalhe_carteira", type="string", nullable=true, length=5)
     * @LG\Labels\Property(name="detalhe_carteira")
     * @LG\Labels\Attributes(text="carteira")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $detalheCarteira;

    /**
     * @var string
     *
     * @ORM\Column(name="detalhe_alegacao", type="text", nullable=true)
     * @LG\Labels\Property(name="detalhe_alegacao")
     * @LG\Labels\Attributes(text="alegação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $detalheAlegacao;

    /**
     * @var string
     *
     * @ORM\Column(name="detalhe_status", type="text", nullable=false)
     * @LG\Labels\Property(name="detalhe_status")
     * @LG\Labels\Attributes(text="Situação do arquivo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $detalheStatus = 'Finalizado';

    /**
     * @var int
     *
     * @ORM\Column(name="detalhe_codigo_movimento", type="text", nullable=true)
     * @LG\Labels\Property(name="detalhe_codigo_movimento")
     * @LG\Labels\Attributes(text="Situação do titulo do detalhe")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $detalheCodigoMovimento;

    /**
     * @var int
     *
     * @ORM\Column(name="titulo_id", type="integer", nullable=true)
     * @LG\Labels\Property(name="titulo_id")
     * @LG\Labels\Attributes(text="titulo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $tituloId;

    /**
     * @var string
     *
     * @ORM\Column(name="detalhe_log", type="string", nullable=true)
     * @LG\Labels\Property(name="detalhe_log")
     * @LG\Labels\Attributes(text="Situação do processamento do detalhe")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $detalheLog;

    /**
     * @return integer
     */
    public function getDetalheId()
    {
        return $this->detalheId;
    }

    /**
     * @param integer $detalheId
     * @return FinanceiroRetornoDetalhe
     */
    public function setDetalheId($detalheId)
    {
        $this->detalheId = $detalheId;

        return $this;
    }

    /**
     * @return \Financeiro\Entity\FinanceiroRetorno
     */
    public function getRetorno()
    {
        return $this->retorno;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroRetorno $retorno
     * @return FinanceiroRetornoDetalhe
     */
    public function setRetorno($retorno)
    {
        $this->retorno = $retorno;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getDetalheData($format = false)
    {
        $detalheData = $this->detalheData;

        if ($format && $detalheData) {
            $detalheData = $detalheData->format('d/m/Y H:i:s');
        }

        return $detalheData;
    }

    /**
     * @param \Datetime $detalheData
     * @return FinanceiroRetornoDetalhe
     */
    public function setDetalheData($detalheData)
    {
        if ($detalheData) {
            if (is_string($detalheData)) {
                $detalheData = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $detalheData
                );
                $detalheData = new \Datetime($detalheData);
            }
        } else {
            $detalheData = null;
        }
        $this->detalheData = $detalheData;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getDetalheDataVencimento($format = false)
    {
        $detalheDataVencimento = $this->detalheDataVencimento;

        if ($format && $detalheDataVencimento) {
            $detalheDataVencimento = $detalheDataVencimento->format('d/m/Y H:i:s');
        }

        return $detalheDataVencimento;
    }

    /**
     * @param \Datetime $detalheDataVencimento
     * @return FinanceiroRetornoDetalhe
     */
    public function setDetalheDataVencimento($detalheDataVencimento)
    {
        if ($detalheDataVencimento) {
            if (is_string($detalheDataVencimento)) {
                $detalheDataVencimento = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $detalheDataVencimento
                );
                $detalheDataVencimento = new \Datetime($detalheDataVencimento);
            }
        } else {
            $detalheDataVencimento = null;
        }
        $this->detalheDataVencimento = $detalheDataVencimento;

        return $this;
    }

    /**
     * @return string
     */
    public function getDetalheNossoNumero()
    {
        return $this->detalheNossoNumero;
    }

    /**
     * @param string $detalheNossoNumero
     * @return FinanceiroRetornoDetalhe
     */
    public function setDetalheNossoNumero($detalheNossoNumero)
    {
        $this->detalheNossoNumero = $detalheNossoNumero;

        return $this;
    }

    /**
     * @return string
     */
    public function getDetalheNumeroDocumento()
    {
        return $this->detalheNumeroDocumento;
    }

    /**
     * @param string $detalheNumeroDocumento
     * @return FinanceiroRetornoDetalhe
     */
    public function setDetalheNumeroDocumento($detalheNumeroDocumento)
    {
        $this->detalheNumeroDocumento = $detalheNumeroDocumento;

        return $this;
    }

    /**
     * @return float
     */
    public function getDetalheValorTitulo()
    {
        return $this->detalheValorTitulo;
    }

    /**
     * @param float $detalheValorTitulo
     * @return FinanceiroRetornoDetalhe
     */
    public function setDetalheValorTitulo($detalheValorTitulo)
    {
        $this->detalheValorTitulo = $detalheValorTitulo;

        return $this;
    }

    /**
     * @return float
     */
    public function getDetalheValorPago()
    {
        return $this->detalheValorPago;
    }

    /**
     * @param float $detalheValorPago
     * @return FinanceiroRetornoDetalhe
     */
    public function setDetalheValorPago($detalheValorPago)
    {
        $this->detalheValorPago = $detalheValorPago;

        return $this;
    }

    /**
     * @return float
     */
    public function getDetalheValorMora()
    {
        return $this->detalheValorMora;
    }

    /**
     * @param float $detalheValorMora
     * @return FinanceiroRetornoDetalhe
     */
    public function setDetalheValorMora($detalheValorMora)
    {
        $this->detalheValorMora = $detalheValorMora;

        return $this;
    }

    /**
     * @return float
     */
    public function getDetalheValorDesconto()
    {
        return $this->detalheValorDesconto;
    }

    /**
     * @param float $detalheValorDesconto
     * @return FinanceiroRetornoDetalhe
     */
    public function setDetalheValorDesconto($detalheValorDesconto)
    {
        $this->detalheValorDesconto = $detalheValorDesconto;

        return $this;
    }

    /**
     * @return string
     */
    public function getDetalheCarteira()
    {
        return $this->detalheCarteira;
    }

    /**
     * @param string $detalheCarteira
     * @return FinanceiroRetornoDetalhe
     */
    public function setDetalheCarteira($detalheCarteira)
    {
        $this->detalheCarteira = $detalheCarteira;

        return $this;
    }

    /**
     * @return string
     */
    public function getDetalheAlegacao()
    {
        return $this->detalheAlegacao;
    }

    /**
     * @param string $detalheAlegacao
     * @return FinanceiroRetornoDetalhe
     */
    public function setDetalheAlegacao($detalheAlegacao)
    {
        $this->detalheAlegacao = $detalheAlegacao;

        return $this;
    }

    /**
     * @return string
     */
    public function getDetalheStatus()
    {
        return $this->detalheStatus;
    }

    /**
     * @param $detalheStatus string
     * @return \Financeiro\Entity\FinanceiroRetornoDetalhe
     */
    public function setDetalheStatus($detalheStatus)
    {
        $this->detalheStatus = $detalheStatus;

        return $this;
    }

    /**
     * @return int
     */
    public function getDetalheCodigoMovimento()
    {
        return $this->detalheCodigoMovimento;
    }

    /**
     * @param $detalheCodigoMovimento int
     * @return \Financeiro\Entity\FinanceiroRetornoDetalhe
     */
    public function setDetalheCodigoMovimento($detalheCodigoMovimento)
    {
        $this->detalheCodigoMovimento = $detalheCodigoMovimento;

        return $this;
    }

    /**
     * @return string
     */
    public function getDetalheLog()
    {
        return $this->detalheLog;
    }

    /**
     * @param $detalheLog string
     * @return \Financeiro\Entity\FinanceiroRetornoDetalhe
     */
    public function setDetalheLog($detalheLog)
    {
        $this->detalheLog = $detalheLog;

        return $this;
    }

    /**
     * @return int
     */
    public function gettituloId()
    {
        return $this->tituloId;
    }

    /**
     * @param int $tituloId
     * @return FinanceiroRetornoDetalhe
     */
    public function setTituloId($tituloId)
    {
        $this->tituloId = $tituloId;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'detalheId'              => $this->getDetalheId(),
            'tituloId'                  => $this->gettituloId(),
            'retorno'                => $this->getRetorno(),
            'detalheData'            => $this->getDetalheData(true),
            'detalheDataVencimento'  => $this->getDetalheDataVencimento(true),
            'detalheNossoNumero'     => $this->getDetalheNossoNumero(),
            'detalheNumeroDocumento' => $this->getDetalheNumeroDocumento(),
            'detalheValorTitulo'     => $this->getDetalheValorTitulo(),
            'detalheValorPago'       => $this->getDetalheValorPago(),
            'detalheValorMora'       => $this->getDetalheValorMora(),
            'detalheValorDesconto'   => $this->getDetalheValorDesconto(),
            'detalheCarteira'        => $this->getDetalheCarteira(),
            'detalheAlegacao'        => $this->getDetalheAlegacao(),
            'detalheStatus'          => $this->getDetalheStatus(),
            'detalheCodigoMovimento' => $this->getDetalheCodigoMovimento(),
        );

        $array['retorno'] = $this->getRetorno() ? $this->getRetorno()->getRetornoId() : null;

        return $array;
    }
}
