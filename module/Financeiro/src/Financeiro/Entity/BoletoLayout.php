<?php

namespace Financeiro\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * BoletoLayout
 *
 * @ORM\Table(name="boleto_layout")
 * @ORM\Entity
 * @LG\LG(id="layoutId",label="LayoutCaminho")
 * @Jarvis\Jarvis(title="Listagem de layout",icon="fa fa-table")
 */
class BoletoLayout
{
    /**
     * @var integer
     *
     * @ORM\Column(name="layout_id", type="integer", nullable=false, length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="layout_id")
     * @LG\Labels\Attributes(text="código layout")
     * @LG\Querys\Conditions(type="=")
     */
    private $layoutId;
    /**
     * @var string
     *
     * @ORM\Column(name="layout_caminho", type="string", nullable=false, length=255)
     * @LG\Labels\Property(name="layout_caminho")
     * @LG\Labels\Attributes(text="caminho")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $layoutCaminho;
    /**
     * @var integer
     *
     * @ORM\Column(name="layout_versao", type="integer", nullable=false, length=11)
     * @LG\Labels\Property(name="layout_versao")
     * @LG\Labels\Attributes(text="versão")
     * @LG\Querys\Conditions(type="=")
     */
    private $layoutVersao;
    /**
     * @var string
     *
     * @ORM\Column(name="layout_data", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="layout_data")
     * @LG\Labels\Attributes(text="data")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $layoutData;
    /**
     * @var string
     *
     * @ORM\Column(name="layout_estado", type="string", nullable=false, length=1)
     * @LG\Labels\Property(name="layout_estado")
     * @LG\Labels\Attributes(text="estado")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $layoutEstado;

    /**
     * @return integer
     */
    public function getLayoutId()
    {
        return $this->layoutId;
    }

    /**
     * @param integer $layoutId
     * @return BoletoLayout
     */
    public function setLayoutId($layoutId)
    {
        $this->layoutId = $layoutId;

        return $this;
    }

    /**
     * @return string
     */
    public function getLayoutCaminho()
    {
        return $this->layoutCaminho;
    }

    /**
     * @param string $layoutCaminho
     * @return BoletoLayout
     */
    public function setLayoutCaminho($layoutCaminho)
    {
        $this->layoutCaminho = $layoutCaminho;

        return $this;
    }

    /**
     * @return integer
     */
    public function getLayoutVersao()
    {
        return $this->layoutVersao;
    }

    /**
     * @param integer $layoutVersao
     * @return BoletoLayout
     */
    public function setLayoutVersao($layoutVersao)
    {
        $this->layoutVersao = $layoutVersao;

        return $this;
    }

    /**
     * @return string
     */
    public function getLayoutData()
    {
        return $this->layoutData;
    }

    /**
     * @param string $layoutData
     * @return BoletoLayout
     */
    public function setLayoutData($layoutData)
    {
        $this->layoutData = $layoutData;

        return $this;
    }

    /**
     * @return string
     */
    public function getLayoutEstado()
    {
        return $this->layoutEstado;
    }

    /**
     * @param string $layoutEstado
     * @return BoletoLayout
     */
    public function setLayoutEstado($layoutEstado)
    {
        $this->layoutEstado = $layoutEstado;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'layoutId'      => $this->getLayoutId(),
            'layoutCaminho' => $this->getLayoutCaminho(),
            'layoutVersao'  => $this->getLayoutVersao(),
            'layoutData'    => $this->getLayoutData(),
            'layoutEstado'  => $this->getLayoutEstado(),
        );

        return $array;
    }
}
