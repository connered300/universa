<?php

namespace Financeiro\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * FinanceiroTituloCheque
 *
 * @ORM\Table(name="financeiro__titulo_cheque")
 * @ORM\Entity
 * @LG\LG(id="titulochequeId",label="TituloId")
 * @Jarvis\Jarvis(title="Listagem de título cheque",icon="fa fa-table")
 */
class FinanceiroTituloCheque
{
    /**
     * @var integer
     *
     * @ORM\Column(name="titulocheque_id", type="integer", nullable=false, length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="titulocheque_id")
     * @LG\Labels\Attributes(text="código cheque de título")
     * @LG\Querys\Conditions(type="=")
     */
    private $titulochequeId;

    /**
     * @var \Financeiro\Entity\FinanceiroTitulo
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\FinanceiroTitulo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="titulo_id", referencedColumnName="titulo_id")
     * })
     */
    private $titulo;

    /**
     * @var \Financeiro\Entity\FinanceiroPagamento
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\FinanceiroPagamento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pagamento_id", referencedColumnName="pagamento_id")
     * })
     */
    private $pagamento;

    /**
     * @var \Financeiro\Entity\FinanceiroCheque
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\FinanceiroCheque")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cheque_id", referencedColumnName="cheque_id")
     * })
     */
    private $cheque;

    /**
     * @var float
     *
     * @ORM\Column(name="titulocheque_valor", type="float", nullable=true, length=10)
     * @LG\Labels\Property(name="titulocheque_valor")
     * @LG\Labels\Attributes(text="valor de")
     * @LG\Querys\Conditions(type="=")
     */
    private $titulochequeValor;

    /**
     * @var string
     *
     * @ORM\Column(name="titulocheque_estado", type="string", nullable=false, length=7)
     * @LG\Labels\Property(name="titulocheque_estado")
     * @LG\Labels\Attributes(text="estado de")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $titulochequeEstado;

    /**
     * @return integer
     */
    public function getTitulochequeId()
    {
        return $this->titulochequeId;
    }

    /**
     * @param integer $titulochequeId
     * @return FinanceiroTituloCheque
     */
    public function setTitulochequeId($titulochequeId)
    {
        $this->titulochequeId = $titulochequeId;

        return $this;
    }

    /**
     * @return \Financeiro\Entity\FinanceiroTitulo
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroTitulo $titulo
     * @return FinanceiroTituloCheque
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * @return \Financeiro\Entity\FinanceiroPagamento
     */
    public function getPagamento()
    {
        return $this->pagamento;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroPagamento $pagamento
     * @return FinanceiroTituloCheque
     */
    public function setPagamento($pagamento)
    {
        $this->pagamento = $pagamento;

        return $this;
    }

    /**
     * @return \Financeiro\Entity\FinanceiroCheque
     */
    public function getCheque()
    {
        return $this->cheque;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroCheque $cheque
     * @return FinanceiroTituloCheque
     */
    public function setCheque($cheque)
    {
        $this->cheque = $cheque;

        return $this;
    }

    /**
     * @return float
     */
    public function getTitulochequeValor()
    {
        return $this->titulochequeValor;
    }

    /**
     * @param float $titulochequeValor
     * @return FinanceiroTituloCheque
     */
    public function setTitulochequeValor($titulochequeValor)
    {
        $this->titulochequeValor = $titulochequeValor;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitulochequeEstado()
    {
        return $this->titulochequeEstado;
    }

    /**
     * @param string $titulochequeEstado
     * @return FinanceiroTituloCheque
     */
    public function setTitulochequeEstado($titulochequeEstado)
    {
        $this->titulochequeEstado = $titulochequeEstado;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'titulochequeId'     => $this->getTitulochequeId(),
            'titulo'             => $this->getTitulo(),
            'pagamento'          => $this->getPagamento(),
            'cheque'             => $this->getCheque(),
            'titulochequeValor'  => $this->getTitulochequeValor(),
            'titulochequeEstado' => $this->getTitulochequeEstado(),
        );

        $array['titulo']    = $this->getTitulo() ? $this->getTitulo()->getTituloId() : null;
        $array['pagamento'] = $this->getPagamento() ? $this->getPagamento()->getPagamentoId() : null;
        $array['cheque']    = $this->getCheque() ? $this->getCheque()->getChequeId() : null;

        return $array;
    }
}
