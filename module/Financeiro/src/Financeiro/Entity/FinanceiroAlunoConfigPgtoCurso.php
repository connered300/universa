<?php

namespace Financeiro\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * FinanceiroAlunoConfigPgtoCurso
 *
 * @ORM\Table(name="financeiro__aluno_config_pgto_curso")
 * @ORM\Entity
 * @LG\LG(id="configPgtoCursoId",label="alunocursoId")
 * @Jarvis\Jarvis(title="Listagem de aluno configuração pgto curso",icon="fa fa-table")
 */
class FinanceiroAlunoConfigPgtoCurso
{
    /**
     * @var integer
     *
     * @ORM\Column(name="config_pgto_curso_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="config_pgto_curso_id")
     * @LG\Labels\Attributes(text="código curso pgto configuração")
     * @LG\Querys\Conditions(type="=")
     */
    private $configPgtoCursoId;

    /**
     * @var \Matricula\Entity\AcadgeralAlunoCurso
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadgeralAlunoCurso", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="alunocurso_id", referencedColumnName="alunocurso_id")
     * })
     */
    private $alunocurso;

    /**
     * @var \Financeiro\Entity\FinanceiroTituloTipo
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\FinanceiroTituloTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipotitulo_id", referencedColumnName="tipotitulo_id")
     * })
     */
    private $tipotitulo;

    /**
     * @var \Financeiro\Entity\FinanceiroValores
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\FinanceiroValores")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="valores_id", referencedColumnName="valores_id")
     * })
     */
    private $valores;

    /**
     * @var integer
     *
     * @ORM\Column(name="config_pgto_curso_parcela", type="integer", nullable=true, length=11)
     * @LG\Labels\Property(name="config_pgto_curso_parcela")
     * @LG\Labels\Attributes(text="parcela")
     * @LG\Querys\Conditions(type="=")
     */
    private $configPgtoCursoParcela;

    /**
     * @var float
     *
     * @ORM\Column(name="config_pgto_curso_valor", type="float", nullable=true)
     * @LG\Labels\Property(name="config_pgto_curso_valor")
     * @LG\Labels\Attributes(text="valor")
     * @LG\Querys\Conditions(type="=")
     */
    private $configPgtoCursoValor;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="config_pgto_curso_vencimento", type="datetime", nullable=true, length=11)
     * @LG\Labels\Property(name="config_pgto_curso_vencimento")
     * @LG\Labels\Attributes(text="vencimento")
     * @LG\Querys\Conditions(type="=")
     */
    private $configPgtoCursoVencimento;

    /**
     * @var \Financeiro\Entity\FinanceiroMeioPagamento
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\FinanceiroMeioPagamento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="meio_pagamento_id", referencedColumnName="meio_pagamento_id")
     * })
     */
    private $meioPagamento;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_alteracao", referencedColumnName="id")
     * })
     */
    private $usuarioAlteracao;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_supervisor", referencedColumnName="id")
     * })
     */
    private $usuarioSupervisor;

    /**
     * @var string
     *
     * @ORM\Column(name="config_pgto_curso_efetivado", type="string", nullable=true, length=3)
     * @LG\Labels\Property(name="config_pgto_curso_efetivado")
     * @LG\Labels\Attributes(text="efetivado")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $configPgtoCursoEfetivado;

    /**
     * @var string
     *
     * @ORM\Column(name="config_pgto_curso_selecionado", type="string", nullable=true, length=3)
     * @LG\Labels\Property(name="config_pgto_curso_selecionado")
     * @LG\Labels\Attributes(text="selecionado")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $configPgtoCursoSelecionado;

    /**
     * @var string
     *
     * @ORM\Column(name="config_pgto_curso_alterado", type="string", nullable=true, length=3)
     * @LG\Labels\Property(name="config_pgto_curso_alterado")
     * @LG\Labels\Attributes(text="alterado")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $configPgtoCursoAlterado;

    /**
     * @var string
     *
     * @ORM\Column(name="config_pgto_curso_obs", type="string", nullable=true)
     * @LG\Labels\Property(name="config_pgto_curso_obs")
     * @LG\Labels\Attributes(text="Observação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $configPgtoCursoObs;

    /**
     * @var string
     *
     * @ORM\Column(name="config_pgto_curso_justificativa", type="string", nullable=true)
     * @LG\Labels\Property(name="config_pgto_curso_justificativa")
     * @LG\Labels\Attributes(text="justificativa")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $configPgtoCursoJustificativa;
    /**
     * @var \Financeiro\Entity\FinanceiroCartao
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\FinanceiroCartao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cartao_id", referencedColumnName="cartao_id")
     * })
     */
    private $cartaoId;

    /**
     * @return integer
     */
    public function getConfigPgtoCursoId()
    {
        return $this->configPgtoCursoId;
    }

    /**
     * @param integer $configPgtoCursoId
     * @return FinanceiroAlunoConfigPgtoCurso
     */
    public function setConfigPgtoCursoId($configPgtoCursoId)
    {
        $this->configPgtoCursoId = $configPgtoCursoId;

        return $this;
    }

    /**
     * @return \Matricula\Entity\AcadgeralAlunoCurso
     */
    public function getAlunocurso()
    {
        return $this->alunocurso;
    }

    /**
     * @param \Matricula\Entity\AcadgeralAlunoCurso $alunocurso
     * @return FinanceiroAlunoConfigPgtoCurso
     */
    public function setAlunocurso($alunocurso)
    {
        $this->alunocurso = $alunocurso;

        return $this;
    }

    /**
     * @return \Financeiro\Entity\FinanceiroTituloTipo
     */
    public function getTipotitulo()
    {
        return $this->tipotitulo;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroTituloTipo $tipotitulo
     * @return FinanceiroAlunoConfigPgtoCurso
     */
    public function setTipotitulo($tipotitulo)
    {
        $this->tipotitulo = $tipotitulo;

        return $this;
    }

    /**
     * @return \Financeiro\Entity\FinanceiroValores
     */
    public function getValores()
    {
        return $this->valores;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroValores $valores
     * @return FinanceiroAlunoConfigPgtoCurso
     */
    public function setValores($valores)
    {
        $this->valores = $valores;

        return $this;
    }

    /**
     * @return \Financeiro\Entity\FinanceiroMeioPagamento
     */
    public function getMeioPagamento()
    {
        return $this->meioPagamento;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroMeioPagamento $meioPagamento
     * @return FinanceiroAlunoConfigPgtoCurso
     */
    public function setMeioPagamento($meioPagamento)
    {
        $this->meioPagamento = $meioPagamento;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuarioAlteracao()
    {
        return $this->usuarioAlteracao;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuarioAlteracao
     * @return FinanceiroAlunoConfigPgtoCurso
     */
    public function setUsuarioAlteracao($usuarioAlteracao)
    {
        $this->usuarioAlteracao = $usuarioAlteracao;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuarioSupervisor()
    {
        return $this->usuarioSupervisor;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuarioSupervisor
     * @return FinanceiroAlunoConfigPgtoCurso
     */
    public function setUsuarioSupervisor($usuarioSupervisor)
    {
        $this->usuarioSupervisor = $usuarioSupervisor;

        return $this;
    }

    /**
     * @return integer
     */
    public function getConfigPgtoCursoParcela()
    {
        return $this->configPgtoCursoParcela;
    }

    /**
     * @param integer $configPgtoCursoParcela
     * @return FinanceiroAlunoConfigPgtoCurso
     */
    public function setConfigPgtoCursoParcela($configPgtoCursoParcela)
    {
        $this->configPgtoCursoParcela = $configPgtoCursoParcela ? $configPgtoCursoParcela : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getConfigPgtoCursoValor()
    {
        return $this->configPgtoCursoValor;
    }

    /**
     * @param float $configPgtoCursoValor
     * @return FinanceiroAlunoConfigPgtoCurso
     */
    public function setConfigPgtoCursoValor($configPgtoCursoValor)
    {
        $this->configPgtoCursoValor = $configPgtoCursoValor ? $configPgtoCursoValor : null;

        return $this;
    }

    /**
     * @return \Datetime|string
     */
    public function getConfigPgtoCursoVencimento($format = false)
    {
        $configPgtoCursoVencimento = $this->configPgtoCursoVencimento;

        if ($format && $configPgtoCursoVencimento) {
            $configPgtoCursoVencimento = $configPgtoCursoVencimento->format('d/m/Y');
        }

        return $configPgtoCursoVencimento;
    }

    /**
     * @param \Datetime|string $configPgtoCursoVencimento
     * @return FinanceiroAlunoConfigPgtoCurso
     */
    public function setConfigPgtoCursoVencimento($configPgtoCursoVencimento)
    {
        if ($configPgtoCursoVencimento) {
            if (is_string($configPgtoCursoVencimento)) {
                $configPgtoCursoVencimento = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $configPgtoCursoVencimento
                );
                $configPgtoCursoVencimento = new \Datetime($configPgtoCursoVencimento);
            }
        } else {
            $configPgtoCursoVencimento = null;
        }

        $this->configPgtoCursoVencimento = $configPgtoCursoVencimento;

        return $this;
    }

    /**
     * @return string
     */
    public function getConfigPgtoCursoEfetivado()
    {
        return $this->configPgtoCursoEfetivado;
    }

    /**
     * @return bool
     */
    public function cursoJaFoiEfetivado()
    {
        return $this->configPgtoCursoEfetivado == \Financeiro\Service\FinanceiroAlunoConfigPgtoCurso::CONFIG_PGTO_CURSO_EFETIVADO_SIM;
    }

    /**
     * @param string $configPgtoCursoEfetivado
     * @return FinanceiroAlunoConfigPgtoCurso
     */
    public function setConfigPgtoCursoEfetivado($configPgtoCursoEfetivado)
    {
        $this->configPgtoCursoEfetivado = $configPgtoCursoEfetivado;

        return $this;
    }

    /**
     * @return string
     */
    public function getConfigPgtoCursoSelecionado()
    {
        return $this->configPgtoCursoSelecionado;
    }

    /**
     * @param string $configPgtoCursoSelecionado
     * @return FinanceiroAlunoConfigPgtoCurso
     */
    public function setConfigPgtoCursoSelecionado($configPgtoCursoSelecionado)
    {
        $this->configPgtoCursoSelecionado = $configPgtoCursoSelecionado;

        return $this;
    }

    /**
     * @return string
     */
    public function getConfigPgtoCursoAlterado()
    {
        return $this->configPgtoCursoAlterado;
    }

    /**
     * @param string $configPgtoCursoAlterado
     * @return FinanceiroAlunoConfigPgtoCurso
     */
    public function setConfigPgtoCursoAlterado($configPgtoCursoAlterado)
    {
        $this->configPgtoCursoAlterado = $configPgtoCursoAlterado;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return string
     */
    public function getConfigPgtoCursoObs()
    {
        return $this->configPgtoCursoObs;
    }

    /**
     * @param string $configPgtoCursoObs
     * @return FinanceiroAlunoConfigPgtoCurso
     */
    public function setConfigPgtoCursoObs($configPgtoCursoObs)
    {
        $this->configPgtoCursoObs = $configPgtoCursoObs;

        return $this;
    }

    /**
     * @return string
     */
    public function getConfigPgtoCursoJustificativa()
    {
        return $this->configPgtoCursoJustificativa;
    }

    /**
     * @param string $configPgtoCursoJustificativa
     * @return FinanceiroAlunoConfigPgtoCurso
     */
    public function setConfigPgtoCursoJustificativa($configPgtoCursoJustificativa)
    {
        $this->configPgtoCursoJustificativa = $configPgtoCursoJustificativa;

        return $this;
    }

    /**
     * @return \Financeiro\Entity\FinanceiroCartao
     */
    public function getCartao()
    {
        return $this->cartaoId;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroCartao $cartaoId
     * @return FinanceiroAlunoConfigPgtoCurso
     */
    public function setCartao($cartaoId)
    {
        $this->cartaoId = $cartaoId;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'tipotitulo'                   => null,
            'tipotituloId'                 => null,
            'tipotituloNome'               => null,
            'valores'                      => null,
            'meioPagamento'                => null,
            'meioPagamentoId'              => null,
            'meioPagamentoDescricao'       => null,
            'usuarioAlteracao'             => null,
            'usuarioAlteracaoId'           => null,
            'usuarioAlteracaoLogin'        => null,
            'usuarioSupervisor'            => null,
            'usuarioSupervisorId'          => null,
            'usuarioSupervisorLogin'       => null,
            'configPgtoCursoId'            => $this->getConfigPgtoCursoId(),
            'alunocurso'                   => $this->getAlunocurso(),
            'configPgtoCursoParcela'       => $this->getConfigPgtoCursoParcela(),
            'configPgtoCursoValor'         => $this->getConfigPgtoCursoValor(),
            'configPgtoCursoVencimento'    => $this->getConfigPgtoCursoVencimento(true),
            'configPgtoCursoEfetivado'     => $this->getConfigPgtoCursoEfetivado(),
            'configPgtoCursoSelecionado'   => $this->getConfigPgtoCursoSelecionado(),
            'configPgtoCursoAlterado'      => $this->getConfigPgtoCursoAlterado(),
            'configPgtoCursoObs'           => (string)$this->getConfigPgtoCursoObs(),
            'configPgtoCursoJustificativa' => (string)$this->getConfigPgtoCursoJustificativa(),
            'cartaoId'                     => $this->getCartao(),
        );

        if ($this->getCartao()) {
            $array['cartaoTitular']    = $this->getCartao()->getCartaoTitular();
            $array['cartaoVencimento'] = $this->getCartao()->getCartaoVencimento();
            $array['cartaoId']         = $this->getCartao()->getCartaoId();
            $array['cartaoFinal']      = substr($this->getCartao()->getCartaoNumero(), -4);
        }

        if ($this->getTipotitulo()) {
            $array['tipotitulo']     = $this->getTipotitulo()->getTipotituloId();
            $array['tipotituloId']   = $this->getTipotitulo()->getTipotituloId();
            $array['tipotituloNome'] = $this->getTipotitulo()->getTipotituloNome();
        }

        if ($this->getValores()) {
            $array['valores']   = $this->getValores()->getValoresId();
            $array['valoresId'] = $this->getValores()->getValoresId();
        }

        if ($this->getMeioPagamento()) {
            $array['meioPagamento']          = $this->getMeioPagamento()->getMeioPagamentoId();
            $array['meioPagamentoId']        = $this->getMeioPagamento()->getMeioPagamentoId();
            $array['meioPagamentoDescricao'] = $this->getMeioPagamento()->getMeioPagamentoDescricao();
        }

        if ($this->getUsuarioAlteracao()) {
            $array['usuarioAlteracao']      = $this->getUsuarioAlteracao()->getId();
            $array['usuarioAlteracaoId']    = $this->getUsuarioAlteracao()->getId();
            $array['usuarioAlteracaoLogin'] = $this->getUsuarioAlteracao()->getLogin();
        }

        if ($this->getUsuarioSupervisor()) {
            $array['usuarioSupervisor']      = $this->getUsuarioSupervisor()->getId();
            $array['usuarioSupervisorId']    = $this->getUsuarioSupervisor()->getId();
            $array['usuarioSupervisorLogin'] = $this->getUsuarioSupervisor()->getLogin();
        }

        return $array;
    }
}
