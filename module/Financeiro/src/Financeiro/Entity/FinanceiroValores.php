<?php

namespace Financeiro\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * FinanceiroValores
 *
 * @ORM\Table(name="financeiro__valores")
 * @ORM\Entity
 * @LG\LG(id="valoresId",label="CursocampusId")
 * @Jarvis\Jarvis(title="Listagem de valores",icon="fa fa-table")
 */
class FinanceiroValores
{
    /**
     * @var integer
     *
     * @ORM\Column(name="valores_id", type="integer", nullable=false, length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="valores_id")
     * @LG\Labels\Attributes(text="código")
     * @LG\Querys\Conditions(type="=")
     */
    private $valoresId;

    /**
     * @var \Matricula\Entity\CampusCurso
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\CampusCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cursocampus_id", referencedColumnName="cursocampus_id")
     * })
     */
    private $cursocampus;

    /**
     * @var \Matricula\Entity\AcadperiodoLetivo
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadperiodoLetivo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="per_id", referencedColumnName="per_id")
     * })
     */
    private $per;

    /**
     * @var \Financeiro\Entity\FinanceiroTituloTipo
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\FinanceiroTituloTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipotitulo_id", referencedColumnName="tipotitulo_id")
     * })
     */
    private $tipotitulo;

    /**
     * @var \Matricula\Entity\AcadgeralAreaConhecimento
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadgeralAreaConhecimento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="area_id", referencedColumnName="area_id")
     * })
     */
    private $area;

    /**
     * @var float
     *
     * @ORM\Column(name="valores_preco", type="decimal", nullable=true)
     * @LG\Labels\Property(name="valores_preco")
     * @LG\Labels\Attributes(text="preço")
     * @LG\Querys\Conditions(type="=")
     */
    private $valoresPreco;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="valores_data_inicio", type="date", nullable=true)
     * @LG\Labels\Property(name="valores_data_inicio")
     * @LG\Labels\Attributes(text="data de início de uso")
     * @LG\Querys\Conditions(type="=")
     */
    private $valoresDataInicio;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="valores_data_fim", type="date", nullable=true)
     * @LG\Labels\Property(name="valores_data_fim")
     * @LG\Labels\Attributes(text="data final de uso")
     * @LG\Querys\Conditions(type="=")
     */
    private $valoresDataFim;

    /**
     * @var integer
     *
     * @ORM\Column(name="valores_parcela", type="integer", nullable=false, length=11)
     * @LG\Labels\Property(name="valores_parcela")
     * @LG\Labels\Attributes(text="parcela")
     * @LG\Querys\Conditions(type="=")
     */
    private $valoresParcela;

    /**
     * @return integer
     */
    public function getValoresId()
    {
        return $this->valoresId;
    }

    /**
     * @param integer $valoresId
     * @return FinanceiroValores
     */
    public function setValoresId($valoresId)
    {
        $this->valoresId = $valoresId;

        return $this;
    }

    /**
     * @return \Matricula\Entity\CampusCurso
     */
    public function getCursocampus()
    {
        return $this->cursocampus;
    }

    /**
     * @param \Matricula\Entity\CampusCurso $cursocampus
     * @return FinanceiroValores
     */
    public function setCursocampus($cursocampus)
    {
        $this->cursocampus = $cursocampus;

        return $this;
    }

    /**
     * @return \Matricula\Entity\AcadperiodoLetivo
     */
    public function getPer()
    {
        return $this->per;
    }

    /**
     * @param \Matricula\Entity\AcadperiodoLetivo $per
     * @return FinanceiroValores
     */
    public function setPer($per)
    {
        $this->per = $per;

        return $this;
    }

    /**
     * @return \Financeiro\Entity\FinanceiroTituloTipo
     */
    public function getTipotitulo()
    {
        return $this->tipotitulo;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroTituloTipo $tipotitulo
     * @return FinanceiroValores
     */
    public function setTipotitulo($tipotitulo)
    {
        $this->tipotitulo = $tipotitulo;

        return $this;
    }

    /**
     * @return \Matricula\Entity\AcadgeralAreaConhecimento
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param \Matricula\Entity\AcadgeralAreaConhecimento $area
     * @return FinanceiroValores
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * @return float
     */
    public function getValoresPreco()
    {
        return $this->valoresPreco;
    }

    /**
     * @param float $valoresPreco
     * @return FinanceiroValores
     */
    public function setValoresPreco($valoresPreco)
    {
        $this->valoresPreco = $valoresPreco;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getValoresDataInicio($format = false)
    {
        $valoresDataInicio = $this->valoresDataInicio;

        if ($format && $valoresDataInicio) {
            $valoresDataInicio = $valoresDataInicio->format('d/m/Y');
        }

        return $valoresDataInicio;
    }

    /**
     * @param \Datetime $valoresDataInicio
     * @return FinanceiroValores
     */
    public function setValoresDataInicio($valoresDataInicio)
    {
        if ($valoresDataInicio) {
            if (is_string($valoresDataInicio)) {
                $valoresDataInicio = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $valoresDataInicio
                );
                $valoresDataInicio = new \Datetime($valoresDataInicio);
            }
        } else {
            $valoresDataInicio = null;
        }
        $this->valoresDataInicio = $valoresDataInicio;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getValoresDataFim($format = false)
    {
        $valoresDataFim = $this->valoresDataFim;

        if ($format && $valoresDataFim) {
            $valoresDataFim = $valoresDataFim->format('d/m/Y');
        }

        return $valoresDataFim;
    }

    /**
     * @param \Datetime $valoresDataFim
     * @return FinanceiroValores
     */
    public function setValoresDataFim($valoresDataFim)
    {
        if ($valoresDataFim) {
            if (is_string($valoresDataFim)) {
                $valoresDataFim = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $valoresDataFim
                );
                $valoresDataFim = new \Datetime($valoresDataFim);
            }
        } else {
            $valoresDataFim = null;
        }
        $this->valoresDataFim = $valoresDataFim;

        return $this;
    }

    /**
     * @return integer
     */
    public function getValoresParcela()
    {
        return $this->valoresParcela;
    }

    /**
     * @param integer $valoresParcela
     * @return FinanceiroValores
     */
    public function setValoresParcela($valoresParcela)
    {
        $this->valoresParcela = $valoresParcela;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'valoresId'         => $this->getValoresId(),
            'cursocampus'       => $this->getCursocampus(),
            'per'               => $this->getPer(),
            'tipotitulo'        => $this->getTipotitulo(),
            'area'              => $this->getArea(),
            'valoresPreco'      => $this->getValoresPreco(),
            'valoresDataInicio' => $this->getValoresDataInicio(true),
            'valoresDataFim'    => $this->getValoresDataFim(true),
            'valoresParcela'    => $this->getValoresParcela(),
        );

        if ($this->getPer()) {
            $array['perAno'] = $this->getPer() ? $this->getPer()->getPerDataInicio()->format('Y') : '';
            $array['perMes'] = $this->getPer() ? $this->getPer()->getPerDataInicio()->format('m') : '';
            $array['per'] = $this->getPer() ? $this->getPer()->getPerId() : null;
        }

        $array['cursocampus'] = $this->getCursocampus() ? $this->getCursocampus()->getCursocampusId() : null;
        $array['tipotitulo']  = $this->getTipotitulo() ? $this->getTipotitulo()->getTipotituloId() : null;
        $array['area']        = $this->getArea() ? $this->getArea()->getAreaId() : null;

        return $array;
    }
}
