<?php

namespace Financeiro\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * FinanceiroMeioPagamento
 *
 * @ORM\Table(name="financeiro__meio_pagamento")
 * @ORM\Entity
 * @LG\LG(id="meioPagamentoId",label="MeioPagamentoDescricao")
 * @Jarvis\Jarvis(title="Listagem de meio de pagamento",icon="fa fa-table")
 */
class FinanceiroMeioPagamento
{
    /**
     * @var integer
     *
     * @ORM\Column(name="meio_pagamento_id", type="integer", nullable=false, length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="meio_pagamento_id")
     * @LG\Labels\Attributes(text="código")
     * @LG\Querys\Conditions(type="=")
     */
    private $meioPagamentoId;

    /**
     * @var string
     *
     * @ORM\Column(name="meio_pagamento_descricao", type="string", nullable=false, length=45)
     * @LG\Labels\Property(name="meio_pagamento_descricao")
     * @LG\Labels\Attributes(text="descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $meioPagamentoDescricao;

    /**
     * @var string
     *
     * @ORM\Column(name="meio_pagamento_tipo", type="string", nullable=true, length=6)
     * @LG\Labels\Property(name="meio_pagamento_tipo")
     * @LG\Labels\Attributes(text="tipo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $meioPagamentoTipo;

    /**
     * @var string
     *
     * @ORM\Column(name="meio_pagamento_uso_externo", type="string", nullable=true, length=3)
     * @LG\Labels\Property(name="meio_pagamento_uso_externo")
     * @LG\Labels\Attributes(text="externo uso")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $meioPagamentoUsoExterno;

    /**
     * @var string
     *
     * @ORM\Column(name="meio_pagamento_status", type="string", nullable=true, length=7)
     * @LG\Labels\Property(name="meio_pagamento_status")
     * @LG\Labels\Attributes(text="status")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $meioPagamentoStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="meio_pagamento_uso_multiplo", type="string", nullable=true, length=3)
     * @LG\Labels\Property(name="meio_pagamento_uso_multiplo")
     * @LG\Labels\Attributes(text="multiplo uso")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $meioPagamentoUsoMultiplo;

    /**
     * @var string
     *
     * @ORM\Column(name="meio_pagamento_campos_cheque", type="string", nullable=true, length=3)
     * @LG\Labels\Property(name="meio_pagamento_campos_cheque")
     * @LG\Labels\Attributes(text="campo de cheque")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $meioPagamentoCamposCheque;

    /**
     * @return integer
     */
    public function getMeioPagamentoId()
    {
        return $this->meioPagamentoId;
    }

    /**
     * @param integer $meioPagamentoId
     * @return FinanceiroMeioPagamento
     */
    public function setMeioPagamentoId($meioPagamentoId)
    {
        $this->meioPagamentoId = $meioPagamentoId;

        return $this;
    }

    /**
     * @return string
     */
    public function getMeioPagamentoDescricao()
    {
        return $this->meioPagamentoDescricao;
    }

    /**
     * @return boolean
     */
    public function getMeioPagamentoPossuiCamposCheque()
    {
        $camposSim = \Financeiro\Service\FinanceiroMeioPagamento::MEIO_PAGAMENTO_CAMPOS_CHEQUE_SIM;

        return $this->getMeioPagamentoCamposCheque() == $camposSim;
    }

    /**
     * @param string $meioPagamentoDescricao
     * @return FinanceiroMeioPagamento
     */
    public function setMeioPagamentoDescricao($meioPagamentoDescricao)
    {
        $this->meioPagamentoDescricao = $meioPagamentoDescricao;

        return $this;
    }

    /**
     * @return string
     */
    public function getMeioPagamentoTipo()
    {
        return $this->meioPagamentoTipo;
    }

    /**
     * @param string $meioPagamentoTipo
     * @return FinanceiroMeioPagamento
     */
    public function setMeioPagamentoTipo($meioPagamentoTipo)
    {
        $this->meioPagamentoTipo = $meioPagamentoTipo;

        return $this;
    }

    /**
     * @return string
     */
    public function getMeioPagamentoUsoExterno()
    {
        return $this->meioPagamentoUsoExterno;
    }

    /**
     * @param string $meioPagamentoUsoExterno
     * @return FinanceiroMeioPagamento
     */
    public function setMeioPagamentoUsoExterno($meioPagamentoUsoExterno)
    {
        $this->meioPagamentoUsoExterno = $meioPagamentoUsoExterno;

        return $this;
    }

    /**
     * @return string
     */
    public function getMeioPagamentoStatus()
    {
        return $this->meioPagamentoStatus;
    }

    /**
     * @param string $meioPagamentoStatus
     * @return FinanceiroMeioPagamento
     */
    public function setMeioPagamentoStatus($meioPagamentoStatus)
    {
        $this->meioPagamentoStatus = $meioPagamentoStatus;

        return $this;
    }

    /**
     * @return string
     */
    public function getMeioPagamentoUsoMultiplo()
    {
        return $this->meioPagamentoUsoMultiplo;
    }

    /**
     * @param string $meioPagamentoUsoMultiplo
     * @return FinanceiroMeioPagamento
     */
    public function setMeioPagamentoUsoMultiplo($meioPagamentoUsoMultiplo)
    {
        $this->meioPagamentoUsoMultiplo = $meioPagamentoUsoMultiplo;

        return $this;
    }

    /**
     * @return string
     */
    public function getMeioPagamentoCamposCheque()
    {
        return $this->meioPagamentoCamposCheque;
    }

    /**
     * @param string $meioPagamentoCamposCheque
     * @return FinanceiroMeioPagamento
     */
    public function setMeioPagamentoCamposCheque($meioPagamentoCamposCheque)
    {
        $this->meioPagamentoCamposCheque = $meioPagamentoCamposCheque;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'meioPagamentoId'           => $this->getMeioPagamentoId(),
            'meioPagamentoDescricao'    => $this->getMeioPagamentoDescricao(),
            'meioPagamentoTipo'         => $this->getMeioPagamentoTipo(),
            'meioPagamentoUsoExterno'   => $this->getMeioPagamentoUsoExterno(),
            'meioPagamentoStatus'       => $this->getMeioPagamentoStatus(),
            'meioPagamentoUsoMultiplo'  => $this->getMeioPagamentoUsoMultiplo(),
            'meioPagamentoCamposCheque' => $this->getMeioPagamentoCamposCheque(),
        );

        return $array;
    }
}
