<?php

namespace Financeiro\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * FinanceiroCartao
 *
 * @ORM\Table(name="financeiro__cartao")
 * @ORM\Entity
 * @LG\LG(id="cartaoId",label="CartaoTitular")
 * @Jarvis\Jarvis(title="Listagem de cartão",icon="fa fa-table")
 */
class FinanceiroCartao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="cartao_id", type="integer", nullable=false, length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="cartao_id")
     * @LG\Labels\Attributes(text="código cartão")
     * @LG\Querys\Conditions(type="=")
     */
    private $cartaoId;

    /**
     * @var string
     *
     * @ORM\Column(name="cartao_titular", type="string", nullable=false, length=70)
     * @LG\Labels\Property(name="cartao_titular")
     * @LG\Labels\Attributes(text="titular")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $cartaoTitular;

    /**
     * @var string
     *
     * @ORM\Column(name="cartao_numero", type="string", nullable=false, length=50)
     * @LG\Labels\Property(name="cartao_numero")
     * @LG\Labels\Attributes(text="número")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $cartaoNumero;

    /**
     * @var string
     *
     * @ORM\Column(name="cartao_vencimento", type="string", nullable=false, length=50)
     * @LG\Labels\Property(name="cartao_vencimento")
     * @LG\Labels\Attributes(text="vencimento")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $cartaoVencimento;

    /**
     * @var string
     *
     * @ORM\Column(name="cartao_cvv", type="string", nullable=false, length=50)
     * @LG\Labels\Property(name="cartao_cvv")
     * @LG\Labels\Attributes(text="cvv")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $cartaoCvv;

    /**
     * @var string
     *
     * @ORM\Column(name="cartao_excluido", type="string", nullable=false, length=3)
     * @LG\Labels\Property(name="cartao_excluido")
     * @LG\Labels\Attributes(text="excluido")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $cartaoExcluido;

    /**
     * @var \Pessoa\Entity\Pessoa
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     */
    private $pes;

    /**
     * @return integer
     */
    public function getCartaoId()
    {
        return $this->cartaoId;
    }

    /**
     * @param integer $cartaoId
     * @return FinanceiroCartao
     */
    public function setCartaoId($cartaoId)
    {
        $this->cartaoId = $cartaoId;

        return $this;
    }

    /**
     * @return string
     */
    public function getCartaoTitular()
    {
        return $this->cartaoTitular;
    }

    /**
     * @param string $cartaoTitular
     * @return FinanceiroCartao
     */
    public function setCartaoTitular($cartaoTitular)
    {
        $this->cartaoTitular = $cartaoTitular;

        return $this;
    }

    /**
     * @return string
     */
    public function getCartaoNumero()
    {
        return $this->cartaoNumero;
    }

    /**
     * @param string $cartaoNumero
     * @return FinanceiroCartao
     */
    public function setCartaoNumero($cartaoNumero)
    {
        $this->cartaoNumero = $cartaoNumero;

        return $this;
    }

    /**
     * @return string
     */
    public function getCartaoVencimento()
    {
        return $this->cartaoVencimento;
    }

    /**
     * @param string $cartaoVencimento
     * @return FinanceiroCartao
     */
    public function setCartaoVencimento($cartaoVencimento)
    {
        $this->cartaoVencimento = $cartaoVencimento;

        return $this;
    }

    /**
     * @return string
     */
    public function getCartaoCvv()
    {
        return $this->cartaoCvv;
    }

    /**
     * @param string $cartaoCvv
     * @return FinanceiroCartao
     */
    public function setCartaoCvv($cartaoCvv)
    {
        $this->cartaoCvv = $cartaoCvv;

        return $this;
    }

    /**
     * @return string
     */
    public function getCartaoExcluido()
    {
        return $this->cartaoExcluido;
    }

    /**
     * @param string $cartaoExcluido
     * @return FinanceiroCartao
     */
    public function setCartaoExcluido($cartaoExcluido)
    {
        $this->cartaoExcluido = $cartaoExcluido;

        return $this;
    }

    /**
     * @return \Pessoa\Entity\Pessoa
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @param \Pessoa\Entity\Pessoa $pes
     * @return FinanceiroCartao
     */
    public function setPes($pes)
    {
        $this->pes = $pes;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'cartaoId'         => $this->getCartaoId(),
            'cartaoTitular'    => $this->getCartaoTitular(),
            'cartaoNumero'     => $this->getCartaoNumero(),
            'cartaoVencimento' => $this->getCartaoVencimento(),
            'cartaoCvv'        => $this->getCartaoCvv(),
            'cartaoExcluido'   => $this->getCartaoExcluido(),
            'pes'              => $this->getPes(),
        );

        $array['pes'] = $this->getPes() ? $this->getPes()->getPesId() : null;

        return $array;
    }
}
