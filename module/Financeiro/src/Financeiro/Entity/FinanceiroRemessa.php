<?php

namespace Financeiro\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FinanceiroRemessa
 *
 * @ORM\Table(name="financeiro__remessa", uniqueConstraints={@ORM\UniqueConstraint(name="remessa_codigo_UNIQUE", columns={"remessa_codigo", "confcont_id"})}, indexes={@ORM\Index(name="fk_financeiro__remessa_arquivo1_idx", columns={"arq_id"}), @ORM\Index(name="IDX_EE656E1518DBE410", columns={"confcont_id"})})
 * @ORM\Entity
 */
class FinanceiroRemessa
{
    /**
     * @var integer
     *
     * @ORM\Column(name="remessa_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $remessaId;

    /**
     * @var integer
     *
     * @ORM\Column(name="remessa_codigo", type="integer", nullable=false)
     */
    private $remessaCodigo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="remessa_data", type="datetime", nullable=false)
     */
    private $remessaData;

    /**
     * @var \GerenciadorArquivos\Entity\Arquivo
     *
     * @ORM\ManyToOne(targetEntity="GerenciadorArquivos\Entity\Arquivo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="arq_id", referencedColumnName="arq_id")
     * })
     */
    private $arq;

    /**
     * @var \Financeiro\Entity\BoletoConfConta
     *
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\BoletoConfConta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="confcont_id", referencedColumnName="confcont_id")
     * })
     */
    private $confcont;

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    public function setRemessaId($remessaId)
    {
        $this->remessaId = $remessaId;
        
        return  $this;
    }

    public function getRemessaId()
    {
        return $this->remessaId;
    }
    
    public function setRemessaCodigo($remessaCodigo)
    {
        $this->remessaCodigo = $remessaCodigo;

        return  $this;
    }
    
    public function getRemessaCodigo()
    {
        return $this->remessaCodigo;
    }
    
    public function setRemessaData($remessaData)
    {
        $this->remessaData = $remessaData;

        return  $this;
    }
    
    public function getRemessaData()
    {
        return $this->remessaData;
    }
    
    public function setArq($arq)
    {
        $this->arq = $arq;

        return  $this;
    }
    
    public function getArq()
    {
        return $this->arq;
    }

    public function setConfcont($confcont)
    {
        $this->confcont = $confcont;

        return  $this;
    }

    public function getConfcont()
    {
        return $this->confcont;
    }

    public function toArray()
    {
        return [
            'remessaId'     => $this->getRemessaId(),
            'remessaCodigo' => $this->getRemessaCodigo(),
            'remessaData'   => $this->getRemessaData(),
            'arq'           => $this->getArq(),
            'confcont'      => $this->getConfcont(),
        ];
    }
}

