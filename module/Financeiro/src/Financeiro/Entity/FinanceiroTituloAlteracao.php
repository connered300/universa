<?php

namespace Financeiro\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * FinanceiroTituloAlteracao
 *
 * @ORM\Table(name="financeiro__titulo_alteracao")
 * @ORM\Entity
 * @LG\LG(id="alteracaoId",label="TituloIdOrigem")
 * @Jarvis\Jarvis(title="Listagem de título alteração",icon="fa fa-table")
 */
class FinanceiroTituloAlteracao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="alteracao_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="alteracao_id")
     * @LG\Labels\Attributes(text="código alteração")
     * @LG\Querys\Conditions(type="=")
     */
    private $alteracaoId;
    /**
     * @var \Financeiro\Entity\FinanceiroTitulo
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\FinanceiroTitulo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="titulo_id_origem", referencedColumnName="titulo_id")
     * })
     */
    private $tituloIdOrigem;
    /**
     * @var \Financeiro\Entity\FinanceiroTitulo
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\FinanceiroTitulo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="titulo_id_destino", referencedColumnName="titulo_id")
     * })
     */
    private $tituloIdDestino;
    /**
     * @var string
     *
     * @ORM\Column(name="alteracao_acao", type="string", nullable=true, length=12)
     * @LG\Labels\Property(name="alteracao_acao")
     * @LG\Labels\Attributes(text="ação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $alteracaoAcao;

    /**
     * @return integer
     */
    public function getAlteracaoId()
    {
        return $this->alteracaoId;
    }

    /**
     * @param integer $alteracaoId
     * @return FinanceiroTituloAlteracao
     */
    public function setAlteracaoId($alteracaoId)
    {
        $this->alteracaoId = $alteracaoId;

        return $this;
    }

    /**
     * @return \Financeiro\Entity\FinanceiroTitulo
     */
    public function getTituloIdOrigem()
    {
        return $this->tituloIdOrigem;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroTitulo $tituloIdOrigem
     * @return FinanceiroTituloAlteracao
     */
    public function setTituloIdOrigem($tituloIdOrigem)
    {
        $this->tituloIdOrigem = $tituloIdOrigem;

        return $this;
    }

    /**
     * @return \Financeiro\Entity\FinanceiroTitulo
     */
    public function getTituloIdDestino()
    {
        return $this->tituloIdDestino;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroTitulo $tituloIdDestino
     * @return FinanceiroTituloAlteracao
     */
    public function setTituloIdDestino($tituloIdDestino)
    {
        $this->tituloIdDestino = $tituloIdDestino;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlteracaoAcao()
    {
        return $this->alteracaoAcao;
    }

    /**
     * @param string $alteracaoAcao
     * @return FinanceiroTituloAlteracao
     */
    public function setAlteracaoAcao($alteracaoAcao)
    {
        $this->alteracaoAcao = $alteracaoAcao;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'alteracaoId'     => $this->getAlteracaoId(),
            'tituloIdOrigem'  => $this->getTituloIdOrigem(),
            'tituloIdDestino' => $this->getTituloIdDestino(),
            'alteracaoAcao'   => $this->getAlteracaoAcao(),
        );

        $array['tituloIdOrigem']  = $this->getTituloIdOrigem() ? $this->getTituloIdOrigem()->getTituloId() : null;
        $array['tituloIdDestino'] = $this->getTituloIdDestino() ? $this->getTituloIdDestino()->getTituloId() : null;

        return $array;
    }
}
