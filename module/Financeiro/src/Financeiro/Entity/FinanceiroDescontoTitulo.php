<?php

namespace Financeiro\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * FinanceiroDescontoTitulo
 *
 * @ORM\Table(name="financeiro__desconto_titulo")
 * @ORM\Entity
 * @LG\LG(id="descontoTituloId",label="DescontoId")
 * @Jarvis\Jarvis(title="Listagem de desconto título",icon="fa fa-table")
 */
class FinanceiroDescontoTitulo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="desconto_titulo_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="desconto_titulo_id")
     * @LG\Labels\Attributes(text="código título desconto")
     * @LG\Querys\Conditions(type="=")
     */
    private $descontoTituloId;

    /**
     * @var \Financeiro\Entity\FinanceiroDesconto
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\FinanceiroDesconto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="desconto_id", referencedColumnName="desconto_id")
     * })
     */
    private $desconto;

    /**
     * @var \Financeiro\Entity\FinanceiroTitulo
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\FinanceiroTitulo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="titulo_id", referencedColumnName="titulo_id")
     * })
     */
    private $titulo;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_alteracao", referencedColumnName="id")
     * })
     */
    private $usuarioAlteracao;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_criacao", referencedColumnName="id")
     * })
     */
    private $usuarioCriacao;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="desconto_titulo_criacao", type="datetime", nullable=false)
     * @LG\Labels\Property(name="desconto_titulo_criacao")
     * @LG\Labels\Attributes(text="criação")
     * @LG\Querys\Conditions(type="=")
     */
    private $descontoTituloCriacao;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="desconto_titulo_alteracao", type="datetime", nullable=false)
     * @LG\Labels\Property(name="desconto_titulo_alteracao")
     * @LG\Labels\Attributes(text="alteração")
     * @LG\Querys\Conditions(type="=")
     */
    private $descontoTituloAlteracao;

    /**
     * @var string
     *
     * @ORM\Column(name="desconto_titulo_aplicado", type="string", nullable=false, length=3)
     * @LG\Labels\Property(name="desconto_titulo_aplicado")
     * @LG\Labels\Attributes(text="aplicado")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $descontoTituloAplicado;

    /**
     * @var float
     *
     * @ORM\Column(name="desconto_titulo_valor", type="float", nullable=true)
     * @LG\Labels\Property(name="desconto_titulo_valor")
     * @LG\Labels\Attributes(text="valor")
     * @LG\Querys\Conditions(type="=")
     */
    private $descontoTituloValor;

    /**
     * @var string
     *
     * @ORM\Column(name="desconto_titulo_situacao", type="string", nullable=false, length=8)
     * @LG\Labels\Property(name="desconto_titulo_situacao")
     * @LG\Labels\Attributes(text="situação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $descontoTituloSituacao;

    /**
     * @return integer
     */
    public function getDescontoTituloId()
    {
        return $this->descontoTituloId;
    }

    /**
     * @param integer $descontoTituloId
     * @return FinanceiroDescontoTitulo
     */
    public function setDescontoTituloId($descontoTituloId)
    {
        $this->descontoTituloId = $descontoTituloId;

        return $this;
    }

    /**
     * @return \Financeiro\Entity\FinanceiroDesconto
     */
    public function getDesconto()
    {
        return $this->desconto;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroDesconto $desconto
     * @return FinanceiroDescontoTitulo
     */
    public function setDesconto($desconto)
    {
        $this->desconto = $desconto;

        return $this;
    }

    /**
     * @return \Financeiro\Entity\FinanceiroTitulo
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroTitulo $titulo
     * @return FinanceiroDescontoTitulo
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuarioAlteracao()
    {
        return $this->usuarioAlteracao;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuarioAlteracao
     * @return FinanceiroDescontoTitulo
     */
    public function setUsuarioAlteracao($usuarioAlteracao)
    {
        $this->usuarioAlteracao = $usuarioAlteracao;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuarioCriacao()
    {
        return $this->usuarioCriacao;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuarioCriacao
     * @return FinanceiroDescontoTitulo
     */
    public function setUsuarioCriacao($usuarioCriacao)
    {
        $this->usuarioCriacao = $usuarioCriacao;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getDescontoTituloCriacao($format = false)
    {
        $descontoTituloCriacao = $this->descontoTituloCriacao;

        if ($format && $descontoTituloCriacao) {
            $descontoTituloCriacao = $descontoTituloCriacao->format('d/m/Y H:i:s');
        }

        return $descontoTituloCriacao;
    }

    /**
     * @param \Datetime $descontoTituloCriacao
     * @return FinanceiroDescontoTitulo
     */
    public function setDescontoTituloCriacao($descontoTituloCriacao)
    {
        if ($descontoTituloCriacao) {
            if (is_string($descontoTituloCriacao)) {
                $descontoTituloCriacao = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $descontoTituloCriacao
                );
                $descontoTituloCriacao = new \Datetime($descontoTituloCriacao);
            }
        } else {
            $descontoTituloCriacao = null;
        }
        $this->descontoTituloCriacao = $descontoTituloCriacao;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getDescontoTituloAlteracao($format = false)
    {
        $descontoTituloAlteracao = $this->descontoTituloAlteracao;

        if ($format && $descontoTituloAlteracao) {
            $descontoTituloAlteracao = $descontoTituloAlteracao->format('d/m/Y H:i:s');
        }

        return $descontoTituloAlteracao;
    }

    /**
     * @param \Datetime $descontoTituloAlteracao
     * @return FinanceiroDescontoTitulo
     */
    public function setDescontoTituloAlteracao($descontoTituloAlteracao)
    {
        if ($descontoTituloAlteracao) {
            if (is_string($descontoTituloAlteracao)) {
                $descontoTituloAlteracao = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $descontoTituloAlteracao
                );
                $descontoTituloAlteracao = new \Datetime($descontoTituloAlteracao);
            }
        } else {
            $descontoTituloAlteracao = null;
        }
        $this->descontoTituloAlteracao = $descontoTituloAlteracao;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescontoTituloAplicado()
    {
        return $this->descontoTituloAplicado;
    }

    /**
     * @param string $descontoTituloAplicado
     * @return FinanceiroDescontoTitulo
     */
    public function setDescontoTituloAplicado($descontoTituloAplicado)
    {
        $this->descontoTituloAplicado = $descontoTituloAplicado;

        return $this;
    }

    /**
     * @return float
     */
    public function getDescontoTituloValor()
    {
        return $this->descontoTituloValor;
    }

    /**
     * @param float $descontoTituloValor
     * @return FinanceiroDescontoTitulo
     */
    public function setDescontoTituloValor($descontoTituloValor)
    {
        $this->descontoTituloValor = $descontoTituloValor;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescontoTituloSituacao()
    {
        return $this->descontoTituloSituacao;
    }

    /**
     * @param string $descontoTituloSituacao
     * @return FinanceiroDescontoTitulo
     */
    public function setDescontoTituloSituacao($descontoTituloSituacao)
    {
        $this->descontoTituloSituacao = $descontoTituloSituacao;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'descontoTituloId'        => $this->getDescontoTituloId(),
            'desconto'                => $this->getDesconto(),
            'titulo'                  => $this->getTitulo(),
            'usuarioAlteracao'        => $this->getUsuarioAlteracao(),
            'usuarioCriacao'          => $this->getUsuarioCriacao(),
            'descontoTituloCriacao'   => $this->getDescontoTituloCriacao(true),
            'descontoTituloAlteracao' => $this->getDescontoTituloAlteracao(true),
            'descontoTituloAplicado'  => $this->getDescontoTituloAplicado(),
            'descontoTituloValor'     => $this->getDescontoTituloValor(),
            'descontoTituloSituacao'  => $this->getDescontoTituloSituacao(),
        );

        $array['desconto']         = $this->getDesconto() ? $this->getDesconto()->getDescontoId() : null;
        $array['titulo']           = $this->getTitulo() ? $this->getTitulo()->getTituloId() : null;
        $array['usuarioAlteracao'] = $this->getUsuarioAlteracao() ? $this->getUsuarioAlteracao()->getId() : null;
        $array['usuarioCriacao']   = $this->getUsuarioCriacao() ? $this->getUsuarioCriacao()->getId() : null;

        return $array;
    }
}
