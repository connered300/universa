<?php

namespace Financeiro\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class FinanceiroTituloAlteracao
 * @package Financeiro\Service
 */
class FinanceiroTituloAlteracao extends AbstractService
{
    const ALTERACAO_ACAO_ATUALIZACAO        = 'Atualizacao';
    const ALTERACAO_ACAO_PARCELAMENTO       = 'Parcelamento';
    const ALTERACAO_ACAO_RENEGOCIACAO       = 'Renegociacao';
    const ALTERACAO_ACAO_PAGAMENTO_MULTIPLO = 'Pagamento Múltiplo';
    const ALTERACAO_ACAO_ESTORNO            = 'Estorno';
    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Financeiro\Entity\FinanceiroTituloAlteracao');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @param $params
     * @return array
     */
    public function pesquisaForJson($params)
    {
        $sql            = '
        SELECT alteracaoId AS alteracao_id, tituloIdOrigem AS titulo_id_origem, tituloIdDestino AS titulo_id_destino, alteracaoAcao AS alteracao_acao
        FROM Financeiro\Entity\FinanceiroTituloAlteracao
        WHERE';
        $tituloIdOrigem = false;
        $alteracaoId    = false;

        if ($params['q']) {
            $tituloIdOrigem = $params['q'];
        } elseif ($params['query']) {
            $tituloIdOrigem = $params['query'];
        }

        if ($params['alteracaoId']) {
            $alteracaoId = $params['alteracaoId'];
        }

        $parameters = array('tituloIdOrigem' => "{$tituloIdOrigem}%");
        $sql .= ' tituloIdOrigem LIKE :tituloIdOrigem';

        if ($alteracaoId) {
            $parameters['alteracaoId'] = explode(',', $alteracaoId);
            $parameters['alteracaoId'] = $alteracaoId;
            $sql .= ' AND alteracaoId NOT IN(:alteracaoId)';
        }

        $sql .= " ORDER BY tituloIdOrigem";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['alteracaoId']) {
                /** @var $objFinanceiroTituloAlteracao \Financeiro\Entity\FinanceiroTituloAlteracao */
                $objFinanceiroTituloAlteracao = $this->getRepository()->find($arrDados['alteracaoId']);

                if (!$objFinanceiroTituloAlteracao) {
                    $this->setLastError('Registro de título alteração não existe!');

                    return false;
                }
            } else {
                $objFinanceiroTituloAlteracao = new \Financeiro\Entity\FinanceiroTituloAlteracao();
            }

            if ($arrDados['tituloIdOrigem']) {
                /** @var $objFinanceiroTitulo \Financeiro\Entity\FinanceiroTitulo */
                $objFinanceiroTitulo = $serviceFinanceiroTitulo->getRepository()->find($arrDados['tituloIdOrigem']);

                if (!$objFinanceiroTitulo) {
                    $this->setLastError('Registro de título não existe!');

                    return false;
                }

                $objFinanceiroTituloAlteracao->setTituloIdOrigem($objFinanceiroTitulo);
            } else {
                $objFinanceiroTituloAlteracao->setTituloIdOrigem(null);
            }

            if ($arrDados['tituloIdDestino']) {
                /** @var $objFinanceiroTitulo \Financeiro\Entity\FinanceiroTitulo */
                $objFinanceiroTitulo = $serviceFinanceiroTitulo->getRepository()->find($arrDados['tituloIdDestino']);

                if (!$objFinanceiroTitulo) {
                    $this->setLastError('Registro de título não existe!');

                    return false;
                }

                $objFinanceiroTituloAlteracao->setTituloIdDestino($objFinanceiroTitulo);
            } else {
                $objFinanceiroTituloAlteracao->setTituloIdDestino(null);
            }

            $objFinanceiroTituloAlteracao->setAlteracaoAcao($arrDados['alteracaoAcao']);

            $this->getEm()->persist($objFinanceiroTituloAlteracao);
            $this->getEm()->flush($objFinanceiroTituloAlteracao);

            $this->getEm()->commit();

            $arrDados['alteracaoId'] = $objFinanceiroTituloAlteracao->getAlteracaoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de título alteração!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['tituloIdOrigem']) {
            $errors[] = 'Por favor preencha o campo "origem código"!';
        }

        if (!$arrParam['tituloIdDestino']) {
            $errors[] = 'Por favor preencha o campo "destino código"!';
        }

        if (!in_array($arrParam['alteracaoAcao'], self::getAlteracaoAcao())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "ação"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public static function getAlteracaoAcao()
    {
        return array(
            self::ALTERACAO_ACAO_ATUALIZACAO,
            self::ALTERACAO_ACAO_PARCELAMENTO,
            self::ALTERACAO_ACAO_RENEGOCIACAO,
            self::ALTERACAO_ACAO_PAGAMENTO_MULTIPLO
        );
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM financeiro__titulo_alteracao";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $alteracaoId
     * @return array
     */
    public function getArray($alteracaoId)
    {
        /** @var $objFinanceiroTituloAlteracao \Financeiro\Entity\FinanceiroTituloAlteracao */
        $objFinanceiroTituloAlteracao = $this->getRepository()->find($alteracaoId);
        $serviceFinanceiroTitulo      = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceFinanceiroTitulo      = new \Financeiro\Service\FinanceiroTitulo($this->getEm());

        try {
            $arrDados = $objFinanceiroTituloAlteracao->toArray();

            if ($arrDados['tituloIdOrigem']) {
                /** @var $objFinanceiroTitulo \Financeiro\Entity\FinanceiroTitulo */
                $objFinanceiroTitulo = $serviceFinanceiroTitulo->getRepository()->find($arrDados['tituloIdOrigem']);

                if ($objFinanceiroTitulo) {
                    $arrDados['tituloIdOrigem'] = array(
                        'id'   => $objFinanceiroTitulo->getTituloId(),
                        'text' => $objFinanceiroTitulo->getTituloId()
                    );
                } else {
                    $arrDados['tituloIdOrigem'] = null;
                }
            }

            if ($arrDados['tituloIdDestino']) {
                /** @var $objFinanceiroTitulo \Financeiro\Entity\FinanceiroTitulo */
                $objFinanceiroTitulo = $serviceFinanceiroTitulo->getRepository()->find($arrDados['tituloIdDestino']);

                if ($objFinanceiroTitulo) {
                    $arrDados['tituloIdDestino'] = array(
                        'id'   => $objFinanceiroTitulo->getTituloId(),
                        'text' => $objFinanceiroTitulo->getTituloId()
                    );
                } else {
                    $arrDados['tituloIdDestino'] = null;
                }
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo($this->getEm());

        if (is_array($arrDados['tituloIdOrigem']) && !$arrDados['tituloIdOrigem']['id']) {
            $arrDados['tituloIdOrigem']['id']   = $arrDados['tituloIdOrigem']['tituloId'];
            $arrDados['tituloIdOrigem']['text'] = $arrDados['tituloIdOrigem']['tituloId'];
        } elseif ($arrDados['tituloIdOrigem'] && is_string($arrDados['tituloIdOrigem'])) {
            $arrDados['tituloIdOrigem'] = $serviceFinanceiroTitulo->getArrSelect2(
                array('id' => $arrDados['tituloIdOrigem'])
            );
        }

        if (is_array($arrDados['tituloIdDestino']) && !$arrDados['tituloIdDestino']['id']) {
            $arrDados['tituloIdDestino']['id']   = $arrDados['tituloIdDestino']['tituloId'];
            $arrDados['tituloIdDestino']['text'] = $arrDados['tituloIdDestino']['tituloId'];
        } elseif ($arrDados['tituloIdDestino'] && is_string($arrDados['tituloIdDestino'])) {
            $arrDados['tituloIdDestino'] = $serviceFinanceiroTitulo->getArrSelect2(
                array('id' => $arrDados['tituloIdDestino'])
            );
        }

        return $arrDados;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('alteracaoId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Financeiro\Entity\FinanceiroTituloAlteracao */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getAlteracaoId(),
                $params['value'] => $objEntity->getTituloIdOrigem()
            );
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['alteracaoId']) {
            $this->setLastError('Para remover um registro de título alteração é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objFinanceiroTituloAlteracao \Financeiro\Entity\FinanceiroTituloAlteracao */
            $objFinanceiroTituloAlteracao = $this->getRepository()->find($param['alteracaoId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objFinanceiroTituloAlteracao);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de título alteração.');

            return false;
        }

        return true;
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo($this->getEm());

        $serviceFinanceiroTitulo->setarDependenciasView($view);
        $serviceFinanceiroTitulo->setarDependenciasView($view);

        $view->setVariable("arrAlteracaoAcao", $this->getArrSelect2AlteracaoAcao());
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2AlteracaoAcao($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getAlteracaoAcao());
    }

    /**
     * @param int $tituloIdOrigem
     * @return array
     */
    public function getArrayBoletosTitulos($tituloIdOrigem = false, $tituloIdDestino = false)
    {
        $param = [];
        $query = "
        SELECT
            a.*,
            fto.titulo_valor_pago AS titulo_valor_pago_origem,
            fto.titulo_valor AS titulo_valor_origem,
            ftd.titulo_valor_pago AS titulo_valor_pago_destino,
            ftd.titulo_valor AS titulo_valor_destino,
            fto.titulo_estado AS titulo_estado_origem,
            ftd.titulo_estado AS titulo_estado_destino,
            fto.titulo_descricao AS titulo_descricao_origem,
            ftd.titulo_descricao AS titulo_descricao_destino,
            date_format(fto.titulo_data_vencimento, '%d/%m/%Y')  AS titulo_data_vencimento_origem,
            date_format(ftd.titulo_data_vencimento, '%d/%m/%Y')  AS titulo_data_vencimento_destino
        FROM financeiro__titulo_alteracao a
        INNER JOIN financeiro__titulo fto ON fto.titulo_id=a.titulo_id_origem
        INNER JOIN financeiro__titulo ftd ON ftd.titulo_id=a.titulo_id_destino
        WHERE 1=1";

        if ($tituloIdOrigem) {
            $query .= ' AND titulo_id_origem=:titulo_id_origem';
            $param['titulo_id_origem'] = $tituloIdOrigem;
        }

        if ($tituloIdDestino) {
            $query .= ' AND titulo_id_destino=:titulo_id_destino';
            $param['titulo_id_destino'] = $tituloIdDestino;
        }

        $arrBoletos = $this->executeQueryWithParam($query, $param)->fetchAll();

        return $arrBoletos;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroTitulo $objTituloOrigem
     * @param \Financeiro\Entity\FinanceiroTitulo $objTituloDestino
     * @param string                              $acao
     * @return bool
     */
    public function vincularTitulos(
        \Financeiro\Entity\FinanceiroTitulo $objTituloOrigem,
        \Financeiro\Entity\FinanceiroTitulo $objTituloDestino,
        $acao = \Financeiro\Service\FinanceiroTituloAlteracao::ALTERACAO_ACAO_ATUALIZACAO
    ) {
        try {
            $this->getEm()->beginTransaction();

            $objTituloAlteracao = new \Financeiro\Entity\FinanceiroTituloAlteracao();
            $objTituloAlteracao
                ->setTituloIdOrigem($objTituloOrigem)
                ->setTituloIdDestino($objTituloDestino)
                ->setAlteracaoAcao($acao);

            $this->getEm()->persist($objTituloAlteracao);
            $this->getEm()->flush($objTituloAlteracao);

            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError("Falha ao vincular título!" . $ex->getMessage());

            return false;
        }

        return true;
    }

    public function retornaTitulosvinculadosAoTitulo(
        \Financeiro\Entity\FinanceiroTitulo $objTituloOrigem = null,
        \Financeiro\Entity\FinanceiroTitulo $objTituloDestino = null
    ) {
        $arrParam = [];

        if ($objTituloOrigem) {
            $arrParam['tituloIdOrigem'] = $objTituloOrigem;
        }

        if ($objTituloDestino) {
            $arrParam['tituloIdDestino'] = $objTituloDestino;
        }

        if (!$arrParam) {
            return array();
        }

        $arrTitulosRelacionados = $this->getRepository()->findBy(
            $arrParam
        );

        $arrTitulos = array(
            'origem'  => [],
            'destino' => [],
        );

        /** @var \Financeiro\Entity\FinanceiroTituloAlteracao $objTituloAlteracao */
        foreach ($arrTitulosRelacionados as $objTituloAlteracao) {
            $arrTitulos['origem'][]  = $objTituloAlteracao->getTituloIdOrigem();
            $arrTitulos['destino'][] = $objTituloAlteracao->getTituloIdDestino();
        }

        return $arrTitulos;
    }

    public function retornaTitulosVinculados($tituloId, $agrupar = false, $tituloNotIn = false)
    {
        if (!$tituloId) {
            $this->setLastError("É necessário informar um título!");

            return false;
        }

        $param = ['tituloId' => $tituloId];

        $sql = '
        SELECT titulo_id_origem AS tituloId, titulo_id_destino AS destino
        FROM financeiro__titulo_alteracao
        WHERE titulo_id_destino IN (
          SELECT titulo_id_destino FROM financeiro__titulo_alteracao WHERE titulo_id_origem=:tituloId
        )';

        if ($tituloNotIn) {
            $sql .= " AND titulo_id_origem NOT IN (:tituloId) ";
        }

        $result = $this->executeQueryWithParam($sql, $param)->fetchAll();

        if ($agrupar && $result) {
            $arr    = $result;
            $result = '';

            foreach ($arr as $linha) {
                $result = $result ? $result . ',' . $linha['tituloId'] : $linha['tituloId'];
            }

            if ($arr[0]['destino'] && $tituloNotIn) {
                $result = $arr[0]['destino'] . ',' . $result;
            }
        }

        return $result;
    }
}
?>