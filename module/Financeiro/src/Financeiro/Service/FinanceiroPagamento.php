<?php

namespace Financeiro\Service;

use Respect\Validation\Rules\Json;
use VersaSpine\Service\AbstractService;

/**
 * Class FinanceiroPagamento
 * @package Financeiro\Service
 */
class FinanceiroPagamento extends AbstractService
{
    private $__lastError = null;
    private $config      = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, $arrConfig = array())
    {
        parent::__construct($em, 'Financeiro\Entity\FinanceiroPagamento');
        $this->setConfig(new \Sistema\Service\SisConfig($em, $arrConfig));
    }

    /**
     * @return \Sistema\Service\SisConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param \Sistema\Service\SisConfig $config
     * @return FinanceiroCartao
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @param $params
     * @return array
     */
    public function pesquisaForJson($params)
    {
        $sql                     = '
        SELECT pagamentoId AS pagamento_id, tituloId AS titulo_id, meioPagamentoId AS meio_pagamento_id, pagamentoUsuario AS pagamento_usuario, pesId AS pes_id, pagamentoValorBruto AS pagamento_valor_bruto, pagamentoValorJuros AS pagamento_valor_juros, pagamentoValorMulta AS pagamento_valor_multa, pagamentoAcrescimos AS pagamento_acrescimos, pagamentoValorDescontos AS pagamento_valor_descontos, pagamentoValorFinal AS pagamento_valor_final, pagamentoDescontoManual AS pagamento_desconto_manual, pagamentoDataBaixa AS pagamento_data_baixa, pagamentoObservacoes AS pagamento_observacoes
        FROM Financeiro\Entity\FinanceiroPagamento
        WHERE';
        $pagamentoDescontoManual = false;
        $pagamentoId             = false;

        if ($params['q']) {
            $pagamentoDescontoManual = $params['q'];
        } elseif ($params['query']) {
            $pagamentoDescontoManual = $params['query'];
        }

        if ($params['pagamentoId']) {
            $pagamentoId = $params['pagamentoId'];
        }

        $parameters = array('pagamentoDescontoManual' => "{$pagamentoDescontoManual}%");
        $sql .= ' pagamentoDescontoManual LIKE :pagamentoDescontoManual';

        if ($pagamentoId) {
            $parameters['pagamentoId'] = explode(',', $pagamentoId);
            $parameters['pagamentoId'] = $pagamentoId;
            $sql .= ' AND pagamentoId NOT IN(:pagamentoId)';
        }

        $sql .= " ORDER BY pagamentoDescontoManual";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceMeioPagamento    = new \Financeiro\Service\FinanceiroMeioPagamento($this->getEm());
        $serviceAcessoPessoas    = new \Acesso\Service\AcessoPessoas($this->getEm());
        $servicePessoa           = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceCartao           = new \Financeiro\Service\FinanceiroCartao($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['pagamentoId']) {
                /** @var $objPagamento \Financeiro\Entity\FinanceiroPagamento */
                $objPagamento = $this->getRepository()->find($arrDados['pagamentoId']);

                if (!$objPagamento) {
                    $this->setLastError('Registro de pagamento não existe!');

                    return false;
                }
            } else {
                $objPagamento = new \Financeiro\Entity\FinanceiroPagamento();
            }

            if ($arrDados['titulo']) {
                /** @var $objFinanceiroTitulo \Financeiro\Entity\FinanceiroTitulo */
                $objFinanceiroTitulo = $serviceFinanceiroTitulo->getRepository()->find($arrDados['titulo']);

                if (!$objFinanceiroTitulo) {
                    $this->setLastError('Registro de título não existe!');

                    return false;
                }

                $objPagamento->setTitulo($objFinanceiroTitulo);
            } else {
                $objPagamento->setTitulo(null);
            }

            if ($arrDados['meioPagamento']) {
                /** @var $objMeioPagamento \Financeiro\Entity\FinanceiroMeioPagamento */
                $objMeioPagamento = $serviceMeioPagamento->getRepository()->find($arrDados['meioPagamento']);

                if (!$objMeioPagamento) {
                    $this->setLastError('Registro de meio de pagamento não existe!');

                    return false;
                }

                $objPagamento->setMeioPagamento($objMeioPagamento);
            } else {
                $objPagamento->setMeioPagamento(null);
            }

            if ($arrDados['pagamentoUsuario']) {
                /** @var $objAcessoPessoas \Acesso\Entity\AcessoPessoas */
                $objAcessoPessoas = $serviceAcessoPessoas->getRepository()->find($arrDados['pagamentoUsuario']);

                if (!$objAcessoPessoas) {
                    $this->setLastError('Registro de pessoas não existe!');

                    return false;
                }

                $objPagamento->setPagamentoUsuario($objAcessoPessoas);
            } else {

                /** @var $usuarioLogado \Acesso\Entity\AcessoPessoas */
                $usuarioLogado = $serviceAcessoPessoas->retornaUsuarioLogado();

                $objPagamento->setPagamentoUsuario($usuarioLogado);
            }

            if ($arrDados['pes']) {
                /** @var $objPessoa \Pessoa\Entity\Pessoa */
                $objPessoa = $servicePessoa->getRepository()->find($arrDados['pes']);

                if (!$objPessoa) {
                    $this->setLastError('Registro de pessoa não existe!');

                    return false;
                }

                $objPagamento->setPes($objPessoa);
            } else {
                $objPagamento->setPes(null);
            }

            if ($arrDados['cartao']) {
                /** @var $objFinanceiroCartao \Financeiro\Entity\FinanceiroCartao */
                $objFinanceiroCartao = $serviceCartao->getRepository()->find($arrDados['cartao']);

                if (!$objFinanceiroCartao) {
                    $this->setLastError('Registro de cartão não existe!');

                    return false;
                }

                $objPagamento->setCartao($objFinanceiroCartao);
            } else {
                $objPagamento->setCartao(null);
            }

            $objPagamento
                ->setPagamentoValorBruto(self::convertNumberFromBRToUS($arrDados['pagamentoValorBruto']))
                ->setPagamentoValorJuros($arrDados['pagamentoValorJuros'])
                ->setPagamentoValorMulta($arrDados['pagamentoValorMulta'])
                ->setPagamentoAcrescimos($arrDados['pagamentoAcrescimos'])
                ->setPagamentoValorDescontos($arrDados['pagamentoValorDescontos'])
                ->setPagamentoValorFinal(self::convertNumberFromBRToUS($arrDados['pagamentoValorFinal']))
                ->setPagamentoDescontoManual($arrDados['pagamentoDescontoManual'])
                ->setPagamentoDataBaixa($arrDados['pagamentoDataBaixa'])
                ->setPagamentoObservacoes($arrDados['pagamentoObservacoes'])
                ->setPagamentoRetornoApi($arrDados['retornoApi']);

            $this->getEm()->persist($objPagamento);
            $this->getEm()->flush($objPagamento);

            $this->getEm()->commit();

            $arrDados['pagamentoId'] = $objPagamento->getPagamentoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de pagamento!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['titulo']) {
            $errors[] = 'Por favor preencha o campo "código título"!';
        }

        if (!$arrParam['meioPagamento']) {
            $errors[] = 'Por favor preencha o campo "código pagamento meio"!';
        }

        if (!$arrParam['pes']) {
            $errors[] = 'Por favor preencha o campo "código pessoa"!';
        }

        if (!$arrParam['pagamentoValorBruto']) {
            $errors[] = 'Por favor preencha o campo "bruto valor"!';
        }

        if (!($arrParam['pagamentoValorJuros'] >= 0)) {
            $errors[] = 'Por favor preencha o campo "juros valor"!';
        }

        if (!($arrParam['pagamentoValorMulta'] >= 0)) {
            $errors[] = 'Por favor preencha o campo "multa valor"!';
        }

        if (!($arrParam['pagamentoAcrescimos'] >= 0)) {
            $errors[] = 'Por favor preencha o campo "acrescimos"!';
        }

        if (!($arrParam['pagamentoValorDescontos'] >= 0)) {
            $errors[] = 'Por favor preencha o campo "descontos valor"!';
        }

        if (!$arrParam['pagamentoValorFinal']) {
            $errors[] = 'Por favor preencha o campo "final valor"!';
        }

        if (!($arrParam['pagamentoDescontoManual'] >= 0)) {
            $errors[] = 'Por favor preencha o campo "manual desconto"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM financeiro__pagamento";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $pagamentoId
     * @return array
     */
    public function getArray($pagamentoId)
    {
        /** @var $objFinanceiroPagamento \Financeiro\Entity\FinanceiroPagamento */
        $objFinanceiroPagamento         = $this->getRepository()->find($pagamentoId);
        $serviceFinanceiroTitulo        = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceFinanceiroMeioPagamento = new \Financeiro\Service\FinanceiroMeioPagamento($this->getEm());
        $serviceAcessoPessoas           = new \Acesso\Service\AcessoPessoas($this->getEm());
        $servicePessoa                  = new \Pessoa\Service\Pessoa($this->getEm());

        try {
            $arrDados = $objFinanceiroPagamento->toArray();

            if ($arrDados['titulo']) {
                /** @var $objFinanceiroTitulo \Financeiro\Entity\FinanceiroTitulo */
                $objFinanceiroTitulo = $serviceFinanceiroTitulo->getRepository()->find($arrDados['titulo']);

                if ($objFinanceiroTitulo) {
                    $arrDados['titulo'] = array(
                        'id'   => $objFinanceiroTitulo->getTituloId(),
                        'text' => $objFinanceiroTitulo->getTituloId()
                    );
                } else {
                    $arrDados['titulo'] = null;
                }
            }

            if ($arrDados['meioPagamento']) {
                /** @var $objFinanceiroMeioPagamento \Financeiro\Entity\FinanceiroMeioPagamento */
                $objFinanceiroMeioPagamento = $serviceFinanceiroMeioPagamento->getRepository()->find(
                    $arrDados['meioPagamento']
                );

                if ($objFinanceiroMeioPagamento) {
                    $arrDados['meioPagamento'] = array(
                        'id'   => $objFinanceiroMeioPagamento->getMeioPagamentoId(),
                        'text' => $objFinanceiroMeioPagamento->getMeioPagamentoId()
                    );
                } else {
                    $arrDados['meioPagamento'] = null;
                }
            }

            if ($arrDados['pagamentoUsuario']) {
                /** @var $objAcessoPessoas \Acesso\Entity\AcessoPessoas */
                $objAcessoPessoas = $serviceAcessoPessoas->getRepository()->find($arrDados['pagamentoUsuario']);

                if ($objAcessoPessoas) {
                    $arrDados['pagamentoUsuario'] = array(
                        'id'   => $objAcessoPessoas->getId(),
                        'text' => $objAcessoPessoas->getId()
                    );
                } else {
                    $arrDados['pagamentoUsuario'] = null;
                }
            }

            if ($arrDados['pes']) {
                /** @var $objPessoa \Pessoa\Entity\Pessoa */
                $objPessoa = $servicePessoa->getRepository()->find($arrDados['pes']);

                if ($objPessoa) {
                    $arrDados['pes'] = array(
                        'id'   => $objPessoa->getPesId(),
                        'text' => $objPessoa->getPesId()
                    );
                } else {
                    $arrDados['pes'] = null;
                }
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function meiotaDadosPost(&$arrDados)
    {
        $serviceFinanceiroTitulo        = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceFinanceiroMeioPagamento = new \Financeiro\Service\FinanceiroMeioPagamento($this->getEm());
        $serviceAcessoPessoas           = new \Acesso\Service\AcessoPessoas($this->getEm());
        $servicePessoa                  = new \Pessoa\Service\Pessoa($this->getEm());

        if (is_array($arrDados['titulo']) && !$arrDados['titulo']['id']) {
            $arrDados['titulo']['id']   = $arrDados['titulo']['tituloId'];
            $arrDados['titulo']['text'] = $arrDados['titulo']['tituloId'];
        } elseif ($arrDados['titulo'] && is_string($arrDados['titulo'])) {
            $arrDados['titulo'] = $serviceFinanceiroTitulo->getArrSelect2(
                array('id' => $arrDados['titulo'])
            );
        }

        if (is_array($arrDados['meioPagamento']) && !$arrDados['meioPagamento']['id']) {
            $arrDados['meioPagamento']['id']   = $arrDados['meioPagamento']['meioPagamentoId'];
            $arrDados['meioPagamento']['text'] = $arrDados['meioPagamento']['meioPagamentoId'];
        } elseif ($arrDados['meioPagamento'] && is_string($arrDados['meioPagamento'])) {
            $arrDados['meioPagamento'] = $serviceFinanceiroMeioPagamento->getArrSelect2(
                array('id' => $arrDados['meioPagamento'])
            );
        }

        if (is_array($arrDados['pagamentoUsuario']) && !$arrDados['pagamentoUsuario']['id']) {
            $arrDados['pagamentoUsuario']['id']   = $arrDados['pagamentoUsuario']['id'];
            $arrDados['pagamentoUsuario']['text'] = $arrDados['pagamentoUsuario']['id'];
        } elseif ($arrDados['pagamentoUsuario'] && is_string($arrDados['pagamentoUsuario'])) {
            $arrDados['pagamentoUsuario'] = $serviceAcessoPessoas->getArrSelect2(
                array('id' => $arrDados['pagamentoUsuario'])
            );
        }

        if (is_array($arrDados['pes']) && !$arrDados['pes']['id']) {
            $arrDados['pes']['id']   = $arrDados['pes']['pesId'];
            $arrDados['pes']['text'] = $arrDados['pes']['pesId'];
        } elseif ($arrDados['pes'] && is_string($arrDados['pes'])) {
            $arrDados['pes'] = $servicePessoa->getArrSelect2(
                array('id' => $arrDados['pes'])
            );
        }

        return $arrDados;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('pagamentoId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Financeiro\Entity\FinanceiroPagamento */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getPagamentoId(),
                $params['value'] => $objEntity->getPagamentoDescontoManual()
            );
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['pagamentoId']) {
            $this->setLastError('Para remover um registro de pagamento é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objFinanceiroPagamento \Financeiro\Entity\FinanceiroPagamento */
            $objFinanceiroPagamento = $this->getRepository()->find($param['pagamentoId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objFinanceiroPagamento);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de pagamento.');

            return false;
        }

        return true;
    }

    /**
     * @param $titulo
     * @return bool
     */
    public function removerPagamentosTitulos($titulo)
    {
        if (!$titulo) {
            $this->setLastError('Para remover pagamentos de título é necessário informar o código do mesmo.');

            return false;
        }

        $serviceFinanceiroTituloCheque = new \Financeiro\Service\FinanceiroTituloCheque($this->getEm());
        $serviceBoleto                 = new \Financeiro\Service\Boleto($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            $arrFinanceiroPagamento = $this->getRepository()->findBy(['titulo' => $titulo]);

            /** @var $objFinanceiroPagamento \Financeiro\Entity\FinanceiroPagamento */
            foreach ($arrFinanceiroPagamento as $objFinanceiroPagamento) {
                if (!$serviceFinanceiroTituloCheque->removeReferenciaPagamentoCheque($objFinanceiroPagamento)) {
                    $this->setLastError($serviceFinanceiroTituloCheque->getLastError());

                    return false;
                }

                if (!$serviceBoleto->removeReferenciaPagamentoBoleto($objFinanceiroPagamento)) {
                    $this->setLastError($serviceBoleto->getLastError());

                    return false;
                }

                $this->getEm()->remove($objFinanceiroPagamento);
                $this->getEm()->flush();
            }

            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover pagamentos de títulos.<br>' . $ex->getMessage());

            return false;
        }

        return true;
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        //$serviceFinanceiroTitulo        = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        //$serviceFinanceiroMeioPagamento = new \Financeiro\Service\FinanceiroMeioPagamento($this->getEm());
        //$serviceAcessoPessoas           = new \Acesso\Service\AcessoPessoas($this->getEm());
        //$servicePessoa                  = new \Pessoa\Service\Pessoa($this->getEm());

        //$serviceFinanceiroTitulo->setarDependenciasView($view);
        //$serviceFinanceiroMeioPagamento->setarDependenciasView($view);
        //$serviceAcessoPessoas->setarDependenciasView($view);
        //$servicePessoa->setarDependenciasView($view);
    }

    public function gerarPagamentoUnicoParaTitulo($titulo, $meioPagamento)
    {
        $serviceFinanceiroTitulo        = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceFinanceiroMeioPagamento = new \Financeiro\Service\FinanceiroMeioPagamento($this->getEm());

        $objMeioPagamento = $serviceFinanceiroMeioPagamento->retornaMeioDePagamento($meioPagamento);

        if (!$objMeioPagamento) {
            $this->setLastError($serviceFinanceiroMeioPagamento->getLastError());

            return false;
        }

        /** @var $objFinanceiroTitulo \Financeiro\Entity\FinanceiroTitulo */
        $objFinanceiroTitulo = $serviceFinanceiroTitulo->getRepository()->find($titulo);

        if (!$objFinanceiroTitulo) {
            $this->setLastError('Registro de título não existe!');

            return false;
        }

        $arrDados = array(
            'titulo'                  => $objFinanceiroTitulo,
            'meioPagamento'           => $objMeioPagamento,
            'pagamentoUsuario'        => $objFinanceiroTitulo->getUsuarioAutor(),
            'pes'                     => $objFinanceiroTitulo->getPes(),
            'pagamentoValorBruto'     => $objFinanceiroTitulo->getTituloValor(),
            'pagamentoValorJuros'     => 0,
            'pagamentoValorMulta'     => 0,
            'pagamentoAcrescimos'     => 0,
            'pagamentoValorDescontos' => 0,
            'pagamentoValorFinal'     => $objFinanceiroTitulo->getTituloValor(),
            'pagamentoDescontoManual' => 0,
            'pagamentoDataBaixa'      => null,
            'pagamentoObservacoes'    => 0,
        );

        if (!$this->save($arrDados)) {
            return false;
        }

        if (!$this->gerarInformacoesComplementaresPagamento($arrDados)) {
            return false;
        }

        return true;
    }

    public function gravarPagamentosTitulo($titulo, $pagamentos)
    {
        $serviceFinanceiroTitulo        = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceFinanceiroMeioPagamento = new \Financeiro\Service\FinanceiroMeioPagamento($this->getEm());

        /** @var $objFinanceiroTitulo \Financeiro\Entity\FinanceiroTitulo */
        $objFinanceiroTitulo = $serviceFinanceiroTitulo->getRepository()->find($titulo);

        if (!$objFinanceiroTitulo) {
            $this->setLastError('Registro de título não existe!');

            return false;
        }

        if (!$this->removerPagamentosTitulos($titulo)) {
            return false;
        }

        $valorPagamento = 0;

        foreach ($pagamentos as $arrpag) {
            $valorPagamento = $arrpag['pagamentoValor'] + $valorPagamento;
        }

        foreach ($pagamentos as $arrPagto) {
            $meioPagamento    = $arrPagto['meioPagamento'] ? $arrPagto['meioPagamento'] : $arrPagto['meioPagamentoId'];
            $objMeioPagamento = $serviceFinanceiroMeioPagamento->retornaMeioDePagamento($meioPagamento);

            if (!$arrPagto['pagamentoValor']) {
                $this->setLastError(
                    'Valor inválido para o meio de pagamento ' . $objMeioPagamento->getMeioPagamentoDescricao() . "."
                );

                return false;
            }

            if (!$objMeioPagamento) {
                $this->setLastError($serviceFinanceiroMeioPagamento->getLastError());

                return false;
            }

            $pagamentoValor          = ($arrPagto['pagamentoValor'] ? $arrPagto['pagamentoValor'] : 0);
            $pagamentoValorBruto     = ($arrPagto['pagamentoValorBruto'] ? $arrPagto['pagamentoValorBruto'] : 0);
            $pagamentoValorJuros     = ($arrPagto['pagamentoValorJuros'] ? $arrPagto['pagamentoValorJuros'] : 0);
            $pagamentoValorMulta     = ($arrPagto['pagamentoValorMulta'] ? $arrPagto['pagamentoValorMulta'] : 0);
            $pagamentoAcrescimos     = ($arrPagto['pagamentoAcrescimos'] ? $arrPagto['pagamentoAcrescimos'] : 0);
            $pagamentoValorDescontos = ($arrPagto['pagamentoValorDescontos'] ? $arrPagto['pagamentoValorDescontos'] : 0);
            $pagamentoValorFinal     = (
            $arrPagto['pagamentoValorFinal'] ? $arrPagto['pagamentoValorFinal'] : $pagamentoValorBruto
            );
            $pagamentoDescontoManual = ($arrPagto['pagamentoDescontoManual'] ? $arrPagto['pagamentoDescontoManual'] : 0);
            $pagamentoObservacoes    = ($arrPagto['pagamentoObservacoes'] ? $arrPagto['pagamentoObservacoes'] : '');

            $pagamentoValorFinal = $pagamentoValorFinal ? $pagamentoValorFinal : $pagamentoValor;
            $pagamentoValorBruto = $pagamentoValorBruto ? $pagamentoValorBruto : $pagamentoValor;

            $arrDados = array(
                'titulo'                  => $objFinanceiroTitulo,
                'meioPagamento'           => $objMeioPagamento,
                'pagamentoUsuario'        => $objFinanceiroTitulo->getUsuarioBaixa(),
                'pes'                     => $objFinanceiroTitulo->getPes(),
                'pagamentoValorBruto'     => $pagamentoValorBruto,
                'pagamentoValorJuros'     => $pagamentoValorJuros,
                'pagamentoValorMulta'     => $pagamentoValorMulta,
                'pagamentoAcrescimos'     => $pagamentoAcrescimos,
                'pagamentoValorDescontos' => $pagamentoValorDescontos,
                'pagamentoValorFinal'     => $pagamentoValorFinal,
                'pagamentoDescontoManual' => $pagamentoDescontoManual,
                'pagamentoDataBaixa'      => $objFinanceiroTitulo->getTituloDataBaixa(),
                'pagamentoObservacoes'    => $pagamentoObservacoes,
            );

            if (!$this->save($arrDados)) {
                return false;
            }

            $arrPagto = array_merge($arrPagto, $arrDados);

            if (!$this->gerarInformacoesComplementaresPagamento($arrPagto)) {
                return false;
            }
        }

        return true;
    }

    private function gerarInformacoesComplementaresPagamento($arrDados = array())
    {
        /** @var $objPagamento \Financeiro\Entity\FinanceiroPagamento */
        $objPagamento = $this->getRepository()->find($arrDados['pagamentoId']);

        if (!$objPagamento) {
            $this->setLastError('Registro de pagamento não existe!');

            return false;
        }

        $meiodePagamentoCheque = $objPagamento->getMeioPagamento()->getMeioPagamentoPossuiCamposCheque();

        if ($meiodePagamentoCheque) {
            $serviceCheque = new \Financeiro\Service\FinanceiroCheque($this->getEm());

            if (!$serviceCheque->registrarChequeParaPagamento($objPagamento, $arrDados)) {
                $this->setLastError($serviceCheque->getLastError());

                return false;
            }
        }

        return true;
    }

    /**
     * @param int $tituloId
     * @return array
     */
    public function getArrayPagamentosTitulo($tituloId)
    {
        $query = "
        SELECT p.*, mp.meio_pagamento_descricao FROM financeiro__pagamento p
        INNER JOIN financeiro__meio_pagamento mp USING (meio_pagamento_id)
        WHERE titulo_id=:titulo";

        $arrBoletos = $this->executeQueryWithParam($query, ['titulo' => $tituloId])->fetchAll();

        return $arrBoletos;
    }

    public function pagamentoRecorrenciaCartao($arrParam)
    {
        $serviceSisFilaMaxiPago  = new \Sistema\Service\SisFilaMaxipago($this->getEm());
        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceAlunoCurso       = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
        $serviceCartao           = new \Financeiro\Service\FinanceiroCartao($this->getEm());

        if (empty($arrParam)) {
            $this->setLastError("Nenhum dado informado!");

            return false;
        }

        $alunoCurso = $serviceSisFilaMaxiPago->desencriptografar($arrParam['token']);

        /** @var $objAlunoCurso \Matricula\Entity\AcadgeralAlunoCurso */
        if (!$objAlunoCurso = $serviceAlunoCurso->getRepository()->findOneBy(['alunocursoId' => $alunoCurso])) {
            $this->setLastError("Aluno não localizado!");

            return false;
        }

        $this->begin();

        $arrParam['pes'] = $objAlunoCurso->getAluno()->getPes();
        $objCartao       = $serviceCartao->save($arrParam, true);

        if (!$objCartao) {
            $this->setLastError("Cartão inválido");

            return false;
        }

        $arrObjTitulos = $serviceFinanceiroTitulo->getRepository()->findBy(
            [
                'tituloId'     => $arrParam['titulos'],
                'alunocurso'   => $alunoCurso,
                'tituloEstado' => $serviceFinanceiroTitulo::TITULO_ESTADO_ABERTO
            ]
        );

        if (empty($arrObjTitulos)) {
            $this->setLastError("Nenhum título localizado, favor entrar em contato com a instituição!");

            return false;
        }

        $dataAtual               = new \DateTime();
        $arrDadosSisFilaMaxiPago = array();

        /** @var $objTitulo  \Financeiro\Entity\FinanceiroTitulo */
        foreach ($arrObjTitulos as $index => $objTitulo) {
            $arrTitulo = array();

            $arrTitulo['referenceNum']         = $objAlunoCurso->getAlunocursoId();
            $arrTitulo['chargeTotal']          = $objTitulo->getTituloValor();
            $arrTitulo['numberOfInstallments'] = 1;

            $arrPagamento = $serviceCartao->tratarDadosPagamentoIndividual(array_merge($arrParam, $arrTitulo));

            if (!$arrPagamento) {
                return false;
            }

            if ($objTitulo->getTituloDataVencimento() <= $dataAtual) {
                $arrPagamento['cartaoId'] = $objCartao;

                if (!$serviceCartao->realizaPagamento($arrPagamento)) {
                    $this->setLastError($serviceCartao->getLastError());

                    return false;
                }

                unset($arrObjTitulos[$index]);
                continue;
            }

            $arrDadosSisFilaMaxiPago[] = [
                'filaConteudo' => json_encode($arrPagamento),
                'titulo'       => $objTitulo,
                'filaSituacao' => $serviceSisFilaMaxiPago::FILA_SITUACAO_PENDENTE,
                'cartaoId'     => $objCartao,
                'cartao'       => $objCartao
            ];
        }

        foreach ($arrDadosSisFilaMaxiPago as $value) {
            $value['cartao'] = $objCartao;

            if (!$serviceSisFilaMaxiPago->save($value)) {
                $this->setLastError($serviceSisFilaMaxiPago->getLastError());

                return false;
            }
        }

        if (!$serviceSisFilaMaxiPago->cancelaRecorrencia($arrParam['titulosParaRecorrencia'])) {
            $this->setLastError($serviceSisFilaMaxiPago->getLastError());

            return false;
        }

        $this->commit();

        return true;
    }
}
?>