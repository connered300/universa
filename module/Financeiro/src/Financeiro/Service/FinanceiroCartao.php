<?php

namespace Financeiro\Service;

use VersaSpine\Service\AbstractService;

class FinanceiroCartao extends AbstractService
{
    const CARTAO_EXCLUIDO_SIM = 'Sim';
    const CARTAO_EXCLUIDO_NAO = 'Não';
    private $__lastError = null;

    /** @var \Sistema\Service\SisConfig */
    private $config = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2CartaoExcluido($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getCartaoExcluido());
    }

    public static function getCartaoExcluido()
    {
        return array(self::CARTAO_EXCLUIDO_SIM, self::CARTAO_EXCLUIDO_NAO);
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, $arrConfig = array())
    {
        parent::__construct($em, 'Financeiro\Entity\FinanceiroCartao');
        $this->setConfig(new \Sistema\Service\SisConfig($em, $arrConfig));
    }

    /**
     * @return \Sistema\Service\SisConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param \Sistema\Service\SisConfig $config
     * @return FinanceiroCartao
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    private static function tratarNumero($numero)
    {
        return preg_replace("/[^0-9]/", "", $numero);
    }

    public function pesquisaForJson($params)
    {
        $sql = '
        SELECT *
        FROM financeiro__cartao
        WHERE
            cartao_titular LIKE :cartao_titular';

        $cartaoTitular = false;
        $cartaoId      = false;

        if ($params['q']) {
            $cartaoTitular = $params['q'];
        } elseif ($params['query']) {
            $cartaoTitular = $params['query'];
        }

        if ($params['cartaoId']) {
            $cartaoId = $params['cartaoId'];
        }

        $parameters = array('cartao_titular' => "{$cartaoTitular}%");

        if ($cartaoId) {
            $parameters['cartao_id'] = $cartaoId;
            $sql .= ' AND cartao_id <> :cartao_id';
        }

        $sql .= " ORDER BY cartao_titular";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$arrDados['cartaoExcluido']) {
            $arrDados['cartaoExcluido'] = self::CARTAO_EXCLUIDO_NAO;
        }

        $cartaoTitular = '';

        if ($arrDados['cartaoTitular'] && !$cartaoTitular) {
            $cartaoTitular = $arrDados['cartaoTitular'];
        }

        if ($arrDados['cartaoNomeTitular'] && !$cartaoTitular) {
            $cartaoTitular = $arrDados['cartaoNomeTitular'];
        }

        $arrDados['cartaoTitular'] = $cartaoTitular;

        if (!$this->valida($arrDados)) {
            return false;
        }

        $servicePessoa = new \Pessoa\Service\Pessoa($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['cartaoId']) {
                /** @var $objFinanceiroCartao \Financeiro\Entity\FinanceiroCartao */
                $objFinanceiroCartao = $this->getRepository()->find($arrDados['cartaoId']);

                if (!$objFinanceiroCartao) {
                    $this->setLastError('Registro de cartão não existe!');

                    return false;
                }
            } else {
                $objFinanceiroCartao = new \Financeiro\Entity\FinanceiroCartao();
            }

            /** @var $objPessoa \Pessoa\Entity\Pessoa */
            $objPessoa = $servicePessoa->getRepository()->findOneBy(['pesId' => $arrDados['pes']]);

            if (!$objPessoa) {
                $this->setLastError('Registro de pessoa não existe!');

                return false;
            }

            $objFinanceiroCartao->setCartaoTitular($arrDados['cartaoTitular']);
            $objFinanceiroCartao->setCartaoNumero($arrDados['cartaoNumero']);
            $objFinanceiroCartao->setCartaoVencimento($arrDados['cartaoVencimento']);
            $objFinanceiroCartao->setCartaoCvv($arrDados['cartaoCvv']);
            $objFinanceiroCartao->setCartaoExcluido($arrDados['cartaoExcluido']);
            $objFinanceiroCartao->setPes($objPessoa);

            $this->getEm()->persist($objFinanceiroCartao);
            $this->getEm()->flush($objFinanceiroCartao);

            $this->getEm()->commit();

            return $objFinanceiroCartao;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de cartão!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        $validaCartao = $this->validaCartao($arrParam['cartaoNumero'], $arrParam['cartaoCvv']);

        if ($validaCartao['erro']) {
            $errors[] = $validaCartao['erro'];
        }

        if (!$arrParam['cartaoTitular']) {
            $errors[] = 'Por favor preencha o campo "titular"!';
        }

        if (!$arrParam['cartaoNumero']) {
            $errors[] = 'Por favor preencha o campo "número"!';
        }

        if (!$arrParam['cartaoVencimento']) {
            $errors[] = 'Por favor preencha o campo "vencimento"!';
        } else {
            $date = new \DateTime();

            $arrData = explode('/', $arrParam['cartaoVencimento']);

            $date2 = new \DateTime($arrData[1] . '-' . $arrData[0] . '-' . $date->format('d'));

            if (($date->getTimestamp()) > ($date2->getTimestamp())) {
                $errors[] = 'Cartão vencido!';
            }
        }

        if (!$arrParam['cartaoCvv']) {
            $errors[] = 'Por favor preencha o campo "cvv"!';
        }

        if (!in_array($arrParam['cartaoExcluido'], self::getCartaoExcluido())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "excluido"!';
        }

        if (!$arrParam['pes']) {
            $errors[] = 'Por favor preencha o campo "código pessoa"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM financeiro__cartao";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($cartaoId)
    {
        $arrDados = array();

        if (!$cartaoId) {
            $this->setLastError('Cartão inválido!');

            return array();
        }

        /** @var $objFinanceiroCartao \Financeiro\Entity\FinanceiroCartao */
        $objFinanceiroCartao = $this->getRepository()->find($cartaoId);

        if (!$objFinanceiroCartao) {
            $this->setLastError('Cartão não existe!');

            return array();
        }

        $servicePessoa = new \Pessoa\Service\Pessoa($this->getEm());

        try {
            $arrDados = $objFinanceiroCartao->toArray();

            if ($arrDados['pes']) {
                $arrPessoa       = $servicePessoa->getArrSelect2(['id' => $arrDados['pes']]);
                $arrDados['pes'] = $arrPessoa ? $arrPessoa[0] : null;
            }
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $servicePessoa = new \Pessoa\Service\Pessoa($this->getEm());

        if (is_array($arrDados['pes']) && !$arrDados['pes']['text']) {
            $arrDados['pes'] = $arrDados['pes']['pesId'];
        }

        if ($arrDados['pes'] && is_string($arrDados['pes'])) {
            $arrDados['pes'] = $servicePessoa->getArrSelect2(array('id' => $arrDados['pes']));
            $arrDados['pes'] = $arrDados['pes'] ? $arrDados['pes'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['cartaoId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['cartaoTitular' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Financeiro\Entity\FinanceiroCartao */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getCartaoId();
            $arrEntity[$params['value']] = $objEntity->getCartaoTitular();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($cartaoId)
    {
        if (!$cartaoId) {
            $this->setLastError('Para remover um registro de cartão é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objFinanceiroCartao \Financeiro\Entity\FinanceiroCartao */
            $objFinanceiroCartao = $this->getRepository()->find($cartaoId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objFinanceiroCartao);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de cartão.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $servicePessoa = new \Pessoa\Service\Pessoa($this->getEm());

        $servicePessoa->setarDependenciasView($view);

        $view->setVariable("arrCartaoExcluido", $this->getArrSelect2CartaoExcluido());
    }

    public function realizaPagamentoDeferimento($arrParam)
    {
        $serviceMaxiPago         = new \Versatecnologia\Maxipago\MaxiPago();
        $servicePagamento        = new \Financeiro\Service\FinanceiroPagamento($this->getEm());
        $serviceMeioPagamento    = new \Financeiro\Service\FinanceiroMeioPagamento($this->getEm());
        $serviceFilaMaxipago     = new \Sistema\Service\SisFilaMaxipago($this->getEm());
        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo($this->getEm());

        /** @var $objMeioPagameno \Financeiro\Entity\FinanceiroMeioPagamento */
        $objMeioPagameno = $serviceMeioPagamento->criaSeNecessarioERetornaMeioPagamento('Cartão');

        $arrBackUp     = $arrParam;
        $recorrencia   = false;
        $numeroParcela = 0;
        $arrParam      = $this->tratarDadosDeferimento($arrParam);

        $merchantId     = $this->getConfig()->localizarChave('FINANCEIRO_CARTAO_IDENTIFICACAO_ESTABELECIMENTO');
        $merchantKey    = $this->getConfig()->localizarChave('FINANCEIRO_CARTAO_CHAVE_ESTABELECIMENTO');
        $processorID    = $this->getConfig()->localizarChave('FINANCEIRO_CARTAO_CODIGO_ADQUIRENTE');
        $ambienteCartao = $this->getConfig()->localizarChave('FINANCEIRO_CARTAO_AMBIENTE', "TEST");

        $arrParam['merchantId']  = $merchantId;
        $arrParam['merchantKey'] = $merchantKey;
        $arrParam['processorID'] = $processorID;

        $serviceMaxiPago->setEnvironment($ambienteCartao);

        if (!$this->validaPagamento($arrParam)) {
            return false;
        }

        $serviceMaxiPago->creditCardSale($arrParam);
        $arrResposta = $serviceMaxiPago->getResult();

        /*pagamento recorrencia*/
        if ($arrResposta['responseCode'] != 0) {
            $recorrencia                      = true;
            $arrParam['numberOfInstallments'] = 1;
            $serviceMaxiPago->creditCardSale($arrParam);
            $arrResposta = $serviceMaxiPago->getResult();

            /*Caso não for possivel realizar o pagamento usando  cartão no mesmo com pagamento recorrencia*/
            if ($arrResposta['responseCode'] != 0) {
                return [
                    'erro' => true,
                    /*Esta retornando array, para se no futuro for realizar novo pagamento via cartao na mesma interface identificar o retorno para erro*/
                    'msg'  => $this->retornaRespostasTransacaoMaxiPago()[$arrResposta['responseCode']]
                ];
            }
        }

        $retornoApi = json_encode($arrResposta);
        $dataAtual  = new \DateTime();

        $this->begin();

        /** @var $objTitulo \Financeiro\Entity\FinanceiroTitulo */
        foreach ($arrBackUp as $objTitulo) {
            $numeroParcela++;
            $arrDadosGravaPagamento = array();

            if (!is_a($objTitulo, '\Financeiro\Entity\FinanceiroTitulo')) {
                continue;
            }

            $arrDadosGravaPagamento['filaRetorno']           = null;
            $arrDadosGravaPagamento['filaDataCadastro']      = $dataAtual;
            $arrDadosGravaPagamento['filaDataProcessamento'] = null;
            $arrDadosGravaPagamento['filaDataFinalizacao']   = null;
            $arrDadosGravaPagamento['filaSituacao']          = $serviceFilaMaxipago::FILA_SITUACAO_PENDENTE;
            $arrDadosGravaPagamento['filaConteudo']          = json_encode($arrParam);
            $arrDadosGravaPagamento['titulo']                = $objTitulo->getTituloId();
            $arrDadosGravaPagamento['retornoApi']            = $retornoApi;
            $arrDadosGravaPagamento['cartao']                = $arrParam['objcartao'];

            if ($recorrencia && $numeroParcela == 1 || !$recorrencia) {
                $arrDadosGravaPagamento['meioPagamento']       = $objMeioPagameno->getMeioPagamentoId();
                $arrDadosGravaPagamento['pes']                 = $objTitulo->getPes();
                $arrDadosGravaPagamento['pagamentoValorFinal'] = $objTitulo->getTituloValor();
                $arrDadosGravaPagamento['pagamentoValorBruto'] = $arrDadosGravaPagamento['pagamentoValorFinal'];
                $arrDadosGravaPagamento['pagamentoDataBaixa']  = $dataAtual;

                $servicePagamento->save($arrDadosGravaPagamento);

                $arrDadosGravaPagamento['filaRetorno']      = $arrDadosGravaPagamento['retornoApi'];
                $arrDadosGravaPagamento['filaDataCadastro'] = $dataAtual;
                $arrDadosGravaPagamento['filaSituacao']     = $serviceFilaMaxipago::FILA_SITUACAO_ENVIADO;

                $objTitulo->setTituloEstado($serviceFinanceiroTitulo::TITULO_ESTADO_PAGO);
                $objTitulo->setTituloDataBaixa($dataAtual);
                $objTitulo->setTituloDataPagamento($dataAtual);
                $objTitulo->setTituloIncluirRemessa($serviceFinanceiroTitulo::TITULO_INCLUIR_REMESSA_NAO);
                $objTitulo->setTituloValorPago($objTitulo->getTituloValor());

                $this->getEm()->persist($objTitulo);
                $this->getEm()->flush($objTitulo);
            }

            if (!$serviceFilaMaxipago->save($arrDadosGravaPagamento)) {
                $this->setLastError($serviceFilaMaxipago->getLastError());

                return false;
            }
        }

        $this->commit();

        return true;
    }

    private function tratarDadosDeferimento($arrParam)
    {
        $arrRetorno = array();
        $parcelas   = 0;
        $valorTotal = 0;

        foreach ($arrParam as $value) {
            /** @var $value \Financeiro\Entity\FinanceiroTitulo */
            if (is_a($value, '\Financeiro\Entity\FinanceiroTitulo')) {
                /** @var $value \Financeiro\Entity\FinanceiroTitulo */
                $valorTotal += $value->getTituloValor();
                $parcelas++;
                /*Dados para serem tratados no recorrencia*/
                $tipo                                      = $value->getTipotitulo()->getTipotituloId();
                $arrRetorno[$tipo]['chargeTotal']          = $valorTotal;
                $arrRetorno[$tipo]['numberOfInstallments'] = 1;
                $arrRetorno[$tipo]['pedido'][]             = $value->getTituloId();
            } elseif (is_a($value, '\Financeiro\Entity\FinanceiroAlunoConfigPgtoCurso')) {
                /** @var $value \Financeiro\Entity\FinanceiroAlunoConfigPgtoCurso */
                /** @var $objCartao \Financeiro\Entity\FinanceiroCartao */
                $objCartao = $value->getCartao();

                if (!$objCartao) {
                    $this->setLastError("Este plano de pagamento não oferece pagamento por cartão!");

                    return false;
                }

                $vencimento = explode('/', $objCartao->getCartaoVencimento());

                $arrRetorno['referenceNum']         = $value->getConfigPgtoCursoId();
                $arrRetorno['number']               = ((integer)str_replace(' ', '', $objCartao->getCartaoNumero()));
                $arrRetorno['cvvNumber']            = $objCartao->getCartaoCvv();
                $arrRetorno['expMonth']             = $vencimento[0];
                $arrRetorno['expYear']              = (integer)$vencimento[1];
                $arrRetorno['numberOfInstallments'] = $parcelas < 1 ? null : $parcelas;
                $arrRetorno['chargeTotal']          = $valorTotal;
                $arrRetorno['cartao']               = $objCartao;
            }
        }

        return $arrRetorno;
    }

    private function validaPagamento($arrParam)
    {
        $msgErro = array();
        $retorno = true;

        if (!$arrParam['merchantId']) {
            $msgErro[] = 'Identificação da loja!';
        }
        if (!$arrParam['merchantKey']) {
            $msgErro[] = 'Autenticação da loja';
        }
        if (!$arrParam['referenceNum']) {
            $msgErro[] = 'Pedido';
        }
        if (!$arrParam['processorID']) {
            $msgErro[] = 'Operadora de cartão';
        }
        if (!$arrParam['chargeTotal']) {
            $msgErro[] = 'Valor ';
        }
        if (!$arrParam['number']) {
            $msgErro[] = 'Número do Cartão';
        }
        if (!$arrParam['expMonth']) {
            $msgErro[] = 'Mês de Vencimento';
        }
        if (!$arrParam['expYear']) {
            $msgErro[] = 'Ano de Vencimento';
        }
        if (!$arrParam['cvvNumber']) {
            $msgErro[] = 'Código Verificador';
        }

        $validaCartao = $this->validaCartao($arrParam['number'], $arrParam['cvvNumber']);

        if ($validaCartao['erro']) {
            $msgErro[] = $validaCartao['erro'];
        }

        if (!empty($msgErro)) {
            $this->setLastError("Informações inválidas: " . implode(", ", $msgErro));
            $retorno = false;
        }

        return $retorno;
    }

    public function retornaRespostasTransacaoMaxiPago($index)
    {
        $arrRespostas[0]    = 'Aprovada';
        $arrRespostas[1]    = 'Negada';
        $arrRespostas[2]    = 'Negada por Duplicidade ou Fraude';
        $arrRespostas[5]    = 'Em Revisão (Análise Manual de Fraude)';
        $arrRespostas[1022] = 'Erro na operadora de cartão';
        $arrRespostas[1024] = 'Erro nos parâmetros enviados';
        $arrRespostas[1025] = 'Erro nas credenciais';
        $arrRespostas[2048] = 'Erro interno no Sistema de Pagamento (Contante o suporte do sistema)!';
        $arrRespostas[4097] = 'Timeout com a adquirente';

        return $index ? $arrRespostas[$index] : $arrRespostas;
    }

    public function validaCartao($cartao, $cvc = false)
    {
        $serviceSisConfig = new \Sistema\Service\SisConfig($this->getEm());

        $arrBandeirasPermitidas = $serviceSisConfig->localizarChave('FINANCEIRO_CARTAO_BANDEIRAS_PERMITIDAS', '');

        $arrBandeirasPermitidas = explode(",", $arrBandeirasPermitidas);

        $cartao = self::tratarNumero($cartao);

        if ($cvc) {
            $cvc = self::tratarNumero($cvc);
        }

        if (!$cartao) {
            return false;
        }

        $cartoes = array(
            'visa'       => array('len' => array(13, 16), 'cvc' => 3),
            'mastercard' => array('len' => array(16), 'cvc' => 3),
            'diners'     => array('len' => array(14, 16), 'cvc' => 3),
            'amex'       => array('len' => array(15), 'cvc' => 4),
            'jcb'        => array('len' => array(16), 'cvc' => 3),
            'discover'   => array('len' => array(16), 'cvc' => 4),
            'aura'       => array('len' => array(16), 'cvc' => 3),
            'elo'        => array('len' => array(16), 'cvc' => 3),
            'hipercard'  => array('len' => array(13, 16, 19), 'cvc' => 3),
        );

        switch ($cartao) {
            case (bool)preg_match('/^(301|305)/', $cartao) :
                $bandeira = 'diners';
                break;
            case (bool)preg_match('/^(34|37)/', $cartao) :
                $bandeira = 'amex';
                break;
            case (bool)preg_match('/^(36,38)/', $cartao) :
                $bandeira = 'diners';
                break;
            case (bool)preg_match('/^(35)/', $cartao) :
                $bandeira = 'jcb';
                break;
            case (bool)preg_match('/^(4)/', $cartao) :
                $bandeira = 'visa';
                break;
            case (bool)preg_match('/^(5)/', $cartao) :
                $bandeira = 'mastercard';
                break;
            case (bool)preg_match('/^(64,65)/', $cartao) :
                $bandeira = 'discover';
                break;
            case (bool)preg_match('/^(636368|438935|504175|451416|636297)/', $cartao) :
                $bandeira = 'elo';
                break;
            case (bool)preg_match('/^(606282)/', $cartao) :
                $bandeira = 'hipercard';
                break;
            case (bool)preg_match('/^(5067|4576|4011)/', $cartao) :
                $bandeira = 'elo';
                break;
            case (bool)preg_match('/^(3841)/', $cartao) :
                $bandeira = 'hipercard';
                break;
            case (bool)preg_match('/^(6011)/', $cartao) :
                $bandeira = 'discover';
                break;
            case (bool)preg_match('/^(622)/', $cartao) :
                $bandeira = 'discover';
                break;
            case (bool)preg_match('/^(50)/', $cartao) :
                $bandeira = 'aura';
                break;
            case (bool)preg_match('/^(60)/', $cartao) :
                $bandeira = 'hipercard';
                break;
        }

        if (!$bandeira) {
            $dados_cartao['erro'] = "Numero de cartão inválido";

            return $dados_cartao;
        }

        if (!in_array($bandeira, $arrBandeirasPermitidas)) {
            $dados_cartao['erro'] = "Bandeira (operadora de cartão ) não permitida!";

            return $dados_cartao;
        }

        $dados_cartao = $cartoes[$bandeira];

        if (is_array($dados_cartao)) {
            $dados_cartao['bandeira'] = 'Bandeira do Cartão: ' . strtoupper($bandeira);
        } else {
            $dados_cartao['erro'] = 'Número inválido!';
        }

        if ($cvc AND strlen($cvc) != $dados_cartao['cvc']) {
            $msgErroCvc           = '</br> Código Verificador inválido!</br>Para este cartão ele deve ter no máximo ' . $dados_cartao['cvc'] . ' dígitos!';
            $dados_cartao['erro'] = $dados_cartao['erro'] ? $dados_cartao['erro'] . $msgErroCvc : $msgErroCvc;
        }

        return $dados_cartao;
    }

    public function retornaBandeirasPossiveis()
    {
        return array(
            'diners'     => 'diners',
            'amex'       => 'amex',
            'jcb'        => 'jcb',
            'visa'       => 'visa',
            'mastercard' => 'mastercard',
            'discover'   => 'discover',
            'elo'        => 'elo',
            'hipercard'  => 'hipercard',
            'aura'       => 'aura'
        );
    }

    public function retornaAdquirentesMaxiPago()
    {
        return array(
            1 => 'SIMULADOR DE TESTES',
            2 => 'Rede',
            3 => 'GetNet',
            4 => 'Cielo',
            5 => 'TEF',
            6 => 'Elavon',
            8 => 'ChasePaymentech',
        );
    }

    public function realizaPagamento($arrParam)
    {
        $serviceMaxiPago         = new \Versatecnologia\Maxipago\MaxiPago();
        $serviceSisConfig        = new \Sistema\Service\SisConfig($this->getEm());
        $servicePagamento        = new \Financeiro\Service\FinanceiroPagamento($this->getEm());
        $serviceMeioPagamento    = new \Financeiro\Service\FinanceiroMeioPagamento($this->getEm());
        $serviceFilaSisCartao    = new \Sistema\Service\SisFilaMaxipago($this->getEm());
        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceCartao           = new \Financeiro\Service\FinanceiroCartao($this->getEm());
        $serviceAcessoPessoas    = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceSisFila          = new \Sistema\Service\SisFilaMaxipago($this->getEm());

        /*TODO : Para pagamentos recorrencias  eu ja terei esses valores, porém isso ira sobrescver, a primeiro momento pensei como já sendo uma possivel solução para falha no pagamento*/

        $arrParam['merchantId']  = $serviceSisConfig->localizarChave('FINANCEIRO_CARTAO_IDENTIFICACAO_ESTABELECIMENTO');
        $arrParam['merchantKey'] = $serviceSisConfig->localizarChave('FINANCEIRO_CARTAO_CHAVE_ESTABELECIMENTO');
        $arrParam['processorID'] = $serviceSisConfig->localizarChave('FINANCEIRO_CARTAO_CODIGO_ADQUIRENTE');
        $ambienteCartao          = $this->getConfig()->localizarChave('FINANCEIRO_CARTAO_AMBIENTE', "TEST");
        $arrDados                = array();

        /** @var $objFila \Sistema\Entity\SisFilaMaxipago */
        $objFila = $serviceSisFila->getRepository()->findOneBy(['filaId' => $arrParam['filaId']]);

        $objCartao = $objFila->getCartao();

        if (!$objCartao) {
            $arrCartao = [
                'cartaoNumero'     => $arrParam['number'],
                'cartaoCvv'        => $arrParam['cvvNumber'],
                'cartaoVencimento' => ($arrParam['expMonth'] . '/' . $arrParam['expYear'])
            ];

            /** @var $objCartao \Financeiro\Entity\FinanceiroCartao */
            $objCartao = $serviceCartao->getRepository()->findOneBy($arrCartao);
        }

        if (!$objCartao) {
            $this->setLastError('Cartão não localizado!');

            return false;
        }

        if ($objCartao->getCartaoExcluido() == $serviceCartao::CARTAO_EXCLUIDO_SIM) {
            $this->setLastError("Este cartão está bloqueado!");

            return false;
        }

        if (!$this->validaPagamento($arrParam)) {
            $this->setLastError("Não possivel realizar pagamento com esse cartão!" . $this->getLastError());

            return false;
        }

        $serviceMaxiPago->setEnvironment($ambienteCartao);
        $serviceMaxiPago->creditCardSale($arrParam);

        if ($serviceMaxiPago->getResponseCode() == 0) {
            $retornoApi = json_encode($serviceMaxiPago->getResult());
            $dataAtual  = new \DateTime();

            /** @var $objMeioPagameno \Financeiro\Entity\FinanceiroMeioPagamento */
            $objMeioPagameno = $serviceMeioPagamento->criaSeNecessarioERetornaMeioPagamento('Cartão');

            try {
                $this->begin();

                /** @var $usuarioLogado \Acesso\Entity\AcessoPessoas */
                $usuarioLogado = $serviceAcessoPessoas->retornaUsuarioLogado(true);

                /** @var $objTitulo \Financeiro\Entity\FinanceiroTitulo */
                $objTitulo = $serviceFinanceiroTitulo->getRepository()->findOneBy(['tituloId' => $arrParam['pedido']]);

                /*Monta array Pagamento*/
                $arrDados['titulo']        = $objTitulo;
                $arrDados['meioPagamento'] = $objMeioPagameno;
                $arrDados['cartao']        = $objCartao;

                $arrDados['pagamentoValorBruto'] = $objTitulo->getTituloValor();
                $arrDados['pagamentoValorFinal'] = $arrDados['pagamentoValorBruto'];
                $arrDados['pagamentoDataBaixa']  = $dataAtual;
                $arrDados['retornoApi']          = $retornoApi;
                $arrDados['pes']                 = $usuarioLogado->getPes();
                $arrDados['pagamentoUsuario']    = $usuarioLogado->getUsuario();

                /*Complementa array para Fila de Pagamento por Cartão*/
                $arrDados['filaId'] = $arrParam['filaId'] ? $arrParam['filaId'] : null;

                $arrDados['filaRetorno']      = $arrDados['retornoApi'];
                $arrDados['filaConteudo']     = json_encode($arrParam);
                $arrDados['filaDataCadastro'] = !$arrDados['filaId'] ? $dataAtual : null;

                $objTitulo
                    ->setTituloEstado($serviceFinanceiroTitulo::TITULO_ESTADO_PAGO)
                    ->setTituloDataPagamento($dataAtual)
                    ->setTituloDataProcessamento($dataAtual)
                    ->setTituloIncluirRemessa($serviceFinanceiroTitulo::TITULO_INCLUIR_REMESSA_NAO)
                    ->setTituloValorPago($objTitulo->getTituloValor());

                $this->getEm()->persist($objTitulo);
                $this->getEm()->flush($objTitulo);

                if (!($servicePagamento->save($arrDados) && $serviceFilaSisCartao->save($arrDados))) {
                    $this->setLastError(
                        "Título pago mas algo deu errado: " .
                        $serviceMeioPagamento->getLastError() . '-' . $serviceFilaSisCartao->getLastError()
                    );

                    return false;
                }
            } catch (\Exception $exception) {
                $this->setLastError(
                    $this->getLastError() ? $this->getLastError() : $exception
                );

                return false;
            }

            $this->commit();

            return $retornoApi;
        }

        $this->setLastError($this->retornaRespostasTransacaoMaxiPago($serviceMaxiPago->getResponseCode()));

        return false;
    }

    public function retornaAAmbientesMaxiPago()
    {
        return array(
            'TEST' => 'Testes',
            'LIVE' => 'Produção',
        );
    }

    public function tratarDadosPagamentoIndividual($arrParam)
    {
        $arrDados = array();
        $arrError = array();

        if (empty($arrParam)) {
            $this->setLastError("Nenhum dado informado!");

            return false;
        }

        if ($arrParam['cartaoVencimento']) {
            $arrParam['vencAux']  = explode('/', (str_replace(" ", "", $arrParam['cartaoVencimento'])));
            $arrParam['expMonth'] = (integer)$arrParam['vencAux'][0];
            $arrParam['expYear']  = (integer)$arrParam['vencAux'][1];
        }

        $arrDados['number']               = self::tratarNumero($arrParam['cartaoNumero']);
        $arrDados['cvvNumber']            = self::tratarNumero($arrParam['cartaoCvv']);
        $arrDados['expMonth']             = str_pad((self::tratarNumero($arrParam['expMonth'])), 2, 0, STR_PAD_LEFT);
        $arrDados['expYear']              = self::tratarNumero($arrParam['expYear']);
        $arrDados['referenceNum']         = self::tratarNumero($arrParam['referenceNum']);
        $arrDados['chargeTotal']          = self::tratarNumero($arrParam['chargeTotal']);
        $arrDados['numberOfInstallments'] = self::tratarNumero($arrParam['numberOfInstallments']);

        foreach ($arrDados as $index => $value) {
            if (!$value) {
                $arrError[] = $index;
            }
        }

        if ($arrError) {
            $this->setLastError('Campos vazios' . explode(',', $arrError));

            return false;
        }

        return $arrDados;
    }

    public function estornaPagamentoCartao($titulo, $valorTotal = true)
    {
        $serviceMaxiPago         = new \Versatecnologia\Maxipago\MaxiPago();
        $ambienteCartao          = $this->getConfig()->localizarChave('FINANCEIRO_CARTAO_AMBIENTE', "TEST");
        $arrParam['merchantId']  =
            $this->getConfig()->localizarChave('FINANCEIRO_CARTAO_IDENTIFICACAO_ESTABELECIMENTO', null);
        $arrParam['merchantKey'] =
            $this->getConfig()->localizarChave('FINANCEIRO_CARTAO_CHAVE_ESTABELECIMENTO', null);
        $serviceFilaMaxiPago     = new \Sistema\Service\SisFilaMaxipago($this->getEm());

        /** @var $objFilaMaxiPago \Sistema\Entity\SisFilaMaxipago */
        $objFilaMaxiPago =
            $serviceFilaMaxiPago->getRepository()->findOneBy(['titulo' => $titulo], ['filaId' => 'desc']);

        if (!$objFilaMaxiPago) {

            $this->setLastError("Título não localizado com meio de pagamento cartão!");

            return false;
        }

        $arrRetornoMaxiPago   = json_decode($objFilaMaxiPago->getFilaRetorno(), true);
        $arrConteudooMaxiPago = json_decode($objFilaMaxiPago->getFilaConteudo(), true);

        if (!$arrRetornoMaxiPago || !$arrConteudooMaxiPago) {
            $this->setLastError("Título ainda não foi processado!");

            return false;
        }

        $arrParam['orderID']      = $arrRetornoMaxiPago['orderID'];
        $arrParam['referenceNum'] = $arrConteudooMaxiPago['referenceNum'];
        $arrParam['chargeTotal']  = $objFilaMaxiPago->getTitulo()->getTituloValorPago();

        if ($valorTotal) {
            $arrParam['chargeTotal'] =
                $arrConteudooMaxiPago[$objFilaMaxiPago->getTitulo()->getTipotitulo()->getTipotituloId()]['chargeTotal'];
        }

        if (!$this->validaEstornoCartao($arrParam)) {
            return false;
        }

        $serviceMaxiPago->setEnvironment($ambienteCartao);
        /*validar , adicionar o conf na hora que instacia o servili getEm()->get('config')*/
        $serviceMaxiPago->creditCardRefund($arrParam);
        $response = (integer)$serviceMaxiPago->getResponseCode();

        if ($response != 0) {
            $msgErro = 'Erro ao processar estorno!';

            if ($response == 264) {
                $msgErro = 'Este pagamento já foi estornado!';
            }

            $this->setLastError($msgErro);

            return false;
        }

        return true;
    }

    public function validaEstornoCartao($arrParam)
    {
        $arrErro = array();

        if (!$arrParam['merchantId']) {
            $arrErro[] = 'código da loja';
        }
        if (!$arrParam['merchantKey']) {
            $arrErro[] = 'chave da loja';
        }
        if (!$arrParam['orderID']) {
            $arrErro[] = 'código maxipago';
        }
        if (!$arrParam['referenceNum']) {
            $arrErro[] = 'pedido';
        }
        if (!$arrParam['chargeTotal']) {
            $arrErro[] = 'valor';
        }

        if (!empty($arrErro)) {
            $this->setLastError("Informações ausentes para o estorno: " . implode(',', $arrErro) . " !");

            return false;
        }

        return true;
    }

    public function retornaTitulosVinculadosCartao($titulo)
    {
        $serviceFilaMaxiPago = new \Sistema\Service\SisFilaMaxipago($this->getEm());

        /** @var $objFilaMaxiPago \Sistema\Entity\SisFilaMaxipago */
        $objFilaMaxiPago =
            $serviceFilaMaxiPago->getRepository()->findOneBy(
                [
                    'titulo'       => $titulo,
                    'filaSituacao' => $serviceFilaMaxiPago::FILA_SITUACAO_ENVIADO
                ],
                ['filaId' => 'desc']
            );

        if (!$objFilaMaxiPago) {
            return [];
        }

        $tipoTitulo = $objFilaMaxiPago->getTitulo()->getTipotitulo()->getTipotituloId();

        $arrConteudo = json_decode($objFilaMaxiPago->getFilaConteudo(), true);
        $arrTitulos  = $arrConteudo[$tipoTitulo];

        if (empty($arrTitulos)) {
            return [];
        }

        return $arrTitulos['pedido'];
    }
}
?>