<?php

namespace Financeiro\Service;

use VersaSpine\Service\AbstractService;

class FinanceiroFiltroTipo extends AbstractService
{
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Financeiro\Entity\FinanceiroFiltroTipo');
    }

    public function pesquisaForJson($params)
    {
    }

    public function valida($arrParam)
    {
    }

    /**
     * @param $filtroId
     * @return array
     */
    public function retornaTipoTituloPeloFiltro($filtroId)
    {
        $arrFinanceiroTituloTipos   = array();
        $arrObjFinanceiroFiltroTipo = $this->pesquisaPeloFiltro($filtroId, false);

        /** @var $objFinanceiroFiltroTipo \Financeiro\Entity\FinanceiroFiltroTipo */
        foreach ($arrObjFinanceiroFiltroTipo as $objFinanceiroFiltroTipo) {
            $arrFinanceiroTituloTipos[] = $objFinanceiroFiltroTipo->getTipotitulo()->getTipotituloId();
        }

        return $arrFinanceiroTituloTipos;
    }

    /**
     * @param           $filtroId
     * @param bool|true $toArray
     * @return array
     */
    public function pesquisaPeloFiltro($filtroId, $toArray = true)
    {
        $arrResult = $this->getRepository()->findBy(array('filtro' => $filtroId));

        if ($toArray) {
            $arrRetorno = array();

            /** @var $objFinanceiroFiltroTipo \Financeiro\Entity\FinanceiroFiltroTipo */
            foreach ($arrResult as $objFinanceiroFiltroTipo) {
                $arrRetorno[] = $objFinanceiroFiltroTipo->toArray();
            }

            return $arrRetorno;
        }

        return $arrResult;
    }

    /**
     * @param                                           $arrTipoTitulo
     * @param \Financeiro\Entity\FinanceiroFiltro       $objFinanceiroFiltro
     * @return bool
     */
    public function salvarArray($arrTipoTitulo, \Financeiro\Entity\FinanceiroFiltro $objFinanceiroFiltro)
    {
        $objEntityManager = $this->getEm();
        $arrExcluir       = array();
        $arrEditar        = array();

        if (!is_array($arrTipoTitulo)) {
            $arrTipoTitulo = explode(',', $arrTipoTitulo);
        }

        $arrFinanceiroFiltroTiposDB = $this->pesquisaPeloFiltro($objFinanceiroFiltro->getFiltroId(), false);
        /** @var $objFinanceiroFiltroTipoDB \Financeiro\Entity\FinanceiroFiltroTipo */
        foreach ($arrFinanceiroFiltroTiposDB as $objFinanceiroFiltroTipoDB) {
            $encontrado   = false;
            $tipotituloId = $objFinanceiroFiltroTipoDB->getTipotitulo()->getTipotituloId();

            foreach ($arrTipoTitulo as $tipotitulo) {
                if ($tipotitulo == $tipotituloId) {
                    $encontrado               = true;
                    $arrEditar[$tipotituloId] = $objFinanceiroFiltroTipoDB;
                    break;
                }
            }

            if (!$encontrado) {
                $arrExcluir[$tipotituloId] = $objFinanceiroFiltroTipoDB;
            }
        }

        if ($arrExcluir) {
            foreach ($arrExcluir as $objReg) {
                $objEntityManager->remove($objReg);
                $objEntityManager->flush();
            }
        }

        foreach ($arrTipoTitulo as $tipotitulo) {
            $serviceFinanceiroTituloTipo = new \Financeiro\Service\FinanceiroTituloTipo($objEntityManager);
            /** @var \Financeiro\Entity\FinanceiroTituloTipo $objFinanceiroTituloTipo */
            $objFinanceiroTituloTipo = $serviceFinanceiroTituloTipo->getRepository()->find($tipotitulo);

            if (!isset($arrEditar[$tipotitulo])) {
                $objFinanceiroFiltroTipo = new \Financeiro\Entity\FinanceiroFiltroTipo();

                $objFinanceiroFiltroTipo->setFiltro($objFinanceiroFiltro);
                $objFinanceiroFiltroTipo->setTipotitulo($objFinanceiroTituloTipo);

                $objEntityManager->persist($objFinanceiroFiltroTipo);
                $objEntityManager->flush($objFinanceiroFiltroTipo);
            }
        }

        return true;
    }

    /**
     * @param $filtroId
     * @return array
     */
    public function retornaArrayPeloFiltro($filtroId)
    {
        $arrObjFinanceiroFiltroTipo = $this->pesquisaPeloFiltro($filtroId, false);
        $arrFinanceiroFiltroTipo    = array();

        /** @var $objFinanceiroFiltroTipo \Financeiro\Entity\FinanceiroFiltroTipo */
        foreach ($arrObjFinanceiroFiltroTipo as $objFinanceiroFiltroTipo) {
            $arrItem = array(
                'id'   => $objFinanceiroFiltroTipo->getTipotitulo()->getTipotituloId(),
                'text' => $objFinanceiroFiltroTipo->getTipotitulo()->getTipotituloNome()
            );

            $arrFinanceiroFiltroTipo[] = $arrItem;
        }

        return $arrFinanceiroFiltroTipo;
    }
}
?>