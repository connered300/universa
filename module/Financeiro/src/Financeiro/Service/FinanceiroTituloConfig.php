<?php

namespace Financeiro\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class FinanceiroTituloConfig
 * @package Financeiro\Service
 */
class FinanceiroTituloConfig extends AbstractService
{
    const TITULO_CONFIG_INSTRUCAO_AUTOMATICA_SIM        = 'Sim';
    const TITULO_CONFIG_INSTRUCAO_AUTOMATICA_NAO        = 'Não';
    const TITULOCONF_DESCONTO_INCENTIVO_ACUMULATIVO_SIM = 'Sim';
    const TITULOCONF_DESCONTO_INCENTIVO_ACUMULATIVO_NAO = 'Não';
    const TITULOCONF_VENCIMENTO_FIXO_SIM                = 'Sim';
    const TITULOCONF_VENCIMENTO_FIXO_NAO                = 'Não';
    const TITULOCONF_VENCIMENTO_ALTERAVEL_SIM           = 'Sim';
    const TITULOCONF_VENCIMENTO_ALTERAVEL_NAO           = 'Não';

    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Financeiro\Entity\FinanceiroTituloConfig');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @param $params
     * @return array
     */
    public function pesquisaForJson($params)
    {
        $parameters = array();

        $sql = '
        SELECT *
        FROM financeiro__titulo_config
        WHERE 1=1';

        $tituloconfPercentDesc = false;
        $tituloconfId          = false;

        if ($params['q']) {
            $tituloconfPercentDesc = $params['q'];
        } elseif ($params['query']) {
            $tituloconfPercentDesc = $params['query'];
        }

        if ($params['tituloconfId']) {
            $tituloconfId = $params['tituloconfId'];
        }

        if ($params['tipoTitulo']) {
            $parameters['tipoTitulo'] = $params['tipoTitulo'];

            $sql .= " AND (tipotitulo_id=:tipoTitulo OR tipotitulo_id IS NULL)";
        }

        if ($params['perId']) {
            $parameters['perId'] = $params['perId'];

            $sql .= " AND (per_id=:perId OR per_id IS NULL)";
        }

        if ($params['cursoCampus']) {
            if (is_array($params['cursoCampus'])) {
                $parameters['cursoCampusId'] =
                    $params['cursoCampus']['cursocampus_id'] ? $params['cursoCampus']['cursocampus_id']
                        : $params['cursoCampus']['cursocampusId'];

                $sql .= " AND (cursocampus_id=:cursoCampusId OR cursocampus_id IS NULL)";
            } else {
                $parameters['cursoCampusId'] = $params['cursoCampus'];

                $sql .= " AND (cursocampus_id =:cursoCampusId OR cursocampus_id IS NULL)";
            }
        }

        if ($tituloconfPercentDesc) {
            $parameters['tituloconfPercentDesc'] = "{$tituloconfPercentDesc}%";
            $sql .= ' AND tituloconf_percent_desc LIKE :tituloconfPercentDesc';
        }

        if ($tituloconfId) {
            $parameters['tituloconfId'] = $tituloconfId;
            $sql .= ' AND tituloconf_id NOT IN(:tituloconfId)';
        }

        $sql .= " ORDER BY tituloconf_percent_desc";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceCampusCurso          = new \Matricula\Service\CampusCurso($this->getEm());
        $serviceAcadperiodoLetivo    = new \Matricula\Service\AcadperiodoLetivo($this->getEm());
        $serviceFinanceiroTituloTipo = new \Financeiro\Service\FinanceiroTituloTipo($this->getEm());
        $serviceBoletoConfConta      = new \Financeiro\Service\BoletoConfConta($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['tituloconfId']) {
                /** @var $objFinanceiroTituloConfig \Financeiro\Entity\FinanceiroTituloConfig */
                $objFinanceiroTituloConfig = $this->getRepository()->find($arrDados['tituloconfId']);

                if (!$objFinanceiroTituloConfig) {
                    $this->setLastError('Registro de configuração de título não existe!');

                    return false;
                }
            } else {
                $objFinanceiroTituloConfig = new \Financeiro\Entity\FinanceiroTituloConfig();
            }

            if ($arrDados['cursocampus']) {
                /** @var $objCampusCurso \Matricula\Entity\CampusCurso */
                $objCampusCurso = $serviceCampusCurso->getRepository()->find($arrDados['cursocampus']);

                if (!$objCampusCurso) {
                    $this->setLastError('Registro de câmpus curso não existe!');

                    return false;
                }

                $objFinanceiroTituloConfig->setCursocampus($objCampusCurso);
            } else {
                $objFinanceiroTituloConfig->setCursocampus(null);
            }

            if ($arrDados['per']) {
                /** @var $objAcadperiodoLetivo \Matricula\Entity\AcadperiodoLetivo */
                $objAcadperiodoLetivo = $serviceAcadperiodoLetivo->getRepository()->find($arrDados['per']);

                if (!$objAcadperiodoLetivo) {
                    $this->setLastError('Registro de letivo não existe!');

                    return false;
                }

                $objFinanceiroTituloConfig->setPer($objAcadperiodoLetivo);
            } else {
                $objFinanceiroTituloConfig->setPer(null);
            }

            if ($arrDados['tipotitulo']) {
                /** @var $objFinanceiroTituloTipo \Financeiro\Entity\FinanceiroTituloTipo */
                $objFinanceiroTituloTipo = $serviceFinanceiroTituloTipo->getRepository()->find($arrDados['tipotitulo']);

                if (!$objFinanceiroTituloTipo) {
                    $this->setLastError('Registro de título tipo não existe!');

                    return false;
                }

                $objFinanceiroTituloConfig->setTipotitulo($objFinanceiroTituloTipo);
            } else {
                $objFinanceiroTituloConfig->setTipotitulo(null);
            }

            if ($arrDados['confcont']) {
                /** @var $objBoletoConfConta \Financeiro\Entity\BoletoConfConta */
                $objBoletoConfConta = $serviceBoletoConfConta->getRepository()->find($arrDados['confcont']);

                if (!$objBoletoConfConta) {
                    $this->setLastError('Registro de configuração conta não existe!');

                    return false;
                }

                $objFinanceiroTituloConfig->setConfcont($objBoletoConfConta);
            } else {
                $objFinanceiroTituloConfig->setConfcont(null);
            }

            if ($arrDados['tituloconfValordesc'] > 0 && $arrDados['tituloconfValordesc2'] > 0) {
                $this->setLastError("Adicione Valor ou Percentual para o segundo desconto de incentivo!");

                return false;
            }

            if ($arrDados['tituloconfValorDesc2'] && $arrDados['tituloconfPercentDesc']) {
                $this->setLastError("Adicione Valor ou Percentual para desconto de incentivo!");

                return false;
            }

            if (is_integer($arrDados['tituloconfDiaDesc']) and is_integer($arrDados['tituloconfDiaDesc2'])
                and $arrDados['tituloconfDiaDesc'] <= $arrDados['tituloconfDiaDesc2']
            ) {
                $this->setLastError("A segunda possibilidade de desconto deve ter a data posterior a primeira!");

                return false;
            }

            $tituloconfPercentDesc  = ((float)$arrDados['tituloconfPercentDesc'] / 100);
            $tituloconfPercentDesc2 = ((float)$arrDados['tituloconfPercentDesc2'] / 100);
            $tituloconfMulta        = ((float)$arrDados['tituloconfMulta'] / 100);
            $tituloconfJuros        = ((float)$arrDados['tituloconfJuros'] / 100);

            $objFinanceiroTituloConfig
                ->setTituloconfNome($arrDados['tituloconfNome'])
                ->setTituloconfDiaVenc($arrDados['tituloconfDiaVenc'])
                ->setTituloconfDiaDesc($arrDados['tituloconfDiaDesc'])
                ->setTituloconfValorDesc($arrDados['tituloconfValorDesc'])
                ->setTituloconfPercentDesc($tituloconfPercentDesc)
                ->setTituloconfDiaDesc2($arrDados['tituloconfDiaDesc2'])
                ->setTituloconfValorDesc2($arrDados['tituloconfValordesc2'])
                ->setTituloconfPercentDesc2($tituloconfPercentDesc2)
                ->setTituloconfMulta($tituloconfMulta)
                ->setTituloconfJuros($tituloconfJuros)
                ->setTituloconfDataInicio($arrDados['tituloconfDataInicio'])
                ->setTituloconfDataFim($arrDados['tituloconfDataFim'])
                ->setTituloconfDiaJuros($arrDados['tituloconfDiaJuros'])
                ->setTitutloconfInstrucaoAutomatica($arrDados['tituloconfigInstrucaoAutomatico'])
                ->setTituloconfDescontoIncentivoAcumulativo($arrDados['tituloconfDescontoIncentivoAcumulativo'])
                ->setTituloconfVencimentoFixo($arrDados['tituloconfVencimentoFixo'])
                ->setTituloconfVencimentoAlteravel($arrDados['tituloconfVencimentoAlteravel']);

            $this->getEm()->persist($objFinanceiroTituloConfig);
            $this->getEm()->flush($objFinanceiroTituloConfig);

            $this->getEm()->commit();

            $arrDados['tituloconfId'] = $objFinanceiroTituloConfig->getTituloconfId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de configuração de título!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if ($arrParam['tituloconfPercentDesc'] > 100 ||
            $arrParam['tituloconfPercentDesc2'] > 100 ||
            $arrParam['tituloconfMulta'] > 100 ||
            $arrParam['tituloconfJuros'] > 100
        ) {
            $this->setLastError('Valor máximo de desconto extrapolado!');

            return false;
        }

        if (!$arrParam['tituloconfDiaVenc']) {
            $errors[] = 'Por favor preencha o campo "dia de vencimento"!';
        }

        if (!$arrParam['confcont']) {
            $errors[] = 'Por favor preencha o campo "configuração de conta"!';
        }

        if ($arrParam['tituloconfDiaDesc'] && (!$arrParam['tituloconfPercentDesc'] && !$arrParam['tituloconfValorDesc'])) {
            $errors[] = 'Preencha o campo de porcetagem ou de valor para o primeiro desconto de incentivo!';
        }

        if ($arrParam['tituloconfDiaDesc2'] && (!$arrParam['tituloconfPercentDesc2'] && !$arrParam['tituloconfValordesc2'])) {
            $errors[] = 'Preencha o campo de porcetagem ou de valor para o segundo desconto de incentivo!';
        }

        if (!$arrParam['per'] && (!$arrParam['tituloconfDataInicio'] || !$arrParam['tituloconfDataFim'])) {
            $errors[] = 'Por favor preencha o campo período letivo ou as datas de início e fim de uso!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "
        SELECT
            tituloconf_nome,
            tituloconf_id,
            c.cursocampus_id,
            c.tipotitulo_id,
            ftt.tipotitulo_nome,
            c.confcont_id,
            CONCAT(
                'Banco: ',
                banc_codigo,
                ' | ',
                'Agência: ',
                confcont_agencia,
                ' | ',
                'Conta: ',
                confcont_conta,
                CASE WHEN (confcont_convenio != '' AND confcont_convenio IS NOT NULL) THEN
                    CONCAT(' | ', 'Convênio: ', confcont_convenio)
                ELSE
                    ''
                END
            ) AS conta,
            cc.curso_id,
            curso_nome,
            cc.camp_id,
            camp_nome,
            CONCAT(CONVERT(camp_nome USING UTF8), ' / ', CONVERT(curso_nome USING UTF8)) AS campus,
            c.per_id,
            per_nome,
            tituloconf_dia_venc,
            tituloconf_dia_desc,
            tituloconf_valor_desc,
            tituloconf_percent_desc,
            tituloconf_multa,
            tituloconf_juros,
            tituloconf_dia_juros,
            DATE_FORMAT(tituloconf_data_fim, '%d/%m/%Y') AS tituloconf_data_fim,
            DATE_FORMAT(tituloconf_data_inicio, '%d/%m/%Y') AS tituloconf_data_inicio
        FROM financeiro__titulo_config c
        LEFT JOIN financeiro__titulo_tipo ftt USING (tipotitulo_id)
        LEFT JOIN boleto_conf_conta bcc USING (confcont_id)
        LEFT JOIN boleto_banco bc USING (banc_id)
        LEFT JOIN campus_curso cc USING (cursocampus_id)
        LEFT JOIN org_campus USING (camp_id)
        LEFT JOIN acad_curso USING (curso_id)
        LEFT JOIN acadperiodo__letivo USING (per_id)
        ";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $tituloconfId
     * @return array
     */
    public function getArray($tituloconfId)
    {
        /** @var $objFinanceiroTituloConfig \Financeiro\Entity\FinanceiroTituloConfig */
        $objFinanceiroTituloConfig   = $this->getRepository()->find($tituloconfId);
        $serviceCampusCurso          = new \Matricula\Service\CampusCurso($this->getEm());
        $serviceAcadperiodoLetivo    = new \Matricula\Service\AcadperiodoLetivo($this->getEm());
        $serviceFinanceiroTituloTipo = new \Financeiro\Service\FinanceiroTituloTipo($this->getEm());
        $serviceBoletoConfConta      = new \Financeiro\Service\BoletoConfConta($this->getEm());

        try {
            $arrDados = $objFinanceiroTituloConfig->toArray();

            if ($arrDados['cursocampus']) {
                $arrCampusCurso          = $serviceCampusCurso->getArrSelect2(['id' => $arrDados['cursocampus']]);
                $arrDados['cursocampus'] = $arrCampusCurso ? $arrCampusCurso[0] : null;
            }

            if ($arrDados['per']) {
                $arrAcadperiodoLetivo = $serviceAcadperiodoLetivo->getArrSelect2(['id' => $arrDados['per']]);
                $arrDados['per']      = $arrAcadperiodoLetivo ? $arrAcadperiodoLetivo[0] : null;
            }

            if ($arrDados['tipotitulo']) {
                $arrFinanceiroTituloTipo = $serviceFinanceiroTituloTipo->getArrSelect2(
                    ['id' => $arrDados['tipotitulo']]
                );
                $arrDados['tipotitulo']  = $arrFinanceiroTituloTipo ? $arrFinanceiroTituloTipo[0] : null;
            }

            if ($arrDados['confcont']) {
                $arrBoletoConfConta   = $serviceBoletoConfConta->getArrSelect2(['id' => $arrDados['confcont']]);
                $arrDados['confcont'] = $arrBoletoConfConta ? $arrBoletoConfConta[0] : null;
            }

            if ($arrDados['tituloconfMulta']) {
                $arrDados['tituloconfMulta'] *= 100;
            }

            if ($arrDados['tituloconfJuros']) {
                $arrDados['tituloconfJuros'] *= 100;
            }

            if ($arrDados['tituloconfPercentDesc']) {
                $arrDados['tituloconfPercentDesc'] *= 100;
            }

            if ($arrDados['tituloconfPercentDesc2']) {
                $arrDados['tituloconfPercentDesc2'] *= 100;
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceCampusCurso          = new \Matricula\Service\CampusCurso($this->getEm());
        $serviceAcadperiodoLetivo    = new \Matricula\Service\AcadperiodoLetivo($this->getEm());
        $serviceFinanceiroTituloTipo = new \Financeiro\Service\FinanceiroTituloTipo($this->getEm());
        $serviceBoletoConfConta      = new \Financeiro\Service\BoletoConfConta($this->getEm());

        if (is_array($arrDados['cursocampus']) && !$arrDados['cursocampus']['text']) {
            $arrDados['cursocampus'] = $arrDados['cursocampus']['cursocampusId'];
        }

        if ($arrDados['cursocampus'] && is_string($arrDados['cursocampus'])) {
            $arrDados['cursocampus'] = $serviceCampusCurso->getArrSelect2(array('id' => $arrDados['cursocampus']));
            $arrDados['cursocampus'] = $arrDados['cursocampus'] ? $arrDados['cursocampus'][0] : null;
        }

        if (is_array($arrDados['per']) && !$arrDados['per']['text']) {
            $arrDados['per'] = $arrDados['per']['perId'];
        }

        if ($arrDados['per'] && is_string($arrDados['per'])) {
            $arrDados['per'] = $serviceAcadperiodoLetivo->getArrSelect2(array('id' => $arrDados['per']));
            $arrDados['per'] = $arrDados['per'] ? $arrDados['per'][0] : null;
        }

        if (is_array($arrDados['tipotitulo']) && !$arrDados['tipotitulo']['text']) {
            $arrDados['tipotitulo'] = $arrDados['tipotitulo']['tipotituloId'];
        }

        if ($arrDados['tipotitulo'] && is_string($arrDados['tipotitulo'])) {
            $arrDados['tipotitulo'] = $serviceFinanceiroTituloTipo->getArrSelect2(
                array('id' => $arrDados['tipotitulo'])
            );
            $arrDados['tipotitulo'] = $arrDados['tipotitulo'] ? $arrDados['tipotitulo'][0] : null;
        }

        if (is_array($arrDados['confcont']) && !$arrDados['confcont']['text']) {
            $arrDados['confcont'] = $arrDados['confcont']['confcontId'];
        }

        if ($arrDados['confcont'] && is_string($arrDados['confcont'])) {
            $arrDados['confcont'] = $serviceBoletoConfConta->getArrSelect2(array('id' => $arrDados['confcont']));
            $arrDados['confcont'] = $arrDados['confcont'] ? $arrDados['confcont'][0] : null;
        }

        return $arrDados;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('tituloconfId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Financeiro\Entity\FinanceiroTituloConfig */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getTituloconfId(),
                $params['value'] => (
                    $objEntity->getCursocampus()->getCamp()->getCampNome() . ' / ' .
                    $objEntity->getCursocampus()->getCurso()->getCursoNome() . ' / ' .
                    (
                    $objEntity->getPer()
                        ?
                        $objEntity->getPer()->getPerNome()
                        :
                        $objEntity->getTituloconfDataInicio(true) .
                        '-' .
                        $objEntity->getTituloconfDataFim(true)
                    )
                )
            );
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['tituloconfId']) {
            $this->setLastError('Para remover um registro de configuração de título é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objFinanceiroTituloConfig \Financeiro\Entity\FinanceiroTituloConfig */
            $objFinanceiroTituloConfig = $this->getRepository()->find($param['tituloconfId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objFinanceiroTituloConfig);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de configuração de título.');

            return false;
        }

        return true;
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        //$serviceCampusCurso       = new \Matricula\Service\CampusCurso($this->getEm());
        //$serviceAcadperiodoLetivo = new \Matricula\Service\AcadperiodoLetivo($this->getEm());

        //$serviceCampusCurso->setarDependenciasView($view);
        //$serviceAcadperiodoLetivo->setarDependenciasView($view);

        $view->setVariable(
            "arrTituloconfDescontoIncentivoAcumulativo",
            $this->getArraySelect2TituloconfDescontoIncentivoAcumulativo()
        );
    }

    /**
     * @param array $param
     * @return bool|\Financeiro\Entity\FinanceiroTituloConfig
     */
    public function pesquisarConfiguracao($param = array())
    {
        $arrCondicoes = array();
        $arrParam     = array();

        if ($param['tipotitulo']) {
            $param['tipotituloId'] = (
            is_object($param['tipotitulo'])
                ?
                $param['tipotitulo']->getTipoTituloId()
                :
                $param['tipotitulo']
            );
        }

        if ($param['tipotituloId']) {
            $arrParam['tipotituloId'] = $param['tipotituloId'];
            $arrCondicoes[]           = '(tipotitulo_id = :tipotituloId OR tipotitulo_id IS NULL)';
        }

        if ($param['cursocampusId']) {
            $arrParam['cursocampusId'] = $param['cursocampusId'];
            $arrCondicoes[]            = '(cursocampus_id = :cursocampusId OR cursocampus_id IS NULL)';
        }

        if ($param['confcontId']) {
            $arrParam['confcontId'] = $param['confcontId'];
            $arrCondicoes[]         = 'confcont_id = :confcontId';
        }

        if ($param['titulocontId']) {
            $arrParam['titulocontId'] = $param['titulocontId'];
            $arrCondicoes[]           = 'titulocont_id = :titulocontId';
        }

        $query        = '
        SELECT c.* FROM view__financeiro_configuracao_titulos c
        -- CONDITIONS --';
        $arrCondicoes = $arrCondicoes ? " WHERE " . implode(' AND ', $arrCondicoes) : "";

        $query = str_replace('-- CONDITIONS --', $arrCondicoes, $query);

        $arrResultado = $this->executeQueryWithParam($query, $arrParam)->fetch();

        //Caso não encontre um configuração válida tenta carregar a primeira registrada no banco
        /** @var $objFinanceiroTituloConfig \Financeiro\Entity\FinanceiroTituloConfig */

        if ($arrResultado['tituloconf_id']) {
            $objFinanceiroTituloConfig = $this->getRepository()->find($arrResultado['tituloconf_id']);
        }

        if (!$objFinanceiroTituloConfig) {
            $this->setLastError('Configuração de título inexistente!');

            return false;
        }

        return $objFinanceiroTituloConfig;
    }

    public static function getInstrucaoBoletoAutomatica()
    {
        return array(
            self::TITULO_CONFIG_INSTRUCAO_AUTOMATICA_SIM,
            self::TITULO_CONFIG_INSTRUCAO_AUTOMATICA_NAO
        );
    }

    /**
     * @param array $param
     * @return array
     */
    public function getArraySelect2InstrucaoBoletoAutomatica($param = array())
    {
        return $this->getArrSelect2Constantes($param, $this->getInstrucaoBoletoAutomatica());
    }

    public function getTituloconfDescontoIncentivoAcumulativo()
    {
        return array(
            self::TITULOCONF_DESCONTO_INCENTIVO_ACUMULATIVO_SIM,
            self::TITULOCONF_DESCONTO_INCENTIVO_ACUMULATIVO_NAO
        );
    }

    public function getArraySelect2TituloconfDescontoIncentivoAcumulativo(
        $param = array()
    ) {
        return $this->getArrSelect2Constantes($param, $this->getTituloconfDescontoIncentivoAcumulativo());
    }

    public function retornarVencimentoTipoTituloMatricula($tituloConf)
    {
        $dataAtual = new \Datetime();
        /** @var $objTituloConf \Financeiro\Entity\FinanceiroTituloConfig */
        $objTituloConf = $this->getRepository()->find($tituloConf);
        $diaVencimento = $objTituloConf->getTituloconfDiaVenc(true);

        if ($objTituloConf->verificarSeVencimentoFixo()) {
            $dataVencimento = new \DateTime($dataAtual->format("Y-m-") . $diaVencimento);

            if ($dataVencimento < $dataAtual) {
                $dataVencimento = $dataVencimento->add(new \DateInterval('P1M'));
            }
        } else {
            $dataVencimento = $dataAtual->add(new \DateInterval('P' . $diaVencimento . 'D'));
        }

        return $dataVencimento;
    }
}
?>