<?php

namespace Financeiro\Service;

use VersaSpine\Service\AbstractService;

class FinanceiroDescontoTipo extends AbstractService
{
    const DESCTIPO_MODALIDADE_BOLSA = 'Bolsa';
    const DESCTIPO_MODALIDADE_FINANCIAMENTO = 'Financiamento';
    const DESCTIPO_MODALIDADE_ANTECIPACAO = 'Antecipacao';
    const DESCTIPO_LIMITA_VENCIMENTO_SIM = 'Sim';
    const DESCTIPO_LIMITA_VENCIMENTO_NAO = 'Não';
    const DESCTIPO_DESCONTO_ACUMULATIVO_SIM = 'Sim';
    const DESCTIPO_DESCONTO_ACUMULATIVO_NAO = 'Não';

    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2DesctipoModalidade($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getDesctipoModalidade());
    }

    public static function getDesctipoModalidade()
    {
        return array(
            self::DESCTIPO_MODALIDADE_BOLSA,
            self::DESCTIPO_MODALIDADE_FINANCIAMENTO,
            self::DESCTIPO_MODALIDADE_ANTECIPACAO
        );
    }

    public function getArrSelect2DesctipoLimitaVencimento($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getDesctipoLimitaVencimento());
    }

    public static function getDesctipoLimitaVencimento()
    {
        return array(self::DESCTIPO_LIMITA_VENCIMENTO_SIM, self::DESCTIPO_LIMITA_VENCIMENTO_NAO);
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Financeiro\Entity\FinanceiroDescontoTipo');
    }

    public function pesquisaForJson($params)
    {
        $sql               = '
        SELECT
            a.desctipo_id,
            a.desctipo_id AS desctipoId,
            a.desctipo_descricao AS desctipo_descricao,
            a.desctipo_descricao AS desctipoDescricao,
            a.desctipo_percmax AS desctipo_percmax,
            a.desctipo_percmax AS desctipoPercmax,
            a.desctipo_valormax AS desctipo_valormax,
            a.desctipo_valormax AS desctipoValormax,
            a.desctipo_modalidade AS desctipo_modalidade,
            a.desctipo_modalidade AS desctipoModalidade,
            a.desctipo_limita_vencimento AS desctipo_limita_vencimento,
            a.desctipo_limita_vencimento AS desctipoLimitaVencimento,
            GROUP_CONCAT(t.tipotitulo_id SEPARATOR ",") AS tipotituloId,
            GROUP_CONCAT(t.tipotitulo_nome SEPARATOR ",") AS tipotituloNome
        FROM financeiro__desconto_tipo a
        LEFT JOIN financeiro__permissao_desconto_tipo_titulo p ON p.desctipo_id = a.desctipo_id
        LEFT JOIN financeiro__titulo_tipo t ON t.tipotitulo_id = p.tipotitulo_id
        WHERE';
        $desctipoDescricao = false;
        $desctipoId        = false;

        if ($params['q']) {
            $desctipoDescricao = $params['q'];
        } elseif ($params['query']) {
            $desctipoDescricao = $params['query'];
        }

        if ($params['desctipoId']) {
            $desctipoId = is_array($params['desctipoId']) ? $params['desctipoId'] : explode(',', $params['desctipoId']);
        }

        $parameters = array('desctipoDescricao' => "{$desctipoDescricao}%");
        $sql .= ' a.desctipo_descricao LIKE :desctipoDescricao';

        if ($desctipoId) {
            $parameters['desctipoId'] = explode(',', $desctipoId);
            $parameters['desctipoId'] = $desctipoId;
            $sql .= ' AND a.desctipo_id NOT IN(:desctipoId)';
        }

        $sql .= "
        GROUP BY desctipo_id
        ORDER BY a.desctipo_descricao
        LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $servicePermissaoDescontoTipoTitulo = new \Financeiro\Service\FinanceiroPermissaoDescontoTipoTitulo(
            $this->getEm()
        );

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['desctipoId']) {
                /** @var $objFinanceiroDescontoTipo \Financeiro\Entity\FinanceiroDescontoTipo */
                $objFinanceiroDescontoTipo = $this->getRepository()->find($arrDados['desctipoId']);

                if (!$objFinanceiroDescontoTipo) {
                    $this->setLastError('Registro de desconto tipo não existe!');

                    return false;
                }
            } else {
                $objFinanceiroDescontoTipo = new \Financeiro\Entity\FinanceiroDescontoTipo();
            }

            $objFinanceiroDescontoTipo->setDesctipoDescricao($arrDados['desctipoDescricao']);
            $objFinanceiroDescontoTipo->setDesctipoPercmax($arrDados['desctipoPercmax']);
            $objFinanceiroDescontoTipo->setDesctipoValormax($arrDados['desctipoValormax']);
            $objFinanceiroDescontoTipo->setDesctipoModalidade($arrDados['desctipoModalidade']);
            $objFinanceiroDescontoTipo->setDesctipoLimitaVencimento($arrDados['desctipoLimitaVencimento']);
            $objFinanceiroDescontoTipo->setDesctipoDescontoAcumulativo(
                $arrDados['desctipoDescontoAcumulativo']
            );

            $this->getEm()->persist($objFinanceiroDescontoTipo);
            $this->getEm()->flush($objFinanceiroDescontoTipo);

            if (!$servicePermissaoDescontoTipoTitulo->salvarArray($arrDados['permissao'], $objFinanceiroDescontoTipo)) {
                $this->setLastError('Falha ao víncular tipode de títulos ao tipo de desconto!');

                return false;
            }

            $this->getEm()->commit();

            $arrDados['desctipoId'] = $objFinanceiroDescontoTipo->getDesctipoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de desconto tipo!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['desctipoDescricao']) {
            $errors[] = 'Por favor preencha o campo "descrição"!';
        }

        if (!in_array($arrParam['desctipoModalidade'], self::getDesctipoModalidade())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "modalidade"!';
        }

        if (!$arrParam['desctipoLimitaVencimento']) {
            $errors[] = 'Por favor preencha o campo "limitado ao vencimento"!';
        }

        if (!in_array($arrParam['desctipoLimitaVencimento'], self::getDesctipoLimitaVencimento())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "limitado ao vencimento"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "
        SELECT
        *
        FROM financeiro__desconto_tipo fdt
        LEFT JOIN financeiro__permissao_desconto_tipo_titulo pdtt USING (desctipo_id)
        LEFT JOIN financeiro__titulo_tipo pdt USING (tipotitulo_id)

        GROUP BY desctipo_id
        ";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($desctipoId)
    {
        /** @var $objFinanceiroDescontoTipo \Financeiro\Entity\FinanceiroDescontoTipo */
        $objFinanceiroDescontoTipo = $this->getRepository()->find($desctipoId);

        $servicePermissaoDescontoTipoTitulo = new \Financeiro\Service\FinanceiroPermissaoDescontoTipoTitulo(
            $this->getEm()
        );

        try {
            $arrDados              = $objFinanceiroDescontoTipo->toArray();
            $arrDados['permissao'] = $servicePermissaoDescontoTipoTitulo->retornaArrayPeloTipoDesconto(
                $objFinanceiroDescontoTipo->getDesctipoId()
            );
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('desctipoId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Financeiro\Entity\FinanceiroDescontoTipo */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getDesctipoId();
            $arrEntity[$params['value']] = $objEntity->getDesctipoDescricao();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['desctipoId']) {
            $this->setLastError('Para remover um registro de desconto tipo é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objFinanceiroDescontoTipo \Financeiro\Entity\FinanceiroDescontoTipo */
            $objFinanceiroDescontoTipo = $this->getRepository()->find($param['desctipoId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objFinanceiroDescontoTipo);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de desconto tipo.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $view->setVariable("arrDesctipoModalidade", $this->getArrSelect2DesctipoModalidade());
        $view->setVariable("arrDesctipoLimitaVencimento", $this->getArrSelect2DesctipoLimitaVencimento());
    }

    /**
     * @param $desctipoId
     * @return bool
     */
    public function getTipoLimitaVencimento($desctipoId)
    {
        if (!$desctipoId) {
            $this->setLastError('É ncessário informar o desconto!');

            return false;
        }

        $objDescontoTipo = $this->getRepository()->findOneBy(['desctipoId' => $desctipoId]);

        if ($objDescontoTipo) {
            return $objDescontoTipo->getDesctipoLimitaVencimento() == self::DESCTIPO_LIMITA_VENCIMENTO_SIM;
        }

        return false;
    }

    public function getDesctipoDescontoAcumulativo()
    {
        return array(
            self::DESCTIPO_DESCONTO_ACUMULATIVO_SIM,
            self::DESCTIPO_DESCONTO_ACUMULATIVO_NAO
        );
    }

    public function getArraySelect2DesctipoDescontoAcumulativo($param = array())
    {
        return $this->getArrSelect2Constantes($param, $this->getDesctipoDescontoAcumulativo());
    }

    public function getDescricaoDescTipo($id)
    {
        if (!$id) {
            return false;
        }
        if (!is_array($id)) {
            $id = array($id);
        }

        $id = implode($id, ',');

        $sql = "
        SELECT  group_concat(desctipo_descricao SEPARATOR ', ') descTipo
         FROM financeiro__desconto_tipo
        WHERE desctipo_id in (" . $id . ")
        ";

        $result = $this->executeQueryWithParam($sql)->fetch();

        return $result['descTipo'];
    }

}

?>