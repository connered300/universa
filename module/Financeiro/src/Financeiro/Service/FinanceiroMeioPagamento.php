<?php

namespace Financeiro\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class FinanceiroMeioPagamento
 * @package Financeiro\Service
 */
class FinanceiroMeioPagamento extends AbstractService
{
    const MEIO_PAGAMENTO_TIPO_A_VISTA      = 'a vista';
    const MEIO_PAGAMENTO_TIPO_A_PRAZO      = 'a prazo';
    const MEIO_PAGAMENTO_USO_EXTERNO_SIM   = 'Sim';
    const MEIO_PAGAMENTO_USO_EXTERNO_NAO   = 'Não';
    const MEIO_PAGAMENTO_STATUS_ATIVA      = 'ativa';
    const MEIO_PAGAMENTO_STATUS_INATIVA    = 'inativa';
    const MEIO_PAGAMENTO_USO_MULTIPLO_SIM  = 'Sim';
    const MEIO_PAGAMENTO_USO_MULTIPLO_NAO  = 'Não';
    const MEIO_PAGAMENTO_CAMPOS_CHEQUE_SIM = 'Sim';
    const MEIO_PAGAMENTO_CAMPOS_CHEQUE_NAO = 'Não';
    const BOLETO                           = 'Boleto';
    const ISENTO                           = 'Isento';
    const DINHEIRO                         = 'Dinheiro';
    const CARTAO                           = 'Cartão';

    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2MeioPagamentoTipo($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getMeioPagamentoTipo());
    }

    /**
     * @return array
     */
    public static function getMeioPagamentoTipo()
    {
        return array(self::MEIO_PAGAMENTO_TIPO_A_VISTA, self::MEIO_PAGAMENTO_TIPO_A_PRAZO);
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2MeioPagamentoUsoExterno($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getMeioPagamentoUsoExterno());
    }

    /**
     * @return array
     */
    public static function getMeioPagamentoUsoExterno()
    {
        return array(self::MEIO_PAGAMENTO_USO_EXTERNO_SIM, self::MEIO_PAGAMENTO_USO_EXTERNO_NAO);
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2MeioPagamentoStatus($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getMeioPagamentoStatus());
    }

    /**
     * @return array
     */
    public static function getMeioPagamentoStatus()
    {
        return array(self::MEIO_PAGAMENTO_STATUS_ATIVA, self::MEIO_PAGAMENTO_STATUS_INATIVA);
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2MeioPagamentoUsoMultiplo($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getMeioPagamentoUsoMultiplo());
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2MeioPagamentoCamposCheque($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getMeioPagamentoCamposCheque());
    }

    /**
     * @return array
     */
    public static function getMeioPagamentoUsoMultiplo()
    {
        return array(self::MEIO_PAGAMENTO_USO_MULTIPLO_SIM, self::MEIO_PAGAMENTO_USO_MULTIPLO_NAO);
    }

    /**
     * @return array
     */
    public static function getMeioPagamentoCamposCheque()
    {
        return array(self::MEIO_PAGAMENTO_CAMPOS_CHEQUE_SIM, self::MEIO_PAGAMENTO_CAMPOS_CHEQUE_NAO);
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Financeiro\Entity\FinanceiroMeioPagamento');
    }

    /**
     * @param $params
     * @return array
     */
    public function pesquisaForJson($params)
    {
        $sql                    = '
        SELECT
            a.meioPagamentoId,
            a.meioPagamentoId AS meio_pagamento_id,
            a.meioPagamentoDescricao,
            a.meioPagamentoDescricao AS meio_pagamento_descricao,
            a.meioPagamentoTipo,
            a.meioPagamentoTipo AS meio_pagamento_tipo,
            a.meioPagamentoUsoExterno,
            a.meioPagamentoUsoExterno AS meio_pagamento_uso_externo,
            a.meioPagamentoStatus,
            a.meioPagamentoStatus AS meio_pagamento_status,
            a.meioPagamentoUsoMultiplo,
            a.meioPagamentoUsoMultiplo AS meio_pagamento_uso_multiplo,
            a.meioPagamentoCamposCheque,
            a.meioPagamentoCamposCheque AS meio_pagamento_campos_cheque
        FROM Financeiro\Entity\FinanceiroMeioPagamento a
        WHERE
            a.meioPagamentoDescricao LIKE :meioPagamentoDescricao AND
            a.meioPagamentoStatus LIKE :meioPagamentoStatus';
        $meioPagamentoDescricao = false;
        $meioPagamentoId        = false;
        $meioPagamentoTipo      = false;
        $meioPagamentoStatus    = self::MEIO_PAGAMENTO_STATUS_ATIVA;

        if ($params['q']) {
            $meioPagamentoDescricao = $params['q'];
        } elseif ($params['query']) {
            $meioPagamentoDescricao = $params['query'];
        }

        if ($params['meioPagamentoId']) {
            $meioPagamentoId = $params['meioPagamentoId'];
        }

        if ($params['meioPagamentoStatus']) {
            $meioPagamentoStatus = $params['meioPagamentoStatus'];
        }

        if ($params['meioPagamentoTipo']) {
            $meioPagamentoTipo = $params['meioPagamentoTipo'];
        }

        $parameters = array(
            'meioPagamentoDescricao' => "{$meioPagamentoDescricao}%",
            'meioPagamentoStatus'    => $meioPagamentoStatus
        );

        if ($meioPagamentoId) {
            $parameters['meioPagamentoId'] = explode(',', $meioPagamentoId);
            $parameters['meioPagamentoId'] = $meioPagamentoId;
            $sql .= ' AND a.meioPagamentoId NOT IN(:meioPagamentoId)';
        }

        if ($meioPagamentoTipo) {
            $parameters['meioPagamentoTipo'] = explode(',', $meioPagamentoTipo);
            $parameters['meioPagamentoTipo'] = $meioPagamentoTipo;
            $sql .= ' AND a.meioPagamentoTipo IN(:meioPagamentoTipo)';
        }

        $sql .= " ORDER BY a.meioPagamentoDescricao";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['meioPagamentoId']) {
                /** @var $objFinanceiroMeioPagamento \Financeiro\Entity\FinanceiroMeioPagamento */
                $objFinanceiroMeioPagamento = $this->getRepository()->find($arrDados['meioPagamentoId']);

                if (!$objFinanceiroMeioPagamento) {
                    $this->setLastError('Registro de meio de pagamento não existe!');

                    return false;
                }
            } else {
                $objFinanceiroMeioPagamento = new \Financeiro\Entity\FinanceiroMeioPagamento();
            }

            $objFinanceiroMeioPagamento->setMeioPagamentoDescricao($arrDados['meioPagamentoDescricao']);
            $objFinanceiroMeioPagamento->setMeioPagamentoTipo($arrDados['meioPagamentoTipo']);
            $objFinanceiroMeioPagamento->setMeioPagamentoUsoExterno($arrDados['meioPagamentoUsoExterno']);
            $objFinanceiroMeioPagamento->setMeioPagamentoStatus($arrDados['meioPagamentoStatus']);
            $objFinanceiroMeioPagamento->setMeioPagamentoUsoMultiplo($arrDados['meioPagamentoUsoMultiplo']);
            $objFinanceiroMeioPagamento->setMeioPagamentoCamposCheque($arrDados['meioPagamentoCamposCheque']);

            $this->getEm()->persist($objFinanceiroMeioPagamento);
            $this->getEm()->flush($objFinanceiroMeioPagamento);

            $this->getEm()->commit();

            $arrDados['meioPagamentoId'] = $objFinanceiroMeioPagamento->getMeioPagamentoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de meio de pagamento!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['meioPagamentoDescricao']) {
            $errors[] = 'Por favor preencha o campo "descrição"!';
        }

        if ($arrParam['meioPagamentoTipo'] && !in_array($arrParam['meioPagamentoTipo'], self::getMeioPagamentoTipo())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "tipo"!';
        }

        if (
            $arrParam['meioPagamentoUsoExterno'] &&
            !in_array($arrParam['meioPagamentoUsoExterno'], self::getMeioPagamentoUsoExterno())
        ) {
            $errors[] = 'Por favor selecione um valor válido para o campo "externo uso"!';
        }

        if (
            $arrParam['meioPagamentoStatus'] &&
            !in_array($arrParam['meioPagamentoStatus'], self::getMeioPagamentoStatus())
        ) {
            $errors[] = 'Por favor selecione um valor válido para o campo "status"!';
        }

        if (
            $arrParam['meioPagamentoUsoMultiplo'] &&
            !in_array($arrParam['meioPagamentoUsoMultiplo'], self::getMeioPagamentoUsoMultiplo())
        ) {
            $errors[] = 'Por favor selecione um valor válido para o campo "multiplo uso"!';
        }

        if (
            $arrParam['meioPagamentoCamposCheque'] &&
            !in_array($arrParam['meioPagamentoCamposCheque'], self::getMeioPagamentoCamposCheque())
        ) {
            $errors[] = 'Por favor selecione um valor válido para o campo "multiplo uso"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM financeiro__meio_pagamento";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $meioPagamentoId
     * @return array
     */
    public function getArray($meioPagamentoId)
    {
        /** @var $objFinanceiroMeioPagamento \Financeiro\Entity\FinanceiroMeioPagamento */
        $objFinanceiroMeioPagamento = $this->getRepository()->find($meioPagamentoId);

        try {
            $arrDados = $objFinanceiroMeioPagamento->toArray();
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {

        return $arrDados;
    }

    public function getArrSelect($params = array())
    {
        $arrEntities = $this->getRepository()->findBy([], ['meioPagamentoDescricao' => 'asc']);

        $arrEntitiesArr = array();

        /* @var $objEntity \Financeiro\Entity\FinanceiroMeioPagamento */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[$objEntity->getMeioPagamentoId()] = $objEntity->getMeioPagamentoDescricao();
        }

        return $arrEntitiesArr;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['meioPagamentoUsoExterno']) {
            $arrParam['meioPagamentoUsoExterno'] = $params['meioPagamentoUsoExterno'];
        }

        if ($params['id']) {
            $arrParam['meioPagamentoId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['meioPagamentoDescricao' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Financeiro\Entity\FinanceiroMeioPagamento */
        foreach ($arrEntities as $objEntity) {
            $arrEntity                   = $objEntity->toArray();
            $arrEntity[$params['key']]   = $objEntity->getMeioPagamentoId();
            $arrEntity[$params['value']] = $objEntity->getMeioPagamentoDescricao();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['meioPagamentoId']) {
            $this->setLastError('Para remover um registro de meio de pagamento é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objFinanceiroMeioPagamento \Financeiro\Entity\FinanceiroMeioPagamento */
            $objFinanceiroMeioPagamento = $this->getRepository()->find($param['meioPagamentoId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objFinanceiroMeioPagamento);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de meio de pagamento.');

            return false;
        }

        return true;
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $view->setVariable("arrMeioPagamentoTipo", $this->getArrSelect2MeioPagamentoTipo());
        $view->setVariable("arrMeioPagamentoUsoExterno", $this->getArrSelect2MeioPagamentoUsoExterno());
        $view->setVariable("arrMeioPagamentoStatus", $this->getArrSelect2MeioPagamentoStatus());
        $view->setVariable("arrMeioPagamentoUsoMultiplo", $this->getArrSelect2MeioPagamentoUsoMultiplo());
        $view->setVariable("arrMeioPagamentoCamposCheque", $this->getArrSelect2MeioPagamentoCamposCheque());
    }

    /**
     * @param $meioPagamentoId
     * @return bool|\Financeiro\Entity\FinanceiroMeioPagamento
     */
    public function retornaMeioDePagamento($meioPagamentoId)
    {
        /** @var $objFinanceiroMeioPagamento \Financeiro\Entity\FinanceiroMeioPagamento */
        $objFinanceiroMeioPagamento = $this->getRepository()->find($meioPagamentoId);

        if (!$objFinanceiroMeioPagamento) {
            $this->setLastError('Meio de pagamento não encontrado!');

            return false;
        }

        if ($objFinanceiroMeioPagamento->getMeioPagamentoStatus() != self::MEIO_PAGAMENTO_STATUS_ATIVA) {
            $this->setLastError('Meio de pagamento inativo!');

            return false;
        }

        return $objFinanceiroMeioPagamento;
    }

    /**
     * @return \Financeiro\Entity\FinanceiroMeioPagamento
     * @throws \Exception
     */
    public function retornaMeioDePagamentoDinheiro()
    {
        return $this->criaSeNecessarioERetornaMeioPagamento('Dinheiro');
    }

    /**
     * @return \Financeiro\Entity\FinanceiroMeioPagamento
     * @throws \Exception
     */
    public function retornaMeioDePagamentoBoleto()
    {
        return $this->criaSeNecessarioERetornaMeioPagamento('Boleto');
    }

    /**
     * @return \Financeiro\Entity\FinanceiroMeioPagamento
     * @throws \Exception
     */
    public function retornaMeioDePagamentoPagseguro()
    {
        return $this->criaSeNecessarioERetornaMeioPagamento('PagSeguro');
    }

    /**
     * @return \Financeiro\Entity\FinanceiroMeioPagamento
     * @throws \Exception
     */
    public function retornaMeioDePagamentoCheque()
    {
        return $this->criaSeNecessarioERetornaMeioPagamento('Cheque');
    }

    /**
     * @return \Financeiro\Entity\FinanceiroMeioPagamento
     * @throws \Exception
     */
    public function retornaMeioDePagamentoDesconto()
    {
        return $this->criaSeNecessarioERetornaMeioPagamento('Desconto');
    }

    /**
     * @param $descricao
     * @return \Financeiro\Entity\FinanceiroMeioPagamento
     * @throws \Exception
     */
    public function criaSeNecessarioERetornaMeioPagamento($descricao)
    {
        if (!$descricao) {
            throw new \Exception('Para pesquisar/criar um meio de pagamento é necessário que se forneça a descrição!!');
        }

        /** @var $objFinanceiroMeioPagamento \Financeiro\Entity\FinanceiroMeioPagamento */
        $objFinanceiroMeioPagamento = $this->getRepository()->findOneBy(['meioPagamentoDescricao' => $descricao]);

        if (!$objFinanceiroMeioPagamento) {
            $arrmeioPagto = [
                'meioPagamentoDescricao'    => $descricao,
                'meioPagamentoTipo'         => self::MEIO_PAGAMENTO_TIPO_A_PRAZO,
                'meioPagamentoUsoExterno'   => self::MEIO_PAGAMENTO_USO_EXTERNO_NAO,
                'meioPagamentoStatus'       => self::MEIO_PAGAMENTO_STATUS_ATIVA,
                'meioPagamentoUsoMultiplo'  => self::MEIO_PAGAMENTO_USO_MULTIPLO_NAO,
                'meioPagamentoCamposCheque' => self::MEIO_PAGAMENTO_CAMPOS_CHEQUE_NAO
            ];

            if ($this->save($arrmeioPagto)) {
                /** @var $objFinanceiroMeioPagamento \Financeiro\Entity\FinanceiroMeioPagamento */
                $objFinanceiroMeioPagamento = $this->getRepository()->find($arrmeioPagto['meioPagamentoId']);
            } else {
                throw new \Exception('Não foi possível encontrar ou criar o meio de pagamento de ' . $descricao . '!');
            }
        }

        return $objFinanceiroMeioPagamento;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getDescricao($id)
    {
        if (!$id) {
            return false;
        }

        if (!is_array($id)) {
            $id = array($id);
        }

        $id = implode($id, ',');

        $sql = "
        SELECT  group_concat(meio_pagamento_descricao SEPARATOR ', ') AS text
        FROM financeiro__meio_pagamento
        WHERE meio_pagamento_id IN (" . $id . ")";

        $result = $this->executeQuery($sql)->fetch();

        return $result['text'];
    }

}

?>