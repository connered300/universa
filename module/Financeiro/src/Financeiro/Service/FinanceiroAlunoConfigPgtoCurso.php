<?php

namespace Financeiro\Service;

use VersaSpine\Service\AbstractService;

class FinanceiroAlunoConfigPgtoCurso extends AbstractService
{
    const CONFIG_PGTO_CURSO_EFETIVADO_SIM   = 'Sim';
    const CONFIG_PGTO_CURSO_EFETIVADO_NAO   = 'Não';
    const CONFIG_PGTO_CURSO_ALTERADO_SIM    = 'Sim';
    const CONFIG_PGTO_CURSO_ALTERADO_NAO    = 'Não';
    const CONFIG_PGTO_CURSO_SELECIONADO_SIM = 'Sim';
    const CONFIG_PGTO_CURSO_SELECIONADO_NAO = 'Não';
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2ConfigPgtoCursoEfetivado($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getConfigPgtoCursoEfetivado());
    }

    public static function getConfigPgtoCursoEfetivado()
    {
        return array(self::CONFIG_PGTO_CURSO_EFETIVADO_SIM, self::CONFIG_PGTO_CURSO_EFETIVADO_NAO);
    }

    public function getArrSelect2ConfigPgtoCursoAlterado($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getConfigPgtoCursoAlterado());
    }

    public static function getConfigPgtoCursoAlterado()
    {
        return array(self::CONFIG_PGTO_CURSO_ALTERADO_SIM, self::CONFIG_PGTO_CURSO_ALTERADO_NAO);
    }

    public function getArrSelect2ConfigPgtoCursoSelecionado($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getConfigPgtoCursoSelecionado());
    }

    public static function getConfigPgtoCursoSelecionado()
    {
        return array(self::CONFIG_PGTO_CURSO_SELECIONADO_SIM, self::CONFIG_PGTO_CURSO_SELECIONADO_NAO);
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Financeiro\Entity\FinanceiroAlunoConfigPgtoCurso');
    }

    public function pesquisaForJson($params)
    {
        $sql               = '
        SELECT
            a.configPgtoCursoId AS config_pgto_curso_id,
            a.alunocurso AS alunocurso_id,
            a.tipotitulo AS tipotitulo_id,
            a.valores AS valores_id,
            a.meioPagamento AS meio_pagamento_id,
            a.usuarioAlteracao AS usuario_alteracao,
            a.usuarioSupervisor AS usuario_supervisor,
            a.configPgtoCursoParcela AS config_pgto_curso_parcela,
            a.configPgtoCursoValor AS config_pgto_curso_valor,
            a.configPgtoCursoVencimento AS config_pgto_curso_vencimento,
            a.configPgtoCursoEfetivado AS config_pgto_curso_efetivado,
            a.configPgtoCursoAlterado AS config_pgto_curso_alterado,
            a.configPgtoCursoSelecionado AS config_pgto_curso_selecionado
        FROM Financeiro\Entity\FinanceiroAlunoConfigPgtoCurso a
        WHERE';
        $alunocursoId      = false;
        $configPgtoCursoId = false;

        if ($params['q']) {
            $alunocursoId = $params['q'];
        } elseif ($params['query']) {
            $alunocursoId = $params['query'];
        }

        if ($params['configPgtoCursoId']) {
            $configPgtoCursoId = $params['configPgtoCursoId'];
        }

        $parameters = array('alunocursoId' => "{$alunocursoId}%");
        $sql .= ' a.alunocursoId LIKE :alunocursoId';

        if ($configPgtoCursoId) {
            $parameters['configPgtoCursoId'] = explode(',', $configPgtoCursoId);
            $parameters['configPgtoCursoId'] = $configPgtoCursoId;
            $sql .= ' AND a.configPgtoCursoId NOT IN(:configPgtoCursoId)';
        }

        $sql .= " ORDER BY a.alunocursoId";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceAcadgeralAlunoCurso     = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
        $serviceFinanceiroTituloTipo    = new \Financeiro\Service\FinanceiroTituloTipo($this->getEm());
        $serviceFinanceiroValores       = new \Financeiro\Service\FinanceiroValores($this->getEm());
        $serviceFinanceiroMeioPagamento = new \Financeiro\Service\FinanceiroMeioPagamento($this->getEm());
        $serviceAcessoPessoas           = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceFinanceiroCartao        = new \Financeiro\Service\FinanceiroCartao($this->getEm());
        $objCartao                      = null;

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['cartaoId']) {
                /** @var $objCartao  \Financeiro\Entity\FinanceiroCartao */
                if (is_a($arrDados['cartaoId'], '\Financeiro\Entity\FinanceiroCartao')) {
                    $objCartao = $arrDados['cartaoId'];
                } else {
                    $objCartao = $serviceFinanceiroCartao->getRepository()->findOneBy(
                        ['cartaoId' => $arrDados['cartaoId']]
                    );
                }

                if (!$objCartao) {
                    $this->setLastError("Cartão não localizado!");

                    return false;
                }
            }

            if ($arrDados['configPgtoCursoId']) {
                /** @var $objAlunoConfigPgtoCurso \Financeiro\Entity\FinanceiroAlunoConfigPgtoCurso */
                $objAlunoConfigPgtoCurso = $this->getRepository()->find($arrDados['configPgtoCursoId']);

                if (!$objAlunoConfigPgtoCurso) {
                    $this->setLastError('Registro de aluno configuração pgto curso não existe!');

                    return false;
                }
            } else {
                $objAlunoConfigPgtoCurso = new \Financeiro\Entity\FinanceiroAlunoConfigPgtoCurso();
            }

            if ($arrDados['alunocurso']) {
                /** @var $objAcadgeralAlunoCurso \Matricula\Entity\AcadgeralAlunoCurso */
                $objAcadgeralAlunoCurso = $serviceAcadgeralAlunoCurso->getRepository()->find($arrDados['alunocurso']);

                if (!$objAcadgeralAlunoCurso) {
                    $this->setLastError('Registro de aluno curso não existe!');

                    return false;
                }

                $objAlunoConfigPgtoCurso->setAlunocurso($objAcadgeralAlunoCurso);
            } else {
                $objAlunoConfigPgtoCurso->setAlunocurso(null);
            }

            if ($arrDados['tipotitulo']) {
                /** @var $objFinanceiroTituloTipo \Financeiro\Entity\FinanceiroTituloTipo */
                $objFinanceiroTituloTipo = $serviceFinanceiroTituloTipo->getRepository()->find($arrDados['tipotitulo']);

                if (!$objFinanceiroTituloTipo) {
                    $this->setLastError('Registro de tipo de título não existe!');

                    return false;
                }

                $objAlunoConfigPgtoCurso->setTipotitulo($objFinanceiroTituloTipo);
            } else {
                $objAlunoConfigPgtoCurso->setTipotitulo(null);
            }

            if ($arrDados['valores']) {
                /** @var $objFinanceiroValores \Financeiro\Entity\FinanceiroValores */
                $objFinanceiroValores = $serviceFinanceiroValores->getRepository()->find($arrDados['valores']);

                if (!$objFinanceiroValores) {
                    $this->setLastError('Registro de valores não existe!');

                    return false;
                }

                $objAlunoConfigPgtoCurso->setValores($objFinanceiroValores);
            } else {
                $objAlunoConfigPgtoCurso->setValores(null);
            }

            if ($arrDados['meioPagamento']) {
                /** @var $objFinanceiroMeioPagamento \Financeiro\Entity\FinanceiroMeioPagamento */
                $objFinanceiroMeioPagamento = $serviceFinanceiroMeioPagamento->getRepository()->find(
                    $arrDados['meioPagamento']
                );

                if (!$objFinanceiroMeioPagamento) {
                    $this->setLastError('Registro de meio pagamento não existe!');

                    return false;
                }

                $objAlunoConfigPgtoCurso->setMeioPagamento($objFinanceiroMeioPagamento);
            } else {
                $objAlunoConfigPgtoCurso->setMeioPagamento(null);
            }

            if ($arrDados['usuarioSupervisor']) {
                /** @var $objAcessoPessoas \Acesso\Entity\AcessoPessoas */
                $objAcessoPessoas = $serviceAcessoPessoas->getRepository()->find($arrDados['usuarioSupervisor']);

                if (!$objAcessoPessoas) {
                    $this->setLastError('Registro de pessoas não existe!');

                    return false;
                }

                $objAlunoConfigPgtoCurso->setUsuarioSupervisor($objAcessoPessoas);
            } else {
                $objAlunoConfigPgtoCurso->setUsuarioSupervisor(null);
            }

            if (!$arrDados['configPgtoCursoAlterado']) {
                $arrDados['configPgtoCursoAlterado'] = self::CONFIG_PGTO_CURSO_ALTERADO_NAO;
            }

            if (!$arrDados['configPgtoCursoEfetivado']) {
                $arrDados['configPgtoCursoEfetivado'] = self::CONFIG_PGTO_CURSO_EFETIVADO_NAO;
            }

            if ($arrDados['configPgtoCursoSelecionado'] == self::CONFIG_PGTO_CURSO_SELECIONADO_NAO) {
                $arrDados['configPgtoCursoParcela']    = null;
                $arrDados['configPgtoCursoValor']      = null;
                $arrDados['configPgtoCursoVencimento'] = null;
            }

            $objAcessoPessoas = $serviceAcessoPessoas->retornaUsuarioLogado();

            $objAlunoConfigPgtoCurso
                ->setUsuarioAlteracao($objAcessoPessoas)
                ->setConfigPgtoCursoParcela($arrDados['configPgtoCursoParcela'])
                ->setConfigPgtoCursoValor($arrDados['configPgtoCursoValor'])
                ->setConfigPgtoCursoVencimento($arrDados['configPgtoCursoVencimento'])
                ->setConfigPgtoCursoEfetivado($arrDados['configPgtoCursoEfetivado'])
                ->setConfigPgtoCursoAlterado($arrDados['configPgtoCursoAlterado'])
                ->setConfigPgtoCursoSelecionado($arrDados['configPgtoCursoSelecionado'])
                ->setConfigPgtoCursoObs($arrDados['configPgtoCursoObs'])
                ->setConfigPgtoCursoJustificativa($arrDados['configPgtoCursoJustificativa'])
                ->setCartao($objCartao);

            $this->getEm()->persist($objAlunoConfigPgtoCurso);
            $this->getEm()->flush($objAlunoConfigPgtoCurso);
            $this->getEm()->commit();

            $arrDados['configPgtoCursoId'] = $objAlunoConfigPgtoCurso->getConfigPgtoCursoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError(
                'Não foi possível salvar o registro de aluno configuração pgto curso!<br>' . $e->getMessage()
            );
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['alunocurso']) {
            $errors[] = 'Por favor preencha o campo "código curso do aluno"!';
        }

        if (!$arrParam['tipotitulo']) {
            $errors[] = 'Por favor preencha o campo "código tipo de título"!';
        }

        if ($arrParam['configPgtoCursoSelecionado'] == self::CONFIG_PGTO_CURSO_SELECIONADO_SIM) {
            if (!$arrParam['configPgtoCursoParcela']) {
                $errors[] = 'Por favor preencha o campo "parcela"!';
            }

            if (!$arrParam['configPgtoCursoValor']) {
                $errors[] = 'Por favor preencha o campo "valor"!';
            }

            if (!$arrParam['configPgtoCursoVencimento']) {
                $errors[] = 'Por favor preencha o campo "vencimento"!';
            }
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    /**
     * @param array $arrParam
     * @return bool
     */
    public function salvarMultiplos(array &$arrParam)
    {
        $serviceFinanceiroValores    = new \Financeiro\Service\FinanceiroValores($this->getEm());
        $serviceFinanceiroTituloTipo = new \Financeiro\Service\FinanceiroTituloTipo($this->getEm());
        $serviceFinanceiroConfig     = new \Financeiro\Service\FinanceiroTituloConfig($this->getEm());
        $serviceFinanceiroCartao     = new \Financeiro\Service\FinanceiroCartao($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            $pesId = $arrParam['pes'] ? $arrParam['pes'] : null;

            if (!$pesId && $arrParam['pesId']) {
                $pesId = $arrParam['pesId'] ? $arrParam['pesId'] : null;
            }

            if ($arrParam['cartaoNumero'] && !isset($arrParam['cartaoId'])) {
                $arrCartao = [
                    'pes'              => $pesId,
                    'cartaoTitular'    => $arrParam['cartaoNomeTitular'],
                    'cartaoNumero'     => $arrParam['cartaoNumero'],
                    'cartaoVencimento' => str_replace(' ', '', $arrParam['cartaoVencimento']),
                    'cartaoCvv'        => $arrParam['cartaoCvv']
                ];

                /** @var $objCartao \Financeiro\Entity\FinanceiroCartao */
                if (!$objCartao = $serviceFinanceiroCartao->save($arrCartao)) {
                    $this->setLastError($serviceFinanceiroCartao->getLastError());

                    return false;
                }
            }

            $arrAlunoCursos = $arrParam['alunoCurso'];
            $arrTipoTitulos = $arrParam['tipoTitulo'];

            foreach ($arrAlunoCursos as $arrCurso) {
                foreach ($arrTipoTitulos as $arrTipo) {
                    if ($arrTipo['campuscurso'] == $arrCurso['cursocampusId'] || !$arrTipo['campuscurso']) {
                        $arrTipo['alunocurso'] = $arrCurso['alunocursoId'];

                        $cursocampusId = '';

                        if (!$cursocampusId && $arrTipo['campuscurso']) {
                            $cursocampusId = $arrTipo['campuscurso'];
                        }

                        if (!$cursocampusId && $arrCurso['cursocampusId']) {
                            $cursocampusId = $arrCurso['cursocampusId'];
                        }

                        if (!$cursocampusId && $arrCurso['campuscursoId']) {
                            $cursocampusId = $arrCurso['campuscursoId'];
                        }

                        if (!$cursocampusId && $arrParam['cursocampusId']) {
                            $cursocampusId = $arrParam['cursocampusId'];
                        }

                        if (!$cursocampusId && $arrParam['campuscurso']) {
                            $cursocampusId = $arrParam['campuscurso'];
                        }

                        /** @var $objTituloTipo \Financeiro\Entity\FinanceiroTituloTipo */
                        $objTituloTipo = $serviceFinanceiroTituloTipo->getRepository()->find($arrTipo['tipotituloId']);

                        /*TODO: TRECHO QUE VALIDA A DATA, A PRIORI SOMENTE PARA CAPTACAO*/
                        if (isset($arrParam['validaVencimentoCaptacao'])) {
                            $arrParamConfig      = array(
                                'tipotituloId'  => $arrTipo['tipotituloId'],
                                'cursocampusId' => $cursocampusId,
                            );
                            $objConfigTipoTitulo = $serviceFinanceiroConfig->pesquisarConfiguracao($arrParamConfig);

                            if ($objTituloTipo->verificaEmissaoOpcional() && !$arrTipo['valoresId']) {
                                continue;
                            }

                            if (!$objConfigTipoTitulo->verificaSeVencimentoAlteravel()) {
                                $vencimentoFixo = $serviceFinanceiroConfig->retornarVencimentoTipoTituloMatricula(
                                    $objConfigTipoTitulo
                                );

                                if ($vencimentoFixo && !$serviceFinanceiroConfig->getLastError()) {
                                    $vencimento = $vencimentoFixo->format("d/m/Y");
                                    // Caso a data de vencimento seja passada ou alterada na interface e passada, aqui valida e sobrescreve para data correta
                                    if ($vencimento != $arrTipo['vencimento']) {
                                        $arrTipo['vencimento'] = $vencimento;

                                        $menssagem = "O tipo título: " . $arrTipo['tipotituloNome'] . " teve a data de vencimento corrigida! \n";

                                        $this->setLastError($this->getLastError() . $menssagem);
                                    }
                                }
                            }
                        }

                        if ($arrTipo['tipotituloId']) {
                            $arrTipo['tipotitulo'] = $arrTipo['tipotituloId'];
                        }

                        if ($arrTipo['vencimento']) {
                            $arrTipo['configPgtoCursoVencimento'] = $arrTipo['vencimento'];
                        }

                        if ($arrTipo['valoresId'] == '0' && $arrTipo['configPgtoCursoParcela'] && $arrTipo['configPgtoCursoValor']) {
                            $arrTipo['configPgtoCursoSelecionado'] = self::CONFIG_PGTO_CURSO_SELECIONADO_SIM;
                        } elseif ($arrTipo['valoresId'] == '0') {
                            $arrTipo['configPgtoCursoSelecionado'] = self::CONFIG_PGTO_CURSO_SELECIONADO_NAO;
                        } elseif ($arrTipo['valoresId']) {
                            $arrTipo['valores']                    = $arrTipo['valoresId'];
                            $arrTipo['configPgtoCursoSelecionado'] = self::CONFIG_PGTO_CURSO_SELECIONADO_SIM;

                            $arrValores = $serviceFinanceiroValores->getArray($arrTipo['valoresId']);

                            $arrTipo['configPgtoCursoParcela'] = $arrValores['valoresParcela'];
                            $arrTipo['configPgtoCursoValor']   = $arrValores['valoresPreco'];

                            if (
                                $arrValores['tipotitulo']['tipotituloMatriculaEmissao'] ==
                                $serviceFinanceiroTituloTipo::TIPOTITULO_MATRICULA_EMISSAO_SIM
                            ) {
                                $arrTipo['emitirTitulo'] = $serviceFinanceiroTituloTipo::TIPOTITULO_MATRICULA_EMISSAO_SIM;
                            }
                        } elseif (!$arrTipo['emitirTitulo']) {
                            $arrTipo['emitirTitulo'] = $serviceFinanceiroTituloTipo::TIPOTITULO_MATRICULA_EMISSAO_NAO;
                        }
                    }

                    $arrTipo['cartaoId'] = null;

                    if ($arrTipo['configPgtoCursoCartao'] == 'sim') {
                        $arrTipo['cartaoId'] = $objCartao;

                        if (!$arrTipo['cartaoId']) {
                            new \Exception("Cartão não informado!");
                        }
                    }

                    if (!$this->save($arrTipo)) {
                        throw new \Exception($this->getLastError());
                    }
                }
            }

            $this->getEm()->commit();

            return true;
        } catch (\Exception $e) {
            $this->getEm()->rollback();
            $this->setLastError(
                'Não foi possível salvar o registros de configuração de pagamento de curso do curso!' .
                '<br>' .
                $e->getMessage()
            );
        }

        return false;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM financeiro__aluno_config_pgto_curso";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($configPgtoCursoId)
    {
        /** @var $objFinanceiroAlunoConfigPgtoCurso \Financeiro\Entity\FinanceiroAlunoConfigPgtoCurso */
        $objFinanceiroAlunoConfigPgtoCurso = $this->getRepository()->find($configPgtoCursoId);

        $serviceAcadgeralAlunoCurso     = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
        $serviceFinanceiroTituloTipo    = new \Financeiro\Service\FinanceiroTituloTipo($this->getEm());
        $serviceFinanceiroMeioPagamento = new \Financeiro\Service\FinanceiroMeioPagamento($this->getEm());
        $serviceFinanceiroValores       = new \Financeiro\Service\FinanceiroValores($this->getEm());
        $serviceAcessoPessoas           = new \Acesso\Service\AcessoPessoas($this->getEm());

        try {
            $arrDados = $objFinanceiroAlunoConfigPgtoCurso->toArray();

            if ($arrDados['alunocurso']) {
                $arrAcadgeralAlunoCurso = $serviceAcadgeralAlunoCurso->getArrSelect2(
                    ['id' => $arrDados['alunocurso']]
                );
                $arrDados['alunocurso'] = $arrAcadgeralAlunoCurso ? $arrAcadgeralAlunoCurso[0] : null;
            }

            if ($arrDados['tipotitulo']) {
                $arrFinanceiroTituloTipo = $serviceFinanceiroTituloTipo->getArrSelect2(
                    ['id' => $arrDados['tipotitulo']]
                );
                $arrDados['tipotitulo']  = $arrFinanceiroTituloTipo ? $arrFinanceiroTituloTipo[0] : null;
            }

            if ($arrDados['valores']) {
                $arrFinanceiroValores = $serviceFinanceiroValores->getArrSelect2(['id' => $arrDados['valores']]);
                $arrDados['valores']  = $arrFinanceiroValores ? $arrFinanceiroValores[0] : null;
            }

            if ($arrDados['meioPagamento']) {
                $arrFinanceiroMeioPagamento = $serviceFinanceiroMeioPagamento->getArrSelect2(
                    ['id' => $arrDados['meioPagamento']]
                );
                $arrDados['meioPagamento']  = $arrFinanceiroMeioPagamento ? $arrFinanceiroMeioPagamento[0] : null;
            }

            if ($arrDados['usuarioAlteracao']) {
                $arrAcessoPessoas             = $serviceAcessoPessoas->getArrSelect2(
                    ['id' => $arrDados['usuarioAlteracao']]
                );
                $arrDados['usuarioAlteracao'] = $arrAcessoPessoas ? $arrAcessoPessoas[0] : null;
            }

            if ($arrDados['usuarioSupervisor']) {
                $arrAcessoPessoas              = $serviceAcessoPessoas->getArrSelect2(
                    ['id' => $arrDados['usuarioSupervisor']]
                );
                $arrDados['usuarioSupervisor'] = $arrAcessoPessoas ? $arrAcessoPessoas[0] : null;
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceAcadgeralAlunoCurso     = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
        $serviceFinanceiroTituloTipo    = new \Financeiro\Service\FinanceiroTituloTipo($this->getEm());
        $serviceFinanceiroMeioPagamento = new \Financeiro\Service\FinanceiroMeioPagamento($this->getEm());
        $serviceFinanceiroValores       = new \Financeiro\Service\FinanceiroValores($this->getEm());
        $serviceAcessoPessoas           = new \Acesso\Service\AcessoPessoas($this->getEm());

        if (is_array($arrDados['alunocurso']) && !$arrDados['alunocurso']['text']) {
            $arrDados['alunocurso'] = $arrDados['alunocurso']['alunocursoId'];
        }

        if ($arrDados['alunocurso'] && is_string($arrDados['alunocurso'])) {
            $arrDados['alunocurso'] = $serviceAcadgeralAlunoCurso->getArrSelect2(
                array('id' => $arrDados['alunocurso'])
            );
            $arrDados['alunocurso'] = $arrDados['alunocurso'] ? $arrDados['alunocurso'][0] : null;
        }

        if (is_array($arrDados['tipotitulo']) && !$arrDados['tipotitulo']['text']) {
            $arrDados['tipotitulo'] = $arrDados['tipotitulo']['tipotituloId'];
        }

        if ($arrDados['tipotitulo'] && is_string($arrDados['tipotitulo'])) {
            $arrDados['tipotitulo'] = $serviceFinanceiroTituloTipo->getArrSelect2(
                array('id' => $arrDados['tipotitulo'])
            );
            $arrDados['tipotitulo'] = $arrDados['tipotitulo'] ? $arrDados['tipotitulo'][0] : null;
        }

        if (is_array($arrDados['valores']) && !$arrDados['valores']['text']) {
            $arrDados['valores'] = $arrDados['valores']['valoresId'];
        }

        if ($arrDados['valores'] && is_string($arrDados['valores'])) {
            $arrDados['valores'] = $serviceFinanceiroValores->getArrSelect2(array('id' => $arrDados['valores']));
            $arrDados['valores'] = $arrDados['valores'] ? $arrDados['valores'][0] : null;
        }

        if (is_array($arrDados['meioPagamento']) && !$arrDados['meioPagamento']['text']) {
            $arrDados['meioPagamento'] = $arrDados['meioPagamento']['meioPagamentoId'];
        }

        if ($arrDados['meioPagamento'] && is_string($arrDados['meioPagamento'])) {
            $arrDados['meioPagamento'] = $serviceFinanceiroMeioPagamento->getArrSelect2(
                array('id' => $arrDados['meioPagamento'])
            );
            $arrDados['meioPagamento'] = $arrDados['meioPagamento'] ? $arrDados['meioPagamento'][0] : null;
        }

        if (is_array($arrDados['usuarioAlteracao']) && !$arrDados['usuarioAlteracao']['text']) {
            $arrDados['usuarioAlteracao'] = $arrDados['usuarioAlteracao']['id'];
        }

        if ($arrDados['usuarioAlteracao'] && is_string($arrDados['usuarioAlteracao'])) {
            $arrDados['usuarioAlteracao'] = $serviceAcessoPessoas->getArrSelect2(
                array('id' => $arrDados['usuarioAlteracao'])
            );
            $arrDados['usuarioAlteracao'] = $arrDados['usuarioAlteracao'] ? $arrDados['usuarioAlteracao'][0] : null;
        }

        if (is_array($arrDados['usuarioSupervisor']) && !$arrDados['usuarioSupervisor']['text']) {
            $arrDados['usuarioSupervisor'] = $arrDados['usuarioSupervisor']['id'];
        }

        if ($arrDados['usuarioSupervisor'] && is_string($arrDados['usuarioSupervisor'])) {
            $arrDados['usuarioSupervisor'] = $serviceAcessoPessoas->getArrSelect2(
                array('id' => $arrDados['usuarioSupervisor'])
            );
            $arrDados['usuarioSupervisor'] = $arrDados['usuarioSupervisor'] ? $arrDados['usuarioSupervisor'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('configPgtoCursoId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Financeiro\Entity\FinanceiroAlunoConfigPgtoCurso */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getConfigPgtoCursoId(),
                $params['value'] => $objEntity->getConfigPgtoCursoId()
            );
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['configPgtoCursoId']) {
            $this->setLastError(
                'Para remover um registro de aluno configuração pgto curso é necessário informar o código.'
            );

            return false;
        }

        try {
            /** @var $objFinanceiroAlunoConfigPgtoCurso \Financeiro\Entity\FinanceiroAlunoConfigPgtoCurso */
            $objFinanceiroAlunoConfigPgtoCurso = $this->getRepository()->find($param['configPgtoCursoId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objFinanceiroAlunoConfigPgtoCurso);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de aluno configuração pgto curso.');

            return false;
        }

        return true;
    }

    public function retornaConfiguracoesDePagamentoDoCurso($alunocursoId, $toArray = true, $somentePendentes = false)
    {
        $arrRetorno = [];
        $arrParam   = ['alunocurso' => $alunocursoId];

        if ($somentePendentes) {
            $arrParam['configPgtoCursoEfetivado']   = self::CONFIG_PGTO_CURSO_EFETIVADO_NAO;
            $arrParam['configPgtoCursoSelecionado'] = self::CONFIG_PGTO_CURSO_SELECIONADO_SIM;
        }

        $arrObjFinanceiroAlunoConfigPgtoCurso = $this->getRepository()->findBy($arrParam);

        if (!$toArray) {
            return $arrObjFinanceiroAlunoConfigPgtoCurso;
        }

        /** @var $objFinanceiroAlunoConfigPgtoCurso \Financeiro\Entity\FinanceiroAlunoConfigPgtoCurso */
        foreach ($arrObjFinanceiroAlunoConfigPgtoCurso as $objFinanceiroAlunoConfigPgtoCurso) {
            $arrRetorno[] = $objFinanceiroAlunoConfigPgtoCurso->toArray();
        }

        return $arrRetorno;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceAcadgeralAlunoCurso     = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
        $serviceFinanceiroTituloTipo    = new \Financeiro\Service\FinanceiroTituloTipo($this->getEm());
        $serviceFinanceiroValores       = new \Financeiro\Service\FinanceiroValores($this->getEm());
        $serviceFinanceiroMeioPagamento = new \Financeiro\Service\FinanceiroMeioPagamento($this->getEm());

        $serviceAcadgeralAlunoCurso->setarDependenciasView($view);
        $serviceFinanceiroTituloTipo->setarDependenciasView($view);
        $serviceFinanceiroValores->setarDependenciasView($view);
        $serviceFinanceiroMeioPagamento->setarDependenciasView($view);

        $view->setVariable("arrConfigPgtoCursoEfetivado", $this->getArrSelect2ConfigPgtoCursoEfetivado());
        $view->setVariable("arrConfigPgtoCursoAlterado", $this->getArrSelect2ConfigPgtoCursoAlterado());
        $view->setVariable("arrConfigPgtoCursoSelecionado", $this->getArrSelect2ConfigPgtoCursoSelecionado());
    }

    /**
     * @param       $alunocursoId
     * @param array $arrTipoTitulos
     * @return bool
     */
    public function salvarMultiplosItemsPeloCurso($alunocursoId, array &$arrTipoTitulos)
    {
        $serviceFinanceiroValores = new \Financeiro\Service\FinanceiroValores($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            $arrEditar  = array();
            $arrExcluir = array();

            $arrFinanceiroAlunoConfigPgtoCursosDB = $this->getRepository()->findBy(['alunocurso' => $alunocursoId]);

            /* @var $objFinanceiroAlunoConfigPgtoCurso \Financeiro\Entity\FinanceiroAlunoConfigPgtoCurso */
            foreach ($arrFinanceiroAlunoConfigPgtoCursosDB as $objFinanceiroAlunoConfigPgtoCurso) {
                $encontrado        = false;
                $configPgtoCursoId = $objFinanceiroAlunoConfigPgtoCurso->getConfigPgtoCursoId();
                $tipotituloId      = $objFinanceiroAlunoConfigPgtoCurso->getTipotitulo()->getTipotituloId();

                foreach ($arrTipoTitulos as $arrTipoTitulo) {
                    if ($tipotituloId == $arrTipoTitulo['tipotituloId']) {
                        $encontrado = true;
                        break;
                    }
                }

                if (!$encontrado) {
                    $arrExcluir[$configPgtoCursoId] = $objFinanceiroAlunoConfigPgtoCurso;
                } else {
                    $arrEditar[$tipotituloId] = $objFinanceiroAlunoConfigPgtoCurso;
                }
            }

            if ($arrExcluir) {
                foreach ($arrExcluir as $objFinanceiroAlunoConfigPgtoCurso) {
                    $this->getEm()->remove($objFinanceiroAlunoConfigPgtoCurso);
                    $this->getEm()->flush();
                }
            }

            foreach ($arrTipoTitulos as $arrTipo) {
                $arrTipo['configPgtoCursoSelecionado'] = self::CONFIG_PGTO_CURSO_SELECIONADO_NAO;

                if ($arrEditar[$arrTipo['tipotituloId']]) {
                    if ($arrEditar[$arrTipo['tipotituloId']]->cursoJaFoiEfetivado()) {
                        continue;
                    }

                    $arrTipo = array_merge($arrEditar[$arrTipo['tipotituloId']]->toArray(), $arrTipo);
                }

                $arrTipo['alunocurso'] = $alunocursoId;

                if ($arrTipo['tipotituloId']) {
                    $arrTipo['tipotitulo'] = $arrTipo['tipotituloId'];
                }

                if ($arrTipo['vencimento']) {
                    $arrTipo['configPgtoCursoVencimento'] = $arrTipo['vencimento'];
                }

                if ($arrTipo['valoresId'] == '0' && $arrTipo['configPgtoCursoParcela'] && $arrTipo['configPgtoCursoValor']) {
                    $arrTipo['configPgtoCursoSelecionado'] = self::CONFIG_PGTO_CURSO_SELECIONADO_SIM;
                } elseif ($arrTipo['valoresId'] == '0') {
                    $arrTipo['configPgtoCursoSelecionado'] = self::CONFIG_PGTO_CURSO_SELECIONADO_NAO;
                } elseif ($arrTipo['valoresId']) {
                    $arrTipo['configPgtoCursoSelecionado'] = self::CONFIG_PGTO_CURSO_SELECIONADO_SIM;
                    $arrTipo['valores']                    = $arrTipo['valoresId'];

                    if ($arrTipo['configPgtoCursoAlterado'] != 'Sim') {
                        $arrValores                        = $serviceFinanceiroValores->getArray($arrTipo['valoresId']);
                        $arrTipo['configPgtoCursoParcela'] = $arrValores['valoresParcela'];
                        $arrTipo['configPgtoCursoValor']   = $arrValores['valoresPreco'];
                    }
                }

                if (!$this->save($arrTipo)) {
                    throw new \Exception($this->getLastError());
                }
            }

            $this->getEm()->commit();

            return true;
        } catch (\Exception $e) {
            $this->setLastError(
                'Não foi possível salvar o registros de configuração de pagamento de curso do curso!' .
                '<br>' .
                $e->getMessage()
            );
        }

        return false;
    }
}
?>