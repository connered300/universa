<?php

namespace Financeiro\Service;

use VersaSpine\Service\AbstractService;

class FinanceiroRetorno extends AbstractService
{
    const PATH_RETORNO       = 6;
    const NOME_DIRETORIO     = 'baixas-bancarias';
    const STATUS_PROCESSANDO = 'Processando';
    const STATUS_PENDENTE    = 'Pendente';
    const STATUS_FINALIZADO  = 'Finalizado';
    const STATUS_REJEITADO   = 'Rejeitado';
    private $__lastError = null;

    /** @var \Sistema\Service\SisConfig */
    private $config = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @return \Sistema\Service\SisConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param \Sistema\Service\SisConfig $config
     * @return FinanceiroTitulo
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     * @param array                       $arrConfig
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, $arrConfig = array())
    {
        parent::__construct($em, 'Financeiro\Entity\FinanceiroRetorno');

        $this->setConfig(new \Sistema\Service\SisConfig($em, $arrConfig));
    }

    public function pesquisaForJson($params)
    {
        $sql       = '
        SELECT
        a.retornoId AS retorno_id,
        a.arqId AS arq_id,
        a.usuarioId AS usuario_id,
        a.retornoBanco AS retorno_banco,
        a.retornoConta AS retorno_conta,
        a.retornoData AS retorno_data
        FROM Financeiro\Entity\FinanceiroRetorno a
        WHERE';
        $arqId     = false;
        $retornoId = false;

        if ($params['q']) {
            $arqId = $params['q'];
        } elseif ($params['query']) {
            $arqId = $params['query'];
        }

        if ($params['retornoId']) {
            $retornoId = $params['retornoId'];
        }

        $parameters = array('arqId' => "{$arqId}%");
        $sql .= ' a.arqId LIKE :arqId';

        if ($retornoId) {
            $parameters['retornoId'] = explode(',', $retornoId);
            $parameters['retornoId'] = $retornoId;
            $sql .= ' AND a.retornoId NOT IN(:retornoId)';
        }

        $sql .= " ORDER BY a.arqId";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceDiretorio     = new \GerenciadorArquivos\Service\ArquivoDiretorios($this->getEm());
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());
        $objDiretorioRetorno  = $serviceDiretorio->retornaDiretorioPorNome(self::NOME_DIRETORIO);

        $serviceArquivo = new \GerenciadorArquivos\Service\Arquivo($this->getEm(), $objDiretorioRetorno);

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['retornoId']) {
                /** @var $objFinanceiroRetorno \Financeiro\Entity\FinanceiroRetorno */
                $objFinanceiroRetorno = $this->getRepository()->find($arrDados['retornoId']);

                if (!$objFinanceiroRetorno) {
                    $this->setLastError('Registro de retorno não existe!');

                    return false;
                }
            } else {
                $objFinanceiroRetorno = new \Financeiro\Entity\FinanceiroRetorno();
            }

            $arquivo = $arrDados['arq'];

            $objArq = null;

            if ($arquivo) {
                if ($arquivo['error'] == 0 || $arquivo['size'] != 0) {
                    try {
                        $objArq = $serviceArquivo->adicionar($arquivo);
                    } catch (\Exception $ex) {
                        throw new \Exception('Falha ao registrar imagem de aluno!');
                    }
                }
            }

            if (!$arrDados['retornoLog']) {
                $arrDados['retornoLog'] = '';
            }

            /** @var $objAcessoPessoas \Acesso\Entity\AcessoPessoas */
            $objAcessoPessoas = $serviceAcessoPessoas->retornaUsuarioLogado($arrDados['usuario']);

            $objFinanceiroRetorno->setArq($objArq);
            $objFinanceiroRetorno->setUsuario($objAcessoPessoas);
            $objFinanceiroRetorno->setRetornoLog($arrDados['retornoLog']);
            $objFinanceiroRetorno->setRetornoBanco($arrDados['retornoBanco']);
            $objFinanceiroRetorno->setRetornoConta($arrDados['retornoConta']);
            $objFinanceiroRetorno->setRetornoConvenio($arrDados['retornoConvenio']);
            $objFinanceiroRetorno->setRetornoData($arrDados['retornoData']);

            if (!$arrDados['retorno_status']) {
                $arrDados['retorno_status'] = self::STATUS_PENDENTE;
            }

            $objFinanceiroRetorno->setRetornoStatus($arrDados['retorno_status']);

            $this->getEm()->persist($objFinanceiroRetorno);
            $this->getEm()->flush($objFinanceiroRetorno);

            $this->getEm()->commit();

            $arrDados['retornoId'] = $objFinanceiroRetorno->getRetornoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de retorno!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['retornoBanco']) {
            $errors[] = 'Por favor preencha o campo "banco"!';
        }

        if (!$arrParam['retornoData']) {
            $errors[] = 'Por favor preencha o campo "data"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM financeiro__retorno LEFT JOIN arquivo USING (arq_id)";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($retornoId)
    {
        /** @var $objFinanceiroRetorno \Financeiro\Entity\FinanceiroRetorno */
        $objFinanceiroRetorno = $this->getRepository()->find($retornoId);
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());

        try {
            $arrDados = $objFinanceiroRetorno->toArray();

            if ($arrDados['usuario']) {
                $arrAcessoPessoas    = $serviceAcessoPessoas->getArrSelect2(['id' => $arrDados['usuario']]);
                $arrDados['usuario'] = $arrAcessoPessoas ? $arrAcessoPessoas[0] : null;
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());

        if (is_array($arrDados['arq']) && !$arrDados['arq']['text']) {
            $arrDados['arq'] = $arrDados['arq']['arqId'];
        }

        if (is_array($arrDados['usuario']) && !$arrDados['usuario']['text']) {
            $arrDados['usuario'] = $arrDados['usuario']['id'];
        }

        if ($arrDados['usuario'] && is_string($arrDados['usuario'])) {
            $arrDados['usuario'] = $serviceAcessoPessoas->getArrSelect2(array('id' => $arrDados['usuario']));
            $arrDados['usuario'] = $arrDados['usuario'] ? $arrDados['usuario'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['retornoId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['arq' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Financeiro\Entity\FinanceiroRetorno */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getRetornoId();
            $arrEntity[$params['value']] = $objEntity->getRetornoData(true);

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['retornoId']) {
            $this->setLastError('Para remover um registro de retorno é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objFinanceiroRetorno \Financeiro\Entity\FinanceiroRetorno */
            $objFinanceiroRetorno = $this->getRepository()->find($param['retornoId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objFinanceiroRetorno);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de retorno.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
    }

    public function processarArquivoRetorno($arrDados)
    {
        if (!$arrDados) {
            return false;
        }

        $serviceContaBancaria = new \Financeiro\Service\BoletoConfConta($this->getEm());
        $cnabFactory          = new \Cnab\Factory();

        $file = array(
            'name'     => $arrDados['file-0']['name'],
            'type'     => $arrDados['file-0']['type'],
            'tmp_name' => $arrDados['file-0']['tmp_name'],
            'error'    => $arrDados['file-0']['error'],
            'size'     => $arrDados['file-0']['size'],
        );

        try {
            $waitTimeout        = $this->executeQuery('SHOW SESSION VARIABLES LIKE "wait_timeout"')->fetch();
            $interactiveTimeout = $this->executeQuery('SHOW SESSION VARIABLES LIKE "interactive_timeout"')->fetch();
            $waitTimeout        = $waitTimeout['Value'];
            $interactiveTimeout = $interactiveTimeout['Value'];

            $this->executeQuery('SET @@session.wait_timeout=' . (60 * 60));
            $this->executeQuery('SET @@session.interactive_timeout=' . (60 * 60));

            /** @var \Cnab\Retorno\Cnab240\Arquivo $arquivo */
            $arquivo = $cnabFactory->createRetorno($file['tmp_name']);

            $convenio = $arquivo->getCodigoConvenio();
            $conta    = $arquivo->getConta();

            if (!$convenio && $arquivo->header->existField('codigo_cedente')) {
                $conta = $arquivo->header->codigo_cedente;
            }

            $contaExiste = $serviceContaBancaria->verificaSeContaExiste(
                $arquivo->getCodigoBanco(),
                $conta,
                $convenio
            );

            if (!$contaExiste) {
                $this->setLastError("Conta bancária não localizada!");

                return false;
            }

            $arrRetorno = $this->criarRetorno($arquivo, $file);
            $this->processarArquivo($arrRetorno['retornoId']);
            $this->atualizarRetornosSemDetalhesPendentes();

            $this->executeQuery('SET @@session.wait_timeout=' . $waitTimeout);
            $this->executeQuery('SET @@session.interactive_timeout=' . $interactiveTimeout);
        } catch (\Exception $ex) {
            $this->setLastError('Erro de leitura. ' . $ex->getMessage());

            return false;
        }

        return true;
    }

    /**
     * Metodo reponsável por realizar a leitura do arquivo de retorno enviado
     * Registra o arquivo de retorno no diretorio de retorno para futuras implementações ou consultas
     *
     * @param array $dataFile : o arquivo submetido pelo usuário
     *
     * @return array:[
     *      array: $messages      => um array de mensagens
     *      array: $erros         => um array de boletos que foram identificados mas ocorreram falha ao efeturar suas baixas
     *      array: $success       => um array de boletos que foram identificados e devidamente baixados
     *      int:   $unidentifieds => quantidade de boletos que não foram identificados no sistema
     * ]
     */
    public function lerArquivoRetorno(array $dataFile)
    {
        $arrRet = [
            'erros'     => array(),
            'baixa'     => array(),
            'total'     => 0,
            'mensagens' => array(),
            'untracked' => array()
        ];

        try {
            $waitTimeout        = $this->executeQuery('SHOW SESSION VARIABLES LIKE "wait_timeout"')->fetch();
            $interactiveTimeout = $this->executeQuery('SHOW SESSION VARIABLES LIKE "interactive_timeout"')->fetch();
            $waitTimeout        = $waitTimeout['Value'];
            $interactiveTimeout = $interactiveTimeout['Value'];

            $this->executeQuery('SET @@session.wait_timeout=' . (60 * 60));
            $this->executeQuery('SET @@session.interactive_timeout=' . (60 * 60));

            $this->getEm()->beginTransaction();

            $serviceArquivoDiretorios = new \GerenciadorArquivos\Service\ArquivoDiretorios($this->getEm());
            $objDiretorioRetorno      = $serviceArquivoDiretorios->retornaDiretorioPorNome(self::NOME_DIRETORIO);

            $serviceArquivo        = new \GerenciadorArquivos\Service\Arquivo($this->getEm(), $objDiretorioRetorno);
            $serviceAcessoPessoas  = new \Acesso\Service\AcessoPessoas($this->getEm());
            $serviceRetornoDetalhe = new \Financeiro\Service\FinanceiroRetornoDetalhe($this->getEm());
            $serviceMeioPagamento  = new \Financeiro\Service\FinanceiroMeioPagamento($this->getEm());
            $servicePagamento      = new \Financeiro\Service\FinanceiroPagamento($this->getEm());
            $serviceTitulo         = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
            $serviceSelecaoInsc    = new \Vestibular\Service\SelecaoInscricao($this->getEm());
            $serviceTituloTipo     = new \Financeiro\Service\FinanceiroTituloTipo($this->getEm());
            $serviceContaBancaria  = new \Financeiro\Service\BoletoConfConta($this->getEm());

            /** @var \Acesso\Entity\AcessoPessoas $objAcessoPessoa */
            $objAcessoPessoa    = $serviceAcessoPessoas->retornaUsuarioLogado();
            $pessoa             = ($objAcessoPessoa->getPes());
            $dataFile['pessoa'] = $pessoa->getPes()->getPesId();

            $serviceTitulo->setConfig($this->getConfig());

            try {
                $cnabFactory = new \Cnab\Factory();
                /** @var \Cnab\Retorno\Cnab240\Arquivo $arquivo */
                $arquivo     = $cnabFactory->createRetorno($dataFile['tmp_name']);
                $detalhes    = $arquivo->listDetalhes();
                $formatoCnab = preg_replace('/[^0-9]*/', '', get_class($arquivo));
            } catch (\Exception $e) {
                $arrRet['mensagens'][] = [
                    'type'    => 'danger',
                    'message' => 'Erro ao ler arquivo' .
                        PHP_EOL .
                        $e->getMessage() .
                        PHP_EOL .
                        $e->getFile() .
                        PHP_EOL .
                        $e->getLine()
                ];

                return $arrRet;
            }

            $objMeioPagamentoBoleto = $serviceMeioPagamento->retornaMeioDePagamentoBoleto();

            $convenio = '';
            $conta    = $arquivo->getConta();

            if (method_exists($arquivo, 'getCodigoConvenio')) {
                try {
                    $convenio = $arquivo->getCodigoConvenio();
                } catch (\Exception $e) {
                }
            }

            if (!$convenio && $arquivo->header->existField('codigo_cedente')) {
                $conta = $arquivo->header->codigo_cedente;
            }

            $arrRetorno = [
                'retornoId'    => '',
                'arq'          => $dataFile,
                'retornoBanco' => $arquivo->getCodigoBanco(),
                'retornoConta' => $conta,
                'retornoData'  => $arquivo->getDataGeracao(),
            ];

            $contaExiste = $serviceContaBancaria->verificaSeContaExiste(
                $arquivo->getCodigoBanco(),
                $conta,
                $convenio
            );

            if (!$contaExiste) {
                $this->setLastError("Conta não localizada no sistema!");

                return false;
            }

            if ($formatoCnab == 240) {
                $arrRetorno['retornoConvenio'] = $arquivo->getCodigoConvenio();
            }

            if (!$this->save($arrRetorno)) {
                $arrRet['mensagens'][] = ['type' => 'danger', 'message' => $this->getLastError()];

                return $arrRet;
            }

            /** @var \Financeiro\Entity\FinanceiroRetorno $objRetorno */
            $objRetorno = $this->getRepository()->find($arrRetorno['retornoId']);

            $arrRet['total'] = count($detalhes);

            /** @var \Cnab\Retorno\Cnab240\Detalhe $detalhe */

            foreach ($detalhes as $detalhe) {
                $arrDetalhe = [
                    'retorno'                => $objRetorno->getRetornoId(),
                    'detalheNossoNumero'     => $detalhe->getNossoNumero(),
                    'detalheNumeroDocumento' => $detalhe->getNumeroDocumento(),
                    'detalheValorDesconto'   => $detalhe->getValorDesconto(),
                    'detalheValorMora'       => $detalhe->getValorMoraMulta(),
                    'detalheDataVencimento'  => $detalhe->getDataVencimento(),
                    'detalheValorTitulo'     => $detalhe->getValorTitulo(),
                    'detalheValorPago'       => $detalhe->getValorRecebido(),
                    'detalheData'            => $detalhe->getDataOcorrencia(),
                    'detalheAlegacao'        => $detalhe->getAlegacaoPagador(),
                    'detalheCodigoMovimento' => $detalhe->getCodigo(),
                ];

                if (method_exists($detalhe, 'getCarteira')) {
                    try {
                        $arrDetalhe['detalheCarteira'] = $detalhe->getCarteira();
                    } catch (\Exception $e) {
                        $this->setLastError($e->getMessage());
                    }
                }

                $identificacaoTitulo = '';

                if (method_exists($detalhe, 'getIdentificacaoTitulo')) {
                    try {
                        $identificacaoTitulo = $detalhe->getIdentificacaoTitulo();
                    } catch (\Exception $e) {
                        $this->setLastError($e->getMessage());
                    }
                }

                if (!$arrDetalhe['detalheNossoNumero']) {
                    $arrDetalhe['detalheNossoNumero'] = $identificacaoTitulo;
                }

                // se código de movimento não é liquidação
                if (!in_array(
                    $detalhe->getCodigo(),
                    [
                        \Cnab\Movimento::RETORNO_LIQUIDACAO,
                        \Cnab\Movimento::RETORNO_LIQUIDACAO_APOS_BAIXA_OU_LIQUIDACAO_TITULO_NAO_REGISTRADO,
                    ]
                )
                ) {
                    /*$arrRet['mensagens'][] = [
                        'type'    => 'danger',
                        'message' => (
                            'Baixa do nosso numero: ' .
                            $detalhe->getNossoNumero() .
                            ' foi rejeitada pelo Banco. '
                        ),
                    ];*/

                    $arrRet['total'] -= 1;
                    continue;
                }

                if (!($detalhe->getValorRecebido() > 0)) {
                    $arrRet['mensagens'][] = [
                        'type'    => 'danger',
                        'message' => 'Baixa do nosso numero: ' . $detalhe->getNossoNumero() . ' com valor zerado. ',
                    ];
                    $arrRet['total'] -= 1;
                    continue;
                }

                if (!$serviceRetornoDetalhe->save($arrDetalhe)) {
                    $arrRet['mensagens'][] = ['type' => 'danger', 'message' => $this->getLastError()];

                    return $arrRet;
                }

                $objBoleto = $this->retornaObjetoBoleto($detalhe, $arquivo);

                if (!$objBoleto) {
                    $arrRet['untracked'][] = (
                    $detalhe->getNossoNumero() ? $detalhe->getNossoNumero() : $identificacaoTitulo);
                    continue;
                }
                //Atualizar antes de submeter o commit
                if ($objBoleto->getBolEstado() == \Financeiro\Service\Boleto::BOL_ESTADO_ABERTO) {
                    try {
                        $this->getEm()->beginTransaction();
                        $objBoleto->setBolEstado(\Financeiro\Service\Boleto::BOL_ESTADO_PAGO);
                        $objBoleto->setBolJuros($detalhe->getValorMoraMulta());

                        $this->getEm()->persist($objBoleto);
                        $this->getEm()->flush($objBoleto);

                        /** @var \Financeiro\Entity\FinanceiroTitulo $objTitulo */
                        $objTitulo = $objBoleto->getTitulo();

                        $usuario = $objAcessoPessoa ? $objAcessoPessoa : $objTitulo->getUsuarioAutor();

                        $arrDados = array(
                            'titulo'                  => $objTitulo,
                            'meioPagamento'           => $objMeioPagamentoBoleto,
                            'pagamentoUsuario'        => $usuario,
                            'pes'                     => $objTitulo->getPes(),
                            'pagamentoValorBruto'     => $objTitulo->getTituloValor(),
                            'pagamentoValorJuros'     => 0,
                            'pagamentoValorMulta'     => $detalhe->getValorMoraMulta(),
                            'pagamentoAcrescimos'     => 0,
                            'pagamentoValorDescontos' => 0,
                            'pagamentoValorFinal'     => $detalhe->getValorRecebido(),
                            'pagamentoDescontoManual' => 0,
                            'pagamentoDataBaixa'      => new \DateTime(),
                            'pagamentoObservacoes'    => 0,
                        );

                        try {
                            $servicePagamento->save($arrDados);
                        } catch (\Exception $e) {
                            $arrRet['erros'][] = $objBoleto->toArray();
                            continue;
                        }

                        // atualiza os valores do titulo
                        $objTitulo->setUsuarioBaixa($objAcessoPessoa);
                        $objTitulo->setTituloValorPago($detalhe->getValorRecebido());
                        $objTitulo->setTituloDataPagamento($detalhe->getDataOcorrencia());
                        $objTitulo->setTituloDataBaixa(new \Datetime());
                        $objTitulo->setTituloEstado(
                            FinanceiroTitulo::TITULO_ESTADO_PAGO
                        );

                        $this->getEm()->persist($objTitulo);
                        $this->getEm()->flush($objTitulo);

                        if (!$serviceTitulo->alteraSituacaoAlunoPorTituloId($objTitulo)) {
                            $arrRet['mensagens'][] = [
                                'type'    => 'danger',
                                'message' => (
                                    'Falha ao alterar situação de matricula de aluno: ' .
                                    $serviceTitulo->getLastError()
                                ),
                            ];
                        }

                        if (
                            $objTitulo->getTipotitulo()->getTipotituloNome() == $serviceTituloTipo::TIPOTITULOVESTIBULAR
                        ) {
                            $retObjInscricao = $serviceSelecaoInsc->atualizaSituacaoInscricaoPorBaixaBoleto($objBoleto);
                        }

                        $this->getEm()->commit();

                        $arrRet['baixa'][] = $objBoleto->toArray();
                    } catch (\Exception $e) {
                        $this->getEm()->rollBack();
                        $arrRet['erros'][] = $objBoleto->toArray();
                    }
                } else {
                    $arrRet['baixados'][] = $objBoleto->toArray();
                }
            }

            // so salva o arquivo se algum boleto for lido
            if ($arrRet['baixa']) {
                try {
                    //                $dataFile['diretorioId'] = 6;
                    //$objFileRetorno = $serviceArquivo->adicionar($dataFile);

                    //                $this->getEm()->beginTransaction();
                    //
                    //                $data = (new \DateTime('now'))->format('Y-m-d H:i');
                    //                $objBaixa = new \Boleto\Entity\BoletoBaixa();
                    //                $objBaixa->setArq($objFileRetorno);
                    //                $objBaixa->setUsuarioBaixa($objAcessoPessoa);
                    //                $objBaixa->setBaixaDataSubmissao($data);
                    //
                    //                $this->getEm()->persist($objBaixa);
                    //                $this->getEm()->flush();
                    //                $this->getEm()->commit();
                } catch (\Exception $e) {
                    $arrRet['mensagens'][] = [
                        'type'    => 'info',
                        'message' => 'Ocorreu uma falha ao salvar o arquivo de retorno, porém os boletos foram baixados',
                    ];
                }
            }

            $this->getEm()->commit();

            $this->executeQuery('SET @@session.wait_timeout=' . $waitTimeout);
            $this->executeQuery('SET @@session.interactive_timeout=' . $interactiveTimeout);
        } catch (\Exception $e) {
            $arrRet['mensagens'][] = [
                'type'    => 'danger',
                'message' => 'Estrutura do arquivo está incorreta<br>' . $e->getMessage()
            ];
        }

        return $arrRet;
    }

    /**
     * @param $detalhe \Cnab\Retorno\Cnab240\Detalhe
     * @param $arquivo \Cnab\Retorno\Cnab240\Arquivo $arquivo
     * @return \Financeiro\Entity\Boleto | null
     */
    public function retornaObjetoBoleto(&$detalhe, &$arquivo)
    {
        $service            = new \Financeiro\Service\Boleto($this->getEm());
        $serviceBoletoBanco = new \Financeiro\Service\BoletoBanco($this->getEm());

        $query = "
            SELECT boleto.bol_id
            FROM boleto
            LEFT JOIN financeiro__titulo ON boleto.titulo_id = financeiro__titulo.titulo_id
            LEFT JOIN boleto_conf_conta ON boleto_conf_conta.confcont_id = boleto.confcont_id
            LEFT JOIN boleto_banco ON boleto_banco.banc_id = boleto_conf_conta.banc_id
            WHERE
                bol_nosso_numero = :nossoNumero AND
                boleto_banco.banc_codigo = :bancoCodigo
            -- NEW WHERE --";

        $nossoNumero = $detalhe->getNossoNumero();
        $convenio    = $arquivo->getCodigoConvenio();

        $params = array(
            'bancoCodigo' => $arquivo->getCodigoBanco(),
            'nossoNumero' => $nossoNumero
        );

        switch ($codigoBanco = $arquivo->getCodigoBanco()) {
            case $serviceBoletoBanco::BANCO_BRASIL:
                $result = $this->executeQueryWithParam($query, $params)->fetch();

                if (!$result) {
                    $nossoNumero           = $detalhe->getIdentificacaoTitulo();
                    $params['nossoNumero'] = $nossoNumero;

                    $result = $this->executeQueryWithParam($query, $params)->fetch();
                }

                if (!$result && $convenio && strpos($nossoNumero, $convenio) == 0) {
                    $params['nossoNumero'] = substr($nossoNumero, strlen($convenio));

                    $result = $this->executeQueryWithParam($query, $params)->fetch();
                }

                break;
            case $serviceBoletoBanco::BANCO_BANCOOB:
                $query .= ' AND financeiro__titulo.titulo_data_vencimento=DATE(:vencimentoTitulo) ';
                $params['nossoNumero']      = substr($detalhe->getNossoNumero(), 0, -6);
                $params['vencimentoTitulo'] = $detalhe->getDataVencimento()->format('Y-m-d');

                $result = $this->executeQueryWithParam($query, $params)->fetch();

                if (!$result) {
                    $params['nossoNumero'] = substr($detalhe->getNossoNumero(), 0, -1);
                    $result                = $this->executeQueryWithParam($query, $params)->fetch();
                }

                break;
            case $serviceBoletoBanco::BANCO_BRADESCO:
                $params['nossoNumero'] = (
                strlen($params['nossoNumero']) == 6 ?
                    substr($params['nossoNumero'], 0, -1) :
                    $params['nossoNumero']
                );
                $result                = $this->executeQueryWithParam($query, $params)->fetch();
                break;
            default:
                $result = $this->executeQueryWithParam($query, $params)->fetch();
                break;
        }

        try {
            if ($objBoleto = $service->getRepository()->findOneBy(['bolId' => $result['bol_id']])) {
                return $objBoleto;
            }
        } catch (\Exception $e) {
            $this->setLastError("Boleto não encontrado no sistema. " . PHP_EOL . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $arquivo \Cnab\Retorno\Cnab240\Arquivo | \Cnab\Retorno\Cnab400\Arquivo | \Cnab\Remessa\IArquivo
     * @param $file array
     * @return bool
     */
    public function criarRetorno($arquivo, $file)
    {
        if (!$arquivo) {
            $this->setLastError("Nenhum dado informado!");

            return false;
        }

        $arrRetorno = [
            'retornoId'    => '',
            'arq'          => $file,
            'retornoBanco' => $arquivo->getCodigoBanco(),
            'retornoConta' => $arquivo->getConta(),
            'retornoData'  => $arquivo->getDataGeracao(),
        ];

        if (preg_replace('/[^0-9]*/', '', get_class($arquivo)) == 240) {
            $arrRetorno['retornoConvenio'] = $arquivo->getCodigoConvenio();
        }

        if (!$this->save($arrRetorno)) {
            return false;
        }

        return $arrRetorno;
    }

    public function atualizarLogArquivoRetorno($retornoId, $log = '')
    {
        if (!$retornoId) {
            return false;
        }

        if ($arrFinanceiroRetorno = $this->getArray($retornoId)) {
            $dataAtual = $this->formatDateBrasileiro((new \DateTime())->format('d/m/Y H:i'));

            if (!$arrFinanceiroRetorno['retornoLog'] OR is_string($arrFinanceiroRetorno['retornoLog'])) {
                $arrFinanceiroRetorno['retornoLog'] .= PHP_EOL . $dataAtual . ' - ' . $log;
            }

            $query = <<<SQL
                    UPDATE
                      financeiro__retorno
                    SET
                      retorno_log = "{$arrFinanceiroRetorno['retornoLog']}"
                    WHERE
                      retorno_id = {$retornoId}
SQL;
            try {
                $this->executeQuery($query)->fetchAll();
            } catch (\Exception $e) {
                if ($e->getErrorCode() != "HY000") {
                    $this->setLastError("Erro ao atualizar o log do retorno");

                    return false;
                }
            }
        }

        return true;
    }

    public function getArrayStatus()
    {
        return array(self::STATUS_FINALIZADO, self::STATUS_PROCESSANDO, self::STATUS_PENDENTE, self::STATUS_REJEITADO);
    }

    public function atualizarStatusArquivoRetorno($retornoId, $status)
    {
        if (!$retornoId || !$status) {
            return false;
        }

        $query = "
        UPDATE financeiro__retorno
        SET retorno_status = :retorno_status
        WHERE retorno_id = :retorno_id";

        try {
            $this->executeQueryWithParam($query, ['retorno_status' => $status, 'retorno_id' => $retornoId]);
        } catch (\Exception $e) {
            $this->setLastError("Erro ao atualizar o log do retorno");

            return false;
        }

        return true;
    }

    public function processarArquivo($retornoId)
    {
        if (!$retornoId) {
            return false;
        }

        $serviceRetornoDetalhe = new \Financeiro\Service\FinanceiroRetornoDetalhe($this->getEm());
        $cnabFactory           = new \Cnab\Factory();

        try {
            /** @var \Financeiro\Entity\FinanceiroRetorno $objFinanceiroRetorno */
            $objFinanceiroRetorno = $this->getRepository()->find($retornoId);

            if (!$objFinanceiroRetorno) {
                $this->setLastError("Retorno não encontrado no sistema!");

                return false;
            }

            $objArquivo = $objFinanceiroRetorno->getArq();
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());
            $this->atualizarLogArquivoRetorno($retornoId, $e->getMessage());

            return false;
        }

        try {
            $endereco = $objArquivo->retornaCaminhoArquivo();
            $arquivo  = $cnabFactory->createRetorno($endereco);
            $detalhes = $arquivo->listDetalhes();
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            $this->atualizarLogArquivoRetorno($retornoId, $e->getMessage());
            $this->atualizarStatusArquivoRetorno($retornoId, self::STATUS_REJEITADO);

            return false;
        }

        $result = array('total' => count($detalhes), 'encontrados' => 0);

        /** @var $detalhe \Cnab\Retorno\Cnab240\Detalhe | \Cnab\Retorno\Cnab400\Detalhe */
        foreach ($detalhes as $detalhe) {
            if (!in_array(
                $detalhe->getCodigo(),
                [
                    \Cnab\Movimento::RETORNO_LIQUIDACAO,
                    \Cnab\Movimento::RETORNO_LIQUIDACAO_APOS_BAIXA_OU_LIQUIDACAO_TITULO_NAO_REGISTRADO,
                ]
            )
            ) {
                $result['total'] -= 1;
                continue;
            }

            $arrDetalhe = [
                'retorno'                => $objFinanceiroRetorno->getRetornoId(),
                'detalheNossoNumero'     => $detalhe->getNossoNumero(),
                'detalheNumeroDocumento' => $detalhe->getNumeroDocumento(),
                'detalheValorDesconto'   => $detalhe->getValorDesconto(),
                'detalheValorMora'       => $detalhe->getValorMoraMulta(),
                'detalheDataVencimento'  => $detalhe->getDataVencimento(),
                'detalheValorTitulo'     => $detalhe->getValorTitulo(),
                'detalheValorPago'       => $detalhe->getValorRecebido(),
                'detalheData'            => $detalhe->getDataOcorrencia(),
                'detalheAlegacao'        => $detalhe->getAlegacaoPagador(),
                'detalheStatus'          => self::STATUS_PENDENTE,
                'detalheCodigoMovimento' => $detalhe->getCodigo(),
            ];

            $serviceRetornoDetalhe->save($arrDetalhe);
        }

        $this->atualizarStatusArquivoRetorno($objFinanceiroRetorno->getRetornoId(), self::STATUS_PROCESSANDO);

        return $result;
    }

    public function retornaDadosParaProcessamento()
    {
        $retornoId = null;
        $query     = "
        SELECT retorno_id
        FROM financeiro__retorno
        INNER JOIN arquivo USING (arq_id)
        WHERE retorno_status IN(:retorno_status)
        ORDER BY retorno_id ASC
        LIMIT 0,1";

        $result = $this
            ->executeQueryWithParam($query, ['retorno_status' => [self::STATUS_PENDENTE, self::STATUS_PROCESSANDO]])
            ->fetch();

        if (!$retornoId = (int)$result['retorno_id']) {
            $this->setLastError("Nenhum retorno pendente para processamento!");
        }

        return $retornoId;
    }

    /**
     * @return bool|array
     */
    public function atualizarRetornosSemDetalhesPendentes()
    {
        $query = "
        UPDATE financeiro__retorno r
        SET retorno_status=:retorno_status
        WHERE (
            SELECT COUNT(*)
            FROM financeiro__retorno_detalhe d
            WHERE d.retorno_id = r.retorno_id AND detalhe_status = :detalhe_status
        ) = 0";

        $param = ['retorno_status' => self::STATUS_FINALIZADO, 'detalhe_status' => self::STATUS_PENDENTE];

        $result = $this->executeQueryWithParam($query, $param);
    }
}
