<?php

namespace Financeiro\Service;

use VersaSpine\Service\AbstractService;

class FinanceiroRemessaLote extends AbstractService
{
    private $__lastError = null;

    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Financeiro\Entity\FinanceiroRemessaLote');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError ''
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function valida($data)
    {
    }

    public function pesquisaForJson($data)
    {
    }

    public function save(array $arrdata)
    {
        if (is_numeric($arrdata['bol'])) {
            $arrdata['bol'] = $this->getEm()
                ->getRepository('Financeiro\Entity\Boleto')
                ->find($arrdata['bol']);
        }

        if (is_numeric($arrdata['remessa'])) {
            $arrdata['remessa'] = $this->getEm()
                ->getRepository('Financeiro\Entity\FinanceiroRemessa')
                ->find($arrdata['remessa']);
        }

        $objRemessa = new \Financeiro\Entity\FinanceiroRemessaLote($arrdata);

        $this->getEm()->persist($objRemessa);
        $this->getEm()->flush($objRemessa);

        return $objRemessa;
    }
}