<?php

namespace Financeiro\Service;

use VersaSpine\Service\AbstractService;

class FinanceiroFiltro extends AbstractService
{
    const FILTRO_ESTADO_PAGO                          = 'Pago';
    const FILTRO_ESTADO_ABERTO                        = 'Aberto';
    const FILTRO_ESTADO_CANCELADO                     = 'Cancelado';
    const FILTRO_ESTADO_ESTORNO                       = 'Estorno';
    const FILTRO_ESTADO_ALTERACAO                     = 'Alteracao';
    const FILTRO_ESTADO_PAGAMENTO_MULTIPLO            = 'Pagamento Múltiplo';
    const FILTRO_ESTADO_ESTORNO_DE_PAGAMENTO_MULTIPLO = 'Estorno De Pagamento Múltiplo';
    const FILTRO_SITUACAO_ATIVO                       = 'Ativo';
    const FILTRO_SITUACAO_INATIVO                     = 'Inativo';
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2FiltroEstado($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getFiltroEstado());
    }

    public static function getFiltroEstado()
    {
        return array(
            self::FILTRO_ESTADO_PAGO,
            self::FILTRO_ESTADO_ABERTO,
            self::FILTRO_ESTADO_CANCELADO,
            self::FILTRO_ESTADO_ESTORNO,
            self::FILTRO_ESTADO_ALTERACAO,
            self::FILTRO_ESTADO_ESTORNO_DE_PAGAMENTO_MULTIPLO,
            self::FILTRO_ESTADO_PAGAMENTO_MULTIPLO
        );
    }

    public function getArrSelect2FiltroSituacao($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getFiltroSituacao());
    }

    public static function getFiltroSituacao()
    {
        return array(self::FILTRO_SITUACAO_ATIVO, self::FILTRO_SITUACAO_INATIVO);
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Financeiro\Entity\FinanceiroFiltro');
    }

    public function pesquisaForJson($params)
    {
        $sql        = '
        SELECT
        a.filtroId as filtro_id,
        a.filtroNome as filtro_nome,
        a.filtroEstado as filtro_estado,
        a.filtroSituacao as filtro_situacao
        FROM Financeiro\Entity\FinanceiroFiltro a
        WHERE';
        $filtroNome = false;
        $filtroId   = false;

        if ($params['q']) {
            $filtroNome = $params['q'];
        } elseif ($params['query']) {
            $filtroNome = $params['query'];
        }

        if ($params['filtroId']) {
            $filtroId = $params['filtroId'];
        }

        $parameters = array('filtroNome' => "{$filtroNome}%");
        $sql .= ' a.filtroNome LIKE :filtroNome';

        if ($filtroId) {
            $parameters['filtroId'] = explode(',', $filtroId);
            $parameters['filtroId'] = $filtroId;
            $sql .= ' AND a.filtroId NOT IN(:filtroId)';
        }

        $sql .= " ORDER BY a.filtroNome";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceFinanceiroFiltroTipo = new \Financeiro\Service\FinanceiroFiltroTipo($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['filtroId']) {
                /** @var $objFinanceiroFiltro \Financeiro\Entity\FinanceiroFiltro */
                $objFinanceiroFiltro = $this->getRepository()->find($arrDados['filtroId']);

                if (!$objFinanceiroFiltro) {
                    $this->setLastError('Registro de filtro não existe!');

                    return false;
                }
            } else {
                $objFinanceiroFiltro = new \Financeiro\Entity\FinanceiroFiltro();
            }

            $objFinanceiroFiltro->setFiltroNome($arrDados['filtroNome']);
            $objFinanceiroFiltro->setFiltroEstado($arrDados['filtroEstado']);
            $objFinanceiroFiltro->setFiltroSituacao($arrDados['filtroSituacao']);

            $this->getEm()->persist($objFinanceiroFiltro);
            $this->getEm()->flush($objFinanceiroFiltro);

            if ($arrDados['tipoTitulo'] && !$serviceFinanceiroFiltroTipo->salvarArray(
                    $arrDados['tipoTitulo'],
                    $objFinanceiroFiltro
                )
            ) {
                $this->setLastError('Falha ao víncular tipode de títulos ao tipo de desconto!');

                return false;
            }

            $this->getEm()->commit();

            $arrDados['filtroId'] = $objFinanceiroFiltro->getFiltroId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de filtro!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (array_diff($arrParam['filtroEstado'], self::getFiltroEstado())) {
            $errors[] = 'Por favor selecione valores válidos para o campo "estado"!';
        }

        if (!in_array($arrParam['filtroSituacao'], self::getFiltroSituacao())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "situação"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM financeiro__filtro";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($filtroId)
    {
        /** @var $objFinanceiroFiltro \Financeiro\Entity\FinanceiroFiltro */
        $objFinanceiroFiltro = $this->getRepository()->find($filtroId);

        $serviceFinanceiroFiltroTipo = new \Financeiro\Service\FinanceiroFiltroTipo($this->getEm());

        try {
            $arrDados               = $objFinanceiroFiltro->toArray();
            $arrDados['tipoTitulo'] = $serviceFinanceiroFiltroTipo->retornaArrayPeloFiltro(
                $objFinanceiroFiltro->getFiltroId()
            );
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        return $arrDados;
    }

    public function getArrSelect()
    {
        $arrEntities = $this->getRepository()->findBy([], ['filtroNome' => 'asc']);

        $arrEntitiesArr = array();

        /* @var $objEntity \Financeiro\Entity\FinanceiroFiltro */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[$objEntity->getFiltroId()] = $objEntity->getFiltroNome();
        }

        return $arrEntitiesArr;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['filtroId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['FiltroNome' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Financeiro\Entity\FinanceiroFiltro */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getFiltroId();
            $arrEntity[$params['value']] = $objEntity->getFiltroNome();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['filtroId']) {
            $this->setLastError('Para remover um registro de filtro é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objFinanceiroFiltro \Financeiro\Entity\FinanceiroFiltro */
            $objFinanceiroFiltro = $this->getRepository()->find($param['filtroId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objFinanceiroFiltro);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de filtro.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $view->setVariable("arrFiltroEstado", $this->getArrSelect2FiltroEstado());
        $view->setVariable("arrFiltroSituacao", $this->getArrSelect2FiltroSituacao());
    }

    public function retornaFiltrosAtivosPainel()
    {
        $arrRetorno = [];
        $arrFiltros = $this->getRepository()->findBy(
            ['filtroSituacao' => self::FILTRO_SITUACAO_ATIVO],
            ['filtroNome' => 'asc']
        );

        /** @var \Financeiro\Entity\FinanceiroFiltro $objFiltro */
        foreach ($arrFiltros as $objFiltro) {
            $arrRetorno[] = $this->getArray($objFiltro->getFiltroId());
        }

        return $arrRetorno;
    }
}
?>