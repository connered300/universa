<?php

namespace Financeiro\Service;

use VersaSpine\Service\AbstractService;

class BoletoBanco extends AbstractService
{
    const BANCO_BRASIL = 1;
    const BANCO_NORDESTE_BRASIL = 4;
    const BANCO_SANTANDER = 33;
    const BANCO_BRADESCO_BDI = 36;
    const BANCO_CAIXA = 104;
    const BANCO_ITAU_BBA = 184;
    const BANCO_BRADESCO = 237;
    const BANCO_ACB_BRASIL = 246;
    const BANCO_ITAU_UNIBANCO = 341;
    const BANCO_MERCANTIL = 389;
    const BANCO_HSBC = 399;
    const BANCO_UNIBANCO = 409;
    const BANCO_BANCOOB = 756;
    const BANCO_UNICRED = 136;

    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Financeiro\Entity\BoletoBanco');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function pesquisaForJson($params)
    {
        $sql      = '
        SELECT
            a.bancId as banc_id,
            a.bancCodigo as banc_codigo,
            a.bancNome as banc_nome
        FROM Financeiro\Entity\BoletoBanco a
        WHERE';
        $bancNome = false;
        $bancId   = false;

        if ($params['q']) {
            $bancNome = $params['q'];
        } elseif ($params['query']) {
            $bancNome = $params['query'];
        }

        if ($params['bancId']) {
            $bancId = $params['bancId'];
        }

        $parameters = array(
            'bancNome' => "%{$bancNome}%",
            'bancIdx'  => "$bancNome"
        );
        $sql .= ' (a.bancNome LIKE :bancNome OR a.bancCodigo = :bancIdx)';

        if ($bancId) {
            $parameters['bancId'] = explode(',', $bancId);
            $parameters['bancId'] = $bancId;
            $sql .= ' AND a.bancId NOT IN(:bancId)';
        }

        $sql .= " ORDER BY a.bancNome";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceBoletoLayout = new \Financeiro\Service\BoletoLayout($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['bancId']) {
                /** @var $objBoletoBanco \Financeiro\Entity\BoletoBanco */
                $objBoletoBanco = $this->getRepository()->find($arrDados['bancId']);

                if (!$objBoletoBanco) {
                    $this->setLastError('Registro de banco não existe!');

                    return false;
                }
            } else {
                $objBoletoBanco = new \Financeiro\Entity\BoletoBanco();
            }

            $objBoletoBanco->setBancCodigo($arrDados['bancCodigo']);
            $objBoletoBanco->setBancNome($arrDados['bancNome']);

            if ($arrDados['layout']) {
                /** @var $objBoletoLayout \Financeiro\Entity\BoletoLayout */
                $objBoletoLayout = $serviceBoletoLayout->getRepository()->find($arrDados['layout']);

                if (!$objBoletoLayout) {
                    $this->setLastError('Registro de layout não existe!');

                    return false;
                }

                $objBoletoBanco->setLayout($objBoletoLayout);
            } else {
                $objBoletoBanco->setLayout(null);
            }

            $this->getEm()->persist($objBoletoBanco);
            $this->getEm()->flush($objBoletoBanco);

            $this->getEm()->commit();

            $arrDados['bancId'] = $objBoletoBanco->getBancId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de banco!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['bancCodigo']) {
            $errors[] = 'Por favor preencha o campo "codigo"!';
        }

        if (!$arrParam['bancNome']) {
            $errors[] = 'Por favor preencha o campo "nome"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM boleto_banco";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($bancId)
    {
        /** @var $objBoletoBanco \Financeiro\Entity\BoletoBanco */
        $objBoletoBanco      = $this->getRepository()->find($bancId);
        $serviceBoletoLayout = new \Financeiro\Service\BoletoLayout($this->getEm());

        try {
            $arrDados = $objBoletoBanco->toArray();

            if ($arrDados['layout']) {
                /** @var $objBoletoLayout \Financeiro\Entity\BoletoLayout */
                $objBoletoLayout = $serviceBoletoLayout->getRepository()->find($arrDados['layout']);

                if ($objBoletoLayout) {
                    $arrDados['layout'] = array(
                        'id'   => $objBoletoLayout->getLayoutId(),
                        'text' => $objBoletoLayout->getLayoutId()
                    );
                } else {
                    $arrDados['layout'] = null;
                }
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceBoletoLayout = new \Financeiro\Service\BoletoLayout($this->getEm());

        if (is_array($arrDados['layout']) && !$arrDados['layout']['id']) {
            $arrDados['layout']['id']   = $arrDados['layout']['layoutId'];
            $arrDados['layout']['text'] = $arrDados['layout']['layoutId'];
        } elseif ($arrDados['layout'] && is_string($arrDados['layout'])) {
            $arrDados['layout'] = $serviceBoletoLayout->getArrSelect2(
                array('id' => $arrDados['layout'])
            );
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('bancId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Financeiro\Entity\BoletoBanco */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getBancId(),
                $params['value'] => $objEntity->getBancNome()
            );
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['bancId']) {
            $this->setLastError('Para remover um registro de banco é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objBoletoBanco \Financeiro\Entity\BoletoBanco */
            $objBoletoBanco = $this->getRepository()->find($param['bancId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objBoletoBanco);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de banco.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceBoletoLayout = new \Financeiro\Service\BoletoLayout($this->getEm());

        $serviceBoletoLayout->setarDependenciasView($view);
    }
}
?>