<?php

namespace Financeiro\Service;

use VersaSpine\Service\AbstractService;

class BoletoConfConta extends AbstractService
{
    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Financeiro\Entity\BoletoConfConta');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function pesquisaForJson($params)
    {
        $sql           = '
        SELECT
            a.confcontId as confcont_id,
            b.bancId as banc_id,
            b.bancNome as banc_nome,
            a.confcontAgencia as confcont_agencia,
            a.confcontAgenciaDigito as confcont_agencia_digito,
            a.confcontConta as confcont_conta,
            a.confcontContaDigito as confcont_conta_digito,
            CONCAT(
                \'Banco: \',
                b.bancCodigo,
                \' | \',
                \'Agência: \',
                a.confcontAgencia,
                \' | \',
                \'Conta: \',
                a.confcontConta,
                CASE WHEN (a.confcontConvenio != \'\' AND a.confcontConvenio IS NOT NULL) THEN
                    CONCAT(\' | \', \'Convênio: \', a.confcontConvenio)
                ELSE
                    \'\'
                END
            ) as conta,
            a.confcontCarteira as confcont_carteira,
            a.confcontVariacao as confcont_variacao,
            a.confcontConvenio as confcont_convenio,
            a.confcontContrato as confcont_contrato,
            a.confcontInstrucoes as confcont_instrucoes,
            a.confcontNossoNumeroInicial as confcont_nosso_numero_inicial
        FROM Financeiro\Entity\BoletoConfConta a
        JOIN Financeiro\Entity\BoletoBanco b
        WHERE
            a.banc = b.bancId AND
            a.confcontConta LIKE :confcontConta';
        $confcontConta = false;
        $confcontId    = false;

        if ($params['q']) {
            $confcontConta = $params['q'];
        } elseif ($params['query']) {
            $confcontConta = $params['query'];
        }

        if ($params['confcontId']) {
            $confcontId = $params['confcontId'];
        }

        $parameters = array('confcontConta' => "{$confcontConta}%");

        if ($confcontId) {
            $parameters['confcontId'] = explode(',', $confcontId);
            $parameters['confcontId'] = $confcontId;
            $sql .= ' AND a.confcontId NOT IN(:confcontId)';
        }

        $sql .= " ORDER BY a.confcontId";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $servicePessoa      = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceBoletoBanco = new \Financeiro\Service\BoletoBanco($this->getEm());
        $serviceArquivo     = new \GerenciadorArquivos\Service\Arquivo($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['confcontId']) {
                /** @var $objBoletoConfConta \Financeiro\Entity\BoletoConfConta */
                $objBoletoConfConta = $this->getRepository()->find($arrDados['confcontId']);

                if (!$objBoletoConfConta) {
                    $this->setLastError('Registro de configuração de conta não existe!');

                    return false;
                }
            } else {
                $objBoletoConfConta = new \Financeiro\Entity\BoletoConfConta();
            }

            if ($arrDados['pes']) {
                /** @var $objPessoa \Pessoa\Entity\Pessoa */
                $objPessoa = $servicePessoa->getRepository()->find($arrDados['pes']);

                if (!$objPessoa) {
                    $this->setLastError('Registro de pessoa não existe!');

                    return false;
                }

                $objBoletoConfConta->setPes($objPessoa);
            } else {
                $objBoletoConfConta->setPes(null);
            }

            if ($arrDados['banc']) {
                /** @var $objBoletoBanco \Financeiro\Entity\BoletoBanco */
                $objBoletoBanco = $serviceBoletoBanco->getRepository()->find($arrDados['banc']);

                if (!$objBoletoBanco) {
                    $this->setLastError('Registro de banco não existe!');

                    return false;
                }

                $objBoletoConfConta->setBanc($objBoletoBanco);
            } else {
                $objBoletoConfConta->setBanc(null);
            }

            if ($arrDados['arq']) {
                /** @var $objArquivo \GerenciadorArquivos\Entity\Arquivo */
                $objArquivo = $serviceArquivo->getRepository()->find($arrDados['arq']);

                if (!$objArquivo) {
                    $this->setLastError('Registro de arquivo não existe!');

                    return false;
                }

                $objBoletoConfConta->setArq($objArquivo);
            } else {
                $objBoletoConfConta->setArq(null);
            }

            $objBoletoConfConta->setConfcontAgencia($arrDados['confcontAgencia']);
            $objBoletoConfConta->setConfcontAgenciaDigito($arrDados['confcontAgenciaDigito']);
            $objBoletoConfConta->setConfcontConta($arrDados['confcontConta']);
            $objBoletoConfConta->setConfcontContaDigito($arrDados['confcontContaDigito']);
            $objBoletoConfConta->setConfcontCarteira($arrDados['confcontCarteira']);
            $objBoletoConfConta->setConfcontVariacao($arrDados['confcontVariacao']);
            $objBoletoConfConta->setConfcontConvenio($arrDados['confcontConvenio']);
            $objBoletoConfConta->setConfcontContrato($arrDados['confcontContrato']);
            $objBoletoConfConta->setConfcontInstrucoes($arrDados['confcontInstrucoes']);
            $objBoletoConfConta->setConfcontNossoNumeroInicial($arrDados['confcontNossoNumeroInicial']);
            $objBoletoConfConta->setConfcontRemessaSequencial($arrDados['confcontRemessaSequencial']);

            $this->getEm()->persist($objBoletoConfConta);
            $this->getEm()->flush($objBoletoConfConta);

            $this->getEm()->commit();

            $arrDados['confcontId'] = $objBoletoConfConta->getConfcontId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de configuração de conta!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['pes']) {
            $errors[] = 'Por favor preencha o campo "código pessoa"!';
        }

        if (!$arrParam['banc']) {
            $errors[] = 'Por favor preencha o campo "código banco"!';
        }

        if (!$arrParam['confcontAgencia']) {
            $errors[] = 'Por favor preencha o campo "agência"!';
        }

        if (!$arrParam['confcontConta']) {
            $errors[] = 'Por favor preencha o campo "conta"!';
        }

        if (!$arrParam['confcontNossoNumeroInicial']) {
            $errors[] = 'Por favor preencha o campo "número nosso inicial"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function verificaSeContaExiste($banco = false, $conta = false, $convenio = false)
    {
        $query = "
        SELECT count(*) as t FROM boleto_conf_conta
        LEFT JOIN boleto_banco USING (banc_id)
        WHERE
            (confcont_conta =:conta AND banc_codigo=:banco) OR
            (confcont_convenio =:convenio AND banc_codigo=:banco)";

        $params = [
            'banco'    => $banco,
            'conta'    => $conta,
            'convenio' => $convenio,
        ];

        $result = $this->executeQueryWithParam($query, $params)->fetch();

        return $result['t'] > 0;
    }

    public function getDataForDatatables($data)
    {
        $query = "
        SELECT * FROM boleto_conf_conta
        LEFT JOIN pessoa USING (pes_id)
        LEFT JOIN boleto_banco USING (banc_id)
        ";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($confcontId)
    {
        /** @var $objBoletoConfConta \Financeiro\Entity\BoletoConfConta */
        $objBoletoConfConta = $this->getRepository()->find($confcontId);
        $servicePessoa      = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceBoletoBanco = new \Financeiro\Service\BoletoBanco($this->getEm());
        $serviceArquivo     = new \GerenciadorArquivos\Service\Arquivo($this->getEm());

        try {
            $arrDados = $objBoletoConfConta->toArray();

            if ($arrDados['pes']) {
                /** @var $objPessoa \Pessoa\Entity\Pessoa */
                $objPessoa = $servicePessoa->getRepository()->find($arrDados['pes']);

                if ($objPessoa) {
                    $arrDados['pes'] = array(
                        'id'   => $objPessoa->getPesId(),
                        'text' => $objPessoa->getPesNome()
                    );
                } else {
                    $arrDados['pes'] = null;
                }
            }

            if ($arrDados['banc']) {
                /** @var $objBoletoBanco \Financeiro\Entity\BoletoBanco */
                $objBoletoBanco = $serviceBoletoBanco->getRepository()->find($arrDados['banc']);

                if ($objBoletoBanco) {
                    $arrDados['banc'] = array(
                        'id'   => $objBoletoBanco->getBancId(),
                        'text' => $objBoletoBanco->getBancNome()
                    );
                } else {
                    $arrDados['banc'] = null;
                }
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $servicePessoa      = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceBoletoBanco = new \Financeiro\Service\BoletoBanco($this->getEm());
        $serviceArquivo     = new \GerenciadorArquivos\Service\Arquivo($this->getEm());

        if (is_array($arrDados['pes']) && !$arrDados['pes']['id']) {
            $arrDados['pes']['id']   = $arrDados['pes']['pesId'];
            $arrDados['pes']['text'] = $arrDados['pes']['pesNome'];
        } elseif ($arrDados['pes'] && is_string($arrDados['pes'])) {
            $arrDados['pes'] = $servicePessoa->getArrSelect2(
                array('id' => $arrDados['pes'])
            );
        }

        if (is_array($arrDados['banc']) && !$arrDados['banc']['id']) {
            $arrDados['banc']['id']   = $arrDados['banc']['bancId'];
            $arrDados['banc']['text'] = $arrDados['banc']['bancNome'];
        } elseif ($arrDados['banc'] && is_string($arrDados['banc'])) {
            $arrDados['banc'] = $serviceBoletoBanco->getArrSelect2(
                array('id' => $arrDados['banc'])
            );
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('confcontId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objConfConta\Financeiro\Entity\BoletoConfConta */
        foreach ($arrEntities as $objConfConta) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objConfConta->getConfcontId(),
                $params['value'] => (
                    'Banco: ' .
                    $objConfConta->getBanc()->getBancNome() .
                    ' | ' .
                    'Agência: ' .
                    $objConfConta->getConfcontAgencia() .
                    ' | ' .
                    'Conta: ' .
                    $objConfConta->getConfcontConta() .
                    (
                    $objConfConta->getConfcontConvenio() ?
                        (
                            ' | ' .
                            'Convênio: ' .
                            $objConfConta->getConfcontConvenio()
                        ) :
                        ''
                    )
                )
            );
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['confcontId']) {
            $this->setLastError('Para remover um registro de configuração de conta é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objBoletoConfConta \Financeiro\Entity\BoletoConfConta */
            $objBoletoConfConta = $this->getRepository()->find($param['confcontId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objBoletoConfConta);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de configuração de conta.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $servicePessoa      = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceBoletoBanco = new \Financeiro\Service\BoletoBanco($this->getEm());
        $serviceArquivo     = new \GerenciadorArquivos\Service\Arquivo($this->getEm());

        //$servicePessoa->setarDependenciasView($view);
        //$serviceBoletoBanco->setarDependenciasView($view);
        //$serviceArquivo->setarDependenciasView($view);
    }
}
?>