<?php

namespace Financeiro\Service;

use VersaSpine\Service\AbstractService;

class FinanceiroCheque extends AbstractService
{
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Financeiro\Entity\FinanceiroCheque');
    }

    public function pesquisaForJson($params)
    {
        $sql        = '
        SELECT * FROM financeiro__cheque            
        WHERE 1=1';
        $parameters = [];
        $chequeId   = false;

        if ($params['chequeBanco'] and $params['chequeAgencia'] and $params['chequeConta'] and $params['chequeNum']) {
            $sql .= '
             AND trim( leading 0 from cheque_num) = trim( leading 0 from :chequeNum ) AND 
                 trim( leading 0 from cheque_agencia) = trim( leading 0 from :chequeAgencia ) AND
                 trim( leading 0 from cheque_banco) = trim( leading 0 from :chequeBanco ) AND
                 trim( leading 0 from cheque_conta) = trim( leading 0 from :chequeConta )';

            $parameters['chequeBanco']   = $params['chequeBanco'];
            $parameters['chequeNum']     = $params['chequeNum'];
            $parameters['chequeConta']   = $params['chequeConta'];
            $parameters['chequeAgencia'] = $params['chequeAgencia'];
        }

        if ($params['chequeId']) {
            $chequeId = $params['chequeId'];
        }

        if ($chequeId) {
            $parameters['chequeId'] = explode(',', $chequeId);
            $parameters['chequeId'] = $chequeId;
            $sql .= ' AND cheque_id NOT IN(:chequeId)';
        }

        if (!isset($params['limit'])) {
            $limit = " LIMIT 40 ";

            if (!$params['limit'] && isset($params['limit'])) {
                $limit = '';
            } elseif (intval($params['limit'])) {
                $limit = " LIMIT " . $params['limit'];
            }

            $sql .= $limit;
        }

        $result = $this->executeQueryWithParam($sql,$parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());
        $servicePessoa        = new \Pessoa\Service\Pessoa($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['chequeId']) {
                /** @var $objFinanceiroCheque \Financeiro\Entity\FinanceiroCheque */
                $objFinanceiroCheque = $this->getRepository()->find($arrDados['chequeId']);

                if (!$objFinanceiroCheque) {
                    $this->setLastError('Registro de cheque não existe!');

                    return false;
                }
            } else {
                $arrAux = $this->pesquisaForJson(
                    [
                        'chequeBanco'   => $arrDados['chequeBanco'],
                        'chequeAgencia' => $arrDados['chequeAgencia'],
                        'chequeConta'   => $arrDados['chequeConta'],
                        'chequeNum'     => $arrDados['chequeNum']
                    ]
                );

                if (!empty($arrAux)) {
                    $this->setLastError("Este cheque já foi cadastrado!");

                    return false;
                }

                $objFinanceiroCheque = new \Financeiro\Entity\FinanceiroCheque();
            }

            if ($arrDados['pes']) {
                $arrDados['pes'] =
                    is_array(
                        $arrDados['pes']) && isset($arrDados['pes']['id']) ? $arrDados['pes']['id'] : $arrDados['pes'];

                /** @var $objPessoa \Pessoa\Entity\Pessoa */
                $objPessoa = $servicePessoa->getRepository()->find($arrDados['pes']);

                if (!$objPessoa) {
                    $this->setLastError('Registro de pessoa não existe!');

                    return false;
                }

                $objFinanceiroCheque->setPes($objPessoa);
            } else {
                $objFinanceiroCheque->setPes(null);
            }

            if (!$objFinanceiroCheque->getChequeDataCadastro()) {
                $dataCadastro = new \DateTime();

                if ($objFinanceiroCheque->getChequeId()) {
                    $dataCadastro = $objFinanceiroCheque->getChequeEmissao();
                }

                $objFinanceiroCheque->setChequeDataCadastro($dataCadastro);
            }

            /** @var $objAcessoPessoas \Acesso\Entity\AcessoPessoas */
            $objAcessoPessoas = $serviceAcessoPessoas->retornaUsuarioLogado($arrDados['usuario']);

            $objFinanceiroCheque->setUsuario($objAcessoPessoas ? $objAcessoPessoas : null);
            $objFinanceiroCheque->setChequeEmitente($arrDados['chequeEmitente']);
            $objFinanceiroCheque->setChequeNum($arrDados['chequeNum']);
            $objFinanceiroCheque->setChequeBanco($arrDados['chequeBanco']);
            $objFinanceiroCheque->setChequeAgencia($arrDados['chequeAgencia']);
            $objFinanceiroCheque->setChequePraca($arrDados['chequePraca']);
            $objFinanceiroCheque->setChequeEmissao($arrDados['chequeEmissao']);
            $objFinanceiroCheque->setChequeVencimento($arrDados['chequeVencimento']);
            $objFinanceiroCheque->setChequeCompensado($arrDados['chequeCompensado']);
            $objFinanceiroCheque->setChequeValor($arrDados['chequeValor']);
            $objFinanceiroCheque->setChequeConta($arrDados['chequeConta']);
            $objFinanceiroCheque->setChequeObservacao($arrDados['chequeObservacao']);

            $this->getEm()->persist($objFinanceiroCheque);
            $this->getEm()->flush($objFinanceiroCheque);

            $this->getEm()->commit();

            $arrDados['chequeId'] = $objFinanceiroCheque->getChequeId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de cheque!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['chequeEmitente']) {
            $errors[] = 'Por favor preencha o campo "emitente"!';
        }

        if (!$arrParam['chequeNum']) {
            $errors[] = 'Por favor preencha o campo "número"!';
        }

        if (!$arrParam['chequeBanco']) {
            $errors[] = 'Por favor preencha o campo "banco"!';
        }

        if (!$arrParam['chequeAgencia']) {
            $errors[] = 'Por favor preencha o campo "agência"!';
        }

        if (!$arrParam['chequePraca']) {
            $errors[] = 'Por favor preencha o campo "praça"!';
        }

        if (!$arrParam['chequeEmissao']) {
            $errors[] = 'Por favor preencha o campo "emissão"!';
        }

        if (!$arrParam['chequeVencimento']) {
            $errors[] = 'Por favor preencha o campo "vencimento"!';
        }

        if (!$arrParam['chequeValor']) {
            $errors[] = 'Por favor preencha o campo "valor"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "
        SELECT fc.*,a.login AS usuario_login, pes_nome,
        IF(bb.banc_nome IS NOT NULL,CONCAT(bb.banc_nome,' / Cod.: ',fc.cheque_banco),fc.cheque_banco) banco,
        (SELECT count(*) FROM financeiro__titulo_cheque ftc WHERE ftc.cheque_id=fc.cheque_id) AS qtd
        FROM financeiro__cheque fc
        LEFT JOIN pessoa p ON p.pes_id=fc.pes_id
        LEFT JOIN acesso_pessoas a ON a.id=fc.usuario_id
        LEFT JOIN boleto_banco bb ON bb.banc_codigo= fc.cheque_banco
        ";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($chequeId)
    {
        /** @var $objFinanceiroCheque \Financeiro\Entity\FinanceiroCheque */
        $objFinanceiroCheque  = $this->getRepository()->find($chequeId);
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());
        $servicePessoa        = new \Pessoa\Service\Pessoa($this->getEm());

        try {
            $arrDados = $objFinanceiroCheque->toArray();

            if ($arrDados['usuario']) {
                $arrAcessoPessoas    = $serviceAcessoPessoas->getArrSelect2(['id' => $arrDados['usuario']]);
                $arrDados['usuario'] = $arrAcessoPessoas ? $arrAcessoPessoas[0] : null;
            }

            if ($arrDados['pes']) {
                $arrPessoa       = $servicePessoa->getArrSelect2(['id' => $arrDados['pes']]);
                $arrDados['pes'] = $arrPessoa ? $arrPessoa[0] : null;
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());
        $servicePessoa        = new \Pessoa\Service\Pessoa($this->getEm());

        if (is_array($arrDados['usuario']) && !$arrDados['usuario']['text']) {
            $arrDados['usuario'] = $arrDados['usuario']['id'];
        }

        if (is_array($arrDados['pes']) && !$arrDados['pes']['text']) {
            $arrDados['pes'] = $arrDados['pes']['id'];
        }

        if ($arrDados['usuario'] && is_string($arrDados['usuario'])) {
            $arrDados['usuario'] = $serviceAcessoPessoas->getArrSelect2(array('id' => $arrDados['usuario']));
            $arrDados['usuario'] = $arrDados['usuario'] ? $arrDados['usuario'][0] : null;
        }

        if ($arrDados['pes'] && is_string($arrDados['pes'])) {
            $arrDados['pes'] = $servicePessoa->getArrSelect2(array('id' => $arrDados['pes']));
            $arrDados['pes'] = $arrDados['pes'] ? $arrDados['pes'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['chequeId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['UsuarioId' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Financeiro\Entity\FinanceiroCheque */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getChequeId();
            $arrEntity[$params['value']] = $objEntity->getChequeNum();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['chequeId']) {
            $this->setLastError('Para remover um registro de cheque é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objFinanceiroCheque \Financeiro\Entity\FinanceiroCheque */
            $objFinanceiroCheque = $this->getRepository()->find($param['chequeId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objFinanceiroCheque);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de cheque.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
    }

    public function verificaSeChequeJaFoiCadastrado(array $arrDadosCheque)
    {
        if (!empty($arrDadosCheque)) {
            $sql = '
            SELECT a FROM Financeiro\Entity\FinanceiroCheque a
            WHERE (trim(a.chequeNum) LIKE :chequeNum) AND
                  (a.chequeConta = :chequeConta OR trim(a.chequeEmitente) LIKE :chequeEmitente) AND
                  (a.chequeBanco = :chequeBanco OR a.chequeBanco * 1 = :chequeBancoInt)';

            $chequeNum      = trim($arrDadosCheque['chequeNum']);
            $chequeConta    = preg_replace('/[^0-9]/', '', $arrDadosCheque['chequeConta']);
            $chequeBanco    = $arrDadosCheque['chequeBanco'];
            $chequeBancoInt = 1 * preg_replace('/[^0-9]/', '', $arrDadosCheque['chequeBanco']);
            $chequeEmitente = trim($arrDadosCheque['chequeEmitente']);

            $parameters = array(
                'chequeNum'      => $chequeNum,
                'chequeConta'    => $chequeConta,
                'chequeBanco'    => $chequeBanco,
                'chequeBancoInt' => $chequeBancoInt,
                'chequeEmitente' => $chequeEmitente,
            );

            $query = $this->getEm()->createQuery($sql);

            foreach ($parameters as $paramNome => $paramValue) {
                $query->setParameter($paramNome, $paramValue);
            }

            $arrFinanceiroCheque = $query->getResult();

            if ($arrFinanceiroCheque) {
                /** @var \Financeiro\Entity\FinanceiroCheque $objFinanceiroCheque */
                $objFinanceiroCheque           = $arrFinanceiroCheque[0];
                $serviceFinanceiroTituloCheque = new \Financeiro\Service\FinanceiroTituloCheque($this->getEm());

                $arrTituloscheques = $serviceFinanceiroTituloCheque
                    ->getRepository()
                    ->findBy(['cheque' => $objFinanceiroCheque->getChequeId()]);

                $valorUtilizadoCheque = 0;

                if ($arrTituloscheques) {

                    /** @var \Financeiro\Entity\FinanceiroTituloCheque $tituloCheque */
                    foreach ($arrTituloscheques as $tituloCheque) {
                        if ($tituloCheque->getTitulochequeEstado() === 'Ativo') {
                            $valorUtilizadoCheque += $tituloCheque->getTitulochequeValor();
                        }
                    }
                }

                return array_merge(
                    $objFinanceiroCheque->toArray(),
                    [
                        'chequeValorUtilizado' => $valorUtilizadoCheque,
                        'chequeValorRestante'  => ($objFinanceiroCheque->getChequeValor() - $valorUtilizadoCheque)
                    ]
                );
            }
        }

        return null;
    }

    public function registrarChequeParaPagamento(\Financeiro\Entity\FinanceiroPagamento $objPagamento, $arrDados)
    {
        $arrInformacoesCheque = $this->verificaSeChequeJaFoiCadastrado($arrDados);

        if (!$arrInformacoesCheque) {
            if (!$this->save($arrDados)) {
                return false;
            }

            $arrInformacoesCheque = $this->verificaSeChequeJaFoiCadastrado($arrDados);
        }

        if ($arrInformacoesCheque['chequeValorRestante'] <= 0) {
            $this->setLastError('O cheque informado não possui valor disponível para uso!');

            return false;
        }

        if ($objPagamento->getPagamentoValorFinal() > $arrInformacoesCheque['chequeValorRestante']) {
            $this->setLastError('O valor solicitado para desconto no cheque é maior que o valor disponível para uso!');

            return false;
        }

        $serviceFinanceiroTituloCheque = new \Financeiro\Service\FinanceiroTituloCheque($this->getEm());

        $arrFinanceiroTituloCheque = array(
            'pagamento'          => $objPagamento,
            'cheque'             => $arrInformacoesCheque['chequeId'],
            'titulochequeValor'  => $objPagamento->getPagamentoValorFinal(),
            'titulochequeEstado' => $serviceFinanceiroTituloCheque::TITULOCHEQUE_ESTADO_ATIVO
        );

        if (!$serviceFinanceiroTituloCheque->save($arrFinanceiroTituloCheque)) {
            $this->setLastError($serviceFinanceiroTituloCheque->getLastError());

            return false;
        }

        return true;
    }

    public function retornaSaldoCheque($chequeId)
    {
        if (!$chequeId) {
            $this->setLastError("É necessário informar um cheque para consulta!");

            return false;
        }

        $serviceTitulo = NEW \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $estadoTitulo  = $serviceTitulo::TITULO_ESTADO_PAGO;

        $sql      = "
        SELECT
            cheques.cheque_valor AS 'valorTotalCheque',
            COALESCE(sum(pagamento.pagamento_valor_final), 0) AS 'valorUsado',
            cheques.cheque_valor AS 'chequeValor',
            (cheques.cheque_valor - COALESCE(SUM(pagamento.pagamento_valor_final), 0)) AS 'chequeSaldo',
            cheque_emitente AS 'chequeEmitente',
            cheque_praca AS 'chequePraca',
            cheque_vencimento AS 'chequeVencimento'
        FROM financeiro__titulo_cheque AS tCheque
        INNER JOIN financeiro__cheque AS cheques ON cheques.cheque_id=tCheque.cheque_id
        LEFT JOIN financeiro__pagamento AS pagamento ON pagamento.pagamento_id=tCheque.pagamento_id
        LEFT JOIN financeiro__titulo AS titulo ON (
            titulo.titulo_id=pagamento.titulo_id OR titulo.titulo_id=tCheque.titulo_id
        )
        WHERE
            tCheque.cheque_id=:chequeId AND
            titulo.titulo_estado=:estadoTitulo AND
            titulocheque_estado=:estado";
        $arrParam = [
            'chequeId'     => $chequeId,
            'estadoTitulo' => $estadoTitulo,
            'estado'       => \Financeiro\Service\FinanceiroTituloCheque::TITULOCHEQUE_ESTADO_ATIVO,
        ];

        $result = $this->executeQueryWithParam($sql, $arrParam)->fetch();

        return $result;
    }

    public function getDataTablesInformacoes($param)
    {
        $param['chequeId'] = $param['chequeId'] * 1;

        if (!$param['chequeId']) {
            $this->setLastError("É necessário informar um cheque!");

            return false;
        }

        $usoInativo   = \Financeiro\Service\FinanceiroTituloCheque::TITULOCHEQUE_ESTADO_INATIVO;
        $usoAtivo     = \Financeiro\Service\FinanceiroTituloCheque::TITULOCHEQUE_ESTADO_ATIVO;
        $estadoTitulo = \Financeiro\Service\FinanceiroTitulo::TITULO_ESTADO_PAGO;

        $condicaoSaldo = 'SUM(COALESCE(IF(titulocheque_estado != "' . $usoInativo . '" AND titulo_estado = "' . $estadoTitulo . '" , titulocheque_valor, 0), 0))';

        $totalizadores = '
        cheque_valor,
        ' . $condicaoSaldo . ' AS valorUtilizado,
        (cheque_valor - ' . $condicaoSaldo . ') saldoCheque,
        COUNT(cheque_id) quantidade_titulos_vinculados';

        $sqlDataTables = "
        SELECT 
            CONCAT(
                pessoa.pes_nome ,
                (IF(ft.alunocurso_id IS NOT NULL, CONCAT(' (', ft.alunocurso_id, ')'), ''))
            ) AS alunoNome,
            /*Dados para Totalizador*/            
            fc.cheque_id,
            fc.cheque_valor,
            financeiro__titulo_cheque.titulocheque_valor,
            ft.titulo_id tituloId,
            ft.titulo_descricao descricaoTitulo,
            ft.titulo_valor_pago valorPago,
            acesso.login AS usuarioCadastro,
            COALESCE(titulocheque_valor, 0) valor_utilizado,
            titulo_estado,
            IF(titulocheque_estado = '" . $usoInativo . "' OR titulo_estado != '" . $estadoTitulo . "' , '" . $usoInativo . "', '" . $usoAtivo . "') as titulocheque_estado
        FROM financeiro__titulo_cheque
        INNER JOIN financeiro__cheque fc ON fc.cheque_id=financeiro__titulo_cheque.cheque_id
        LEFT JOIN financeiro__pagamento fp ON fp.pagamento_id=financeiro__titulo_cheque.pagamento_id
        LEFT JOIN financeiro__titulo ft ON ft.titulo_id = fp.titulo_id OR ft.titulo_id=financeiro__titulo_cheque.titulo_id
        LEFT JOIN pessoa  ON pessoa.pes_id=ft.pes_id
        LEFT JOIN acesso_pessoas acesso ON acesso.id=coalesce(fp.pagamento_usuario,ft.usuario_baixa)

        WHERE financeiro__titulo_cheque.cheque_id = " . $param["chequeId"] . "
        ORDER BY fp.pagamento_data_baixa, ft.titulo_id";

        $result = parent::paginationDataTablesAjax($sqlDataTables, $param, null, false, false, "", $totalizadores);

        $result['data'];

        return $result;
    }
}
?>