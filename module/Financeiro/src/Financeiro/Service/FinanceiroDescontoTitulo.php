<?php

namespace Financeiro\Service;

use Aluno\Service\Financeiro;
use VersaSpine\Service\AbstractService;

class FinanceiroDescontoTitulo extends AbstractService
{
    const DESCONTO_TITULO_APLICADO_SIM = 'Sim';
    const DESCONTO_TITULO_APLICADO_NAO = 'Não';
    const DESCONTO_TITULO_SITUACAO_PENDENTE = 'Pendente';
    const DESCONTO_TITULO_SITUACAO_DEFERIDO = 'Deferido';
    const DESCONTO_TITULO_SITUACAO_INDEFERIDO = 'Indeferido';

    const FINANCEIRO_CONFIG_CALCULO_DESCONTO_INCENTIVO_VALOR_BRUTO = 'valorBruto';
    const FINANCEIRO_CONFIG_CALCULO_DESCONTO_INCENTIVO_VALOR_LIQUIDO = 'valorLiquido';
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2DescontoTituloAplicado($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getDescontoTituloAplicado());
    }

    public static function getDescontoTituloAplicado()
    {
        return array(self::DESCONTO_TITULO_APLICADO_SIM, self::DESCONTO_TITULO_APLICADO_NAO);
    }

    public function getArrSelect2DescontoTituloSituacao($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getDescontoTituloSituacao());
    }
    public function getArrSelect2DescontoTituloModoCalculo($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getDescontoTituloModo());
    }

    public static function getDescontoTituloSituacao()
    {
        return array(
            self::DESCONTO_TITULO_SITUACAO_PENDENTE,
            self::DESCONTO_TITULO_SITUACAO_DEFERIDO,
            self::DESCONTO_TITULO_SITUACAO_INDEFERIDO
        );
    }

    public static function getDescontoTituloModo()
    {
        return array(
            self::FINANCEIRO_CONFIG_CALCULO_DESCONTO_INCENTIVO_VALOR_BRUTO,
            self::FINANCEIRO_CONFIG_CALCULO_DESCONTO_INCENTIVO_VALOR_LIQUIDO
        );
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Financeiro\Entity\FinanceiroDescontoTitulo');
    }

    public function pesquisaForJson($params)
    {
        $sql              = '
        SELECT
            a.descontoTituloId AS desconto_titulo_id,
            a.descontoId AS desconto_id,
            a.tituloId AS titulo_id,
            a.usuarioAlteracao AS usuario_alteracao,
            a.usuarioCriacao AS usuario_criacao,
            a.descontoTituloCriacao AS desconto_titulo_criacao,
            a.descontoTituloAlteracao AS desconto_titulo_alteracao,
            a.descontoTituloAplicado AS desconto_titulo_aplicado,
            a.descontoTituloValor AS desconto_titulo_valor,
            a.descontoTituloSituacao AS desconto_titulo_situacao
        FROM Financeiro\Entity\FinanceiroDescontoTitulo a
        WHERE';
        $descontoId       = false;
        $descontoTituloId = false;

        if ($params['q']) {
            $descontoId = $params['q'];
        } elseif ($params['query']) {
            $descontoId = $params['query'];
        }

        if ($params['descontoTituloId']) {
            $descontoTituloId = $params['descontoTituloId'];
        }

        $parameters = array('descontoId' => "{$descontoId}%");
        $sql .= ' a.descontoId LIKE :descontoId';

        if ($descontoTituloId) {
            $parameters['descontoTituloId'] = explode(',', $descontoTituloId);
            $parameters['descontoTituloId'] = $descontoTituloId;
            $sql .= ' AND a.descontoTituloId NOT IN(:descontoTituloId)';
        }

        $sql .= " ORDER BY a.descontoId";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    public function save(array &$arrDados, $reversaoDesconto = true)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceFinanceiroPagamento = new \Financeiro\Service\FinanceiroPagamento($this->getEm());
        $serviceFinanceiroDesconto  = new \Financeiro\Service\FinanceiroDesconto($this->getEm());
        $serviceFinanceiroTitulo    = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceAcessoPessoas       = new \Acesso\Service\AcessoPessoas($this->getEm());

        if ($arrDados['descontoPercentual'] > 100) {
            $this->setLastError('Percentual de desconto extrapoou 100% !');

            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['descontoTituloId']) {
                /** @var $objFinanceiroDescontoTitulo \Financeiro\Entity\FinanceiroDescontoTitulo */
                $objFinanceiroDescontoTitulo = $this->getRepository()->find($arrDados['descontoTituloId']);

                if (!$objFinanceiroDescontoTitulo) {
                    $this->setLastError('Registro de desconto título não existe!');

                    return false;
                }
            } else {
                $objFinanceiroDescontoTitulo = new \Financeiro\Entity\FinanceiroDescontoTitulo();

                $objFinanceiroDescontoTitulo
                    ->setDescontoTituloCriacao(new \DateTime())
                    ->setDescontoTituloAplicado(self::DESCONTO_TITULO_APLICADO_NAO);

                if (!$arrDados['descontoTituloSituacao']) {
                    $arrDados['descontoTituloSituacao'] = self::DESCONTO_TITULO_SITUACAO_PENDENTE;
                }
            }

            if ($arrDados['desconto']) {
                /** @var $objFinanceiroDesconto \Financeiro\Entity\FinanceiroDesconto */
                $objFinanceiroDesconto = $serviceFinanceiroDesconto->getRepository()->find($arrDados['desconto']);

                if (!$objFinanceiroDesconto) {
                    $this->setLastError('Registro de desconto não existe!');

                    return false;
                }

                $objFinanceiroDescontoTitulo->setDesconto($objFinanceiroDesconto);
            } else {
                $objFinanceiroDescontoTitulo->setDesconto(null);
            }

            if ($arrDados['titulo']) {
                /** @var $objFinanceiroTitulo \Financeiro\Entity\FinanceiroTitulo */
                $objFinanceiroTitulo = $serviceFinanceiroTitulo->getRepository()->find($arrDados['titulo']);

                if (!$objFinanceiroTitulo) {
                    $this->setLastError('Registro de título não existe!');

                    return false;
                }

                $objFinanceiroDescontoTitulo->setTitulo($objFinanceiroTitulo);
            } else {
                $objFinanceiroDescontoTitulo->setTitulo(null);
            }

            /** @var $objAcessoPessoas1 \Acesso\Entity\AcessoPessoas */
            $objAcessoPessoas1 = $serviceAcessoPessoas->retornaUsuarioLogado($arrDados['usuarioCriacao']);

            if (!$objAcessoPessoas1) {
                $this->setLastError('Registro de pessoas não existe!');

                return false;
            }

            /** @var $objAcessoPessoas2 \Acesso\Entity\AcessoPessoas */
            $objAcessoPessoas2 = $serviceAcessoPessoas->retornaUsuarioLogado();

            if (!$objAcessoPessoas2) {
                $this->setLastError('Registro de pessoas não existe!');

                return false;
            }

            $objFinanceiroDescontoTitulo->setDescontoTituloAlteracao(new \DateTime());
            $objFinanceiroDescontoTitulo->setUsuarioCriacao($objAcessoPessoas1);
            $objFinanceiroDescontoTitulo->setUsuarioAlteracao($objAcessoPessoas2);
            $objFinanceiroDescontoTitulo->setDescontoTituloSituacao($arrDados['descontoTituloSituacao']);
            $objFinanceiroDescontoTitulo->setDescontoTituloAplicado($arrDados['descontoTituloAplicado']);

            $this->getEm()->persist($objFinanceiroDescontoTitulo);
            $this->getEm()->flush($objFinanceiroDescontoTitulo);

            if (!$reversaoDesconto) {
                $tituloId = $arrDados['tituloId'] ? $arrDados['tituloId'] : $arrDados['titulo'];

                /** @var \Financeiro\Entity\FinanceiroTitulo $objFinanceiroTitulo */
                $objFinanceiroTitulo = $serviceFinanceiroTitulo->getRepository()->find($tituloId);

                $abrirTitulo = (
                    $arrDados['descontoStatus'] == $serviceFinanceiroDesconto::DESCONTO_STATUS_INATIVO OR
                    $arrDados['descontoTituloSituacao'] == self::DESCONTO_TITULO_SITUACAO_INDEFERIDO
                );

                $tituloPago = (
                    $objFinanceiroTitulo->getTituloEstado() == $serviceFinanceiroTitulo::TITULO_ESTADO_PAGO
                );

                /** @var  $objFinanceiroPagamento \Financeiro\Entity\FinanceiroPagamento */
                $objFinanceiroPagamento = $serviceFinanceiroPagamento->getRepository()->findOneBy(
                    ['titulo' => $objFinanceiroTitulo->getTituloId()]
                );

                if (($abrirTitulo && $tituloPago && $reversaoDesconto)) {
                    if (!$objFinanceiroPagamento) {
                        if (!$this->cancelaPagamentoPorDesconto($arrDados)) {
                            return false;
                        }
                    } else {
                        $this->setLastError(
                            ($this->getLastError() ? $this->getLastError() . ' \n ' : '') .
                            'O título ' . $objFinanceiroTitulo->getTituloId() .
                            ' foi pago, não é possivel inativar o desconto!'
                        );

                        return false;
                    }
                }
            }

            $arrDados['descontoTituloId'] = $objFinanceiroDescontoTitulo->getDescontoTituloId();

            $this->getEm()->commit();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de desconto título!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $errors                  = array();

        $titulo                  = $arrParam['titulo'];
        $informacoesExtras       = $serviceFinanceiroTitulo->retornaInformacoesTitulo($titulo);
        $arrParam['tituloValor'] = str_replace(array('.', ','), array('', '.'), $arrParam['tituloValor']);
        $descontos               = ($informacoesExtras['somaDescontos'] + $arrParam['descontoAplicacaoValor']);

        if (($arrParam['tituloValor'] - $descontos) < 0) {
            $this->setLastError('O desconto extrapalou o valor para o título:' + $arrParam['tituloId'] + '!');

            return false;
        }

        if (($informacoesExtras['tituloValor'] - (($arrParam['tituloValor'] * $arrParam['descontoPercentual'] / 100) + $descontos)) < 0) {
            $errors[] = 'O valor do desconto é maior que o valor líquido do título!';
        }

        $serviceFinanceiroDesconto = new \Financeiro\Service\FinanceiroDesconto($this->getEm());
        /** @var \Financeiro\Entity\FinanceiroDesconto $objDesconto */
        $objDesconto = $serviceFinanceiroDesconto->find($arrParam['desconto']);

        $limitaVencimento = $objDesconto->getDesctipo()->getDesctipoLimitaVencimento();
        $limitaVencimento = (
            $limitaVencimento == \Financeiro\Service\FinanceiroDescontoTipo::DESCTIPO_DESCONTO_ACUMULATIVO_SIM
        );

        $validarPelaData = (
            $arrParam['descontoTituloSituacao'] == self::DESCONTO_TITULO_SITUACAO_DEFERIDO AND
            $arrParam['descontoTituloAplicado'] == self::DESCONTO_TITULO_APLICADO_NAO AND
            $objDesconto->getDescontoStatus() == $serviceFinanceiroDesconto::DESCONTO_STATUS_ATIVO
        );

        if ($limitaVencimento && $validarPelaData) {
            $dataAtual      = date('Y-m-d');
            $dataVencimento = strtotime($this->formatDateAmericano($informacoesExtras['tituloDataVencimento']));
            $tituloAberto   = ($informacoesExtras['tituloDataVencimento'] == $serviceFinanceiroTitulo::TITULO_ESTADO_ABERTO);

            if ($dataVencimento < strtotime($dataAtual) && $tituloAberto) {
                $errors[] = 'Titulo com restrição na data de vencimento!';
            }
        }

        if ($validaDescontoAcumulativo = $this->validaDescontoAcumulativo($arrParam, $arrParam['desctipoId'])) {
            $errors = $validaDescontoAcumulativo;
        }

        if (!$arrParam['desconto']) {
            $errors[] = 'Por favor preencha o campo "código desconto"!';
        }

        if (!$arrParam['titulo']) {
            $errors[] = 'Por favor preencha o campo "código título"!';
        }

        if (
            !$arrParam['descontoTituloSituacao'] &&
            !in_array($arrParam['descontoTituloSituacao'], self::getDescontoTituloSituacao())
        ) {
            $errors[] = 'Por favor selecione um valor válido para o campo "situação"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM view__financeiro_descontos_titulos";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($descontoTituloId)
    {
        /** @var $objFinanceiroDescontoTitulo \Financeiro\Entity\FinanceiroDescontoTitulo */
        $objFinanceiroDescontoTitulo = $this->getRepository()->find($descontoTituloId);
        $serviceFinanceiroDesconto   = new \Financeiro\Service\FinanceiroDesconto($this->getEm());
        $serviceFinanceiroTitulo     = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceAcessoPessoas        = new \Acesso\Service\AcessoPessoas($this->getEm());

        try {
            $arrDados = $objFinanceiroDescontoTitulo->toArray();

            if ($arrDados['desconto']) {
                $arrFinanceiroDesconto = $serviceFinanceiroDesconto->getArrSelect2(['id' => $arrDados['desconto']]);
                $arrDados['desconto']  = $arrFinanceiroDesconto ? $arrFinanceiroDesconto[0] : null;
            }

            if ($arrDados['titulo']) {
                $arrFinanceiroTitulo = $serviceFinanceiroTitulo->getArrSelect2(['id' => $arrDados['titulo']]);
                $arrDados['titulo']  = $arrFinanceiroTitulo ? $arrFinanceiroTitulo[0] : null;
            }

            if ($arrDados['usuarioAlteracao']) {
                $arrAcessoPessoas             = $serviceAcessoPessoas->getArrSelect2(
                    ['id' => $arrDados['usuarioAlteracao']]
                );
                $arrDados['usuarioAlteracao'] = $arrAcessoPessoas ? $arrAcessoPessoas[0] : null;
            }

            if ($arrDados['usuarioCriacao']) {
                $arrAcessoPessoas           = $serviceAcessoPessoas->getArrSelect2(
                    ['id' => $arrDados['usuarioCriacao']]
                );
                $arrDados['usuarioCriacao'] = $arrAcessoPessoas ? $arrAcessoPessoas[0] : null;
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceFinanceiroDesconto = new \Financeiro\Service\FinanceiroDesconto($this->getEm());
        $serviceFinanceiroTitulo   = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceAcessoPessoas      = new \Acesso\Service\AcessoPessoas($this->getEm());

        if (is_array($arrDados['desconto']) && !$arrDados['desconto']['text']) {
            $arrDados['desconto'] = $arrDados['desconto']['descontoId'];
        }

        if ($arrDados['desconto'] && is_string($arrDados['desconto'])) {
            $arrDados['desconto'] = $serviceFinanceiroDesconto->getArrSelect2(
                array('id' => $arrDados['desconto'])
            );
            $arrDados['desconto'] = $arrDados['desconto'] ? $arrDados['desconto'][0] : null;
        }

        if (is_array($arrDados['titulo']) && !$arrDados['titulo']['text']) {
            $arrDados['titulo'] = $arrDados['titulo']['tituloId'];
        }

        if ($arrDados['titulo'] && is_string($arrDados['titulo'])) {
            $arrDados['titulo'] = $serviceFinanceiroTitulo->getArrSelect2(
                array('id' => $arrDados['titulo'])
            );
            $arrDados['titulo'] = $arrDados['titulo'] ? $arrDados['titulo'][0] : null;
        }

        if (is_array($arrDados['usuarioAlteracao']) && !$arrDados['usuarioAlteracao']['text']) {
            $arrDados['usuarioAlteracao'] = $arrDados['usuarioAlteracao']['id'];
        }

        if ($arrDados['usuarioAlteracao'] && is_string($arrDados['usuarioAlteracao'])) {
            $arrDados['usuarioAlteracao'] = $serviceAcessoPessoas->getArrSelect2(
                array('id' => $arrDados['usuarioAlteracao'])
            );
            $arrDados['usuarioAlteracao'] = $arrDados['usuarioAlteracao'] ? $arrDados['usuarioAlteracao'][0] : null;
        }

        if (is_array($arrDados['usuarioCriacao']) && !$arrDados['usuarioCriacao']['text']) {
            $arrDados['usuarioCriacao'] = $arrDados['usuarioCriacao']['id'];
        }

        if ($arrDados['usuarioCriacao'] && is_string($arrDados['usuarioCriacao'])) {
            $arrDados['usuarioCriacao'] = $serviceAcessoPessoas->getArrSelect2(
                array('id' => $arrDados['usuarioCriacao'])
            );
            $arrDados['usuarioCriacao'] = $arrDados['usuarioCriacao'] ? $arrDados['usuarioCriacao'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('descontoTituloId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Financeiro\Entity\FinanceiroDescontoTitulo */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getDescontoTituloId(),
                $params['value'] => (
                    $objEntity->getDescontoTituloId() . ' / ' .
                    $objEntity->getDesconto()->getDesctipo()->getDesctipoDescricao() . ' / ' .
                    $objEntity->getTitulo()->getTituloDescricao() . ' / ' .
                    $objEntity->getDesconto()->getAlunocurso()->getPes()->getPes()->getPesNome()
                )
            );
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['descontoTituloId']) {
            $this->setLastError('Para remover um registro de desconto título é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objFinanceiroDescontoTitulo \Financeiro\Entity\FinanceiroDescontoTitulo */
            $objFinanceiroDescontoTitulo = $this->getRepository()->find($param['descontoTituloId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objFinanceiroDescontoTitulo);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de desconto título.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        //        $serviceFinanceiroDesconto = new \Financeiro\Service\FinanceiroDesconto($this->getEm());
        //        $serviceFinanceiroTitulo   = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        //        $serviceAcessoPessoas      = new \Acesso\Service\AcessoPessoas($this->getEm());
        //
        //        $serviceFinanceiroDesconto->setarDependenciasView($view);
        //        $serviceFinanceiroTitulo->setarDependenciasView($view);
        //        $serviceAcessoPessoas->setarDependenciasView($view);

        $view->setVariable("arrDescontoTituloSituacao", $this->getArrSelect2DescontoTituloSituacao());
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrayTitulosVinculados($params)
    {
        $paramConsulta = array();

        if ($params['desconto']) {
            $paramConsulta['desconto'] = $params['desconto'];
        }

        if ($params['titulo']) {
            $paramConsulta['titulo'] = $params['titulo'];
        }

        $arrDescontoTituloRetorno = [];
        $arrDescontoTitulos       = $this->getRepository()->findBy($paramConsulta);

        /* @var $objDescontoTitulo \Financeiro\Entity\FinanceiroDescontoTitulo */
        foreach ($arrDescontoTitulos as $objDescontoTitulo) {
            $arrDescontoTitulo = $objDescontoTitulo->toArray();

            $arrDescontoTitulo['tituloId']                 = $objDescontoTitulo->getTitulo()->getTituloId();
            $arrDescontoTitulo['titulo']                   = $objDescontoTitulo->getTitulo()->getTituloId();
            $arrDescontoTitulo['tituloDescricao']          = $objDescontoTitulo->getTitulo()->getTituloDescricao();
            $arrDescontoTitulo['tituloDataVencimento']     = (
            $objDescontoTitulo->getTitulo()->getTituloDataVencimento(true)
            );
            $arrDescontoTitulo['pesNome']                  = $objDescontoTitulo->getTitulo()->getPes()->getPesNome();
            $arrDescontoTitulo['cursoSigla']               = (
            $objDescontoTitulo->getTitulo()->getAlunocurso() ?
                $objDescontoTitulo->getTitulo()->getAlunocurso()->getCursocampus()->getCurso()->getCursoNome() :
                '-'
            );
            $arrDescontoTitulo['tituloValor']              = $objDescontoTitulo->getTitulo()->getTituloValor();
            $arrDescontoTitulo['tituloEstado']             = $objDescontoTitulo->getTitulo()->getTituloEstado();
            $arrDescontoTitulo['desctipoLimitaVencimento'] = (
            $objDescontoTitulo->getDesconto()->getDesctipo()->getDesctipoLimitaVencimento()
            );
            $arrDescontoTitulo['descontoTituloValor']      = $objDescontoTitulo->getDescontoTituloValor();
            $arrDescontoTitulo['descontoTituloAplicado']   = $objDescontoTitulo->getDescontoTituloAplicado();
            $arrDescontoTitulo['descontoTituloSituacao']   = $objDescontoTitulo->getDescontoTituloSituacao();
            $arrDescontoTitulo['tituloValorFormatado']     = (
            number_format($arrDescontoTitulo['tituloValor'], 2, ',', '.')
            );

            $arrDescontoTituloRetorno[] = $arrDescontoTitulo;
        }

        return $arrDescontoTituloRetorno;
    }

    /**
     * @param int $titulo
     * @return array
     */
    public function getArrayDescontosTitulosAplicacao($titulo)
    {
        $query = '
        SELECT *
        FROM view__financeiro_descontos_titulos_atualizada
        WHERE titulo_id=:titulo';

        $arrDescontoTitulos = $this->executeQueryWithParam($query, ['titulo' => $titulo])->fetchAll();

        return $arrDescontoTitulos;
    }

    /**
     * @param int $titulo
     * @return array
     */
    public function getSomaDescontosTitulo($titulo, $descontoId = false, $tipoLimiteVencimento = false)
    {
        if (!$titulo) {
            $this->setLastError('É necessário informar um título para busca!');

            return false;
        }

        $serviceFinanceiroDescontoTipo = new \Financeiro\Service\FinanceiroDescontoTipo($this->getEm());

        $param = ['titulo' => $titulo];

        $query = "
        SELECT round(coalesce(sum(desconto_aplicacao_valor), 0),2) AS desconto, desctipo_desconto_acumulativo
        FROM view__financeiro_descontos_titulos_atualizada fdt
        WHERE fdt.titulo_id = :titulo
        ";

        if ($descontoId) {
            $param['desconto_id'] = $descontoId;
            $query .= ' AND fdt.desconto_id != :desconto_id';
        }

        if (
            $tipoLimiteVencimento &&
            in_array($tipoLimiteVencimento, $serviceFinanceiroDescontoTipo::getDesctipoLimitaVencimento())
        ) {
            $param['desctipo_limita_vencimento'] = $tipoLimiteVencimento;
            $query .= ' AND desctipo_limita_vencimento= :desctipo_limita_vencimento';
        }

        $query .= " ORDER BY desctipo_desconto_acumulativo ASC";

        $arrDesconto = $this->executeQueryWithParam($query, $param)->fetch();

        return $arrDesconto;
    }

    public function salvarMultiplos(array &$arrParam, \Financeiro\Entity\FinanceiroDesconto $objFinanceiroDesconto)
    {
        $serviceFinanceiroDesconto = new \Financeiro\Service\FinanceiroDesconto($this->getEm());
        $serviceFinanceiroTitulo   = new \Financeiro\Service\FinanceiroTitulo($this->getEm());

        $arrExcluir = array();

        try {
            $this->getEm()->beginTransaction();

            $arrTitulosVinculados = $arrParam['titulosVinculados'];

            if (!is_array($arrTitulosVinculados)) {
                $arrTitulosVinculados = json_decode($arrTitulosVinculados, true);
            }

            $arrTitulosVinculadosDB = $this->getRepository()->findBy(
                ['desconto' => $objFinanceiroDesconto->getDescontoId()]
            );

            /* @var $objFinanceiroDescontoTitulo \Financeiro\Entity\FinanceiroDescontoTitulo */
            foreach ($arrTitulosVinculadosDB as $objFinanceiroDescontoTitulo) {
                $encontrado       = false;
                $descontoTituloId = $objFinanceiroDescontoTitulo->getDescontoTituloId();

                foreach ($arrTitulosVinculados as $arrDescontoTitulo) {
                    if ($descontoTituloId == $arrDescontoTitulo['descontoTituloId']) {
                        $encontrado = true;
                        break;
                    }
                }

                if (!$encontrado) {
                    $arrExcluir[$descontoTituloId] = $objFinanceiroDescontoTitulo;
                }
            }

            if ($arrExcluir) {
                foreach ($arrExcluir as $objFinanceiroDescontoTitulo) {
                    $this->getEm()->remove($objFinanceiroDescontoTitulo);
                    $this->getEm()->flush();
                }
            }

            foreach ($arrTitulosVinculados as $arrDescontoTitulo) {
                $arrDescontoTitulo['desconto']   = $objFinanceiroDesconto->getDescontoId();
                $arrDescontoTitulo['desctipoId'] = $objFinanceiroDesconto->getDesctipo()->getDesctipoId();

                if (!$serviceFinanceiroTitulo->incluirTituloNaRemessa($arrDescontoTitulo['tituloId'])) {
                    throw new \Exception($serviceFinanceiroTitulo->getLastError());
                }

                if (!$arrDescontoTitulo['titulo'] && $arrDescontoTitulo['tituloId']) {
                    $arrDescontoTitulo['titulo'] = $arrDescontoTitulo['tituloId'];
                }

                if ($arrParam['descontoStatus'] == $serviceFinanceiroDesconto::DESCONTO_STATUS_INATIVO) {
                    $arrDescontoTitulo['descontoTituloSituacao'] = self::DESCONTO_TITULO_SITUACAO_INDEFERIDO;
                    $arrDescontoTitulo['descontoTituloAplicado'] = self::DESCONTO_TITULO_APLICADO_NAO;
                }

                if (!$this->save($arrDescontoTitulo, false)) {
                    throw new \Exception($this->getLastError());
                }
            }

            $this->getEm()->commit();

            return true;
        } catch (\Exception $e) {
            $this->setLastError(
                'Não foi possível salvar o registro de desconto de título!' . '<br>' . $e->getMessage()
            );
        }

        return false;
    }

    public function aplicarDescontosTitulo($tituloId)
    {
        try {
            $this->getEm()->beginTransaction();
            $arrTitulos = $this->getArrayDescontosTitulosAplicacao($tituloId);

            foreach ($arrTitulos as $arrTitulo) {
                /* @var $objDescontoTitulo \Financeiro\Entity\FinanceiroDescontoTitulo */
                $objDescontoTitulo = $this->getRepository()->find($arrTitulo['desconto_titulo_id']);

                if ($objDescontoTitulo) {
                    $objDescontoTitulo
                        /*
                         * VALIDAR SO DEVE SER SIM CASO O DESCONTO SEJA BAIXADO 
                         *
                         *
                         *
                         * */
                        ->setDescontoTituloAplicado(self::DESCONTO_TITULO_APLICADO_SIM)
                        ->setDescontoTituloAlteracao(new \DateTime())
                        ->setDescontoTituloValor($arrTitulo['desconto_aplicacao_valor']);

                    $this->getEm()->persist($objDescontoTitulo);
                    $this->getEm()->flush($objDescontoTitulo);
                }
            }

            $this->getEm()->commit();
        } catch (\Exception $e) {
            $this->setLastError(
                'Não foi possível aplicar descontos aos títulos!' . '<br>' . $e->getMessage()
            );

            return false;
        }

        return true;
    }

    public function cancelaDescontosTitulo(\Financeiro\Entity\FinanceiroTitulo $objTitulo)
    {
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());

        try {
            $this->getEm()->beginTransaction();
            $arrTitulosVinculados = $this->getRepository()->findBy(['titulo' => $objTitulo]);
            $objAcessoPessoas     = $serviceAcessoPessoas->retornaUsuarioLogado();

            /* @var $objDescontoTitulo \Financeiro\Entity\FinanceiroDescontoTitulo */
            foreach ($arrTitulosVinculados as $objDescontoTitulo) {
                $objDescontoTitulo
                    ->setDescontoTituloSituacao(self::DESCONTO_TITULO_SITUACAO_INDEFERIDO)
                    ->setDescontoTituloAlteracao(new \DateTime())
                    ->setUsuarioAlteracao($objAcessoPessoas);

                $this->getEm()->persist($objDescontoTitulo);
                $this->getEm()->flush($objDescontoTitulo);
            }

            $this->getEm()->commit();
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível cancelar descontos do título!' . '<br>' . $e->getMessage());

            return false;
        }

        return true;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroTitulo $objTituloNovo
     * @param int                                 $tituloId
     * @return boolean
     */
    public function copiarDescontoDeTitulo(&$objTituloNovo, $tituloId)
    {
        $arrObjDescontoTitulo = $this->getRepository()->findBy(['titulo' => $tituloId]);

        if ($arrObjDescontoTitulo) {
            try {
                /** @var \Financeiro\Entity\FinanceiroDescontoTitulo $novoDescontoTitulo */
                foreach ($arrObjDescontoTitulo as $objDescontoTitulo) {
                    $this->getEm()->beginTransaction();
                    $novoDescontoTitulo = clone($objDescontoTitulo);
                    $novoDescontoTitulo->setDescontoTituloId(null);
                    $novoDescontoTitulo->setDescontoTituloAplicado(self::DESCONTO_TITULO_APLICADO_NAO);
                    $novoDescontoTitulo->setTitulo($objTituloNovo);
                    $this->getEm()->persist($novoDescontoTitulo);
                    $this->getEm()->flush($novoDescontoTitulo);
                    $this->getEm()->commit();
                }
            } catch (\Exception $e) {
                $this->setLastError($e->getMessage());

                return false;
            }
        }

        return true;
    }

    public function retornaArrayDescontosTituloId($tituloId)
    {
        if (!$tituloId) {
            $this->setLastError("É necessário informar um título para consulta!");

            return false;
        }

        $sql = "
            SELECT
            fd.desconto_dia_limite,
            fd.desconto_id,
            fdt.titulo_id,
            fdt.desconto_titulo_valor,
            fd.desconto_dia_limite,
            fdtp.desctipo_desconto_acumulativo,
            fdtp.desctipo_limita_vencimento,
            ft.titulo_desconto AS descontoFinanceiroTitulo,
            ft.titulo_desconto_manual descontoFinanceiroTituloManual,
            fdtp.desctipo_descricao descontoDescrição,
            fdtp.desctipo_id desId,
            fd.desconto_dia_limite diaDesconto,
            fdt.desconto_titulo_aplicado,
            @descontoNaoLimitaVencimento:=format(
            IF((fdtp.desctipo_limita_vencimento='Não'),
                IF((fd.desconto_valor IS NOT NULL),
                fd.desconto_valor,
                ((fd.desconto_percentual/100)*ft.titulo_valor)),
            0),2,'de_DE'
            ) AS descontoNaoLimitaVencimento,
            @descontoLimitaVencimento:=format(
            IF((fdtp.desctipo_limita_vencimento='Sim'),
                IF((fd.desconto_valor IS NOT NULL),
                fd.desconto_valor,
                ((fd.desconto_percentual/100)*ft.titulo_valor)),
            0),2,'de_DE'
            ) AS descontoLimitaVencimento,
            IF(
                (fd.desconto_valor != 0 OR fd.desconto_valor IS NOT NULL),
                (fd.desconto_valor),
                ((fd.desconto_percentual / 100) * ft.titulo_valor)
            ) AS descontoValor,
            fdt.desconto_titulo_situacao,
            fd.desconto_status
            
            FROM financeiro__desconto_titulo fdt
            LEFT JOIN financeiro__desconto fd ON fd.desconto_id=fdt.desconto_id
            LEFT JOIN financeiro__desconto_tipo fdtp ON fdtp.desctipo_id=fd.desctipo_id
            LEFT JOIN financeiro__titulo ft ON ft.titulo_id=fdt.titulo_id
            WHERE ft.titulo_id= :tituloID AND
            (fdt.desconto_titulo_aplicado='Sim' OR fdt.desconto_titulo_situacao='Deferido') AND fd.desconto_status='Ativo'
            ORDER BY fd.desconto_dia_limite ASC, fdt.titulo_id, descontoNaoLimitaVencimento
      ";

        $result = $this->executeQueryWithParam($sql, ['tituloID' => $tituloId])->fetchAll();

        return $result;
    }

    public function validaDescontoAcumulativo($arrParam, $desctipoId)
    {
        $serviceFinanceiroDesconto     = new \Financeiro\Service\FinanceiroDesconto($this->getEm());
        $serviceFinanceiroDescontoTipo = new \Financeiro\Service\FinanceiroDescontoTipo($this->getEm());

        $errors = array();

        $titulosVinculados = array();

        if (is_array($arrParam['titulosVinculados'])) {
            $titulosVinculados = $arrParam['titulosVinculados'];
        } else {
            $titulosVinculados[] = $arrParam;
        }

        /** @var \Financeiro\Entity\FinanceiroDescontoTipo $objDescontoTipo */
        $objDescontoTipo = $serviceFinanceiroDescontoTipo->getRepository()->find($desctipoId);

        $descontoAcumulativo = (
            $objDescontoTipo->getDesctipoDescontoAcumulativo() ==
            $serviceFinanceiroDescontoTipo::DESCTIPO_DESCONTO_ACUMULATIVO_SIM
        );

        if ($arrParam['descontoStatus'] == $serviceFinanceiroDesconto::DESCONTO_STATUS_ATIVO) {
            foreach ($titulosVinculados as $titulos) {
                if ($titulos['descontoTituloSituacao'] != self::DESCONTO_TITULO_SITUACAO_INDEFERIDO) {
                    $tituloId = $titulos['tituloId'] ? $titulos['tituloId'] : $arrParam['tituloId'];

                    $arrInfoDesconto = $serviceFinanceiroDesconto->retornarInformacoesDeDescontosAtivos(
                        ['tituloId' => $tituloId, 'descontoId' => $arrParam['descontoId']]
                    );

                    if (!$descontoAcumulativo && $arrInfoDesconto) {
                        $errors[] = 'Este titulo: ' . $tituloId . ' possui outros descontos e este desconto não é acumulativo!';
                    } elseif ($descontoAcumulativo) {
                        foreach ($arrInfoDesconto as $desconto) {
                            if ($desconto['desctipo_desconto_acumulativo'] == $serviceFinanceiroDescontoTipo::DESCTIPO_DESCONTO_ACUMULATIVO_NAO) {
                                $errors[] = (
                                    'O desconto ' . $desconto['desconto_id'] .
                                    '  foi vinculado a este título, porém o desconto não é acumulativo!'
                                );
                            }
                        }
                    }
                }
            }
        }

        return $errors;
    }

    public function retornaSomaDescontosValidos($tituloId, $desc = false)
    {
        if (!$tituloId) {
            $this->setLastError('É necessário informar um título!');

            return false;
        }

        $sql = '
            SELECT ROUND(sum(coalesce(desconto_aplicacao_valor,0)),2) somaDescontos
            FROM view__financeiro_descontos_titulos_atualizada
            WHERE titulo_id=:titulo_id
        ';

        $param = [
            'titulo_id' => $tituloId
        ];

        if ($desc) {
            $sql .= ' or (desconto_id=:desconto  AND titulo_id=:titulo_id )';
            $param['desconto'] = $desc;
        }

        $result = $this->executeQueryWithParam($sql, $param)->fetch();

        return $result['somaDescontos'];
    }

    public function cancelaPagamentoPorDesconto($arrDados)
    {
        $serviceFinanceiroTitulo   = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceFinanceiroDesconto = new\Financeiro\Service\FinanceiroDesconto($this->getEm());

        $erro               = "";
        $titulosValidaAbrir = array();
        $dataAtual          = new \DateTime();

        if ($arrDados['titulosvinculados'] && is_array($arrDados['titulosvinculados'])) {
            $titulosValidaAbrir = $arrDados['titulosvinculados'];
        } else {
            $titulosValidaAbrir[] = $arrDados;
        }

        foreach ($titulosValidaAbrir as $value) {
            $tituloId = $value['tituloId'] ? $value['tituloId'] : $value['titulo'];

            /** @var \Financeiro\Entity\FinanceiroTitulo $objFinanceiroTitulo */
            $objFinanceiroTitulo = $serviceFinanceiroTitulo->getRepository()->find($tituloId);

            $abrirTitulo = (
                $arrDados['descontoStatus'] == $serviceFinanceiroDesconto::DESCONTO_STATUS_INATIVO OR
                $value['descontoTituloSituacao'] == self::DESCONTO_TITULO_SITUACAO_INDEFERIDO
            );

            $tituloPago = (
                $objFinanceiroTitulo->getTituloEstado() == $serviceFinanceiroTitulo::TITULO_ESTADO_PAGO
            );

            if ($abrirTitulo && $tituloPago) {
                $descontos                = $this->retornaSomaDescontosValidos(
                    $objFinanceiroTitulo->getTituloId(),
                    $value['desconto']
                );
                $tituloBaixadoPorDesconto = ($descontos == $objFinanceiroTitulo->getTituloValor());

                if ($tituloBaixadoPorDesconto) {
                    $arrDadosTitulo                        = $objFinanceiroTitulo->toArray();
                    $arrDadosTitulo['tituloEstado']        = $serviceFinanceiroTitulo::TITULO_ESTADO_ABERTO;
                    $arrDadosTitulo['tituloDataPagamento'] = null;
                    $arrDadosTitulo['tituloDataBaixa']     = null;
                    $arrDadosTitulo['tituloObservacoes'] .= (
                        "\nReversão de desconto na data: " . $dataAtual->format('d/m/Y H:i:s')
                    );
                    $arrDadosTitulo['usuarioBaixa'] = null;

                    $ok = $serviceFinanceiroTitulo->save($arrDadosTitulo);

                    if (!$ok) {
                        $erro .= $serviceFinanceiroTitulo->getLastError() . '<br>';
                    }
                }
            }
        }

        if ($erro) {
            $this->setLastError($erro);

            return false;
        }

        return true;
    }
}
?>