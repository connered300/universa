<?php

namespace Financeiro\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class Boleto
 * @package Financeiro\Service
 */
class Boleto extends AbstractService
{
    const BOL_ESTADO_PAGO      = 'Pago';
    const BOL_ESTADO_ABERTO    = 'Aberto';
    const BOL_ESTADO_CANCELADO = 'Cancelado';
    private $__lastError = null;

    /** @var \Sistema\Service\SisConfig */
    private $config = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Financeiro\Entity\Boleto');
        $this->setConfig(new \Sistema\Service\SisConfig($em, $arrConfig));
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @return \Sistema\Service\SisConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param \Sistema\Service\SisConfig $config
     * @return IntegracaoPagseguro
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * @param $params
     * @return array
     */
    public function pesquisaForJson($params)
    {
        $sql              = '
        SELECT
            a.bolId AS bol_id,
            a.confcont AS confcont_id,
            a.tituloId AS titulo_id,
            a.pagamentoId AS pagamento_id,
            a.bolNossoNumero AS bol_nosso_numero,
            a.bolNumeroDocumento AS bol_numero_documento,
            a.bolEstado AS bol_estado,
            a.bolValorUnitario AS bol_valor_unitario,
            a.bolQuantidade AS bol_quantidade,
            a.bolDemostrativo AS bol_demostrativo,
            a.bolInstrucoes AS bol_instrucoes,
            a.bolVia AS bol_via,
            a.bolDescontoData AS bol_desconto_data,
            a.bolDescontoValor AS bol_desconto_valor,
            a.bolMulta AS bol_multa,
            a.bolJuros AS bol_juros,
            a.bolAcrescimos AS bol_acrescimos,
            a.bolValorPago AS bol_valor_pago
        FROM Financeiro\Entity\Boleto a
        WHERE';
        $bolDescontoValor = false;
        $bolId            = false;

        if ($params['q']) {
            $bolDescontoValor = $params['q'];
        } elseif ($params['query']) {
            $bolDescontoValor = $params['query'];
        }

        if ($params['bolId']) {
            $bolId = $params['bolId'];
        }

        $parameters = array('bolDescontoValor' => "{$bolDescontoValor}%");
        $sql .= ' a.bolDescontoValor LIKE :bolDescontoValor';

        if ($bolId) {
            $parameters['bolId'] = explode(',', $bolId);
            $parameters['bolId'] = $bolId;
            $sql .= ' AND a.bolId NOT IN(:bolId)';
        }

        $sql .= " ORDER BY a.bolDescontoValor";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceBoletoConfConta     = new \Financeiro\Service\BoletoConfConta($this->getEm());
        $serviceFinanceiroTitulo    = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceFinanceiroPagamento = new \Financeiro\Service\FinanceiroPagamento($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['bolId']) {
                /** @var $objBoleto \Financeiro\Entity\Boleto */
                $objBoleto = $this->getRepository()->find($arrDados['bolId']);

                if (!$objBoleto) {
                    $this->setLastError('Registro de boleto não existe!');

                    return false;
                }
            } else {
                $objBoleto = new \Financeiro\Entity\Boleto();
            }

            if ($arrDados['confcont']) {
                /** @var $objBoletoConfConta \Financeiro\Entity\BoletoConfConta */
                $objBoletoConfConta = $serviceBoletoConfConta->getRepository()->find($arrDados['confcont']);

                if (!$objBoletoConfConta) {
                    $this->setLastError('Registro de configuração conta não existe!');

                    return false;
                }

                $objBoleto->setConfcont($objBoletoConfConta);
            } else {
                $objBoleto->setConfcont(null);
            }

            if ($arrDados['titulo']) {
                /** @var $objFinanceiroTitulo \Financeiro\Entity\FinanceiroTitulo */
                $objFinanceiroTitulo = $serviceFinanceiroTitulo->getRepository()->find($arrDados['titulo']);

                if (!$objFinanceiroTitulo) {
                    $this->setLastError('Registro de título não existe!');

                    return false;
                }

                $objBoleto->setTitulo($objFinanceiroTitulo);
            } else {
                $objBoleto->setTitulo(null);
            }

            if ($arrDados['pagamento']) {
                /** @var $objFinanceiroPagamento \Financeiro\Entity\FinanceiroPagamento */
                $objFinanceiroPagamento = $serviceFinanceiroPagamento->getRepository()->find($arrDados['pagamento']);

                if (!$objFinanceiroPagamento) {
                    $this->setLastError('Registro de pagamento não existe!');

                    return false;
                }

                $objBoleto->setPagamento($objFinanceiroPagamento);
            } else {
                $objBoleto->setPagamento(null);
            }

            $objBoleto->setBolNossoNumero($arrDados['bolNossoNumero']);
            $objBoleto->setBolNumeroDocumento($arrDados['bolNumeroDocumento']);
            $objBoleto->setBolCarteira($arrDados['bolCarteira']);
            $objBoleto->setBolEstado($arrDados['bolEstado']);
            $objBoleto->setBolValorUnitario($arrDados['bolValorUnitario']);
            $objBoleto->setBolQuantidade($arrDados['bolQuantidade']);
            $objBoleto->setBolDemostrativo($arrDados['bolDemostrativo']);
            $objBoleto->setBolInstrucoes($arrDados['bolInstrucoes']);
            $objBoleto->setBolVia($arrDados['bolVia']);
            $objBoleto->setBolDescontoData($arrDados['bolDescontoData']);
            $objBoleto->setBolDescontoValor($arrDados['bolDescontoValor']);
            $objBoleto->setBolMulta($arrDados['bolMulta']);
            $objBoleto->setBolJuros($arrDados['bolJuros']);
            $objBoleto->setBolAcrescimos($arrDados['bolAcrescimos']);
            $objBoleto->setBolValorPago($arrDados['bolValorPago']);

            $this->getEm()->persist($objBoleto);
            $this->getEm()->flush($objBoleto);

            $this->getEm()->commit();

            $arrDados['bolId'] = $objBoleto->getBolId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de boleto!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['confcont']) {
            $errors[] = 'Por favor preencha o campo "configuração de conta"!';
        }

        if (!$arrParam['bolNossoNumero']) {
            $errors[] = 'Por favor preencha o campo "número nosso"!';
        }

        if (!$arrParam['bolNumeroDocumento']) {
            $errors[] = 'Por favor preencha o campo "documento número"!';
        }

        if (!$arrParam['bolEstado']) {
            $errors[] = 'Por favor preencha o campo "estado"!';
        }

        if (!in_array($arrParam['bolEstado'], self::getBolEstado())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "estado"!';
        }

        if (!$arrParam['bolVia']) {
            $errors[] = 'Por favor preencha o campo "via"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public static function getBolEstado()
    {
        return array(self::BOL_ESTADO_PAGO, self::BOL_ESTADO_ABERTO, self::BOL_ESTADO_CANCELADO);
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM boleto";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param int $tituloId
     * @return array
     */
    public function getArrayBoletosTitulos($tituloId)
    {
        $query = "
        SELECT
        b.*,
        (SELECT count(bol_id)>0 FROM financeiro__remessa_lote WHERE bol_id=b.bol_id) AS esta_na_remessa
        FROM boleto b
        WHERE titulo_id=:titulo
        GROUP BY bol_id";

        $arrBoletos = $this->executeQueryWithParam($query, ['titulo' => $tituloId])->fetchAll();

        return $arrBoletos;
    }

    /**
     * @param $bolId
     * @return array
     */
    public function getArray($bolId)
    {
        /** @var $objBoleto \Financeiro\Entity\Boleto */
        $objBoleto                  = $this->getRepository()->find($bolId);
        $serviceBoletoConfConta     = new \Financeiro\Service\BoletoConfConta($this->getEm());
        $serviceFinanceiroTitulo    = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceFinanceiroPagamento = new \Financeiro\Service\FinanceiroPagamento($this->getEm());

        try {
            $arrDados = $objBoleto->toArray();

            if ($arrDados['confcont']) {
                /** @var $objBoletoConfConta \Financeiro\Entity\BoletoConfConta */
                $objBoletoConfConta = $serviceBoletoConfConta->getRepository()->find($arrDados['confcont']);

                if ($objBoletoConfConta) {
                    $arrDados['confcont'] = array(
                        'id'   => $objBoletoConfConta->getConfcontId(),
                        'text' => $objBoletoConfConta->getConfcontId()
                    );
                } else {
                    $arrDados['confcont'] = null;
                }
            }

            if ($arrDados['titulo']) {
                /** @var $objFinanceiroTitulo \Financeiro\Entity\FinanceiroTitulo */
                $objFinanceiroTitulo = $serviceFinanceiroTitulo->getRepository()->find($arrDados['titulo']);

                if ($objFinanceiroTitulo) {
                    $arrDados['titulo'] = array(
                        'id'   => $objFinanceiroTitulo->getTituloId(),
                        'text' => $objFinanceiroTitulo->getTituloId()
                    );
                } else {
                    $arrDados['titulo'] = null;
                }
            }

            if ($arrDados['pagamento']) {
                /** @var $objFinanceiroPagamento \Financeiro\Entity\FinanceiroPagamento */
                $objFinanceiroPagamento = $serviceFinanceiroPagamento->getRepository()->find($arrDados['pagamento']);

                if ($objFinanceiroPagamento) {
                    $arrDados['pagamento'] = array(
                        'id'   => $objFinanceiroPagamento->getPagamentoId(),
                        'text' => $objFinanceiroPagamento->getPagamentoId()
                    );
                } else {
                    $arrDados['pagamento'] = null;
                }
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceBoletoConfConta     = new \Financeiro\Service\BoletoConfConta($this->getEm());
        $serviceFinanceiroTitulo    = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceFinanceiroPagamento = new \Financeiro\Service\FinanceiroPagamento($this->getEm());

        if (is_array($arrDados['confcont']) && !$arrDados['confcont']['id']) {
            $arrDados['confcont']['id']   = $arrDados['confcont']['confcontId'];
            $arrDados['confcont']['text'] = $arrDados['confcont']['confcontId'];
        } elseif ($arrDados['confcont'] && is_string($arrDados['confcont'])) {
            $arrDados['confcont'] = $serviceBoletoConfConta->getArrSelect2(
                array('id' => $arrDados['confcont'])
            );
        }

        if (is_array($arrDados['titulo']) && !$arrDados['titulo']['id']) {
            $arrDados['titulo']['id']   = $arrDados['titulo']['tituloId'];
            $arrDados['titulo']['text'] = $arrDados['titulo']['tituloId'];
        } elseif ($arrDados['titulo'] && is_string($arrDados['titulo'])) {
            $arrDados['titulo'] = $serviceFinanceiroTitulo->getArrSelect2(
                array('id' => $arrDados['titulo'])
            );
        }

        if (is_array($arrDados['pagamento']) && !$arrDados['pagamento']['id']) {
            $arrDados['pagamento']['id']   = $arrDados['pagamento']['pagamentoId'];
            $arrDados['pagamento']['text'] = $arrDados['pagamento']['pagamentoId'];
        } elseif ($arrDados['pagamento'] && is_string($arrDados['pagamento'])) {
            $arrDados['pagamento'] = $serviceFinanceiroPagamento->getArrSelect2(
                array('id' => $arrDados['pagamento'])
            );
        }

        return $arrDados;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('bolId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Financeiro\Entity\Boleto */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getBolId(),
                $params['value'] => $objEntity->getBolDescontoValor()
            );
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['bolId']) {
            $this->setLastError('Para remover um registro de boleto é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objBoleto \Financeiro\Entity\Boleto */
            $objBoleto = $this->getRepository()->find($param['bolId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objBoleto);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de boleto.');

            return false;
        }

        return true;
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        //$serviceBoletoConfConta = new \Financeiro\Service\BoletoConfConta($this->getEm());
        //$serviceFinanceiroTitulo    = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        //$serviceFinanceiroPagamento = new \Financeiro\Service\FinanceiroPagamento($this->getEm());
        //
        //$serviceBoletoConfConta->setarDependenciasView($view);
        //$serviceFinanceiroTitulo->setarDependenciasView($view);
        //$serviceFinanceiroPagamento->setarDependenciasView($view);

        $view->setVariable("arrBolEstado", $this->getArrSelect2BolEstado());
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2BolEstado($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getBolEstado());
    }

    public function formatarInstrucaoBoleto($instrucao = '', $variaveis = array())
    {
        $prazo       = $this->getConfig()->localizarChave('PRAZO_MAXIMO_DIAS_BAIXA_REMESSA', 60);
        $multa       = $variaveis['multa'] ? $variaveis['multa'] : 0;
        $juros       = $variaveis['juros'] ? $variaveis['juros'] : 0;
        $tituloId    = $variaveis['tituloId'] ? $variaveis['tituloId'] : 0;
        $tituloValor = $variaveis['tituloValor'] ? $variaveis['tituloValor'] : 0;

        if (!$instrucao) {
            $instrucao = 'SR. CAIXA NÃO RECEBER APÓS O DIA DO VENCIMENTO.';

            if ($multa || $juros || $prazo) {
                $instrucao = 'SR. CAIXA,';
            }
        }

        $instrucao = str_replace('$multa', $multa, $instrucao);
        $instrucao = str_replace('$juros', $juros, $instrucao);
        $instrucao = str_replace('$tituloId', $tituloId, $instrucao);
        $instrucao = str_replace('$tituloValor', number_format($tituloValor, 2, ',', '.'), $instrucao);

        if ($variaveis['valorDescontoAntecipado1']) {
            $valorDescontoAntecipado1 = $variaveis['valorDescontoAntecipado1'] ? $variaveis['valorDescontoAntecipado1']
                : 0;
            $instrucao .= PHP_EOL . 'DESCONTO DE $valorDescontoAntecipado1 ATÉ $dataDescontoAntecipado1.';
            $instrucao = str_replace(
                '$valorDescontoAntecipado1',
                number_format($valorDescontoAntecipado1, 2, ',', '.'),
                $instrucao
            );
            $instrucao = str_replace(
                '$dataDescontoAntecipado1',
                $variaveis['dataDescontoAntecipado1'],
                $instrucao
            );
        }

        if ($variaveis['valorDescontoAntecipado2']) {
            $valorDescontoAntecipado2 = $variaveis['valorDescontoAntecipado2'] ? $variaveis['valorDescontoAntecipado2']
                : 0;
            $instrucao .= PHP_EOL . 'DESCONTO DE $valorDescontoAntecipado2 ATÉ $dataDescontoAntecipado2.';
            $instrucao = str_replace(
                '$valorDescontoAntecipado2',
                number_format($valorDescontoAntecipado2, 2, ',', '.'),
                $instrucao
            );
            $instrucao = str_replace(
                '$dataDescontoAntecipado2',
                $variaveis['dataDescontoAntecipado2'],
                $instrucao
            );
        }

        return $instrucao;
    }

    public function registrarBoletoParaTitulo(\Financeiro\Entity\FinanceiroTitulo $objTitulo)
    {
        $arrParamPesquisaConfig = array(
            'tipotituloId' => $objTitulo->getTipotitulo()->getTipotituloId(),
        );

        if ($objTitulo->getAlunocurso()) {
            $cursocampusId = $objTitulo->getAlunocurso()->getCursocampus();
            $cursocampusId = is_object($cursocampusId) ? $cursocampusId->getCursocampusId() : $cursocampusId;

            $arrParamPesquisaConfig['cursocampusId'] = $cursocampusId;
        }

        //Pesquisa configuração de boleto baseado no campus/curso e tipo de título
        /** @var \Financeiro\Entity\FinanceiroTituloConfig $objFinanceiroTituloConfig */
        $objFinanceiroTituloConfig = $objTitulo->getTituloconf();

        if (!$objFinanceiroTituloConfig) {
            $this->setLastError("Não foi possível instanciar objFinanceiroTituloConfig");

            return false;
        }

        $confcontId = $objFinanceiroTituloConfig->getConfcont()->getConfcontId();

        $multa       = $objFinanceiroTituloConfig->getTituloconfMulta() * $objTitulo->getTituloValor();
        $juros       = $objFinanceiroTituloConfig->getTituloconfJuros();
        $tituloId    = $objTitulo->getTituloId();
        $tituloValor = $objTitulo->getTituloValor();

        $instrucao = $objFinanceiroTituloConfig->getConfcont()->getConfcontInstrucoes();

        $instrucao = $this->formatarInstrucaoBoleto(
            $instrucao,
            ['multa' => $multa, 'juros' => $juros, 'tituloId' => $tituloId, 'tituloValor' => $tituloValor]
        );

        $arrDados = array(
            'confcont'           => $confcontId,
            'bolCarteira'        => $objFinanceiroTituloConfig->getConfcont()->getConfcontCarteira(),
            'titulo'             => $tituloId,
            'bolNumeroDocumento' => $tituloId,
            'bolDemostrativo'    => $objTitulo->getTituloDescricao(),
            'bolEstado'          => self::BOL_ESTADO_ABERTO,
            'bolValorUnitario'   => $tituloValor,
            'bolQuantidade'      => 1,
            'bolInstrucoes'      => $instrucao,
            'bolVia'             => 1,
            'bolDescontoData'    => null,
            'bolDescontoValor'   => null,
            'bolMulta'           => $multa,
            'bolJuros'           => $juros,
        );

        $query = '
        INSERT INTO boleto
        (
            bol_id, confcont_id, titulo_id,
            bol_nosso_numero, bol_carteira,
            bol_numero_documento, bol_estado, bol_valor_unitario,
            bol_quantidade, bol_demostrativo, bol_instrucoes,
            bol_via, bol_desconto_data, bol_desconto_valor,
            bol_multa, bol_juros
        )
        VALUES
        (NULL, :confcont, :titulo,
            (
                SELECT (COALESCE(confcont_nosso_numero_inicial,0) + :titulo) bol_nosso_numero
                FROM boleto_conf_conta
                WHERE confcont_id = ' . $confcontId . '
            ), :bolCarteira,
            :bolNumeroDocumento, :bolEstado, :bolValorUnitario,
            :bolQuantidade, :bolDemostrativo, :bolInstrucoes,
            :bolVia, :bolDescontoData, :bolDescontoValor,
            :bolMulta, :bolJuros)';
        $ok    = $this->executeQueryWithParam($query, $arrDados);

        if (!$ok) {
            return false;
        }

        return true;
    }

    public function retornaNossoNumero($confContId)
    {
        $serviceConfConta = new \Financeiro\Service\BoletoConfConta($this->getEm());

        /** @var \Financeiro\Entity\BoletoConfConta $objConfCont */
        $objConfCont = $serviceConfConta->getRepository()->find($confContId);

        $query = "
        SELECT
            MAX(bol_nosso_numero) bol_nosso_numero,
            MAX(bol_numero_documento) bol_numero_documento
        FROM boleto
        WHERE confcont_id = :confcont_id";

        $arrUltimoBoleto = $this
            ->executeQueryWithParam($query, array('confcont_id' => $confContId))
            ->fetch();

        $bolNossoNumero = 1;

        if (!empty($arrUltimoBoleto['bol_nosso_numero'])) {
            $bolNossoNumero = $arrUltimoBoleto['bol_nosso_numero'] + 1;
        } elseif ($objConfCont->getConfcontNossoNumeroInicial()) {
            $bolNossoNumero = $objConfCont->getConfcontNossoNumeroInicial();
        }

        if ($bolNossoNumero < $objConfCont->getConfcontNossoNumeroInicial()) {
            $bolNossoNumero = $objConfCont->getConfcontNossoNumeroInicial();
        }

        return $bolNossoNumero;
    }

    /**
     * @param $pagamento
     * @return bool
     */
    public function removeReferenciaPagamentoBoleto($pagamento)
    {
        if (!$pagamento) {
            $this->setLastError(
                'Para remover um vínculos de pagamento dos boletos é necessário informar o código do pagamento.'
            );

            return false;
        }

        try {
            $this->getEm()->beginTransaction();
            $arrBoleto = $this->getRepository()->findBy(['pagamento' => $pagamento]);

            /** @var $objBoleto \Financeiro\Entity\Boleto */
            foreach ($arrBoleto as $objBoleto) {
                if ($objBoleto->getTitulo()) {
                    $objBoleto->setPagamento(null);
                    $this->getEm()->persist($objBoleto);
                    $this->getEm()->flush($objBoleto);
                } else {
                    $this->getEm()->remove($objBoleto);
                    $this->getEm()->flush();
                }
            }
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover vínculos de pagamentos de boletos.<br>' . $ex->getMessage());

            return false;
        }

        return true;
    }

    public function buscaBoleto($bolId = null)
    {
        if (!$bolId) {
            return false;
        }

        $query = "
        SELECT
            boleto.*,
            boleto_banco.*,
            financeiro__titulo.*,
            boleto_conf_conta.*,
            cedente.pes_nome AS cedente_nome,
            cedente_fisica.pes_cpf AS cedente_cpf,
            cedente_jurica.pes_cnpj AS cedente_cnpj,
            sacado.pes_nome AS sacado_nome,
            sacado_fisica.pes_cpf AS sacado_cpf,
            sacado_juridica.pes_cnpj AS sacaca_cnpj,
            endereco.*,
            tipo_endereco.*
        FROM boleto
            INNER JOIN boleto_conf_conta
                ON boleto_conf_conta.confcont_id = boleto.confcont_id
            INNER JOIN boleto_banco
                ON boleto_banco.banc_id = boleto_conf_conta.banc_id
            INNER JOIN financeiro__titulo
                ON boleto.titulo_id = financeiro__titulo.titulo_id
            INNER JOIN pessoa AS cedente
                ON cedente.pes_id = boleto_conf_conta.pes_id
            LEFT JOIN pessoa_juridica AS cedente_jurica
                ON cedente_jurica.pes_id = boleto_conf_conta.pes_id
            LEFT JOIN pessoa_fisica AS cedente_fisica
                ON cedente_fisica.pes_id = boleto_conf_conta.pes_id
            INNER JOIN pessoa AS sacado
                ON sacado.pes_id = financeiro__titulo.pes_id
            LEFT JOIN pessoa_juridica AS sacado_juridica
                ON sacado_juridica.pes_id = financeiro__titulo.pes_id
            LEFT JOIN pessoa_fisica AS sacado_fisica
                ON  sacado_fisica.pes_id = financeiro__titulo.pes_id
            LEFT JOIN endereco
                ON sacado.pes_id = endereco.pes_id
            LEFT JOIN tipo_endereco
                ON endereco.tipo_endereco = tipo_endereco.tipo_endereco_id AND tipo_endereco.nome = 'Comercial'
        WHERE boleto.bol_id = :bol_id";

        $sql   = $this->executeQueryWithParam($query, ['bol_id' => $bolId]);
        $dados = $sql->fetch();

        return $dados;
    }

    public function formatarDadosBoletoImpressao(\Financeiro\Entity\Boleto $objBoleto)
    {
        $objAcadgeralAlunoCurso = $objBoleto->getTitulo()->getAlunocurso();

        return array(
            'sacado'       => $this->formatarDadosBoletoImpressaoSacado($objBoleto),
            'cedente'      => $this->formatarDadosBoletoImpressaoCedente($objBoleto),
            'boleto'       => $this->formatarDadosBoletoImpressaoBoleto($objBoleto),
            'alunocursoId' => ($objAcadgeralAlunoCurso ? $objAcadgeralAlunoCurso->getAlunocursoId() : '')
        );
    }

    public function formatarDadosBoletoImpressaoBoleto(\Financeiro\Entity\Boleto $objBoleto)
    {
        $serviceFinanceiroTitulo         = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceFinanceiroDescontoTitulo = new \Financeiro\Service\FinanceiroDescontoTitulo($this->getEm());
        $serviceDescontoTipo             = new \Financeiro\Service\FinanceiroDescontoTipo($this->getEm());
        $serviceFinanceiroTituloConfig   = new \Financeiro\Service\FinanceiroTituloConfig($this->getEm());
        $serviceSisConfig                = new \Sistema\Service\SisConfig($this->getEm());

        $tituloId                  = $objBoleto->getTitulo()->getTituloId();
        $objAcadgeralAlunoCurso    = $objBoleto->getTitulo()->getAlunocurso();
        $objFinanceiroTituloConfig = $objBoleto->getTitulo()->getTituloconf();

        $tituloconfdiaDesc  = $objFinanceiroTituloConfig->getTituloconfDiaDesc();
        $tituloconfdiaDesc2 = $objFinanceiroTituloConfig->getTituloconfDiaDesc2();

        $tituloconfDiaJuros           = $objFinanceiroTituloConfig->getTituloconfDiaJuros();
        $tituloconfJuros              = $objFinanceiroTituloConfig->getTituloconfJuros();
        $tituloconfMulta              = $objFinanceiroTituloConfig->getTituloconfMulta();
        $instrucao                    = $objBoleto->getBolInstrucoes();
        $dataVencimentoTitulo         = $objBoleto->getTitulo()->getTituloDataVencimento();
        $descontosNaoLimitaVencimento = 0;
        $arrDescontosLimitados        = array();
        $msgIntrucoes                 = '';

        $titutloconfInstrucaoAutomatica = (
            $objFinanceiroTituloConfig->getTitutloconfInstrucaoAutomatica() ==
            $serviceFinanceiroTituloConfig::TITULO_CONFIG_INSTRUCAO_AUTOMATICA_SIM
        );
        $arrDadosAtualizadosTitulo      = $serviceFinanceiroTitulo->retornaInformacoesTitulo($tituloId);

        $tituloValorComDesconto = (
            $arrDadosAtualizadosTitulo['tituloValor'] -
            ($arrDadosAtualizadosTitulo['descontosNaoLimitadoAoVencimento']
                ? $arrDadosAtualizadosTitulo['descontosNaoLimitadoAoVencimento'] : 0)
        );

        $ultimoDiaVencimento = '';

        if ($arrDadosAtualizadosTitulo['valorDescontoAntecipado1']) {
            $dataDescontoAntecipado1 = clone($dataVencimentoTitulo);
            $dataDescontoAntecipado1 = $this->retornaDataDesconto($tituloconfdiaDesc, $dataDescontoAntecipado1);

            $arrDescontosLimitados[] = [
                'diaVencimentoCobrar' => $dataDescontoAntecipado1->format('Ymd'),
                'descontoValor'       => $arrDadosAtualizadosTitulo['valorDescontoAntecipado1'],
                'dataDesconto'        => $dataDescontoAntecipado1->format('d/m/Y'),
                'descontoIncentivo'   => true
            ];
        }

        if ($arrDadosAtualizadosTitulo['valorDescontoAntecipado2']) {
            $dataDescontoAntecipado2 = clone($dataVencimentoTitulo);
            $dataDescontoAntecipado2 = $this->retornaDataDesconto($tituloconfdiaDesc2, $dataDescontoAntecipado2);

            $arrDescontosLimitados[] = [
                'diaVencimentoCobrar' => $dataDescontoAntecipado2->format('Ymd') . '.0',
                'descontoValor'       => $arrDadosAtualizadosTitulo['valorDescontoAntecipado2'],
                'dataDesconto'        => $dataDescontoAntecipado2->format('d/m/Y'),
                'descontoIncentivo'   => true
            ];
        }

        if ($titutloconfInstrucaoAutomatica) {
            $arrDescontos = $serviceFinanceiroDescontoTitulo->retornaArrayDescontosTituloId($tituloId);

            foreach ($arrDescontos as $arrDesconto) {
                if ($arrDesconto['desctipo_limita_vencimento'] == $serviceDescontoTipo::DESCTIPO_LIMITA_VENCIMENTO_SIM) {
                    $arrDesconto['dataDesconto']        = $dataVencimentoTitulo->format('d/m/Y');
                    $arrDesconto['diaVencimentoCobrar'] = $dataVencimentoTitulo->format('Ymd') . '.1';
                    $arrDescontosLimitados[]            = $arrDesconto;
                } else {
                    $descontosNaoLimitaVencimento += floatval($arrDesconto['descontoValor']);
                }
            }

            usort(
                $arrDescontosLimitados,
                function ($a, $b) {
                    return $a['diaVencimentoCobrar'] > $b['diaVencimentoCobrar'];
                }
            );

            foreach ($arrDescontosLimitados as $posicaoDesconto => $desconto) {
                $descontoLimitaVencimento = $desconto['descontoValor'];
                $posicaoProximoDesconto   = $posicaoDesconto + 1;

                while ($posicaoProximoDesconto < count($arrDescontosLimitados)) {
                    $proximoDesconto = $arrDescontosLimitados[$posicaoProximoDesconto];

                    if ($desconto['diaVencimentoCobrar'] !== false && $desconto['diaVencimentoCobrar'] <= $proximoDesconto['diaVencimentoCobrar']) {
                        if (!$proximoDesconto['descontoIncentivo']) {
                            $descontoLimitaVencimento += $proximoDesconto['descontoValor'];
                        }
                    }

                    if (round($desconto['diaVencimentoCobrar']) == round($proximoDesconto['diaVencimentoCobrar'])) {
                        $arrDescontosLimitados[$posicaoProximoDesconto]['diaVencimentoCobrar'] = false;
                    }

                    $posicaoProximoDesconto++;
                }

                if ($arrDescontosLimitados[$posicaoDesconto]['diaVencimentoCobrar']) {
                    $ultimoDiaVencimento = $desconto['dataDesconto'];

                    $descontoLimitaVencimento += $descontosNaoLimitaVencimento;

                    $dataVencimento = $desconto['dataDesconto'];

                    $valorDesconto = (
                        $objBoleto->getTitulo()->getTituloValor() - $descontoLimitaVencimento
                    );
                    $msgIntrucoes .= (
                        'RECEBER R$ ' . number_format($valorDesconto, 2, ',', '.') . ' ATÉ ' . $dataVencimento . ";\n"
                    );
                }
            }
        }

        if ($ultimoDiaVencimento && $titutloconfInstrucaoAutomatica) {
            $valorDesconto = ($objBoleto->getTitulo()->getTituloValor() - $descontosNaoLimitaVencimento);
            $msgIntrucoes .= "\nAPÓS " . $ultimoDiaVencimento . ' RECEBER R$ ' .
                number_format($valorDesconto, 2, ',', '.') . ";";
        } elseif ($descontosNaoLimitaVencimento) {
            $valorDesconto = ($objBoleto->getTitulo()->getTituloValor() - $descontosNaoLimitaVencimento);
            $msgIntrucoes .= ("\nATENÇÃO: RECEBER R$ " . number_format($valorDesconto, 2, ',', '.'));
        }

        if ($msgIntrucoes) {
            $instrucao = $instrucao ? "\n" . $msgIntrucoes : $msgIntrucoes;
            $instrucao = preg_replace("/\n+/", "\n", $instrucao);
        }

        if (!$instrucao && $titutloconfInstrucaoAutomatica) {
            $arrInstrucao = [
                'multa'       => number_format($objBoleto->getBolMulta(), 2),
                'juros'       => number_format($objBoleto->getBolJuros(), 2),
                'tituloId'    => $objBoleto->getBolNumeroDocumento(),
                'tituloValor' => number_format($tituloValorComDesconto, 2, ',', '.')
            ];

            if ($arrDadosAtualizadosTitulo['valorDescontoAntecipado']) {
                $arrInstrucao['dataDescontoAntecipado']  = $arrDadosAtualizadosTitulo['dataDescontoAntecipado'];
                $arrInstrucao['valorDescontoAntecipado'] = $arrDadosAtualizadosTitulo['valorDescontoAntecipado'];
            }

            if ($arrDadosAtualizadosTitulo['valorDescontoAntecipado2']) {
                $arrInstrucao['dataDescontoAntecipado2']  = $arrDadosAtualizadosTitulo['dataDescontoAntecipado2'];
                $arrInstrucao['valorDescontoAntecipado2'] = $arrDadosAtualizadosTitulo['valorDescontoAntecipado2'];
            }

            $instrucao = $this->formatarInstrucaoBoleto($instrucao, $arrInstrucao);
        }

        /* Tratamento para adicionar informações no campo de instrução para calculo de Juros*/
        if ($titutloconfInstrucaoAutomatica && ($tituloconfMulta > 0 || $tituloconfJuros > 0)) {
            $mensagem = '';

            if ($tituloconfMulta) {
                $mensagem .= 'COBRAR MULTA DE ' .
                    number_format($tituloconfMulta * 100, 2, ",", "") . '%  ' . ' AO MÊS ';
            }

            if ($tituloconfJuros) {
                $mensagem .= ' COBRAR JUROS DE ' . number_format($tituloconfJuros * 100, 4, ",", "") . '% AO DIA';
            }

            $date = clone($dataVencimentoTitulo);

            if ($tituloconfDiaJuros) {
                $date = $date->modify('+' . $tituloconfDiaJuros . ' days');
            }

            $instrucao .= '<br>' . $mensagem . ' APÓS ' . $date->format('d/m/Y') . ";";
        }

        //$demonstrativo = $objBoleto->getBolDemostrativo();
        $demonstrativo = $objBoleto->getTitulo()->getTipotitulo()->getTipotituloNome();
        $demonstrativo .= " - Parcela: " . $objBoleto->getTitulo()->getTituloParcela();

        if ($objAcadgeralAlunoCurso) {
            $demonstrativo .= "\n Curso: " .
                $objAcadgeralAlunoCurso->getCursocampus()->getCurso()->getCursoNome();
        }

        $instrucao = str_replace("<br>", "\n", trim(trim($instrucao), '|'));
        $instrucao = preg_replace("/\n+/", "\n", trim($instrucao));
        $instrucao = explode("\n", trim($instrucao));

        if (count($instrucao) > 4) {
            $instrucao[3] .= $instrucao[4];
        }

        $demonstrativo = explode("\n", trim($demonstrativo));

        return array(
            'codigoDeBarras'        => '',
            'dataDocumento'         => $objBoleto->getTitulo()->getTituloDataProcessamento()->format("d/m/Y"),
            'dataProcessamento'     => $objBoleto->getTitulo()->getTituloDataProcessamento()->format("d/m/Y"),
            'dataVencimento'        => $objBoleto->getTitulo()->getTituloDataVencimento()->format("d/m/Y"),
            'demonstrativo1'        => $demonstrativo[0] ? $demonstrativo[0] : '',
            'demonstrativo2'        => $demonstrativo[1] ? $demonstrativo[1] : '',
            'demonstrativo3'        => $demonstrativo[2] ? $demonstrativo[2] : '',
            'formatacaoNossoNumero' => $objBoleto->getBolNossoNumero(),
            'instrucoes1'           => $instrucao[0] ? $instrucao[0] : '',
            'instrucoes2'           => $instrucao[1] ? $instrucao[1] : '',
            'instrucoes3'           => $instrucao[2] ? $instrucao[2] : '',
            'instrucoes4'           => $instrucao[3] ? $instrucao[3] : '',
            'nossoNumero'           => $objBoleto->getBolNossoNumero(),
            'nossoNumeroFormatado'  => '',
            'numeroDocumento'       => $tituloId,
            'quantidade'            => '1',
            'parcela'               => $objBoleto->getTitulo()->getTituloParcela(),
            'taxaBoleto'            => '',
            'especieDoc'            => \Cnab\Especie::siglaFebraban(\Cnab\Especie::FEBRABAN_DUPLICATA_DE_SERVICO),
            // Pegar valor com descontos
            'valor'                 => (
            $titutloconfInstrucaoAutomatica
                ?
                (number_format($tituloValorComDesconto, 2, '', ''))
                :
                (number_format($arrDadosAtualizadosTitulo['tituloValorFinal'], 2, '', ''))
            ),
            'formatacaoConvenio'    => '7',//Formatação BB
            'aceite'                => 'A',
            'valorUnitario'         => '',
            'codigoBanco'           => $objBoleto->getConfcont()->getBanc()->getBancCodigo(),
            'carteira'              => $objBoleto->getBolCarteira() ? $objBoleto->getBolCarteira()
                : $objBoleto->getConfcont()->getConfcontCarteira(),
            'alunocursoId'          => $objAcadgeralAlunoCurso ? $objAcadgeralAlunoCurso->getAlunocursoId() : '',
        );
    }

    public function formatarDadosBoletoImpressaoCedente(\Financeiro\Entity\Boleto $objBoleto)
    {
        $servicePessoa = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceCampus = new \Organizacao\Service\OrgCampus($this->getEm());

        $arrInfoIes = $serviceCampus->retornaDadosInstituicao();
        $logo       = $arrInfoIes['logo'];
        $logo       = str_replace(DIRECTORY_SEPARATOR, '/', $logo);
        $logo       = explode('/public', $logo);
        $logo       = $logo[1] ? $logo[1] : '';

        $arrCedente             = $servicePessoa->getArray($objBoleto->getConfcont()->getPes()->getPesId());
        $enderecoCedente        = $servicePessoa->formatarEndereco($arrCedente);
        $objAcadgeralAlunoCurso = $objBoleto->getTitulo()->getAlunocurso();

        $enderecoFormatado = implode(
            ', ',
            [
                $arrCedente['endLogradouro'],
                $arrCedente['endNumero'],
                $arrCedente['endBairro']
                ,
                $arrCedente['endCidade']
            ]
        );

        /*
         * TODO: analisar se os campos agenciaCodigo, carteiraDescricao, codigocliente e pontodevenda
         * são necessários para o banco do brasil)
         */

        $arrRetornoCedente = array(
            'agencia'            => $objBoleto->getConfcont()->getConfcontAgencia(),
            'agenciaCodigo'      => '',
            'agenciaDv'          => $objBoleto->getConfcont()->getConfcontAgenciaDigito(),
            'carteira'           => $objBoleto->getBolCarteira() ? $objBoleto->getBolCarteira()
                : $objBoleto->getConfcont()->getConfcontCarteira(),
            'carteiraDescricao'  => '',
            'cidade'             => $arrCedente['endCidade'],
            'codigocliente'      => '',
            'contaCorrente'      => $objBoleto->getConfcont()->getConfcontConta(),
            'contaCorrenteDv'    => $objBoleto->getConfcont()->getConfcontContaDigito(),
            'contaCedente'       => $objBoleto->getConfcont()->getConfcontConta(),
            'contaCedenteDv'     => $objBoleto->getConfcont()->getConfcontContaDigito(),
            'contrato'           => '',
            'convenio'           => $objBoleto->getConfcont()->getConfcontConvenio(),
            'documento'          => $arrCedente['pesCnpj'] != "" ? $arrCedente['pesCnpj'] : $arrCedente['pesCpf'],
            'endereco'           => $enderecoCedente,
            'enderecoFormatado'  => $enderecoFormatado,
            'cep'                => $arrCedente['endCep'],
            'formatacaoConvenio' => strlen($objBoleto->getConfcont()->getConfcontConvenio()),
            'identificacao'      => $arrCedente['pesNome'],
            'logoCedente'        => $logo,
            'nomeCedente'        => $arrCedente['pesNome'],
            'pontodevenda'       => '',
            'uf'                 => $arrCedente['endEstado'],
            'variacaoCarteira'   => $objBoleto->getConfcont()->getConfcontVariacao(),
            'alunocursoId'       => $objAcadgeralAlunoCurso ? $objAcadgeralAlunoCurso->getAlunocursoId() : ''
        );

        if (
            $objBoleto->getConfcont()->getConfcontConvenio() &&
            $objBoleto->getConfcont()->getBanc()->getBancCodigo() != \Financeiro\Service\BoletoBanco::BANCO_BRASIL
        ) {
            $arrRetornoCedente['contaCedente']   = $objBoleto->getConfcont()->getConfcontConvenio();
            $arrRetornoCedente['contaCedenteDv'] = '';
        }

        return $arrRetornoCedente;
    }

    public function formatarDadosBoletoImpressaoSacado(\Financeiro\Entity\Boleto $objBoleto)
    {
        $servicePessoa = new \Pessoa\Service\Pessoa($this->getEm());

        $arrSacado      = $servicePessoa->getArray($objBoleto->getTitulo()->getPes()->getPesId());
        $enderecoSacado = $servicePessoa->formatarEnderecoDuasLinhas($arrSacado);

        /** @var \Financeiro\Entity\Boleto $objBoleto */

        return array(
            'documento' => $arrSacado['pesCpf'] ? $arrSacado['pesCpf'] : $arrSacado['pesCnpj'],
            'endereco1' => $enderecoSacado[0],
            'endereco2' => $enderecoSacado[1],
            'nome'      => $arrSacado['pesNome'],
            'rg'        => $arrSacado['pesRg']
        );
    }

    public function pesquisaBoletoPorTitulo($tituloId)
    {
        /** @var \Financeiro\Entity\Boleto $objTitulo */
        $objTitulo = $this->getRepository()->findOneBy(['titulo' => $tituloId]);

        return $objTitulo;
    }

    public function retornaLinhaDigitavelBoleto(
        \Financeiro\Entity\Boleto $objBoleto,
        \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator,
        $tipo = 'string'
    ) {
        $arrDadosBoleto = $this->formatarDadosBoletoImpressao($objBoleto);
        $bancCodigo     = $objBoleto->getConfcont()->getBanc()->getBancCodigo();

        $sacado         = new \PhpBoletoZf2\Model\Sacado($arrDadosBoleto['sacado']);
        $cedente        = new \PhpBoletoZf2\Model\Cedente($arrDadosBoleto['cedente']);
        $arrDadosBoleto = $arrDadosBoleto['boleto'];

        if ($bancCodigo == \Financeiro\Service\BoletoBanco::BANCO_BRADESCO) {
            $boleto    = new \PhpBoletoZf2\Model\BoletoBradesco($arrDadosBoleto);
            $ObjBoleto = $serviceLocator->get('Boleto\Bradesco');
        } elseif ($bancCodigo == \Financeiro\Service\BoletoBanco::BANCO_BRASIL) {
            $boleto    = new \PhpBoletoZf2\Model\BoletoBB($arrDadosBoleto);
            $ObjBoleto = $serviceLocator->get('Boleto\BB');
        } elseif ($bancCodigo == \Financeiro\Service\BoletoBanco::BANCO_BANCOOB) {
            $boleto    = new \PhpBoletoZf2\Model\BoletoBancoob($arrDadosBoleto);
            $ObjBoleto = $serviceLocator->get('Boleto\Bancoob');
        } elseif ($bancCodigo == \Financeiro\Service\BoletoBanco::BANCO_CAIXA) {
            $boleto    = new \PhpBoletoZf2\Model\BoletoCaixa($arrDadosBoleto);
            $ObjBoleto = $serviceLocator->get('Boleto\CaixaSigcb');
        } else {
            return '';
        }

        $ObjBoleto->setSacado($sacado);
        $ObjBoleto->setCedente($cedente);
        $ObjBoleto->setBoleto($boleto);
        $ObjBoleto->prepare();

        return $boleto->getLinhaDigitavel($tipo);
    }

    public function cancelaBoletosTitulo(\Financeiro\Entity\FinanceiroTitulo $objTitulo)
    {
        try {
            $this->getEm()->beginTransaction();
            $arrBoletosVinculados = $this->getRepository()->findBy(['titulo' => $objTitulo]);

            /* @var $objBoleto \Financeiro\Entity\Boleto */
            foreach ($arrBoletosVinculados as $objBoleto) {
                $objBoleto->setBolEstado(self::BOL_ESTADO_CANCELADO);
                $this->getEm()->persist($objBoleto);
                $this->getEm()->flush($objBoleto);
            }

            $this->getEm()->commit();
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível cancelar boletos!' . '<br>' . $e->getMessage());

            return false;
        }

        return true;
    }

    public function retornaDataConcatenandoDia($dia, $data)
    {
        if (!$dia and is_integer($dia)) {
            $this->setLastError("É necessário informar um dia");

            return false;
        }
        if (!$data) {
            $data = new \Datetime();
        }

        return (str_pad($dia, 2, "0", STR_PAD_LEFT)) . '/' . $data->format('m/Y');
    }

    public function retornaDataDesconto($dias, $dataDesconto, $formato = false)
    {
        if (!$dataDesconto) {
            $dataDesconto = new \Datetime();
        }

        $dias = (1 * $dias);

        if ($dias < 0) {
            $dias *= -1;
            $dataDesconto->add(new \DateInterval('P' . $dias . 'D'));
        } else {
            $dataDesconto->sub(new \DateInterval('P' . $dias . 'D'));
        }

        if ($formato) {
            return $dataDesconto->format($formato);
        }

        return $dataDesconto;
    }

    /**
     * @param $detalhe \Cnab\Retorno\Cnab240\Detalhe | \Cnab\Retorno\Cnab400\Detalhe
     * @param $entity boolean caso true, busca detalhe da base de dados ao invéz do arquivo
     * @return boolean | \Financeiro\Entity\Boleto
     */
    public function buscaBoletoPeloDetalhe($detalhe, $entity = false)
    {
        $query = "SELECT * FROM boleto WHERE 1 = 1";

        $serviceFinanceiroBoleto = new \Financeiro\Service\Boleto($this->getEm());
        $serviceRetornoDetalhe   = new \Financeiro\Service\FinanceiroRetornoDetalhe($this->getEm());

        $nossoNumero     = null;
        $numeroDocumento = null;

        if ($entity) {
            $nossoNumero     = $detalhe['detalhe_nosso_numero'];
            $numeroDocumento = $detalhe['detalhe_numero_documento'];
        } else {
            if ($detalhe->getNossoNumero()) {
                $nossoNumero = $detalhe->getNossoNumero();
            }

            if ($detalhe->getNumeroDocumento()) {
                $numeroDocumento = $detalhe->getNumeroDocumento();
            }
        }

        if ($nossoNumero) {
            $query .= " AND bol_nosso_numero = {$nossoNumero}";
        }

        if ($numeroDocumento) {
            $query .= " AND bol_numero_documento = {$numeroDocumento}";
        }

        if ($result = $this->executeQuery($query)->fetch()) {
            if ($entity) {
                return $serviceFinanceiroBoleto->getRepository()->find($result['bol_id']);
            }

            return true;
        }

        return false;
    }
}

?>