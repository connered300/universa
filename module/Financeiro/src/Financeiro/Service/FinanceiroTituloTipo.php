<?php

namespace Financeiro\Service;

use VersaSpine\Service\AbstractService;

class FinanceiroTituloTipo extends AbstractService
{
    const TIPOTITULO_TEMPO_VENCIMENTO_TIPO_DIA               = 'DIA';
    const TIPOTITULO_TEMPO_VENCIMENTO_TIPO_MES               = 'MES';
    const TIPOTITULO_MATRICULA_EMISSAO_SIM                   = 'Sim';
    const TIPOTITULO_MATRICULA_EMISSAO_NAO                   = 'Não';
    const TIPOTITULO_MATRICULA_EMISSAO_OPCIONAL              = 'Opcional';
    const TIPOTITULO_MATRICULA_EMISSAO_OPCIONAL_INTERNAMENTE = 'Opcional Internamente';
    const TIPOTITULO_MENSALIDADE                             = 2;
    const TIPOTITULO_MATRICULA                               = 36;
    const TIPOTITULO_GRUPO_GERAL                             = 'Geral';
    const TIPOTITULO_GRUPO_DOCUMENTOS                        = 'Documentos';
    const TIPOTITULO_GRUPO_ACADEMICO                         = 'Academico';
    const TIPOTITULOVESTIBULAR                               = 'Vestibular';

    const MULTA_BIBLIOTECA = 14;
    const MENSALIDADE      = 2;
    const DEPENDENCIA      = 26;
    const ADAPTACAO        = 27;
    const MONOGRAFICA      = 28;
    const VESTIBULAR       = 1;

    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Financeiro\Entity\FinanceiroTituloTipo');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2TipotituloTempoVencimentoTipo($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getTipotituloTempoVencimentoTipo());
    }

    public static function getTipotituloTempoVencimentoTipo()
    {
        return array(self::TIPOTITULO_TEMPO_VENCIMENTO_TIPO_DIA, self::TIPOTITULO_TEMPO_VENCIMENTO_TIPO_MES);
    }

    /**
     * @param $params
     * @return array
     */
    public function pesquisaForJson($params)
    {
        $sql            = '
        SELECT
            a.tipotituloId AS tipotitulo_id,
            a.tipotituloNome AS tipotitulo_nome,
            a.tipotituloDescricao AS tipotitulo_descricao,
            a.tipotituloTempoVencimento AS tipotitulo_tempo_vencimento,
            a.tipotituloTempoVencimentoTipo AS tipotitulo_tempo_vencimento_tipo,
            a.tipotituloMatriculaEmissao AS tipotitulo_matricula_emissao,
            a.tipotituloDescontoPercentualMaximo AS tipotitulo_desconto_percentual_maximo,
            a.tipotituloNumeroMaximoParcelas AS tipotitulo_numero_maximo_parcelas,
            a.tipotituloGrupo AS tipotitulo_grupo
        FROM Financeiro\Entity\FinanceiroTituloTipo a
        WHERE';
        $tipotituloNome = false;
        $tipotituloId   = false;

        if ($params['q']) {
            $tipotituloNome = $params['q'];
        } elseif ($params['query']) {
            $tipotituloNome = $params['query'];
        }

        if ($params['tipotituloId']) {
            $tipotituloId = $params['tipotituloId'];
        }

        $parameters = array('tipotituloNome' => "{$tipotituloNome}%");
        $sql .= ' a.tipotituloNome LIKE :tipotituloNome';

        if ($tipotituloId) {
            $parameters['tipotituloId'] = explode(',', $tipotituloId);
            $parameters['tipotituloId'] = $tipotituloId;
            $sql .= ' AND a.tipotituloId NOT IN(:tipotituloId)';
        }

        if ($params['pesquisaGrupo']) {
            $grupotitulo = $params['pesquisaGrupo'];
            $sql .= " AND a.tipotituloId = '$grupotitulo'";
        }

        if ($params['pesquisaGrupo']) {
            $grupotitulo                   = $params['pesquisaGrupo'];
            $parameters['tipotituloGrupo'] = $grupotitulo;

            $sql .= " AND a.tipotituloGrupo = :tipotituloGrupo";
        }

        $sql .= " ORDER BY a.tipotituloNome";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setFirstResult(0)->setMaxResults(40)->getResult();

        return $result;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['tipotituloId']) {
                /** @var $objFinanceiroTituloTipo \Financeiro\Entity\FinanceiroTituloTipo */
                $objFinanceiroTituloTipo = $this->getRepository()->find($arrDados['tipotituloId']);

                if (!$objFinanceiroTituloTipo) {
                    $this->setLastError('Registro de tipo de título não existe!');

                    return false;
                }
            } else {
                $objFinanceiroTituloTipo = new \Financeiro\Entity\FinanceiroTituloTipo();
            }

            $objFinanceiroTituloTipo->setTipotituloNome($arrDados['tipotituloNome']);
            $objFinanceiroTituloTipo->setTipotituloDescricao($arrDados['tipotituloDescricao']);
            $objFinanceiroTituloTipo->setTipotituloTempoVencimento($arrDados['tipotituloTempoVencimento']);
            $objFinanceiroTituloTipo->setTipotituloTempoVencimentoTipo($arrDados['tipotituloTempoVencimentoTipo']);
            $objFinanceiroTituloTipo->setTipotituloMatriculaEmissao($arrDados['tipotituloMatriculaEmissao']);

            if ($arrDados['tipotituloGrupo']) {
                $objFinanceiroTituloTipo->setTipotituloGrupo($arrDados['tipotituloGrupo']);
            } else {
                $objFinanceiroTituloTipo->setTipotituloGrupo(null);
            }

            $this->getEm()->persist($objFinanceiroTituloTipo);
            $this->getEm()->flush($objFinanceiroTituloTipo);

            $this->getEm()->commit();

            $arrDados['tipotituloId'] = $objFinanceiroTituloTipo->getTipotituloId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de tipo de título!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['tipotituloNome']) {
            $errors[] = 'Por favor preencha o campo "nome"!';
        }

        if (!in_array($arrParam['tipotituloTempoVencimentoTipo'], self::getTipotituloTempoVencimentoTipo())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "tipo de tempo de vencimento"!';
        }

        if (!in_array($arrParam['tipotituloMatriculaEmissao'], self::getTipotituloMatriculaEmissao())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "emissão de título na matrícula"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public static function getTipotituloMatriculaEmissao()
    {
        return array(
            self::TIPOTITULO_MATRICULA_EMISSAO_SIM,
            self::TIPOTITULO_MATRICULA_EMISSAO_NAO,
            self::TIPOTITULO_MATRICULA_EMISSAO_OPCIONAL,
            self::TIPOTITULO_MATRICULA_EMISSAO_OPCIONAL_INTERNAMENTE
        );
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "
        SELECT financeiro__titulo_tipo.*,prioridade.prioridade FROM financeiro__titulo_tipo
        INNER JOIN (
        SELECT tipotitulo_id,prioridade,tipotitulo_prioridade FROM (
            SELECT t.*, (@p1:=@p1+1) as prioridade FROM financeiro__titulo_tipo as t 
            JOIN (SELECT @p1:=0 FROM dual) as prioridade 	    
            ORDER BY tipotitulo_prioridade asc ) as x
         ) prioridade ON prioridade.tipotitulo_id=financeiro__titulo_tipo.tipotitulo_id
 ";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $tipotituloId
     * @return array
     */
    public function getArray($tipotituloId)
    {
        /** @var $objFinanceiroTituloTipo \Financeiro\Entity\FinanceiroTituloTipo */
        $objFinanceiroTituloTipo = $this->getRepository()->find($tipotituloId);

        try {
            $arrDados = $objFinanceiroTituloTipo->toArray();
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {

        return $arrDados;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('tipotituloId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Financeiro\Entity\FinanceiroTituloTipo */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getTipotituloId();
            $arrEntity[$params['value']] = $objEntity->getTipotituloNome();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['tipotituloId']) {
            $this->setLastError('Para remover um registro de tipo de título é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objFinanceiroTituloTipo \Financeiro\Entity\FinanceiroTituloTipo */
            $objFinanceiroTituloTipo = $this->getRepository()->find($param['tipotituloId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objFinanceiroTituloTipo);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de tipo de título.');

            return false;
        }

        return true;
    }

    /**
     * @return \Financeiro\Entity\FinanceiroTituloTipo
     * @throws \Exception
     */
    public function getTipoRenegociacao()
    {
        return $this->criaSeNecessarioERetornaTituloTipo('Renegociação');
    }

    /**
     * @return \Financeiro\Entity\FinanceiroTituloTipo
     * @throws \Exception
     */
    public function getTipoPagamentoMultiplo()
    {
        return $this->criaSeNecessarioERetornaTituloTipo('Pagamento Múltiplo');
    }

    /**
     * @param $descricao
     * @return \Financeiro\Entity\FinanceiroTituloTipo
     * @throws \Exception
     */
    public function criaSeNecessarioERetornaTituloTipo($descricao)
    {
        if (!$descricao) {
            throw new \Exception('Para pesquisar/criar um tipo de título é necessário que se forneça a descrição!');
        }

        /** @var $objTituloTipo \Financeiro\Entity\FinanceiroTituloTipo */
        $objTituloTipo = $this->getRepository()->findOneBy(['tipotituloNome' => $descricao]);

        if (!$objTituloTipo) {
            $arrTituloTipo = [
                'tipotituloNome'                     => $descricao,
                'tipotituloDescricao'                => $descricao,
                'tipotituloTempoVencimento'          => 0,
                'tipotituloTempoVencimentoTipo'      => self::TIPOTITULO_TEMPO_VENCIMENTO_TIPO_DIA,
                'tipotituloMatriculaEmissao'         => self::TIPOTITULO_MATRICULA_EMISSAO_NAO,
                'tipotituloDescontoPercentualMaximo' => 0,
                'tipotituloNumeroMaximoParcelas'     => 0,
                'tipotituloGrupo'                    => self::TIPOTITULO_GRUPO_GERAL,
            ];

            if ($this->save($arrTituloTipo)) {
                /** @var $objFinanceiroTituloTipo \Financeiro\Entity\FinanceiroTituloTipo */
                $objTituloTipo = $this->getRepository()->find($arrTituloTipo['tipotituloNome']);
            } else {
                throw new \Exception('Não foi possível encontrar ou criar o tipo de título de ' . $descricao . '!');
            }
        }

        return $objTituloTipo;
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $view->setVariable("arrTipotituloMatriculaEmissao", $this->getArrSelect2TipotituloMatriculaEmissao());
        $view->setVariable("arrTipotituloTempoVencimentoTipo", $this->getArrSelect2TipotituloTempoVencimentoTipo());
        $view->setVariable("arrTipotituloDocumentoGrupo", $this->getArrSelect2DocumentoGrupo());
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2TipotituloMatriculaEmissao($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getTipotituloMatriculaEmissao());
    }

    /**
     * @param $cursoCampusId
     * @return array
     */
    public function retornoTiposDeTituloParaEmissaoNaMatricula($params, $todosOsMeiosDePagamento = true)
    {
        $serviceCampusCurso             = new \Matricula\Service\CampusCurso($this->getEm());
        $serviceFinanceiroValores       = new \Financeiro\Service\FinanceiroValores($this->getEm());
        $serviceFinanceiroMeioPagamento = new \Financeiro\Service\FinanceiroMeioPagamento($this->getEm());
        $serviceFinanceiroConfig        = new \Financeiro\Service\FinanceiroTituloConfig($this->getEm());

        $perId = null;

        if (is_array($params)) {
            $perId         = $params['perId'];
            $cursoCampusId = $params['cursocampus'];
        } else {
            $cursoCampusId = $params;
        }

        $arrFinanceiroTituloTipo = $this->getRepository()->findBy(
            array(
                'tipotituloMatriculaEmissao' => [
                    self::TIPOTITULO_MATRICULA_EMISSAO_SIM,
                    self::TIPOTITULO_MATRICULA_EMISSAO_OPCIONAL,
                    self::TIPOTITULO_MATRICULA_EMISSAO_OPCIONAL_INTERNAMENTE
                ]
            ),
            array('tipotituloPrioridade' => 'asc')
        );

        /** @var \Matricula\Entity\CampusCurso $objCampusCurso */
        $objCampusCurso = $serviceCampusCurso->getRepository()->find($cursoCampusId);

        $arrFiltroMeioPagto = array();

        if (!$todosOsMeiosDePagamento) {
            $arrFiltroMeioPagto = array(
                'meioPagamentoUsoExterno' => \Financeiro\Service\FinanceiroMeioPagamento::MEIO_PAGAMENTO_USO_EXTERNO_SIM
            );
        }

        $arrMeioPagamento = $serviceFinanceiroMeioPagamento->getArrSelect2($arrFiltroMeioPagto);

        $arrEntitiesArr = array();

        $perDataVencimentoInicialEditavel = false;
        $perDataVencimentoInicial         = false;

        if ($perId) {
            $servicePeriodo = new \Matricula\Service\AcadperiodoLetivo($this->getEm());
            /** @var \Matricula\Entity\AcadperiodoLetivo $objPeriodo */
            $objPeriodo = $servicePeriodo->getRepository()->find($perId);

            $perDataVencimentoInicialEditavel = $objPeriodo->getPerDataVencimentoInicialEditavel();
            $perDataVencimentoInicial         = $objPeriodo->getPerDataVencimentoInicial(true);
        }

        /* @var $objFinanceiroTituloTipo \Financeiro\Entity\FinanceiroTituloTipo */
        foreach ($arrFinanceiroTituloTipo as $index => $objFinanceiroTituloTipo) {
            $arrEntities                      = $objFinanceiroTituloTipo->toArray();
            $arrEntities['vencimentoInicial'] = $perDataVencimentoInicial;
            $arrEntities['financeiroValores'] = $serviceFinanceiroValores->retornaValoresPeloTipo(
                $objFinanceiroTituloTipo,
                $cursoCampusId
            );

            $arrParamPesquisaConfig = array(
                'tipotituloId'  => $objFinanceiroTituloTipo->getTipotituloId(),
                'cursocampusId' => $cursoCampusId,
            );

            $objConfigTipoTitulo = $serviceFinanceiroConfig->pesquisarConfiguracao($arrParamPesquisaConfig);

            if ($objConfigTipoTitulo) {
                $arrEntities['dataVencimentoAlteravel'] = (int)$objConfigTipoTitulo->verificaSeVencimentoAlteravel();

                $vencimentoFixo = $serviceFinanceiroConfig->retornarVencimentoTipoTituloMatricula($objConfigTipoTitulo);

                if ($vencimentoFixo && !$serviceFinanceiroConfig->getLastError()) {
                    $arrEntities['dataVencimento'] = $vencimentoFixo->format("d/m/Y");
                }
            }

            if ($perDataVencimentoInicialEditavel && $perDataVencimentoInicial) {
                $arrEntities['financeiroValores'][$index]['perDataVencimentoInicialEditavel'] = $perDataVencimentoInicialEditavel;
                $arrEntities['financeiroValores'][$index]['perDataVencimentoInicial']         = $perDataVencimentoInicial;
            }

            $arrEntities['arrMeioPagamento'] = $arrMeioPagamento;

            $arrEntitiesArr[] = $arrEntities;
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getDescricao($id)
    {
        if (!$id) {
            return false;
        }

        if (!is_array($id)) {
            $id = array($id);
        }

        $id = implode($id, ',');

        $sql = "
        SELECT group_concat(tipotitulo_nome SEPARATOR ', ') AS text
        FROM financeiro__titulo_tipo
        WHERE tipotitulo_id IN(" . $id . ")";

        $result = $this->executeQuery($sql)->fetch();

        return $result['text'];
    }

    /**
     * @return array
     */

    public function getDocumentoGrupo()
    {

        return array(
            self::TIPOTITULO_GRUPO_DOCUMENTOS,
            self::TIPOTITULO_GRUPO_ACADEMICO,
            self::TIPOTITULO_GRUPO_GERAL
        );
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2DocumentoGrupo($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getDocumentoGrupo());
    }

    public function getArrSelect($params = array())
    {
        $arrEntities = $this->getRepository()->findBy([], ['tipotituloNome' => 'asc']);

        $arrEntitiesArr = array();

        /* @var $objEntity \Financeiro\Entity\FinanceiroTituloTipo */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[$objEntity->getTipotituloId()] = $objEntity->getTipotituloNome();
        }

        return $arrEntitiesArr;
    }

    public function alteraPrioridade($param)
    {
        $error = array();

        if (!$param['tipotitulo_id']) {
            $error[] = 'tipo título não informado';
        }
        if (!$param['acao']) {
            $error[] = 'ação não informada';
        }

        if (!empty($error)) {
            $this->setLastError('Erro:' . explode($error));

            return false;
        }

        $arrTipoTitulos = $this->retornaPrioridades(
            ['tipoituloId' => $param['tipotitulo_id'], 'acao' => $param['acao']]
        );

        if ($arrTipoTitulos && count($arrTipoTitulos) > 1) {
            $sqlAlteracao[] = "
            UPDATE `financeiro__titulo_tipo`
            SET `tipotitulo_prioridade`= {$arrTipoTitulos[1]['prioridade']}
            WHERE `tipotitulo_id`={$arrTipoTitulos[0]['tipotitulo_id']}";
            $sqlAlteracao[] = "
            UPDATE `financeiro__titulo_tipo`
            SET `tipotitulo_prioridade`= {$arrTipoTitulos[0]['prioridade']}
            WHERE `tipotitulo_id`={$arrTipoTitulos[1]['tipotitulo_id']}";

            foreach ($sqlAlteracao as $value) {
                $this->executeQuery($value);
            }
        }

        return true;
    }

    public function retornaPrioridades($arrParam)
    {
        $sql = '
        SELECT tipotitulo_id,prioridade,tipotitulo_prioridade FROM (
            SELECT t.*, (@p1:=@p1+1) as prioridade FROM financeiro__titulo_tipo as t
            JOIN (SELECT @p1:=0 FROM dual) as prioridade
            order by tipotitulo_prioridade asc
        ) as x
        WHERE 1=1
        -- CONDICAO --
        ';

        $param = array();

        if ($arrParam['tipoituloId']) {
            $param['tipoituloId'] = $arrParam['tipoituloId'];

            $sql = str_replace('-- CONDICAO --', ' AND x.tipotitulo_id=:tipoituloId ', $sql);
        }

        if ($arrParam['acao']) {
            $sql = '
            SELECT prioridade_primaria.tipotitulo_id,
            prioridade_primaria.prioridade,prioridade_primaria.tipotitulo_prioridade
            FROM (
                SELECT t.*, (@p:=@p+1) as prioridade FROM financeiro__titulo_tipo as t
                JOIN (SELECT @p:=0 FROM dual) as prioridade
                order by tipotitulo_prioridade asc
            ) as prioridade_primaria
            JOIN(' . $sql . ') as prioridade_secundaria
            WHERE
            (
                prioridade_primaria.prioridade=prioridade_secundaria.prioridade OR
                prioridade_primaria.prioridade=prioridade_secundaria.prioridade-1
            )';

            if ($arrParam['acao'] == 'down') {
                $sql .= '+2';
            }

            $sql .= ' LIMIT 2';
        }

        $arrRetorno = $this->executeQueryWithParam($sql, $param)->fetchAll();

        return $arrRetorno;
    }
}
?>