<?php

namespace Financeiro\Service;

use PhpBoletoZf2\Lib\Util;
use VersaSpine\Service\AbstractService;

class FinanceiroRemessa extends AbstractService
{
    const NOME_DIRETORIO = 'Remessas';
    private $fileLog = '';

    private $__lastError = null;

    /** @var \Sistema\Service\SisConfig */
    private $config = null;

    /**
     * @return \Sistema\Service\SisConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param \Sistema\Service\SisConfig $config
     * @return FinanceiroRemessa
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     * @param array                       $arrConfig
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, $arrConfig = array())
    {
        parent::__construct($em, 'Financeiro\Entity\FinanceiroRemessa');

        $this->setConfig(new \Sistema\Service\SisConfig($em, $arrConfig));
        $this->fileLog = '/tmp/log-remessa_' . date("Y-m-d_H-i-s") . '.txt';
    }

    private function registrarLog($mensagem)
    {
        $mensagem = date("d/m/Y H:i:s") . ": " . trim($mensagem) . "\n";

        file_put_contents($this->fileLog, $mensagem, FILE_APPEND);
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function valida($arrdata)
    {
        $errors = array();

        $registro = $arrdata['sacado_cpf'] ? $arrdata['sacado_cpf'] : $arrdata['sacado_cnpj'];

        if (!$registro) {
            $errors[] = (
                strtoupper($arrdata['sacado_tipo']) . " de " . $arrdata['sacado_nome'] . " precisa ser informado"
            );
        }

        /*if (!$arrdata['sacado_cep']) {
            $errors[] = "CEP de " . $arrdata['sacado_nome'] . " precisa ser informado";
        }

        if (!$arrdata['sacado_logradouro']) {
            $errors[] = "Logradouro do " . $arrdata['sacado_nome'] . " precisa ser informado";
        }

        if (!$arrdata['sacado_cidade']) {
            $errors[] = "Cidade de " . $arrdata['sacado_nome'] . " precisa ser informado";
        }

        if (!$arrdata['sacado_uf']) {
            $errors[] = "UF de " . $arrdata['sacado_nome'] . " precisa ser informado";
        }*/

        return $errors;
    }

    public function pesquisaForJson($data)
    {
    }

    public function search(array $data)
    {
        $query = "
          SELECT
		   banc_nome,
		   confcont_convenio convenio,
		   confcont_conta conta,
		   confcont_agencia agencia, 
           remessa_id codigo,
           remessa_codigo codigoRemessa,
           remessa_data dataCriacao,
            DATE_FORMAT(
            remessa_data,
              '%d/%m/%Y %H:%i:%s'
            ) dataCriacaoFormatada,
           arq_nome arquivoNome,
           arq_chave arquivo,
           (SELECT COUNT(bol_id) FROM financeiro__remessa_lote frl WHERE frl.remessa_id=financeiro__remessa.remessa_id) AS quantidadeBoletos
        FROM financeiro__remessa
        INNER JOIN arquivo ON financeiro__remessa.arq_id = arquivo.arq_id
        INNER JOIN boleto_conf_conta USING(confcont_id)
        INNER JOIN boleto_banco USING(banc_id)
        ORDER BY remessa_data DESC
        ";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function searchBoletos($arrData)
    {
        if (!isset($arrData['confContaId'])) {
            $this->setLastError('É necessário informar uma conta!');

            $result                    = array();
            $result['draw']            = "1";
            $result["recordsTotal"]    = 0;
            $result["recordsFiltered"] = 0;
            $result["data"]            = [];

            return $result;
        }

        $query = "
        SELECT
            titulo_id,
            alunocurso_id,
            bol_nosso_numero nosso_numero,
            financeiro__titulo.titulo_valor valor,
            financeiro__titulo_tipo.tipotitulo_nome tipo_titulo,
            DATE_FORMAT(
              financeiro__titulo.titulo_data_vencimento,
              '%d/%m/%Y'
            ) data_vencimento,
            DATE_FORMAT(
              financeiro__titulo.titulo_data_processamento,
              '%d/%m/%Y'
            ) data_processamento,
            boleto.bol_id bol_id,
            pessoa.pes_nome sacado,
            titulo_estado,
            bol_carteira
            -- cnpf_cnpj_endereco_validos --
        FROM boleto
        INNER JOIN boleto_conf_conta bcc ON bcc.confcont_id = boleto.confcont_id
        INNER JOIN financeiro__titulo USING(titulo_id)
        INNER JOIN financeiro__titulo_tipo USING(tipotitulo_id)
        INNER JOIN pessoa ON pessoa.pes_id = financeiro__titulo.pes_id
        LEFT JOIN pessoa_fisica pf ON pf.pes_id=pessoa.pes_id
        LEFT JOIN pessoa_juridica pj ON pj.pes_id=pessoa.pes_id
        LEFT JOIN endereco e ON e.pes_id=pessoa.pes_id
        ";

        $where = [" bcc.confcont_id = {$arrData['confContaId']}"];

        if ($arrData['dataPross']) {
            $dataPross = $arrData['dataPross'];

            if ($dataPross['dataInicial']) {
                $dataInicial = self::formatDateAmericano($dataPross['dataInicial']);
                $where[] .= "titulo_data_processamento >= '$dataInicial'";
            }

            if ($dataPross['dataFinal']) {
                $dataFinal = self::formatDateAmericano($dataPross['dataFinal']);
                $where[] .= "titulo_data_processamento <= '$dataFinal'";
            }
        }

        if ($arrData['boletoCarteira']) {
            $where[] .= " substr(bol_carteira, 1, 1) in ( " . '"' . $arrData['boletoCarteira'] . '"' . " )";
        }

        if ($arrData['titulosValidosRemessa']) {
            $queryExt = ",
            (
                (pf.pes_cpf IS NULL OR  pf.pes_cpf='') AND
                (pj.pes_cnpj IS NULL OR pj.pes_cnpj='')
            ) cpf_cnpj_invalido,
            (
                -- (e.end_estado !='' OR e.end_estado IS NOT NULL) OR
                -- (e.end_cidade!='' OR e.end_cidade IS NOT NULL) OR
                -- (e.end_cep!='' OR e.end_cep IS NOT NULL) OR
                -- (e.end_logradouro!='' OR e.end_logradouro IS NOT NULL)
                -- desabilita validação de endereço
                0
            ) AS endereco_invalido,
            (curdate() > financeiro__titulo.titulo_data_vencimento) boleto_vencido,
            @dataAtual:=now() as dataAtual";

            $query = str_replace("-- cnpf_cnpj_endereco_validos --", $queryExt, $query);
        }

        if ($arrData['boletosValidosRemessa']) {
            $where[] .= "
            (
                (pf.pes_cpf IS NOT NULL AND  pf.pes_cpf!='') OR
                (pj.pes_cnpj IS NOT NULL AND pj.pes_cnpj!='')
            )
            -- desabilita validação de endereço
            -- AND (
                -- (e.end_estado !='' OR e.end_estado IS NOT NULL) AND
                -- (e.end_cidade!='' OR e.end_cidade IS NOT NULL) AND
                -- (e.end_cep!='' OR e.end_cep IS NOT NULL) AND
                -- (e.end_logradouro!='' OR e.end_logradouro IS NOT NULL)
            -- )
            AND (CURDATE() <= financeiro__titulo.titulo_data_vencimento)
            ";
        }

        if ($arrData['boletosInValidosRemessa']) {
            $where[] .= "
            (
                (
                    (pf.pes_cpf IS NULL OR  pf.pes_cpf='') AND
                    (pj.pes_cnpj IS NULL OR pj.pes_cnpj='')
                ) OR
                (
                    -- (e.end_estado !='' OR e.end_estado IS NOT NULL) OR
                    -- (e.end_cidade!='' OR e.end_cidade IS NOT NULL) OR
                    -- (e.end_cep!='' OR e.end_cep IS NOT NULL) OR
                    -- (e.end_logradouro!='' OR e.end_logradouro IS NOT NULL)
                    -- desabilita validação de endereço
                    0
                ) OR
                (CURDATE() > financeiro__titulo.titulo_data_vencimento)
            ) ";
        }

        if ($arrData['dataVenc']) {
            $dataVenc = $arrData['dataVenc'];

            if ($dataVenc['dataInicial']) {
                $dataInicial = new \DateTime(self::formatDateAmericano($dataVenc['dataInicial']));

                if ((new \DateTime('now')) > $dataInicial) {
                    $dataInicial = (new \DateTime('now'))->format('d/m/Y');
                } else {
                    $dataInicial = $dataInicial->format('d/m/Y');
                }

                $dataInicial = self::formatDateAmericano($dataInicial);

                $where[] .= "titulo_data_vencimento >= '$dataInicial'";
            }

            if ($dataVenc['dataFinal']) {
                $dataFinal = self::formatDateAmericano($dataVenc['dataFinal']);
                $where[] .= "titulo_data_vencimento <= '$dataFinal'";
            }
        }

        if ($arrData['boletosNotInclused']) {
            $where[] = "boleto.bol_id NOT IN(" . implode(',', array_filter($arrData['boletosNotInclused'])) . ")";
        }

        if ($arrData['tituloEstado']) {
            $where[] = "FIND_IN_SET(titulo_estado, '" . implode(',', $arrData['tituloEstado']) . "')";
        }

        if ($arrData['reinclusao'] != 'Sim') {
            $where[] = "titulo_incluir_remessa = 'Sim'";
        }

        $query .= ' WHERE ' . implode(' AND ', $where);
        $query .= " GROUP BY titulo_id";
        $query .= " ORDER BY titulo_data_vencimento ASC";

        $result = $this->paginationDataTablesAjax($query, $arrData, null, false);

        return $result;
    }

    public function save(array $arrdata)
    {
        $objRemessa = new \Financeiro\Entity\FinanceiroRemessa();
        $objArquivo = $arrdata['arq'];

        $this->getEm()->persist($objArquivo);
        $this->getEm()->flush($objArquivo);

        $objRemessa->setArq($objArquivo);
        $objRemessa->setConfcont($arrdata['confCont']);
        $objRemessa->setRemessaData($arrdata['remessaData']);
        $objRemessa->setRemessaCodigo($arrdata['remessaCodigo']);

        $this->getEm()->persist($objRemessa);
        $this->getEm()->flush($objRemessa);

        return $objRemessa;
    }

    public function corrigirRemessas()
    {
        $serviceBoleto = new \Financeiro\Service\Boleto($this->getEm());

        $arrRemessas = $this->getRepository()->findAll();
        $query       = [];

        /** @var \Financeiro\Entity\FinanceiroRemessa $objRemessa */
        foreach ($arrRemessas as $objRemessa) {
            $file = (
                $objRemessa->getArq()->getDiretorio()->getArqDiretorioEndereco() . '/' .
                $objRemessa->getArq()->getArqChave()
            );

            if ($content = file_get_contents($file)) {
                $titulos = [];

                $content = explode("\n", $content);

                foreach ($content as $line) {
                    if (preg_match('/\d{13}P (\d{2})/', $line, $movimento)) {
                        $titulo = trim(substr($line, 62, 11));

                        /** @var \Financeiro\Entity\Boleto $objBoleto */
                        $objBoleto = $serviceBoleto->getRepository()->findOneBy(['titulo' => $titulo]);

                        if (!$objBoleto) {
                            continue;
                        }

                        $titulo = array(1, $objRemessa->getRemessaId(), $objBoleto->getBolId(), $movimento[1]);

                        $titulos[] = '(' . implode(', ', $titulo) . ')';
                    }
                }

                if (!$titulos) {
                    continue;
                }

                $query[] = "
                DELETE FROM financeiro__remessa_lote WHERE remessa_id=" . $objRemessa->getRemessaId() . ";";
                $query[] = "
                INSERT INTO financeiro__remessa_lote(lote_codigo, remessa_id, bol_id, lote_movimento)
                VALUES " . implode(', ', $titulos) . ';';
            }
        }

        echo implode("\n", $query);
        exit();
    }

    public function getRemessaCode($confcontId = false)
    {
        $query = "
        SELECT MAX(remessa_codigo) codigo
        FROM financeiro__remessa";

        if ($confcontId) {
            $query .= ' WHERE confcont_id=' . $confcontId;
        }

        $result = $this->executeQueryWithParam($query)->fetch();

        $codigo = ((integer)$result['codigo']) + 1;

        return $codigo;
    }

    public function getNossoNumeroBB($convenio, $bolNossoNumero)
    {
        $padLength      = 17 - strlen($convenio);
        $bolNossoNumero = str_pad($bolNossoNumero, $padLength, 0, STR_PAD_LEFT);

        $nossoNumero = $convenio . $bolNossoNumero;

        return str_pad($nossoNumero, 20, ' ', STR_PAD_RIGHT);
    }

    public function retornaBoletosDisponiveis($arrData)
    {
        $query = "
        SELECT boleto.bol_id AS boletos
        FROM boleto
        INNER JOIN boleto_conf_conta bcc ON bcc.confcont_id = boleto.confcont_id
        INNER JOIN financeiro__titulo USING(titulo_id)
        INNER JOIN financeiro__titulo_tipo USING(tipotitulo_id)
        INNER JOIN pessoa ON pessoa.pes_id = financeiro__titulo.pes_id
        LEFT JOIN pessoa_fisica pf ON pf.pes_id=pessoa.pes_id
        LEFT JOIN pessoa_juridica pj ON pj.pes_id=pessoa.pes_id
        LEFT JOIN endereco e ON e.pes_id=pessoa.pes_id
        ";

        $where = array();

        $where[] = "bcc.confcont_id =" . $arrData['confContaId'];

        if ($arrData['dataPross']) {
            $dataPross = $arrData['dataPross'];

            if ($dataPross['dataInicial']) {
                $dataInicial = self::formatDateAmericano($dataPross['dataInicial']);
                $where[]     = "titulo_data_processamento >= '$dataInicial'";
            }

            if ($dataPross['dataFinal']) {
                $dataFinal = self::formatDateAmericano($dataPross['dataFinal']);
                $where[]   = "titulo_data_processamento <= '$dataFinal'";
            }
        }

        if ($arrData['boletoCarteira']) {
            $where[] = "substr(bol_carteira, 1, 1) in ( " . '"' . $arrData['boletoCarteira'] . '"' . " )";
        }

        if ($arrData['titulosValidosRemessa']) {
            $where[] = "
            (
                (pf.pes_cpf IS  NOT NULL AND  pf.pes_cpf!='') OR
                (pj.pes_cnpj IS  NOT NULL AND pj.pes_cnpj!='')
            ) AND

            -- desabilita validação de endereço
            -- AND (
                -- (e.end_estado !='' OR e.end_estado IS NOT NULL) AND
                -- (e.end_cidade!='' OR e.end_cidade IS NOT NULL) AND
                -- (e.end_cep!='' OR e.end_cep IS NOT NULL) AND
                -- (e.end_logradouro!='' OR e.end_logradouro IS NOT NULL)
            -- )

             (CURDATE() <= financeiro__titulo.titulo_data_vencimento)
            ";
        }

        if ($arrData['dataVenc']) {
            $dataVenc = $arrData['dataVenc'];

            if ($dataVenc['dataInicial']) {
                $dataInicial = new \DateTime(self::formatDateAmericano($dataVenc['dataInicial']));

                if ((new \DateTime('now')) > $dataInicial) {
                    $dataInicial = (new \DateTime('now'))->format('d/m/Y');
                } else {
                    $dataInicial = $dataInicial->format('d/m/Y');
                }

                $dataInicial = self::formatDateAmericano($dataInicial);

                $where[] .= "titulo_data_vencimento >= '$dataInicial'";
            }

            if ($dataVenc['dataFinal']) {
                $dataFinal = self::formatDateAmericano($dataVenc['dataFinal']);
                $where[] .= "titulo_data_vencimento <= '$dataFinal'";
            }
        }

        if ($arrData['boletosNotInclused']) {
            $where[] = "boleto.bol_id NOT IN(" . implode(',', $arrData['boletosNotInclused']) . ")";
        }

        if ($arrData['tituloEstado']) {
            $where[] = "FIND_IN_SET(titulo_estado, '" . implode(',', $arrData['tituloEstado']) . "')";
        }

        if ($arrData['reinclusao'] != 'Sim') {
            $where[] = "titulo_incluir_remessa = 'Sim'";
        }

        $query .= ' WHERE ' . implode(' AND ', $where);
        $query .= ' GROUP BY bol_id';

        $result = $this->executeQuery($query)->fetchAll(\PDO::FETCH_COLUMN);

        return ['boletos' => $result];
    }

    /**
     * @param $arrParam array
     * @return array|bool
     */
    public function retornaDadosRemessa($arrParam)
    {
        if (!$arrParam) {
            $this->setLastError('É necessário informar algum dado para a busca!');

            return false;
        }

        $serviceDescontoTitulo = new \Financeiro\Service\FinanceiroDescontoTitulo($this->getEm());

        $formaCalculoIncentivo = $this->getConfig()->localizarChave(
            'FINANCEIRO_CONFIG_CALCULO_DESCONTO_INCENTIVO',
            $serviceDescontoTitulo::FINANCEIRO_CONFIG_CALCULO_DESCONTO_INCENTIVO_VALOR_BRUTO
        );

        $valorLiquido = $serviceDescontoTitulo::FINANCEIRO_CONFIG_CALCULO_DESCONTO_INCENTIVO_VALOR_LIQUIDO;

        $testaSeFormaDeCalculoValorLiquido = $valorLiquido == $formaCalculoIncentivo ? 1 : 0;

        $sql = "
        SELECT
   @valorLiquido:=(
   (
      ft.titulo_valor+
      COALESCE(ft.titulo_acrescimo_manual,0)
   ) -
   (COALESCE(total__descontos_nlimitado_vencimento.desconto,0)+COALESCE(ft.titulo_desconto_manual,0)+COALESCE(ft.titulo_desconto,0))
   ) AS valor_liquido,

   @valorLiquidoSemAcrescimo:=(
   ft.titulo_valor -
   (COALESCE(total__descontos_nlimitado_vencimento.desconto,0)+COALESCE(ft.titulo_desconto_manual,0)+COALESCE(ft.titulo_desconto,0))
   ) AS valor_liquido_sem_acrescimo,

   @descontosPreFixados:=(
   COALESCE(total__descontos_limitado_vencimento.desconto,0)+COALESCE(total__descontos_nlimitado_vencimento.desconto,0)   
   )AS desconto_pre_fixado,

   /** DESCONTO PRE FIXADO 1**/
   @dataDescontoAntecipado1:=(
   DATE_SUB(ft.titulo_data_vencimento, INTERVAL ftc.tituloconf_dia_desc DAY)
   ) AS data_desconto_antecipado1,
   
   (date_format(@dataDescontoAntecipado1, '%Y%m%d')) data_desc_antecipado_formatada_1,   
   
  /* TRAS O VALOR DO DESCONTO DE INCEITO OU NULL*/
   @ValorDescontAntecipado1:=(
   IF(
   (@descontosPreFixados=0 OR 
   ((total__descontos_limitado_vencimento.desctipo_desconto_acumulativo='Sim' OR total__descontos_limitado_vencimento.desctipo_desconto_acumulativo IS NULL )
   AND 
   (total__descontos_nlimitado_vencimento.desctipo_desconto_acumulativo='Sim' OR total__descontos_nlimitado_vencimento.desctipo_desconto_acumulativo IS NULL )
   AND  
   ftc.tituloconf_desconto_incentivo_acumulativo = 'Sim') AND DATE(NOW()) <= @dataDescontoAntecipado1),   
   IF(
   COALESCE(ftc.tituloconf_valor_desc,0) !=0,
   IF(ftc.tituloconf_valor_desc>ft.titulo_valor,0,ftc.tituloconf_valor_desc),
   (IF($testaSeFormaDeCalculoValorLiquido,@valorLiquidoSemAcrescimo,ft.titulo_valor)*ftc.tituloconf_percent_desc)
   )
  ,NULL ) 
   )AS valor_desconto_antecipado1,
  
  /** DESCONTO PRE FIXADO 1**/
   @dataDescontoAntecipado2:=(
   DATE_SUB(ft.titulo_data_vencimento, INTERVAL ftc.tituloconf_dia_desc2 DAY)
   ) AS data_desconto_antecipado2,
  
  (date_format(@dataDescontoAntecipado2, '%Y%m%d'))data_desc_antecipado_formatada_2,
  
  /* TRAS O VALOR DO DESCONTO DE INCEITO OU NULL*/ 
   @ValorDescontAntecipado2:=(
    IF(
   (@descontosPreFixados=0 OR 
   ((total__descontos_limitado_vencimento.desctipo_desconto_acumulativo='Sim' OR total__descontos_limitado_vencimento.desctipo_desconto_acumulativo IS NULL )
   AND 
   (total__descontos_nlimitado_vencimento.desctipo_desconto_acumulativo='Sim'OR total__descontos_nlimitado_vencimento.desctipo_desconto_acumulativo IS NULL ) 
   AND  
   ftc.tituloconf_desconto_incentivo_acumulativo = 'Sim') AND DATE(NOW()) <= @dataDescontoAntecipado2),   
   IF(
   COALESCE(ftc.tituloconf_valor_desc2,0) !=0,
   IF(ftc.tituloconf_valor_desc2>ft.titulo_valor,0,ftc.tituloconf_valor_desc2),
   (IF($testaSeFormaDeCalculoValorLiquido,@valorLiquidoSemAcrescimo,ft.titulo_valor)*ftc.tituloconf_percent_desc2)
   )
  ,NULL ) 
   )AS valor_desconto_antecipado2,	
                
           boleto.bol_carteira,   
           boleto.bol_id AS boleto,
           boleto.bol_nosso_numero,
           boleto.bol_numero_documento,
           ft.titulo_id titulo_id,
           ft.titulo_valor,
           DATE_FORMAT(ft.titulo_data_vencimento, '%Y%m%d')titulo_data_vencimento,
           DATE_FORMAT(ft.titulo_data_processamento, '%Y%m%d')titulo_data_processamento,
           tituloconf_multa,
           coalesce(tituloconf_multa, 0) as tituloconf_multa,
           coalesce(tituloconf_juros, 0) as tituloconf_juros,
           ft.titulo_parcela,
           bcc.confcont_id,
           bcc.confcont_conta conf_conta,
           bcc.confcont_agencia,
           bcc.pes_id pes__id_cedente,
           bcc.confcont_agencia_digito,
           bcc.confcont_conta,
           bcc.confcont_conta_digito,
           bcc.confcont_convenio,
           bcc.confcont_carteira,
           e.end_bairro,
           e.end_cep,
           e.end_cidade,
           e.end_logradouro,
           e.end_numero,
           e.end_complemento,
           e.end_estado,
           p.pes_nome,
           p.pes_id,
           if(p.pes_tipo IN('Fisica','Equiparada'), 1, 2) pes_tipo_numero,
           if(p.pes_tipo IN('Fisica','Equiparada'), pf.pes_cpf, pj.pes_cnpj) pes_documento,
           total__descontos_limitado_vencimento.desconto AS desconto_limita_vencimento,
           @tituloId:=(ft.titulo_id) AS tituloId,
           ftc.tituloconf_id,
           ultimaRemessaLote.remessa_id,
           ultimaRemessaLote.remessa_data,
           ultimaRemessaLote.lote_movimento
        FROM boleto
        INNER JOIN financeiro__titulo ft USING(titulo_id)
        INNER JOIN pessoa p ON p.pes_id = ft.pes_id
        LEFT JOIN financeiro__titulo_config ftc USING(tituloconf_id)
        LEFT JOIN pessoa_fisica pf ON pf.pes_id=p.pes_id
        LEFT JOIN pessoa_juridica pj ON pj.pes_id=p.pes_id
        LEFT JOIN endereco e ON e.pes_id=p.pes_id
        LEFT JOIN boleto_conf_conta bcc ON bcc.confcont_id = boleto.confcont_id
        /* DESCONTOS LIMITADOS AO VENCIMENTO*/
        LEFT JOIN
        (
			SELECT coalesce(sum(desconto_aplicacao_valor), 0) AS desconto, desctipo_desconto_acumulativo, fdt.titulo_id
            FROM view__financeiro_descontos_titulos_atualizada fdt
            WHERE desconto_aplicacao_valor > 0 AND fdt.desctipo_limita_vencimento='Sim'
            GROUP BY titulo_id
            ORDER BY desctipo_desconto_acumulativo ASC
        ) as total__descontos_limitado_vencimento on total__descontos_limitado_vencimento.titulo_id=ft.titulo_id
        /* DESCONTOS NÃO LIMITADOS AO VENCIMENTO*/
        LEFT JOIN
        (
			SELECT coalesce(sum(desconto_aplicacao_valor), 0) AS desconto, desctipo_desconto_acumulativo, fdt.titulo_id
            FROM view__financeiro_descontos_titulos_atualizada fdt
            WHERE desconto_aplicacao_valor > 0 AND fdt.desctipo_limita_vencimento!='Sim'
            GROUP BY titulo_id
            ORDER BY desctipo_desconto_acumulativo ASC
        ) as total__descontos_nlimitado_vencimento on total__descontos_nlimitado_vencimento.titulo_id=ft.titulo_id
        LEFT JOIN
        (
            SELECT max(remessa_data) AS remessa_data, remessa_id, titulo_id, bol_id, lote_movimento
            FROM financeiro__remessa_lote frl
            INNER JOIN boleto USING (bol_id)
            INNER JOIN financeiro__remessa USING (remessa_id)
            GROUP BY bol_id
        ) as ultimaRemessaLote on ultimaRemessaLote.titulo_id=ft.titulo_id
        WHERE
          titulo_estado='" . \Financeiro\Service\FinanceiroTitulo::TITULO_ESTADO_ABERTO . "'
          -- AND curdate() <= titulo_data_vencimento
        ";

        if (is_array($arrParam)) {
            $boletos = implode(',', $arrParam);
            $sql .= " AND boleto.bol_id in ( " . $boletos . ')';
        } elseif (is_string($arrParam)) {
            $sql .= " AND  boleto.bol_id in ( " . $arrParam . ')';
        } else {
            $this->setLastError("Dados inválidos para pesquisa!");

            return false;
        }

        $sql .= ' GROUP BY ft.titulo_id';
        $sql .= ' ORDER BY ft.titulo_id';

        $result = $this->executeQuery($sql)->fetchAll();

        return $result;
    }

    public function retornaBoletosContidosNaRemessa($remessaId)
    {
        $sql = "SELECT bol_id FROM financeiro__remessa_lote WHERE remessa_id=:remessa_id";

        $result = $this->executeQueryWithParam($sql, ['remessa_id' => $remessaId])->fetchAll(\PDO::FETCH_COLUMN);

        return $result;
    }

    public function retornaArrayConfiguracao(\Financeiro\Entity\BoletoConfConta $objConfConta)
    {
        $pessoa   = new \Pessoa\Service\Pessoa($this->getEm());
        $arrDados = $pessoa->getArray($objConfConta->getPes()->getPesId());

        $agenciaMaisCedenteDv = $objConfConta->getConfcontAgencia() . $objConfConta->getConfcontConta();
        $agenciaMaisCedenteDv = Util::modulo11($agenciaMaisCedenteDv, 9);
        $agenciaMaisCedenteDv = $agenciaMaisCedenteDv > 9 || $agenciaMaisCedenteDv == "P" ? 0 : $agenciaMaisCedenteDv;

        $numeroSequencial = $this->getRemessaCode($objConfConta->getConfcontId());
        $numeroSequencial = max($numeroSequencial, $objConfConta->getConfcontRemessaSequencial());

        $arrConfiguracao = array(
            'data_geracao'               => new \DateTime('now'),
            'data_gravacao'              => new \DateTime('now'),
            'nome_fantasia'              => $arrDados['pesNomeFantasia'],
            'razao_social'               => $arrDados['pesNome'],
            'tipo_inscricao'             => $arrDados['pesTipoNumero'],
            'cpf_cnpj'                   => preg_replace('[^0-9]', '', $arrDados['cpfCnpj']),
            'banco'                      => $objConfConta->getBanc()->getBancCodigo(),
            'logradouro'                 => $arrDados['endLogradouro'],
            'numero'                     => $arrDados['endNumero'],
            'bairro'                     => $arrDados['endBairro'],
            'cidade'                     => $arrDados['endCidade'],
            'uf'                         => $arrDados['endEstadoUF'],
            'cep'                        => $arrDados['endCep'],
            'agencia'                    => $objConfConta->getConfcontAgencia(),
            'agencia_dv'                 => $objConfConta->getConfcontAgenciaDigito(),
            'codigo_cedente'             => $objConfConta->getConfcontConta(),
            'codigo_cedente_dv'          => $objConfConta->getConfcontContaDigito(),
            'conta'                      => $objConfConta->getConfcontConta(),
            'conta_dv'                   => $objConfConta->getConfcontContaDigito(),
            'conta_dac'                  => '',
            'numero_sequencial_arquivo'  => $numeroSequencial,
            'cobranca_cedente'           => $agenciaMaisCedenteDv,
            'agencia_mais_cedente_dv'    => $agenciaMaisCedenteDv,
            'codigo_convenio'            => $objConfConta->getConfcontConvenio(),
            'numero_carteira_cobranca'   => $objConfConta->getConfcontCarteira(),
            'variacao_carteira_cobranca' => $objConfConta->getConfcontVariacao(),
        );

        if ($objConfConta->getBanc()->getBancCodigo() == \Cnab\Banco::BANCO_DO_BRASIL) {
            $arrConfiguracao['numero_contrato_operacao_credito'] = $objConfConta->getConfcontContrato();
            $arrConfiguracao['identificacao_emissao_bloqueto']   = 4;
            $arrConfiguracao['identificacao_distribuicao']       = 2;
        }

        if ($objConfConta->getBanc()->getBancCodigo() == \Cnab\Banco::SICOOB) {
            $arrConfiguracao['agencia_mais_cedente_dv'] = '';
        }

        return $arrConfiguracao;
    }

    public function retornaArrayDetalhe($arrTitulo, \Financeiro\Entity\BoletoConfConta $objConfConta)
    {
        $arrDescontosCalc = [];
        $x                = 0;

        $dataAtual = date('Ymd');

        if ($arrTitulo['valor_desconto_antecipado1'] && $arrTitulo['data_desc_antecipado_formatada_1'] >= $dataAtual) {
            $arrDescontosCalc[$x]['data']     = $arrTitulo['data_desc_antecipado_formatada_1'];
            $arrDescontosCalc[$x]['desconto'] = $arrTitulo['valor_desconto_antecipado1'];

            if ($arrTitulo['desconto_limita_vencimento'] && $arrTitulo['data_desc_antecipado_formatada_1'] <= $arrTitulo['titulo_data_vencimento']) {
                $arrDescontosCalc[$x]['desconto'] += $arrTitulo['desconto_limita_vencimento'];
            }

            $x++;
        } else {
            $arrTitulo['data_desc_antecipado_formatada_1'] = null;
        }

        if ($arrTitulo['valor_desconto_antecipado2'] && $arrTitulo['data_desc_antecipado_formatada_2'] >= $dataAtual) {
            $arrDescontosCalc[$x]['data']     = $arrTitulo['data_desc_antecipado_formatada_2'];
            $arrDescontosCalc[$x]['desconto'] = $arrTitulo['valor_desconto_antecipado2'];

            if ($arrTitulo['desconto_limita_vencimento'] && $arrTitulo['data_desc_antecipado_formatada_2'] <= $arrTitulo['titulo_data_vencimento']) {
                $arrDescontosCalc[$x]['desconto'] += $arrTitulo['desconto_limita_vencimento'];
            }

            $x++;
        } else {
            $arrTitulo['data_desc_antecipado_formatada_2'] = null;;
        }

        if (
            $arrTitulo['desconto_limita_vencimento'] &&
            $arrTitulo['data_desc_antecipado_formatada_1'] != $arrTitulo['titulo_data_vencimento'] &&
            $arrTitulo['data_desc_antecipado_formatada_2'] != $arrTitulo['titulo_data_vencimento']
        ) {
            $arrDescontosCalc[$x]['data']     = $arrTitulo['titulo_data_vencimento'];
            $arrDescontosCalc[$x]['desconto'] = $arrTitulo['desconto_limita_vencimento'];
        }

        usort(
            $arrDescontosCalc,
            function ($a, $b) {
                return $a['data'] > $b['data'];
            }
        );

        $tamanho = count($arrDescontosCalc);

        foreach ($arrDescontosCalc as $index => $value) {
            $x = $index + 1;

            while ($x <= $tamanho) {
                if ($value['data'] == $arrDescontosCalc[$x]['data']) {
                    $arrDescontosCalc[$index]['desconto'] += $arrDescontosCalc[$x]['desconto'];
                    unset($arrDescontosCalc[$x]);
                    $tamanho--;
                }
                $x++;
            }
        }

        $arrDescontos = [];

        foreach ($arrDescontosCalc as $value) {
            $arrDescontos[] = $value;
        }

        /*ORDENAR E SOMAR OS DESCONTOS COM A MESMA DATA, MUDAR O VALOR DO TITULO PARA REMASSA PARA PEGAR O VALOR LIQUIDO*/

        $cod_desc_01   = $arrDescontos[0] ? 1 : null;
        $data_desc_01  = $arrDescontos[0] ? $arrDescontos[0]['data'] : 0;
        $valor_desc_01 = $arrDescontos[0] ? $arrDescontos[0]['desconto'] : 0;

        $cod_desc_02   = $arrDescontos[1] ? 1 : null;
        $data_desc_02  = $arrDescontos[1] ? $arrDescontos[1]['data'] : 0;
        $valor_desc_02 = $arrDescontos[1] ? $arrDescontos[1]['desconto'] : 0;

        $cod_desc_03   = $arrDescontos[2] ? 1 : null;
        $data_desc_03  = $arrDescontos[2] ? $arrDescontos[2]['data'] : 0;
        $valor_desc_03 = $arrDescontos[2] ? $arrDescontos[2]['desconto'] : 0;

        $data_desc_01 = $data_desc_01 ? str_replace('/', '', $data_desc_01) : 00000000;
        $data_desc_02 = $data_desc_02 ? str_replace('/', '', $data_desc_02) : 00000000;
        $data_desc_03 = $data_desc_03 ? str_replace('/', '', $data_desc_03) : 00000000;

        $tamNossoNumero = 10;

        if ($objConfConta->getBanc()->getBancCodigo() == \Cnab\Banco::BANCO_DO_BRASIL) {
            $tamNossoNumero = 10;
        } elseif ($objConfConta->getBanc()->getBancCodigo() == \Cnab\Banco::BRADESCO) {
            $tamNossoNumero = 7;
        } elseif ($objConfConta->getBanc()->getBancCodigo() == \Cnab\Banco::CEF) {
            $tamNossoNumero = 8;
        } elseif ($objConfConta->getBanc()->getBancCodigo() == \Cnab\Banco::SICOOB) {
            $tamNossoNumero = 10;
        }

        $nossoNumeroProcessado = (int)$arrTitulo["bol_nosso_numero"];
        $nossoNumeroProcessado = str_pad($nossoNumeroProcessado, $tamNossoNumero, '0', STR_PAD_LEFT);
        $carteira              = '';
        $uf                    = \Pessoa\Service\Estado::nomeUF($arrTitulo['end_estado']);
        $logradouro            = trim(
            ($arrTitulo["end_logradouro"] . ', ' . $arrTitulo["end_numero"] . ', ' . $arrTitulo["end_complemento"]),
            ', .'
        );
        $pesCpfCnpj            = preg_replace('/[^0-9]/', '', $arrTitulo["pes_documento"]);

        if (isset($arrTitulo["bol_carteira"])) {
            $carteira = $arrTitulo["bol_carteira"];
        } elseif (isset($arrTitulo["confcont_carteira"])) {
            $carteira = $arrTitulo["confcont_carteira"];
        }

        $percentualMulta = number_format($arrTitulo['tituloconf_multa'], 6);
        $percentualJuros = number_format($arrTitulo['tituloconf_juros'], 6);

        $multa = number_format($arrTitulo['valor_liquido'] * $percentualMulta, 2);
        $juros = number_format(($arrTitulo['valor_liquido'] + $multa) * $percentualJuros, 2);

        $arrDetalhe = array(
            'aceite'                  => 'A', #falta no bradesco
            'carteira'                => $carteira[0],
            'modalidade'              => str_pad($carteira, 2, '0', STR_PAD_LEFT),
            'modalidade_carteira'     => $carteira,
            //Independente se a carteira for 24 ou 14 a aorma de cadastramento do título no banco é registrada
            'registrado'              => 1,
            'codigo_juros_mora'       => '1',
            'codigo_ocorrencia'       => $arrTitulo['codigo_ocorrencia'],
            'data_cadastro'           => $arrTitulo['titulo_data_processamento'],
            'data_vencimento'         => $arrTitulo['titulo_data_vencimento'],
            'especie'                 => \Cnab\Especie::FEBRABAN_DUPLICATA_DE_SERVICO,
            'instrucao1'              => 2,
            'instrucao2'              => 0,
            //TODO: configurar multa pela configuração de título
            'juros_de_um_dia'         => $juros,
            'mensagem'                => '',
            'nosso_numero'            => $nossoNumeroProcessado,
            'numero_documento'        => $arrTitulo["bol_numero_documento"],
            'parcela'                 => 1,
            'prazo'                   => $arrTitulo["prazo"],
            'prazo_protesto'          => 0,
            'sacado_codigo_inscricao' => $arrTitulo["pes_tipo_numero"],
            'sacado_tipo'             => $arrTitulo["pes_tipo_numero"] == 1 ? 'cpf' : 'cnpj',
            'tipo_inscricao'          => $arrTitulo["pes_tipo_numero"],
            'sacado_nome'             => $arrTitulo['pes_nome'],
            'sacador_nome'            => $arrTitulo['pes_nome'],
            'sacado_logradouro'       => $logradouro,
            'sacado_bairro'           => $arrTitulo['end_bairro'],
            'sacado_cep'              => $arrTitulo['end_cep'],
            'sacado_cidade'           => $arrTitulo['end_cidade'],
            'sacado_uf'               => $uf,
            //TODO: configurar tempo de permanencia pela configuração de título
            'taxa_de_permanencia'     => '00',
            'valor'                   => $arrTitulo['valor_liquido'],
            //TODO: configurar multa pela configuração de título
            'data_multa'              => $arrTitulo['titulo_data_vencimento'],
            'valor_multa'             => $multa,
            //desconto 1
            'cod_desc_1'              => $cod_desc_01,
            'data_desconto'           => $data_desc_01,
            'valor_desconto'          => $valor_desc_01,
            //desconto 2
            'cod_desc_2'              => $cod_desc_02,
            'data_desconto_2'         => $data_desc_02,
            'valor_desconto_2'        => $valor_desc_02,
            //desconto 3
            'cod_desc_3'              => $cod_desc_03,
            'data_desconto_3'         => $data_desc_03,
            'valor_desconto_3'        => $valor_desc_03,
        );

        if ($arrTitulo["pes_tipo_numero"] == 1) {
            $arrDetalhe['sacado_cpf'] = $pesCpfCnpj;
        } else {
            $arrDetalhe['sacado_cnpj'] = $pesCpfCnpj;
        }

        if ($_GET["identificacao_emissao"]) {
            $arrDetalhe['identificacao_emissao'] = $_GET["identificacao_emissao"];
        }

        $entradaDeTitulos    = ($arrTitulo['lote_movimento'] == \Cnab\Movimento::REMESSA_ENTRADA_DE_TITULO);
        $alteracaoVencimento = ($arrTitulo['codigo_ocorrencia'] == \Cnab\Movimento::REMESSA_ALTERACAO_DE_VENCIMENTO);

        //Verifica se o boleto já foi incluso na remessa e se o movimento anterior foi de inclusão
        if ($entradaDeTitulos && !$alteracaoVencimento && $arrTitulo['remessa_id']) {
            if ($arrTitulo['valor_liquido'] != $arrTitulo['titulo_valor']) {
                $arrDetalhe['codigo_ocorrencia'] = \Cnab\Movimento::REMESSA_ALTERACAO_DO_VALOR_NOMINAL_DO_TITULO;
            } else {
                $arrDetalhe['codigo_ocorrencia'] = \Cnab\Movimento::REMESSA_ALTERACAO_DE_OUTROS_DADOS;

                if ($objConfConta->getBanc()->getBancCodigo() != \Cnab\Banco::SICOOB) {
                    $arrDetalhe['identificacao_emissao'] = 5;
                }
            }
        }

        if ($objConfConta->getBanc()->getBancCodigo() == \Cnab\Banco::BANCO_DO_BRASIL) {
            $numeroConvenio             = str_pad((int)$arrTitulo["confcont_convenio"], 7, '0', STR_PAD_LEFT);
            $nossoNumero                = str_pad((int)$arrTitulo["bol_nosso_numero"], 10, '0', STR_PAD_LEFT);
            $nossoNumeroProcessado      = $numeroConvenio . $nossoNumero;
            $arrDetalhe['nosso_numero'] = "$nossoNumeroProcessado";
        } elseif ($objConfConta->getBanc()->getBancCodigo() == \Cnab\Banco::SICOOB) {
            $numeroCliente         = str_pad((int)$arrTitulo["confcont_convenio"], 10, '0', STR_PAD_LEFT);
            $nossoNumeroProcessado = str_pad((int)$arrTitulo["bol_nosso_numero"], 7, '0', STR_PAD_LEFT);
            $numeroCooperativa     = str_pad((int)$arrTitulo["confcont_agencia"], 4, '0', STR_PAD_LEFT);
            $sequencia             = ($numeroCooperativa . $numeroCliente . $nossoNumeroProcessado);
            $dvNossoNumero         = \PhpBoletoZf2\Lib\Util::digitoVerificadorNossoNumeroBancoob($sequencia, '3197');
            $parcelaTitulo         = '01';
            $modalidade            = str_pad((int)$carteira, 2, '0', STR_PAD_LEFT);
            $numTitulo             = str_pad((int)$nossoNumeroProcessado . $dvNossoNumero, 10, '0', STR_PAD_LEFT);
            $posicaoEmBranco       = '    ';
            $tipoPapel             = 4;

            $arrDetalhe['nosso_numero']            = "$numTitulo$parcelaTitulo$modalidade$tipoPapel$posicaoEmBranco";
            $arrDetalhe['agencia_mais_cedente_dv'] = "";
        } elseif ($objConfConta->getBanc()->getBancCodigo() == \Cnab\Banco::BRADESCO) {
            $dac = $carteira;
            $dac .= str_pad($arrTitulo['titulo_id'], 11, '0', STR_PAD_LEFT);
            $dac = \PhpBoletoZf2\Lib\Util::modulo11($dac, 7);
            $dac = $dac == 'X' ? 'P' : $dac;

            $arrDetalhe['codigo_banco']        = 000;
            $arrDetalhe['digito_nosso_numero'] = $dac;
            $arrDetalhe['zeros02']             = 000;
        }

        return $arrDetalhe;
    }

    public function gerarRemessa(array $arrData)
    {
        $serviceArquivoDiretorios = new \GerenciadorArquivos\Service\ArquivoDiretorios($this->getEm());
        $serviceAcessoPessoas     = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceBoletoConfConta   = new \Financeiro\Service\BoletoConfConta($this->getEm());
        $serviceFinanceiroTitulo  = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceRemessaLote       = new \Financeiro\Service\FinanceiroRemessaLote($this->getEm());

        $cobrancaOnline      = true;
        $arrChavesDownload   = array();
        $arrTitulosInseridos = array();
        $arrBoletosInseridos = array();
        $arrNaoGerados       = array();
        $messages            = array();

        if (!$arrData["confContaId"]) {
            $this->setLastError('A conta não foi informada!');

            return false;
        }

        /** @var \Financeiro\Entity\BoletoConfConta $objConfConta */
        $objConfConta    = $serviceBoletoConfConta->getRepository()->find($arrData['confContaId']);
        $objBanco        = $objConfConta->getBanc();
        $codigoBanco     = $objBanco->getBancCodigo() ? $objBanco->getBancCodigo() : null;
        $codigoMovimento = \Cnab\Movimento::REMESSA_ENTRADA_DE_TITULO;

        if ($arrData['codigoMovimento']) {
            $codigoMovimento = $arrData['codigoMovimento'];
        }

        $prazo = $this->getConfig()->localizarChave('PRAZO_MAXIMO_DIAS_BAIXA_REMESSA', 60);

        $arrBancos = array(\Cnab\Banco::SICOOB, \Cnab\Banco::CEF, \Cnab\Banco::BRADESCO, \Cnab\Banco::BANCO_DO_BRASIL);

        if (!in_array($codigoBanco, $arrBancos)) {
            $this->setLastError('Tratamento para remessa do banco ' . $codigoBanco . ' ainda não foi implementado!');

            return false;
        }

        $objDiretorioRemessa = $serviceArquivoDiretorios->retornaDiretorioPorNome(self::NOME_DIRETORIO);

        if (!$objDiretorioRemessa) {
            $this->setLastError('Diretório de remessas ainda não foi criado!');

            return false;
        }

        $serviceArquivo = new \GerenciadorArquivos\Service\Arquivo($this->getEm(), $objDiretorioRemessa);

        $this->registrarLog("Inicio pesquisa dados");

        /** @var array $arrDadosGerais */
        $arrDadosGerais = $this->retornaDadosRemessa($arrData['boletos']);

        if (!$arrDadosGerais) {
            $this->setLastError('Nenhum título disponível!');

            return false;
        }

        $this->registrarLog("Fim pesquisa dados");

        $cedente = $objConfConta->getConfcontConta();

        if ($objConfConta->getConfcontConvenio()) {
            $cedente = $objConfConta->getConfcontConvenio();
        }

        $nomeArquivo = (
            'BANCO_' . $codigoBanco .
            '.CEDENTE_' . $cedente .
            ".DATA_" . date("Y-m-d_H-i-s")
        );

        $arrUsuario   = $serviceAcessoPessoas->retornaUsuarioLogado()->toArray();
        $pessoaLogada = $arrUsuario['pesFisica'] ? $arrUsuario['pesFisica'] : $arrUsuario['pesJuridica'];

        $arrDividido       = [$arrDadosGerais];
        $cobrancaOnlineTam = $this->getConfig()->localizarChave('QUANTIDADE_MAXIMA_BOLETOS_REMESSA', 950);

        if (count($arrDadosGerais) > $cobrancaOnlineTam && $cobrancaOnline) {
            $total       = ceil(count($arrDadosGerais) / $cobrancaOnlineTam);
            $arrDividido = [];

            for ($i = 0; $i < $total; $i++) {
                $arrDividido[] = array_slice($arrDadosGerais, $i * $cobrancaOnlineTam, $cobrancaOnlineTam);
            }
        }

        foreach ($arrDividido as $parte => $arrDadosGerais) {
            try {
                $this->registrarLog("Inicio tratamento geração de remessa " . $parte . " de " . count($arrDividido));
                $arrBoletosInseridosRemessa = array();
                $arrTitulosInseridos        = array();
                $arrConfiguracao            = $this->retornaArrayConfiguracao($objConfConta);
                $remessaCode                = $arrConfiguracao['numero_sequencial_arquivo'];

                $arquivoCnab = new \Cnab\Remessa\Cnab240\Arquivo($codigoBanco);
                $arquivoCnab->configure($arrConfiguracao);

                foreach ($arrDadosGerais as $arrTitulo) {
                    $arrTitulo['codigo_ocorrencia'] = $codigoMovimento;
                    $arrTitulo['prazo']             = $prazo;

                    if (!$serviceFinanceiroTitulo->incluirTituloNaRemessa($arrTitulo['tituloId'], false)) {
                        $this->setLastError($serviceFinanceiroTitulo->getLastError());

                        return false;
                    }

                    $boletoId = $arrTitulo["boleto"];
                    $this->registrarLog("Inicio tratamento boleto " . $boletoId);
                    $arrDetalhe = $this->retornaArrayDetalhe($arrTitulo, $objConfConta);

                    if (!$arrDetalhe['sacado_cep'] || (int)$arrDetalhe['sacado_cep'] == 0) {
                        $arrDetalhe['sacado_cep'] = $arrConfiguracao['cep'];
                    }

                    $valid = $this->valida($arrDetalhe);

                    if (!$valid) {
                        $arquivoCnab->insertDetalhe($arrDetalhe);
                        $arrBoletosInseridos[]        = $boletoId;
                        $arrBoletosInseridosRemessa[] = [
                            'bol'       => $boletoId,
                            'movimento' => $arrDetalhe['codigo_ocorrencia']
                        ];
                        $arrTitulosInseridos[]        = $arrTitulo['tituloId'];
                    } else {
                        $arrNaoGerados[$arrTitulo["titulo_id"]] = $valid;
                    }

                    $this->registrarLog("Fim tratamento boleto " . $boletoId);
                }

                $this->registrarLog("Inicio gravar arquivo");
                //Não inseriu nenhum registro na remessa
                if (!$arrBoletosInseridosRemessa) {
                    continue;
                }

                $this->getEm()->getConnection()->beginTransaction();
                $objfileRemessa = null;

                //BANCO_{BANCO}.CONTA_{CONTA}.DATA_{DATAHORA}.PARTE_{PARTE}.REMESSA_{REMESA}
                $fileName = $nomeArquivo . '.PARTE_' . ($parte + 1) . '.REMESSA_' . $remessaCode . ".rem";
                $filePath = $arquivoCnab->save(sys_get_temp_dir() . DIRECTORY_SEPARATOR . $fileName);

                $this->registrarLog("Fim gravar arquivo");
                $this->registrarLog("Inicio do registro da remessa base de dados");

                try {
                    $arrArquivo = [
                        'tmp_name' => $filePath,
                        'name'     => $fileName,
                        'type'     => 'text/plain',
                        'size'     => filesize($filePath),
                        'pessoa'   => $pessoaLogada,
                    ];
                    /** @var \GerenciadorArquivos\Entity\Arquivo $objfileRemessa */
                    $objfileRemessa = $serviceArquivo->adicionar($arrArquivo);

                    $arrChavesDownload[] = [
                        'name'  => $objfileRemessa->getArqNome(),
                        'chave' => $objfileRemessa->getArqChave()
                    ];
                } catch (\Exception $e) {
                    $messages[] = ['type' => "danger", 'message' => 'Erro ao salvar arquivo de remessa!'];
                    $this->registrarLog("Falha no registro da remessa na base de dados");
                }

                $this->registrarLog("Fim no registro da remessa na base de dados");

                if (!$objfileRemessa) {
                    continue;
                }

                $this->registrarLog("Inicio no registro dos detalhes da remessa na base de dados");

                try {
                    $objRemessa = $this->save(
                        [
                            'arq'           => $objfileRemessa,
                            'confCont'      => $objConfConta,
                            'remessaData'   => new \DateTime('now'),
                            'remessaCodigo' => $remessaCode,
                        ]
                    );

                    $this->getEm()->persist($objRemessa);
                    $this->getEm()->flush($objRemessa);

                    if (!$serviceFinanceiroTitulo->incluirTituloNaRemessa($arrTitulosInseridos, false)) {
                        throw new \Exception($serviceFinanceiroTitulo->getLastError());
                    }

                    foreach ($arrBoletosInseridosRemessa as $arrBoleto) {
                        //Somente um lote por arquivo
                        $lote    = 1;
                        $arrLote = [
                            'bol'           => $arrBoleto['bol'],
                            'remessa'       => $objRemessa,
                            'loteCodigo'    => $lote,
                            'loteMovimento' => $arrBoleto['movimento'],
                        ];

                        $salvou = $serviceRemessaLote->save($arrLote);

                        if (!$salvou) {
                            throw new \Exception($serviceRemessaLote->getLastError());
                        }
                    }
                } catch (\Exception $ex) {
                    $err = $ex->getMessage();
                    $this->registrarLog("Falha no registro dos detalhes da remessa na base de dados");

                    try {
                        // se não foi possível salvar os dados de remessa no banco, exclui o arquivo gerado se houver gerado
                        if ($objfileRemessa) {
                            $serviceArquivo->excluir($objfileRemessa->getArqId());
                        }
                    } catch (\Exception $ex2) {
                    }

                    $messages[] = [
                        'type'    => "error",
                        'message' => 'Erro ao registrar os boletos inclusos no arquivo! ' . $err
                    ];
                }

                $this->registrarLog("Fim do registro dos detalhes da remessa na base de dados");

                $this->getEm()->getConnection()->commit();

                $this->registrarLog(
                    "Finalizando tratamento geração de remessa " . $parte . " de " . count($arrDividido)
                );
            } catch (\Exception $ex) {
                $messages[] = [
                    'type'    => "error",
                    'message' => 'Erro ao registrar os boletos inclusos no arquivo! ' . $ex->getMessage()
                ];
            }
        }

        if (!$arrChavesDownload && !$arrNaoGerados && !$messages && !$arrBoletosInseridos) {
            $this->setLastError('Nenhuma informação foi tratada!');

            return false;
        }

        return [
            'chaveDownload' => $arrChavesDownload,
            'errors'        => $arrNaoGerados,
            'message'       => $messages,
            'inseridos'     => $arrBoletosInseridos,
        ];
    }
}