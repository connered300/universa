<?php

namespace Financeiro\Service;

use VersaSpine\Service\AbstractService;

class BoletoLayout extends AbstractService
{
    const LAYOUT_ESTADO_A = 'A';
    const LAYOUT_ESTADO_I = 'I';
    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Financeiro\Entity\BoletoLayout');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function pesquisaForJson($params)
    {
        $sql           = '
        SELECT layoutId as layout_id, layoutCaminho as layout_caminho, layoutVersao as layout_versao, layoutData as layout_data, layoutEstado as layout_estado
        FROM Financeiro\Entity\BoletoLayout
        WHERE';
        $layoutCaminho = false;
        $layoutId      = false;

        if ($params['q']) {
            $layoutCaminho = $params['q'];
        } elseif ($params['query']) {
            $layoutCaminho = $params['query'];
        }

        if ($params['layoutId']) {
            $layoutId = $params['layoutId'];
        }

        $parameters = array('layoutCaminho' => "{$layoutCaminho}%");
        $sql .= ' layoutCaminho LIKE :layoutCaminho';

        if ($layoutId) {
            $parameters['layoutId'] = explode(',', $layoutId);
            $parameters['layoutId'] = $layoutId;
            $sql .= ' AND layoutId NOT IN(:layoutId)';
        }

        $sql .= " ORDER BY layoutCaminho";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['layoutId']) {
                /** @var $objBoletoLayout \Financeiro\Entity\BoletoLayout */
                $objBoletoLayout = $this->getRepository()->find($arrDados['layoutId']);

                if (!$objBoletoLayout) {
                    $this->setLastError('Registro de layout não existe!');

                    return false;
                }
            } else {
                $objBoletoLayout = new \Financeiro\Entity\BoletoLayout();
            }

            $objBoletoLayout->setLayoutCaminho($arrDados['layoutCaminho']);
            $objBoletoLayout->setLayoutVersao($arrDados['layoutVersao']);
            $objBoletoLayout->setLayoutData($arrDados['layoutData']);
            $objBoletoLayout->setLayoutEstado($arrDados['layoutEstado']);

            $this->getEm()->persist($objBoletoLayout);
            $this->getEm()->flush($objBoletoLayout);

            $this->getEm()->commit();

            $arrDados['layoutId'] = $objBoletoLayout->getLayoutId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de layout!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['layoutCaminho']) {
            $errors[] = 'Por favor preencha o campo "caminho"!';
        }

        if (!$arrParam['layoutVersao']) {
            $errors[] = 'Por favor preencha o campo "versão"!';
        }

        if (!$arrParam['layoutEstado']) {
            $errors[] = 'Por favor preencha o campo "estado"!';
        }

        if (!in_array($arrParam['layoutEstado'], self::getLayoutEstado())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "estado"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public static function getLayoutEstado()
    {
        return array(self::LAYOUT_ESTADO_A, self::LAYOUT_ESTADO_I);
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM boleto_layout";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($layoutId)
    {
        /** @var $objBoletoLayout \Financeiro\Entity\BoletoLayout */
        $objBoletoLayout = $this->getRepository()->find($layoutId);

        try {
            $arrDados = $objBoletoLayout->toArray();
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('layoutId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Financeiro\Entity\BoletoLayout */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getLayoutId(),
                $params['value'] => $objEntity->getLayoutCaminho()
            );
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['layoutId']) {
            $this->setLastError('Para remover um registro de layout é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objBoletoLayout \Financeiro\Entity\BoletoLayout */
            $objBoletoLayout = $this->getRepository()->find($param['layoutId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objBoletoLayout);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de layout.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $view->setVariable("arrLayoutEstado", $this->getArrSelect2LayoutEstado());
    }

    public function getArrSelect2LayoutEstado($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getLayoutEstado());
    }
}
?>