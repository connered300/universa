<?php

namespace Financeiro\Service;

use VersaSpine\Service\AbstractService;

class FinanceiroTituloCheque extends AbstractService
{
    const TITULOCHEQUE_ESTADO_ATIVO   = 'Ativo';
    const TITULOCHEQUE_ESTADO_INATIVO = 'Inativo';
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2TitulochequeEstado($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getTitulochequeEstado());
    }

    public static function getTitulochequeEstado()
    {
        return array(self::TITULOCHEQUE_ESTADO_ATIVO, self::TITULOCHEQUE_ESTADO_INATIVO);
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Financeiro\Entity\FinanceiroTituloCheque');
    }

    public function pesquisaForJson($params)
    {
        $sql            = '
        SELECT a.titulochequeId AS titulocheque_id, a.tituloId AS titulo_id, a.pagamentoId AS pagamento_id, a.chequeId AS cheque_id, a.titulochequeValor AS titulocheque_valor, a.titulochequeEstado AS titulocheque_estado
        FROM Financeiro\Entity\FinanceiroTituloCheque a
        WHERE';
        $tituloId       = false;
        $titulochequeId = false;

        if ($params['q']) {
            $tituloId = $params['q'];
        } elseif ($params['query']) {
            $tituloId = $params['query'];
        }

        if ($params['titulochequeId']) {
            $titulochequeId = $params['titulochequeId'];
        }

        $parameters = array('tituloId' => "{$tituloId}%");
        $sql .= ' a.tituloId LIKE :tituloId';

        if ($titulochequeId) {
            $parameters['titulochequeId'] = explode(',', $titulochequeId);
            $parameters['titulochequeId'] = $titulochequeId;
            $sql .= ' AND a.titulochequeId NOT IN(:titulochequeId)';
        }

        $sql .= " ORDER BY a.tituloId";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceFinanceiroTitulo    = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceFinanceiroPagamento = new \Financeiro\Service\FinanceiroPagamento($this->getEm());
        $serviceFinanceiroCheque    = new \Financeiro\Service\FinanceiroCheque($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['titulochequeId']) {
                /** @var $objFinanceiroTituloCheque \Financeiro\Entity\FinanceiroTituloCheque */
                $objFinanceiroTituloCheque = $this->getRepository()->find($arrDados['titulochequeId']);

                if (!$objFinanceiroTituloCheque) {
                    $this->setLastError('Registro de título cheque não existe!');

                    return false;
                }
            } else {
                $objFinanceiroTituloCheque = new \Financeiro\Entity\FinanceiroTituloCheque();
            }

            if ($arrDados['titulo']) {
                /** @var $objFinanceiroTitulo \Financeiro\Entity\FinanceiroTitulo */
                $objFinanceiroTitulo = $serviceFinanceiroTitulo->getRepository()->find($arrDados['titulo']);

                if (!$objFinanceiroTitulo) {
                    $this->setLastError('Registro de título não existe!');

                    return false;
                }

                $objFinanceiroTituloCheque->setTitulo($objFinanceiroTitulo);
            } else {
                $objFinanceiroTituloCheque->setTitulo(null);
            }

            if ($arrDados['pagamento']) {
                /** @var $objFinanceiroPagamento \Financeiro\Entity\FinanceiroPagamento */
                $objFinanceiroPagamento = $serviceFinanceiroPagamento->getRepository()->find($arrDados['pagamento']);

                if (!$objFinanceiroPagamento) {
                    $this->setLastError('Registro de pagamento não existe!');

                    return false;
                }

                $objFinanceiroTituloCheque->setPagamento($objFinanceiroPagamento);
            } else {
                $objFinanceiroTituloCheque->setPagamento(null);
            }

            if ($arrDados['cheque']) {
                /** @var $objFinanceiroCheque \Financeiro\Entity\FinanceiroCheque */
                $objFinanceiroCheque = $serviceFinanceiroCheque->getRepository()->find($arrDados['cheque']);

                if (!$objFinanceiroCheque) {
                    $this->setLastError('Registro de cheque não existe!');

                    return false;
                }

                $objFinanceiroTituloCheque->setCheque($objFinanceiroCheque);
            } else {
                $objFinanceiroTituloCheque->setCheque(null);
            }

            $objFinanceiroTituloCheque->setTitulochequeValor($arrDados['titulochequeValor']);
            $objFinanceiroTituloCheque->setTitulochequeEstado($arrDados['titulochequeEstado']);

            $this->getEm()->persist($objFinanceiroTituloCheque);
            $this->getEm()->flush($objFinanceiroTituloCheque);

            $this->getEm()->commit();

            $arrDados['titulochequeId'] = $objFinanceiroTituloCheque->getTitulochequeId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de título cheque!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['cheque']) {
            $errors[] = 'Por favor preencha o campo "código cheque"!';
        }

        if (!$arrParam['titulochequeEstado']) {
            $errors[] = 'Por favor preencha o campo "estado de"!';
        }

        if (!in_array($arrParam['titulochequeEstado'], self::getTitulochequeEstado())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "estado de"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function inativarChequesVinculadosAoTitulo($tituloId)
    {
        try {
            $query = "
            SELECT titulocheque_id
            FROM financeiro__titulo_cheque c
            LEFT JOIN financeiro__pagamento p USING(pagamento_id)
            WHERE c.titulo_id=:titulo OR p.titulo_id=:titulo";

            $arrTitulocheque = $this->executeQueryWithParam($query, ['titulo' => $tituloId])->fetchAll(
                \PDO::FETCH_COLUMN
            );

            if ($arrTitulocheque) {
                $queryUpdt = "
                UPDATE financeiro__titulo_cheque c
                SET titulocheque_estado=:estado
                WHERE titulocheque_id IN(:titulocheque)";

                $this->executeQueryWithParam(
                    $queryUpdt,
                    ['estado' => self::TITULOCHEQUE_ESTADO_INATIVO, 'titulocheque' => $arrTitulocheque]
                );
            }

            return true;
        } catch (\Exception $ex) {
        }

        return false;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM financeiro__titulo_cheque";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($titulochequeId)
    {
        /** @var $objFinanceiroTituloCheque \Financeiro\Entity\FinanceiroTituloCheque */
        $objFinanceiroTituloCheque  = $this->getRepository()->find($titulochequeId);
        $serviceFinanceiroTitulo    = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceFinanceiroPagamento = new \Financeiro\Service\FinanceiroPagamento($this->getEm());
        $serviceFinanceiroCheque    = new \Financeiro\Service\FinanceiroCheque($this->getEm());

        try {
            $arrDados = $objFinanceiroTituloCheque->toArray();

            if ($arrDados['titulo']) {
                $arrFinanceiroTitulo = $serviceFinanceiroTitulo->getArrSelect2(['id' => $arrDados['titulo']]);
                $arrDados['titulo']  = $arrFinanceiroTitulo ? $arrFinanceiroTitulo[0] : null;
            }

            if ($arrDados['pagamento']) {
                $arrFinanceiroPagamento = $serviceFinanceiroPagamento->getArrSelect2(['id' => $arrDados['pagamento']]);
                $arrDados['pagamento']  = $arrFinanceiroPagamento ? $arrFinanceiroPagamento[0] : null;
            }

            if ($arrDados['cheque']) {
                $arrFinanceiroCheque = $serviceFinanceiroCheque->getArrSelect2(['id' => $arrDados['cheque']]);
                $arrDados['cheque']  = $arrFinanceiroCheque ? $arrFinanceiroCheque[0] : null;
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceFinanceiroTitulo    = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceFinanceiroPagamento = new \Financeiro\Service\FinanceiroPagamento($this->getEm());
        $serviceFinanceiroCheque    = new \Financeiro\Service\FinanceiroCheque($this->getEm());

        if (is_array($arrDados['titulo']) && !$arrDados['titulo']['text']) {
            $arrDados['titulo'] = $arrDados['titulo']['tituloId'];
        }

        if ($arrDados['titulo'] && is_string($arrDados['titulo'])) {
            $arrDados['titulo'] = $serviceFinanceiroTitulo->getArrSelect2(array('id' => $arrDados['titulo']));
            $arrDados['titulo'] = $arrDados['titulo'] ? $arrDados['titulo'][0] : null;
        }

        if (is_array($arrDados['pagamento']) && !$arrDados['pagamento']['text']) {
            $arrDados['pagamento'] = $arrDados['pagamento']['pagamentoId'];
        }

        if ($arrDados['pagamento'] && is_string($arrDados['pagamento'])) {
            $arrDados['pagamento'] = $serviceFinanceiroPagamento->getArrSelect2(array('id' => $arrDados['pagamento']));
            $arrDados['pagamento'] = $arrDados['pagamento'] ? $arrDados['pagamento'][0] : null;
        }

        if (is_array($arrDados['cheque']) && !$arrDados['cheque']['text']) {
            $arrDados['cheque'] = $arrDados['cheque']['chequeId'];
        }

        if ($arrDados['cheque'] && is_string($arrDados['cheque'])) {
            $arrDados['cheque'] = $serviceFinanceiroCheque->getArrSelect2(array('id' => $arrDados['cheque']));
            $arrDados['cheque'] = $arrDados['cheque'] ? $arrDados['cheque'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['titulochequeId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['TituloId' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Financeiro\Entity\FinanceiroTituloCheque */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getTitulochequeId();
            $arrEntity[$params['value']] = $objEntity->getTituloId();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['titulochequeId']) {
            $this->setLastError('Para remover um registro de título cheque é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objFinanceiroTituloCheque \Financeiro\Entity\FinanceiroTituloCheque */
            $objFinanceiroTituloCheque = $this->getRepository()->find($param['titulochequeId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objFinanceiroTituloCheque);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de título cheque.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceFinanceiroTitulo    = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceFinanceiroPagamento = new \Financeiro\Service\FinanceiroPagamento($this->getEm());
        $serviceFinanceiroCheque    = new \Financeiro\Service\FinanceiroCheque($this->getEm());

        $serviceFinanceiroTitulo->setarDependenciasView($view);
        $serviceFinanceiroPagamento->setarDependenciasView($view);
        $serviceFinanceiroCheque->setarDependenciasView($view);

        $view->setVariable("arrTitulochequeEstado", $this->getArrSelect2TitulochequeEstado());
    }

    /**
     * @param $pagamento
     * @return bool
     */
    public function removeReferenciaPagamentoCheque($pagamento)
    {
        if (!$pagamento) {
            $this->setLastError(
                'Para remover um vínculos de pagamento dos cheques é necessário informar o código do pagamento.'
            );

            return false;
        }

        try {
            $this->getEm()->beginTransaction();
            $arrTituloCheques = $this->getRepository()->findBy(['pagamento' => $pagamento]);

            /** @var $objTituloCheque \Financeiro\Entity\FinanceiroTituloCheque */
            foreach ($arrTituloCheques as $objTituloCheque) {
                if ($objTituloCheque->getTitulo()) {
                    $objTituloCheque->setPagamento(null);
                    $this->getEm()->persist($objTituloCheque);
                    $this->getEm()->flush($objTituloCheque);
                } else {
                    $this->getEm()->remove($objTituloCheque);
                    $this->getEm()->flush();
                }
            }
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover vínculos de pagamentos de cheques.<br>' . $ex->getMessage());

            return false;
        }

        return true;
    }
}
?>