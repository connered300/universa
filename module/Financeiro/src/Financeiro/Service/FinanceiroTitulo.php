<?php

namespace Financeiro\Service;

use VersaSpine\Service\AbstractService;
use Vestibular\Entity\selecaoEdicaoFechamento;

/**
 * Class FinanceiroTitulo
 * @package Financeiro\Service
 */
class FinanceiroTitulo extends AbstractService
{
    const TITULO_TIPO_PAGAMENTO_DINHEIRO           = 'Dinheiro';
    const TITULO_TIPO_PAGAMENTO_BOLETO             = 'Boleto';
    const TITULO_TIPO_PAGAMENTO_ISENTO             = 'Isento';
    const TITULO_TIPO_PAGAMENTO_CHEQUE             = 'Cheque';
    const TITULO_TIPO_PAGAMENTO_DEPOSITO           = 'Deposito';
    const TITULO_TIPO_PAGAMENTO_CREDITO_FIES       = 'Credito Fies';
    const TITULO_ESTADO_PAGO                       = 'Pago';
    const TITULO_ESTADO_PAGAMENTO_MULTIPLO         = 'Pagamento Múltiplo';
    const TITULO_ESTADO_PAGAMENTO_MULTIPLO_EXTORNO = 'Estorno De Pagamento Múltiplo';
    const TITULO_ESTADO_ABERTO                     = 'Aberto';
    const TITULO_ESTADO_CANCELADO                  = 'Cancelado';
    const TITULO_ESTADO_ESTORNO                    = 'Estorno';
    const TITULO_ESTADO_ALTERACAO                  = 'Alteracao';
    const TITULO_INCLUIR_REMESSA_SIM               = 'Sim';
    const TITULO_INCLUIR_REMESSA_NAO               = 'Não';

    /**
     * @var null
     */
    private $__lastError = null;

    /** @var \Sistema\Service\SisConfig */
    private $config = null;

    /**
     * @return \Sistema\Service\SisConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param \Sistema\Service\SisConfig $config
     * @return FinanceiroTitulo
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     * @param array                       $arrConfig
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, $arrConfig = array())
    {
        parent::__construct($em, 'Financeiro\Entity\FinanceiroTitulo');

        $this->setConfig(new \Sistema\Service\SisConfig($em, $arrConfig));
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @param $params
     * @return array
     */
    public function pesquisaForJson($params)
    {
        $sql             = '
        SELECT a.tituloId AS titulo_id,
             a.pesId AS pes_id,
             a.tipotituloId AS tipotitulo_id,
             a.usuarioBaixa AS usuario_baixa,
             a.usuarioAutor AS usuario_autor,
             a.alunocursoId AS alunocurso_id,
             a.tituloNovo AS titulo_novo,
             a.tituloDescricao AS titulo_descricao,
             a.tituloParcela AS titulo_parcela,
             a.tituloDataProcessamento AS titulo_data_processamento,
             a.tituloDataVencimento AS titulo_data_vencimento,
             a.tituloTipoPagamento AS titulo_tipo_pagamento,
             a.tituloValor AS titulo_valor,
             a.tituloDataPagamento AS titulo_data_pagamento,
             a.tituloMulta AS titulo_multa,
             a.tituloJuros AS titulo_juros,
             a.tituloEstado AS titulo_estado,
             a.tituloValorPago AS titulo_valor_pago,
             a.tituloValorPagoDinheiro AS titulo_valor_pago_dinheiro,
             a.tituloDesconto AS titulo_desconto,
             a.tituloObservacoes AS titulo_observacoes,
             a.tituloDescontoManual AS titulo_desconto_manual,
             a.tituloAcrescimoManual AS titulo_acrescimo_manual
        FROM Financeiro\Entity\FinanceiroTitulo a
        WHERE';
        $tituloDescricao = false;
        $tituloId        = false;

        if ($params['q']) {
            $tituloDescricao = $params['q'];
        } elseif ($params['query']) {
            $tituloDescricao = $params['query'];
        }

        if ($params['tituloId']) {
            $tituloId = $params['tituloId'];
        }

        $parameters = array('tituloDescricao' => "{$tituloDescricao}%");
        $sql .= ' a.tituloDescricao LIKE :tituloDescricao';

        if ($tituloId) {
            $parameters['tituloId'] = explode(',', $tituloId);
            $parameters['tituloId'] = $tituloId;
            $sql .= ' AND a.tituloId NOT IN(:tituloId)';
        }

        $sql .= " ORDER BY a.tituloDataProcessamento";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados, $retornaObjTitulo = false)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $servicePessoa                 = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceAcessoPessoas          = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceFinanceiroTitulo       = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceAcadgeralAlunoCurso    = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
        $serviceAcadperiodoAluno       = new \Matricula\Service\AcadperiodoAluno($this->getEm());
        $serviceFinanceiroTituloTipo   = new \Financeiro\Service\FinanceiroTituloTipo($this->getEm());
        $serviceFinanceiroTituloConfig = new \Financeiro\Service\FinanceiroTituloConfig($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['tituloId']) {
                /** @var $objFinanceiroTitulo \Financeiro\Entity\FinanceiroTitulo */
                $objFinanceiroTitulo = $this->getRepository()->find($arrDados['tituloId']);

                if (!$objFinanceiroTitulo) {
                    $this->setLastError('Registro de título não existe!');

                    return false;
                }

                if ($this->tituloUsaMaxiPago($objFinanceiroTitulo)) {
                    return false;
                }
            } else {
                $objFinanceiroTitulo = new \Financeiro\Entity\FinanceiroTitulo();
            }

            if ($arrDados['pes']) {
                /** @var $objPessoa \Pessoa\Entity\Pessoa */
                $objPessoa = $servicePessoa->getRepository()->find($arrDados['pes']);

                if (!$objPessoa) {
                    $this->setLastError('Registro de pessoa não existe!');

                    return false;
                }

                $objFinanceiroTitulo->setPes($objPessoa);
            } else {
                $objFinanceiroTitulo->setPes(null);
            }

            if ($arrDados['tipotitulo']) {
                /** @var $objFinanceiroTituloTipo \Financeiro\Entity\FinanceiroTituloTipo */
                if (is_object($arrDados['tipotitulo'])) {
                    $objFinanceiroTituloTipo = $arrDados['tipotitulo'];
                } else {
                    $objFinanceiroTituloTipo = $serviceFinanceiroTituloTipo->getRepository()->find(
                        $arrDados['tipotitulo']
                    );
                }

                if (!$objFinanceiroTituloTipo) {
                    $this->setLastError('Registro de tipo de título não existe!');

                    return false;
                }

                $objFinanceiroTitulo->setTipotitulo($objFinanceiroTituloTipo);

                if (!$arrDados['tituloDescricao']) {
                    $arrDados['tituloDescricao'] = $objFinanceiroTituloTipo->getTipotituloNome();
                }
            } else {
                $objFinanceiroTitulo->setTipotitulo(null);
            }

            if ($arrDados['usuarioBaixa']) {
                /** @var $objAcessoPessoas \Acesso\Entity\AcessoPessoas */
                $objAcessoPessoas = $serviceAcessoPessoas->getRepository()->find($arrDados['usuarioBaixa']);

                if (!$objAcessoPessoas) {
                    $this->setLastError('Registro de pessoas não existe!');

                    return false;
                }

                $objFinanceiroTitulo->setUsuarioBaixa($objAcessoPessoas);
            } else {
                $objFinanceiroTitulo->setUsuarioBaixa(null);
            }

            /** @var $objAcessoPessoas \Acesso\Entity\AcessoPessoas */
            $objAcessoPessoas = $serviceAcessoPessoas->retornaUsuarioLogado($arrDados['usuarioAutor']);

            //TODO: Configurar sistema para identificar tipos de tíulos que possam vir a exigir usuário de cadastro
            /*
            if (!$objAcessoPessoas) {
                $this->setLastError('Registro de usuário não existe!');

                return false;
            }*/

            if ($arrDados['tituloNovo']) {
                /** @var $objFinanceiroTitulo \Financeiro\Entity\FinanceiroTitulo */
                $objFinanceiroTitulo = $serviceFinanceiroTitulo->getRepository()->find($arrDados['tituloNovo']);

                if (!$objFinanceiroTitulo) {
                    $this->setLastError('Registro de título não existe!');

                    return false;
                }

                $objFinanceiroTitulo->setTituloNovo($objFinanceiroTitulo);
            } else {
                $objFinanceiroTitulo->setTituloNovo(null);
            }

            if ($arrDados['alunocurso']) {
                /** @var $objAcadgeralAlunoCurso \Matricula\Entity\AcadgeralAlunoCurso */
                $objAcadgeralAlunoCurso = $serviceAcadgeralAlunoCurso->getRepository()->find($arrDados['alunocurso']);

                if (!$objAcadgeralAlunoCurso) {
                    $this->setLastError('Registro de aluno curso não existe!');

                    return false;
                }

                $objFinanceiroTitulo->setAlunocurso($objAcadgeralAlunoCurso);
                $arrDados['cursocampusId'] = $objAcadgeralAlunoCurso->getCursocampus()->getCursocampusId();
            } else {
                $objFinanceiroTitulo->setAlunocurso(null);
            }

            if ($arrDados['alunoper']) {
                /** @var $objAcadperiodoAluno \Matricula\Entity\AcadperiodoAluno */
                $objAcadperiodoAluno = $serviceAcadperiodoAluno->getRepository()->find($arrDados['alunoper']);

                if (!$objAcadperiodoAluno) {
                    $this->setLastError('Registro de aluno periodo não existe!');

                    return false;
                }

                $objFinanceiroTitulo->setAlunoPer($objAcadperiodoAluno);
            } else {
                $objFinanceiroTitulo->setAlunoPer(null);
            }

            if ($arrDados['tituloconf']) {
                /** @var $objFinanceiroTituloConfig \Financeiro\Entity\FinanceiroTituloConfig */
                $objFinanceiroTituloConfig = $serviceFinanceiroTituloConfig->getRepository()->find(
                    $arrDados['tituloconf']
                );

                if (!$objFinanceiroTituloConfig) {
                    $this->setLastError('Registro de configuraçao de título não existe!');
                }

                $objFinanceiroTitulo->setTituloconf($objFinanceiroTituloConfig);
            } else {
                $objFinanceiroTitulo->setTituloconf(null);

                if ($objTituloConfig = $serviceFinanceiroTituloConfig->pesquisarConfiguracao($arrDados)) {
                    $objFinanceiroTitulo->setTituloconf($objTituloConfig);
                }
            }

            if (!$objFinanceiroTitulo->getTituloconf()) {
                $this->setLastError('Configuração de título inexistente!');

                return false;
            }

            $arrDados['tituloParcela'] = (int)$arrDados['tituloParcela'];
            $arrDados['tituloParcela'] = $arrDados['tituloParcela'] ? $arrDados['tituloParcela'] : 1;

            $objFinanceiroTitulo->setUsuarioAutor($objAcessoPessoas);
            $objFinanceiroTitulo->setTituloDescricao($arrDados['tituloDescricao']);
            $objFinanceiroTitulo->setTituloParcela($arrDados['tituloParcela']);
            $objFinanceiroTitulo->setTituloDataProcessamento($arrDados['tituloDataProcessamento']);
            $objFinanceiroTitulo->setTituloDataVencimento($arrDados['tituloDataVencimento']);
            $objFinanceiroTitulo->setTituloTipoPagamento($arrDados['tituloTipoPagamento']);
            $objFinanceiroTitulo->setTituloValor($arrDados['tituloValor']);
            $objFinanceiroTitulo->setTituloDataBaixa($arrDados['tituloDataBaixa']);
            $objFinanceiroTitulo->setTituloDataPagamento($arrDados['tituloDataPagamento']);
            $objFinanceiroTitulo->setTituloMulta($arrDados['tituloMulta']);
            $objFinanceiroTitulo->setTituloJuros($arrDados['tituloJuros']);
            $objFinanceiroTitulo->setTituloEstado($arrDados['tituloEstado']);
            $objFinanceiroTitulo->setTituloPagseguroLink($arrDados['tituloPagseguroLink']);
            $objFinanceiroTitulo->setTituloValorPago($arrDados['tituloValorPago']);
            $objFinanceiroTitulo->setTituloValorPagoDinheiro($arrDados['tituloValorPagoDinheiro']);
            $objFinanceiroTitulo->setTituloDesconto($arrDados['tituloDesconto']);
            $objFinanceiroTitulo->setTituloObservacoes($arrDados['tituloObservacoes']);
            $objFinanceiroTitulo->setTituloDescontoManual($arrDados['tituloDescontoManual']);
            $objFinanceiroTitulo->setTituloAcrescimoManual($arrDados['tituloAcrescimoManual']);
            $objFinanceiroTitulo->setTituloComprovanteVia($arrDados['tituloComprovanteVia']);

            if ($arrDados['tituloIncluirRemessa']) {
                $objFinanceiroTitulo->setTituloIncluirRemessa($arrDados['tituloIncluirRemessa']);
            } else {
                $objFinanceiroTitulo->setTituloIncluirRemessa(self::TITULO_INCLUIR_REMESSA_SIM);
            }

            $this->getEm()->persist($objFinanceiroTitulo);
            $this->getEm()->flush($objFinanceiroTitulo);

            $this->getEm()->commit();

            $arrDados['tituloId'] = $objFinanceiroTitulo->getTituloId();

            if ($retornaObjTitulo) {
                return $objFinanceiroTitulo;
            }

            return true;
        } catch (\Exception $e) {
            $this->setLastError(
                'Não foi possível salvar o registro de título!<br>' . $this->getLastError() . '<br>' . $e->getMessage()
            );
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {

        $errors = array();

        if (!$arrParam['pes']) {
            $errors[] = 'Por favor preencha o campo "código pessoa"!';
        }

        if (!$arrParam['tipotitulo']) {
            $errors[] = 'Por favor preencha o campo "código tipo de título"!';
        }

        if (!$arrParam['tituloDataProcessamento']) {
            $errors[] = 'Por favor preencha o campo "processamento data"!';
        }

        if (!$arrParam['tituloDataVencimento']) {
            $errors[] = 'Por favor preencha o campo "vencimento data"!';
        }

        if ($arrParam['tituloTipoPagamento'] && array_diff(
                $arrParam['tituloTipoPagamento'],
                self::getTituloTipoPagamento()
            )
        ) {
            $errors[] = 'Por favor selecione valores válidos para o campo "pagamento tipo"!';
        }

        if (!$arrParam['tituloValor'] && !$arrParam['desabilitaVerificacaoValor']) {
            $errors[] = 'Por favor preencha o campo "valor"!';
        }

        if (!in_array($arrParam['tituloEstado'], self::getTituloEstado())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "estado"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public static function getTituloTipoPagamento()
    {
        return array(
            self::TITULO_TIPO_PAGAMENTO_DINHEIRO,
            self::TITULO_TIPO_PAGAMENTO_BOLETO,
            self::TITULO_TIPO_PAGAMENTO_ISENTO,
            self::TITULO_TIPO_PAGAMENTO_CHEQUE,
            self::TITULO_TIPO_PAGAMENTO_DEPOSITO,
            self::TITULO_TIPO_PAGAMENTO_CREDITO_FIES
        );
    }

    /**
     * @return array
     */
    public static function getTituloEstado()
    {
        return array(
            self::TITULO_ESTADO_PAGO,
            self::TITULO_ESTADO_ABERTO,
            self::TITULO_ESTADO_CANCELADO,
            self::TITULO_ESTADO_ESTORNO,
            self::TITULO_ESTADO_ALTERACAO,
            self::TITULO_ESTADO_PAGAMENTO_MULTIPLO,
            self::TITULO_ESTADO_PAGAMENTO_MULTIPLO_EXTORNO
        );
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $conditions = array();

        if ($data["filter"]['pesId'] == '-1') {
            return array(
                "draw"            => 0,
                "recordsFiltered" => 0,
                "recordsTotal"    => 0,
                "data"            => [],
            );
        }

        $conditionsTitulosVencidos = array();

        if ($data["filter"]['alunocursoId']) {
            $conditions[]                = "t.alunocurso_id IN(" . $data["filter"]['alunocursoId'] . ")";
            $conditionsTitulosVencidos[] = " t.alunocurso_id IN(" . $data["filter"]['alunocursoId'] . ") ";
        }

        if ($data["filter"]['pesId']) {
            $pesId       = $data["filter"]['pesId'];
            $queryPessoa = "(
                t.pes_id IN(" . $pesId . ") OR
                t.alunocurso_id IN(select alunocurso_id FROM acadgeral__aluno_curso INNER JOIN acadgeral__aluno USING(aluno_id) WHERE pes_id IN(" . $pesId . ")) OR
		        t.alunoper_id IN(select alunoper_id FROM acadperiodo__aluno INNER JOIN acadgeral__aluno_curso USING(alunocurso_id) INNER JOIN acadgeral__aluno USING(aluno_id) WHERE pes_id IN(" . $pesId . "))
            )";

            $conditions[]                = $queryPessoa;
            $conditionsTitulosVencidos[] = $queryPessoa;
        }

        if ($data["filter"]['tipotitulo']) {
            $conditions[] = "t.tipotitulo_id IN(" . $data["filter"]['tipotitulo'] . ")";
        }

        if ($data["filter"]['tituloId']) {
            $conditions[] = "t.titulo_id IN(" . implode(',', $data["filter"]['tituloId']) . ")";
        }

        if ($data["filter"]['tituloDataInicial']) {
            $tituloDataInicial = self::formatDateAmericano($data["filter"]['tituloDataInicial']);
            $conditions[]      = "t.titulo_data_vencimento >= '" . $tituloDataInicial . "'";
        }

        if ($data["filter"]['tituloDataFinal']) {
            $tituloDataFinal = self::formatDateAmericano($data["filter"]['tituloDataFinal']);
            $conditions[]    = "t.titulo_data_vencimento <= '" . $tituloDataFinal . "'";
        }

        if ($data["filter"]['tituloDataPagamentoInicial']) {
            $tituloDataPagamentoInicial = self::formatDateAmericano($data["filter"]['tituloDataPagamentoInicial']);
            $conditions[]               = "t.titulo_data_pagamento >= '" . $tituloDataPagamentoInicial . "'";
        }

        if ($data["filter"]['tituloDataPagamentoFinal']) {
            $tituloDataPagamentoFinal = self::formatDateAmericano($data["filter"]['tituloDataPagamentoFinal']);
            $conditions[]             = "t.titulo_data_pagamento <= '" . $tituloDataPagamentoFinal . "'";
        }

        if ($data["filter"]['tituloEstado']) {
            $tituloEstado = implode("','", $data["filter"]['tituloEstado']);
            $tituloEstado = str_replace(',', "','", $tituloEstado);
            $tituloEstado = preg_replace('/\'+/', "'", $tituloEstado);
            $conditions[] = "t.titulo_estado IN('" . $tituloEstado . "')";
        }

        $query = $this->retornaConsultaSQLTitulosComInformacoesExtras($conditions);

        if ($data['orderBy']) {
            $query .= ' ORDER BY ' . $data['orderBy'];
        }

        $totalizadores = '' .
            'sum(titulo_valor_calc) as total_final,' .
            'sum(titulo_multa_calc) as total_multa,' .
            'sum(titulo_desconto_calc) as total_desconto,' .
            'sum(titulo_juros_calc) as total_juros';

        $result = parent::paginationDataTablesAjax($query, $data, null, false, false, false, $totalizadores, 1);

        if ($conditionsTitulosVencidos) {
            $conditionsTitulosVencidos[] = 't.titulo_data_vencimento<NOW()';
            $conditionsTitulosVencidos[] = 't.titulo_estado="Aberto"';

            $queryTotalizadorVencidos = $this->retornaConsultaSQLTitulosComInformacoesExtras(
                $conditionsTitulosVencidos
            );

            $queryTotalizadorVencidos = "
            SELECT SUM(titulo_valor_calc) AS total_final_vencidos,
            count(titulo_id) qtd_titulos_vencidos
            FROM ( " . $queryTotalizadorVencidos . ")
            AS tatalizador";

            $arrRegistrosVencidos   = $this->executeQuery($queryTotalizadorVencidos)->fetch();
            $result['totalizacoes'] = array_merge($result['totalizacoes'], $arrRegistrosVencidos);
        }

        return $result;
    }

    /**
     * @param array $conditions
     * @return mixed|string
     */
    private function retornaConsultaSQLTitulosComInformacoesExtrasAntiga($conditions = array(), $conditions2 = array())
    {
        $serviceDescontoTitulo = new \Financeiro\Service\FinanceiroDescontoTitulo($this->getEm());
        $serviceAcessoPessoas  = new \Acesso\Service\AcessoPessoas($this->getEm());

        $formaCalculoIncentivo = $this->getConfig()->localizarChave('FINANCEIRO_CONFIG_CALCULO_DESCONTO_INCENTIVO');
        $formaCalculoIncentivo = (
        $formaCalculoIncentivo ? $formaCalculoIncentivo
            : $serviceDescontoTitulo::FINANCEIRO_CONFIG_CALCULO_DESCONTO_INCENTIVO_VALOR_BRUTO
        );

        $valorLiquido = $serviceDescontoTitulo::FINANCEIRO_CONFIG_CALCULO_DESCONTO_INCENTIVO_VALOR_LIQUIDO;

        $testaSeFormaDeCalculoValorLiquido = $valorLiquido == $formaCalculoIncentivo;

        $query = "
        SELECT
              coalesce(if(valor_desconto_antecipado1 > 0 AND valor_desconto_antecipado1 < titulo_valor_liquido, valor_desconto_antecipado1,
              if(valor_desconto_antecipado2 < titulo_valor_liquido AND valor_desconto_antecipado2!=0,valor_desconto_antecipado2,0 )), 0) valor_desconto_antecipado,
              coalesce(greatest(data_desconto_antecipado1, data_desconto_antecipado2),0) data_desconto_antecipado,
              t.*,
              format(t.titulo_valor, 2, 'de_DE') as titulo_valor_Format,
              c.curso_nome,
              c.curso_sigla,
              (select count(bol_id)>0 from financeiro__remessa_lote WHERE bol_id=t.bol_id) as esta_na_remessa
        FROM (
          SELECT
            t.*,
            @CalculoDescontoIncentivo:='$formaCalculoIncentivo' AS calculoDescontoIncentivo,

            @DiaCobrancaJuros:=
            DATE_ADD(
              t.titulo_data_vencimento, INTERVAL COALESCE(t.tituloconf_dia_juros,0) DAY
            ) AS data_cobranca_juros,
            @descontos_vinculados_validos:= format((t.descontos + coalesce(t.titulo_desconto, 0) + coalesce(t.titulo_desconto_manual,0)),2,'de_DE') as titulo_desconto_calc_formatado,
            @ValorLiquido:=(
              t.titulo_valor +
              COALESCE (t.titulo_acrescimo_manual,0) -
              (
                t.descontos +
                COALESCE (t.titulo_desconto,0)
              )
            ) AS titulo_valor_liquido,

            @ValorLiquidoSemAcrescimo:=(
              t.titulo_valor - (
                COALESCE (t.descontos, 0) +
                COALESCE (t.titulo_desconto,0) +
                COALESCE(t.titulo_desconto_manual,0))

            ) AS titulo_valor_liquido_sem_acrescimo,

              @ValorMulta:=round(
                coalesce(
                   if(titulo_data_pagamento IS NULL AND t.dias_atraso > 0 AND date(now()) > @DiaCobrancaJuros ,
                      -- configuração de juros e multa para título
                         @ValorLiquido * coalesce(tituloconf_multa, 0),
                         titulo_multa
                   ),
                   0
                ),
                2
            ) AS titulo_multa_calc,

            @ValorJuros:=round(
                coalesce(
                   if(titulo_data_pagamento IS NULL AND t.dias_atraso > 0 AND date(now()) > @DiaCobrancaJuros,
                     ((@ValorLiquido + @ValorMulta) * coalesce(tituloconf_juros, 0) * t.dias_atraso),
                     coalesce(titulo_juros, 0)
                   )
                ),
                2
             ) AS titulo_juros_calc,

            @DataDescontoAntecipado1:=(
              DATE_SUB(t.titulo_data_vencimento, INTERVAL tituloconf_dia_desc day)
            )as data_desconto_antecipado1,
            @ValorDescontoAntecipado1:=if(
              (
				(t.descontos = 0 OR (t.desctipo_desconto_acumulativo = 'Sim' AND t.tituloconf_desconto_incentivo_acumulativo = 'Sim' )) AND
				DATE(now()) <= @DataDescontoAntecipado1
              ),
              if(
                coalesce(t.tituloconf_valor_desc, 0) != 0,
                t.tituloconf_valor_desc,
               (if('$testaSeFormaDeCalculoValorLiquido', @ValorLiquidoSemAcrescimo, t.titulo_valor) * t.tituloconf_percent_desc)
              ),
              0
            ) AS valor_desconto_antecipado1,
            @DataDescontoAntecipado2:=(
              DATE_SUB(t.titulo_data_vencimento, INTERVAL tituloconf_dia_desc2 day)
            ) as data_desconto_antecipado2,

             @ValorDescontoAntecipado2:=if(
              (
				(t.descontos = 0 OR (t.desctipo_desconto_acumulativo = 'Sim' AND t.tituloconf_desconto_incentivo_acumulativo = 'Sim' )) AND
				DATE(now()) <= @DataDescontoAntecipado2
              ),
              if(
                coalesce(t.tituloconf_valor_desc2, 0) != 0,
                t.tituloconf_valor_desc2,
                (if('$testaSeFormaDeCalculoValorLiquido', @ValorLiquidoSemAcrescimo, t.titulo_valor) * t.tituloconf_percent_desc2)
              ),
              0
            ) AS valor_desconto_antecipado2,
            (@ValorLiquido + @ValorMulta + @ValorJuros) AS titulo_valor_calc,
            (
               t.descontos +
               coalesce(t.titulo_desconto, 0) +
               coalesce(t.titulo_desconto_manual, 0)
            ) AS titulo_desconto_calc,
            format((@ValorLiquido + @ValorMulta + @ValorJuros),2,'de_DE') as titulo_valor_calc_formatado
          FROM (
            SELECT
              t.titulo_id,
              t.pes_id,
              p.pes_nome,
              t.tipotitulo_id,
              t.usuario_baixa,
              '-'       usuario_baixa_login,
              t.usuario_autor,
              '-'       usuario_autor_login,
              t.titulo_novo,
              t.alunocurso_id,
              ac.aluno_id,
              a.pes_id AS pes_id_aluno,
              t.titulo_descricao,
              t.titulo_parcela,
              t.titulo_valor_pago,
              t.titulo_valor_pago_dinheiro,
              t.titulo_multa,
              t.titulo_juros,
              t.titulo_desconto,
              t.titulo_desconto_manual,
              t.titulo_acrescimo_manual,
              t.titulo_data_processamento,
              t.titulo_data_vencimento,
              t.titulo_data_pagamento,
              t.titulo_data_baixa,
              t.titulo_tipo_pagamento,
              t.titulo_valor,
              t.titulo_estado,
              t.financeiro_desconto_incentivo,
              ac.cursocampus_id,
              GREATEST(
                IF(
                  titulo_data_pagamento IS NULL,
                  DATEDIFF(DATE(NOW()), DATE_ADD(DATE(t.titulo_data_vencimento),INTERVAL coalesce(tituloconf_dia_juros,0) DAY)),
                  DATEDIFF(DATE(t.titulo_data_pagamento), DATE(t.titulo_data_vencimento))
                ),
                0
              ) AS dias_atraso,

               COALESCE(total_descontos.desconto, 0) AS descontos,              
              IF(
              (total_descontos.desconto IS NOT NULL AND total_descontos.desconto > 0 ) AND total_descontos.desctipo_desconto_acumulativo IS NULL,
              'Sim',
              total_descontos.desctipo_desconto_acumulativo ) AS desctipo_desconto_acumulativo,
              
              tc.tituloconf_id,
              tc.tituloconf_dia_venc,
              tc.tituloconf_dia_juros,
              tc.tituloconf_dia_desc,
              tc.tituloconf_valor_desc,
              tc.tituloconf_percent_desc,
              tc.tituloconf_dia_desc2,
              tc.tituloconf_valor_desc2,
              tc.tituloconf_percent_desc2,
              tc.tituloconf_multa,
              tc.tituloconf_juros,
              tc.confcont_id,
              b.bol_id,
              tt.tipotitulo_nome,
              tc.tituloconf_desconto_incentivo_acumulativo,
              coalesce(pes.pes_nome ,'-') as aluno_nome
            FROM financeiro__titulo t
            INNER JOIN financeiro__titulo_tipo AS tt on tt.tipotitulo_id=t.tipotitulo_id

            -- dados aluno
            INNER JOIN pessoa p ON t.pes_id = p.pes_id

            -- curso aluno
            LEFT JOIN acadgeral__aluno_curso ac ON ac.alunocurso_id = t.alunocurso_id
            LEFT JOIN acadgeral__aluno a ON a.aluno_id = ac.aluno_id

            -- nome para alunos
            LEFT JOIN pessoa pes ON a.pes_id=pes.pes_id

            -- boleto
            LEFT JOIN boleto b ON t.titulo_id = b.titulo_id

            -- usuário baixa
            LEFT JOIN financeiro__titulo_config  AS tc USING(tituloconf_id)
            LEFT JOIN
            (
            SELECT
            coalesce(sum(desconto_aplicacao_valor), 0) AS desconto,
            (
            select desctipo_desconto_acumulativo from view__financeiro_descontos_titulos_atualizada
            WHERE desconto_aplicacao_valor > 0 AND desctipo_desconto_acumulativo!='Sim' and titulo_id=fdt.titulo_id
                   limit 1
            ) as desctipo_desconto_acumulativo,
               fdt.titulo_id
            FROM view__financeiro_descontos_titulos_atualizada fdt
               WHERE desconto_aplicacao_valor >  0 
               GROUP BY titulo_id           
                        
            ) AS total_descontos ON total_descontos.titulo_id=t.titulo_id

            -- CONDITIONS --
          ) t
            -- CONDITIONS 2 --
          GROUP BY titulo_id
        ) t
        LEFT JOIN campus_curso cc ON cc.cursocampus_id = t.cursocampus_id
        LEFT JOIN acad_curso c ON c.curso_id = cc.curso_id
        ";

        $arrCampusCursoPermitido = implode(",", $serviceAcessoPessoas->retornaCampusCursoDoUsuarioLogado(true));

        if ($arrCampusCursoPermitido) {
            $conditions[] = " (ac.cursocampus_id IN(" . $arrCampusCursoPermitido . ") OR ac.cursocampus_id IS NULL)";
        }

        $conditions = $conditions ? "WHERE " . implode(' AND ', $conditions) : "";

        $query = str_replace('-- CONDITIONS --', $conditions, $query);

        $conditions2 = $conditions2 ? "WHERE " . implode(' AND ', $conditions2) : "";

        $query = str_replace('-- CONDITIONS 2 --', $conditions2, $query);

        return $query;
    }

    /**
     * @param array $conditions
     * @return mixed|string
     */
    private function retornaTitulosIdsCondicoes($conditions = array())
    {
        $query      = "SELECT t.titulo_id AS titulos FROM financeiro__titulo t -- CONDITIONS --";
        $conditions = $conditions ? "WHERE " . implode(' AND ', $conditions) : "";
        $query      = str_replace('-- CONDITIONS --', $conditions, $query);
        $result     = $this->executeQueryWithParam($query)->fetchAll(\PDO::FETCH_COLUMN);

        return implode(',', $result);
    }

    /**
     * @param array $conditions
     * @return mixed|string
     */
    private function retornaConsultaSQLTitulosComInformacoesExtras($conditions = array())
    {
        $usarQueryNova = $this->getConfig()->localizarChave('FINANCEIRO_ATIVAR_QUERY_NOVA');

        if ($usarQueryNova) {
            return $this->retornaConsultaSQLTitulosComInformacoesExtrasNova($conditions);
        }

        return $this->retornaConsultaSQLTitulosComInformacoesExtrasAntiga($conditions);
    }

    /**
     * @param array $conditions
     * @return mixed|string
     */
    private function retornaConsultaSQLTitulosComInformacoesExtrasNova($conditions = array())
    {
        $serviceConfig         = new \Sistema\Service\SisConfig($this->getEm());
        $serviceDescontoTitulo = new \Financeiro\Service\FinanceiroDescontoTitulo($this->getEm());
        $serviceAcessoPessoas  = new \Acesso\Service\AcessoPessoas($this->getEm());

        $formaCalculoIncentivo = $serviceConfig->localizarChave('FINANCEIRO_CONFIG_CALCULO_DESCONTO_INCENTIVO')
            ?
            $serviceConfig->localizarChave('FINANCEIRO_CONFIG_CALCULO_DESCONTO_INCENTIVO')
            :
            $serviceDescontoTitulo::FINANCEIRO_CONFIG_CALCULO_DESCONTO_INCENTIVO_VALOR_BRUTO;

        $valorLiquido = $serviceDescontoTitulo::FINANCEIRO_CONFIG_CALCULO_DESCONTO_INCENTIVO_VALOR_LIQUIDO;

        $testaSeFormaDeCalculoValorLiquido = $valorLiquido == $formaCalculoIncentivo;

        $query = "
        SELECT
          t.titulo_id,
          t.pes_id,
          p.pes_nome,
          t.tipotitulo_id,
          t.usuario_baixa,
          '-' as      usuario_baixa_login,
          t.usuario_autor,
          '-' as      usuario_autor_login,
          t.titulo_novo,
          t.alunocurso_id,
          ac.aluno_id,
          a.pes_id AS pes_id_aluno,
          t.titulo_descricao,
          t.titulo_parcela,
          t.titulo_valor_pago,
          t.titulo_valor_pago_dinheiro,
          t.titulo_multa,
          t.titulo_juros,
          t.titulo_desconto,
          t.titulo_desconto_manual,
          t.titulo_acrescimo_manual,
          t.titulo_data_processamento,
          t.titulo_data_vencimento,
          t.titulo_data_pagamento,
          t.titulo_data_baixa,
          t.titulo_tipo_pagamento,
          t.titulo_valor,
          t.titulo_estado,
          t.financeiro_desconto_incentivo,
          ac.cursocampus_id,          
          @diasAtraso:=GREATEST(
            IF(
              titulo_data_pagamento IS NULL,
              DATEDIFF(DATE(NOW()), DATE_ADD(DATE(t.titulo_data_vencimento),INTERVAL coalesce(tituloconf_dia_juros,0) DAY)),
              DATEDIFF(DATE(t.titulo_data_pagamento), DATE(t.titulo_data_vencimento))
            ),
            0
          ) AS dias_atraso,

          @descontosPrefixados:=COALESCE(total_descontos.desconto, 0) AS descontos,
          IF((total_descontos.desconto IS NOT NULL AND total_descontos.desconto > 0 ) AND total_descontos.desctipo_desconto_acumulativo IS NULL,
          'Sim',
          total_descontos.desctipo_desconto_acumulativo ) AS desctipo_desconto_acumulativo,

          tc.tituloconf_id,
          tc.tituloconf_dia_venc,
          tc.tituloconf_dia_juros,
          tc.tituloconf_dia_desc,
          tc.tituloconf_valor_desc,
          tc.tituloconf_percent_desc,
          tc.tituloconf_dia_desc2,
          tc.tituloconf_valor_desc2,
          tc.tituloconf_percent_desc2,
          tc.tituloconf_multa,
          tc.tituloconf_juros,
          tc.confcont_id,
          b.bol_id,
          tt.tipotitulo_nome,
          tc.tituloconf_desconto_incentivo_acumulativo,
          '-' as aluno_nome,
          c.curso_nome,
          c.curso_sigla,


          @CalculoDescontoIncentivo:='$formaCalculoIncentivo' AS calculoDescontoIncentivo,

          @DiaCobrancaJuros:= DATE_ADD(
            t.titulo_data_vencimento, INTERVAL COALESCE(tc.tituloconf_dia_juros,0) DAY
          ) AS data_cobranca_juros,
          @descontos_vinculados_validos:= format((@descontosPrefixados + coalesce(t.titulo_desconto, 0) + coalesce(t.titulo_desconto_manual,0)),2,'de_DE') as titulo_desconto_calc_formatado,
          @ValorLiquido:=(
            t.titulo_valor +
            COALESCE (t.titulo_acrescimo_manual,0) -
            (
              @descontosPrefixados +
              COALESCE (t.titulo_desconto,0)
            )
          ) AS titulo_valor_liquido,

          @ValorLiquidoSemAcrescimo:=(
            t.titulo_valor - (
            @descontosPrefixados +
            COALESCE (t.titulo_desconto,0) +
            COALESCE(t.titulo_desconto_manual,0))

          ) AS titulo_valor_liquido_sem_acrescimo,

          @ValorMulta:=round(
            coalesce(
              if(
                titulo_data_pagamento IS NULL AND
                @diasAtraso > 0 AND
                date(now()) > @DiaCobrancaJuros ,
                -- configuração de juros e multa para título
                @ValorLiquido * coalesce(tituloconf_multa, 0),
                titulo_multa
              ),
              0
            ),
            2
          ) AS titulo_multa_calc,

          @ValorJuros:=round(
            coalesce(
              if(
                titulo_data_pagamento IS NULL AND
                @diasAtraso > 0 AND
                date(now()) > @DiaCobrancaJuros,
                ((@ValorLiquido + @ValorMulta) * coalesce(tituloconf_juros, 0) * @diasAtraso),
                coalesce(titulo_juros, 0)
              )
            ),
            2
          ) AS titulo_juros_calc,

          @DataDescontoAntecipado1:=(
            DATE_SUB(t.titulo_data_vencimento, INTERVAL tituloconf_dia_desc day)
          )as data_desconto_antecipado1,
          @ValorDescontoAntecipado1:=if(
            (
              (@descontosPrefixados = 0 OR (total_descontos.desctipo_desconto_acumulativo = 'Sim' AND tc.tituloconf_desconto_incentivo_acumulativo = 'Sim' )) AND
              DATE(now()) <= @DataDescontoAntecipado1
            ),
            if(
              coalesce(tc.tituloconf_valor_desc, 0) != 0,
              tc.tituloconf_valor_desc,
              (if('$testaSeFormaDeCalculoValorLiquido', @ValorLiquidoSemAcrescimo, t.titulo_valor) * tc.tituloconf_percent_desc)
            ),
            0
          ) AS valor_desconto_antecipado1,
          @DataDescontoAntecipado2:=(
            DATE_SUB(t.titulo_data_vencimento, INTERVAL tituloconf_dia_desc2 day)
          ) as data_desconto_antecipado2,

          @ValorDescontoAntecipado2:=if(
            (
              (@descontosPrefixados = 0 OR (total_descontos.desctipo_desconto_acumulativo = 'Sim' AND tc.tituloconf_desconto_incentivo_acumulativo = 'Sim' )) AND
              DATE(now()) <= @DataDescontoAntecipado2
            ),
            if(
              coalesce(tc.tituloconf_valor_desc2, 0) != 0,
              tc.tituloconf_valor_desc2,
              (if('$testaSeFormaDeCalculoValorLiquido', @ValorLiquidoSemAcrescimo, t.titulo_valor) * tc.tituloconf_percent_desc2)
            ),
            0
          ) AS valor_desconto_antecipado2,
          (@ValorLiquido + @ValorMulta + @ValorJuros) AS titulo_valor_calc,
          (
            @descontosPrefixados +
            coalesce(t.titulo_desconto, 0) +
            coalesce(t.titulo_desconto_manual, 0)
          ) AS titulo_desconto_calc,
          format((@ValorLiquido + @ValorMulta + @ValorJuros),2,'de_DE') as titulo_valor_calc_formatado,



          coalesce(
            if(
              @ValorDescontoAntecipado1 > 0 AND
              @ValorDescontoAntecipado1 < @ValorLiquido,
              @ValorDescontoAntecipado1,
              if(
                @ValorDescontoAntecipado2 < @ValorLiquido AND
                @ValorDescontoAntecipado2 != 0,
                @ValorDescontoAntecipado2, 0
              )
            ),
            0
          ) valor_desconto_antecipado,


          coalesce(greatest(@DataDescontoAntecipado1, @DataDescontoAntecipado2),0) data_desconto_antecipado,
          format(t.titulo_valor, 2, 'de_DE') as titulo_valor_Format

        FROM financeiro__titulo t
          INNER JOIN financeiro__titulo_tipo AS tt on tt.tipotitulo_id=t.tipotitulo_id

          -- dados aluno
          INNER JOIN pessoa p ON t.pes_id = p.pes_id

          -- curso aluno
          LEFT JOIN acadgeral__aluno_curso ac ON ac.alunocurso_id = t.alunocurso_id
          LEFT JOIN campus_curso cc ON cc.cursocampus_id = ac.cursocampus_id
          LEFT JOIN acad_curso c ON c.curso_id = cc.curso_id
          LEFT JOIN acadgeral__aluno a ON a.aluno_id = ac.aluno_id

          -- nome para alunos
          -- LEFT JOIN pessoa pes ON a.pes_id=pes.pes_id

          -- boleto
          LEFT JOIN boleto b ON t.titulo_id = b.titulo_id

          -- usuário baixa
          LEFT JOIN financeiro__titulo_config  AS tc USING(tituloconf_id)
          LEFT JOIN
          (
          SELECT
          coalesce(sum(desconto_aplicacao_valor), 0) AS desconto,
            (
            select desctipo_desconto_acumulativo from view__financeiro_descontos_titulos_atualizada
            WHERE desconto_aplicacao_valor > 0 AND desctipo_desconto_acumulativo!='Sim' and titulo_id=fdt.titulo_id
                   limit 1
            ) as desctipo_desconto_acumulativo,
               fdt.titulo_id
            FROM view__financeiro_descontos_titulos_atualizada fdt
               WHERE desconto_aplicacao_valor >  0 
               GROUP BY titulo_id
            
            ) AS total_descontos ON total_descontos.titulo_id=t.titulo_id
          -- CONDITIONS --
          ORDER BY t.titulo_id ASC
            ";

        $arrCampusCursoPermitido = implode(",", $serviceAcessoPessoas->retornaCampusCursoDoUsuarioLogado(true));

        if ($arrCampusCursoPermitido) {
            $conditions[] = " (ac.cursocampus_id IN(" . $arrCampusCursoPermitido . ") OR ac.cursocampus_id IS NULL)";
        }

        $conditions = $conditions ? "WHERE " . implode(' AND ', $conditions) : "";

        $query = str_replace('-- CONDITIONS --', $conditions, $query);

        return $query;
    }

    /**
     * @param int $alunocursoId
     * @return int
     */
    public function numeroDeTitulosDoAluno($alunocursoId)
    {
        $query = "SELECT count(*) AS qtd FROM financeiro__titulo WHERE alunocurso_id=:alunocurso_id";

        $result = $this->executeQueryWithParam($query, ['alunocurso_id' => $alunocursoId])->fetch();

        return (int)$result['qtd'];
    }

    /**
     * @param $titulos
     * @return array
     */
    public function retornaTitulosComInformacoesExtras($titulos)
    {
        $query = $this->retornaConsultaSQLTitulosComInformacoesExtras(
            ["t.titulo_id IN(" . implode(',', $titulos) . ")"]
        );

        $result = $this->executeQueryWithParam($query)->fetchAll();

        return $result ? $result : array();
    }

    /**
     * @param $tituloId
     * @return array
     */
    public function getArray($tituloId)
    {
        /** @var $objFinanceiroTitulo \Financeiro\Entity\FinanceiroTitulo */
        $objFinanceiroTitulo         = $this->getRepository()->find($tituloId);
        $servicePessoa               = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceFinanceiroTituloTipo = new \Financeiro\Service\FinanceiroTituloTipo($this->getEm());
        $serviceAcessoPessoas        = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceAcadgeralAlunoCurso  = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());

        try {
            $arrDados = $objFinanceiroTitulo->toArray();

            if ($arrDados['pes']) {
                $arrPessoa       = $servicePessoa->getArrSelect2(['id' => $arrDados['pes']]);
                $arrDados['pes'] = $arrPessoa ? $arrPessoa[0] : null;
            }

            if ($arrDados['tipotitulo']) {
                $arrFinanceiroTituloTipo = $serviceFinanceiroTituloTipo->getArrSelect2(
                    ['id' => $arrDados['tipotitulo']]
                );
                $arrDados['tipotitulo']  = $arrFinanceiroTituloTipo ? $arrFinanceiroTituloTipo[0] : null;
            }

            if ($arrDados['usuarioBaixa']) {
                $arrAcessoPessoas         = $serviceAcessoPessoas->getArrSelect2(['id' => $arrDados['usuarioBaixa']]);
                $arrDados['usuarioBaixa'] = $arrAcessoPessoas ? $arrAcessoPessoas[0] : null;
            }

            if ($arrDados['usuarioAutor']) {
                $arrAcessoPessoas         = $serviceAcessoPessoas->getArrSelect2(['id' => $arrDados['usuarioAutor']]);
                $arrDados['usuarioAutor'] = $arrAcessoPessoas ? $arrAcessoPessoas[0] : null;
            }

            if ($arrDados['alunocurso']) {
                $arrAcadgeralAlunoCurso = $serviceAcadgeralAlunoCurso->getArrSelect2(['id' => $arrDados['alunocurso']]);
                $arrDados['alunocurso'] = $arrAcadgeralAlunoCurso ? $arrAcadgeralAlunoCurso[0] : null;
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroTitulo $objTitulo
     * @return int|mixed
     */
    public function getDiasAtraso(\Financeiro\Entity\FinanceiroTitulo $objTitulo)
    {
        $dataFinal    = null;
        $tituloEstado = $objTitulo->getTituloEstado();

        if ($tituloEstado == self::TITULO_ESTADO_PAGO || $tituloEstado == self::TITULO_ESTADO_ESTORNO) {
            $dataFinal = $objTitulo->getTituloDataPagamento();
        }

        if (!$dataFinal) {
            $dataFinal = new \DateTime();
        }

        $dataVencimento = clone($objTitulo->getTituloDataVencimento());

        if ($objTitulo->getTituloconf()->getTituloconfDiaJuros()) {
            $dataVencimento->modify('+' . $objTitulo->getTituloconf()->getTituloconfDiaJuros() . ' days');
        }

        $dataFinal->setTime(0, 0, 0);
        $dataVencimento->setTime(0, 0, 0);

        $diff = $dataVencimento->diff($dataFinal);

        if ($diff) {
            $diff->format('%R');
            $atraso = $diff->days;

            if ($dataFinal < $dataVencimento) {
                $atraso *= -1;
            }
        } else {
            $atraso = 0;
        }

        if ($atraso < 0) {
            $atraso = 0;
        }

        return $atraso;
    }

    /**
     * @param $tituloId
     * @return array
     */
    public function retornaInformacoesTitulo($tituloId)
    {
        /** @var $objTitulo \Financeiro\Entity\FinanceiroTitulo */
        $objTitulo = $this->getRepository()->find($tituloId);

        if (!$objTitulo) {
            $this->setLastError('Título não encontrado!');

            return false;
        }

        $serviceTituloConfig        = new \Financeiro\Service\FinanceiroTituloConfig($this->getEm());
        $serviceDescontoTitulo      = new \Financeiro\Service\FinanceiroDescontoTitulo($this->getEm());
        $serviceTituloAlteracao     = new \Financeiro\Service\FinanceiroTituloAlteracao($this->getEm());
        $serviceFinanceiroBoleto    = new \Financeiro\Service\Boleto($this->getEm());
        $serviceFinanceiroPagamento = new \Financeiro\Service\FinanceiroPagamento($this->getEm());
        $serviceDescontoTipo        = new \Financeiro\Service\FinanceiroDescontoTipo($this->getEm());
        $serviceConfig              = new \Sistema\Service\SisConfig($this->getEm());
        $serviceAluno               = new \Matricula\Service\AcadgeralAluno($this->getEm());
        $serviceAlunoCurso          = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
        $serviceFinanceiroCartao    = new \Financeiro\Service\FinanceiroCartao($this->getEm());

        $formaCalculoIncentivo = $serviceConfig->localizarChave('FINANCEIRO_CONFIG_CALCULO_DESCONTO_INCENTIVO');

        if (!$formaCalculoIncentivo) {
            $formaCalculoIncentivo = $serviceDescontoTitulo::FINANCEIRO_CONFIG_CALCULO_DESCONTO_INCENTIVO_VALOR_BRUTO;
        }

        try {
            $arrDados = $objTitulo->toArray();

            $arrDados['tipotituloId']   = $objTitulo->getTipotitulo()->getTipotituloId();
            $arrDados['tipotituloNome'] = $objTitulo->getTipotitulo()->getTipotituloNome();

            if ($arrDados['usuarioBaixa']) {
                $arrDados['usuarioBaixaLogin'] = $objTitulo->getUsuarioBaixa()->getLogin();
            }

            if ($arrDados['usuarioAutor']) {
                $arrDados['usuarioAutorLogin'] = $objTitulo->getUsuarioAutor()->getLogin();
            }

            if ($arrDados['alunoPer']) {
                $arrDados['turmaNome'] = $objTitulo->getAlunoPer()->getTurma()->getTurmaNome();
            }

            /** @var $objAlunoCurso \Matricula\Entity\AcadgeralAlunoCurso */
            $objAlunoCurso = $objTitulo->getAlunocurso();

            if (!$objAlunoCurso) {
                /** @var $objAluno \Matricula\Entity\AcadgeralAluno */
                $objAluno = $serviceAluno->getRepository()->findOneBy(
                    ['pes' => $objTitulo->getPes()->getPesId()]
                );

                if ($objAluno) {
                    $objAlunoCurso = $serviceAlunoCurso->getRepository()->findOneBy(
                        ['aluno' => $objAluno->getAlunoId()]
                    );
                }
            }

            if ($objAlunoCurso) {
                $arrDados['alunocursoId']  = $objAlunoCurso->getAlunocursoId();
                $arrDados['alunoId']       = $objAlunoCurso->getAluno()->getAlunoId();
                $arrDados['alunoNome']     = $objAlunoCurso->getAluno()->getPes()->getPes()->getPesNome();
                $arrDados['cursocampusId'] = $objAlunoCurso->getCursocampus()->getCursocampusId();
                $arrDados['cursoId']       = $objAlunoCurso->getCursocampus()->getCurso()->getCursoId();
                $arrDados['cursoNome']     = $objAlunoCurso->getCursocampus()->getCurso()->getCursoNome();
                $arrDados['campNome']      = $objAlunoCurso->getCursocampus()->getCamp()->getCampNome();
                $arrDados['ies']           = $objAlunoCurso->getCursocampus()->getCamp()->getIes()->getIesNome();
                $arrDados['mantenedora']   = (
                $objAlunoCurso->getCursocampus()->getCamp()->getIes()->getPesMantenedora()->getPes()->getPesNome()
                );
                $arrDados['campId']        = $objAlunoCurso->getCursocampus()->getCamp()->getCampId();
            }

            if ($arrDados['pes']) {
                $arrDados['pesId']   = $objTitulo->getPes()->getPesId();
                $arrDados['pesNome'] = $objTitulo->getPes()->getPesNome();
            }

            $arrDados['diasAtraso'] = $this->getDiasAtraso($objTitulo);

            //Pesquisa configuração válida baseada no tipo de título e cursocampus
            /** @var \Financeiro\Entity\FinanceiroTituloConfig $objTituloConfig */
            $objTituloConfig = $objTitulo->getTituloconf();

            if (!$objTituloConfig) {
                $this->setLastError($serviceTituloConfig->getLastError());

                return array();
            }

            $arrDesconto             = $serviceDescontoTitulo->getArrayDescontosTitulosAplicacao($tituloId);
            $arrDestino              = $serviceTituloAlteracao->getArrayBoletosTitulos($tituloId);
            $arrOrigem               = $serviceTituloAlteracao->getArrayBoletosTitulos(false, $tituloId);
            $titulosVinculados       = $serviceTituloAlteracao->retornaTitulosVinculados($tituloId, true, true);
            $titulosVinculadosCartao = $serviceFinanceiroCartao->retornaTitulosVinculadosCartao($tituloId);

            $arrDados['tituloConfig']                        = $objTituloConfig->toArray();
            $arrDados['descontos']                           =
                $serviceDescontoTitulo->getSomaDescontosTitulo($tituloId);
            $arrDados['arrDescontos']                        = $arrDesconto;
            $arrDados['boletos']                             =
                $serviceFinanceiroBoleto->getArrayBoletosTitulos($tituloId);
            $arrDados['vinculos']                            = [];
            $arrDados['vinculos']['destino']                 = $arrDestino;
            $arrDados['vinculos']['tituloDestino']           = $arrDestino ? $arrDestino[0]['titulo_id_destino'] : null;
            $arrDados['vinculos']['origem']                  = $arrOrigem;
            $arrDados['pagamentos']                          = array();
            $arrDados['vinculos']['titulosVinculados']       = $titulosVinculados;
            $arrDados['vinculos']['titulosVinculadosCartao'] = $titulosVinculadosCartao;

            $descontosNaoLimitadoAoVencimento             = $serviceDescontoTitulo->getSomaDescontosTitulo(
                $tituloId,
                null,
                $serviceDescontoTipo::DESCTIPO_LIMITA_VENCIMENTO_NAO
            );
            $arrDados['descontosNaoLimitadoAoVencimento'] = $descontosNaoLimitadoAoVencimento['desconto'];
            $arrDados['desctipo_desconto_acumulativo']    = $serviceDescontoTipo::DESCTIPO_DESCONTO_ACUMULATIVO_SIM;

            foreach ($arrDados['arrDescontos'] as $chave) {
                if ($chave['desctipo_desconto_acumulativo'] == $serviceDescontoTipo::DESCTIPO_DESCONTO_ACUMULATIVO_NAO) {
                    $arrDados['desctipo_desconto_acumulativo'] = $serviceDescontoTipo::DESCTIPO_DESCONTO_ACUMULATIVO_NAO;
                    break;
                }
            }

            $arrDados['descontos'] = $arrDados['descontos']['desconto'];
            $dataVencimento        = clone($objTitulo->getTituloDataVencimento());

            $dataDescontoAntecipado1 = $this->formatDateAmericano($arrDados['tituloDataVencimento']);
            $dataDescontoAntecipado1 = new \Datetime($dataDescontoAntecipado1);
            $tituloconfDiaDesc1      = (1 * $arrDados['tituloConfig']['tituloconfDiaDesc']);

            if ($tituloconfDiaDesc1 < 0) {
                $tituloconfDiaDesc1 *= -1;
                $dataDescontoAntecipado1->add(new \DateInterval('P' . $tituloconfDiaDesc1 . 'D'));
            } else {
                $dataDescontoAntecipado1->sub(new \DateInterval('P' . $tituloconfDiaDesc1 . 'D'));
            }

            $dataDescontoAntecipado2 = $this->formatDateAmericano($arrDados['tituloDataVencimento']);
            $dataDescontoAntecipado2 = new \Datetime($dataDescontoAntecipado2);
            $tituloconfDiaDesc2      = (1 * $arrDados['tituloConfig']['tituloconfDiaDesc2']);

            if ($tituloconfDiaDesc2 < 0) {
                $tituloconfDiaDesc2 *= -1;
                $dataDescontoAntecipado2->add(new \DateInterval('P' . $tituloconfDiaDesc2 . 'D'));
            } else {
                $dataDescontoAntecipado2->sub(new \DateInterval('P' . $tituloconfDiaDesc2 . 'D'));
            }

            if (
                $arrDados['tituloEstado'] == self::TITULO_ESTADO_PAGO ||
                $arrDados['tituloEstado'] == self::TITULO_ESTADO_PAGAMENTO_MULTIPLO ||
                $arrDados['tituloEstado'] == self::TITULO_ESTADO_ESTORNO
            ) {
                $arrDados['pagamentos'] = $serviceFinanceiroPagamento->getArrayPagamentosTitulo($tituloId);
            }

            $arrDados['somaDescontos'] = ($arrDados['tituloDesconto'] + $arrDados['descontos']);

            $arrDados['tituloValorFinal'] = $arrDados['tituloValor'] + ($arrDados['tituloAcrescimoManual']
                    ? $arrDados['tituloAcrescimoManual'] : 0) -
                ($arrDados['somaDescontos'] ? $arrDados['somaDescontos'] : 0);

            $dataCobrancaJuros = new \DateTime(self::formatDateAmericano($arrDados['tituloDataVencimento']));

            if (($objTituloConfig->getTituloconfDiaJuros() * 1) > 0) {
                $dataCobrancaJuros->add(
                    new \DateInterval('P' . (1 * $objTituloConfig->getTituloconfDiaJuros()) . 'D')
                );
            }

            $dateAtual = new \Datetime(date('Y-m-d'));

            if ($objTitulo->getTituloEstado() != self::TITULO_ESTADO_ABERTO) {
                $arrDados['tituloMultaCalc'] = $objTitulo->getTituloMulta();
            } elseif ($arrDados['diasAtraso'] > 0 && $dateAtual > $dataCobrancaJuros) {
                $arrDados['tituloMultaCalc'] = (
                    $objTituloConfig->getTituloconfMulta() * $arrDados['tituloValorFinal']
                );
            }

            $arrDados['tituloValorFinal'] = $arrDados['tituloValorFinal'] + $arrDados['tituloMultaCalc'];

            if ($objTitulo->getTituloEstado() != self::TITULO_ESTADO_ABERTO) {
                $arrDados['tituloJurosCalc'] = $objTitulo->getTituloJuros();
            } elseif ($dateAtual > $dataCobrancaJuros) {
                $arrDados['tituloJurosCalc'] = (
                    $objTituloConfig->getTituloconfJuros() * $arrDados['tituloValorFinal'] * $arrDados['diasAtraso']
                );
            }

            $arrDados['somaAcrescimos'] = ($arrDados['tituloJurosCalc'] + $arrDados['tituloMultaCalc'] + $arrDados['tituloAcrescimoManual']);

            $arrDados['tituloValorFinal'] = $arrDados['tituloValorFinal'] + $arrDados['tituloJurosCalc'];

            if (
                $objTitulo->getTituloEstado() == self::TITULO_ESTADO_PAGO ||
                $objTitulo->getTituloEstado() == self::TITULO_ESTADO_PAGAMENTO_MULTIPLO ||
                $objTitulo->getTituloEstado() == self::TITULO_ESTADO_ESTORNO
            ) {
                $arrDados['valorDescontoAntecipado'] =
                    $objTitulo->getPagamentoDescontoIncentivo() ? $objTitulo->getPagamentoDescontoIncentivo() : 0;
            } else {
                $arrDados['dataDescontoAntecipado1'] = $dataDescontoAntecipado1->format('d/m/Y');
                $arrDados['dataDescontoAntecipado2'] = $dataDescontoAntecipado2->format('d/m/Y');

                $arrDados['valorDescontoAntecipado1'] = 0;
                $arrDados['valorDescontoAntecipado2'] = 0;

                $descontoAcumulativoComDescontoDeIncentivo = $arrDados['desctipo_desconto_acumulativo'];
                $descontoAcumulativoComDescontoDeIncentivo = (
                    $descontoAcumulativoComDescontoDeIncentivo == $serviceDescontoTipo::DESCTIPO_DESCONTO_ACUMULATIVO_SIM
                );

                $descontoAcumulativoTitulo = $objTituloConfig->getTituloconfDescontoIncentivoAcumulativo();
                $descontoAcumulativoTitulo = (
                    $descontoAcumulativoTitulo == $serviceTituloConfig::TITULOCONF_DESCONTO_INCENTIVO_ACUMULATIVO_SIM
                );

                if ($dateAtual <= $dataDescontoAntecipado1) {
                    if (($descontoAcumulativoComDescontoDeIncentivo && $descontoAcumulativoTitulo) || ($arrDados['tituloDesconto'] + $arrDados['descontos']) == 0) {
                        if ($objTituloConfig->getTituloconfValorDesc()) {
                            $arrDados['valorDescontoAntecipado1'] = $objTituloConfig->getTituloconfValorDesc();
                        } else {
                            if ($formaCalculoIncentivo == $serviceDescontoTitulo::FINANCEIRO_CONFIG_CALCULO_DESCONTO_INCENTIVO_VALOR_LIQUIDO) {
                                $arrDados['valorDescontoAntecipado1'] = (
                                    ($arrDados['tituloValor'] - $arrDados['somaDescontos']) * ($objTituloConfig->getTituloconfPercentDesc(
                                    ))
                                );
                            } elseif ($formaCalculoIncentivo == $serviceDescontoTitulo::FINANCEIRO_CONFIG_CALCULO_DESCONTO_INCENTIVO_VALOR_BRUTO) {
                                $arrDados['valorDescontoAntecipado1'] = (
                                    $objTitulo->getTituloValor() * ($objTituloConfig->getTituloconfPercentDesc())
                                );
                            }
                        }
                    }
                }

                if ($dateAtual <= $dataDescontoAntecipado2) {
                    if (($descontoAcumulativoComDescontoDeIncentivo && $descontoAcumulativoTitulo) || ($arrDados['tituloDesconto'] + $arrDados['descontos']) == 0) {
                        if ($objTituloConfig->getTituloconfValorDesc2()) {
                            $arrDados['valorDescontoAntecipado2'] = $objTituloConfig->getTituloconfValorDesc2();
                        } else {
                            if ($formaCalculoIncentivo == $serviceDescontoTitulo::FINANCEIRO_CONFIG_CALCULO_DESCONTO_INCENTIVO_VALOR_LIQUIDO) {
                                $arrDados['valorDescontoAntecipado2'] = (
                                    ($arrDados['tituloValor'] - $arrDados['somaDescontos']) * ($objTituloConfig->getTituloconfPercentDesc2(
                                    ))
                                );
                            } elseif ($formaCalculoIncentivo == $serviceDescontoTitulo::FINANCEIRO_CONFIG_CALCULO_DESCONTO_INCENTIVO_VALOR_BRUTO) {
                                $arrDados['valorDescontoAntecipado2'] = (
                                    $objTitulo->getTituloValor() * ($objTituloConfig->getTituloconfPercentDesc2())
                                );
                            }
                        }
                    }
                }

                if (($arrDados['valorDescontoAntecipado1'] > $arrDados['tituloValorFinal'] AND
                        ($arrDados['tituloEstado'] == self::TITULO_ESTADO_ABERTO OR
                            $arrDados['tituloEstado'] == self::TITULO_ESTADO_ALTERACAO OR
                            $arrDados['tituloEstado'] == self::TITULO_ESTADO_PAGAMENTO_MULTIPLO)) OR
                    ($arrDados['valorDescontoAntecipado1'] > $arrDados['tituloValorPago'] AND
                        $arrDados['tituloEstado'] == self::TITULO_ESTADO_PAGO)
                ) {
                    $arrDados['msgWarning'] .= "Desconto de incentivo não foi aplicado, porque é maior que o valor do título!</br>";
                    $arrDados['valorDescontoAntecipado1'] = 0;
                }

                if ($arrDados['valorDescontoAntecipado2'] > $arrDados['tituloValorFinal'] AND
                    ($arrDados['tituloEstado'] == self::TITULO_ESTADO_ABERTO OR
                        $arrDados['tituloEstado'] == self::TITULO_ESTADO_ALTERACAO OR
                        $arrDados['tituloEstado'] == self::TITULO_ESTADO_PAGAMENTO_MULTIPLO) OR
                    ($arrDados['valorDescontoAntecipado1'] > $arrDados['tituloValorPago'] AND
                        $arrDados['tituloEstado'] == self::TITULO_ESTADO_PAGO)
                ) {
                    $arrDados['msgWarning'] .= "Desconto de incentivo não foi aplicado, porque é maior que o valor do título!</br>";
                    $arrDados['valorDescontoAntecipado2'] = 0;
                } else {
                    $arrDados['msgWarning'] = null;
                }

                if (floatval($arrDados['tituloValorFinal']) < 0) {
                    $this->setLastError("Houve um erro no calculo do título, o mesmo retornou valor negativo!");

                    return false;
                }

                $arrDados['valorDescontoAntecipado'] = 0;

                if ($arrDados['valorDescontoAntecipado1'] && $dateAtual <= $dataDescontoAntecipado1) {
                    $arrDados['valorDescontoAntecipado'] = $arrDados['valorDescontoAntecipado1'];
                } elseif ($arrDados['valorDescontoAntecipado2'] && $dateAtual <= $dataDescontoAntecipado2) {
                    $arrDados['valorDescontoAntecipado'] = $arrDados['valorDescontoAntecipado2'];
                }
            }
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    public function calcularDescontoDeIncentivo(\Financeiro\Entity\FinanceiroTitulo $objTitulo)
    {
        if (!$objTitulo) {
            $this->setLastError('Título não encontrado!');

            return false;
        }

        $serviceTituloConfig   = new \Financeiro\Service\FinanceiroTituloConfig($this->getEm());
        $serviceDesconto       = new \Financeiro\Service\FinanceiroDesconto($this->getEm());
        $serviceDescontoTitulo = new \Financeiro\Service\FinanceiroDescontoTitulo($this->getEm());
        $serviceDescontoTipo   = new \Financeiro\Service\FinanceiroDescontoTipo($this->getEm());
        $serviceConfig         = new \Sistema\Service\SisConfig($this->getEm());

        $formaCalculoIncentivo = $serviceConfig->localizarChave('FINANCEIRO_CONFIG_CALCULO_DESCONTO_INCENTIVO');

        if (!$formaCalculoIncentivo) {
            $formaCalculoIncentivo = $serviceDescontoTitulo::FINANCEIRO_CONFIG_CALCULO_DESCONTO_INCENTIVO_VALOR_BRUTO;
        }

        //Pesquisa configuração válida baseada no tipo de título e cursocampus
        /** @var \Financeiro\Entity\FinanceiroTituloConfig $objTituloConfig */
        $objTituloConfig = $objTitulo->getTituloconf();

        if (!$objTituloConfig) {
            $this->setLastError($serviceTituloConfig->getLastError());

            return array();
        }

        $arrDescontosPreFixados = $serviceDesconto->retornarInformacoesDeDescontosAtivos(
            ['tituloId' => $objTitulo->getTituloId()]
        );

        $dataVencimentoTitulo    = $objTitulo->getTituloDataVencimento();
        $dataDescontoAntecipado1 = clone($dataVencimentoTitulo);
        $dataDescontoAntecipado2 = clone($dataVencimentoTitulo);

        $descontoLimitadoAoVencimento    = 0;
        $descontoNaoLimitadoAoVencimento = 0;
        $desctipoDescontoAcumulativo     = false;

        $arrDescontosLimitados = array();

        foreach ($arrDescontosPreFixados as $desconto) {
            if ($desconto['desctipo_limita_vencimento'] == $serviceDescontoTipo::DESCTIPO_LIMITA_VENCIMENTO_SIM) {
                $descontoLimitadoAoVencimento += $desconto['desconto_aplicacao_valor'];
            } else {
                $descontoNaoLimitadoAoVencimento += $desconto['desconto_aplicacao_valor'];
            }

            if (
                $desconto['desctipo_desconto_acumulativo'] == $serviceDescontoTipo::DESCTIPO_DESCONTO_ACUMULATIVO_NAO &&
                $desconto['desconto_aplicacao_valor'] > 0 && !$desctipoDescontoAcumulativo
            ) {
                $desctipoDescontoAcumulativo = $desconto['desctipo_desconto_acumulativo'];
            }
        }

        $tituloconfDiaDesc1 = (1 * $objTituloConfig->getTituloconfDiaDesc());

        if ($tituloconfDiaDesc1 < 0) {
            $tituloconfDiaDesc1 *= -1;
            $dataDescontoAntecipado1->add(new \DateInterval('P' . $tituloconfDiaDesc1 . 'D'));
        } else {
            $dataDescontoAntecipado1->sub(new \DateInterval('P' . $tituloconfDiaDesc1 . 'D'));
        }

        $tituloconfDiaDesc2 = (1 * $objTituloConfig->getTituloconfDiaDesc2());

        if ($tituloconfDiaDesc2 < 0) {
            $tituloconfDiaDesc2 *= -1;
            $dataDescontoAntecipado2->add(new \DateInterval('P' . $tituloconfDiaDesc2 . 'D'));
        } else {
            $dataDescontoAntecipado2->sub(new \DateInterval('P' . $tituloconfDiaDesc2 . 'D'));
        }

        $tituloValorBruto = $objTitulo->getTituloValor();

        if ($descontoNaoLimitadoAoVencimento < $tituloValorBruto) {
            $tituloValorLiquido = $tituloValorBruto - $descontoNaoLimitadoAoVencimento;
        } else {
            $tituloValorLiquido = $tituloValorBruto;
        }

        $dataAtual = new \Datetime(date('Y-m-d'));

        $valorDescontoAntecipado1 = 0;
        $valorDescontoAntecipado2 = 0;

        $descontoAcumulativoComDescontoDeIncentivo = (
            $desctipoDescontoAcumulativo != $serviceDescontoTipo::DESCTIPO_DESCONTO_ACUMULATIVO_NAO
        );

        $descontoAcumulativoTitulo = $objTituloConfig->getTituloconfDescontoIncentivoAcumulativo();
        $descontoAcumulativoTitulo = (
            $descontoAcumulativoTitulo == $serviceTituloConfig::TITULOCONF_DESCONTO_INCENTIVO_ACUMULATIVO_SIM
        );

        $descontosSaoAcumulativos = ($descontoAcumulativoComDescontoDeIncentivo && $descontoAcumulativoTitulo);

        if ($dataAtual <= $dataDescontoAntecipado1) {
            if ($descontosSaoAcumulativos || $descontoLimitadoAoVencimento == 0) {
                if ($objTituloConfig->getTituloconfValorDesc()) {
                    $valorDescontoAntecipado1 = $objTituloConfig->getTituloconfValorDesc();
                } elseif ($formaCalculoIncentivo == $serviceDescontoTitulo::FINANCEIRO_CONFIG_CALCULO_DESCONTO_INCENTIVO_VALOR_LIQUIDO) {
                    $valorDescontoAntecipado1 = ($tituloValorLiquido * $objTituloConfig->getTituloconfPercentDesc());
                } elseif ($formaCalculoIncentivo == $serviceDescontoTitulo::FINANCEIRO_CONFIG_CALCULO_DESCONTO_INCENTIVO_VALOR_BRUTO) {
                    $valorDescontoAntecipado1 = ($tituloValorBruto * $objTituloConfig->getTituloconfPercentDesc());
                }
            }
        }

        if ($dataAtual <= $dataDescontoAntecipado2) {
            if ($descontosSaoAcumulativos || $descontoLimitadoAoVencimento == 0) {
                if ($objTituloConfig->getTituloconfValorDesc2()) {
                    $valorDescontoAntecipado2 = $objTituloConfig->getTituloconfValorDesc2();
                } elseif ($formaCalculoIncentivo == $serviceDescontoTitulo::FINANCEIRO_CONFIG_CALCULO_DESCONTO_INCENTIVO_VALOR_LIQUIDO) {
                    $valorDescontoAntecipado2 = ($tituloValorLiquido * $objTituloConfig->getTituloconfPercentDesc2());
                } elseif ($formaCalculoIncentivo == $serviceDescontoTitulo::FINANCEIRO_CONFIG_CALCULO_DESCONTO_INCENTIVO_VALOR_BRUTO) {
                    $valorDescontoAntecipado2 = ($tituloValorBruto * $objTituloConfig->getTituloconfPercentDesc2());
                }
            }
        }

        if ($valorDescontoAntecipado1 > $tituloValorLiquido) {
            $valorDescontoAntecipado1 = 0;
        }

        if ($valorDescontoAntecipado2 > $tituloValorLiquido) {
            $valorDescontoAntecipado2 = 0;
        }

        $valorDescontoAntecipado = 0;
        $dataDescontoAntecipado  = $dataVencimentoTitulo;

        if ($valorDescontoAntecipado1 && $dataAtual <= $dataDescontoAntecipado1) {
            $valorDescontoAntecipado = $valorDescontoAntecipado1;
            $dataDescontoAntecipado  = $dataDescontoAntecipado1;
        } elseif ($valorDescontoAntecipado2 && $dataAtual <= $dataDescontoAntecipado2) {
            $valorDescontoAntecipado = $valorDescontoAntecipado2;
            $dataDescontoAntecipado  = $dataDescontoAntecipado2;
        }

        if ($valorDescontoAntecipado1) {
            $arrDescontosLimitados[] = [
                'diaVencimentoCobrar' => $dataDescontoAntecipado1->format('Ymd') . '.0',
                'descontoValor'       => $valorDescontoAntecipado1,
                'dataDesconto'        => $dataDescontoAntecipado1->format('d/m/Y'),
                'descontoIncentivo'   => true
            ];
        }

        if ($valorDescontoAntecipado2) {
            $arrDescontosLimitados[] = [
                'diaVencimentoCobrar' => $dataDescontoAntecipado2->format('Ymd') . '.0',
                'descontoValor'       => $valorDescontoAntecipado2,
                'dataDesconto'        => $dataDescontoAntecipado2->format('d/m/Y'),
                'descontoIncentivo'   => true
            ];
        }

        if ($descontoLimitadoAoVencimento) {
            $arrDescontosLimitados[] = [
                'dataDesconto'        => $dataVencimentoTitulo->format('d/m/Y'),
                'descontoValor'       => $descontoLimitadoAoVencimento,
                'diaVencimentoCobrar' => $dataVencimentoTitulo->format('Ymd') . '.1',
            ];
        }

        usort(
            $arrDescontosLimitados,
            function ($a, $b) {
                return $a['diaVencimentoCobrar'] > $b['diaVencimentoCobrar'];
            }
        );

        $arrDescontos = array();

        foreach ($arrDescontosLimitados as $posicaoDesconto => $desconto) {
            $descontoValor          = $desconto['descontoValor'];
            $posicaoProximoDesconto = $posicaoDesconto + 1;

            while ($posicaoProximoDesconto < count($arrDescontosLimitados)) {
                $proximoDesconto = $arrDescontosLimitados[$posicaoProximoDesconto];

                if ($desconto['diaVencimentoCobrar'] !== false && $desconto['diaVencimentoCobrar'] <= $proximoDesconto['diaVencimentoCobrar']) {
                    if (!$proximoDesconto['descontoIncentivo']) {
                        $descontoValor += $proximoDesconto['descontoValor'];
                    }
                }

                if (round($desconto['diaVencimentoCobrar']) == round($proximoDesconto['diaVencimentoCobrar'])) {
                    $arrDescontosLimitados[$posicaoProximoDesconto]['diaVencimentoCobrar'] = false;
                }

                $posicaoProximoDesconto++;
            }

            if ($arrDescontosLimitados[$posicaoDesconto]['diaVencimentoCobrar']) {
                $arrDescontos[] = [
                    'data'     => $desconto['dataDesconto'],
                    'valor'    => $tituloValorLiquido - $descontoValor,
                    'desconto' => $descontoValor,
                ];
            }
        }

        return [
            'dataDescontoAntecipado'                    => $dataDescontoAntecipado,
            'valorDescontoAntecipado'                   => $valorDescontoAntecipado,
            'dataDescontoAntecipado2'                   => $dataDescontoAntecipado2,
            'valorDescontoAntecipado2'                  => $valorDescontoAntecipado2,
            'dataDescontoAntecipado1'                   => $dataDescontoAntecipado1,
            'valorDescontoAntecipado1'                  => $valorDescontoAntecipado1,
            'descontoLimitadoAoVencimento'              => $descontoLimitadoAoVencimento,
            'descontoNaoLimitadoAoVencimento'           => $descontoNaoLimitadoAoVencimento,
            'dataVencimentoTitulo'                      => $dataVencimentoTitulo,
            'tituloValorBruto'                          => $tituloValorBruto,
            'tituloValorLiquido'                        => $tituloValorLiquido,
            'descontoAcumulativoComDescontoDeIncentivo' => $descontoAcumulativoComDescontoDeIncentivo,
            'descontos'                                 => $arrDescontos,
        ];
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $servicePessoa               = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceFinanceiroTituloTipo = new \Financeiro\Service\FinanceiroTituloTipo($this->getEm());
        $serviceAcessoPessoas        = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceAcadgeralAlunoCurso  = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());

        if (is_array($arrDados['pes']) && !$arrDados['pes']['text']) {
            $arrDados['pes'] = $arrDados['pes']['pesId'];
        }

        if ($arrDados['pes'] && is_string($arrDados['pes'])) {
            $arrDados['pes'] = $servicePessoa->getArrSelect2(array('id' => $arrDados['pes']));
            $arrDados['pes'] = $arrDados['pes'] ? $arrDados['pes'][0] : null;
        }

        if (is_array($arrDados['tipotitulo']) && !$arrDados['tipotitulo']['text']) {
            $arrDados['tipotitulo'] = $arrDados['tipotitulo']['tipotituloId'];
        }

        if ($arrDados['tipotitulo'] && is_string($arrDados['tipotitulo'])) {
            $arrDados['tipotitulo'] = $serviceFinanceiroTituloTipo->getArrSelect2(
                array('id' => $arrDados['tipotitulo'])
            );
            $arrDados['tipotitulo'] = $arrDados['tipotitulo'] ? $arrDados['tipotitulo'][0] : null;
        }

        if (is_array($arrDados['usuarioBaixa']) && !$arrDados['usuarioBaixa']['text']) {
            $arrDados['usuarioBaixa'] = $arrDados['usuarioBaixa']['id'];
        }

        if ($arrDados['usuarioBaixa'] && is_string($arrDados['usuarioBaixa'])) {
            $arrDados['usuarioBaixa'] = $serviceAcessoPessoas->getArrSelect2(array('id' => $arrDados['usuarioBaixa']));
            $arrDados['usuarioBaixa'] = $arrDados['usuarioBaixa'] ? $arrDados['usuarioBaixa'][0] : null;
        }

        if (is_array($arrDados['usuarioAutor']) && !$arrDados['usuarioAutor']['text']) {
            $arrDados['usuarioAutor'] = $arrDados['usuarioAutor']['id'];
        }

        if ($arrDados['usuarioAutor'] && is_string($arrDados['usuarioAutor'])) {
            $arrDados['usuarioAutor'] = $serviceAcessoPessoas->getArrSelect2(array('id' => $arrDados['usuarioAutor']));
            $arrDados['usuarioAutor'] = $arrDados['usuarioAutor'] ? $arrDados['usuarioAutor'][0] : null;
        }

        if (is_array($arrDados['alunocurso']) && !$arrDados['alunocurso']['text']) {
            $arrDados['alunocurso'] = $arrDados['alunocurso']['alunocursoId'];
        }

        if ($arrDados['alunocurso'] && is_string($arrDados['alunocurso'])) {
            $arrDados['alunocurso'] = $serviceAcadgeralAlunoCurso->getArrSelect2(
                array('id' => $arrDados['alunocurso'])
            );
            $arrDados['alunocurso'] = $arrDados['alunocurso'] ? $arrDados['alunocurso'][0] : null;
        }

        return $arrDados;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('tituloId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Financeiro\Entity\FinanceiroTitulo */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getTituloId(),
                $params['value'] => $objEntity->getTituloDescontoManual()
            );
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $param
     * @return bool
     */
    public function remover($param)
    {
        if (!$param['tituloId']) {
            $this->setLastError('Para remover um registro de título é necessário informar o código.');

            return false;
        }

        if ($this->tituloUsaMaxiPago($param['titulos'])) {
            return false;
        }

        try {
            /** @var $objFinanceiroTitulo \Financeiro\Entity\FinanceiroTitulo */
            $objFinanceiroTitulo = $this->getRepository()->find($param['tituloId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objFinanceiroTitulo);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de título.');

            return false;
        }

        return true;
    }

    /**
     * @param $tituloId
     * @return bool
     */
    public function incrementaComprovante($tituloId)
    {
        if (!$tituloId) {
            $this->setLastError('Para incrementar um comprovante de título é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objFinanceiroTitulo \Financeiro\Entity\FinanceiroTitulo */
            $objFinanceiroTitulo = $this->getRepository()->find($tituloId);

            $objFinanceiroTitulo->setTituloComprovanteVia(
                $objFinanceiroTitulo->getTituloComprovanteVia() + 1
            );

            $this->getEm()->beginTransaction();
            $this->getEm()->persist($objFinanceiroTitulo);
            $this->getEm()->flush($objFinanceiroTitulo);
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao incrementar comprovante de título.');

            return false;
        }

        return true;
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $servicePessoa               = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceFinanceiroTituloTipo = new \Financeiro\Service\FinanceiroTituloTipo($this->getEm());
        $serviceFinanceiroFiltro     = new \Financeiro\Service\FinanceiroFiltro($this->getEm());

        $servicePessoa->setarDependenciasView($view);
        $serviceFinanceiroTituloTipo->setarDependenciasView($view);

        $view->setVariable("arrTituloTipoPagamento", $this->getArrSelect2TituloTipoPagamento());
        $view->setVariable("arrTituloEstado", $this->getArrSelect2TituloEstado());
        $view->setVariable("arrFiltros", $serviceFinanceiroFiltro->retornaFiltrosAtivosPainel());
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2TituloTipoPagamento($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getTituloTipoPagamento());
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2TituloEstado($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getTituloEstado());
    }

    /**
     * @param $alunocursoId
     * @return bool
     */
    public function gerarTitulosPelaConfiguracaoDoCursoDoAluno($alunocursoId)
    {
        $serviceAlunoConfigPgtoCurso = new \Financeiro\Service\FinanceiroAlunoConfigPgtoCurso($this->getEm());
        $servicePeriodoAluno         = new \Matricula\Service\AcadperiodoAluno($this->getEm());
        $serviceAlunoCurso           = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
        $arrRetorno                  = [];

        try {
            /* @var $objAlunoCurso \Matricula\Entity\AcadgeralAlunoCurso */
            $objAlunoCurso = $serviceAlunoCurso->getRepository()->find($alunocursoId);
            $alunocursoId  = $objAlunoCurso->getAlunocursoId();

            $arrFinanceiroAlunoConfigPgtoCursos =
                $serviceAlunoConfigPgtoCurso->retornaConfiguracoesDePagamentoDoCurso($alunocursoId, false, true);

            if (!$arrFinanceiroAlunoConfigPgtoCursos) {
                return true;
            }

            $this->getEm()->beginTransaction();

            /** @var $objAlunoConfigPgtoCurso \Financeiro\Entity\FinanceiroAlunoConfigPgtoCurso */
            foreach ($arrFinanceiroAlunoConfigPgtoCursos as $objAlunoConfigPgtoCurso) {
                $objTituloTipo = $objAlunoConfigPgtoCurso->getTipotitulo();

                if ($objAlunoConfigPgtoCurso->cursoJaFoiEfetivado()) {
                    continue;
                }

                $dataVencimento = $objAlunoConfigPgtoCurso->getConfigPgtoCursoVencimento();

                /*if ($objTituloTipo->getTipotituloTempoVencimento()) {
                    $strInt = 'P' . $objTituloTipo->getTipotituloTempoVencimento() . 'D';

                    if (
                        $objTituloTipo->getTipotituloTempoVencimentoTipo() ==
                        $serviceTituloTipo::TIPOTITULO_TEMPO_VENCIMENTO_TIPO_MES
                    ) {
                        $strInt = 'P' . $objTituloTipo->getTipotituloTempoVencimento() . 'M';
                    }

                    $dataVencimento = $dataVencimento->add(new \DateInterval($strInt));
                }*/

                $dataVencimento = $dataVencimento->format('Y-m-d');

                $arrTitulo = [
                    'pes'                  => $objAlunoConfigPgtoCurso
                        ->getAlunocurso()
                        ->getAluno()
                        ->getPes()
                        ->getPes()
                        ->getPesId(),
                    'tipotitulo'           => $objAlunoConfigPgtoCurso
                        ->getTipotitulo()
                        ->getTipotituloId(),
                    'cartao'               => $objAlunoConfigPgtoCurso->getCartao(),
                    'alunocurso'           => $alunocursoId,
                    'alunoper'             => $servicePeriodoAluno->buscaAlunoperiodoPorMatricula($alunocursoId),
                    'valores'              => $objAlunoConfigPgtoCurso->getValores(),
                    'tituloDataVencimento' => $dataVencimento,
                    'tituloValorParcelas'  => $objAlunoConfigPgtoCurso->getConfigPgtoCursoValor(),
                    'tituloNumeroParcelas' => $objAlunoConfigPgtoCurso->getConfigPgtoCursoParcela()
                ];

                $tipoTitulo = $objTituloTipo->getTipotituloId();
                $retorno    = $this->criarNovosTitulos($arrTitulo, true, false);

                if ($retorno === false) {
                    return false;
                }

                if ($objAlunoConfigPgtoCurso->getCartao()) {
                    $arrRetorno[$tipoTitulo]                         = $retorno;
                    $arrRetorno[$tipoTitulo]['cursoconfigPgtocurso'] = $objAlunoConfigPgtoCurso;
                }

                $objAlunoConfigPgtoCurso->setConfigPgtoCursoEfetivado(
                    $serviceAlunoConfigPgtoCurso::CONFIG_PGTO_CURSO_EFETIVADO_SIM
                );

                $this->getEm()->persist($objAlunoConfigPgtoCurso);
                $this->getEm()->flush($objAlunoConfigPgtoCurso);
            }

            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao criar títulos.<br>' . $ex->getMessage());

            return false;
        }

        return $arrRetorno;
    }

    /**
     * @param $param
     * @return bool
     * @throws \Exception
     */
    public function renegociarTitulos($param)
    {
        $servicePessoa        = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceTituloTipo    = new \Financeiro\Service\FinanceiroTituloTipo($this->getEm());

        if (!$param['titulos']) {
            $this->setLastError('Para renegociar títulos é necessário informar ao menos um código de título.');

            return false;
        }

        if ($this->tituloUsaMaxiPago($param['titulos'])) {
            return false;
        }

        if (!$param['responsavelTitulo']) {
            $this->setLastError('Para renegociar títulos é necessário informar quem será o responsável pelo título.');

            return false;
        }

        /** @var $objPessoa \Pessoa\Entity\Pessoa */
        $objPessoa = $servicePessoa->getRepository()->find($param['responsavelTitulo']);

        if (!$objPessoa) {
            $this->setLastError('A pessoa informada não existe!');

            return false;
        }

        /** @var $objTituloTipo \Financeiro\Entity\FinanceiroTituloTipo */
        $objTituloTipo = $serviceTituloTipo->getTipoRenegociacao();

        $numeroTitulos     = false;
        $valorTotalTitulos = false;
        $valorTotalMulta   = false;
        $valorTotalJuros   = false;
        $diasAtraso        = false;
        $mediaAtraso       = false;
        $valorTotalAPagar  = false;
        $valorFinal        = false;
        $valorParcela      = false;
        $acrescimo         = $param['acrescimo'];
        $descontoManual    = $param['desconto'];
        $parcelas          = $param['parcelas'];
        $observacao        = $param['observacao'];
        $vencimento        = $param['vencimento'];
        $isentaJuros       = $param['isentarJuros'];
        $isentaMulta       = $param['isentarMulta'];

        if (!$parcelas) {
            $this->setLastError('O número de parcelas para renegociação é inválido!');

            return false;
        }

        $arrTitulos = $this->retornaTitulosComInformacoesExtras($param['titulos']);

        if (count($arrTitulos) != count($param['titulos'])) {
            $this->setLastError('O número de títulos encontrados não corresponde ao número de títulos informados.');

            return false;
        }

        $arrAlunoCurso = array();

        foreach ($arrTitulos as $arrTitulo) {
            if ($arrTitulo['titulo_estado'] != self::TITULO_ESTADO_ABERTO) {
                $this->setLastError('O título ' . $arrTitulo['titulo_id'] . ' não está com estado aberto.');

                return false;
            }
            if ($arrTitulo['alunocurso_id']) {
                if (!$arrAlunoCurso[$arrTitulo['alunocurso_id']]) {
                    $arrAlunoCurso[$arrTitulo['alunocurso_id']] = array(
                        'alunocursoId' => $arrTitulo['alunocurso_id'],
                        'qtd'          => 0,
                        'valor'        => 0,
                    );
                }

                $arrAlunoCurso[$arrTitulo['alunocurso_id']]['qtd'] += 1;
                $arrAlunoCurso[$arrTitulo['alunocurso_id']]['valor'] += $arrTitulo['titulo_valor'];
            }

            $valorTotalTitulos += $arrTitulo['titulo_valor'];

            if (!$isentaMulta) {
                $valorTotalMulta += $arrTitulo['titulo_multa_calc'];
            }
            if (!$isentaJuros) {
                $valorTotalJuros += $arrTitulo['titulo_juros_calc'];
            }
        }

        //Encontra aluno com maior valor na negociação
        uksort(
            $arrAlunoCurso,
            function ($a, $b) {
                //Alterar para 'qtd' para pegar o aluno com maior quantidade de títulos
                $chave = 'valor';

                if ($a[$chave] == $b[$chave]) {
                    $r = 0;
                } else {
                    $r = ($a[$chave] > $b[$chave]) ? 1 : -1;
                }

                return $r;
            }
        );

        $alunoCurso   = array_shift($arrAlunoCurso);
        $alunocursoId = $alunoCurso['alunocursoId'] ? $alunoCurso['alunocursoId'] : null;

        $valorTotalJuros = $param['valorTotalJuros'];
        $valorTotalMulta = $param['valorTotalMulta'];

        if ($param['isentarJuros'] == 'true') {
            $valorTotalJuros = 0;
        }

        if ($param['isentarMulta'] == 'true') {
            $valorTotalMulta = 0;
        }

        $valorTotalAPagar = ($valorTotalJuros + $valorTotalMulta + $valorTotalTitulos);
        $valorFinal       = round(($valorTotalAPagar + $acrescimo - $descontoManual), 2);
        $valorParcela     = round($valorFinal / $parcelas, 2);

        if ($valorFinal != $param['valorFinal'] && $valorParcela != $param['valorParcela']) {
            $this->setLastError(
                'Os valores negociados estão divergindo.<br>' .
                'Tente remover e adiconar novamente os títulos.'
            );

            return false;
        }

        $percentualDescontoManual = ($descontoManual * 100) / $valorTotalAPagar;

        $maxParcelas           = $this->getConfig()->localizarChave('FINANCEIRO_MAX_PARCELAS');
        $maxPercentualDesconto = $this->getConfig()->localizarChave('FINANCEIRO_MAX_PERCENTUAL_DESCONTO');
        $grupoSupervisao       = $this->getConfig()->localizarChave('GRUPO_FINANCEIRO_SUPERVISAO');

        if (($parcelas > $maxParcelas) || ($percentualDescontoManual > $maxPercentualDesconto)) {
            $usuarioSupervisor = $serviceAcessoPessoas->validaUsuarioEVerificaSeEstaNoGrupo(
                $param['login'],
                $param['senha'],
                $grupoSupervisao
            );

            if (!$usuarioSupervisor) {
                $this->setLastError($serviceAcessoPessoas->getLastError());

                return false;
            }
        }

        $dataAtual          = date('Ymd');
        $dataAtualFormatada = date('Y-m-d');

        try {
            $this->getEm()->beginTransaction();

            $objUsuarioBaixa = $serviceAcessoPessoas->retornaUsuarioLogado();

            $arrTituloRenegociacao = [
                'pes'                     => $objPessoa->getPesId(),
                'tipotitulo'              => $objTituloTipo->getTipotituloId(),
                'tituloDescricao'         => (
                    $objTituloTipo->getTipotituloNome() . ' - ' .
                    date('d/m/Y') . ' - ' .
                    count($param['titulos']) . ' títulos'
                ),
                'alunocurso'              => $alunocursoId,
                'tituloParcela'           => 1,
                'usuarioBaixa'            => $objUsuarioBaixa->getId(),
                'tituloDataProcessamento' => $dataAtualFormatada,
                'tituloDataVencimento'    => $dataAtualFormatada,
                'tituloValor'             => $valorFinal,
                'tituloObservacoes'       => (
                    $observacao . "\n" .
                    $objTituloTipo->getTipotituloNome() . ' - ' .
                    date('d/m/Y') . ' - ' .
                    implode(', ', $param['titulos'])
                ),
                'tituloEstado'            => self::TITULO_ESTADO_ALTERACAO,
            ];

            if (!$this->save($arrTituloRenegociacao)) {
                return false;
            }

            /** @var $objTituloRenegociacao \Financeiro\Entity\FinanceiroTitulo */
            $objTituloRenegociacao = $this->getRepository()->find($arrTituloRenegociacao['tituloId']);

            foreach ($arrTitulos as $arrTitulo) {
                /** @var $objFinanceiroTitulo \Financeiro\Entity\FinanceiroTitulo */
                $objFinanceiroTitulo = $this->getRepository()->find($arrTitulo['titulo_id']);
                $objFinanceiroTitulo
                    ->setTituloJuros($arrTitulo['titulo_juros_calc'])
                    ->setTituloMulta($arrTitulo['titulo_multa_calc'])
                    ->setTituloValorPago(
                        $arrTitulo['titulo_valor'] +
                        $arrTitulo['titulo_multa_calc'] +
                        $arrTitulo['titulo_juros_calc']
                    )
                    ->setTituloDataBaixa(new \Datetime())
                    ->setUsuarioBaixa($objUsuarioBaixa)
                    ->setTituloEstado(self::TITULO_ESTADO_ALTERACAO);

                $objTituloAlteracao = new \Financeiro\Entity\FinanceiroTituloAlteracao();
                $objTituloAlteracao
                    ->setTituloIdOrigem($objFinanceiroTitulo)
                    ->setTituloIdDestino($objTituloRenegociacao)
                    ->setAlteracaoAcao(\Financeiro\Service\FinanceiroTituloAlteracao::ALTERACAO_ACAO_RENEGOCIACAO);

                $this->getEm()->persist($objFinanceiroTitulo);
                $this->getEm()->flush($objFinanceiroTitulo);

                $this->getEm()->persist($objTituloAlteracao);
                $this->getEm()->flush($objTituloAlteracao);
            }

            for ($i = 0; $i < $parcelas; $i++) {
                $dataVencimento    = $this->retornaDataVencimento($vencimento, $i, true);
                $arrParamDescricao = [
                    'tipotituloNome'       => $objTituloTipo->getTipotituloNome(),
                    'parcela'              => ($i + 1),
                    'totalParcelas'        => $parcelas,
                    'tituloDataVencimento' => $dataVencimento->format("d/m/Y"),
                ];

                $arrTitulo = [
                    'pes'                     => $objPessoa->getPesId(),
                    'tipotitulo'              => $objTituloTipo->getTipotituloId(),
                    'tituloDescricao'         => $this->formatarDescricaoPadraoTitulo($arrParamDescricao),
                    'alunocurso'              => $alunocursoId,
                    'tituloParcela'           => ($i + 1),
                    'tituloDataProcessamento' => $dataAtualFormatada,
                    'tituloDataVencimento'    => $dataVencimento,
                    'tituloValor'             => $valorParcela,
                    'tituloObservacoes'       => $observacao,
                    'tituloEstado'            => self::TITULO_ESTADO_ABERTO,
                ];

                if (!$this->save($arrTitulo)) {
                    return false;
                }

                /** @var $objTituloParcela \Financeiro\Entity\FinanceiroTitulo */
                $objTituloParcela = $this->getRepository()->find($arrTitulo['tituloId']);

                $serviceBoleto = new \Financeiro\Service\Boleto($this->getEm());

                if (!$serviceBoleto->registrarBoletoParaTitulo($objTituloParcela)) {
                    $this->setLastError($serviceBoleto->getLastError());

                    return false;
                }

                $objTituloAlteracao = new \Financeiro\Entity\FinanceiroTituloAlteracao();
                $objTituloAlteracao
                    ->setTituloIdOrigem($objTituloRenegociacao)
                    ->setTituloIdDestino($objTituloParcela)
                    ->setAlteracaoAcao(\Financeiro\Service\FinanceiroTituloAlteracao::ALTERACAO_ACAO_RENEGOCIACAO);

                $this->getEm()->persist($objTituloAlteracao);
                $this->getEm()->flush($objTituloAlteracao);
            }

            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao renegociar títulos.<br>' . $ex->getMessage());

            return false;
        }

        return true;
    }

    /**
     * @param $param
     * @return bool
     * @throws \Exception
     */
    public function pagamentoTitulosManual($param)
    {
        $servicePessoa              = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceAcessoPessoas       = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceTituloTipo          = new \Financeiro\Service\FinanceiroTituloTipo($this->getEm());
        $serviceInscricaoVestibular = new \Vestibular\Service\SelecaoInscricao($this->getEm());
        $servicePagamento           = new \Financeiro\Service\FinanceiroPagamento($this->getEm());
        $serviceDescontoTitulo      = new \Financeiro\Service\FinanceiroDescontoTitulo($this->getEm());
        $serviceSisConfig           = new \Sistema\Service\SisConfig($this->getEm());
        $serviceCheque              = new \Financeiro\Service\FinanceiroCheque($this->getEm());

        foreach ($param['pagamentos'] as $pagamento) {
            if ($pagamento['meioPagamentoCamposCheque'] == 'Sim') {
                if ($pagamento['chequeBanco'] AND $pagamento['chequeAgencia'] AND $pagamento['chequeConta'] AND $pagamento['chequeNum']) {
                    $dadosCheque = $serviceCheque->pesquisaForJson(
                        [
                            'chequeBanco'   => $pagamento['chequeBanco'],
                            'chequeAgencia' => $pagamento['chequeAgencia'],
                            'chequeConta'   => $pagamento['chequeConta'],
                            'chequeNum'     => $pagamento['chequeNum']
                        ]
                    );

                    if ($dadosCheque) {
                        $saldoCheque = $serviceCheque->retornaSaldoCheque($dadosCheque[0]['cheque_id']);
                        $saldoCheque = floatval($saldoCheque['valorTotalCheque'] - $saldoCheque['valorUsado']);

                        if ($saldoCheque < floatval($param['pagamentoValor'])) {
                            $this->setLastError(
                                'Este cheque já foi cadastrado e não possui saldo suficiente para o valor do pagamento!'
                            );

                            return false;
                        }
                    }
                } else {
                    $this->setLastError("Informações incompletas para o meio de pagamento: Cheque");

                    return false;
                }
            }
        }

        if (!$param['titulos']) {
            $this->setLastError('Para efetuar pagamento é necessário informar ao menos um código de título.');

            return false;
        }

        if ($this->tituloUsaMaxiPago($param['titulos'])) {
            return false;
        }

        if (!$param['responsavelTitulo']) {
            $this->setLastError('Para efetuar pagamento é necessário informar quem será o responsável pelo título.');

            return false;
        }

        if ($param['pagamentoValor'] > $param['valorFinal'] && !$param['meioTroco']) {
            $this->setLastError('É necessário informar em qual meio de pagamento sera aplicado o troco!');

            return false;
        }

        if ($param['meioTroco']) {
            if (
                self::convertNumberFromBRToUS(
                    $param['pagamentoTroco']
                ) > $param['pagamentos'][$param['meioTroco']]['pagamentoValor']
            ) {
                $this->setLastError('O troco não pode ser maior que meio de pagamento aplicado!');

                return false;
            } else {
                $pagamentoValor = $param['pagamentos'][$param['meioTroco']]['pagamentoValor'];

                $param['pagamentos'][$param['meioTroco']]['pagamentoValorBruto'] = $pagamentoValor;
                $param['pagamentos'][$param['meioTroco']]['pagamentoValor']      = ($pagamentoValor - $param['pagamentoTroco']);
                $param['pagamentos'][$param['meioTroco']]['pagamentoValorFinal'] = ($pagamentoValor - $param['pagamentoTroco']);
            }
        }

        $pagamentos          = $param['pagamentos'] ? $param['pagamentos'] : array();
        $param['pagamentos'] = $pagamentos;

        if (!$pagamentos) {
            $this->setLastError('Para efetuar pagamento é necessário informar ao menos uma meio de pagamento.');

            return false;
        }

        $arrMeioPagamentos        = [];
        $arrCheque                = [];
        $meiodePagamentoDuplicado = false;
        $chequeDuplicado          = false;
        $totalizador              = 0;

        foreach ($pagamentos as $pagamento) {
            $totalizador = $totalizador + $pagamento['valorPagamento'];
        }

        if ($param['lancarDiferencaNoMeioDePagamentoPadrao']) {
            $meioPagamentoPadrao = $serviceSisConfig->localizarChave('FINANCEIRO_MEIO_PAGAMENTO_PADRAO');

            //TODO: impossibilitar que o meio de pagamento cheque seja lançado de forma automática

            if (!$meioPagamentoPadrao) {
                $this->setLastError('Não foi localizado nenhum meio padrão de pagamento!');

                return false;
            }

            $totalizadorDiferenca = (
            (floatval($param['valorFinal']) - floatval($totalizador))
            );

            foreach ($pagamentos as $pagamento) {
                if ($meioPagamentoPadrao == $pagamento['meioPagamentoId'] && $totalizadorDiferenca > 0) {
                    $pagamentos[$pagamento['pagamentoGUID']]['pagamentoValor']          = (
                        $pagamento['pagamentoValor'] + $totalizadorDiferenca
                    );
                    $pagamentos[$pagamento['pagamentoGUID']]['pagamentoValorFormatado'] = (
                    number_format($pagamentos[$pagamento['pagamentoGUID']]['pagamentoValor'], 2, ',', '.')
                    );

                    $totalizadorDiferenca = 0;
                }
            }

            if ($totalizadorDiferenca > 0) {
                $pagamentos['pagPadrao']['meioPagamentoId']        = $meioPagamentoPadrao;
                $pagamentos['pagPadrao']['pagamentoMeioPagamento'] = $meioPagamentoPadrao;
                $pagamentos['pagPadrao']['meioPagamento']          = $meioPagamentoPadrao;
                $pagamentos['pagPadrao']['pagamentoValor']         = $totalizadorDiferenca;
            }
        } else {
            $acrescimos = floatval($param['descontoManual']) ? floatval($param['descontoManual']) : 0;

            $totalizadorDiferenca = (
                floatval($totalizador) - (floatval($param['valorFinal']) - (floatval($acrescimos)))
            );

            if ($totalizadorDiferenca < 0) {
                $this->setLastError("Erro inesperado, valor negativo na operação, tente novamente!");

                return false;
            }
        }

        /** @var array $pagamento */
        foreach ($pagamentos as $pagamento) {
            $meiodePagamentoExiste = ($arrMeioPagamentos[$pagamento['meioPagamentoId']]);
            $usoMultiplo           = ($pagamento['meioPagamentoUsoMultiplo'] == 'Sim');
            $meiodePagamentoCheque = ($pagamento['meioPagamentoCamposCheque'] == 'Sim');

            if (!$usoMultiplo && $meiodePagamentoExiste) {
                $meiodePagamentoDuplicado = true;
            }

            if ($meiodePagamentoCheque) {
                $chaveCheque = (
                    $pagamento['chequeBanco'] .
                    '+' .
                    $pagamento['chequeAgencia'] .
                    '+' .
                    $pagamento['chequeConta'] .
                    '+' .
                    $pagamento['chequeNum']
                );

                if ($arrCheque[$chaveCheque]) {
                    $chequeDuplicado = true;
                }

                $arrCheque[$chaveCheque] = true;
            }

            $arrMeioPagamentos[$pagamento['meioPagamentoId']] = true;

            if ($meiodePagamentoDuplicado == true) {
                $this->setLastError(
                    'Não é possível incluir o meio de pagamento ' .
                    $pagamento['meioPagamentoDescricao'] .
                    ' mais de uma vez!'
                );

                return false;
            }

            if ($chequeDuplicado == true) {
                $this->setLastError(
                    'Não é possível incluir o mesmo cheque mais de uma vez!'
                );

                return false;
            }
        }

        /** @var $objPessoa \Pessoa\Entity\Pessoa */
        $objPessoa = $servicePessoa->getRepository()->find($param['responsavelTitulo']);

        if (!$objPessoa) {
            $this->setLastError('A pessoa informada não existe!');

            return false;
        }

        /** @var $objTituloTipo \Financeiro\Entity\FinanceiroTituloTipo */
        $objTituloTipo = $serviceTituloTipo->getTipoPagamentoMultiplo();

        $valorTotalTitulos = 0;
        $valorTotalMulta   = 0;
        $valorTotalJuros   = 0;
        $descontos         = 0;
        $diasAtraso        = 0;
        $mediaAtraso       = 0;
        $valorTotalAPagar  = 0;
        $valorFinal        = 0;
        $valorParcela      = 0;
        $acrescimo         = $param['acrescimo'];
        $descontoManual    = $param['descontoManual'];
        $parcelas          = $param['parcelas'];
        $observacao        = $param['observacao'];
        $vencimento        = $param['vencimento'];
        $dataPagamento     = $param['pagamentoTituloData'] ? $param['pagamentoTituloData'] : (new \DateTime('now'));

        $isentarJuros = (
            $param['isentarJuros'] == 'true' ||
            $param['isentarJuros'] === true ||
            $param['isentarJuros'] == '1' ||
            $param['isentarJuros'] === 1
        );
        $isentarMulta = (
            $param['isentarMulta'] == 'true' ||
            $param['isentarMulta'] === true ||
            $param['isentarMulta'] == '1' ||
            $param['isentarMulta'] === 1
        );

        $arrTitulos = $this->retornaTitulosComInformacoesExtras($param['titulos']);

        if (count($arrTitulos) != count($param['titulos'])) {
            $this->setLastError('O número de títulos encontrados não corresponde ao número de títulos informados.');

            return false;
        }

        $dataAtual = new \Datetime(date('Y-m-d'));

        foreach ($arrTitulos as $arrTitulo) {
            if ($arrTitulo['titulo_estado'] != self::TITULO_ESTADO_ABERTO) {
                $this->setLastError('O título ' . $arrTitulo['titulo_id'] . ' não está com estado aberto.');

                return false;
            }

            $descontos += $arrTitulo['descontos'];
            $valorTotalTitulos += $arrTitulo['titulo_valor'];
            $valorTotalMulta += $arrTitulo['titulo_multa_calc'];
            $valorTotalJuros += $arrTitulo['titulo_juros_calc'];

            if ($arrTitulo['data_desconto_antecipado'] != "0") {
                $dataDescontoAntecipado = new \Datetime($arrTitulo['data_desconto_antecipado']);
            }

            if ($arrTitulo['descontos'] == 0 && $dataAtual <= $dataDescontoAntecipado) {
                $descontos += $arrTitulo['valor_desconto_antecipado'];
            }
        }

        $valorTotalAPagar = (
            ($isentarJuros ? 0 : $valorTotalJuros) +
            ($isentarMulta ? 0 : $valorTotalMulta) +
            $valorTotalTitulos
        );

        $valorFinal = round(($valorTotalAPagar + $acrescimo - ($descontoManual + $descontos)), 2);

        if (
            round($valorFinal) != round($param['valorFinal']) &&
            round($valorTotalAPagar) != round($param['valorTotalAPagar'])
        ) {
            $this->setLastError(
                'Os valores de pagamento estão divergindo.<br>' .
                'Tente remover e adiconar novamente os títulos.'
            );

            return false;
        }

        if ($valorFinal < 0) {
            $this->setLastError('O valor do pagamento deve ser maior que zero!');

            return false;
        }

        $maxParcelas           = $this->getConfig()->localizarChave('FINANCEIRO_MAX_PARCELAS');
        $maxPercentualDesconto = $this->getConfig()->localizarChave('FINANCEIRO_MAX_PERCENTUAL_DESCONTO');
        $grupoSupervisao       = $this->getConfig()->localizarChave('GRUPO_FINANCEIRO_SUPERVISAO');

        $percentualDescontoManual = ($descontoManual * 100) / $valorTotalAPagar;

        if ($percentualDescontoManual > $maxPercentualDesconto) {
            $usuarioSupervisor = $serviceAcessoPessoas->validaUsuarioEVerificaSeEstaNoGrupo(
                $param['login'],
                $param['senha'],
                $grupoSupervisao
            );

            if (!$usuarioSupervisor) {
                $this->setLastError($serviceAcessoPessoas->getLastError());

                return false;
            }
        }

        $dataAtual          = date('Ymd');
        $dataAtualFormatada = date('Y-m-d');

        try {
            $this->getEm()->beginTransaction();

            $objUsuarioBaixa = $serviceAcessoPessoas->retornaUsuarioLogado();

            $objTituloPagamentoMultiplo = null;
            $objTitulo                  = null;

            if (count($arrTitulos) > 1) {
                $arrTituloPagamento = [
                    'pes'                     => $objPessoa->getPesId(),
                    'tipotitulo'              => $objTituloTipo->getTipotituloId(),
                    'tituloDescricao'         => (
                        $objTituloTipo->getTipotituloNome() . ' - ' .
                        date('d/m/Y') . ' - ' .
                        count($param['titulos']) . ' títulos'
                    ),
                    'tituloParcela'           => 1,
                    'usuarioBaixa'            => $objUsuarioBaixa->getId(),
                    'tituloDescontoManual'    => $descontoManual,
                    'tituloAcrescimoManual'   => $acrescimo,
                    'tituloDataProcessamento' => $dataAtualFormatada,
                    'tituloDataBaixa'         => $dataAtualFormatada,
                    'tituloDataPagamento'     => $dataPagamento,
                    'tituloDataVencimento'    => $dataAtualFormatada,
                    'tituloValor'             => ($valorFinal - $acrescimo + $descontoManual),
                    'tituloValorPago'         => $valorFinal,
                    'tituloObservacoes'       => (
                        $observacao . "\n" .
                        $objTituloTipo->getTipotituloNome() . ' - ' .
                        date('d/m/Y') . ' - ' .
                        implode(', ', $param['titulos'])
                    ),
                    'tituloEstado'            => self::TITULO_ESTADO_PAGO,
                ];

                if (!$this->save($arrTituloPagamento)) {
                    return false;
                }

                /** @var $objTituloPagamentoMultiplo \Financeiro\Entity\FinanceiroTitulo */
                $objTituloPagamentoMultiplo = $this->getRepository()->find($arrTituloPagamento['tituloId']);
                $objTitulo                  = $objTituloPagamentoMultiplo;
            }

            foreach ($arrTitulos as $arrTitulo) {
                /** @var $objFinanceiroTitulo \Financeiro\Entity\FinanceiroTitulo */
                $objFinanceiroTitulo = $this->getRepository()->find($arrTitulo['titulo_id']);

                $valorMulta = $isentarMulta ? 0 : $arrTitulo['titulo_multa_calc'];
                $valorJuros = $isentarJuros ? 0 : $arrTitulo['titulo_juros_calc'];
                //Valor com acrescido de juros, multa e desconto
                $valorPago = $arrTitulo['titulo_valor_calc'];
                $valorPago -= $isentarJuros ? $arrTitulo['titulo_juros_calc'] : 0;
                $valorPago -= $isentarMulta ? $arrTitulo['titulo_multa_calc'] : 0;
                $valorPago -= $arrTitulo['valor_desconto_antecipado'];
                $valorPago -= $objTituloPagamentoMultiplo ? 0 : $descontoManual;
                $valorPago += $objTituloPagamentoMultiplo ? 0 : $acrescimo;
                $tituloObservacao = $objFinanceiroTitulo->getTituloObservacoes();
                $tituloObservacao .= $objTituloPagamentoMultiplo ? '' : "\n" . $observacao;
                $tituloEstado            = ($objTituloPagamentoMultiplo ?
                    self::TITULO_ESTADO_PAGAMENTO_MULTIPLO : self::TITULO_ESTADO_PAGO
                );
                $tituloDescontoIncentivo = self::convertNumberFromBRToUS($arrTitulo['valor_desconto_antecipado']);

                $objFinanceiroTitulo
                    ->setTituloJuros($valorJuros)
                    ->setTituloMulta($valorMulta)
                    ->setTituloDescontoManual(round($objTituloPagamentoMultiplo ? 0 : $descontoManual, 2))
                    ->setTituloAcrescimoManual(round($objTituloPagamentoMultiplo ? 0 : $acrescimo, 2))
                    ->setTituloValorPago(round($valorPago, 2))
                    ->setUsuarioBaixa($objUsuarioBaixa)
                    ->setTituloObservacoes($tituloObservacao)
                    ->setTituloEstado($tituloEstado)
                    ->setTituloDataPagamento($dataPagamento)
                    ->setTituloDataBaixa(new \DateTime())
                    ->setPagamentoDescontoIncentivo($tituloDescontoIncentivo ? $tituloDescontoIncentivo : 0.00);

                if (!$serviceDescontoTitulo->aplicarDescontosTitulo($objFinanceiroTitulo->getTituloId())) {
                    $this->setLastError($serviceDescontoTitulo->getLastError());

                    return false;
                }

                $this->getEm()->persist($objFinanceiroTitulo);
                $this->getEm()->flush($objFinanceiroTitulo);

                if ($objTituloPagamentoMultiplo) {
                    $objTituloAlteracao = new \Financeiro\Entity\FinanceiroTituloAlteracao();
                    $objTituloAlteracao
                        ->setTituloIdOrigem($objFinanceiroTitulo)
                        ->setTituloIdDestino($objTituloPagamentoMultiplo)
                        ->setAlteracaoAcao(
                            \Financeiro\Service\FinanceiroTituloAlteracao::ALTERACAO_ACAO_PAGAMENTO_MULTIPLO
                        );

                    $this->getEm()->persist($objTituloAlteracao);
                    $this->getEm()->flush($objTituloAlteracao);
                } else {
                    $objTitulo = $objFinanceiroTitulo;
                }

                if (!$this->alteraSituacaoAlunoPorTituloId($objFinanceiroTitulo)) {
                    return false;
                }

                if ($objTitulo->getTipotitulo()->getTipotituloNome() == $serviceTituloTipo::TIPOTITULOVESTIBULAR) {
                    $serviceInscricaoVestibular->atualizaSituacaoInscricaoPorBaixaBoleto(null, $objTitulo);
                }
            }

            //Gerar pagamento para titulo de renegociação
            if (!$servicePagamento->gravarPagamentosTitulo($objTitulo, $pagamentos)) {
                $this->setLastError($servicePagamento->getLastError());

                return false;
            }

            $this->getEm()->commit();

            return $objTitulo->getTituloId();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao efetuar pagamento de títulos.<br>' . $ex->getMessage());

            return false;
        }

        return true;
    }

    /**
     * @param       $titulo
     * @param       $dataPagamento
     * @param       $valorPagamento
     * @param array $arrPagamento
     * @param array $arrOutrasInformacoes
     * @return bool|int
     */
    public function registrarPagamentoTitulo(
        $titulo,
        $dataPagamento,
        $valorPagamento,
        array $arrPagamento = array(),
        $arrOutrasInformacoes = array()
    ) {
        //Caso o valor do pagamento seja igual a zero, não exigir o meio de pagamento
        if (!$arrPagamento && $valorPagamento > 0) {
            $this->setLastError('Para efetuar pagamento é necessário informar ao menos uma meio de pagamento.');

            return false;
        }

        /** @var $objFinanceiroTitulo \Financeiro\Entity\FinanceiroTitulo */
        $objFinanceiroTitulo = $this->getRepository()->find($titulo);

        if ($objFinanceiroTitulo->getTituloEstado() != self::TITULO_ESTADO_ABERTO) {
            $this->setLastError('O título informado não está aberto.');

            return false;
        }

        if ($this->tituloUsaMaxiPago($objFinanceiroTitulo)) {
            return false;
        }

        $serviceAcessoPessoas  = new \Acesso\Service\AcessoPessoas($this->getEm());
        $servicePagamento      = new \Financeiro\Service\FinanceiroPagamento($this->getEm());
        $serviceDescontoTitulo = new \Financeiro\Service\FinanceiroDescontoTitulo($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            $objUsuarioBaixa = $serviceAcessoPessoas->retornaUsuarioLogado();

            if (!$objUsuarioBaixa) {
                $objUsuarioBaixa = $objFinanceiroTitulo->getUsuarioAutor();
            }

            $tituloObservacao      = $objFinanceiroTitulo->getTituloObservacoes();
            $tituloJuros           = $objFinanceiroTitulo->getTituloJuros();
            $tituloMulta           = $objFinanceiroTitulo->getTituloMulta();
            $tituloDescontoManual  = $objFinanceiroTitulo->getTituloDescontoManual();
            $tituloAcrescimoManual = $objFinanceiroTitulo->getTituloAcrescimoManual();

            if ($arrOutrasInformacoes['tituloJuros']) {
                $tituloJuros = $arrOutrasInformacoes['tituloJuros'];
            }

            if ($arrOutrasInformacoes['tituloMulta']) {
                $tituloMulta = $arrOutrasInformacoes['tituloMulta'];
            }

            if ($arrOutrasInformacoes['tituloDescontoManual']) {
                $tituloDescontoManual = $arrOutrasInformacoes['tituloDescontoManual'];
            }

            if ($arrOutrasInformacoes['tituloAcrescimoManual']) {
                $tituloAcrescimoManual = $arrOutrasInformacoes['tituloAcrescimoManual'];
            }

            if ($arrOutrasInformacoes['tituloObservacoes']) {
                $tituloObservacao = trim($tituloObservacao . "\n" . $arrOutrasInformacoes['tituloObservacoes']);
            }

            $objFinanceiroTitulo
                ->setUsuarioBaixa($objUsuarioBaixa)
                ->setTituloJuros($tituloJuros)
                ->setTituloMulta($tituloMulta)
                ->setTituloDescontoManual($tituloDescontoManual)
                ->setTituloAcrescimoManual($tituloAcrescimoManual)
                ->setTituloValorPago(round($valorPagamento, 2))
                ->setTituloObservacoes($tituloObservacao)
                ->setTituloEstado(self::TITULO_ESTADO_PAGO)
                ->setTituloDataPagamento($dataPagamento)
                ->setTituloDataBaixa(new \DateTime());

            if (!$serviceDescontoTitulo->aplicarDescontosTitulo($objFinanceiroTitulo->getTituloId())) {
                $this->setLastError($serviceDescontoTitulo->getLastError());

                return false;
            }

            $this->getEm()->persist($objFinanceiroTitulo);
            $this->getEm()->flush($objFinanceiroTitulo);

            //Gerar pagamento para titulo de renegociação
            if (!$servicePagamento->gravarPagamentosTitulo($objFinanceiroTitulo, $arrPagamento)) {
                $this->setLastError($servicePagamento->getLastError());

                return false;
            }

            $this->getEm()->commit();

            return $objFinanceiroTitulo->getTituloId();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao efetuar pagamento de títulos.<br>' . $ex->getMessage());

            return false;
        }

        return true;
    }

    /**
     * @param $dataEmissao
     * @param $parcela
     * @return \DateTime
     */
    private function retornaDataVencimento($dataEmissao, $parcela, $aceitarDataRetroativa = false)
    {
        $dataAtual       = new \DateTime();
        $objDataEmissao  = new \DateTime($this->formatDateAmericano($dataEmissao));
        $objDataEmissao1 = new \DateTime($objDataEmissao->format('Y-m-01'));

        while ($objDataEmissao < $dataAtual && !$aceitarDataRetroativa) {
            $objDataEmissao  = $objDataEmissao->add(new \DateInterval('P1M'));
            $objDataEmissao1 = $objDataEmissao1->add(new \DateInterval('P1M'));
        }

        $objDataVencimento  = $objDataEmissao->add(new \DateInterval('P' . $parcela . 'M'));
        $objDataVencimento1 = $objDataEmissao1->add(new \DateInterval('P' . $parcela . 'M'));

        // Verifica se pulou um mês e lança para o último dia do mês anterior
        if ($objDataVencimento->format('m') != $objDataVencimento1->format('m')) {
            $objDataVencimento = $objDataVencimento->sub(
                new \DateInterval('P' . $objDataVencimento->format('d') . 'D')
            );
        }

        return $objDataVencimento;
    }

    /**
     * @param $alunoId
     * @param $pesId
     * @return mixed
     */
    public function getArrayFinanceiroTotais($alunoId, $pesId = false)
    {
        $arrCondicoes = [
            "t.titulo_estado IN('" . self::TITULO_ESTADO_PAGO . "','" . self::TITULO_ESTADO_ABERTO . "')",
        ];

        if ($pesId) {
            $arrCondicoes[] = "(
                t.pes_id IN(" . $pesId . ") OR
                t.alunocurso_id IN(select alunocurso_id from acadgeral__aluno_curso INNER JOIN acadgeral__aluno USING(aluno_id) WHERE pes_id IN(" . $pesId . ") OR aluno_id IN(" . $alunoId . ")) OR
		        t.alunoper_id IN(select alunoper_id from acadperiodo__aluno INNER JOIN acadgeral__aluno_curso USING(alunocurso_id) INNER JOIN acadgeral__aluno USING(aluno_id) WHERE pes_id IN(" . $pesId . ") OR aluno_id IN(" . $alunoId . "))
            )";
        } else {
            $arrCondicoes[] = "(
                t.alunocurso_id IN(select alunocurso_id FROM acadgeral__aluno_curso WHERE aluno_id IN(" . $alunoId . ")) OR
                t.alunoper_id IN(select alunoper_id FROM acadperiodo__aluno INNER JOIN acadgeral__aluno_curso USING(alunocurso_id) WHERE aluno_id IN(" . $alunoId . "))
            )";
        }

        $query = $this->retornaConsultaSQLTitulosComInformacoesExtras($arrCondicoes);

        return $this->executeQuery($query)->fetchAll();
    }

    /**
     * @param $param
     * @return bool
     */
    public function criarNovosTitulos($param, $aceitarDataRetroativa = true, $validarParcelaEDescontoMaximo = true)
    {
        $serviceAcesso                 = new \Acesso\Service\AcessoPessoas($this->getEm());
        $servicePessoa                 = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceFinanceiroTituloTipo   = new \Financeiro\Service\FinanceiroTituloTipo($this->getEm());
        $serviceFinanceiroValores      = new \Financeiro\Service\FinanceiroValores($this->getEm());
        $serviceAcadgeralSituacao      = new \Matricula\Service\AcadgeralSituacao($this->getEm());
        $serviceAcadCurso              = new \Matricula\Service\AcadCurso($this->getEm());
        $serviceAcadgeralAlunoCurso    = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
        $serviceAcadperiodoAluno       = new \Matricula\Service\AcadperiodoAluno($this->getEm());
        $serviceFinanceiroTituloConfig = new \Financeiro\Service\FinanceiroTituloConfig($this->getEm());
        $serviceBoleto                 = new \Financeiro\Service\Boleto($this->getEm());
        $arrObjTitulos                 = array();

        if (!$param['pes']) {
            $this->setLastError('É necessário selecionar a pessoa!');

            return false;
        }

        if (!$param['tipotitulo']) {
            $this->setLastError('É necessário selecionar o tipo de título!');

            return false;
        }

        $pes        = $param['pes'];
        $alunocurso = $param['alunocurso'] ? $param['alunocurso'] : null;
        $alunoper   =
            ($param['arrPeriodoLetivo']['alunoper_id'] ? $param['arrPeriodoLetivo']['alunoper_id'] : ($param['alunoper_id'] ? $param['alunoper_id'] : null));

        $tipotitulo           = $param['tipotitulo'];
        $tituloDataVencimento = self::formatDateAmericano($param['tituloDataVencimento']);
        $tituloNumeroParcelas = (int)$param['tituloNumeroParcelas'];
        $tituloValorParcelas  = (float)$param['tituloValorParcelas'];
        $tituloValorTotal     = ($tituloNumeroParcelas * $tituloValorParcelas);
        $tituloObservacoes    = $param['tituloObservacoes'];

        if (!$alunoper && $param['arrPeriodoLetivo']['alunoper_id']) {
            $alunoper = $param['arrPeriodoLetivo']['alunoper_id'];
        }

        $objAlunoCurso             = null;
        $cursoPossuiPeriodo        = false;
        $alunoSituacaoRegularidade = false;

        /** @var $objFinanceiroTituloTipo \Financeiro\Entity\FinanceiroTituloTipo */
        $objFinanceiroTituloTipo = $serviceFinanceiroTituloTipo->getRepository()->find($tipotitulo);

        if (!$objFinanceiroTituloTipo) {
            $this->setLastError('É necessário selecionar o tipo de título!');

            return false;
        }

        $tituloGrupo = $objFinanceiroTituloTipo->getTipotituloGrupo();

        /** @var $objAlunoCurso \Matricula\Entity\AcadgeralAlunoCurso */
        $objAlunoCurso = $serviceAcadgeralAlunoCurso->getRepository()->findOneBy(['alunocursoId' => $alunocurso]);

        if (!$objAlunoCurso && $tituloGrupo == $serviceFinanceiroTituloTipo::TIPOTITULO_GRUPO_DOCUMENTOS) {
            $this->setLastError('Esse tipo de título requer aluno!');

            return false;
        }

        if ($objAlunoCurso) {
            $cursoPossuiPeriodo = (
                $objAlunoCurso
                    ->getCursocampus()
                    ->getCurso()
                    ->getCursoPossuiPeriodoLetivo() != $serviceAcadCurso::CURSO_POSSUI_PERIODO_LETIVO_NAO
            );

            $alunoSituacaoRegularidade = in_array(
                $objAlunoCurso->getAlunocursoSituacao(),
                $serviceAcadgeralAlunoCurso->getAlunocursoSituacaoAtividade()
            );
        }

        $objAlunoPer = null;

        if ($alunoper) {
            /** @var $objAlunoPer \Matricula\Entity\AcadperiodoAluno */
            $objAlunoPer = $serviceAcadperiodoAluno->getRepository()->find($alunoper);

            $alunoSituacaoRegularidade = (
                $objAlunoPer->getMatsituacao()->getMatsitRegular() == $serviceAcadgeralSituacao::SITUACAO_REGULAR_SIM
            );
            $situacao                  = $objAlunoPer->getMatsituacao()->getSituacaoId();
            $arrSituacoesDesligamento  = $serviceAcadgeralSituacao->situacoesDesligamento();

            if (in_array($situacao, $arrSituacoesDesligamento)) {
                $this->setLastError('Aluno desligado!');

                return false;
            }
        }

        if ($tituloGrupo == $serviceFinanceiroTituloTipo::TIPOTITULO_GRUPO_ACADEMICO) {
            if (!$alunoSituacaoRegularidade) {
                $this->setLastError('Título do grupo Acadêmico, é necessário que o aluno esteja situação regular!');

                return false;
            } else {
                if ($cursoPossuiPeriodo && !$alunoper) {
                    $this->setLastError(
                        'Título do grupo Acadêmico, é necessário que o aluno esteja em um periodo letivo!'
                    );

                    return false;
                }
            }
        }

        if ($validarParcelaEDescontoMaximo) {
            $valoresId = $param['valores'];

            if (!$valoresId) {
                $this->setLastError('Por favor informe o uma configuração válida!');

                return false;
            }

            /** @var $objValores \Financeiro\Entity\FinanceiroValores */
            $objValores = $serviceFinanceiroValores->getRepository()->find($valoresId);

            if (!$objValores) {
                $this->setLastError('Por favor informe o uma configuração válida!');

                return false;
            }

            $tituloValorTotalConfiguracao = ($objValores->getValoresParcela() * $objValores->getValoresPreco());

            $percentualDesconto = (
                (($tituloValorTotalConfiguracao - $tituloValorTotal) * 100) / ($tituloValorTotalConfiguracao)
            );

            $maxParcelas           = $this->getConfig()->localizarChave('FINANCEIRO_MAX_PARCELAS');
            $maxPercentualDesconto = $this->getConfig()->localizarChave('FINANCEIRO_MAX_PERCENTUAL_DESCONTO');
            $grupoSupervisao       = $this->getConfig()->localizarChave('GRUPO_FINANCEIRO_SUPERVISAO');

            $valorLegado = $serviceFinanceiroValores->retornaTituloLegado($objValores->getValoresId());

            if (
                $tituloNumeroParcelas > $maxParcelas || $percentualDesconto > $maxPercentualDesconto || $valorLegado
            ) {
                $usuarioSupervisor = $serviceAcesso->validaUsuarioEVerificaSeEstaNoGrupo(
                    $param['login'],
                    $param['senha'],
                    $grupoSupervisao
                );

                if (!$usuarioSupervisor) {
                    $this->setLastError($serviceAcesso->getLastError());

                    return false;
                }
            }

            if ($valorLegado) {
                $tituloObservacoes .= " - Título de valor legado, autorizado por: " . $param['login'];
            }
        }

        $objTituloConfig = null;

        if ($param['tituloConfig']) {
            /** @var $objTituloConfig \Financeiro\Entity\FinanceiroTituloConfig */
            $objTituloConfig = $serviceFinanceiroTituloConfig->getRepository()->find($param['tituloConfig']);

            if (!$objTituloConfig) {
                $this->setLastError('Configuração de título não encontrada!');

                return false;
            }
        }

        /** @var $objPessoa \Pessoa\Entity\Pessoa */
        $objPessoa = $servicePessoa->getRepository()->find($pes);

        if (!$objPessoa) {
            $this->setLastError('Por favor informe o responsável pelo título!');

            return false;
        }

        /** @var $objFinanceiroTituloTipo \Financeiro\Entity\FinanceiroTituloTipo */
        $objFinanceiroTituloTipo = $serviceFinanceiroTituloTipo->getRepository()->find($tipotitulo);

        if (!$objFinanceiroTituloTipo) {
            $this->setLastError('Por favor informe um tipo de título válido!');

            return false;
        }

        if (!$tituloDataVencimento) {
            $this->setLastError('por favor informe uma data de vencimento válida.');

            return false;
        }

        if ($tituloNumeroParcelas <= 0) {
            $this->setLastError('Selecione um número de parcelas válido!');

            return false;
        }

        if ($tituloValorParcelas <= 0) {
            $this->setLastError('Selecione um valor de parcelas válido!');

            return false;
        }

        $dataAtualFormatada = date('Y-m-d H:i:s');

        $ativado = false;

        if ($objAlunoPer && $ativado) {
            $serviceModalidade = new \Matricula\Service\AcadModalidade($this->getEm());
            $cursoModalidade   = $objAlunoCurso->getCursocampus()->getCurso()->getMod()->getModNome();
            $turmaSerie        = $objAlunoPer->getTurma()->getTurmaSerie();

            if ($turmaSerie != 1 && $cursoModalidade == $serviceModalidade::MODALIDADE_DESCRICAO_EAD) {
                $objTituloTipoMatricula = $serviceFinanceiroTituloTipo->getRepository()->find(
                    $serviceFinanceiroTituloTipo::TIPOTITULO_MATRICULA
                );

                if (!$objTituloTipoMatricula) {
                    $this->setLastError("Tipo de título 'Matrícula' não cadastrado no sistema!");

                    return false;
                }

                $dataVencimento = $this->retornaDataVencimento($tituloDataVencimento, 0, $aceitarDataRetroativa);

                $arrParamDescricao = [
                    'tipotituloNome'       => $objTituloTipoMatricula->getTipotituloNome(),
                    'parcela'              => 1,
                    'totalParcelas'        => 1,
                    'tituloDataVencimento' => $dataVencimento->format("d/m/Y"),
                ];

                $arrTitulo = [
                    'pes'                     => $objPessoa,
                    'tipotitulo'              => $objTituloTipoMatricula,
                    'tituloDescricao'         => $this->formatarDescricaoPadraoTitulo($arrParamDescricao),
                    'alunocurso'              => $alunocurso,
                    'tituloParcela'           => 1,
                    'tituloDataProcessamento' => $dataAtualFormatada,
                    'tituloDataVencimento'    => $dataVencimento,
                    'tituloValor'             => $tituloValorParcelas,
                    'tituloObservacoes'       => $tituloObservacoes,
                    'tituloEstado'            => self::TITULO_ESTADO_ABERTO,
                    'alunoper'                => $alunoper,
                    'tituloGrupo'             => $tituloGrupo,
                    'tituloconf'              => $objTituloConfig ? $objTituloConfig : '',
                    'retornaObjTitulo'        => $param['cartao'] ? true : false
                ];

                if (!$this->save($arrTitulo)) {
                    return false;
                }

                /** @var \Financeiro\Entity\FinanceiroTitulo $objTitulo */
                $objTitulo = $this->getRepository()->find($arrTitulo['tituloId']);

                if (!$serviceBoleto->registrarBoletoParaTitulo($objTitulo)) {
                    $this->setLastError($serviceBoleto->getLastError());

                    return false;
                }

                $arrObjTitulos[] = $objTitulo;

                $tituloDataVencimento = $this->incrementarMesNoVencimentoDoTitulo($tituloDataVencimento);
            }
        }

        try {
            $this->getEm()->beginTransaction();
            for ($i = 0; $i < $tituloNumeroParcelas; $i++) {
                $dataVencimento    = $this->retornaDataVencimento($tituloDataVencimento, $i, $aceitarDataRetroativa);
                $arrParamDescricao = [
                    'tipotituloNome'       => $objFinanceiroTituloTipo->getTipotituloNome(),
                    'parcela'              => ($i + 1),
                    'totalParcelas'        => $tituloNumeroParcelas,
                    'tituloDataVencimento' => $dataVencimento->format("d/m/Y"),
                ];

                $arrTitulo = [
                    'pes'                     => $objPessoa,
                    'tipotitulo'              => $objFinanceiroTituloTipo,
                    'tituloDescricao'         => $this->formatarDescricaoPadraoTitulo($arrParamDescricao),
                    'alunocurso'              => $alunocurso,
                    'tituloParcela'           => ($i + 1),
                    'tituloDataProcessamento' => $dataAtualFormatada,
                    'tituloDataVencimento'    => $dataVencimento,
                    'tituloValor'             => $tituloValorParcelas,
                    'tituloObservacoes'       => $tituloObservacoes,
                    'tituloEstado'            => self::TITULO_ESTADO_ABERTO,
                    'alunoper'                => $alunoper,
                    'tituloGrupo'             => $tituloGrupo,
                    'tituloconf'              => $objTituloConfig ? $objTituloConfig : '',
                    'retornaObjTitulo'        => $param['cartao'] ? true : false
                ];

                if (!$this->save($arrTitulo)) {

                    return false;
                }

                /** @var \Financeiro\Entity\FinanceiroTitulo $objTitulo */
                $objTitulo = $this->getRepository()->find($arrTitulo['tituloId']);

                if (!$serviceBoleto->registrarBoletoParaTitulo($objTitulo)) {
                    $this->setLastError($serviceBoleto->getLastError());

                    return false;
                }

                $arrObjTitulos[] = $objTitulo;
            }

            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao gerar título(s).<br>' . $ex->getMessage());

            return false;
        }

        return $arrObjTitulos;
    }

    public function formatarDescricaoPadraoTitulo($arrVariaveis)
    {
        $tituloDescricaoPadrao = $this->getConfig()->localizarChave('FINANCEIRO_TITULO_DESCRICAO_FORMATO');

        if (!$tituloDescricaoPadrao) {
            $tituloDescricaoPadrao = '__TIPO__ - __PARCELA__ / __TOTAL_PARCELAS__';
        }

        $arrVariaveisSubstituicao = array(
            '__TIPO__'                    => ($arrVariaveis['tipotituloNome'] ? $arrVariaveis['tipotituloNome'] : ''),
            '__PARCELA__'                 => ($arrVariaveis['parcela'] ? $arrVariaveis['parcela'] : ''),
            '__TOTAL_PARCELAS__'          => ($arrVariaveis['totalParcelas'] ? $arrVariaveis['totalParcelas'] : ''),
            '__DATA_PAGAMENTO__'          => ($arrVariaveis['tituloDataVencimento']
                ? $arrVariaveis['tituloDataVencimento'] : ''),
            '__DATA_PAGAMENTO_DIA__'      => '',
            '__DATA_PAGAMENTO_MES__'      => '',
            '__DATA_PAGAMENTO_MES_NOME__' => '',
            '__DATA_PAGAMENTO_ANO__'      => '',
        );

        if ($arrVariaveisSubstituicao['__DATA_PAGAMENTO__']) {
            $arrData = explode('/', $this->formatDateBrasileiro($arrVariaveisSubstituicao['__DATA_PAGAMENTO__']));

            $arrVariaveisSubstituicao['__DATA_PAGAMENTO_DIA__']      = $arrData[0];
            $arrVariaveisSubstituicao['__DATA_PAGAMENTO_MES__']      = $arrData[1];
            $arrVariaveisSubstituicao['__DATA_PAGAMENTO_ANO__']      = $arrData[2];
            $arrVariaveisSubstituicao['__DATA_PAGAMENTO_MES_NOME__'] = $this->mesReferencia($arrData[1]);
        }

        $tituloDescricao = str_replace(
            array_keys($arrVariaveisSubstituicao),
            array_values($arrVariaveisSubstituicao),
            $tituloDescricaoPadrao
        );

        $tituloDescricao = preg_replace('/( [-_\/]){2,}/im', ' \1', $tituloDescricao);
        $tituloDescricao = preg_replace('/\s+/i', ' ', $tituloDescricao);

        return $tituloDescricao;
    }

    /**
     * Efetua cancelamento de um título
     * @param array $arrTitulos
     * @return bool
     * @throws \Exception
     */
    public function cancelaTitulo(array $arrTitulos, $observacao = '', $atualizar = false, $validaObservacao = false)
    {
        $serviceAcessoPessoas            = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceFinanceiroDescontoTitulo = new \Financeiro\Service\FinanceiroDescontoTitulo($this->getEm());
        $serviceBoleto                   = new \Financeiro\Service\Boleto($this->getEm());

        if (!$arrTitulos) {
            $this->setLastError('Por favor informe ao menos um título para cancelar.');

            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            $usuarioBaixa = $serviceAcessoPessoas->retornaUsuarioLogado();

            if ($this->tituloUsaMaxiPago($arrTitulos)) {
                return false;
            }

            foreach ($arrTitulos as $tituloId) {
                /** @var \Financeiro\Entity\FinanceiroTitulo $objTitulo */
                $objTitulo = $this->getRepository()->find($tituloId);

                if (!$objTitulo) {
                    $this->setLastError($tituloId . ' título inválido.');

                    return false;
                }

                $objTitulo->setUsuarioBaixa($usuarioBaixa);
                $objTitulo->setTituloDataBaixa(new \DateTime('now'));

                if (!empty($observacao)) {
                    $strObs = trim($objTitulo->getTituloObservacoes());
                    $strObs .= "\n" . trim($observacao);

                    $objTitulo->setTituloObservacoes(trim($strObs));
                } elseif ($validaObservacao) {
                    $this->setLastError('É necessário informar a observação de cancelamento do título!');

                    return false;
                }

                if ($atualizar) {
                    $objTitulo->setTituloEstado(self::TITULO_ESTADO_ALTERACAO);
                } else {
                    $objTitulo->setTituloEstado(self::TITULO_ESTADO_CANCELADO);

                    if (!$serviceFinanceiroDescontoTitulo->cancelaDescontosTitulo($objTitulo)) {
                        $this->setLastError($serviceFinanceiroDescontoTitulo->getLastError());

                        return false;
                    }
                }

                if (!$serviceBoleto->cancelaBoletosTitulo($objTitulo)) {
                    $this->setLastError($serviceBoleto->getLastError());

                    return false;
                }

                $this->getEm()->persist($objTitulo);
                $this->getEm()->flush($objTitulo);
            }

            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao cancelar título(s).<br>' . $ex->getMessage());

            return false;
        }

        return true;
    }

    /**
     * @param array                                        $arrTitulos
     * @param                                              $via
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     * @param string                                       $urlBoleto
     * @param string                                       $urlPagseguro
     * @param bool                                         $enviarEmailComCopia
     * @return bool
     */
    public function enviarTitulos(
        array $arrTitulos,
        $via,
        \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator,
        $arrOutrasInformacoes = array()
    ) {
        $urlBoleto            = $arrOutrasInformacoes['urlBoleto'] ? $arrOutrasInformacoes['urlBoleto'] : false;
        $urlPagseguro         = $arrOutrasInformacoes['urlPagseguro'] ? $arrOutrasInformacoes['urlPagseguro'] : false;
        $enviarEmailComCopia  = $arrOutrasInformacoes['enviarEmailComCopia']
            ? $arrOutrasInformacoes['enviarEmailComCopia'] : false;
        $boletoRemessaAtivada = $arrOutrasInformacoes['boletoRemessaAtivada']
            ? $arrOutrasInformacoes['boletoRemessaAtivada'] : false;

        if (is_string($via)) {
            $via = explode(',', $via);
        }

        if (!$arrTitulos) {
            $this->setLastError('Por favor informe ao menos um título para ennviar.');

            return false;
        }

        if (!$via) {
            $this->setLastError('Por favor informe ao menos uma forma de envio.');

            return false;
        }

        $serviceBoleto                = new \Financeiro\Service\Boleto($this->getEm());
        $serviceFinanceiroRemessaLote = new \Financeiro\Service\FinanceiroRemessaLote($this->getEm());
        $servicePessoa                = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceOrgComunicacaoModelo  = new \Organizacao\Service\OrgComunicacaoModelo($this->getEm());

        try {
            $this->getEm()->beginTransaction();
            $arrInformacaoTitulo = array();

            foreach ($arrTitulos as $tituloId) {
                /** @var \Financeiro\Entity\FinanceiroTitulo $objTitulo */
                $objTitulo = $this->getRepository()->find($tituloId);

                if (!$objTitulo) {
                    $this->setLastError($tituloId . ' título inválido.');

                    return false;
                }

                $pesId = $objTitulo->getPes()->getPesId();

                $objBoleto = $serviceBoleto->pesquisaBoletoPorTitulo($objTitulo);

                $linhaDigitavel = '';
                $linkDoBoleto   = '';

                $objConfconta = $objTitulo->getTituloconf()->getConfcont();
                $objSacado    = $objTitulo->getPes();
                $objCedente   = $objConfconta->getPes();

                if ($objBoleto && $urlBoleto) {
                    $estaNaRemessa = false;

                    if ($boletoRemessaAtivada) {
                        $estaNaRemessa = $serviceFinanceiroRemessaLote
                            ->getRepository()
                            ->findOneBy(['bol' => $objBoleto]);
                    }

                    if (!$boletoRemessaAtivada || ($boletoRemessaAtivada && $estaNaRemessa)) {
                        $linhaDigitavel = $serviceBoleto->retornaLinhaDigitavelBoleto($objBoleto, $serviceLocator);
                    }

                    $linkDoBoleto = $urlBoleto . '/' . $objBoleto->getBolId();
                }

                $titulo                   = $objTitulo->toArray();
                $titulo['linhaDigitavel'] = $linhaDigitavel ? $linhaDigitavel : '-';
                $titulo['linkBoleto']     = $linkDoBoleto ? $linkDoBoleto : '-';
                $titulo['linkPagseguro']  = $urlPagseguro ? $urlPagseguro . '/' . $objTitulo->getTituloId() : '';

                $arrInfo = array(
                    'objBoleto'    => $objBoleto,
                    'boleto'       => $objBoleto->toArray(),
                    'objTitulo'    => $objTitulo,
                    'titulo'       => $titulo,
                    'objSacado'    => $objSacado,
                    'sacado'       => $servicePessoa->getArray($objSacado->getPesId()),
                    'objCedente'   => $objCedente,
                    'cedente'      => $servicePessoa->getArray($objCedente->getPesId()),
                    'objConfconta' => $objConfconta,
                    'confconta'    => $objConfconta->toArray(),
                );

                $arrInformacoesEnvio = array(
                    'nome'     => $arrInfo['sacado']['pesNome'],
                    'email'    => $arrInfo['sacado']['conContatoEmail'],
                    'telefone' => $arrInfo['sacado']['conContatoTelefone'],
                    'celular'  => $arrInfo['sacado']['conContatoCelular'],
                );

                $arrInfo = array_merge($arrInfo, $arrInformacoesEnvio);

                if ($enviarEmailComCopia) {
                    $arrInformacoesEnvio['destinatarioCopia'] = $this->getContatoAgenciadorDoAlunoPeloTitulo(
                        $objTitulo
                    );
                }

                if (!$arrInformacaoTitulo[$pesId]) {
                    $arrInformacaoTitulo[$pesId] = array(
                        'objCedente' => $arrInfo['objCedente'],
                        'cedente'    => $arrInfo['cedente'],
                        'objSacado'  => $arrInfo['objSacado'],
                        'sacado'     => $arrInfo['sacado'],
                        'titulos'    => array()
                    );
                    $arrInformacaoTitulo[$pesId] = array_merge($arrInformacaoTitulo[$pesId], $arrInformacoesEnvio);
                }

                $arrInformacaoTitulo[$pesId]['titulos'][$objTitulo->getTituloId()] = $arrInfo;

                if (in_array('sms', $via) && $this->getConfig()->localizarChave('SMS_ATIVO')) {
                    $envio = $serviceOrgComunicacaoModelo->registrarComunicacao(
                        \Organizacao\Service\OrgComunicacaoTipo::TIPO_ENVIO_BOLETO,
                        \Organizacao\Service\OrgComunicacaoModelo::MODELO_TIPO_SMS,
                        $arrInfo
                    );

                    if (!$envio) {
                        throw new \Exception(
                            'Verifique se o(s) responsáveis pelos títulos possuem telefone celular registrado.'
                        );
                    }
                }
            }

            if (in_array('email', $via)) {
                foreach ($arrInformacaoTitulo as $arrInfo) {
                    $envio = $serviceOrgComunicacaoModelo->registrarComunicacao(
                        \Organizacao\Service\OrgComunicacaoTipo::TIPO_ENVIO_BOLETO,
                        \Organizacao\Service\OrgComunicacaoModelo::MODELO_TIPO_EMAIL,
                        $arrInfo
                    );

                    if (!$envio) {
                        throw new \Exception('Verifique se o(s) responsáveis pelos títulos possuem email registrado.');
                    }
                }
            }

            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao enviar título(s).<br>' . $ex->getMessage());
        }

        return true;
    }

    /**
     * Retorna títulos abertos do aluno
     * @param int $alunocursoId
     * @return array
     */
    public function buscaTitulosAbertoEmAtrasoAluno($arrParam)
    {
        $alunocursoId   = $arrParam['alunocursoId'] ? $arrParam['alunocursoId'] : false;
        $pesId          = $arrParam['pesId'] ? $arrParam['pesId'] : false;
        $titulosAtraso  = $arrParam['titulosAtraso'] ? $arrParam['titulosAtraso'] : false;
        $alunoPerId     = $arrParam['alunoPerId'] ? $arrParam['alunoPerId'] : false;
        $tipoTitulo     = $arrParam['tipoTitulo'] ? $arrParam['tipoTitulo'] : false;
        $grupoAcademico = $arrParam['grupoAcademico'] ? $arrParam['grupoAcademico'] : false;

        if (!$alunocursoId) {
            $this->setLastError('Matricula de aluno não informada!');

            return false;
        }

        $arrCondicoes = ["titulo_estado = 'Aberto'"];

        $arrCondicoesOu = [];

        $arrCondicoesOu[] = "alunocurso_id = " . $alunocursoId;

        if ($pesId) {
            $arrCondicoesOu[] = "pes_id=" . $pesId;
        }

        if ($alunoPerId) {
            $arrCondicoesOu[] = "alunoper_id=" . $alunoPerId;
        }

        if ($tipoTitulo) {
            $tipoTitulo     = is_array($tipoTitulo) ? implode(',', array_filter($tipoTitulo)) : $tipoTitulo;
            $arrCondicoes[] = "tipotitulo_id IN (" . $tipoTitulo . ")";
        }

        if ($grupoAcademico) {
            $arrCondicoes[] = "tipotitulo_grupo = '" . \Financeiro\Service\FinanceiroTituloTipo::TIPOTITULO_GRUPO_ACADEMICO . "'";
        }

        $arrCondicoes[] = "(" . implode(" OR ", $arrCondicoesOu) . ")";

        if ($titulosAtraso) {
            $arrCondicoes[] = "greatest(
                  if(
                      titulo_data_pagamento IS NULL,
                      datediff(date(now()), date(titulo_data_vencimento)),
                      datediff(date(titulo_data_pagamento), date(titulo_data_vencimento))
                  ),
                  0
              ) > 0";
        }

        $query = "
        SELECT count(*) AS qtd
        FROM financeiro__titulo
        INNER JOIN financeiro__titulo_tipo USING(tipotitulo_id)
        WHERE " . implode(' AND ', $arrCondicoes);

        $result = $this->executeQueryWithParam($query)->fetch();

        return (int)$result['qtd'];
    }

    /**
     * metodo que cancela todos os titulos do aluno em um periodo letivo que possuem vinculo com financeiro__titulo_mensalidade
     * @param $alunocursoId
     * @param $strObservacao
     * @param $cancelaVencidos
     * @return bool
     */
    public function cancelaTodosTitulosEmAbertoDeUmAlunoCurso(
        $alunocursoId,
        $strObservacao = '',
        $cancelaVencidos = false
    ) {
        $param = [
            'alunocurso'   => $alunocursoId,
            'tituloEstado' => self::TITULO_ESTADO_ABERTO
        ];

        $arrObjTitulos = $this->getRepository()->findBy($param);
        $arrTitulos    = array();

        $dataAtual = new \DateTime();

        /** @var \Financeiro\Entity\FinanceiroTitulo $objTitulo */
        foreach ($arrObjTitulos as $objTitulo) {
            $tituloVencido = $objTitulo->getTituloDataVencimento() < $dataAtual;

            if (!$cancelaVencidos && $tituloVencido) {
                continue;
            }

            $arrTitulos[] = $objTitulo->getTituloId();
        }

        $cancelaTitulos = true;

        if (!empty($arrTitulos)) {
            $cancelaTitulos = $this->cancelaTitulo($arrTitulos, $strObservacao);
        }

        return $cancelaTitulos;
    }

    /**
     * @param $tituloId
     * @return array|bool
     */
    public function atualizarTitulo($tituloId)
    {
        $serviceAcessoPessoas  = new \Acesso\Service\AcessoPessoas($this->getEm());
        $maxPercentualDesconto = $this->getConfig()->localizarChave('FINANCEIRO_MAX_PERCENTUAL_DESCONTO');

        if (!$tituloId['tituloId']) {
            return false;
        }

        if ($this->tituloUsaMaxiPago($tituloId['tituloId'])) {
            return false;
        }

        if (strtotime(date("Y-m-d")) > strtotime(
                self::formatDateAmericano($tituloId['tituloAtualizarDataVencimento'])
            )
        ) {
            $this->setLastError("A data informada é inválida!");

            return false;
        }
        if (((float)$tituloId['tituloAtualizarDescontoManual']) < 0 || ((float)$tituloId['tituloAtualizarAcrescimoManual']) < 0) {
            $this->setLastError("Valor inválido!");

            return false;
        }

        $grupoSupervisao = $this->getConfig()->localizarChave('GRUPO_FINANCEIRO_SUPERVISAO');

        try {
            $this->getEm()->beginTransaction();
            /** @var \Financeiro\Entity\FinanceiroTitulo $objTitulo */
            $objTitulo        = $this->getRepository()->find($tituloId['tituloId']);
            $objUsuarioLogado = $serviceAcessoPessoas->retornaUsuarioLogado();
            $objNovoTitulo    = clone($objTitulo);

            $arrTitulosInformacoes = $this->retornaInformacoesTitulo($tituloId['tituloId']);

            $valorDesconto      = str_replace('.', '', $tituloId['tituloAtualizarDescontoManual']);
            $valorAcrescimo     = str_replace('.', '', $tituloId['tituloAtualizarAcrescimoManual']);
            $valorDesconto      = str_replace(',', '.', $valorDesconto);
            $valorAcrescimo     = str_replace(',', '.', $valorAcrescimo);
            $valorFinal         = round(
                $arrTitulosInformacoes['tituloValorFinal'] - $valorDesconto + $valorAcrescimo,
                2
            );
            $dateVencimento     = self::formatDateAmericano($tituloId['tituloAtualizarDataVencimento']);
            $percentualDesconto = (($valorDesconto * 100) / $arrTitulosInformacoes['tituloValorFinal']);

            if ($percentualDesconto > $maxPercentualDesconto) {
                $usuarioSupervisor = $serviceAcessoPessoas->validaUsuarioEVerificaSeEstaNoGrupo(
                    $tituloId['atualizarTituloLogin'],
                    $tituloId['atualizarTituloSenha'],
                    $grupoSupervisao
                );

                if (!$usuarioSupervisor) {
                    $this->setLastError($serviceAcessoPessoas->getLastError());

                    return false;
                }
            }

            if ($arrTitulosInformacoes['tituloValorFinal'] <= $valorDesconto) {
                $this->setLastError("O valor do desconto não pode ser maior ou igual o valor liquido do título!");

                return false;
            }

            $objNovoTitulo
                ->setTituloId(null)
                ->setTituloDataVencimento($dateVencimento)
                ->setTituloDataBaixa(null)
                ->setUsuarioAutor($objUsuarioLogado)
                ->setTituloDesconto(0)
                ->setTituloIncluirRemessa(self::TITULO_INCLUIR_REMESSA_SIM)
                ->setTituloJuros(0)
                ->setTituloMulta(0)
                ->setTituloObservacoes($tituloId['tituloAtualizarObservacao'])
                ->setUsuarioBaixa(null)
                ->setTituloValor($valorFinal)
                ->setTituloValorPago(0)
                ->setTituloValorPagoDinheiro(0)
                ->setTituloPagseguroLink(null)
                ->setTituloDataProcessamento(new \DateTime());

            $this->getEm()->persist($objNovoTitulo);
            $this->getEm()->flush($objNovoTitulo);

            $serviceBoleto = new \Financeiro\Service\Boleto($this->getEm());

            $objTituloAlteracao = new \Financeiro\Entity\FinanceiroTituloAlteracao();
            $objTituloAlteracao
                ->setTituloIdOrigem($objTitulo)
                ->setTituloIdDestino($objNovoTitulo)
                ->setAlteracaoAcao(\Financeiro\Service\FinanceiroTituloAlteracao::ALTERACAO_ACAO_ATUALIZACAO);

            $this->getEm()->persist($objTituloAlteracao);
            $this->getEm()->flush($objTituloAlteracao);

            if (!$serviceBoleto->registrarBoletoParaTitulo($objNovoTitulo)) {
                $this->setLastError($serviceBoleto->getLastError());

                return false;
            }

            $objTitulo
                ->setTituloMulta($arrTitulosInformacoes['tituloMultaCalc'])
                ->setTituloJuros($arrTitulosInformacoes['tituloJurosCalc'])
                ->setTituloAcrescimoManual($valorAcrescimo)
                ->setTituloDescontoManual($valorDesconto);

            $this->getEm()->persist($objTitulo);
            $this->getEm()->flush($objTitulo);

            if (!$this->cancelaTitulo([$objTitulo], null, true)) {
                return false;
            }

            $this->getEm()->commit();

            $novoTituloId = $objNovoTitulo->getTituloId();
        } catch (\Exception $ex) {
            $this->setLastError("Falha ao atualizar título!" . $ex->getMessage());

            return false;
        }

        return array('tituloId' => $novoTituloId);
    }

    /**
     * @param $arrDados
     * @return bool
     * @throws \Exception
     */
    public function estornarTitulo($arrDados)
    {
        $this->getConfig();
        $serviceTituloAlteracao  = new \Financeiro\Service\FinanceiroTituloAlteracao($this->getEm());
        $serviceDescontoTitulo   = new \Financeiro\Service\FinanceiroDescontoTitulo($this->getEm());
        $serviceTituloCheque     = new \Financeiro\Service\FinanceiroTituloCheque($this->getEm());
        $serviceTituloTipo       = new \Financeiro\Service\FinanceiroTituloTipo($this->getEm());
        $serviceAcesso           = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceFinanceiroCartao = new \Financeiro\Service\FinanceiroCartao($this->getEm(), $this->getConfig());
        $serviceFilaMaxiPago     = new \Sistema\Service\SisFilaMaxipago($this->getEm());

        if (empty($arrDados)) {
            $this->setLastError('É necessário selecionar ou informar um título para estorno!');

            return false;
        }

        if (!$arrDados['observacao']) {
            $this->setLastError('É necessário informar uma observação para o estorno!');

            return false;
        }

        $arrNovosTitulos = [];
        $arrTitulos[]    = $arrDados['tituloId'];
        $usaMaxiPago     = $this->tituloUsaMaxiPago($arrDados['tituloId'], false);

        if ($usaMaxiPago) {
            $arrTitulos = $serviceFinanceiroCartao->retornaTitulosVinculadosCartao($arrDados['tituloId']);
        }

        $this->getEm()->beginTransaction();

        foreach ($arrTitulos as $tituloId) {

            /** @var \Financeiro\Entity\FinanceiroTitulo $objTitulo */
            $objTitulo = $this->getRepository()->find($tituloId);

            if ($objTitulo->getTituloEstado() == self::TITULO_ESTADO_ESTORNO) {
                $this->setLastError('O titulo já se encontra em situação de estorno!');

                return false;
            }

            $pagamentoMultiplo = $serviceTituloTipo->getTipoPagamentoMultiplo()->getTipotituloId();

            if ($objTitulo) {
                $arrTitulosRelacionados = array($objTitulo);

                if ($objTitulo->getTipotitulo()->getTipotituloId() == $pagamentoMultiplo) {
                    $arrTitulosRelacionados = $serviceTituloAlteracao->retornaTitulosvinculadosAoTitulo(
                        null,
                        $objTitulo
                    );

                    if ($arrTitulosRelacionados['origem']) {
                        $arrTitulosRelacionados = $arrTitulosRelacionados['origem'];
                    } else {
                        $this->setLastError('O titulo não possui registros relacionados!');

                        return false;
                    }
                }

                try {

                    /** @var \Acesso\Entity\AcessoPessoas $objAcessoPessoas */
                    $objAcessoPessoas = $serviceAcesso->retornaUsuarioLogado();
                    $objTitulo
                        ->setTituloEstado(self::TITULO_ESTADO_ESTORNO)
                        ->setTituloObservacoes($arrDados['observacao']);

                    if (!$serviceTituloCheque->inativarChequesVinculadosAoTitulo($objTitulo->getTituloId())) {
                        $this->setLastError('Ocorreu um erro ao inativar cheques vinculados ao título!');

                        return false;
                    }

                    $this->getEm()->persist($objTitulo);
                    $this->getEm()->flush($objTitulo);

                    /** @var \Financeiro\Entity\FinanceiroTitulo $objTituloEstorno */
                    foreach ($arrTitulosRelacionados as $objTituloEstorno) {
                        $objNovoTitulo = clone($objTituloEstorno);

                        $objNovoTitulo
                            ->setTituloId(null)
                            ->setTituloEstado(self::TITULO_ESTADO_ABERTO)
                            ->setTituloDataBaixa(null)
                            ->setTituloDataPagamento(null)
                            ->setUsuarioBaixa(null)
                            ->setTituloIncluirRemessa(self::TITULO_INCLUIR_REMESSA_SIM)
                            ->setUsuarioAutor($objAcessoPessoas)
                            ->setTituloAcrescimoManual(null)
                            ->setTituloPagseguroLink(null)
                            ->setTituloDataProcessamento(new \DateTime())
                            ->setTituloDescontoManual(null);

                        $this->getEm()->persist($objNovoTitulo);
                        $this->getEm()->flush($objNovoTitulo);

                        $objTituloEstorno->setTituloIncluirRemessa(self::TITULO_INCLUIR_REMESSA_NAO);

                        if ($objTitulo->getTipotitulo()->getTipotituloId() == $pagamentoMultiplo) {
                            $objTituloEstorno->setTituloEstado(self::TITULO_ESTADO_PAGAMENTO_MULTIPLO_EXTORNO);
                        }

                        $this->getEm()->persist($objTituloEstorno);
                        $this->getEm()->flush($objTituloEstorno);

                        if ($objTitulo->getTipoTitulo()->getTipoTituloId() == $serviceTituloTipo::MULTA_BIBLIOTECA) {
                            $serviceBibliotecaMulta = new \Biblioteca\Service\Multa($this->getEm());
                            $serviceBibliotecaMulta->atualizaTituloMulta($objTitulo, $objNovoTitulo);
                        }

                        if (!$serviceDescontoTitulo->copiarDescontoDeTitulo($objNovoTitulo, $tituloId)) {
                            $this->setLastError("Não foi possível vincular os descontos ao novo título");

                            return false;
                        }

                        if (!$serviceTituloAlteracao->vincularTitulos(
                            $objTitulo,
                            $objNovoTitulo,
                            $serviceTituloAlteracao::ALTERACAO_ACAO_ESTORNO
                        )
                        ) {
                            $this->setLastError($serviceTituloAlteracao->getLastError());

                            return false;
                        }

                        $serviceBoleto = new \Financeiro\Service\Boleto($this->getEm());

                        if (!$serviceBoleto->registrarBoletoParaTitulo($objNovoTitulo)) {
                            $this->setLastError($serviceBoleto->getLastError());

                            return false;
                        }

                        $arrNovosTitulos[] = $objNovoTitulo->getTituloId();
                    }
                } catch (\Exception $ex) {
                    $this->setLastError("Falha ao estornar título! " . $ex->getMessage());

                    return false;
                }
            }

            if ($usaMaxiPago) {

                /** @var $objFilaMaxiPago \Sistema\Entity\SisFilaMaxipago */
                $objFilaMaxiPago = $serviceFilaMaxiPago->getRepository()->findOneBy(
                    [
                        'titulo'            => $objTitulo,
                        'filaSituacao'      => $serviceFilaMaxiPago::FILA_SITUACAO_ENVIADO,
                        'filaRecorrenciaId' => null
                    ]
                );

                $objFilaMaxiPago->setFilaSituacao($serviceFilaMaxiPago::FILA_ESTORNO);

                $this->getEm()->persist($objFilaMaxiPago);
                $this->getEm()->flush($objFilaMaxiPago);
            }
        }

        /*Esse trecho foi adicionado no final, porque o doctrine não consegue interferir na ação da api, caso haja algum erro*/
        if ($this->tituloUsaMaxiPago($objTitulo)) {
            if (!$serviceFinanceiroCartao->estornaPagamentoCartao($objTitulo)) {
                $this->setLastError('</br>' . $serviceFinanceiroCartao->getLastError());

                return false;
            }
        }

        $this->getEm()->commit();

        return array('tituloId' => $arrNovosTitulos);
    }

    /*
 * geraRelatorioFinanceiroPeriodo
 * $dataInicial and $dataFinal: Intervalo de data para geracao do relatório.
 * $tipoPagamento: Array contento os tipo de pagamento que deseja filtrar.
 * $servicos: Array contento os serviços que deseja filtra. Ex: Mensalidade, Multa de Biblioteca.
 * $estado: Estado dos titulos que deseja-se filtrar. Ex: Pagos, Abertos, Cancelados.
 * */
    public function geraRelatorioFinanceiroPeriodo($dados)
    {
        $condicoes = '';

        if ($dados['campusCurso']) {
            $condicoes .= " AND campus_curso.cursocampus_id " . $dados['campusCurso-filtro'] . " (" . $dados['campusCurso'] . ")";
        }

        if ($dados['tipoTitulo']) {
            $condicoes .= " AND financeiro__titulo_tipo.tipotitulo_id " . $dados['tipoTitulo-filtro'] . " (" . $dados['tipoTitulo'] . ")";
        }

        if ($dados['pagamentoUsuario']) {
            $condicoes .= " AND financeiro__titulo.usuario_baixa " . $dados['pagamentoUsuario-filtro'] . " (" . $dados['pagamentoUsuario'] . ")";
        }

        if ($dados['meioPagamento']) {
            $condicoes .= " AND financeiro__meio_pagamento.meio_pagamento_id " . $dados['meioPagamento-filtro'] . " (" . $dados['meioPagamento'] . ")";
        }

        if ($dados['dataInicial']) {
            $data = $this->formatDateAmericano($dados['dataInicial']);
            $condicoes .= " AND DATE(financeiro__titulo.titulo_data_pagamento) >= '" . $data . "'";
        }

        if ($dados['dataFinal']) {
            $data = $this->formatDateAmericano($dados['dataFinal']);
            $condicoes .= " AND DATE(financeiro__titulo.titulo_data_pagamento) <= '" . $data . "'";
        }

        $query = "
        SELECT
             @TituloDescontosPreFixados:=(COALESCE (desconto,0)) titulo_desconto_pre_fixados,

            count(financeiro__titulo.titulo_id) AS titulos_quantidade,
            pes_nome,
            (
                TRIM(LEADING 0 FROM acadgeral__aluno_curso.alunocurso_id)
            ) alunocurso_id,
            (
                TRIM(LEADING 0 FROM financeiro__titulo.titulo_id)
            ) titulo_id,
            
            financeiro__titulo.titulo_descricao,
            acad_curso.curso_nome,
            acad_curso.curso_id,
            financeiro__meio_pagamento.meio_pagamento_id,
            financeiro__meio_pagamento.meio_pagamento_descricao,
            titulo_estado,
            tipotitulo_nome,
            titulo_tipo_pagamento,
            pagamento_id,
            (
                SELECT login
                FROM acesso_pessoas
                WHERE usuario = financeiro__titulo.usuario_baixa
                GROUP BY usuario
            )usuario_baixa,
            (
                DATE_FORMAT(financeiro__titulo.titulo_data_vencimento, '%d/%m/%Y')
            ) titulo_data_vencimento,
            (
                DATE_FORMAT(titulo_data_pagamento, '%d/%m/%Y %H:%i')
            ) titulo_data_pagamento,
            (
                GREATEST(COALESCE(datediff(date(titulo_data_pagamento), date(titulo_data_vencimento)), 0), 0)
            ) tituloDiasAtraso,
            (
                round((titulo_multa + titulo_juros + titulo_acrescimo_manual), 2)
            ) acrescimo,
            titulo_valor_pago,
            pagamento_valor_final,
            titulo_valor,
            
            (
                coalesce(@TituloDescontosPreFixados,0) +
                coalesce(titulo_desconto,0)+
                coalesce(titulo_desconto_manual,0)
            ) titulo_desconto,
            
            tituloconf_desconto_incentivo_acumulativo,
            desctipo_desconto_acumulativo
            
        FROM financeiro__titulo
            INNER JOIN financeiro__titulo_tipo USING (tipotitulo_id)
            LEFT JOIN pessoa USING (pes_id)
            LEFT JOIN acadgeral__aluno USING (pes_id)
            LEFT JOIN acadgeral__aluno_curso USING (aluno_id)
            LEFT JOIN campus_curso ON campus_curso.cursocampus_id = acadgeral__aluno_curso.cursocampus_id
            LEFT JOIN acad_curso ON acad_curso.curso_id = campus_curso.curso_id
            LEFT JOIN financeiro__pagamento ON financeiro__titulo.titulo_id = financeiro__pagamento.titulo_id
            LEFT JOIN financeiro__meio_pagamento
                ON financeiro__meio_pagamento.meio_pagamento_id = financeiro__pagamento.meio_pagamento_id
            LEFT JOIN financeiro__titulo_config AS tc USING(tituloconf_id)             
            LEFT JOIN
			(
			    SELECT coalesce(sum(desconto_aplicacao_valor), 0) AS desconto, desctipo_desconto_acumulativo, fdt.titulo_id
                FROM view__financeiro_descontos_titulos_atualizada fdt
                WHERE desconto_aplicacao_valor > 0
                GROUP BY titulo_id
                ORDER BY desctipo_desconto_acumulativo ASC
            ) AS total_descontos ON total_descontos.titulo_id=financeiro__titulo.titulo_id
        WHERE  titulo_estado = 'pago'
              AND acad_curso.curso_id IS NOT NULL
              AND titulo_valor_pago IS NOT NULL
              {$condicoes}              
        GROUP BY financeiro__pagamento.titulo_id,financeiro__meio_pagamento.meio_pagamento_id,financeiro__pagamento.pagamento_id
        ORDER BY financeiro__titulo.titulo_data_pagamento,financeiro__titulo.titulo_data_vencimento, financeiro__titulo.titulo_id";

        if ($dados['tipoRelatorio'] == "sintetico") {
            $query = "
            SELECT
                pagamento_id,
                pes_nome,
                alunocurso_id,
                titulo_id,
                titulo_descricao,
                curso_nome,
                curso_id,
                meio_pagamento_id,
                meio_pagamento_descricao,
                titulo_estado,
                tipotitulo_nome,
                titulo_tipo_pagamento,
                titulo_data_vencimento,
                titulo_data_pagamento,
                GREATEST(COALESCE(tituloDiasAtraso,0), 0) AS tituloDiasAtraso,
                sum(acrescimo) AS acrescimo,
                sum(pagamento_valor_final) titulo_valor_pago,
                sum(titulo_valor) titulo_valor,
                sum(titulo_desconto) titulo_desconto
            FROM ({$query}) AS pagamentos
            GROUP BY curso_id, meio_pagamento_id
            ORDER BY curso_nome, meio_pagamento_descricao";
        }

        $arrResultados = $this->executeQuery($query)->fetchAll();

        $arrGeral                          = array(
            'nome'    => 'Totalização Geral',
            'titulos' => [],
            'totais'  => [
                'acrescimo'          => 0,
                'titulo_valor_pago'  => 0,
                'titulo_valor'       => 0,
                'titulo_desconto'    => 0,
                'titulos_quantidade' => 0,
                'titulos'            => 0
            ]
        );
        $arrTituloProcessados              = array();
        $arrTituloPorCurso                 = array();
        $arrTituloPorCursoEMeioDePagamento = array();

        foreach ($arrResultados as $index => $titulo) {
            $cursoId         = $titulo['curso_id'];
            $meioPagamentoId = $titulo['meio_pagamento_id'];

            if (!$cursoId) {
                $cursoId              = '-';
                $titulo['curso_nome'] = 'Indisponível';
            }

            if (!$meioPagamentoId) {
                $titulo['meio_pagamento_descricao'] = 'Indisponível';
            }

            if (!$arrTituloPorCurso[$cursoId]) {
                $arrTituloPorCurso[$cursoId] = array(
                    'nome'    => $titulo['curso_nome'],
                    'titulos' => [],
                    'totais'  => [
                        'acrescimo'         => 0,
                        'titulo_valor_pago' => 0,
                        'titulo_valor'      => 0,
                        'titulo_desconto'   => 0
                    ]
                );
            }

            if (!$arrTituloPorCursoEMeioDePagamento[$cursoId][$meioPagamentoId]) {
                $arrTituloPorCursoEMeioDePagamento[$cursoId][$meioPagamentoId] = array(
                    'nome'    => $titulo['meio_pagamento_descricao'],
                    'titulos' => [],
                    'totais'  => [
                        'acrescimo'         => 0,
                        'titulo_valor_pago' => 0,
                        'titulo_valor'      => 0,
                        'titulo_desconto'   => 0
                    ]
                );
            }

            $arrGeral['titulos'][]                                                      = $titulo;
            $arrTituloPorCurso[$cursoId]['titulos'][]                                   = $titulo;
            $arrTituloPorCursoEMeioDePagamento[$cursoId][$meioPagamentoId]['titulos'][] = $titulo;

            if (
            (!$titulo['pagamento_id'] || $arrTituloProcessados[$titulo['titulo_id']]['pagamento_id'] != $titulo['pagamento_id'])
            ) {
                if (!$arrTituloProcessados[$titulo['titulo_id']]) {
                    $arrGeral['totais']['titulos_quantidade'] += $titulo['titulos_quantidade'];
                    $arrGeral['totais']['titulos'] += 1;
                }

                $arrTituloProcessados[$titulo['titulo_id']]['pagamento_id'] = $titulo['pagamento_id'];

                $arrGeral['totais']['titulo_valor'] += $titulo['pagamento_valor_final']
                    ? $titulo['pagamento_valor_final'] : $titulo['titulo_valor_pago'];

                $arrTituloPorCurso[$cursoId]['totais']['titulo_valor'] += $titulo['pagamento_valor_final']
                    ? $titulo['pagamento_valor_final'] : $titulo['titulo_valor_pago'];

                $arrTituloPorCursoEMeioDePagamento[$cursoId][$meioPagamentoId]['totais']['titulo_valor'] +=
                    $titulo['pagamento_valor_final'] ? $titulo['pagamento_valor_final'] : $titulo['titulo_valor_pago'];

                $arrGeral['totais']['titulo_valor_pago'] += $titulo['pagamento_valor_final']
                    ? $titulo['pagamento_valor_final'] : $titulo['titulo_valor_pago'];

                $arrTituloPorCurso[$cursoId]['totais']['titulo_valor_pago'] +=
                    $titulo['pagamento_valor_final'] ? $titulo['pagamento_valor_final'] : $titulo['titulo_valor_pago'];

                $arrTituloPorCursoEMeioDePagamento[$cursoId][$meioPagamentoId]['totais']['titulo_valor_pago'] +=
                    $titulo['pagamento_valor_final'] ? $titulo['pagamento_valor_final'] : $titulo['titulo_valor_pago'];
            }
        }

        return array(
            'informacaoGeral'                    => $arrGeral,
            'informacaoPorCurso'                 => $arrTituloPorCurso,
            'informacaoPorCursoEMeioDePagamento' => $arrTituloPorCursoEMeioDePagamento,
        );
    }

    public function alteraSituacaoAlunoPorTituloId(\Financeiro\Entity\FinanceiroTitulo $objTitulo)
    {
        $servicePeriodoAluno = new \Matricula\Service\AcadperiodoAluno($this->getEm());

        if (!$objTitulo) {
            $this->setLastError("Titulo não encontrado!");

            return false;
        }

        $tipotituloId    = $objTitulo->getTipotitulo()->getTipotituloId();
        $primeiraParcela = $objTitulo->getTituloParcela() == 1;
        $arrTipoDeTitulo = $this->getConfig()->localizarChave('FINANCEIRO_TIPOS_TITULO_ALTERACAO_SITUACAO_ACADEMICA');
        $arrTipoDeTitulo = trim($arrTipoDeTitulo);
        $arrTipoDeTitulo = $arrTipoDeTitulo ? explode(',', $arrTipoDeTitulo) : [];

        if ($objTitulo->getAlunocurso() && in_array($tipotituloId, $arrTipoDeTitulo) && $primeiraParcela) {
            if (!$servicePeriodoAluno->alterarSituacaoQuandoMatriculaProvisoria($objTitulo->getAlunocurso())) {
                $this->setLastError($servicePeriodoAluno->getLastError());

                return false;
            }
        }

        return true;
    }

    public function retornaDadosComprovante(&$arrTitulos)
    {
        if (!$arrTitulos) {
            $this->setLastError("Sem dados para gerar comprovante!");

            return false;
        }

        $serviceAcadgeralAlunocurso = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());

        $arrTitulo = array();

        foreach ($arrTitulos as $indice => $dado) {
            $this->incrementaComprovante($dado);
            $arrTitulo[] = $this->retornaInformacoesTitulo($dado);

            $arrTitulo[$indice]['tituloComprovante'] = "COMPROVANTE DE PAGAMENTO";
            $arrTitulo[$indice]['tituloNomeCampo']   = "Código do título";
            $arrTitulo[$indice]['pagamentoMultiplo'] = false;

            if ($arrTitulo[$indice]['tipotituloNome'] == self::TITULO_ESTADO_PAGAMENTO_MULTIPLO) {
                $arrTitulo[$indice]['tituloComprovante'] = "COMPROVANTE DE PAGAMENTO DE MÚLTIPLOS TÍTULOS";
                $arrTitulo[$indice]['tituloNomeCampo']   = "Código da Baixa";
                $arrTitulo[$indice]['pagamentoMultiplo'] = true;
                $arrTitulosAlteracao                     = [];

                foreach ($arrTitulo[$indice]['vinculos']['origem'] as $index => $tituloAlteracao) {
                    $arrTituloAlteracao           = $this->retornaInformacoesTitulo(
                        $tituloAlteracao['titulo_id_origem']
                    );
                    $codigoResponsavelRelacionado = 'P' . $arrTituloAlteracao['pesId'];

                    if ($arrTituloAlteracao['alunocursoId']) {
                        $codigoResponsavelRelacionado = 'A' . $arrTituloAlteracao['alunocursoId'];
                    }

                    $arrTituloAlteracao['somaDescontos'] = (
                        ($arrTituloAlteracao['tituloValor'] + $arrTituloAlteracao['somaAcrescimos']) -
                        $arrTituloAlteracao['tituloValorPago']
                    );

                    $arrTituloAlteracao['alunocursoId']     = ltrim($arrTituloAlteracao['alunocursoId'], '0');
                    $arrTituloAlteracao['tituloValorFinal'] = \Boleto\Service\Moeda::decToMoeda(
                        $arrTituloAlteracao['tituloValorFinal']
                    );
                    $arrTituloAlteracao['tituloValor']      = \Boleto\Service\Moeda::decToMoeda(
                        $arrTituloAlteracao['tituloValor']
                    );
                    $arrTituloAlteracao['tituloValorPago']  = \Boleto\Service\Moeda::decToMoeda(
                        $arrTituloAlteracao['tituloValorPago']
                    );
                    $arrTituloAlteracao['somaDescontos']    = \Boleto\Service\Moeda::decToMoeda(
                        $arrTituloAlteracao['somaDescontos']
                    );
                    $arrTituloAlteracao['somaAcrescimos']   = \Boleto\Service\Moeda::decToMoeda(
                        $arrTituloAlteracao['somaAcrescimos']
                    );

                    $arrTitulosAlteracao[$codigoResponsavelRelacionado][$arrTituloAlteracao['tituloId']] = $arrTituloAlteracao;

                    if (!$arrTitulo[$indice]['alunocurso']) {
                        $arrTitulo[$indice]['alunocursoId'] = $arrTituloAlteracao['alunocursoId'];
                    }

                    if (!$arrTitulo[$indice]['alunoPer']) {
                        $arrTitulo[$indice]['alunoPer'] = $arrTituloAlteracao['alunoPer'];
                    }

                    if (!$arrTitulo[$indice]['cursoNome']) {
                        $arrTitulo[$indice]['cursoNome'] = $arrTituloAlteracao['cursoNome'];
                    }

                    if (!$arrTitulo[$indice]['turmaNome']) {
                        $arrTitulo[$indice]['turmaNome'] = $arrTituloAlteracao['turmaNome'];
                    }

                    if (!$arrTitulo[$indice]['campNome']) {
                        $arrTitulo[$indice]['campNome'] = $arrTituloAlteracao['campNome'];
                    }

                    if (!$arrTitulo[$indice]['alunoNome']) {
                        $arrTitulo[$indice]['alunoNome'] = $arrTituloAlteracao['pesNome'];
                    }

                    if (!$arrTitulo[$indice]['ies']) {
                        $arrTitulo[$indice]['ies'] = $arrTituloAlteracao['iesNome'];
                    }

                    if (!$arrTitulo[$indice]['mantenedora']) {
                        $arrTitulo[$indice]['mantenedora'] = $arrTituloAlteracao['mantenedora'];
                    }
                }

                $arrTitulo[$indice]['titulosAlteracao'] = $arrTitulosAlteracao;
            }

            $arrTitulo[$indice]['somaDescontos'] = (
                ($arrTitulo[$indice]['tituloValor'] + $arrTitulo[$indice]['somaAcrescimos']) - $arrTitulo[$indice]['tituloValorPago']
            );

            $arrTitulo[$indice]['alunocursoId']     = ltrim($arrTitulo[$indice]['alunocursoId'], '0');
            $arrTitulo[$indice]['tituloValorFinal'] = \Boleto\Service\Moeda::decToMoeda(
                $arrTitulo[$indice]['tituloValorFinal']
            );
            $arrTitulo[$indice]['tituloValor']      = \Boleto\Service\Moeda::decToMoeda(
                $arrTitulo[$indice]['tituloValor']
            );
            $arrTitulo[$indice]['tituloValorPago']  = \Boleto\Service\Moeda::decToMoeda(
                $arrTitulo[$indice]['tituloValorPago']
            );
            $arrTitulo[$indice]['somaDescontos']    = \Boleto\Service\Moeda::decToMoeda(
                $arrTitulo[$indice]['somaDescontos']
            );
            $arrTitulo[$indice]['somaAcrescimos']   = \Boleto\Service\Moeda::decToMoeda(
                $arrTitulo[$indice]['somaAcrescimos']
            );
        }

        return $arrTitulo;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroTitulo|int $tituloId
     * @return array
     */
    public function getContatoAgenciadorDoAlunoPeloTitulo($tituloId)
    {
        if (!$tituloId) {
            $this->setLastError("Informe um título válido!");

            return false;
        }

        if (is_object($tituloId)) {
            $objTitulo = $tituloId;
        } else {
            $objTitulo = $this->getRepository()->find($tituloId);
        }

        if (!$objTitulo) {
            $this->setLastError("Não foi possível localizar o título informado!");

            return false;
        }

        $serviceContato = new \Pessoa\Service\Contato($this->getEm());
        $objAgente      = null;

        $arrInfoAgenciador = array();

        if ($objAlunoCurso = $objTitulo->getAlunocurso()) {
            if (!$objAgente = $objAlunoCurso->getPesIdAgente()) {
                $objAgente = $objAlunoCurso->getAluno()->getPesIdAgenciador();
            }
        }

        if (!$objAgente && $objAlunoPer = $objTitulo->getAlunoPer()) {
            if (!$objAgente = $objAlunoPer->getAlunocurso()->getPesIdAgente()) {
                $objAgente = $objAlunoPer->getAlunocurso()->getAluno()->getPesIdAgenciador();
            }
        }

        if (!$objAgente && $objPes = $objTitulo->getPes()) {
            $serviceAcadgeralAluno = new \Matricula\Service\AcadgeralAluno($this->getEm());

            /** @var \Matricula\Entity\AcadgeralAluno $objAluno */
            if ($objAluno = $serviceAcadgeralAluno->getRepository()->findOneBy(['pes' => $objPes->getPesId()])) {
                $objAgente = $objAluno->getPesIdAgenciador();
            }
        }

        /** @var \Organizacao\Entity\OrgAgenteEducacional $objAgente */
        if ($objAgente) {
            $arrInfoAgenciador['nome']     = $objAgente->getPes()->getPesNome();
            $arrInfoAgenciador['email']    = $objAgente->getPes()->getConContatoEmail();
            $arrInfoAgenciador['telefone'] = $objAgente->getPes()->getConContatoTelefone();
            $arrInfoAgenciador['celular']  = $objAgente->getPes()->getConContatoCelular();
        }

        return $arrInfoAgenciador;
    }

    public function buscaTitulosAlunoPeriodo($alunoPer)
    {
        if (!$alunoPer) {
            $this->setLastError("Nenhum aluno informado");

            return false;
        }

        $query = "SELECT * FROM financeiro__titulo WHERE alunoper_id = {$alunoPer}";

        if ($result = $this->executeQuery($query)->fetchAll()) {
            return $result;
        }

        return false;
    }

    public function incrementarMesNoVencimentoDoTitulo($data)
    {
        $data = explode('-', $data);
        $data[1]++;
        $data[1] = str_pad($data[1], 2, 0, STR_PAD_LEFT);

        return implode('-', $data);
    }

    public function incluirTituloNaRemessa($arrTitulo, $incluir = true)
    {
        if (!$arrTitulo) {
            $this->setLastError("Informe pelo menos um título!");

            return false;
        }

        if (!is_array($arrTitulo)) {
            $arrTitulo = [$arrTitulo];
        }

        try {
            if ($incluir) {
                $tituloIncluirRemessa = self::TITULO_INCLUIR_REMESSA_SIM;
            } else {
                $tituloIncluirRemessa = self::TITULO_INCLUIR_REMESSA_NAO;
            }

            $query = '
            UPDATE financeiro__titulo
            SET titulo_incluir_remessa = "' . $tituloIncluirRemessa . '"
            WHERE titulo_id IN(' . implode(',', $arrTitulo) . ')';

            $this->executeQueryWithParam($query);
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return false;
        }

        return true;
    }

    public function retornaTitulosAluno($param)
    {
        $serviceSituacoes = new \Matricula\Service\AcadgeralSituacao($this->getEm());

        $sql = '
        SELECT titulo.*,boleto.*
        FROM financeiro__titulo titulo
        INNER JOIN boleto USING(titulo_id)
        LEFT JOIN pessoa USING(pes_id)
        LEFT JOIN acadgeral__aluno ON pessoa.pes_id=acadgeral__aluno.pes_id
        LEFT JOIN acadgeral__aluno_curso  USING(aluno_id)
        LEFT JOIN  campus_curso USING (cursocampus_id)
        LEFT JOIN org_campus USING(camp_id)
        LEFT JOIN acad_curso USING (curso_id)
        LEFT JOIN acadperiodo__aluno ON acadgeral__aluno_curso.alunocurso_id=acadperiodo__aluno.alunocurso_id
        LEFT JOIN acadperiodo__turma ON acadperiodo__aluno.turma_id = acadperiodo__turma.turma_id
        LEFT JOIN acadperiodo__letivo ON acadperiodo__letivo.per_id = acadperiodo__turma.per_id
        LEFT JOIN acadgeral__situacao ON acadgeral__situacao.situacao_id = acadperiodo__aluno.matsituacao_id
        -- CONDICOES --
        GROUP BY titulo.titulo_id
        ORDER BY pes_nome ASC,acadgeral__aluno_curso.alunocurso_id ASC,titulo.titulo_id';

        $params = [];
        $where  = [];

        if ($param['matriculasNotIn']) {
            $where[]                   = 'acadgeral__aluno_curso.alunocurso_id NOT IN ( :matriculasNotIn )';
            $params['matriculasNotIn'] = $param['matriculasNotIn'];
        }

        if ($param['camp_id']) {
            $where[]          = 'org_campus.camp_id IN ( :campId ) ';
            $params['campId'] = $param['camp_id'];
        }

        if ($param['curso_id']) {
            $where[]           = 'acad_curso.curso_id IN ( :cursoId ) ';
            $params['cursoId'] = $param['curso_id'];
        }

        if ($perId = $param['per_id']) {
            $where[] = "acad_curso.curso_possui_periodo_letivo = 'Sim'";

            if ($perId == "desligados" || $perId == "null") {
                $situacoesDesligamento = implode(',', $serviceSituacoes::situacoesDesligamento());

                $param['situacao_id'] = $situacoesDesligamento;
            } else {
                $params['perId']    = $param['per_id'];
                $params['sitAluno'] = array("Deferido", "Pendente");

                $where[] = 'acadperiodo__letivo.per_id IN ( :perId ) ';
                $where[] = 'acadgeral__aluno_curso.alunocurso_situacao IN ( :sitAluno ) ';
            }
        }

        if ($param['situacao_id']) {
            $where[]              = 'acadgeral__situacao.situacao_id IN ( :situacaoId ) ';
            $params['situacaoId'] = $param['situacao_id'];
        }

        if ($param['turma_id']) {
            $where[]           = 'acadperiodo__turma.turma_id IN ( :turmaId ) ';
            $params['turmaId'] = explode(',', $param['turma_id']);
        }

        if ($param['titulosAbertos']) {
            $where[] = 'titulo_estado="' . self::TITULO_ESTADO_ABERTO . '"';
        }

        if ($param['tipotitulo_id']) {
            $where[]              = 'tipotitulo_id IN ( :tipoTitulo)';
            $params['tipoTitulo'] = $param['tipotitulo_id'];
        }

        if ($param['vencimento_inicial']) {
            $vencimentoInicial = self::formatDateAmericano($param['vencimento_inicial']);
            $where[]           = "titulo_data_vencimento >= '" . $vencimentoInicial . "'";
        }

        if ($param['vencimento_final']) {
            $vencimentoFinal = self::formatDateAmericano($param['vencimento_final']);
            $where[]         = "titulo_data_vencimento <= '" . $vencimentoFinal . "'";
        }

        if ($param['titulosNotIn']) {
            $where[]            = ' titulo.titulo_id NOT IN (:tituloId)';
            $params['tituloId'] = array_filter(explode(',', $param['titulosNotIn']));
        }

        if ($where) {
            $sql = str_replace('-- CONDICOES --', ' WHERE ' . implode(' AND ', $where), $sql);
        }

        $arrRetorno = $this->executeQueryWithParam($sql, $params)->fetchAll();

        return $arrRetorno;
    }

    public function tituloUsaMaxiPago($arrTitulo, $setLastError = true)
    {
        $serviceFilaMaxiPago = new \Sistema\Service\SisFilaMaxipago($this->getEm());

        if (!$arrTitulo || empty($arrTitulo)) {
            $this->setLastError("Nenhum dado informado para busca!");

            return true;
        }

        $arTitulos = $arrTitulo;

        if (!is_array($arrTitulo)) {
            $entidade    = is_a($arrTitulo, '\Financeiro\Entity\FinanceiroTitulo');
            $arTitulos = $entidade ? $arrTitulo : explode(",", $arrTitulo);
        }

        $erro = array();

        foreach ($arTitulos as $tituloId) {

            if ($objMaxiPago = $serviceFilaMaxiPago->getRepository()->findOneBy(['titulo' => $tituloId])) {
                $erro[] = $objMaxiPago->getTitulo()->getTituloId();
            }
        }

        if (!empty($erro)) {
            if ($setLastError) {

                $this->setLastError(
                    " O título:" . implode(",", $erro) . " foi pago por cartão e não pode ser alterado!"
                );
            }

            return true;
        }

        return false;
    }
}

?>

