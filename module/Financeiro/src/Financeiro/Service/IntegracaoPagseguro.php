<?php

namespace Financeiro\Service;

use VersaSpine\Service\AbstractService;

class IntegracaoPagseguro extends AbstractService
{
    const ENV_SANDBOX = "sandbox";
    const ENV_PROD = "production";

    const  STATUS_TRANSACAO_AGUARDANDO_PAGAMENTO = 1;
    const  STATUS_TRANSACAO_EM_ANALISE = 2;
    const  STATUS_TRANSACAO_PAGA = 3;
    const  STATUS_TRANSACAO_DISPONIVEL = 4;
    const  STATUS_TRANSACAO_EM_DISPUTA = 5;
    const  STATUS_TRANSACAO_DEVOLVIDA = 6;
    const  STATUS_TRANSACAO_CANCELADA = 7;
    const  STATUS_TRANSACAO_DEBITADO = 8;
    const  STATUS_TRANSACAO_RETENCAO_TEMPORARIA = 9;

    private $lastError = null;
    private $auth = null;

    /** @var \Sistema\Service\SisConfig */
    private $config = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, $arrConfig = array())
    {
        parent::__construct($em, 'Matricula\Entity\AcadgeralAluno');

        $this->setConfig(new \Sistema\Service\SisConfig($em, $arrConfig));

        $this->configurarParametros();
    }

    /**
     * @return null
     */
    public function getAuth()
    {
        return $this->auth;
    }

    public static function situacoesPagSeguro(){
        return [
            self::STATUS_TRANSACAO_AGUARDANDO_PAGAMENTO => 'Aguardando Pagamento',
            self::STATUS_TRANSACAO_EM_ANALISE           => 'Em Análise',
            self::STATUS_TRANSACAO_PAGA                 => 'Paga',
            self::STATUS_TRANSACAO_DISPONIVEL           => 'Disponível',
            self::STATUS_TRANSACAO_EM_DISPUTA           => 'Em Disputa',
            self::STATUS_TRANSACAO_DEVOLVIDA            => 'Devolvida',
            self::STATUS_TRANSACAO_CANCELADA            => 'Cancelada',
            self::STATUS_TRANSACAO_DEBITADO             => 'Debitado',
            self::STATUS_TRANSACAO_RETENCAO_TEMPORARIA  => 'Retenção Temporária',
        ];
    }

    /**
     * @param null $auth
     * @return IntegracaoPagseguro
     */
    public function setAuth($auth)
    {
        $this->auth = $auth;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    /**
     * @return \Sistema\Service\SisConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param \Sistema\Service\SisConfig $config
     * @return IntegracaoPagseguro
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    protected function valida($dados)
    {
    }

    protected function pesquisaForJson($criterios)
    {
    }

    private function configurarParametros()
    {
        $projeto  = $this->getConfig()->localizarChave('projectName');
        $ambiente = $this->getConfig()->localizarChave('PAGSEGURO_AMBIENTE');

        if ($ambiente == self::ENV_SANDBOX) {
            $pagseguroEmail = $this->getConfig()->localizarChave('PAGSEGURO_EMAIL_SANDBOX');
            $pagseguroToken = $this->getConfig()->localizarChave('PAGSEGURO_TOKEN_SANDBOX');
        } else {
            $pagseguroEmail = $this->getConfig()->localizarChave('PAGSEGURO_EMAIL');
            $pagseguroToken = $this->getConfig()->localizarChave('PAGSEGURO_TOKEN');
        }

        $arquivoLog = sys_get_temp_dir() . '/universa-pgs.log';

        \PagSeguro\Library::initialize();
        \PagSeguro\Library::cmsVersion()->setName($projeto)->setRelease("1.0.0");
        \PagSeguro\Library::moduleVersion()->setName($projeto)->setRelease("1.0.0");
        \PagSeguro\Configuration\Configure::setEnvironment($ambiente);
        \PagSeguro\Configuration\Configure::setLog(true, $arquivoLog);
        \PagSeguro\Configuration\Configure::setAccountCredentials($pagseguroEmail, $pagseguroToken);
        $this->setAuth(\PagSeguro\Configuration\Configure::getAccountCredentials());
        \PagSeguro\Configuration\Configure::setCharset('UTF-8');
    }

    public function verificarNotificacoes()
    {
        try {
            if (\PagSeguro\Helpers\Xhr::hasPost()) {
                /** @var \PagSeguro\Parsers\Transaction\Response $objResponse */
                $objResponse = \PagSeguro\Services\Transactions\Notification::check(
                    $this->getAuth()
                );

                $status = $this->getConfig()->localizarChave('PAGSEGURO_STATUS_BAIXA', self::STATUS_TRANSACAO_PAGA);

                if ($objResponse->getStatus() == $status) {
                    $serviceTitulo        = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
                    $serviceMeioPagamento = new \Financeiro\Service\FinanceiroMeioPagamento($this->getEm());

                    //if($arrDados['pagseguroId']==$objResponse->getCode()){};

                    $valorASerRecebido = $objResponse->getNetAmount();
                    $valorPago         = $objResponse->getGrossAmount();
                    $observacao        = 'Valor Pago:' . $valorPago . "; Valor a ser recebido:" . $valorASerRecebido;
                    $arrPagamento      = [
                        'meioPagamento'        => $serviceMeioPagamento->retornaMeioDePagamentoPagseguro(),
                        'pagamentoValorBruto'  => $valorPago,
                        'pagamentoValor'       => $valorPago,
                        'pagamentoObservacoes' => $observacao
                    ];
                    $arrPagamento      = [$arrPagamento];

                    $registrouPagamento = $serviceTitulo->registrarPagamentoTitulo(
                        $objResponse->getReference(),
                        $objResponse->getLastEventDate(),
                        $valorPago,
                        $arrPagamento,
                        ['tituloObservacao' => $observacao]
                    );

                    if (!$registrouPagamento) {
                        $this->setLastError($serviceTitulo->getLastError());
                    }

                    return $registrouPagamento;
                }
            }
        } catch (\Exception $e) {
            return false;
        }

        return false;
    }

    public function criaCobranca($tituloId, $urlApp, $urlRetorno, $urlNotificacao)
    {
        $servicePessoa           = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $arrDados                = $serviceFinanceiroTitulo->retornaInformacoesTitulo($tituloId);

        if (empty($arrDados)) {
            $this->setLastError($serviceFinanceiroTitulo->getLastError());

            return false;
        }

        if ($arrDados['tituloEstado'] != $serviceFinanceiroTitulo::TITULO_ESTADO_ABERTO) {
            $this->setLastError('O título solicitado não está em aberto!');

            return false;
        }

        $arrDadosPessoa = $servicePessoa->getArray($arrDados['pesId']);

        try {
            $ambiente = $this->getConfig()->localizarChave('PAGSEGURO_AMBIENTE');

            $payment = new \PagSeguro\Domains\Requests\Payment();
            $payment->setCurrency("BRL");
            $payment->setReference($arrDados['tituloId']);
            $payment->setRedirectUrl($urlApp . $urlRetorno);
            $payment->setNotificationUrl($urlApp . $urlNotificacao);

            if ($ambiente == self::ENV_SANDBOX) {
                $payment->setSender()->setName('João Comprador');
                $payment->setSender()->setEmail('c58564229064925372980@sandbox.pagseguro.com.br');
            } else {
                $payment->setSender()->setName($arrDadosPessoa['pesNome']);
                $payment->setSender()->setEmail($arrDadosPessoa['conContatoEmail']);

                $cpf = preg_replace('/[^0-9]/', '', $arrDadosPessoa['pesCpf']);
                $tel = preg_replace('/[^0-9]/', '', $arrDadosPessoa['conContatoEmail']);

                if (preg_match('/^([0-9]{2})([0-9]{8,9}$)', $tel, $tel)) {
                    $payment->setSender()->setPhone()->withParameters($tel[1][0], $tel[1][1]);
                }

                $payment->setSender()->setDocument()->withParameters('CPF', $cpf);
            }

            $payment->setShipping()->setType()->withParameters(\PagSeguro\Enum\Shipping\Type::NOT_SPECIFIED);

            $payment->addItems()->withParameters(
                $arrDados['tituloId'],
                $arrDados['tituloDescricao'],
                1,
                $arrDados['tituloValorFinal']
            );

            try {
                $result = $payment->register($this->getAuth());

                $this->getEm()->beginTransaction();

                /** @var \Financeiro\Entity\FinanceiroTitulo $objTitulo */
                $objTitulo = $serviceFinanceiroTitulo->getRepository()->find($arrDados['tituloId']);

                $objTitulo->setTituloPagseguroLink($result);
                $this->getEm()->persist($objTitulo);
                $this->getEm()->flush($objTitulo);

                $this->getEm()->commit();

                $arrDados['tituloPagseguroLink'] = $objTitulo->getTituloPagseguroLink();

                return $arrDados;
            } catch (\Exception $e) {
                $this->setLastError($e->getMessage());
            }
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());
        }

        return false;
    }
}