<?php

namespace Financeiro\Service;

use VersaSpine\Service\AbstractService;

class FinanceiroValores extends AbstractService
{
    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Financeiro\Entity\FinanceiroValores');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function pesquisaForJson($params)
    {
        if ($params['cursoPossuiPeriodoLetivo']) {
            $this->setLastError('Curso com periodicidade!');

            return false;
        }

        $sql          = '
        SELECT
            a.valoresId AS valores_id,
            a.valoresPreco AS valores_preco,
            a.valoresDataInicio AS valores_data_inicio,
            a.valoresDataFim AS valores_data_fim,
            a.valoresParcela
        FROM Financeiro\Entity\FinanceiroValores a
        WHERE';
        $valoresPreco = false;
        $valoresId    = false;

        if ($params['q']) {
            $valoresPreco = $params['q'];
        } elseif ($params['query']) {
            $valoresPreco = $params['query'];
        }

        if ($params['valoresId']) {
            $valoresId = $params['valoresId'];
        }

        $parameters = array('valoresPreco' => "{$valoresPreco}%");
        $sql .= ' a.valoresPreco LIKE :valoresPreco';

        if ($valoresId) {
            $parameters['valoresId'] = explode(',', $valoresId);
            $parameters['valoresId'] = $valoresId;
            $sql .= ' AND a.valoresId NOT IN(:valoresId)';
        }

        $sql .= " ORDER BY a.valoresPreco";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceCampusCurso               = new \Matricula\Service\CampusCurso($this->getEm());
        $serviceAcadperiodoLetivo         = new \Matricula\Service\AcadperiodoLetivo($this->getEm());
        $serviceFinanceiroTituloTipo      = new \Financeiro\Service\FinanceiroTituloTipo($this->getEm());
        $serviceAcadgeralAreaConhecimento = new \Matricula\Service\AcadgeralAreaConhecimento($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['valoresId']) {
                /** @var $objFinanceiroValores \Financeiro\Entity\FinanceiroValores */
                $objFinanceiroValores = $this->getRepository()->find($arrDados['valoresId']);

                if (!$objFinanceiroValores) {
                    $this->setLastError('Registro de valores não existe!');

                    return false;
                }
            } else {
                $objFinanceiroValores = new \Financeiro\Entity\FinanceiroValores();
            }

            if ($arrDados['cursocampus']) {
                /** @var $objCampusCurso \Matricula\Entity\CampusCurso */
                $objCampusCurso = $serviceCampusCurso->getRepository()->find($arrDados['cursocampus']);

                if (!$objCampusCurso) {
                    $this->setLastError('Registro de câmpus curso não existe!');

                    return false;
                }

                $objFinanceiroValores->setCursocampus($objCampusCurso);
            } else {
                $objFinanceiroValores->setCursocampus(null);
            }

            if ($arrDados['per']) {
                /** @var $objAcadperiodoLetivo \Matricula\Entity\AcadperiodoLetivo */
                $objAcadperiodoLetivo = $serviceAcadperiodoLetivo->getRepository()->find($arrDados['per']);

                if (!$objAcadperiodoLetivo) {
                    $this->setLastError('Registro de letivo não existe!');

                    return false;
                }

                $objFinanceiroValores->setPer($objAcadperiodoLetivo);
            } else {
                $objFinanceiroValores->setPer(null);
            }

            if ($arrDados['tipotitulo']) {
                /** @var $objFinanceiroTituloTipo \Financeiro\Entity\FinanceiroTituloTipo */
                $objFinanceiroTituloTipo = $serviceFinanceiroTituloTipo->getRepository()->find($arrDados['tipotitulo']);

                if (!$objFinanceiroTituloTipo) {
                    $this->setLastError('Registro de tipo de título não existe!');

                    return false;
                }

                $objFinanceiroValores->setTipotitulo($objFinanceiroTituloTipo);
            } else {
                $objFinanceiroValores->setTipotitulo(null);
            }

            if ($arrDados['area']) {
                /** @var $objAcadgeralAreaConhecimento \Matricula\Entity\AcadgeralAreaConhecimento */
                $objAcadgeralAreaConhecimento = $serviceAcadgeralAreaConhecimento->getRepository()->find(
                    $arrDados['area']
                );

                if (!$objAcadgeralAreaConhecimento) {
                    $this->setLastError('Registro de área conhecimento não existe!');

                    return false;
                }

                $objFinanceiroValores->setArea($objAcadgeralAreaConhecimento);
            } else {
                $objFinanceiroValores->setArea(null);
            }

            $objFinanceiroValores->setValoresPreco($arrDados['valoresPreco']);
            $objFinanceiroValores->setValoresDataInicio($arrDados['valoresDataInicio']);
            $objFinanceiroValores->setValoresDataFim($arrDados['valoresDataFim']);
            $objFinanceiroValores->setValoresParcela($arrDados['valoresParcela']);

            $this->getEm()->persist($objFinanceiroValores);
            $this->getEm()->flush($objFinanceiroValores);

            $this->getEm()->commit();

            $arrDados['valoresId'] = $objFinanceiroValores->getValoresId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de valores!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['tipotitulo']) {
            $errors[] = 'Por favor preencha o campo "código tipo de título"!';
        }

        if (!$arrParam['valoresParcela']) {
            $errors[] = 'Por favor preencha o campo "parcela"!';
        }

        if (!$arrParam['per'] && (!$arrParam['valoresDataInicio'] || !$arrParam['valoresDataFim'])) {
            $errors[] = 'Por favor preencha o campo período letivo ou as datas de início e fim de uso!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "
        SELECT
            fv.valores_id,
            DATE_FORMAT(fv.valores_data_fim, '%d/%m/%Y') AS valores_data_fim,
            DATE_FORMAT(fv.valores_data_inicio, '%d/%m/%Y') AS valores_data_inicio,
            fv.valores_parcela,
            fv.valores_preco,
            fv.tipotitulo_id,
            tipotitulo_nome,
            fv.cursocampus_id,
            CONCAT(convert(camp_nome USING UTF8), ' / ', convert(curso_nome USING UTF8)) AS campus,
            fv.per_id,
            per_nome,
            fv.area_id,
            area_descricao
        FROM financeiro__valores fv
        INNER JOIN financeiro__titulo_tipo USING (tipotitulo_id)
        LEFT JOIN campus_curso USING (cursocampus_id)
        LEFT JOIN org_campus USING (camp_id)
        LEFT JOIN acad_curso USING (curso_id)
        LEFT JOIN acadperiodo__letivo USING (per_id)
        LEFT JOIN acadgeral__area_conhecimento a ON fv.area_id=a.area_id
        ";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($valoresId)
    {
        /** @var $objFinanceiroValores \Financeiro\Entity\FinanceiroValores */
        $objFinanceiroValores             = $this->getRepository()->find($valoresId);
        $serviceCampusCurso               = new \Matricula\Service\CampusCurso($this->getEm());
        $serviceAcadperiodoLetivo         = new \Matricula\Service\AcadperiodoLetivo($this->getEm());
        $serviceFinanceiroTituloTipo      = new \Financeiro\Service\FinanceiroTituloTipo($this->getEm());
        $serviceAcadgeralAreaConhecimento = new \Matricula\Service\AcadgeralAreaConhecimento($this->getEm());

        try {
            $arrDados = $objFinanceiroValores->toArray();

            if ($arrDados['cursocampus']) {
                $arrCampusCurso          = $serviceCampusCurso->getArrSelect2(['id' => $arrDados['cursocampus']]);
                $arrDados['cursocampus'] = $arrCampusCurso ? $arrCampusCurso[0] : null;
            }

            if ($arrDados['per']) {
                $arrCampusCurso  = $serviceAcadperiodoLetivo->getArrSelect2(['id' => $arrDados['per']]);
                $arrDados['per'] = $arrCampusCurso ? $arrCampusCurso[0] : null;
            }

            if ($arrDados['tipotitulo']) {
                $arrCampusCurso         = $serviceFinanceiroTituloTipo->getArrSelect2(
                    ['id' => $arrDados['tipotitulo']]
                );
                $arrDados['tipotitulo'] = $arrCampusCurso ? $arrCampusCurso[0] : null;
            }

            if ($arrDados['area']) {
                $arrCampusCurso   = $serviceAcadgeralAreaConhecimento->getArrSelect2(['id' => $arrDados['area']]);
                $arrDados['area'] = $arrCampusCurso ? $arrCampusCurso[0] : null;
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceCampusCurso               = new \Matricula\Service\CampusCurso($this->getEm());
        $serviceAcadperiodoLetivo         = new \Matricula\Service\AcadperiodoLetivo($this->getEm());
        $serviceFinanceiroTituloTipo      = new \Financeiro\Service\FinanceiroTituloTipo($this->getEm());
        $serviceAcadgeralAreaConhecimento = new \Matricula\Service\AcadgeralAreaConhecimento($this->getEm());

        if (is_array($arrDados['cursocampus']) && !$arrDados['cursocampus']['id']) {
            $arrDados['cursocampus']['id']   = $arrDados['cursocampus']['cursocampusId'];
            $arrDados['cursocampus']['text'] = (
                $arrDados['cursocampus']['campNome'] . ' / ' . $arrDados['cursocampus']['cursoNome']
            );
        } elseif ($arrDados['cursocampus'] && is_string($arrDados['cursocampus'])) {
            $arrDados['cursocampus'] = $serviceCampusCurso->getArrSelect2(
                array('id' => $arrDados['cursocampus'])
            );
        }

        if (is_array($arrDados['per']) && !$arrDados['per']['id']) {
            $arrDados['per']['id']   = $arrDados['per']['perId'];
            $arrDados['per']['text'] = $arrDados['per']['perNome'];
        } elseif ($arrDados['per'] && is_string($arrDados['per'])) {
            $arrDados['per'] = $serviceAcadperiodoLetivo->getArrSelect2(
                array('id' => $arrDados['per'])
            );
        }

        if (is_array($arrDados['tipotitulo']) && !$arrDados['tipotitulo']['id']) {
            $arrDados['tipotitulo']['id']   = $arrDados['tipotitulo']['tipotituloId'];
            $arrDados['tipotitulo']['text'] = $arrDados['tipotitulo']['tipotituloNome'];
        } elseif ($arrDados['tipotitulo'] && is_string($arrDados['tipotitulo'])) {
            $arrDados['tipotitulo'] = $serviceFinanceiroTituloTipo->getArrSelect2(
                array('id' => $arrDados['tipotitulo'])
            );
        }

        if (is_array($arrDados['area']) && !$arrDados['area']['id']) {
            $arrDados['area']['id']   = $arrDados['area']['areaId'];
            $arrDados['area']['text'] = $arrDados['area']['areaDescricao'];
        } elseif ($arrDados['area'] && is_string($arrDados['area'])) {
            $arrDados['area'] = $serviceAcadgeralAreaConhecimento->getArrSelect2(
                array('id' => $arrDados['area'])
            );
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('valoresId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Financeiro\Entity\FinanceiroValores */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getValoresId(),
                $params['value'] => (
                    $objEntity->getTipotitulo()->getTipotituloNome() . ' / ' .
                    $objEntity->getCursocampus()->getCamp()->getCampNome() . ' / ' .
                    $objEntity->getCursocampus()->getCurso()->getCursoNome() . ' / ' .
                    (
                    $objEntity->getPer() ?
                        $objEntity->getPer()->getPerNome() :
                        $objEntity->getValoresDataInicio(true) .
                        '-' .
                        $objEntity->getValoresDataFim(true)
                    )
                )
            );
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['valoresId']) {
            $this->setLastError('Para remover um registro de valores é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objFinanceiroValores \Financeiro\Entity\FinanceiroValores */
            $objFinanceiroValores = $this->getRepository()->find($param['valoresId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objFinanceiroValores);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de valores.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        //$serviceCampusCurso          = new \Matricula\Service\CampusCurso($this->getEm());
        //$serviceAcadperiodoLetivo    = new \Matricula\Service\AcadperiodoLetivo($this->getEm());
        //$serviceFinanceiroTituloTipo = new \Financeiro\Service\FinanceiroTituloTipo($this->getEm());

        //$serviceCampusCurso->setarDependenciasView($view);
        //$serviceAcadperiodoLetivo->setarDependenciasView($view);
        //$serviceFinanceiroTituloTipo->setarDependenciasView($view);
    }

    public function buscaValorMensalidadeAno($perId)
    {
        if (!is_null($perId)) {
            $valor = $this->executeQuery(
                "
                SELECT
                  valores_preco
                FROM
                  financeiro__valores
                NATURAL JOIN
                  acadperiodo__letivo
                WHERE
                acadperiodo__letivo.per_id = '{$perId}'
            "
            );

            if ($valor->rowCount() > 0) {
                return $valor->fetch()['valores_preco'];
            }
        }

        return false;
    }

    public function buscaValoresAtivosParaTipoTitulo($titulotipo, $arrFiltro = array())
    {
        $addQuery      = '';
        $areaId        = $arrFiltro['area'] ? $arrFiltro['area'] : false;
        $cursocampusId = $arrFiltro['cursoCampus'] ? $arrFiltro['cursoCampus'] : false;
        $dataEmissao   = $arrFiltro['dataEmissao'] ? $arrFiltro['dataEmissao'] : date('Y-m-d');
        $dataEmissao   = self::formatDateAmericano($dataEmissao);

        if ($cursocampusId) {
            $addQuery .= 'v.cursocampus = :cursocampus AND ';
        } else {
            $addQuery .= 'v.cursocampus IS NULL AND ';
        }

        if ($areaId) {
            $addQuery .= 'v.area = :area AND ';
        } else {
            $addQuery .= 'v.area IS NULL AND ';
        }

        $query = "
        SELECT v FROM \Financeiro\Entity\FinanceiroValores v
        LEFT JOIN \Financeiro\Entity\FinanceiroTituloTipo tt WITH tt.tipotituloId=v.tipotitulo
        LEFT JOIN v.per p
        LEFT JOIN v.cursocampus c
        WHERE v.tipotitulo = :tipotitulo AND
            " . $addQuery . "
            (
                (:dataAtual >= v.valoresDataInicio AND :dataAtual <= v.valoresDataFim)
                OR
                (:dataAtual >= p.perDataInicio AND :dataAtual <= p.perDataFim)
                OR
                (v.per IS NULL AND v.valoresDataInicio IS NULL AND v.valoresDataFim IS NULL)
            )
        ORDER BY v.valoresDataInicio ASC,
                 p.perDataInicio ASC,
                 v.valoresParcela ASC,
                 v.valoresPreco ASC
        ";

        $dql = $this
            ->getEm()
            ->createQuery($query)
            ->setParameter('tipotitulo', $titulotipo)
            ->setParameter('dataAtual', new \DateTime($dataEmissao));

        if ($cursocampusId) {
            $dql->setParameter('cursocampus', $cursocampusId);
        }

        if ($areaId) {
            $dql->setParameter('area', $areaId);
        }

        $arrObjFinanceiroValores = $dql->getResult();

        /** @var  $objFinanceiroValores \Financeiro\Entity\FinanceiroValores */
        foreach ($arrObjFinanceiroValores as $pos => $objFinanceiroValores) {
            $arrObjFinanceiroValores[$pos] = array_merge_recursive(
                $objFinanceiroValores->toArray(),
                $objFinanceiroValores->getTipotitulo()->toArray()
            );
        }

        return $arrObjFinanceiroValores;
    }

    public function retornaValoresPeloTipo($tipo, $cursocampusId = false, $perId = false)
    {
        $serviceFinanceiroTipo    = new \Financeiro\Service\FinanceiroTituloTipo($this->getEm());
        $serviceCampusCurso       = new \Matricula\Service\CampusCurso($this->getEm());
        $serviceAcadperiodoLetivo = new \Matricula\Service\AcadperiodoLetivo($this->getEm());
        $arrRetorno               = array();
        $dataEmissao              = date('Y-m-d');

        /* @var $objFinanceiroTituloTipo \Financeiro\Entity\FinanceiroTituloTipo */
        $objFinanceiroTituloTipo = $serviceFinanceiroTipo->getRepository()->find($tipo);

        if ($perId) {
            /** @var \Matricula\Entity\AcadperiodoLetivo $objPeriodoLetivo */
            $objPeriodoLetivo = $serviceAcadperiodoLetivo->getRepository()->find($perId);
            $dataEmissao      = $objPeriodoLetivo->getPerDataInicio()->format('Y-m-d');
        }

        if ($cursocampusId) {
            /** @var \Matricula\Entity\CampusCurso $objCampusCurso */
            $objCampusCurso = $serviceCampusCurso->getRepository()->find($cursocampusId);

            //Busca possíveis valores do tipo de título baseado no curso
            $arrRetorno = $this->buscaValoresAtivosParaTipoTitulo(
                $objFinanceiroTituloTipo,
                ['cursoCampus' => $cursocampusId, 'dataEmissao' => $dataEmissao]
            );

            if (!$arrRetorno && $objCampusCurso && $objCampusCurso->getCurso()->getArea()) {
                //Busca possíveis valores do tipo de título baseado na área do curso
                $arrRetorno = $this->buscaValoresAtivosParaTipoTitulo(
                    $objFinanceiroTituloTipo,
                    [
                        'area'        => $objCampusCurso->getCurso()->getArea() ? $objCampusCurso->getCurso()->getArea(
                        )->getAreaId() : null,
                        'dataEmissao' => $dataEmissao
                    ]
                );

                if (!$arrRetorno) {
                    //Busca possíveis valores do tipo de título generico
                    $arrRetorno = $this->buscaValoresAtivosParaTipoTitulo(
                        $objFinanceiroTituloTipo,
                        ['dataEmissao' => $dataEmissao]
                    );
                }
            }
        } else {
            $arrRetorno = $this->buscaValoresAtivosParaTipoTitulo(
                $objFinanceiroTituloTipo,
                ['dataEmissao' => $dataEmissao]
            );
        }

        return $arrRetorno;
    }

    public function retornaValoresPeloTipoPeriodoCursoCampus($tipo, $cursocampusId, $periodoLetivo)
    {
        $erros = [];

        if (!$tipo) {
            $erros[] = 'É necessário informar o tipo!';
        }

        if (!$cursocampusId) {
            $erros[] = 'É necessário informar a matricula!';
        }

        if (!$periodoLetivo) {
            $erros[] = 'É necessário informar o período letivo!';
        }

        if ($erros) {
            $this->setLastError(implode("\n", $erros));

            return false;
        }

        $arrParametros = [
            'tipo'          => $tipo,
            'cursocampusId' => $cursocampusId,
            'periodoLetivo' => $periodoLetivo
        ];

        $query = "
        SELECT
            valores_id AS valoresId,
            valores_parcela AS valoresParcela,
            valores_parcela AS valoresParcelaOriginal,
            valores_preco AS valoresPreco,
            valores_preco AS valoresPrecoOriginal,
            tipo.tipotitulo_grupo AS grupoTitulo
        FROM financeiro__valores AS v
        LEFT JOIN financeiro__titulo_tipo AS tipo ON tipo.tipotitulo_id=v.tipotitulo_id
        WHERE v.tipotitulo_id = :tipo AND
              cursocampus_id = :cursocampusId AND
              per_id = :periodoLetivo";

        $arrValores = $this->executeQueryWithParam($query, $arrParametros)->fetchAll();

        return $arrValores;
    }

    public function buscaValoresPeriodoConfig($tituloTipo = false, $cursocampusId = false, $periodoLetivo = false)
    {
        if (!$tituloTipo) {
            $this->setLastError("É necessário informar um tipo de título!");

            return false;
        }

        $serviceCampusCurso   = new \Matricula\Service\CampusCurso($this->getEm());
        $serviceAcadCurso     = new \Matricula\Service\AcadCurso($this->getEm());
        $servicePeriodoLetivo = new \Matricula\Service\AcadperiodoLetivo($this->getEm());
        $serviceTituloTipo    = new \Financeiro\Service\FinanceiroTituloTipo($this->getEm());

        $cursoPossuiPeriodoLetivo = "";
        $dataInicio               = "";
        $dataFim                  = "";
        $areaId                   = "";
        $dataAtual                = date('Ymd');

        //TODO: Otimizar código

        /** @var  \Financeiro\Entity\FinanceiroTituloTipo */
        $objTituloTipo = $serviceTituloTipo->getRepository()->find($tituloTipo);

        if (!$objTituloTipo) {
            $this->setLastError("Esse tipo de título não foi localizado !");

            return false;
        }

        /** @var \Matricula\Entity\CampusCurso $objCampusCurso */
        $objCampusCurso = $serviceCampusCurso->getRepository()->findOneBy(['cursocampusId' => $cursocampusId]);

        if ($periodoLetivo && !$objCampusCurso) {
            $this->setLastError("É necessário informar um curso de aluno!");

            return false;
        }

        if ($objCampusCurso) {
            if ($objCampusCurso->getCurso()->getCursoPossuiPeriodoLetivo(
                ) != $serviceAcadCurso::CURSO_POSSUI_PERIODO_LETIVO_NAO
            ) {
                $cursoPossuiPeriodoLetivo = $objCampusCurso->getCurso()->getCursoUnidadeMedida();
            } else {
                $cursoPossuiPeriodoLetivo = false;
            }

            if ($objCampusCurso->getCurso()->getArea()) {
                $areaId = $objCampusCurso->getCurso()->getArea()->getAreaId();
            }
        }

        if ($objCampusCurso) {
            $sqlCondicoes = "
        (
            v.tipotitulo_id=:tituloTipo AND
            (
                __DATA__ >= date(v.valores_data_inicio) AND __DATA__ <= date(v.valores_data_fim)
             ) AND
            (
              (v.cursocampus_id IS NULL AND v.area_id IS NULL) OR
              (v.cursocampus_id=:cursocampusId AND v.area_id IS NULL) OR
              (v.cursocampus_id IS NULL AND v.area_id=:areaId) OR
              (v.cursocampus_id=:cursocampusId AND v.area_id=:areaId)
            ) )
        ";

            $arrParam = [
                'tituloTipo'    => $tituloTipo,
                'areaId'        => $areaId,
                'cursocampusId' => $cursocampusId,
            ];
        } else {
            $sqlCondicoes = "
        (
            v.tipotitulo_id=:tituloTipo AND
            (
                __DATA__ >= date(v.valores_data_inicio) AND __DATA__ <= date(v.valores_data_fim)
            ))
        ";

            $arrParam = [
                'tituloTipo' => $tituloTipo
            ];
        }

        if ($cursoPossuiPeriodoLetivo) {
            /** @var \Matricula\Entity\AcadperiodoLetivo $objPeriodoLetivo */
            $objPeriodoLetivo = $servicePeriodoLetivo->getRepository()->findOneBy(['perId' => $periodoLetivo]);

            $eGrupoAcademico = ($objTituloTipo->getTipotituloGrupo() == $serviceTituloTipo::TIPOTITULO_GRUPO_ACADEMICO);

            if (!$periodoLetivo && $eGrupoAcademico) {
                $this->setLastError("É necessário informar o período letivo!");

                return false;
            }

            if ($objPeriodoLetivo) {
                $dataInicio = $objPeriodoLetivo->getPerDataInicio()->format('Ymd');
                $dataFim    = $objPeriodoLetivo->getPerDataFim()->format('Ymd');

                $arrParam['periodoLetivo'] = $periodoLetivo;
                $arrParam['dataInicio']    = $dataInicio;
                $arrParam['dataFim']       = $dataFim;

                $sqlCondicoes = "
                (
                    v.tipotitulo_id=:tituloTipo AND
                    (
                        v.per_id=:periodoLetivo OR
                        (__DATA__ >= date(v.valores_data_inicio) AND __DATA__ <= date(v.valores_data_fim))
                     ) AND
                    (
                      (v.cursocampus_id IS NULL AND v.area_id IS NULL) OR
                      (v.cursocampus_id=:cursocampusId AND v.area_id IS NULL) OR
                      (v.cursocampus_id IS NULL AND v.area_id=:areaId) OR
                      (v.cursocampus_id=:cursocampusId AND v.area_id=:areaId)
                    )
                )";
            }
        }

        $sqlJoins = [];

        $sqlTpl = "
            SELECT
                v.valores_id AS valoresId,
                v.valores_parcela AS valoresParcela,
                v.valores_parcela AS valoresParcelaOriginal,
                v.valores_preco AS valoresPreco,
                v.valores_preco AS valoresPrecoOriginal,
            COALESCE(
                (
                    (date(now()) >= v.valores_data_inicio AND date(now()) <= v.valores_data_fim)
                    OR
                    (date(now()) >= pl.per_data_inicio AND date(now())<= pl.per_data_fim)
                ),
                0
            ) AS valorAtual

            FROM financeiro__valores v
            LEFT JOIN financeiro__titulo_config tc ON tc.tipotitulo_id=v.tipotitulo_id
            LEFT JOIN acadperiodo__letivo AS pl ON pl.per_id=v.per_id
            WHERE {$sqlCondicoes}
        ";

        $sqlJoins[] = str_replace('__DATA__', $dataAtual, $sqlTpl);

        if ($dataInicio) {
            $sqlJoins[] = str_replace('__DATA__', $dataInicio, $sqlTpl);
        }

        if ($dataFim) {
            $sqlJoins[] = str_replace('__DATA__', $dataFim, $sqlTpl);
        }

        $sqlJoins = implode(' UNION ', $sqlJoins);
        $sqlJoins .= ' GROUP BY valores_parcela, valores_preco';

        $arrDados = $this->executeQueryWithParam($sqlJoins, $arrParam)->fetchAll();

        return $arrDados;
    }

    public function retornaTituloLegado($valorId = false)
    {
        if (!$valorId) {
            $this->setLastError('É necessário informar um titulo!');

            return false;
        }

        $param['valorId'] = $valorId;

        $sql = '
         SELECT
            COALESCE(
                (
                    (date(now()) >= v.valores_data_inicio AND date(now()) <= v.valores_data_fim)
                    OR
                    (date(now()) >= pl.per_data_inicio AND date(now())<= pl.per_data_fim)
                ),
                0
            ) AS valorLegado
            FROM financeiro__valores v
            LEFT JOIN acadperiodo__letivo AS pl ON pl.per_id=v.per_id
            WHERE v.valores_id = :valorId';

        $valorLegado = $this->executeQueryWithParam($sql, $param)->fetch();

        return !$valorLegado['valorLegado'];
    }
}
?>