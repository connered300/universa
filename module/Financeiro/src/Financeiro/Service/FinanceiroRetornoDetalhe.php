<?php

namespace Financeiro\Service;

use VersaSpine\Service\AbstractService;

class FinanceiroRetornoDetalhe extends AbstractService
{
    const STATUS_PROCESSANDO = 'Processando';
    const STATUS_PENDENTE    = 'Pendente';
    const STATUS_FINALIZADO  = 'Finalizado';
    const STATUS_REJEITADO   = 'Rejeitado';

    private $__lastError = null;
    private $__feedback  = array();

    /** @var \Sistema\Service\SisConfig */
    private $config = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @return array
     */
    public function getFeedback()
    {
        return $this->__feedback;
    }

    /**
     * @param array $feedback
     */
    public function setFeedback($feedback)
    {
        $this->__feedback = $feedback;
    }

    /**
     * @return \Sistema\Service\SisConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param \Sistema\Service\SisConfig $config
     * @return FinanceiroRetornoDetalhe
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, $arrConfig = array())
    {
        parent::__construct($em, 'Financeiro\Entity\FinanceiroRetornoDetalhe');

        $this->setConfig(new \Sistema\Service\SisConfig($em, $arrConfig));
    }

    public function pesquisaForJson($params)
    {
        $sql                  = '
        SELECT
        a.detalheId AS detalhe_id,
        a.retorno AS retorno_id,
        a.detalheData AS detalhe_data,
        a.detalheDataVencimento AS detalhe_data_vencimento,
        a.detalheNossoNumero AS detalhe_nosso_numero,
        a.detalheNumeroDocumento AS detalhe_numero_documento,
        a.detalheValorTitulo AS detalhe_valor_titulo,
        a.detalheValorPago AS detalhe_valor_pago,
        a.detalheValorMora AS detalhe_valor_mora,
        a.detalheValorDesconto AS detalhe_valor_desconto,
        a.detalheCarteira AS detalhe_carteira,
        a.detalheStatus AS detalhe_status,
        a.detalheAlegacao AS detalhe_alegacao
        FROM Financeiro\Entity\FinanceiroRetornoDetalhe a
        WHERE';
        $detalheValorDesconto = false;
        $detalheId            = false;
        $retornoId            = false;

        if ($params['q']) {
            $detalheValorDesconto = $params['q'];
        } elseif ($params['query']) {
            $detalheValorDesconto = $params['query'];
        }

        if ($params['detalheId']) {
            $detalheId = $params['detalheId'];
        }

        if ($params['retornoId']) {
            $retornoId = $params['retornoId'];
        }

        $parameters = array('detalheValorDesconto' => "{$detalheValorDesconto}%");
        $sql .= ' a.detalheValorDesconto LIKE :detalheValorDesconto';

        if ($detalheId) {
            $parameters['detalheId'] = explode(',', $detalheId);
            $parameters['detalheId'] = $detalheId;
            $sql .= ' AND a.detalheId NOT IN(:detalheId)';
        }

        if ($retornoId) {
            $parameters['retornoId'] = $retornoId;
            $sql .= ' AND a.retorno IN(:detalheId)';
        }

        $sql .= " ORDER BY a.detalheValorDesconto";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceFinanceiroRetorno = new \Financeiro\Service\FinanceiroRetorno($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['detalheId']) {
                /** @var $objFinanceiroRetornoDetalhe \Financeiro\Entity\FinanceiroRetornoDetalhe */
                $objFinanceiroRetornoDetalhe = $this->getRepository()->find($arrDados['detalheId']);

                if (!$objFinanceiroRetornoDetalhe) {
                    $this->setLastError('Registro de retorno detalhe não existe!');

                    return false;
                }
            } else {
                $objFinanceiroRetornoDetalhe = new \Financeiro\Entity\FinanceiroRetornoDetalhe();
            }

            if ($arrDados['retorno']) {
                /** @var $objFinanceiroRetorno \Financeiro\Entity\FinanceiroRetorno */
                $objFinanceiroRetorno = $serviceFinanceiroRetorno->getRepository()->find($arrDados['retorno']);

                if (!$objFinanceiroRetorno) {
                    $this->setLastError('Registro de retorno não existe!');

                    return false;
                }

                $objFinanceiroRetornoDetalhe->setRetorno($objFinanceiroRetorno);
            } else {
                $objFinanceiroRetornoDetalhe->setRetorno(null);
            }

            if (!$arrDados['detalheStatus']) {
                $arrDados['detalheStatus'] = self::STATUS_PENDENTE;
            }

            $objFinanceiroRetornoDetalhe->setDetalheStatus($arrDados['detalheStatus']);
            $objFinanceiroRetornoDetalhe->setDetalheData($arrDados['detalheData']);
            $objFinanceiroRetornoDetalhe->setDetalheDataVencimento($arrDados['detalheDataVencimento']);
            $objFinanceiroRetornoDetalhe->setDetalheNossoNumero($arrDados['detalheNossoNumero']);
            $objFinanceiroRetornoDetalhe->setDetalheNumeroDocumento($arrDados['detalheNumeroDocumento']);
            $objFinanceiroRetornoDetalhe->setDetalheValorTitulo($arrDados['detalheValorTitulo']);
            $objFinanceiroRetornoDetalhe->setDetalheValorPago($arrDados['detalheValorPago']);
            $objFinanceiroRetornoDetalhe->setDetalheValorMora($arrDados['detalheValorMora']);
            $objFinanceiroRetornoDetalhe->setDetalheValorDesconto($arrDados['detalheValorDesconto']);
            $objFinanceiroRetornoDetalhe->setDetalheCarteira($arrDados['detalheCarteira']);
            $objFinanceiroRetornoDetalhe->setDetalheAlegacao($arrDados['detalheAlegacao']);
            $objFinanceiroRetornoDetalhe->setDetalheCodigoMovimento($arrDados['detalheCodigoMovimento']);

            if (!$arrDados['detalheLog']) {
                $arrDados['detalheLog'] = '';
            }

            $objFinanceiroRetornoDetalhe->setDetalheLog($arrDados['detalheLog']);

            $this->getEm()->persist($objFinanceiroRetornoDetalhe);
            $this->getEm()->flush($objFinanceiroRetornoDetalhe);

            $this->getEm()->commit();

            $arrDados['detalheId'] = $objFinanceiroRetornoDetalhe->getDetalheId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de retorno detalhe!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['retorno']) {
            $errors[] = 'Por favor preencha o campo "código retorno"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM financeiro__retorno_detalhe";

        if ($data['retornoId']) {
            $query .= ' WHERE retorno_id = ' . $data['retornoId'];
        }

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($detalheId)
    {
        /** @var $objFinanceiroRetornoDetalhe \Financeiro\Entity\FinanceiroRetornoDetalhe */
        $objFinanceiroRetornoDetalhe = $this->getRepository()->find($detalheId);
        $serviceFinanceiroRetorno    = new \Financeiro\Service\FinanceiroRetorno($this->getEm());

        try {
            $arrDados = $objFinanceiroRetornoDetalhe->toArray();

            if ($arrDados['retorno']) {
                $arrFinanceiroRetorno = $serviceFinanceiroRetorno->getArrSelect2(['id' => $arrDados['retorno']]);
                $arrDados['retorno']  = $arrFinanceiroRetorno ? $arrFinanceiroRetorno[0] : null;
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceFinanceiroRetorno = new \Financeiro\Service\FinanceiroRetorno($this->getEm());

        if (is_array($arrDados['retorno']) && !$arrDados['retorno']['text']) {
            $arrDados['retorno'] = $arrDados['retorno']['retornoId'];
        }

        if ($arrDados['retorno'] && is_string($arrDados['retorno'])) {
            $arrDados['retorno'] = $serviceFinanceiroRetorno->getArrSelect2(array('id' => $arrDados['retorno']));
            $arrDados['retorno'] = $arrDados['retorno'] ? $arrDados['retorno'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['detalheId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['detalheValorDesconto' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Financeiro\Entity\FinanceiroRetornoDetalhe */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getDetalheId();
            $arrEntity[$params['value']] = $objEntity->getDetalheValorDesconto();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['detalheId']) {
            $this->setLastError('Para remover um registro de retorno detalhe é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objFinanceiroRetornoDetalhe \Financeiro\Entity\FinanceiroRetornoDetalhe */
            $objFinanceiroRetornoDetalhe = $this->getRepository()->find($param['detalheId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objFinanceiroRetornoDetalhe);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de retorno detalhe.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceFinanceiroRetorno = new \Financeiro\Service\FinanceiroRetorno($this->getEm());

        $serviceFinanceiroRetorno->setarDependenciasView($view);
    }

    public function processaDetalhesRetorno()
    {
        $serviceFinanceiroboleto        = new \Financeiro\Service\Boleto($this->getEm());
        $serviceFinanceiroTitulo        = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $serviceFinanceiroPagamento     = new \Financeiro\Service\FinanceiroPagamento($this->getEm());
        $serviceFinanceiroMeioPagamento = new \Financeiro\Service\FinanceiroMeioPagamento($this->getEm());
        $serviceSelecaoInscricao        = new \Vestibular\Service\SelecaoInscricao($this->getEm());
        $serviceFinanceiroRetorno       = new \Financeiro\Service\FinanceiroRetorno($this->getEm());

        $serviceFinanceiroRetorno->atualizarRetornosSemDetalhesPendentes();

        $feedback    = array();
        $arrDetalhes = $this->retornaDetalhesParaProcessamento();
        $arrDetalhes = $this->atualizarSituacaoDetalheEmProcessamento($arrDetalhes);

        $feedback[] = "Quantidade de detalhes: " . count($arrDetalhes);

        foreach ($arrDetalhes as $detalhe) {
            try {
                $this->getEm()->beginTransaction();

                /** @var \Financeiro\Entity\FinanceiroRetornoDetalhe $objDetalhe */
                $objDetalhe = $this->getRepository()->find($detalhe['detalhe_id']);

                if (!$objBoleto = $serviceFinanceiroboleto->buscaBoletoPeloDetalhe($detalhe, true)) {
                    $this->setLastError(
                        "Boleto com nosso número: {$detalhe['detalhe_nosso_numero']} não encontrado!"
                    );
                    $feedback[] = 'Boleto não encontrado: ' . $detalhe['detalhe_nosso_numero'];
                    $objDetalhe->setDetalheStatus(self::STATUS_REJEITADO);
                } else {
                    $objBoleto->setBolEstado(\Financeiro\Service\Boleto::BOL_ESTADO_PAGO);
                    $objBoleto->setBolJuros($detalhe['detalhe_valor_mora']);

                    $this->getEm()->persist($objBoleto);
                    $this->getEm()->flush($objBoleto);


                    /** @var \Financeiro\Entity\FinanceiroTitulo $objTitulo */
                    $objTitulo = $objBoleto->getTitulo();

                    $objDetalhe->setTituloId($objTitulo->getTituloId());

                    $arrDados = array(
                        'titulo'                  => $objTitulo,
                        'meioPagamento'           => $serviceFinanceiroMeioPagamento->retornaMeioDePagamentoBoleto(),
                        'pagamentoUsuario'        => $objTitulo->getUsuarioAutor(),
                        'pes'                     => $objTitulo->getPes(),
                        'pagamentoValorBruto'     => $objTitulo->getTituloValor(),
                        'pagamentoValorJuros'     => 0,
                        'pagamentoValorMulta'     => $detalhe['detalhe_valor_mora'],
                        'pagamentoAcrescimos'     => 0,
                        'pagamentoValorDescontos' => 0,
                        'pagamentoValorFinal'     => $detalhe['detalhe_valor_titulo'],
                        'pagamentoDescontoManual' => 0,
                        'pagamentoDataBaixa'      => new \DateTime(),
                        'pagamentoObservacoes'    => 0,
                    );

                    try {
                        $serviceFinanceiroPagamento->save($arrDados);
                    } catch (\Exception $e) {
                        $objDetalhe->setDetalheStatus(self::STATUS_PENDENTE);
                        continue;
                    }

                    // atualiza os valores do titulo
                    $objTitulo->setUsuarioBaixa($objTitulo->getUsuarioAutor());
                    $objTitulo->setTituloValorPago($detalhe['detalhe_valor_pago']);
                    $objTitulo->setTituloDataPagamento($detalhe['detalhe_data']);
                    $objTitulo->setTituloDataBaixa(new \Datetime());
                    $objTitulo->setTituloEstado(FinanceiroTitulo::TITULO_ESTADO_PAGO);

                    $this->getEm()->persist($objTitulo);
                    $this->getEm()->flush($objTitulo);

                    $objDetalhe->setDetalheStatus(self::STATUS_FINALIZADO);

                    if (!$serviceFinanceiroTitulo->alteraSituacaoAlunoPorTituloId($objTitulo)) {
                        $objDetalhe->setDetalheStatus(self::STATUS_PENDENTE);
                        $objDetalhe->setDetalheLog(
                            'Não foi possível ativar a matŕicula do aluno:' .
                            $serviceFinanceiroTitulo->getLastError() .
                            'Matrícula: ' . $objTitulo->getAlunocurso()->getAlunocursoId()
                        );
                    }

                    if ($objTitulo->getTipotitulo()->verificaSeEdeVertibular()) {
                        $serviceSelecaoInscricao->atualizaSituacaoInscricaoPorBaixaBoleto($objBoleto);
                    }
                }

                $this->getEm()->persist($objDetalhe);
                $this->getEm()->flush($objDetalhe);

                $this->getEm()->commit();
            } catch (\Exception $e) {
                $this->setLastError($e->getMessage());
                $this->atualizarStatusDetalhe($detalhe['detalhe_id'], self::STATUS_REJEITADO);
            }

            $feedback[] = 'Detalhe inválido: ' . $detalhe['detalhe_nosso_numero'];
        }

        $this->setFeedback($feedback);

        $serviceFinanceiroRetorno->atualizarRetornosSemDetalhesPendentes();

        return true;
    }

    /**
     * @return bool|array
     */
    public function retornaDetalhesParaProcessamento($retornoId = false)
    {
        $serviceFinanceiroRetorno = new \Financeiro\Service\FinanceiroRetorno($this->getEm());
        $serviceFinanceiroRetorno->atualizarRetornosSemDetalhesPendentes();

        $numeroRegistros = $this->getConfig()->localizarChave('FINANCEIRO_RETORNO_NUMERO_DETALHES_PROCESSAMENTO', 10);

        $query = "
        SELECT d.*
        FROM financeiro__retorno_detalhe d
        INNER JOIN financeiro__retorno r ON
            r.retorno_id=d.retorno_id AND r.retorno_status IN(:retorno_status)
        WHERE detalhe_status = :detalhe_status
        -- CONDICOES --
        LIMIT 0, " . $numeroRegistros;

        $param = [
            'retorno_status' => [self::STATUS_PENDENTE, self::STATUS_PROCESSANDO],
            'detalhe_status' => self::STATUS_PENDENTE
        ];

        if ($retornoId) {
            $param['retorno_id'] = $retornoId;

            $query = str_replace('-- CONDICOES --', 'AND retorno_id = :retorno_id', $query);
        }

        $arrDetalhes = $this->executeQueryWithParam($query, $param)->fetchAll();

        return $arrDetalhes ? $arrDetalhes : [];
    }

    /**
     * @param $detalheId integer
     * @return array
     */
    public function atualizarSituacaoDetalheEmProcessamento($arrDetalhes = [])
    {
        $arrProcessamento = [
            self::STATUS_REJEITADO   => [],
            self::STATUS_PROCESSANDO => [],
            self::STATUS_FINALIZADO  => [],
        ];

        $arrCodigosAceitar = array(
            \Cnab\Movimento::RETORNO_LIQUIDACAO,
            \Cnab\Movimento::RETORNO_LIQUIDACAO_APOS_BAIXA_OU_LIQUIDACAO_TITULO_NAO_REGISTRADO,
        );

        foreach ($arrDetalhes as $arrDetalhe) {
            if (!in_array($arrDetalhe['detalhe_codigo_movimento'], $arrCodigosAceitar)) {
                $arrProcessamento[self::STATUS_REJEITADO][$arrDetalhe['detalhe_id']] = $arrDetalhe;
            } elseif ($arrDetalhe['detalhe_valor_pago'] == 0) {
                $arrProcessamento[self::STATUS_FINALIZADO][$arrDetalhe['detalhe_id']] = $arrDetalhe;
            } else {
                $arrProcessamento[self::STATUS_PROCESSANDO][$arrDetalhe['detalhe_id']] = $arrDetalhe;
            }
        }

        foreach ($arrProcessamento as $status => $arrDetalhes) {
            $this->atualizarStatusDetalhe(array_keys($arrDetalhes), $status);
        }

        return $arrProcessamento[self::STATUS_PROCESSANDO];
    }

    public function atualizarLogDetalhe($detalheId, $log = '')
    {
        if (!$detalheId || !$log) {
            return false;
        }

        $dataAtual = (new \DateTime())->format('d/m/Y H:i:s');

        $log = "\n" . $dataAtual . ":" . $log;

        $query = "
            UPDATE financeiro__retorno_detalhe
            SET detalhe_log = trim(concat(detalhe_log,:detalhe_log))
            WHERE detalhe_id IN(:detalhe_id)";

        try {
            $param = ['detalhe_log' => $log, 'detalhe_id' => $detalheId];
            $this->executeQueryWithParam($query, $param);
        } catch (\Exception $e) {
            $this->setLastError("Erro ao atualizar o log do retorno");

            return false;
        }

        return true;
    }

    public function atualizarStatusDetalhe($detalheId, $status)
    {
        if (!$detalheId) {
            return false;
        }

        $query = "
        UPDATE financeiro__retorno_detalhe
        SET detalhe_status = :detalhe_status
        WHERE detalhe_id IN(:detalhe_id)";

        try {
            $this->executeQueryWithParam($query, ['detalhe_status' => $status, 'detalhe_id' => $detalheId]);
        } catch (\Exception $e) {
            $this->setLastError("Erro ao atualizar o log do retorno");

            return false;
        }

        return true;
    }

    public function getArrayStatus()
    {
        return array(self::STATUS_FINALIZADO, self::STATUS_PROCESSANDO, self::STATUS_PENDENTE, self::STATUS_REJEITADO);
    }

    public function pesquisaDetalhes($params)
    {
        $query = "
        SELECT *
        FROM financeiro__retorno_detalhe
        WHERE detalhe_status IN (:detalhe_status)";

        if ($params['retornoId']) {
            $query .= " AND retorno_id in (:retorno_id)";
        }

        $param = [
            'detalhe_status' => [
                self::STATUS_PENDENTE,
                self::STATUS_REJEITADO,
                self::STATUS_FINALIZADO,
                self::STATUS_PROCESSANDO
            ],
            'retorno_id'     => $params['retornoId'],
        ];

        $result = $this->executeQueryWithParam($query, $param)->fetchAll();

        return $result;
    }
}