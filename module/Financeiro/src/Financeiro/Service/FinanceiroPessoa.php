<?php

namespace Financeiro\Service;

use VersaSpine\Service\AbstractService;
use Zend\Form\Element\DateTime;

class FinanceiroPessoa extends AbstractService
{
    private $__lastError = null;

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Financeiro\Entity\BoletoConfConta');
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function pesquisaForJson($criterios)
    {
        // TODO: Implement pesquisaForJson() method.
    }

    public function valida($dados)
    {
        // TODO: Implement valida() method.
    }

    public function getDataTables($arrParam)
    {
        $serviceSisConfig     = new \Sistema\Service\SisConfig($this->getEm());
        $boletoRemessaAtivada = $serviceSisConfig->localizarChave('BOLETO_REMESSA_ATIVADA');

        if ($arrParam['filter']['naoTrazerDados']) {
            return array(
                "draw"            => 0,
                "recordsFiltered" => 0,
                "recordsTotal"    => 0,
                "data"            => [],
            );
        }

        $tipoTitulo  = $arrParam['filter']['tituloTipo'];
        $dataInicial = $this::formatDateAmericano($arrParam['filter']['vencTituloInicial']);
        $dataFinal   = $this::formatDateAmericano($arrParam['filter']['vencTituloFinal']);
        unset($arrParam['filter']['tituloTipo']);
        unset($arrParam['filter']['vencTituloInicial']);
        unset($arrParam['filter']['vencTituloFinal']);

        $serviceAcadPeriodoAluno = (new \Matricula\Service\AcadperiodoAluno($this->getEm()));

        $query = $serviceAcadPeriodoAluno->retornaConsultaAcademicaAluno($arrParam['filter']);
        $whery = [];

        $arrParam['titulosNotIn'] = implode(',', array_filter($arrParam['titulosNotIn']));
        $queryTitulos             = '
        INNER JOIN
        (SELECT pes_id,titulo_id,titulo_data_vencimento,titulo_valor,tipotitulo_nome,alunoper_id,alunocurso_id
        FROM financeiro__titulo INNER JOIN financeiro__titulo_tipo USING(tipotitulo_id)	INNER JOIN boleto USING(titulo_id)
        ' . ($boletoRemessaAtivada ? ' INNER JOIN financeiro__remessa_lote USING(bol_id) ' : '') . '
         -- WHERE --
         )financeiro__titulo ON acadperiodo__aluno.alunoper_id=financeiro__titulo.alunoper_id OR
          pessoa.pes_id=financeiro__titulo.pes_id OR acadgeral__aluno_curso.aluno_id=financeiro__titulo.alunocurso_id ';

        if ($arrParam['titulosNotIn']) {
            $whery[] = " financeiro__titulo.titulo_id NOT IN (" . $arrParam['titulosNotIn'] . ") ";
        }

        if ($dataInicial) {
            $whery[] = " titulo_data_vencimento>='" . $dataInicial . "' ";
        }
        if ($dataFinal) {
            $whery[] = " titulo_data_vencimento<='" . $dataFinal . "' ";
        }
        if ($tipoTitulo) {
            $whery[] = " tipotitulo_id IN ( " . $tipoTitulo . ") ";
        }

        $whery        = !empty($whery) ? " WHERE " . implode(" AND ", $whery) : '';
        $camposExtras = ',financeiro__titulo.titulo_id,financeiro__titulo.titulo_data_vencimento,financeiro__titulo.titulo_valor,tipotitulo_nome ';

        $queryTitulos = str_replace('-- WHERE --', $whery, $queryTitulos);
        $query        = str_replace('-- OUTROS CAMPOS --', $camposExtras, $query);
        $query        = str_replace('-- JOIN --', $queryTitulos, $query);

        $result = $this->paginationDataTablesAjax($query, $arrParam, null, false);

        return $result;
    }
}
?>