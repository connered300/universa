<?php

namespace Financeiro\Service;

use VersaSpine\Service\AbstractService;

class FinanceiroDesconto extends AbstractService
{
    const DESCONTO_STATUS_ATIVO   = 'Ativo';
    const DESCONTO_STATUS_INATIVO = 'Inativo';
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2DescontoStatus($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getDescontoStatus());
    }

    public static function getDescontoStatus()
    {
        return array(self::DESCONTO_STATUS_ATIVO, self::DESCONTO_STATUS_INATIVO);
    }

    /** @var \Sistema\Service\SisConfig */
    private $config = null;

    /**
     * @return \Sistema\Service\SisConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param \Sistema\Service\SisConfig $config
     * @return FinanceiroTitulo
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     * @param array                       $arrConfig
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, $arrConfig = array())
    {
        parent::__construct($em, 'Financeiro\Entity\FinanceiroDesconto');
        $this->setConfig(new \Sistema\Service\SisConfig($em, $arrConfig));
    }

    public function pesquisaForJson($params)
    {
        $sql        = '
        SELECT
            a.descontoId AS desconto_id,
            a.desctipoId AS desctipo_id,
            a.usuario AS usuario_id,
            a.descontoValor AS desconto_valor,
            a.descontoPercentual AS desconto_percentual,
            a.descontoDiaLimite AS desconto_dia_limite,
            a.descontoStatus AS desconto_status,
            a.aluno AS aluno_id
        FROM Financeiro\Entity\FinanceiroDesconto a
        WHERE';
        $desctipoId = false;
        $descontoId = false;

        if ($params['q']) {
            $desctipoId = $params['q'];
        } elseif ($params['query']) {
            $desctipoId = $params['query'];
        }

        if ($params['descontoId']) {
            $descontoId = $params['descontoId'];
        }

        $parameters = array('desctipoId' => "{$desctipoId}%");
        $sql .= ' a.desctipoId LIKE :desctipoId';

        if ($descontoId) {
            $parameters['descontoId'] = explode(',', $descontoId);
            $parameters['descontoId'] = $descontoId;
            $sql .= ' AND a.descontoId NOT IN(:descontoId)';
        }

        $sql .= " ORDER BY a.desctipoId";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceDescontoTipo   = new \Financeiro\Service\FinanceiroDescontoTipo($this->getEm());
        $serviceDescontoTitulo = new \Financeiro\Service\FinanceiroDescontoTitulo($this->getEm());
        $serviceAcessoPessoas  = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceAcadgeralAluno = new \Matricula\Service\AcadgeralAluno($this->getEm());

        $baixarTituloPorDesconto         = null;
        $reversaoDePagamentosPorDesconto = null;

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['descontoId']) {
                /** @var $objFinanceiroDesconto \Financeiro\Entity\FinanceiroDesconto */
                $objFinanceiroDesconto = $this->getRepository()->find($arrDados['descontoId']);

                if (!$objFinanceiroDesconto) {
                    $this->setLastError('Registro de desconto não existe!');

                    return false;
                }
            } else {
                $objFinanceiroDesconto = new \Financeiro\Entity\FinanceiroDesconto();
            }

            if ($arrDados['desctipo']) {
                /** @var $objFinanceiroDescontoTipo \Financeiro\Entity\FinanceiroDescontoTipo */
                $objFinanceiroDescontoTipo = $serviceDescontoTipo->getRepository()->find(
                    $arrDados['desctipo']
                );

                if (!$objFinanceiroDescontoTipo) {
                    $this->setLastError('Registro de tipo não existe!');

                    return false;
                }

                $objFinanceiroDesconto->setDesctipo($objFinanceiroDescontoTipo);
            } else {
                $objFinanceiroDesconto->setDesctipo(null);
            }

            if ($arrDados['aluno']) {
                /** @var $objAcadgeralAluno \Matricula\Entity\AcadgeralAluno */
                $objAcadgeralAluno = $serviceAcadgeralAluno->getRepository()->find($arrDados['aluno']);

                if (!$objAcadgeralAluno) {
                    $this->setLastError('Registro de aluno curso não existe!');

                    return false;
                }

                $objFinanceiroDesconto->setAlunocurso($objAcadgeralAluno);
            } else {
                $objFinanceiroDesconto->setAlunocurso(null);
            }

            /** @var $objAcessoPessoas \Acesso\Entity\AcessoPessoas */
            $objAcessoPessoas = $serviceAcessoPessoas->retornaUsuarioLogado($arrDados['usuario']);

            if (!$objAcessoPessoas) {
                $this->setLastError('Registro de pessoas não existe!');

                return false;
            }

            if ($arrDados['descontoValor']) {
                $arrDados['descontoValor'] = str_replace([".", ","], ["", "."], $arrDados['descontoValor']);
            }

            if ($arrDados['descontoPercentual']) {
                $arrDados['descontoPercentual'] = str_replace(",", ".", $arrDados['descontoPercentual']);
            }

            $objFinanceiroDesconto->setUsuario($objAcessoPessoas);
            $objFinanceiroDesconto->setDescontoValor($arrDados['descontoValor']);
            $objFinanceiroDesconto->setDescontoPercentual($arrDados['descontoPercentual']);
            $objFinanceiroDesconto->setDescontoStatus($arrDados['descontoStatus']);
            $objFinanceiroDesconto->setDescontoObservacao($arrDados['descontoObservacao']);

            $objTipo = $objFinanceiroDesconto->getDesctipo();

            if (
                $objTipo->getDesctipoPercmax() &&
                ($objFinanceiroDesconto->getDescontoPercentual() > $objTipo->getDesctipoPercmax())
            ) {
                $this->setLastError('O desconto concedido é maior que o permitido pelo tipo de desconto!');

                return false;
            }

            if (
                $objTipo->getDesctipoValormax() &&
                ($objFinanceiroDesconto->getDescontoValor() > $objTipo->getDesctipoValormax())
            ) {
                $this->setLastError('O desconto concedido é maior que o permitido pelo tipo de desconto!');

                return false;
            }

            $this->getEm()->persist($objFinanceiroDesconto);
            $this->getEm()->flush($objFinanceiroDesconto);

            if (!$serviceDescontoTitulo->salvarMultiplos($arrDados, $objFinanceiroDesconto)) {
                throw new \Exception($serviceDescontoTitulo->getLastError());
            }

            $this->getEm()->commit();

            if (!$this->baixaTitulosComDescontoTotalAPartirDeDesconto($objFinanceiroDesconto->getDescontoId())) {
                return false;
            }

            $arrDados['descontoId'] = $objFinanceiroDesconto->getDescontoId();

            $this->setLastError(
                $this->getLastError() . ' \n ' . $serviceDescontoTitulo->getLastError(
                ) ? $serviceDescontoTitulo->getLastError() : ''
            );

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de desconto!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        $serviceFinanceiroDescontoTitulo = new\Financeiro\Service\FinanceiroDescontoTitulo($this->getEm());
        $serviceFinanceiroDesconto       = new \Financeiro\Service\FinanceiroDesconto($this->getEm());

        if ($arrParam['descontoId']) {
            /** @var \Financeiro\Entity\FinanceiroDesconto $objFinanceiroDescontoValida */
            $objFinanceiroDescontoValida = $serviceFinanceiroDesconto->getRepository()->find($arrParam['descontoId']);

            if (
                $objFinanceiroDescontoValida->getDescontoStatus() == $serviceFinanceiroDesconto::DESCONTO_STATUS_INATIVO
            ) {
                $errors[] = 'Desconto inativo não pode ser alterado!';
            }
        }

        $validaDescontoAcumulativo = $serviceFinanceiroDescontoTitulo
            ->validaDescontoAcumulativo($arrParam, $arrParam['desctipo']);

        if ($validaDescontoAcumulativo) {
            $errors = $validaDescontoAcumulativo;
        }

        if (!$arrParam['aluno']) {
            $errors[] = 'Por favor preencha o campo "aluno"!';
        }

        if (!$arrParam['desctipo']) {
            $errors[] = 'Por favor preencha o campo "tipo de desconto"!';
        }

        if (!$arrParam['descontoStatus']) {
            $errors[] = 'Por favor preencha o campo "status"!';
        }

        if (!in_array($arrParam['descontoStatus'], self::getDescontoStatus())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "status"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "
        SELECT
        format(desconto.desconto_percentual,2,'de_DE') AS desconto_percentual_formatado,
        (
        SELECT format((sum(coalesce(fdtt.desconto_aplicacao_valor,0))),2,'de_DE')
        FROM view__financeiro_descontos_titulos fdtt
        WHERE  fdtt.desconto_id = desconto.desconto_id
         ) AS desconto_valor_formatado,
                
        pessoa.pes_id,
        pessoa.pes_nome,
        desconto.*,
        tipo.desctipo_descricao,
        acesso.login AS usuario_login,
        (
            SELECT count(*)
            FROM financeiro__desconto_titulo fdt
            WHERE fdt.desconto_id=desconto.desconto_id
        ) AS quantidade_titulos_vinculados

        FROM financeiro__desconto desconto
        INNER JOIN financeiro__desconto_tipo tipo USING (desctipo_id)
        INNER JOIN acadgeral__aluno aluno USING (aluno_id)
        INNER JOIN pessoa USING (pes_id)
        LEFT JOIN acesso_pessoas acesso ON desconto.usuario_id=acesso.id
        ";

        $desconto = 0;

        if ($data['filter']['painel'] != "false" && !$data['filter']['alunoId']) {
            return array(
                "draw"            => 0,
                "recordsFiltered" => 0,
                "recordsTotal"    => 0,
                "data"            => [],
            );
        }

        if (isset($data['filter']['alunoId'])) {
            if ($data['filter']['alunoId'][0] == '-') {
                $query .= ' WHERE aluno_id in(" ")';
            } else {
                $alunoId     = implode(',', $data['filter']['alunoId']);
                $arrAlunoPer = $data['filter']['alunoPer'];

                $desconto = $this->percentualMedioDeDescontoDoAluno($alunoId, $arrAlunoPer);

                $query .= ' WHERE aluno_id in(' . $alunoId . ')';
            }
        }

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        $result['descontoPercentual'] = $desconto;

        return $result;
    }

    /**
     * @param int $alunoId
     * @return int
     */
    public function percentualMedioDeDescontoDoAluno($alunoId, $arrAlunoPerId = null)
    {
        $serviceAlunoPeriodo = new \Matricula\Service\AcadperiodoAluno($this->getEm());

        $query = "
         SELECT
            aluno_id,
            round(AVG(desconto__percentual_calc), 2) AS percentualDesconto
        FROM view__financeiro_descontos_titulos_atualizada
        WHERE aluno_id = :alunoId";

        $objAlunoPer             = null;
        $param['alunoId']        = $alunoId;
        $calcularDescontoPeriodo =
            $this->getConfig()->localizarChave('PORCETAGEM_DESCONTO_PAINEL_FINANCEIRO_ALUNOPERID',false);


        if (isset($arrAlunoPerId['alunoper'])) {
            /** @var $objAlunoPer \Matricula\Entity\AcadperiodoAluno */
            $objAlunoPer =
                $serviceAlunoPeriodo->getRepository()->findOneBy(['alunoperId' => $arrAlunoPerId['alunoper']]);
        }

        if ($objAlunoPer  AND $calcularDescontoPeriodo) {
            $tiposTituloCalcDesc = $this->getConfig()->localizarChave(
                "FINANCEIRO_TIPOS_TITULO_CALCULO_PORCENTAGEM_DESCONTO"
            );

            $queryAdd = "";

            if ($tiposTituloCalcDesc) {
                $queryAdd = " AND tipotitulo_id in ({$tiposTituloCalcDesc})";
            }

            $query = "
             SELECT SUM(IF(desconto_valor IS NOT NULL,desconto_valor,titulo_valor*(desconto_percentual/100))) valorDesconto ,  
              ((SELECT SUM(financeiro__titulo.titulo_valor ) FROM financeiro__titulo WHERE alunoper_id=:alunoPerId
              OR (titulo_data_processamento BETWEEN per_data_inicio AND per_data_fim AND alunocurso_id=:alunocursoId AND alunoper_id IS NULL))) AS valorTitulosVinculadoPeriodo
                        
              FROM financeiro__desconto_titulo
              INNER JOIN financeiro__desconto USING(desconto_id)
              INNER JOIN financeiro__titulo USING(titulo_id)
              JOIN (
                SELECT per_data_inicio,per_data_fim FROM acadperiodo__aluno
                INNER JOIN acadperiodo__turma USING(turma_id)
                INNER JOIN acadperiodo__letivo USING(per_id)
                WHERE alunoper_id=:alunoPerId
                  ) AS dtp
                  
              WHERE titulo_id IN 
              (SELECT titulo_id FROM financeiro__titulo WHERE alunoper_id=:alunoPerId 
               OR (titulo_data_processamento BETWEEN per_data_inicio AND per_data_fim AND alunocurso_id=:alunocursoId AND alunoper_id IS NULL))
               
               AND financeiro__desconto.desconto_status='Ativo' AND desconto_titulo_situacao='Deferido' 
               AND financeiro__titulo.titulo_estado IN ('Pago','Aberto','Pagamento Múltiplo') {$queryAdd} ";

            $param['alunoPerId']   = $arrAlunoPerId['alunoper'];
            $param['alunocursoId'] = $objAlunoPer->getAlunocurso()->getAlunocursoId();
        }

        $result = $this->executeQueryWithParam($query, $param)->fetch();

        if (!isset($result['percentualDesconto']) && isset($result['valorDesconto']) && isset($result['valorTitulosVinculadoPeriodo'])) {
            $result['percentualDesconto'] = (($result['valorDesconto'] * 100) / $result['valorTitulosVinculadoPeriodo']);
            $result['percentualDesconto'] = round($result['percentualDesconto'], 2);
        }

        return (float)$result['percentualDesconto'];
    }

    public function getArray($descontoId)
    {
        /** @var $objFinanceiroDesconto \Financeiro\Entity\FinanceiroDesconto */
        $objFinanceiroDesconto           = $this->getRepository()->find($descontoId);
        $serviceFinanceiroDescontoTipo   = new \Financeiro\Service\FinanceiroDescontoTipo($this->getEm());
        $serviceAcessoPessoas            = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceAcadgeralAluno           = new \Matricula\Service\AcadgeralAluno($this->getEm());
        $serviceFinanceiroDescontoTitulo = new \Financeiro\Service\FinanceiroDescontoTitulo($this->getEm());

        try {
            $arrDados = $objFinanceiroDesconto->toArray();

            if ($arrDados['desctipo']) {
                $arrFinanceiroDescontoTipo = $serviceFinanceiroDescontoTipo->getArrSelect2(
                    ['id' => $arrDados['desctipo']]
                );
                $arrDados['desctipo']      = $arrFinanceiroDescontoTipo ? $arrFinanceiroDescontoTipo[0] : null;
            }

            if ($arrDados['usuario']) {
                $arrAcessoPessoas    = $serviceAcessoPessoas->getArrSelect2(['id' => $arrDados['usuario']]);
                $arrDados['usuario'] = $arrAcessoPessoas ? $arrAcessoPessoas[0] : null;
            }

            if ($arrDados['aluno']) {
                $arrAcadgeralAluno = $serviceAcadgeralAluno->getArrSelect2(['id' => $arrDados['aluno']]);
                $arrDados['aluno'] = $arrAcadgeralAluno ? $arrAcadgeralAluno[0] : null;
            }

            $arrDados['titulosVinculados'] = $serviceFinanceiroDescontoTitulo->getArrayTitulosVinculados(
                ['desconto' => $objFinanceiroDesconto->getDescontoId()]
            );
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceFinanceiroDescontoTipo = new \Financeiro\Service\FinanceiroDescontoTipo($this->getEm());
        $serviceAcessoPessoas          = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceAcadgeralAluno         = new \Matricula\Service\AcadgeralAluno($this->getEm());

        if (is_array($arrDados['desctipo']) && !$arrDados['desctipo']['text']) {
            $arrDados['desctipo'] = $arrDados['desctipo']['desctipoId'];
        }

        if (is_array($arrDados['usuario']) && !$arrDados['usuario']['text']) {
            $arrDados['usuario'] = $arrDados['usuario']['id'];
        }

        if (is_array($arrDados['aluno']) && !$arrDados['aluno']['text']) {
            $arrDados['aluno'] = $arrDados['aluno']['alunoId'];
        }

        if ($arrDados['desctipo'] && is_string($arrDados['desctipo'])) {
            $arrDados['desctipo'] = $serviceFinanceiroDescontoTipo->getArrSelect2(['id' => $arrDados['desctipo']]);
            $arrDados['desctipo'] = $arrDados['desctipo'] ? $arrDados['desctipo'][0] : null;
        }

        if ($arrDados['usuario'] && is_string($arrDados['usuario'])) {
            $arrDados['usuario'] = $serviceAcessoPessoas->getArrSelect2(['id' => $arrDados['usuario']]);
            $arrDados['usuario'] = $arrDados['usuario'] ? $arrDados['usuario'][0] : null;
        }

        if ($arrDados['aluno'] && is_string($arrDados['aluno'])) {
            $arrDados['aluno'] = $serviceAcadgeralAluno->getArrSelect2(['id' => $arrDados['aluno']]);
            $arrDados['aluno'] = $arrDados['aluno'] ? $arrDados['aluno'][0] : null;
        }

        if (is_string($arrDados['titulosVinculados'])) {
            $arrDados['titulosVinculados'] = json_decode($arrDados['titulosVinculados'], true);
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        if ($params['id']) {
            $arrEntities = $this->getRepository()->findBy(array('descontoId' => $params['id']));
        } else {
            $arrEntities = $this->getRepository()->findAll();
        }

        $arrEntitiesArr = array();
        /* @var $objEntity \Financeiro\Entity\FinanceiroDesconto */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[] = array(
                $params['key']   => $objEntity->getDescontoId(),
                $params['value'] => (
                    $objEntity->getAlunocurso()->getPes()->getPes()->getPesNome() . ' / ' .
                    $objEntity->getDesctipo()->getDesctipoDescricao()
                )
            );
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['descontoId']) {
            $this->setLastError('Para remover um registro de desconto é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objFinanceiroDesconto \Financeiro\Entity\FinanceiroDesconto */
            $objFinanceiroDesconto = $this->getRepository()->find($param['descontoId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objFinanceiroDesconto);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de desconto.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $view->setVariable("arrDescontoStatus", $this->getArrSelect2DescontoStatus());
    }

    /**
     * @param $descontoId
     * @return boolean
     */
    public function baixaTitulosComDescontoTotalAPartirDeDesconto($descontoId)
    {
        $serviceFinanceiroTitulo         = new \Financeiro\Service\FinanceiroTitulo($this->getEm());
        $servideFinanceiroDescontoTitulo = new \Financeiro\Service\FinanceiroDescontoTitulo($this->getEm());

        $arrDados = $this->getArray($descontoId);

        foreach ($arrDados['titulosVinculados'] as $arrTitulo) {
            $arrInformacoesTitulo = $serviceFinanceiroTitulo->retornaInformacoesTitulo($arrTitulo['tituloId']);

            $tituloAberto = $arrInformacoesTitulo['tituloEstado'] == $serviceFinanceiroTitulo::TITULO_ESTADO_ABERTO;

            if (
                $arrTitulo['descontoTituloSituacao'] == $servideFinanceiroDescontoTitulo::DESCONTO_TITULO_SITUACAO_INDEFERIDO ||
                $arrDados['descontoStatus'] == self::DESCONTO_STATUS_INATIVO
            ) {
                continue;
            }

            if ($arrInformacoesTitulo['tituloValorFinal'] <= 0 && $tituloAberto) {
                $observacao = (
                    'Liquidação de título por desconto' .
                    ' / Código do desconto: ' . $arrDados['descontoId'] .
                    ' / Valor Total de descontos: ' . $arrInformacoesTitulo['descontos']
                );

                $efetuouBaixa = $serviceFinanceiroTitulo->registrarPagamentoTitulo(
                    $arrInformacoesTitulo['tituloId'],
                    new \DateTime(),
                    0,
                    [],
                    array('tituloObservacoes' => $observacao)
                );

                if (!$efetuouBaixa) {
                    $this->setLastError($serviceFinanceiroTitulo->getLastError());

                    return false;
                }

                $objTitulo = $serviceFinanceiroTitulo->getRepository()->find($arrInformacoesTitulo['tituloId']);

                if (!$serviceFinanceiroTitulo->alteraSituacaoAlunoPorTituloId($objTitulo)) {
                    $this->setLastError($serviceFinanceiroTitulo->getLastError());

                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @param $param
     * @return bool|array
     */
    public function retornarInformacoesDeDescontosAtivos($param)
    {
        if (!$param) {
            $this->setLastError("É necessário informar dados para retornar as informações de desconto!");

            return false;
        }

        $filter = [];

        $sql = 'SELECT * FROM view__financeiro_descontos_titulos_atualizada WHERE 1';

        if ($param['tituloId']) {
            $sql .= " AND titulo_id=:tituloId ";
            $filter = ['tituloId' => $param['tituloId']];
        }

        if ($param['descontoId']) {
            $sql .= ' AND desconto_id not in (:descontoId)';
            $filter['descontoId'] = $param['descontoId'];
        }

        $arrResult = $this->executeQueryWithParam($sql, $filter)->fetchAll();

        return $arrResult;
    }

    public function retornaPercentualDescontoFiesAlunoPeriodo($objAlunoPer)
    {
        $servicePeriodo                  = new \Matricula\Service\AcadperiodoLetivo($this->getEm());
        $serviceFinanceiroDescontoTitulo = new \Financeiro\Service\FinanceiroDescontoTitulo($this->getEm());
        $serviceAlunoPeriodo             = new \Matricula\Service\AcadperiodoAluno($this->getEm());

        $objAlunoPer = $serviceAlunoPeriodo->getRepository()->findOneBy(['alunoperId' => $objAlunoPer]);

        /** @var $objAlunoPer \Matricula\Entity\AcadperiodoAluno */
        if (!$objAlunoPer) {
            $this->setLastError("Aluno não localizado!");

            return false;
        }

        if (!$pesId = $objAlunoPer->getAlunocurso()->getAluno()->getPes()->getPes()->getPesId()) {
            $this->setLastError("Pessoa não localizada!");

            return false;
        }

        /** @var \Matricula\Entity\AcadperiodoLetivo $objUltimoPeriodo */
        $objUltimoPeriodo = $servicePeriodo->retornaUltimoPeriodoAluno(
            $objAlunoPer->getAlunocurso()->getAlunocursoId(),
            true
        );

        if (!$objUltimoPeriodo) {
            $this->setLastError("Nenhum período localizado!");

            return false;
        }

        $dataPeriodoInicial = $objUltimoPeriodo->getPerDataInicio()->format('Y-m-d');
        $dataPeriodoFinal   = $objUltimoPeriodo->getPerDataFim()->format('Y-m-d');

        $sql = "
        SELECT
            financeiro__desconto.alunoper_id,
            financeiro__desconto.desconto_percentual,
            financeiro__desconto.desconto_valor,
            financeiro__desconto.aluno_id,
            financeiro__titulo.alunocurso_id,
            financeiro__titulo.titulo_id,
            financeiro__titulo.titulo_valor,
            financeiro__titulo.titulo_data_processamento,
           
            FORMAT(((SUM(
            IF(desconto_valor IS NOT NULL,desconto_valor ,
            ((desconto_percentual/100)*titulo_valor))            
            )*100)/(SUM(financeiro__titulo.titulo_valor))),2,'de_DE')AS percentual__desconto_geral
            
            FROM financeiro__desconto_titulo
            INNER JOIN financeiro__desconto USING(desconto_id)
            INNER JOIN financeiro__titulo USING(titulo_id)
            WHERE  financeiro__desconto.desctipo_id IN 
              (SELECT desctipo_id FROM financeiro__desconto_tipo WHERE desctipo_descricao LIKE '%FIES%') AND
              financeiro__desconto.desconto_status=:desconto_status AND
              financeiro__desconto_titulo.desconto_titulo_situacao=:situacao AND
              pes_id=:pesId AND
              (
                financeiro__desconto.alunoper_id OR
                financeiro__titulo.titulo_data_processamento BETWEEN :data1 AND :data2
              ) 
            ";

        $arrParam = array(
            'desconto_status' => self::DESCONTO_STATUS_ATIVO,
            'situacao'        => $serviceFinanceiroDescontoTitulo::DESCONTO_TITULO_SITUACAO_DEFERIDO,
            'pesId'           => $pesId,
            'data1'           => $dataPeriodoInicial,
            'data2'           => $dataPeriodoFinal,
        );

        $result = $this->executeQueryWithParam($sql, $arrParam)->fetch();

        $result['perNome'] = $objUltimoPeriodo->getPerNome();
        $result['perId']   = $objUltimoPeriodo->getPerId();

        if (!$result['aluno_id']) {
            $result = null;
        }

        return $result;
    }
}
?>