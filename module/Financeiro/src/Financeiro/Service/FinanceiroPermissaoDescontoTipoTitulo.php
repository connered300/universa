<?php

namespace Financeiro\Service;

use VersaSpine\Service\AbstractService;

/**
 * Class FinanceiroPermissaoDescontoTipoTitulo
 * @package Financeiro\Service
 */
class FinanceiroPermissaoDescontoTipoTitulo extends AbstractService
{
    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Financeiro\Entity\FinanceiroPermissaoDescontoTipoTitulo');
    }

    /**
     * @param $tipotituloId
     * @return bool|\Financeiro\Entity\FinanceiroDescontoTipo
     */
    public function pesquisaTipoDescontoPorTipoTitulo($tipotituloId)
    {
        /** @var $objFinanceiroPermissaoDescontoTipoTitulo \Financeiro\Entity\FinanceiroPermissaoDescontoTipoTitulo */
        $objFinanceiroPermissaoDescontoTipoTitulo = $this->getRepository()->findOneBy(['tipotitulo' => $tipotituloId]);

        if ($objFinanceiroPermissaoDescontoTipoTitulo) {
            return $objFinanceiroPermissaoDescontoTipoTitulo->getDesctipo();
        }

        return false;
    }

    /**
     * @param $desctipoId
     * @return array
     */
    public function retornaTipoTituloPeloTipoDesconto($desctipoId)
    {
        $arrFinanceiroTituloTipos                    = array();
        $arrObjFinanceiroPermissaoDescontoTipoTitulo = $this->pesquisaPeloTipoDesconto($desctipoId, false);

        /** @var $objFinanceiroPermissaoDescontoTipoTitulo \Financeiro\Entity\FinanceiroPermissaoDescontoTipoTitulo */
        foreach ($arrObjFinanceiroPermissaoDescontoTipoTitulo as $objFinanceiroPermissaoDescontoTipoTitulo) {
            $arrFinanceiroTituloTipos[] = $objFinanceiroPermissaoDescontoTipoTitulo->getTipotitulo()->getTipotituloId();
        }

        return $arrFinanceiroTituloTipos;
    }

    /**
     * @param           $desctipoId
     * @param bool|true $toArray
     * @return array
     */
    public function pesquisaPeloTipoDesconto($desctipoId, $toArray = true)
    {
        $arrResult = $this->getRepository()->findBy(array('desctipo' => $desctipoId));

        if ($toArray) {
            $arrRetorno = array();

            /** @var $objFinanceiroPermissaoDescontoTipoTitulo \Financeiro\Entity\FinanceiroPermissaoDescontoTipoTitulo */
            foreach ($arrResult as $objFinanceiroPermissaoDescontoTipoTitulo) {
                $arrRetorno[] = $objFinanceiroPermissaoDescontoTipoTitulo->toArray();
            }

            return $arrRetorno;
        }

        return $arrResult;
    }

    /**
     * @param                                           $arrTipoTitulo
     * @param \Financeiro\Entity\FinanceiroDescontoTipo $objDescontoTipo
     * @return bool
     */
    public function salvarArray(
        $arrTipoTitulo,
        \Financeiro\Entity\FinanceiroDescontoTipo $objDescontoTipo
    )
    {
        $objEntityManager = $this->getEm();
        $arrExcluir       = array();
        $arrEditar        = array();

        if ($arrTipoTitulo && !is_array($arrTipoTitulo)) {
            $arrTipoTitulo = explode(',', $arrTipoTitulo);
        }

        $arrPermissaoDescontoTipoTitulosDB = $this->pesquisaPeloTipoDesconto(
            $objDescontoTipo->getDesctipoId(),
            false
        );
        /** @var $objFinanceiroPermissaoDescontoTipoTituloDB \Financeiro\Entity\FinanceiroPermissaoDescontoTipoTitulo */
        foreach ($arrPermissaoDescontoTipoTitulosDB as $objFinanceiroPermissaoDescontoTipoTituloDB) {
            $encontrado   = false;
            $tipotituloId = $objFinanceiroPermissaoDescontoTipoTituloDB->getTipotitulo()->getTipotituloId();

            foreach ($arrTipoTitulo as $tipotitulo) {
                if ($tipotitulo == $tipotituloId) {
                    $encontrado               = true;
                    $arrEditar[$tipotituloId] = $objFinanceiroPermissaoDescontoTipoTituloDB;
                    break;
                }
            }

            if (!$encontrado) {
                $arrExcluir[$tipotituloId] = $objFinanceiroPermissaoDescontoTipoTituloDB;
            }
        }

        if ($arrExcluir) {
            foreach ($arrExcluir as $objReg) {
                $objEntityManager->remove($objReg);
                $objEntityManager->flush();
            }
        }

        foreach ($arrTipoTitulo as $tipotitulo) {
            $serviceFinanceiroTituloTipo = new \Financeiro\Service\FinanceiroTituloTipo($objEntityManager);
            /** @var \Financeiro\Entity\FinanceiroTituloTipo $objFinanceiroTituloTipo */
            $objFinanceiroTituloTipo = $serviceFinanceiroTituloTipo->getRepository()->find($tipotitulo);

            if (!isset($arrEditar[$tipotitulo])) {
                $objPermissaoDescontoTipoTitulo = new \Financeiro\Entity\FinanceiroPermissaoDescontoTipoTitulo();

                $objPermissaoDescontoTipoTitulo->setDesctipo($objDescontoTipo);
                $objPermissaoDescontoTipoTitulo->setTipotitulo($objFinanceiroTituloTipo);

                $objEntityManager->persist($objPermissaoDescontoTipoTitulo);
                $objEntityManager->flush($objPermissaoDescontoTipoTitulo);
            }
        }

        return true;
    }

    /**
     * @param $desctipoId
     * @return array
     */
    public function retornaArrayPeloTipoDesconto($desctipoId)
    {
        $arrObjFinanceiroPermissaoDescontoTipoTitulo = $this->pesquisaPeloTipoDesconto($desctipoId, false);
        $arrFinanceiroPermissaoDescontoTipoTitulo    = array();

        /** @var $objFinanceiroPermissaoDescontoTipoTitulo \Financeiro\Entity\FinanceiroPermissaoDescontoTipoTitulo */
        foreach ($arrObjFinanceiroPermissaoDescontoTipoTitulo as $objFinanceiroPermissaoDescontoTipoTitulo) {
            $arrItem = array(
                'id'   => $objFinanceiroPermissaoDescontoTipoTitulo->getTipotitulo()->getTipotituloId(),
                'text' => $objFinanceiroPermissaoDescontoTipoTitulo->getTipotitulo()->getTipotituloNome()
            );

            $arrFinanceiroPermissaoDescontoTipoTitulo[] = $arrItem;
        }

        return $arrFinanceiroPermissaoDescontoTipoTitulo;
    }

    /**
     * @param $dados
     * @return null;
     */
    protected function valida($dados)
    {
        return null;
    }

    /**
     * @param $criterios
     */
    protected function pesquisaForJson($criterios)
    {
    }
}

?>
