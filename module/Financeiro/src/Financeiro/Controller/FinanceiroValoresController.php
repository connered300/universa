<?php

namespace Financeiro\Controller;

use VersaSpine\Controller\AbstractCoreController;

class FinanceiroValoresController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request                  = $this->getRequest();
        $paramsGet                = $request->getQuery()->toArray();
        $paramsPost               = $request->getPost()->toArray();
        $param                    = array_merge($paramsGet, $paramsPost);
        $serviceFinanceiroValores = new \Financeiro\Service\FinanceiroValores($this->getEntityManager());

        if ($param['titulotipoId']) {
            $arrDados = $serviceFinanceiroValores->buscaValoresPeriodoConfig(
                $param['titulotipoId'],
                $param['cursocampusId'],
                $param['periodoLetivo']
            );

            $arrDados = $arrDados ? $arrDados : array();

            if (!$arrDados) {
                $arrDados['erro'] = $serviceFinanceiroValores->getLastError();
            }
        } elseif (!$param) {
            $arrDados = $serviceFinanceiroValores->pesquisaForJson();
        }

        return new \Zend\View\Model\JsonModel($arrDados ? $arrDados : array());
    }

    public function indexAction()
    {
        $serviceFinanceiroValores = new \Financeiro\Service\FinanceiroValores($this->getEntityManager());
        $serviceFinanceiroValores->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceFinanceiroValores = new \Financeiro\Service\FinanceiroValores($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceFinanceiroValores->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function editAction()
    {
        $valoresId = $this->params()->fromRoute("id", 0);

        if (!$valoresId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($valoresId);
    }

    public function addAction($valoresId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados                 = array();
        $serviceFinanceiroValores = new \Financeiro\Service\FinanceiroValores($this->getEntityManager());

        if ($valoresId) {
            $arrDados = $serviceFinanceiroValores->getArray($valoresId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceFinanceiroValores->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de valores salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceFinanceiroValores->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceFinanceiroValores->getLastError());
                }
            }
        }

        $serviceFinanceiroValores->formataDadosPost($arrDados);
        $serviceFinanceiroValores->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceFinanceiroValores = new \Financeiro\Service\FinanceiroValores($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceFinanceiroValores->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceFinanceiroValores->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}
?>