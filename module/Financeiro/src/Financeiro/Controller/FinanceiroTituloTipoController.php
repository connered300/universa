<?php

namespace Financeiro\Controller;

use VersaSpine\Controller\AbstractCoreController;

class FinanceiroTituloTipoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request    = $this->getRequest();
        $paramsGet  = $request->getQuery()->toArray();
        $paramsPost = $request->getPost()->toArray();
        $param      = array_merge($paramsGet, $paramsPost);

        $serviceTituloTipo = new \Financeiro\Service\FinanceiroTituloTipo($this->getEntityManager());

        if ($param['emissaoMatricula']) {
            $todosOsMeiosDePagamento  = $param['todosOsMeiosDePagamento'] ? true : false;

            $arrDados = $serviceTituloTipo->retornoTiposDeTituloParaEmissaoNaMatricula($param, $todosOsMeiosDePagamento);
        } else {
            $arrDados = $serviceTituloTipo->pesquisaForJson(array_merge($paramsGet, $paramsPost));
        }

        return new \Zend\View\Model\JsonModel($arrDados ? $arrDados : array());
    }

    public function indexAction()
    {
        $serviceFinanceiroTituloTipo = new \Financeiro\Service\FinanceiroTituloTipo($this->getEntityManager());
        $serviceFinanceiroTituloTipo->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceFinanceiroTituloTipo = new \Financeiro\Service\FinanceiroTituloTipo($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceFinanceiroTituloTipo->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function editAction()
    {
        $tipotituloId = $this->params()->fromRoute("id", 0);

        if (!$tipotituloId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($tipotituloId);
    }

    public function addAction($tipotituloId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados                    = array();
        $serviceFinanceiroTituloTipo = new \Financeiro\Service\FinanceiroTituloTipo($this->getEntityManager());

        if ($tipotituloId) {
            $arrDados = $serviceFinanceiroTituloTipo->getArray($tipotituloId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceFinanceiroTituloTipo->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de tipo de título salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceFinanceiroTituloTipo->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceFinanceiroTituloTipo->getLastError());
                }
            }
        }

        $serviceFinanceiroTituloTipo->formataDadosPost($arrDados);
        $serviceFinanceiroTituloTipo->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceFinanceiroTituloTipo = new \Financeiro\Service\FinanceiroTituloTipo($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceFinanceiroTituloTipo->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceFinanceiroTituloTipo->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }

    public function alteraPrioridadeAction(){
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceFinanceiroTituloTipo = new \Financeiro\Service\FinanceiroTituloTipo($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();

            $ok            = $serviceFinanceiroTituloTipo->alteraPrioridade($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceFinanceiroTituloTipo->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}
?>