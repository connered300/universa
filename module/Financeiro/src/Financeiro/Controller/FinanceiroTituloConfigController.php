<?php

namespace Financeiro\Controller;

use VersaSpine\Controller\AbstractCoreController;

class FinanceiroTituloConfigController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request                       = $this->getRequest();
        $params                        = array_merge($request->getQuery()->toArray(), $request->getPost()->toArray());
        $serviceFinanceiroTituloConfig = new \Financeiro\Service\FinanceiroTituloConfig($this->getEntityManager());

        $result = $serviceFinanceiroTituloConfig->pesquisaForJson($params);

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceFinanceiroTituloConfig = new \Financeiro\Service\FinanceiroTituloConfig($this->getEntityManager());
        $serviceFinanceiroTituloConfig->setarDependenciasView($this->getView());
        $this->getView()->setVariable(
            'constDescontoIncentivoAcumulativo',
            $serviceFinanceiroTituloConfig->getArraySelect2TituloconfDescontoIncentivoAcumulativo()
        );

        $this->getView()
             ->setVariable(
                 "titutloconfInstrucaoAutomaticaSelect2",
                 $serviceFinanceiroTituloConfig->getArraySelect2InstrucaoBoletoAutomatica()
             );

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceFinanceiroTituloConfig = new \Financeiro\Service\FinanceiroTituloConfig($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceFinanceiroTituloConfig->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function editAction()
    {
        $tituloconfId = $this->params()->fromRoute("id", 0);

        if (!$tituloconfId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($tituloconfId);
    }

    public function addAction($tituloconfId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados                      = array();
        $serviceFinanceiroTituloConfig = new \Financeiro\Service\FinanceiroTituloConfig($this->getEntityManager());

        if ($tituloconfId) {
            $arrDados = $serviceFinanceiroTituloConfig->getArray($tituloconfId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceFinanceiroTituloConfig->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de configuração de título salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceFinanceiroTituloConfig->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceFinanceiroTituloConfig->getLastError());
                }
            }
        }

        $serviceFinanceiroTituloConfig->formataDadosPost($arrDados);
        $serviceFinanceiroTituloConfig->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceFinanceiroTituloConfig = new \Financeiro\Service\FinanceiroTituloConfig($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceFinanceiroTituloConfig->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceFinanceiroTituloConfig->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}

?>