<?php

namespace Financeiro\Controller;

use VersaSpine\Controller\AbstractCoreController;

class IndexController extends AbstractCoreController
{
    public function __construct()
    {
    }

    public function indexAction()
    {
        return $this->getView();
    }
}
