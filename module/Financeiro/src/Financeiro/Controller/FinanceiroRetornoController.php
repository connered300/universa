<?php

namespace Financeiro\Controller;

use VersaSpine\Controller\AbstractCoreController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class FinanceiroRetornoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function indexAction()
    {
        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $service = new \Financeiro\Service\FinanceiroRetorno($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $service->getDataForDatatables($dataPost);

            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function baixaOldAction()
    {
        @ini_set('memory_limit', '-1');

        $request  = $this->getRequest();
        $mensagem = '';

        if ($request->isPost()) {
            $dados = array_merge($request->getPost()->toArray(), $this->params()->fromFiles());

            if (!empty($dados['file-0']['name'])) {
                $srvRetorno = new \Financeiro\Service\FinanceiroRetorno(
                    $this->getEntityManager(),
                    $this->getServiceManager()->get('Config')
                );
                $arrFile    = array(
                    'name'     => $dados['file-0']['name'],
                    'type'     => $dados['file-0']['type'],
                    'tmp_name' => $dados['file-0']['tmp_name'],
                    'error'    => $dados['file-0']['error'],
                    'size'     => $dados['file-0']['size'],
                );

                if (!$feedBack = $srvRetorno->lerArquivoRetorno($arrFile)) {
                    $mensagem = $srvRetorno->getLastError();
                }

                $this->getJson()->setVariables(['feedback' => $feedBack, 'mensagem' => $mensagem]);

                return $this->getJson();
            }

            $this->getJson()->setVariable(
                [
                    'messages' =>
                        'Nenhum arquivo foi enviado! Favor selecionar um arquivo e tentar novamente.'
                ]
            );
        }

        return $this->getJson();
    }

    public function baixaAction()
    {
        @ini_set('memory_limit', '-1');

        $request        = $this->getRequest();
        $serviceRetorno = new \Financeiro\Service\FinanceiroRetorno($this->getEntityManager());

        if ($request->isPost()) {
            $dados = array_merge($request->getPost()->toArray(), $this->params()->fromFiles());

            if (!empty($dados['file-0']['name'])) {
                if (!$result = $serviceRetorno->processarArquivoRetorno($dados)) {
                    $this->getJson()->setVariables(
                        ['error' => true, 'message' => $serviceRetorno->getLastError()]
                    );

                    return $this->getJson();
                } else {
                    $this->getJson()->setVariables(
                        ['error' => false, 'message' => 'Arquivo de retorno enviado com sucesso!']
                    );
                }
            } else {
                $this->getJson()->setVariables(
                    ['error' => false, 'message' => 'Nenhum arquivo informado']
                );
            }
        }

        return $this->getJson();
    }

    public function processaArquivoAction()
    {
        $serviceFinanceiroRetorno = new \Financeiro\Service\FinanceiroRetorno($this->getEntityManager());

        if ($retornoId = $serviceFinanceiroRetorno->retornaDadosParaProcessamento()) {

            if ($result = $serviceFinanceiroRetorno->processarArquivo($retornoId)) {
                $this->getJson()->setVariables(
                    ['message' => 'Arquivo processado com sucesso!', 'resultado' => $result]
                );
            } else {
                $this->getJson()->setVariable('message', $serviceFinanceiroRetorno->getLastError());
            }
        }

        return $this->getJson();
    }

    public function processaDetalhesAction()
    {
        $serviceRetornoDetalhe = new \Financeiro\Service\FinanceiroRetornoDetalhe(
            $this->getEntityManager(), $this->getServiceManager()->get('config')
        );

        if (!$feedback = $serviceRetornoDetalhe->processaDetalhesRetorno()) {
            $this->getJson()->setVariable('message', $serviceRetornoDetalhe->getLastError());
        } else {
            $this->getJson()->setVariable('message', $serviceRetornoDetalhe->getFeedback());
        }

        return $this->getJson();
    }

    public function searchDetalhesAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $service = new \Financeiro\Service\FinanceiroRetornoDetalhe($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $service->getDataForDatatables($dataPost);

            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result['data']);
        }

        return $this->getJson();
    }
}