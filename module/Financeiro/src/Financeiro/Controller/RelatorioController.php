<?php

namespace Financeiro\Controller;

use DOMPDFModule\View\Model\PdfModel;
use VersaSpine\Controller\AbstractCoreController;

class RelatorioController extends AbstractCoreController

{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
    }

    public function indexAction()
    {
        $arrConfig            = $this->getServiceManager()->get('Config');
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEntityManager(), $arrConfig);

        $arrRelatorios = array(
            'sintetico'          => 'Relatório Sintético',
            'analitico'          => 'Relatório Analítico',
            'adimplentes'        => 'Relatório dos alunos Adimplentes',
            'comissao-agentes'   => 'Relatório de apuração de comissão de agentes',
            'fechamento-caixa'   => 'Fechamento de caixa',
            'comprovante-renda'  => 'Comprovante de Renda',
            'relatorio-desconto' => 'Relatório de Descontos',
            'boletos-aluno'      => 'Impressão de Boletos para Alunos'
        );

        $arrRelatoriosPermitidos = [];

        foreach ($arrRelatorios as $slug => $nome) {
            $permissao = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
                'Financeiro\Controller\Relatorio',
                $slug
            );

            if ($permissao) {
                $arrRelatoriosPermitidos[$slug] = $nome;
            }
        }

        $this->getView()->setVariable('relatorios', $arrRelatoriosPermitidos);

        return $this->getView();
    }

    public function sinteticoAction()
    {
        $request                 = $this->getRequest();
        $objServiceTitulo        = new \Financeiro\Service\FinanceiroTitulo($this->getEntityManager());
        $objServiceCampus        = new \Organizacao\Service\OrgCampus($this->getEntityManager());
        $objServiceTituloTipo    = new \Financeiro\Service\FinanceiroTituloTipo($this->getEntityManager());
        $objServiceAgente        = new \Organizacao\Service\OrgAgenteEducacional($this->getEntityManager());
        $objServiceAcesso        = new \Acesso\Service\AcessoPessoas($this->getEntityManager());
        $serviceAcadperiodoAluno = new \Matricula\Service\AcadperiodoAluno($this->getEntityManager());

        $arrRelatorios = [
            'financeiro_titulos'                => [
                'id'      => 'financeiro_titulos',
                'text'    => 'Todos os títulos',
                'filtros' => [],
                'ext'     => [
                    'pdf',//financeiro_titulos_pdf
                    'xlsx'//financeiro_titulos_xlsx
                ]
            ],
            'financeiro_titulos_agente'         => [
                'id'      => 'financeiro_titulos_agente',
                'text'    => 'Todos os títulos com Agente Educacional',
                'filtros' => [],
                'ext'     => [
                    'pdf',//financeiro_titulos_pdf
                    'xlsx'//financeiro_titulos_xlsx
                ]
            ],
            'financeiro_titulos_abertos'        => [
                'id'      => 'financeiro_titulos_abertos',
                'text'    => 'Títulos abertos',
                'filtros' => [
                    'meioDePagamento'           => false,
                    'tituloEstado'              => 'Aberto',
                    'tituloDataPagamentoInicio' => false,
                    'tituloDataPagamentoFim'    => false,
                    'tituloDataBaixaInicio'     => false,
                    'tituloDataBaixaFim'        => false,
                    'meioPag'                   => false
                ],
                'ext'     => 'xlsx'
            ],
            'financeiro_titulos_abertos_agente' => [
                'id'      => 'financeiro_titulos_abertos_agente',
                'text'    => 'Títulos abertos com Agente Educacional',
                'filtros' => [
                    'meioDePagamento'           => false,
                    'tituloEstado'              => 'Aberto',
                    'tituloDataPagamentoInicio' => false,
                    'tituloDataPagamentoFim'    => false,
                    'tituloDataBaixaInicio'     => false,
                    'tituloDataBaixaFim'        => false,
                    'meioPag'                   => false
                ],
                'ext'     => 'xlsx'
            ]
        ];

        /** @var \SnowReportsZf2\Service\SnowReportsZf2 $snow */
        $snow = $this->getServiceLocator()->get('SnowReportsZf2\Service\SnowReportsZf2');

        if ($request->isPost()) {
            $dataPost = $request->getPost()->toArray();

            $tipoRelatorio     = $dataPost['tipoRelatorio'] ? $dataPost['tipoRelatorio'] : '';
            $extensaoRelatorio = strtolower($dataPost['tipo'] ? $dataPost['tipo'] : '');

            if (!in_array($tipoRelatorio, array_keys($arrRelatorios))) {
                $tipoRelatorio = 'financeiro_titulos';
            }

            if (!in_array($extensaoRelatorio, ['pdf', 'xlsx'])) {
                $extensaoRelatorio = 'pdf';
            }

            foreach ($arrRelatorios[$tipoRelatorio]['filtros'] as $filtro => $valor) {
                $dataPost[$filtro] = $valor;
            }

            $nomeArquivo = $tipoRelatorio . '_' . $extensaoRelatorio . '.jrxml';

            $dadosinstituicao = $objServiceCampus->retornaDadosInstituicao();

            $where = '1 = 1';

            if ($dataPost['areaId']) {
                $where .= " AND area.area_id in({$dataPost['areaId']})";
            }

            if ($dataPost['campId']) {
                $where .= " AND oc.camp_id in({$dataPost['campId']})";
            }

            if ($dataPost['cursoId']) {
                $where .= " AND c.curso_id in({$dataPost['cursoId']})";
            }

            if ($dataPost['nivelEnsino']) {
                $where .= " AND c.nivel_id in({$dataPost['nivelEnsino']}) ";
            }

            if ($dataPost['unidadeId']) {
                $where .= " AND act.unidade_id in({$dataPost['unidadeId']}) ";
            }

            $tipoTituloInfo = '';

            if ($dataPost['tipotituloId']) {
                $tipoTituloInfo = $objServiceTituloTipo->getDescricao($dataPost['tipotituloId']);

                $where .= " AND t.tipotitulo_id in({$dataPost['tipotituloId']})";
            }

            $pesIdAgente = '';

            if ($dataPost['pesIdAgente']) {
                if ($dataPost['agentesOpcao'] == "excluirAgente") {
                    $pesIdAgente = "Exceto: " . $objServiceAgente->getDescricao($dataPost['pesIdAgente']);
                    $where .= " AND COALESCE (ac.pes_id_agente, a.pes_id_agenciador) NOT in({$dataPost['pesIdAgente']})";
                } else {
                    $pesIdAgente = $objServiceAgente->getDescricao($dataPost['pesIdAgente']);
                    $where .= " AND COALESCE (ac.pes_id_agente, a.pes_id_agenciador)  in({$dataPost['pesIdAgente']})";
                }
            }

            if ($dataPost['tituloDataVencimentoInicio']) {
                $dateVencInicio = $objServiceAcesso::formatDateAmericano($dataPost['tituloDataVencimentoInicio']);
                $dateVencInicio = str_replace('-', '', $dateVencInicio);
                $where .= " AND DATE(t.titulo_data_vencimento) >= DATE(" . $dateVencInicio . ")";
            }

            if ($dataPost['tituloDataVencimentoFim']) {
                $dateVencFim = $objServiceAcesso::formatDateAmericano($dataPost['tituloDataVencimentoFim']);
                $dateVencFim = str_replace('-', '', $dateVencFim);
                $where .= " AND DATE(t.titulo_data_vencimento) <=  " . $dateVencFim . "";
            }

            if ($dataPost['tituloDataPagamentoInicio']) {
                $datePagIni = $objServiceAcesso::formatDateAmericano($dataPost['tituloDataPagamentoInicio']);
                $datePagIni = str_replace('-', '', $datePagIni);
                $where .= " AND DATE(t.titulo_data_pagamento) >=" . $datePagIni . " ";
            }

            if ($dataPost['tituloDataPagamentoFim']) {
                $datePagFim = $objServiceAcesso::formatDateAmericano($dataPost['tituloDataPagamentoFim']);
                $datePagFim = str_replace('-', '', $datePagFim);
                $where .= " AND DATE(t.titulo_data_pagamento) <= " . $datePagFim . "";
            }

            if ($dataPost['tituloDataBaixaInicio']) {
                $dateBaixaIni = $objServiceAcesso::formatDateAmericano($dataPost['tituloDataBaixaInicio']);
                $dateBaixaIni = str_replace('-', '', $dateBaixaIni);
                $where .= " AND DATE(t.titulo_data_baixa) >= " . $dateBaixaIni . "";
            }

            if ($dataPost['tituloDataBaixaFim']) {
                $dateBaixaFim = $objServiceAcesso::formatDateAmericano($dataPost['tituloDataBaixaFim']);
                $dateBaixaFim = str_replace('-', '', $dateBaixaFim);
                $where .= " AND DATE(t.titulo_data_baixa) <=" . $dateBaixaFim . "";
            }

            if ($dataPost['alunoCadastroInicio']) {
                $dataCadastroInicio = $objServiceAcesso::formatDateAmericano($dataPost['alunoCadastroInicio']);
                $dataCadastroInicio = str_replace('-', '', $dataCadastroInicio);
                $where .= " AND DATE(alunocurso_data_cadastro) >=  " . $dataCadastroInicio . "";
            }

            if ($dataPost['alunoCadastroFim']) {
                $dataCadastroFim = $objServiceAcesso::formatDateAmericano($dataPost['alunoCadastroFim']);
                $dataCadastroFim = str_replace('-', '', $dataCadastroFim);
                $where .= " AND DATE(alunocurso_data_cadastro) <=  " . $dataCadastroFim . "";
            }

            if ($dataPost['tituloDataProcessamentoInicio']) {
                $dateEmissaoIni = $objServiceAcesso::formatDateAmericano($dataPost['tituloDataProcessamentoInicio']);
                $dateEmissaoIni = str_replace('-', '', $dateEmissaoIni);
                $where .= " AND DATE(DATE_FORMAT(t.titulo_data_processamento, " . '"' . "%Y-%m-%d" . '"' . ") ) >= " . $dateEmissaoIni . "";
            }

            if ($dataPost['tituloDataProcessamentoFim']) {
                $dateEmissaoFim = $objServiceAcesso::formatDateAmericano($dataPost['tituloDataProcessamentoFim']);
                $dateEmissaoFim = str_replace('-', '', $dateEmissaoFim);
                $where .= " AND DATE(DATE_FORMAT(t.titulo_data_processamento, " . '"' . "%Y-%m-%d" . '"' . ") ) <=" . $dateEmissaoFim . "";
            }

            $usuariosAutores = '';

            if ($dataPost['usuarioAutor']) {
                $usuariosAutores = explode(',', $dataPost['usuarioAutor']);

                $usuariosAutores = $objServiceAcesso->getDescricao(
                    count(explode(',', $dataPost['usuarioAutor'])) > 1 ? explode(
                        ',',
                        $dataPost['usuarioAutor']
                    ) : $usuariosAutores[0]
                );

                $where .= " AND t.usuario_autor in ({$dataPost['usuarioAutor']}) ";
            }

            $usuariosCadastroAluno = '';

            if ($dataPost['usuarioCadastroAluno']) {
                $usuariosCadastroAluno = explode(',', $dataPost['usuarioCadastroAluno']);

                $usuariosCadastroAluno = $objServiceAcesso->getDescricao(
                    count(explode(',', $dataPost['usuarioCadastroAluno'])) > 1 ? explode(
                        ',',
                        $dataPost['usuarioCadastroAluno']
                    ) : $usuariosCadastroAluno[0]
                );

                $where .= " AND COALESCE (ac.usuario_cadastro, a.usuario_cadastro) in ({$dataPost['usuarioCadastroAluno']}) ";
            }

            if ($dataPost['tituloParcela']) {
                if ($dataPost['parcela'] == "incluirParcela") {
                    $where .= "  AND titulo_parcela IN ( {$dataPost['tituloParcela']} )";
                } else {
                    $where .= "  AND titulo_parcela NOT IN ( {$dataPost['tituloParcela']} )";
                }
            }
            if ($dataPost['periodoLetivo']) {
                $where .= " AND act.per_id IN ({$dataPost['periodoLetivo']}) ";
            }
            if ($dataPost['situacaoalunoPeriodoLetivo']) {
                $where .= " AND aa.matsituacao_id={$dataPost['situacaoalunoPeriodoLetivo']} ";
            }
            if ($dataPost['alunosTurma']) {
                $where .= " AND act.turma_id IN ({$dataPost['alunosTurma']}) ";
            }

            $usuariosBaixa = '';

            if ($dataPost['usuarioBaixa']) {
                $usuariosBaixa = explode(',', $dataPost['usuarioBaixa']);
                $usuariosBaixa = $objServiceAcesso->getDescricao(
                    count(explode(',', $dataPost['usuarioBaixa'])) > 1 ? explode(
                        ',',
                        $dataPost['usuarioBaixa']
                    ) : $usuariosBaixa[0]
                );

                $where .= " AND t.usuario_baixa in ({$dataPost['usuarioBaixa']}) ";
            }

            if ($dataPost['tituloEstado']) {
                $situacaoArr = explode(",", $dataPost['tituloEstado']);
                $situacaoStr = '"' . implode('","', $situacaoArr) . '"';
                $where .= " AND t.titulo_estado IN(" . $situacaoStr . ")";
            };

            if ($dataPost['meioDePagamento'] && $dataPost['meioDePagamento'] != "false") {
                $pagamento = str_replace('"', '', $dataPost['meioDePagamento']);

                if ($dataPost['meioPag'] == "incluirMeioPag") {
                    $where .= " AND meio_pagamento_id IN (" . $pagamento . ")";
                } else {
                    $where .= " AND meio_pagamento_id NOT IN (" . $pagamento . ")";
                }
            }

            $nomeRelatorio = 'financeiro_titulos_' . (new \DateTime('now'))->format('YmdHi');

            $snow->setJrxmlPath(__DIR__ . '/../Reports/')
                ->setJrxmlFile($nomeArquivo)
                ->setAttrbutes(
                    [
                        'logo'                             => $dadosinstituicao['logo'],
                        'mantenedora'                      => $dadosinstituicao['mantenedora'],
                        'instituicao'                      => $dadosinstituicao['ies'],
                        'instituicaoinfo'                  => '',
                        'datainicio'                       => $dataPost['dataInicial'],
                        'datafim'                          => $dataPost['dataFinal'],
                        'titulo'                           => 'Relatório Financeiro Sintético',
                        'endereco'                         => $dadosinstituicao['endereco'],
                        'sistema'                          => 'Universa',
                        'camp'                             => '',
                        'cursos'                           => '',
                        'tipo_titulos'                     => '',
                        'usuario_autor'                    => '',
                        'usuario_baixa'                    => '',
                        'info_tipo_titulo'                 => $tipoTituloInfo,
                        'info_agente'                      => $pesIdAgente,
                        'info_usuarios_autores'            => $usuariosAutores,
                        'info_usuarios_cadastro_aluno'     => $usuariosCadastroAluno,
                        'info_usuarios_baixa'              => $usuariosBaixa,
                        'info_estado_titulo'               => $dataPost['tituloEstado'],
                        'titulo_data_vencimento_inicio'    => $dataPost['tituloDataVencimentoInicio'],
                        'titulo_data_vencimento_fim'       => $dataPost['tituloDataVencimentoFim'],
                        'titulo_data_pagamento_inicio'     => $dataPost['tituloDataPagamentoInicio'],
                        'titulo_data_pagamento_fim'        => $dataPost['tituloDataPagamentoFim'],
                        'titulo_data_baixa_inicio'         => $dataPost['tituloDataBaixaInicio'],
                        'titulo_data_baixa_fim'            => $dataPost['tituloDataBaixaFim'],
                        'titulo_data_processamento_inicio' => $dataPost['tituloDataProcessamentoInicio'],
                        'titulo_data_processamento_fim'    => $dataPost['tituloDataProcessamentoFim'],
                        'where'                            => $where
                    ]
                )->setOutputFile($nomeRelatorio, $dataPost['tipo'])
                ->downloadHtmlPdf(true)
                ->execute();
        }
        $this->getView()->setVariable(
            'arrSituacaoAlunoPeriodo',
            $serviceAcadperiodoAluno->getArrSelect2AlunoperiodoSituacao()
        );

        $this->getView()->setVariable('arrRelatorios', array_values($arrRelatorios));
        $objServiceTitulo->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function comissaoAgentesAction()
    {
        $serviceOrgCampus   = new \Organizacao\Service\OrgCampus($this->getEntityManager());
        $serviceEducacional = new \Organizacao\Service\OrgAgenteEducacional($this->getEntityManager());
        $serviceTitulo      = new \Financeiro\Service\FinanceiroTituloTipo($this->getEntityManager());
        $serviceNivel       = new \Matricula\Service\AcadNivel($this->getEntityManager());

        $request = $this->getRequest();

        if ($request->isPost()) {
            $pdf                 = $this->getServiceLocator()->get('SnowReportsZf2\Service\SnowReportsZf2');
            $arrDadosInstituicao = $serviceOrgCampus->retornaDadosInstituicao();
            $dados               = $request->getPost()->toArray();
            $info                = '';
            $tipoRelatorio       = 'comissao';

            if ($dados['relatorioTipo'] == 'valorPago') {
                $tipoRelatorio = 'valor_pago';
            }

            $dataInicio = $serviceOrgCampus::formatDateAmericano($dados['dataInicio']);
            $dataFinal  = $serviceOrgCampus::formatDateAmericano($dados['dataFinal']);
            $dataInicio = str_replace('-', '', $dataInicio);
            $dataFinal  = str_replace('-', '', $dataFinal);

            $tipoTitulo = $dados['tipoTitulo'] ? $dados['tipoTitulo'] : null;
            $agente     = $dados['agente'] ? $dados['agente'] : null;

            if ($agente) {
                $descricao = $serviceEducacional->getDescricao($agente);
                $info .= 'Agente: ' . $descricao . ', ';
            }

            if ($tipoTitulo) {
                $descricao = $serviceTitulo->getDescricao($tipoTitulo);
                $info .= 'Tipo de Título: ' . $descricao . ', ';
            }

            $nivelDescricao = '';
            $ensinoSql      = " 1 = 1";

            if ($dados['nivelEnsino']) {
                $nivelDescricao = $serviceNivel->getDescricao($dados['nivelEnsino']);

                if ($nivelDescricao) {
                    $info .= ' Nível de Ensino: ' . $nivelDescricao . ', ';
                }

                $ensinoSql = " curso__nivel_id IN (" . $dados['nivelEnsino'] . ")";
            }

            if ($dataInicio && $dataFinal) {
                $info .= ' De: ' . $dados['dataInicio'] . ' até ' . $dados['dataFinal'];
            } elseif ($dataInicio) {
                $info .= ' A partir da data: ' . $dados['dataInicio'];
            } elseif ($dataFinal) {
                $info .= ' Anterior a data: ' . $dados['dataFinal'];
            }

            $pdf->setJrxmlPath(__DIR__ . '/../Reports/')
                ->setJrxmlFile("relatorio-apuracao-agentes.jrxml")
                ->setAttrbutes(
                    [
                        'instituicao'   => $arrDadosInstituicao['ies'],
                        'mantenedora'   => $arrDadosInstituicao['mantenedora'],
                        'titulo'        => 'Relatorio de Apuração de Comissão',
                        'logo'          => $arrDadosInstituicao['logo'],
                        'relatorioinfo' => trim(trim($info), ','),
                        'sistema'       => 'Universa',
                        'endereco'      => $arrDadosInstituicao['endereco'],
                        'agentes'       => $agente,
                        'datainicio'    => $dataInicio,
                        'datafim'       => $dataFinal,
                        'tipo'          => $tipoTitulo,
                        'nivelensino'   => $ensinoSql,
                        'relatoriotipo' => $tipoRelatorio
                    ]

                )
                ->setOutputFile('relatorio-apuracao-agentes', 'pdf')
                ->downloadHtmlPdf(true)
                ->execute();
        }

        return $this->getView();
    }

    public function analiticoAction()
    {
        $request                         = $this->getRequest();
        $arrConfig                       = $this->getServiceManager()->get('Config');
        $objServiceTitulo                = new \Financeiro\Service\FinanceiroTitulo($this->getEntityManager());
        $objServiceCampus                = new \Organizacao\Service\OrgCampus($this->getEntityManager());
        $objServiceTituloTipo            = new \Financeiro\Service\FinanceiroTituloTipo($this->getEntityManager());
        $objServiceAgente                = new \Organizacao\Service\OrgAgenteEducacional($this->getEntityManager());
        $objServiceAcesso                = new \Acesso\Service\AcessoPessoas($this->getEntityManager());
        $serviceSisConfig                = new \Sistema\Service\SisConfig($this->getEntityManager(), $arrConfig);
        $serviceAcadperiodoAluno         = new \Matricula\Service\AcadperiodoAluno($this->getEntityManager());
        $serviceFinanceiroDescontoTitulo = new \Financeiro\Service\FinanceiroDescontoTitulo($this->getEntityManager());

        /** @var \SnowReportsZf2\Service\SnowReportsZf2 $snow */
        $snow = $this->getServiceLocator()->get('SnowReportsZf2\Service\SnowReportsZf2');

        $arrRelatorios = [
            'financeiro-analitico-inadimplentes' => [
                'id'      => 'financeiro_titulos',
                'text'    => 'Inadimplentes',
                'filtros' => [
                    'tituloEstado'              => 'Aberto',
                    'tituloDataPagamentoInicio' => false,
                    'meioPag'                   => false,
                    'meioDePagamento'           => false,
                    'tituloDataPagamentoFim'    => false,
                    'tituloDataBaixaInicio'     => false,
                    'tituloDataBaixaFim'        => false,
                ],
                'ext'     => 'pdf'
            ],
        ];

        if ($request->isPost()) {
            $dataPost = $request->getPost()->toArray();

            $tipoRelatorio = $dataPost['tipoRelatorio'] ? $dataPost['tipoRelatorio'] : '';

            if (!in_array($tipoRelatorio, array_keys($arrRelatorios))) {
                $tipoRelatorio = 'financeiro-analitico-inadimplentes';
            }

            foreach ($arrRelatorios[$tipoRelatorio]['filtros'] as $filtro => $valor) {
                $dataPost[$filtro] = $valor;
            }

            $dadosinstituicao = $objServiceCampus->retornaDadosInstituicao();

            $where = ' 1 = 1 ';

            if ($dataPost['tituloParcela']) {
                $where .= "  AND t.titulo_parcela = {$dataPost['tituloParcela']} ";
            }

            if ($dataPost['meioDePagamento']) {
                if (in_array($dataPost['meioPag'], [' IN ', ' NOT IN '])) {
                    $where .= " AND meio_pagamento_id {$dataPost['meioPag']} ({$dataPost['meioDePagamento']})";
                }
            }

            if ($dataPost['areaId']) {
                $where .= " AND t.area_id in({$dataPost['areaId']})";
            }

            if ($dataPost['campId']) {
                $where .= " AND t.camp_id in({$dataPost['campId']})";
            }

            if ($dataPost['periodoLetivo']) {
                $where .= " AND t.per_id in ({$dataPost['periodoLetivo']})";
            }

            if ($dataPost['situacaoalunoPeriodoLetivo']) {
                $where .= " AND t.matsituacao_id in ({$dataPost['situacaoalunoPeriodoLetivo']})";
            }

            if ($dataPost['alunosTurma']) {
                $where .= " AND t.turma_id in ({$dataPost['alunosTurma']})";
            }

            if ($dataPost['cursoId']) {
                $where .= " AND t.curso_id in({$dataPost['cursoId']})";
            }

            if ($dataPost['cursoId']) {
                $where .= " AND t.curso_id in({$dataPost['cursoId']})";
            }

            $tipoTituloInfo = '';

            if ($dataPost['tipotituloId']) {
                $tipoTituloInfo = $objServiceTituloTipo->getDescricao($dataPost['tipotituloId']);

                $where .= " AND t.tipotitulo_id in({$dataPost['tipotituloId']})";
            }

            $pesIdAgente = '';

            if ($dataPost['pesIdAgente']) {
                $pesIdAgente = $objServiceAgente->getDescricao($dataPost['pesIdAgente']);
                $where .= " AND (t.pes_id_agente {$dataPost['agenteOpcao']}({$dataPost['pesIdAgente']}) OR (t.pes_id_agente IS NULL AND t.pes_id_agenciador {$dataPost['agenteOpcao']}({$dataPost['pesIdAgente']})))";
            }

            if ($dataPost['tituloDataVencimentoInicio']) {
                $dateVencInicio = $objServiceAcesso::formatDateAmericano($dataPost['tituloDataVencimentoInicio']);
                $dateVencInicio = str_replace('-', '', $dateVencInicio);
                $where .= " AND DATE(t.titulo_data_vencimento) >= DATE(" . $dateVencInicio . ")";
            }

            if ($dataPost['tituloDataVencimentoFim']) {
                $dateVencFim = $objServiceAcesso::formatDateAmericano($dataPost['tituloDataVencimentoFim']);
                $dateVencFim = str_replace('-', '', $dateVencFim);
                $where .= " AND DATE(t.titulo_data_vencimento) <=  " . $dateVencFim . "";
            }

            if ($dataPost['tituloDataPagamentoInicio']) {
                $datePagIni = $objServiceAcesso::formatDateAmericano($dataPost['tituloDataPagamentoInicio']);
                $datePagIni = str_replace('-', '', $datePagIni);
                $where .= " AND DATE(t.titulo_data_pagamento) >=" . $datePagIni . " ";
            }

            if ($dataPost['tituloDataPagamentoFim']) {
                $datePagFim = $objServiceAcesso::formatDateAmericano($dataPost['tituloDataPagamentoFim']);
                $datePagFim = str_replace('-', '', $datePagFim);
                $where .= " AND DATE(t.titulo_data_pagamento) <= " . $datePagFim . "";
            }

            if ($dataPost['tituloDataBaixaInicio']) {
                $dateBaixaIni = $objServiceAcesso::formatDateAmericano($dataPost['tituloDataBaixaInicio']);
                $dateBaixaIni = str_replace('-', '', $dateBaixaIni);
                $where .= " AND DATE(t.titulo_data_baixa) >= " . $dateBaixaIni . "";
            }

            if ($dataPost['tituloDataBaixaFim']) {
                $dateBaixaFim = $objServiceAcesso::formatDateAmericano($dataPost['tituloDataBaixaFim']);
                $dateBaixaFim = str_replace('-', '', $dateBaixaFim);
                $where .= " AND DATE(t.titulo_data_baixa) <=" . $dateBaixaFim . "";
            }

            $usuariosAutores = '';

            if ($dataPost['usuarioAutor']) {
                $usuariosAutores = explode(',', $dataPost['usuarioAutor']);

                $usuariosAutores = $objServiceAcesso->getDescricao(
                    count(explode(',', $dataPost['usuarioAutor'])) > 1 ? explode(
                        ',',
                        $dataPost['usuarioAutor']
                    ) : $usuariosAutores[0]
                );

                $where .= " AND t.usuario_autor in ({$dataPost['usuarioAutor']}) ";
            }

            $usuariosCadastroAluno = '';

            if ($dataPost['usuarioCadastroAluno']) {
                $usuariosCadastroAluno = explode(',', $dataPost['usuarioCadastroAluno']);

                $usuariosCadastroAluno = $objServiceAcesso->getDescricao(
                    count(explode(',', $dataPost['usuarioCadastroAluno'])) > 1 ? explode(
                        ',',
                        $dataPost['usuarioCadastroAluno']
                    ) : $usuariosCadastroAluno[0]
                );

                $where .= " AND COALESCE (t.usuario_cadastro_aluno_curso, t.usuario_cadastro_aluno) in ({$dataPost['usuarioCadastroAluno']}) ";
            }

            $usuariosBaixa = '';

            if ($dataPost['usuarioBaixa']) {
                $usuariosBaixa = explode(',', $dataPost['usuarioBaixa']);
                $usuariosBaixa = $objServiceAcesso->getDescricao(
                    count(explode(',', $dataPost['usuarioBaixa'])) > 1 ? explode(
                        ',',
                        $dataPost['usuarioBaixa']
                    ) : $usuariosBaixa[0]
                );

                $where .= " AND t.usuario_baixa in ({$dataPost['usuarioBaixa']}) ";
            }

            if ($dataPost['tituloEstado']) {
                $situacaoArr = explode(",", $dataPost['tituloEstado']);
                $situacaoStr = '"' . implode('","', $situacaoArr) . '"';
                $where .= " AND t.titulo_estado IN(" . $situacaoStr . ")";
            };

            $meioCalculoDescAntecipado = $serviceSisConfig->localizarChave(
                'FINANCEIRO_CONFIG_CALCULO_DESCONTO_INCENTIVO'
            );

            if (!$meioCalculoDescAntecipado) {
                $this->flashMessenger()->addErrorMessage(
                    'É necessário configurar o meio para cálculo de desconto antecipado! Contate o administrador do sistema!'
                );

                return false;
            }

            $nomeRelatorio = 'financeiro_titulos_' . (new \DateTime('now'))->format('YmdHi');

            $snow->setJrxmlPath(__DIR__ . '/../Reports/')
                ->setJrxmlFile('financeiro-analitico-inadimplentes_' . $dataPost['tipo'] . '.jrxml')
                ->setAttrbutes(
                    [
                        'logo'                             => $dadosinstituicao['logo'],
                        'mantenedora'                      => $dadosinstituicao['mantenedora'],
                        'instituicao'                      => $dadosinstituicao['ies'],
                        'instituicaoinfo'                  => '',
                        'datainicio'                       => $dataPost['dataInicial'],
                        'datafim'                          => $dataPost['dataFinal'],
                        'titulo'                           => 'Relatório Financeiro Analítico',
                        'relatorioinfo'                    => 'Relatório Financeiro Analítico',
                        'endereco'                         => $dadosinstituicao['endereco'],
                        'sistema'                          => 'Universa',
                        'camp'                             => '',
                        'cursos'                           => '',
                        'tipo_titulos'                     => '',
                        'usuario_autor'                    => '',
                        'usuario_baixa'                    => '',
                        'info_tipo_titulo'                 => $tipoTituloInfo,
                        'info_agente'                      => $pesIdAgente,
                        'info_usuarios_autores'            => $usuariosAutores,
                        'info_usuarios_cadastro_aluno'     => $usuariosCadastroAluno,
                        'info_usuarios_baixa'              => $usuariosBaixa,
                        'info_estado_titulo'               => $dataPost['tituloEstado'],
                        'titulo_data_vencimento_inicio'    => $dataPost['tituloDataVencimentoInicio'],
                        'titulo_data_vencimento_fim'       => $dataPost['tituloDataVencimentoFim'],
                        'titulo_data_pagamento_inicio'     => $dataPost['tituloDataPagamentoInicio'],
                        'titulo_data_pagamento_fim'        => $dataPost['tituloDataPagamentoFim'],
                        'titulo_data_baixa_inicio'         => $dataPost['tituloDataBaixaInicio'],
                        'titulo_data_baixa_fim'            => $dataPost['tituloDataBaixaFim'],
                        'titulo_data_processamento_inicio' => $dataPost['tituloDataProcessamentoInicio'],
                        'titulo_data_processamento_fim'    => $dataPost['tituloDataProcessamentoFim'],
                        'meio_calculo_desc_antecipado'     => $meioCalculoDescAntecipado,
                        'where'                            => $where
                    ]
                )->setOutputFile($nomeRelatorio, $dataPost['tipo'])
                ->downloadHtmlPdf(true)
                ->execute();
        }

        $this->getView()->setVariable(
            'arrSituacaoAlunoPeriodo',
            $serviceAcadperiodoAluno->getArrSelect2AlunoperiodoSituacao()
        );
        $this->getView()->setVariable('arrRelatorios', array_values($arrRelatorios));
        $objServiceTitulo->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function adimplentesAction()
    {
        $request                 = $this->getRequest();
        $arrConfig               = $this->getServiceManager()->get('Config');
        $objServiceTitulo        = new \Financeiro\Service\FinanceiroTitulo($this->getEntityManager(), $arrConfig);
        $objServiceCampus        = new \Organizacao\Service\OrgCampus($this->getEntityManager());
        $objServiceTituloTipo    = new \Financeiro\Service\FinanceiroTituloTipo($this->getEntityManager());
        $objServiceAgente        = new \Organizacao\Service\OrgAgenteEducacional($this->getEntityManager());
        $objServiceAcesso        = new \Acesso\Service\AcessoPessoas($this->getEntityManager(), $arrConfig);
        $serviceSisConfig        = new \Sistema\Service\SisConfig($this->getEntityManager(), $arrConfig);
        $serviceAcadperiodoAluno = new \Matricula\Service\AcadperiodoAluno($this->getEntityManager());

        /** @var \SnowReportsZf2\Service\SnowReportsZf2 $snow */
        $snow = $this->getServiceLocator()->get('SnowReportsZf2\Service\SnowReportsZf2');

        $arrRelatorios = [
            'financeiro-analitico-adimplentes' => [
                'id'      => 'financeiro_titulos',
                'text'    => 'Adimplentes',
                'filtros' => [
                    'tituloEstado'                  => 'Pago',
                    'tituloDataPagamentoInicio'     => false,
                    'meioPag'                       => false,
                    'meioDePagamento'               => false,
                    'tituloDataPagamentoFim'        => false,
                    'tituloDataBaixaInicio'         => false,
                    'tituloDataBaixaFim'            => false,
                    'tituloDataProcessamentoInicio' => false,
                    'tituloDataProcessamentoFim'    => false,
                ],
                'ext'     => 'pdf'
            ],
        ];

        if ($request->isPost()) {
            $dataPost = $request->getPost()->toArray();

            $tipoRelatorio = $dataPost['tipoRelatorio'] ? $dataPost['tipoRelatorio'] : '';

            if (!in_array($tipoRelatorio, array_keys($arrRelatorios))) {
                $tipoRelatorio = 'financeiro-analitico-inadimplentes';
            }

            foreach ($arrRelatorios[$tipoRelatorio]['filtros'] as $filtro => $valor) {
                $dataPost[$filtro] = $valor;
            }

            $dadosinstituicao = $objServiceCampus->retornaDadosInstituicao();

            $where = '1 = 1';

            if ($dataPost['meioDePagamento']) {
                if (in_array($dataPost['meioPag'], [' IN ', ' NOT IN '])) {
                    $where .= " AND meio_pagamento_id {$dataPost['meioPag']} ({$dataPost['meioDePagamento']})";
                }
            }

            if ($dataPost['areaId']) {
                $where .= " AND t.area_id in({$dataPost['areaId']})";
            }

            if ($dataPost['campId']) {
                $where .= " AND t.camp_id in({$dataPost['campId']})";
            }

            if ($dataPost['periodoLetivo']) {
                $where .= " AND t.per_id in ({$dataPost['periodoLetivo']})";
            }

            if ($dataPost['situacaoalunoPeriodoLetivo']) {
                $where .= " AND t.matsituacao_id in ({$dataPost['situacaoalunoPeriodoLetivo']})";
            }

            if ($dataPost['alunosTurma']) {
                $where .= " AND t.turma_id in ({$dataPost['alunosTurma']})";
            }

            if ($dataPost['cursoId']) {
                $where .= " AND t.curso_id in({$dataPost['cursoId']})";
            }

            if ($dataPost['cursoId']) {
                $where .= " AND t.curso_id in({$dataPost['cursoId']})";
            }

            $tipoTituloInfo = '';

            if ($dataPost['tipotituloId']) {
                $tipoTituloInfo = $objServiceTituloTipo->getDescricao($dataPost['tipotituloId']);

                $where .= " AND t.tipotitulo_id in({$dataPost['tipotituloId']})";
            }

            if ($dataPost['tituloParcela']) {
                $where .= " AND titulo_parcela ={$dataPost['tituloParcela']} ";
            }

            $pesIdAgente = '';

            if ($dataPost['pesIdAgente']) {
                $pesIdAgente = $objServiceAgente->getDescricao($dataPost['pesIdAgente']);
                $where .= " AND (t.pes_id_agente {$dataPost['agenteOpcao']}({$dataPost['pesIdAgente']}) OR (t.pes_id_agente IS NULL AND t.pes_id_agenciador {$dataPost['agenteOpcao']}({$dataPost['pesIdAgente']})))";
            }

            if ($dataPost['tituloDataVencimentoInicio']) {
                $dateVencInicio = $objServiceAcesso::formatDateAmericano($dataPost['tituloDataVencimentoInicio']);
                $dateVencInicio = str_replace('-', '', $dateVencInicio);
                $where .= " AND DATE(t.titulo_data_vencimento) >= DATE(" . $dateVencInicio . ")";
            }

            if ($dataPost['tituloDataVencimentoFim']) {
                $dateVencFim = $objServiceAcesso::formatDateAmericano($dataPost['tituloDataVencimentoFim']);
                $dateVencFim = str_replace('-', '', $dateVencFim);
                $where .= " AND DATE(t.titulo_data_vencimento) <=  " . $dateVencFim . "";
            }

            if ($dataPost['tituloDataPagamentoInicio']) {
                $datePagIni = $objServiceAcesso::formatDateAmericano($dataPost['tituloDataPagamentoInicio']);
                $datePagIni = str_replace('-', '', $datePagIni);
                $where .= " AND DATE(t.titulo_data_pagamento) >=" . $datePagIni . " ";
            }

            if ($dataPost['tituloDataPagamentoFim']) {
                $datePagFim = $objServiceAcesso::formatDateAmericano($dataPost['tituloDataPagamentoFim']);
                $datePagFim = str_replace('-', '', $datePagFim);
                $where .= " AND DATE(t.titulo_data_pagamento) <= " . $datePagFim . "";
            }

            if ($dataPost['tituloDataBaixaInicio']) {
                $dateBaixaIni = $objServiceAcesso::formatDateAmericano($dataPost['tituloDataBaixaInicio']);
                $dateBaixaIni = str_replace('-', '', $dateBaixaIni);
                $where .= " AND DATE(t.titulo_data_baixa) >= " . $dateBaixaIni . "";
            }

            if ($dataPost['tituloDataBaixaFim']) {
                $dateBaixaFim = $objServiceAcesso::formatDateAmericano($dataPost['tituloDataBaixaFim']);
                $dateBaixaFim = str_replace('-', '', $dateBaixaFim);
                $where .= " AND DATE(t.titulo_data_baixa) <=" . $dateBaixaFim . "";
            }

            $desconto = $dataPost['tipoDesconto'];

            switch ($desconto) {
                case 'nao':
                    $descontoTipo = 'Sem descontos';
                    $desconto     = "ifnull(t.descontos,0)";
                    $where .= ' AND ' . $desconto . ' <= 0 ';
                    break;
                case 'pagamento':
                    $descontoTipo = 'Desconto no ato do pagamento';
                    $desconto     = "(titulo_valor + titulo_desconto_manual - titulo_valor_pago)";
                    $where .= ' AND ' . $desconto . ' > 0';
                    break;
                case 'prefixado':
                    $descontoTipo = 'Descontos prefixados';
                    $desconto     = "(ifnull(titulo_valor,0) + ifnull(t.titulo_acrescimo_manual,0)) - (ifnull(titulo_valor_pago,0) + ifnull(titulo_desconto_manual,0))";
                    $where .= ' AND ' . $desconto . ' > 0 ';
                    break;
                default:
                    $descontoTipo = 'Com e sem descontos';
                    $desconto     = "ifnull(t.descontos,0)";
                    break;
            }

            $usuariosAutores = '';

            if ($dataPost['usuarioAutor']) {
                $usuariosAutores = explode(',', $dataPost['usuarioAutor']);

                $usuariosAutores = $objServiceAcesso->getDescricao(
                    count(explode(',', $dataPost['usuarioAutor'])) > 1 ? explode(
                        ',',
                        $dataPost['usuarioAutor']
                    ) : $usuariosAutores[0]
                );

                $where .= " AND t.usuario_autor in ({$dataPost['usuarioAutor']}) ";
            }

            $usuariosCadastroAluno = '';

            if ($dataPost['usuarioCadastroAluno']) {
                $usuariosCadastroAluno = explode(',', $dataPost['usuarioCadastroAluno']);

                $usuariosCadastroAluno = $objServiceAcesso->getDescricao(
                    count(explode(',', $dataPost['usuarioCadastroAluno'])) > 1 ? explode(
                        ',',
                        $dataPost['usuarioCadastroAluno']
                    ) : $usuariosCadastroAluno[0]
                );

                $where .= " AND COALESCE (t.usuario_cadastro_aluno_curso, t.usuario_cadastro_aluno) in ({$dataPost['usuarioCadastroAluno']}) ";
            }

            $usuariosBaixa = '';

            if ($dataPost['usuarioBaixa']) {
                $usuariosBaixa = explode(',', $dataPost['usuarioBaixa']);
                $usuariosBaixa = $objServiceAcesso->getDescricao(
                    count(explode(',', $dataPost['usuarioBaixa'])) > 1 ? explode(
                        ',',
                        $dataPost['usuarioBaixa']
                    ) : $usuariosBaixa[0]
                );

                $where .= " AND t.usuario_baixa in ({$dataPost['usuarioBaixa']}) ";
            }

            if ($dataPost['tituloEstado']) {
                $situacaoArr = explode(",", $dataPost['tituloEstado']);
                $situacaoStr = '"' . implode('","', $situacaoArr) . '"';
                $where .= " AND t.titulo_estado IN(" . $situacaoStr . ")";
            };

            $meioCalculoDescAntecipado = $serviceSisConfig->localizarChave(
                'FINANCEIRO_CONFIG_CALCULO_DESCONTO_INCENTIVO'
            );

            if (!$meioCalculoDescAntecipado) {
                $this->flashMessenger()->addErrorMessage(
                    'É necessário configurar o meio para cálculo de desconto antecipado! Contate o administrador do sistema!'
                );

                return false;
            }

            $meioCalculoDescAntecipado = "'" . $meioCalculoDescAntecipado . "'";

            $nomeRelatorio = 'financeiro_titulos_' . (new \DateTime('now'))->format('YmdHi');

            $snow->setJrxmlPath(__DIR__ . '/../Reports/')
                ->setJrxmlFile('financeiro-analitico-adimplentes_' . $dataPost['tipo'] . '.jrxml')
                ->setAttrbutes(
                    [
                        'logo'                             => $dadosinstituicao['logo'],
                        'mantenedora'                      => $dadosinstituicao['mantenedora'],
                        'instituicao'                      => $dadosinstituicao['ies'],
                        'instituicaoinfo'                  => '',
                        'datainicio'                       => $dataPost['dataInicial'],
                        'datafim'                          => $dataPost['dataFinal'],
                        'titulo'                           => 'Relatório Financeiro Analítico',
                        'relatorioinfo'                    => $descontoTipo,
                        'endereco'                         => $dadosinstituicao['endereco'],
                        'sistema'                          => 'Universa',
                        'camp'                             => '',
                        'cursos'                           => '',
                        'tipo_titulos'                     => '',
                        'usuario_autor'                    => '',
                        'usuario_baixa'                    => '',
                        'info_tipo_titulo'                 => $tipoTituloInfo,
                        'info_agente'                      => $pesIdAgente,
                        'info_usuarios_autores'            => $usuariosAutores,
                        'info_usuarios_cadastro_aluno'     => $usuariosCadastroAluno,
                        'info_usuarios_baixa'              => $usuariosBaixa,
                        'info_estado_titulo'               => $dataPost['tituloEstado'],
                        'titulo_data_vencimento_inicio'    => $dataPost['tituloDataVencimentoInicio'],
                        'titulo_data_vencimento_fim'       => $dataPost['tituloDataVencimentoFim'],
                        'titulo_data_pagamento_inicio'     => $dataPost['tituloDataPagamentoInicio'],
                        'titulo_data_pagamento_fim'        => $dataPost['tituloDataPagamentoFim'],
                        'titulo_data_baixa_inicio'         => $dataPost['tituloDataBaixaInicio'],
                        'titulo_data_baixa_fim'            => $dataPost['tituloDataBaixaFim'],
                        'titulo_data_processamento_inicio' => $dataPost['tituloDataProcessamentoInicio'],
                        'titulo_data_processamento_fim'    => $dataPost['tituloDataProcessamentoFim'],
                        'meiocalculodescantecipado'        => $meioCalculoDescAntecipado,
                        'desconto'                         => $desconto,
                        'where'                            => $where
                    ]
                )->setOutputFile($nomeRelatorio, $dataPost['tipo'])
                ->downloadHtmlPdf(true)
                ->execute();
        }

        $this->getView()->setVariable(
            'arrSituacaoAlunoPeriodo',
            $serviceAcadperiodoAluno->getArrSelect2AlunoperiodoSituacao()
        );
        $this->getView()->setVariable('arrRelatorios', array_values($arrRelatorios));
        $objServiceTitulo->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function fechamentoCaixaAction()
    {
        $service              = new \Financeiro\Service\FinanceiroTitulo($this->getEntityManager());
        $serviceTipoTitulo    = new \Financeiro\Service\FinanceiroTituloTipo($this->getEntityManager());
        $serviceAcesso        = new \Acesso\Service\AcessoPessoas($this->getEntityManager());
        $serviceCampusCurso   = new \Matricula\Service\CampusCurso($this->getEntityManager());
        $serviceMeioPagamento = new \Financeiro\Service\FinanceiroMeioPagamento($this->getEntityManager());
        $request              = $this->getRequest();
        $arrConfig            = $this->getServiceManager()->get('Config');
        $serviceSisConfig     = new \Sistema\Service\SisConfig($this->getEntityManager(), $arrConfig);

        if ($request->isPost()) {
            $serviceOrgCampus = new \Organizacao\Service\OrgCampus($this->getEntityManager());
            $this->getView()->setVariables($serviceOrgCampus->retornaDadosInstituicao());

            $dados = $request->getPost()->toArray();

            $arrApuracao = $service->geraRelatorioFinanceiroPeriodo($dados);

            $cursos = $serviceCampusCurso->getDescricao($dados['campusCurso']);

            if ($dados['campusCurso-filtro'] == "NOT IN" && $cursos) {
                $cursos = "Exceto: " . $cursos;
            }

            $cursos = $cursos ? $cursos : 'Todos';

            $tipoTitulo = $serviceTipoTitulo->getDescricao($dados['tipoTitulo']);

            if ($dados['tipoTitulo-filtro'] == "NOT IN" && $tipoTitulo) {
                $tipoTitulo = "Exceto: " . $tipoTitulo;
            }

            $tipoTitulo = $tipoTitulo ? $tipoTitulo : 'Todos';

            $responsavel = $serviceAcesso->getDescricao($dados['pagamentoUsuario']);

            if ($dados['pagamentoUsuario-filtro'] == "NOT IN" && $responsavel) {
                $responsavel = "Exceto: " . $responsavel;
            }

            $responsavel = $responsavel ? $responsavel : 'Todos';

            $meiosPagamento = $serviceMeioPagamento->getDescricao($dados['meioPagamento']);

            if ($dados['meioPagamento-filtro'] == "NOT IN" && $meiosPagamento) {
                $meiosPagamento = "Exceto: " . $meiosPagamento;
            }

            $meiosPagamento = $meiosPagamento ? $meiosPagamento : 'Todos;';

            if ($dados['tipoRelatorio'] === 'analitico') {
                $this->getView()->setTemplate('financeiro/relatorio/relatorio-fechamento-caixa-analitico');
            } else {
                $this->getView()->setTemplate('financeiro/relatorio/relatorio-fechamento-caixa-sintetico');
            }

            $this->getView()->setVariables(
                [
                    'dataIni'                            => $serviceAcesso->formatDateBrasileiro($dados['dataInicial']),
                    'dataFim'                            => $serviceAcesso->formatDateBrasileiro($dados['dataFinal']),
                    'usuario'                            => $serviceAcesso->retornaUsuarioLogado(),
                    'informacaoGeral'                    => $arrApuracao['informacaoGeral'],
                    'informacaoPorCurso'                 => $arrApuracao['informacaoPorCurso'],
                    'informacaoPorCursoEMeioDePagamento' => $arrApuracao['informacaoPorCursoEMeioDePagamento'],
                    'arrApuracao'                        => $arrApuracao,
                    'curso'                              => $cursos,
                    'tipoTitulo'                         => $tipoTitulo,
                    'responsavel'                        => $responsavel,
                    'meioPagamento'                      => $meiosPagamento,
                ]
            );

            $this->getView()->setTerminal(true);

            return $this->view;
        }

        $gruposUsuariosNaListagem = $serviceSisConfig->localizarChave('FINANCEIRO_FILTRO_GRUPO_ACESSO');

        $this->getView()->setVariable('gruposNaListagemUsarios', $gruposUsuariosNaListagem);

        $this->getView()->setTemplate('financeiro/relatorio/fechamento-caixa');

        return $this->view;
    }

    public function comprovanteRendaAction()
    {
        $service          = new \Boleto\Service\FinanceiroRelatorios($this->getEntityManager());
        $serviceOrgCampus = new \Organizacao\Service\OrgCampus($this->getEntityManager());

        $request = $this->getRequest();

        if ($request->isPost()) {
            $dados  = $request->getPost()->toArray();
            $alunos = $service->buscaTitulosPagosPes($dados);

            for ($i = 0; $i < count($alunos); $i++) {
                $alunos[$i]['valorExtenso'] = $service->valorPorExtenso($alunos[$i]['total']);
            }

            $pdf = new PdfModel();

            $pdf->setVariables($serviceOrgCampus->retornaDadosInstituicao());

            $pdf->setVariables(
                [
                    'alunos'   => $alunos,
                    'data'     => explode('/', (new \DateTime('now'))->format("d/m/Y")),
                    'dataBase' => (new \DateTime('now'))->format("d/m/Y H:i"),
                    'mesBase'  => \Boleto\Service\FinanceiroRelatorios::mesReferencia(
                        explode('/', (new \DateTime('now'))->format("d/m/Y"))[1]
                    ),
                    'ano'      => $dados['anoBase']
                ]
            );

            $pdf->setTemplate("financeiro/relatorio/comprovante-renda-documento");
            $pdf->setOption('a4', 'landscape');

            $this->view->setTerminal(true);

            return $pdf;
        }

        $this->view->setVariables(
            [
                'data' => (new \DateTime('now'))->format("d/m/Y"),
            ]
        );

        return $this->view;
    }

    public function relatorioDescontoAction()
    {
        $serviceFinanceiroTitulo         = new \Financeiro\Service\FinanceiroTitulo($this->getEntityManager());
        $serviceFinanceiroDescontoTitulo = new \Financeiro\Service\FinanceiroDescontoTitulo($this->getEntityManager());
        $serviceAcesso                   = new \Acesso\Service\AcessoPessoas($this->getEntityManager());
        $objServiceCampus                = new \Organizacao\Service\OrgCampus($this->getEntityManager());
        $request                         = $this->getRequest();
        $serviceAcadPeriodoLetivo        = new \Matricula\Service\AcadperiodoLetivo($this->getEntityManager());
        $serviceSisConfig                = new \Sistema\Service\SisConfig($this->getEntityManager());
        $serviceDescontoTipo             = new \Financeiro\Service\FinanceiroDescontoTipo($this->getEntityManager());

        /** @var /Matricula/Entity/AcadperiodoLetivo @objPeriodoLetivo */
        $objPeriodoLetivo = $serviceAcadPeriodoLetivo->buscaPeriodoAtual();

        if ($request->isPost()) {
            $dataPost = $request->getPost()->toArray();

            $dadosinstituicao = $objServiceCampus->retornaDadosInstituicao();

            $where = ' 1=1 ';

            if ($dataPost['areaId']) {
                $where .= " and areac.area_id in ({$dataPost['areaId']}) ";
            }

            if ($dataPost['periodoLetivo']) {
                $where .= " and apl.per_id in ({$dataPost['periodoLetivo']}) ";
            }

            if ($dataPost['campusCurso']) {
                $where .= " and cc.camp_id in ({$dataPost['campusCurso']}) ";
            }

            if ($dataPost['curso']) {
                $where .= " and cc.curso_id in ({$dataPost['curso']}) ";
            }

            if ($dataPost['turma']) {
                $where .= " and apt.turma_id in ({$dataPost['turma']}) ";
            }

            if ($dataPost['tipoTitulo']) {
                $where .= " and ftt.tipotitulo_id in ({$dataPost['tipoTitulo']}) ";
            }

            if ($dataPost['estadoTitulo']) {
                $situacaoStr = str_replace(",", '","', $dataPost['estadoTitulo']);
                $where .= " AND ft.titulo_estado IN(\"" . $situacaoStr . "\") ";
            }

            if ($dataPost['vencimentoInicial']) {
                $dateVencInicio = $serviceAcesso::formatDateAmericano($dataPost['vencimentoInicial']);
                $dateVencInicio = str_replace('-', '', $dateVencInicio);
                $where .= " AND DATE(ft.titulo_data_vencimento) >= DATE(" . $dateVencInicio . ") ";
            }
            if ($dataPost['vencimentoFim']) {
                $dateVencFim = $serviceAcesso::formatDateAmericano($dataPost['vencimentoFim']);
                $dateVencFim = str_replace('-', '', $dateVencFim);
                $where .= " AND DATE(ft.titulo_data_vencimento) <= DATE(" . $dateVencFim . ") ";
            }

            $descontos = '';
            if ($dataPost['tiposDesconto']) {
                $tiposDescontos = explode(',', $dataPost['tiposDesconto']);

                foreach ($tiposDescontos as $value) {
                    /** @var \Financeiro\Entity\FinanceiroDescontoTipo $objDescontoTipo */
                    $objDescontoTipo = $serviceDescontoTipo->getRepository()->find($value);

                    $objDescontoTipo ? (
                    $descontos ? $descontos .= ", " . $objDescontoTipo->getDesctipoDescricao(
                        ) : $descontos .= $objDescontoTipo->getDesctipoDescricao()
                    ) : '';
                }

                $where .= " and dscT.desctipo_id in ({$dataPost['tiposDesconto']}) ";
            }

            if ($dataPost['situacaoDesconto']) {
                $situacaoDesconto = '"' . str_replace(",", '","', $dataPost['situacaoDesconto']) . '"';

                $where .= " and (desconto.desconto_status in (" . $situacaoDesconto . ") OR dt.desconto_titulo_aplicado in (" . $situacaoDesconto . "))";
            }

            $usuariosCadastroAluno = '';

            if ($dataPost['usuarioDesconto']) {
                $usuariosCadastroAluno = explode(',', $dataPost['usuarioDesconto']);

                $where .= " and desconto.usuario_criacao in ({$dataPost['usuarioDesconto']}) ";
            }

            if ($dataPost['percentualInicial']) {
                $where .= " and desconto.desconto_percentual >= ({$dataPost['percentualInicial']})";
            }

            if ($dataPost['estadoDesconto']) {
                $estadoDesconto = '"' . $dataPost['estadoDesconto'] . '"';
                $where .= " and desconto.desconto_status={$estadoDesconto }";
            }

            if ($dataPost['percentualFinal']) {
                $where .= " and desconto.desconto_percentual <=({$dataPost['percentualFinal']}) ";
            }

            /** @var \SnowReportsZf2\Service\SnowReportsZf2 $snow */
            $snow = $this->getServiceLocator()->get('SnowReportsZf2\Service\SnowReportsZf2');

            $nomeRelatorio = 'financeiro_descontos' . (new \DateTime('now'))->format('YmdHi');

            /** @var \Organizacao\Entity\OrgCampus $objOrgCampus */
            $objOrgCampus         = $dadosinstituicao['camp'];
            $usuariosCadastrantes = '';

            foreach ($usuariosCadastroAluno as $value) {
                /** @var  $objAcessoPessoas \Acesso\Entity\AcessoPessoas */
                $objAcessoPessoas = $serviceAcesso->getRepository()->find($value);

                if ($objAcessoPessoas) {
                    $usuario = $objAcessoPessoas->getPesJuridica() ?
                        $objAcessoPessoas->getPesJuridica()->getPesNomeFantasia() :
                        $objAcessoPessoas->getPesFisica()->getPes()->getPesNome();

                    $usuariosCadastrantes .= $usuariosCadastrantes ? ', ' . $usuario : $usuario;
                }
            }

            $snow->setJrxmlPath(__DIR__ . '/../Reports/')
                ->setJrxmlFile('financeiro-descontos_' . $dataPost['tipo'] . '.jrxml')
                ->setAttrbutes(
                    [
                        'logo'              => $dadosinstituicao['logo'],
                        'mantenedora'       => $dadosinstituicao['mantenedora'],
                        'instituicao'       => $dadosinstituicao['ies'],
                        'instituicaoinfo'   => '',
                        'percentualinicial' => $dataPost['percentualInicial'],
                        'percentualmaximo'  => $dataPost['percentualFinal'],
                        'descontostipos'    => $descontos,
                        'situacaodesconto'  => $dataPost['estadoDesconto'],
                        'usuariosdesconto'  => $usuariosCadastrantes,
                        'where'             => $where,
                        'titulo'            => 'Relatório de Descontos' . $dataPost['tipoRelatorio'] = 'sintetico' ? ' - ' . 'Sintético por Aluno' : '',
                        'endereco'          => $dadosinstituicao['endereco'],
                        'sistema'           => 'Universa',
                        'camp'              => $objOrgCampus ? $objOrgCampus->getCampNome() : '-'
                    ]
                )->setOutputFile($nomeRelatorio, $dataPost['tipo'])
                ->downloadHtmlPdf(true)
                ->execute();
        }

        $this->getView()->setVariable(
            'arrDescontoSituacao',
            $serviceFinanceiroDescontoTitulo->getArrSelect2DescontoTituloSituacao()
        );

        $this->getView()->setVariable(
            'grupoUsuariosFiltro',
            $serviceSisConfig->localizarChave('FINANCEIRO_DESCONTO_GRUPO_USUARIOS_FILTRO')
        );
        $this->getView()->setVariable('arrTituloEstado', $serviceFinanceiroTitulo->getArrSelect2TituloEstado());

        $this->getView()->setVariable(
            'arrPeriodoAtual',
            $objPeriodoLetivo ?
                ['id' => $objPeriodoLetivo->getPerId(), 'text' => $objPeriodoLetivo->getPerNome()] : ''
        );
        $this->getView()->setVariable(
            'pessoaLogada',
            [
                'id'   => $serviceAcesso->retornaUsuarioLogado()->getId(),
                'text' =>
                    $serviceAcesso->retornaUsuarioLogado()->getPes()->getPes()->getPesNome() ?
                        $serviceAcesso->retornaUsuarioLogado()->getPes()->getPes()->getPesNome() :
                        $serviceAcesso->retornaUsuarioLogado()->getLogin()
            ]
        );
        $this->getView()->setVariable('dataAtual', (new \DateTime('now'))->format('d/m/Y'));

        return $this->getView();
    }

    public function boletosAlunoAction()
    {
        $serviceAcadPeriodoAluno = new \Matricula\Service\AcadperiodoAluno($this->getEntityManager());
        $arrSituacao             = $serviceAcadPeriodoAluno->getArrSelect2AlunoperiodoSituacao();

        $this->getView()->setVariable('arrSituacao', $arrSituacao);
        $this->getView()->setTemplate('financeiro/relatorio/boletos-aluno');

        return $this->getView();
    }
}