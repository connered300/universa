<?php

namespace Financeiro\Controller;

use VersaSpine\Controller\AbstractCoreController;

class FinanceiroPessoaController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function boletosAlunoTableAction()
    {
        $request                 = $this->getRequest();
        $serviceFinanceiroPessoa = (new \Financeiro\Service\FinanceiroPessoa($this->getEntityManager()));

        if ($request->isPost()) {
            $arrayData = $request->getPost()->toArray();

            $result = $serviceFinanceiroPessoa->getDataTables($arrayData);

            $this->json->setVariable("draw", $result["draw"]);
            $this->json->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->json->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->json->setVariable("data", $result["data"]);
        }

        return $this->json;
    }
}
?>