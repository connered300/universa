<?php

namespace Financeiro\Controller;

use VersaSpine\Controller\AbstractCoreController;

class FinanceiroRemessaController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function indexAction()
    {
        $serviceConfConta        = new \Financeiro\Service\BoletoConfConta($this->getEntityManager());
        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo(
            $this->getEntityManager(),
            $this->getServiceManager()->get('Config')
        );
        $arrDataOptions          = $serviceConfConta->getArrSelect2();

        $qtdMaximaBoletosRemessa = $serviceFinanceiroTitulo
            ->getConfig()
            ->localizarChave('QUANTIDADE_MAXIMA_BOLETOS_REMESSA', 950);

        $this->getView()->setVariable('optsConfConta', $arrDataOptions);
        $this->getView()->setVariable('quantidadeMaximaBoletos', $qtdMaximaBoletosRemessa);
        $serviceFinanceiroTitulo->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceFinanceiroRemessa = new \Financeiro\Service\FinanceiroRemessa(
                $this->getEntityManager(),
                $this->getServiceManager()->get('Config')
            );

            $arrData = $request->getPost()->toArray();
            $result  = $serviceFinanceiroRemessa->search($arrData);

            $this->getJson()->setVariable("draw", $result['draw']);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function searchBoletosAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $arrData = $request->getPost()->toArray();
            $service = new \Financeiro\Service\FinanceiroRemessa(
                $this->getEntityManager(),
                $this->getServiceManager()->get('Config')
            );

            if ($arrData['todosBoletosValidos']) {
                $result = $service->retornaBoletosDisponiveis($arrData);

                $this->getJson()->setVariable("data", $result);
            } else {
                $result = $service->searchBoletos($arrData);

                $this->getJson()->setVariables(
                    array(
                        "draw"            => $result['draw'] ? $result['draw'] : "1",
                        "recordsTotal"    => $result["recordsTotal"] ? $result["recordsTotal"] : 0,
                        "recordsFiltered" => $result["recordsFiltered"] ? $result["recordsFiltered"] : 0,
                        "data"            => $result["data"] ? $result["data"] : [],
                    )
                );
            }
        }

        return $this->getJson();
    }

    public function geraArquivoRemessaAction()
    {
        $request    = $this->getRequest();
        $paramsGet  = $request->getQuery()->toArray();
        $paramsPost = $request->getPost()->toArray();
        $arrDados   = array_merge($paramsGet, $paramsPost);
        $result     = [];

        if ($arrDados) {
            $serviceRemessa = new \Financeiro\Service\FinanceiroRemessa(
                $this->getEntityManager(),
                $this->getServiceManager()->get('Config')
            );

            try {
                @ini_set('memory_limit', '-1');
                $waitTimeout        = $serviceRemessa->executeQuery(
                    'SHOW SESSION VARIABLES LIKE "wait_timeout"'
                )->fetch();
                $interactiveTimeout = $serviceRemessa->executeQuery(
                    'SHOW SESSION VARIABLES LIKE "interactive_timeout"'
                )->fetch();
                $waitTimeout        = $waitTimeout['Value'];
                $interactiveTimeout = $interactiveTimeout['Value'];

                $serviceRemessa->executeQuery('SET @@session.wait_timeout=' . (60 * 60));
                $serviceRemessa->executeQuery('SET @@session.interactive_timeout=' . (60 * 60));

                //Gerar nova remessa baseada nos boletos de uma remessa antiga
                if ($arrDados['correcaoRemessa']) {
                    $serviceRemessa->corrigirRemessas();
                } elseif ($arrDados['remessaId']) {
                    $arrDados['boletos'] = $serviceRemessa->retornaBoletosContidosNaRemessa($arrDados['remessaId']);
                } elseif (!is_array($arrDados['boletos'])) {
                    $arrDados['boletos'] = explode(',', $arrDados['boletos']);
                }

                if ($arrDados['gerarRemessaTodosTitulos']) {
                    $arrBoletos = $serviceRemessa->retornaBoletosDisponiveis($arrDados);

                    if (empty($arrBoletos['boletos'])) {
                        $result['error']   = true;
                        $result['message'] = "É necessário selecionar pelo menos um título! </br> Nenhum título selecionado/localizado!";

                        $this->getJson()->setVariables($result);

                        return $this->getJson();
                    }

                    if ($arrBoletos) {
                        $arrBoletos['confContaId']     = $arrDados['confContaId'];
                        $arrBoletos['codigoMovimento'] = $arrDados['codigoMovimento'];
                        $result                        = $serviceRemessa->gerarRemessa($arrBoletos);
                    }
                } else {
                    $result = $serviceRemessa->gerarRemessa($arrDados);
                }

                $serviceRemessa->executeQuery('SET @@session.wait_timeout=' . $waitTimeout);
                $serviceRemessa->executeQuery('SET @@session.interactive_timeout=' . $interactiveTimeout);
            } catch (\Exception $e) {
            }

            if (!$result) {
                $result['error']   = true;
                $result['message'] = $serviceRemessa->getLastError();
            }

            $this->getJson()->setVariables($result);
        }

        return $this->getJson();
    }

}