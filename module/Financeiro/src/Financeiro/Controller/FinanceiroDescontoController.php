<?php

namespace Financeiro\Controller;

use VersaSpine\Controller\AbstractCoreController;

class FinanceiroDescontoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request                   = $this->getRequest();
        $paramsGet                 = $request->getQuery()->toArray();
        $paramsPost                = $request->getPost()->toArray();
        $serviceFinanceiroDesconto = new \Financeiro\Service\FinanceiroDesconto($this->getEntityManager());

        $result = $serviceFinanceiroDesconto->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceFinanceiroDesconto = new \Financeiro\Service\FinanceiroDesconto($this->getEntityManager());
        $serviceFinanceiroDesconto->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceFinanceiroDesconto = new \Financeiro\Service\FinanceiroDesconto(
                $this->getEntityManager(),
                $this->getServiceManager()->get('Config')
            );

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceFinanceiroDesconto->getDataForDatatables($dataPost);
            $this->getJson()->setVariables($result);
        }

        return $this->getJson();
    }

    public function addAction($descontoId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados                  = array();
        $serviceFinanceiroDesconto = new \Financeiro\Service\FinanceiroDesconto($this->getEntityManager());

        if ($descontoId) {
            $arrDados = $serviceFinanceiroDesconto->getArray($descontoId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceFinanceiroDesconto->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de desconto salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    if ($serviceFinanceiroDesconto->getLastError()) {
                        $this->flashMessenger()->addWarningMessage($serviceFinanceiroDesconto->getLastError());
                    }

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceFinanceiroDesconto->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceFinanceiroDesconto->getLastError());
                }
            }
        }

        $serviceFinanceiroDesconto->formataDadosPost($arrDados);
        $serviceFinanceiroDesconto->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function viewAction()
    {
        $descontoId = $this->params()->fromRoute("id", 0);
        $request    = $this->getRequest();
        $ajax       = false;

        $arrDados = array_merge_recursive($request->getPost()->toArray(), $request->getQuery()->toArray());

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $serviceFinanceiroDesconto = new \Financeiro\Service\FinanceiroDesconto($this->getEntityManager());

        $arrDados = $serviceFinanceiroDesconto->getArray($descontoId);

        if (empty($arrDados)) {
            $this->getView()->setVariable("erro", false);

            if (!$ajax) {
                return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
            }

            return $this->getView();
        }

        $serviceFinanceiroDesconto->formataDadosPost($arrDados);
        $serviceFinanceiroDesconto->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);

        return $this->getView();
    }

    public function editAction()
    {
        $descontoId = $this->params()->fromRoute("id", 0);

        if (!$descontoId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($descontoId);
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceFinanceiroDesconto = new \Financeiro\Service\FinanceiroDesconto($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceFinanceiroDesconto->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceFinanceiroDesconto->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}
?>