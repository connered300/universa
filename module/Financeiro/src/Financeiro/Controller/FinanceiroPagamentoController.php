<?php

namespace Financeiro\Controller;

use VersaSpine\Controller\AbstractCoreController;

class FinanceiroPagamentoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function indexAction()
    {
        return $this->redirect()->toRoute($this->getRoute(), array('controller' => 'painel'));
    }

    public function validaCartaoAction()
    {
        $serviceFinanceiroCartao = new \Financeiro\Service\FinanceiroCartao($this->getEntityManager());

        $request    = $this->getRequest();
        $paramsGet  = $request->getQuery()->toArray();
        $paramsPost = $request->getPost()->toArray();
        $param      = array_merge($paramsGet, $paramsPost);
        $retorno    = false;

        if ($request->isPost()) {
            $retorno = $serviceFinanceiroCartao->validaCartao($param['cartaoNumero'], $param['cartaoCvv']);
        }

        $this->getJson()->setVariable("retorno", $retorno);

        return $this->getJson();
    }

    public function pagamentoCartaoRecorrenciaAction()
    {
        $arrConfig           = $this->getServiceManager()->get('Config');
        $servicePagamento    = new \Financeiro\Service\FinanceiroPagamento($this->getEntityManager(), $arrConfig);
        $serviceAlunoCurso   = new \Matricula\Service\AcadgeralAlunoCurso($this->getEntityManager(), $arrConfig);
        $serviceFilaMaxipago = new \Sistema\Service\SisFilaMaxipago($this->getEntityManager(), $arrConfig);
        $serviceCartao       = new \Financeiro\Service\FinanceiroCartao($this->getEntityManager(), $arrConfig);

        /** @var \Zend\Http\Request $request */
        $request = $this->getRequest();
        $param   = array_merge($request->getQuery()->toArray(), $request->getPost()->toArray());
        $retorno = true;

        try {
            if (!$param['token']) {
                throw new \Exception("Token de acesso não informado!");
            }

            $alunoCurso = $serviceFilaMaxipago->retornaAlunoToken($param['token']);

            /** @var $objAlunoCurso \Matricula\Entity\AcadgeralAlunoCurso */
            $objAlunoCurso = $serviceAlunoCurso->getRepository()->findOneBy(['alunocursoId' => $alunoCurso]);

            if (!$objAlunoCurso) {
                throw new \Exception("Aluno não localizado, tenta novamente!");
            }

            $cpf       = $objAlunoCurso->getAluno()->getPes()->getPesCpf();
            $rotaSaida = $this->url()->fromRoute(
                'financeiro/default',
                array('controller' => 'painel', 'action' => 'titulos'),
                array('query' => ['pesCpf' => $cpf])
            );

            $titulosParaRecorrencia = $serviceFilaMaxipago->retornarTitulosRecorrencia(
                $objAlunoCurso->getAlunocursoId()
            );

            if (!$titulosParaRecorrencia) {
                throw new \Exception("Não há títulos para recorrência!");
            }

            $param['titulosParaRecorrencia'] = $titulosParaRecorrencia;

            if ($request->isPost()) {
                if ($param['cartaoNumero'] && $param['cartaoCvv']) {
                    $arrValidaCartao = $serviceCartao->validaCartao($param['cartaoNumero'], $param['cartaoCvv']);

                    if ($arrValidaCartao['erro']) {
                        $this->getJson()->setVariable('erro', $arrValidaCartao);

                        return $this->getJson();
                    }

                    if (!$servicePagamento->pagamentoRecorrenciaCartao($param)) {
                        $this->getJson()->setVariable("msgErro", ['erro' => $servicePagamento->getLastError()]);

                        return $this->getJson();
                    }
                } elseif (!$serviceFilaMaxipago->cancelaRecorrencia($titulosParaRecorrencia)) {
                    $this->getJson()->setVariable("msgErro", ['erro' => $servicePagamento->getLastError()]);

                    return $this->getJson();
                }

                $this->flashMessenger()->addSuccessMessage("Forma de pagamento salva com sucesso!");

                $this->getJson()->setVariable('rotaSaida', '/financeiro/painel/titulos?pesCpf=' . $cpf);

                return $this->getJson();
            }

            $arrParam['filter'] = ['alunoCurso' => $alunoCurso, 'falhaPagamento' => 'true'];
            $arrParam['order']  = []['tipotitulo_id'];
            $arrDataTables      = $serviceFilaMaxipago->sisFilaMaxiPagoDataTable($arrParam);

            $this->getView()->setTemplate('/financeiro/financeiro-pagamento/recorrencia-cartao');
            $this->getView()->setVariables(
                [
                    'token'        => $param['token'],
                    'arrDataTable' => $arrDataTables,
                    'alunoNome'    => $objAlunoCurso->getAluno()->getPes()->getPes()->getPesNome(),
                    'alunoCurso'   => $objAlunoCurso->getCursocampus()->getCurso()->getCursoNome()
                ]
            );

            $this->getJson()->setVariable(['rotaSaida'], $rotaSaida);
        } catch (\Exception $ex) {
            if ($ex->getMessage()) {
                $this->flashMessenger()->addErrorMessage($ex->getMessage());
            }

            $this->getView()->setTemplate('/financeiro/painel/titulos');
        }

        return $this->getView();
    }

}
?>