<?php

namespace Financeiro\Controller;

use VersaSpine\Controller\AbstractCoreController;

class FinanceiroFiltroController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request                 = $this->getRequest();
        $paramsGet               = $request->getQuery()->toArray();
        $paramsPost              = $request->getPost()->toArray();
        $serviceFinanceiroFiltro = new \Financeiro\Service\FinanceiroFiltro($this->getEntityManager());

        $result = $serviceFinanceiroFiltro->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceFinanceiroFiltro = new \Financeiro\Service\FinanceiroFiltro($this->getEntityManager());
        $serviceFinanceiroFiltro->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceFinanceiroFiltro = new \Financeiro\Service\FinanceiroFiltro($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceFinanceiroFiltro->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function addAction($filtroId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados                = array();
        $serviceFinanceiroFiltro = new \Financeiro\Service\FinanceiroFiltro($this->getEntityManager());

        if ($filtroId) {
            $arrDados = $serviceFinanceiroFiltro->getArray($filtroId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceFinanceiroFiltro->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de filtro salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceFinanceiroFiltro->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceFinanceiroFiltro->getLastError());
                }
            }
        }

        $serviceFinanceiroFiltro->formataDadosPost($arrDados);
        $serviceFinanceiroFiltro->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $filtroId = $this->params()->fromRoute("id", 0);

        if (!$filtroId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($filtroId);
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceFinanceiroFiltro = new \Financeiro\Service\FinanceiroFiltro($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceFinanceiroFiltro->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceFinanceiroFiltro->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}
?>