<?php

namespace Financeiro\Controller;

use Sistema\Service\SisConfig;
use VersaSpine\Controller\AbstractCoreController;

class PainelController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function indexAction()
    {
        $arrConfig            = $this->getServiceManager()->get('Config');
        $serviceSisConfig     = new \Sistema\Service\SisConfig($this->getEntityManager(), $arrConfig);
        $serviceMeioPagamento = new \Financeiro\Service\FinanceiroMeioPagamento($this->getEntityManager());
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEntityManager(), $arrConfig);
        $serviceTitulo        = new \Financeiro\Service\FinanceiroTitulo($this->getEntityManager(), $arrConfig);
        $serviceDesconto      = new \Financeiro\Service\FinanceiroDesconto($this->getEntityManager(), $arrConfig);
        $serviceTituloConfig  = new \Financeiro\Service\FinanceiroTituloConfig($this->getEntityManager());

        $serviceTitulo->setarDependenciasView($this->getView());
        $serviceDesconto->setarDependenciasView($this->getView());

        $arrPesId = $this->params()->fromRoute("id", 0);

        $arrPessoas = [];

        if ($arrPesId) {
            $arrPesId      = explode(',', $arrPesId);
            $servicePessoa = new \Pessoa\Service\Pessoa($this->getEntityManager());

            foreach ($arrPesId as $pesId) {
                $arrPessoasPesquisa = $servicePessoa->pesquisaForJson(['somentePesId' => $pesId]);

                foreach ($arrPessoasPesquisa as $arrAluno) {
                    if ((int)$arrAluno['pes_id'] == (int)$pesId) {
                        $arrPessoas[$arrAluno['pes_id']] = $arrAluno;
                        break;
                    }
                }
            }
        }

        $permissaoRenegociacao  = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Financeiro\Controller\Painel',
            'renegociacao'
        );
        $permissaoPagamento     = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Financeiro\Controller\Painel',
            'pagamento'
        );
        $permissaoCancelamento  = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Financeiro\Controller\Painel',
            'cancelar-titulo'
        );
        $permissaoNovoTitulo    = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Financeiro\Controller\Painel',
            'novo-titulo'
        );
        $permissaoEnvioTitulos  = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Financeiro\Controller\Painel',
            'enviar-titulos'
        );
        $permissaoEstornoTitulo = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Financeiro\Controller\Painel',
            'estornar-titulo'
        );

        $permissaoAtualizarTitulo = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Financeiro\Controller\Painel',
            'atualizar-titulo'
        );

        $permissaoAlterarObservacao = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Pessoa\Controller\Pessoa',
            'alterar-observacoes'
        );

        $permissaoAdicionarDesconto = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Financeiro\Controller\FinanceiroDesconto',
            'add'
        );

        $permissaoEditarDesconto = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Financeiro\Controller\FinanceiroDesconto',
            'edit'
        );

        $permissaoExcluirDesconto = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Financeiro\Controller\FinanceiroDesconto',
            'remove'
        );

        $arrPermissoes = array(
            'renegociacao'      => ($permissaoRenegociacao ? 1 : 0),
            'pagamento'         => ($permissaoPagamento ? 1 : 0),
            'novoTitulo'        => ($permissaoNovoTitulo ? 1 : 0),
            'cancelarTitulo'    => ($permissaoCancelamento ? 1 : 0),
            'envioTitulos'      => ($permissaoEnvioTitulos ? 1 : 0),
            'estornarTitulo'    => ($permissaoEstornoTitulo ? 1 : 0),
            'atualizarTitulo'   => ($permissaoAtualizarTitulo ? 1 : 0),
            'alterarObservacao' => ($permissaoAlterarObservacao ? 1 : 0),
            'adicionarDesconto' => ($permissaoAdicionarDesconto ? 1 : 0),
            'alterarDesconto'   => ($permissaoEditarDesconto ? 1 : 0),
            'excluirDesconto'   => ($permissaoExcluirDesconto ? 1 : 0)
        );

        $boletoAtivado                        = $serviceSisConfig->localizarChave('BOLETO_ATIVO');
        $boletoRemessaAtivada                 = $serviceSisConfig->localizarChave('BOLETO_REMESSA_ATIVADA');
        $pagseguroAtivado                     = $serviceSisConfig->localizarChave('PAGSEGURO_ATIVO');
        $smsAtivado                           = $serviceSisConfig->localizarChave('SMS_ATIVO');
        $meioPagamentoPadrao                  =
            $serviceSisConfig->localizarChave('FINANCEIRO_MEIO_PAGAMENTO_PADRAO');
        $financeiroMaxParcelas                = $serviceSisConfig->localizarChave('FINANCEIRO_MAX_PARCELAS');
        $financeiroEmissaoCarneAtivado        = $serviceSisConfig->localizarChave('FINANCEIRO_EMISSAO_CARNE_ATIVADO');
        $financeiroMaxPercentualDesconto      =
            $serviceSisConfig->localizarChave('FINANCEIRO_MAX_PERCENTUAL_DESCONTO');
        $financeiroFiltroPadrao               = $serviceSisConfig->localizarChave('FINANCEIRO_FILTRO_PADRAO');
        $financeiroSelecaoMultipla            = $serviceSisConfig->localizarChave('FINANCEIRO_SELECAO_MULTIPLA');
        $financeiroExibicaoCodigoPessoa       =
            $serviceSisConfig->localizarChave('FINANCEIRO_EXIBICAO_CODIGO_PESSOA');
        $financeiroExibicaoCPFPessoa          =
            $serviceSisConfig->localizarChave('FINANCEIRO_BUSCA_EXIBICAO_CPF_PESSOA');
        $financeiroExibicaoPessoaJuridica     =
            $serviceSisConfig->localizarChave('FINANCEIRO_EXIBICAO_PESSOA_JURIDICA');
        $financeiroAtualizacaoTituloAberto    =
            $serviceSisConfig->localizarChave('FINANCEIRO_ATUALIZACAO_TITULO_ABERTO');
        $financeiroComprovanteImpressaoDireta =
            $serviceSisConfig->localizarChave('FINANCEIRO_COMPROVANTE_IMPRESSAO_DIRETA');
        $financeiroBuscaExibeCodigoAluno      =
            $serviceSisConfig->localizarChave('FINANCEIRO_BUSCA_EXIBICAO_CODIGO_ALUNO');
        $financeiroExibicaoSituacaoAluno      = $serviceSisConfig->localizarChave('FINANCEIRO_EXIBICAO_SITUACAO_ALUNO');
        $financeiroExibicaoPeriodoAluno       = $serviceSisConfig->localizarChave('FINANCEIRO_EXIBICAO_PERIODO_ALUNO');
        $financeiroExibicaoTurmaAluno         = $serviceSisConfig->localizarChave('FINANCEIRO_EXIBICAO_TURMA_ALUNO');
        $financeiroEnviarTitulosCopiaAgente   =
            $serviceSisConfig->localizarChave('FINANCEIRO_ENVIAR_TITULOS_COPIA_AGENTE');

        $this->getView()
            ->setVariable('pagseguroAtivado', $pagseguroAtivado)
            ->setVariable('boletoAtivado', $boletoAtivado)
            ->setVariable('boletoRemessaAtivada', $boletoRemessaAtivada)
            ->setVariable('smsAtivado', $smsAtivado)
            ->setVariable('meioPagamentoPadrao', $meioPagamentoPadrao)
            ->setVariable('financeiroMaxParcelas', $financeiroMaxParcelas)
            ->setVariable('financeiroEmissaoCarneAtivado', $financeiroEmissaoCarneAtivado)
            ->setVariable('financeiroMaxPercentualDesconto', $financeiroMaxPercentualDesconto)
            ->setVariable('financeiroFiltroPadrao', $financeiroFiltroPadrao)
            ->setVariable('financeiroSelecaoMultipla', $financeiroSelecaoMultipla)
            ->setVariable('financeiroExibicaoCodigoPessoa', $financeiroExibicaoCodigoPessoa)
            ->setVariable('financeiroExibicaoCPFPessoa', $financeiroExibicaoCPFPessoa)
            ->setVariable('financeiroExibicaoPessoaJuridica', $financeiroExibicaoPessoaJuridica)
            ->setVariable('financeiroAtualizacaoTituloAberto', $financeiroAtualizacaoTituloAberto)
            ->setVariable('financeiroComprovanteImpressaoDireta', $financeiroComprovanteImpressaoDireta)
            ->setVariable('financeiroBuscaExibeCodigoAluno', $financeiroBuscaExibeCodigoAluno)
            ->setVariable('financeiroExibicaoSituacaoAluno', $financeiroExibicaoSituacaoAluno)
            ->setVariable('financeiroExibicaoPeriodoAluno', $financeiroExibicaoPeriodoAluno)
            ->setVariable('financeiroExibicaoTurmaAluno', $financeiroExibicaoTurmaAluno)
            ->setVariable('financeiroEnviarTitulosCopiaAgente', $financeiroEnviarTitulosCopiaAgente)
            ->setVariable('arrPessoas', array_values($arrPessoas))
            ->setVariable('arrPermissoes', $arrPermissoes)
            ->setVariable('arrMeioPagamento', $serviceMeioPagamento->getArrSelect2())
            ->setVariable(
                'constDescontoIncentivoAcumulativoSim',
                $serviceTituloConfig::TITULOCONF_DESCONTO_INCENTIVO_ACUMULATIVO_SIM
            );

        return $this->getView();
    }

    public function comprovanteAction()
    {
        $arrConfig                            = $this->getServiceManager()->get('Config');
        $serviceSisConfig                     = new \Sistema\Service\SisConfig($this->getEntityManager(), $arrConfig);
        $financeiroComprovanteImpressaoDireta = $serviceSisConfig->localizarChave(
            'FINANCEIRO_COMPROVANTE_IMPRESSAO_DIRETA'
        );

        /** @var \Zend\Http\Request $request */
        $request  = $this->getRequest();
        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo(
            $this->getEntityManager(),
            $this->getServiceManager()->get('Config')
        );

        $this->getView()->setTemplate('/financeiro/painel/comprovante-pagamento');

        $arrTitulos = explode(',', $arrDados['tituloId']);

        $result = $serviceFinanceiroTitulo->retornaDadosComprovante($arrTitulos);

        if ($result) {
            $this->getView()->setVariables(
                [
                    'titulo'                               => $result,
                    'financeiroComprovanteImpressaoDireta' => $financeiroComprovanteImpressaoDireta
                ]
            );
            $this->getView()->setTerminal(true);

            return $this->getView();
        }

        $this->getView()->setTerminal(true);

        return $this->getView();
    }

    public function renegociacaoAction()
    {
        /** @var \Zend\Http\Request $request */
        $request  = $this->getRequest();
        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );
        $this->setView(new \Zend\View\Model\JsonModel());

        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo(
            $this->getEntityManager(),
            $this->getServiceManager()->get('Config')
        );

        $renegociacaoEfetuada = $serviceFinanceiroTitulo->renegociarTitulos($arrDados);

        if ($renegociacaoEfetuada) {
            $this->getView()->setVariable("erro", false);
            $mensagem = 'Renegociação efetuada com sucesso!';
            $this->getView()->setVariable("mensagem", $mensagem);
        } else {
            $this->getView()->setVariable("erro", true);
            $this->getView()->setVariable("mensagem", $serviceFinanceiroTitulo->getLastError());
        }

        return $this->getView();
    }

    public function atualizarTituloAction()
    {
        /** @var \Zend\Http\Request $request */
        $request  = $this->getRequest();
        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );
        $this->setView(new \Zend\View\Model\JsonModel());
        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo(
            $this->getEntityManager(),
            $this->getServiceManager()->get('config')
        );

        $atualizarTitulos = $serviceFinanceiroTitulo->atualizarTitulo($arrDados);

        if (is_array($atualizarTitulos)) {
            $this->getView()->setVariable("erro", false);
            $mensagem = $atualizarTitulos['message'] ? $atualizarTitulos['message'] : 'Títulos atualizados!';
            $this->getView()->setVariable("mensagem", $mensagem);
            $this->getView()->setVariable("tituloId", $atualizarTitulos['tituloId']);
        } else {
            $this->getView()->setVariable("erro", true);
            $this->getView()->setVariable("mensagem", $serviceFinanceiroTitulo->getLastError());
        }

        return $this->getView();
    }

    public function pagamentoAction()
    {
        /** @var \Zend\Http\Request $request */
        $request  = $this->getRequest();
        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );
        $this->setView(new \Zend\View\Model\JsonModel());

        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo(
            $this->getEntityManager(),
            $this->getServiceManager()->get('Config')
        );

        $pagamentoEfetuado = $serviceFinanceiroTitulo->pagamentoTitulosManual($arrDados);

        if ($pagamentoEfetuado) {
            $this->getView()->setVariable("erro", false);
            $mensagem = 'Pagamento efetuado com sucesso!';
            $this->getView()->setVariable("mensagem", $mensagem);
            $this->getView()->setVariable("tituloId", $pagamentoEfetuado);
        } else {
            $this->getView()->setVariable("erro", true);
            $this->getView()->setVariable("mensagem", $serviceFinanceiroTitulo->getLastError());
        }

        return $this->getView();
    }

    public function novoTituloAction()
    {
        /** @var \Zend\Http\Request $request */
        $request  = $this->getRequest();
        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );
        $this->setView(new \Zend\View\Model\JsonModel());

        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo(
            $this->getEntityManager(),
            $this->getServiceManager()->get('Config')
        );
        $pagamentoEfetuado       = $serviceFinanceiroTitulo->criarNovosTitulos($arrDados);

        if ($pagamentoEfetuado) {
            $this->getView()->setVariable("erro", false);
            $mensagem = 'Geração de título(s) efetuada com sucesso!';
            $this->getView()->setVariable("mensagem", $mensagem);
        } else {
            $this->getView()->setVariable("erro", true);
            $this->getView()->setVariable("mensagem", $serviceFinanceiroTitulo->getLastError());
        }

        return $this->getView();
    }

    public function cancelarTituloAction()
    {
        /** @var \Zend\Http\Request $request */
        $request  = $this->getRequest();
        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );
        $this->setView(new \Zend\View\Model\JsonModel());

        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo(
            $this->getEntityManager(),
            $this->getServiceManager()->get('Config')
        );
        $pagamentoEfetuado       = $serviceFinanceiroTitulo->cancelaTitulo(
            $arrDados['titulos'],
            $arrDados['observacao'],
            false,
            true
        );

        if ($pagamentoEfetuado) {
            $this->getView()->setVariable("erro", false);
            $mensagem = 'Título(s) cancelados com sucesso!';
            $this->getView()->setVariable("mensagem", $mensagem);
        } else {
            $this->getView()->setVariable("erro", true);
            $this->getView()->setVariable("mensagem", $serviceFinanceiroTitulo->getLastError());
        }

        return $this->getView();
    }

    public function enviarTitulosAction()
    {
        /** @var \Zend\Http\Request $request */
        $request  = $this->getRequest();
        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );
        $this->setView(new \Zend\View\Model\JsonModel());

        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo(
            $this->getEntityManager(),
            $this->getServiceManager()->get('Config')
        );

        $serviceSisConfig = new SisConfig($this->getEntityManager(), $this->getServiceManager()->get('config'));

        $pagseguroAtivo       = $serviceSisConfig->localizarChave('PAGSEGURO_ATIVO');
        $boletoAtivado        = $serviceSisConfig->localizarChave('BOLETO_ATIVO');
        $boletoRemessaAtivada = $serviceSisConfig->localizarChave('BOLETO_REMESSA_ATIVADA');

        $urlBoleto    = false;
        $urlPagseguro = false;

        if ($boletoAtivado) {
            $urlBoleto = $this->url()->fromRoute(
                'boleto-bancario/default',
                array('controller' => 'boleto', 'action' => 'exibe')
            );

            $urlBoleto = $this->getBasePath() . $urlBoleto;
        }

        if ($pagseguroAtivo) {
            $urlPagseguro = $this->url()->fromRoute(
                'financeiro/default',
                array('controller' => 'financeiro-titulo', 'action' => 'pagseguro-cobranca')
            );

            $urlPagseguro = $this->getBasePath() . $urlPagseguro;
        }

        $arrDados['enviarEmailComCopia'] = $arrDados['enviarEmailComCopia'] ? true : false;

        $envioEfetuado = $serviceFinanceiroTitulo->enviarTitulos(
            $arrDados['titulos'],
            $arrDados['via'],
            $this->getServiceLocator(),
            array(
                'urlBoleto'            => $urlBoleto,
                'urlPagseguro'         => $urlPagseguro,
                'boletoRemessaAtivada' => $boletoRemessaAtivada,
                'enviarEmailComCopia'  => $arrDados['enviarEmailComCopia'],
            )
        );

        if ($envioEfetuado) {
            $this->getView()->setVariable("erro", false);
            $mensagem = 'Título(s) enviados com sucesso!';
            $this->getView()->setVariable("mensagem", $mensagem);
        } else {
            $this->getView()->setVariable("erro", true);
            $this->getView()->setVariable("mensagem", $serviceFinanceiroTitulo->getLastError());
        }

        return $this->getView();
    }

    public function estornarTituloAction()
    {
        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo(
            $this->getEntityManager(),
            $this->getServiceManager()->get('Config')
        );

        $arrConfig = $this->getServiceManager()->get('Config');

        $request  = $this->getRequest();
        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        $estornarTitulo = $serviceFinanceiroTitulo->estornarTitulo($arrDados );

        if (is_array($estornarTitulo)) {
            $this->getJson()->setVariable("erro", false);
            $mensagem = 'Titulo estornado com Sucesso';
            $this->getJson()->setVariable("mensagem", $mensagem);
            $this->getJson()->setVariable("tituloId", $estornarTitulo['tituloId']);
        } else {
            $this->getJson()->setVariable("erro", true);
            $this->getJson()->setVariable("mensagem", $serviceFinanceiroTitulo->getLastError());
        }

        return $this->getJson();
    }

    public function titulosAction()
    {
        $request  = $this->getRequest();
        $arrDados = [];
        $arrDados = array_merge(
            $arrDados,
            array_merge(
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                ),
                $request->getQuery()->toArray()
            )
        );

        $arrConfig = $this->getServiceManager()->get('Config');

        $servicePessoa          = new \Pessoa\Service\Pessoa($this->getEntityManager());
        $serviceSisConfig       = new \Sistema\Service\SisConfig($this->getEntityManager(), $arrConfig);
        $serviceAcadResponsavel = new \Matricula\Service\AcadgeralResponsavel($this->getEntityManager());
        $serviceFinanceiro      = new\Financeiro\Service\FinanceiroTitulo($this->getEntityManager());

        if ($arrDados['pesCpf']) {
            $digitos                 = strlen(preg_replace("/[^0-9]/", "", $arrDados['pesCpf']));
            $digitosQuantidadeDoMeio = strlen($arrDados['pesCpf']);

            if (($digitos != 11 AND $digitosQuantidadeDoMeio == 14) OR ($digitos != 18 AND $digitosQuantidadeDoMeio == 18)) {
                $this->flashMessenger()->addErrorMessage(
                    strlen($arrDados['pesCpf']) == 14 ? 'CPF Inválido!' : 'CNPJ Inválido!'
                );

                return $this->getView();
            }

            $pesId = $servicePessoa->buscaPessoaPorCPFOuCNPJ($arrDados['pesCpf']);

            if (!$pesId) {
                $this->flashMessenger()->addErrorMessage('Registro não encontrado!');
            } else {
                $arrDados          = $servicePessoa->getArray($pesId);
                $pesIdRelacionados = $serviceAcadResponsavel->retornaAlunosPorResponsavel($pesId);

                if ($pesIdRelacionados) {
                    $pesId .= ',' . $pesIdRelacionados;
                }

                $estados   = [];
                $estados[] = $serviceFinanceiro::TITULO_ESTADO_ABERTO;
                $estados[] = $serviceFinanceiro::TITULO_ESTADO_PAGO;

                $arrDataTables = $serviceFinanceiro->getDataForDatatables(
                    [
                        'filter'  => ['pesId' => $pesId, 'tituloEstado' => $estados],
                        'length'  => -1,
                        'orderBy' => "  titulo_estado desc, titulo_data_vencimento ASC, titulo_id ASC "
                    ]
                );

                $this->getView()->setVariable('dataTables', $arrDataTables);
                $this->getView()->setVariable('pagSeguroAtivo', $serviceSisConfig->localizarChave('PAGSEGURO_ATIVO'));
                $this->getView()->setVariable(
                    'boletoRemessaAtivada',
                    $serviceSisConfig->localizarChave('BOLETO_REMESSA_ATIVADA')
                );
                $this->getView()->setVariable('boletoAtivo', $serviceSisConfig->localizarChave('BOLETO_ATIVO'));
                $this->getView()->setVariable('arrDados', $arrDados);
                $this->getView()->setTemplate($this->getTemplateToRoute('titulos-dataTables', 'painel'));
            }
        }

        return $this->getView();
    }
}

?>