<?php

namespace Financeiro\Controller;

use VersaSpine\Controller\AbstractCoreController;

class FinanceiroChequeController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request                 = $this->getRequest();
        $paramsGet               = $request->getQuery()->toArray();
        $paramsPost              = $request->getPost()->toArray();
        $serviceFinanceiroCheque = new \Financeiro\Service\FinanceiroCheque($this->getEntityManager());

        $result = $serviceFinanceiroCheque->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        if ($paramsPost['chequeBanco'] AND $paramsPost['chequeAgencia'] AND
            $paramsPost['chequeConta'] AND $paramsPost['chequeNum']
        ) {
            $result = $result[0] ? $result[0] : [];

            if ($result) {
                $arrSaldoCheque = $serviceFinanceiroCheque->retornaSaldoCheque($result['cheque_id']);

                if ($arrSaldoCheque) {
                    $result = array_merge($result, $arrSaldoCheque);
                }
            }
        };

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $arrConfig            = $this->getServiceManager()->get('Config');
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEntityManager(), $arrConfig);

        $permissaoEditarCheque    = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Financeiro\Controller\FinanceiroCheque',
            'edit'
        );
        $permissaoAdicionarCheque = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Financeiro\Controller\FinanceiroCheque',
            'add'
        );

        $serviceFinanceiroCheque = new \Financeiro\Service\FinanceiroCheque($this->getEntityManager());
        $serviceFinanceiroCheque->setarDependenciasView($this->getView());

        $this->getView()->setVariables(
            ["permissaoEditaCheque" => $permissaoEditarCheque, "permissaoAdicionarCheque" => $permissaoAdicionarCheque]
        );

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceFinanceiroCheque = new \Financeiro\Service\FinanceiroCheque($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceFinanceiroCheque->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function addAction($chequeId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados                = array();
        $serviceFinanceiroCheque = new \Financeiro\Service\FinanceiroCheque($this->getEntityManager());

        if ($chequeId) {
            $arrDados = $serviceFinanceiroCheque->getArray($chequeId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceFinanceiroCheque->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de cheque salvo!';

                if ($arrDados['editaObservacao']) {
                    $this->getJson()->setVariable("erro", false);
                    $this->getJson()->setVariable("mensagem", $mensagem);

                    return $this->getJson();
                }

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);
                $mensagem = $serviceFinanceiroCheque->getLastError();

                if ($arrDados['editaObservacao']) {
                    $this->getJson()->setVariable("erro", true);
                    $this->getJson()->setVariable("mensagem", $mensagem);

                    return $this->getJson();
                }

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceFinanceiroCheque->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            }
        }

        $serviceFinanceiroCheque->formataDadosPost($arrDados);
        $serviceFinanceiroCheque->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $request = $this->getRequest();

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        $chequeId = $arrDados['id'] ? $arrDados['id'] : $this->params()->fromRoute("id", 0);

        if (!$chequeId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($chequeId);
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceFinanceiroCheque = new \Financeiro\Service\FinanceiroCheque($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceFinanceiroCheque->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceFinanceiroCheque->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }

    public function retornaDadosChequeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceFinanceiroChque = new \Financeiro\Service\FinanceiroCheque($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();

            $result = $serviceFinanceiroChque->getDataTablesInformacoes($dataPost);

            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
            $this->getJson()->setVariable("somatorio", $result['totalizacoes']);
        }

        return $this->getJson();
    }
}
?>