<?php

namespace Financeiro\Controller;

use Acesso\Adapter\Acesso;
use VersaSpine\Controller\AbstractCoreController;

class FinanceiroTituloController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request                 = $this->getRequest();
        $paramsGet               = $request->getQuery()->toArray();
        $paramsPost              = $request->getPost()->toArray();
        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo($this->getEntityManager());

        $result = $serviceFinanceiroTitulo->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo($this->getEntityManager());
        $serviceFinanceiroTitulo->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();

            $result = $serviceFinanceiroTitulo->getDataForDatatables($dataPost);

            $this->getJson()->setVariables($result);
        }

        return $this->getJson();
    }

    public function editAction()
    {
        $tituloId = $this->params()->fromRoute("id", 0);

        if (!$tituloId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($tituloId);
    }

    public function addAction($tituloId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados                = array();
        $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo($this->getEntityManager());

        if ($tituloId) {

            if($serviceFinanceiroTitulo->tituloUsaMaxiPago($tituloId)){
                $this->flashMessenger()->addErrorMessage($serviceFinanceiroTitulo->getLastError());
                return false;
            }

            $arrDados = $serviceFinanceiroTitulo->getArray($tituloId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceFinanceiroTitulo->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de título salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceFinanceiroTitulo->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceFinanceiroTitulo->getLastError());
                }
            }
        }

        $serviceFinanceiroTitulo->formataDadosPost($arrDados);
        $serviceFinanceiroTitulo->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceFinanceiroTitulo->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceFinanceiroTitulo->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }

    public function tituloInformacoesAction()
    {
        $tituloId = $this->params()->fromRoute("id", 0);
        $erro     = false;
        $mensagem = '';
        $arrDados = array();

        if ($tituloId) {
            $serviceFinanceiroTitulo = new \Financeiro\Service\FinanceiroTitulo($this->getEntityManager());

            $arrDados = $serviceFinanceiroTitulo->retornaInformacoesTitulo($tituloId);

            if (empty($arrDados)) {
                $erro     = true;
                $mensagem = $serviceFinanceiroTitulo->getLastError();
            }
        } else {
            $erro     = true;
            $mensagem = 'Título inválido!';
        }

        $this->getJson()->setVariable('arrDados', $arrDados);
        $this->getJson()->setVariable('erro', $erro);
        $this->getJson()->setVariable('mensagem', $mensagem);

        return $this->getJson();
    }

    public function pagseguroRetornoAction()
    {
        $mensagem = "Obrigado pelo pagamento, em breve entraremos em contato.";
        $this->getView()->setVariable('erro', false);
        $this->getView()->setVariable('mensagem', $mensagem);

        $this->layout('layout/simples');

        return $this->getView();
    }

    public function pagseguroNotificacaoAction()
    {
        $serviceIntegracaoPagseguro = new \Financeiro\Service\IntegracaoPagseguro(
            $this->getEntityManager(), $this->getServiceManager()->get('Config')
        );

        if ($serviceIntegracaoPagseguro->verificarNotificacoes()) {
            $erro     = false;
            $mensagem = 'Pagamento processado com sucesso!';
        } else {
            $erro     = true;
            $mensagem = 'Não foi possível processar pagamento. ' . $serviceIntegracaoPagseguro->getLastError();
        }

        $this->getJson()->setVariable('erro', $erro);
        $this->getJson()->setVariable('mensagem', $mensagem);

        return $this->getJson();
    }

    public function pagseguroCobrancaAction()
    {
        $erro     = false;
        $mensagem = '';
        $arrDados = array();
        $tituloId = $this->params()->fromRoute("id", 0);

        if ($tituloId) {
            $serviceIntegracaoPagseguro = new \Financeiro\Service\IntegracaoPagseguro(
                $this->getEntityManager(), $this->getServiceManager()->get('Config')
            );
            $serviceSisConfig           = new \Sistema\Service\SisConfig(
                $this->getEntityManager(), $this->getServiceManager()->get('config')
            );

            $pagseguroAtivo       = $serviceSisConfig->localizarChave('PAGSEGURO_ATIVO');

            $urlNotificacaoPagseguro = $this->url()->fromRoute(
                'financeiro/default',
                ['controller' => 'financeiro-titulo', 'action' => 'pagseguro-notificacao']
            );
            $urlRetornoPagseguro     = $this->url()->fromRoute(
                'financeiro/default',
                ['controller' => 'financeiro-titulo', 'action' => 'pagseguro-retorno']
            );

            if ($pagseguroAtivo) {
                $arrDados = $serviceIntegracaoPagseguro->criaCobranca(
                    $tituloId,
                    $this->getBasePath(),
                    $urlRetornoPagseguro,
                    $urlNotificacaoPagseguro
                );

                if (empty($arrDados)) {
                    $erro     = true;
                    $mensagem = 'Falha ao emitir cobrança!<br>' . $serviceIntegracaoPagseguro->getLastError();
                }
            } else {
                $erro     = true;
                $mensagem = 'Pagamento via Pagseguro encontra-se desabilitado!<br>';
            }
        } else {
            $erro     = true;
            $mensagem = 'Título inválido!';
        }

        $this->getView()->setVariable('arrDados', $arrDados);
        $this->getView()->setVariable('erro', $erro);
        $this->getView()->setVariable('mensagem', $mensagem);

        $this->layout('layout/simples');

        return $this->getView();
    }
}
?>