<?php

namespace Avaliacao\Controller;

use VersaSpine\Controller\AbstractCoreController;

class AvaliacaoQuestionarioController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request                      = $this->getRequest();
        $paramsGet                    = $request->getQuery()->toArray();
        $paramsPost                   = $request->getPost()->toArray();
        $serviceAvaliacaoQuestionario = new \Avaliacao\Service\AvaliacaoQuestionario($this->getEntityManager());

        $result = $serviceAvaliacaoQuestionario->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceAvaliacaoQuestionario = new \Avaliacao\Service\AvaliacaoQuestionario($this->getEntityManager());
        $serviceAvaliacaoQuestionario->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAvaliacaoQuestionario = new \Avaliacao\Service\AvaliacaoQuestionario($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceAvaliacaoQuestionario->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function addAction($questionarioId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados                     = array();
        $serviceAvaliacaoQuestionario = new \Avaliacao\Service\AvaliacaoQuestionario($this->getEntityManager());

        if ($questionarioId) {
            $arrDados = $serviceAvaliacaoQuestionario->getArray($questionarioId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceAvaliacaoQuestionario->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de questionario salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceAvaliacaoQuestionario->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceAvaliacaoQuestionario->getLastError());
                }
            }
        }

        $serviceAvaliacaoQuestionario->formataDadosPost($arrDados);
        $serviceAvaliacaoQuestionario->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $questionarioId = $this->params()->fromRoute("id", 0);

        if (!$questionarioId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($questionarioId);
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAvaliacaoQuestionario = new \Avaliacao\Service\AvaliacaoQuestionario($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceAvaliacaoQuestionario->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceAvaliacaoQuestionario->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }

    public function responderAction()
    {
        $questionarioId = $this->params()->fromRoute("id", 0);

        if (!$questionarioId) {
            $erro = "Questionário não informado!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        $serviceConfig                = new \Sistema\Service\SisConfig(
            $this->getEntityManager(),
            $this->getServiceManager()->get('Config')
        );
        $serviceAvaliacaoQuestionario = new \Avaliacao\Service\AvaliacaoQuestionario($this->getEntityManager());

        $arrQuestionarioFormatado = $serviceAvaliacaoQuestionario->formataDadosQuestionario($questionarioId);

        if (!$arrQuestionarioFormatado) {
            $arrQuestionarioFormatado['erro']     = true;
            $arrQuestionarioFormatado['mensagem'] = (
                'Não foi possível carregar questionário!<br>' .
                $serviceAvaliacaoQuestionario->getLastError()
            );
        }

        $this->getView()->setTemplate('avaliacao/avaliacao-questionario/questionario');
        $this->getView()->setVariables($arrQuestionarioFormatado);
        $this->getView()->setVariable(
            'ativarExibicaoGrade',
            $serviceConfig->localizarChave('AVALIACAO_EXIBICAO_GRADE', 0)
        );

        return $this->getView();
    }

    public function salvarQuestionarioAction($arrDados = array())
    {
        $request       = $this->getRequest();
        $erro          = false;
        $ok            = false;
        $erroDescricao = '';

        if ($request->isPost() || $arrDados) {
            $serviceAvaliacaoQuestionario = new \Avaliacao\Service\AvaliacaoQuestionario($this->getEntityManager());

            $dataPost = $arrDados ? $arrDados : $request->getPost()->toArray();

            $questionarioId = $dataPost['questionarioId'];

            if ($dataPost) {
                $ok = $serviceAvaliacaoQuestionario->salvarQuestionario($dataPost);
            }

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceAvaliacaoQuestionario->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('mensagem', $erroDescricao);
            $this->getJson()->setVariable('retorno', $ok);
            $this->getJson()->setVariable('id', $questionarioId);
        }

        return $this->getJson();
    }

    public function importarFromLogAction()
    {
        @ini_set('memory_limit', '-1');
        $serviceAvaliacaoQuestionario = new \Avaliacao\Service\AvaliacaoQuestionario($this->getEntityManager());
        $waitTimeout                  = $serviceAvaliacaoQuestionario->executeQuery(
            'SHOW SESSION VARIABLES LIKE "wait_timeout"'
        )->fetch();
        $interactiveTimeout           = $serviceAvaliacaoQuestionario->executeQuery(
            'SHOW SESSION VARIABLES LIKE "interactive_timeout"'
        )->fetch();
        $waitTimeout                  = $waitTimeout['Value'];
        $interactiveTimeout           = $interactiveTimeout['Value'];

        $serviceAvaliacaoQuestionario->executeQuery('SET @@session.wait_timeout=' . (60 * 60));
        $serviceAvaliacaoQuestionario->executeQuery('SET @@session.interactive_timeout=' . (60 * 60));

        $arrDados = [];
        $arrPes   = [];
        $arrDados = require('./public/log-avaliacao.php');
        $arrPes   = require('./public/log-avaliacao-pessoa.php');
        $arrFalha = [];

        foreach ($arrDados as $arrReq) {
            $dados = $arrReq['Dados'];

            if ($arrReq['Usuario'] == 'leonardoweslei' || $arrReq['Usuario'] == 'admin') {
                continue;
            }

            if (!in_array($dados['pesId'], $arrPes)) {
                continue;
            }

            $dados['forcar']       = 's';
            $dados['deletarAntes'] = 's';

            $ok = $serviceAvaliacaoQuestionario->salvarQuestionario($dados);

            if (!$ok) {
                $arrFalha[] = $arrReq;
            }
        }

        $serviceAvaliacaoQuestionario->executeQuery('SET @@session.wait_timeout=' . $waitTimeout);
        $serviceAvaliacaoQuestionario->executeQuery('SET @@session.interactive_timeout=' . $interactiveTimeout);

        echo var_export($arrFalha, true);
        exit(0);
    }
}
?>