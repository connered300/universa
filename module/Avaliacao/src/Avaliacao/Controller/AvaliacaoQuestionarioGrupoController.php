<?php

namespace Avaliacao\Controller;

use VersaSpine\Controller\AbstractCoreController;

class AvaliacaoQuestionarioGrupoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request                           = $this->getRequest();
        $paramsGet                         = $request->getQuery()->toArray();
        $paramsPost                        = $request->getPost()->toArray();
        $serviceAvaliacaoQuestionarioGrupo = new \Avaliacao\Service\AvaliacaoQuestionarioGrupo(
            $this->getEntityManager()
        );

        $result = $serviceAvaliacaoQuestionarioGrupo->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceAvaliacaoQuestionarioGrupo = new \Avaliacao\Service\AvaliacaoQuestionarioGrupo(
            $this->getEntityManager()
        );
        $serviceAvaliacaoQuestionarioGrupo->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAvaliacaoQuestionarioGrupo = new \Avaliacao\Service\AvaliacaoQuestionarioGrupo(
                $this->getEntityManager()
            );

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceAvaliacaoQuestionarioGrupo->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function addAction($questionarioId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados                          = array();
        $serviceAvaliacaoQuestionarioGrupo = new \Avaliacao\Service\AvaliacaoQuestionarioGrupo(
            $this->getEntityManager()
        );

        if ($questionarioId) {
            $arrDados = $serviceAvaliacaoQuestionarioGrupo->getArray($questionarioId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceAvaliacaoQuestionarioGrupo->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de questionario grupo salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceAvaliacaoQuestionarioGrupo->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceAvaliacaoQuestionarioGrupo->getLastError());
                }
            }
        }

        $serviceAvaliacaoQuestionarioGrupo->formataDadosPost($arrDados);
        $serviceAvaliacaoQuestionarioGrupo->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $questionarioId = $this->params()->fromRoute("id", 0);

        if (!$questionarioId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($questionarioId);
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceAvaliacaoQuestionarioGrupo = new \Avaliacao\Service\AvaliacaoQuestionarioGrupo(
                $this->getEntityManager()
            );

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceAvaliacaoQuestionarioGrupo->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceAvaliacaoQuestionarioGrupo->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}
?>