<?php

namespace Avaliacao\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AvaliacaoQuestaoResposta
 *
 * @ORM\Table(name="avaliacao__questao_resposta")
 * @ORM\Entity
 * @LG\LG(id="questaorespostaId",label="QuestaorespostaDescricao")
 * @Jarvis\Jarvis(title="Listagem de questão resposta",icon="fa fa-table")
 */
class AvaliacaoQuestaoResposta
{
    /**
     * @var integer
     *
     * @ORM\Column(name="questaoresposta_id", type="integer", nullable=false, length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="questaoresposta_id")
     * @LG\Labels\Attributes(text="Questãoresposta")
     * @LG\Querys\Conditions(type="=")
     */
    private $questaorespostaId;

    /**
     * @var \Avaliacao\Entity\AvaliacaoQuestao
     * @ORM\ManyToOne(targetEntity="Avaliacao\Entity\AvaliacaoQuestao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="questao_id", referencedColumnName="questao_id")
     * })
     */
    private $questao;

    /**
     * @var string
     *
     * @ORM\Column(name="questaoresposta_descricao", type="string", nullable=false, length=255)
     * @LG\Labels\Property(name="questaoresposta_descricao")
     * @LG\Labels\Attributes(text="Descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $questaorespostaDescricao;

    /**
     * @var integer
     *
     * @ORM\Column(name="questaoresposta_posicao", type="integer", nullable=true, length=11)
     * @LG\Labels\Property(name="questaoresposta_posicao")
     * @LG\Labels\Attributes(text="Posição")
     * @LG\Querys\Conditions(type="=")
     */
    private $questaorespostaPosicao;

    /**
     * @var string
     *
     * @ORM\Column(name="questaoresposta_gabarito", type="string", nullable=false, length=3)
     * @LG\Labels\Property(name="questaoresposta_gabarito")
     * @LG\Labels\Attributes(text="Gabarito")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $questaorespostaGabarito;

    /**
     * @return integer
     */
    public function getQuestaorespostaId()
    {
        return $this->questaorespostaId;
    }

    /**
     * @param integer $questaorespostaId
     * @return AvaliacaoQuestaoResposta
     */
    public function setQuestaorespostaId($questaorespostaId)
    {
        $this->questaorespostaId = $questaorespostaId;

        return $this;
    }

    /**
     * @return \Avaliacao\Entity\AvaliacaoQuestao
     */
    public function getQuestao()
    {
        return $this->questao;
    }

    /**
     * @param \Avaliacao\Entity\AvaliacaoQuestao $questao
     * @return AvaliacaoQuestaoResposta
     */
    public function setQuestao($questao)
    {
        $this->questao = $questao;

        return $this;
    }

    /**
     * @return string
     */
    public function getQuestaorespostaDescricao()
    {
        return $this->questaorespostaDescricao;
    }

    /**
     * @param string $questaorespostaDescricao
     * @return AvaliacaoQuestaoResposta
     */
    public function setQuestaorespostaDescricao($questaorespostaDescricao)
    {
        $this->questaorespostaDescricao = $questaorespostaDescricao;

        return $this;
    }

    /**
     * @return integer
     */
    public function getQuestaorespostaPosicao()
    {
        return $this->questaorespostaPosicao;
    }

    /**
     * @param integer $questaorespostaPosicao
     * @return AvaliacaoQuestaoResposta
     */
    public function setQuestaorespostaPosicao($questaorespostaPosicao)
    {
        $this->questaorespostaPosicao = $questaorespostaPosicao;

        return $this;
    }

    /**
     * @return string
     */
    public function getQuestaorespostaGabarito()
    {
        return $this->questaorespostaGabarito;
    }

    /**
     * @param string $questaorespostaGabarito
     * @return AvaliacaoQuestaoResposta
     */
    public function setQuestaorespostaGabarito($questaorespostaGabarito)
    {
        $this->questaorespostaGabarito = $questaorespostaGabarito;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'questaorespostaId'        => $this->getQuestaorespostaId(),
            'questao'                  => $this->getQuestao(),
            'questaorespostaDescricao' => $this->getQuestaorespostaDescricao(),
            'questaorespostaPosicao'   => $this->getQuestaorespostaPosicao(),
            'questaorespostaGabarito'  => $this->getQuestaorespostaGabarito()
        );

        $array['questao'] = $this->getQuestao() ? $this->getQuestao()->getQuestaoId() : null;

        return $array;
    }
}
