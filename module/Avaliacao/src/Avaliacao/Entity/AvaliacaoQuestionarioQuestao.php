<?php

namespace Avaliacao\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AvaliacaoQuestionarioQuestao
 *
 * @ORM\Table(name="avaliacao__questionario_questao")
 * @ORM\Entity
 * @LG\LG(id="questionarioquestaoId",label="QuestionarioId")
 * @Jarvis\Jarvis(title="Listagem de questionario questão",icon="fa fa-table")
 */
class AvaliacaoQuestionarioQuestao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="questionarioquestao_id", type="integer", nullable=false, length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="questionarioquestao_id")
     * @LG\Labels\Attributes(text="Código")
     * @LG\Querys\Conditions(type="=")
     */
    private $questionarioquestaoId;

    /**
     * @var \Avaliacao\Entity\AvaliacaoQuestionario
     * @ORM\ManyToOne(targetEntity="Avaliacao\Entity\AvaliacaoQuestionario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="questionario_id", referencedColumnName="questionario_id")
     * })
     */
    private $questionario;

    /**
     * @var \Avaliacao\Entity\AvaliacaoSecao
     * @ORM\ManyToOne(targetEntity="Avaliacao\Entity\AvaliacaoSecao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="secao_id", referencedColumnName="secao_id")
     * })
     */
    private $secao;

    /**
     * @var \Avaliacao\Entity\AvaliacaoQuestao
     * @ORM\ManyToOne(targetEntity="Avaliacao\Entity\AvaliacaoQuestao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="questao_id", referencedColumnName="questao_id")
     * })
     */
    private $questao;

    /**
     * @var integer
     *
     * @ORM\Column(name="questionarioquestao_posicao", type="integer", nullable=false, length=11)
     * @LG\Labels\Property(name="questionarioquestao_posicao")
     * @LG\Labels\Attributes(text="Posição")
     * @LG\Querys\Conditions(type="=")
     */
    private $questionarioquestaoPosicao;

    /**
     * @var string
     *
     * @ORM\Column(name="questionarioquestao_anulada", type="string", nullable=true, length=3)
     * @LG\Labels\Property(name="questionarioquestao_anulada")
     * @LG\Labels\Attributes(text="Anulada")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $questionarioquestaoAnulada;

    /**
     * @var string
     *
     * @ORM\Column(name="questionarioquestao_obrigatoria", type="string", nullable=true, length=3)
     * @LG\Labels\Property(name="questionarioquestao_obrigatoria")
     * @LG\Labels\Attributes(text="Obrigatória")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $questionarioquestaoObrigatoria;

    /**
     * @var string
     *
     * @ORM\Column(name="questionarioquestao_peso", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="questionarioquestao_peso")
     * @LG\Labels\Attributes(text="Peso")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $questionarioquestaoPeso;

    /**
     * @return integer
     */
    public function getQuestionarioquestaoId()
    {
        return $this->questionarioquestaoId;
    }

    /**
     * @param integer $questionarioquestaoId
     * @return AvaliacaoQuestionarioQuestao
     */
    public function setQuestionarioquestaoId($questionarioquestaoId)
    {
        $this->questionarioquestaoId = $questionarioquestaoId;

        return $this;
    }

    /**
     * @return \Avaliacao\Entity\AvaliacaoQuestionario
     */
    public function getQuestionario()
    {
        return $this->questionario;
    }

    /**
     * @param \Avaliacao\Entity\AvaliacaoQuestionario $questionario
     * @return AvaliacaoQuestionarioQuestao
     */
    public function setQuestionario($questionario)
    {
        $this->questionario = $questionario;

        return $this;
    }

    /**
     * @return \Avaliacao\Entity\AvaliacaoSecao
     */
    public function getSecao()
    {
        return $this->secao;
    }

    /**
     * @param \Avaliacao\Entity\AvaliacaoSecao $secao
     * @return AvaliacaoQuestionarioQuestao
     */
    public function setSecao($secao)
    {
        $this->secao = $secao;

        return $this;
    }

    /**
     * @return \Avaliacao\Entity\AvaliacaoQuestao
     */
    public function getQuestao()
    {
        return $this->questao;
    }

    /**
     * @param \Avaliacao\Entity\AvaliacaoQuestao $questao
     * @return AvaliacaoQuestionarioQuestao
     */
    public function setQuestao($questao)
    {
        $this->questao = $questao;

        return $this;
    }

    /**
     * @return integer
     */
    public function getQuestionarioquestaoPosicao()
    {
        return $this->questionarioquestaoPosicao;
    }

    /**
     * @param integer $questionarioquestaoPosicao
     * @return AvaliacaoQuestionarioQuestao
     */
    public function setQuestionarioquestaoPosicao($questionarioquestaoPosicao)
    {
        $this->questionarioquestaoPosicao = $questionarioquestaoPosicao;

        return $this;
    }

    /**
     * @return string
     */
    public function getQuestionarioquestaoAnulada()
    {
        return $this->questionarioquestaoAnulada;
    }

    /**
     * @param string $questionarioquestaoAnulada
     * @return AvaliacaoQuestionarioQuestao
     */
    public function setQuestionarioquestaoAnulada($questionarioquestaoAnulada)
    {
        $this->questionarioquestaoAnulada = $questionarioquestaoAnulada;

        return $this;
    }

    /**
     * @return string
     */
    public function getQuestionarioquestaoObrigatoria()
    {
        return $this->questionarioquestaoObrigatoria;
    }

    /**
     * @param string $questionarioquestaoObrigatoria
     * @return AvaliacaoQuestionarioQuestao
     */
    public function setQuestionarioquestaoObrigatoria($questionarioquestaoObrigatoria)
    {
        $this->questionarioquestaoObrigatoria = $questionarioquestaoObrigatoria;

        return $this;
    }

    /**
     * @return string
     */
    public function getQuestionarioquestaoPeso()
    {
        return $this->questionarioquestaoPeso;
    }

    /**
     * @param string $questionarioquestaoPeso
     * @return AvaliacaoQuestionarioQuestao
     */
    public function setQuestionarioquestaoPeso($questionarioquestaoPeso)
    {
        $this->questionarioquestaoPeso = $questionarioquestaoPeso;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'questionarioquestaoId'          => $this->getQuestionarioquestaoId(),
            'questionario'                   => $this->getQuestionario(),
            'secao'                          => $this->getSecao(),
            'questao'                        => $this->getQuestao(),
            'questionarioquestaoPosicao'     => $this->getQuestionarioquestaoPosicao(),
            'questionarioquestaoAnulada'     => $this->getQuestionarioquestaoAnulada(),
            'questionarioquestaoObrigatoria' => $this->getQuestionarioquestaoObrigatoria(),
            'questionarioquestaoPeso'        => $this->getQuestionarioquestaoPeso(),
        );

        $array['questionario'] = $this->getQuestionario() ? $this->getQuestionario()->getQuestionarioId() : null;
        $array['secao']        = $this->getSecao() ? $this->getSecao()->getSecaoId() : null;
        $array['questao']      = $this->getQuestao() ? $this->getQuestao()->getQuestaoId() : null;

        return $array;
    }
}
