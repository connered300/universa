<?php

namespace Avaliacao\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AvaliacaoQuestao
 *
 * @ORM\Table(name="avaliacao__questao")
 * @ORM\Entity
 * @LG\LG(id="questaoId",label="QuestaoDescricao")
 * @Jarvis\Jarvis(title="Listagem de questão",icon="fa fa-table")
 */
class AvaliacaoQuestao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="questao_id", type="integer", nullable=false, length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="questao_id")
     * @LG\Labels\Attributes(text="Questão")
     * @LG\Querys\Conditions(type="=")
     */
    private $questaoId;

    /**
     * @var \Avaliacao\Entity\AvaliacaoQuestaoTipo
     * @ORM\ManyToOne(targetEntity="Avaliacao\Entity\AvaliacaoQuestaoTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="questaotipo_id", referencedColumnName="questaotipo_id")
     * })
     */
    private $questaotipo;

    /**
     * @var \Avaliacao\Entity\AvaliacaoQuestaoAplicacao
     * @ORM\ManyToOne(targetEntity="Avaliacao\Entity\AvaliacaoQuestaoAplicacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="aplicacao_id", referencedColumnName="aplicacao_id")
     * })
     */
    private $aplicacao;

    /**
     * @var string
     *
     * @ORM\Column(name="questao_descricao", type="string", nullable=true, length=255)
     * @LG\Labels\Property(name="questao_descricao")
     * @LG\Labels\Attributes(text="descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $questaoDescricao;

    /**
     * @return integer
     */
    public function getQuestaoId()
    {
        return $this->questaoId;
    }

    /**
     * @param integer $questaoId
     * @return AvaliacaoQuestao
     */
    public function setQuestaoId($questaoId)
    {
        $this->questaoId = $questaoId;

        return $this;
    }

    /**
     * @return \Avaliacao\Entity\AvaliacaoQuestaoTipo
     */
    public function getQuestaotipo()
    {
        return $this->questaotipo;
    }

    /**
     * @param \Avaliacao\Entity\AvaliacaoQuestaoTipo $questaotipo
     * @return AvaliacaoQuestao
     */
    public function setQuestaotipo($questaotipo)
    {
        $this->questaotipo = $questaotipo;

        return $this;
    }

    /**
     * @return \Avaliacao\Entity\AvaliacaoQuestaoAplicacao
     */
    public function getAplicacao()
    {
        return $this->aplicacao;
    }

    /**
     * @param \Avaliacao\Entity\AvaliacaoQuestaoAplicacao $aplicacao
     * @return AvaliacaoQuestao
     */
    public function setAplicacao($aplicacao)
    {
        $this->aplicacao = $aplicacao;

        return $this;
    }

    /**
     * @return string
     */
    public function getQuestaoDescricao()
    {
        return $this->questaoDescricao;
    }

    /**
     * @param string $questaoDescricao
     * @return AvaliacaoQuestao
     */
    public function setQuestaoDescricao($questaoDescricao)
    {
        $this->questaoDescricao = $questaoDescricao;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'questaoId'        => $this->getQuestaoId(),
            'questaotipo'      => $this->getQuestaotipo(),
            'aplicacao'        => $this->getAplicacao(),
            'questaoDescricao' => $this->getQuestaoDescricao(),
        );

        $array['questaotipo'] = $this->getQuestaotipo() ? $this->getQuestaotipo()->getQuestaotipoId() : null;
        $array['aplicacao']   = $this->getAplicacao() ? $this->getAplicacao()->getAplicacaoId() : null;

        return $array;
    }
}
