<?php

namespace Avaliacao\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AvaliacaoQuestionarioRespondente
 *
 * @ORM\Table(name="avaliacao__questionario_respondente")
 * @ORM\Entity
 * @LG\LG(id="respondenteId",label="SecaoId")
 * @Jarvis\Jarvis(title="Listagem de questionario respondente",icon="fa fa-table")
 */
class AvaliacaoQuestionarioRespondente
{
    /**
     * @var integer
     *
     * @ORM\Column(name="respondente_id", type="integer", nullable=false, length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="respondente_id")
     * @LG\Labels\Attributes(text="código respondente")
     * @LG\Querys\Conditions(type="=")
     */
    private $respondenteId;

    /**
     * @var \Avaliacao\Entity\AvaliacaoSecao
     * @ORM\ManyToOne(targetEntity="Avaliacao\Entity\AvaliacaoSecao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="secao_id", referencedColumnName="secao_id")
     * })
     */
    private $secao;

    /**
     * @var string
     *
     * @ORM\Column(name="respondente_status", type="string", nullable=false, length=7)
     * @LG\Labels\Property(name="respondente_status")
     * @LG\Labels\Attributes(text="status")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $respondenteStatus;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="respondente_data", type="datetime", nullable=true)
     * @LG\Labels\Property(name="respondente_data")
     * @LG\Labels\Attributes(text="data")
     * @LG\Querys\Conditions(type="=")
     */
    private $respondenteData;

    /**
     * @var \Pessoa\Entity\Pessoa
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     */
    private $pes;

    /**
     * @var \Avaliacao\Entity\AvaliacaoQuestionario
     * @ORM\ManyToOne(targetEntity="Avaliacao\Entity\AvaliacaoQuestionario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="questionario_id", referencedColumnName="questionario_id")
     * })
     */
    private $questionario;

    /**
     * @return integer
     */
    public function getRespondenteId()
    {
        return $this->respondenteId;
    }

    /**
     * @param integer $respondenteId
     * @return AvaliacaoQuestionarioRespondente
     */
    public function setRespondenteId($respondenteId)
    {
        $this->respondenteId = $respondenteId;

        return $this;
    }

    /**
     * @return \Avaliacao\Entity\AvaliacaoSecao
     */
    public function getSecao()
    {
        return $this->secao;
    }

    /**
     * @param \Avaliacao\Entity\AvaliacaoSecao $secao
     * @return AvaliacaoQuestionarioRespondente
     */
    public function setSecao($secao)
    {
        $this->secao = $secao;

        return $this;
    }

    /**
     * @return string
     */
    public function getRespondenteStatus()
    {
        return $this->respondenteStatus;
    }

    /**
     * @return boolean
     */
    public function questionarioRespondenteConcluido()
    {
        $c = $this->respondenteStatus == \Avaliacao\Service\AvaliacaoQuestionarioRespondente::RESPONDENTE_STATUS_CONCLUIDO;

        return $c;
    }

    /**
     * @param string $respondenteStatus
     * @return AvaliacaoQuestionarioRespondente
     */
    public function setRespondenteStatus($respondenteStatus)
    {
        $this->respondenteStatus = $respondenteStatus;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getRespondenteData($format = false)
    {
        $respondenteData = $this->respondenteData;

        if ($format && $respondenteData) {
            $respondenteData = $respondenteData->format('d/m/Y H:i:s');
        }

        return $respondenteData;
    }

    /**
     * @param \Datetime $respondenteData
     * @return AvaliacaoQuestionarioRespondente
     */
    public function setRespondenteData($respondenteData)
    {
        if ($respondenteData) {
            if (is_string($respondenteData)) {
                $respondenteData = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $respondenteData
                );
                $respondenteData = new \Datetime($respondenteData);
            }
        } else {
            $respondenteData = null;
        }
        $this->respondenteData = $respondenteData;

        return $this;
    }

    /**
     * @return \Pessoa\Entity\Pessoa
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @param \Pessoa\Entity\Pessoa $pes
     * @return AvaliacaoQuestionarioRespondente
     */
    public function setPes($pes)
    {
        $this->pes = $pes;

        return $this;
    }

    /**
     * @return \Avaliacao\Entity\AvaliacaoQuestionario
     */
    public function getQuestionario()
    {
        return $this->questionario;
    }

    /**
     * @param /Avaliacao\Entity\AvaliacaoQuestionario
     * @return AvaliacaoQuestionarioRespondente
     */
    public function setQuestionario($questionario)
    {
        $this->questionario = $questionario;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'respondenteId'     => $this->getRespondenteId(),
            'secao'             => $this->getSecao(),
            'respondenteStatus' => $this->getRespondenteStatus(),
            'respondenteData'   => $this->getRespondenteData(true),
            'pes'               => $this->getPes(),
            'questionario'      => $this->getQuestionario()
        );

        $array['secao'] = $this->getSecao() ? $this->getSecao()->getSecaoId() : null;
        $array['pes']   = $this->getPes() ? $this->getPes()->getPesId() : null;

        return $array;
    }
}
