<?php

namespace Avaliacao\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AvaliacaoQuestaoAplicacao
 *
 * @ORM\Table(name="avaliacao__questao_aplicacao")
 * @ORM\Entity
 * @LG\LG(id="aplicacaoId",label="AplicacaoDescricao")
 * @Jarvis\Jarvis(title="Listagem de questão aplicação",icon="fa fa-table")
 */
class AvaliacaoQuestaoAplicacao
{
    /**
     * @var string
     *
     * @ORM\Column(name="aplicacao_id", type="string", nullable=false, length=20)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @LG\Labels\Property(name="aplicacao_id")
     * @LG\Labels\Attributes(text="Aplicação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $aplicacaoId;

    /**
     * @var string
     *
     * @ORM\Column(name="aplicacao_descricao", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="aplicacao_descricao")
     * @LG\Labels\Attributes(text="descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $aplicacaoDescricao;

    /**
     * @return string
     */
    public function getAplicacaoId()
    {
        return $this->aplicacaoId;
    }

    /**
     * @param string $aplicacaoId
     * @return AvaliacaoQuestaoAplicacao
     */
    public function setAplicacaoId($aplicacaoId)
    {
        $this->aplicacaoId = $aplicacaoId;

        return $this;
    }

    /**
     * @return string
     */
    public function getAplicacaoDescricao()
    {
        return $this->aplicacaoDescricao;
    }

    /**
     * @param string $aplicacaoDescricao
     * @return AvaliacaoQuestaoAplicacao
     */
    public function setAplicacaoDescricao($aplicacaoDescricao)
    {
        $this->aplicacaoDescricao = $aplicacaoDescricao;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'aplicacaoId'        => $this->getAplicacaoId(),
            'aplicacaoIdOld'     => $this->getAplicacaoId(),
            'aplicacaoDescricao' => $this->getAplicacaoDescricao(),
        );

        return $array;
    }
}
