<?php

namespace Avaliacao\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AvaliacaoQuestionario
 *
 * @ORM\Table(name="avaliacao__questionario")
 * @ORM\Entity
 * @LG\LG(id="questionarioId",label="QuestionarioDescricao")
 * @Jarvis\Jarvis(title="Listagem de questionario",icon="fa fa-table")
 */
class AvaliacaoQuestionario
{
    /**
     * @var integer
     *
     * @ORM\Column(name="questionario_id", type="integer", nullable=false, length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="questionario_id")
     * @LG\Labels\Attributes(text="Questionário")
     * @LG\Querys\Conditions(type="=")
     */
    private $questionarioId;

    /**
     * @var \Avaliacao\Entity\AvaliacaoFim
     * @ORM\ManyToOne(targetEntity="Avaliacao\Entity\AvaliacaoFim")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fim_id", referencedColumnName="fim_id")
     * })
     */
    private $fim;

    /**
     * @var string
     *
     * @ORM\Column(name="questionario_titulo", type="string", nullable=false, length=45)
     * @LG\Labels\Property(name="questionario_titulo")
     * @LG\Labels\Attributes(text="título")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $questionarioTitulo;

    /**
     * @var string
     *
     * @ORM\Column(name="questionario_descricao", type="text", nullable=true)
     * @LG\Labels\Property(name="questionario_descricao")
     * @LG\Labels\Attributes(text="descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $questionarioDescricao;

    /**
     * @var string
     *
     * @ORM\Column(name="questionario_mensagem_inicial", type="text", nullable=true)
     * @LG\Labels\Property(name="questionario_mensagem_inicial")
     * @LG\Labels\Attributes(text="Mensagem Inicial")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $questionarioMensagemInicial;

    /**
     * @var string
     *
     * @ORM\Column(name="questionario_mensagem_agradecimento", type="text", nullable=true)
     * @LG\Labels\Property(name="questionario_mensagem_agradecimento")
     * @LG\Labels\Attributes(text="Mensagem Agradecimento")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $questionarioMensagemAgradecimento;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="questionario_inicio", type="datetime", nullable=false)
     * @LG\Labels\Property(name="questionario_inicio")
     * @LG\Labels\Attributes(text="início")
     * @LG\Querys\Conditions(type="=")
     */
    private $questionarioInicio;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="questionario_fim", type="datetime", nullable=true)
     * @LG\Labels\Property(name="questionario_fim")
     * @LG\Labels\Attributes(text="fim")
     * @LG\Querys\Conditions(type="=")
     */
    private $questionarioFim;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="questionario_alteracao", type="datetime", nullable=true)
     * @LG\Labels\Property(name="questionario_alteracao")
     * @LG\Labels\Attributes(text="alteração")
     * @LG\Querys\Conditions(type="=")
     */
    private $questionarioAlteracao;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="questionario_criacao", type="datetime", nullable=true)
     * @LG\Labels\Property(name="questionario_criacao")
     * @LG\Labels\Attributes(text="criação")
     * @LG\Querys\Conditions(type="=")
     */
    private $questionarioCriacao;

    /**
     * @var string
     *
     * @ORM\Column(name="questionario_estado", type="string", nullable=true, length=8)
     * @LG\Labels\Property(name="questionario_estado")
     * @LG\Labels\Attributes(text="estado")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $questionarioEstado;

    /**
     * @var integer
     *
     * @ORM\Column(name="questionario_secoes", type="integer", nullable=true, length=11)
     * @LG\Labels\Property(name="questionario_secoes")
     * @LG\Labels\Attributes(text="secões")
     * @LG\Querys\Conditions(type="=")
     */
    private $questionarioSecoes;

    /**
     * @var string
     *
     * @ORM\Column(name="questionario_obrigatoriedade", type="string", nullable=true, length=11)
     * @LG\Labels\Property(name="questionario_obrigatoriedade")
     * @LG\Labels\Attributes(text="obrigatoriedade")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $questionarioObrigatoriedade;

    /**
     * @var string
     *
     * @ORM\Column(name="questionario_anonimo", type="string", nullable=true, length=3)
     * @LG\Labels\Property(name="questionario_anonimo")
     * @LG\Labels\Attributes(text="anonimo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $questionarioAnonimo;

    /**
     * @return integer
     */
    public function getQuestionarioId()
    {
        return $this->questionarioId;
    }

    /**
     * @param integer $questionarioId
     * @return AvaliacaoQuestionario
     */
    public function setQuestionarioId($questionarioId)
    {
        $this->questionarioId = $questionarioId;

        return $this;
    }

    /**
     * @return \Avaliacao\Entity\AvaliacaoFim
     */
    public function getFim()
    {
        return $this->fim;
    }

    /**
     * @param \Avaliacao\Entity\AvaliacaoFim $fim
     * @return AvaliacaoQuestionario
     */
    public function setFim($fim)
    {
        $this->fim = $fim;

        return $this;
    }

    /**
     * @return string
     */
    public function getQuestionarioTitulo()
    {
        return $this->questionarioTitulo;
    }

    /**
     * @param string $questionarioTitulo
     * @return AvaliacaoQuestionario
     */
    public function setQuestionarioTitulo($questionarioTitulo)
    {
        $this->questionarioTitulo = $questionarioTitulo;

        return $this;
    }

    /**
     * @return string
     */
    public function getQuestionarioDescricao()
    {
        return $this->questionarioDescricao;
    }

    /**
     * @param string $questionarioDescricao
     * @return AvaliacaoQuestionario
     */
    public function setQuestionarioDescricao($questionarioDescricao)
    {
        $this->questionarioDescricao = $questionarioDescricao;

        return $this;
    }

    /**
     * @return string
     */
    public function getQuestionarioMensagemInicial()
    {
        return $this->questionarioMensagemInicial;
    }

    /**
     * @param string $questionarioMensagemInicial
     * @return AvaliacaoQuestionario
     */
    public function setQuestionarioMensagemInicial($questionarioMensagemInicial)
    {
        $this->questionarioMensagemInicial = $questionarioMensagemInicial;

        return $this;
    }

    /**
     * @return string
     */
    public function getQuestionarioMensagemAgradecimento()
    {
        return $this->questionarioMensagemAgradecimento;
    }

    /**
     * @param string $questionarioMensagemAgradecimento
     * @return AvaliacaoQuestionario
     */
    public function setQuestionarioMensagemAgradecimento($questionarioMensagemAgradecimento)
    {
        $this->questionarioMensagemAgradecimento = $questionarioMensagemAgradecimento;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getQuestionarioInicio($format = false)
    {
        $questionarioInicio = $this->questionarioInicio;

        if ($format && $questionarioInicio) {
            $questionarioInicio = $questionarioInicio->format('d/m/Y H:i:s');
        }

        return $questionarioInicio;
    }

    /**
     * @param \Datetime $questionarioInicio
     * @return AvaliacaoQuestionario
     */
    public function setQuestionarioInicio($questionarioInicio)
    {
        if ($questionarioInicio) {
            if (is_string($questionarioInicio)) {
                $questionarioInicio = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $questionarioInicio
                );
                $questionarioInicio = new \Datetime($questionarioInicio);
            }
        } else {
            $questionarioInicio = null;
        }
        $this->questionarioInicio = $questionarioInicio;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getQuestionarioFim($format = false)
    {
        $questionarioFim = $this->questionarioFim;

        if ($format && $questionarioFim) {
            $questionarioFim = $questionarioFim->format('d/m/Y H:i:s');
        }

        return $questionarioFim;
    }

    /**
     * @param \Datetime $questionarioFim
     * @return AvaliacaoQuestionario
     */
    public function setQuestionarioFim($questionarioFim)
    {
        if ($questionarioFim) {
            if (is_string($questionarioFim)) {
                $questionarioFim = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $questionarioFim
                );
                $questionarioFim = new \Datetime($questionarioFim);
            }
        } else {
            $questionarioFim = null;
        }
        $this->questionarioFim = $questionarioFim;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getQuestionarioAlteracao($format = false)
    {
        $questionarioAlteracao = $this->questionarioAlteracao;

        if ($format && $questionarioAlteracao) {
            $questionarioAlteracao = $questionarioAlteracao->format('d/m/Y H:i:s');
        }

        return $questionarioAlteracao;
    }

    /**
     * @param \Datetime $questionarioAlteracao
     * @return AvaliacaoQuestionario
     */
    public function setQuestionarioAlteracao($questionarioAlteracao)
    {
        if ($questionarioAlteracao) {
            if (is_string($questionarioAlteracao)) {
                $questionarioAlteracao = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $questionarioAlteracao
                );
                $questionarioAlteracao = new \Datetime($questionarioAlteracao);
            }
        } else {
            $questionarioAlteracao = null;
        }
        $this->questionarioAlteracao = $questionarioAlteracao;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getQuestionarioCriacao($format = false)
    {
        $questionarioCriacao = $this->questionarioCriacao;

        if ($format && $questionarioCriacao) {
            $questionarioCriacao = $questionarioCriacao->format('d/m/Y H:i:s');
        }

        return $questionarioCriacao;
    }

    /**
     * @param \Datetime $questionarioCriacao
     * @return AvaliacaoQuestionario
     */
    public function setQuestionarioCriacao($questionarioCriacao)
    {
        if ($questionarioCriacao) {
            if (is_string($questionarioCriacao)) {
                $questionarioCriacao = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $questionarioCriacao
                );
                $questionarioCriacao = new \Datetime($questionarioCriacao);
            }
        } else {
            $questionarioCriacao = null;
        }
        $this->questionarioCriacao = $questionarioCriacao;

        return $this;
    }

    /**
     * @return string
     */
    public function getQuestionarioEstado()
    {
        return $this->questionarioEstado;
    }

    /**
     * @param string $questionarioEstado
     * @return AvaliacaoQuestionario
     */
    public function setQuestionarioEstado($questionarioEstado)
    {
        $this->questionarioEstado = $questionarioEstado;

        return $this;
    }

    /**
     * @return integer
     */
    public function getQuestionarioSecoes()
    {
        return $this->questionarioSecoes;
    }

    /**
     * @param integer $questionarioSecoes
     * @return AvaliacaoQuestionario
     */
    public function setQuestionarioSecoes($questionarioSecoes)
    {
        $this->questionarioSecoes = $questionarioSecoes;

        return $this;
    }

    /**
     * @return string
     */
    public function getQuestionarioObrigatoriedade()
    {
        return $this->questionarioObrigatoriedade;
    }

    /**
     * @param string $questionarioObrigatoriedade
     * @return AvaliacaoQuestionario
     */
    public function setQuestionarioObrigatoriedade($questionarioObrigatoriedade)
    {
        $this->questionarioObrigatoriedade = $questionarioObrigatoriedade;

        return $this;
    }

    /**
     * @return string
     */
    public function getQuestionarioAnonimo()
    {
        return $this->questionarioAnonimo;
    }

    /**
     * @return boolean
     */
    public function verificarQuestionarioAnonimo()
    {
        return $this->questionarioAnonimo == \Avaliacao\Service\AvaliacaoQuestionario::QUESTIONARIO_ANONIMO_SIM;
    }

    /**
     * @param string $questionarioAnonimo
     * @return AvaliacaoQuestionario
     */
    public function setQuestionarioAnonimo($questionarioAnonimo)
    {
        $this->questionarioAnonimo = $questionarioAnonimo;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'questionarioId'                    => $this->getQuestionarioId(),
            'fim'                               => $this->getFim(),
            'questionarioTitulo'                => $this->getQuestionarioTitulo(),
            'questionarioDescricao'             => $this->getQuestionarioDescricao(),
            'questionarioMensagemInicial'       => $this->getQuestionarioMensagemInicial(),
            'questionarioMensagemAgradecimento' => $this->getQuestionarioMensagemAgradecimento(),
            'questionarioInicio'                => $this->getQuestionarioInicio(true),
            'questionarioFim'                   => $this->getQuestionarioFim(true),
            'questionarioAlteracao'             => $this->getQuestionarioAlteracao(true),
            'questionarioCriacao'               => $this->getQuestionarioCriacao(true),
            'questionarioEstado'                => $this->getQuestionarioEstado(),
            'questionarioSecoes'                => $this->getQuestionarioSecoes(),
            'questionarioObrigatoriedade'       => $this->getQuestionarioObrigatoriedade(),
            'questionarioAnonimo'               => $this->getQuestionarioAnonimo(),
        );

        $array['fim'] = $this->getFim() ? $this->getFim()->getFimId() : null;

        return $array;
    }
}
