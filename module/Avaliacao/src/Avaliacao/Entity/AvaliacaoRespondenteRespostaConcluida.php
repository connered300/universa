<?php

namespace Avaliacao\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AvaliacaoRespondenteRespostaConcluida
 *
 * @ORM\Table(name="avaliacao__respondente_resposta_concluida")
 * @ORM\Entity
 * @LG\LG(id="respondenterespostaId",label="QuestionarioquestaoId")
 * @Jarvis\Jarvis(title="Listagem de respondente resposta concluida",icon="fa fa-table")
 */
class AvaliacaoRespondenteRespostaConcluida
{
    /**
     * @var integer
     *
     * @ORM\Column(name="respondenteresposta_id", type="integer", nullable=false, length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="respondenteresposta_id")
     * @LG\Labels\Attributes(text="código respondenteresposta")
     * @LG\Querys\Conditions(type="=")
     */
    private $respondenterespostaId;

    /**
     * @var \Avaliacao\Entity\AvaliacaoQuestionarioQuestao
     * @ORM\ManyToOne(targetEntity="Avaliacao\Entity\AvaliacaoQuestionarioQuestao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="questionarioquestao_id", referencedColumnName="questionarioquestao_id")
     * })
     */
    private $questionarioquestao;

    /**
     * @var \Avaliacao\Entity\AvaliacaoQuestionarioRespondente
     * @ORM\ManyToOne(targetEntity="Avaliacao\Entity\AvaliacaoQuestionarioRespondente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="respondente_id", referencedColumnName="respondente_id")
     * })
     */
    private $respondente;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="respondenteresposta_submissao", type="datetime", nullable=false)
     * @LG\Labels\Property(name="respondenteresposta_submissao")
     * @LG\Labels\Attributes(text="submissão respondenteresposta")
     * @LG\Querys\Conditions(type="=")
     */
    private $respondenterespostaSubmissao;

    /**
     * @return integer
     */
    public function getRespondenterespostaId()
    {
        return $this->respondenterespostaId;
    }

    /**
     * @param integer $respondenterespostaId
     * @return AvaliacaoRespondenteRespostaConcluida
     */
    public function setRespondenterespostaId($respondenterespostaId)
    {
        $this->respondenterespostaId = $respondenterespostaId;

        return $this;
    }

    /**
     * @return \Avaliacao\Entity\AvaliacaoQuestionarioQuestao
     */
    public function getQuestionarioquestao()
    {
        return $this->questionarioquestao;
    }

    /**
     * @param \Avaliacao\Entity\AvaliacaoQuestionarioQuestao $questionarioquestao
     * @return AvaliacaoRespondenteRespostaConcluida
     */
    public function setQuestionarioquestao($questionarioquestao)
    {
        $this->questionarioquestao = $questionarioquestao;

        return $this;
    }

    /**
     * @return \Avaliacao\Entity\AvaliacaoQuestionarioRespondente
     */
    public function getRespondente()
    {
        return $this->respondente;
    }

    /**
     * @param \Avaliacao\Entity\AvaliacaoQuestionarioRespondente $respondente
     * @return AvaliacaoRespondenteRespostaConcluida
     */
    public function setRespondente($respondente)
    {
        $this->respondente = $respondente;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getRespondenterespostaSubmissao($format = false)
    {
        $respondenterespostaSubmissao = $this->respondenterespostaSubmissao;

        if ($format && $respondenterespostaSubmissao) {
            $respondenterespostaSubmissao = $respondenterespostaSubmissao->format('d/m/Y H:i:s');
        }

        return $respondenterespostaSubmissao;
    }

    /**
     * @param \Datetime $respondenterespostaSubmissao
     * @return AvaliacaoRespondenteRespostaConcluida
     */
    public function setRespondenterespostaSubmissao($respondenterespostaSubmissao)
    {
        if ($respondenterespostaSubmissao) {
            if (is_string($respondenterespostaSubmissao)) {
                $respondenterespostaSubmissao = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $respondenterespostaSubmissao
                );
                $respondenterespostaSubmissao = new \Datetime($respondenterespostaSubmissao);
            }
        } else {
            $respondenterespostaSubmissao = null;
        }
        $this->respondenterespostaSubmissao = $respondenterespostaSubmissao;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'respondenterespostaId'        => $this->getRespondenterespostaId(),
            'questionarioquestao'          => $this->getQuestionarioquestao(),
            'respondente'                  => $this->getRespondente(),
            'respondenterespostaSubmissao' => $this->getRespondenterespostaSubmissao(true),
        );

        $array['questionarioquestao'] = $this->getQuestionarioquestao() ? $this->getQuestionarioquestao(
        )->getQuestionarioquestaoId() : null;
        $array['respondente']         = $this->getRespondente() ? $this->getRespondente()->getRespondenteId() : null;

        return $array;
    }
}
