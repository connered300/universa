<?php

namespace Avaliacao\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AvaliacaoQuestaoTipo
 *
 * @ORM\Table(name="avaliacao__questao_tipo")
 * @ORM\Entity
 * @LG\LG(id="questaotipoId",label="QuestaotipoDescricao")
 * @Jarvis\Jarvis(title="Listagem de questão tipo",icon="fa fa-table")
 */
class AvaliacaoQuestaoTipo
{
    /**
     * @var string
     *
     * @ORM\Column(name="questaotipo_id", type="string", nullable=false, length=20)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @LG\Labels\Property(name="questaotipo_id")
     * @LG\Labels\Attributes(text="Tipo da Questão")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $questaotipoId;

    /**
     * @var string
     *
     * @ORM\Column(name="questaotipo_descricao", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="questaotipo_descricao")
     * @LG\Labels\Attributes(text="Descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $questaotipoDescricao;

    /**
     * @return string
     */
    public function getQuestaotipoId()
    {
        return $this->questaotipoId;
    }

    /**
     * @param string $questaotipoId
     * @return AvaliacaoQuestaoTipo
     */
    public function setQuestaotipoId($questaotipoId)
    {
        $this->questaotipoId = $questaotipoId;

        return $this;
    }

    /**
     * @return string
     */
    public function getQuestaotipoDescricao()
    {
        return $this->questaotipoDescricao;
    }

    /**
     * @param string $questaotipoDescricao
     * @return AvaliacaoQuestaoTipo
     */
    public function setQuestaotipoDescricao($questaotipoDescricao)
    {
        $this->questaotipoDescricao = $questaotipoDescricao;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'questaotipoId'        => $this->getQuestaotipoId(),
            'questaotipoIdOld'     => $this->getQuestaotipoId(),
            'questaotipoDescricao' => $this->getQuestaotipoDescricao(),
        );

        return $array;
    }
}
