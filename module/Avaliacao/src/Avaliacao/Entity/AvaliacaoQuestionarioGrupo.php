<?php

namespace Avaliacao\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AvaliacaoQuestionarioGrupo
 *
 * @ORM\Table(name="avaliacao__questionario_grupo")
 * @ORM\Entity
 * @LG\LG(id="questionarioId",label="QuestionarioId")
 * @Jarvis\Jarvis(title="Listagem de questionario grupo",icon="fa fa-table")
 */
class AvaliacaoQuestionarioGrupo
{
    /**
     * @var \Acesso\Entity\AcessoGrupo
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoGrupo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="grupo_id", referencedColumnName="id")
     * })
     */
    private $grupo;

    /**
     * @var \Avaliacao\Entity\AvaliacaoQuestionario
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="Avaliacao\Entity\AvaliacaoQuestionario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="questionario_id", referencedColumnName="questionario_id")
     * })
     */
    private $questionario;

    /**
     * @return \Acesso\Entity\AcessoGrupo
     */
    public function getGrupo()
    {
        return $this->grupo;
    }

    /**
     * @param \Acesso\Entity\AcessoGrupo $grupo
     * @return AvaliacaoQuestionarioGrupo
     */
    public function setGrupo($grupo)
    {
        $this->grupo = $grupo;

        return $this;
    }

    /**
     * @return \Avaliacao\Entity\AvaliacaoQuestionario
     */
    public function getQuestionario()
    {
        return $this->questionario;
    }

    /**
     * @param \Avaliacao\Entity\AvaliacaoQuestionario $questionario
     * @return AvaliacaoQuestionarioGrupo
     */
    public function setQuestionario($questionario)
    {
        $this->questionario = $questionario;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'grupo'        => $this->getGrupo(),
            'questionario' => $this->getQuestionario(),
        );

        $array['grupo']        = $this->getGrupo() ? $this->getGrupo()->getId() : null;
        $array['questionario'] = $this->getQuestionario() ? $this->getQuestionario()->getQuestionarioId() : null;

        return $array;
    }
}
