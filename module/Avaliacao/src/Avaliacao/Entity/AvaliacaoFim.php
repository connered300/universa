<?php

namespace Avaliacao\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AvaliacaoFim
 *
 * @ORM\Table(name="avaliacao__fim")
 * @ORM\Entity
 * @LG\LG(id="fimId",label="FimDescricao")
 * @Jarvis\Jarvis(title="Listagem de fim",icon="fa fa-table")
 */
class AvaliacaoFim
{
    /**
     * @var string
     *
     * @ORM\Column(name="fim_id", type="string", nullable=false, length=20)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @LG\Labels\Property(name="fim_id")
     * @LG\Labels\Attributes(text="Fim")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $fimId;

    /**
     * @var string
     *
     * @ORM\Column(name="fim_nome", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="fim_nome")
     * @LG\Labels\Attributes(text="nome")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $fimNome;

    /**
     * @var string
     *
     * @ORM\Column(name="fim_descricao", type="string", nullable=true, length=250)
     * @LG\Labels\Property(name="fim_descricao")
     * @LG\Labels\Attributes(text="descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $fimDescricao;

    /**
     * @return string
     */
    public function getFimId()
    {
        return $this->fimId;
    }

    /**
     * @param string $fimId
     * @return AvaliacaoFim
     */
    public function setFimId($fimId)
    {
        $this->fimId = $fimId;

        return $this;
    }

    /**
     * @return string
     */
    public function getFimNome()
    {
        return $this->fimNome;
    }

    /**
     * @param string $fimNome
     * @return AvaliacaoFim
     */
    public function setFimNome($fimNome)
    {
        $this->fimNome = $fimNome;

        return $this;
    }

    /**
     * @return string
     */
    public function getFimDescricao()
    {
        return $this->fimDescricao;
    }

    /**
     * @param string $fimDescricao
     * @return AvaliacaoFim
     */
    public function setFimDescricao($fimDescricao)
    {
        $this->fimDescricao = $fimDescricao;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'fimId'        => $this->getFimId(),
            'fimIdOld'        => $this->getFimId(),
            'fimNome'      => $this->getFimNome(),
            'fimDescricao' => $this->getFimDescricao(),
        );

        return $array;
    }
}
