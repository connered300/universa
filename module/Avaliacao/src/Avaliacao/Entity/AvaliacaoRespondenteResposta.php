<?php

namespace Avaliacao\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AvaliacaoRespondenteResposta
 *
 * @ORM\Table(name="avaliacao__respondente_resposta")
 * @ORM\Entity
 * @LG\LG(id="respondenterespostaId",label="QuestionarioquestaoId")
 * @Jarvis\Jarvis(title="Listagem de respondente resposta",icon="fa fa-table")
 */
class AvaliacaoRespondenteResposta
{
    /**
     * @var integer
     *
     * @ORM\Column(name="respondenteresposta_id", type="integer", nullable=false, length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="respondenteresposta_id")
     * @LG\Labels\Attributes(text="código respondenteresposta")
     * @LG\Querys\Conditions(type="=")
     */
    private $respondenterespostaId;

    /**
     * @var \Avaliacao\Entity\AvaliacaoQuestionarioQuestao
     * @ORM\ManyToOne(targetEntity="Avaliacao\Entity\AvaliacaoQuestionarioQuestao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="questionarioquestao_id", referencedColumnName="questionarioquestao_id")
     * })
     */
    private $questionarioquestao;

    /**
     * @var \Avaliacao\Entity\AvaliacaoQuestaoResposta
     * @ORM\ManyToOne(targetEntity="Avaliacao\Entity\AvaliacaoQuestaoResposta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="questaoresposta_id", referencedColumnName="questaoresposta_id")
     * })
     */
    private $questaoresposta;

    /**
     * @var \Avaliacao\Entity\AvaliacaoQuestionarioRespondente
     * @ORM\ManyToOne(targetEntity="Avaliacao\Entity\AvaliacaoQuestionarioRespondente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="respondente_id", referencedColumnName="respondente_id")
     * })
     */
    private $respondente;

    /**
     * @var string
     *
     * @ORM\Column(name="respondenteresposta_peso", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="respondenteresposta_peso")
     * @LG\Labels\Attributes(text="peso respondenteresposta")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $respondenterespostaPeso;

    /**
     * @var string
     *
     * @ORM\Column(name="respondenteresposta_complemento", type="string", nullable=true, length=128)
     * @LG\Labels\Property(name="respondenteresposta_complemento")
     * @LG\Labels\Attributes(text="complemento respondenteresposta")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $respondenterespostaComplemento;

    /**
     * @var string
     *
     * @ORM\Column(name="respondenteresposta_resposta_aberta", type="string", nullable=false)
     * @LG\Labels\Property(name="resposta_aberta")
     * @LG\Labels\Attributes(text="Reposta aberta")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $respostaAberta;

    /**
     * @return integer
     */
    public function getRespondenterespostaId()
    {
        return $this->respondenterespostaId;
    }

    /**
     * @param integer $respondenterespostaId
     * @return AvaliacaoRespondenteResposta
     */
    public function setRespondenterespostaId($respondenterespostaId)
    {
        $this->respondenterespostaId = $respondenterespostaId;

        return $this;
    }

    /**
     * @return \Avaliacao\Entity\AvaliacaoQuestionarioQuestao
     */
    public function getQuestionarioquestao()
    {
        return $this->questionarioquestao;
    }

    /**
     * @param \Avaliacao\Entity\AvaliacaoQuestionarioQuestao $questionarioquestao
     * @return AvaliacaoRespondenteResposta
     */
    public function setQuestionarioquestao($questionarioquestao)
    {
        $this->questionarioquestao = $questionarioquestao;

        return $this;
    }

    /**
     * @return \Avaliacao\Entity\AvaliacaoQuestaoResposta
     */
    public function getQuestaoresposta()
    {
        return $this->questaoresposta;
    }

    /**
     * @param \Avaliacao\Entity\AvaliacaoQuestaoResposta $questaoresposta
     * @return AvaliacaoRespondenteResposta
     */
    public function setQuestaoresposta($questaoresposta)
    {
        $this->questaoresposta = $questaoresposta;

        return $this;
    }

    /**
     * @return \Avaliacao\Entity\AvaliacaoQuestionarioRespondente
     */
    public function getRespondente()
    {
        return $this->respondente;
    }

    /**
     * @param \Avaliacao\Entity\AvaliacaoQuestionarioRespondente $respondente
     * @return AvaliacaoRespondenteResposta
     */
    public function setRespondente($respondente)
    {
        $this->respondente = $respondente;

        return $this;
    }

    /**
     * @return string
     */
    public function getRespondenterespostaPeso()
    {
        return $this->respondenterespostaPeso;
    }

    /**
     * @param string $respondenterespostaPeso
     * @return AvaliacaoRespondenteResposta
     */
    public function setRespondenterespostaPeso($respondenterespostaPeso)
    {
        $this->respondenterespostaPeso = $respondenterespostaPeso;

        return $this;
    }

    /**
     * @return string
     */
    public function getRespondenterespostaComplemento()
    {
        return $this->respondenterespostaComplemento;
    }

    /**
     * @param string $respondenterespostaComplemento
     * @return AvaliacaoRespondenteResposta
     */
    public function setRespondenterespostaComplemento($respondenterespostaComplemento)
    {
        $this->respondenterespostaComplemento = $respondenterespostaComplemento;

        return $this;
    }

    /**
     * @return string
     */
    public function getRespostaAberta()
    {
        return $this->respostaAberta;
    }

    /**
     * @param string $repostaAberta
     * @return AvaliacaoQuestaoResposta
     */
    public function setRespostaAberta($repostaAberta)
    {
        $this->respostaAberta = $repostaAberta;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'respondenterespostaId'          => $this->getRespondenterespostaId(),
            'questionarioquestao'            => $this->getQuestionarioquestao(),
            'questaoresposta'                => $this->getQuestaoresposta(),
            'respondente'                    => $this->getRespondente(),
            'respondenterespostaPeso'        => $this->getRespondenterespostaPeso(),
            'respondenterespostaComplemento' => $this->getRespondenterespostaComplemento(),
        );

        $array['questionarioquestao'] = $this->getQuestionarioquestao() ? $this->getQuestionarioquestao(
        )->getQuestionarioquestaoId() : null;
        $array['questaoresposta']     = $this->getQuestaoresposta() ? $this->getQuestaoresposta()->getQuestaorespostaId(
        ) : null;
        $array['respondente']         = $this->getRespondente() ? $this->getRespondente()->getRespondenteId() : null;

        return $array;
    }
}
