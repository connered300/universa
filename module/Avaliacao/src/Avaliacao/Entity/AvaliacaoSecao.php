<?php

namespace Avaliacao\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * AvaliacaoSecao
 *
 * @ORM\Table(name="avaliacao__secao")
 * @ORM\Entity
 * @LG\LG(id="secaoId",label="SecaoDescricao")
 * @Jarvis\Jarvis(title="Listagem de seção",icon="fa fa-table")
 */
class AvaliacaoSecao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="secao_id", type="integer", nullable=false, length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="secao_id")
     * @LG\Labels\Attributes(text="Seção")
     * @LG\Querys\Conditions(type="=")
     */
    private $secaoId;

    /**
     * @var string
     *
     * @ORM\Column(name="secao_nome", type="string", nullable=false, length=45)
     * @LG\Labels\Property(name="secao_nome")
     * @LG\Labels\Attributes(text="nome")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $secaoNome;

    /**
     * @var string
     *
     * @ORM\Column(name="secao_descricao", type="string", nullable=true, length=100)
     * @LG\Labels\Property(name="secao_descricao")
     * @LG\Labels\Attributes(text="descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $secaoDescricao;

    /**
     * @var integer
     *
     * @ORM\Column(name="secao_ordem", type="integer", nullable=false, length=11)
     * @LG\Labels\Property(name="secao_ordem")
     * @LG\Labels\Attributes(text="ordem")
     * @LG\Querys\Conditions(type="=")
     */
    private $secaoOrdem;

    /**
     * @return integer
     */
    public function getSecaoId()
    {
        return $this->secaoId;
    }

    /**
     * @param integer $secaoId
     * @return AvaliacaoSecao
     */
    public function setSecaoId($secaoId)
    {
        $this->secaoId = $secaoId;

        return $this;
    }

    /**
     * @return string
     */
    public function getSecaoNome()
    {
        return $this->secaoNome;
    }

    /**
     * @param string $secaoNome
     * @return AvaliacaoSecao
     */
    public function setSecaoNome($secaoNome)
    {
        $this->secaoNome = $secaoNome;

        return $this;
    }

    /**
     * @return string
     */
    public function getSecaoDescricao()
    {
        return $this->secaoDescricao;
    }

    /**
     * @param string $secaoDescricao
     * @return AvaliacaoSecao
     */
    public function setSecaoDescricao($secaoDescricao)
    {
        $this->secaoDescricao = $secaoDescricao;

        return $this;
    }

    /**
     * @return integer
     */
    public function getSecaoOrdem()
    {
        return $this->secaoOrdem;
    }

    /**
     * @param integer $secaoOrdem
     * @return AvaliacaoSecao
     */
    public function setSecaoOrdem($secaoOrdem)
    {
        $this->secaoOrdem = $secaoOrdem;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'secaoId'        => $this->getSecaoId(),
            'secaoNome'      => $this->getSecaoNome(),
            'secaoDescricao' => $this->getSecaoDescricao(),
            'secaoOrdem'     => $this->getSecaoOrdem(),
        );

        return $array;
    }
}
