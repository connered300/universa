<?php

namespace Avaliacao\Service;

use VersaSpine\Service\AbstractService;

class AvaliacaoSecao extends AbstractService
{
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Avaliacao\Entity\AvaliacaoSecao');
    }

    public function pesquisaForJson($params)
    {
        $sql = '
        SELECT *
        FROM avaliacao__secao
        WHERE
            secao_descricao LIKE :secao_descricao';

        $secaoDescricao = false;
        $secaoId        = false;

        if ($params['q']) {
            $secaoDescricao = $params['q'];
        } elseif ($params['query']) {
            $secaoDescricao = $params['query'];
        }

        if ($params['secaoId']) {
            $secaoId = $params['secaoId'];
        }

        $parameters = array('secao_descricao' => "{$secaoDescricao}%");

        if ($secaoId) {
            $parameters['secao_id'] = $secaoId;
            $sql .= ' AND secao_id <> :secao_id';
        }

        $sql .= " ORDER BY secao_descricao";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['secaoId']) {
                /** @var $objAvaliacaoSecao \Avaliacao\Entity\AvaliacaoSecao */
                $objAvaliacaoSecao = $this->getRepository()->find($arrDados['secaoId']);

                if (!$objAvaliacaoSecao) {
                    $this->setLastError('Registro de seção não existe!');

                    return false;
                }
            } else {
                $objAvaliacaoSecao = new \Avaliacao\Entity\AvaliacaoSecao();
            }

            $objAvaliacaoSecao->setSecaoNome($arrDados['secaoNome']);
            $objAvaliacaoSecao->setSecaoDescricao($arrDados['secaoDescricao']);
            $objAvaliacaoSecao->setSecaoOrdem($arrDados['secaoOrdem']);

            $this->getEm()->persist($objAvaliacaoSecao);
            $this->getEm()->flush($objAvaliacaoSecao);

            $this->getEm()->commit();

            $arrDados['secaoId'] = $objAvaliacaoSecao->getSecaoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de seção!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['secaoNome']) {
            $errors[] = 'Por favor preencha o campo "nome"!';
        }

        if (!$arrParam['secaoOrdem']) {
            $errors[] = 'Por favor preencha o campo "ordem"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM avaliacao__secao";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($secaoId)
    {
        $arrDados = array();

        if (!$secaoId) {
            $this->setLastError('Seção inválido!');

            return array();
        }

        /** @var $objAvaliacaoSecao \Avaliacao\Entity\AvaliacaoSecao */
        $objAvaliacaoSecao = $this->getRepository()->find($secaoId);

        if (!$objAvaliacaoSecao) {
            $this->setLastError('Seção não existe!');

            return array();
        }

        try {
            $arrDados = $objAvaliacaoSecao->toArray();
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['secaoId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['secaoDescricao' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Avaliacao\Entity\AvaliacaoSecao */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getSecaoId();
            $arrEntity[$params['value']] = $objEntity->getSecaoDescricao();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($secaoId)
    {
        if (!$secaoId) {
            $this->setLastError('Para remover um registro de seção é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objAvaliacaoSecao \Avaliacao\Entity\AvaliacaoSecao */
            $objAvaliacaoSecao = $this->getRepository()->find($secaoId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAvaliacaoSecao);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de seção.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
    }
}
?>