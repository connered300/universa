<?php

namespace Avaliacao\Service;

use VersaSpine\Service\AbstractService;

class AvaliacaoQuestionarioRespondente extends AbstractService
{
    const RESPONDENTE_STATUS_CONCLUIDO = 'Concluído';
    const RESPONDENTE_STATUS_PARCIAL = 'Parcial';
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2RespondenteStatus($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getRespondenteStatus());
    }

    public static function getRespondenteStatus()
    {
        return array(self::RESPONDENTE_STATUS_CONCLUIDO, self::RESPONDENTE_STATUS_PARCIAL);
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Avaliacao\Entity\AvaliacaoQuestionarioRespondente');
    }

    public function pesquisaForJson($params)
    {
        $sql = '
        SELECT *
        FROM avaliacao__questionario_respondente
        WHERE
            secao_id LIKE :secao_id';

        $secaoId       = false;
        $respondenteId = false;

        if ($params['q']) {
            $secaoId = $params['q'];
        } elseif ($params['query']) {
            $secaoId = $params['query'];
        }

        if ($params['respondenteId']) {
            $respondenteId = $params['respondenteId'];
        }

        $parameters = array('secao_id' => "{$secaoId}%");

        if ($respondenteId) {
            $parameters['respondente_id'] = $respondenteId;
            $sql .= ' AND respondente_id <> :respondente_id';
        }

        $sql .= " ORDER BY secao_id";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceAvaliacaoSecao        = new \Avaliacao\Service\AvaliacaoSecao($this->getEm());
        $servicePessoa                = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceAvaliacaoQuestionario = new \Avaliacao\Service\AvaliacaoQuestionario($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['respondenteId']) {
                /** @var $objAvaliacaoQuestionarioRespondente \Avaliacao\Entity\AvaliacaoQuestionarioRespondente */
                $objAvaliacaoQuestionarioRespondente = $this->getRepository()->find($arrDados['respondenteId']);

                if (!$objAvaliacaoQuestionarioRespondente) {
                    $this->setLastError('Registro de questionario respondente não existe!');

                    return false;
                }
            } else {
                $objAvaliacaoQuestionarioRespondente = new \Avaliacao\Entity\AvaliacaoQuestionarioRespondente();
            }

            if ($arrDados['questionarioId']) {

                /** @var \Avaliacao\Entity\AvaliacaoQuestionario $objAvaliacaoQuestionario */
                $objAvaliacaoQuestionario = $serviceAvaliacaoQuestionario->getRepository()->find(
                    $arrDados['questionarioId']
                );

                if (!$objAvaliacaoQuestionario) {
                    $this->setLastError('Questionário inválido! ');

                    return false;
                }

                $objAvaliacaoQuestionarioRespondente->setQuestionario($objAvaliacaoQuestionario);
            }

            if ($arrDados['secao'] !== null) {
                /** @var $objAvaliacaoSecao \Avaliacao\Entity\AvaliacaoSecao */
                $objAvaliacaoSecao = $serviceAvaliacaoSecao->getRepository()->find($arrDados['secao']);

                if (!$objAvaliacaoSecao) {
                    $this->setLastError('Registro de seção não existe!');

                    return false;
                }

                $objAvaliacaoQuestionarioRespondente->setSecao($objAvaliacaoSecao);
            } else {
                $objAvaliacaoQuestionarioRespondente->setSecao(null);
            }

            $objAvaliacaoQuestionarioRespondente->setRespondenteStatus($arrDados['respondenteStatus']);
            $objAvaliacaoQuestionarioRespondente->setRespondenteData($arrDados['respondenteData']);

            if ($arrDados['pes']) {
                /** @var $objPessoa \Pessoa\Entity\Pessoa */
                $objPessoa = $servicePessoa->getRepository()->find($arrDados['pes']);

                if (!$objPessoa) {
                    $this->setLastError('Registro de pessoa não existe!');

                    return false;
                }

                $objAvaliacaoQuestionarioRespondente->setPes($objPessoa);
            } else {
                $objAvaliacaoQuestionarioRespondente->setPes(null);
            }

            $this->getEm()->persist($objAvaliacaoQuestionarioRespondente);
            $this->getEm()->flush($objAvaliacaoQuestionarioRespondente);

            $this->getEm()->commit();

            $arrDados['respondenteId'] = $objAvaliacaoQuestionarioRespondente->getRespondenteId();

            return $objAvaliacaoQuestionarioRespondente;
        } catch (\Exception $e) {
            $this->setLastError(
                'Não foi possível salvar o registro de questionario respondente!<br>' . $e->getMessage()
            );
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['respondenteStatus']) {
            $errors[] = 'Por favor preencha o campo "status"!';
        }
        if (!$arrParam['questionarioId']) {
            $errors[] = 'Questinario não localizado!';
        }

        if (!in_array($arrParam['respondenteStatus'], self::getRespondenteStatus())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "status"!';
        }

        if (!$arrParam['pes']) {
            $errors[] = 'Por favor preencha o campo "código pessoa"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM avaliacao__questionario_respondente";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($respondenteId)
    {
        $arrDados = array();

        if (!$respondenteId) {
            $this->setLastError('Questionario respondente inválido!');

            return array();
        }

        /** @var $objAvaliacaoQuestionarioRespondente \Avaliacao\Entity\AvaliacaoQuestionarioRespondente */
        $objAvaliacaoQuestionarioRespondente = $this->getRepository()->find($respondenteId);

        if (!$objAvaliacaoQuestionarioRespondente) {
            $this->setLastError('Questionario respondente não existe!');

            return array();
        }

        $serviceAvaliacaoSecao = new \Avaliacao\Service\AvaliacaoSecao($this->getEm());
        $servicePessoa         = new \Pessoa\Service\Pessoa($this->getEm());

        try {
            $arrDados = $objAvaliacaoQuestionarioRespondente->toArray();

            if ($arrDados['secao']) {
                $arrAvaliacaoSecao = $serviceAvaliacaoSecao->getArrSelect2(['id' => $arrDados['secao']]);
                $arrDados['secao'] = $arrAvaliacaoSecao ? $arrAvaliacaoSecao[0] : null;
            }

            if ($arrDados['pes']) {
                $arrPessoa       = $servicePessoa->getArrSelect2(['id' => $arrDados['pes']]);
                $arrDados['pes'] = $arrPessoa ? $arrPessoa[0] : null;
            }
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceAvaliacaoSecao = new \Avaliacao\Service\AvaliacaoSecao($this->getEm());
        $servicePessoa         = new \Pessoa\Service\Pessoa($this->getEm());

        if (is_array($arrDados['secao']) && !$arrDados['secao']['text']) {
            $arrDados['secao'] = $arrDados['secao']['secaoId'];
        }

        if ($arrDados['secao'] && is_string($arrDados['secao'])) {
            $arrDados['secao'] = $serviceAvaliacaoSecao->getArrSelect2(array('id' => $arrDados['secao']));
            $arrDados['secao'] = $arrDados['secao'] ? $arrDados['secao'][0] : null;
        }

        if (is_array($arrDados['pes']) && !$arrDados['pes']['text']) {
            $arrDados['pes'] = $arrDados['pes']['pesId'];
        }

        if ($arrDados['pes'] && is_string($arrDados['pes'])) {
            $arrDados['pes'] = $servicePessoa->getArrSelect2(array('id' => $arrDados['pes']));
            $arrDados['pes'] = $arrDados['pes'] ? $arrDados['pes'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['respondenteId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['secaoId' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Avaliacao\Entity\AvaliacaoQuestionarioRespondente */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getRespondenteId();
            $arrEntity[$params['value']] = $objEntity->getPes()->getPesNome();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($respondenteId)
    {
        if (!$respondenteId) {
            $this->setLastError('Para remover um registro de questionario respondente é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objAvaliacaoQuestionarioRespondente \Avaliacao\Entity\AvaliacaoQuestionarioRespondente */
            $objAvaliacaoQuestionarioRespondente = $this->getRepository()->find($respondenteId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAvaliacaoQuestionarioRespondente);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de questionario respondente.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceAvaliacaoSecao = new \Avaliacao\Service\AvaliacaoSecao($this->getEm());
        $servicePessoa         = new \Pessoa\Service\Pessoa($this->getEm());

        $serviceAvaliacaoSecao->setarDependenciasView($view);
        $servicePessoa->setarDependenciasView($view);

        $view->setVariable("arrRespondenteStatus", $this->getArrSelect2RespondenteStatus());
    }
}
?>