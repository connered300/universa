<?php

namespace Avaliacao\Service;

use VersaSpine\Service\AbstractService;

class AvaliacaoQuestao extends AbstractService
{
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Avaliacao\Entity\AvaliacaoQuestao');
    }

    public function pesquisaForJson($params)
    {
        $questaoDescricao = false;
        $questaoId        = false;

        if ($params['q']) {
            $questaoDescricao = $params['q'];
        } elseif ($params['query']) {
            $questaoDescricao = $params['query'];
        }

        $parameters = array('questao_descricao' => "{$questaoDescricao}%");
        
        if ($params['contador'] && $params['questionarioId']) {
            $sql = '
            SELECT
                *, (
                    SELECT count(DISTINCT(questao_id))
                    FROM avaliacao__questionario_questao
                    INNER JOIN avaliacao__questionario aq USING(questionario_id)
                    WHERE
                        aq.questionario_id = :questionarioId AND
                        avaliacao__questionario_questao.questionarioquestao_id NOT IN (
                            SELECT arrc.questionarioquestao_id FROM avaliacao__questionario_respondente aqresp
                            INNER JOIN avaliacao__respondente_resposta_concluida arrc USING(respondente_id)
                            WHERE
                                aqresp.pes_id = :pesId AND
                                aqresp.questionario_id = :questionarioId
                        )
                ) AS quantidadePerguntas
            FROM avaliacao__questao
            INNER JOIN avaliacao__questionario_questao USING(questao_id)
            WHERE questao_descricao LIKE :questao_descricao';

            $parameters["pesId"]          = $params['pes'];
            $parameters["questionarioId"] = $params['questionarioId'];
        } else {
            $sql = '
            SELECT * FROM avaliacao__questao
            WHERE questao_descricao LIKE :questao_descricao';
        }

        $orderBy          = " ORDER BY questao_descricao";

        if ($params['questaoId']) {
            $questaoId = $params['questaoId'];
        }

        if ($questaoId) {
            $parameters['questao_id'] = $questaoId;
            $sql .= ' AND questao_id = :questao_id';
        }

        if ($params['questoesId']) {
            $sql .= ' AND questao_id IN (' . $params['questoesId'] . ')';
            $orderBy = '';
        }

        $sql .= $orderBy;

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceAvaliacaoQuestaoTipo      = new \Avaliacao\Service\AvaliacaoQuestaoTipo($this->getEm());
        $serviceAvaliacaoQuestaoAplicacao = new \Avaliacao\Service\AvaliacaoQuestaoAplicacao($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['questaoId']) {
                /** @var $objAvaliacaoQuestao \Avaliacao\Entity\AvaliacaoQuestao */
                $objAvaliacaoQuestao = $this->getRepository()->find($arrDados['questaoId']);

                if (!$objAvaliacaoQuestao) {
                    $this->setLastError('Registro de questão não existe!');

                    return false;
                }
            } else {
                $objAvaliacaoQuestao = new \Avaliacao\Entity\AvaliacaoQuestao();
            }

            if ($arrDados['questaotipo']) {
                /** @var $objAvaliacaoQuestaoTipo \Avaliacao\Entity\AvaliacaoQuestaoTipo */
                $objAvaliacaoQuestaoTipo = $serviceAvaliacaoQuestaoTipo->getRepository()->find(
                    $arrDados['questaotipo']
                );

                if (!$objAvaliacaoQuestaoTipo) {
                    $this->setLastError('Registro de questão tipo não existe!');

                    return false;
                }

                $objAvaliacaoQuestao->setQuestaotipo($objAvaliacaoQuestaoTipo);
            } else {
                $objAvaliacaoQuestao->setQuestaotipo(null);
            }

            if ($arrDados['aplicacao']) {
                /** @var $objAvaliacaoQuestaoAplicacao \Avaliacao\Entity\AvaliacaoQuestaoAplicacao */
                $objAvaliacaoQuestaoAplicacao = $serviceAvaliacaoQuestaoAplicacao->getRepository()->find(
                    $arrDados['aplicacao']
                );

                if (!$objAvaliacaoQuestaoAplicacao) {
                    $this->setLastError('Registro de questão aplicação não existe!');

                    return false;
                }

                $objAvaliacaoQuestao->setAplicacao($objAvaliacaoQuestaoAplicacao);
            } else {
                $objAvaliacaoQuestao->setAplicacao(null);
            }

            $objAvaliacaoQuestao->setQuestaoDescricao($arrDados['questaoDescricao']);

            $this->getEm()->persist($objAvaliacaoQuestao);
            $this->getEm()->flush($objAvaliacaoQuestao);

            $this->getEm()->commit();

            $arrDados['questaoId'] = $objAvaliacaoQuestao->getQuestaoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de questão!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['questaotipo']) {
            $errors[] = 'Por favor preencha o campo "Tipo da Questão"!';
        }

        if (!$arrParam['aplicacao']) {
            $errors[] = 'Por favor preencha o campo "Aplicação"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM avaliacao__questao";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($questaoId)
    {
        $arrDados = array();

        if (!$questaoId) {
            $this->setLastError('Questão inválido!');

            return array();
        }

        /** @var $objAvaliacaoQuestao \Avaliacao\Entity\AvaliacaoQuestao */
        $objAvaliacaoQuestao = $this->getRepository()->find($questaoId);

        if (!$objAvaliacaoQuestao) {
            $this->setLastError('Questão não existe!');

            return array();
        }

        $serviceAvaliacaoQuestaoTipo      = new \Avaliacao\Service\AvaliacaoQuestaoTipo($this->getEm());
        $serviceAvaliacaoQuestaoAplicacao = new \Avaliacao\Service\AvaliacaoQuestaoAplicacao($this->getEm());

        try {
            $arrDados = $objAvaliacaoQuestao->toArray();

            if ($arrDados['questaotipo']) {
                $arrAvaliacaoQuestaoTipo = $serviceAvaliacaoQuestaoTipo->getArrSelect2(
                    ['id' => $arrDados['questaotipo']]
                );
                $arrDados['questaotipo'] = $arrAvaliacaoQuestaoTipo ? $arrAvaliacaoQuestaoTipo[0] : null;
            }

            if ($arrDados['aplicacao']) {
                $arrAvaliacaoQuestaoAplicacao = $serviceAvaliacaoQuestaoAplicacao->getArrSelect2(
                    ['id' => $arrDados['aplicacao']]
                );
                $arrDados['aplicacao']        = $arrAvaliacaoQuestaoAplicacao ? $arrAvaliacaoQuestaoAplicacao[0] : null;
            }
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceAvaliacaoQuestaoTipo      = new \Avaliacao\Service\AvaliacaoQuestaoTipo($this->getEm());
        $serviceAvaliacaoQuestaoAplicacao = new \Avaliacao\Service\AvaliacaoQuestaoAplicacao($this->getEm());

        if (is_array($arrDados['questaotipo']) && !$arrDados['questaotipo']['text']) {
            $arrDados['questaotipo'] = $arrDados['questaotipo']['questaotipoId'];
        }

        if ($arrDados['questaotipo'] && is_string($arrDados['questaotipo'])) {
            $arrDados['questaotipo'] = $serviceAvaliacaoQuestaoTipo->getArrSelect2(
                array('id' => $arrDados['questaotipo'])
            );
            $arrDados['questaotipo'] = $arrDados['questaotipo'] ? $arrDados['questaotipo'][0] : null;
        }

        if (is_array($arrDados['aplicacao']) && !$arrDados['aplicacao']['text']) {
            $arrDados['aplicacao'] = $arrDados['aplicacao']['aplicacaoId'];
        }

        if ($arrDados['aplicacao'] && is_string($arrDados['aplicacao'])) {
            $arrDados['aplicacao'] = $serviceAvaliacaoQuestaoAplicacao->getArrSelect2(
                array('id' => $arrDados['aplicacao'])
            );
            $arrDados['aplicacao'] = $arrDados['aplicacao'] ? $arrDados['aplicacao'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['questaoId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['questaoDescricao' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Avaliacao\Entity\AvaliacaoQuestao */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getQuestaoId();
            $arrEntity[$params['value']] = $objEntity->getQuestaoDescricao();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($questaoId)
    {
        if (!$questaoId) {
            $this->setLastError('Para remover um registro de questão é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objAvaliacaoQuestao \Avaliacao\Entity\AvaliacaoQuestao */
            $objAvaliacaoQuestao = $this->getRepository()->find($questaoId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAvaliacaoQuestao);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de questão.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceAvaliacaoQuestaoTipo      = new \Avaliacao\Service\AvaliacaoQuestaoTipo($this->getEm());
        $serviceAvaliacaoQuestaoAplicacao = new \Avaliacao\Service\AvaliacaoQuestaoAplicacao($this->getEm());

        $serviceAvaliacaoQuestaoTipo->setarDependenciasView($view);
        $serviceAvaliacaoQuestaoAplicacao->setarDependenciasView($view);
    }
}
?>