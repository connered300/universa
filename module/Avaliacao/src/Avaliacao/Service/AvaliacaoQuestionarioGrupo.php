<?php

namespace Avaliacao\Service;

use VersaSpine\Service\AbstractService;

class AvaliacaoQuestionarioGrupo extends AbstractService
{
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Avaliacao\Entity\AvaliacaoQuestionarioGrupo');
    }

    public function pesquisaForJson($params)
    {
        $sql = '
        SELECT *
        FROM avaliacao__questionario_grupo
        WHERE
            questionario_id LIKE :questionario_id';

        $questionarioId = false;
        $questionarioId = false;

        if ($params['q']) {
            $questionarioId = $params['q'];
        } elseif ($params['query']) {
            $questionarioId = $params['query'];
        }

        if ($params['questionarioId']) {
            $questionarioId = $params['questionarioId'];
        }

        $parameters = array('questionario_id' => "{$questionarioId}%");

        if ($questionarioId) {
            $parameters['questionario_id'] = $questionarioId;
            $sql .= ' AND questionario_id <> :questionario_id';
        }

        $sql .= " ORDER BY questionario_id";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceAcessoGrupo           = new \Acesso\Service\AcessoGrupo($this->getEm());
        $serviceAvaliacaoQuestionario = new \Avaliacao\Service\AvaliacaoQuestionario($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['questionarioId']) {
                /** @var $objAvaliacaoQuestionarioGrupo \Avaliacao\Entity\AvaliacaoQuestionarioGrupo */
                $objAvaliacaoQuestionarioGrupo = $this->getRepository()->find($arrDados['questionarioId']);

                if (!$objAvaliacaoQuestionarioGrupo) {
                    $this->setLastError('Registro de questionario grupo não existe!');

                    return false;
                }
            } else {
                $objAvaliacaoQuestionarioGrupo = new \Avaliacao\Entity\AvaliacaoQuestionarioGrupo();
            }

            if ($arrDados['grupo']) {
                /** @var $objAcessoGrupo \Acesso\Entity\AcessoGrupo */
                $objAcessoGrupo = $serviceAcessoGrupo->getRepository()->find($arrDados['grupo']);

                if (!$objAcessoGrupo) {
                    $this->setLastError('Registro de grupo não existe!');

                    return false;
                }

                $objAvaliacaoQuestionarioGrupo->setGrupo($objAcessoGrupo);
            } else {
                $objAvaliacaoQuestionarioGrupo->setGrupo(null);
            }

            if ($arrDados['questionario']) {
                /** @var $objAvaliacaoQuestionario \Avaliacao\Entity\AvaliacaoQuestionario */
                $objAvaliacaoQuestionario = $serviceAvaliacaoQuestionario->getRepository()->find(
                    $arrDados['questionario']
                );

                if (!$objAvaliacaoQuestionario) {
                    $this->setLastError('Registro de questionario não existe!');

                    return false;
                }

                $objAvaliacaoQuestionarioGrupo->setQuestionario($objAvaliacaoQuestionario);
            } else {
                $objAvaliacaoQuestionarioGrupo->setQuestionario(null);
            }

            $this->getEm()->persist($objAvaliacaoQuestionarioGrupo);
            $this->getEm()->flush($objAvaliacaoQuestionarioGrupo);

            $this->getEm()->commit();

            $arrDados['questionarioId'] = $objAvaliacaoQuestionarioGrupo->getQuestionario()->getQuestionarioId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de questionario grupo!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM avaliacao__questionario_grupo";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($questionarioId)
    {
        $arrDados = array();

        if (!$questionarioId) {
            $this->setLastError('Questionario grupo inválido!');

            return array();
        }

        /** @var $objAvaliacaoQuestionarioGrupo \Avaliacao\Entity\AvaliacaoQuestionarioGrupo */
        $objAvaliacaoQuestionarioGrupo = $this->getRepository()->find($questionarioId);

        if (!$objAvaliacaoQuestionarioGrupo) {
            $this->setLastError('Questionario grupo não existe!');

            return array();
        }

        $serviceAcessoGrupo           = new \Acesso\Service\AcessoGrupo($this->getEm());
        $serviceAvaliacaoQuestionario = new \Avaliacao\Service\AvaliacaoQuestionario($this->getEm());

        try {
            $arrDados = $objAvaliacaoQuestionarioGrupo->toArray();

            if ($arrDados['grupo']) {
                $arrAcessoGrupo    = $serviceAcessoGrupo->getArrSelect2(['id' => $arrDados['grupo']]);
                $arrDados['grupo'] = $arrAcessoGrupo ? $arrAcessoGrupo[0] : null;
            }

            if ($arrDados['questionario']) {
                $arrAvaliacaoQuestionario = $serviceAvaliacaoQuestionario->getArrSelect2(
                    ['id' => $arrDados['questionario']]
                );
                $arrDados['questionario'] = $arrAvaliacaoQuestionario ? $arrAvaliacaoQuestionario[0] : null;
            }
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceAcessoGrupo           = new \Acesso\Service\AcessoGrupo($this->getEm());
        $serviceAvaliacaoQuestionario = new \Avaliacao\Service\AvaliacaoQuestionario($this->getEm());

        if (is_array($arrDados['grupo']) && !$arrDados['grupo']['text']) {
            $arrDados['grupo'] = $arrDados['grupo']['id'];
        }

        if ($arrDados['grupo'] && is_string($arrDados['grupo'])) {
            $arrDados['grupo'] = $serviceAcessoGrupo->getArrSelect2(array('id' => $arrDados['grupo']));
            $arrDados['grupo'] = $arrDados['grupo'] ? $arrDados['grupo'][0] : null;
        }

        if (is_array($arrDados['questionario']) && !$arrDados['questionario']['text']) {
            $arrDados['questionario'] = $arrDados['questionario']['questionarioId'];
        }

        if ($arrDados['questionario'] && is_string($arrDados['questionario'])) {
            $arrDados['questionario'] = $serviceAvaliacaoQuestionario->getArrSelect2(
                array('id' => $arrDados['questionario'])
            );
            $arrDados['questionario'] = $arrDados['questionario'] ? $arrDados['questionario'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['questionarioId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['questionarioId' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Avaliacao\Entity\AvaliacaoQuestionarioGrupo */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getQuestionario()->getQuestionarioId();
            $arrEntity[$params['value']] = $objEntity->getQuestionario()->getQuestionarioId();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($questionarioId)
    {
        if (!$questionarioId) {
            $this->setLastError('Para remover um registro de questionario grupo é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objAvaliacaoQuestionarioGrupo \Avaliacao\Entity\AvaliacaoQuestionarioGrupo */
            $objAvaliacaoQuestionarioGrupo = $this->getRepository()->find($questionarioId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAvaliacaoQuestionarioGrupo);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de questionario grupo.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceAcessoGrupo           = new \Acesso\Service\AcessoGrupo($this->getEm());
        $serviceAvaliacaoQuestionario = new \Avaliacao\Service\AvaliacaoQuestionario($this->getEm());

        $serviceAcessoGrupo->setarDependenciasView($view);
        $serviceAvaliacaoQuestionario->setarDependenciasView($view);
    }
}
?>