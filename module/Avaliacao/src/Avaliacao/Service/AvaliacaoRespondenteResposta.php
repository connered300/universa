<?php

namespace Avaliacao\Service;

use VersaSpine\Service\AbstractService;

class AvaliacaoRespondenteResposta extends AbstractService
{
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Avaliacao\Entity\AvaliacaoRespondenteResposta');
    }

    public function pesquisaForJson($params)
    {
        $sql = '
        SELECT *
        FROM avaliacao__respondente_resposta
        WHERE
            questionarioquestao_id LIKE :questionarioquestao_id';

        $questionarioquestaoId = false;
        $respondenterespostaId = false;

        if ($params['q']) {
            $questionarioquestaoId = $params['q'];
        } elseif ($params['query']) {
            $questionarioquestaoId = $params['query'];
        }

        if ($params['respondenterespostaId']) {
            $respondenterespostaId = $params['respondenterespostaId'];
        }

        $parameters = array('questionarioquestao_id' => "{$questionarioquestaoId}%");

        if ($respondenterespostaId) {
            $parameters['respondenteresposta_id'] = $respondenterespostaId;
            $sql .= ' AND respondenteresposta_id <> :respondenteresposta_id';
        }

        $sql .= " ORDER BY questionarioquestao_id";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceAvaliacaoQuestionarioQuestao     = new \Avaliacao\Service\AvaliacaoQuestionarioQuestao($this->getEm());
        $serviceAvaliacaoQuestaoResposta         = new \Avaliacao\Service\AvaliacaoQuestaoResposta($this->getEm());
        $serviceAvaliacaoQuestionarioRespondente = new \Avaliacao\Service\AvaliacaoQuestionarioRespondente(
            $this->getEm()
        );

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['respondenterespostaId']) {
                /** @var $objAvaliacaoRespondenteResposta \Avaliacao\Entity\AvaliacaoRespondenteResposta */
                $objAvaliacaoRespondenteResposta = $this->getRepository()->find($arrDados['respondenterespostaId']);

                if (!$objAvaliacaoRespondenteResposta) {
                    $this->setLastError('Registro de respondente resposta não existe!');

                    return false;
                }
            } else {
                $objAvaliacaoRespondenteResposta = new \Avaliacao\Entity\AvaliacaoRespondenteResposta();
            }

            if ($arrDados['questionarioquestao']) {
                /** @var $objAvaliacaoQuestionarioQuestao \Avaliacao\Entity\AvaliacaoQuestionarioQuestao */
                $objAvaliacaoQuestionarioQuestao = $serviceAvaliacaoQuestionarioQuestao->getRepository()->find(
                    $arrDados['questionarioquestao']
                );

                if (!$objAvaliacaoQuestionarioQuestao) {
                    $this->setLastError('Registro de questionario questão não existe!');

                    return false;
                }

                $objAvaliacaoRespondenteResposta->setQuestionarioquestao($objAvaliacaoQuestionarioQuestao);
            } else {
                $objAvaliacaoRespondenteResposta->setQuestionarioquestao(null);
            }

            if ($arrDados['questaoresposta']) {
                /** @var $objAvaliacaoQuestaoResposta \Avaliacao\Entity\AvaliacaoQuestaoResposta */
                $objAvaliacaoQuestaoResposta = $serviceAvaliacaoQuestaoResposta->getRepository()->find(
                    $arrDados['questaoresposta']
                );

                if (!$objAvaliacaoQuestaoResposta) {
                    $this->setLastError('Registro de questão resposta não existe!');

                    return false;
                }

                $objAvaliacaoRespondenteResposta->setQuestaoresposta($objAvaliacaoQuestaoResposta);
            } elseif ($arrDados['respostaAberta']) {
                $objAvaliacaoRespondenteResposta->setRespostaAberta($arrDados['respostaAberta']);
            } else {
                $objAvaliacaoRespondenteResposta->setQuestaoresposta(null);
            }

            if ($arrDados['respondente']) {
                /** @var $objAvaliacaoQuestionarioRespondente \Avaliacao\Entity\AvaliacaoQuestionarioRespondente */
                $objAvaliacaoQuestionarioRespondente = $serviceAvaliacaoQuestionarioRespondente->getRepository()->find(
                    $arrDados['respondente']
                );

                if (!$objAvaliacaoQuestionarioRespondente) {
                    $this->setLastError('Registro de questionario respondente não existe!');

                    return false;
                }

                $objAvaliacaoRespondenteResposta->setRespondente($objAvaliacaoQuestionarioRespondente);
            } else {
                $objAvaliacaoRespondenteResposta->setRespondente(null);
            }

            $objAvaliacaoRespondenteResposta->setRespondenterespostaPeso($arrDados['respondenterespostaPeso']);
            $objAvaliacaoRespondenteResposta->setRespondenterespostaComplemento(
                $arrDados['respondenterespostaComplemento']
            );

            $this->getEm()->persist($objAvaliacaoRespondenteResposta);
            $this->getEm()->flush($objAvaliacaoRespondenteResposta);

            $this->getEm()->commit();

            $arrDados['respondenterespostaId'] = $objAvaliacaoRespondenteResposta->getRespondenterespostaId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de respondente resposta!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['questionarioquestao']) {
            $errors[] = 'Por favor preencha o campo "Código"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM avaliacao__respondente_resposta";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($respondenterespostaId)
    {
        $arrDados = array();

        if (!$respondenterespostaId) {
            $this->setLastError('Respondente resposta inválido!');

            return array();
        }

        /** @var $objAvaliacaoRespondenteResposta \Avaliacao\Entity\AvaliacaoRespondenteResposta */
        $objAvaliacaoRespondenteResposta = $this->getRepository()->find($respondenterespostaId);

        if (!$objAvaliacaoRespondenteResposta) {
            $this->setLastError('Respondente resposta não existe!');

            return array();
        }

        $serviceAvaliacaoQuestionarioQuestao     = new \Avaliacao\Service\AvaliacaoQuestionarioQuestao($this->getEm());
        $serviceAvaliacaoQuestaoResposta         = new \Avaliacao\Service\AvaliacaoQuestaoResposta($this->getEm());
        $serviceAvaliacaoQuestionarioRespondente = new \Avaliacao\Service\AvaliacaoQuestionarioRespondente(
            $this->getEm()
        );

        try {
            $arrDados = $objAvaliacaoRespondenteResposta->toArray();

            if ($arrDados['questionarioquestao']) {
                $arrAvaliacaoQuestionarioQuestao = $serviceAvaliacaoQuestionarioQuestao->getArrSelect2(
                    ['id' => $arrDados['questionarioquestao']]
                );
                $arrDados['questionarioquestao'] = $arrAvaliacaoQuestionarioQuestao ? $arrAvaliacaoQuestionarioQuestao[0] : null;
            }

            if ($arrDados['questaoresposta']) {
                $arrAvaliacaoQuestaoResposta = $serviceAvaliacaoQuestaoResposta->getArrSelect2(
                    ['id' => $arrDados['questaoresposta']]
                );
                $arrDados['questaoresposta'] = $arrAvaliacaoQuestaoResposta ? $arrAvaliacaoQuestaoResposta[0] : null;
            }

            if ($arrDados['respondente']) {
                $arrAvaliacaoQuestionarioRespondente = $serviceAvaliacaoQuestionarioRespondente->getArrSelect2(
                    ['id' => $arrDados['respondente']]
                );
                $arrDados['respondente']             = $arrAvaliacaoQuestionarioRespondente ? $arrAvaliacaoQuestionarioRespondente[0] : null;
            }
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceAvaliacaoQuestionarioQuestao     = new \Avaliacao\Service\AvaliacaoQuestionarioQuestao($this->getEm());
        $serviceAvaliacaoQuestaoResposta         = new \Avaliacao\Service\AvaliacaoQuestaoResposta($this->getEm());
        $serviceAvaliacaoQuestionarioRespondente = new \Avaliacao\Service\AvaliacaoQuestionarioRespondente(
            $this->getEm()
        );

        if (is_array($arrDados['questionarioquestao']) && !$arrDados['questionarioquestao']['text']) {
            $arrDados['questionarioquestao'] = $arrDados['questionarioquestao']['questionarioquestaoId'];
        }

        if ($arrDados['questionarioquestao'] && is_string($arrDados['questionarioquestao'])) {
            $arrDados['questionarioquestao'] = $serviceAvaliacaoQuestionarioQuestao->getArrSelect2(
                array('id' => $arrDados['questionarioquestao'])
            );
            $arrDados['questionarioquestao'] = $arrDados['questionarioquestao'] ? $arrDados['questionarioquestao'][0] : null;
        }

        if (is_array($arrDados['questaoresposta']) && !$arrDados['questaoresposta']['text']) {
            $arrDados['questaoresposta'] = $arrDados['questaoresposta']['questaorespostaId'];
        }

        if ($arrDados['questaoresposta'] && is_string($arrDados['questaoresposta'])) {
            $arrDados['questaoresposta'] = $serviceAvaliacaoQuestaoResposta->getArrSelect2(
                array('id' => $arrDados['questaoresposta'])
            );
            $arrDados['questaoresposta'] = $arrDados['questaoresposta'] ? $arrDados['questaoresposta'][0] : null;
        }

        if (is_array($arrDados['respondente']) && !$arrDados['respondente']['text']) {
            $arrDados['respondente'] = $arrDados['respondente']['respondenteId'];
        }

        if ($arrDados['respondente'] && is_string($arrDados['respondente'])) {
            $arrDados['respondente'] = $serviceAvaliacaoQuestionarioRespondente->getArrSelect2(
                array('id' => $arrDados['respondente'])
            );
            $arrDados['respondente'] = $arrDados['respondente'] ? $arrDados['respondente'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['respondenterespostaId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['questionarioquestaoId' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Avaliacao\Entity\AvaliacaoRespondenteResposta */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getRespondenterespostaId();
            $arrEntity[$params['value']] = $objEntity->getRespondenterespostaId();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($respondenterespostaId)
    {
        if (!$respondenterespostaId) {
            $this->setLastError('Para remover um registro de respondente resposta é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objAvaliacaoRespondenteResposta \Avaliacao\Entity\AvaliacaoRespondenteResposta */
            $objAvaliacaoRespondenteResposta = $this->getRepository()->find($respondenterespostaId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAvaliacaoRespondenteResposta);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de respondente resposta.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceAvaliacaoQuestionarioQuestao     = new \Avaliacao\Service\AvaliacaoQuestionarioQuestao($this->getEm());
        $serviceAvaliacaoQuestaoResposta         = new \Avaliacao\Service\AvaliacaoQuestaoResposta($this->getEm());
        $serviceAvaliacaoQuestionarioRespondente = new \Avaliacao\Service\AvaliacaoQuestionarioRespondente(
            $this->getEm()
        );

        $serviceAvaliacaoQuestionarioQuestao->setarDependenciasView($view);
        $serviceAvaliacaoQuestaoResposta->setarDependenciasView($view);
        $serviceAvaliacaoQuestionarioRespondente->setarDependenciasView($view);
    }
}
?>