<?php

namespace Avaliacao\Service;

use VersaSpine\Service\AbstractService;

class AvaliacaoQuestaoAplicacao extends AbstractService
{
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Avaliacao\Entity\AvaliacaoQuestaoAplicacao');
    }

    public function pesquisaForJson($params)
    {
        $sql = '
        SELECT *
        FROM avaliacao__questao_aplicacao
        WHERE
            aplicacao_descricao LIKE :aplicacao_descricao';

        $aplicacaoDescricao = false;
        $aplicacaoId        = false;

        if ($params['q']) {
            $aplicacaoDescricao = $params['q'];
        } elseif ($params['query']) {
            $aplicacaoDescricao = $params['query'];
        }

        if ($params['aplicacaoId']) {
            $aplicacaoId = $params['aplicacaoId'];
        }

        $parameters = array('aplicacao_descricao' => "{$aplicacaoDescricao}%");

        if ($aplicacaoId) {
            $parameters['aplicacao_id'] = $aplicacaoId;
            $sql .= ' AND aplicacao_id <> :aplicacao_id';
        }

        $sql .= " ORDER BY aplicacao_descricao";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            $aplicacaoId                  = ($arrDados['aplicacaoIdOld'] ? $arrDados['aplicacaoIdOld'] : null);
            $aplicacaoId                  = (!$aplicacaoId && $arrDados['aplicacaoId'] ? $arrDados['aplicacaoId'] : null);
            $objAvaliacaoQuestaoAplicacao = null;

            if ($aplicacaoId) {
                /** @var $objAvaliacaoQuestaoAplicacao \Avaliacao\Entity\AvaliacaoQuestaoAplicacao */
                $objAvaliacaoQuestaoAplicacao = $this->getRepository()->find($aplicacaoId);
            }

            if (!$objAvaliacaoQuestaoAplicacao) {
                $objAvaliacaoQuestaoAplicacao = new \Avaliacao\Entity\AvaliacaoQuestaoAplicacao();
            }

            $objAvaliacaoQuestaoAplicacao->setaplicacaoId($arrDados['aplicacaoId']);
            $objAvaliacaoQuestaoAplicacao->setAplicacaoDescricao($arrDados['aplicacaoDescricao']);

            $this->getEm()->persist($objAvaliacaoQuestaoAplicacao);
            $this->getEm()->flush($objAvaliacaoQuestaoAplicacao);

            $this->getEm()->commit();

            $arrDados['aplicacaoId'] = $objAvaliacaoQuestaoAplicacao->getAplicacaoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de questão aplicação!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM avaliacao__questao_aplicacao";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($aplicacaoId)
    {
        $arrDados = array();

        if (!$aplicacaoId) {
            $this->setLastError('Questão aplicação inválido!');

            return array();
        }

        /** @var $objAvaliacaoQuestaoAplicacao \Avaliacao\Entity\AvaliacaoQuestaoAplicacao */
        $objAvaliacaoQuestaoAplicacao = $this->getRepository()->find($aplicacaoId);

        if (!$objAvaliacaoQuestaoAplicacao) {
            $this->setLastError('Questão aplicação não existe!');

            return array();
        }

        try {
            $arrDados = $objAvaliacaoQuestaoAplicacao->toArray();
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['aplicacaoId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['aplicacaoDescricao' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Avaliacao\Entity\AvaliacaoQuestaoAplicacao */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getAplicacaoId();
            $arrEntity[$params['value']] = $objEntity->getAplicacaoDescricao();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($aplicacaoId)
    {
        if (!$aplicacaoId) {
            $this->setLastError('Para remover um registro de questão aplicação é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objAvaliacaoQuestaoAplicacao \Avaliacao\Entity\AvaliacaoQuestaoAplicacao */
            $objAvaliacaoQuestaoAplicacao = $this->getRepository()->find($aplicacaoId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAvaliacaoQuestaoAplicacao);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de questão aplicação.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
    }
}
?>