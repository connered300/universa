<?php

namespace Avaliacao\Service;

use VersaSpine\Service\AbstractService;

class AvaliacaoFim extends AbstractService
{
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Avaliacao\Entity\AvaliacaoFim');
    }

    public function pesquisaForJson($params)
    {
        $sql = '
        SELECT *
        FROM avaliacao__fim
        WHERE
            fim_descricao LIKE :fim_descricao';

        $fimDescricao = false;
        $fimId        = false;

        if ($params['q']) {
            $fimDescricao = $params['q'];
        } elseif ($params['query']) {
            $fimDescricao = $params['query'];
        }

        if ($params['fimId']) {
            $fimId = $params['fimId'];
        }

        $parameters = array('fim_descricao' => "{$fimDescricao}%");

        if ($fimId) {
            $parameters['fim_id'] = $fimId;
            $sql .= ' AND fim_id <> :fim_id';
        }

        $sql .= " ORDER BY fim_descricao";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            $fimId           = ($arrDados['fimIdOld'] ? $arrDados['fimIdOld'] : null);
            $fimId           = (!$fimId && $arrDados['fimId'] ? $arrDados['fimId'] : null);
            $objAvaliacaoFim = null;

            if ($fimId) {
                /** @var $objAvaliacaoFim \Avaliacao\Entity\AvaliacaoFim */
                $objAvaliacaoFim = $this->getRepository()->find($fimId);
            }

            if (!$objAvaliacaoFim) {
                $objAvaliacaoFim = new \Avaliacao\Entity\AvaliacaoFim();
            }

            $objAvaliacaoFim->setFimId($arrDados['fimId']);
            $objAvaliacaoFim->setFimNome($arrDados['fimNome']);
            $objAvaliacaoFim->setFimDescricao($arrDados['fimDescricao']);

            $this->getEm()->persist($objAvaliacaoFim);
            $this->getEm()->flush($objAvaliacaoFim);

            $this->getEm()->commit();

            $arrDados['fimId'] = $objAvaliacaoFim->getFimId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de fim!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['fimId']) {
            $errors[] = 'Por favor preencha o campo "Id"!';
        }

        if (!$arrParam['fimNome']) {
            $errors[] = 'Por favor preencha o campo "Nome"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM avaliacao__fim";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($fimId)
    {
        $arrDados = array();

        if (!$fimId) {
            $this->setLastError('Fim inválido!');

            return array();
        }

        /** @var $objAvaliacaoFim \Avaliacao\Entity\AvaliacaoFim */
        $objAvaliacaoFim = $this->getRepository()->find($fimId);

        if (!$objAvaliacaoFim) {
            $this->setLastError('Fim não existe!');

            return array();
        }

        try {
            $arrDados = $objAvaliacaoFim->toArray();
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['fimId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['fimDescricao' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Avaliacao\Entity\AvaliacaoFim */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getFimId();
            $arrEntity[$params['value']] = $objEntity->getFimDescricao();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($fimId)
    {
        if (!$fimId) {
            $this->setLastError('Para remover um registro de fim é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objAvaliacaoFim \Avaliacao\Entity\AvaliacaoFim */
            $objAvaliacaoFim = $this->getRepository()->find($fimId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAvaliacaoFim);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de fim.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
    }
}
?>