<?php

namespace Avaliacao\Service;

use VersaSpine\Service\AbstractService;

class AvaliacaoQuestaoResposta extends AbstractService
{
    const QUESTAORESPOSTA_GABARITO_SIM           = 'Sim';
    const QUESTAORESPOSTA_GABARITO_NAO           = 'Não';
    const QUESTAORESPOSTA_GABARITO_NAO_APLICAVEL = 'Não Aplicável';
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2QuestaorespostaGabarito($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getQuestaorespostaGabarito());
    }

    public static function getQuestaorespostaGabarito()
    {
        return array(
            self::QUESTAORESPOSTA_GABARITO_SIM,
            self::QUESTAORESPOSTA_GABARITO_NAO,
            self::QUESTAORESPOSTA_GABARITO_NAO_APLICAVEL
        );
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Avaliacao\Entity\AvaliacaoQuestaoResposta');
    }

    public function pesquisaForJson($params)
    {
        $sql = '
        SELECT *
        FROM avaliacao__questao_resposta
        WHERE
            questaoresposta_descricao LIKE :questaoresposta_descricao';

        $questaorespostaDescricao = false;
        $questaorespostaId        = false;

        if ($params['q']) {
            $questaorespostaDescricao = $params['q'];
        } elseif ($params['query']) {
            $questaorespostaDescricao = $params['query'];
        }

        if ($params['questaorespostaId']) {
            $questaorespostaId = $params['questaorespostaId'];
        }

        $parameters = array('questaoresposta_descricao' => "{$questaorespostaDescricao}%");

        if ($questaorespostaId) {
            $parameters['questaoresposta_id'] = $questaorespostaId;
            $sql .= ' AND questaoresposta_id <> :questaoresposta_id';
        }

        $sql .= " ORDER BY questaoresposta_descricao";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceAvaliacaoQuestao = new \Avaliacao\Service\AvaliacaoQuestao($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['questaorespostaId']) {
                /** @var $objAvaliacaoQuestaoResposta \Avaliacao\Entity\AvaliacaoQuestaoResposta */
                $objAvaliacaoQuestaoResposta = $this->getRepository()->find($arrDados['questaorespostaId']);

                if (!$objAvaliacaoQuestaoResposta) {
                    $this->setLastError('Registro de questão resposta não existe!');

                    return false;
                }
            } else {
                $objAvaliacaoQuestaoResposta = new \Avaliacao\Entity\AvaliacaoQuestaoResposta();
            }

            if ($arrDados['questao']) {
                /** @var $objAvaliacaoQuestao \Avaliacao\Entity\AvaliacaoQuestao */
                $objAvaliacaoQuestao = $serviceAvaliacaoQuestao->getRepository()->find($arrDados['questao']);

                if (!$objAvaliacaoQuestao) {
                    $this->setLastError('Registro de questão não existe!');

                    return false;
                }

                $objAvaliacaoQuestaoResposta->setQuestao($objAvaliacaoQuestao);
            } else {
                $objAvaliacaoQuestaoResposta->setQuestao(null);
            }

            $objAvaliacaoQuestaoResposta->setQuestaorespostaDescricao($arrDados['questaorespostaDescricao']);
            $objAvaliacaoQuestaoResposta->setQuestaorespostaPosicao($arrDados['questaorespostaPosicao']);
            $objAvaliacaoQuestaoResposta->setQuestaorespostaGabarito($arrDados['questaorespostaGabarito']);

            $this->getEm()->persist($objAvaliacaoQuestaoResposta);
            $this->getEm()->flush($objAvaliacaoQuestaoResposta);

            $this->getEm()->commit();

            $arrDados['questaorespostaId'] = $objAvaliacaoQuestaoResposta->getQuestaorespostaId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de questão resposta!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['questao']) {
            $errors[] = 'Por favor preencha o campo "Questão"!';
        }

        if (!$arrParam['questaorespostaDescricao']) {
            $errors[] = 'Por favor preencha o campo "Descrição"!';
        }

        if (!$arrParam['questaorespostaGabarito']) {
            $errors[] = 'Por favor preencha o campo "Gabarito"!';
        }

        if (!in_array($arrParam['questaorespostaGabarito'], self::getQuestaorespostaGabarito())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "Gabarito"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM avaliacao__questao_resposta";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($questaorespostaId)
    {
        $arrDados = array();

        if (!$questaorespostaId) {
            $this->setLastError('Questão resposta inválido!');

            return array();
        }

        /** @var $objAvaliacaoQuestaoResposta \Avaliacao\Entity\AvaliacaoQuestaoResposta */
        $objAvaliacaoQuestaoResposta = $this->getRepository()->find($questaorespostaId);

        if (!$objAvaliacaoQuestaoResposta) {
            $this->setLastError('Questão resposta não existe!');

            return array();
        }

        $serviceAvaliacaoQuestao = new \Avaliacao\Service\AvaliacaoQuestao($this->getEm());

        try {
            $arrDados = $objAvaliacaoQuestaoResposta->toArray();

            if ($arrDados['questao']) {
                $arrAvaliacaoQuestao = $serviceAvaliacaoQuestao->getArrSelect2(['id' => $arrDados['questao']]);
                $arrDados['questao'] = $arrAvaliacaoQuestao ? $arrAvaliacaoQuestao[0] : null;
            }
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceAvaliacaoQuestao = new \Avaliacao\Service\AvaliacaoQuestao($this->getEm());

        if (is_array($arrDados['questao']) && !$arrDados['questao']['text']) {
            $arrDados['questao'] = $arrDados['questao']['questaoId'];
        }

        if ($arrDados['questao'] && is_string($arrDados['questao'])) {
            $arrDados['questao'] = $serviceAvaliacaoQuestao->getArrSelect2(array('id' => $arrDados['questao']));
            $arrDados['questao'] = $arrDados['questao'] ? $arrDados['questao'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['questaorespostaId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['questaorespostaDescricao' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Avaliacao\Entity\AvaliacaoQuestaoResposta */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getQuestaorespostaId();
            $arrEntity[$params['value']] = $objEntity->getQuestaorespostaDescricao();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($questaorespostaId)
    {
        if (!$questaorespostaId) {
            $this->setLastError('Para remover um registro de questão resposta é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objAvaliacaoQuestaoResposta \Avaliacao\Entity\AvaliacaoQuestaoResposta */
            $objAvaliacaoQuestaoResposta = $this->getRepository()->find($questaorespostaId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAvaliacaoQuestaoResposta);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de questão resposta.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceAvaliacaoQuestao = new \Avaliacao\Service\AvaliacaoQuestao($this->getEm());

        $serviceAvaliacaoQuestao->setarDependenciasView($view);

        $view->setVariable("arrQuestaorespostaGabarito", $this->getArrSelect2QuestaorespostaGabarito());
    }
}
?>