<?php

namespace Avaliacao\Service;

use VersaSpine\Service\AbstractService;

class AvaliacaoQuestionarioQuestao extends AbstractService
{
    const QUESTIONARIOQUESTAO_ANULADA_SIM     = 'Sim';
    const QUESTIONARIOQUESTAO_ANULADA_NAO     = 'Não';
    const QUESTIONARIOQUESTAO_OBRIGATORIA_SIM = 'Sim';
    const QUESTIONARIOQUESTAO_OBRIGATORIA_NAO = 'Não';
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2QuestionarioquestaoAnulada($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getQuestionarioquestaoAnulada());
    }

    public static function getQuestionarioquestaoAnulada()
    {
        return array(self::QUESTIONARIOQUESTAO_ANULADA_SIM, self::QUESTIONARIOQUESTAO_ANULADA_NAO);
    }

    public function getArrSelect2QuestionarioquestaoObrigatoria($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getQuestionarioquestaoObrigatoria());
    }

    public static function getQuestionarioquestaoObrigatoria()
    {
        return array(self::QUESTIONARIOQUESTAO_OBRIGATORIA_SIM, self::QUESTIONARIOQUESTAO_OBRIGATORIA_NAO);
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Avaliacao\Entity\AvaliacaoQuestionarioQuestao');
    }

    public function pesquisaForJson($params)
    {
        $sql = '
        SELECT *
        FROM avaliacao__questionario_questao
        WHERE
            questionario_id LIKE :questionario_id';

        $questionarioId        = false;
        $questionarioquestaoId = false;

        if ($params['q']) {
            $questionarioId = $params['q'];
        } elseif ($params['query']) {
            $questionarioId = $params['query'];
        }

        if ($params['questionarioquestaoId']) {
            $questionarioquestaoId = $params['questionarioquestaoId'];
        }

        $parameters = array('questionario_id' => "{$questionarioId}%");

        if ($questionarioquestaoId) {
            $parameters['questionarioquestao_id'] = $questionarioquestaoId;
            $sql .= ' AND questionarioquestao_id <> :questionarioquestao_id';
        }

        $sql .= " ORDER BY questionario_id";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceAvaliacaoQuestionario = new \Avaliacao\Service\AvaliacaoQuestionario($this->getEm());
        $serviceAvaliacaoSecao        = new \Avaliacao\Service\AvaliacaoSecao($this->getEm());
        $serviceAvaliacaoQuestao      = new \Avaliacao\Service\AvaliacaoQuestao($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['questionarioquestaoId']) {
                /** @var $objAvaliacaoQuestionarioQuestao \Avaliacao\Entity\AvaliacaoQuestionarioQuestao */
                $objAvaliacaoQuestionarioQuestao = $this->getRepository()->find($arrDados['questionarioquestaoId']);

                if (!$objAvaliacaoQuestionarioQuestao) {
                    $this->setLastError('Registro de questionario questão não existe!');

                    return false;
                }
            } else {
                $objAvaliacaoQuestionarioQuestao = new \Avaliacao\Entity\AvaliacaoQuestionarioQuestao();
            }

            if ($arrDados['questionario']) {
                /** @var $objAvaliacaoQuestionario \Avaliacao\Entity\AvaliacaoQuestionario */
                $objAvaliacaoQuestionario = $serviceAvaliacaoQuestionario->getRepository()->find(
                    $arrDados['questionario']
                );

                if (!$objAvaliacaoQuestionario) {
                    $this->setLastError('Registro de questionario não existe!');

                    return false;
                }

                $objAvaliacaoQuestionarioQuestao->setQuestionario($objAvaliacaoQuestionario);
            } else {
                $objAvaliacaoQuestionarioQuestao->setQuestionario(null);
            }

            if ($arrDados['secao']) {
                /** @var $objAvaliacaoSecao \Avaliacao\Entity\AvaliacaoSecao */
                $objAvaliacaoSecao = $serviceAvaliacaoSecao->getRepository()->find($arrDados['secao']);

                if (!$objAvaliacaoSecao) {
                    $this->setLastError('Registro de seção não existe!');

                    return false;
                }

                $objAvaliacaoQuestionarioQuestao->setSecao($objAvaliacaoSecao);
            } else {
                $objAvaliacaoQuestionarioQuestao->setSecao(null);
            }

            if ($arrDados['questao']) {
                /** @var $objAvaliacaoQuestao \Avaliacao\Entity\AvaliacaoQuestao */
                $objAvaliacaoQuestao = $serviceAvaliacaoQuestao->getRepository()->find($arrDados['questao']);

                if (!$objAvaliacaoQuestao) {
                    $this->setLastError('Registro de questão não existe!');

                    return false;
                }

                $objAvaliacaoQuestionarioQuestao->setQuestao($objAvaliacaoQuestao);
            } else {
                $objAvaliacaoQuestionarioQuestao->setQuestao(null);
            }

            $objAvaliacaoQuestionarioQuestao->setQuestionarioquestaoPosicao($arrDados['questionarioquestaoPosicao']);
            $objAvaliacaoQuestionarioQuestao->setQuestionarioquestaoAnulada($arrDados['questionarioquestaoAnulada']);
            $objAvaliacaoQuestionarioQuestao->setQuestionarioquestaoObrigatoria(
                $arrDados['questionarioquestaoObrigatoria']
            );
            $objAvaliacaoQuestionarioQuestao->setQuestionarioquestaoPeso($arrDados['questionarioquestaoPeso']);

            $this->getEm()->persist($objAvaliacaoQuestionarioQuestao);
            $this->getEm()->flush($objAvaliacaoQuestionarioQuestao);

            $this->getEm()->commit();

            $arrDados['questionarioquestaoId'] = $objAvaliacaoQuestionarioQuestao->getQuestionarioquestaoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de questionario questão!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['questionario']) {
            $errors[] = 'Por favor preencha o campo "Questionário"!';
        }

        if (!$arrParam['secao']) {
            $errors[] = 'Por favor preencha o campo "Seção"!';
        }

        if (!$arrParam['questao']) {
            $errors[] = 'Por favor preencha o campo "Questão"!';
        }

        if (!$arrParam['questionarioquestaoPosicao']) {
            $errors[] = 'Por favor preencha o campo "Posição"!';
        }

        if (!in_array($arrParam['questionarioquestaoAnulada'], self::getQuestionarioquestaoAnulada())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "Anulada"!';
        }

        if (!in_array($arrParam['questionarioquestaoObrigatoria'], self::getQuestionarioquestaoObrigatoria())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "Obrigatória"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM avaliacao__questionario_questao";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($questionarioquestaoId)
    {
        $arrDados = array();

        if (!$questionarioquestaoId) {
            $this->setLastError('Questionario questão inválido!');

            return array();
        }

        /** @var $objAvaliacaoQuestionarioQuestao \Avaliacao\Entity\AvaliacaoQuestionarioQuestao */
        $objAvaliacaoQuestionarioQuestao = $this->getRepository()->find($questionarioquestaoId);

        if (!$objAvaliacaoQuestionarioQuestao) {
            $this->setLastError('Questionario questão não existe!');

            return array();
        }

        $serviceAvaliacaoQuestionario = new \Avaliacao\Service\AvaliacaoQuestionario($this->getEm());
        $serviceAvaliacaoSecao        = new \Avaliacao\Service\AvaliacaoSecao($this->getEm());
        $serviceAvaliacaoQuestao      = new \Avaliacao\Service\AvaliacaoQuestao($this->getEm());

        try {
            $arrDados = $objAvaliacaoQuestionarioQuestao->toArray();

            if ($arrDados['questionario']) {
                $arrAvaliacaoQuestionario = $serviceAvaliacaoQuestionario->getArrSelect2(
                    ['id' => $arrDados['questionario']]
                );
                $arrDados['questionario'] = $arrAvaliacaoQuestionario ? $arrAvaliacaoQuestionario[0] : null;
            }

            if ($arrDados['secao']) {
                $arrAvaliacaoSecao = $serviceAvaliacaoSecao->getArrSelect2(['id' => $arrDados['secao']]);
                $arrDados['secao'] = $arrAvaliacaoSecao ? $arrAvaliacaoSecao[0] : null;
            }

            if ($arrDados['questao']) {
                $arrAvaliacaoQuestao = $serviceAvaliacaoQuestao->getArrSelect2(['id' => $arrDados['questao']]);
                $arrDados['questao'] = $arrAvaliacaoQuestao ? $arrAvaliacaoQuestao[0] : null;
            }
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceAvaliacaoQuestionario = new \Avaliacao\Service\AvaliacaoQuestionario($this->getEm());
        $serviceAvaliacaoSecao        = new \Avaliacao\Service\AvaliacaoSecao($this->getEm());
        $serviceAvaliacaoQuestao      = new \Avaliacao\Service\AvaliacaoQuestao($this->getEm());

        if (is_array($arrDados['questionario']) && !$arrDados['questionario']['text']) {
            $arrDados['questionario'] = $arrDados['questionario']['questionarioId'];
        }

        if ($arrDados['questionario'] && is_string($arrDados['questionario'])) {
            $arrDados['questionario'] = $serviceAvaliacaoQuestionario->getArrSelect2(
                array('id' => $arrDados['questionario'])
            );
            $arrDados['questionario'] = $arrDados['questionario'] ? $arrDados['questionario'][0] : null;
        }

        if (is_array($arrDados['secao']) && !$arrDados['secao']['text']) {
            $arrDados['secao'] = $arrDados['secao']['secaoId'];
        }

        if ($arrDados['secao'] && is_string($arrDados['secao'])) {
            $arrDados['secao'] = $serviceAvaliacaoSecao->getArrSelect2(array('id' => $arrDados['secao']));
            $arrDados['secao'] = $arrDados['secao'] ? $arrDados['secao'][0] : null;
        }

        if (is_array($arrDados['questao']) && !$arrDados['questao']['text']) {
            $arrDados['questao'] = $arrDados['questao']['questaoId'];
        }

        if ($arrDados['questao'] && is_string($arrDados['questao'])) {
            $arrDados['questao'] = $serviceAvaliacaoQuestao->getArrSelect2(array('id' => $arrDados['questao']));
            $arrDados['questao'] = $arrDados['questao'] ? $arrDados['questao'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['questionarioquestaoId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['questionarioId' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Avaliacao\Entity\AvaliacaoQuestionarioQuestao */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getQuestionarioquestaoId();
            $arrEntity[$params['value']] = $objEntity->getQuestao()->getQuestaoDescricao();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($questionarioquestaoId)
    {
        if (!$questionarioquestaoId) {
            $this->setLastError('Para remover um registro de questionario questão é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objAvaliacaoQuestionarioQuestao \Avaliacao\Entity\AvaliacaoQuestionarioQuestao */
            $objAvaliacaoQuestionarioQuestao = $this->getRepository()->find($questionarioquestaoId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAvaliacaoQuestionarioQuestao);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de questionario questão.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceAvaliacaoQuestionario = new \Avaliacao\Service\AvaliacaoQuestionario($this->getEm());
        $serviceAvaliacaoSecao        = new \Avaliacao\Service\AvaliacaoSecao($this->getEm());
        $serviceAvaliacaoQuestao      = new \Avaliacao\Service\AvaliacaoQuestao($this->getEm());

        $serviceAvaliacaoQuestionario->setarDependenciasView($view);
        $serviceAvaliacaoSecao->setarDependenciasView($view);
        $serviceAvaliacaoQuestao->setarDependenciasView($view);

        $view->setVariable("arrQuestionarioquestaoAnulada", $this->getArrSelect2QuestionarioquestaoAnulada());

        $view->setVariable("arrQuestionarioquestaoObrigatoria", $this->getArrSelect2QuestionarioquestaoObrigatoria());
    }
}
?>