<?php

namespace Avaliacao\Service;

use VersaSpine\Service\AbstractService;

class AvaliacaoQuestaoTipo extends AbstractService
{
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Avaliacao\Entity\AvaliacaoQuestaoTipo');
    }

    public function pesquisaForJson($params)
    {
        $sql = '
        SELECT *
        FROM avaliacao__questao_tipo
        WHERE
            questaotipo_descricao LIKE :questaotipo_descricao';

        $questaotipoDescricao = false;
        $questaotipoId        = false;

        if ($params['q']) {
            $questaotipoDescricao = $params['q'];
        } elseif ($params['query']) {
            $questaotipoDescricao = $params['query'];
        }

        if ($params['questaotipoId']) {
            $questaotipoId = $params['questaotipoId'];
        }

        $parameters = array('questaotipo_descricao' => "{$questaotipoDescricao}%");

        if ($questaotipoId) {
            $parameters['questaotipo_id'] = $questaotipoId;
            $sql .= ' AND questaotipo_id <> :questaotipo_id';
        }

        $sql .= " ORDER BY questaotipo_descricao";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            $questaotipoId           = ($arrDados['questaotipoIdOld'] ? $arrDados['questaotipoIdOld'] : null);
            $questaotipoId           = (!$questaotipoId && $arrDados['questaotipoId'] ? $arrDados['questaotipoId'] : $questaotipoId);
            $objAvaliacaoQuestaoTipo = null;

            if ($questaotipoId) {
                /** @var $objAvaliacaoQuestaoTipo \Avaliacao\Entity\AvaliacaoQuestaoTipo */
                $objAvaliacaoQuestaoTipo = $this->getRepository()->find($questaotipoId);
            }

            if (!$objAvaliacaoQuestaoTipo) {
                $objAvaliacaoQuestaoTipo = new \Avaliacao\Entity\AvaliacaoQuestaoTipo();
            }

            $objAvaliacaoQuestaoTipo->setquestaotipoId($arrDados['questaotipoId']);
            $objAvaliacaoQuestaoTipo->setQuestaotipoDescricao($arrDados['questaotipoDescricao']);

            $this->getEm()->persist($objAvaliacaoQuestaoTipo);
            $this->getEm()->flush($objAvaliacaoQuestaoTipo);

            $this->getEm()->commit();

            $arrDados['questaotipoId'] = $objAvaliacaoQuestaoTipo->getQuestaotipoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de questão tipo!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['questaotipoId']) {
            $errors[] = 'Por favor preencha o campo "Código"!';
        }

        if (!$arrParam['questaotipoDescricao']) {
            $errors[] = 'Por favor preencha o campo "Descrição"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM avaliacao__questao_tipo";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($questaotipoId)
    {
        $arrDados = array();

        if (!$questaotipoId) {
            $this->setLastError('Questão tipo inválido!');

            return array();
        }

        /** @var $objAvaliacaoQuestaoTipo \Avaliacao\Entity\AvaliacaoQuestaoTipo */
        $objAvaliacaoQuestaoTipo = $this->getRepository()->find($questaotipoId);

        if (!$objAvaliacaoQuestaoTipo) {
            $this->setLastError('Questão tipo não existe!');

            return array();
        }

        try {
            $arrDados = $objAvaliacaoQuestaoTipo->toArray();
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['questaotipoId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['questaotipoDescricao' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Avaliacao\Entity\AvaliacaoQuestaoTipo */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getQuestaotipoId();
            $arrEntity[$params['value']] = $objEntity->getQuestaotipoDescricao();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($questaotipoId)
    {
        if (!$questaotipoId) {
            $this->setLastError('Para remover um registro de questão tipo é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objAvaliacaoQuestaoTipo \Avaliacao\Entity\AvaliacaoQuestaoTipo */
            $objAvaliacaoQuestaoTipo = $this->getRepository()->find($questaotipoId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAvaliacaoQuestaoTipo);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de questão tipo.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
    }
}
?>