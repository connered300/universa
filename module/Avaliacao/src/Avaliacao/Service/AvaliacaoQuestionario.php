<?php

namespace Avaliacao\Service;

use VersaSpine\Service\AbstractService;

class AvaliacaoQuestionario extends AbstractService
{
    const QUESTIONARIO_ESTADO_RASCUNHO             = 'Rascunho';
    const QUESTIONARIO_ESTADO_PUBLICADO            = 'Publicado';
    const QUESTIONARIO_ESTADO_FECHADO              = 'Fechado';
    const QUESTIONARIO_OBRIGATORIEDADE_OBRIGATORIO = 'Obrigatorio';
    const QUESTIONARIO_OBRIGATORIEDADE_DESEJAVEL   = 'Desejavel';
    const QUESTIONARIO_OBRIGATORIEDADE_FACULTATIVO = 'Facultativo';
    const QUESTIONARIO_ANONIMO_SIM                 = 'Sim';
    const QUESTIONARIO_ANONIMO_NAO                 = 'Não';
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2QuestionarioEstado($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getQuestionarioEstado());
    }

    /**
     * @return array
     */
    public static function getQuestionarioEstado()
    {
        return array(
            self::QUESTIONARIO_ESTADO_RASCUNHO,
            self::QUESTIONARIO_ESTADO_PUBLICADO,
            self::QUESTIONARIO_ESTADO_FECHADO
        );
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2QuestionarioObrigatoriedade($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getQuestionarioObrigatoriedade());
    }

    /**
     * @return array
     */
    public static function getQuestionarioObrigatoriedade()
    {
        return array(
            self::QUESTIONARIO_OBRIGATORIEDADE_OBRIGATORIO,
            self::QUESTIONARIO_OBRIGATORIEDADE_DESEJAVEL,
            self::QUESTIONARIO_OBRIGATORIEDADE_FACULTATIVO
        );
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2QuestionarioAnonimo($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getQuestionarioAnonimo());
    }

    /**
     * @return array
     */
    public static function getQuestionarioAnonimo()
    {
        return array(self::QUESTIONARIO_ANONIMO_SIM, self::QUESTIONARIO_ANONIMO_NAO);
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Avaliacao\Entity\AvaliacaoQuestionario');
    }

    /**
     * @param $params
     * @return mixed
     */
    public function pesquisaForJson($params)
    {
        $sql = '
        SELECT *
        FROM avaliacao__questionario
        WHERE
            questionario_descricao LIKE :questionario_descricao';

        $questionarioDescricao = false;
        $questionarioId        = false;

        if ($params['q']) {
            $questionarioDescricao = $params['q'];
        } elseif ($params['query']) {
            $questionarioDescricao = $params['query'];
        }

        if ($params['questionarioId']) {
            $questionarioId = $params['questionarioId'];
        }

        $parameters = array('questionario_descricao' => "{$questionarioDescricao}%");

        if ($questionarioId) {
            $parameters['questionario_id'] = $questionarioId;
            $sql .= ' AND questionario_id <> :questionario_id';
        }

        $sql .= " ORDER BY questionario_descricao";
        $sql .= " LIMIT 0,40";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    /**
     * @param array $arrDados
     * @return bool
     */
    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceAvaliacaoFim = new \Avaliacao\Service\AvaliacaoFim($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['questionarioId']) {
                /** @var $objAvaliacaoQuestionario \Avaliacao\Entity\AvaliacaoQuestionario */
                $objAvaliacaoQuestionario = $this->getRepository()->find($arrDados['questionarioId']);

                if (!$objAvaliacaoQuestionario) {
                    $this->setLastError('Registro de questionario não existe!');

                    return false;
                }
            } else {
                $objAvaliacaoQuestionario = new \Avaliacao\Entity\AvaliacaoQuestionario();
            }

            if ($arrDados['fim']) {
                /** @var $objAvaliacaoFim \Avaliacao\Entity\AvaliacaoFim */
                $objAvaliacaoFim = $serviceAvaliacaoFim->getRepository()->find($arrDados['fim']);

                if (!$objAvaliacaoFim) {
                    $this->setLastError('Registro de fim não existe!');

                    return false;
                }

                $objAvaliacaoQuestionario->setFim($objAvaliacaoFim);
            } else {
                $objAvaliacaoQuestionario->setFim(null);
            }

            $objAvaliacaoQuestionario->setQuestionarioTitulo($arrDados['questionarioTitulo']);
            $objAvaliacaoQuestionario->setQuestionarioDescricao($arrDados['questionarioDescricao']);
            $objAvaliacaoQuestionario->setQuestionarioMensagemInicial($arrDados['questionarioMensagemInicial']);
            $objAvaliacaoQuestionario->setQuestionarioMensagemAgradecimento(
                $arrDados['questionarioMensagemAgradecimento']
            );
            $objAvaliacaoQuestionario->setQuestionarioInicio($arrDados['questionarioInicio']);
            $objAvaliacaoQuestionario->setQuestionarioFim($arrDados['questionarioFim']);
            $objAvaliacaoQuestionario->setQuestionarioAlteracao($arrDados['questionarioAlteracao']);
            $objAvaliacaoQuestionario->setQuestionarioCriacao($arrDados['questionarioCriacao']);
            $objAvaliacaoQuestionario->setQuestionarioEstado($arrDados['questionarioEstado']);
            $objAvaliacaoQuestionario->setQuestionarioSecoes($arrDados['questionarioSecoes']);
            $objAvaliacaoQuestionario->setQuestionarioObrigatoriedade($arrDados['questionarioObrigatoriedade']);
            $objAvaliacaoQuestionario->setQuestionarioAnonimo($arrDados['questionarioAnonimo']);

            $this->getEm()->persist($objAvaliacaoQuestionario);
            $this->getEm()->flush($objAvaliacaoQuestionario);

            $this->getEm()->commit();

            $arrDados['questionarioId'] = $objAvaliacaoQuestionario->getQuestionarioId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de questionario!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return bool
     */
    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['fim']) {
            $errors[] = 'Por favor preencha o campo "Fim"!';
        }

        if (!$arrParam['questionarioTitulo']) {
            $errors[] = 'Por favor preencha o campo "título"!';
        }

        if (!$arrParam['questionarioInicio']) {
            $errors[] = 'Por favor preencha o campo "início"!';
        }

        if (!in_array($arrParam['questionarioEstado'], self::getQuestionarioEstado())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "estado"!';
        }

        if (!in_array($arrParam['questionarioObrigatoriedade'], self::getQuestionarioObrigatoriedade())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "obrigatoriedade"!';
        }

        if (!in_array($arrParam['questionarioAnonimo'], self::getQuestionarioAnonimo())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "anonimo"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    /**
     * @param $data
     * @return array
     */
    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM avaliacao__questionario";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    /**
     * @param $questionarioId
     * @return array
     */
    public function getArray($questionarioId)
    {
        $arrDados = array();

        if (!$questionarioId) {
            $this->setLastError('Questionario inválido!');

            return array();
        }

        /** @var $objAvaliacaoQuestionario \Avaliacao\Entity\AvaliacaoQuestionario */
        $objAvaliacaoQuestionario = $this->getRepository()->find($questionarioId);

        if (!$objAvaliacaoQuestionario) {
            $this->setLastError('Questionario não existe!');

            return array();
        }

        $serviceAvaliacaoFim = new \Avaliacao\Service\AvaliacaoFim($this->getEm());

        try {
            $arrDados = $objAvaliacaoQuestionario->toArray();

            if ($arrDados['fim']) {
                $arrAvaliacaoFim = $serviceAvaliacaoFim->getArrSelect2(['id' => $arrDados['fim']]);
                $arrDados['fim'] = $arrAvaliacaoFim ? $arrAvaliacaoFim[0] : null;
            }
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceAvaliacaoFim = new \Avaliacao\Service\AvaliacaoFim($this->getEm());

        if (is_array($arrDados['fim']) && !$arrDados['fim']['text']) {
            $arrDados['fim'] = $arrDados['fim']['fimId'];
        }

        if ($arrDados['fim'] && is_string($arrDados['fim'])) {
            $arrDados['fim'] = $serviceAvaliacaoFim->getArrSelect2(array('id' => $arrDados['fim']));
            $arrDados['fim'] = $arrDados['fim'] ? $arrDados['fim'][0] : null;
        }

        return $arrDados;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['questionarioId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['questionarioDescricao' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Avaliacao\Entity\AvaliacaoQuestionario */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getQuestionarioId();
            $arrEntity[$params['value']] = $objEntity->getQuestionarioDescricao();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    /**
     * @param $questionarioId
     * @return bool
     */
    public function remover($questionarioId)
    {
        if (!$questionarioId) {
            $this->setLastError('Para remover um registro de questionario é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objAvaliacaoQuestionario \Avaliacao\Entity\AvaliacaoQuestionario */
            $objAvaliacaoQuestionario = $this->getRepository()->find($questionarioId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objAvaliacaoQuestionario);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de questionario.');

            return false;
        }

        return true;
    }

    /**
     * @param \Zend\View\Model\ViewModel $view
     */
    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceAvaliacaoFim = new \Avaliacao\Service\AvaliacaoFim($this->getEm());
        $serviceAvaliacaoFim->setarDependenciasView($view);
        $view->setVariable("arrQuestionarioEstado", $this->getArrSelect2QuestionarioEstado());
        $view->setVariable("arrQuestionarioObrigatoriedade", $this->getArrSelect2QuestionarioObrigatoriedade());
        $view->setVariable("arrQuestionarioAnonimo", $this->getArrSelect2QuestionarioAnonimo());
    }

    /**
     * @return bool
     */
    public function verificaFormularioAbertoUsuario()
    {
        $serviceAcessoGrupo   = new \Acesso\Service\AcessoGrupo($this->getEm());
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceRespondente   = new \Avaliacao\Service\AvaliacaoQuestionarioRespondente($this->getEm());

        /** @var \Acesso\Entity\AcessoPessoas $objUsuarioLogado */
        $objUsuarioLogado = $serviceAcessoPessoas->retornaUsuarioLogado();

        if (!$objUsuarioLogado) {
            return false;
        }

        try {
            $pesId    = $objUsuarioLogado->getPes()->getPes()->getPesId();
            $arrGrupo = $serviceAcessoGrupo->buscaGrupoUsr($objUsuarioLogado->getUsuario()->getId());
            //$arrGrupo = implode(',', $serviceAcessoGrupo->buscaGrupoUsr($objUsuarioLogado->getUsuario()->getId()));

            if (!$arrGrupo) {
                return false;
            }

            $arrParam = [
                'pes'                => $pesId,
                'grupos'             => $arrGrupo,
                'questionarioEstado' => self::QUESTIONARIO_ESTADO_PUBLICADO,
                'respondenteStatus'  => $serviceRespondente::RESPONDENTE_STATUS_CONCLUIDO
            ];

            $query = "
        SELECT
            questionario_id, questionario_titulo, questionario_descricao,
            questionario_inicio, questionario_fim, questionario_estado,
            questionario_obrigatoriedade, questionario_anonimo, fim_id,
            respondente_id, secao_id, respondente_status, respondente_data, pes_id,
            count(questionarioquestao_id) total_questoes, sum(questao_respondida) total_respondidas
        FROM (
                SELECT
                    aq.*,
                    questao.questao_id,
                    aqq.questionarioquestao_id,
                    if(arr.respondenteresposta_id IS NOT NULL OR arrc.respondenteresposta_id IS NOT NULL, 1, 0) AS questao_respondida,
                    respondente.respondente_id,
                    respondente.secao_id,
                    respondente.respondente_status,
                    respondente.respondente_data,
                    respondente.pes_id
                FROM avaliacao__questionario aq
                LEFT JOIN avaliacao__questionario_grupo aqg USING(questionario_id)
                INNER JOIN avaliacao__questionario_questao aqq USING(questionario_id)
                INNER JOIN avaliacao__questao questao USING(questao_id)
                INNER JOIN avaliacao__questao_aplicacao USING(aplicacao_id)
                LEFT JOIN avaliacao__secao secao USING(secao_id)
                INNER JOIN avaliacao__questao_resposta respostas USING(questao_id)
                LEFT JOIN avaliacao__questionario_respondente respondente ON
                    respondente.questionario_id = aq.questionario_id AND
                    respondente.pes_id = :pes
                LEFT JOIN avaliacao__respondente_resposta_concluida arrc ON
                    arrc.questionarioquestao_id = aqq.questionarioquestao_id AND
                    arrc.respondente_id = respondente.respondente_id
                LEFT JOIN avaliacao__respondente_resposta arr ON
                    arr.questionarioquestao_id = aqq.questionarioquestao_id AND
                    arr.respondente_id=respondente.respondente_id
                WHERE
                    (now() BETWEEN aq.questionario_inicio AND aq.questionario_fim) AND
                    aq.questionario_estado = :questionarioEstado AND
                    coalesce(respondente.respondente_status,'') != :respondenteStatus AND
                    ( respondente.pes_id = :pes OR respondente.pes_id IS NULL ) AND aqg.grupo_id IN ( :grupos )
                GROUP BY aq.questionario_id, aqq.questionarioquestao_id
        ) AS questao_respostas
        GROUP BY questionario_id";

            return $this->executeQueryWithParam($query, $arrParam)->fetchAll();
        } catch (\Exception $ex) {
            $this->setLastError($ex->getMessage());
        }

        return false;
    }

    /**
     * @param $param
     * @return array|bool
     */
    public function retornaQuestionario($param)
    {
        $serviceQuestionarioRespondente = new \Avaliacao\Service\AvaliacaoQuestionarioRespondente($this->getEm());
        $questionarioPublicado          = self::QUESTIONARIO_ESTADO_PUBLICADO;
        $statusQuestionario             = $serviceQuestionarioRespondente::RESPONDENTE_STATUS_PARCIAL;

        $sql = "
        SELECT
            avaliacao__questao_aplicacao.aplicacao_id,
            aqg.grupo_id grupo_permitido,
            questao.questao_descricao,
            questao.questao_id,
            respostas.questaoresposta_descricao,
            respostas.questaoresposta_gabarito,
            respostas.questaoresposta_id,
            respostas.questaoresposta_posicao,
            questao.questaotipo_id,
            aq.questionario_anonimo,
            aq.questionario_descricao,
            aq.questionario_id,
            aq.questionario_mensagem_agradecimento,
            aq.questionario_mensagem_inicial,
            aq.questionario_secoes,
            aq.questionario_titulo,
            aqq.questionarioquestao_id,
            respondente.respondente_id,
            arr.respondenteresposta_complemento,
            coalesce(arr.respondenteresposta_id,arrc.respondenteresposta_id) respondenteresposta_id,
            arr.respondenteresposta_peso,
            arr.respondenteresposta_resposta_aberta,
            arrc.respondenteresposta_submissao,
            if(
                arr.respondenteresposta_id IS NOT NULL OR
                arrc.respondenteresposta_id IS NOT NULL,
                1,
                0
            ) AS respondida,
            respostas.questaoresposta_id AS resposta_id,
            secao.secao_id,
            secao.secao_nome,
            secao.secao_ordem
        FROM avaliacao__questionario aq
        LEFT JOIN avaliacao__questionario_grupo aqg USING(questionario_id)
        INNER JOIN avaliacao__questionario_questao aqq USING(questionario_id)
        INNER JOIN avaliacao__questao questao USING(questao_id)
        INNER JOIN avaliacao__questao_aplicacao USING(aplicacao_id)
        LEFT JOIN avaliacao__secao secao USING(secao_id)
        LEFT JOIN avaliacao__questao_resposta respostas USING(questao_id)
        LEFT JOIN avaliacao__questionario_respondente respondente ON
            respondente.questionario_id = aq.questionario_id AND
            respondente.pes_id = :pesId
        LEFT JOIN avaliacao__respondente_resposta_concluida arrc ON
            arrc.questionarioquestao_id = aqq.questionarioquestao_id AND
            arrc.respondente_id = respondente.respondente_id
        LEFT JOIN avaliacao__respondente_resposta arr ON
            arr.questionarioquestao_id = aqq.questionarioquestao_id AND
            arr.respondente_id=respondente.respondente_id
        WHERE
            aq.questionario_id=:questionarioId AND aq.questionario_estado=" . '"' . $questionarioPublicado . '"' . " AND
            (respondente.respondente_status IS NULL OR respondente.respondente_status=" . '"' . $statusQuestionario . '"' . ") AND 
            
            (now() BETWEEN aq.questionario_inicio AND aq.questionario_fim)
        ";

        $paramet = ['questionarioId' => $param['questionarioId']];

        if ($param['pesId']) {
            $sql .= " AND ( respondente.pes_id=:pesId OR respondente.pes_id IS NULL )";

            $paramet['pesId'] = $param['pesId'];
        }

        if ($param['grupoPes']) {
            $sql .= " AND aqg.grupo_id IN ( " . $param['grupoPes'] . " )";
        }

        $sql .= "
        GROUP BY aqq.questionarioquestao_id, respostas.questaoresposta_id, respostas.questaoresposta_id
        ORDER BY
            aq.questionario_id, secao.secao_ordem, secao.secao_nome, questao.questao_descricao,
            respostas.questaoresposta_posicao";

        try {
            $result = $this->executeQueryWithParam($sql, $paramet)->fetchAll();

            return $result;
        } catch (\Exception $ex) {
            $this->setLastError($ex->getMessage());
        }

        return false;
    }

    /**
     * @param $arrParam
     * @return string
     */
    public function salvarQuestionario($arrParam)
    {
        $statusQuestionarioRespondente = '';

        try {
            $servicePessoa                  = new \Pessoa\Service\Pessoa($this->getEm());
            $serviceAvaliacaoQuestao        = new \Avaliacao\Service\AvaliacaoQuestao($this->getEm());
            $serviceRespondente             = new \Avaliacao\Service\AvaliacaoQuestionarioRespondente($this->getEm());
            $serviceQuestionario            = new \Avaliacao\Service\AvaliacaoQuestionario($this->getEm());
            $serviceRespostaConcluida       = new \Avaliacao\Service\AvaliacaoRespondenteRespostaConcluida(
                $this->getEm()
            );
            $serviceRespondenteResposta     = new \Avaliacao\Service\AvaliacaoRespondenteResposta($this->getEm());
            $serviceQuestionarioQuestao     = new \Avaliacao\Service\AvaliacaoQuestionarioQuestao($this->getEm());
            $objQuestionarioRespondente     = null;
            $arrRespondenteRepostaConcluida = [];

            if (!$arrParam || !$arrParam['pesId']) {
                $this->setLastError("Dados inválidos ou incompletos!");

                return false;
            }

            /** @var \Pessoa\Entity\Pessoa $objPessoa */
            $objPessoa = $servicePessoa->getRepository()->find($arrParam['pesId']);

            if (!$objPessoa) {
                $this->setLastError("Pessoa não encontrada!");

                return false;
            }

            $questionarioId = $arrParam['questionarioId'];

            /** @var \Avaliacao\Entity\AvaliacaoQuestionario $objQuestionario */
            $objQuestionario = $serviceQuestionario->getRepository()->find($questionarioId);

            $questoes            = [];
            $arrQuestaoAnterior  = [];
            $questoesRespondidas = 0;

            /*Tras todas as questões para uma string para busca*/
            foreach ($arrParam['questao'] as $indice => $arrQuestao) {
                if ($arrQuestao['resposta'] || $arrQuestao['respondida'] || $arrQuestao['respostaAberta']) {
                    $questoesRespondidas++;
                }

                if ($arrQuestao['perguntaId']) {
                    $questoes[] = $arrQuestao['perguntaId'];
                }

                if (!$arrQuestaoAnterior) {
                    $arrQuestaoAnterior = $arrQuestao;
                    continue;
                }

                if ($arrQuestaoAnterior['perguntaId'] == $arrQuestao['perguntaId']) {
                    if ($arrQuestaoAnterior['resposta'] && !isset($arrQuestao['resposta'])) {
                        $this->setLastError("Por favor responda a questão: " . '"' . $arrQuestao['enunciado'] . '"');

                        return false;
                    }

                    if ($arrQuestao['resposta'] && !isset($arrQuestaoAnterior['resposta'])) {
                        $this->setLastError(
                            "Por favor responda a questão: " . '"' . $arrQuestaoAnterior['enunciado'] . '"'
                        );

                        return false;
                    }
                } else {
                    $arrQuestaoAnterior = $arrQuestao;
                }
            }

            /*Busca informaçõers extras das questões */
            $arrDadosQuestoes = $serviceAvaliacaoQuestao->pesquisaForJson(
                [
                    'contador'       => true,
                    'questionarioId' => $questionarioId,
                    'questoesId'     => implode(',', $questoes),
                    'pes'            => $objPessoa->getPesId()
                ]
            );

            foreach ($arrParam['questao'] as $indice => $arrQuestao) {
                foreach ($arrDadosQuestoes as $questao) {
                    if ($arrQuestao['perguntaId'] == $questao['questao_id']) {
                        $arrParam['questao'][$indice] = array_merge($questao, $arrParam['questao'][$indice]);
                    }
                }
            }

            $statusQuestionarioRespondente = (
            ($questoesRespondidas) < (count($arrParam['questao'])) ? 'Parcial' : 'Concluído'
            );

            $date = new \DateTime('now');

            $this->getEm()->beginTransaction();

            /** @var \Avaliacao\Entity\AvaliacaoQuestionarioRespondente $objQuestionarioRespondente */
            $objQuestionarioRespondente = $serviceRespondente->getRepository()->findOneBy(
                ['questionario' => $questionarioId, 'pes' => $objPessoa]
            );

            $arrQuestionario = [
                'secao'             => null,
                'respondenteStatus' => $statusQuestionarioRespondente,
                'pes'               => $objPessoa,
                'respondenteData'   => $date,
                'questionarioId'    => $questionarioId
            ];

            if ($objQuestionarioRespondente) {
                if ($objQuestionarioRespondente->questionarioRespondenteConcluido() && $arrParam['forcar'] != 's') {
                    $this->setLastError("Este questionário já foi concluído por esta pessoa!");

                    return false;
                }
                /*Caso exista sobrescrevo pelo dados da entidade assim garantindo a integridade dos dados e evitando um else*/
                $arrQuestionario = [
                    'respondenteId'     => $objQuestionarioRespondente->getRespondenteId(),
                    'secao'             => null,
                    'respondenteStatus' => $statusQuestionarioRespondente,
                    'pes'               => $objPessoa,
                    'respondenteData'   => $objQuestionarioRespondente->getRespondenteData(),
                    'questionarioId'    => $questionarioId
                ];

                if ($arrParam['deletarAntes'] == 's') {
                    $query = '
                    DELETE FROM avaliacao__respondente_resposta_concluida
                    WHERE respondente_id=' . $objQuestionarioRespondente->getRespondenteId();
                    $x     = $this->executeQuery($query);

                    $query = '
                    DELETE FROM avaliacao__respondente_resposta
                    WHERE respondente_id=' . $objQuestionarioRespondente->getRespondenteId();
                    $x     = $this->executeQuery($query);
                }
            }

            /** @var |Avaliacao\Entity\AvaliacaoQuestionarioRespondente $objQuestionarioRespondente */
            $objQuestionarioRespondente = $serviceRespondente->save($arrQuestionario);

            if (!$objQuestionarioRespondente) {
                $this->setLastError($serviceRespondente->getLastError());

                return false;
            }

            foreach ($arrParam['questao'] as $indice => $arrQuestao) {
                if ((!$arrQuestao['resposta'] && !$arrQuestao['respostaAberta']) || !$arrQuestao) {
                    continue;
                }

                $arrRespondenteResposta = [];

                $resondenteId = $objQuestionarioRespondente->getRespondenteId();

                /** @var \Avaliacao\Entity\AvaliacaoQuestionarioQuestao $objQuestionarioQuestao */
                $objQuestionarioQuestao = $serviceQuestionarioQuestao->getRepository()->findOneBy(
                    [
                        'secao'        => $arrQuestao['secao_id'],
                        'questao'      => $arrQuestao['questao_id'],
                        'questionario' => $questionarioId
                    ]
                );

                if (!$objQuestionarioQuestao) {
                    $this->setLastError("Questão não localizada!");

                    return false;
                    break;
                }

                $chave = $objQuestionarioQuestao->getQuestionarioquestaoId() . '-' . $resondenteId;

                if (!$arrRespondenteRepostaConcluida[$chave]) {
                    $arrRespondenteRepostaConcluida[$chave] = [
                        'questionarioquestao'          => $objQuestionarioQuestao->getQuestionarioquestaoId(),
                        'respondente'                  => $resondenteId,
                        'respondenterespostaSubmissao' => $date,
                        'gravado'                     => 0
                    ];
                }

                $arrRespostas = is_array($arrQuestao['resposta']) ? $arrQuestao['resposta'] : [$arrQuestao['resposta']];

                foreach ($arrRespostas as $indice2 => $resposta) {
                    $arrRespondenteResposta[$indice2] = [
                        'questionarioquestao'            => $objQuestionarioQuestao->getQuestionarioquestaoId(),
                        'questaoresposta'                => $resposta,
                        'respondenterespostaPeso'        => $arrQuestao['questionarioquestao_peso'],
                        'respondenterespostaComplemento' => $arrQuestao['complemento'],
                        'respostaAberta'                 => $arrQuestao['respostaAberta']

                    ];

                    if (!$objQuestionario->verificarQuestionarioAnonimo()) {
                        $arrRespondenteResposta[$indice2]['respondente'] = $resondenteId;
                    }
                }

                try {
                    if ($arrRespondenteRepostaConcluida[$chave]['gravado'] == 0) {
                        if (!$serviceRespostaConcluida->save($arrRespondenteRepostaConcluida[$chave])) {
                            $okRespondenteConcluida = false;
                        }

                        $arrRespondenteRepostaConcluida[$chave]['gravado'] = 1;
                    }

                    foreach ($arrRespondenteResposta as $valor) {
                        if (!$serviceRespondenteResposta->save($valor)) {
                            $okRespondenteResposta = false;
                        }
                    }
                } catch (\Exception $e) {
                    $this->setLastError(
                        $serviceRespostaConcluida->getLastError() . "\n" . $serviceRespondenteResposta->getLastError()
                    );
                }
            }

            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError(
                "Não foi possível gravar questionário!\n" . $ex->getMessage()
            );

            $statusQuestionarioRespondente = false;
        }

        return $statusQuestionarioRespondente;
    }

    /**
     * @param $questionarioId
     * @return array|bool
     */
    public function formataDadosQuestionario($questionarioId)
    {
        if (!$questionarioId) {
            $this->setLastError('Questionário inválido!');

            return false;
        }

        $serviceAcessoGrupo   = new \Acesso\Service\AcessoGrupo($this->getEm());
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());

        $serviceProfessorDocente       = new \Professor\Service\AcadgeralDocente($this->getEm());
        $serviceTurma                  = new \Matricula\Service\AcadperiodoTurma($this->getEm());
        $serviceAlunoPeriodoDisciplina = new \Matricula\Service\AcadperiodoAlunoDisciplina($this->getEm());

        /** @var \Acesso\Entity\AcessoPessoas $objUsuarioLogado */
        $objUsuarioLogado = $serviceAcessoPessoas->retornaUsuarioLogado();

        if (!$objUsuarioLogado) {
            $this->setLastError('Usuário não logado!');

            return false;
        }

        $pesId    = $objUsuarioLogado->getPes()->getPes()->getPesId();
        $arrGrupo = implode(',', $serviceAcessoGrupo->buscaGrupoUsr($objUsuarioLogado->getUsuario()->getId()));

        $arrQuestionarioFormatado = [];

        $arrComplemento  = [];
        $arrQuestionario = $this->retornaQuestionario(
            ['grupoPes' => $arrGrupo, 'pesId' => $pesId, 'questionarioId' => $questionarioId]
        );

        if (isset($arrQuestionario['arrComplemento'])) {
            $arrComplemento = $arrQuestionario['arrComplemento'];

            unset($arrQuestionario['arrComplemento']);
        }

        $alunoDocencia     = false;
        $alunoProfessor    = false;
        $professorDocencia = false;
        $qtdQuestoes       = 0;
        $qtdRespondidas    = 0;

        foreach ($arrQuestionario as $index => $value) {
            $questionarioId = $value['questionario_id'];
            $secaoId        = $value['secao_id'];
            $perguntaId     = $value['questao_id'];
            $respostaId     = $value['resposta_id'];

            if (!$arrQuestionarioFormatado[$questionarioId]) {
                $arrQuestionarioFormatado[$questionarioId] = [
                    'questionario'                      => $questionarioId,
                    'questionarioTitulo'                => $value['questionario_titulo'],
                    'questionarioDescricao'             => $value['questionario_descricao'],
                    'questionarioMensagemInicial'       => $value['questionario_mensagem_inicial'],
                    'questionarioMensagemAgradecimento' => $value['questionario_mensagem_agradecimento'],
                    'secoes'                            => []
                ];
            };

            if (!isset($arrQuestionarioFormatado[$questionarioId]['secoes'][$secaoId]) && !$arrQuestionarioFormatado[$questionarioId]['secoes'][$secaoId]) {
                $arrQuestionarioFormatado[$questionarioId]['secoes'][$secaoId] = [
                    'secaoId'   => $secaoId,
                    'secaoNome' => $value['secao_nome'],
                    'perguntas' => []
                ];
            };

            if (!isset($arrQuestionarioFormatado[$questionarioId]['secoes'][$secaoId]['perguntas'][$perguntaId]) &&
                !$arrQuestionarioFormatado[$questionarioId]['secoes'][$secaoId]['perguntas'][$perguntaId]
            ) {
                $arrQuestionarioFormatado[$questionarioId]['secoes'][$secaoId]['perguntas'][$perguntaId] = [
                    'questaoId'         => $value['questao_id'],
                    'questaoEnunciado'  => $value['questao_descricao'],
                    'aplicacao'         => $value['aplicacao_id'],
                    'questaoTipo'       => $value['questaotipo_id'],
                    'questaoRespondida' => ($value['respondida'] ? true : false),
                    'respostas'         => []
                ];

                if ($value['aplicacao_id'] == 'alunoDocencia') {
                    $alunoDocencia = true;
                } elseif ($value['aplicacao_id'] == 'alunoProfessor') {
                    $alunoProfessor = true;
                } elseif ($value['aplicacao_id'] == 'docenteTurma') {
                    $professorDocencia = true;
                }

                $qtdQuestoes += 1;
                $qtdRespondidas += ($value['respondida'] ? 1 : 0);
            };

            if (!isset($arrQuestionarioFormatado[$questionarioId]['secoes'][$secaoId]['perguntas'][$perguntaId]['respostas'][$respostaId]) &&
                !$arrQuestionarioFormatado[$questionarioId]['secoes'][$secaoId]['perguntas'][$perguntaId]['respostas'][$respostaId]
            ) {
                $arrQuestionarioFormatado[$questionarioId]['secoes'][$secaoId]['perguntas'][$perguntaId]['respostas'][$respostaId] = [
                    'repostaId'         => $value['resposta_id'],
                    'respostaDescricao' => $value['questaoresposta_descricao']
                ];
            }
        }

        if ($qtdQuestoes == 0 || $qtdQuestoes == $qtdRespondidas) {
            $this->setLastError("O questionário não possui nenhuma pergunta aberta!");

            return false;
        }

        if ($professorDocencia) {
            /** @var \Professor\Entity\AcadgeralDocente $professor */
            $professor = $serviceProfessorDocente->buscaDocentePorUsuario();

            if ($professor) {
                $arrDocencias = $serviceTurma->pesquisaForJson(['docente' => $professor->getDocenteId()]);

                /* Deve ter pelo menos 1 disciplina ativa*/
                if (count($arrDocencias) == 0) {
                    $this->setLastError("O professor não possui nenhuma docência ativa!");

                    return false;
                }

                $arrComplemento['turmas'] = [];

                foreach ($arrDocencias as $value) {
                    $arrComplemento['turmas'][$value['id']] = $value['descricao'];
                }
            }
        }

        if ($alunoDocencia || $alunoProfessor) {
            $servicePeriodo   = new \Matricula\Service\AcadperiodoLetivo($this->getEm());
            $serviceTurmaTipo = new \Matricula\Service\AcadgeralTurmaTipo($this->getEm());
            $serviceSituacao  = new \Matricula\Service\AcadgeralSituacao($this->getEm());

            $arrParam = [
                'pes'      => $pesId,
                'per'      => $servicePeriodo->buscaPeriodoCorrente(true, true),
                'tturma'   => $serviceTurmaTipo::CONVENCIONAL,
                'situacao' => $serviceSituacao::situacoesAtividade(),
            ];

            $arrDocencias = $serviceAlunoPeriodoDisciplina->pesquisaDisciplinasAluno($arrParam);

            /* Deve ter pelo menos 1 disciplina ativa*/
            if (count($arrDocencias) == 0) {
                $this->setLastError("O aluno não possui nenhuma disciplina!");

                return false;
            }

            $arrComplemento['docencias']   = [];
            $arrComplemento['professores'] = [];

            foreach ($arrDocencias as $value) {
                $arrComplemento['docencias'][$value['docdisc_id']]       = (
                    $value['disc_nome'] . ' / ' . $value['pes_nome_docente']
                );
                $arrComplemento['professores'][$value['pes_id_docente']] = $value['pes_nome_docente'];
            }
        }

        return [
            'pesId'           => $pesId,
            'questionarioId'  => $questionarioId,
            'arrComplemento'  => $arrComplemento,
            'arrQuestionario' => $arrQuestionarioFormatado,
        ];
    }
}
?>