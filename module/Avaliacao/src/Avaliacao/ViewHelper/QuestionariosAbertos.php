<?php

namespace Avaliacao\ViewHelper;

use Zend\Navigation;
use Zend\View\Helper\Navigation\AbstractHelper;
use Doctrine\ORM\EntityManager;

class QuestionariosAbertos extends AbstractHelper
{

    /**
     * Gerenciador de entidade do Doctrine
     * @var EntityManager
     */
    private $em               = null;
    private $arrQuestionarios = [];

    /**
     * @return EntityManager
     */
    public function getEm()
    {
        return $this->em;
    }

    /**
     * @param EntityManager $em
     * @return QuestionariosAbertos
     */
    public function setEm($em)
    {
        $this->em = $em;

        return $this;
    }

    /**
     * @return array
     */
    public function getArrQuestionarios()
    {
        if ($this->arrQuestionarios) {
            return $this->arrQuestionarios;
        }

        $serviceQuestionario = new \Avaliacao\Service\AvaliacaoQuestionario($this->getEm());

        $arrQuestionarios = $serviceQuestionario->verificaFormularioAbertoUsuario();
        $arrQuestionarios = $arrQuestionarios ? $arrQuestionarios : [];

        $this->setArrQuestionarios($arrQuestionarios);

        return $arrQuestionarios;
    }

    /**
     * @param array $arrQuestionarios
     * @return QuestionariosAbertos
     */
    public function setArrQuestionarios($arrQuestionarios)
    {
        $this->arrQuestionarios = $arrQuestionarios;

        return $this;
    }

    public function __construct(EntityManager $em)
    {
        $this->setEm($em);
    }

    public function existemPendencias()
    {
        return $this->getArrQuestionarios() ? true : false;
    }

    /**
     * Renders helper
     *
     * @param  string|Navigation\AbstractContainer $container [optional] container to render.
     *                                         Default is null, which indicates
     *                                         that the helper should render
     *                                         the container returned by {@link
     *                                         getContainer()}.
     * @return string helper output
     * @throws \Zend\View\Exception\ExceptionInterface
     */
    public function render($container = null)
    {
        $listaQuestionario = '';

        foreach ($this->getArrQuestionarios() as $arrQuestionario) {
            $obrigatorio = false;

            if ($arrQuestionario['questionario_obrigatoriedade'] == \Avaliacao\Service\AvaliacaoQuestionario::QUESTIONARIO_OBRIGATORIEDADE_OBRIGATORIO) {
                $obrigatorio = true;
            }

            $listaQuestionario .= $this->formatarQuestionario($arrQuestionario, $obrigatorio);

            if ($obrigatorio) {
                break;
            }
        }

        return $listaQuestionario;
    }

    public function getHtmlQuestaoObrigatoria($url)
    {
        return '
        <p><b>O questionário é obrigatório!</b><br>Em 5 segundos você será redirecionado para o questionário!</p>
        <script type="text/javascript">setTimeout(function() {location.href="' . $url . '";},5000);</script>';
    }

    public function formatarQuestionario($arrQuestionario, $obrigatorio = false)
    {
        $urlQuestionario = $this->getView()->url(
            'avaliacao/default',
            array(
                'controller' => 'avaliacao-questionario',
                'action'     => 'responder',
                'id'         => $arrQuestionario['questionario_id']
            )
        );

        $htmlObrigatoria = '';

        if ($obrigatorio) {
            $htmlObrigatoria = $this->getHtmlQuestaoObrigatoria($urlQuestionario);
        }

        return '
        <div class="alert alert-info">
            <strong>' . $arrQuestionario['questionario_titulo'] . '</strong>
            <p>' . $arrQuestionario['questionario_descricao'] . '</p>
            ' . $htmlObrigatoria . '
            <a href="' . $urlQuestionario . '" target="_blank" class="alert-link">Clique aqui para acessar e participe</a>
        </div>';
    }
}