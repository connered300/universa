<?php

namespace Avaliacao;

return array(
    'router'                    => array(
        'routes' => array(
            'avaliacao' => array(
                'type'          => 'Literal',
                'options'       => array(
                    'route'    => '/avaliacao',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Avaliacao\Controller',
                        'controller'    => 'AvaliacaoFim',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes'  => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'       => '/[:controller[/:action[/:id]]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id'         => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults'    => array(
                                'action' => 'index'
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'controllers'               => array(
        'invokables' => array(
            'Avaliacao\Controller\AvaliacaoFim'                          => 'Avaliacao\Controller\AvaliacaoFimController',
            'Avaliacao\Controller\AvaliacaoQuestao'                      => 'Avaliacao\Controller\AvaliacaoQuestaoController',
            'Avaliacao\Controller\AvaliacaoQuestaoAplicacao'             => 'Avaliacao\Controller\AvaliacaoQuestaoAplicacaoController',
            'Avaliacao\Controller\AvaliacaoQuestaoResposta'              => 'Avaliacao\Controller\AvaliacaoQuestaoRespostaController',
            'Avaliacao\Controller\AvaliacaoQuestaoTipo'                  => 'Avaliacao\Controller\AvaliacaoQuestaoTipoController',
            'Avaliacao\Controller\AvaliacaoQuestionario'                 => 'Avaliacao\Controller\AvaliacaoQuestionarioController',
            'Avaliacao\Controller\AvaliacaoQuestionarioGrupo'            => 'Avaliacao\Controller\AvaliacaoQuestionarioGrupoController',
            'Avaliacao\Controller\AvaliacaoQuestionarioQuestao'          => 'Avaliacao\Controller\AvaliacaoQuestionarioQuestaoController',
            'Avaliacao\Controller\AvaliacaoQuestionarioRespondente'      => 'Avaliacao\Controller\AvaliacaoQuestionarioRespondenteController',
            'Avaliacao\Controller\AvaliacaoRespondenteResposta'          => 'Avaliacao\Controller\AvaliacaoRespondenteRespostaController',
            'Avaliacao\Controller\AvaliacaoRespondenteRespostaConcluida' => 'Avaliacao\Controller\AvaliacaoRespondenteRespostaConcluidaController',
            'Avaliacao\Controller\AvaliacaoSecao'                        => 'Avaliacao\Controller\AvaliacaoSecaoController'
        ),
    ),
    'service_manager'           => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases'            => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'doctrine'                  => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default'             => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            ),
        ),
    ),
    'view_manager'              => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'console'                   => array(
        'router' => array(
            'routes' => array(),
        ),
    ),
    'funcionalidades_liberadas' => array(
        'controllers' => array(),
        'actions'     => array(
            'responder',
            'salvar-questionario',
            'importar-from-log'
        ),
    ),
    'namesDictionary'           => array(
        'Avaliacao\AvaliacaoFim'                                => 'fim',
        'Avaliacao\AvaliacaoQuestao'                            => 'questão',
        'Avaliacao\AvaliacaoQuestaoAplicacao'                   => 'questão aplicação',
        'Avaliacao\AvaliacaoQuestaoResposta'                    => 'questão resposta',
        'Avaliacao\AvaliacaoQuestaoTipo'                        => 'questão tipo',
        'Avaliacao\AvaliacaoQuestionario'                       => 'questionario',
        'Avaliacao\AvaliacaoQuestionario::responder' => '',
        'Avaliacao\AvaliacaoQuestionarioGrupo'                  => 'questionario grupo',
        'Avaliacao\AvaliacaoQuestionarioQuestao'                => 'questionario questão',
        'Avaliacao\AvaliacaoQuestionarioRespondente'            => 'questionario respondente',
        'Avaliacao\AvaliacaoRespondenteResposta'                => 'respondente resposta',
        'Avaliacao\AvaliacaoRespondenteRespostaConcluida'       => 'respondente resposta concluida',
        'Avaliacao\AvaliacaoSecao'                              => 'seção'
    )
);
?>