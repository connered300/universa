<?php

namespace Protocolo\Controller;

use Acesso\Service\AcessoPessoas;
use Protocolo\Service\Protocolo;
use Zend\View\Model\JsonModel;
use VersaSpine\Controller\AbstractCoreController;

class ProtocoloVinculadoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request  = $this->getRequest();
        $arrParam = array_merge(
            $request->getQuery()->toArray(),
            $request->getPost()->toArray()
        );

        $serviceProtocolo = new Protocolo($this->getEntityManager());

        if ($arrParam['solicitante']) {
            $result = $serviceProtocolo->pesquisaSolicitantesJson($arrParam);
        } else {
            $result = $serviceProtocolo->pesquisaForJson($arrParam);
        }

        return new JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceAcessoPessoas = new AcessoPessoas($this->getEntityManager());
        $serviceProtocolo     = new Protocolo($this->getEntityManager());
        $serviceProtocolo->setarDependenciasView($this->getView());

        $arrUsuarioLogado = $serviceAcessoPessoas->getArrSelect2UsuarioLogado();

        $this->getView()->setVariable("arrDados", []);
        $this->getView()->setVariable("filtrarSolicitante", true);
        $this->getView()->setVariable("arrUsuarioLogado", $arrUsuarioLogado);
        $this->getView()->setVariable("editar", false);

        return $this->getView();
    }

    public function addAction($protocoloId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados         = array();
        $serviceProtocolo = new \Protocolo\Service\Protocolo($this->getEntityManager());

        if ($protocoloId) {
            $arrDados = $serviceProtocolo->getArray($protocoloId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceProtocolo->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de protocolo salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceProtocolo->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceProtocolo->getLastError());
                }
            }
        }

        $serviceProtocolo->formataDadosPost($arrDados);
        $serviceProtocolo->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $protocoloId = $this->params()->fromRoute("id", 0);
        $protocoloId = $protocoloId ? $protocoloId : $arrDados['protocoloId'];
        $service     = new \Protocolo\Service\Protocolo($this->getEntityManager());

        if ($arrDados['local'] == 'index') {
            return new \Zend\View\Model\JsonModel($service->retornaDadosCompletosDoProtocolo($protocoloId));
        }

        if ($arrDados['local'] == 'responsavel') {
            $result                 = $service->trocarProtocoloResponsavel($arrDados);
            $result['protocolo_id'] = $protocoloId;

            return new \Zend\View\Model\JsonModel($result);
        }

        $arrDados = $service->retornaDadosCompletosDoProtocolo($protocoloId);
        $service->setarDependenciasView($this->getView());
        $this->getView()->setVariable("arrDados", $arrDados);

        return $this->getView();
    }

    public function addMensagemAction($mensagemId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $serviceProtocoloMensagem = new \Protocolo\Service\ProtocoloMensagem($this->getEntityManager());

        if ($mensagemId) {
            $arrDados = $serviceProtocoloMensagem->getArray($mensagemId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceProtocoloMensagem->save($arrDados, true);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de mensagem salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceProtocoloMensagem->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceProtocoloMensagem->getLastError());
                }
            }
        }

        $serviceProtocoloMensagem->formataDadosPost($arrDados);
        $serviceProtocoloMensagem->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function searchProtocoloArquivoAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceProtocoloMensagem = new \Protocolo\Service\ProtocoloMensagem($this->getEntityManager());
            $dataPost                 = $request->getPost()->toArray();
            $result                   = $serviceProtocoloMensagem->getArrMensagensAnexoPorIdProtocolo(
                $dataPost['protocoloId']
            );
            $this->getJson()->setVariable("data", $result);
        }

        return $this->getJson();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceProtocolo = new \Protocolo\Service\Protocolo($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();

            $result = $serviceProtocolo->getDataForDatatablesByUserLogged($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
            $this->getJson()->setVariable("usuarioLogadoId", $result["usuarioLogadoId"]);
        }

        return $this->getJson();
    }

    public function searchMensagemAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceProtocoloMensagem = new \Protocolo\Service\ProtocoloMensagem($this->getEntityManager());
            $dataPost                 = $request->getPost()->toArray();
            $result                   = $serviceProtocoloMensagem->getArrMensagensPorIdProtocolo(
                $dataPost['protocoloId']
            );
            $this->getJson()->setVariable("messages", $result);
        }

        return $this->getJson();
    }

    public function atualizaSituacaoAction()
    {
        $service = new \Protocolo\Service\Protocolo($this->getEntityManager());
        $request = $this->getRequest();
        $result  = null;

        if ($request->isPost()) {
            $dados  = $request->getPost()->toArray();
            $result = $service->alterarSituacaoProtocolo($dados);
            if ($result) {
                $result = [
                    'erro'     => false,
                    'mensagem' => 'Situação atualizada com sucesso!'
                ];
            } elseif (empty($result)) {
                $result = [
                    'erro'     => null,
                    'mensagem' => 'Nada foi alterado!'
                ];
            } else {
                $result = [
                    'erro'     => true,
                    'mensagem' => 'Não foi possível atualizar a situação!'
                ];
            }
        }

        return new \Zend\View\Model\JsonModel($result ? $result : null);
    }
}
?>