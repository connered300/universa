<?php

namespace Protocolo\Controller;

use VersaSpine\Controller\AbstractCoreController;

class ProtocoloController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request  = $this->getRequest();
        $arrParam = array_merge(
            $request->getQuery()->toArray(),
            $request->getPost()->toArray()
        );

        $serviceProtocolo = new \Protocolo\Service\Protocolo($this->getEntityManager());

        if ($arrParam['solicitante']) {
            $result = $serviceProtocolo->pesquisaSolicitantesJson($arrParam);
        } else {
            $result = $serviceProtocolo->pesquisaForJson($arrParam);
        }

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEntityManager());
        $serviceProtocolo     = new \Protocolo\Service\Protocolo($this->getEntityManager());

        $permissaoImprimir = $serviceAcessoPessoas->verificaSeUsuarioLogadoPossuiAcessoAFuncionalidade(
            'Protocolo\Controller\Protocolo',
            'imprimir'
        );

        $arrUsuarioLogado = $serviceAcessoPessoas->getArrSelect2UsuarioLogado();

        $serviceProtocolo->setarDependenciasView($this->getView());
        $this->getView()->setVariable("permissaoImprimir", $permissaoImprimir);
        $this->getView()->setVariable("arrDados", []);
        $this->getView()->setVariable("arrUsuarioLogado", $arrUsuarioLogado);

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceProtocolo = new \Protocolo\Service\Protocolo($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();

            $fazerBusca = 1;

            if ($dataPost['filtroObrigatorio'] && count($dataPost['filter']) == 0 && !$dataPost['search']['value']) {
                $fazerBusca = 0;
            }

            if ($fazerBusca) {
                $result = $serviceProtocolo->getDataForDatatables($dataPost);
            }

            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable(
                "recordsFiltered",
                ($result["recordsFiltered"] ? $result["recordsFiltered"] : 1)
            );
            $this->getJson()->setVariable("data", ($result["data"] ? $result["data"] : []));
            $this->getJson()->setVariable("usuarioLogadoId", $result["usuarioLogadoId"]);
        }

        return $this->getJson();
    }

    public function addAction($protocoloId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados         = array();
        $serviceProtocolo = new \Protocolo\Service\Protocolo($this->getEntityManager());

        if ($protocoloId) {
            $arrDados = $serviceProtocolo->getArray($protocoloId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceProtocolo->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de protocolo salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceProtocolo->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceProtocolo->getLastError());
                }
            }
        }

        $serviceProtocolo->formataDadosPost($arrDados);
        $serviceProtocolo->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function transferenciaAction()
    {
        $request = $this->getRequest();

        $erro                    = false;
        $erroDescricao           = '';
        $arrProtocoloTransferido = array();

        if ($request->isPost()) {
            $serviceProtocolo = new \Protocolo\Service\Protocolo($this->getEntityManager());

            $dataPost                = $request->getPost()->toArray();
            $arrProtocoloTransferido = $serviceProtocolo->transferirProtocolo($dataPost);

            if (empty($arrProtocoloTransferido)) {
                $erro          = true;
                $erroDescricao = $serviceProtocolo->getLastError();
            }
        } else {
            $erro          = true;
            $erroDescricao = 'Requisição inválida!';
        }

        $this->getJson()->setVariable('erro', $erro);
        $this->getJson()->setVariable('protocolo', $arrProtocoloTransferido);
        $this->getJson()->setVariable('erroDescricao', $erroDescricao);

        return $this->getJson();
    }

    public function editAction()
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $protocoloId = $this->params()->fromRoute("id", 0);
        $protocoloId = $protocoloId ? $protocoloId : $arrDados['protocoloId'];
        $service     = new \Protocolo\Service\Protocolo($this->getEntityManager());

        if ($arrDados['local'] == 'index') {
            return new \Zend\View\Model\JsonModel($service->retornaDadosCompletosDoProtocolo($protocoloId));
        }

        if ($arrDados['local'] == 'responsavel') {
            $result                 = $service->trocarProtocoloResponsavel($arrDados);
            $result['protocolo_id'] = $protocoloId;

            return new \Zend\View\Model\JsonModel($result);
        }

        $arrDados = $service->retornaDadosCompletosDoProtocolo($protocoloId);
        $service->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);

        return $this->getView();
    }

    public function atualizaSituacaoAction()
    {
        $service = new \Protocolo\Service\Protocolo($this->getEntityManager());
        $request = $this->getRequest();
        $result  = null;

        if ($request->isPost()) {
            $dados  = $request->getPost()->toArray();
            $result = $service->alterarSituacaoProtocolo($dados);
            if ($result) {
                $result = [
                    'erro'     => false,
                    'mensagem' => 'Situação atualizada com sucesso!'
                ];
            } elseif (empty($result)) {
                $result = [
                    'erro'     => null,
                    'mensagem' => 'Nada foi alterado!'
                ];
            } else {
                $result = [
                    'erro'     => true,
                    'mensagem' => 'Não foi possível atualizar a situação!'
                ];
            }
        }

        return new \Zend\View\Model\JsonModel($result ? $result : null);
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceProtocolo = new \Protocolo\Service\Protocolo($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceProtocolo->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceProtocolo->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }

    public function protocoloMensagemAddAction($mensagemId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $serviceProtocoloMensagem = new \Protocolo\Service\ProtocoloMensagem($this->getEntityManager());

        if ($mensagemId) {
            $arrDados = $serviceProtocoloMensagem->getArray($mensagemId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );

            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceProtocoloMensagem->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de mensagem salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceProtocoloMensagem->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceProtocoloMensagem->getLastError());
                }
            }
        }

        $serviceProtocoloMensagem->formataDadosPost($arrDados);
        $serviceProtocoloMensagem->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function imprimirAction()
    {
        $serviceProtoco         = new \Protocolo\Service\Protocolo($this->getEntityManager());
        $serviceProtocoMensagem = new \Protocolo\Service\ProtocoloMensagem($this->getEntityManager());
        $request                = $this->getRequest();
        $arrProtocolo           = [];

        $arrDados    = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );
        $protocoloId = isset($arrDados['protocoloId']) ? $arrDados['protocoloId'] :
            isset($_GET['protocoloId']) ? $_GET['protocoloId'] : null;

        if (!$protocoloId) {
            $this->flashMessenger()->addErrorMessage("Nenhum protocolo informado!");

            return false;
        }

        $arrProtocolo = $serviceProtoco->retornaDadosCompletosDoProtocolo($protocoloId);

        $this->getView()->setTemplate('/protocolo/protocolo/protocolo-impressao');
        $this->getView()->setVariable('arrDados', $arrProtocolo);

        $this->getView()->setTerminal(true);

        return $this->getView();
    }
}
?>