<?php

namespace Protocolo\Controller;

use VersaSpine\Controller\AbstractCoreController;

class ProtocoloSolicitacaoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request                     = $this->getRequest();
        $paramsGet                   = $request->getQuery()->toArray();
        $paramsPost                  = $request->getPost()->toArray();
        $serviceProtocoloSolicitacao = new \Protocolo\Service\ProtocoloSolicitacao($this->getEntityManager());

        $result = $serviceProtocoloSolicitacao->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceProtocoloSolicitacao = new \Protocolo\Service\ProtocoloSolicitacao($this->getEntityManager());
        $serviceProtocoloSolicitacao->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceProtocoloSolicitacao = new \Protocolo\Service\ProtocoloSolicitacao($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceProtocoloSolicitacao->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function addAction($solicitacaoId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados                    = array();
        $serviceProtocoloSolicitacao = new \Protocolo\Service\ProtocoloSolicitacao($this->getEntityManager());

        if ($solicitacaoId) {
            $arrDados = $serviceProtocoloSolicitacao->getArray($solicitacaoId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceProtocoloSolicitacao->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de solicitação salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceProtocoloSolicitacao->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceProtocoloSolicitacao->getLastError());
                }
            }
        }

        $serviceProtocoloSolicitacao->formataDadosPost($arrDados);
        $serviceProtocoloSolicitacao->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $solicitacaoId = $this->params()->fromRoute("id", 0);

        if (!$solicitacaoId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($solicitacaoId);
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceProtocoloSolicitacao = new \Protocolo\Service\ProtocoloSolicitacao($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceProtocoloSolicitacao->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceProtocoloSolicitacao->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}
?>