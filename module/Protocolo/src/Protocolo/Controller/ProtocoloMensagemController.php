<?php

namespace Protocolo\Controller;

use GerenciadorArquivos\Service\Arquivo;
use Protocolo\Service\ProtocoloMensagem;
use VersaSpine\Controller\AbstractCoreController;

class ProtocoloMensagemController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request                  = $this->getRequest();
        $paramsGet                = $request->getQuery()->toArray();
        $paramsPost               = $request->getPost()->toArray();
        $serviceProtocoloMensagem = new \Protocolo\Service\ProtocoloMensagem($this->getEntityManager());

        $result = $serviceProtocoloMensagem->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceProtocoloMensagem = new \Protocolo\Service\ProtocoloMensagem($this->getEntityManager());
        $serviceProtocoloMensagem->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceProtocoloMensagem = new \Protocolo\Service\ProtocoloMensagem($this->getEntityManager());
            $dataPost                 = $request->getPost()->toArray();
            $result                   = $serviceProtocoloMensagem->getArrMensagensPorIdProtocolo(
                $dataPost['protocoloId'], $dataPost['usuarioLogado']
            );
            $this->getJson()->setVariable("messages", $result);
        }

        return $this->getJson();
    }

    public function searchProtocoloArquivoAction(){
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceProtocoloMensagem = new \Protocolo\Service\ProtocoloMensagem($this->getEntityManager());
            $dataPost                 = $request->getPost()->toArray();
            $result                   = $serviceProtocoloMensagem->getArrMensagensAnexoPorIdProtocolo(
                $dataPost['protocoloId']
            );
            $this->getJson()->setVariable("data", $result);
        }

        return $this->getJson();
    }

    public function addAction($mensagemId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $serviceProtocoloMensagem = new \Protocolo\Service\ProtocoloMensagem($this->getEntityManager());

        if ($mensagemId) {
            $arrDados = $serviceProtocoloMensagem->getArray($mensagemId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceProtocoloMensagem->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de mensagem salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceProtocoloMensagem->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceProtocoloMensagem->getLastError());
                }
            }
        }

        $serviceProtocoloMensagem->formataDadosPost($arrDados);
        $serviceProtocoloMensagem->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $mensagemId = $this->params()->fromRoute("id", 0);

        if (!$mensagemId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($mensagemId);
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceProtocoloMensagem = new \Protocolo\Service\ProtocoloMensagem($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceProtocoloMensagem->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceProtocoloMensagem->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}
?>