<?php

namespace Protocolo\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * ProtocoloSetorGrupo
 *
 * @ORM\Table(name="protocolo__setor_grupo")
 * @ORM\Entity
 * @LG\LG(id="setorGrupoId",label="SetorId")
 * @Jarvis\Jarvis(title="Listagem de setor grupo",icon="fa fa-table")
 */
class ProtocoloSetorGrupo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="setor_grupo_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="setor_grupo_id")
     * @LG\Labels\Attributes(text="Grupo setor")
     * @LG\Querys\Conditions(type="=")
     */
    private $setorGrupoId;

    /**
     * @var \Protocolo\Entity\ProtocoloSetor
     * @ORM\ManyToOne(targetEntity="Protocolo\Entity\ProtocoloSetor")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="setor_id", referencedColumnName="setor_id", onDelete="CASCADE")
     * })
     */
    private $setor;

    /**
     * @var \Acesso\Entity\AcessoGrupo
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoGrupo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="grupo_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $grupo;

    /**
     * @return integer
     */
    public function getSetorGrupoId()
    {
        return $this->setorGrupoId;
    }

    /**
     * @param integer $setorGrupoId
     * @return ProtocoloSetorGrupo
     */
    public function setSetorGrupoId($setorGrupoId)
    {
        $this->setorGrupoId = $setorGrupoId;

        return $this;
    }

    /**
     * @return \Protocolo\Entity\ProtocoloSetor
     */
    public function getSetor()
    {
        return $this->setor;
    }

    /**
     * @param \Protocolo\Entity\ProtocoloSetor $setor
     * @return ProtocoloSetorGrupo
     */
    public function setSetor($setor)
    {
        $this->setor = $setor;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoGrupo
     */
    public function getGrupo()
    {
        return $this->grupo;
    }

    /**
     * @param \Acesso\Entity\AcessoGrupo $grupo
     * @return ProtocoloSetorGrupo
     */
    public function setGrupo($grupo)
    {
        $this->grupo = $grupo;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'setorGrupoId' => $this->getSetorGrupoId(),
            'setor'        => $this->getSetor(),
            'grupo'        => $this->getGrupo(),
        );

        $array['setor']     = $this->getSetor() ? $this->getSetor()->getSetorId() : null;
        $array['grupoId']   = $this->getGrupo() ? $this->getGrupo()->getId() : null;
        $array['grupoNome'] = $this->getGrupo() ? $this->getGrupo()->getGrupNome() : null;

        return $array;
    }
}
