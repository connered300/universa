<?php

namespace Protocolo\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * ProtocoloMensagem
 *
 * @ORM\Table(name="protocolo__mensagem")
 * @ORM\Entity
 * @LG\LG(id="mensagemId",label="ProtocoloId")
 * @Jarvis\Jarvis(title="Listagem de mensagem",icon="fa fa-table")
 */
class ProtocoloMensagem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="mensagem_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="mensagem_id")
     * @LG\Labels\Attributes(text="código mensagem")
     * @LG\Querys\Conditions(type="=")
     */
    private $mensagemId;

    /**
     * @var \Protocolo\Entity\Protocolo
     * @ORM\ManyToOne(targetEntity="Protocolo\Entity\Protocolo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="protocolo_id", referencedColumnName="protocolo_id")
     * })
     */
    private $protocolo;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mensagem_usuario_id", referencedColumnName="id")
     * })
     */
    private $mensagemUsuario;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="mensagem_data", type="datetime", nullable=false)
     * @LG\Labels\Property(name="mensagem_data")
     * @LG\Labels\Attributes(text="data")
     * @LG\Querys\Conditions(type="=")
     */
    private $mensagemData;

    /**
     * @var string
     *
     * @ORM\Column(name="mensagem_origem", type="string", nullable=false, length=11)
     * @LG\Labels\Property(name="mensagem_origem")
     * @LG\Labels\Attributes(text="origem")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $mensagemOrigem;

    /**
     * @var string
     *
     * @ORM\Column(name="mensagem_solicitante_visivel", type="string", nullable=false, length=3)
     * @LG\Labels\Property(name="mensagem_solicitante_visivel")
     * @LG\Labels\Attributes(text="visível solicitante")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $mensagemSolicitanteVisivel;

    /**
     * @var string
     *
     * @ORM\Column(name="mensagem_conteudo", type="text", nullable=false)
     * @LG\Labels\Property(name="mensagem_conteudo")
     * @LG\Labels\Attributes(text="conteúdo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $mensagemConteudo;

    /**
     * @return integer
     */
    public function getMensagemId()
    {
        return $this->mensagemId;
    }

    /**
     * @param integer $mensagemId
     * @return ProtocoloMensagem
     */
    public function setMensagemId($mensagemId)
    {
        $this->mensagemId = $mensagemId;

        return $this;
    }

    /**
     * @return \Protocolo\Entity\Protocolo
     */
    public function getProtocolo()
    {
        return $this->protocolo;
    }

    /**
     * @param \Protocolo\Entity\Protocolo $protocolo
     * @return ProtocoloMensagem
     */
    public function setProtocolo($protocolo)
    {
        $this->protocolo = $protocolo;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getMensagemUsuario()
    {
        return $this->mensagemUsuario;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $mensagemUsuario
     * @return ProtocoloMensagem
     */
    public function setMensagemUsuario($mensagemUsuario)
    {
        $this->mensagemUsuario = $mensagemUsuario;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getMensagemData($format = false)
    {
        $mensagemData = $this->mensagemData;

        if ($format && $mensagemData) {
            $mensagemData = $mensagemData->format('d/m/Y H:i:s');
        }

        return $mensagemData;
    }

    /**
     * @param \Datetime $mensagemData
     * @return ProtocoloMensagem
     */
    public function setMensagemData($mensagemData)
    {
        if ($mensagemData) {
            if (is_string($mensagemData)) {
                $mensagemData = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $mensagemData
                );
                $mensagemData = new \Datetime($mensagemData);
            }
        } else {
            $mensagemData = null;
        }
        $this->mensagemData = $mensagemData;

        return $this;
    }

    /**
     * @return string
     */
    public function getMensagemOrigem()
    {
        return $this->mensagemOrigem;
    }

    /**
     * @param string $mensagemOrigem
     * @return ProtocoloMensagem
     */
    public function setMensagemOrigem($mensagemOrigem)
    {
        $this->mensagemOrigem = $mensagemOrigem;

        return $this;
    }

    /**
     * @return string
     */
    public function getMensagemSolicitanteVisivel()
    {
        return $this->mensagemSolicitanteVisivel;
    }

    /**
     * @param string $mensagemSolicitanteVisivel
     * @return ProtocoloMensagem
     */
    public function setMensagemSolicitanteVisivel($mensagemSolicitanteVisivel)
    {
        $this->mensagemSolicitanteVisivel = $mensagemSolicitanteVisivel;

        return $this;
    }

    /**
     * @return string
     */
    public function getMensagemConteudo()
    {
        return $this->mensagemConteudo;
    }

    /**
     * @param string $mensagemConteudo
     * @return ProtocoloMensagem
     */
    public function setMensagemConteudo($mensagemConteudo)
    {
        $this->mensagemConteudo = $mensagemConteudo;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'mensagemId'                 => $this->getMensagemId(),
            'protocolo'                  => $this->getProtocolo(),
            'mensagemUsuario'            => $this->getMensagemUsuario(),
            'mensagemData'               => $this->getMensagemData(true),
            'mensagemOrigem'             => $this->getMensagemOrigem(),
            'mensagemSolicitanteVisivel' => $this->getMensagemSolicitanteVisivel(),
            'mensagemConteudo'           => $this->getMensagemConteudo(),
        );

        $array['protocolo']       = $this->getProtocolo() ? $this->getProtocolo()->getProtocoloId() : null;
        $array['mensagemUsuario'] = $this->getMensagemUsuario() ? $this->getMensagemUsuario()->getId() : null;

        return $array;
    }
}
