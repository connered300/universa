<?php

namespace Protocolo\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * ProtocoloSetor
 *
 * @ORM\Table(name="protocolo__setor")
 * @ORM\Entity
 * @LG\LG(id="setorId",label="SetorDescricao")
 * @Jarvis\Jarvis(title="Listagem de setor",icon="fa fa-table")
 */
class ProtocoloSetor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="setor_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="setor_id")
     * @LG\Labels\Attributes(text="código setor")
     * @LG\Querys\Conditions(type="=")
     */
    private $setorId;

    /**
     * @var string
     *
     * @ORM\Column(name="setor_descricao", type="string", nullable=false, length=200)
     * @LG\Labels\Property(name="setor_descricao")
     * @LG\Labels\Attributes(text="descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $setorDescricao;

    /**
     * @var string
     *
     * @ORM\Column(name="setor_visibilidade", type="string", nullable=false, length=7)
     * @LG\Labels\Property(name="setor_visibilidade")
     * @LG\Labels\Attributes(text="visibilidade")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $setorVisibilidade;

    /**
     * @var string
     *
     * @ORM\Column(name="setor_email", type="string", nullable=true, length=100)
     * @LG\Labels\Property(name="setor_email")
     * @LG\Labels\Attributes(text="e-mail")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $setorEmail;

    /**
     * @var \Pessoa\Entity\Pessoa
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     */
    private $pes;

    /**
     * @return integer
     */
    public function getSetorId()
    {
        return $this->setorId;
    }

    /**
     * @param integer $setorId
     * @return ProtocoloSetor
     */
    public function setSetorId($setorId)
    {
        $this->setorId = $setorId;

        return $this;
    }

    /**
     * @return string
     */
    public function getSetorDescricao()
    {
        return $this->setorDescricao;
    }

    /**
     * @param string $setorDescricao
     * @return ProtocoloSetor
     */
    public function setSetorDescricao($setorDescricao)
    {
        $this->setorDescricao = $setorDescricao;

        return $this;
    }

    /**
     * @return string
     */
    public function getSetorVisibilidade()
    {
        return $this->setorVisibilidade;
    }

    /**
     * @param string $setorVisibilidade
     * @return ProtocoloSetor
     */
    public function setSetorVisibilidade($setorVisibilidade)
    {
        $this->setorVisibilidade = $setorVisibilidade;

        return $this;
    }

    /**
     * @return string
     */
    public function getSetorEmail()
    {
        return $this->setorEmail;
    }

    /**
     * @param string $setorEmail
     * @return ProtocoloSetor
     */
    public function setSetorEmail($setorEmail)
    {
        $this->setorEmail = $setorEmail;

        return $this;
    }

    /**
     * @return \Pessoa\Entity\Pessoa
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @param \Pessoa\Entity\Pessoa $pes
     * @return ProtocoloSetor
     */
    public function setPes($pes)
    {
        $this->pes = $pes;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'setorId'           => $this->getSetorId(),
            'setorDescricao'    => $this->getSetorDescricao(),
            'setorVisibilidade' => $this->getSetorVisibilidade(),
            'setorEmail'        => $this->getSetorEmail(),
            'pes'               => $this->getPes(),
        );

        $array['pes'] = $this->getPes() ? $this->getPes()->getPesId() : null;

        return $array;
    }
}
