<?php

namespace Protocolo\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * ProtocoloMensagemPadrao
 *
 * @ORM\Table(name="protocolo__mensagem_padrao")
 * @ORM\Entity
 * @LG\LG(id="mensagemId",label="MensagemDescricao")
 * @Jarvis\Jarvis(title="Listagem de mensagem padrão",icon="fa fa-table")
 */
class ProtocoloMensagemPadrao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="mensagem_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="mensagem_id")
     * @LG\Labels\Attributes(text="código mensagem")
     * @LG\Querys\Conditions(type="=")
     */
    private $mensagemId;

    /**
     * @var \Protocolo\Entity\ProtocoloSetor
     * @ORM\ManyToOne(targetEntity="Protocolo\Entity\ProtocoloSetor")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="setor_id", referencedColumnName="setor_id")
     * })
     */
    private $setor;

    /**
     * @var \Protocolo\Entity\ProtocoloSolicitacao
     * @ORM\ManyToOne(targetEntity="Protocolo\Entity\ProtocoloSolicitacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="solicitacao_id", referencedColumnName="solicitacao_id")
     * })
     */
    private $solicitacao;

    /**
     * @var string
     *
     * @ORM\Column(name="mensagem_descricao", type="string", nullable=true, length=150)
     * @LG\Labels\Property(name="mensagem_descricao")
     * @LG\Labels\Attributes(text="descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $mensagemDescricao;

    /**
     * @var string
     *
     * @ORM\Column(name="mensagem_conteudo", type="text", nullable=true)
     * @LG\Labels\Property(name="mensagem_conteudo")
     * @LG\Labels\Attributes(text="conteúdo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $mensagemConteudo;

    /**
     * @return integer
     */
    public function getMensagemId()
    {
        return $this->mensagemId;
    }

    /**
     * @param integer $mensagemId
     * @return ProtocoloMensagemPadrao
     */
    public function setMensagemId($mensagemId)
    {
        $this->mensagemId = $mensagemId;

        return $this;
    }

    /**
     * @return \Protocolo\Entity\ProtocoloSetor
     */
    public function getSetor()
    {
        return $this->setor;
    }

    /**
     * @param \Protocolo\Entity\ProtocoloSetor $setor
     * @return ProtocoloMensagemPadrao
     */
    public function setSetor($setor)
    {
        $this->setor = $setor;

        return $this;
    }

    /**
     * @return \Protocolo\Entity\ProtocoloSolicitacao
     */
    public function getSolicitacao()
    {
        return $this->solicitacao;
    }

    /**
     * @param \Protocolo\Entity\ProtocoloSolicitacao $solicitacao
     * @return ProtocoloMensagemPadrao
     */
    public function setSolicitacao($solicitacao)
    {
        $this->solicitacao = $solicitacao;

        return $this;
    }

    /**
     * @return string
     */
    public function getMensagemDescricao()
    {
        return $this->mensagemDescricao;
    }

    /**
     * @param string $mensagemDescricao
     * @return ProtocoloMensagemPadrao
     */
    public function setMensagemDescricao($mensagemDescricao)
    {
        $this->mensagemDescricao = $mensagemDescricao;

        return $this;
    }

    /**
     * @return string
     */
    public function getMensagemConteudo()
    {
        return $this->mensagemConteudo;
    }

    /**
     * @param string $mensagemConteudo
     * @return ProtocoloMensagemPadrao
     */
    public function setMensagemConteudo($mensagemConteudo)
    {
        $this->mensagemConteudo = $mensagemConteudo;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'mensagemId'        => $this->getMensagemId(),
            'setor'             => $this->getSetor(),
            'solicitacao'       => $this->getSolicitacao(),
            'mensagemDescricao' => $this->getMensagemDescricao(),
            'mensagemConteudo'  => $this->getMensagemConteudo(),
        );

        $array['setor']       = $this->getSetor() ? $this->getSetor()->getSetorId() : null;
        $array['solicitacao'] = $this->getSolicitacao() ? $this->getSolicitacao()->getSolicitacaoId() : null;

        return $array;
    }
}
