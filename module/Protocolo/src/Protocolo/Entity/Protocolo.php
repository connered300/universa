<?php

namespace Protocolo\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * Protocolo
 *
 * @ORM\Table(name="protocolo")
 * @ORM\Entity
 * @LG\LG(id="protocoloId",label="ProtocoloSolicitanteNome")
 * @Jarvis\Jarvis(title="Listagem de protocolo",icon="fa fa-table")
 */
class Protocolo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="protocolo_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="protocolo_id")
     * @LG\Labels\Attributes(text="código")
     * @LG\Querys\Conditions(type="=")
     */
    private $protocoloId;

    /**
     * @var \Protocolo\Entity\ProtocoloSolicitacao
     * @ORM\ManyToOne(targetEntity="Protocolo\Entity\ProtocoloSolicitacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="solicitacao_id", referencedColumnName="solicitacao_id")
     * })
     */
    private $solicitacao;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id_criacao", referencedColumnName="id")
     * })
     */
    private $usuarioIdCriacao;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id_responsavel", referencedColumnName="id")
     * })
     */
    private $usuarioIdResponsavel;

    /**
     * @var \Pessoa\Entity\Pessoa
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="protocolo_solicitante_pes_id", referencedColumnName="pes_id")
     * })
     */
    private $protocoloSolicitantePes;

    /**
     * @var \Matricula\Entity\AcadgeralAlunoCurso
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadgeralAlunoCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="protocolo_solicitante_alunocurso_id", referencedColumnName="alunocurso_id")
     * })
     */
    private $protocoloSolicitanteAlunocurso;

    /**
     * @var string
     *
     * @ORM\Column(name="protocolo_solicitante_nome", type="string", nullable=true, length=100)
     * @LG\Labels\Property(name="protocolo_solicitante_nome")
     * @LG\Labels\Attributes(text="nome solicitante")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $protocoloSolicitanteNome;

    /**
     * @var string
     *
     * @ORM\Column(name="protocolo_solicitante_email", type="string", nullable=true, length=100)
     * @LG\Labels\Property(name="protocolo_solicitante_email")
     * @LG\Labels\Attributes(text="e-mail solicitante")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $protocoloSolicitanteEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="protocolo_solicitante_telefone", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="protocolo_solicitante_telefone")
     * @LG\Labels\Attributes(text="telefone solicitante")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $protocoloSolicitanteTelefone;

    /**
     * @var string
     *
     * @ORM\Column(name="protocolo_solicitante_visivel", type="string", nullable=false, length=3)
     * @LG\Labels\Property(name="protocolo_solicitante_visivel")
     * @LG\Labels\Attributes(text="visível solicitante")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $protocoloSolicitanteVisivel;

    /**
     * @var string
     *
     * @ORM\Column(name="protocolo_assunto", type="string", nullable=false, length=200)
     * @LG\Labels\Property(name="protocolo_assunto")
     * @LG\Labels\Attributes(text="assunto")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $protocoloAssunto;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="protocolo_data_cadastro", type="datetime", nullable=true)
     * @LG\Labels\Property(name="protocolo_data_cadastro")
     * @LG\Labels\Attributes(text="cadastro data")
     * @LG\Querys\Conditions(type="=")
     */
    private $protocoloDataCadastro;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="protocolo_data_alteracao", type="datetime", nullable=false)
     * @LG\Labels\Property(name="protocolo_data_alteracao")
     * @LG\Labels\Attributes(text="alteração data")
     * @LG\Querys\Conditions(type="=")
     */
    private $protocoloDataAlteracao;

    /**
     * @var integer
     *
     * @ORM\Column(name="protocolo_avaliacao", type="integer", nullable=false, length=5)
     * @LG\Labels\Property(name="protocolo_avaliacao")
     * @LG\Labels\Attributes(text="avaliação")
     * @LG\Querys\Conditions(type="=")
     */
    private $protocoloAvaliacao;

    /**
     * @var string
     *
     * @ORM\Column(name="protocolo_situacao", type="string", nullable=true, length=12)
     * @LG\Labels\Property(name="protocolo_situacao")
     * @LG\Labels\Attributes(text="situação")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $protocoloSituacao;

    /**
     * @return integer
     */
    public function getProtocoloId()
    {
        return $this->protocoloId;
    }

    /**
     * @param integer $protocoloId
     * @return Protocolo
     */
    public function setProtocoloId($protocoloId)
    {
        $this->protocoloId = $protocoloId;

        return $this;
    }

    /**
     * @return \Protocolo\Entity\ProtocoloSolicitacao
     */
    public function getSolicitacao()
    {
        return $this->solicitacao;
    }

    /**
     * @param \Protocolo\Entity\ProtocoloSolicitacao $solicitacao
     * @return Protocolo
     */
    public function setSolicitacao($solicitacao)
    {
        $this->solicitacao = $solicitacao;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuarioIdCriacao()
    {
        return $this->usuarioIdCriacao;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuarioIdCriacao
     * @return Protocolo
     */
    public function setUsuarioIdCriacao($usuarioIdCriacao)
    {
        $this->usuarioIdCriacao = $usuarioIdCriacao;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuarioIdResponsavel()
    {
        return $this->usuarioIdResponsavel;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuarioIdResponsavel
     * @return Protocolo
     */
    public function setUsuarioIdResponsavel($usuarioIdResponsavel)
    {
        $this->usuarioIdResponsavel = $usuarioIdResponsavel;

        return $this;
    }

    /**
     * @return \Pessoa\Entity\Pessoa
     */
    public function getProtocoloSolicitantePes()
    {
        return $this->protocoloSolicitantePes;
    }

    /**
     * @param \Pessoa\Entity\Pessoa $protocoloSolicitantePes
     * @return Protocolo
     */
    public function setProtocoloSolicitantePes($protocoloSolicitantePes)
    {
        $this->protocoloSolicitantePes = $protocoloSolicitantePes;

        return $this;
    }

    /**
     * @return \Matricula\Entity\AcadgeralAlunoCurso
     */
    public function getProtocoloSolicitanteAlunocurso()
    {
        return $this->protocoloSolicitanteAlunocurso;
    }

    /**
     * @param \Matricula\Entity\AcadgeralAlunoCurso $protocoloSolicitanteAlunocurso
     * @return Protocolo
     */
    public function setProtocoloSolicitanteAlunocurso($protocoloSolicitanteAlunocurso)
    {
        $this->protocoloSolicitanteAlunocurso = $protocoloSolicitanteAlunocurso;

        return $this;
    }

    /**
     * @return string
     */
    public function getProtocoloSolicitanteNome()
    {
        return $this->protocoloSolicitanteNome;
    }

    /**
     * @param string $protocoloSolicitanteNome
     * @return Protocolo
     */
    public function setProtocoloSolicitanteNome($protocoloSolicitanteNome)
    {
        $this->protocoloSolicitanteNome = $protocoloSolicitanteNome;

        return $this;
    }

    /**
     * @return string
     */
    public function getProtocoloSolicitanteEmail()
    {
        return $this->protocoloSolicitanteEmail;
    }

    /**
     * @param string $protocoloSolicitanteEmail
     * @return Protocolo
     */
    public function setProtocoloSolicitanteEmail($protocoloSolicitanteEmail)
    {
        $this->protocoloSolicitanteEmail = $protocoloSolicitanteEmail;

        return $this;
    }

    /**
     * @return string
     */
    public function getProtocoloSolicitanteTelefone()
    {
        return $this->protocoloSolicitanteTelefone;
    }

    /**
     * @param string $protocoloSolicitanteTelefone
     * @return Protocolo
     */
    public function setProtocoloSolicitanteTelefone($protocoloSolicitanteTelefone)
    {
        $this->protocoloSolicitanteTelefone = $protocoloSolicitanteTelefone;

        return $this;
    }

    /**
     * @return string
     */
    public function getProtocoloSolicitanteVisivel()
    {
        return $this->protocoloSolicitanteVisivel;
    }

    /**
     * @param string $protocoloSolicitanteVisivel
     * @return Protocolo
     */
    public function setProtocoloSolicitanteVisivel($protocoloSolicitanteVisivel)
    {
        $this->protocoloSolicitanteVisivel = $protocoloSolicitanteVisivel;

        return $this;
    }

    /**
     * @return string
     */
    public function getProtocoloAssunto()
    {
        return $this->protocoloAssunto;
    }

    /**
     * @param string $protocoloAssunto
     * @return Protocolo
     */
    public function setProtocoloAssunto($protocoloAssunto)
    {
        $this->protocoloAssunto = $protocoloAssunto;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getProtocoloDataCadastro($format = false)
    {
        $protocoloDataCadastro = $this->protocoloDataCadastro;

        if ($format && $protocoloDataCadastro) {
            $protocoloDataCadastro = $protocoloDataCadastro->format('d/m/Y H:i:s');
        }

        return $protocoloDataCadastro;
    }

    /**
     * @param \Datetime $protocoloDataCadastro
     * @return Protocolo
     */
    public function setProtocoloDataCadastro($protocoloDataCadastro)
    {
        if ($protocoloDataCadastro) {
            if (is_string($protocoloDataCadastro)) {
                $protocoloDataCadastro = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $protocoloDataCadastro
                );
                $protocoloDataCadastro = new \Datetime($protocoloDataCadastro);
            }
        } else {
            $protocoloDataCadastro = null;
        }
        $this->protocoloDataCadastro = $protocoloDataCadastro;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getProtocoloDataAlteracao($format = false)
    {
        $protocoloDataAlteracao = $this->protocoloDataAlteracao;

        if ($format && $protocoloDataAlteracao) {
            $protocoloDataAlteracao = $protocoloDataAlteracao->format('d/m/Y H:i:s');
        }

        return $protocoloDataAlteracao;
    }

    /**
     * @param \Datetime $protocoloDataAlteracao
     * @return Protocolo
     */
    public function setProtocoloDataAlteracao($protocoloDataAlteracao)
    {
        if ($protocoloDataAlteracao) {
            if (is_string($protocoloDataAlteracao)) {
                $protocoloDataAlteracao = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $protocoloDataAlteracao
                );
                $protocoloDataAlteracao = new \Datetime($protocoloDataAlteracao);
            }
        } else {
            $protocoloDataAlteracao = null;
        }
        $this->protocoloDataAlteracao = $protocoloDataAlteracao;

        return $this;
    }

    /**
     * @return integer
     */
    public function getProtocoloAvaliacao()
    {
        return $this->protocoloAvaliacao;
    }

    /**
     * @param integer $protocoloAvaliacao
     * @return Protocolo
     */
    public function setProtocoloAvaliacao($protocoloAvaliacao)
    {
        $this->protocoloAvaliacao = $protocoloAvaliacao;

        return $this;
    }

    /**
     * @return string
     */
    public function getProtocoloSituacao()
    {
        return $this->protocoloSituacao;
    }

    /**
     * @param string $protocoloSituacao
     * @return Protocolo
     */
    public function setProtocoloSituacao($protocoloSituacao)
    {
        $this->protocoloSituacao = $protocoloSituacao;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'protocoloId'                    => $this->getProtocoloId(),
            'solicitacao'                    => $this->getSolicitacao(),
            'usuarioIdCriacao'               => $this->getUsuarioIdCriacao(),
            'usuarioIdResponsavel'           => $this->getUsuarioIdResponsavel(),
            'protocoloSolicitantePes'        => $this->getProtocoloSolicitantePes(),
            'protocoloSolicitanteAlunocurso' => $this->getProtocoloSolicitanteAlunocurso(),
            'protocoloSolicitanteNome'       => (
            $this->getProtocoloSolicitanteNome() ? $this->getProtocoloSolicitanteNome() : null
            ),
            'protocoloSolicitanteEmail'      => (
            $this->getProtocoloSolicitanteEmail() ? $this->getProtocoloSolicitanteEmail() : null
            ),
            'protocoloSolicitanteTelefone'   => (
            $this->getProtocoloSolicitanteTelefone() ? $this->getProtocoloSolicitanteTelefone() : null
            ),
            'protocoloSolicitanteVisivel'    => $this->getProtocoloSolicitanteVisivel(),
            'protocoloAssunto'               => $this->getProtocoloAssunto(),
            'protocoloDataCadastro'          => $this->getProtocoloDataCadastro(true),
            'protocoloDataAlteracao'         => $this->getProtocoloDataAlteracao(true),
            'protocoloAvaliacao'             => $this->getProtocoloAvaliacao(),
            'protocoloSituacao'              => $this->getProtocoloSituacao(),
        );

        if ($this->getSolicitacao()) {
            $array['solicitacao']          = $this->getSolicitacao()->getSolicitacaoId();
            $array['solicitacaoDescricao'] = $this->getSolicitacao()->getSolicitacaoDescricao();
            $array['setor']                = $this->getSolicitacao()->getSetor()->getSetorId();
            $array['setorDescricao']       = $this->getSolicitacao()->getSetor()->getSetorDescricao();
        }

        if ($this->getUsuarioIdCriacao()) {
            $array['usuarioIdCriacao']      = $this->getUsuarioIdCriacao()->getId();
            $array['usuarioIdCriacaoLogin'] = $this->getUsuarioIdCriacao()->getLogin();
        }

        if ($this->getUsuarioIdResponsavel()) {
            $array['usuarioIdResponsavel']      = $this->getUsuarioIdResponsavel()->getId();
            $array['usuarioIdResponsavelLogin'] = $this->getUsuarioIdResponsavel()->getLogin();
        }

        if ($this->getProtocoloSolicitantePes()) {
            $array['protocoloSolicitantePes']     = $this->getProtocoloSolicitantePes()->getPesId();
            $array['protocoloSolicitantePesNome'] = $this->getProtocoloSolicitantePes()->getPesNome();
            $array['protocoloSolicitanteNome']    = $this->getProtocoloSolicitantePes()->getPesNome();
        }

        if ($this->getProtocoloSolicitanteAlunocurso()) {
            $array['protocoloSolicitanteAlunocurso'] = $this->getProtocoloSolicitanteAlunocurso()->getAlunocursoId();
        }

        return $array;
    }
}
