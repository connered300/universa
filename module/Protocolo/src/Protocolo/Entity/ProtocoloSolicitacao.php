<?php

namespace Protocolo\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * ProtocoloSolicitacao
 *
 * @ORM\Table(name="protocolo__solicitacao")
 * @ORM\Entity
 * @LG\LG(id="solicitacaoId",label="SolicitacaoDescricao")
 * @Jarvis\Jarvis(title="Listagem de solicitação",icon="fa fa-table")
 */
class ProtocoloSolicitacao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="solicitacao_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="solicitacao_id")
     * @LG\Labels\Attributes(text="código solicitação")
     * @LG\Querys\Conditions(type="=")
     */
    private $solicitacaoId;

    /**
     * @var \Protocolo\Entity\ProtocoloSetor
     * @ORM\ManyToOne(targetEntity="Protocolo\Entity\ProtocoloSetor")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="setor_id", referencedColumnName="setor_id")
     * })
     */
    private $setor;

    /**
     * @var string
     *
     * @ORM\Column(name="solicitacao_descricao", type="string", nullable=false, length=200)
     * @LG\Labels\Property(name="solicitacao_descricao")
     * @LG\Labels\Attributes(text="descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $solicitacaoDescricao;

    /**
     * @var string
     *
     * @ORM\Column(name="solicitacao_visibilidade", type="string", nullable=false, length=7)
     * @LG\Labels\Property(name="solicitacao_visibilidade")
     * @LG\Labels\Attributes(text="visibilidade")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $solicitacaoVisibilidade;

    /**
     * @return integer
     */
    public function getSolicitacaoId()
    {
        return $this->solicitacaoId;
    }

    /**
     * @param integer $solicitacaoId
     * @return ProtocoloSolicitacao
     */
    public function setSolicitacaoId($solicitacaoId)
    {
        $this->solicitacaoId = $solicitacaoId;

        return $this;
    }

    /**
     * @return \Protocolo\Entity\ProtocoloSetor
     */
    public function getSetor()
    {
        return $this->setor;
    }

    /**
     * @param \Protocolo\Entity\ProtocoloSetor $setor
     * @return ProtocoloSolicitacao
     */
    public function setSetor($setor)
    {
        $this->setor = $setor;

        return $this;
    }

    /**
     * @return string
     */
    public function getSolicitacaoDescricao()
    {
        return $this->solicitacaoDescricao;
    }

    /**
     * @param string $solicitacaoDescricao
     * @return ProtocoloSolicitacao
     */
    public function setSolicitacaoDescricao($solicitacaoDescricao)
    {
        $this->solicitacaoDescricao = $solicitacaoDescricao;

        return $this;
    }

    /**
     * @return string
     */
    public function getSolicitacaoVisibilidade()
    {
        return $this->solicitacaoVisibilidade;
    }

    /**
     * @param string $solicitacaoVisibilidade
     * @return ProtocoloSolicitacao
     */
    public function setSolicitacaoVisibilidade($solicitacaoVisibilidade)
    {
        $this->solicitacaoVisibilidade = $solicitacaoVisibilidade;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'solicitacaoId'           => $this->getSolicitacaoId(),
            'setor'                   => $this->getSetor(),
            'solicitacaoDescricao'    => $this->getSolicitacaoDescricao(),
            'solicitacaoVisibilidade' => $this->getSolicitacaoVisibilidade(),

        );

        $array['setor'] = $this->getSetor() ? $this->getSetor()->getSetorId() : null;

        return $array;
    }
}
