<?php

namespace Protocolo\Service;

use VersaSpine\Service\AbstractService;

class ProtocoloSetor extends AbstractService
{
    const SETOR_VISIBILIDADE_ALUNO   = 'Aluno';
    const SETOR_VISIBILIDADE_EXTERNO = 'Externo';
    const SETOR_VISIBILIDADE_INTERNO = 'Interno';
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2SetorVisibilidade($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getSetorVisibilidade());
    }

    public static function getSetorVisibilidade()
    {
        return array(
            self::SETOR_VISIBILIDADE_ALUNO,
            self::SETOR_VISIBILIDADE_EXTERNO,
            self::SETOR_VISIBILIDADE_INTERNO
        );
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Protocolo\Entity\ProtocoloSetor');
    }

    public function pesquisaForJson($params)
    {
        $serviceAcesso = new \Acesso\Service\AcessoPessoas($this->getEm());

        $sql            = '
        SELECT
        a.setorId AS setor_id,
        a.setorDescricao AS setor_descricao,
        a.setorVisibilidade AS setor_visibilidade,
        a.setorEmail AS setor_email
        FROM Protocolo\Entity\ProtocoloSetor a
        INNER JOIN Protocolo\Entity\ProtocoloSetorGrupo p WITH p.setor = a.setorId
        WHERE';
        $setorDescricao = false;
        $setorId        = false;
        $arrGrupo       = array(-1);

        if ($objAcessoPessoa = $serviceAcesso->retornaUsuarioLogado()) {
            foreach ($objAcessoPessoa->getUsuario()->getGrupo() as $objAcessoGrupo) {
                $arrGrupo[] = $objAcessoGrupo->getId();
            }
        }

        if ($params['q']) {
            $setorDescricao = $params['q'];
        } elseif ($params['query']) {
            $setorDescricao = $params['query'];
        }

        if ($params['setorId']) {
            $setorId = $params['setorId'];
        }

        $parameters = array(
            'setorDescricao' => "{$setorDescricao}%",
            'setorId1'       => "{$setorDescricao}"
        );

        $sql .= ' (a.setorDescricao LIKE :setorDescricao OR a.setorId=:setorId1)';

        if ($setorId) {
            $parameters['setorId'] = explode(',', $setorId);
            $parameters['setorId'] = $setorId;
            $sql .= ' AND a.setorId NOT IN(:setorId)';
        }

        if ($arrGrupo) {
            $parameters['grupo'] = $arrGrupo;
            $sql .= ' AND p.grupo IN(:grupo)';
        }

        $sql .= " GROUP BY a.setorId";
        $sql .= " ORDER BY a.setorDescricao";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $servicePessoa = new \Pessoa\Service\Pessoa($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['setorId']) {
                /** @var $objProtocoloSetor \Protocolo\Entity\ProtocoloSetor */
                $objProtocoloSetor = $this->getRepository()->find($arrDados['setorId']);

                if (!$objProtocoloSetor) {
                    $this->setLastError('Registro de setor não existe!');

                    return false;
                }
            } else {
                $objProtocoloSetor = new \Protocolo\Entity\ProtocoloSetor();
            }

            $objProtocoloSetor->setSetorDescricao($arrDados['setorDescricao']);
            $objProtocoloSetor->setSetorVisibilidade($arrDados['setorVisibilidade']);
            $objProtocoloSetor->setSetorEmail($arrDados['setorEmail']);

            if ($arrDados['pes']) {
                /** @var $objPessoa \Pessoa\Entity\Pessoa */
                $objPessoa = $servicePessoa->getRepository()->find($arrDados['pes']);

                if (!$objPessoa) {
                    $this->setLastError('Registro de pessoa não existe!');

                    return false;
                }

                $objProtocoloSetor->setPes($objPessoa);
            } else {
                $objProtocoloSetor->setPes(null);
            }

            $this->getEm()->persist($objProtocoloSetor);
            $this->getEm()->flush($objProtocoloSetor);

            $this->salvarMultiplos($arrDados, $objProtocoloSetor);

            $this->getEm()->commit();

            $arrDados['setorId'] = $objProtocoloSetor->getSetorId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de setor!<br>' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param array                            $arrDados
     * @param \Protocolo\Entity\ProtocoloSetor $objSetor
     * @return boolean
     */
    public function salvarMultiplos($arrDados, &$objSetor)
    {
        if (!$arrDados['grupoId']) {
            $this->setLastError("Nenhum grupo informado!");

            return false;
        }

        $serviceSetorGrupo = new \Protocolo\Service\ProtocoloSetorGrupo($this->getEm());
        $arrObjGrupo       = array();

        $arrGrupoId       = explode(',', $arrDados['grupoId']);
        $arrObjSetorGrupo = $serviceSetorGrupo->getRepository()->findBy(['setor' => $objSetor->getSetorId()]);

        foreach ($arrGrupoId as $grupoId) {
            $objGrupo = $this->getRepository('\Acesso\Entity\AcessoGrupo')->findOneBy(
                ['id' => $grupoId]
            );

            $arrObjGrupo[] = $objGrupo;
        }

        if ($arrObjSetorGrupo) {
            foreach ($arrObjSetorGrupo as $objReg) {
                $this->getEm()->beginTransaction();
                $this->getEm()->remove($objReg);
                $this->getEm()->flush();
                $this->getEm()->commit();
            }
        }

        /** @var \Acesso\Entity\AcessoGrupo $objGrupo */
        foreach ($arrObjGrupo as $objGrupo) {
            try {
                $this->getEm()->beginTransaction();
                $objSetorGrupo = new \Protocolo\Entity\ProtocoloSetorGrupo();

                $objSetorGrupo->setGrupo($objGrupo);
                $objSetorGrupo->setSetor($objSetor);

                $this->getEm()->persist($objSetorGrupo);
                $this->getEm()->flush($objSetorGrupo);
                $this->getEm()->commit();
            } catch (\Exception $e) {
                $this->setLastError($e->getMessage());

                return false;
            }
        }

        return true;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['setorDescricao']) {
            $errors[] = 'Por favor preencha o campo "descrição"!';
        }

        if (!$arrParam['setorVisibilidade']) {
            $errors[] = 'Por favor preencha o campo "visibilidade"!';
        }

        if (array_diff($arrParam['setorVisibilidade'], self::getSetorVisibilidade())) {
            $errors[] = 'Por favor selecione valores válidos para o campo "visibilidade"!';
        }

        if (!$arrParam['pes']) {
            $errors[] = 'Por favor preencha o campo "código pessoa"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM protocolo__setor LEFT JOIN pessoa USING(pes_id)";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($setorId)
    {
        $arrDados = array();

        if (!$setorId) {
            $this->setLastError('Setor inválido!');

            return array();
        }

        /** @var $objProtocoloSetor \Protocolo\Entity\ProtocoloSetor */
        $objProtocoloSetor = $this->getRepository()->find($setorId);

        if (!$objProtocoloSetor) {
            $this->setLastError('Setor não existe!');

            return array();
        }

        $servicePessoa     = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceSetorGrupo = new \Protocolo\Service\ProtocoloSetorGrupo($this->getEm());

        try {
            $arrDados = $objProtocoloSetor->toArray();

            if ($arrDados['pes']) {
                $arrPessoa       = $servicePessoa->getArrSelect2(['id' => $arrDados['pes']]);
                $arrDados['pes'] = $arrPessoa ? $arrPessoa[0] : null;
            }

            $arrDados['grupo'] = $serviceSetorGrupo->getArrSelect2UsingSetorId($setorId);
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $servicePessoa = new \Pessoa\Service\Pessoa($this->getEm());

        if (is_array($arrDados['pes']) && !$arrDados['pes']['text']) {
            $arrDados['pes'] = $arrDados['pes']['pesId'];
        }

        if ($arrDados['pes'] && is_string($arrDados['pes'])) {
            $arrDados['pes'] = $servicePessoa->getArrSelect2(array('id' => $arrDados['pes']));
            $arrDados['pes'] = $arrDados['pes'] ? $arrDados['pes'][0] : null;
        }

        if ($arrDados['setorVisibilidade']) {
            $array           = array();
            $arrVisibilidade = explode(',', $arrDados['setorVisibilidade']);
            foreach ($arrVisibilidade as $index => $visibilidade) {
                $array[$index]['text'] = $visibilidade;
                $array[$index]['id']   = $visibilidade;
            }
            $arrDados['setorVisibilidade'] = $array;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['setorId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['setorDescricao' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Protocolo\Entity\ProtocoloSetor */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getSetorId();
            $arrEntity[$params['value']] = $objEntity->getSetorDescricao();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($setorId)
    {
        if (!$setorId) {
            $this->setLastError('Para remover um registro de setor é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objProtocoloSetor \Protocolo\Entity\ProtocoloSetor */
            $objProtocoloSetor = $this->getRepository()->find($setorId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objProtocoloSetor);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de setor.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $servicePessoa = new \Pessoa\Service\Pessoa($this->getEm());

        $servicePessoa->setarDependenciasView($view);

        $view->setVariable("arrSetorVisibilidade", $this->getArrSelect2SetorVisibilidade());
    }

    public function retornarSetoresUsuarios($setorSemGrupo = false)
    {
        $service       = new \Acesso\Service\AcessoPessoas($this->getEm());
        $usuarioLogado = $service->retornaUsuarioLogado();
        $usuarioLogado = $usuarioLogado ? $usuarioLogado->getUsuario() : null;

        $arrGrupo = array();

        if ($usuarioLogado) {
            foreach ($usuarioLogado->getGrupo() as $objAcessoGrupo) {
                $arrGrupo[] = $objAcessoGrupo->getId();
            }
        }

        $query = 'SELECT setor_id FROM protocolo__setor_grupo WHERE grupo_id IN(' . implode(',', $arrGrupo) . ')';

        if ($setorSemGrupo) {
            $query .= 'UNION ALL';
            $query .= '
            SELECT setor_id FROM protocolo__setor setor
            LEFT JOIN protocolo__setor_grupo AS setor_grupo USING (setor_id)
            WHERE setor_grupo.setor_id IS NULL';
        }

        $arrSetor = $this->executeQueryWithParam($query)->fetchAll(\PDO::FETCH_COLUMN);

        return $arrSetor ? $arrSetor : array(-1);
    }
}
?>
