<?php

namespace Protocolo\Service;

use VersaSpine\Service\AbstractService;

class ProtocoloSetorGrupo extends AbstractService
{
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Protocolo\Entity\ProtocoloSetorGrupo');
    }

    public function pesquisaForJson($params)
    {
        $sql          = '
        SELECT
        a.setorGrupoId as setor_grupo_id,
        a.setorId as setor_id,
        a.grupoId as grupo_id
        FROM Protocolo\Entity\ProtocoloSetorGrupo a
        WHERE';
        $setorId      = false;
        $setorGrupoId = false;

        if ($params['q']) {
            $setorId = $params['q'];
        } elseif ($params['query']) {
            $setorId = $params['query'];
        }

        if ($params['setorGrupoId']) {
            $setorGrupoId = $params['setorGrupoId'];
        }

        $parameters = array('setorId' => "{$setorId}%");
        $sql .= ' a.setorId LIKE :setorId';

        if ($setorGrupoId) {
            $parameters['setorGrupoId'] = explode(',', $setorGrupoId);
            $parameters['setorGrupoId'] = $setorGrupoId;
            $sql .= ' AND a.setorGrupoId NOT IN(:setorGrupoId)';
        }

        $sql .= " ORDER BY a.setorId";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceProtocoloSetor = new \Protocolo\Service\ProtocoloSetor($this->getEm());
        $serviceAcessoGrupo    = new \Acesso\Service\AcessoGrupo($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['setorGrupoId']) {
                /** @var $objProtocoloSetorGrupo \Protocolo\Entity\ProtocoloSetorGrupo */
                $objProtocoloSetorGrupo = $this->getRepository()->find($arrDados['setorGrupoId']);

                if (!$objProtocoloSetorGrupo) {
                    $this->setLastError('Registro de setor grupo não existe!');

                    return false;
                }
            } else {
                $objProtocoloSetorGrupo = new \Protocolo\Entity\ProtocoloSetorGrupo();
            }

            if ($arrDados['setor']) {
                /** @var $objProtocoloSetor \Protocolo\Entity\ProtocoloSetor */
                $objProtocoloSetor = $serviceProtocoloSetor->getRepository()->find($arrDados['setor']);

                if (!$objProtocoloSetor) {
                    $this->setLastError('Registro de setor não existe!');

                    return false;
                }

                $objProtocoloSetorGrupo->setSetor($objProtocoloSetor);
            } else {
                $objProtocoloSetorGrupo->setSetor(null);
            }

            if ($arrDados['grupo']) {
                /** @var $objAcessoGrupo \Acesso\Entity\AcessoGrupo */
                $objAcessoGrupo = $serviceAcessoGrupo->getRepository()->find($arrDados['grupo']);

                if (!$objAcessoGrupo) {
                    $this->setLastError('Registro de grupo não existe!');

                    return false;
                }

                $objProtocoloSetorGrupo->setGrupo($objAcessoGrupo);
            } else {
                $objProtocoloSetorGrupo->setGrupo(null);
            }

            $this->getEm()->persist($objProtocoloSetorGrupo);
            $this->getEm()->flush($objProtocoloSetorGrupo);

            $this->getEm()->commit();

            $arrDados['setorGrupoId'] = $objProtocoloSetorGrupo->getSetorGrupoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de setor grupo!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['setor']) {
            $errors[] = 'Por favor preencha o campo "código setor"!';
        }

        if (!$arrParam['grupo']) {
            $errors[] = 'Por favor preencha o campo "Grupo"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM protocolo__setor_grupo";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($setorGrupoId)
    {
        $arrDados = array();

        if (!$setorGrupoId) {
            $this->setLastError('Setor grupo inválido!');

            return array();
        }

        /** @var $objProtocoloSetorGrupo \Protocolo\Entity\ProtocoloSetorGrupo */
        $objProtocoloSetorGrupo = $this->getRepository()->find($setorGrupoId);

        if (!$objProtocoloSetorGrupo) {
            $this->setLastError('Setor grupo não existe!');

            return array();
        }

        $serviceProtocoloSetor = new \Protocolo\Service\ProtocoloSetor($this->getEm());
        $serviceAcessoGrupo    = new \Acesso\Service\AcessoGrupo($this->getEm());

        try {
            $arrDados = $objProtocoloSetorGrupo->toArray();

            if ($arrDados['setor']) {
                $arrProtocoloSetor = $serviceProtocoloSetor->getArrSelect2(['id' => $arrDados['setor']]);
                $arrDados['setor'] = $arrProtocoloSetor ? $arrProtocoloSetor[0] : null;
            }

            if ($arrDados['grupo']) {
                $arrAcessoGrupo    = $serviceAcessoGrupo->getArrSelect2(['id' => $arrDados['grupo']]);
                $arrDados['grupo'] = $arrAcessoGrupo ? $arrAcessoGrupo[0] : null;
            }
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceProtocoloSetor = new \Protocolo\Service\ProtocoloSetor($this->getEm());
        $serviceAcessoGrupo    = new \Acesso\Service\AcessoGrupo($this->getEm());

        if (is_array($arrDados['setor']) && !$arrDados['setor']['text']) {
            $arrDados['setor'] = $arrDados['setor']['setorId'];
        }

        if ($arrDados['setor'] && is_string($arrDados['setor'])) {
            $arrDados['setor'] = $serviceProtocoloSetor->getArrSelect2(array('id' => $arrDados['setor']));
            $arrDados['setor'] = $arrDados['setor'] ? $arrDados['setor'][0] : null;
        }

        if (is_array($arrDados['grupo']) && !$arrDados['grupo']['text']) {
            $arrDados['grupo'] = $arrDados['grupo']['id'];
        }

        if ($arrDados['grupo'] && is_string($arrDados['grupo'])) {
            $arrDados['grupo'] = $serviceAcessoGrupo->getArrSelect2(array('id' => $arrDados['grupo']));
            $arrDados['grupo'] = $arrDados['grupo'] ? $arrDados['grupo'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['setorGrupoId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['setorId' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Protocolo\Entity\ProtocoloSetorGrupo */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getSetorGrupoId();
            $arrEntity[$params['value']] = $objEntity->getSetorId();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($setorGrupoId)
    {
        if (!$setorGrupoId) {
            $this->setLastError('Para remover um registro de setor grupo é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objProtocoloSetorGrupo \Protocolo\Entity\ProtocoloSetorGrupo */
            $objProtocoloSetorGrupo = $this->getRepository()->find($setorGrupoId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objProtocoloSetorGrupo);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de setor grupo.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceProtocoloSetor = new \Protocolo\Service\ProtocoloSetor($this->getEm());
        $serviceAcessoGrupo    = new \Acesso\Service\AcessoGrupo($this->getEm());

        $serviceProtocoloSetor->setarDependenciasView($view);
        $serviceAcessoGrupo->setarDependenciasView($view);
    }

    public function getArrSelect2UsingSetorId($setorId)
    {
        $arrObjSetorGrupo = $this->getRepository()->findBy(['setor' => $setorId]);
        $arrGrupos        = array();

        if (!empty($arrObjSetorGrupo)) {
            /** @var \Protocolo\Entity\ProtocoloSetorGrupo $objSetorGrupo */
            foreach ($arrObjSetorGrupo as $index => $objSetorGrupo) {
                $arrSetorGrupo = $objSetorGrupo->toArray();

                $arrGrupos[$index]['text'] = $arrSetorGrupo['grupoNome'];
                $arrGrupos[$index]['id']   = $arrSetorGrupo['grupoId'];
            }
        }

        return $arrGrupos;
    }
}
?>