<?php

namespace Protocolo\Service;

use VersaSpine\Service\AbstractService;

class ProtocoloMensagemPadrao extends AbstractService
{
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Protocolo\Entity\ProtocoloMensagemPadrao');
    }

    public function pesquisaForJson($params)
    {
        $sql               = '
        SELECT
        *
        FROM protocolo__mensagem_padrao
        WHERE';
        $mensagemDescricao = false;
        $mensagemId        = false;

        if ($params['q']) {
            $mensagemDescricao = $params['q'];
        } elseif ($params['query']) {
            $mensagemDescricao = $params['query'];
        }

        if ($params['mensagemId']) {
            $mensagemId = is_array($params['mensagemId']) ? $params['mensagemId'] : explode(',', $params['mensagemId']);
        }

        $parameters = array('mensagemDescricao' => "{$mensagemDescricao}%");
        $sql .= ' mensagem_descricao LIKE :mensagemDescricao';

        if ($mensagemId) {
            $parameters['mensagemId'] = explode(',', $mensagemId);
            $parameters['mensagemId'] = $mensagemId;
            $sql .= ' AND mensagem_id NOT IN(:mensagemId)';
        }

        if ($params['setorId'] && $params['solicitacaoId']) {
            $parameters['setorId']       = $params['setorId'];
            $parameters['solicitacaoId'] = $params['solicitacaoId'];
            $sql .= " AND ((setor_id = :setorId AND solicitacao_id = :solicitacaoId) OR (setor_id = :setorId AND solicitacao_id IS NULL))";
        } elseif ($params['setorId']) {
            $parameters['setorId'] = $params['setorId'];
            $sql .= " AND setor_id = :setorId";
        } elseif ($params['solicitacaoId']) {
            $parameters['solicitacaoId'] = $params['solicitacaoId'];
            $sql .= " AND solicitacao_id = :solicitacaoId";
        }

        $sql .= " ORDER BY mensagem_descricao";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceProtocoloSetor       = new \Protocolo\Service\ProtocoloSetor($this->getEm());
        $serviceProtocoloSolicitacao = new \Protocolo\Service\ProtocoloSolicitacao($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['mensagemId']) {
                /** @var $objProtocoloMensagemPadrao \Protocolo\Entity\ProtocoloMensagemPadrao */
                $objProtocoloMensagemPadrao = $this->getRepository()->find($arrDados['mensagemId']);

                if (!$objProtocoloMensagemPadrao) {
                    $this->setLastError('Registro de mensagem padrão não existe!');

                    return false;
                }
            } else {
                $objProtocoloMensagemPadrao = new \Protocolo\Entity\ProtocoloMensagemPadrao();
            }

            if ($arrDados['setor']) {
                /** @var $objProtocoloSetor \Protocolo\Entity\ProtocoloSetor */
                $objProtocoloSetor = $serviceProtocoloSetor->getRepository()->find($arrDados['setor']);

                if (!$objProtocoloSetor) {
                    $this->setLastError('Registro de setor não existe!');

                    return false;
                }

                $objProtocoloMensagemPadrao->setSetor($objProtocoloSetor);
            } else {
                $objProtocoloMensagemPadrao->setSetor(null);
            }

            if ($arrDados['solicitacao']) {
                /** @var $objProtocoloSolicitacao \Protocolo\Entity\ProtocoloSolicitacao */
                $objProtocoloSolicitacao = $serviceProtocoloSolicitacao->getRepository()->find(
                    $arrDados['solicitacao']
                );

                if (!$objProtocoloSolicitacao) {
                    $this->setLastError('Registro de solicitação não existe!');

                    return false;
                }

                $objProtocoloMensagemPadrao->setSolicitacao($objProtocoloSolicitacao);
            } else {
                $objProtocoloMensagemPadrao->setSolicitacao(null);
            }

            $objProtocoloMensagemPadrao->setMensagemDescricao($arrDados['mensagemDescricao']);
            $objProtocoloMensagemPadrao->setMensagemConteudo($arrDados['mensagemConteudo']);

            $this->getEm()->persist($objProtocoloMensagemPadrao);
            $this->getEm()->flush($objProtocoloMensagemPadrao);

            $this->getEm()->commit();

            $arrDados['mensagemId'] = $objProtocoloMensagemPadrao->getMensagemId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de mensagem padrão!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT 
                     setor_descricao,solicitacao_descricao,mensagem_id,mensagem_descricao
                     FROM protocolo__mensagem_padrao mp
                        LEFT JOIN protocolo__setor setor  ON setor.setor_id=mp.setor_id
                        LEFT JOIN protocolo__solicitacao solicitacao ON solicitacao.solicitacao_id=mp.solicitacao_id";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($mensagemId)
    {
        $arrDados = array();

        if (!$mensagemId) {
            $this->setLastError('Mensagem padrão inválido!');

            return array();
        }

        /** @var $objProtocoloMensagemPadrao \Protocolo\Entity\ProtocoloMensagemPadrao */
        $objProtocoloMensagemPadrao = $this->getRepository()->find($mensagemId);

        if (!$objProtocoloMensagemPadrao) {
            $this->setLastError('Mensagem padrão não existe!');

            return array();
        }

        $serviceProtocoloSetor       = new \Protocolo\Service\ProtocoloSetor($this->getEm());
        $serviceProtocoloSolicitacao = new \Protocolo\Service\ProtocoloSolicitacao($this->getEm());

        try {
            $arrDados = $objProtocoloMensagemPadrao->toArray();

            if ($arrDados['setor']) {
                $arrProtocoloSetor = $serviceProtocoloSetor->getArrSelect2(['id' => $arrDados['setor']]);
                $arrDados['setor'] = $arrProtocoloSetor ? $arrProtocoloSetor[0] : null;
            }

            if ($arrDados['solicitacao']) {
                $arrProtocoloSolicitacao = $serviceProtocoloSolicitacao->getArrSelect2(
                    ['id' => $arrDados['solicitacao']]
                );
                $arrDados['solicitacao'] = $arrProtocoloSolicitacao ? $arrProtocoloSolicitacao[0] : null;
            }
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceProtocoloSetor       = new \Protocolo\Service\ProtocoloSetor($this->getEm());
        $serviceProtocoloSolicitacao = new \Protocolo\Service\ProtocoloSolicitacao($this->getEm());

        if (is_array($arrDados['setor']) && !$arrDados['setor']['text']) {
            $arrDados['setor'] = $arrDados['setor']['setorId'];
        }

        if ($arrDados['setor'] && is_string($arrDados['setor'])) {
            $arrDados['setor'] = $serviceProtocoloSetor->getArrSelect2(array('id' => $arrDados['setor']));
            $arrDados['setor'] = $arrDados['setor'] ? $arrDados['setor'][0] : null;
        }

        if (is_array($arrDados['solicitacao']) && !$arrDados['solicitacao']['text']) {
            $arrDados['solicitacao'] = $arrDados['solicitacao']['solicitacaoId'];
        }

        if ($arrDados['solicitacao'] && is_string($arrDados['solicitacao'])) {
            $arrDados['solicitacao'] = $serviceProtocoloSolicitacao->getArrSelect2(
                array('id' => $arrDados['solicitacao'])
            );
            $arrDados['solicitacao'] = $arrDados['solicitacao'] ? $arrDados['solicitacao'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['mensagemId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['mensagemDescricao' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Protocolo\Entity\ProtocoloMensagemPadrao */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getMensagemId();
            $arrEntity[$params['value']] = $objEntity->getMensagemDescricao();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($mensagemId)
    {
        if (!$mensagemId) {
            $this->setLastError('Para remover um registro de mensagem padrão é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objProtocoloMensagemPadrao \Protocolo\Entity\ProtocoloMensagemPadrao */
            $objProtocoloMensagemPadrao = $this->getRepository()->find($mensagemId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objProtocoloMensagemPadrao);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de mensagem padrão.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceProtocoloSetor       = new \Protocolo\Service\ProtocoloSetor($this->getEm());
        $serviceProtocoloSolicitacao = new \Protocolo\Service\ProtocoloSolicitacao($this->getEm());

        $serviceProtocoloSetor->setarDependenciasView($view);
        $serviceProtocoloSolicitacao->setarDependenciasView($view);
    }

    public function getArrSelect()
    {
        $arrEntities = $this->getRepository()->findBy([], ['mensagemDescricao' => 'asc']);

        $arrEntitiesArr = array();

        /* @var $objEntity \Protocolo\Entity\ProtocoloMensagemPadrao */
        foreach ($arrEntities as $objEntity) {
            $arrEntitiesArr[$objEntity->getMensagemId()] = $objEntity->getMensagemDescricao();
        }

        return $arrEntitiesArr;
    }
}
?>