<?php

namespace Protocolo\Service;

use Eden\Mail\Exception;
use GerenciadorArquivos\Service\Arquivo;
use VersaSpine\Service\AbstractService;

class ProtocoloMensagem extends AbstractService
{
    const MENSAGEM_ORIGEM_SOLICITANTE = 'Solicitante';
    const MENSAGEM_ORIGEM_OPERADOR = 'Operador';
    const MENSAGEM_SOLICITANTE_VISIVEL_SIM = 'Sim';
    const MENSAGEM_SOLICITANTE_VISIVEL_NAO = 'Não';
    const MENSAGEM_ARQUIVO_DIRETORIO = 10;
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2MensagemOrigem($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getMensagemOrigem());
    }

    public static function getMensagemOrigem()
    {
        return array(self::MENSAGEM_ORIGEM_SOLICITANTE, self::MENSAGEM_ORIGEM_OPERADOR);
    }

    public function getArrSelect2MensagemSolicitanteVisivel($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getMensagemSolicitanteVisivel());
    }

    public static function getMensagemSolicitanteVisivel()
    {
        return array(self::MENSAGEM_SOLICITANTE_VISIVEL_SIM, self::MENSAGEM_SOLICITANTE_VISIVEL_NAO);
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Protocolo\Entity\ProtocoloMensagem');
    }

    public function pesquisaForJson($params)
    {
        $sql = '
        SELECT
        a.mensagemId AS mensagem_id,
        a.protocoloId AS protocolo_id,
        a.mensagemUsuarioId AS mensagem_usuario_id,
        a.mensagemData AS mensagem_data,
        a.mensagemOrigem AS mensagem_origem,
        a.mensagemSolicitanteVisivel AS mensagem_solicitante_visivel,
        a.mensagemConteudo AS mensagem_conteudo
        FROM Protocolo\Entity\ProtocoloMensagem a
        WHERE';
        $protocoloId = false;
        $mensagemId = false;

        if ($params['q']) {
            $protocoloId = $params['q'];
        } elseif ($params['query']) {
            $protocoloId = $params['query'];
        }

        if ($params['mensagemId']) {
            $mensagemId = $params['mensagemId'];
        }

        $parameters = array('protocoloId' => "{$protocoloId}%");
        $sql .= ' a.protocoloId LIKE :protocoloId';

        if ($mensagemId) {
            $parameters['mensagemId'] = explode(',', $mensagemId);
            $parameters['mensagemId'] = $mensagemId;
            $sql .= ' AND a.mensagemId NOT IN(:mensagemId)';
        }

        $sql .= " ORDER BY a.protocoloId";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    public function save(array &$arrDados, $restringirUsuario = false)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceProtocolo = new Protocolo($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            $objProtocolo = null;
            $objUsuario = $serviceAcessoPessoas->retornaUsuarioLogado();
            $objPessoa = null;
            $objAlunoCurso = null;
            $objAluno = null;
            $pesSolicitanteId = null;

            if ($arrDados['objPessoa']) {
                $objPessoa = $arrDados['objPessoa'];
            }

            if ($objUsuario && !$objPessoa) {
                $objPessoa = $objUsuario->getPes()->getPes();
            }

            if ($arrDados['mensagemId']) {
                /** @var $objProtocoloMensagem \Protocolo\Entity\ProtocoloMensagem */
                $objProtocoloMensagem = $this->getRepository()->find($arrDados['mensagemId']);

                if (!$objProtocoloMensagem) {
                    $this->setLastError('Registro de mensagem não existe!');

                    return false;
                }
            } else {
                $objProtocoloMensagem = new \Protocolo\Entity\ProtocoloMensagem();
            }

            if ($arrDados['protocolo']) {
                /** @var $objProtocolo \Protocolo\Entity\Protocolo */
                $objProtocolo = $serviceProtocolo->getRepository()->find($arrDados['protocolo']);

                if (!$objProtocolo) {
                    $this->setLastError('Registro de protocolo não existe!');

                    return false;
                }

                $objProtocoloMensagem->setProtocolo($objProtocolo);
            } else {
                $objProtocoloMensagem->setProtocolo(null);
            }

            if ($objProtocolo->getProtocoloSituacao() == $serviceProtocolo::PROTOCOLO_SITUACAO_CONCLUIDO) {
                $this->setLastError("Não é possível enviar mensagem em protocolos concluídos");

                return false;
            }

            if($restringirUsuario) {
                if ($objUsuarioCriacao = $objProtocolo->getUsuarioIdCriacao()) {
                    if ($objUsuarioCriacao->getId() != $objUsuario->getId()) {
                        $this->setLastError("Permissão negada para enviar esta mensagem!");

                        return false;
                    }
                }else{
                    $this->setLastError("Permissão negada para enviar esta mensagem!");

                    return false;
                }
            }

            /** @var $objAcessoPessoas \Acesso\Entity\AcessoPessoas */
            $objAcessoPessoas = $serviceAcessoPessoas->retornaUsuarioLogado($arrDados['mensagemUsuario']);

            $objProtocoloMensagem->setMensagemUsuario($objAcessoPessoas);

            if ($objProtocolo->getProtocoloSolicitantePes()) {
                $pesSolicitanteId = $objProtocolo->getProtocoloSolicitantePes()->getPesId();
            }

            if ($pesSolicitanteId && $objPessoa) {
                if ($objPessoa->getPesId() == $pesSolicitanteId) {
                    $objProtocoloMensagem->setMensagemOrigem("Solicitante");
                    $arrDados['mensagemSolicitanteVisivel'] = "Sim";
                } else {
                    $objProtocoloMensagem->setMensagemOrigem("Operador");
                }
            } else {
                if ($arrDados['mensagemOrigem'] == 'Solicitante') {
                    $objProtocoloMensagem->setMensagemOrigem('Solicitante');
                    $arrDados['mensagemSolicitanteVisivel'] = "Sim";
                } else {
                    $objProtocoloMensagem->setMensagemOrigem('Operador');
                }
            }

            if ($arrDados['mensagemSolicitanteVisivel'] == "Sim") {
                $objProtocoloMensagem->setMensagemSolicitanteVisivel("Sim");
            } else {
                $objProtocoloMensagem->setMensagemSolicitanteVisivel("Não");
            }

            if($arrDados['solicitanteVisivelSobrescrever']){
                $objProtocoloMensagem->setMensagemSolicitanteVisivel($arrDados['solicitanteVisivelSobrescrever']);
            }

            $objProtocoloMensagem->setMensagemData(new \DateTime());
            $objProtocoloMensagem->setMensagemConteudo($arrDados['mensagemConteudo']);

            $this->getEm()->persist($objProtocoloMensagem);
            $this->getEm()->flush($objProtocoloMensagem);

            $this->getEm()->commit();

            $arrDados['mensagemId'] = $objProtocoloMensagem->getMensagemId();

            $this->tratarAnexos($arrDados);

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de mensagem!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['protocolo']) {
            $errors[] = 'Por favor preencha o campo "protocolo"!';
        }

        if (!$arrParam['mensagemConteudo']) {
            $errors[] = 'Por favor preencha o campo correspondente a mensagem!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM protocolo__mensagem";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($mensagemId)
    {
        $arrDados = array();

        if (!$mensagemId) {
            $this->setLastError('Mensagem inválido!');

            return array();
        }

        /** @var $objProtocoloMensagem \Protocolo\Entity\ProtocoloMensagem */
        $objProtocoloMensagem = $this->getRepository()->find($mensagemId);

        if (!$objProtocoloMensagem) {
            $this->setLastError('Mensagem não existe!');

            return array();
        }

        $serviceProtocolo = new \Protocolo\Service\Protocolo($this->getEm());
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());

        try {
            $arrDados = $objProtocoloMensagem->toArray();

            if ($arrDados['protocolo']) {
                $arrProtocolo = $serviceProtocolo->getArrSelect2(['id' => $arrDados['protocolo']]);
                $arrDados['protocolo'] = $arrProtocolo ? $arrProtocolo[0] : null;
            }

            if ($arrDados['mensagemUsuario']) {
                $arrAcessoPessoas = $serviceAcessoPessoas->getArrSelect2(
                    ['id' => $arrDados['mensagemUsuario']]
                );
                $arrDados['mensagemUsuario'] = $arrAcessoPessoas ? $arrAcessoPessoas[0] : null;
            }
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceProtocolo = new \Protocolo\Service\Protocolo($this->getEm());
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());

        if (is_array($arrDados['protocolo']) && !$arrDados['protocolo']['text']) {
            $arrDados['protocolo'] = $arrDados['protocolo']['protocoloId'];
        }

        if ($arrDados['protocolo'] && is_string($arrDados['protocolo'])) {
            $arrDados['protocolo'] = $serviceProtocolo->getArrSelect2(array('id' => $arrDados['protocolo']));
            $arrDados['protocolo'] = $arrDados['protocolo'] ? $arrDados['protocolo'][0] : null;
        }

        if (is_array($arrDados['mensagemUsuario']) && !$arrDados['mensagemUsuario']['text']) {
            $arrDados['mensagemUsuario'] = $arrDados['mensagemUsuario']['id'];
        }

        if ($arrDados['mensagemUsuario'] && is_string($arrDados['mensagemUsuario'])) {
            $arrDados['mensagemUsuario'] = $serviceAcessoPessoas->getArrSelect2(
                array('id' => $arrDados['mensagemUsuario'])
            );
            $arrDados['mensagemUsuario'] = $arrDados['mensagemUsuario'] ? $arrDados['mensagemUsuario'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['mensagemId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['protocoloId' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Protocolo\Entity\ProtocoloMensagem */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']] = $objEntity->getMensagemId();
            $arrEntity[$params['value']] = $objEntity->getProtocoloId();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($mensagemId)
    {
        if (!$mensagemId) {
            $this->setLastError('Para remover um registro de mensagem é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objProtocoloMensagem \Protocolo\Entity\ProtocoloMensagem */
            $objProtocoloMensagem = $this->getRepository()->find($mensagemId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objProtocoloMensagem);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de mensagem.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceProtocolo = new \Protocolo\Service\Protocolo($this->getEm());
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());

        $serviceProtocolo->setarDependenciasView($view);
        $serviceAcessoPessoas->setarDependenciasView($view);

        $view->setVariable("arrMensagemOrigem", $this->getArrSelect2MensagemOrigem());

        $view->setVariable("arrMensagemSolicitanteVisivel", $this->getArrSelect2MensagemSolicitanteVisivel());
    }

    public function getArrMensagensPorIdProtocolo($id, $somenteVisiveisSolicitante = false)
    {
        $arrParamPesquisa = ['protocolo' => $id];

        if ($somenteVisiveisSolicitante) {
            $arrParamPesquisa['mensagemSolicitanteVisivel'] = self::MENSAGEM_SOLICITANTE_VISIVEL_SIM;
        }

        $arrObjProtocoloMensagem = $this->getRepository()->findBy($arrParamPesquisa);
        $arrMensagem = false;

        $arrAnexos = $this->getArrMensagensAnexoPorIdProtocolo($id, true);

        /** @var \Protocolo\Entity\ProtocoloMensagem $objProtocoloMensagem */
        foreach ($arrObjProtocoloMensagem as $index => $objProtocoloMensagem) {
            $data = $objProtocoloMensagem->getMensagemData()->format('d/m/Y H:i');
            $objUsuario = $objProtocoloMensagem->getMensagemUsuario();
            $login = "";

            if ($objUsuario) {
                if ($objProtocoloMensagem->getMensagemOrigem() == self::MENSAGEM_ORIGEM_SOLICITANTE) {
                    $objPessoa = $objUsuario->getPes();
                    $strNome = $objPessoa ? $objPessoa->getPes()->getPesNome() : '';

                    if (preg_match('/^([^\s]+)((?:\s.*[^\s])?\s([^\s]+).*|\s*)$/', $strNome, $arrNome)) {
                        $login = trim($arrNome[0] . ' ' . $arrNome[count($arrNome) - 1]);
                    }
                }

                if (!$login) {
                    $login = $objUsuario->getLogin();
                }
            }

            if (!$login) {
                $login = $objProtocoloMensagem->getMensagemOrigem();
            }

            $arrMensagem[] = [
                'mensagemId' => $objProtocoloMensagem->getMensagemId(),
                'mensagem' => $objProtocoloMensagem->getMensagemConteudo(),
                'visivel' => $objProtocoloMensagem->getMensagemSolicitanteVisivel(),
                'autor' => strtoupper($login),
                'origem' => $objProtocoloMensagem->getMensagemOrigem(),
                'data' => $data,
                'anexos' => $arrAnexos[$objProtocoloMensagem->getMensagemId()]
            ];
        }

        return $arrMensagem;
    }

    public function getArrMensagensAnexoPorIdProtocolo($protocoloId, $separarPorMensagemId = false)
    {
        if (!$protocoloId) {
            return false;
        }

        $query = "
              SELECT
                  protocolo__mensagem.*, arquivo.*, pessoa.pes_nome mensagem_usuario
                FROM
                  protocolo__mensagem_arquivo
                  LEFT JOIN
                  protocolo__mensagem ON protocolo__mensagem.mensagem_id = protocolo__mensagem_arquivo.mensagem_id
                  LEFT JOIN
                  arquivo ON arquivo.arq_id = protocolo__mensagem_arquivo.arq_id
                  LEFT JOIN
                  acesso_pessoas usuarioMensagem ON usuarioMensagem.id = protocolo__mensagem.mensagem_usuario_id
                  LEFT JOIN
                  pessoa ON pessoa.pes_id = coalesce(usuarioMensagem.pes_fisica,usuarioMensagem.pes_juridica)
              WHERE
                protocolo_id = :protocoloId
               ";

        $result = $this->executeQueryWithParam($query, array('protocoloId' => $protocoloId))->fetchAll();

        if ($separarPorMensagemId) {
            $arrAnexosAgrupadosPorMensagemId = array();

            foreach ($result as $dados) {
                $mensagemId = (integer)$dados['mensagem_id'];
                $arrAnexosAgrupadosPorMensagemId[$mensagemId][] = $dados;
            }

            $result = $arrAnexosAgrupadosPorMensagemId;
        }

        if (is_array($result)) {
            return $result;
        }

        return false;
    }

    /**
     * @param $arrDados array
     * @param $arquivos array
     * @return \GerenciadorArquivos\Entity\Arquivo | boolean
     */
    public function anexarArquivoNaMensagem(&$arrDados, $arquivos = array())
    {
        if (!$arrDados) {
            return false;
        }

        $arrExtensoesPermitidas = array_merge(
            ['3gp', '3gpp', 'avi', 'bmp', 'doc', 'docx', 'gif', 'htm', 'html', 'jpeg', 'jpg', 'm4a', 'mc3', 'mp4'],
            ['odg', 'odt', 'ott', 'pdf', 'png', 'pptx', 'ret', 'rtf', 'tif', 'tiff', 'txt', 'xls', 'xlsx', 'xps']
        );

        $arrArquivos = $arrDados['arrArquivo'];
        $arrObjArquivos = array();

        if (is_null($arrArquivos)) {
            $arrArquivos = $arquivos;
        }

        try {
            $serviceDiretorio = new \GerenciadorArquivos\Service\ArquivoDiretorios($this->getEm());
            $objArqDiretorio = $serviceDiretorio->getRepository()->find(self::MENSAGEM_ARQUIVO_DIRETORIO);
            $serviceArquivo = new Arquivo($this->getEm(), $objArqDiretorio);

            $query = "INSERT INTO protocolo__mensagem_arquivo (arq_id,mensagem_id) VALUES(:arqId,:mensagemId)";

            foreach ($arrArquivos as $arquivo) {
                if (is_array($arquivo)) {
                    $ext = strtolower(array_pop(explode('.', $arquivo['name'])));

                    //Verifica se a extensão do arquivo é permitida
                    if (!in_array($ext, $arrExtensoesPermitidas)) {
                        //TODO: tratar retorno de erros
                        continue;
                    }

                    /** @var $objArquivo \GerenciadorArquivos\Entity\Arquivo */
                    $objArquivo = $serviceArquivo->adicionar($arquivo);

                    $this->executeQueryWithParam(
                        $query,
                        array('arqId' => $objArquivo->getArqId(), 'mensagemId' => $arrDados['mensagemId'])
                    );

                    $arrObjArquivos[] = $objArquivo->toArray();
                }
            }

            return $arrObjArquivos;
        } catch (\Exception $e) {
            $this->setLastError('Erro ao salvar arquivo' . $e->getMessage());
        }

        return false;
    }

    public function tratarAnexos(&$arrDados)
    {
        if (!$arrDados['arrArquivo']) {
            return false;
        }

        if (is_array($arrDados['arrArquivo'])) {
            $this->anexarArquivoNaMensagem($arrDados);

            return true;
        }

        $arrAnexos = explode(PHP_EOL, $arrDados['anexos']);
        $arrArquivosDaUrl = array();

        foreach ($arrAnexos as $anexo) {
            if ($anexo) {
                $arrArquivosDaUrl[] = $this->getFileFromUrl($anexo);
            }
        }

        if ($link = $arrDados['linkParaDownload']) {
            $arrAnexosSalvos = $this->anexarArquivoNaMensagem($arrDados, $arrArquivosDaUrl);
            $arrAnexosTemp = array();

            foreach ($arrAnexosSalvos as $anexos) {
                $arrAnexosTemp[] = [
                    'Nome' => $anexos['arqNome'],
                    'Link' => $link . $anexos['arqChave']
                ];
            }

            $arrDados['anexos'] = $arrAnexosTemp;
        } else {
            $arrDados['anexos'] = $this->anexarArquivoNaMensagem($arrDados, $arrArquivosDaUrl);
        }

        return true;
    }

    public function getFileFromUrl($url)
    {
        $data = file_get_contents($url);

        if (!$data) {
            $this->setLastError("Tempo da requisição ultrapassou o limite");

            return false;
        }

        $tempFolder = tempnam(sys_get_temp_dir(), '');
        $tempHandle = fopen($tempFolder, 'r+');

        fwrite($tempHandle, $data);
        $result['tmp_name'] = stream_get_meta_data($tempHandle)['uri'];
        fclose($tempHandle);

        $result['size'] = strlen($data);
        $result['name'] = preg_replace('/.*\//', "", $url);

        return $result;
    }
}

?>