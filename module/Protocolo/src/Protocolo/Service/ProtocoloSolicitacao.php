<?php

namespace Protocolo\Service;

use VersaSpine\Service\AbstractService;
use \Protocolo\Service\ProtocoloSetor;

class ProtocoloSolicitacao extends AbstractService
{
    const SOLICITACAO_VISIBILIDADE_ALUNO = 'Aluno';
    const SOLICITACAO_VISIBILIDADE_EXTERNO = 'Externo';
    const SOLICITACAO_VISIBILIDADE_INTERNO = 'Interno';
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2SolicitacaoVisibilidade($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getSolicitacaoVisibilidade());
    }

    public static function getSolicitacaoVisibilidade()
    {
        return array(
            self::SOLICITACAO_VISIBILIDADE_ALUNO,
            self::SOLICITACAO_VISIBILIDADE_EXTERNO,
            self::SOLICITACAO_VISIBILIDADE_INTERNO
        );
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Protocolo\Entity\ProtocoloSolicitacao');
    }

    public function pesquisaForJson($params)
    {
        $service       = new \Acesso\Service\AcessoPessoas($this->getEm());
        $arrGrupo      = array(-1);

        if ($objAcessoPessoa = $service->retornaUsuarioLogado()) {
            foreach ($objAcessoPessoa->getUsuario()->getGrupo() as $objAcessoGrupo) {
                $arrGrupo[] = $objAcessoGrupo->getId();
            }
        }

        $sql                  = '
        SELECT
          a.*, pa.setor_descricao
        FROM protocolo__solicitacao a
        INNER JOIN protocolo__setor pa USING(setor_id)
        INNER JOIN protocolo__setor_grupo AS setor_grupo ON setor_grupo.setor_id = pa.setor_id
        WHERE';
        $solicitacaoDescricao = false;
        $solicitacaoId        = false;
        $setorId              = false;

        if ($params['q']) {
            $solicitacaoDescricao = $params['q'];
        } elseif ($params['query']) {
            $solicitacaoDescricao = $params['query'];
        }

        if ($params['solicitacaoId']) {
            $solicitacaoId = $params['solicitacaoId'];
        }

        if ($params['setorId']) {
            $setorId = $params['setorId'];
        }

        $parameters = array('solicitacao_descricao' => "{$solicitacaoDescricao}%");
        $sql .= ' a.solicitacao_descricao LIKE :solicitacao_descricao';

        if ($solicitacaoId) {
            $parameters['solicitacao_id'] = explode(',', $solicitacaoId);
            $parameters['solicitacao_id'] = $solicitacaoId;
            $sql .= ' AND a.solicitacao_id NOT IN(:solicitacao_id)';
        }

        if ($setorId) {
            $setorId = preg_replace('/[^0-9,]/', '', $setorId);
            $sql .= ' AND pa.setor_id in (' . $setorId . ')';
        }

        if ($arrGrupo) {
            $sql .= ' AND setor_grupo.grupo_id IN(' . implode(',', $arrGrupo) . ')';
        }

        $sql .= " GROUP BY a.solicitacao_id";
        $sql .= " ORDER BY a.solicitacao_descricao";

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceProtocoloSetor = new \Protocolo\Service\ProtocoloSetor($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['solicitacaoId']) {
                /** @var $objProtocoloSolicitacao \Protocolo\Entity\ProtocoloSolicitacao */
                $objProtocoloSolicitacao = $this->getRepository()->find($arrDados['solicitacaoId']);

                if (!$objProtocoloSolicitacao) {
                    $this->setLastError('Registro de solicitação não existe!');

                    return false;
                }
            } else {
                $objProtocoloSolicitacao = new \Protocolo\Entity\ProtocoloSolicitacao();
            }

            if ($arrDados['setor']) {
                /** @var $objProtocoloSetor \Protocolo\Entity\ProtocoloSetor */
                $objProtocoloSetor = $serviceProtocoloSetor->getRepository()->find($arrDados['setor']);

                if (!$objProtocoloSetor) {
                    $this->setLastError('Registro de setor não existe!');

                    return false;
                }

                $objProtocoloSolicitacao->setSetor($objProtocoloSetor);
            } else {
                $objProtocoloSolicitacao->setSetor(null);
            }

            if (is_array($arrDados['solicitacaoVisibilidade'])) {
                $arrDados['solicitacaoVisibilidade'] = implode(',', $arrDados['solicitacaoVisibilidade']);
            }

            $objProtocoloSolicitacao->setSolicitacaoDescricao($arrDados['solicitacaoDescricao']);
            $objProtocoloSolicitacao->setSolicitacaoVisibilidade($arrDados['solicitacaoVisibilidade']);

            $this->getEm()->persist($objProtocoloSolicitacao);
            $this->getEm()->flush($objProtocoloSolicitacao);

            $this->getEm()->commit();

            $arrDados['solicitacaoId'] = $objProtocoloSolicitacao->getSolicitacaoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de solicitação!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['setor']) {
            $errors[] = 'Por favor preencha o campo "código setor"!';
        }

        if (!$arrParam['solicitacaoDescricao']) {
            $errors[] = 'Por favor preencha o campo "descrição"!';
        }

        if (!$arrParam['solicitacaoVisibilidade']) {
            $errors[] = 'Por favor preencha o campo "visibilidade"!';
        }

        if (array_diff($arrParam['solicitacaoVisibilidade'], self::getSolicitacaoVisibilidade())) {
            $errors[] = 'Por favor selecione valores válidos para o campo "visibilidade"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT solicitacao.*, setor.setor_descricao
        FROM protocolo__solicitacao AS solicitacao
        INNER JOIN protocolo__setor AS setor ON
        setor.setor_id = solicitacao.setor_id";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($solicitacaoId)
    {
        $arrDados = array();

        if (!$solicitacaoId) {
            $this->setLastError('Solicitação inválido!');

            return array();
        }

        /** @var $objProtocoloSolicitacao \Protocolo\Entity\ProtocoloSolicitacao */
        $objProtocoloSolicitacao = $this->getRepository()->find($solicitacaoId);

        if (!$objProtocoloSolicitacao) {
            $this->setLastError('Solicitação não existe!');

            return array();
        }

        $serviceProtocoloSetor = new \Protocolo\Service\ProtocoloSetor($this->getEm());

        try {
            $arrDados = $objProtocoloSolicitacao->toArray();

            if ($arrDados['setor']) {
                $arrProtocoloSetor = $serviceProtocoloSetor->getArrSelect2(['id' => $arrDados['setor']]);
                $arrDados['setor'] = $arrProtocoloSetor ? $arrProtocoloSetor[0] : null;
            }
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());

            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceProtocoloSetor = new \Protocolo\Service\ProtocoloSetor($this->getEm());

        if (is_array($arrDados['setor']) && !$arrDados['setor']['text']) {
            $arrDados['setor'] = $arrDados['setor']['setorId'];
        }

        if ($arrDados['setor'] && is_string($arrDados['setor'])) {
            $arrDados['setor'] = $serviceProtocoloSetor->getArrSelect2(array('id' => $arrDados['setor']));
            $arrDados['setor'] = $arrDados['setor'] ? $arrDados['setor'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['solicitacaoId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['solicitacaoDescricao' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Protocolo\Entity\ProtocoloSolicitacao */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getSolicitacaoId();
            $arrEntity[$params['value']] = $objEntity->getSolicitacaoDescricao();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($solicitacaoId)
    {
        if (!$solicitacaoId) {
            $this->setLastError('Para remover um registro de solicitação é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objProtocoloSolicitacao \Protocolo\Entity\ProtocoloSolicitacao */
            $objProtocoloSolicitacao = $this->getRepository()->find($solicitacaoId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objProtocoloSolicitacao);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de solicitação.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceProtocoloSetor = new \Protocolo\Service\ProtocoloSetor($this->getEm());

        $serviceProtocoloSetor->setarDependenciasView($view);

        $view->setVariable("arrSolicitacaoVisibilidade", $this->getArrSelect2SolicitacaoVisibilidade());
    }

    public function retornaArraySolicitacaoComSetor()
    {
        $query = "
            SELECT
                *
            FROM
                protocolo__solicitacao
            LEFT JOIN
                protocolo__setor USING (setor_id)
            WHERE
              (
                  find_in_set('Aluno', solicitacao_visibilidade) OR
                  find_in_set('Externo', solicitacao_visibilidade)
              ) AND
              (
                find_in_set('Aluno', setor_visibilidade) OR
                find_in_set('Externo', setor_visibilidade)
              )
            ORDER BY
                setor_descricao
        ";

        $arrDados = $this->executeQueryWithParam($query)->fetchAll();

        if ($arrDados) {
            $array            = array();
            $arraySolicitacao = array();

            foreach ($arrDados as $index => $dados) {
                $setorId = trim($dados['setor_id'], 0);

                $array[$setorId] = [
                    'setor_id'           => $dados['setor_id'],
                    'setor_descricao'    => $dados['setor_descricao'],
                    'setor_visibilidade' => $dados['setor_visibilidade']
                ];

                $arraySolicitacao[$setorId][] = [
                    'solicitacao_id'           => $dados['solicitacao_id'],
                    'solicitacao_descricao'    => $dados['solicitacao_descricao'],
                    'solicitacao_visibilidade' => $dados['solicitacao_visibilidade']
                ];

                $array[$setorId]['solicitacao'] = $arraySolicitacao[$setorId];
            }

            $arrDados = $array;
        }

        return $arrDados;
    }
}
?>
