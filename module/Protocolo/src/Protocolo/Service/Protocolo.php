<?php

namespace Protocolo\Service;

use VersaSpine\Service\AbstractService;

class Protocolo extends AbstractService
{
    const PROTOCOLO_SOLICITANTE_VISIVEL_SIM = 'Sim';
    const PROTOCOLO_SOLICITANTE_VISIVEL_NAO = 'Não';
    const PROTOCOLO_SITUACAO_ABERTO         = 'Aberto';
    const PROTOCOLO_SITUACAO_EM_ANDAMENTO   = 'Em Andamento';
    const PROTOCOLO_SITUACAO_CONCLUIDO      = 'Concluído';
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2ProtocoloSolicitanteVisivel($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getProtocoloSolicitanteVisivel());
    }

    public static function getProtocoloSolicitanteVisivel()
    {
        return array(self::PROTOCOLO_SOLICITANTE_VISIVEL_SIM, self::PROTOCOLO_SOLICITANTE_VISIVEL_NAO);
    }

    public function getArrSelect2ProtocoloSituacao($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getProtocoloSituacao());
    }

    public static function getProtocoloSituacao()
    {
        return array(
            self::PROTOCOLO_SITUACAO_ABERTO,
            self::PROTOCOLO_SITUACAO_EM_ANDAMENTO,
            self::PROTOCOLO_SITUACAO_CONCLUIDO
        );
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Protocolo\Entity\Protocolo');
    }

    public function pesquisaForJson($params)
    {
        $sql                      = '
        SELECT
        a.protocoloId AS protocolo_id,
        a.solicitacaoId AS solicitacao_id,
        a.usuarioIdCriacao AS usuario_id_criacao,
        a.usuarioIdResponsavel AS usuario_id_responsavel,
        a.protocoloSolicitantePesId AS protocolo_solicitante_pes_id,
        a.protocoloSolicitantealunocursoId AS protocolo_solicitante_alunocurso_id,
        a.protocoloSolicitanteNome AS protocolo_solicitante_nome,
        a.protocoloSolicitanteEmail AS protocolo_solicitante_email,
        a.protocoloSolicitanteTelefone AS protocolo_solicitante_telefone,
        a.protocoloSolicitanteVisivel AS protocolo_solicitante_visivel,
        a.protocoloAssunto AS protocolo_assunto,
        a.protocoloDataCadastro AS protocolo_data_cadastro,
        a.protocoloDataAlteracao AS protocolo_data_alteracao,
        a.protocoloAvaliacao AS protocolo_avaliacao,
        a.protocoloSituacao AS protocolo_situacao
        FROM Protocolo\Entity\Protocolo a
        WHERE';
        $protocoloSolicitanteNome = false;
        $protocoloId              = false;

        if ($params['q']) {
            $protocoloSolicitanteNome = $params['q'];
        } elseif ($params['query']) {
            $protocoloSolicitanteNome = $params['query'];
        }

        if ($params['protocoloId']) {
            $protocoloId = $params['protocoloId'];
        }

        $parameters = array('protocoloSolicitanteNome' => "{$protocoloSolicitanteNome}%");
        $sql .= ' a.protocoloSolicitanteNome LIKE :protocoloSolicitanteNome';

        if ($protocoloId) {
            $parameters['protocoloId'] = explode(',', $protocoloId);
            $parameters['protocoloId'] = $protocoloId;
            $sql .= ' AND a.protocoloId NOT IN(:protocoloId)';
        }

        $sql .= " ORDER BY a.protocoloSolicitanteNome";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    public function pesquisaSolicitantesJson($params)
    {
        $protocoloSolicitante = false;

        if ($params['q']) {
            $protocoloSolicitante = $params['q'];
        } elseif ($params['query']) {
            $protocoloSolicitante = $params['query'];
        }

        $parameters = array('protocoloSolicitante' => "{$protocoloSolicitante}%");

        $sql = '';

        if ($params['solicitante'] == 'externo') {
            $sql = '
            SELECT
            protocolo_solicitante_nome AS id,
            protocolo_solicitante_nome AS protocolo_solicitante
            FROM protocolo AS a
            GROUP BY protocolo_solicitante_nome';
        }

        $sql = 'SELECT * FROM (
          ' . $sql . '
        ) solicitantes
        WHERE protocolo_solicitante LIKE :protocoloSolicitante
        ORDER BY protocolo_solicitante
        LIMIT 0,40';

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceProtocoloSolicitacao = new \Protocolo\Service\ProtocoloSolicitacao($this->getEm());
        $serviceProtocoloMensagem    = new \Protocolo\Service\ProtocoloMensagem($this->getEm());
        $serviceAcessoPessoas        = new \Acesso\Service\AcessoPessoas($this->getEm());
        $servicePessoa               = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceAcadgeralAlunoCurso  = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());

        $objPessoaAva     = null;
        $objAlunoCursoAva = null;
        $objAlunoAva      = null;

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['protocoloId']) {
                /** @var $objProtocolo \Protocolo\Entity\Protocolo */
                $objProtocolo = $this->getRepository()->find($arrDados['protocoloId']);

                if (!$objProtocolo) {
                    $this->setLastError('Registro de protocolo não existe!');

                    return false;
                }
            } else {
                $objProtocolo = new \Protocolo\Entity\Protocolo();
                $objProtocolo->setProtocoloDataCadastro(new \DateTime());
            }

            if ($arrDados['codigoAluno'] && $arrDados['integracaoId']) {
                $serviceAlunoIntegracao = new \Sistema\Service\SisIntegracaoAluno($this->getEm());

                $arrAlunoIntegracao = $serviceAlunoIntegracao->retornaAlunoCursoIdIntegracao(
                    $arrDados['integracaoId'],
                    $arrDados['codigoAluno'],
                    $arrDados['codigoCurso']
                );

                $arrDados['protocoloSolicitanteAlunocurso'] = (
                $arrAlunoIntegracao['alunocurso_id'] ? $arrAlunoIntegracao['alunocurso_id'] : ''
                );

                $arrDados['protocoloSolicitantePes'] = (
                $arrAlunoIntegracao['pes_id'] ? $arrAlunoIntegracao['pes_id'] : ''
                );
            }

            if ($arrDados['solicitacao']) {
                /** @var $objProtocoloSolicitacao \Protocolo\Entity\ProtocoloSolicitacao */
                $objProtocoloSolicitacao = $serviceProtocoloSolicitacao->getRepository()->find(
                    $arrDados['solicitacao']
                );

                if (!$objProtocoloSolicitacao) {
                    $this->setLastError('Registro de solicitação não existe!');

                    return false;
                }

                $objProtocolo->setSolicitacao($objProtocoloSolicitacao);
            } else {
                $objProtocolo->setSolicitacao(null);
            }

            if ($arrDados['usuarioIdResponsavel']) {
                /** @var $objAcessoPessoas \Acesso\Entity\AcessoPessoas */
                $objAcessoPessoas = $serviceAcessoPessoas->getRepository()->find($arrDados['usuarioIdResponsavel']);

                if (!$objAcessoPessoas) {
                    $this->setLastError('Registro de pessoas não existe!');

                    return false;
                }

                $objProtocolo->setUsuarioIdResponsavel($objAcessoPessoas);
            } else {
                $objProtocolo->setUsuarioIdResponsavel(null);
            }

            if ($arrDados['protocoloSolicitantePes']) {
                /** @var $objPessoa \Pessoa\Entity\Pessoa */
                $objPessoa = $servicePessoa->getRepository()->find($arrDados['protocoloSolicitantePes']);

                if (!$objPessoa) {
                    $this->setLastError('Registro de pessoa não existe!');

                    return false;
                }

                $objProtocolo->setProtocoloSolicitantePes($objPessoa);
            } elseif ($arrDados['filtrarSolicitante']) {
                $objAcessoPessoa = $serviceAcessoPessoas->retornaUsuarioLogado();
                $objPessoa       = $objAcessoPessoa->getPes()->getPes();

                $objProtocolo->setProtocoloSolicitantePes($objPessoa);
            } else {
                $objProtocolo->setProtocoloSolicitantePes($objPessoaAva);
            }

            if ($arrDados['protocoloSolicitanteAlunocurso']) {
                /** @var $objAcadgeralAlunoCurso \Matricula\Entity\AcadgeralAlunoCurso */
                $objAcadgeralAlunoCurso = $serviceAcadgeralAlunoCurso->getRepository()->find(
                    $arrDados['protocoloSolicitanteAlunocurso']
                );

                if (!$objAcadgeralAlunoCurso) {
                    $this->setLastError('Registro de aluno curso não existe!');

                    return false;
                }

                $objProtocolo->setProtocoloSolicitanteAlunocurso($objAcadgeralAlunoCurso);
            } else {
                $objProtocolo->setProtocoloSolicitanteAlunocurso($objAlunoCursoAva);
            }

            if ($arrDados['protocoloAvaliacao']) {
                $objProtocolo->setProtocoloAvaliacao($arrDados['protocoloAvaliacao']);
            }

            if ($arrDados['protocoloSolicitanteVisivel']) {
                $objProtocolo->setProtocoloSolicitanteVisivel($arrDados['protocoloSolicitanteVisivel']);
            } else {
                $objProtocolo->setProtocoloSolicitanteVisivel('Sim');
            }

            /** @var $objAcessoPessoas \Acesso\Entity\AcessoPessoas */
            $objAcessoPessoas = $serviceAcessoPessoas->retornaUsuarioLogado($arrDados['usuarioIdCriacao']);
            $objProtocolo->setUsuarioIdCriacao($objAcessoPessoas);
            $objProtocolo->setProtocoloDataAlteracao(new \DateTime());
            $objProtocolo->setProtocoloSolicitanteNome($arrDados['protocoloSolicitanteNome']);
            $objProtocolo->setProtocoloSolicitanteEmail($arrDados['protocoloSolicitanteEmail']);
            $objProtocolo->setProtocoloSolicitanteTelefone($arrDados['protocoloSolicitanteTelefone']);
            $objProtocolo->setProtocoloAssunto($arrDados['protocoloAssunto']);

            if ($arrDados['protocoloSituacao']) {
                $objProtocolo->setProtocoloSituacao($arrDados['protocoloSituacao']);
            } else {
                $objProtocolo->setProtocoloSituacao('Aberto');
            }

            $this->getEm()->persist($objProtocolo);
            $this->getEm()->flush($objProtocolo);

            $arrDados['protocoloId'] = $objProtocolo->getProtocoloId();

            if ($arrDados['protocoloMensagem']) {
                if ($objPessoa = $objProtocolo->getProtocoloSolicitantePes()) {
                    $arrDados['objPessoa'] = $objPessoa;
                }

                $arrDados['protocolo']        = $arrDados['protocoloId'];
                $arrDados['mensagemConteudo'] = $arrDados['protocoloMensagem'];
                $serviceProtocoloMensagem->save($arrDados);
            }

            $this->getEm()->commit();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de protocolo!<br>' . $e->getMessage());
        }

        return false;
    }

    public function transferirProtocolo(array &$arrDados)
    {
        $serviceProtocoloMensagem    = new \Protocolo\Service\ProtocoloMensagem($this->getEm());
        $serviceProtocoloSolicitacao = new \Protocolo\Service\ProtocoloSolicitacao($this->getEm());
        $serviceAcessoPessoas        = new \Acesso\Service\AcessoPessoas($this->getEm());

        if (!$arrDados['protocoloId']) {
            $this->setLastError('Registro de protocolo não existe!');

            return false;
        }

        /** @var $objProtocolo \Protocolo\Entity\Protocolo */
        $objProtocolo = $this->getRepository()->find($arrDados['protocoloId']);

        if (!$objProtocolo) {
            $this->setLastError('Registro de protocolo não existe!');

            return false;
        }

        if ($objProtocolo->getProtocoloSituacao() == self::PROTOCOLO_SITUACAO_CONCLUIDO) {
            $this->setLastError('Protocolo encontra-se concluído!');

            return false;
        }

        if (!$arrDados['solicitacao']) {
            $this->setLastError('Registro de solicitação não existe!');

            return false;
        }

        /** @var $objProtocoloSolicitacao \Protocolo\Entity\ProtocoloSolicitacao */
        $objProtocoloSolicitacao = $serviceProtocoloSolicitacao->getRepository()->find(
            $arrDados['solicitacao']
        );

        if (!$objProtocoloSolicitacao) {
            $this->setLastError('Registro de solicitação não existe!');

            return false;
        }

        if ($objProtocoloSolicitacao == $objProtocolo->getSolicitacao()) {
            $this->setLastError('Selecione uma solicitação diferente da registrada no protocolo!');

            return false;
        }

        try {
            $this->getEm()->beginTransaction();
            $objAcessoPessoas = $serviceAcessoPessoas->retornaUsuarioLogado();

            $arrDadosMensagem = array(
                'protocolo'                  => $objProtocolo->getProtocoloId(),
                'mensagemUsuario'            => $objAcessoPessoas,
                'mensagemData'               => new \DateTime(),
                'mensagemSolicitanteVisivel' => (
                $serviceProtocoloMensagem::MENSAGEM_SOLICITANTE_VISIVEL_NAO
                ),
                'mensagemConteudo'           => (
                    'Transferido do setor ' .
                    $objProtocolo->getSolicitacao()->getSetor()->getSetorDescricao() .
                    ' para setor ' .
                    $objProtocoloSolicitacao->getSetor()->getSetorDescricao() .
                    ($objAcessoPessoas ? ' pelo operador ' . $objAcessoPessoas->getLogin() : '')
                )
            );

            if (!$serviceProtocoloMensagem->save($arrDadosMensagem)) {
                $this->setLastError(
                    'Falha ao registrar mensagem de transferência!' .
                    $serviceProtocoloMensagem->getLastError()
                );

                return false;
            }

            $objProtocolo->setProtocoloDataAlteracao(new \DateTime());
            $objProtocolo->setSolicitacao($objProtocoloSolicitacao);

            $this->getEm()->persist($objProtocolo);
            $this->getEm()->flush($objProtocolo);

            $this->getEm()->commit();

            $arrDados['protocoloId'] = $objProtocolo->getProtocoloId();

            return $objProtocolo->toArray();
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível transferir o protocolo!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['solicitacao']) {
            $errors[] = 'Por favor preencha o campo "código solicitação"!';
        }

        if (!$arrParam['protocoloMensagem']) {
            $errors[] = 'Por favor, escreva alguma mensagem!';
        }

        if (!$arrParam['protocoloAssunto']) {
            $errors[] = 'Por favor preencha o campo "assunto"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $serviceAcessoPessoas = new \Acesso\Service\AcessoPessoas($this->getEm());
        $usuarioLogado        = $serviceAcessoPessoas->retornaUsuarioLogado();
        $usuarioLogado        = $usuarioLogado ? $usuarioLogado->getUsuario()->getId() : '';

        $serviceSetor = new \Protocolo\Service\ProtocoloSetor($this->getEm());
        $arrSetor     = $serviceSetor->retornarSetoresUsuarios(true);

        $conditions = '';

        if ($data['filter']['situacao']) {
            $situacoes = explode(',', $data['filter']['situacao']);

            $data['filter']['situacao'] = '';

            foreach ($situacoes as $situacao) {
                $data['filter']['situacao'] .= "'" . $situacao . "',";
            }

            $data['filter']['situacao'] = trim($data['filter']['situacao'], ',');

            $conditions .= "AND a.protocolo_situacao IN({$data['filter']['situacao']})";
        }

        if ($data['filter']['setor']) {
            $conditions .= " AND setor.setor_id IN({$data['filter']['setor'] })";
        }

        if ($data['filter']['solicitacao']) {
            $conditions .= " AND solicitacao.solicitacao_id IN({$data['filter']['solicitacao'] } )";
        }

        if ($data['filter']['protocoloSolicitanteAluno']) {
            $conditions .= " AND  alunoCurso.aluno_id  IN({$data['filter']['protocoloSolicitanteAluno'] })";
        }

        if ($data['filter']['protocoloSolicitanteInterno']) {
            $conditions .= " AND  a.protocolo_solicitante_pes_id  IN({$data['filter']['protocoloSolicitanteInterno'] })   ";
        }

        if ($data['filter']['protocoloSolicitanteOutros']) {
            $conditions .= " AND a.protocolo_solicitante_nome LIKE ('{$data['filter']['protocoloSolicitanteOutros'] }%')  ";
        }

        if ($data['filter']['responsavel']) {
            $conditions .= " AND a.usuario_id_responsavel IN({$data['filter']['responsavel'] })";
        }

        if ($data['filter']['operadoresCadastro']) {
            $conditions .= " AND a.usuario_id_criacao IN({$data['filter']['operadoresCadastro'] })";
        }

        if ($data['filter']['operadoresAlteracao']) {
            $conditions .= " AND a.usuario_id_alteracao IN({$data['filter']['operadoresAlteracao'] })";
        }

        if ($data['filter']['dataCadastroInicial']) {
            $conditions .= " AND date(a.protocolo_data_cadastro) >= " .
                "'" . self::formatDateAmericano($data['filter']['dataCadastroInicial']) . "'";
        }

        if ($data['filter']['dataCadastroFinal']) {
            $conditions .= " AND date(a.protocolo_data_cadastro) <= " .
                "'" . self::formatDateAmericano($data['filter']['dataCadastroFinal']) . "'";
        }

        if ($data['filter']['dataAlteracaoInicial']) {
            $conditions .= " AND date(a.protocolo_data_alteracao) >= " .
                "'" . self::formatDateAmericano($data['filter']['dataAlteracaoInicial']) . "'";
        }

        if ($data['filter']['dataAlteracaoFinal']) {
            $conditions .= " AND date(a.protocolo_data_alteracao) <= " .
                "'" . self::formatDateAmericano($data['filter']['dataAlteracaoFinal']) . "'";
        }

        if ($data['protocoloSolicitante']) {
            if (is_numeric($data['protocoloSolicitante'])) {
                $conditions .= " AND protocolo_solicitante_pes_id in (" . $data['protocoloSolicitante'] . ")";
            } else {
                $conditions .= " AND protocolo_solicitante_nome like '" . $data['protocoloSolicitante'] . "'";
            }
        }

        if ($data['filter']['filtrarPorUsuarioLogado']) {
            $usuarioLogadoPesId = $serviceAcessoPessoas->retornaUsuarioLogado();
            $usuarioLogadoPesId = $usuarioLogadoPesId ? $usuarioLogadoPesId->getPes()->getPes()->getPesId() : '-1';

            $conditions .= " AND
            usuario_id_responsavel in ('" . $usuarioLogado . "')
            OR
            protocolo_solicitante_pes_id in ('" . $usuarioLogadoPesId . "')
            OR
            pes_id_agente in ('" . $usuarioLogadoPesId . "')
            OR
            pes_id_agenciador in ('" . $usuarioLogadoPesId . "')
            ";
            $conditions .= " AND protocolo_solicitante_visivel like 'Sim'";
        }

        $query = "
        SELECT
        a.*,
        TRIM(LEADING 0 FROM a.protocolo_id) as protocoloIdFormatado,
        setor.setor_id,
        setor.setor_descricao,
        solicitacao.solicitacao_descricao,
        responsavel.login login_responsavel,
        pessoaResponsavel.pes_nome nome_responsavel,
        solicitantePesFisica.pes_cpf,
        TRIM(LEADING 0 FROM a.protocolo_solicitante_alunocurso_id) alunocurso_id,
        COALESCE(pessoa.pes_nome, a.protocolo_solicitante_nome) protocolo_solicitante
        FROM protocolo AS a
        INNER JOIN protocolo__solicitacao AS solicitacao ON solicitacao.solicitacao_id=a.solicitacao_id
        INNER JOIN protocolo__setor AS setor ON setor.setor_id=solicitacao.setor_id
        LEFT JOIN acesso_pessoas responsavel ON responsavel.id=a.usuario_id_responsavel
        LEFT JOIN pessoa as pessoaResponsavel on pessoaResponsavel.pes_id = coalesce(responsavel.pes_fisica,responsavel.pes_juridica)
        LEFT JOIN pessoa AS pessoa ON pessoa.pes_id=a.protocolo_solicitante_pes_id
        LEFT JOIN pessoa_fisica AS solicitantePesFisica ON pessoa.pes_id= solicitantePesFisica.pes_id
        LEFT JOIN acadgeral__aluno_curso AS alunoCurso ON alunoCurso.alunocurso_id=a.protocolo_solicitante_alunocurso_id
        WHERE 1=1 {$conditions}";

        if ($arrSetor) {
            $query .= ' AND (setor.setor_id IN(' . implode(',', $arrSetor) . '))';
        }

        if ($data['search']['value']) {
            $busca = $data['search']['value'];

            $query .= '
             AND (
                 a.protocolo_id = "' . $busca . '" OR
                 protocolo_assunto like "%' . $busca . '%" OR
                 a.protocolo_solicitante_alunocurso_id like "%' . $busca . '%" OR
                 setor_descricao like "%' . $busca . '%" OR
                 solicitacao_descricao like "%' . $busca . '%" OR
                 protocolo_solicitante_alunocurso_id = "' . $busca . '" OR
                 COALESCE(pessoa.pes_nome, a.protocolo_solicitante_nome) LIKE "%' . $busca . '%"
             )';
        }

        $query .= " GROUP BY a.protocolo_id";
        $result["recordsFiltered"] = $this->executeQueryWithParam($query)->rowCount();

        $orderBy = array();

        foreach ($data['order'] as $order) {
            $orderColumn    = $order["column"];
            $orderColumnDir = $order["dir"];
            $columnName     = $data['columns'][$orderColumn]["name"];

            $orderBy[] = "`{$columnName}` $orderColumnDir";
        }

        $orderBy = implode(" , ", $orderBy);

        if (!empty($orderBy)) {
            $orderBy = " ORDER BY " . $orderBy;
        }

        $query .= $orderBy;

        if ($data['length'] > 0) {
            $query .= " LIMIT {$data['length']} OFFSET {$data['start']}";
        }

        $result["data"]            = $this->executeQuery($query)->fetchAll();
        $result['usuarioLogadoId'] = $usuarioLogado;

        return $result;
    }

    public function getDataForDatatablesByUserLogged($data)
    {
        $service = new \Acesso\Service\AcessoPessoas($this->getEm());

        $usuarioLogado      = $service->retornaUsuarioLogado()->getUsuario();
        $usuarioLogadoPesId = $service->retornaUsuarioLogado()->getPes()->getPes()->getPesId();

        $service  = new \Protocolo\Service\ProtocoloSetor($this->getEm());
        $arrSetor = $service->retornarSetoresUsuarios();

        $usuarioLogado = $usuarioLogado->getId();

        $query = "
            SELECT
            a.*,
            TRIM(LEADING 0 FROM a.protocolo_id) AS protocoloIdFormatado,
            setor.setor_id,
            setor.setor_descricao,
            solicitacao.solicitacao_descricao,
            responsavel.login login_responsavel,            
            pessoaResponsavel.pes_nome nome_responsavel,
            solicitantePesFisica.pes_cpf,
            usuarioCriador.login AS usuario_criacao,
            usuarioAlteracao.login AS usuario_alteracao,
            TRIM(LEADING 0 FROM alunoCurso.alunocurso_id) alunocurso_id,
            COALESCE(pessoa.pes_nome, a.protocolo_solicitante_nome) protocolo_solicitante
            FROM protocolo AS a
            INNER JOIN protocolo__solicitacao AS solicitacao
                ON solicitacao.solicitacao_id=a.solicitacao_id
            LEFT JOIN protocolo__setor AS setor
                ON setor.setor_id=solicitacao.setor_id
            LEFT JOIN acesso_pessoas responsavel
                ON responsavel.id=a.usuario_id_responsavel                
            LEFT JOIN acesso_pessoas usuarioCriador
                ON usuarioCriador.id=a.usuario_id_criacao                
            LEFT JOIN acesso_pessoas usuarioAlteracao
                ON usuarioCriador.id=a.usuario_id_alteracao
            LEFT JOIN pessoa AS pessoaResponsavel
                ON pessoaResponsavel.pes_id = coalesce(responsavel.pes_fisica,responsavel.pes_juridica)
            LEFT JOIN pessoa AS pessoa
                ON pessoa.pes_id=a.protocolo_solicitante_pes_id
            LEFT JOIN pessoa_fisica AS solicitantePesFisica
                ON pessoa.pes_id= solicitantePesFisica.pes_id
            LEFT JOIN acadgeral__aluno_curso AS alunoCurso
                ON alunoCurso.alunocurso_id=a.protocolo_solicitante_alunocurso_id
          LEFT JOIN acadgeral__aluno AS aluno USING(aluno_id)
            WHERE
            (
            usuario_id_responsavel IN ('" . $usuarioLogado . "')
            OR
            protocolo_solicitante_pes_id IN ('" . $usuarioLogadoPesId . "')
            OR
            pes_id_agente IN ('" . $usuarioLogadoPesId . "')
            OR
            pes_id_agenciador IN ('" . $usuarioLogadoPesId . "')
            )
            AND protocolo_solicitante_visivel LIKE 'Sim'  ";

        if ($arrSetor) {
            $query .= ' AND (setor.setor_id IN(' . implode(',', $arrSetor) . '))';
        }

        $params = array();
        $result = array(
            "draw"         => $data['draw'],
            "recordsTotal" => $this->executeQuery($query . ' GROUP BY protocolo_id')->rowCount(),
        );

        $vSearch = $data["search"]['value'];

        if (!empty($vSearch)) {
            $query .= '
            AND(
                a.protocolo_assunto like :vSearch OR
                a.solicitacao_id like :vSearch OR
                solicitacao.setor_id like :vSearch OR
                usuario_id_responsavel    like :vSearch OR
                a.usuario_id_criacao like :vSearch OR
                pessoa.pes_nome like :vSearch OR
                alunocurso_situacao like :vSearch OR
                alunoCurso.alunocurso_id like :vSearch OR
                alunoCurso.aluno_id like :vSearch or
                solicitantePesFisica.pes_cpf like :vSearch or
                solicitantePesFisica.pes_rg like :vSearch or        
                a.protocolo_solicitante_email like :vSearch or
                a.protocolo_solicitante_nome like :vSearch or
                a.protocolo_solicitante_alunocurso_id like :vSearch or
                solicitacao.solicitacao_descricao like :vSearch or
                responsavel.login like :vSearch or
                pessoaResponsavel.pes_nome like :vSearch or
                solicitantePesFisica.pes_sobrenome like :vSearch or
                a.protocolo_solicitante_telefone like :vSearch or
                alunoCurso.cursocampus_id like :vSearch
                )';

            $params['vSearch'] = "%$vSearch%";
        }

        $result["recordsFiltered"] = $this->executeQueryWithParam($query . ' GROUP BY protocolo_id', $params)->rowCount(
        );

        $orderBy = array();

        foreach ($data['order'] as $order) {
            $orderColumn    = $order["column"];
            $orderColumnDir = $order["dir"];
            $columnName     = $data['columns'][$orderColumn]["name"];

            $orderBy[] = "`{$columnName}` $orderColumnDir";
        }

        $query .= 'GROUP BY protocolo_id';

        $orderBy = implode(" , ", $orderBy);

        if (!empty($orderBy)) {
            $orderBy = " ORDER BY " . $orderBy;
        }

        $query .= $orderBy;

        if ($data['length'] > 0) {
            $query .= " LIMIT {$data['length']} OFFSET {$data['start']}";
        }

        $result["data"] = $this->executeQueryWithParam($query, $params)->fetchAll();

        return $result;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceProtocoloSolicitacao = new \Protocolo\Service\ProtocoloSolicitacao($this->getEm());
        $serviceAcessoPessoas        = new \Acesso\Service\AcessoPessoas($this->getEm());
        $servicePessoa               = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceAcadgeralAlunoCurso  = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());

        if (is_array($arrDados['solicitacao']) && !$arrDados['solicitacao']['text']) {
            $arrDados['solicitacao'] = $arrDados['solicitacao']['solicitacaoId'];
        }

        if ($arrDados['solicitacao'] && is_string($arrDados['solicitacao'])) {
            $arrDados['solicitacao'] = $serviceProtocoloSolicitacao->getArrSelect2(
                array('id' => $arrDados['solicitacao'])
            );
            $arrDados['solicitacao'] = $arrDados['solicitacao'] ? $arrDados['solicitacao'][0] : null;
        }

        if (is_array($arrDados['usuarioIdCriacao']) && !$arrDados['usuarioIdCriacao']['text']) {
            $arrDados['usuarioIdCriacao'] = $arrDados['usuarioIdCriacao']['id'];
        }

        if ($arrDados['usuarioIdCriacao'] && is_string($arrDados['usuarioIdCriacao'])) {
            $arrDados['usuarioIdCriacao'] = $serviceAcessoPessoas->getArrSelect2(
                array('id' => $arrDados['usuarioIdCriacao'])
            );
            $arrDados['usuarioIdCriacao'] = $arrDados['usuarioIdCriacao'] ? $arrDados['usuarioIdCriacao'][0] : null;
        }

        if (is_array($arrDados['usuarioIdResponsavel']) && !$arrDados['usuarioIdResponsavel']['text']) {
            $arrDados['usuarioIdResponsavel'] = $arrDados['usuarioIdResponsavel']['id'];
        }

        if ($arrDados['usuarioIdResponsavel'] && is_string($arrDados['usuarioIdResponsavel'])) {
            $arrDados['usuarioIdResponsavel'] = $serviceAcessoPessoas->getArrSelect2(
                array('id' => $arrDados['usuarioIdResponsavel'])
            );
            $arrDados['usuarioIdResponsavel'] = $arrDados['usuarioIdResponsavel'] ? $arrDados['usuarioIdResponsavel'][0]
                : null;
        }

        if (is_array($arrDados['protocoloSolicitantePes']) && !$arrDados['protocoloSolicitantePes']['text']) {
            $arrDados['protocoloSolicitantePes'] = $arrDados['protocoloSolicitantePes']['pesId'];
        }

        if ($arrDados['protocoloSolicitantePes'] && is_string($arrDados['protocoloSolicitantePes'])) {
            $arrDados['protocoloSolicitantePes'] = $servicePessoa->getArrSelect2(
                array('id' => $arrDados['protocoloSolicitantePes'])
            );
            $arrDados['protocoloSolicitantePes'] = $arrDados['protocoloSolicitantePes']
                ? $arrDados['protocoloSolicitantePes'][0] : null;
        }

        if (is_array(
                $arrDados['protocoloSolicitanteAlunocurso']
            ) && !$arrDados['protocoloSolicitanteAlunocurso']['text']
        ) {
            $arrDados['protocoloSolicitanteAlunocurso'] = $arrDados['protocoloSolicitanteAlunocurso']['alunocursoId'];
        }

        if ($arrDados['protocoloSolicitanteAlunocurso'] && is_string($arrDados['protocoloSolicitanteAlunocurso'])) {
            $arrDados['protocoloSolicitanteAlunocurso'] = $serviceAcadgeralAlunoCurso->getArrSelect2(
                array('id' => $arrDados['protocoloSolicitanteAlunocurso'])
            );
            $arrDados['protocoloSolicitanteAlunocurso'] = $arrDados['protocoloSolicitanteAlunocurso']
                ? $arrDados['protocoloSolicitanteAlunocurso'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['protocoloId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['protocoloSolicitanteNome' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Protocolo\Entity\Protocolo */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getProtocoloId();
            $arrEntity[$params['value']] = $objEntity->getProtocoloSolicitanteNome();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($protocoloId)
    {
        if (!$protocoloId) {
            $this->setLastError('Para remover um registro de protocolo é necessário informar o código.');

            return null;
        }

        try {
            /** @var $objProtocolo \Protocolo\Entity\Protocolo */
            $objProtocolo = $this->getRepository()->find($protocoloId);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objProtocolo);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de protocolo.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceProtocoloSolicitacao = new \Protocolo\Service\ProtocoloSolicitacao($this->getEm());
        $serviceAcessoPessoas        = new \Acesso\Service\AcessoPessoas($this->getEm());
        $servicePessoa               = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceAcadgeralAlunoCurso  = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());

        $serviceProtocoloSolicitacao->setarDependenciasView($view);
        $serviceAcessoPessoas->setarDependenciasView($view);
        $serviceAcessoPessoas->setarDependenciasView($view);
        $servicePessoa->setarDependenciasView($view);
        $serviceAcadgeralAlunoCurso->setarDependenciasView($view);

        $view->setVariable("arrProtocoloSolicitanteVisivel", $this->getArrSelect2ProtocoloSolicitanteVisivel());
        $view->setVariable("arrProtocoloSituacao", $this->getArrSelect2ProtocoloSituacao());
    }

    public function retornaDadosCompletosDoProtocolo($protocoloId)
    {
        /** @var \Protocolo\Entity\Protocolo $objProtocolo */
        $objProtocolo             = $this->getRepository()->find($protocoloId);
        $serviceProtocoloMensagem = new \Protocolo\Service\ProtocoloMensagem($this->getEm());
        $serviceContato           = new \Pessoa\Service\Contato($this->getEm());
        $servicePessoa            = new \Pessoa\Service\Pessoa($this->getEm());
        $array                    = false;

        if ($objProtocolo) {
            $array = $objProtocolo->toArray();

            $protocoloSolicitantePes = $objProtocolo->getProtocoloSolicitantePes();

            $array['protocoloSolicitanteNome']       = $objProtocolo->getProtocoloSolicitanteNome();
            $array['protocoloSolicitanteAlunocurso'] = '-';
            $array['protocoloSetor']                 = $objProtocolo->getSolicitacao()->getSetor()->getSetorDescricao();
            $array['solicitacao']                    = $objProtocolo->getSolicitacao()->getSolicitacaoDescricao();
            $array['pesResponsavel']                 =(
                $objProtocolo->getUsuarioIdResponsavel() ?
                    $objProtocolo->getUsuarioIdResponsavel()->getPes()->getPes()->getPesNome() :
                    ''
            );

            $array['protocoloDataCadastroFormatado'] = self::formatDateBrasileiro($array['protocoloDataCadastro']);
            $array['protocoloDataAlteracaoFormatado'] = self::formatDateBrasileiro($array['protocoloDataAlteracao']);

            $array['protocoloSituacao'] = [
                'text' => $objProtocolo->getProtocoloSituacao(),
                'id'   => $objProtocolo->getProtocoloSituacao()
            ];

            if ($objProtocolo->getProtocoloSolicitanteAlunocurso()) {
                $objAlunocurso = $objProtocolo->getProtocoloSolicitanteAlunocurso();

                $array['protocoloSolicitanteAlunocurso'] = (
                    $objAlunocurso->getAluno()->getPes()->getPes()->getPesNome() .
                    ' / ' .
                    $objAlunocurso->getCursocampus()->getCurso()->getCursoNome()
                );

                $array['aluno_id']  = $objAlunocurso->getAluno()->getAlunoId();
                $array['matricula'] = $objAlunocurso->getAlunocursoId();
                $array['pesCpf']    = $objAlunocurso->getAluno()->getPes()->getPesCpf();
            }

            if ($protocoloSolicitantePes) {
                $arrContato = $protocoloSolicitantePes->toArray();

                $array['protocoloSolicitanteNome'] = $arrContato['pesNome'];

                if (is_null($array['protocoloSolicitanteEmail'])) {
                    $array['protocoloSolicitanteEmail'] = $arrContato['conContatoEmail'];
                }

                if (is_null($array['protocoloSolicitanteTelefone'])) {
                    $array['protocoloSolicitanteTelefone'] = $arrContato['conContatoTelefone'];
                }

                if (is_null($array['protocoloSolicitanteCelular'])) {
                    $array['protocoloSolicitanteCelular'] = $arrContato['conContatoCelular'];
                }
            }
        }

        $array['arrMensagem'] = $serviceProtocoloMensagem->getArrMensagensPorIdProtocolo($protocoloId);

        return $array;
    }

    public function alterarSituacaoProtocolo($arrDados)
    {
        $service                  = new \Acesso\Service\AcessoPessoas($this->getEm());
        $serviceProtocoloMensagem = new \Protocolo\Service\ProtocoloMensagem($this->getEm());

        if (!$arrDados['protocoloId'] && !$arrDados['protocoloSituacao']) {
            return false;
        }

        $arrProtocoloMensagem = array();

        $situacaoAtual = $this->retornaSituacaoProtocolo($arrDados['protocoloId']);

        $params['protocoloSituacao']      = $arrDados['protocoloSituacao'];
        $params['protocoloId']            = $arrDados['protocoloId'];
        $params['usuarioIdAlteracao']     = $service->retornaUsuarioLogado()->getId();
        $params['protocoloDataAlteracao'] = (new \DateTime('now'))->format('Y-m-d H:i:s');

        $sql = "
        UPDATE protocolo
        SET
            protocolo_situacao = :protocoloSituacao,
            usuario_id_alteracao = :usuarioIdAlteracao,
            protocolo_data_alteracao = :protocoloDataAlteracao
        WHERE protocolo_id = :protocoloId";

        try {
            $this->executeQueryWithParam($sql, $params);

            $arrProtocoloMensagem['mensagemConteudo']               =
                "Situação do protocolo foi alterada de " . $situacaoAtual . " para " . $params['protocoloSituacao'] .
                " na data " . (new \DateTime())->format("d/m/Y H:i");
            $arrProtocoloMensagem['protocolo']                      = $params['protocoloId'];
            $arrProtocoloMensagem['solicitanteVisivelSobrescrever'] = "Não";

            $serviceProtocoloMensagem->save($arrProtocoloMensagem);

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function retornaTipoSolicitantePeloPesId($pesId)
    {
        $service = new \Acesso\Service\AcessoPessoas($this->getEm());

        if (!$pesId) {
            return false;
        }

        $objAcessoPessoas = $service->retornaUsuarioLogado();

        $usuarioLogado = $objAcessoPessoas->getPesFisica() ? $objAcessoPessoas->getPesFisica()->getPes()->getPesId()
            : $objAcessoPessoas->getPesJuridica()->getPes()->getPesId();

        if ($usuarioLogado == $pesId) {
            return "Solicitante";
        } else {
            return "Operador";
        }
    }

    public function retornaProtocoloPeloId($protocoloId)
    {
        $array                    = false;
        $arrId                    = explode(',', $protocoloId);
        $servicePessoa            = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceProtocoloMensagem = new \Protocolo\Service\ProtocoloMensagem($this->getEm());

        foreach ($arrId as $protocoloId) {
            $temp = null;
            /** @var \Protocolo\Entity\Protocolo $objProtocolo */
            $objProtocolo = $this->getRepository()->findOneBy(['protocoloId' => $protocoloId]);

            if ($objProtocolo) {
                $temp['protocolo'] = $objProtocolo->toArray();
            }

            if ($objPessoa = $objProtocolo->getProtocoloSolicitantePes()) {
                $arrPessoa = $servicePessoa->getArray($objPessoa->getPesId());

                $solicitanteEmail    = $temp['protocolo']['protocoloSolicitanteEmail'];
                $solicitanteTelefone = $temp['protocolo']['protocoloSolicitanteTelefone'];

                if (!$solicitanteEmail || $solicitanteEmail == "") {
                    $temp['protocolo']['protocoloSolicitanteEmail'] = $arrPessoa['conContatoEmail'];
                }

                if (!$solicitanteTelefone || $solicitanteTelefone == "") {
                    $telefone = $arrPessoa['conContatoCelular'] . ' / ' . $arrPessoa['conContatoTelefone'];

                    $temp['protocolo']['protocoloSolicitanteTelefone'] = $telefone;
                }
            }

            if ($objAluno = $objProtocolo->getProtocoloSolicitanteAlunocurso()) {
                $arrAluno       = $objAluno->getAluno()->toArray();
                $arrPessoaAluno = $servicePessoa->getArray($arrAluno['pesId']);

                $arrAluno['conContatoEmail']    = $arrPessoaAluno['conContatoEmail'];
                $arrAluno['conContatoTelefone'] = $arrPessoaAluno['conContatoTelefone'];
                $arrAluno['conContatoCelular']  = $arrPessoaAluno['conContatoCelular'];

                $temp['protocolo']['protocoloSolicitanteTelefone'] =
                    $arrAluno['conContatoCelular'] . ' / ' . $arrAluno['conContatoTelefone'];
                $temp['protocolo']['protocoloSolicitanteEmail']    =
                    $arrAluno['conContatoEmail'];

                $temp['solicitanteAlunoCurso'] = $arrAluno;
            }

            $temp['mensagem'] = $serviceProtocoloMensagem->getArrMensagensPorIdProtocolo($protocoloId, true);

            $array[] = $temp;
        }

        return $array;
    }

    public function trocarProtocoloResponsavel($arrDados)
    {
        $service  = new \Acesso\Service\AcessoPessoas($this->getEm());
        $error    = true;
        $mensagem = 'Não foi possível concluir a alteração!';

        /** @var $objPessoalLogada \Acesso\Entity\AcessoPessoas */
        $objPessoalLogada = $service->retornaUsuarioLogado();

        if (!$objPessoalLogada) {
            $this->setLastError("Falha ao identificar pessoa logada!");

            return false;
        }

        try {
            $usuarioIdAlteracao = $objPessoalLogada->getId();
            $params             = array();
            $dados              = null;

            if ($arrDados['usuarioIdResponsavel'] == 'pessoaLogada') {
                $arrDados['usuarioIdResponsavel'] = $objPessoalLogada->getId();
                $arrDados['alterarSituacao']      = true;
            }

            if (empty($arrDados['protocoloId']) && empty($arrDados['usuarioIdResponsavel'])) {
                return false;
            }

            if ($arrDados['usuarioIdResponsavel'] == 0) {
                $arrDados['usuarioIdResponsavel'] = $usuarioIdAlteracao;
            }

            $params['usuarioIdResponsavel']   = $arrDados['usuarioIdResponsavel'];
            $params['usuarioIdAlteracao']     = $usuarioIdAlteracao;
            $params['protocoloDataAlteracao'] = (new \DateTime('now'))->format('Y-m-d H:i:s');

            $sql = "
            UPDATE protocolo
            SET
                usuario_id_responsavel = :usuarioIdResponsavel,
                usuario_id_alteracao = :usuarioIdAlteracao,
                protocolo_data_alteracao = :protocoloDataAlteracao
            WHERE protocolo_id = {$arrDados['protocoloId']}";

            $result = $this->executeQueryWithParam($sql, $params);

            if ($objUsuarioResponsavel = $service->retornaUsuarioLogado($arrDados['usuarioIdResponsavel'])) {
                /** @var \Protocolo\Entity\Protocolo $objProtocolo */
                $objProtocolo = $this->getRepository()->find($arrDados['protocoloId']);

                $dados['nome_responsavel']  = $objUsuarioResponsavel->getPes()->getPes()->getPesNome();
                $dados['protocolo_assunto'] = $objProtocolo->getProtocoloAssunto();
            }

            if ($arrDados['alterarSituacao']) {
                /** @var \Protocolo\Entity\Protocolo $objProtocolo */
                $objProtocolo = $this->getRepository()->find($arrDados['protocoloId']);

                if ($objProtocolo->getProtocoloSituacao() == self::PROTOCOLO_SITUACAO_ABERTO) {
                    $arrDadosAlteracao = [
                        'protocoloId'       => $arrDados['protocoloId'],
                        'protocoloSituacao' => self::PROTOCOLO_SITUACAO_EM_ANDAMENTO
                    ];

                    if ($this->alterarSituacaoProtocolo($arrDadosAlteracao)) {
                        $mensagem = 'Atualizado com sucesso e situação alterada para "em andamento"!';
                    } else {
                        $mensagem = 'Atualizado com sucesso! Por favor altere a situação para "em andamento"!';
                    }
                }
            }

            $error = false;
        } catch (\Exception $e) {
            return false;
            $error = true;
        }

        return ['error' => $error, 'message' => $mensagem, 'result' => $dados];
    }

    /**
     * @param integer $id
     * @param integer $integracaoId
     * @return array | boolean
     */
    public function retornaSolicitanteProtocolos($id, $integracaoId)
    {
        if (!$id or !$integracaoId) {
            return false;
        }

        $serviceAlunoIntegracao = new \Sistema\Service\SisIntegracaoAluno($this->getEm());

        /** @var \Sistema\Entity\SisIntegracaoAluno $objAlunoIntegracao */
        $objAlunoIntegracao = $serviceAlunoIntegracao->getRepository()->findOneBy(
            [
                'codigo'     => $id,
                'integracao' => $integracaoId
            ]
        );

        if (!$objAlunoIntegracao) {
            return false;
        }

        $params = [
            'alunoId' => $objAlunoIntegracao->getAluno()->getAlunoId(),
            'pesId'   => $objAlunoIntegracao->getAluno()->getPes()->getPes()->getPesId()
        ];

        $query = "
            SELECT protocolo.*
            FROM protocolo
            LEFT JOIN acadgeral__aluno_curso ON alunocurso_id = protocolo.protocolo_solicitante_alunocurso_id
            WHERE
              (aluno_id = :alunoId OR protocolo_solicitante_pes_id = :pesId) AND
              protocolo_solicitante_visivel = 'Sim'
            ORDER BY protocolo_data_alteracao DESC";

        try {
            $result = $this->executeQueryWithParam($query, $params)->fetchAll();
        } catch (\Exception $e) {
            $result = false;
            $this->setLastError($e->getMessage());
        }

        return $result;
    }

    public function retornaSituacaoProtocolo($protocoloId)
    {
        if (!$protocoloId) {
            $this->setLastError("Nenhum protocolo foi informado!");

            return false;
        }

        /** @var \Protocolo\Entity\Protocolo $objProtocolo */
        $objProtocolo = $this->getRepository()->find($protocoloId);

        if ($objProtocolo) {
            return $objProtocolo->getProtocoloSituacao();
        }

        return false;
    }
}
?>