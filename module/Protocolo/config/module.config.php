<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Protocolo;

return array(
    'router'                    => array(
        'routes' => array(
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'protocolo' => array(
                'type'          => 'Literal',
                'options'       => array(
                    'route'    => '/protocolo',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Protocolo\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes'  => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'       => '/[:controller[/:action[/:id]]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults'    => array(),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager'           => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases'            => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'doctrine'                  => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default'             => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            ),
        ),
    ),
    'controllers'               => array(
        'invokables' => array(
            'Protocolo\Controller\Protocolo'               => 'Protocolo\Controller\ProtocoloController',
            'Protocolo\Controller\ProtocoloVinculado'      => 'Protocolo\Controller\ProtocoloVinculadoController',
            'Protocolo\Controller\ProtocoloMensagem'       => 'Protocolo\Controller\ProtocoloMensagemController',
            'Protocolo\Controller\ProtocoloMensagemPadrao' => 'Protocolo\Controller\ProtocoloMensagemPadraoController',
            'Protocolo\Controller\ProtocoloSetor'          => 'Protocolo\Controller\ProtocoloSetorController',
            'Protocolo\Controller\ProtocoloSetorGrupo'     => 'Protocolo\Controller\ProtocoloSetorGrupoController',
            'Protocolo\Controller\ProtocoloSolicitacao'    => 'Protocolo\Controller\ProtocoloSolicitacaoController',
        ),
    ),
    'view_manager'              => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console'                   => array(
        'router' => array(
            'routes' => array(),
        ),
    ),
    'funcionalidades_liberadas' => array(
        'controllers' => array(),
        'actions'     => array()
    ),
    'namesDictionary'           => array(
        'Protocolo\Protocolo'               => 'Protocolo',
        'Protocolo\ProtocoloVinculado'      => 'Protocolos Relacionados',
        'Protocolo\ProtocoloMensagem'       => 'Mensagem de Protocolo',
        'Protocolo\ProtocoloMensagemPadrao' => 'Mensagens Padrão de Protocolo',
        'Protocolo\ProtocoloSetor'          => 'Setor',
        'Protocolo\ProtocoloSolicitacao'    => 'Solicitações',
    )
);
