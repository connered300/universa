<?php
namespace Documentos\Service;

namespace Documentos\Service;

use VersaSpine\Service\AbstractService;

class DocumentoGrupo extends AbstractService
{
    const DOCGRUPO_EXIGENCIA_OBRIGATORIO = 'Obrigatório';
    const DOCGRUPO_EXIGENCIA_FACULTATIVO = 'Facultativo';
    const DOCGRUPO_EXIGENCIA_CONTEXTO = 'Contexto';
    const DOCGRUPO_FORMATO_SIMPLES = 'Simples';
    const DOCGRUPO_FORMATO_AUTENTICADO = 'Autenticado';
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    public function getArrSelect2DocgrupoExigencia($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getDocgrupoExigencia());
    }

    public static function getDocgrupoExigencia()
    {
        return array(
            self::DOCGRUPO_EXIGENCIA_OBRIGATORIO,
            self::DOCGRUPO_EXIGENCIA_FACULTATIVO,
            self::DOCGRUPO_EXIGENCIA_CONTEXTO
        );
    }

    public function getArrSelect2DocgrupoFormato($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getDocgrupoFormato());
    }

    public static function getDocgrupoFormato()
    {
        return array(self::DOCGRUPO_FORMATO_SIMPLES, self::DOCGRUPO_FORMATO_AUTENTICADO);
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Documentos\Entity\DocumentoGrupo');
    }

    public function pesquisaForJson($params)
    {
        $sql               = '
        SELECT
        a.docgrupoId as docgrupo_id,
        a.id as id,
        a.tdocId as tdoc_id,
        a.docgrupoExigencia as docgrupo_exigencia,
        a.docgrupoFormato as docgrupo_formato,
        a.docgrupoDescricao as docgrupo_descricao
        FROM Documentos\Entity\DocumentoGrupo a
        WHERE';
        $docgrupoDescricao = false;
        $docgrupoId        = false;

        if ($params['q']) {
            $docgrupoDescricao = $params['q'];
        } elseif ($params['query']) {
            $docgrupoDescricao = $params['query'];
        }

        if ($params['docgrupoId']) {
            $docgrupoId = $params['docgrupoId'];
        }

        $parameters = array('docgrupoDescricao' => "{$docgrupoDescricao}%");
        $sql .= ' a.docgrupoDescricao LIKE :docgrupoDescricao';

        if ($docgrupoId) {
            $parameters['docgrupoId'] = explode(',', $docgrupoId);
            $parameters['docgrupoId'] = $docgrupoId;
            $sql .= ' AND a.docgrupoId NOT IN(:docgrupoId)';
        }

        $sql .= " ORDER BY a.docgrupoDescricao";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceAcessoGrupo   = new \Acesso\Service\AcessoGrupo($this->getEm());
        $serviceDocumentoTipo = new \Documentos\Service\DocumentoTipo($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['docgrupoId']) {
                /** @var $objDocumentoGrupo \Documentos\Entity\DocumentoGrupo */
                $objDocumentoGrupo = $this->getRepository()->find($arrDados['docgrupoId']);

                if (!$objDocumentoGrupo) {
                    $this->setLastError('Registro de grupo não existe!');

                    return false;
                }
            } else {
                $objDocumentoGrupo = new \Documentos\Entity\DocumentoGrupo();
            }

            if ($arrDados['id']) {
                /** @var $objAcessoGrupo \Acesso\Entity\AcessoGrupo */
                $objAcessoGrupo = $serviceAcessoGrupo->getRepository()->find($arrDados['id']);

                if (!$objAcessoGrupo) {
                    $this->setLastError('Registro de grupo não existe!');

                    return false;
                }

                $objDocumentoGrupo->setId($objAcessoGrupo);
            } else {
                $objDocumentoGrupo->setId(null);
            }

            if ($arrDados['tdoc']) {
                /** @var $objDocumentoTipo \Documentos\Entity\DocumentoTipo */
                $objDocumentoTipo = $serviceDocumentoTipo->getRepository()->find($arrDados['tdoc']);

                if (!$objDocumentoTipo) {
                    $this->setLastError('Registro de tipo não existe!');

                    return false;
                }

                $objDocumentoGrupo->setTdoc($objDocumentoTipo);
            } else {
                $objDocumentoGrupo->setTdoc(null);
            }

            $objDocumentoGrupo->setDocgrupoExigencia($arrDados['docgrupoExigencia']);
            $objDocumentoGrupo->setDocgrupoFormato($arrDados['docgrupoFormato']);
            $objDocumentoGrupo->setDocgrupoDescricao($arrDados['docgrupoDescricao']);

            $this->getEm()->persist($objDocumentoGrupo);
            $this->getEm()->flush($objDocumentoGrupo);

            $this->getEm()->commit();

            $arrDados['docgrupoId'] = $objDocumentoGrupo->getDocgrupoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de grupo!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['id']) {
            $errors[] = 'Por favor preencha o campo "código"!';
        }

        if (!$arrParam['tdoc']) {
            $errors[] = 'Por favor preencha o campo "código tipo de documento"!';
        }

        if (!$arrParam['docgrupoExigencia']) {
            $errors[] = 'Por favor preencha o campo "exigencia de documento"!';
        }

        if (!in_array($arrParam['docgrupoExigencia'], self::getDocgrupoExigencia())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "exigencia de documento"!';
        }

        if (!$arrParam['docgrupoFormato']) {
            $errors[] = 'Por favor preencha o campo "formato de documento"!';
        }

        if (!in_array($arrParam['docgrupoFormato'], self::getDocgrupoFormato())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "formato de documento"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "
        SELECT documento__grupo.*, tdoc_descricao, grup_nome FROM documento__grupo
        INNER JOIN acesso_grupo on acesso_grupo.id = documento__grupo.id
        INNER JOIN documento__tipo using(tdoc_id)
        ";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($docgrupoId)
    {
        /** @var $objDocumentoGrupo \Documentos\Entity\DocumentoGrupo */
        $objDocumentoGrupo    = $this->getRepository()->find($docgrupoId);
        $serviceAcessoGrupo   = new \Acesso\Service\AcessoGrupo($this->getEm());
        $serviceDocumentoTipo = new \Documentos\Service\DocumentoTipo($this->getEm());

        try {
            $arrDados = $objDocumentoGrupo->toArray();

            if ($arrDados['id']) {
                $arrAcessoGrupo = $serviceAcessoGrupo->getArrSelect2(['id' => $arrDados['id']]);
                $arrDados['id'] = $arrAcessoGrupo ? $arrAcessoGrupo[0] : null;
            }

            if ($arrDados['tdoc']) {
                $arrDocumentoTipo = $serviceDocumentoTipo->getArrSelect2(['id' => $arrDados['tdoc']]);
                $arrDados['tdoc'] = $arrDocumentoTipo ? $arrDocumentoTipo[0] : null;
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceAcessoGrupo   = new \Acesso\Service\AcessoGrupo($this->getEm());
        $serviceDocumentoTipo = new \Documentos\Service\DocumentoTipo($this->getEm());

        if (is_array($arrDados['id']) && !$arrDados['id']['text']) {
            $arrDados['id'] = $arrDados['id']['id'];
        }

        if ($arrDados['id'] && is_string($arrDados['id'])) {
            $arrDados['id'] = $serviceAcessoGrupo->getArrSelect2(array('id' => $arrDados['id']));
            $arrDados['id'] = $arrDados['id'] ? $arrDados['id'][0] : null;
        }

        if (is_array($arrDados['tdoc']) && !$arrDados['tdoc']['text']) {
            $arrDados['tdoc'] = $arrDados['tdoc']['tdocId'];
        }

        if ($arrDados['tdoc'] && is_string($arrDados['tdoc'])) {
            $arrDados['tdoc'] = $serviceDocumentoTipo->getArrSelect2(array('id' => $arrDados['tdoc']));
            $arrDados['tdoc'] = $arrDados['tdoc'] ? $arrDados['tdoc'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['docgrupoId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['docgrupoDescricao' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Documentos\Entity\DocumentoGrupo */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getDocgrupoId();
            $arrEntity[$params['value']] = $objEntity->getDocgrupoDescricao();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['docgrupoId']) {
            $this->setLastError('Para remover um registro de grupo é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objDocumentoGrupo \Documentos\Entity\DocumentoGrupo */
            $objDocumentoGrupo = $this->getRepository()->find($param['docgrupoId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objDocumentoGrupo);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de grupo.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceAcessoGrupo   = new \Acesso\Service\AcessoGrupo($this->getEm());
        $serviceDocumentoTipo = new \Documentos\Service\DocumentoTipo($this->getEm());

        $serviceAcessoGrupo->setarDependenciasView($view);
        $serviceDocumentoTipo->setarDependenciasView($view);

        $view->setVariable("arrDocgrupoExigencia", $this->getArrSelect2DocgrupoExigencia());

        $view->setVariable("arrDocgrupoFormato", $this->getArrSelect2DocgrupoFormato());
    }

    public function documentosGrupo($nomeGrupo, $arrDocumentoPessoa = array())
    {
        $query = '
          SELECT CAST(docgrupo_id as SIGNED INTEGER) docgrupo_id, tipo.tdoc_id, docgrupo_exigencia, tdoc_descricao, docgrupo_formato
          FROM documento__grupo as doc
          INNER JOIN acesso_grupo as grupo ON grupo.id = doc.id
          INNER JOIN documento__tipo as tipo ON tipo.tdoc_id = doc.tdoc_id
          WHERE grupo.grup_nome = :nomeGrupo';

        $documentosGrupo = $this
            ->executeQueryWithParam($query, array('nomeGrupo' => $nomeGrupo))
            ->fetchAll();

        foreach ($documentosGrupo as $pos => $documento) {
            $documento['selecionado'] = 'Não';

            $documento['opcoes'] = array(
                array('valor' => 'Sim', 'descricao' => 'Sim'),
                array('valor' => 'Não', 'descricao' => 'Não'),
            );

            if ($documento['docgrupo_exigencia'] == 'Contexto') {
                $documento['opcoes'][]    = array('valor' => 'Dispensavel', 'descricao' => 'Não Aplicável');
                $documento['selecionado'] = 'Não';
            }

            $documento['obrigatorio'] = ($documento['docgrupo_exigencia'] == 'Obrigatório');
            $documento['id']          = $documento['docgrupo_id'];
            $documento['descricao']   = $documento['tdoc_descricao'];

            if ($arrDocumentoPessoa[$documento['docgrupo_id']]) {
                $documento['selecionado'] = $arrDocumentoPessoa[$documento['docgrupo_id']]['docpessoaStatus'];
            }

            $documentosGrupo[$pos] = $documento;
        }

        return $documentosGrupo;
    }
}