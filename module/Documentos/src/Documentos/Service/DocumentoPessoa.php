<?php


namespace Documentos\Service;

use VersaSpine\Service\AbstractService;

class DocumentoPessoa extends AbstractService
{
    /**
     * Armazena o último erro registrado
     * @var null
     */
    private $lastError = null;

    /**
     * Retorna o último erro registrado
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->lastError;
    }

    /**
     * Altera o último erro registrado
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Documentos\Entity\DocumentoPessoa');
    }

    /**
     * Este método deve ser implementado pelas classe filhas, o mesmo é chamado antes de executar
     * as operações de insere e edita, com a impletação deste método pode-se validar os dados antes de
     * usar os mesmos no banco de dados
     * Caso sua validação for reprovar os dados, retorne um array como o exemplo na linha seguinte
     * array('msg'=>'Mensagem de erro','code'=>1,'previos'=>'seila')
     * Se ele retornar um array o método em questão irá disparar um execeção com base nas informações do array retornado
     * @return array | true
     * @throws \Exception
     */
    protected function valida($dados)
    {
    }

    public function edita(array $dados)
    {
        if ($dados['docpessoaId'] == "") {
            //$dados['docpessoaId'] = $this->findOneBy(['pes' => $dados['pes'], 'docgrupo' => $dados['docgrupo']])->getDocpessoaId();
            $documento = $this->findOneBy(['pes' => $dados['pes'], 'docgrupo' => $dados['docgrupo']]);
            if ($documento) {
                $documento                 = $documento->toArray();
                $dados['docpessoaId']      = $documento['docpessoaId'];
                $dados['docpessoaEntrega'] = $documento['docpessoaStatus'] != 'Dispensavel' ? (new \DateTime()) : null;
                parent::edita($dados);
            } else {
                parent::adicionar($dados);
            }
        }
        parent::edita($dados);
    }

    public function adicionar(array $data)
    {
        if (!is_object($data['docgrupo'])) {
            $data['docgrupo'] = $this->getRepository('Documentos\Entity\DocumentoGrupo')->find((int)$data['docgrupo']);
        }

        if (!is_object($data['pes'])) {
            $data['pes'] = $this->getRepository('Pessoa\Entity\Pessoa')->find((int)$data['pes']);
        }

        $documento = new \Documentos\Entity\DocumentoPessoa($data);

        try {
            $this->getEm()->persist($documento);
            $this->getEm()->flush();
            $this->getEm()->clear();
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
        }

        return $documento;
    }

    public function getArrayDocumentosPessoa($pesId)
    {
        $arrDocumentos      = [];
        $arrDocumentoPessoa = $this->getRepository()->findBy(['pes' => $pesId]);

        /* @var $objDocumentoPessoa \Documentos\Entity\DocumentoPessoa */
        foreach ($arrDocumentoPessoa as $objDocumentoPessoa) {
            $arrDocumentos[$objDocumentoPessoa->getDocgrupo()->getDocgrupoId()] = [
                'docpessoaId'       => $objDocumentoPessoa->getDocpessoaId(),
                'docpessoaEntrega'  => $objDocumentoPessoa->getDocpessoaEntrega(),
                'docpessoaStatus'   => $objDocumentoPessoa->getDocpessoaStatus(),
                'pesId'             => $objDocumentoPessoa->getPes()->getPesId(),
                'docgrupoId'        => $objDocumentoPessoa->getDocgrupo()->getDocgrupoId(),
                'docgrupoDescricao' => $objDocumentoPessoa->getDocgrupo()->getDocgrupoDescricao(),
                'docgrupoExigencia' => $objDocumentoPessoa->getDocgrupo()->getDocgrupoExigencia(),
                'tdocDescricao'     => $objDocumentoPessoa->getDocgrupo()->getTdoc()->getTdocDescricao(),
            ];
        }

        return $arrDocumentos;
    }

    public function  retornaDocumentosNaoEntregues($pesId)
    {
        if (!$pesId) {
            $this->setLastError('Informe uma pessoa!');

            return false;
        }

        $query = "
        SELECT  *
        FROM documento__grupo g
        inner join documento__tipo t on t.tdoc_id = g.tdoc_id
        where
        g.docgrupo_id not in(
          select docgrupo_id
          from documento__pessoa
          where pes_id={$pesId} and
                docpessoa_status != 'Não'
        ) and
        g.docgrupo_exigencia in ('Obrigatório','Contexto')";

        $arrDados = $this->executeQuery($query)->fetchAll();

        $arrConversao = [];

        foreach ($arrDados as $value) {
            $arrConversao[] = $value['tdoc_descricao'];
        }

        return $arrConversao;
    }

    /**
     * Este método deve ser implementado nas classes filhas retornando um array com as informações da busca,
     * este método pode ser utilzado para retornado buscas no formato json
     */
    protected function pesquisaForJson($criterios)
    {
    }

    public function pesquisaPelaPessoa($pesId, $toArray = true)
    {
        $arrResult = $this->getRepository()->findBy(array('pes' => $pesId));

        if ($toArray) {
            $arrRetorno = array();

            /* @var $objDocumentoPessoa \Documentos\Entity\DocumentoPessoa */
            foreach ($arrResult as $objDocumentoPessoa) {
                $arrRetorno[] = $objDocumentoPessoa->toArray();
            }

            return $arrRetorno;
        }

        return $arrResult;
    }

    public function salvarMultiplos($arrDocumentosPessoa, \Pessoa\Entity\Pessoa $objPessoa)
    {
        $arrExcluir = array();
        $arrEditar  = array();

        try {
            $this->getEm()->beginTransaction();

            $arrDocumentosPessoaDB = $this->getRepository()->findBy(array('pes' => $objPessoa->getPesId()));

            /* @var $objDocumentosPessoa \Documentos\Entity\DocumentoPessoa */
            foreach ($arrDocumentosPessoaDB as $objDocumentosPessoa) {
                $encontrado = false;
                $docGrupoId = $objDocumentosPessoa->getDocgrupo()->getDocgrupoId();

                foreach ($arrDocumentosPessoa as $docGrupoId2 => $valor) {
                    if ($docGrupoId == $docGrupoId2) {
                        $encontrado              = true;
                        $arrEditar[$docGrupoId2] = $objDocumentosPessoa;
                        break;
                    }
                }

                if (!$encontrado) {
                    $arrExcluir[$docGrupoId] = $objDocumentosPessoa;
                }
            }

            if ($arrExcluir) {
                foreach ($arrExcluir as $objReg) {
                    $this->getEm()->remove($objReg);
                    $this->getEm()->flush();
                }
            }

            foreach ($arrDocumentosPessoa as $documentoGruopoId => $valor) {
                if (!isset($arrEditar[$documentoGruopoId])) {
                    $serviceDocumentoGrupo = new \Documentos\Service\DocumentoGrupo($this->getEm());

                    /* @var $objDocumentoGrupo \Documentos\Entity\DocumentoGrupo */
                    $objDocumentoGrupo = $serviceDocumentoGrupo->getRepository()->find($documentoGruopoId);

                    $objDocumentoPessoa = new \Documentos\Entity\DocumentoPessoa();
                    $objDocumentoPessoa->setDocgrupo($objDocumentoGrupo);
                    $objDocumentoPessoa->setDocpessoaEntrega(new \DateTime());
                    $objDocumentoPessoa->setPes($objPessoa);
                } else {
                    /* @var $objDocumentoPessoa \Documentos\Entity\DocumentoPessoa */
                    $objDocumentoPessoa = $arrEditar[$documentoGruopoId];

                    if ($objDocumentoPessoa->getDocpessoaStatus() != $valor) {
                        $objDocumentoPessoa->setDocpessoaEntrega(new \DateTime());
                    }
                }

                $objDocumentoPessoa->setDocpessoaStatus($valor);

                $this->getEm()->persist($objDocumentoPessoa);
                $this->getEm()->flush($objDocumentoPessoa);
            }

            $this->getEm()->commit();

            return true;
        } catch (\Exception $e) {
            $this->getEm()->rollback();
            $this->setLastError(
                'Não foi possível salvar o registro de documento do aluno!' .
                '<br>' .
                $e->getMessage()
            );
        }

        return false;
    }
}