<?php

namespace Documentos\Service;

use VersaSpine\Service\AbstractService;

class DocumentoEmissao extends AbstractService
{
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Documentos\Entity\DocumentoEmissao');
    }

    public function pesquisaForJson($params)
    {
        $sql       = '
        SELECT
        a.emissaoId as emissao_id,
        a.modeloId as modelo_id,
        a.pesId as pes_id,
        a.emissaoData as emissao_data,
        a.usuarioIdEmissao as usuario_id_emissao
        FROM Documentos\Entity\DocumentoEmissao a
        WHERE';
        $modeloId  = false;
        $emissaoId = false;

        if ($params['q']) {
            $modeloId = $params['q'];
        } elseif ($params['query']) {
            $modeloId = $params['query'];
        }

        if ($params['emissaoId']) {
            $emissaoId = $params['emissaoId'];
        }

        $parameters = array('modeloId' => "{$modeloId}%");
        $sql .= ' a.modeloId LIKE :modeloId';

        if ($emissaoId) {
            $parameters['emissaoId'] = explode(',', $emissaoId);
            $parameters['emissaoId'] = $emissaoId;
            $sql .= ' AND a.emissaoId NOT IN(:emissaoId)';
        }

        $sql .= " ORDER BY a.modeloId";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceDocumentoModelo = new \Documentos\Service\DocumentoModelo($this->getEm());
        $servicePessoa          = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceAcessoPessoas   = new \Acesso\Service\AcessoPessoas($this->getEm());
        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['emissaoId']) {
                /** @var $objDocumentoEmissao \Documentos\Entity\DocumentoEmissao */
                $objDocumentoEmissao = $this->getRepository()->find($arrDados['emissaoId']);

                if (!$objDocumentoEmissao) {
                    $this->setLastError('Registro de emissão não existe!');

                    return false;
                }
            } else {
                $objDocumentoEmissao = new \Documentos\Entity\DocumentoEmissao();
            }

            if ($arrDados['modelo']) {
                /** @var $objDocumentoModelo \Documentos\Entity\DocumentoModelo */
                $objDocumentoModelo = $serviceDocumentoModelo->getRepository()->find($arrDados['modelo']);

                if (!$objDocumentoModelo) {
                    $this->setLastError('Registro de modelo não existe!');

                    return false;
                }

                $objDocumentoEmissao->setModelo($objDocumentoModelo);
            } else {
                $objDocumentoEmissao->setModelo(null);
            }

            if ($arrDados['pes']) {
                /** @var $objPessoa \Pessoa\Entity\Pessoa */
                $objPessoa = $servicePessoa->getRepository()->find($arrDados['pes']);

                if (!$objPessoa) {
                    $this->setLastError('Registro de pessoa não existe!');

                    return false;
                }

                $objDocumentoEmissao->setPes($objPessoa);
            } else {
                $objDocumentoEmissao->setPes(null);
            }

            $objDocumentoEmissao->setEmisasoData($arrDados['emissaoData']);

            /** @var $objAcessoPessoas \Acesso\Entity\AcessoPessoas */
            $objAcessoPessoas = $serviceAcessoPessoas->retornaUsuarioLogado($arrDados['usuarioIdEmissao']);

            $objDocumentoEmissao->setUsuarioIdEmissao($objAcessoPessoas);

            $this->getEm()->persist($objDocumentoEmissao);
            $this->getEm()->flush($objDocumentoEmissao);

            $this->getEm()->commit();

            $arrDados['emissaoId'] = $objDocumentoEmissao->getEmissaoId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError(
                "Não foi possível salvar o registro de emissão!<br>" . $e->getMessage()
            );

            return false;
        }

        return true;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['modelo']) {
            $errors[] = 'Por favor preencha o campo "modelo"!';
        }

        if (!$arrParam['pes']) {
            $errors[] = 'Por favor preencha o campo "pessoa"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "
        SELECT * FROM documento__emissao d
        INNER JOIN pessoa USING (pes_id)
        INNER JOIN documento__modelo USING (modelo_id)
        LEFT JOIN acesso_pessoas ap ON ap.id=d.usuario_id_emissao
        ";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($emissaoId)
    {
        /** @var $objDocumentoEmissao \Documentos\Entity\DocumentoEmissao */
        $objDocumentoEmissao    = $this->getRepository()->find($emissaoId);
        $serviceDocumentoModelo = new \Documentos\Service\DocumentoModelo($this->getEm());
        $servicePessoa          = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceAcessoPessoas   = new \Acesso\Service\AcessoPessoas($this->getEm());

        try {
            $arrDados = $objDocumentoEmissao->toArray();

            if ($arrDados['modelo']) {
                $arrDocumentoModelo = $serviceDocumentoModelo->getArrSelect2(['id' => $arrDados['modelo']]);
                $arrDados['modelo'] = $arrDocumentoModelo ? $arrDocumentoModelo[0] : null;
            }

            if ($arrDados['pes']) {
                $arrPessoa       = $servicePessoa->getArrSelect2(['id' => $arrDados['pes']]);
                $arrDados['pes'] = $arrPessoa ? $arrPessoa[0] : null;
            }

            if ($arrDados['usuarioIdEmissao']) {
                $arrAcessoPessoas             = $serviceAcessoPessoas->getArrSelect2(
                    ['id' => $arrDados['usuarioIdEmissao']]
                );
                $arrDados['usuarioIdEmissao'] = $arrAcessoPessoas ? $arrAcessoPessoas[0] : null;
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceDocumentoModelo = new \Documentos\Service\DocumentoModelo($this->getEm());
        $servicePessoa          = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceAcessoPessoas   = new \Acesso\Service\AcessoPessoas($this->getEm());

        if (is_array($arrDados['modelo']) && !$arrDados['modelo']['text']) {
            $arrDados['modelo'] = $arrDados['modelo']['modeloId'];
        }

        if ($arrDados['modelo'] && is_string($arrDados['modelo'])) {
            $arrDados['modelo'] = $serviceDocumentoModelo->getArrSelect2(array('id' => $arrDados['modelo']));
            $arrDados['modelo'] = $arrDados['modelo'] ? $arrDados['modelo'][0] : null;
        }

        if (is_array($arrDados['pes']) && !$arrDados['pes']['text']) {
            $arrDados['pes'] = $arrDados['pes']['pesId'];
        }

        if ($arrDados['pes'] && is_string($arrDados['pes'])) {
            $arrDados['pes'] = $servicePessoa->getArrSelect2(array('id' => $arrDados['pes']));
            $arrDados['pes'] = $arrDados['pes'] ? $arrDados['pes'][0] : null;
        }

        if (is_array($arrDados['usuarioIdEmissao']) && !$arrDados['usuarioIdEmissao']['text']) {
            $arrDados['usuarioIdEmissao'] = $arrDados['usuarioIdEmissao']['id'];
        }

        if ($arrDados['usuarioIdEmissao'] && is_string($arrDados['usuarioIdEmissao'])) {
            $arrDados['usuarioIdEmissao'] = $serviceAcessoPessoas->getArrSelect2(
                array('id' => $arrDados['usuarioIdEmissao'])
            );
            $arrDados['usuarioIdEmissao'] = $arrDados['usuarioIdEmissao'] ? $arrDados['usuarioIdEmissao'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['emissaoId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['modeloId' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Documentos\Entity\DocumentoEmissao */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getEmissaoId();
            $arrEntity[$params['value']] = $objEntity->getEmisasoData(true);

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['emissaoId']) {
            $this->setLastError('Para remover um registro de emissão é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objDocumentoEmissao \Documentos\Entity\DocumentoEmissao */
            $objDocumentoEmissao = $this->getRepository()->find($param['emissaoId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objDocumentoEmissao);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de emissão.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceDocumentoModelo = new \Documentos\Service\DocumentoModelo($this->getEm());
        $servicePessoa          = new \Pessoa\Service\Pessoa($this->getEm());
        $serviceAcessoPessoas   = new \Acesso\Service\AcessoPessoas($this->getEm());

        $serviceDocumentoModelo->setarDependenciasView($view);
        $servicePessoa->setarDependenciasView($view);
        $serviceAcessoPessoas->setarDependenciasView($view);
    }
}
?>