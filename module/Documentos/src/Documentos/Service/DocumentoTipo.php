<?php

namespace Documentos\Service;

use VersaSpine\Service\AbstractService;

class DocumentoTipo extends AbstractService
{
    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Documentos\Entity\DocumentoTipo');
    }

    public function pesquisaForJson($params)
    {
        $sql           = '
        SELECT
        a.tdocId as tdoc_id,
        a.tdocDescricao as tdoc_descricao
        FROM Documentos\Entity\DocumentoTipo a
        WHERE ';

        $tdocDescricao = false;
        $tdocId        = false;

        if ($params['q']) {
            $tdocDescricao = $params['q'];
        } elseif ($params['query']) {
            $tdocDescricao = $params['query'];
        }

        if ($params['tdocId']) {
            $tdocId = $params['tdocId'];
        }

        $parameters['tdocDescricao'] = "{$tdocDescricao}%";
        $sql .= ' a.tdocDescricao LIKE :tdocDescricao';

        if ($tdocId) {
            $parameters['tdocId'] = explode(',', $tdocId);
            $parameters['tdocId'] = $tdocId;
            $sql .= ' AND a.tdocId NOT IN(:tdocId)';
        }

        $sql .= " GROUP BY a.tdocId";

        $sql .= " ORDER BY a.tdocDescricao";

        $query = $this->getEm()->createQuery($sql);

        foreach ($parameters as $paramNome => $paramValue) {
            $query->setParameter($paramNome, $paramValue);
        }

        $result = $query->setMaxResults(40)->setFirstResult(0)->getResult();

        return $result;
    }

    public function save(array &$arrDados)
    {
        if (!$this->valida($arrDados)) {
            return false;
        }

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['tdocId']) {
                /** @var $objDocumentoTipo \Documentos\Entity\DocumentoTipo */
                $objDocumentoTipo = $this->getRepository()->find($arrDados['tdocId']);

                if (!$objDocumentoTipo) {
                    $this->setLastError('Registro de tipo de documento não existe!');

                    return false;
                }
            } else {
                $objDocumentoTipo = new \Documentos\Entity\DocumentoTipo();
            }

            $objDocumentoTipo->setTdocDescricao($arrDados['tdocDescricao']);

            $this->getEm()->persist($objDocumentoTipo);
            $this->getEm()->flush($objDocumentoTipo);

            $this->getEm()->commit();

            $arrDados['tdocId'] = $objDocumentoTipo->getTdocId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de tipo de documento!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['tdocDescricao']) {
            $errors[] = 'Por favor preencha o campo "descrição de documento"!';
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "SELECT * FROM documento__tipo";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($tdocId)
    {
        /** @var $objDocumentoTipo \Documentos\Entity\DocumentoTipo */
        $objDocumentoTipo = $this->getRepository()->find($tdocId);

        try {
            $arrDados = $objDocumentoTipo->toArray();
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['tdocId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['tdocDescricao' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Documentos\Entity\DocumentoTipo */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getTdocId();
            $arrEntity[$params['value']] = $objEntity->getTdocDescricao();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['tdocId']) {
            $this->setLastError('Para remover um registro de tipo de documento é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objDocumentoTipo \Documentos\Entity\DocumentoTipo */
            $objDocumentoTipo = $this->getRepository()->find($param['tdocId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objDocumentoTipo);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de tipo de documento.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
    }
}
?>