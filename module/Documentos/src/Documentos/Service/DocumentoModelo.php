<?php

namespace Documentos\Service;

use Doctrine\Common\Collections\ArrayCollection;
use VersaSpine\Service\AbstractService;

class DocumentoModelo extends AbstractService
{
    const MODELO_CONTEXTO_ALUNO = 'Aluno';
    const MODELO_CONTEXTO_ALUNO_CURSO = 'Aluno Curso';
    const MODELO_CONTEXTO_PESSOA = 'Pessoa';
    const MODELO_CONTEXTO_AGENTE_EDUCACIONAL = 'Agente Educacional';

    private $__lastError = null;

    /**
     * @return string
     */
    public function getLastError()
    {
        return (string)$this->__lastError;
    }

    /**
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->__lastError = $lastError;
    }

    /**
     * Construtor
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        parent::__construct($em, 'Documentos\Entity\DocumentoModelo');
    }

    public function getArrSelect2ModeloContexto($params = array())
    {
        return $this->getArrSelect2Constantes($params, $this->getModeloContexto());
    }

    public function getArrSelect2DocumentoGrupo($params = array())
    {
        $array = array();

        if (!$params['modeloId']) {
            return $array;
        }

        /** @var $objDocumentoModelo \Documentos\Entity\DocumentoModelo */
        $objDocumentoModelo = $this->getRepository()->find($params['modeloId']);

        if (!$objDocumentoModelo) {
            return $array;
        }

        try {
            $grupoCollection = $objDocumentoModelo->getGrupo();
            $arrGrupo        = $grupoCollection->getValues();

            /** @var \Acesso\Entity\AcessoGrupo $grupo */
            foreach ($arrGrupo as $grupo) {
                $array[] = array('text' => $grupo->getGrupNome(), 'id' => $grupo->getId());
            }
        } catch (\Exception $e) {
        }

        return $array;
    }

    public static function getModeloContexto()
    {
        return array(
            self::MODELO_CONTEXTO_ALUNO,
            self::MODELO_CONTEXTO_ALUNO_CURSO,
            self::MODELO_CONTEXTO_PESSOA,
            self::MODELO_CONTEXTO_AGENTE_EDUCACIONAL
        );
    }

    public function pesquisaForJson($params)
    {
        $serviceAcessoGrupo = new \Acesso\Service\AcessoGrupo($this->getEm());
        $arrGrupo           = $serviceAcessoGrupo->buscaGrupoUsr();

        $sqlAdd = [];

        if ($arrGrupo) {
            $sqlAdd[] = 'docGrupo.grupo_id in (' . implode(',', $arrGrupo) . ')';
        }

        $sql             = '
        SELECT
        a.modelo_id,
        a.modelo_descricao,
        a.modelo_contexto,
        a.tipotitulo_id
        FROM documento__modelo a
        LEFT JOIN acad_curso c ON c.curso_id = a.curso_id
        LEFT JOIN acad_curso c2 ON c2.area_id = a.area_id
        LEFT JOIN org_campus cc ON cc.camp_id = a.camp_id
        LEFT JOIN documento__modelo_grupo_permissao docGrupo USING (modelo_id)
        -- CONDITIONS --
        GROUP BY a.modelo_id
        ORDER BY a.modelo_descricao';
        $modeloDescricao = false;
        $modeloId        = false;

        if ($params['q']) {
            $modeloDescricao = $params['q'];
        } elseif ($params['query']) {
            $modeloDescricao = $params['query'];
        }

        if ($params['modeloId']) {
            $modeloId = $params['modeloId'];
        }

        $parameters['modeloDescricao'] = "{$modeloDescricao}%";

        $sqlAdd[] = 'a.modelo_descricao LIKE :modeloDescricao';
        $sqlAdd[] = 'curdate() between date(a.modelo_data_inicio) AND date(a.modelo_data_fim)';

        if ($modeloId) {
            $modeloId = explode(',', $modeloId);
            $sqlAdd[] = 'a.modelo_id NOT IN(' . $modeloId . ')';
        }

        if ($params['cursoId']) {
            $parameters['cursoId'] = $params['cursoId'];

            $sqlAdd[] = '(a.curso_id = :cursoId OR a.curso_id IS NULL)';
            $sqlAdd[] = '((c2.curso_id = :cursoId AND a.area_id IS NOT NULL) OR a.area_id IS NULL)';
        }

        if ($params['campId']) {
            $parameters['campId'] = $params['campId'];

            $sqlAdd[] = '(a.camp_id = :campId OR a.camp_id IS NULL)';
        }

        if ($params['modeloContexto']) {
            $arrModeloContexto = $params['modeloContexto'];
            $arrModeloContexto = !is_array($arrModeloContexto) ? explode(',', $arrModeloContexto) : $arrModeloContexto;

            $sqlContextoAdd = array();

            foreach ($arrModeloContexto as $c => $modeloContexto) {
                $parameters['modeloContexto' . $c] = $modeloContexto;
                $sqlContextoAdd[]                  = '(a.modelo_contexto = :modeloContexto' . $c . ')';
            }

            $sqlAdd[] = '(' . implode(' OR ', $sqlContextoAdd) . ')';
        }

        if ($sqlAdd) {
            $sqlAdd = 'WHERE ' . implode(' AND ', $sqlAdd);
            $sql    = str_replace('-- CONDITIONS --', $sqlAdd, $sql);
        }

        $result = $this->executeQueryWithParam($sql, $parameters)->fetchAll();

        return $result;
    }

    public function save(array &$arrDados)
    {
        $arrDados['modeloConteudo'] = str_replace(
            array('&quot;', '&#39;'),
            array('"', "'"),
            $arrDados['modeloConteudo']
        );

        if (!$this->valida($arrDados)) {
            return false;
        }

        $serviceDocumentoTipo             = new \Documentos\Service\DocumentoTipo($this->getEm());
        $serviceOrgCampus                 = new \Organizacao\Service\OrgCampus($this->getEm());
        $serviceAcadCurso                 = new \Matricula\Service\AcadCurso($this->getEm());
        $serviceAcessoGrupo               = new \Acesso\Service\AcessoGrupo($this->getEm());
        $serviceAcadgeralAreaConhecimento = new \Matricula\Service\AcadgeralAreaConhecimento($this->getEm());
        $serviceFinanceiroTituloTipo      = new \Financeiro\Service\FinanceiroTituloTipo($this->getEm());

        try {
            $this->getEm()->beginTransaction();

            if ($arrDados['modeloId']) {
                /** @var $objDocumentoModelo \Documentos\Entity\DocumentoModelo */
                $objDocumentoModelo = $this->getRepository()->find($arrDados['modeloId']);

                if (!$objDocumentoModelo) {
                    $this->setLastError('Registro de modelo não existe!');

                    return false;
                }
            } else {
                $objDocumentoModelo = new \Documentos\Entity\DocumentoModelo();
            }

            if ($arrDados['grupoId']) {
                $arrCollectionGrupo = new ArrayCollection(
                    $serviceAcessoGrupo->getArrayAcessoGrupoPeloId($arrDados['grupoId'])
                );

                $objDocumentoModelo->setGrupo($arrCollectionGrupo);
            }

            if ($arrDados['tdoc']) {
                /** @var $objDocumentoTipo \Documentos\Entity\DocumentoTipo */
                $objDocumentoTipo = $serviceDocumentoTipo->getRepository()->find($arrDados['tdoc']);

                if (!$objDocumentoTipo) {
                    $this->setLastError('Registro de tipo não existe!');

                    return false;
                }

                $objDocumentoModelo->setTdoc($objDocumentoTipo);
            } else {
                $objDocumentoModelo->setTdoc(null);
            }

            if ($arrDados['camp']) {
                /** @var $objOrgCampus \Organizacao\Entity\OrgCampus */
                $objOrgCampus = $serviceOrgCampus->getRepository()->find($arrDados['camp']);

                if (!$objOrgCampus) {
                    $this->setLastError('Registro de câmpus não existe!');

                    return false;
                }

                $objDocumentoModelo->setCamp($objOrgCampus);
            } else {
                $objDocumentoModelo->setCamp(null);
            }

            if ($arrDados['curso']) {
                /** @var $objAcadCurso \Matricula\Entity\AcadCurso */
                $objAcadCurso = $serviceAcadCurso->getRepository()->find($arrDados['curso']);

                if (!$objAcadCurso) {
                    $this->setLastError('Registro de curso não existe!');

                    return false;
                }

                $objDocumentoModelo->setCurso($objAcadCurso);
            } else {
                $objDocumentoModelo->setCurso(null);
            }

            if ($arrDados['area']) {
                /** @var $objAcadgeralAreaConhecimento \Matricula\Entity\AcadgeralAreaConhecimento */
                $objAcadgeralAreaConhecimento = $serviceAcadgeralAreaConhecimento->getRepository()->find(
                    $arrDados['area']
                );

                if (!$objAcadgeralAreaConhecimento) {
                    $this->setLastError('Registro de área conhecimento não existe!');

                    return false;
                }

                $objDocumentoModelo->setArea($objAcadgeralAreaConhecimento);
            } else {
                $objDocumentoModelo->setArea(null);
            }

            if ($arrDados['tipotitulo']) {
                /** @var $objFinanceiroTituloTipo \Financeiro\Entity\FinanceiroTituloTipo */
                $objFinanceiroTituloTipo = $serviceFinanceiroTituloTipo->getRepository()->find($arrDados['tipotitulo']);

                if (!$objFinanceiroTituloTipo) {
                    $this->setLastError('Registro de tipo de título não existe!');

                    return false;
                }

                $objDocumentoModelo->setTipotitulo($objFinanceiroTituloTipo);
            } else {
                $objDocumentoModelo->setTipotitulo(null);
            }

            $objDocumentoModelo->setModeloDataInicio($arrDados['modeloDataInicio']);
            $objDocumentoModelo->setModeloDataFim($arrDados['modeloDataFim']);
            $objDocumentoModelo->setModeloDescricao($arrDados['modeloDescricao']);
            $objDocumentoModelo->setModeloConteudo($arrDados['modeloConteudo']);
            $objDocumentoModelo->setModeloContexto($arrDados['modeloContexto']);

            $this->getEm()->persist($objDocumentoModelo);
            $this->getEm()->flush($objDocumentoModelo);

            $this->getEm()->commit();

            $arrDados['modeloId'] = $objDocumentoModelo->getModeloId();

            return true;
        } catch (\Exception $e) {
            $this->setLastError('Não foi possível salvar o registro de modelo!<br>' . $e->getMessage());
        }

        return false;
    }

    public function valida($arrParam)
    {
        $errors = array();

        if (!$arrParam['tdoc']) {
            $errors[] = 'Por favor preencha o campo "código tipo de documento"!';
        }

        if (!$arrParam['modeloDataInicio']) {
            $errors[] = 'Por favor preencha o campo "início data"!';
        }

        if (!$arrParam['modeloDataFim']) {
            $errors[] = 'Por favor preencha o campo "fim data"!';
        }

        if (!$arrParam['modeloContexto']) {
            $errors[] = 'Por favor preencha o campo "contexto"!';
        }

        if (!$arrParam['grupoId']) {
            $errors[] = 'Por favor preencha o campo "Grupos"!';
        }

        if (!in_array($arrParam['modeloContexto'], self::getModeloContexto())) {
            $errors[] = 'Por favor selecione um valor válido para o campo "contexto"!';
        }

        if (!$arrParam['modeloConteudo']) {
            $errors[] = 'Por favor preencha o campo "conteúdo"!';
        } else {
            try {
                $loader    = new \Twig_Loader_Filesystem(array());
                $twig      = new \Twig_Environment($loader, array());
                $tplModelo = $twig->createTemplate($arrParam['modeloConteudo']);
                $tplModelo->render(array());
            } catch (\Exception $e) {
                $errors[] = 'Existe um ou mais erros no conteúdo do modelo!';
            }
        }

        if (!empty($errors)) {
            $this->setLastError(implode("<br>", $errors));

            return false;
        }

        return true;
    }

    public function getDataForDatatables($data)
    {
        $query = "
        SELECT
            m.*,
            camp_nome,
            curso_nome,
            area_descricao,
            tdoc_descricao
        FROM documento__modelo m
        LEFT JOIN documento__tipo USING (tdoc_id)
        LEFT JOIN org_campus USING (camp_id)
        LEFT JOIN acad_curso USING (curso_id)
        LEFT JOIN acadgeral__area_conhecimento a ON m.area_id=a.area_id
        ";

        $result = parent::paginationDataTablesAjax($query, $data, null, false);

        return $result;
    }

    public function getArray($modeloId)
    {
        /** @var $objDocumentoModelo \Documentos\Entity\DocumentoModelo */
        $objDocumentoModelo = $this->getRepository()->find($modeloId);

        $serviceDocumentoTipo             = new \Documentos\Service\DocumentoTipo($this->getEm());
        $serviceOrgCampus                 = new \Organizacao\Service\OrgCampus($this->getEm());
        $serviceAcadCurso                 = new \Matricula\Service\AcadCurso($this->getEm());
        $serviceAcadgeralAreaConhecimento = new \Matricula\Service\AcadgeralAreaConhecimento($this->getEm());
        $serviceFinanceiroTituloTipo      = new \Financeiro\Service\FinanceiroTituloTipo($this->getEm());

        try {
            $arrDados = $objDocumentoModelo->toArray();

            if ($arrDados['tdoc']) {
                $arrDocumentoTipo = $serviceDocumentoTipo->getArrSelect2(['id' => $arrDados['tdoc']]);
                $arrDados['tdoc'] = $arrDocumentoTipo ? $arrDocumentoTipo[0] : null;
            }

            if ($arrDados['camp']) {
                $arrOrgCampus     = $serviceOrgCampus->getArrSelect2(['id' => $arrDados['camp']]);
                $arrDados['camp'] = $arrOrgCampus ? $arrOrgCampus[0] : null;
            }

            if ($arrDados['curso']) {
                $arrAcadCurso      = $serviceAcadCurso->getArrSelect2(['id' => $arrDados['curso']]);
                $arrDados['curso'] = $arrAcadCurso ? $arrAcadCurso[0] : null;
            }

            if ($arrDados['area']) {
                $arrAcadgeralAreaConhecimento = $serviceAcadgeralAreaConhecimento->getArrSelect2(
                    ['id' => $arrDados['area']]
                );
                $arrDados['area']             = $arrAcadgeralAreaConhecimento ? $arrAcadgeralAreaConhecimento[0] : null;
            }

            if ($arrDados['tipotitulo']) {
                $arrFinanceiroTituloTipo = $serviceFinanceiroTituloTipo->getArrSelect2(
                    ['id' => $arrDados['tipotitulo']]
                );
                $arrDados['tipotitulo']  = $arrFinanceiroTituloTipo ? $arrFinanceiroTituloTipo[0] : null;
            }
        } catch (\Exception $e) {
            return array();
        }

        return $arrDados;
    }

    /**
     * @param $arrDados
     * @return mixed
     */
    public function formataDadosPost(&$arrDados)
    {
        $serviceDocumentoTipo             = new \Documentos\Service\DocumentoTipo($this->getEm());
        $serviceOrgCampus                 = new \Organizacao\Service\OrgCampus($this->getEm());
        $serviceAcadCurso                 = new \Matricula\Service\AcadCurso($this->getEm());
        $serviceAcadgeralAreaConhecimento = new \Matricula\Service\AcadgeralAreaConhecimento($this->getEm());
        $serviceFinanceiroTituloTipo      = new \Financeiro\Service\FinanceiroTituloTipo($this->getEm());

        if (is_array($arrDados['tdoc']) && !$arrDados['tdoc']['text']) {
            $arrDados['tdoc'] = $arrDados['tdoc']['tdocId'];
        }

        if ($arrDados['tdoc'] && is_string($arrDados['tdoc'])) {
            $arrDados['tdoc'] = $serviceDocumentoTipo->getArrSelect2(array('id' => $arrDados['tdoc']));
            $arrDados['tdoc'] = $arrDados['tdoc'] ? $arrDados['tdoc'][0] : null;
        }

        if (is_array($arrDados['camp']) && !$arrDados['camp']['text']) {
            $arrDados['camp'] = $arrDados['camp']['campId'];
        }

        if ($arrDados['camp'] && is_string($arrDados['camp'])) {
            $arrDados['camp'] = $serviceOrgCampus->getArrSelect2(array('id' => $arrDados['camp']));
            $arrDados['camp'] = $arrDados['camp'] ? $arrDados['camp'][0] : null;
        }

        if (is_array($arrDados['curso']) && !$arrDados['curso']['text']) {
            $arrDados['curso'] = $arrDados['curso']['cursoId'];
        }

        $arrDados['arrGrupo'] = $this->getArrSelect2DocumentoGrupo($arrDados);

        if ($arrDados['curso'] && is_string($arrDados['curso'])) {
            $arrDados['curso'] = $serviceAcadCurso->getArrSelect2(array('id' => $arrDados['curso']));
            $arrDados['curso'] = $arrDados['curso'] ? $arrDados['curso'][0] : null;
        }

        if (is_array($arrDados['area']) && !$arrDados['area']['text']) {
            $arrDados['area'] = $arrDados['area']['areaId'];
        }

        if ($arrDados['area'] && is_string($arrDados['area'])) {
            $arrDados['area'] = $serviceAcadgeralAreaConhecimento->getArrSelect2(array('id' => $arrDados['area']));
            $arrDados['area'] = $arrDados['area'] ? $arrDados['area'][0] : null;
        }

        if ($arrDados['tipotitulo'] && is_string($arrDados['tipotitulo'])) {
            $arrDados['tipotitulo'] = $serviceFinanceiroTituloTipo->getArrSelect2(
                array('id' => $arrDados['tipotitulo'])
            );
            $arrDados['tipotitulo'] = $arrDados['tipotitulo'] ? $arrDados['tipotitulo'][0] : null;
        }

        return $arrDados;
    }

    public function getArrSelect2($params = array())
    {
        $params = array_merge(array('key' => 'id', 'value' => 'text', 'id' => false), $params);

        $arrParam = array();

        if ($params['id']) {
            $arrParam['modeloId'] = $params['id'];
        }

        $arrEntities = $this->getRepository()->findBy($arrParam, ['modeloDescricao' => 'asc']);

        $arrEntitiesArr = array();
        /* @var $objEntity \Documentos\Entity\DocumentoModelo */
        foreach ($arrEntities as $objEntity) {
            $arrEntity = $objEntity->toArray();

            $arrEntity[$params['key']]   = $objEntity->getModeloId();
            $arrEntity[$params['value']] = $objEntity->getModeloDescricao();

            $arrEntitiesArr[] = $arrEntity;
        }

        return $arrEntitiesArr;
    }

    public function remover($param)
    {
        if (!$param['modeloId']) {
            $this->setLastError('Para remover um registro de modelo é necessário informar o código.');

            return false;
        }

        try {
            /** @var $objDocumentoModelo \Documentos\Entity\DocumentoModelo */
            $objDocumentoModelo = $this->getRepository()->find($param['modeloId']);

            $this->getEm()->beginTransaction();
            $this->getEm()->remove($objDocumentoModelo);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao remover registro de modelo.');

            return false;
        }

        return true;
    }

    public function setarDependenciasView(\Zend\View\Model\ViewModel &$view)
    {
        $serviceDocumentoTipo = new \Documentos\Service\DocumentoTipo($this->getEm());
        //$serviceOrgCampus                 = new \Organizacao\Service\OrgCampus($this->getEm());
        //$serviceAcadCurso                 = new \Matricula\Service\AcadCurso($this->getEm());
        $serviceAcadgeralAreaConhecimento = new \Matricula\Service\AcadgeralAreaConhecimento($this->getEm());
        $serviceFinanceiroTituloTipo      = new \Financeiro\Service\FinanceiroTituloTipo($this->getEm());

        $serviceDocumentoTipo->setarDependenciasView($view);
        //$serviceOrgCampus->setarDependenciasView($view);
        //$serviceAcadCurso->setarDependenciasView($view);
        $serviceAcadgeralAreaConhecimento->setarDependenciasView($view);
        $serviceFinanceiroTituloTipo->setarDependenciasView($view);
        $view->setVariable("arrModeloContexto", $this->getArrSelect2ModeloContexto());
    }

    /**
     * @param $modelo
     * @param $arrVariavies
     * @return array
     * @throws \Exception
     */
    public function renderizarModelo($modelo, $arrVariavies)
    {
        try {
            /** @var \Documentos\Entity\DocumentoModelo $objModelo */
            $objModelo = $this->getRepository()->find($modelo);
            $arrDados  = [];

            $serviceAlunoCurso       = new \Matricula\Service\AcadgeralAlunoCurso($this->getEm());
            $serviceAluno            = new \Matricula\Service\AcadgeralAluno($this->getEm());
            $serviceOrgCampus        = new \Organizacao\Service\OrgCampus($this->getEm());
            $serviceDocumentoEmissao = new \Documentos\Service\DocumentoEmissao($this->getEm());
            $servicePessoa           = new \Pessoa\Service\Pessoa($this->getEm());
            $serviceTituloValores    = new \Financeiro\Service\FinanceiroValores($this->getEm());
            $serviceTitulo           = new \Financeiro\Service\FinanceiroTitulo($this->getEm());

            $arrRetorno = ['arquivoNome' => $objModelo->getModeloDescricao(), 'arquivoConteudo' => ''];

            if ($objModelo->getModeloContexto() == self::MODELO_CONTEXTO_ALUNO_CURSO && $arrVariavies['alunocursoId']) {
                $arrDados['alunoCurso'] = $serviceAlunoCurso->getAlunocursoArray($arrVariavies['alunocursoId']);
                $arrDados['aluno']      = $arrDados['alunoCurso']['aluno'];
                $arrDados['pessoa']     = $arrDados['alunoCurso']['aluno'];
                $arrDados['campId']     = $arrDados['alunoCurso']['campId'];
                $arrRetorno['arquivoNome'] .= "-" . $arrVariavies['alunocursoId'];
            } elseif ($objModelo->getModeloContexto() == self::MODELO_CONTEXTO_ALUNO && $arrVariavies['alunoId']) {
                $arrDados['aluno']      = $serviceAluno->getArray($arrVariavies['alunoId']);
                $arrDados['alunoCurso'] = $arrDados['aluno']['alunoCurso'][0];
                $arrDados['pessoa']     = $arrDados['aluno'];
                $arrRetorno['arquivoNome'] .= "-" . $arrVariavies['alunoId'];
            } elseif ($objModelo->getModeloContexto() == self::MODELO_CONTEXTO_PESSOA && $arrVariavies['pesId']) {
                $arrDados['pessoa'] = $servicePessoa->getArray($arrVariavies['pesId']);
                $arrRetorno['arquivoNome'] .= "-" . $arrVariavies['pesId'];
            }

            $arrDados['ies'] = $serviceOrgCampus->retornaDadosInstituicao($arrVariavies['campId']);

            $arrRetorno['arquivoNome'] = strtolower(str_replace(' ', '-', $arrRetorno['arquivoNome']));

            $loader    = new \Twig_Loader_Filesystem(array());
            $twig      = new \Twig_Environment($loader, array());
            $tplModelo = $twig->createTemplate($objModelo->getModeloConteudo());

            try {
                $arrRetorno['arquivoConteudo'] = $tplModelo->render($arrDados);

                if ($objModelo->getTipotitulo()) {
                    if ($arrVariavies['valorId']) {
                        /** @var \Financeiro\Entity\FinanceiroValores $objValores */
                        $objValores = $serviceTituloValores->getRepository()->find($arrVariavies['valorId']);

                        if (!$objValores) {
                            $this->setLastError("É necessário selecionar um valor !");

                            return false;
                        }

                        if ($objModelo->getTipotitulo() != $objValores->getTipotitulo()) {
                            $this->setLastError(
                                "Erro na referência de títulos<br>Tente novamente ou contate o administrador"
                            );

                            return false;
                        }

                        $arrTitulo                         = [];
                        $arrTitulo['pes']                  = $arrDados['pessoa']['pesId'];
                        $arrTitulo['alunocurso']           = $arrVariavies['alunocursoId'];
                        $arrTitulo['tipotitulo']           = $objModelo->getTipotitulo()->getTipotituloId();
                        $arrTitulo['tituloDataVencimento'] = $arrVariavies['dataVencimento'];
                        $arrTitulo['tituloNumeroParcelas'] = $objValores->getValoresParcela();
                        $arrTitulo['tituloValorParcelas']  = $objValores->getValoresPreco();
                        $arrTitulo['tituloObservacoes']    = (
                            'Emissão de ' . $objModelo->getModeloDescricao() . ' ' .
                            ' em ' . Date('d/m/Y')
                        );

                        if ($serviceTitulo->criarNovosTitulos($arrTitulo, false, false) === false) {
                            $this->setLastError(
                                'Falha ao emitir título referente a emissão de documento!<br>' .
                                $serviceTitulo->getLastError()
                            );
                            $arrRetorno = [];
                        }
                    }
                }

                $arrDadosEmissao = [
                    'modelo' => $objModelo->getModeloId(),
                    'pes'    => $arrDados['pessoa']['pesId'],
                ];

                if (!$serviceDocumentoEmissao->save($arrDadosEmissao)) {
                    $this->setLastError(
                        'Falha ao gravar log de emissão de documento!<br>' . $serviceTitulo->getLastError()
                    );
                    $arrRetorno = [];
                }
            } catch (\Exception $ex) {
                $this->setLastError('Falha ao renderizar modelo! ' . $ex->getMessage());
                $arrRetorno = [];
            }
        } catch (\Exception $ex) {
            $this->setLastError('Falha ao emitir documento! ' . $ex->getMessage());
            $arrRetorno = [];
        }

        return $arrRetorno;
    }
}
?>