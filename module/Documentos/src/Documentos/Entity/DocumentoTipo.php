<?php

namespace Documentos\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * DocumentoTipo
 *
 * @ORM\Table(name="documento__tipo")
 * @ORM\Entity
 * @LG\LG(id="tdocId",label="TdocDescricao")
 * @Jarvis\Jarvis(title="Listagem de tipo de documento",icon="fa fa-table")
 */
class DocumentoTipo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="tdoc_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="tdoc_id")
     * @LG\Labels\Attributes(text="código")
     * @LG\Querys\Conditions(type="=")
     */
    private $tdocId;

    /**
     * @var string
     *
     * @ORM\Column(name="tdoc_descricao", type="string", nullable=false, length=45)
     * @LG\Labels\Property(name="tdoc_descricao")
     * @LG\Labels\Attributes(text="descrição de documento")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $tdocDescricao;

    /**
     * @return integer
     */
    public function getTdocId()
    {
        return $this->tdocId;
    }

    /**
     * @param integer $tdocId
     * @return DocumentoTipo
     */
    public function setTdocId($tdocId)
    {
        $this->tdocId = $tdocId;

        return $this;
    }

    /**
     * @return string
     */
    public function getTdocDescricao()
    {
        return $this->tdocDescricao;
    }

    /**
     * @param string $tdocDescricao
     * @return DocumentoTipo
     */
    public function setTdocDescricao($tdocDescricao)
    {
        $this->tdocDescricao = $tdocDescricao;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'tdocId'        => $this->getTdocId(),
            'tdocDescricao' => $this->getTdocDescricao(),
        );

        return $array;
    }
}
