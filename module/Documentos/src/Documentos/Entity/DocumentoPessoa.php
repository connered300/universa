<?php

namespace Documentos\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DocumentoPessoa
 *
 * @ORM\Table(name="documento__pessoa", indexes={@ORM\Index(name="fk_documentos_grupo_pessoa_fisica_documentos_grupo1_idx", columns={"docgrupo_id"}), @ORM\Index(name="fk_documentos_grupo_pessoa_fisica_pessoa_fisica1_idx", columns={"pes_id"})})
 * @ORM\Entity
 */
class DocumentoPessoa
{
    /**
     * @var integer
     *
     * @ORM\Column(name="docpessoa_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $docpessoaId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="docpessoa_entrega", type="datetime", nullable=true)
     */
    private $docpessoaEntrega;

    /**
     * @var string
     *
     * @ORM\Column(name="docpessoa_status", type="string", nullable=false)
     */
    private $docpessoaStatus = 'Não';

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="DocumentoGrupo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="docgrupo_id", referencedColumnName="docgrupo_id")
     * })
     */
    private $docgrupo;

    /**
     * @var \Pessoa\Entity\Pessoa
     *
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     */
    private $pes;

    public function __construct(array $data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return int
     */
    public function getDocpessoaId()
    {
        return $this->docpessoaId;
    }

    /**
     * @param int $docpessoaId
     */
    public function setDocpessoaId($docpessoaId)
    {
        $this->docpessoaId = $docpessoaId;
    }

    /**
     * @return \DateTime
     */
    public function getDocpessoaEntrega()
    {
        return $this->docpessoaEntrega;
    }

    /**
     * @param \DateTime $docpessoaEntrega
     */
    public function setDocpessoaEntrega($docpessoaEntrega)
    {
        $this->docpessoaEntrega = $docpessoaEntrega;
    }

    /**
     * @return string
     */
    public function getDocpessoaStatus()
    {
        return $this->docpessoaStatus;
    }

    /**
     * @param string $docpessoaStatus
     */
    public function setDocpessoaStatus($docpessoaStatus)
    {
        $this->docpessoaStatus = $docpessoaStatus;
    }

    /**
     * @return DocumentoGrupo
     */
    public function getDocgrupo()
    {
        return $this->docgrupo;
    }

    /**
     * @param DocumentoGrupo $docgrupo
     */
    public function setDocgrupo($docgrupo)
    {
        $this->docgrupo = $docgrupo;
    }

    /**
     * @return \Pessoa\Entity\Pessoa
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @param \Pessoa\Entity\Pessoa $pes
     */
    public function setPes($pes)
    {
        $this->pes = $pes;
    }

    public function toArray()
    {
        return [
            'docpessoaId'      => $this->getDocpessoaId(),
            'docgrupo'         => $this->getDocgrupo()->getDocgrupoId(),
            'pes'              => $this->getPes()->getPesId(),
            'docpessoaEntrega' => $this->getDocpessoaEntrega(),
            'docpessoaStatus'  => $this->getDocpessoaStatus(),
        ];
    }

}
