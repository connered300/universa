<?php

namespace Documentos\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * DocumentoModelo
 *
 * @ORM\Table(name="documento__modelo")
 * @ORM\Entity
 * @LG\LG(id="modeloId",label="ModeloDescricao")
 * @Jarvis\Jarvis(title="Listagem de modelo",icon="fa fa-table")
 */
class DocumentoModelo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="modelo_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="modelo_id")
     * @LG\Labels\Attributes(text="código modelo")
     * @LG\Querys\Conditions(type="=")
     */
    private $modeloId;

    /**
     * @var \Documentos\Entity\DocumentoTipo
     * @ORM\ManyToOne(targetEntity="Documentos\Entity\DocumentoTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tdoc_id", referencedColumnName="tdoc_id")
     * })
     */
    private $tdoc;

    /**
     * @var \Organizacao\Entity\OrgCampus
     * @ORM\ManyToOne(targetEntity="Organizacao\Entity\OrgCampus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="camp_id", referencedColumnName="camp_id")
     * })
     */
    private $camp;

    /**
     * @var \Matricula\Entity\AcadCurso
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="curso_id", referencedColumnName="curso_id")
     * })
     */
    private $curso;

    /**
     * @var \Matricula\Entity\AcadgeralAreaConhecimento
     * @ORM\ManyToOne(targetEntity="Matricula\Entity\AcadgeralAreaConhecimento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="area_id", referencedColumnName="area_id")
     * })
     */
    private $area;

    /**
     * @var \Financeiro\Entity\FinanceiroTituloTipo
     * @ORM\ManyToOne(targetEntity="Financeiro\Entity\FinanceiroTituloTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipotitulo_id", referencedColumnName="tipotitulo_id")
     * })
     */
    private $tipotitulo;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="modelo_data_inicio", type="date", nullable=false)
     * @LG\Labels\Property(name="modelo_data_inicio")
     * @LG\Labels\Attributes(text="início data")
     * @LG\Querys\Conditions(type="=")
     */
    private $modeloDataInicio;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="modelo_data_fim", type="date", nullable=false)
     * @LG\Labels\Property(name="modelo_data_fim")
     * @LG\Labels\Attributes(text="fim data")
     * @LG\Querys\Conditions(type="=")
     */
    private $modeloDataFim;

    /**
     * @var string
     *
     * @ORM\Column(name="modelo_descricao", type="string", nullable=true, length=150)
     * @LG\Labels\Property(name="modelo_descricao")
     * @LG\Labels\Attributes(text="descrição")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $modeloDescricao;

    /**
     * @var string
     *
     * @ORM\Column(name="modelo_conteudo", type="text", nullable=true)
     * @LG\Labels\Property(name="modelo_conteudo")
     * @LG\Labels\Attributes(text="conteúdo")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $modeloConteudo;

    /**
     * @var string
     *
     * @ORM\Column(name="modelo_contexto", type="string", nullable=false, length=6)
     * @LG\Labels\Property(name="modelo_contexto")
     * @LG\Labels\Attributes(text="contexto")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $modeloContexto;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Acesso\Entity\AcessoGrupo")
     * @ORM\JoinTable(name="documento__modelo_grupo_permissao",
     *   joinColumns={
     *     @ORM\JoinColumn(name="modelo_id", referencedColumnName="modelo_id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="grupo_id", referencedColumnName="id")
     *   }
     * )
     */
    private $grupo;

    /**
     * @return integer
     */
    public function getModeloId()
    {
        return $this->modeloId;
    }

    /**
     * @param integer $modeloId
     * @return DocumentoModelo
     */
    public function setModeloId($modeloId)
    {
        $this->modeloId = $modeloId;

        return $this;
    }

    /**
     * @return \Documentos\Entity\DocumentoTipo
     */
    public function getTdoc()
    {
        return $this->tdoc;
    }

    /**
     * @param \Documentos\Entity\DocumentoTipo $tdoc
     * @return DocumentoModelo
     */
    public function setTdoc($tdoc)
    {
        $this->tdoc = $tdoc;

        return $this;
    }

    /**
     * @return \Organizacao\Entity\OrgCampus
     */
    public function getCamp()
    {
        return $this->camp;
    }

    /**
     * @param \Organizacao\Entity\OrgCampus $camp
     * @return DocumentoModelo
     */
    public function setCamp($camp)
    {
        $this->camp = $camp;

        return $this;
    }

    /**
     * @return \Matricula\Entity\AcadCurso
     */
    public function getCurso()
    {
        return $this->curso;
    }

    /**
     * @param \Matricula\Entity\AcadCurso $curso
     * @return DocumentoModelo
     */
    public function setCurso($curso)
    {
        $this->curso = $curso;

        return $this;
    }

    /**
     * @return \Matricula\Entity\AcadgeralAreaConhecimento
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param \Matricula\Entity\AcadgeralAreaConhecimento $area
     * @return DocumentoModelo
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * @return \Financeiro\Entity\FinanceiroTituloTipo
     */
    public function getTipotitulo()
    {
        return $this->tipotitulo;
    }

    /**
     * @param \Financeiro\Entity\FinanceiroTituloTipo $tipotitulo
     * @return DocumentoModelo
     */
    public function setTipotitulo($tipotitulo)
    {
        $this->tipotitulo = $tipotitulo;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getModeloDataInicio($format = false)
    {
        $modeloDataInicio = $this->modeloDataInicio;

        if ($format && $modeloDataInicio) {
            $modeloDataInicio = $modeloDataInicio->format('d/m/Y');
        }

        return $modeloDataInicio;
    }

    /**
     * @param \Datetime $modeloDataInicio
     * @return DocumentoModelo
     */
    public function setModeloDataInicio($modeloDataInicio)
    {
        if ($modeloDataInicio) {
            if (is_string($modeloDataInicio)) {
                $modeloDataInicio = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $modeloDataInicio
                );
                $modeloDataInicio = new \Datetime($modeloDataInicio);
            }
        } else {
            $modeloDataInicio = null;
        }
        $this->modeloDataInicio = $modeloDataInicio;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getModeloDataFim($format = false)
    {
        $modeloDataFim = $this->modeloDataFim;

        if ($format && $modeloDataFim) {
            $modeloDataFim = $modeloDataFim->format('d/m/Y');
        }

        return $modeloDataFim;
    }

    /**
     * @param \Datetime $modeloDataFim
     * @return DocumentoModelo
     */
    public function setModeloDataFim($modeloDataFim)
    {
        if ($modeloDataFim) {
            if (is_string($modeloDataFim)) {
                $modeloDataFim = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $modeloDataFim
                );
                $modeloDataFim = new \Datetime($modeloDataFim);
            }
        } else {
            $modeloDataFim = null;
        }
        $this->modeloDataFim = $modeloDataFim;

        return $this;
    }

    /**
     * @return string
     */
    public function getModeloDescricao()
    {
        return $this->modeloDescricao;
    }

    /**
     * @param string $modeloDescricao
     * @return DocumentoModelo
     */
    public function setModeloDescricao($modeloDescricao)
    {
        $this->modeloDescricao = $modeloDescricao;

        return $this;
    }

    /**
     * @return string
     */
    public function getModeloConteudo()
    {
        return $this->modeloConteudo;
    }

    /**
     * @param string $modeloConteudo
     * @return DocumentoModelo
     */
    public function setModeloConteudo($modeloConteudo)
    {
        $this->modeloConteudo = $modeloConteudo;

        return $this;
    }

    /**
     * @return string
     */
    public function getModeloContexto()
    {
        return $this->modeloContexto;
    }

    /**
     * @param string $modeloContexto
     * @return DocumentoModelo
     */
    public function setModeloContexto($modeloContexto)
    {
        $this->modeloContexto = $modeloContexto;

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGrupo()
    {
        return $this->grupo;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $grupo
     * @return DocumentoModelo
     */
    public function setGrupo($grupo)
    {
        $this->grupo = $grupo;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {

        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'modeloId'         => $this->getModeloId(),
            'tdoc'             => $this->getTdoc(),
            'camp'             => $this->getCamp(),
            'curso'            => $this->getCurso(),
            'area'             => $this->getArea(),
            'tipotitulo'       => $this->getTipotitulo(),
            'modeloDataInicio' => $this->getModeloDataInicio(true),
            'modeloDataFim'    => $this->getModeloDataFim(true),
            'modeloDescricao'  => $this->getModeloDescricao(),
            'modeloConteudo'   => $this->getModeloConteudo(),
            'modeloContexto'   => $this->getModeloContexto(),
            'grupo'            => $this->getGrupo()->toArray()
        );

        $array['tdoc']       = $this->getTdoc() ? $this->getTdoc()->getTdocId() : null;
        $array['camp']       = $this->getCamp() ? $this->getCamp()->getCampId() : null;
        $array['curso']      = $this->getCurso() ? $this->getCurso()->getCursoId() : null;
        $array['area']       = $this->getArea() ? $this->getArea()->getAreaId() : null;
        $array['tipotitulo'] = $this->getTipotitulo() ? $this->getTipotitulo()->getTipotituloId() : null;

        return $array;
    }
}
