<?php

namespace Documentos\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * DocumentoGrupo
 *
 * @ORM\Table(name="documento__grupo")
 * @ORM\Entity
 * @LG\LG(id="docgrupoId",label="DocgrupoDescricao")
 * @Jarvis\Jarvis(title="Listagem de grupo",icon="fa fa-table")
 */
class DocumentoGrupo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="docgrupo_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="docgrupo_id")
     * @LG\Labels\Attributes(text="código documento de grupo")
     * @LG\Querys\Conditions(type="=")
     */
    private $docgrupoId;

    /**
     * @var \Acesso\Entity\AcessoGrupo
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoGrupo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id", referencedColumnName="id")
     * })
     */
    private $id;

    /**
     * @var \Documentos\Entity\DocumentoTipo
     * @ORM\ManyToOne(targetEntity="Documentos\Entity\DocumentoTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tdoc_id", referencedColumnName="tdoc_id")
     * })
     */
    private $tdoc;

    /**
     * @var string
     *
     * @ORM\Column(name="docgrupo_exigencia", type="string", nullable=false, length=11)
     * @LG\Labels\Property(name="docgrupo_exigencia")
     * @LG\Labels\Attributes(text="exigencia de documento")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $docgrupoExigencia;

    /**
     * @var string
     *
     * @ORM\Column(name="docgrupo_formato", type="string", nullable=false, length=7)
     * @LG\Labels\Property(name="docgrupo_formato")
     * @LG\Labels\Attributes(text="formato de documento")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $docgrupoFormato;

    /**
     * @var string
     *
     * @ORM\Column(name="docgrupo_descricao", type="string", nullable=true, length=45)
     * @LG\Labels\Property(name="docgrupo_descricao")
     * @LG\Labels\Attributes(text="descrição de documento")
     * @LG\Querys\Conditions(type="LIKE")
     */
    private $docgrupoDescricao;

    /**
     * @return integer
     */
    public function getDocgrupoId()
    {
        return $this->docgrupoId;
    }

    /**
     * @param integer $docgrupoId
     * @return DocumentoGrupo
     */
    public function setDocgrupoId($docgrupoId)
    {
        $this->docgrupoId = $docgrupoId;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoGrupo
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param \Acesso\Entity\AcessoGrupo $id
     * @return DocumentoGrupo
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return \Documentos\Entity\DocumentoTipo
     */
    public function getTdoc()
    {
        return $this->tdoc;
    }

    /**
     * @param \Documentos\Entity\DocumentoTipo $tdoc
     * @return DocumentoGrupo
     */
    public function setTdoc($tdoc)
    {
        $this->tdoc = $tdoc;

        return $this;
    }

    /**
     * @return string
     */
    public function getDocgrupoExigencia()
    {
        return $this->docgrupoExigencia;
    }

    /**
     * @param string $docgrupoExigencia
     * @return DocumentoGrupo
     */
    public function setDocgrupoExigencia($docgrupoExigencia)
    {
        $this->docgrupoExigencia = $docgrupoExigencia;

        return $this;
    }

    /**
     * @return string
     */
    public function getDocgrupoFormato()
    {
        return $this->docgrupoFormato;
    }

    /**
     * @param string $docgrupoFormato
     * @return DocumentoGrupo
     */
    public function setDocgrupoFormato($docgrupoFormato)
    {
        $this->docgrupoFormato = $docgrupoFormato;

        return $this;
    }

    /**
     * @return string
     */
    public function getDocgrupoDescricao()
    {
        return $this->docgrupoDescricao;
    }

    /**
     * @param string $docgrupoDescricao
     * @return DocumentoGrupo
     */
    public function setDocgrupoDescricao($docgrupoDescricao)
    {
        $this->docgrupoDescricao = $docgrupoDescricao;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'docgrupoId'        => $this->getDocgrupoId(),
            'id'                => $this->getId(),
            'tdoc'              => $this->getTdoc(),
            'docgrupoExigencia' => $this->getDocgrupoExigencia(),
            'docgrupoFormato'   => $this->getDocgrupoFormato(),
            'docgrupoDescricao' => $this->getDocgrupoDescricao(),
        );

        $array['id']   = $this->getId() ? $this->getId()->getId() : null;
        $array['tdoc'] = $this->getTdoc() ? $this->getTdoc()->getTdocId() : null;

        return $array;
    }
}
