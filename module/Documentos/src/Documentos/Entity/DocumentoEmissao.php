<?php

namespace Documentos\Entity;

use Doctrine\ORM\Mapping as ORM;
use VersaSpine\ListGenerator\Annotations\Entity as LG;
use VersaSpine\ListGenerator\Annotations\Entity\Jarvis as Jarvis;

/**
 * DocumentoEmissao
 *
 * @ORM\Table(name="documento__emissao")
 * @ORM\Entity
 * @LG\LG(id="emissaoId",label="ModeloId")
 * @Jarvis\Jarvis(title="Listagem de emissão",icon="fa fa-table")
 */
class DocumentoEmissao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="emissao_id", type="integer", nullable=false, length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @LG\Labels\Property(name="emissao_id")
     * @LG\Labels\Attributes(text="código emissão")
     * @LG\Querys\Conditions(type="=")
     */
    private $emissaoId;

    /**
     * @var \Documentos\Entity\DocumentoModelo
     * @ORM\ManyToOne(targetEntity="Documentos\Entity\DocumentoModelo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="modelo_id", referencedColumnName="modelo_id")
     * })
     */
    private $modelo;

    /**
     * @var \Pessoa\Entity\Pessoa
     * @ORM\ManyToOne(targetEntity="Pessoa\Entity\Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pes_id", referencedColumnName="pes_id")
     * })
     */
    private $pes;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="emissao_data", type="datetime", nullable=true)
     * @LG\Labels\Property(name="emissao_data")
     * @LG\Labels\Attributes(text="data emissão")
     * @LG\Querys\Conditions(type="=")
     */
    private $emissaoData;

    /**
     * @var \Acesso\Entity\AcessoPessoas
     * @ORM\ManyToOne(targetEntity="Acesso\Entity\AcessoPessoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id_emissao", referencedColumnName="id")
     * })
     */
    private $usuarioIdEmissao;

    /**
     * @return integer
     */
    public function getEmissaoId()
    {
        return $this->emissaoId;
    }

    /**
     * @param integer $emissaoId
     * @return DocumentoEmissao
     */
    public function setEmissaoId($emissaoId)
    {
        $this->emissaoId = $emissaoId;

        return $this;
    }

    /**
     * @return \Documentos\Entity\DocumentoModelo
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * @param \Documentos\Entity\DocumentoModelo $modelo
     * @return DocumentoEmissao
     */
    public function setModelo($modelo)
    {
        $this->modelo = $modelo;

        return $this;
    }

    /**
     * @return \Pessoa\Entity\Pessoa
     */
    public function getPes()
    {
        return $this->pes;
    }

    /**
     * @param \Pessoa\Entity\Pessoa $pes
     * @return DocumentoEmissao
     */
    public function setPes($pes)
    {
        $this->pes = $pes;

        return $this;
    }

    /**
     * @param bool|false $format
     * @return \Datetime|string
     */
    public function getEmisasoData($format = false)
    {
        $emissaoData = $this->emissaoData;

        if ($format && $emissaoData) {
            $emissaoData = $emissaoData->format('d/m/Y H:i:s');
        }

        return $emissaoData;
    }

    /**
     * @param \Datetime $emissaoData
     * @return DocumentoEmissao
     */
    public function setEmisasoData($emissaoData)
    {
        if ($emissaoData) {
            if (is_string($emissaoData)) {
                $emissaoData = \VersaSpine\Service\AbstractService::formatDateAmericano(
                    $emissaoData
                );
                $emissaoData = new \Datetime($emissaoData);
            }
        } else {
            $emissaoData = new \DateTime();
        }
        $this->emissaoData = $emissaoData;

        return $this;
    }

    /**
     * @return \Acesso\Entity\AcessoPessoas
     */
    public function getUsuarioIdEmissao()
    {
        return $this->usuarioIdEmissao;
    }

    /**
     * @param \Acesso\Entity\AcessoPessoas $usuarioIdEmissao
     * @return DocumentoEmissao
     */
    public function setUsuarioIdEmissao($usuarioIdEmissao)
    {
        $this->usuarioIdEmissao = $usuarioIdEmissao;

        return $this;
    }

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        (new \Zend\Stdlib\Hydrator\ClassMethods())->hydrate($data, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array(
            'emissaoId'        => $this->getEmissaoId(),
            'modelo'           => $this->getModelo(),
            'pes'              => $this->getPes(),
            'emissaoData'      => $this->getEmisasoData(true),
            'usuarioIdEmissao' => $this->getUsuarioIdEmissao(),
        );

        $array['modelo']           = $this->getModelo() ? $this->getModelo()->getModeloId() : null;
        $array['pes']              = $this->getPes() ? $this->getPes()->getPesId() : null;
        $array['usuarioIdEmissao'] = $this->getUsuarioIdEmissao() ? $this->getUsuarioIdEmissao()->getId() : null;

        return $array;
    }
}
