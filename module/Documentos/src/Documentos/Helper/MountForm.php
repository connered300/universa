<?php
namespace Documentos\Helper;

use Zend\Form\Element;

class MountForm
{
    /**
     * Armazena um array com os documentos;
     * */
    private $pesDocumentos = array();

    /**
     * Armazena um array com os documentos obrigatórios;
     * */
    private $obrigatorios = array();

    /**
     * Armazena um array com os documentos facultativos;
     * */
    private $facultativos = array();

    /**
     * Armazena um array com os documentos que dependem do contexto.
     * */
    private $contexto = array();

    /**
     * Recebe a ViewHelper que será utilizada para criar os campos do formulário.
     * */
    public $helper;

    function __construct($nomeGrupo, $helper, $em, $pes = null)
    {
        $service = new \Documentos\Service\DocumentoGrupo($em);

        $this->helper = $helper;

        $arrayDpcumentos = array();

        if ($pes) {
            $pesQuery = "AND pes_id = " . $pes;
        } else {
            $pesQuery = "AND pes_id = -1";
        }

        $documentosGrupo = $service->executeQuery(
            "
          SELECT
              documento__grupo.docgrupo_id, tdoc_id, docgrupo_exigencia, tdoc_descricao, docgrupo_formato,
              COALESCE(docpessoa_status,'Não') as docpessoa_status
            FROM documento__grupo
              INNER JOIN acesso_grupo as grupo ON grupo.id = documento__grupo.id
              LEFT JOIN documento__tipo USING (tdoc_id)
              LEFT JOIN documento__pessoa ON documento__pessoa.docgrupo_id = documento__grupo.docgrupo_id {$pesQuery}
              LEFT JOIN pessoa USING (pes_id)
            WHERE grupo.grup_nome = '{$nomeGrupo}' GROUP BY docgrupo_id
        "
        )->fetchAll();

        foreach ($documentosGrupo as $documento) {
            switch ($documento['docgrupo_exigencia']) {
                case 'Obrigatório':
                    $this->obrigatorios[] = $documento;
                    break;
                case 'Facultativo':
                    $this->facultativos[] = $documento;
                    break;
                case 'Contexto':
                    $this->contexto[] = $documento;
                    break;
            }

            $arrayDpcumentos[] = array(
                $documento['docgrupo_id'],
                $documento['docpessoa_status']
            );
        }

        $this->pesDocumentos = $arrayDpcumentos;
    }

    /**
     * @return mixed
     */
    public function getObrigatorios()
    {
        return $this->obrigatorios;
    }

    /**
     * @param mixed $obrigatorios
     */
    public function setObrigatorios($obrigatorios)
    {
        $this->obrigatorios = $obrigatorios;
    }

    /**
     * @return mixed
     */
    public function getFacultativos()
    {
        return $this->facultativos;
    }

    /**
     * @param mixed $facultativos
     */
    public function setFacultativos($facultativos)
    {
        $this->facultativos = $facultativos;
    }

    /**
     * @return mixed
     */
    public function getContexto()
    {
        return $this->contexto;
    }

    /**
     * @param mixed $contexto
     */
    public function setContexto($contexto)
    {
        $this->contexto = $contexto;
    }

    /**
     * @return mixed
     */
    public function getPesDocumentos()
    {
        return $this->pesDocumentos;
    }

    /**
     * @param mixed $pesDocumentos
     */
    public function setPesDocumentos($pesDocumentos)
    {
        $this->pesDocumentos = $pesDocumentos;
    }

    /**
     * Esta função retorna o html com os formulários do tipo checkbox
     * */
    public function montaFormularios()
    {
        $html      = '';
        $intervalo = 0;
        $count     = 0;
        $maxCount  = 4;

        foreach ($this->obrigatorios as $obrigatorio) {
            $checked = "Não";

            foreach ($this->pesDocumentos as $doc) {
                if ($doc[0] == $obrigatorio['docgrupo_id']) {
                    $checked = $doc[1];
                }
            }

            $elemento = new Element\Radio('documentos[' . $intervalo . ']');
            $elemento->setAttributes(
                [
                    'col'              => '2',
                    'wrap'             => true,
                    'label'            => $obrigatorio['tdoc_descricao'],
                    'data-validations' => 'Required',
                    'value'            => $checked

                ]
            );
            $elemento->setValueOptions(
                [
                    'Sim' => 'Sim',
                    'Não' => 'Não'
                ]
            );

            if ($count == 0) {
                $html .= "<div class='row'>";
            }

            $html .= $this->helper->formInput($elemento);
            $html .= "<input type='hidden' name='documentoGrupo[" . $intervalo . "]' value='" . $obrigatorio['docgrupo_id'] . "'>";
            $intervalo++;
            $count++;

            if ($count == $maxCount) {
                $html .= "</div>";
                $count = 0;
            }
        }
        foreach ($this->facultativos as $facultativo) {
            $checked = "";

            foreach ($this->pesDocumentos as $doc) {
                if ($doc[0] == $facultativo['docgrupo_id']) {
                    $checked = $doc[1];
                }
            }

            $elemento = new Element\Radio('documentos[' . $intervalo . ']');
            $elemento->setAttributes(
                [
                    'col'              => '2',
                    'wrap'             => true,
                    'label'            => $facultativo['tdoc_descricao'],
                    'data-validations' => 'Required',
                    'value'            => $checked
                ]
            );
            $elemento->setValueOptions(
                [
                    'Sim' => 'Sim',
                    'Não' => 'Não'
                ]
            );

            if ($count == 0) {
                $html .= "<div class='row'>";
            }

            $html .= $this->helper->formInput($elemento);
            $html .= "<input type='hidden' name='documentoGrupo[" . $intervalo . "]' value='" . $facultativo['docgrupo_id'] . "'>";
            $intervalo++;
            $count++;

            if ($count == $maxCount) {
                $html .= "</div>";
                $count = 0;
            }
        }

        foreach ($this->contexto as $contexto) {
            $checked = "Não";

            foreach ($this->pesDocumentos as $doc) {
                if ($doc[0] == $contexto['docgrupo_id']) {
                    $checked = $doc[1] == "Dispensavel" ? 'Não Aplicavel' : $doc[1];
                }
            }

            $elemento = new Element\Radio('documentos[' . $intervalo . ']');
            $elemento->setAttributes(
                [
                    'col'              => '2',
                    'wrap'             => true,
                    'label'            => $contexto['tdoc_descricao'],
                    'data-validations' => 'Required',
                    'value'            => $checked
                ]
            );
            $elemento->setValueOptions(
                [
                    'Sim'           => 'Sim',
                    'Não'           => 'Não',
                    'Não Aplicavel' => 'Não Aplicavel'
                ]
            );

            if ($count == 0) {
                $html .= "<div class='row'>";
            }

            $html .= $this->helper->formInput($elemento);
            $html .= "<input type='hidden' name='documentoGrupo[" . $intervalo . "]' value='" . $contexto['docgrupo_id'] . "'>";
            $intervalo++;
            $count++;

            if ($count == $maxCount) {
                $html .= "</div>";
                $count = 0;
            }
        }

        if ($count % $maxCount != 0) {
            $html .= '</div>';
        }

        return $html;
    }
}