<?php

namespace Documentos\Controller;

use DOMPDFModule\View\Model\PdfModel;
use VersaSpine\Controller\AbstractCoreController;

class DocumentoModeloController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function emitirAction()
    {
        $request  = $this->getRequest();
        $arrDados = array_merge($request->getQuery()->toArray(), $request->getPost()->toArray());

        $serviceDocumentoModelo = new \Documentos\Service\DocumentoModelo($this->getEntityManager());

        $modelo = $serviceDocumentoModelo->renderizarModelo($arrDados['modeloId'], $arrDados);

        if ($modelo) {
            $pdf = new PdfModel();

            $pdf->setTemplate("documentos/documento-modelo/emitir")
                ->setOption('paperSize', 'Letter')
                ->setOption('filename', $modelo['arquivoNome'])
                ->setVariables(['html' => $modelo['arquivoConteudo']]);

            return $pdf;
        } else {
            $this->getView()->setVariables(
                array(
                    'erro'     => true,
                    'mensagem' => $serviceDocumentoModelo->getLastError()
                )
            );
        }

        return $this->getView();
    }

    public function searchForJsonAction()
    {
        $request                = $this->getRequest();
        $paramsGet              = $request->getQuery()->toArray();
        $paramsPost             = $request->getPost()->toArray();
        $serviceDocumentoModelo = new \Documentos\Service\DocumentoModelo($this->getEntityManager());

        $result = $serviceDocumentoModelo->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceDocumentoModelo = new \Documentos\Service\DocumentoModelo($this->getEntityManager());
        $serviceDocumentoModelo->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceDocumentoModelo = new \Documentos\Service\DocumentoModelo($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceDocumentoModelo->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function addAction($modeloId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados               = array();
        $serviceDocumentoModelo = new \Documentos\Service\DocumentoModelo($this->getEntityManager());

        if ($modeloId) {
            $arrDados = $serviceDocumentoModelo->getArray($modeloId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceDocumentoModelo->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de modelo salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceDocumentoModelo->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceDocumentoModelo->getLastError());
                }
            }
        }

        $serviceDocumentoModelo->formataDadosPost($arrDados);
        $serviceDocumentoModelo->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $modeloId = $this->params()->fromRoute("id", 0);

        if (!$modeloId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($modeloId);
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceDocumentoModelo = new \Documentos\Service\DocumentoModelo($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceDocumentoModelo->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceDocumentoModelo->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}
?>