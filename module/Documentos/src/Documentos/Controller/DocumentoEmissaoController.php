<?php

namespace Documentos\Controller;

use VersaSpine\Controller\AbstractCoreController;

class DocumentoEmissaoController extends AbstractCoreController
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function searchForJsonAction()
    {
        $request                 = $this->getRequest();
        $paramsGet               = $request->getQuery()->toArray();
        $paramsPost              = $request->getPost()->toArray();
        $serviceDocumentoEmissao = new \Documentos\Service\DocumentoEmissao($this->getEntityManager());

        $result = $serviceDocumentoEmissao->pesquisaForJson(array_merge($paramsGet, $paramsPost));

        return new \Zend\View\Model\JsonModel($result ? $result : array());
    }

    public function indexAction()
    {
        $serviceDocumentoEmissao = new \Documentos\Service\DocumentoEmissao($this->getEntityManager());
        $serviceDocumentoEmissao->setarDependenciasView($this->getView());

        return $this->getView();
    }

    public function searchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceDocumentoEmissao = new \Documentos\Service\DocumentoEmissao($this->getEntityManager());

            $dataPost = $request->getPost()->toArray();
            $result   = $serviceDocumentoEmissao->getDataForDatatables($dataPost);
            $this->getJson()->setVariable("draw", $result["draw"]);
            $this->getJson()->setVariable("recordsTotal", $result["recordsTotal"]);
            $this->getJson()->setVariable("recordsFiltered", $result["recordsFiltered"]);
            $this->getJson()->setVariable("data", $result["data"]);
        }

        return $this->getJson();
    }

    public function addAction($emissaoId = false)
    {
        $request = $this->getRequest();
        $ajax    = false;

        $arrDados = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getQuery()->toArray()
        );

        if (isset($arrDados['ajax']) && $arrDados['ajax']) {
            $this->setView(new \Zend\View\Model\JsonModel());
            $ajax = true;
        }

        $arrDados                = array();
        $serviceDocumentoEmissao = new \Documentos\Service\DocumentoEmissao($this->getEntityManager());

        if ($emissaoId) {
            $arrDados = $serviceDocumentoEmissao->getArray($emissaoId);

            if (empty($arrDados)) {
                $this->getView()->setVariable("erro", false);

                if (!$ajax) {
                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                }

                return $this->getView();
            }
        }

        if ($request->isPost()) {
            $arrDados = array_merge(
                $arrDados,
                array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                )
            );
            $arrDados = \VersaSpine\Stdlib\Util::sanitizeArray($arrDados);
            $salvar   = $serviceDocumentoEmissao->save($arrDados);

            if ($salvar) {
                $this->getView()->setVariable("erro", false);
                $mensagem = 'Registro de emissão salvo!';

                if (!$ajax) {
                    $this->flashMessenger()->addSuccessMessage($mensagem);

                    return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
                } else {
                    $this->getView()->setVariable("mensagem", $mensagem);
                }
            } else {
                $this->getView()->setVariable("erro", true);

                if (!$ajax) {
                    $this->flashMessenger()->addErrorMessage($serviceDocumentoEmissao->getLastError());
                } else {
                    $this->getView()->setVariable("mensagem", $serviceDocumentoEmissao->getLastError());
                }
            }
        }

        $serviceDocumentoEmissao->formataDadosPost($arrDados);
        $serviceDocumentoEmissao->setarDependenciasView($this->getView());

        $this->getView()->setVariable("arrDados", $arrDados);
        $this->getView()->setTemplate($this->getTemplateToRoute('add'));

        return $this->getView();
    }

    public function editAction()
    {
        $emissaoId = $this->params()->fromRoute("id", 0);

        if (!$emissaoId) {
            $erro = "Para editar um registro é necessário informar o identificador do mesmo pela rota!";
            $this->flashMessenger()->addErrorMessage($erro);

            return $this->redirect()->toRoute($this->getRoute(), array('controller' => $this->getController()));
        }

        return $this->addAction($emissaoId);
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $serviceDocumentoEmissao = new \Documentos\Service\DocumentoEmissao($this->getEntityManager());

            $erro          = false;
            $erroDescricao = '';
            $dataPost      = $request->getPost()->toArray();
            $ok            = $serviceDocumentoEmissao->remover($dataPost);

            if (empty($ok)) {
                $erro          = true;
                $erroDescricao = $serviceDocumentoEmissao->getLastError();
            }

            $this->getJson()->setVariable('erro', $erro);
            $this->getJson()->setVariable('erroDescricao', $erroDescricao);
        }

        return $this->getJson();
    }
}
?>