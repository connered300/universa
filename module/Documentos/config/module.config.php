<?php

namespace Documentos;

return array(
    'router'                    => array(
        'routes' => array(
            'documentos' => array(
                'type'          => 'Literal',
                'options'       => array(
                    'route'    => '/documentos',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Documentos\Controller',
                        'controller'    => 'DocumentosTipo',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes'  => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'       => '/[:controller[/:action[/:id]]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id'         => '[a-zA-Z0-9_,-]+',
                            ),
                            'defaults'    => array(
                                'action' => 'index'
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'controllers'               => array(
        'invokables' => array(
            'Documentos\Controller\DocumentoEmissao' => 'Documentos\Controller\DocumentoEmissaoController',
            'Documentos\Controller\DocumentoGrupo'   => 'Documentos\Controller\DocumentoGrupoController',
            'Documentos\Controller\DocumentoModelo'  => 'Documentos\Controller\DocumentoModeloController',
            'Documentos\Controller\DocumentoTipo'    => 'Documentos\Controller\DocumentoTipoController',
        ),
    ),
    'service_manager'           => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases'            => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'doctrine'                  => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default'             => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            ),
        ),
    ),
    'view_manager'              => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'console'                   => array(
        'router' => array(
            'routes' => array(),
        ),
    ),
    'funcionalidades_liberadas' => array(
        'controllers' => array(),
        'actions'     => array('emitir'),
    ),
    'namesDictionary'           => array(
        'Documentos\DocumentoEmissao' => 'Histórico de Documentos Emitidos',
        'Documentos\DocumentoGrupo'   => 'Documentos de Grupos',
        'Documentos\DocumentoModelo'  => 'Modelos de Documentos',
        'Documentos\DocumentoTipo'    => 'Tipos de Documentos',
    )
);
