

-- migrations/S031-C001-SQL-001.sql


ALTER TABLE `selecao_cursos` DROP FOREIGN KEY `fk_selecao_cursos_campus_curso1`;
ALTER TABLE `inscricao_cursos` DROP FOREIGN KEY fk_inscricao_cursos_selecao_cursos1;
ALTER TABLE `prova_edicao` DROP FOREIGN KEY fk_prova_edicao_selecao_cursos1;
ALTER TABLE `prova_edicao_caderno` DROP FOREIGN KEY fk_prova_edicao_caderno_prova_edicao1;
ALTER TABLE `selecao_cursos`
ADD CONSTRAINT `fk_selecao_cursos_campus_curso1` FOREIGN KEY (`cursocampus_id`) REFERENCES `campus_curso` (`cursocampus_id`) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE `inscricao_cursos`
ADD CONSTRAINT `fk_inscricao_cursos_selecao_cursos1` FOREIGN KEY (`selcursos_id`) REFERENCES `selecao_cursos` (`selcursos_id`) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE `prova_edicao`
ADD CONSTRAINT `fk_prova_edicao_selecao_cursos1` FOREIGN KEY (`selcursos_id`) REFERENCES `selecao_cursos` (`selcursos_id`) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE `prova_edicao_caderno`
ADD CONSTRAINT `fk_prova_edicao_caderno_prova_edicao1` FOREIGN KEY (`cad_id`) REFERENCES `prova_caderno` (`cad_id`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `selecao_cursos` DROP FOREIGN KEY `fk_selecao_cursos_campus_curso1`;
ALTER TABLE `inscricao_cursos` DROP FOREIGN KEY fk_inscricao_cursos_selecao_cursos1;
ALTER TABLE `prova_edicao` DROP FOREIGN KEY fk_prova_edicao_selecao_cursos1;
ALTER TABLE `prova_edicao_caderno` DROP FOREIGN KEY fk_prova_edicao_caderno_prova_edicao1;
ALTER TABLE `selecao_cursos` ADD CONSTRAINT `fk_selecao_cursos_campus_curso1` FOREIGN KEY (`cursocampus_id`) REFERENCES `campus_curso` (`cursocampus_id`) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE `inscricao_cursos` ADD CONSTRAINT `fk_inscricao_cursos_selecao_cursos1` FOREIGN KEY (`selcursos_id`) REFERENCES `selecao_cursos` (`selcursos_id`) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE `prova_edicao` ADD CONSTRAINT `fk_prova_edicao_selecao_cursos1` FOREIGN KEY (`selcursos_id`) REFERENCES `selecao_cursos` (`selcursos_id`) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE `prova_edicao_caderno` ADD CONSTRAINT `fk_prova_edicao_caderno_prova_edicao1` FOREIGN KEY (`cad_id`) REFERENCES `prova_caderno` (`cad_id`) ON UPDATE CASCADE ON DELETE CASCADE;

-- migrations/S031-C003-SQL-001.sql


CREATE TABLE sis__integracao_disciplina (
  curso_disc_id INT(10) UNSIGNED ZEROFILL NOT NULL,
  integracao_id INT(10) UNSIGNED ZEROFILL NOT NULL,
  codigo        INT                       NOT NULL,
  PRIMARY KEY (curso_disc_id, integracao_id),
  CONSTRAINT fk_CursoDiscIdDiscId FOREIGN KEY (curso_disc_id) REFERENCES acadgeral__disciplina (disc_id),
  CONSTRAINT fk_IntegracaoDisciplinaIntegracao FOREIGN KEY (integracao_id) REFERENCES sis__integracao (integracao_id)
);


ALTER TABLE acadgeral__disciplina_curso
ADD COLUMN disc__nucleo_comum ENUM('Sim', 'Nao') NOT NULL DEFAULT 'Sim';

ALTER TABLE `sis__integracao_disciplina`
DROP FOREIGN KEY `fk_CursoDiscIdDiscId`,
DROP FOREIGN KEY `fk_IntegracaoDisciplinaIntegracao`;
ALTER TABLE `sis__integracao_disciplina`
ADD CONSTRAINT `fk_CursoDiscIdDiscId`
FOREIGN KEY (`curso_disc_id`)
REFERENCES `acadgeral__disciplina_curso` (`disc_curso_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_IntegracaoDisciplinaIntegracao`
FOREIGN KEY (`integracao_id`)
REFERENCES `sis__integracao` (`integracao_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `org__email_tipo`
ADD CONSTRAINT `fk_org__email_tipo__org__email_conta`
FOREIGN KEY (`conta_id`)
REFERENCES `org__email_conta`(`conta_id`)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `org__email_tipo`
ADD CONSTRAINT `fk_org__email_tipo__org__comunicacao_tipo`
FOREIGN KEY (`tipo_id`)
REFERENCES `org__comunicacao_tipo`(`tipo_id`)
  ON DELETE CASCADE ON UPDATE CASCADE;

-- migrations/S031-C004-SQL-001.sql


--
-- Estrutura da tabela `sis__integracao_turma`
--

CREATE TABLE IF NOT EXISTS `sis__integracao_turma` (
  `integracao_id` INT(10) UNSIGNED ZEROFILL NOT NULL,
  `turma_id`      INT(10) UNSIGNED ZEROFILL NOT NULL,
  `codigo`        VARCHAR(45)               NOT NULL,
  PRIMARY KEY (`integracao_id`, `turma_id`),
  KEY `turma_id` (`turma_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `sis__integracao_turma`
--
ALTER TABLE `sis__integracao_turma`
ADD CONSTRAINT `sis__integracao_turma_ibfk_1` FOREIGN KEY (`integracao_id`) REFERENCES `sis__integracao` (`integracao_id`),
ADD CONSTRAINT `sis__integracao_turma_ibfk_2` FOREIGN KEY (`turma_id`) REFERENCES `acadperiodo__turma` (`turma_id`);


ALTER TABLE `acadperiodo__turma`
ADD COLUMN `unidade_id` INT(10) UNSIGNED ZEROFILL NULL DEFAULT NULL;


SET foreign_key_checks = 0;

ALTER TABLE `acadperiodo__turma`
ADD CONSTRAINT `fk_unidade__turma_123`
FOREIGN KEY (`unidade_id`)
REFERENCES `org__unidade_estudo` (`unidade_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

SET foreign_key_checks = 1;

-- migrations/S031-C006-SQL-001.sql


CREATE TABLE IF NOT EXISTS `acadgeral__atividade_log` (
  `log_id` INT NOT NULL AUTO_INCREMENT,
  `log_data` DATETIME NOT NULL,
  `log_action` VARCHAR(200) NOT NULL,
  `log_user_agent` VARCHAR(200) NOT NULL,
  `log_ip` VARCHAR(45) NOT NULL,
  `log_info` TEXT(3000) NULL,
  `aluno_id` INT UNSIGNED ZEROFILL NOT NULL,
  `disc_id` INT UNSIGNED ZEROFILL NULL,
  `curso_id` INT ZEROFILL NULL,
  PRIMARY KEY (`log_id`),
  INDEX `fk_acadgeral__atividade_log_acadgeral__aluno1_idx` (`aluno_id` ASC),
  INDEX `fk_acadgeral__atividade_log_acadgeral__disciplina1_idx` (`disc_id` ASC),
  INDEX `fk_acadgeral__atividade_log_acad_curso1_idx` (`curso_id` ASC),
  CONSTRAINT `fk_acadgeral__atividade_log_acadgeral__aluno1`
    FOREIGN KEY (`aluno_id`)
    REFERENCES `acadgeral__aluno` (`aluno_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_acadgeral__atividade_log_acadgeral__disciplina1`
    FOREIGN KEY (`disc_id`)
    REFERENCES `acadgeral__disciplina` (`disc_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_acadgeral__atividade_log_acad_curso1`
    FOREIGN KEY (`curso_id`)
    REFERENCES `acad_curso` (`curso_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

-- migrations/S031-C007-SQL-001.sql


SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `acadperiodo__letivo`
-- -----------------------------------------------------
ALTER TABLE `acadperiodo__letivo`
ADD `per_periodicidade` enum('semestral','anual','Quadrimestral','Trimestral','Nao Aplicado') not null,
ADD `per_descricao` VARCHAR(45) NULL,
ADD `per_data_finalizacao` DATETIME NULL,
ADD `nivel_id` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL,
ADD INDEX `fk_acadperiodo__letivo_acad_nivel1_idx` (`nivel_id` ASC),
ADD CONSTRAINT `fk_acadperiodo__letivo_acad_nivel1` FOREIGN KEY (`nivel_id`) REFERENCES `acad_nivel` (`nivel_id`)
ON DELETE CASCADE
ON UPDATE CASCADE;

-- -----------------------------------------------------
-- Table `acadperiodo__letivo_campus_curso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `acadperiodo__letivo_campus_curso` (
  `per_id` INT UNSIGNED ZEROFILL NOT NULL,
  `cursocampus_id` INT ZEROFILL UNSIGNED NOT NULL,
  PRIMARY KEY (`per_id`, `cursocampus_id`),
  INDEX `fk_acadperiodo__letivo_campus_curso_campus_curso1_idx` (`cursocampus_id` ASC),
  INDEX `fk_acadperiodo__letivo_campus_curso_acadperiodo__letivo1_idx` (`per_id` ASC),
  CONSTRAINT `fk_acadperiodo__letivo_campus_curso_acadperiodo__letivo1`
  FOREIGN KEY (`per_id`)
  REFERENCES `acadperiodo__letivo` (`per_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_acadperiodo__letivo_campus_curso_campus_curso1`
  FOREIGN KEY (`cursocampus_id`)
  REFERENCES `campus_curso` (`cursocampus_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `acadperiodo__letivo_selecao_edicao` (
  `per_id` INT UNSIGNED ZEROFILL NOT NULL,
  `edicao_id` INT UNSIGNED ZEROFILL NOT NULL,
  PRIMARY KEY (`per_id`, `edicao_id`),
  INDEX `fk_acadperiodo__letivo_selecao_edicao_selecao_edicao1_idx` (`edicao_id` ASC),
  INDEX `fk_acadperiodo__letivo_selecao_edicao_acadperiodo__letivo1_idx` (`per_id` ASC),
  CONSTRAINT `fk_acadperiodo__letivo_selecao_edicao_acadperiodo__letivo1`
    FOREIGN KEY (`per_id`)
    REFERENCES `acadperiodo__letivo` (`per_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_acadperiodo__letivo_selecao_edicao_selecao_edicao1`
    FOREIGN KEY (`edicao_id`)
    REFERENCES `selecao_edicao` (`edicao_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


-- Atualiza Registro de Semestralidade

UPDATE acadperiodo__letivo
SET
  per_periodicidade = 'semestral'
WHERE
  per_periodicidade IS NULL;

-- Atualiza Registros de Nivel de Ensino

UPDATE acadperiodo__letivo
SET
  nivel_id = (SELECT
                nivel_id
              FROM
                acad_nivel
              WHERE
                nivel_nome = 'Bacharelado')
WHERE
  nivel_id IS NULL OR nivel_id < 1;

-- Atualiza Registros de Finalização de Periodo

set sql_safe_updates=0;

UPDATE acadperiodo__letivo
SET
  per_data_finalizacao = per_data_fim
WHERE
  per_data_finalizacao IS NULL OR per_data_finalizacao < 1;

set sql_safe_updates=1;




-- migrations/S032-C001-SQL-001.sql


ALTER TABLE `acad_curso`
ADD `curso_possui_periodo_letivo` ENUM('Sim', 'Não')  NOT NULL DEFAULT 'Sim' COMMENT 'É o período em que o curso forma novas turmas';

ALTER TABLE `acad_curso`
ADD `curso_unidade_medida` ENUM('Mensal', 'Bimestral', 'Trimestral', 'Quadrimestral', 'Semestral', 'Anual') NOT NULL DEFAULT 'Semestral';

-- migrations/S032-C003-SQL-001.sql


REPLACE INTO `acesso_funcionalidades` (modulo, func_nome, func_controller, func_status, func_visivel)
  SELECT distinct acesso_modulos.id, func.descricao, func.controller,'Ativa','Sim'  from acesso_modulos,
    (
      select 'Meus Protocolos' as descricao, 'Protocolo\\Controller\\ProtocoloVinculado' as controller from dual
    ) as func
  where mod_nome like 'Protocolo';

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
    (
      select 'index' as act,'Index' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'search' as act,'Busca' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'edit' as act,'Busca' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'add' as act,'Adicionar' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'search-mensagem' as act,'Busca' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'add-mensagem' as act,'Adicionar Mensagem' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'search-protocolo-arquivo' as act,'Buscar arquivos' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'atualiza-situacao' as act,'Listagem' as descricao,'Leitura' as tipo,'N' as usr FROM dual
    ) as actions
  where func_controller IN('Protocolo\\Controller\\ProtocoloVinculado');

REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
  select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
    inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
    acesso_grupo
  where func_controller IN(
    'Protocolo\\Controller\\ProtocoloVinculado'
  )
  and grup_nome LIKE 'admin%';

-- migrations/S032-C004-SQL-001.sql


REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
SELECT DISTINCT
  acesso_funcionalidades.id,
  actions.act,
  actions.descricao,
  actions.tipo,
  actions.usr
FROM acesso_funcionalidades,
  (
    SELECT
      'relatorio-desconto'    AS act,
      'Relatório de Descontos' AS descricao,
      'Leitura'  AS tipo,
      'N'        AS usr
    FROM dual
  ) AS actions
WHERE func_controller IN (
  'Financeiro\\Controller\\Relatorio'
);
REPLACE INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`)
SELECT
  acesso_actions.id,
  acesso_grupo.id
FROM acesso_funcionalidades
  INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
  ,
  acesso_grupo
WHERE func_controller IN (
  'Financeiro\\Controller\\Relatorio'
)
      AND grup_nome LIKE 'Admin%';

-- migrations/S032-C008-SQL-001.sql


ALTER TABLE `acadperiodo__aluno_resumo`
  MODIFY `resaluno_origem` ENUM('Legado','Transferência','Novo título','Manual','Sistema', 'WebService') NOT NULL DEFAULT 'Sistema',
  MODIFY `resaluno_serie` int(11) DEFAULT NULL;


ALTER TABLE `acadperiodo__aluno_resumo`
ADD COLUMN `resaluno_detalhe` VARCHAR(100) NULL ;


ALTER TABLE `acadperiodo__aluno_resumo`
CHANGE COLUMN `resaluno_origem` `resaluno_origem` ENUM('Legado', 'Transferência', 'Novo título', 'Manual', 'Sistema', 'WebService') NOT NULL DEFAULT 'WebService' ;


-- migrations/S032-C015-SQL-001.sql


REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT DISTINCT
    acesso_funcionalidades.id,
    actions.act,
    actions.descricao,
    actions.tipo,
    actions.usr
  FROM acesso_funcionalidades,
    (
      SELECT
        'adimplentes'    AS act,
        'Relatório dos alunos Adimplentes' AS descricao,
        'Leitura'  AS tipo,
        'N'        AS usr
      FROM dual
    ) AS actions
  WHERE func_controller IN (
    'Financeiro\\Controller\\Relatorio'
  );
REPLACE INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
    ,
    acesso_grupo
  WHERE func_controller IN (
    'Financeiro\\Controller\\Relatorio'
  )
        AND grup_nome LIKE 'Admin%';

-- migrations/S032-C025-SQL-001.sql


ALTER TABLE `acadgeral__motivo_alteracao`
ADD COLUMN `motivo_situacao` ENUM('Deferido', 'Pendente', 'Trancado', 'Cancelado', 'Indeferido', 'Transferencia', 'Concluido') NOT NULL DEFAULT 'Deferido'
AFTER `motivo_descricao`;

ALTER TABLE `acadgeral__aluno_curso`
CHANGE COLUMN `alunocurso_situacao` `alunocurso_situacao` ENUM('Deferido', 'Pendente', 'Trancado', 'Cancelado', 'Indeferido', 'Transferencia', 'Concluido') NULL DEFAULT NULL;

ALTER TABLE `acadgeral__motivo_alteracao`
CHANGE COLUMN `motivo_situacao` `motivo_situacao` SET('Deferido', 'Pendente', 'Trancado', 'Cancelado', 'Indeferido', 'Transferencia', 'Concluido') NOT NULL DEFAULT 'Deferido,Pendente,Trancado,Cancelado,Indeferido,Transferencia,Concluido';

UPDATE `acadgeral__motivo_alteracao`
SET `motivo_situacao` = 'Deferido,Pendente,Trancado,Cancelado,Indeferido,Transferencia,Concluido'
WHERE motivo_id > 0;


/*
Script para alterar a situação dos alunos, na tabela acadgeral__aluno_curso
*/
UPDATE `acadgeral__aluno_curso` SET `alunocurso_situacao`='Concluido'
WHERE (alunocurso_data_expedicao_diploma is not null OR alunocurso_data_colacao is not null) and alunocurso_id>0;

-- migrations/S032-C027-SQL-001.sql


ALTER TABLE acadgeral__aluno
  CHANGE COLUMN `aluno_etnia` `aluno_etnia` ENUM('Branca', 'Preta', 'Amarela', 'Parda', 'Indígena', 'Não dispõe da informação', 'Não declarado') NOT NULL DEFAULT 'Não declarado' COMMENT 'Cor da pele do aluno.' ;

-- migrations/S032-C034-SQL-001.sql


ALTER TABLE `financeiro__titulo_config`
ADD COLUMN `tituloconf_nome` VARCHAR(45) NOT NULL;



UPDATE `financeiro__titulo_config` SET `tituloconf_nome`=concat('Configuração de Título: ',tituloconf_id) WHERE `tituloconf_id`>0;


-- migrations/NOVA-INTERFACE-MATRICULA.sql


ALTER TABLE `acad_curso`
CHANGE curso_integralizacao_medida curso_integralizacao_medida TEXT NULL DEFAULT  NULL;

ALTER TABLE acad_curso
CHANGE curso_periodicidade curso_periodicidade TEXT NULL DEFAULT  NULL;

UPDATE acad_curso
SET curso_possui_periodo_letivo=IF(curso_possui_periodo_letivo='Não' OR curso_possui_periodo_letivo='Não Aplicado', 'Não', 'Sim') where curso_id>0;

ALTER TABLE acad_curso
CHANGE curso_possui_periodo_letivo curso_possui_periodo_letivo enum('Sim', 'Não') NOT NULL COMMENT 'É o período em que o curso forma novas turmas';

CREATE TABLE `acadperiodo__letivo_campus_curso` (
  `per_id` int(10) unsigned zerofill NOT NULL,
  `cursocampus_id` int(10) unsigned zerofill NOT NULL,
  PRIMARY KEY (`per_id`,`cursocampus_id`),
  KEY `fk_acadperiodo__letivo_campus_curso_campus_curso1_idx` (`cursocampus_id`),
  KEY `fk_acadperiodo__letivo_campus_curso_acadperiodo__letivo1_idx` (`per_id`),
  CONSTRAINT `fk_acadperiodo__letivo_campus_curso_acadperiodo__letivo1` FOREIGN KEY (`per_id`) REFERENCES `acadperiodo__letivo` (`per_id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_acadperiodo__letivo_campus_curso_campus_curso1` FOREIGN KEY (`cursocampus_id`) REFERENCES `campus_curso` (`cursocampus_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



## A SEGUINTE CONSULTA SÓ DEVE SER USADA PARA EFEITO DE TESTE

REPLACE INTO acadperiodo__letivo_campus_curso (per_id, cursocampus_id)
SELECT DISTINCT
  periodo_campus.periodo,
  periodo_campus.campus
FROM
  (
    SELECT per_id periodo, cursocampus_id campus FROM acadperiodo__letivo JOIN campus_curso
  ) as periodo_campus;

## É NECESSÁRIO EXECUTAR ESSA MIGRATION PARA QUE MODALIDADE E TIPO DE CURSO FUNCIONEM

REPLACE INTO `acesso_funcionalidades` (modulo, func_nome, func_controller, func_status, func_visivel)
  SELECT distinct acesso_modulos.id, func.descricao, func.controller,'Ativa','Não'  from acesso_modulos,
    (
      select 'Nível' as descricao, 'Matricula\\Controller\\AcadNivel' as controller UNION ALL
      select 'Modalidade' as descricao, 'Matricula\\Controller\\AcadModalidade' as controller from dual
    ) as func
  where mod_nome like 'Matricula';

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
    (
      select 'index' as act,'Index' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'search-for-json' as act,'Pesquisa' as descricao,'Leitura' as tipo,'N' as usr FROM dual
    ) as actions
  where func_controller IN('Matricula\\Controller\\AcadNivel', 'Matricula\\Controller\\AcadModalidade');

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
    (
      select 'search-for-json' as act,'Pesquisa' as descricao,'Leitura' as tipo,'N' as usr FROM dual
    ) as actions
  where func_controller IN('Vestibular\\Controller\\SelecaoEdicao');


REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
  select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
    inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
    acesso_grupo
  where func_controller IN(
    'Matricula\\Controller\\AcadNivel','Matricula\\Controller\\AcadModalidade', 'Vestibular\\Controller\\SelecaoEdicao'
  )
        and grup_nome LIKE 'admin%';



-- migrations/NOVA-INTERFACE-MATRICULA-2.sql


ALTER TABLE `acadgeral__aluno_curso`
ADD `origem_id` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL,
ADD `alunocurso_pessoa_indicacao` VARCHAR(255) NULL,
ADD `alunocurso_mediador` VARCHAR(255) NULL,
ADD INDEX `fk_acadgeral__aluno_curso_acadgeral__cadastro_origem1_idx` (`origem_id` ASC);

ALTER TABLE `acadgeral__aluno_curso`
ADD CONSTRAINT `fk_acadgeral__aluno_curso_acadgeral__cadastro_origem1`
FOREIGN KEY (`origem_id`) REFERENCES `acadgeral__cadastro_origem` (`origem_id`);

-- migrations/S032-EXTRA-1-SQL.sql


REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
(
	select 'index' AS act, 'Matrícula' AS descricao, 'Escrita' as tipo,'N' as usr from dual UNION
	select 'matricula' AS act, 'Acadêmico' AS descricao, 'Escrita' as tipo,'N' as usr from dual
) as actions
where func_controller IN('Matricula\\Controller\\AcadgeralAlunoCurso');

REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
acesso_grupo
where func_controller IN(
		'Matricula\\Controller\\AcadgeralAlunoCurso'
)
and grup_nome LIKE 'admin%';

ALTER TABLE `acadperiodo__letivo`
  ADD `per_data_vencimento_inicial` DATETIME NOT NULL,
  ADD `per_data_vencimento_inicial_editavel` ENUM('Sim', 'Não') NOT NULL;

-- migrations/S033-C016-SQL-001.sql


ALTER TABLE org_ies
  ADD COLUMN `ies_credenciamento` TEXT DEFAULT NULL;

ALTER TABLE campus_curso
  ADD COLUMN cursocampus_autorizacao TEXT DEFAULT NULL,
  ADD COLUMN cursocampus_reconhecimento TEXT DEFAULT NULL;

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT DISTINCT
    acesso_funcionalidades.id,
    actions.act,
    actions.descricao,
    actions.tipo,
    actions.usr
  FROM acesso_funcionalidades,
    (
      SELECT
        'emissao-certificados'    AS act,
        'Emissão de certificados' AS descricao,
        'Leitura'  AS tipo,
        'N'        AS usr
      FROM dual
    ) AS actions
  WHERE func_controller IN (
    'Matricula\\Controller\\Relatorios'
  );

REPLACE INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
    ,
    acesso_grupo
  WHERE func_controller IN (
    'Matricula\\Controller\\Relatorios'
  )
        AND grup_nome LIKE 'Admin%';

-- migrations/S033-C018-SQL-001.sql


CREATE TABLE IF NOT EXISTS `acesso_pessoas_campus_curso` (
  `acesso_pessoas_id` INT(11) UNSIGNED ZEROFILL NOT NULL,
  `cursocampus_id` INT ZEROFILL UNSIGNED NOT NULL,
  PRIMARY KEY (`acesso_pessoas_id`, `cursocampus_id`),
  INDEX `fk_acesso_pessoas_campus_curso_campus_curso1_idx` (`cursocampus_id` ASC),
  INDEX `fk_acesso_pessoas_campus_curso_acesso_pessoas1_idx` (`acesso_pessoas_id` ASC),
  CONSTRAINT `fk_acesso_pessoas_campus_curso_acesso_pessoas1`
    FOREIGN KEY (`acesso_pessoas_id`)
    REFERENCES `acesso_pessoas` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_acesso_pessoas_campus_curso_campus_curso1`
    FOREIGN KEY (`cursocampus_id`)
    REFERENCES `campus_curso` (`cursocampus_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- migrations/remessa-tables.sql


--
-- Estrutura da tabela `financeiro__remessa`
--

DROP TABLE IF EXISTS financeiro__remessa;
CREATE TABLE IF NOT EXISTS `financeiro__remessa` (
  `remessa_id` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `confcont_id` int(11) NOT NULL,
  `remessa_codigo` int(11) NOT NULL,
  `remessa_data` datetime NOT NULL,
  `arq_id` int(10) unsigned zerofill NOT NULL,
  PRIMARY KEY (`remessa_id`),
  UNIQUE KEY `remessa_codigo_UNIQUE` (`remessa_codigo`,`confcont_id`),
  KEY `fk_financeiro__remessa_arquivo1_idx` (`arq_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `financeiro__remessa_lote`
--
DROP TABLE IF EXISTS financeiro__remessa_lote;
CREATE TABLE IF NOT EXISTS `financeiro__remessa_lote` (
  `remessa_lote_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bol_id` int(11) NOT NULL,
  `remessa_id` int(10) unsigned zerofill NOT NULL,
  `lote_codigo` int(11) NOT NULL,
  PRIMARY KEY (`remessa_lote_id`),
  UNIQUE KEY `uniq` (`bol_id`,`remessa_id`,`lote_codigo`),
  KEY `fk_boleto_financeiro__remessa_financeiro__remessa1_idx` (`remessa_id`),
  KEY `fk_boleto_financeiro__remessa_boleto1_idx` (`bol_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `financeiro__remessa`
--
ALTER TABLE `financeiro__remessa`
  ADD CONSTRAINT `fk_financeiro__remessa_arquivo1` FOREIGN KEY (`arq_id`) REFERENCES `arquivo` (`arq_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_financeiro_remessa_boleto_conf_conta1` FOREIGN KEY (`confcont_id`) REFERENCES `boleto_conf_conta` (`confcont_id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `financeiro__remessa_lote`
--
ALTER TABLE `financeiro__remessa_lote`
  ADD CONSTRAINT `fk_boleto_financeiro__remessa_boleto1` FOREIGN KEY (`bol_id`) REFERENCES `boleto` (`bol_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_boleto_financeiro__remessa_financeiro__remessa1` FOREIGN KEY (`remessa_id`) REFERENCES `financeiro__remessa` (`remessa_id`) ON UPDATE CASCADE;

update acad_curso set curso_possui_periodo_letivo='Sim' where curso_id>=0;
update acad_curso set curso_unidade_medida='Semestral' where curso_id>=0;
update acad_curso set mod_id=1 where curso_id>=0;
