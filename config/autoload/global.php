<?php
/**
 * Global Configuration Override
 */
session_start();
$login = $_SESSION['Zend_Auth']['storage']['login'];

return array(
    'doctrine'                                             => array(
        'connection' => array(
            'orm_default' => array(
                'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
                /**
                 * ATENÇÃO: setar no local.php
                 * OBS.: caso não possua o arquivo local.php, crie a partir do arquivo local.php.dist
                 */
                'params'      => array(
                    'host'          => '',
                    'port'          => '',
                    'user'          => '',
                    'password'      => '',
                    'dbname'        => '',
                    'driverOptions' => array(
                        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'"
                    )
                )
            )
        )
    ),
    'redis'                                                => array(
        'host' => '127.0.0.1',
        'port' => '6379'
    ),
    'layouts'                                              => array(
        'SmartAdmin'    => array(
            "GerenciadorArquivos",
            "Vestibular",
        ),
        'SmartAdmin2'   => array(
            "Acesso",
            "Biblioteca",
            "Coordenacao",
            "Matricula",
            "Pessoa",
            "Documentos",
            "Organizacao",
            "Sistema",
            "Crm",
            "Acesso",
            "Financeiro",
            "Atividades",
            "Comunicacao",
            "Protocolo",
            "Diario",
            "Vestibular\\InscricaoCursos",
            "Vestibular\\SelecaoInscricao::edit",
            "Vestibular\\SelecaoInscricao::index",
            "Vestibular\\SelecaoEdicao::index"
        ),
        'MaterialAdmin' => array(
            "Avaliacao",
            ($login == "leonardoweslei" ? "Matricula\\AcadgeralAluno::index" : 'desabilitado'),
            ($login == "leonardoweslei" ? "Matricula\\AcadCurso::index" : 'desabilitado'),
            ($login == "leonardoweslei" ? "Matricula\\AcadperiodoTurma::index" : 'desabilitado'),
            ($login == "leonardoweslei" ? "Matricula\\AcadperiodoAluno::index" : 'desabilitado'),
        ),
        //        'VersaSite' => array("Professor"),
        'Universa'      => array('Professor', 'Aluno'),
        'Simples'       => array(
            "Financeiro\\FinanceiroPagamento::pagamento-cartao-recorrencia",
            "Financeiro\\Painel::titulos",
            "Biblioteca\\Titulo::consulta",
            "Biblioteca\\Titulo::visualizar-titulo",
            "Avaliacao\\AvaliacaoQuestionario::responder",
            "Crm\\CrmLead::captacao",
        ),
    ),
    'dompdf_module'                                        => array(
        'enable_remote' => true
    ),
    'emailRecovery'                                        => array(
        'connection_class' => 'login',
        'name'             => 'Universa',
        'host'             => 'mail.versatecnologia.com.br',
        'userName'         => 'uni@versatecnologia.com.br',
        'password'         => '',
        'port'             => 587,
        'from'             => 'uni@versatecnologia.com.br',
    ),
    'emailSuporte'                                         => array(
        'connection_class' => 'login',
        'name'             => 'Universa',
        'host'             => 'mail.versatecnologia.com.br',
        'userName'         => 'universa@versatecnologia.com.br',
        'password'         => '',
        'port'             => 587,
        'from'             => 'universa@versatecnologia.com.br',
    ),
    'secretTokenAuth'                                      => '',
    'googleAnalyticsId'                                    => 'UA-120032210-1',
    'viewLogin'                                            => 'organizacao/autenticacao/login',
    'projectName'                                          => 'Universa',
    'projectVersion'                                       => '3.0.7',
    'secret-key-recaptcha'                                 => '6LcerwgUAAAAADxCH8OVYDkEnZaSxrwzwzxt15mx',
    'MATRICULA_PADRAO'                                     => 'AAAAAAAAAAAA',
    'DESABILITAR_OBRIGATORIEDADE_CPF'                      => 0,
    'MATRICULA_GERAR_TITULO_MENSALIDADE'                   => 1,
    'ATIVA_NOVA_TELA_MATRICULA'                            => 0,
    'VALIDAR_DOCUMENTOS_PEDENTES_REMATRICULA'              => 0,
    'VALIDAR_PENDENCIAS_ACADEMICAS_REMATRICULA'            => 1,
    'VALIDAR_TITULOS_ABERTOS_REMATRICULA'                  => 1,
    'VALIDAR_TITULOS_VENCIDOS_REMATRICULA'                 => 0,
    'maintenance'                                          => false,
    'SMS_PROVEDOR'                                         => 'LOCASMS',
    'SMSEMPRESA_CHAVE'                                     => '',
    'LOCASMS_USER'                                         => '',
    'LOCASMS_PASSWORD'                                     => '',
    'SMS_ATIVO'                                            => true,
    'ENVIAR_EMAIL_BOAS_VINDAS_ALUNO'                       => 0,
    'ENVIAR_SMS_BOAS_VINDAS_ALUNO'                         => 0,
    'ENVIAR_EMAIL_COMUNICACAO_ALUNO_APOS_DEFERIMENTO'      => 0,
    'ENVIAR_SMS_COMUNICACAO_ALUNO_APOS_DEFERIMENTO'        => 0,
    'ENVIAR_EMAIL_BOAS_VINDAS_AGENTE'                      => 0,
    'ENVIAR_SMS_BOAS_VINDAS_AGENTE'                        => 0,
    'EFETUAR_INTEGRACAO_CURSO'                             => 0,
    'EFETUAR_INTEGRACAO_ALUNO'                             => 0,
    'ALUNO_POSSUI_AGENTE_EDUCACIONAL'                      => 0,
    'ALUNO_CAMPOS_CAPTACAO'                                => 'pesNaturalidade,pesNascUf,alunoEtnia,pesRg,pessoaNecessidades,alunoMae,alunoPai,origem,alunocursoPessoaIndicacao,formacao',
    'ARREDONDAMENTO_CALCULO_NOTA_SEMESTRAL'                => 0,
    'ALUNO_POSSUI_UNIDADE_DE_ESTUDO'                       => 0,
    'GRUPO_EDITA_INDICACAO_ALUNO'                          => '',
    'GRUPO_EDITA_AGENTE_EDUCACIONAL_ALUNO'                 => '',
    'GRUPO_FINANCEIRO_SUPERVISAO'                          => 1,
    'ALUNO_FORMA_INGRESSO_PADRAO'                          => 5,
    'FINANCEIRO_MEIO_PAGAMENTO_PADRAO'                     => 1,
    'FINANCEIRO_MAX_PARCELAS'                              => 18,
    'FINANCEIRO_EMISSAO_CARNE_ATIVADO'                     => 1,
    'FINANCEIRO_ENVIAR_TITULOS_COPIA_AGENTE'               => 1,
    'FINANCEIRO_MAX_PERCENTUAL_DESCONTO'                   => 0,
    'FINANCEIRO_FILTRO_PADRAO'                             => false,
    'FINANCEIRO_TITULO_DESCRICAO_FORMATO'                  => '__TIPO__ - __PARCELA__ / __TOTAL_PARCELAS__',
    'FINANCEIRO_SELECAO_MULTIPLA'                          => true,
    'FINANCEIRO_ATUALIZACAO_TITULO_ABERTO'                 => true,
    'FINANCEIRO_EXIBICAO_CODIGO_PESSOA'                    => true,
    'FINANCEIRO_EXIBICAO_PESSOA_JURIDICA'                  => true,
    'FINANCEIRO_FILTRO_GRUPO_ACESSO'                       => '',
    'FINANCEIRO_COMPROVANTE_IMPRESSAO_DIRETA'              => 0,
    'FINANCEIRO_BUSCA_EXIBICAO_CPF_PESSOA'                 => true,
    'FINANCEIRO_BUSCA_EXIBICAO_CODIGO_ALUNO'               => true,
    'FINANCEIRO_EXIBICAO_SITUACAO_ALUNO'                   => true,
    'FINANCEIRO_EXIBICAO_PERIODO_ALUNO'                    => true,
    'FINANCEIRO_EXIBICAO_TURMA_ALUNO'                      => true,
    'PORCETAGEM_DESCONTO_PAINEL_FINANCEIRO_ALUNOPERID'     => false,
    'FINANCEIRO_TIPOS_TITULO_CALCULO_PORCENTAGEM_DESCONTO' => '',
    'FINANCEIRO_TIPOS_TITULO_ALTERACAO_SITUACAO_ACADEMICA' => '2,36',
    'FINANCEIRO_CONFIG_CALCULO_DESCONTO_INCENTIVO'         => 'valorBruto',
    /*
     * CONFIGURAÇÃO MAXIPAGO
     */
    'FINANCEIRO_MAXI_PAGO_ATIVO'                           => false,
    'FINANCEIRO_ENVIAR_EMAIL_COBRANCA_MAXIPAGO'            => 0,
    'FINANCEIRO_ENVIAR_SMS_COBRANCA_MAXIPAGO'              => 0,
    'FINANCEIRO_ENVIAR_EMAIL_RECORRENCIA_MAXIPAGO'         => 0,
    'FINANCEIRO_ENVIAR_SMS_RECORRENCIA_MAXIPAGO'           => 1,
    /**
     * CHAVES CRIPTOGRAFIA / USANDO PARA MAXIPAGO     *
     */
    'FINANCEIRO_CARTAO_IDENTIFICACAO_ESTABELECIMENTO'      => '',
    'FINANCEIRO_CARTAO_CHAVE_ESTABELECIMENTO'              => '',
    'FINANCEIRO_CARTAO_CODIGO_ADQUIRENTE'                  => '',
    'FINANCEIRO_CARTAO_AMBIENTE'                           => 'TEST',
    'FINANCEIRO_INADIMPLENCIA_TIPO_TITULO'                 => '',
    /*
     * CONFIGURAÇÃO DO RELATORIO FINANCEIRO
     */
    'FINANCEIRO_DESCONTO_GRUPO_USUARIOS_FILTRO'            => '',
    /**
     * VARIÁVEIS DE CONFIGURAÇÃO DO PAGSEGURO
     */
    'PAGSEGURO_AMBIENTE'                                   => 'sandbox',
    'PAGSEGURO_EMAIL'                                      => '',
    'PAGSEGURO_TOKEN'                                      => '',
    'PAGSEGURO_EMAIL_SANDBOX'                              => '',
    'PAGSEGURO_TOKEN_SANDBOX'                              => '',
    'PAGSEGURO_STATUS_BAIXA'                               => '3',
    'PAGSEGURO_ATIVO'                                      => false,
    /**
     * CONFIGURAÇÕES RELACIONADAS A CAPTAÇÃO DE LEADS EM CAIXAS DE EMAIL
     */
    'EMAIL_ENTRADA_LEAD'                                   => 'INBOX',
    'EMAIL_ARQUIVO_LEAD'                                   => '',
    'EMAIL_ARQUIVO_ERRO_LEAD'                              => '',
    'CRM_QUORUM_MINIMO'                                    => 30,
    /**
     * LINK DE SUPORTE DO FRESHDESK OU SERVICEDESK
     */
    'FRESHDESK_LINK'                                       => 'https://universa.freshdesk.com',
    /**
     * CONFIGURAÇÃO DE CRIAÇÃO AUTOMÁTICA DE USUÁRIO DO SISTEMA PARA AGENTE
     */
    'AGENTE_EDUCACIONAL_CRIAR_USUARIO'                     => 0,
    'AGENTE_EDUCACIONAL_ACESSO_GRUPO'                      => '',
    'ACADEMICO_CAPTACAO_AGENTE_NIVEIS'                     => '',
    /**
     * CONFIGURAÇÃO DE CRIAÇÃO AUTOMÁTICA DE USUÁRIO DO SISTEMA PARA ALUNO
     */
    'ALUNO_CRIAR_USUARIO'                                  => 0,
    'ALUNO_ACESSO_GRUPO'                                   => '',
    /**
     * TAMANHO DAS FILAS DE PROCESSAMENTO
     */
    'SIS_FILA_EMAIL_TAMANHO'                               => 5,
    'SIS_FILA_SMS_TAMANHO'                                 => 10,
    'SIS_FILA_MAXIPAGO_TAMANHO'                            => 5,
    'SIS_FILA_INTEGRACAO_TAMANHO'                          => 10,
    'SIS_ROBO_PROCESSAMENTO_LIMITE'                        => 60,
    /*
     * CONFIGURAÇÃO DE PROTOCOLO
     */
    'PROTOCOLO_MENSAGEM_PADRAO_ID'                         => '',
    'CARTEIRINHA_MENSAGEM'                                 => 'Indispensável para sua identificação e acesso ao ambiente interno da instituição',
    'MATRICULA_CONTRATO_SERVICO'                           => 'matricula/acadperiodo-aluno/contrato-servico.phtml',
    'TEMPLATE_REQUERIMENTO_MATRICULA'                      => 'requerimento-matricula_modelo1',
    'TEMPLATE_REQUERIMENTO_REMATRICULA'                    => 'requerimento-rematricula',
    'ALTURA_CABECALHO_TIMBRADO'                            => '155',
    'ALTURA_RODAPE_TIMBRADO'                               => '100',
    'minify'                                               => '',
    /**
     * CONFIGURAÇÃO DO BOLETO
     */
    'BOLETO_ATIVO'                                         => true, // Se a instituição trabalha com boleto
    'BOLETO_REMESSA_ATIVADA'                               => false, // Se necessita do arquivo de remessa
    'BOLETO_ATIVO_ALUNO'                                   => true, // Se necessita do arquivo de remessa
    'loginDestaques'                                       => '',
    'TCC_DISC_ID'                                          => 1,
    'LINK_ACESSO_AVA'                                      => '',
    /*
     * CONFIGURAÇÂO PESQUISA INSTITUCIONAL
     */
    'PESQUISA_INSTITUCIONAL_PROFESSOR'                     => 0, //Se professor responde a pesquisa
    'PESQUISA_INSTITUCIONAL_PROFESSOR_ENDERECO'            => "", //endereco da pesquisa
    'PESQUISA_INSTITUCIONAL_PROFESSOR_MSG'                 => "", //Msg a ser exibida para o professor
    /*
        * CONFIGURAÇÂO SENDGRID
        */
    'SENDGRID_ATIVO'                                       => false,
    'SENDGRID_CHAVE'                                       => false,
    /*
     * CONFIGURAÇÂO API SONAVOIP
     */
    'SONAX_CLICK2CALL_ATIVO'                               => false,
    'SONAX_CLICK2CALL_CONTA'                               => '',
    'SONAX_CLICK2CALL_TOKEN'                               => '',
    'FINANCEIRO_RETORNO_NUMERO_DETALHES_PROCESSAMENTO'     => 50,
    'AVALIACAO_EXIBICAO_GRADE'                             => true,
    'FINANCEIRO_GERAR_REMESSA_CARTEIRA_REGISTRADA'         => false,
    'TITULOS_FINANCEIRO_VALIDACAO_DESLIGAMENTO_ALUNO'      => true,
    /**
     *
     */
    'BIBLIOTECA_VIRTUAL_API_KEY'                           => '',
    'BIBLIOTECA_VIRTUAL_PRODUCAO'                          => 0,

    /*
     * CONFIGURAÇÃO QUE FALA SE O SUMARIZADOR DO RELATORIO ESTA ATIVO
     */
    'MATRICULA_SUMARIZADOR_RELATORIO_ATIVO'                => 0,
);
