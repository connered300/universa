<?php

return array(
    'modules'                 => array(
        'DoctrineModule',
        'DoctrineORMModule',
        'Pessoa',
        'Acesso',
        'Documentos',
        'VersaSpine',
        'GerenciadorArquivos',
        'Vestibular',
        'Organizacao',
        'Infraestrutura',
        'PhpBoletoZf2',
        'DOMPDFModule',
        'Provas',
        'ArquivoRetorno',
        'CorrecaoProva',
        'Matricula',
        'Documentos',
        'Boleto',
        'Professor',
        'Aluno',
        'Biblioteca',
        'Sistema',
        'Financeiro',
        'Crm',
        'Atividades',
        'Coordenacao',
        'Api',
        'Protocolo',
        'Avaliacao',
        'Comunicacao',
        'Layout',
        'SnowReportsZf2',
        'Diario',
    ),
    'module_listener_options' => array(
        'module_paths'      => array(
            './module',
            './vendor'
        ),
        'config_glob_paths' => array(
            'config/autoload/{,*.}{global,local}.php'
        )
    )
);
