<?php
/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
error_reporting(E_ERROR);
ini_set('display_errors', 1);
ini_set('max_execution_time', 0);
chdir(dirname(__DIR__));
ini_set('date.timezone', 'America/Sao_Paulo');
ini_set('memory_limit', '512M');

// Decline static file requests back to the PHP built-in webserver
if (php_sapi_name() === 'cli-server' && is_file(__DIR__ . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH))) {
    return false;
}

function pre($data, $die = true)
{
    echo "<pre>";
    print_r($data);

    if ($die) {
        die();
    }
}

function shutdown()
{
         $error = error_get_last();
    $fatalError = (E_ERROR | E_USER_ERROR | E_PARSE | E_CORE_ERROR | E_COMPILE_ERROR | E_RECOVERABLE_ERROR);

    if ($error && ($error['type'] & $fatalError)) {
        pre($error);
    }
}

register_shutdown_function('shutdown');

if (!function_exists('array_column')) {
    function array_column(array $input, $columnKey, $indexKey = null)
    {
        $array = array();
        foreach ($input as $value) {
            if (!isset($value[$columnKey])) {
                trigger_error("Key \"$columnKey\" does not exist in array");

                return false;
            }
            if (is_null($indexKey)) {
                $array[] = $value[$columnKey];
            } else {
                if (!isset($value[$indexKey])) {
                    trigger_error("Key \"$indexKey\" does not exist in array");

                    return false;
                }
                if (!is_scalar($value[$indexKey])) {
                    trigger_error("Key \"$indexKey\" does not contain scalar value");

                    return false;
                }
                $array[$value[$indexKey]] = $value[$columnKey];
            }
        }

        return $array;
    }
}

// Setup autoloading
require 'init_autoloader.php';

try {
    // Run the application!
    Zend\Mvc\Application::init(require 'config/application.config.php')->run();
} catch (\Exception $e) {
    $errArr = array(
        'message' => $e->getMessage(),
        'file'    => $e->getFile(),
        'line'    => $e->getLine(),
        'trace'   => $e->getTraceAsString()
    );
    pre($errArr);
}


