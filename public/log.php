<?php
ini_set('track_errors', 'on');
$data            = isset($_GET['data']) ? $_GET['data'] : "*";
$usuarioPesquisa = isset($_GET['user']) ? $_GET['user'] : "*";
$pesquisa        = isset($_GET['pesquisa']) ? $_GET['pesquisa'] : false;
$out             = isset($_GET['out']) ? $_GET['out'] : 'array';

if ($data != "*" && !preg_match('/[0-9\-\*]+/', $data)) {
    $data = "*";
}

if ((!$data || $data == '*') && !$usuarioPesquisa && (!$pesquisa || trim($pesquisa) == "")) {
    echo 'Solicitação invalida!';
    exit();
}

$files = glob("../data/log/" . $data . "/" . $usuarioPesquisa . ".txt");

@asort($files);

$arrCampos = [
    'Hora'       => '/^[0-9:]+\s*$/is',
    'IP'         => '/^[0-9\.]+\s*$/is',
    'Requisicao' => '/^\/.*$/is',
    'Dados'      => '/^{.*$/is'
];
$arrLog    = [];

foreach ($files as $fileNum => $filename) {
    $usuario = str_replace(".txt", "", basename($filename));
    $dir     = basename(dirname($filename));

    if ($usuarioPesquisa && !preg_match('/' . str_replace('*', '.*', $usuarioPesquisa) . '/', $usuario)) {
        continue;
    }

    $content = file($filename);

    foreach ($content as $lineNum => $line) {
        if ($pesquisa) {
            $php_errormsg = '';
            @preg_match($pesquisa, '');

            if ($php_errormsg || !preg_match($pesquisa, $line)) {
                continue;
            }
        }

        $arrTmp = preg_split('/(\| |)(Hora|IP|Requisicao|Dados): /is', $line);
        $arrTmp = array_filter($arrTmp);

        $arrLog[$fileNum . '-' . $lineNum]['Usuario'] = $usuario;
        $arrLog[$fileNum . '-' . $lineNum]['Data']    = $dir;

        foreach ($arrTmp as $str) {
            foreach ($arrCampos as $name => $er) {
                if (preg_match($er, $str)) {
                    $str = trim($str);

                    $arrLog[$fileNum . '-' . $lineNum][$name] = $name == 'Dados' ? json_decode($str, true) : $str;
                }
            }
        }
    }
}

usort(
    $arrLog,
    function ($a, $b) {
        $x = preg_replace('/[^0-9]/', '', $a['Data'] . $a['Hora']);
        $y = preg_replace('/[^0-9]/', '', $b['Data'] . $b['Hora']);

        return $x - $y;
    }
);

echo '<pre>';

$data = '';

if ($out == 'jsonr') {
    $data = json_encode($arrLog);
} elseif ($out == 'json') {
    $data = json_encode($arrLog, JSON_PRETTY_PRINT);
} elseif ($out == 'var') {
    $data = var_export($arrLog, true);
} else {
    $data = print_r($arrLog, true);
}

echo htmlentities($data);

echo '</pre>';
?>