jQuery(document).ready(function(){

    var respNumero = 0
    //Feito pelo caio - gordo
    $("input[name=pesCpf]:visible").change(function(){
        var cpfAluno = $(this).val();
        $.ajax({
            async: true,
            type: "POST",
            url: "/pessoa/pessoa/busca-dados-pessoais",
            data: {
                cnpjCpf: cpfAluno,
                endereco: true,
                contato: true,
                atributos: {
                    nome: {classe: "Pes",atributo: "PesNome"},
                    id: {classe: "Pes",atributo:"PesId"},
                    rg: {atributo:"PesRg"},
                    rgEmissao: {atributo:"pesRgEmissao"},
                    sexo: {atributo:"pesSexo"}
                }
            },
            success: function( data ){
                console.log(data);
                if( data.nome != undefined){
                    $("input[name=pes]").val(data.id);
                    $("input[name=pesNome]:visible").val(data.nome);
                    $("input[name=pesRg]:visible").val(data.rg);
                    $("input[name=pesRgEmissao]:visible").val(data.rgEmissao);
                    $("input:radio[name=pesSexo]:visible").filter("[value="+data.sexo+"]").attr("checked", true);
                    $("input[name='endCep[]']:visible").val(data.cep);
                    $("input[name='endLogradouro[]']:visible").val(data.logradouro);
                    $("input[name='endNumero[]']:visible").val(data.numero);
                    $("input[name='endComplemento[]']:visible").val(data.complemento);
                    $("input[name='endBairro[]']:visible").val(data.bairro);
                    $("input[name='endCidade[]']:visible").val(data.cidade);
                    $("select[name='endEstado[]']:visible").val(data.estado);
                    $("input[name='alunoEmail']:visible").val(data.email);
                    $("input[name='alunoTelefone']:visible").val(data.telefone);
                }
            },
            error: function( a ){
                //alert("Erro ao conectar ao servidor verifique sua conexão a internet!")
            }

        })
    });

    function strip(str, c) {
        var tmp = str.split(c);
        return tmp.join("");
    }

    $(".cnpjCpf").keyup(function(){
        var tmp = strip( $(this).val() , "." );
        tmp = strip( tmp , "/" );
        tmp = strip( tmp , "-" );
        if ( 12 < tmp.length ) $(this).val(tmp.substr(0,2) + '.' + tmp.substr(2,3) + '.' + tmp.substr(5,3) + '/' + tmp.substr(8,4)+ '-'  + tmp.substr(12,2));
        else if ( 9 < tmp.length ) $(this).val(tmp.substr(0,3) + '.' + tmp.substr(3,3) + '.' + tmp.substr(6,3) + '-' + tmp.substr(9,3));
        else if ( 6 < tmp.length ) $(this).val(tmp.substr(0,3) + '.' + tmp.substr(3,3) + '.' + tmp.substr(6,3));
        else if ( 3 < tmp.length )  $(this).val(tmp.substr(0,3) + '.' + tmp.substr(3,3));
        else $(this).val(tmp);
    });
    $(".cnpjCpf").on("keydown", function( event ){
        var aKey;

        if (window.event)
            aKey = event.keyCode;
        else
            aKey = event.which;

        //if das teclas precionadas
        if(!(((aKey==67)&&(event.ctrlKey))||((aKey==86)&&(event.ctrlKey))||(aKey==8)||(aKey==9)||(aKey==13)||(aKey==16)||(aKey==17)||(aKey==92)
            ||((aKey>=96)&&(aKey<=105))||((aKey>=48)&&(aKey<=57))||((aKey>=37)&&(aKey<=40))||((aKey>=112)&&(aKey<=123))))
        {
            return false;
        }
    });
    $(".cnpjCpf").blur(function () {
        var tamanho = this.value.length;
        if(tamanho == 18){
            $("[name='respPesRg']").parent().parent().hide();
            $("[name='respPesRgEmissao']").parent().parent().hide();
        } else {
            $("[name='respPesRg']").parent().parent().show();
            $("[name='respPesRgEmissao']").parent().parent().show();
        }
    });

    jQuery("input[name='pesDocEstrangeiro']").parent().parent().hide();

    jQuery("select[name='pesNacionalidade']").change(function(){
        if( jQuery(this).val() == 'Estrangeiro'){
            jQuery("input[name='pesCpf']").parent().parent().hide();
            jQuery("input[name='pesDocEstrangeiro']").parent().parent().show();
        } else {
            jQuery("input[name='pesCpf']").parent().parent().show();
            jQuery("input[name='pesDocEstrangeiro']").parent().parent().hide();
        }
    });


    //Determina a quantidade inicial de periodos
    var quantPeriodos = jQuery("select[name='curso'] option:selected").attr('data-periodos');
    for(i = 1; i <= quantPeriodos; i++){
        jQuery("select[name='alunoPeriodo']").append("<option value="+i+">"+i+"</option>");
    }


    jQuery("select[name='curso']").click(function(){
        var quantPeriodos = jQuery("select[name='curso'] option:selected").attr('data-periodos');
        var periodos = "";
        for(i = 1; i <= quantPeriodos; i++){
            periodos += "<option value="+i+">"+i+"</option>";
        }
        jQuery("select[name='alunoPeriodo']").html(periodos);
        buscaTurmas();
    });

    //busca turmas para o curso com base no curso, câmpus e periodo
    buscaTurmas();
    jQuery("select[name='alunoPeriodo']").click(function() {
        buscaTurmas();
    });

    jQuery("select[name='alunoTurmaPrincipal']").click(function() {
        buscaDisciplinas();
    });

    function buscaTurmas(){
        $.ajax({
            async: true,
            type: "POST",
            url: "/matricula/acadperiodo-turma/busca-turmas",
            data: {
                cursoCampusId: jQuery("select[name='curso'] option:selected").attr('data-campus'),
                periodo: jQuery("select[name='alunoPeriodo']").val()
            },
            success: function (data) {
                if (data.turmas != undefined) {
                    var turmas = "";
                    for (i = 0; i < data.turmas.length; i++) {
                        turmas = turmas + "<option value=" + data.turmas[i].turmaId + " data-matriz=" + data.turmas[i].matrizId + ">" + data.turmas[i].turmaNome + "</option>";
                    }
                    jQuery("select[name='alunoTurmaPrincipal']").html(turmas);
                    buscaDisciplinas();
                }
            },
            error: function (a) {

                alert("Erro ao conectar ao servidor verifique sua conexão a internet!");
            }

        });
    }

    function buscaDisciplinas() {
        $.ajax({
            async: true,
            type: "POST",
            url: "/matricula/acadperiodo-matriz-disciplina/busca-disciplinas",
            data: {
                matriz : jQuery("select[name=alunoTurmaPrincipal] option:selected").attr("data-matriz"),
                periodo:  jQuery("select[name=alunoPeriodo] option:selected").val()
            },
            success: function( data ){
                console.log(data);
                var situacoes = "";
                jQuery("input[name='situacao']").each(function() {
                    situacoes = situacoes + '<option value="' + jQuery(this).val() + '">' + jQuery(this).val() + '</option>';
                });
                //jQuery("#blocoDisciplinas").each()
                if( data.disciplinas != undefined){
                    jQuery("#blocoDisciplinas").children().each(function(){jQuery(this).detach()})
                    jQuery("#blocoDisciplinas").append('<legend>Disciplinas</legend>');
                    for(i = 0; i < data.disciplinas.length; i++){
//                            jQuery("select[name='alunoTurmaPrincipal']").append("<option value="+data.disciplinas[i].disc+">"+data.disciplinas[i].discNome+"</option>") ;
                        jQuery("#blocoDisciplinas").append(
                            '<div class="row">'+
                            '<section class="col-lg-2 col-sm-2 col-xs-2" style="padding-left:30px;">'+
                            '<label class="input">'+
                            '<input type="hidden" name="disciplina[]" value="'+data.disciplinas[i].disc+'">'+
                            '<i></i> '+data.disciplinas[i].discNome+
                            '</label>'+
                            '</section>'+
                            '<section class="col-lg-2 col-sm-2 col-xs-2" style="padding-left:30px;">'+
                            '<label class="select">'+
                            '<i></i>'+
                            '<select name="disciplinaTurma[]" >'+
                            jQuery("select[name=alunoTurmaPrincipal]").html()+
                            '</select>'+
                            '</label>'+
                            '</section>'+
                            '<section class="col-lg-1 col-sm-1 col-xs-1" style="padding-left:30px;">'+
                            '<label class="select">'+
                            '<i></i>'+
                            '<select name="disciplinaSituacao[]" >'+
                            situacoes +
                            '</select>'+
                            '</label>'+
                            '</section>'+
                            '</div>'
                        );
                    }
                    jQuery("select[name='disciplinaTurma[]']").each(function(){
                        jQuery(this).val(jQuery("select[name=alunoTurmaPrincipal]").val());
                    });
                    jQuery("#blocoDisciplinas").show();
                }
            },
            error: function( a ){
                alert("Erro ao conectar ao servidor verifique sua conexão a internet!");
            }

        });
    }

    function buscaTurmasParaDisciplina() {
        $.ajax({
            async: true,
            type: "POST",
            url: "/matricula/acadperiodo-matriz-disciplina/busca-disciplinas",
            data: {
                matriz : jQuery("select[name=alunoTurmaPrincipal] option:selected").attr("data-matriz"),
                periodo:  jQuery("select[name=alunoPeriodo] option:selected").val()
            },
            success: function( data ){
                console.log(data);
                if( data.turmas != undefined){

                }
            },
            error: function( a ){
                alert("Erro ao conectar ao servidor verifique sua conexão a internet!");
            }

        });
    }

    function previewFile() {
        var preview = document.getElementById('preview_image');
        var file    = document.querySelector('input[type=file]').files[0];
        var reader  = new FileReader();

        reader.onloadend = function () {
            preview.src = reader.result;
        }

        if (file) {
            reader.readAsDataURL(file);
        } else {
            preview.src = "";
        }
    }

    function adicionaNovoResponsavel() {
        var responsavel = {};
        $("[name^='resp']:visible").each(function () {
            responsavel[this.name] = this.value;
        });
        responsavel['pesNacionalidade'] = $("select[name='respPesNacionalidade']").val();
        responsavel['endCep[]'] = $("input[name='endCep[]']:visible").val();
        responsavel['endLogradouro[]'] = $("input[name='endLogradouro[]']:visible").val();
        responsavel['endCidade[]'] = $("input[name='endCidade[]']:visible").val();
        responsavel['endEstado[]'] = $("select[name='endEstado[]']:visible").val();
        responsavel['endNumero[]'] = $("input[name='endNumero[]']:visible").val();
        responsavel['endComplemento[]'] = $("input[name='endComplemento[]']:visible").val();
//        console.log(responsavel);
        $('#tableResponsaveis').append(
            "<tr data-target='"+respNumero+"'>" +
            "<td>" +
            "<a name='editarResponsavel' data-target='"+respNumero+"'>Editar</a> | " +
            "<a name='removeResponsavel' data-target='"+respNumero+"'>Remover</a>" +
            "<input type='hidden' name='responsavelNivel[]' targetOff='"+respNumero+"' value='"+responsavel['respNivel[]']+"'>" +
            "<input type='hidden' name='responsavelCnpjCpf[]' targetOff='"+respNumero+"' value='"+responsavel['respPesCnpjCpf']+"'>" +
            "<input type='hidden' name='responsavelNome[]' targetOff='"+respNumero+"' value='"+responsavel['respPesNome']+"'>" +
            "<input type='hidden' name='responsavelDocEstrangeiro[]' targetOff='"+respNumero+"' value='"+responsavel['respPesDocEstrangeiro']+"'>" +
            "<input type='hidden' name='responsavelVinculo[]' targetOff='"+respNumero+"' value='"+responsavel['respVinculo[]']+"'>" +
            "<input type='hidden' name='responsavelRg[]' targetOff='"+respNumero+"' value='"+responsavel['respPesRg']+"'>" +
            "<input type='hidden' name='responsavelRgEmissao[]' targetOff='"+respNumero+"' value='"+responsavel['respPesRgEmissao']+"'>" +
            "<input type='hidden' name='responsavelNacionalidade[]' targetOff='"+respNumero+"' value='"+responsavel['pesNacionalidade']+"'>" +
            "<input type='hidden' name='responsavelTipoEndereco[]' targetOff='"+respNumero+"' value='1'>" +
            "<input type='hidden' name='responsavelCep[]' targetOff='"+respNumero+"' value='"+responsavel['endCep[]']+"'>" +
            "<input type='hidden' name='responsavelLogradouro[]' targetOff='"+respNumero+"' value='"+responsavel['endLogradouro[]']+"'>" +
            "<input type='hidden' name='responsavelCidade[]' targetOff='"+respNumero+"' value='"+responsavel['endCidade[]']+"'>" +
            "<input type='hidden' name='responsavelEstado[]' targetOff='"+respNumero+"' value='"+responsavel['endEstado[]']+"'>" +
            "<input type='hidden' name='responsavelBairro[]' targetOff='"+respNumero+"' value='"+responsavel['endBairro[]']+"'>" +
            "<input type='hidden' name='responsavelNumero[]' targetOff='"+respNumero+"' value='"+responsavel['endNumero[]']+"'>" +
            "<input type='hidden' name='responsavelComplemento[]' targetOff='"+respNumero+"' value='"+responsavel['endComplemento[]']+"'>" +
            "<input type='hidden' name='responsavelTelefone[]' targetOff='"+respNumero+"' value='"+responsavel['respTelefone']+"'>" +
            "<input type='hidden' name='responsavelCelular[]' targetOff='"+respNumero+"' value='"+responsavel['respCelular']+"'>" +
            "<input type='hidden' name='responsavelEmail[]' targetOff='"+respNumero+"' value='"+responsavel['respEmail']+"'>" +
            "</td>" +
            "<td>"+responsavel['respPesNome']+"</td>" +
            "<td>"+responsavel['respPesCnpjCpf']+"</td>" +
            "<td>"+$("select[name='respVinculo[]']").find("option[value='"+responsavel['respVinculo[]']+"']").text()+"</td>" +
            "</tr>"
        );
        limpaFormResponsavel();
        respNumero++;
    }

    function limpaFormResponsavel () {
        $("select[name='respNivel[]']").val('NULL');
        $("input[name='respPesCnpjCpf']").val('');
        $("input[name='respPesNome']").val('');
        $("input[name='respPesDocEstrangeiro']").val('');
        $("select[name='respVinculo[]']").val('NULL');
        $("input[name='respPesRg']").val('');
        $("input[name='respPesRgEmissao']").val('');
        $("select[name='respPesNacionalidade']").val("NULL");
        $("input[name='endCep[]']:visible").val("");
        $("input[name='endLogradouro[]']:visible").val("");
        $("input[name='endCidade[]']:visible").val("");
        $("select[name='endEstado[]']:visible").val("NULL");
        $("input[name='endNumero[]']:visible").val("");
        $("input[name='endComplemento[]']:visible").val("");
        $("input[name='respTelefone']:visible").val("");
        $("input[name='respEmail']:visible").val("");
        $("input[name='respCelular']:visible").val("");
    }

    $('#respAdd').click(function () {
        if($('#resp').hasClass('hidden')){
            $('#resp').removeClass('hidden');
            $spine().validateForm();
        } else {
            $('#resp').addClass('hidden');
        }
        if(!$('#editaResp').hasClass('hidden')){
            $('#editaResp').addClass('hidden');
            $('#adicionarResp').removeClass('hidden');
        }
        limpaFormResponsavel();
    });
    $("select[name='respPesNacionalidade']").change(function () {
        if(this.value == 'Estrangeiro'){
            $("#documentoResp").append("<section class='col-lg-8 col-sm-8 col-xs-8' style='padding-left:30px;'><label class='label'>Documento Estrangeiro:</label><label class='input'><i class='icon-prepend fa fa-credit-card'></i><input name='respPesDocEstrangeiro' placeholder='Informe o documento estrangeiro' data-validations='Required' value='' type='text'></label><div class='note'></div></section>");
            $("input[name='respPesCnpjCpf']").parent().parent().addClass('hidden');
//            <label class='label'>CPF</label><label class='input'><i class='icon-prepend fa fa-credit-card'></i><input name='pesCpf' placeholder='Informe o CPF' data-validations='Required,Cpf' data-masked='Cpf' value='' type='text'></label><div class='note'></div>
        }
        else {
            $("input[name='respPesCnpjCpf']").parent().parent().removeClass('hidden');
            $("input[name='respPesDocEstrangeiro']").parent().parent().detach();
        }
    });
    $('#adicionarResp').click(function (){
        if(stepValidate(valida, 0) ){
            adicionaNovoResponsavel();
            $('#resp').addClass('hidden');
        }
    });
    $(document).on('click', "a[name='editarResponsavel']", function () {
        var target = $(this).attr('data-target');
        var resp = {};
        $("input[targetOff='"+target+"']").each(function () {
            resp[this.name] = this.value;
        });

        if($('#resp').hasClass('hidden')){
            $('#resp').removeClass('hidden');
        }
        $("select[name='respNivel[]']").val(resp['responsavelNivel[]']);
        $("input[name='respPesCnpjCpf']").val(resp['responsavelCnpjCpf[]']);
        $("input[name='respPesNome']").val(resp['responsavelNome[]']);
        $("input[name='respPesDocEstrangeiro']").val(resp['responsavelDocEstrangeiro[]']);
        $("select[name='respVinculo[]']").val(resp['responsavelVinculo[]']);
        $("input[name='respPesRg']").val(resp['responsavelRg[]']);
        $("input[name='respPesRgEmissao']").val(resp['responsavelRgEmissao[]']);
        $("select[name='respPesNacionalidade']").val(resp['responsavelNacionalidade[]']);
        $("input[name='endCep[]']:visible").val(resp['responsavelCep[]']);
        $("input[name='endLogradouro[]']:visible").val(resp['responsavelLogradouro[]']);
        $("input[name='endCidade[]']:visible").val(resp['responsavelCidade[]']);
        $("select[name='endEstado[]']:visible").val(resp['responsavelEstado[]']);
        $("input[name='endNumero[]']:visible").val(resp['responsavelNumero[]']);
        $("input[name='endComplemento[]']:visible").val(resp['responsavelComplemento[]']);
        $("input[name='respTelefone']:visible").val(resp['responsavelTelefone[]']);
        $("input[name='respCelular']:visible").val(resp['responsavelCelular[]']);
        $("input[name='respEmail']:visible").val(resp['responsavelEmail[]']);

        if($('#editaResp').hasClass('hidden')){
            $('#editaResp').removeClass('hidden');
            $('#adicionarResp').addClass('hidden');
        }
        $('#editaResp').attr('data-target', target);

    });
    $("#editaResp").click(function () {
        var targ = $(this).attr('data-target');
        adicionaNovoResponsavel();
        $("tr[data-target='"+targ+"']").detach();
        $('#resp').addClass('hidden');
    });

    /**
     * Script para fazer a mudança de um passo para outro.
     * */
    var valida = {
        0: {
            'respNivel[]' : 'select',
            respPesNacionalidade: 'select',
            respPesCnpjCpf : 'input',
            respPesNome: 'input',
            respPesDocEstrangeiro: 'input',
            'respVinculo[]': 'select',
            respPesRg: 'input',
            respPesRgEmissao: 'input',
            'endCep[]': 'input',
            'endLogradouro[]': 'input',
            'endBairro[]': 'input',
            'endCidade[]': 'input',
            'endEstado[]': 'select'
        }
    };

    $("#next").click(function () {
        step = $("ul[class='steps']").find("li.active").attr('data-target');
        if(step  == "#step2"){
            $("#next").text("Salvar");
        }
        validations = {
            step1: {
                pesNome: 'input',
                alunoFormaIngresso: 'select',
                pesNacionalidade: 'select',
                pesCpf: 'input',
                pesRg: 'input',
                pesRgEmissao: 'input',
                pesSexo: 'input',
                alunoEtnia: 'select',
                pesFalecido: 'select',
                alunoPai: 'input',
                alunoMae: 'input',
                'endCep[]': 'input',
                'endLogradouro[]': 'input',
                'endNumero[]': 'input',
                'endBairro[]': 'input',
                'endCidade[]': 'input',
                'endEstado[]': 'select',
                //alunoTelefone: 'input',
                //alunoCelular: 'input'
            },
            step2: {
            },
            step3: {
                curso: 'select',
                alunoPeriodo: 'select',
                alunoTurmaPrincipal: 'select'
            }
        };
        if(stepValidate( validations, step.replace('#', '') ) ) {
            proximoStep = parseInt( step.replace('#step', '') ) + 1;
            proximoStep = '#step' + proximoStep;

            if($("li[data-target='"+proximoStep+"']")){
                $(step).removeClass('active');
                $('#prev').removeAttr('disabled');
                if($(proximoStep).length > 0){
                    $("ul[class='steps']").find("li.active").attr('data-target', step).addClass('complete').removeClass('active');
                    $(proximoStep).addClass('active');
                    $("li[data-target='"+proximoStep+"']").addClass('active');
                    $spine().validateForm();
                }
            }
            if(step  == "#step3"){
                $("#next").attr("type", "submit");
                $("form[name='PessoaFisica']").submit(function () {
                    if(stepValidate(validations, 'step3')){
                        return true;
                    } else {
                        return false;
                    }
                });
            }
        }
        $("li[class='complete']").bind('click' ,function () {
            atual = $('ul.steps').find("li.active").attr('data-target');
            atual = parseInt(atual.replace('#step', ''));
            destino = $(this).attr('data-target');
            destino = parseInt(destino.replace('#step', ''));
            while(atual > destino){
                $("li[data-target='#step"+atual+"']").removeClass('active complete');
                $("#step"+atual).removeClass('active')
                atual--;
            }
            if(atual == 1){
                $('#prev').attr('disabled', 'disabled');
            }
            $("li[data-target='#step"+atual+"']").addClass('active');
            $("#step"+atual).addClass('active');
        });

    });
    $("#prev").click(function () {
        $("#next").text("Proximo");

        step = $("ul[class='steps']").find("li.active").attr('data-target');
        $(step).removeClass('active');
        $("ul[class='steps']").find("li.active").attr('data-target', step).removeClass('active complete');

        proximoStep = parseInt( step.replace('#step', '') ) - 1;
        if(proximoStep == 1){
            $('#prev').attr('disabled', 'disabled');
        }
        proximoStep = '#step' + proximoStep;
        if($(proximoStep)){
            $(proximoStep).addClass('active');
            $("li[data-target='"+proximoStep+"']").addClass('active');
        }
    });

    /**
     * Funçao que valida os campos de um passo do wizard,
     * recebe como parametro um objeto contendo o passo ex:
     //    validations = {
     //        step1: { Aqui esta o nome do passo, que no caso e o id do <li> da lista de passos.
     //            pesNome: 'input' Aqui temos o nome do campo e o tipo dele.
     //        }
     //    }
     * */
    function stepValidate(validacao, passo){
        var stop = false;
        if(!validacao[passo]){
            console.log("Não foi encontrada nenhuma validação para o step passado.");
            return false;
        }
        for (elementoNome in validacao[passo]) {
            //Recebe o elemento que será validado.
            elemento = $(validacao[passo][elementoNome]+"[name='"+elementoNome+"']:visible");

            //Recebe as funções de validação que serão executadas para o elemento.
            funcao = $(elemento).attr('data-validations');
            if(funcao){
                if(funcao.indexOf(',')){
                    funcao = funcao.split(',');
                }
                for(i = 0; i < funcao.length; i++){
//                retorno = $spine(elemento).funcValidations[ funcao[i] ](elemento.value);
                    if (!$spine(elemento).funcValidations[ funcao[i] ]($(elemento).val())) {
                        $(elemento).parent().addClass('state-error');
                        $(elemento).parent().focus();
                        $(elemento).parent().parent().find("div[class^='note']").addClass('note-error').html($spine().msgValidations[funcao[i]]);
                        stop = true;
                        break;
                    } else {
                        $(elemento).parent().removeClass('state-error');
                        $(elemento).parent().parent().find("div[class^='note']").removeClass('note-error').html("");
                    }
                }
            }
        }
        return !stop;
    }

    jQuery("input[label='Certificado de Reservista']").click(function(){
        if(jQuery(this).val() != "Sim"){
            jQuery("input[name='alunoCertMilitar']").parent().parent().hide();
        } else {
            jQuery("input[name='alunoCertMilitar']").parent().parent().show();
        }
    });
    jQuery("select[name='alunoFormaIngresso']").attr('disabled','disabled');
});

$(document).on('click', "a[name='removeResponsavel']",function () {
    if(confirm("Deseja realmente remover este responsável ?")){
        $(this).parent().parent().detach();
    }
});