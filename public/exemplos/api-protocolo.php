<?php

function baseUrl()
{
    $pageURL = 'http';

    if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
        $pageURL .= "s";
    }

    $pageURL .= "://";

    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }

    $pageURL = explode('/exemplos/', $pageURL);
    $pageURL = $pageURL[0];

    return $pageURL;
}

function getParamsFromUrl()
{
    $arrParams = array();

    foreach ($_GET as $key => $value) {
        $arrParams[$key] = $value;
    }

    return $arrParams;
}

function joinAttachmentsIntoString(&$arrParams)
{
    if($arrParams['anexos'])
    {
        $anexos = '';

        foreach($arrParams['anexos'] as $anexo){
            $anexos .= PHP_EOL . $anexo;
        }

        $arrParams['anexos'] = $anexos;
    }
}

/** PARÂMETROS
token
conteudo
protocolo
origem
anexos
solicitacao
protocoloMensagem
protocoloSolicitanteVisivel
protocoloAssunto
codigoIntegracaoAluno
codigoIntegracaoCurso
usuarioIdResponsavel
protocoloSolicitantePes
protocoloSolicitanteAlunocurso
protocoloAvaliacao
protocoloSolicitanteNome
protocoloSolicitanteEmail
protocoloSolicitanteTelefone
protocoloSituacao
mensagemUsuario
mensagemSolicitanteVisivel
id
 */

$hostUniversa = baseUrl();
$linkDownload = $hostUniversa . '/gerenciador-arquivos/arquivo/download/';
$arrParams    = getParamsFromUrl();
$arrParams['linkParaDownload'] = $linkDownload;
//$hostUniversa   = 'http://127.0.0.1:8001';


joinAttachmentsIntoString($arrParams);

$url=$hostUniversa . '/api/v1/protocolo';

if ($arrParams['id']) {
    $url .= '?id=' . $arrParams['id'];
}

$process = curl_init($url);
curl_setopt_array(
    $process,
    array(
        CURLOPT_USERPWD        => $arrParams['token'] . ":" . $arrParams['token'],
        CURLOPT_RETURNTRANSFER => true,
    )
);

if (!$arrParams['id']) {
    curl_setopt($process, CURLOPT_POSTFIELDS, $arrParams);
    curl_setopt($process, CURLOPT_POST, true);
}



$return = curl_exec($process);
curl_close($process);
echo '<h2>Código:</h2>';
highlight_string(file_get_contents(__FILE__));
echo '<h2>Resposta:</h2>';
echo '<pre>' . print_r(json_encode(json_decode($return, 1), JSON_PRETTY_PRINT), 1) . '</pre>';
?>