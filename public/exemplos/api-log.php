<?php

function baseUrl()
{
    $pageURL = 'http';

    if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
        $pageURL .= "s";
    }

    $pageURL .= "://";

    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }

    $pageURL = explode('/exemplos/', $pageURL);
    $pageURL = $pageURL[0];

    return $pageURL;
}

function getParamsFromUrl()
{
    $arrParams = array();

    foreach ($_GET as $key => $value) {
        $arrParams[$key] = $value;
    }

    return $arrParams;
}

/** PARÂMETROS
 * logData
 * logAction
 * logUserAgent
 * logIp
 * logInfo
 * alunoId
 * discId
 * cursoId
 */

//SE VOCÊ PASSAR O PARAMETRO logId, UMA BUSCA SERÁ REALIZADA AO INVÉS DA INSERÇÃO

$hostUniversa = baseUrl();
$linkDownload = $hostUniversa . '/gerenciador-arquivos/arquivo/download/';
$arrParams    = getParamsFromUrl();

$process = curl_init($hostUniversa . '/api/v1/log');

curl_setopt_array(
    $process,
    array(
        CURLOPT_USERPWD        => $arrParams['token'] . ":" . $arrParams['token'],
        CURLOPT_POST           => true,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POSTFIELDS     => $arrParams
    )
);

$return = curl_exec($process);
curl_close($process);
echo '<h2>Código:</h2>';
highlight_string(file_get_contents(__FILE__));
echo '<h2>Resposta:</h2>';
echo '<pre>' . print_r(json_encode(json_decode($return, 1), JSON_PRETTY_PRINT), 1) . '</pre>';
?>