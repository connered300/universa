<?php

function baseUrl()
{
    $pageURL = 'http';

    if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
        $pageURL .= "s";
    }

    $pageURL .= "://";

    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }

    $pageURL = explode('/exemplos/', $pageURL);
    $pageURL = $pageURL[0];

    return $pageURL;
}

$codigoAlunoAva = $_GET['codigo'] ? $_GET['codigo'] : 14;
$tokenUniversa  = $_GET['token'] ? $_GET['token'] : 'dc0298f7556f4cb195a1da307d59c53587dae855';
$hostUniversa   = baseUrl();
//$hostUniversa   = 'http://127.0.0.1:8001';

$process = curl_init($hostUniversa . '/api/v1/integracaoFinanceiro/' . $codigoAlunoAva);
curl_setopt($process, CURLOPT_USERPWD, $tokenUniversa . ":" . $tokenUniversa);
curl_setopt($process, CURLOPT_RETURNTRANSFER, true);
$return = curl_exec($process);
curl_close($process);
echo '<h2>Código:</h2>';
highlight_string(file_get_contents(__FILE__));
echo '<h2>Resposta:</h2>';
echo '<pre>' . print_r(json_encode(json_decode($return, 1), JSON_PRETTY_PRINT), 1) . '</pre>';
?>