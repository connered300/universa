<?php

function baseUrl()
{
    $pageURL = 'http';

    if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
        $pageURL .= "s";
    }

    $pageURL .= "://";

    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }

    $pageURL = explode('/exemplos/', $pageURL);
    $pageURL = $pageURL[0];

    return $pageURL;
}

function joinAttachmentsIntoString()
{
    if($_GET['anexos'])
    {
        $anexos = '';

        foreach($_GET['anexos'] as $anexo){
            $anexos .= PHP_EOL . $anexo;
        }

        return $anexos;
    }

    return null;
}

$tokenUniversa  = $_GET['token'];
$mensagemConteudo = $_GET['mensagemConteudo'];
$protocolo 	= $_GET['protocolo'];
$mensagemOrigem = $_GET['mensagemOrigem'];
$hostUniversa   = baseUrl();
$linkDownload = $hostUniversa . '/gerenciador-arquivos/arquivo/download/';
//$hostUniversa   = 'http://127.0.0.1:8001';

$process = curl_init($hostUniversa . '/api/v1/protocolo-mensagem');

$anexos = joinAttachmentsIntoString();

curl_setopt_array($process, array(
    CURLOPT_USERPWD => $tokenUniversa . ":" . $tokenUniversa,
    CURLOPT_POST => true,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_POSTFIELDS => array(
        'mensagemConteudo' => $mensagemConteudo,
        'protocolo' => $protocolo,
        'mensagemOrigem' => $mensagemOrigem,
        'anexos' => $anexos,
        'linkParaDownload' => $linkDownload,
    )
));

$return = curl_exec($process);
curl_close($process);
echo '<h2>Código:</h2>';
highlight_string(file_get_contents(__FILE__));
echo '<h2>Resposta:</h2>';
echo '<pre>' . print_r(json_encode(json_decode($return, 1), JSON_PRETTY_PRINT), 1) . '</pre>';
?>