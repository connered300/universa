<?php

function baseUrl()
{
    $pageURL = 'http';

    if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
        $pageURL .= "s";
    }

    $pageURL .= "://";

    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }

    $pageURL = explode('/exemplos/', $pageURL);
    $pageURL = $pageURL[0];

    return $pageURL;
}

function getParamsFromUrl()
{
    $arrParams = array();

    foreach ($_GET as $key => $value) {
        $arrParams[$key] = $value;
    }

    return $arrParams;
}

/** PARÂMETROS
 * codigoAluno
 * codigoCurso
 * disciplina
 * situacao
 * dataInformacao
 * nota
 * anoAprovacao
 * detalhe
 */

$hostUniversa = baseUrl();
$arrParams    = getParamsFromUrl();

$process = curl_init($hostUniversa . '/api/v1/aluno-notas');

curl_setopt_array(
    $process,
    array(
        CURLOPT_USERPWD        => $arrParams['token'] . ":" . $arrParams['token'],
        CURLOPT_POST           => true,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POSTFIELDS     => $arrParams
    )
);

$return = curl_exec($process);
curl_close($process);
echo '<h2>Código:</h2>';
highlight_string(file_get_contents(__FILE__));
echo '<h2>Resposta:</h2>';
echo '<pre>' . print_r(json_encode(json_decode($return, 1), JSON_PRETTY_PRINT), 1) . '</pre>';
?>