(function ($, window, document, undefined) {
    var componente = function () {
        var pub = this, priv = {};


        /**
         * Cria um identificador exclusivo (GUID)
         * @returns {string}
         */
        this.createGUID = function () {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }

            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                   s4() + '-' + s4() + s4() + s4();
        };

        priv.defaults = {
            inputName: 'componente',
            btnAdd: ".btn-add",
            list: '.list',
            element: null,
            addCallback: null,
            removeCallback: null,
            loadCallback: null,
            changeOrderCallback: null,
            classCardExtra: '',
            vfileThumbBaseUrl: '',
            vfileDownloadBaseUrl: '',
            labelContainer: '<div/>',
            tplItem: '',
            labelPosition: 'before',
            fieldClass: '',
            sortable: true,
            sortableHandle: '.card-head',
            fields: [
                {name: 'text', label: 'Descrição', type: 'textarea'},
                {
                    name: 'image',
                    label: 'Imagem',
                    type: 'file',
                    addCallback: function (e) {
                        var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;

                        if (typeof $.fn.vfile != 'undefined') {
                            var vfile = field.vfile({
                                thumbBaseUrl: config.settings.vfileThumbBaseUrl,
                                downloadBaseUrl: config.settings.vfileDownloadBaseUrl
                            });

                            if (itemConfig.image) {
                                if (Object.keys(itemConfig.image).length > 0) {
                                    vfile.setSelection(itemConfig.image);
                                }
                            }
                        }
                    }
                },
                {name: 'codigo', label: 'Código', type: 'hidden'}
            ]
        };

        priv.settings = priv.defaults;

        priv.componenteFields = [
            {name: 'id', label: '', type: 'hidden'},
            {name: 'index', label: '', type: 'hidden'}
        ];

        priv.charToNum = function (alpha) {
            var index = 0;

            for (var i = 0, j = 1; i < j; i++, j++) {
                if (alpha == priv.numToChar(i)) {
                    index = i;
                    j = i;
                }
            }
        };

        priv.numToChar = function (number) {
            var numeric = (number - 1) % 26;
            var letter = priv.chr(65 + numeric);
            var number2 = parseInt((number - 1) / 26);

            if (number2 > 0) {
                return priv.numToChar(number2) + letter;
            } else {
                return letter;
            }
        };

        priv.chr = function (codePt) {
            if (codePt > 0xFFFF) {
                codePt -= 0x10000;

                return String.fromCharCode(0xD800 + (codePt >> 10), 0xDC00 + (codePt & 0x3FF));
            }

            return String.fromCharCode(codePt);
        };

        pub.get = function (key) {
            key = key || false;

            if (!key) {
                return priv;
            }

            return priv.settings[key];
        };

        pub.set = function (key, value) {
            key = key || false;

            if (!key) {
                return false;
            }

            value = value || false;

            return priv.settings[key] = value;
        };

        priv.getList = function () {
            return priv.getContainer().find(priv.settings.list);
        };

        priv.getContainer = function () {
            return priv.settings.element;
        };

        priv.getTplItem = function () {
            var tplItem = priv.settings.tplItem + '';

            if (tplItem.length == 0) {
                tplItem = '\
            <div class="' + priv.settings.classCardExtra + '">\
                <div class="card card-bordered style-primary">\
                    <div class="card-head card-head-xs">\
                        <div class="tools">\
                            <div class="btn-group">\
                                <a href="#" class="btn btn-icon-toggle item-remove">\
                                    <i class="fa fa-close"></i>\
                                </a>\
                            </div>\
                        </div>\
                    </div>\
                    <div class="card-body style-default-bright componente-content"">\
                    </div >\
                </div >\
            </div >';
            }

            var $item = $(tplItem);
            $item.addClass('componente-item');

            return $item;
        };

        priv.makeFieldName = function (param) {
            var id = param.id || '';
            var name = param.name || '';
            return priv.settings.inputName + '[' + id + '][' + name + ']';

        };

        pub.makeFieldName = priv.makeFieldName;

        priv.addAction = function (param) {
            param = param || [];

            var opt = {
                event: false,
                text: '',
                image: null,
                index: null,
                codigo: ''
            };

            opt = $.extend(opt, param);

            //verifica se a chamada foi feita através de um evento
            if (typeof param.target != 'undefined') {
                param = {event: param};
            }


            if (!opt.index) {
                opt.index = pub.length();
            } else {
                opt.index *= 1;
            }

            opt.id = pub.createGUID();

            var item = priv.getTplItem();

            priv.getList().append(item);

            var fields = $.merge($.merge([], priv.settings.fields), priv.componenteFields);

            $.each(fields, function (i, fieldItem) {
                var fieldName = priv.makeFieldName({id: opt.id, name: fieldItem.name});
                var fieldId = pub.createGUID();
                var $fieldElement = null;
                var fieldValue = '';

                if (typeof(opt[fieldItem.name]) != 'undefined') {
                    fieldValue = opt[fieldItem.name];
                }

                if (fieldItem.type == 'textarea') {
                    $fieldElement = $('<textarea name="' + fieldName + '">' + fieldValue + '</textarea>');
                } else if (fieldItem.type == 'select') {
                    $fieldElement = $('<select name="' + fieldName + '"><option/></select>');

                    $.each(fieldItem.options || [], function (optionIndex, optionItem) {
                        var $option = $('<option/>');
                        $option.val(optionItem.value).html(optionItem.text);

                        if (optionItem.value == fieldValue) {
                            $option.attr('selected', true);
                        }

                        $fieldElement.append($option);
                    });
                } else if (fieldItem.type == 'radio') {
                    $fieldElement = $('<div/>');

                    $.each(fieldItem.options || [], function (optionIndex, optionItem) {
                        var $optionLabel = $('<label class="radio-inline"></label>');
                        var $option = $('<input type="radio" name="' + fieldName + '" id="' + fieldName + '[' +
                                        optionIndex + ']">');
                        $option.val(optionItem.value);

                        if (optionItem.value == fieldValue) {
                            $option.prop('checked', true);
                        }

                        $optionLabel.append($option);
                        $optionLabel.append(optionItem.text);

                        $fieldElement.append($optionLabel);
                    });
                } else {
                    $fieldElement =
                        $('<input type="' + fieldItem.type + '" name="' + fieldName + '" value="' + fieldValue + '">');
                }

                var fieldClass = priv.settings.fieldClass;

                if (fieldItem.type == 'radio' && fieldClass == 'form-control') {
                    fieldClass = 'input-group';
                }

                $fieldElement.attr('class', fieldClass);
                $fieldElement.attr('id', fieldId);

                if (!(fieldItem.type == 'hidden' || fieldItem.label.trim() == '')) {
                    var $labelContainer = $(priv.settings.labelContainer);
                    $labelContainer.append($fieldElement);

                    var labelHTML = '<label class="componente-label" for="' + fieldId + '">' + fieldItem.label +
                                    ':</label>';

                    if (priv.settings.labelPosition == 'before') {
                        $labelContainer.prepend(labelHTML);
                    } else {
                        $labelContainer.append(labelHTML);
                    }

                    item.find('.componente-content').append($labelContainer);
                } else {
                    item.find('.componente-content').append($fieldElement);
                }


                var parametersCallback = {item: item, field: $fieldElement, itemSettings: opt, settings: priv};

                if (typeof fieldItem.addCallback == 'function') {
                    fieldItem.addCallback({data: parametersCallback});
                }

                if (typeof fieldItem.changeCallback == 'function') {
                    $(document).on('change', '#' + fieldId, parametersCallback, fieldItem.changeCallback);
                }
            });
            var firstField = item.find('input:visible:first, textarea:visible:first, select:visible:first').eq(0);

            $(priv.getList()).animate({
                scrollTop: priv.getList().children().length * item.offset().top
            }, 500);

            firstField.focus();

            item.data('componente.id', opt.id);

            var parametersCallback = {item: item, itemSettings: opt, settings: priv};

            if (typeof priv.settings.addCallback == 'function') {
                priv.settings.addCallback({data: parametersCallback});
            }

            if (opt.event) {
                opt.event.preventDefault();
            }

            return item;
        };

        priv.removeAction = function (e) {
            var target = $(this).closest('.componente-item');

            if (typeof priv.settings.removeCallback == 'function') {
                var parametersCallback = {item: target, event: e, settings: priv};
                var stop = priv.settings.removeCallback(parametersCallback) || false;

                if (stop) {
                    e.preventDefault();
                    return true;
                }
            }
            target.remove();
            priv.changeOrder();
            e.preventDefault();
        };

        pub.add = function (param) {
            priv.addAction(param);
        };

        pub.addFields = function (fields) {
            priv.settings.fields = $.merge(priv.settings.fields, fields);
        };

        pub.removeField = function (fieldName) {
            var fields = [];

            $.each(priv.settings.fields, function (i, item) {
                if (item.name != fieldName) {
                    fields.push(item);
                }
            });

            priv.settings.fields = fields;
        };

        pub.remove = function (itemIndex) {
            itemIndex = itemIndex || 0;
            itemIndex = (itemIndex <= 0 ? 0 : itemIndex);

            priv.getList()
                .find('.componente-item')
                .eq(itemIndex)
                .find('.item-remove')
                .click();
        };

        priv.changeOrder = function () {
            var elements = pub.items();

            for (var i = 0; i < elements.length; i++) {
                var item = elements[i];
                var $item = $(item);
                var id = $item.data('componente.id') || '';

                if (!id) {
                    id = $item.find('input[name^="[id]"]');
                }

                var fieldName = priv.makeFieldName({id: id, name: 'index'});

                $item.find('input[name="' + fieldName + '"]').val(i);

                var parametersCallback = {item: $item, index: i, settings: priv};

                if (typeof priv.settings.changeOrderCallback == 'function') {
                    priv.settings.changeOrderCallback({data: parametersCallback});
                }
            }
        };

        pub.length = function () {
            return pub.items().length;
        };

        pub.items = function () {
            return priv.getList().find('.componente-item') || [];
        };

        pub.removeAll = function () {
            $.each(pub.items(), function (i, item) {
                $(item).remove();
            });

        };

        pub.exportJSON = function () {
            var arrRet = [];
            var items = pub.items();

            var fields = $.merge($.merge([], priv.settings.fields), priv.componenteFields);

            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                var id = $(item).data('componente.id');
                var arrItem = {};

                for (var j = 0; j < fields.length; j++) {
                    var fieldItem = fields[j];
                    var fieldName = priv.makeFieldName({id: id, name: fieldItem.name});
                    arrItem[fieldItem.name] = $('[name="' + fieldName + '"]').val() || '';
                }

                arrRet[i] = arrItem;
            }

            return arrRet;
        };

        pub.init = function (param) {
            param = param || [];
            priv.settings = $.extend(priv.defaults, param);

            if (priv.settings.sortable) {
                priv.getList().sortable({handle: priv.settings.sortableHandle, update: priv.changeOrder});
                priv.getList().find(priv.settings.sortableHandle).disableSelection();
            }

            var container = priv.getContainer();
            container.on('click', priv.settings.btnAdd, {}, priv.addAction);
            container.on('click', '.item-remove', {}, priv.removeAction);

            if (typeof priv.settings.loadCallback == 'function') {
                priv.settings.loadCallback();
            }
        };
    };

    $.fn.componente = function () {
        var params = $.extend([], arguments);

        if (params.length == 1) {
            params = params[0];
        }

        var method = false;
        var retval = false;

        if (typeof params === 'string' || typeof componente[params] === 'function') {
            method = params;
            params = undefined;
        } else {
            method = (params || [false])[0];

            if (typeof method === 'string' || typeof componente[method] === 'function') {
                params = params.slice(1)[0];
            }
        }
        var ret = [];

        this.each(function () {
            var $this = $(this);
            var obj = $this.data("componente");

            if (!obj) {
                obj = new componente();

                var initParam = params || [];
                initParam['element'] = $this;
                obj.init(initParam);

                $this.data("componente", obj);
            } else if (method) {
                retval = params ? obj[method](params) : obj[method]();
            }

            ret.push(retval === false ? obj : retval);
        });

        if (ret.length == 1) {
            ret = ret.pop();
        }

        return ret;
    };
})(window.jQuery, window, document);