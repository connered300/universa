(function ($, window, document) {
    'use strict';

    var TipoContatoAdd = function () {
        VersaShared.call(this);
        var __tipoContatoAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {},
            data: {},
            value: {},
            datatables: {},
            wizardElement: '#tipo-contato-wizard',
            formElement: '#tipo-contato-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
        };

        this.setValidations = function () {
            __tipoContatoAdd.options.validator.settings.rules =
            {tipContatoNome: {maxlength: 45, required: true}, tipContatoDescricao: {maxlength: 255, required: true},};
            __tipoContatoAdd.options.validator.settings.messages = {
                tipContatoNome: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                tipContatoDescricao: {maxlength: 'Tamanho máximo: 255!', required: 'Campo obrigatório!'},
            };

            $(__tipoContatoAdd.options.formElement).submit(function (e) {
                var ok = $(__tipoContatoAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__tipoContatoAdd.options.formElement);

                if (__tipoContatoAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __tipoContatoAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __tipoContatoAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__tipoContatoAdd.options.listagem) {
                                    $.tipoContatoIndex().reloadDataTableTipoContato();
                                    __tipoContatoAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#tipo-contato-modal').modal('hide');
                                }

                                __tipoContatoAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaTipoContato = function (tip_contato_id, callback) {
            var $form = $(__tipoContatoAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __tipoContatoAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + tip_contato_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __tipoContatoAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __tipoContatoAdd.removeOverlay($form);
                },
                erro: function () {
                    __tipoContatoAdd.showNotificacaoDanger('Erro desconhecido!');
                    __tipoContatoAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.tipoContatoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.pessoa.tipo-contato.add");

        if (!obj) {
            obj = new TipoContatoAdd();
            obj.run(params);
            $(window).data('universa.pessoa.tipo-contato.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);