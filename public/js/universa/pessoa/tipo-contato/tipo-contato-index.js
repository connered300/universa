(function ($, window, document) {
    'use strict';
    var TipoContatoIndex = function () {
        VersaShared.call(this);
        var __tipoContatoIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                tipoContato: null
            },
        };

        this.setSteps = function () {
            var colNum = 0;
            __tipoContatoIndex.options.datatables.tipoContato = $('#dataTableTipoContato').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __tipoContatoIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary tipoContato-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger tipoContato-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                        }

                        __tipoContatoIndex.removeOverlay($('#container-tipo-contato'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "tip_contato_id", targets: colNum++, data: "tip_contato_id"},
                    {name: "tip_contato_nome", targets: colNum++, data: "tip_contato_nome"},
                    {name: "tip_contato_descricao", targets: colNum++, data: "tip_contato_descricao"},
                    {name: "tip_contato_id", targets: colNum++, data: "acao"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $('#dataTableTipoContato').on('click', '.tipoContato-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __tipoContatoIndex.options.datatables.tipoContato.fnGetData($pai);

                if (!__tipoContatoIndex.options.ajax) {
                    location.href = __tipoContatoIndex.options.url.edit + '/' + data['tip_contato_id'];
                } else {
                    $.tipoContatoAdd().pesquisaTipoContato(
                        data['tip_contato_id'],
                        function (data) { __tipoContatoIndex.showModal(data); }
                    );
                }
            });


            $('#btn-tipo-contato-add').click(function (e) {
                if (__tipoContatoIndex.options.ajax) {
                    __tipoContatoIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#dataTableTipoContato').on('click', '.tipoContato-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __tipoContatoIndex.options.datatables.tipoContato.fnGetData($pai);
                var arrRemover = {
                    tipContatoId: data['tip_contato_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de tipo "' + data['tip_contato_descricao'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __tipoContatoIndex.addOverlay(
                                $('#container-tipo-contato'), 'Aguarde, solicitando remoção de registro de tipo...'
                            );
                            $.ajax({
                                url: __tipoContatoIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __tipoContatoIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de tipo:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __tipoContatoIndex.removeOverlay($('#container-tipo-contato'));
                                    } else {
                                        __tipoContatoIndex.reloadDataTableTipoContato();
                                        __tipoContatoIndex.showNotificacaoSuccess(
                                            "Registro de tipo removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#tipo-contato-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['tipContatoId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['tipContatoId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);


            $('#tipo-contato-modal .modal-title').html(actionTitle + ' tipo');
            $('#tipo-contato-modal').modal();
        };

        this.reloadDataTableTipoContato = function () {
            this.getDataTableTipoContato().api().ajax.reload(null, false);
        };

        this.getDataTableTipoContato = function () {
            if (!__tipoContatoIndex.options.datatables.tipoContato) {
                if (!$.fn.dataTable.isDataTable('#dataTableTipoContato')) {
                    __tipoContatoIndex.options.datatables.tipoContato = $('#dataTableTipoContato').DataTable();
                } else {
                    __tipoContatoIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __tipoContatoIndex.options.datatables.tipoContato;
        };

        this.run = function (opts) {
            __tipoContatoIndex.setDefaults(opts);
            __tipoContatoIndex.setSteps();
        };
    };

    $.tipoContatoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.pessoa.tipo-contato.index");

        if (!obj) {
            obj = new TipoContatoIndex();
            obj.run(params);
            $(window).data('universa.pessoa.tipo-contato.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);