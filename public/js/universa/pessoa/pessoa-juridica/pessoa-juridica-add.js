(function ($, window, document) {
    'use strict';

    var PessoaJuridicaAdd = function () {
        VersaShared.call(this);
        var __pessoaJuridicaAdd = this;
        this.defaults = {
            url: {
                buscaCEP: '',
                pesquisaPJ: ''
            },
            ajaxSubmit: false,
            desativaAutocompletePessoa: false,
            data: {
                arrEstados: [],
                pessoaPesquisa: null,
            },
            value: {},
            callback: {},
            datatables: {},
            wizardElement: '#pessoa-juridica-wizard',
            formElement: '#pessoa-juridica-form',
            validator: null
        };

        this.steps = {};

        this.steps.dadosPessoais = {
            init: function () {
                var $form = $(__pessoaJuridicaAdd.options.formElement);
                $form.find("[name=pesCnpj]").inputmask({
                    showMaskOnHover: false, clearIncomplete: true, mask: ['99.999.999/9999-99']
                });

                $form.find("[name=pesInscMunicipal],[name=pesInscEstadual]").inputmask({
                    showMaskOnHover: false, clearIncomplete: true, mask: ['9{8,14}']
                });

                if (
                    $form.find("[name=pesId]").val() != "" ||
                    __pessoaJuridicaAdd.options.desativaAutocompletePessoa
                ) {
                    $form.find('.buscarDocumentoPessoa').parent().addClass('hide');
                } else {
                    $form.find("[name=pesNome]")
                        .blur(function () {
                            $(this).removeClass('ui-autocomplete-loading');
                        })
                        .autocomplete({
                            source: function (request, response) {
                                var $element = $(this.element);
                                var previous_request = $element.data("jqXHR");

                                if (previous_request) {
                                    previous_request.abort();
                                }

                                $element.data("jqXHR", $.ajax({
                                    url: __pessoaJuridicaAdd.options.url.pesquisaPJ,
                                    data: {query: request.term},
                                    type: 'POST',
                                    dataType: "json",
                                    success: function (data) {
                                        var transformed = $.map(data || [], function (el) {
                                            el.label = el.pesNome;

                                            return el;
                                        });

                                        response(transformed);
                                    }
                                }));
                            },
                            minLength: 2,
                            select: function (event, ui) {
                                __pessoaJuridicaAdd.options.data.pessoaPesquisa = ui.item;
                                __pessoaJuridicaAdd.steps.dadosPessoais.selecaoPessoa($form);
                            }
                        });
                }

                $form.find(".buscarDocumentoPessoa").click(function () {
                    var arrPesquisa = {};
                    var pesCnpj = $form.find("[name=pesCnpj]").val();
                    var pesCnpjNums = pesCnpj.replace(/[^0-9]/g, '');

                    if (pesCnpjNums.length < 11) {
                        __pessoaJuridicaAdd.showNotificacaoWarning('CNPJ inválido!');

                        return;
                    }
                    arrPesquisa.pesCnpj = pesCnpj;

                    __pessoaJuridicaAdd.steps.dadosPessoais.pesquisarPesssoaPeloDocumento(arrPesquisa, $form);
                });
            },
            selecaoPessoa: function ($form) {
                var dados = __pessoaJuridicaAdd.options.data.pessoaPesquisa || {};

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: '' +
                    'Deseja realmente carregar os dados no formulário?' +
                    '<br>Razão Social:<b> ' + dados['pesNome'] + '</b>' +
                    '<br>Nome Fantasia:<b> ' + dados['pesNomeFantasia'] + '</b>' +
                    '<br>CNPJ:<b> ' + dados['pesCnpj'] + '</b>' +
                    '<br>Cidade/Estado:<b> ' + dados['endCidade'] + ' - ' + dados['endEstado'] + '</b>',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        var dados = __pessoaJuridicaAdd.options.data.pessoaPesquisa || {};
                        __pessoaJuridicaAdd.steps.dadosPessoais.buscarDadosPessoa(dados['pesId'], $form);
                    }

                    __pessoaJuridicaAdd.options.data.pessoaPesquisa = null;
                });

            },
            pesquisarPesssoaPeloDocumento: function (criterioBusca, $form) {
                $form = $(__pessoaJuridicaAdd.options.formElement);
                criterioBusca = criterioBusca || {};
                __pessoaJuridicaAdd.addOverlay($form, 'Aguarde. Pesquisando pessoa pelo documento...');

                $.ajax({
                    url: __pessoaJuridicaAdd.options.url.pesquisaPJ,
                    dataType: 'json',
                    type: 'post',
                    data: criterioBusca,
                    success: function (data) {
                        __pessoaJuridicaAdd.removeOverlay($form);

                        if (Object.keys(data).length == 0) {
                            __pessoaJuridicaAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                        } else {
                            __pessoaJuridicaAdd.options.data.pessoaPesquisa = data[0];
                            __pessoaJuridicaAdd.steps.dadosPessoais.selecaoPessoa($form);
                        }
                    },
                    erro: function () {
                        __pessoaJuridicaAdd.showNotificacaoDanger('Erro desconhecido!');
                        __pessoaJuridicaAdd.removeOverlay($form);
                    }
                });
            },
            buscarDadosPessoa: function (pesId, $form) {
                $form = $form || $(__pessoaJuridicaAdd.options.formElement);
                pesId = pesId || '';

                if (!$.isNumeric(pesId)) {
                    $form[0].reset();
                    $form.find('[name=pesId]').val('');
                    __pessoaJuridicaAdd.validate();

                    return;
                }

                var $parent = $form.closest('.modal');

                if ($parent.length == 0) {
                    $parent = $form;
                }

                __pessoaJuridicaAdd.addOverlay($parent, 'Aguarde. Buscando informações adicionais...');

                $.ajax({
                    url: __pessoaJuridicaAdd.options.url.pesquisaPJ,
                    dataType: 'json',
                    type: 'post',
                    data: {pesId: pesId, dadosCompletos: true},
                    success: function (data) {
                        __pessoaJuridicaAdd.removeOverlay($parent);

                        if (Object.keys(data).length == 0) {
                            __pessoaJuridicaAdd.showNotificacaoInfo('Dados não encontrados!');
                        } else {
                            var dados = data;
                            $form[0].reset();
                            $form.deserialize(dados);
                            __pessoaJuridicaAdd.validate();
                        }
                    },
                    erro: function () {
                        __pessoaJuridicaAdd.showNotificacaoDanger('Erro desconhecido!');
                        __pessoaJuridicaAdd.removeOverlay($parent);
                    }
                });
            },
            validate: function () {
                __pessoaJuridicaAdd.options.validator.settings.rules = {
                    pesNacionalidade: {required: true},
                    pesCnpj: {cnpj: true, required: false},
                    pesNome: {required: true, maxlength: 255},
                    pesNomeFantasia: {required: true, maxlength: 255}
                };
                __pessoaJuridicaAdd.options.validator.settings.messages = {
                    pesNacionalidade: {required: 'Campo obrigatório!'},
                    pesCnpj: {required: 'Campo obrigatório!', cnpj: 'CNPJ inválido!'},
                    pesNome: {required: 'Campo obrigatório!', maxlength: 'Tamanho máximo: 255!'},
                    pesNomeFantasia: {required: 'Campo obrigatório!', maxlength: 'Tamanho máximo: 255!'}
                };

                return !$(__pessoaJuridicaAdd.options.formElement).valid();
            }
        };

        this.steps.contatoEndereco = {
            init: function () {
                var $form = $(__pessoaJuridicaAdd.options.formElement);

                $form.find("[name=endCep]").inputmask({
                    showMaskOnHover: false, clearIncomplete: true, mask: ['99999-999']
                });
                $form.find("[name=endNumero]").inputmask({showMaskOnHover: false, mask: ['9{0,9}']});
                $form.find("[name=conContatoTelefone], [name=conContatoCelular]").inputmask({
                    showMaskOnHover: false, clearIncomplete: true, mask: ['(99) 9999-9999', '(99) 99999-9999']
                });
                $form.find(".buscarCep").click(function () {
                    var $element = $(this);
                    var endCep = $form.find("[name=endCep]").val();
                    var endCepNums = endCep.replace(/[^0-9]/g, '');

                    if (endCepNums.length < 8) {
                        __pessoaJuridicaAdd.showNotificacaoWarning('CEP inválido ou não encontrado!');

                        return;
                    }

                    $element.prop('disabled', true);

                    $.ajax({
                        url: __pessoaJuridicaAdd.options.url.buscaCEP,
                        dataType: 'json',
                        type: 'post',
                        data: {cep: endCep},
                        success: function (data) {
                            $element.prop('disabled', false);

                            if (!data.dados) {
                                __pessoaJuridicaAdd.showNotificacaoWarning('CEP inválido ou não encontrado!');
                            }

                            $form.find("[name=endLogradouro]").val(
                                ((data.dados.tip_logr_nome || '') + ' ' + (data.dados.logr_nome || '')).trim()
                            );
                            $form.find("[name=endCidade]").val(data.dados.cid_nome || '');
                            $form.find("[name=endEstado]").val(data.dados.est_uf || '').trigger('change');
                            $form.find("[name=endBairro]").val(data.dados.bai_nome || '');
                        },
                        erro: function () {
                            __pessoaJuridicaAdd.showNotificacaoDanger('Falha ao buscar CEP!');
                            $element.prop('disabled', false);
                        }
                    });
                });
                $form.find("[name=endCidade]")
                    .blur(function () {
                        $(this).removeClass('ui-autocomplete-loading');
                    })
                    .autocomplete({
                        source: function (request, response) {
                            var $element = $(this.element);
                            var previous_request = $element.data("jqXHR");

                            if (previous_request) {
                                previous_request.abort();
                            }

                            $element.data("jqXHR", $.ajax({
                                url: __pessoaJuridicaAdd.options.url.buscaCEP,
                                data: {cidade: request.term},
                                type: 'POST',
                                dataType: "json",
                                success: function (data) {
                                    var transformed = $.map(data.dados || [], function (el) {
                                        el.label = el.cid_nome;

                                        return el;
                                    });

                                    response(transformed);
                                }
                            }));
                        },
                        minLength: 2,
                        focus: function (event, ui) {
                            var $element = $(this);
                            $element.val(ui.item.cid_nome);

                            return false;
                        },
                        select: function (event, ui) {
                            var $element = $(this);
                            $element.val(ui.item.cid_nome);
                            $form.find("[name=endEstado]").val(ui.item.est_uf).trigger('change');

                            return false;
                        }
                    });

                $form.find('[name=pesNascUf], [name=endEstado]').select2({
                    language: 'pt-BR',
                    allowClear: true,
                    data: function () {
                        return {results: __pessoaJuridicaAdd.options.data.arrEstados || []};
                    }
                });

            },

            validate: function () {
                var $form = $(__pessoaJuridicaAdd.options.formElement);
                __pessoaJuridicaAdd.options.validator.settings.rules = {
                    endCep: {required: false},
                    endLogradouro: {required: true},
                    endNumero: {
                        required: function () {
                            return $form.find('[name=endComplemento]').val() == '';
                        }, number: true
                    },
                    endComplemento: {
                        required: function () {
                            return $form.find('[name=endNumero]').val() == '';
                        }
                    },
                    endBairro: {required: false},
                    endCidade: {required: true},
                    endEstado: {required: true},
                    conContatoTelefone: {telefoneValido:true, required: false},
                    conContatoEmail: {required: true, email: true}
                };
                __pessoaJuridicaAdd.options.validator.settings.messages = {
                    endCep: {required: 'Campo obrigatório!'},
                    endLogradouro: {required: 'Campo obrigatório!'},
                    endNumero: {required: 'Campo obrigatório!', number: 'Número inválido!'},
                    endComplemento: {required: 'Campo obrigatório!'},
                    endBairro: {required: 'Campo obrigatório!'},
                    endCidade: {required: 'Campo obrigatória!'},
                    endEstado: {required: 'Campo obrigatório!'},
                    conContatoTelefone: {required: 'Campo obrigatório!'},
                    conContatoEmail: {required: 'Campo obrigatório!', email: 'Email inválido!'}
                };

                return !$(__pessoaJuridicaAdd.options.formElement).valid();
            }
        };

        this.salvar = function (form) {
            var $form = $(__pessoaJuridicaAdd.options.formElement);

            if ($form.data('noSubmit') == undefined) {
                __pessoaJuridicaAdd.validate();
            }

            if ($form.data('noSubmit')) {
                return false;
            }

            if (__pessoaJuridicaAdd.options.ajaxSubmit || $form.data('ajax')) {
                var $parent = $form.closest('.modal');

                if ($parent.length == 0) {
                    $parent = $form;
                }

                __pessoaJuridicaAdd.addOverlay($parent, 'Salvando registro. Aguarde...');
                var dados = $form.serializeJSON();
                dados['ajax'] = true;

                if (!$form.data('ajax')) {
                    $form.data('ajax', true);

                    $.ajax({
                        url: $form.attr('action'),
                        type: 'POST',
                        dataType: 'json',
                        data: dados,
                        success: function (data) {
                            var retorno = data['arrDados'] || dados;

                            if (data.erro) {
                                __pessoaJuridicaAdd.showNotificacaoDanger(
                                    data['mensagem'] || 'Não foi possível salvar registro.'
                                );
                            } else {
                                var aposSalvarFN = __pessoaJuridicaAdd
                                    .getFunction(__pessoaJuridicaAdd.options.callback, 'aposSalvar');

                                __pessoaJuridicaAdd.showNotificacaoSuccess(
                                    data['mensagem'] || 'Registro salvo com sucesso!'
                                );

                                if (aposSalvarFN) {
                                    aposSalvarFN(retorno, data);
                                }
                            }

                            $form.data('ajax', false);
                            __pessoaJuridicaAdd.removeOverlay($parent);
                        }
                    });
                } else {
                    $form.data('ajax', false);
                }

                return false;
            } else {
                form.submit();
            }
        };

        this.submitHandler = this.salvar;

        this.buscarDadosPessoa = function (pesId) {
            __pessoaJuridicaAdd.steps.dadosPessoais.buscarDadosPessoa(pesId);
        };

        this.setCallback = function (callback, func) {
            __pessoaJuridicaAdd.options.callback[callback] = func;
        };

        this.setValidations = function () {
            $(document).on("change", __pessoaJuridicaAdd.options.formElement + " .form-control:hidden", function () {
                $(__pessoaJuridicaAdd.options.formElement).valid();
            });

            $(__pessoaJuridicaAdd.options.formElement).submit(function (e) {
                var ok = __pessoaJuridicaAdd.validate();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }
            });
        };

        this.run = function (opts) {
            __pessoaJuridicaAdd.setDefaults(opts);
            __pessoaJuridicaAdd.loadSteps();
            __pessoaJuridicaAdd.setValidations();
            __pessoaJuridicaAdd.wizard();
        };
    };

    $.pessoaJuridicaAdd = function (params) {
        params = $.extend([], arguments);

        if (params.length == 1) {
            params = params[0];
        }

        var method = false;
        var retval = false;

        if (typeof params === 'string' || typeof PessoaJuridicaAdd[params] === 'function') {
            method = params;
            params = undefined;
        } else {
            method = (params || [false])[0];

            if (typeof method === 'string' || typeof PessoaJuridicaAdd[method] === 'function') {
                params = params.slice(1)[0];
            }
        }

        var ret = [];

        var obj = $(window).data("universa.pessoaJuridica.add");

        if (!obj) {
            obj = new PessoaJuridicaAdd();
            obj.run(params);
            $(window).data('universa.pessoaJuridica.add', obj);
        } else if (method) {
            retval = params ? obj[method](params) : obj[method]();
        }

        ret.push(retval === false ? obj : retval);

        if (ret.length == 1) {
            ret = ret.pop();
        }

        return ret;
    };
})(window.jQuery, window, document);