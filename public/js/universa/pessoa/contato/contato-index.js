(function ($, window, document) {
    'use strict';
    var ContatoIndex = function () {
        VersaShared.call(this);
        var __contatoIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                contato: null
            },
        };

        this.setSteps = function () {
            var colNum = 0;
            __contatoIndex.options.datatables.contato = $('#dataTableContato').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __contatoIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary contato-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger contato-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                        }

                        __contatoIndex.removeOverlay($('#container-contato'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "con_id", targets: colNum++, data: "con_id"},
                    {name: "pes_nome", targets: colNum++, data: "pes_nome"},
                    {name: "tip_contato_nome", targets: colNum++, data: "tip_contato_nome"},
                    {name: "con_contato", targets: colNum++, data: "con_contato"},
                    {name: "con_id", targets: colNum++, data: "acao"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $('#dataTableContato').on('click', '.contato-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __contatoIndex.options.datatables.contato.fnGetData($pai);

                if (!__contatoIndex.options.ajax) {
                    location.href = __contatoIndex.options.url.edit + '/' + data['con_id'];
                } else {
                    $.contatoAdd().pesquisaContato(
                        data['con_id'],
                        function (data) { __contatoIndex.showModal(data); }
                    );
                }
            });


            $('#btn-contato-add').click(function (e) {
                if (__contatoIndex.options.ajax) {
                    __contatoIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#dataTableContato').on('click', '.contato-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __contatoIndex.options.datatables.contato.fnGetData($pai);
                var arrRemover = {
                    conId: data['con_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de contato "' + data['pessoa'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __contatoIndex.addOverlay(
                                $('#container-contato'), 'Aguarde, solicitando remoção de registro de contato...'
                            );
                            $.ajax({
                                url: __contatoIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __contatoIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de contato:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __contatoIndex.removeOverlay($('#container-contato'));
                                    } else {
                                        __contatoIndex.reloadDataTableContato();
                                        __contatoIndex.showNotificacaoSuccess(
                                            "Registro de contato removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#contato-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['conId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['conId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);

            $("#pessoa").select2('data', data['pessoa']).trigger("change");
            $("#tipoContato").select2('data', data['tipoContato']).trigger("change");

            $('#contato-modal .modal-title').html(actionTitle + ' contato');
            $('#contato-modal').modal();
        };

        this.reloadDataTableContato = function () {
            this.getDataTableContato().api().ajax.reload(null, false);
        };

        this.getDataTableContato = function () {
            if (!__contatoIndex.options.datatables.contato) {
                if (!$.fn.dataTable.isDataTable('#dataTableContato')) {
                    __contatoIndex.options.datatables.contato = $('#dataTableContato').DataTable();
                } else {
                    __contatoIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __contatoIndex.options.datatables.contato;
        };

        this.run = function (opts) {
            __contatoIndex.setDefaults(opts);
            __contatoIndex.setSteps();
        };
    };

    $.contatoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.pessoa.contato.index");

        if (!obj) {
            obj = new ContatoIndex();
            obj.run(params);
            $(window).data('universa.pessoa.contato.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);