(function ($, window, document) {
    'use strict';

    var ContatoAdd = function () {
        VersaShared.call(this);
        var __contatoAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                pessoa: '',
                tipoContato: ''
            },
            data: {},
            value: {
                pessoa: null,
                tipoContato: null
            },
            datatables: {},
            wizardElement: '#contato-wizard',
            formElement: '#contato-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#pessoa").select2({
                language: 'pt-BR',
                ajax: {
                    url: __contatoAdd.options.url.pessoa,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.pes_nome, id: el.pes_id};
                        });

                        return {results: transformed};
                    }
                }
            });

            if (__contatoAdd.getPessoa()) {
                $('#pessoa').select2("data", __contatoAdd.getPessoa());
            }
            $("#tipoContato").select2({
                language: 'pt-BR',
                ajax: {
                    url: __contatoAdd.options.url.tipoContato,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.tip_contato_nome, id: el.tip_contato_id};
                        });

                        return {results: transformed};
                    }
                }
            });

            if (__contatoAdd.getTipoContato()) {
                $('#tipoContato').select2("data", __contatoAdd.getTipoContato());
            }
        };

        this.setPessoa = function (pessoa) {
            this.options.value.pessoa = pessoa || null;
        };

        this.getPessoa = function () {
            return this.options.value.pessoa || null;
        };

        this.setTipoContato = function (tipoContato) {
            this.options.value.tipoContato = tipoContato || null;
        };

        this.getTipoContato = function () {
            return this.options.value.tipoContato || null;
        };
        this.setValidations = function () {
            __contatoAdd.options.validator.settings.rules = {
                pessoa: {number: true, required: true}, tipoContato: {number: true, required: true},
                conContato: {maxlength: 255, required: true},
            };
            __contatoAdd.options.validator.settings.messages = {
                pessoa: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                tipoContato: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                conContato: {maxlength: 'Tamanho máximo: 255!', required: 'Campo obrigatório!'},
            };

            $(__contatoAdd.options.formElement).submit(function (e) {
                var ok = $(__contatoAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__contatoAdd.options.formElement);

                if (__contatoAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __contatoAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __contatoAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__contatoAdd.options.listagem) {
                                    $.contatoIndex().reloadDataTableContato();
                                    __contatoAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#contato-modal').modal('hide');
                                }

                                __contatoAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaContato = function (con_id, callback) {
            var $form = $(__contatoAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __contatoAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + con_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __contatoAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __contatoAdd.removeOverlay($form);
                },
                erro: function () {
                    __contatoAdd.showNotificacaoDanger('Erro desconhecido!');
                    __contatoAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.contatoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.pessoa.contato.add");

        if (!obj) {
            obj = new ContatoAdd();
            obj.run(params);
            $(window).data('universa.pessoa.contato.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);