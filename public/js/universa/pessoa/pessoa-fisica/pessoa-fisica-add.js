(function ($, window, document) {
    'use strict';

    var PessoaFisicaAdd = function () {
        VersaShared.call(this);
        var __pessoaFisicaAdd = this;
        this.defaults = {
            url: {
                buscaCEP: '',
                pesquisaPF: ''
            },
            data: {
                pessoaPesquisa: null,
                arrEstados: [],
                necessidadesEspeciais: []
            },
            ajaxSubmit: false,
            desativaAutocompletePessoa: false,
            value: {
                pessoaNecessidades: null
            },
            callback: {},
            datatables: {},
            wizardElement: '#pessoa-fisica-wizard',
            formElement: '#pessoa-fisica-form',
            validator: null
        };

        this.steps = {};

        this.steps.dadosPessoais = {
            init: function () {
                var $form = $(__pessoaFisicaAdd.options.formElement);

                $form.find("[name=pesCpf]").inputmask({
                    showMaskOnHover: false, clearIncomplete: true, mask: ['999.999.999-99']
                });
                $form
                    .find("[name=pesNacionalidade],[name=pesSexo],[name=pesEstadoCivil]")
                    .select2({
                        language: 'pt-BR'
                    });
                $form.find("[name=pesNacionalidade]").change(function () {
                    var pesNacionalidade = this.value;
                    $form
                        .find(".campoPesCpf")
                        [pesNacionalidade == 'Estrangeiro' ? 'addClass' : 'removeClass']('hidden');
                    $form
                        .find(".campoPesDocEstrangeiro")
                        [pesNacionalidade != 'Estrangeiro' ? 'addClass' : 'removeClass']('hidden');
                }).trigger('change');

                if (
                    $form.find("[name=pesId]").val() != "" ||
                    __pessoaFisicaAdd.options.desativaAutocompletePessoa
                ) {
                    $form.find('.buscarDocumentoPessoa').parent().addClass('hide');
                } else {
                    $form.find("[name=pesNome]")
                        .blur(function () {
                            $(this).removeClass('ui-autocomplete-loading');
                        })
                        .autocomplete({
                            source: function (request, response) {
                                var $element = $(this.element);
                                var previous_request = $element.data("jqXHR");

                                if (previous_request) {
                                    previous_request.abort();
                                }

                                $element.data("jqXHR", $.ajax({
                                    url: __pessoaFisicaAdd.options.url.pesquisaPF,
                                    data: {query: request.term},
                                    type: 'POST',
                                    dataType: "json",
                                    success: function (data) {
                                        var transformed = $.map(data || [], function (el) {
                                            el.label = el.pesNome;

                                            return el;
                                        });

                                        response(transformed);
                                    }
                                }));
                            },
                            minLength: 2,
                            select: function (event, ui) {
                                __pessoaFisicaAdd.options.data.pessoaPesquisa = ui.item;
                                __pessoaFisicaAdd.steps.dadosPessoais.selecaoPessoa($form);
                            }
                        });

                    $form.find(".buscarDocumentoPessoa").click(function () {
                        var pesNacionalidade = $form.find("[name=pesNacionalidade]").val();
                        var arrPesquisa = {};

                        if (pesNacionalidade != 'Estrangeiro') {
                            var pesCpf = $form.find("[name=pesCpf]").val();
                            var pesCpfNums = pesCpf.replace(/[^0-9]/g, '');

                            if (pesCpfNums.length < 11) {
                                __pessoaFisicaAdd.showNotificacaoDanger('CPF inválido!');

                                return;
                            }
                            arrPesquisa.pesCpf = pesCpf;
                        } else {
                            arrPesquisa.pesDocEstrangeiro = $form.find("[name=pesDocEstrangeiro]").val();
                        }

                        __pessoaFisicaAdd.steps.dadosPessoais.pesquisarPesssoaPeloDocumento(arrPesquisa, $form);
                    });
                }

                $form.find("[name=pesDataNascimento], [name=pesRgEmissao]")
                    .datepicker({
                        prevText: '<i class="fa fa-chevron-left"></i>',
                        nextText: '<i class="fa fa-chevron-right"></i>'
                    })
                    .inputmask({
                        showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']
                    });

                var $pessoaNecessidades = $("[name=pessoaNecessidades]");

                $pessoaNecessidades.select2({
                    language: 'pt-BR',
                    multiple: true,
                    data: function () {
                        return {results: __pessoaFisicaAdd.options.data.necessidadesEspeciais};
                    }
                });
                $pessoaNecessidades.select2(
                    'data',
                    __pessoaFisicaAdd.options.value.pessoaNecessidades || []
                );
            },
            selecaoPessoa: function ($form) {
                var dados = __pessoaFisicaAdd.options.data.pessoaPesquisa || {};

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: '' +
                    'Deseja realmente carregar os dados no formulário?' +
                    '<br>Nome:<b> ' + dados['pesNome'] + '</b>' +
                    '<br>Sexo:<b> ' + dados['pesSexo'] + '</b>' +
                    '<br>CPF:<b> ' + dados['pesCpf'] + '</b>' +
                    '<br>Data de nascimento:<b> ' + dados['pesDataNascimento'] + '</b>' +
                    '<br>Cidade/Estado:<b> ' + dados['endCidade'] + ' - ' + dados['endEstado'] + '</b>',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        var dados = __pessoaFisicaAdd.options.data.pessoaPesquisa || {};
                        __pessoaFisicaAdd.steps.dadosPessoais.buscarDadosPessoa(
                            dados['pesId'], $form
                        );
                    }

                    __pessoaFisicaAdd.options.data.pessoaPesquisa = null;
                });

            },
            pesquisarPesssoaPeloDocumento: function (criterioBusca, $form) {
                $form = $(__pessoaFisicaAdd.options.formElement);
                criterioBusca = criterioBusca || {};
                __pessoaFisicaAdd.addOverlay($form, 'Aguarde. Pesquisando pessoa pelo documento...');

                $.ajax({
                    url: __pessoaFisicaAdd.options.url.pesquisaPF,
                    dataType: 'json',
                    type: 'post',
                    data: criterioBusca,
                    success: function (data) {
                        __pessoaFisicaAdd.removeOverlay($form);

                        if (Object.keys(data).length == 0) {
                            __pessoaFisicaAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                            __pessoaFisicaAdd.steps.dadosPessoais.buscarDadosPessoa('', $form);
                        } else {
                            __pessoaFisicaAdd.options.data.pessoaPesquisa = data[0];
                            __pessoaFisicaAdd.steps.dadosPessoais.selecaoPessoa($form);
                        }
                    },
                    erro: function () {
                        __pessoaFisicaAdd.showNotificacaoDanger('Erro desconhecido!');
                        __pessoaFisicaAdd.removeOverlay($form);
                    }
                });
            },
            buscarDadosPessoa: function (pesId, $form) {
                $form = $form || $(__pessoaFisicaAdd.options.formElement);
                pesId = pesId || '';

                if (!$.isNumeric(pesId)) {
                    $form[0].reset();
                    $form.find('[name=pesId]').val('');
                    __pessoaFisicaAdd.validate();

                    return;
                }

                var $parent = $form.closest('.modal');

                if ($parent.length == 0) {
                    $parent = $form;
                }

                __pessoaFisicaAdd.addOverlay($parent, 'Aguarde. Buscando informações adicionais...');

                $.ajax({
                    url: __pessoaFisicaAdd.options.url.pesquisaPF,
                    dataType: 'json',
                    type: 'post',
                    data: {pesId: pesId, dadosCompletos: true},
                    success: function (data) {
                        __pessoaFisicaAdd.removeOverlay($parent);

                        if (Object.keys(data).length == 0) {
                            __pessoaFisicaAdd.showNotificacaoInfo('Dados não encontrados!');
                        } else {
                            var dados = data;
                            $form[0].reset();
                            $form.deserialize(dados);
                            $form.find("[name=pesNacionalidade],[name=pesSexo],[name=endEstado],[name=pesNascUf],[name=pesEstadoCivil]").trigger('change');
                            __pessoaFisicaAdd.options.value.pessoaNecessidades = dados['pessoaNecessidades'] || [];
                            $("[name=pessoaNecessidades]").select2(
                                'data',
                                __pessoaFisicaAdd.options.value.pessoaNecessidades || []
                            );
                            __pessoaFisicaAdd.validate();
                        }
                    },
                    erro: function () {
                        __pessoaFisicaAdd.showNotificacaoDanger('Erro desconhecido!');
                        __pessoaFisicaAdd.removeOverlay($parent);
                    }
                });
            },
            validate: function () {
                var $form = $(__pessoaFisicaAdd.options.formElement);
                __pessoaFisicaAdd.options.validator.settings.rules = {
                    pesNacionalidade: {required: true},
                    pesCpf: {
                        cpf: true,
                        required: function () {
                            return $form.find("[name=pesNacionalidade]").val() != 'Estrangeiro';
                        }
                    },
                    pesDocEstrangeiro: {
                        required: function () {
                            return $form.find("[name=pesNacionalidade]").val() == 'Estrangeiro';
                        }
                    },
                    pesNome: {required: true, maxlength: 255},
                    pesNaturalidade: {required: true},
                    pesNascUf: {required: true},
                    pesDataNascimento: {required: true, dateBR: true},
                    pesSexo: {required: true},
                    pesRg: {required: true},
                    pesRgEmissao: {required: false, dateBR: true}
                };

                __pessoaFisicaAdd.options.validator.settings.messages = {
                    pesNacionalidade: {required: 'Campo obrigatório!'},
                    pesCpf: {
                        required: 'Campo obrigatório!',
                        cpf: 'CPF inválido!'
                    },
                    pesDocEstrangeiro: {required: 'Campo obrigatório!'},
                    pesNome: {required: 'Campo obrigatório!', maxlength: 'Tamanho máximo: 255!'},
                    pesNaturalidade: {required: 'Campo obrigatório!'},
                    pesNascUf: {required: 'Campo obrigatório!'},
                    pesDataNascimento: {
                        required: 'Campo obrigatório!',
                        dateBR: 'Data inválida!'
                    },
                    pesSexo: {required: 'Campo obrigatório!'},
                    pesRg: {required: 'Campo obrigatório!'},
                    pesRgEmissao: {
                        required: 'Campo obrigatório!',
                        dateBR: 'Data inválida!'
                    }
                };

                return !$(__pessoaFisicaAdd.options.formElement).valid();
            }
        };

        this.steps.contatoEndereco = {
            init: function () {
                var $form = $(__pessoaFisicaAdd.options.formElement);
                $form.find("[name=endCep]").inputmask({
                    showMaskOnHover: false, clearIncomplete: true, mask: ['99999-999']
                });
                $form.find("[name=endNumero]").inputmask({showMaskOnHover: false, mask: ['9{0,9}']});
                $form.find("[name=conContatoTelefone], [name=conContatoCelular]").inputmask({
                    showMaskOnHover: false, clearIncomplete: true, mask: ['(99) 9999-9999', '(99) 99999-9999']
                });
                $form.find(".buscarCep").click(function () {
                    var $element = $(this);
                    var endCep = $form.find("[name=endCep]").val();
                    var endCepNums = endCep.replace(/[^0-9]/g, '');

                    if (endCepNums.length < 8) {
                        __pessoaFisicaAdd.showNotificacaoWarning('CEP inválido ou não encontrado!');

                        return;
                    }

                    $element.prop('disabled', true);

                    $.ajax({
                        url: __pessoaFisicaAdd.options.url.buscaCEP,
                        dataType: 'json',
                        type: 'post',
                        data: {cep: endCep},
                        success: function (data) {
                            $element.prop('disabled', false);

                            if (!data.dados) {
                                __pessoaFisicaAdd.showNotificacaoWarning('CEP inválido ou não encontrado!');
                            }

                            $form.find("[name=endLogradouro]").val(
                                ((data.dados.tip_logr_nome || '') + ' ' + (data.dados.logr_nome || '')).trim()
                            );
                            $form.find("[name=endCidade]").val(data.dados.cid_nome || '');
                            $form.find("[name=endEstado]").val(data.dados.est_uf || '').trigger('change');
                            $form.find("[name=endBairro]").val(data.dados.bai_nome || '');
                        },
                        erro: function () {
                            __pessoaFisicaAdd.showNotificacaoDanger('Falha ao buscar CEP!');
                            $element.prop('disabled', false);
                        }
                    });
                });
                $form.find("[name=pesNaturalidade], [name=endCidade]")
                    .blur(function () {
                        $(this).removeClass('ui-autocomplete-loading');
                    })
                    .autocomplete({
                        source: function (request, response) {
                            var $element = $(this.element);
                            var previous_request = $element.data("jqXHR");

                            if (previous_request) {
                                previous_request.abort();
                            }

                            $element.data("jqXHR", $.ajax({
                                url: __pessoaFisicaAdd.options.url.buscaCEP,
                                data: {cidade: request.term},
                                type: 'POST',
                                dataType: "json",
                                success: function (data) {
                                    var transformed = $.map(data.dados || [], function (el) {
                                        el.label = el.cid_nome;

                                        return el;
                                    });

                                    response(transformed);
                                }
                            }));
                        },
                        minLength: 2,
                        focus: function (event, ui) {
                            var $element = $(this);
                            $element.val(ui.item.cid_nome);

                            return false;
                        },
                        select: function (event, ui) {
                            var $element = $(this);
                            var campo = $element.attr('name') == 'endCidade' ? "endEstado" : "pesNascUf";
                            $element.val(ui.item.cid_nome);
                            $form.find('[name=' + campo + ']').val(ui.item.est_uf).trigger('change');

                            return false;
                        }
                    });

                $form.find('[name=pesNascUf], [name=endEstado]').select2({
                    language: 'pt-BR',
                    allowClear: true,
                    data: function () {
                        return {results: __pessoaFisicaAdd.options.data.arrEstados || []};
                    }
                });
            },
            validate: function () {
                var $form = $(__pessoaFisicaAdd.options.formElement);
                __pessoaFisicaAdd.options.validator.settings.rules = {
                    endCep: {required: false},
                    endLogradouro: {required: true},
                    endNumero: {
                        required: function () {
                            return $form.find('[name=endComplemento]').val() == '';
                        }, number: true
                    },
                    endComplemento: {
                        required: function () {
                            return $form.find('[name=endNumero]').val() == '';
                        }
                    },
                    endBairro: {required: false},
                    endCidade: {required: true},
                    endEstado: {required: true},
                    conContatoTelefone: {telefoneValido:true, required: false},
                    conContatoCelular: {celular: true, required: true},
                    conContatoEmail: {required: true, email: true}
                };
                __pessoaFisicaAdd.options.validator.settings.messages = {
                    endCep: {required: 'Campo obrigatório!'},
                    endLogradouro: {required: 'Campo obrigatório!'},
                    endNumero: {required: 'Campo obrigatório!', number: 'Número inválido!'},
                    endComplemento: {required: 'Campo obrigatório!'},
                    endBairro: {required: 'Campo obrigatório!'},
                    endCidade: {required: 'Campo obrigatória!'},
                    endEstado: {required: 'Campo obrigatório!'},
                    conContatoTelefone: {required: 'Campo obrigatório!'},
                    conContatoCelular: {required: 'Campo obrigatório!'},
                    conContatoEmail: {required: 'Campo obrigatório!', email: 'Email inválido!'}
                };

                return !$(__pessoaFisicaAdd.options.formElement).valid();
            }
        };

        this.salvar = function (form) {
            var $form = $(__pessoaFisicaAdd.options.formElement);

            if ($form.data('noSubmit') == undefined) {
                __pessoaFisicaAdd.validate();
            }

            if ($form.data('noSubmit')) {
                return false;
            }

            if (__pessoaFisicaAdd.options.ajaxSubmit) {
                var $parent = $form.closest('.modal');

                if ($parent.length == 0) {
                    $parent = $form;
                }

                __pessoaFisicaAdd.addOverlay($parent, 'Salvando registro. Aguarde...');

                var dados = $form.serializeJSON();
                dados['ajax'] = true;

                $.ajax({
                    url: $form.attr('action'),
                    type: 'POST',
                    dataType: 'json',
                    data: dados,
                    success: function (data) {
                        var retorno = data['arrDados'] || dados;

                        if (data.erro) {
                            __pessoaFisicaAdd.showNotificacaoDanger(
                                data['mensagem'] || 'Não foi possível salvar registro.'
                            );
                        } else {
                            var aposSalvarFN = __pessoaFisicaAdd
                                .getFunction(__pessoaFisicaAdd.options.callback, 'aposSalvar');

                            __pessoaFisicaAdd.showNotificacaoSuccess(
                                data['mensagem'] || 'Registro salvo com sucesso!'
                            );

                            if (aposSalvarFN) {
                                aposSalvarFN(retorno, data);
                            }
                        }
                        __pessoaFisicaAdd.removeOverlay($parent);
                    }
                });
            } else {
                form.submit();
            }
        };

        this.submitHandler = this.salvar;

        this.buscarDadosPessoa = function (pesId) {
            __pessoaFisicaAdd.steps.dadosPessoais.buscarDadosPessoa(pesId);
        };

        this.setCallback = function (callback, func) {
            __pessoaFisicaAdd.options.callback[callback] = func;
        };

        this.setValidations = function () {
            $(document).on("change", __pessoaFisicaAdd.options.formElement + " .form-control:hidden", function () {
                $(__pessoaFisicaAdd.options.formElement).valid();
            });

            $(__pessoaFisicaAdd.options.formElement).submit(function (e) {
                var ok = __pessoaFisicaAdd.validate();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }
            });
        };

        this.run = function (opts) {
            __pessoaFisicaAdd.setDefaults(opts);
            __pessoaFisicaAdd.loadSteps();
            __pessoaFisicaAdd.setValidations();
            __pessoaFisicaAdd.wizard();
        };
    };

    $.pessoaFisicaAdd = function (params) {
        params = $.extend([], arguments);

        if (params.length == 1) {
            params = params[0];
        }

        var method = false;
        var retval = false;

        if (typeof params === 'string' || typeof PessoaFisicaAdd[params] === 'function') {
            method = params;
            params = undefined;
        } else {
            method = (params || [false])[0];

            if (typeof method === 'string' || typeof PessoaFisicaAdd[method] === 'function') {
                params = params.slice(1)[0];
            }
        }

        var ret = [];

        var obj = $(window).data("universa.pessoaFisica.add");

        if (!obj) {
            obj = new PessoaFisicaAdd();
            obj.run(params);
            $(window).data('universa.pessoaFisica.add', obj);
        } else if (method) {
            retval = params ? obj[method](params) : obj[method]();
        }

        ret.push(retval === false ? obj : retval);

        if (ret.length == 1) {
            ret = ret.pop();
        }

        return ret;
    };
})(window.jQuery, window, document);