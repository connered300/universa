(function ($, window, document) {
    'use strict';

    var PessoaFisicaDuplicadas = function () {
        VersaShared.call(this);
        var __pessoaFisicaDuplicadas = this;
        this.defaults = {
            url: {
                search: '',
                urlMerge: '',
            },
        };

        this.setSteps = function () {
            $(".pessoa-merge").on("click", function(e){

                var mergePessoa = $(this).data("pes");
                __pessoaFisicaDuplicadas.addOverlay($("#container-merge"));

                var url = __pessoaFisicaDuplicadas.options.url.urlMerge;

                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    data: $("[name^='pessoas["+mergePessoa+"]']:checked").serializeArray(),
                    async: false,
                    success: function (data) {
                        if(data.error){
                            __pessoaFisicaDuplicadas.showNotificacaoDanger(
                                data.error || "Não foi possível mesclar o registro!"
                            )
                        } else if(data.success){
                            __pessoaFisicaDuplicadas.showNotificacaoSuccess(
                                data.success || "Registro mesclado com sucesso!"
                            )
                        }
                        location.href = url;
                    }
                });

                e.stopImmediatePropagation();
                e.preventDefault();
                return false;
            });
        };

        this.run = function (opts) {
            __pessoaFisicaDuplicadas.setDefaults(opts);
            __pessoaFisicaDuplicadas.setSteps();
        };
    };

    $.pessoaFisicaDuplicadas = function (params) {
        params = params || [];

        var obj = $(window).data("universa.pessoa.pessoa-fisica.duplicadas");

        if (!obj) {
            obj = new PessoaFisicaDuplicadas();
            obj.run(params);
            $(window).data('universa.pessoa.pessoa-fisica.duplicadas', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);