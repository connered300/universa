(function ($, window, document) {
    'use strict';

    var PessoaAdd = function () {
        VersaShared.call(this);
        var __pessoaAdd = this;
        this.defaults = {
            url: {
                pesquisa: '',
                buscaCEP: ''
            },
            data: {
                arrEstados: [],
                pessoaPesquisa: null,
                necessidadesEspeciais: [],
            },
            ajaxSubmit: false,
            desativaAutocompletePessoa: false,
            value: {
                pessoaNecessidades: null,
            },
            callback: {},
            datatables: {},
            wizardElement: '#pessoa-wizard',
            formElement: '#pessoa-form',
            validator: null
        };

        this.steps = {};

        this.steps.dadosPessoais = {
            init: function () {
                var $form = $(__pessoaAdd.options.formElement);

                $form.find("[name=pesCpf]").inputmask({
                    showMaskOnHover: false, clearIncomplete: true, mask: ['999.999.999-99']
                });

                $form.find("[name=pesTipo],[name=pesNacionalidade],[name=pesSexo],[name=pesEstadoCivil]").select2({
                    language: 'pt-BR'
                });

                $form.find("[name=pesTipo]").change(function () {
                    var pesTipo = this.value;

                    if (pesTipo == 'Fisica' || pesTipo == 'Equiparada') {
                        $form.find("#pessoa-dados-pf").removeClass('hidden');
                    } else {
                        $form.find("#pessoa-dados-pf").addClass('hidden');
                    }

                    if (pesTipo == 'Juridica' || pesTipo == 'Equiparada') {
                        $form.find("#pessoa-dados-pj").removeClass('hidden');
                    } else {
                        $form.find("#pessoa-dados-pj").addClass('hidden');
                    }
                }).trigger('change');

                $form.find("[name=pesNacionalidade]").change(function () {
                    var pesNacionalidade = this.value;

                    if (pesNacionalidade == 'Estrangeiro') {
                        $form.find(".campoPesCpf").addClass('hidden');
                        $form.find(".campoPesDocEstrangeiro").removeClass('hidden');
                    } else {
                        $form.find(".campoPesCpf").removeClass('hidden');
                        $form.find(".campoPesDocEstrangeiro").addClass('hidden');
                    }
                }).trigger('change');

                $form.find("[name=pesCnpj]").inputmask({
                    showMaskOnHover: false, clearIncomplete: true, mask: ['99.999.999/9999-99']
                });

                $form.find("[name=pesInscMunicipal],[name=pesInscEstadual]").inputmask({
                    showMaskOnHover: false, clearIncomplete: true, mask: ['9{8,14}']
                });


                $form.find("[name=pesDataNascimento], [name=pesRgEmissao]")
                    .datepicker({
                        prevText: '<i class="fa fa-chevron-left"></i>',
                        nextText: '<i class="fa fa-chevron-right"></i>'
                    })
                    .inputmask({
                        showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']
                    });
                $form.find("[name=pessoaNecessidades]").select2({
                    language: 'pt-BR',
                    multiple: true,
                    data: function () {
                        return {results: __pessoaAdd.options.data.necessidadesEspeciais};
                    }
                });
                $form.find("[name=pessoaNecessidades]").select2(
                    'data',
                    __pessoaAdd.options.value.pessoaNecessidades || []
                );

                if ($form.find("[name=pesId]").val() != "" || __pessoaAdd.options.desativaAutocompletePessoa) {
                    $form.find('.buscarDocumentoPessoa').parent().addClass('hide');
                } else {
                    $form.find("[name=pesNome]").blur(function () {
                        $(this).removeClass('ui-autocomplete-loading');
                    }).autocomplete({
                        source: function (request, response) {
                            var $this = $(this);
                            var $element = $(this.element);
                            var previous_request = $element.data("jqXHR");

                            if (previous_request) {
                                previous_request.abort();
                            }

                            $element.data("jqXHR", $.ajax({
                                url: __pessoaAdd.options.url.pesquisa,
                                data: {query: request.term, pesquisaPFPJ: true},
                                type: 'POST',
                                dataType: "json",
                                success: function (data) {
                                    var transformed = $.map(data || [], function (el) {
                                        el.label = el.pesNome;

                                        return el;
                                    });

                                    response(transformed);
                                }
                            }));
                        },
                        minLength: 2,
                        select: function (event, ui) {
                            var $element = $(this);
                            __pessoaAdd.options.data.pessoaPesquisa = ui.item;
                            __pessoaAdd.steps.dadosPessoais.selecaoPessoa($form);
                        }
                    });
                }

                $form.find(".buscarDocumentoPessoa").click(function () {
                    var arrPesquisa = {},
                        pesTipo = $form.find("[name=pesTipo]").val(),
                        pesNacionalidade = $form.find("[name=pesNacionalidade]").val(),
                        pesCpf = $form.find("[name=pesCpf]").val(),
                        pesCnpj = $form.find("[name=pesCnpj]").val(),
                        pesDocEstrangeiro = $form.find("[name=pesDocEstrangeiro]").val();

                    var pesCpfNums = pesCpf.replace(/[^0-9]/g, ''),
                        pesCnpjNums = pesCnpj.replace(/[^0-9]/g, '');

                    if (pesTipo == 'Fisica' || pesTipo == 'Equiparada') {
                        if (pesNacionalidade != 'Estrangeiro' && pesCpfNums.length == 11) {
                            arrPesquisa.pesCpf = pesCpf;
                        } else if (pesDocEstrangeiro.trim().length != 0) {
                            arrPesquisa.pesDocEstrangeiro = pesDocEstrangeiro;
                        }
                    }

                    if (pesTipo == 'Juridica' || pesTipo == 'Equiparada') {
                        if (pesCnpjNums.length == 14) {
                            arrPesquisa.pesCnpj = pesCnpj;
                        }
                    }


                    if (Object.keys(arrPesquisa).length == 0) {
                        __pessoaAdd.showNotificacaoWarning('Documento inválido!');

                        return;
                    }

                    __pessoaAdd.steps.dadosPessoais.pesquisarPesssoaPeloDocumento(arrPesquisa, $form);
                });
            },
            selecaoPessoa: function ($form) {
                var dados = __pessoaAdd.options.data.pessoaPesquisa || {};

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: '' +
                    'Deseja realmente carregar os dados no formulário?' +
                    '<br>Nome/Razão Social:<b> ' + dados['pesNome'] + '</b>' +
                    '<br>CPF/CNPJ:<b> ' + (dados['pesCpf'] || dados['pesCnpj']) + '</b>' +
                    '<br>Cidade/Estado:<b> ' + dados['endCidade'] + ' - ' + dados['endEstado'] + '</b>',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        var dados = __pessoaAdd.options.data.pessoaPesquisa || {};
                        __pessoaAdd.steps.dadosPessoais.buscarDadosPessoa(dados['pesId'], $form);
                    }

                    __pessoaAdd.options.data.pessoaPesquisa = null;
                });

            },
            pesquisarPesssoaPeloDocumento: function (criterioBusca, $form) {
                $form = $(__pessoaAdd.options.formElement);
                criterioBusca = criterioBusca || {};
                __pessoaAdd.addOverlay($form, 'Aguarde. Pesquisando pessoa pelo documento...');

                criterioBusca.pesquisaPFPJ = true;

                $.ajax({
                    url: __pessoaAdd.options.url.pesquisa,
                    dataType: 'json',
                    type: 'post',
                    data: criterioBusca,
                    success: function (data) {
                        __pessoaAdd.removeOverlay($form);

                        if (Object.keys(data).length == 0) {
                            __pessoaAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                        } else {
                            __pessoaAdd.options.data.pessoaPesquisa = data[0];
                            __pessoaAdd.steps.dadosPessoais.selecaoPessoa($form);
                        }
                    },
                    erro: function () {
                        __pessoaAdd.showNotificacaoDanger('Erro desconhecido!');
                        __pessoaAdd.removeOverlay($form);
                    }
                });
            },
            buscarDadosPessoa: function (pesId, $form) {
                $form = $form || $(__pessoaAdd.options.formElement);
                pesId = pesId || '';

                if (!$.isNumeric(pesId)) {
                    $form[0].reset();
                    $form.find('[name=pesId]').val('');
                    __pessoaAdd.validate();

                    return;
                }

                var $parent = $form.closest('.modal');

                if ($parent.length == 0) {
                    $parent = $form;
                }

                __pessoaAdd.addOverlay($parent, 'Aguarde. Buscando informações adicionais...');

                $.ajax({
                    url: __pessoaAdd.options.url.pesquisa,
                    dataType: 'json',
                    type: 'post',
                    data: {pesId: pesId, dadosCompletos: true, pesquisaPFPJ: true},
                    success: function (data) {
                        __pessoaAdd.removeOverlay($parent);

                        if (Object.keys(data).length == 0) {
                            __pessoaAdd.showNotificacaoInfo('Dados não encontrados!');
                        } else {
                            var dados = data;

                            $form[0].reset();
                            $form.deserialize(dados);

                            $form.find("[name=pesTipo],[name=pesNacionalidade],[name=pesSexo],[name=pesEstadoCivil]").trigger('change');

                            __pessoaAdd.options.value.pessoaNecessidades = dados['pessoaNecessidades'] || [];

                            $form.find("[name=pessoaNecessidades]")
                                .select2('data', __pessoaAdd.options.value.pessoaNecessidades);

                            __pessoaAdd.validate();
                        }
                    },
                    erro: function () {
                        __pessoaAdd.showNotificacaoDanger('Erro desconhecido!');
                        __pessoaAdd.removeOverlay($parent);
                    }
                });
            },
            validate: function () {
                var $form = $(__pessoaAdd.options.formElement);

                var requerDadosPF = function () {
                    var pesTipo = $form.find("[name=pesTipo]").val();
                    var pesFisica = (pesTipo == 'Fisica' || pesTipo == 'Equiparada');

                    return pesFisica;
                };

                var requerDadosPJ = function () {
                    var pesTipo = $form.find("[name=pesTipo]").val();
                    var pesFisica = (pesTipo == 'Juridica' || pesTipo == 'Equiparada');

                    return pesFisica;
                };

                __pessoaAdd.options.validator.settings.rules = {
                    pesTipo: {required: true},
                    pesNacionalidade: {required: true},
                    pesCpf: {
                        cpf: true,
                        required: function () {
                            var pesCpf = ($form.find("[name=pesNacionalidade]").val() != 'Estrangeiro');

                            return requerDadosPF() && pesCpf;
                        }
                    },
                    pesDocEstrangeiro: {
                        required: function () {
                            var pesCpf = ($form.find("[name=pesNacionalidade]").val() != 'Estrangeiro');

                            return requerDadosPF() && !pesCpf;
                        }
                    },
                    pesNome: {required: true, maxlength: 255},
                    pesNaturalidade: {required: requerDadosPF},
                    pesNascUf: {required: requerDadosPF},
                    pesDataNascimento: {required: requerDadosPF, dateBR: true},
                    pesSexo: {required: requerDadosPF},
                    pesRg: {required: requerDadosPF},
                    pesRgEmissao: {required: false, dateBR: true},
                    pesCnpj: {cnpj: true, required: requerDadosPJ},
                    pesNomeFantasia: {required: requerDadosPJ, maxlength: 255}
                };
                __pessoaAdd.options.validator.settings.messages = {
                    pesTipo: {required: 'Campo obrigatório!'},
                    pesNacionalidade: {required: 'Campo obrigatório!'},
                    pesCpf: {
                        required: 'Campo obrigatório!',
                        cpf: 'CPF inválido!'
                    },
                    pesDocEstrangeiro: {required: 'Campo obrigatório!'},
                    pesNome: {required: 'Campo obrigatório!', maxlength: 'Tamanho máximo: 255!'},
                    pesNaturalidade: {required: 'Campo obrigatório!'},
                    pesNascUf: {required: 'Campo obrigatório!'},
                    pesDataNascimento: {
                        required: 'Campo obrigatório!',
                        dateBR: 'Data inválida!'
                    },
                    pesSexo: {required: 'Campo obrigatório!'},
                    pesRg: {required: 'Campo obrigatório!'},
                    pesRgEmissao: {
                        required: 'Campo obrigatório!',
                        dateBR: 'Data inválida!'
                    },
                    pesCnpj: {required: 'Campo obrigatório!', cnpj: 'CNPJ inválido!'},
                    pesNomeFantasia: {required: 'Campo obrigatório!', maxlength: 'Tamanho máximo: 255!'}
                };

                return !$(__pessoaAdd.options.formElement).valid();
            }
        };

        this.steps.contatoEndereco = {
            init: function () {
                var $form = $(__pessoaAdd.options.formElement);

                $form.find("[name=endCep]").inputmask({
                    showMaskOnHover: false, clearIncomplete: true, mask: ['99999-999']
                });
                $form.find("[name=endNumero]").inputmask({showMaskOnHover: false, mask: ['9{0,9}']});
                $form.find("[name=conContatoTelefone], [name=conContatoCelular]").inputmask({
                    showMaskOnHover: false, clearIncomplete: true, mask: ['(99) 9999-9999', '(99) 99999-9999']
                });
                $form.find(".buscarCep").click(function () {
                    var $element = $(this);
                    var endCep = $form.find("[name=endCep]").val();
                    var endCepNums = endCep.replace(/[^0-9]/g, '');

                    if (endCepNums.length < 8) {
                        __pessoaAdd.showNotificacaoWarning('CEP inválido ou não encontrado!');

                        return;
                    }

                    $element.prop('disabled', true);

                    $.ajax({
                        url: __pessoaAdd.options.url.buscaCEP,
                        dataType: 'json',
                        type: 'post',
                        data: {cep: endCep},
                        success: function (data) {
                            $element.prop('disabled', false);

                            if (!data.dados) {
                                __pessoaAdd.showNotificacaoWarning('CEP inválido ou não encontrado!');
                            }

                            $form.find("[name=endLogradouro]").val(
                                ((data.dados.tip_logr_nome || '') + ' ' + (data.dados.logr_nome || '')).trim()
                            );
                            $form.find("[name=endCidade]").val(data.dados.cid_nome || '');
                            $form.find("[name=endEstado]").val(data.dados.est_uf).trigger('change');
                            $form.find("[name=endBairro]").val(data.dados.bai_nome || '');
                        },
                        erro: function () {
                            __pessoaAdd.showNotificacaoDanger('Falha ao buscar CEP!');
                            $element.prop('disabled', false);
                        }
                    });
                });
                $form.find("[name=endCidade]")
                    .blur(function () {
                        $(this).removeClass('ui-autocomplete-loading');
                    })
                    .autocomplete({
                        source: function (request, response) {
                            var $this = $(this);
                            var $element = $(this.element);
                            var previous_request = $element.data("jqXHR");

                            if (previous_request) {
                                previous_request.abort();
                            }

                            $element.data("jqXHR", $.ajax({
                                url: __pessoaAdd.options.url.buscaCEP,
                                data: {cidade: request.term},
                                type: 'POST',
                                dataType: "json",
                                success: function (data) {
                                    var transformed = $.map(data.dados || [], function (el) {
                                        el.label = el.cid_nome;

                                        return el;
                                    });

                                    response(transformed);
                                }
                            }));
                        },
                        minLength: 2,
                        focus: function (event, ui) {
                            var $element = $(this);
                            $element.val(ui.item.cid_nome);

                            return false;
                        },
                        select: function (event, ui) {
                            var $element = $(this);
                            $element.val(ui.item.cid_nome);
                            $form.find("[name=endEstado]").val(ui.item.est_uf).trigger('change');

                            return false;
                        }
                    });

                $('[name=pesNascUf], [name=endEstado]').select2({
                    language: 'pt-BR',
                    allowClear: true,
                    data: function () {
                        return {results: __pessoaAdd.options.data.arrEstados || []};
                    }
                });
            },
            validate: function () {
                var $form = $(__pessoaAdd.options.formElement);
                __pessoaAdd.options.validator.settings.rules = {
                    endCep: {required: false},
                    endLogradouro: {required: true},
                    endNumero: {
                        required: function () {
                            return $form.find('[name=endComplemento]').val() == '';
                        }, number: true
                    },
                    endComplemento: {
                        required: function () {
                            return $form.find('[name=endNumero]').val() == '';
                        }
                    },
                    endBairro: {required: false},
                    endCidade: {required: true},
                    endEstado: {required: true},
                    conContatoTelefone: {telefoneValido:true, required: false},
                    conContatoEmail: {required: true, email: true}
                };
                __pessoaAdd.options.validator.settings.messages = {
                    endCep: {required: 'Campo obrigatório!'},
                    endLogradouro: {required: 'Campo obrigatório!'},
                    endNumero: {required: 'Campo obrigatório!', number: 'Número inválido!'},
                    endComplemento: {required: 'Campo obrigatório!'},
                    endBairro: {required: 'Campo obrigatório!'},
                    endCidade: {required: 'Campo obrigatória!'},
                    endEstado: {required: 'Campo obrigatório!'},
                    conContatoTelefone: {required: 'Campo obrigatório!'},
                    conContatoEmail: {required: 'Campo obrigatório!', email: 'Email inválido!'},
                };

                return !$(__pessoaAdd.options.formElement).valid();
            }
        };


        this.salvar = function (form) {
            var $form = $(__pessoaAdd.options.formElement);

            if ($form.data('noSubmit') == undefined) {
                __pessoaAdd.validate();
            }

            if ($form.data('noSubmit')) {
                return false;
            }

            if (__pessoaAdd.options.ajaxSubmit) {
                var $parent = $form.closest('.modal');

                if ($parent.length == 0) {
                    $parent = $form;
                }

                __pessoaAdd.addOverlay($parent, 'Salvando registro. Aguarde...');

                var dados = $form.serializeJSON();
                dados['ajax'] = true;

                $.ajax({
                    url: $form.attr('action'),
                    type: 'POST',
                    dataType: 'json',
                    data: dados,
                    success: function (data) {
                        var retorno = data['arrDados'] || dados;

                        if (data.erro) {
                            __pessoaAdd.showNotificacaoDanger(
                                data['mensagem'] || 'Não foi possível salvar registro.'
                            );
                        } else {
                            var aposSalvarFN = __pessoaAdd
                                .getFunction(__pessoaAdd.options.callback, 'aposSalvar');

                            __pessoaAdd.showNotificacaoSuccess(
                                data['mensagem'] || 'Registro salvo com sucesso!'
                            );

                            if (aposSalvarFN) {
                                aposSalvarFN(retorno, data);
                            }
                        }
                        __pessoaAdd.removeOverlay($parent);
                    }
                });
            } else {
                form.submit();
            }
        };

        this.submitHandler = this.salvar;

        this.buscarDadosPessoa = function (pesId) {
            __pessoaAdd.steps.dadosPessoais.buscarDadosPessoa(pesId);
        };

        this.setCallback = function (callback, func) {
            __pessoaAdd.options.callback[callback] = func;
        };

        this.setValidations = function () {
            $(document).on("change", __pessoaAdd.options.formElement + " .form-control:hidden", function () {
                $(__pessoaAdd.options.formElement).valid();
            });

            $(__pessoaAdd.options.formElement).submit(function (e) {
                var ok = __pessoaAdd.validate();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }
            });
        };

        this.run = function (opts) {
            __pessoaAdd.setDefaults(opts);
            __pessoaAdd.loadSteps();
            __pessoaAdd.setValidations();
            __pessoaAdd.wizard();
        }
    };

    $.pessoaAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.pessoa.pessoa.add");

        if (!obj) {
            obj = new PessoaAdd();
            obj.run(params);
            $(window).data('universa.pessoa.pessoa.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);