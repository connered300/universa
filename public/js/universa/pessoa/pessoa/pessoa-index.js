(function ($, window, document) {
    'use strict';
    var PessoaIndex = function () {
        VersaShared.call(this);
        var __pessoaIndex = this;
        this.defaults = {
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                pessoa: null
            },
        };

        this.setSteps = function () {
            var colNum = 0;
            __pessoaIndex.options.datatables.pessoa = $('#dataTablePessoa').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __pessoaIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary pessoa-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger pessoa-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                        }

                        __pessoaIndex.removeOverlay($('.widget-body'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "pes_id", targets: colNum++, data: "pes_id"},
                    {name: "pes_nome", targets: colNum++, data: "pes_nome"},
                    {name: "pes_tipo", targets: colNum++, data: "pes_tipo"},
                    {name: "pes_nacionalidade", targets: colNum++, data: "pes_nacionalidade"},
                    {name: "pes_id", targets: colNum++, data: "acao"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'desc']]
            });

            $('#dataTablePessoa').on('click', '.pessoa-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __pessoaIndex.options.datatables.pessoa.fnGetData($pai);

                location.href = __pessoaIndex.options.url.edit + '/' + data['pes_id'];
            });

            $('#dataTablePessoa').on('click', '.pessoa-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __pessoaIndex.options.datatables.pessoa.fnGetData($pai);
                var arrRemover = {
                    pesId: data['pes_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de pessoa "' + data['pes_nome'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        __pessoaIndex.addOverlay($('.widget-body'),
                                                 'Aguarde, solicitando remoção de registro de pessoa...');
                        $.ajax({
                            url: __pessoaIndex.options.url.remove,
                            type: 'POST',
                            dataType: 'json',
                            data: arrRemover,
                            success: function (data) {
                                if (data.erro) {
                                    __pessoaIndex.showNotificacaoDanger(
                                        "Não foi possível remover registro de pessoa:\n" +
                                        "<i>" + data['erroDescricao'] + "</i>"
                                    );
                                    __pessoaIndex.removeOverlay($('.widget-body'));
                                } else {
                                    __pessoaIndex.options.datatables.pessoa.api().ajax.reload(null, false);
                                    __pessoaIndex.showNotificacaoSuccess(
                                        "Registro de pessoa removido!"
                                    );
                                }
                            }
                        });
                    }
                });

                e.preventDefault();
            });
        };

        this.run = function (opts) {
            __pessoaIndex.setDefaults(opts);
            __pessoaIndex.setSteps();
        };
    };

    $.pessoaIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.pessoa.pessoa.index");

        if (!obj) {
            obj = new PessoaIndex();
            obj.run(params);
            $(window).data('universa.pessoa.pessoa.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);