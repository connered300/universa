(function ($, window, document) {
    'use strict';

    var DocentesIndex = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlSearch: '',
                urlPainel: ''
            }
        };

        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);
        };

        priv.setSteps = function () {
            var colNum = 0;
            priv.dataTableDocentes = $('#docentsTable').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: priv.options.urlSearch,
                    type: "POST"
                },
                columnDefs: [
                    {name: "docente_descricao", targets: colNum++, data: "docente_descricao"},
                    {name: "qtd_disc", targets: colNum++, data: "qtd_disc"},
                    {name: "qtd_alunos", targets: colNum++, data: "qtd_alunos"},
                    {name: "media_notas", targets: colNum++, data: "media_notas"},
                    {name: "qtd_anotacoes", targets: colNum++, data: "qtd_anotacoes"},
                    {name: "faltas_lancadas", targets: colNum++, data: "faltas_lancadas"}
                ],
                "dom": "<'dt-toolbar'<'col-xs-6'f><'col-xs-6'T>r>" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-xs-6'i><'col-xs-6'p>>",
                "oTableTools": {
                    "aButtons": []
                },
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $(document).on("click", "#docentsTable tbody tr", function () {
                var position = priv.dataTableDocentes.fnGetPosition(this);
                var data = priv.dataTableDocentes.fnGetData(position);

                location.href = priv.options.urlPainel + '/' + data['docente_id'];
            });
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
        };
    };

    $.docentesIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.docentes.index");

        if (!obj) {
            obj = new DocentesIndex();
            obj.run(params);
            $(window).data('universa.docentes.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);