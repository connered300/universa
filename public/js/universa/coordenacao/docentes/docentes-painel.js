(function ($, window, document) {
    'use strict';

    var DocentesPainel = function () {
        VersaShared.call(this);
        var pub = this;
        var priv = {
            options: {
                url: {
                    docenciaInformacao: '',
                    docenciaExtensao: '',
                    docenciaExtensaoGravar: '',
                    docenciaNotasAlunosEtapas: '',
                    docenciaAnotacoes: '',
                    docenciaAnotacaoAnexos: '',
                    arquivoDownload: ''
                },
                datatable: {
                    docencias: null,
                    anotacoes: null,
                    notas: null
                },
                data: {
                    docencia: [],
                    docencias: [],
                    extensaoEtapa: []
                },
                value: {
                    cursoConfigMetodo: ''
                },
                acoesPendentes: 0
            }
        };

        priv.acoesPendentes = function (v) {
            priv.options.acoesPendentes += v;
            var msg = '';

            if (priv.options.acoesPendentes > 0) {
                if (priv.options.acoesPendentes == 1) {
                    msg = 'requisição pendente.';
                } else {
                    msg = 'requisições pendentes.';
                }

                msg = 'Aguarde... ' + priv.options.acoesPendentes + ' ' + msg;
            }

            $('#acoes-pendentes').html(msg);
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];

            priv.options = $.extend(priv.options, opts);

            $.validator.setDefaults({
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    }
                    else if (element.parent('label').length) {
                        error.insertAfter(element.parent());
                    }
                    else {
                        error.insertAfter(element);
                    }
                }
            });
        };

        priv.showNotificacao = function (param) {
            var options = $.extend(
                true,
                {content: '', title: '', type: 'info', icon: '', color: '', timeout: false},
                param
            );

            switch (options.type) {
                case 'danger':
                    options.icon = 'fa-times-circle';
                    options.title = options.title || 'Atenção:';
                    options.color = '#a90329';
                    break;
                case 'info':
                    options.icon = 'fa-info-circle';
                    options.title = options.title || 'Informação:';
                    options.color = '#57889c';
                    break;
                case 'success':
                    options.icon = 'fa-check-circle';
                    options.title = options.title || 'Sucesso:';
                    options.color = '#739e73';
                    break;
                case 'warning':
                    options.icon = 'fa-exclamation-circle';
                    options.title = options.title || 'Atenção:';
                    options.color = '#c79121';
                    break;
            }

            var optionsNotification = {
                title: options.title,
                content: "<i>" + options.content + "</i>",
                color: options.color,
                icon: "fa " + options.icon
            };

            if (options.timeout) {
                optionsNotification.timeout = options.timeout;
            }

            $.smallBox(optionsNotification);
        };

        priv.addOverlay = function (el, mensagem) {
            mensagem = mensagem || 'Aguarde...';
            el.append('<div class="loader-overlay">' + mensagem + '</div>');
        };

        priv.removeOverlay = function (el) {
            el.find('.loader-overlay').remove();
        };

        priv.carregaInformacoesDocencia = function (data) {
            data = data || {};
            var docdiscId = data['docdisc_id'] || '';
            $('#docenteAcoes')[docdiscId ? 'removeClass' : 'addClass']('hidden');
            $('#docenteAcoesAviso')[!docdiscId ? 'removeClass' : 'addClass']('hidden');
            $('#docenteAcoes').trigger('disciplina-alterada');
        };

        priv.iniciaListagemDocencia = function () {
            var colNum = 0;
            priv.options.datatable.docencias = $('#docenciasDataTable').dataTable({
                processing: true,
                data: priv.options.data.docencias,
                columns: [
                    {name: "turma_nome", targets: colNum++, data: "turma_nome"},
                    {name: "disc_nome", targets: colNum++, data: "disc_nome"}
                ],
                "dom": "r" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-12'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'desc']]
            });

            $(document).on("click", "#docenciasDataTable tbody tr", function () {
                var position = priv.options.datatable.docencias.fnGetPosition(this);
                var data = priv.options.datatable.docencias.fnGetData(position);

                if (priv.options.acoesPendentes == 0) {
                    priv.options.data.docencia = data || [];
                    $('#docenciasDataTable tr.tr-active').removeClass('tr-active');
                    $(this).addClass('tr-active');

                    priv.carregaInformacoesDocencia(data);
                }
            });

            priv.carregaInformacoesDocencia();
        };

        priv.carregaAbaInformacoes = function () {
            var turmaNome = priv.options.data.docencia['turma_nome'] || '-';
            var disciplinaNome = priv.options.data.docencia['disc_nome'] || '-';
            var docdiscId = priv.options.data.docencia['docdisc_id'] || '';

            $('#turmaNome').html(turmaNome);
            $('#disciplinaNome').html(disciplinaNome);

            if (docdiscId) {
                priv.acoesPendentes(1);
                priv.addOverlay($('#tab-geral'), 'Aguarde, carregando informações gerais da docência.');

                $.ajax({
                    url: priv.options.url.docenciaInformacao,
                    data: {docdiscId: docdiscId},
                    type: 'POST',
                    dataType: "json",
                    success: function (data) {
                        priv.acoesPendentes(-1);
                        priv.removeOverlay($('#tab-geral'));
                        var qtdAlunos = data['qtd_alunos'] || '0';
                        var qtdAnotacoes = data['qtd_anotacoes'] || '0';
                        var qtdFaltas = data['qtd_faltas'] || '0';
                        var qtdNotas = data['qtd_notas'] || '0';
                        var mediaNotas = data['media_notas'] || '0';

                        $('#numeroDeAlunos').html(qtdAlunos);
                        $('#numeroDeNotasLancadas').html(qtdNotas);
                        $('#numeroDeAnotacoesLancadas').html(qtdAnotacoes);
                        $('#numeroDeFaltasLancadas').html(qtdFaltas);
                        $('#mediaDeNotasLancadas').html(mediaNotas);
                    },
                    error: function () {
                        priv.acoesPendentes(-1);
                    }
                });
            }
        };

        priv.iniciaAbaExtensao = function () {
            $("#extensaoData").datepicker({
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>'
            }).inputmask({
                showMaskOnHover: false,
                clearIncomplete: true,
                mask: ['99/99/9999']
            });

            $('#optionEtapa').select2({
                allowClear: false,
                language: 'pt-BR',
                data: function () {
                    var arrEtapas = $.map(priv.options.data.extensaoEtapa || [], function (extensaoEtapa) {
                        extensaoEtapa.id = extensaoEtapa.etapaId;
                        extensaoEtapa.text = extensaoEtapa.etapaDescricao;

                        return extensaoEtapa;
                    });

                    return {results: arrEtapas};
                }
            });

            $('#optionEtapa').on('change', function () {
                var arrEtapa = $('#optionEtapa').select2('data') || [];
                var optionEtapa = '';
                var dataFinalEtapa = '';
                var extensaoData = '';
                var extensaoJustificativa = '';
                var extensaoObservacao = '';
                var extensaoId = '';

                if (arrEtapa['etapaExtensao']) {
                    optionEtapa = arrEtapa['etapaExtensao']['optionEtapa'] || '';
                    dataFinalEtapa =
                        arrEtapa['etapaExtensao']['dataFinalEtapa'] || arrEtapa['etapaDiarioDataFinalDefault'] || '';
                    extensaoData = arrEtapa['etapaExtensao']['extensaoDataFinal'] || '';
                    extensaoJustificativa = arrEtapa['etapaExtensao']['extensaoJustificativa'] || '';
                    extensaoObservacao = arrEtapa['etapaExtensao']['extensaoObservacao'] || '';
                    extensaoId = arrEtapa['etapaExtensao']['extensaoId'] || '';
                } else {
                    dataFinalEtapa = arrEtapa['etapaDiarioDataFinalDefault'] || '';
                }

                var diario = arrEtapa['diario'] || '';
                var etapa = arrEtapa['etapaId'] || '';
                var docdisc = arrEtapa['docdisc'] || '';

                $('#optionEtapa').val(optionEtapa);
                $('#dataFinalEtapa').val(dataFinalEtapa);
                $('#extensaoData').val(extensaoData);
                $('#extensaoJustificativa').val(extensaoJustificativa);
                $('#extensaoObservacao').val(extensaoObservacao);
                $('#diario').val(diario);
                $('#extensaoId').val(extensaoId);
                $('#etapa').val(etapa);
                $('#docdisc').val(docdisc);
            });

            $('#extensaoData').on("change", function () {
                $('#extensaoObservacao').val('');
            });

            $('#salvarEtapa').on("click", function () {
                priv.acoesPendentes(1);
                priv.addOverlay($('#tab-extensao'), 'Salvando extensão de docência.');

                var arrData = {
                    diarioId: $("#diario").val(),
                    etapaId: $("#etapa").val(),
                    docdisc: $("#docdisc").val(),
                    extensaoId: $("#extensaoId").val(),
                    dataFinalEtapa: $("#dataFinalEtapa").val(),
                    extensaoData: $("#extensaoData").val(),
                    extensaoObservacao: $("#extensaoObservacao").val()
                };

                $.ajax({
                    url: priv.options.url.docenciaExtensaoGravar,
                    data: arrData,
                    type: 'POST',
                    success: function (data) {
                        priv.acoesPendentes(-1);
                        var result = data.result;

                        priv.removeOverlay($('#tab-extensao'));

                        if (result.type == 'success') {
                            var etapaId = $("#etapa").val();
                            priv.options.data.extensaoEtapa = result['etapas'] || {};
                            $('#optionEtapa').select2('val', etapaId).trigger('change');

                            priv.showNotificacao({
                                type: 'success',
                                content: "Alterações Salvas!",
                                timeout: 10000
                            });
                        } else {
                            priv.showNotificacao({
                                type: 'warning',
                                content: result.message.join('<br>'),
                                timeout: 10000
                            });
                        }
                    },
                    error: function () {
                        priv.acoesPendentes(-1);
                    }
                });
            });
        };

        priv.carregaAbaExtensao = function () {
            var docdiscId = priv.options.data.docencia['docdisc_id'] || '';

            if (docdiscId) {
                priv.acoesPendentes(1);
                priv.addOverlay($('#tab-extensao'), 'Aguarde, carregando informações sobre datas limites de entrega.');

                $.ajax({
                    url: priv.options.url.docenciaExtensao,
                    data: {docdiscId: docdiscId},
                    type: 'POST',
                    dataType: "json",
                    success: function (data) {
                        priv.acoesPendentes(-1);
                        priv.removeOverlay($('#tab-extensao'));
                        priv.options.data.extensaoEtapa = data || {};
                        var primeiraEtapa = Object.keys(priv.options.data.extensaoEtapa)[0] || 0;
                        $('#optionEtapa').select2('val', primeiraEtapa).trigger('change');
                    },
                    error: function () {
                        priv.acoesPendentes(-1);
                    }
                });
            }
        };

        priv.iniciaAbaAnotacoes = function () {
            var colNum = 0;
            priv.options.datatable.anotacoes = $('#anotacoesDataTable').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: priv.options.url.docenciaAnotacoes,
                    type: "POST",
                    data: function (d) {
                        d.filter = d.filter || {};
                        var data = priv.options.data.docencia || [];
                        d.filter['docdiscId'] = data['docdisc_id'] || '-';
                        priv.acoesPendentes(1);

                        return d;
                    },
                    error: function () {
                        priv.acoesPendentes(-1);
                    },
                    dataSrc: function (json) {
                        var data = json.data;
                        priv.acoesPendentes(-1);

                        for (var row in data) {
                            var anot_data = $.formatDateTime('dd/mm/yy', new Date(data[row]['anot_data']));
                            data[row]['anot_data_formatada'] = anot_data;
                        }

                        return data;
                    }
                },
                columnDefs: [
                    {name: "anot_data", targets: colNum++, data: "anot_data_formatada", type: "date-eu"},
                    {name: "anot_titulo", targets: colNum++, data: "anot_titulo"},
                    {name: "anot_anexo_qtd", targets: colNum++, data: "anot_anexo_qtd"},
                    {name: "anot_id", targets: colNum++, data: "anot_id", visible: false},
                    {name: "docdisc_id", targets: colNum++, data: "docdisc_id", visible: false},
                    {name: "anot_descricao", targets: colNum++, data: "anot_descricao", visible: false}
                ],
                "dom": "rf" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'desc']]
            });

            $(document).on("click", "#anotacoesDataTable tbody tr", function () {
                var position = priv.options.datatable.anotacoes.fnGetPosition(this);
                var data = priv.options.datatable.anotacoes.fnGetData(position);

                priv.showModalAnotacao(data);
            });
        };

        priv.iniciaAbaNotas = function () {
            var colNum = 0;
            priv.options.datatable.notas = $('#notasDataTable').dataTable({
                processing: true,
                ajax: {
                    url: priv.options.url.docenciaNotasAlunosEtapas,
                    type: "POST",
                    data: function (d) {
                        var data = priv.options.data.docencia || [];
                        d.docdiscId = data['docdisc_id'] || '-';
                        priv.acoesPendentes(1);

                        return d;
                    },
                    error: function () {
                        priv.acoesPendentes(-1);
                    },
                    dataSrc: function (json) {
                        var data = json.data;
                        priv.acoesPendentes(-1);

                        var totalEtapa                   = {'nota_etapa_01': 0, 'nota_etapa_02': 0, 'nota_etapa_03': 0},
                            $constCursoConfigMetodoMedia = priv.options.value.cursoConfigMetodo;

                        for (var row in data) {
                            data[row]['nota_total'] =  parseFloat(data[row]['nota_etapa_01'] || 0);
                            data[row]['nota_total'] += parseFloat(data[row]['nota_etapa_02'] || 0);
                            data[row]['nota_total'] += parseFloat(data[row]['nota_etapa_03'] || 0);

                            if (data[row]['metodoCalculoNotas'] == $constCursoConfigMetodoMedia) {
                                data[row]['nota_total'] = (data[row]['nota_total'] / data[row]['etapasPeriodo']);
                                data[row]['nota_total'] = pub.formatNumberUS(data[row]['nota_total'], 1);
                            }


                            totalEtapa['nota_etapa_01'] += parseFloat(data[row]['nota_etapa_01'] || 0);
                            totalEtapa['nota_etapa_02'] += parseFloat(data[row]['nota_etapa_02'] || 0);
                            totalEtapa['nota_etapa_03'] += parseFloat(data[row]['nota_etapa_03'] || 0);
                        }

                        var mediaNotasEtapa01 = Math.round(totalEtapa['nota_etapa_01'] / data.length);
                        var mediaNotasEtapa02 = Math.round(totalEtapa['nota_etapa_02'] / data.length);
                        var mediaNotasEtapa03 = Math.round(totalEtapa['nota_etapa_03'] / data.length);

                        $('#mediaNotasEtapa01').html(mediaNotasEtapa01);
                        $('#mediaNotasEtapa02').html(mediaNotasEtapa02);
                        $('#mediaNotasEtapa03').html(mediaNotasEtapa03);
                        $('#mediaNotasGeral').html(mediaNotasEtapa01 + mediaNotasEtapa02 + mediaNotasEtapa03);

                        return data;
                    }
                },
                columnDefs: [
                    {name: "aluno_nome", targets: colNum++, data: "aluno_nome"},
                    {name: "nota_etapa_01", targets: colNum++, data: "nota_etapa_01"},
                    {name: "nota_etapa_02", targets: colNum++, data: "nota_etapa_02"},
                    {name: "nota_etapa_03", targets: colNum++, data: "nota_etapa_03"},
                    {name: "aluno_id", targets: colNum++, data: "nota_total"}
                ],
                "dom": "rf" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });
        };

        priv.showModalAnotacao = function (data) {
            data = data || {};

            $('#anotacaoData').html(data['anot_data_formatada'] || '');
            $('#anotacaoAnexoQtd').html(data['anot_anexo_qtd'] || '');
            $('#anotacaoTitulo').html(data['anot_titulo'] || '');
            $('#anotacaoDescricao').html(data['anot_descricao'] || '');
            $('#anotacao-informacao').modal();

            if (data['anot_anexo_qtd'] == 0) {
                $('#anotacaoAnexos').html('<div class="list-group-item list-group-item-info">Sem anexos cadastrados.</div>');
            } else {
                $('#anotacaoAnexos').html('<div>&nbsp;</div>');
                priv.acoesPendentes(1);
                priv.addOverlay($('#anotacaoAnexos'), 'Aguarde, buscando anexos da anotação.');

                $.ajax({
                    url: priv.options.url.docenciaAnotacaoAnexos,
                    data: {anotId: data['anot_id']},
                    type: 'POST',
                    dataType: "json",
                    success: function (data) {
                        priv.acoesPendentes(-1);
                        priv.removeOverlay($('#anotacaoAnexos'));
                        $('#anotacaoAnexos').html('');
                        var anexos = data['anexos'] || [];

                        $(anexos).each(function (i, item) {
                            var url = priv.options.url.arquivoDownload + '/';
                            url += item['arqChave'];

                            $('#anotacaoAnexos').append(
                                '<a href="' + url + '" class="list-group-item list-group-item-info">' +
                                '<i class="fa pull-right fa-arrow-down"></i>' +
                                item['arqNome'] +
                                '</a>'
                            );
                        });
                    },
                    error: function () {
                        priv.acoesPendentes(-1);
                    }
                });
            }
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.iniciaListagemDocencia();
            priv.iniciaAbaExtensao();
            priv.iniciaAbaAnotacoes();
            priv.iniciaAbaNotas();

            $('#docenteAcoes').on('disciplina-alterada', function () {
                priv.carregaAbaInformacoes();
                priv.carregaAbaExtensao();
                priv.options.datatable.anotacoes.api().ajax.reload(null, false);
                priv.options.datatable.notas.api().ajax.reload(null, false);
            });
        };
    };

    $.docentesPainel = function (params) {
        params = params || [];

        var obj = $(window).data("universa.docentesPainel");

        if (!obj) {
            obj = new DocentesPainel();
            obj.run(params);
            $(window).data('universa.docentesPainel', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);