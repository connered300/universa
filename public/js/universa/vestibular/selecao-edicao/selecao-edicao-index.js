/**
 * Created by eduardo on 12/21/16.
 */

$(document).ready(function(){
    listagemDataTable();
    encerra();
});

function listagemDataTable(){
    $('#table-vestibular').DataTable({
        processing: true,
        serverSide: true,
        searching: true,
        ajax: {
            url: window.vestibular,
            type: "POST",
            dataSrc: function (json) {
                var data = json.data;
                var i = 1;

                for (var row in data) {
                    var urlEdit = '/vestibular/selecao-edicao/edit/' + data[row]['id'];
                    data[row]['descricao']         = "<a href='"+urlEdit+"'>"+data[row]['descricao']+"</a>";
                    data[row]['inicio']         = "<a href='"+urlEdit+"'>"+data[row]['inicio']+"</a>";
                    data[row]['termino']         = "<a href='"+urlEdit+"'>"+data[row]['termino']+"</a>";
                    data[row]['divulgacao']         = "<a href='"+urlEdit+"'>"+data[row]['divulgacao']+"</a>";
                    data[row]['aceito']         = "<a href='"+urlEdit+"'>"+data[row]['aceito']+"</a>";
                    data[row]['solicitado']         = "<a href='"+urlEdit+"'>"+data[row]['solicitado']+"</a>";
                    data[row]['total']         = "<a href='"+urlEdit+"'>"+data[row]['total']+"</a>";
                    data[row]['ultimo']         = "<a href='"+urlEdit+"'>"+data[row]['ultimo']+"</a>";

                    var prop = data[i] == undefined ? 'enabled' : 'disabled';

                    data[row]['id']         = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                    <button type="submit" class="btn btn-xs btn-danger encerrar" id="'+data[row]['id']+'" '+prop+'>\
                                        Encerrar\
                                    </button>\
                            </div>\
                        </div>';
                    i++;
                }

                return data;
            }
        },
        columnDefs: [
            {name: "descricao", targets: 0, data: "descricao"},
            {name: "inicio", targets: 1, data: "inicio"},
            {name: "termino", targets: 2, data: "termino"},
            {name: "divulgacao", targets: 3, data: "divulgacao"},
            {name: "aceito", targets: 4, data: "aceito"},
            {name: "solicitado", targets: 5, data: "solicitado"},
            {name: "caca", targets: 6, data: "total"},
            {name: "ultimo", targets: 7, data: "ultimo"},
            {name: "id", targets: 8, data: "id"}
        ],
        "dom": "<'dt-toolbar'<'col-xs-6'f><'col-xs-6'T>r>" +
               "t" +
               "<'dt-toolbar-footer'<'col-xs-6'i><'col-xs-6'p>>",
        "oTableTools": {
            "aButtons": []
        },
        "autoWidth": true,
        oLanguage: {
            sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
            sLengthMenu: "Mostrar _MENU_ registros por página",
            sZeroRecords: "Nenhum registro encontrado",
            sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
            sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
            sInfoFiltered: "(filtrado de _MAX_ registros)",
            sSearch: "Procurar: ",
            oPaginate: {
                sFirst: "Início",
                sPrevious: "Anterior",
                sNext: "Próximo",
                sLast: "Último"
            }
        },
    });
}

function encerra(){
    var edicao = '';
    $(document).on('click', '.encerrar', function(){
        edicao = this.id;
        $('#modalEncerramento').modal('show');
    });
    $('#salvar').click(function(){
        encerramento(edicao);
    })
}

function encerramento(edicao){
    $.ajax({
        url: window.edit,
        type: 'POST',
        dataType: 'json',
        data: {
            edicao: edicao,
            classificados: $("#qtd").val()
        },
        success: function (data) {
            console.log(data);
            if (data.result['type'] == 'error') {
                showNotificacao({
                    type: 'danger',
                    content: data.result['message']
                });

                $('#modalEncerramento').modal('hide');
            } else {
                showNotificacao({
                    type: 'success',
                    content: data.result['message']
                });

                $('#modalEncerramento').modal('hide');
            }
        }
    });
}

function showNotificacao (param) {
    var options = $.extend(
        true,
        {content: '', title: '', type: 'info', icon: '', color: '', timeout: false},
        param
    );

    switch (options.type) {
        case 'danger':
            options.icon = 'fa-times-circle';
            options.title = options.title || 'Atenção:';
            options.color = '#a90329';
            break;
        case 'info':
            options.icon = 'fa-info-circle';
            options.title = options.title || 'Informação:';
            options.color = '#57889c';
            break;
        case 'success':
            options.icon = 'fa-check-circle';
            options.title = options.title || 'Sucesso:';
            options.color = '#739e73';
            break;
        case 'warning':
            options.icon = 'fa-exclamation-circle';
            options.title = options.title || 'Atenção:';
            options.color = '#c79121';
            break;
    }

    var optionsNotification = {
        title: options.title,
        content: "<i>" + options.content + "</i>",
        color: options.color,
        icon: "fa " + options.icon
    };

    if (options.timeout) {
        optionsNotification.timeout = options.timeout;
    }

    $.smallBox(optionsNotification);
}