(function ($, window, document) {
    'use strict';

    var InscricaoCursosAdd;
    InscricaoCursosAdd = function () {
        VersaShared.call(this);
        var __inscricaoCursosAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                inscricaoResultado: ''
            },
            data: {
                arrInscricaoResultado:null

            },
            value: {},
            datatables: {},
            wizardElement: '#inscricao-cursos-wizard',
            formElement: '#inscricao-cursos-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#inscricaoResultado").select2({
                language: 'pt-BR',
                placeholder: 'Selecione o resutado',
                allowClear: true,
                data: __inscricaoCursosAdd.options.data.arrInscricaoResultado
            });

            $('#inscricaoNota').inputmask({
                showMaskOnHover: false,
                alias: 'currency',
                groupSeparator: ".",
                radixPoint: ",",
                placeholder: "0",
                prefix: ""
            });

        };

        this.setValidations = function () {
            $(__inscricaoCursosAdd.options.formElement).submit(function (e) {
                var ok = $(__inscricaoCursosAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__inscricaoCursosAdd.options.formElement);

                if (__inscricaoCursosAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __inscricaoCursosAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __inscricaoCursosAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__inscricaoCursosAdd.options.listagem) {
                                    $.inscricaoCursosIndex().reloadDataTableInscricaoCursos();
                                    __inscricaoCursosAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#inscricao-cursos-modal').modal('hide');
                                }

                                __inscricaoCursosAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaInscricaoCursos = function (incricao_cursos_id, callback) {
            var $form = $(__inscricaoCursosAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __inscricaoCursosAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + incricao_cursos_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    __inscricaoCursosAdd.removeOverlay($form);

                    if (Object.keys(data).length ===0) {
                        __inscricaoCursosAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }
                },
                erro: function () {
                    __inscricaoCursosAdd.showNotificacaoDanger('Erro desconhecido!');
                    __inscricaoCursosAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.inscricaoCursosAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.vestibular.inscricao-cursos.add");

        if (!obj) {
            obj = new InscricaoCursosAdd();
            obj.run(params);
            $(window).data('universa.vestibular.inscricao-cursos.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);