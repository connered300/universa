(function ($, window, document) {
    'use strict';
    var InscricaoCursosIndex;
    InscricaoCursosIndex = function () {
        VersaShared.call(this);
        var __inscricaoCursosIndex = this;

        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: ''
            },
            datatables: {
                inscricaoCursos: null
            }
        };

        this.setSteps = function () {
            var colNum = 0;
            __inscricaoCursosIndex.options.datatables.inscricaoCursos =
                $('#inscricao-cursos').dataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: __inscricaoCursosIndex.options.url.search,
                        type: "POST",
                        dataSrc: function (json) {
                            var data = json.data;
                            var btnGroup = '\
                            <div class="text-center">\
                                <div class="btn-group">\
                                    <button type="button" class="btn btn-xs btn-primary inscricao-cursos-edit">\
                                        <i class="fa fa-pencil fa-inverse"></i>\
                                    </button>\
                                </div>\
                            </div>';

                            for (var row in data) {
                                data[row]['acao'] = btnGroup;
                            }

                            __inscricaoCursosIndex.removeOverlay($('#container-inscricao-cursos'));

                            return data;
                        }
                    },
                    columnDefs: [
                        {name: "inscricao_id", targets: colNum++, data: "inscricao_id"},
                        {name: "pes_nome", targets: colNum++, data: "pes_nome"},
                        {name: "edicao_descricao", targets: colNum++, data: "edicao_descricao"},
                        {name: "edicao_agendado", targets: colNum++, data: "edicao_agendado"},
                        {name: "curso_nome", targets: colNum++, data: "curso_nome"},
                        {name: "inscricao_opcao", targets: colNum++, data: "inscricao_opcao"},
                        {name: "inscricao_resultado", targets: colNum++, data: "inscricao_resultado"},
                        {name: "inscricao_nota", targets: colNum++, data: "inscricao_nota"},
                        {name: "inscricao_id", targets: colNum++, data: "acao"}
                    ],
                    "dom": "fr" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                    "autoWidth": true,
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "Procurar: ",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    },
                    order: [[0, 'asc']]
                });

        };
        $('#inscricao-cursos').on('click', '.inscricao-cursos-edit', function (event) {
            var $pai = $(this).closest('tr');
            var data = __inscricaoCursosIndex.options.datatables.inscricaoCursos.fnGetData($pai);

            __inscricaoCursosIndex.addOverlay($('.inscricao-curso-view'),
                'Carregando informações...'
            );

            if (!__inscricaoCursosIndex.options.ajax) {
                location.href = __inscricaoCursosIndex.options.url.edit + '/' + data['inscricao_cursos_id'];
            } else {
                $.inscricaoCursosAdd().pesquisaInscricaoCursos(
                    data['incricao_cursos_id'],
                    function (data) {
                        __inscricaoCursosIndex.showModal(data);
                    }
                );
            }
        });

        this.showModal = function (data) {
            data = data || {};
            var $form = $('#inscricao-cursos-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionForm[actionForm.length - 1] = 'add';
                actionForm.push(data['incricaoCursosId']);
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();

            try{
            $form.deserialize(data);
            }
            catch (e){}

            $form.find('.inscricaoId').html(data['inscricaoId'] ? data['inscricaoId'] : '-');
            $form.find('.edicaoDescricao').html(data['edicaoDescricao'] ? data['edicaoDescricao'] : '-');
            $form.find('.inscricaoDataHorarioProva').html(data['inscricaoDataHorarioProva'] ? data['inscricaoDataHorarioProva'] : '-');
            $form.find('.inscricaoDataHorarioProvaUS').html(data['inscricaoDataHorarioProvaUS'] ? data['inscricaoDataHorarioProvaUS'] : '-');
            $form.find('.pesNome').html(data['pesNome'] ? data['pesNome'] : '-');
            $form.find('.cursoNome').html(data['cursoNome'] ? data['cursoNome'] : '-');
            $form.find('.campNome').html(data['campNome'] ? data['campNome'] : '-');
            $form.find('.inscricaoClassificacao').html(data['inscricaoClassificacao'] ? data['inscricaoClassificacao'] : '-');
            $form.find('.inscricaoResultado').html(data['inscricaoResultado'] ? data['inscricaoResultado'] : '-');
            $form.find('.inscricaoOpcao').html(data['inscricaoOpcao'] ? data['inscricaoOpcao'] : '-');
            $form.find('.inscricaoStatus').html(data['inscricaoStatus'] ? data['inscricaoStatus'] : '-');
            var dataAtual = new Date();
            var dataExame = new Date(data['inscricaoDataHorarioProvaUS']);

            var arrMesg = [];

            $("#inscricaoResultado").select2('enable');
            $("#inscricaoNota").prop("disabled", false);
            $('#btn-vestibular-inscricao-cursos-salvar').show();
            $("#inscricaoStatusAlertaDiv").html("");
            $("#inscricaoStatusAlertaDiv").hide();

            var arrResultado = {id: '', text: 'Indefinida'};

            if (!data['inscricaoDataHorarioProvaUS'] || data['inscricaoStatus'] !== 'Aceita' || dataAtual < dataExame) {
                $("#inscricaoResultado").select2('disable');
                $("#inscricaoNota").prop("disabled", true);
                $('#btn-vestibular-inscricao-cursos-salvar').hide();
            }

            if (data['inscricaoResultado']) {
                arrResultado = {id: data['inscricaoResultado'], text: data['inscricaoResultado']};
            }

            if (data['inscricaoStatus'] !== 'Aceita') {
                arrMesg.push("A inscrição do candidato não está definida!<br>");
                __inscricaoCursosIndex.showNotificacaoWarning("A inscrição do candidato \"" + (data['inscricaoId'] || '-') + "\" não está definida!");
            }

            if (data['inscricaoDataHorarioProvaUS']) {
                if (dataAtual < dataExame) {
                    arrMesg.push("O exame ainda não foi aplicado! Verifique a data de aplicação.<br>");
                    __inscricaoCursosIndex.showNotificacaoWarning("O exame ainda não foi aplicado! Verifique a data de aplicação.");
                }
            } else {
                arrMesg.push("O candidato não possui a data do exame em seus registros!<br>");
                __inscricaoCursosIndex.showNotificacaoWarning("O candidato não possui a data do exame em seus registros!");
            }

            if (arrMesg.length > 0) {
                $("#inscricaoStatusAlertaDiv").html("ATENÇÃO:<br>" + arrMesg.join('<br>'));
                $('#inscricaoStatusAlertaDiv').show();
            }


            $("#inscricaoResultado").select2('data', arrResultado);

            $('#inscricao-cursos-modal .modal-title').html('Lançamento de nota manual');
            $('#inscricao-cursos-modal').modal();

            __inscricaoCursosIndex.removeOverlay($('.inscricao-curso-view'));
        };

        this.reloadDataTableInscricaoCursos = function () {
            this.getDataTableInscricaoCursos().api().ajax.reload(null, false);
        };

        this.getDataTableInscricaoCursos = function () {
            if (!__inscricaoCursosIndex.options.datatables.inscricaoCursos) {
                if (!$.fn.dataTable.isDataTable('#inscricao-cursos')) {
                    __inscricaoCursosIndex.options.datatables.inscricaoCursos =
                        $('#inscricao-cursos').DataTable();
                } else {
                    __inscricaoCursosIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __inscricaoCursosIndex.options.datatables.inscricaoCursos;
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
        };
    };

    $.inscricaoCursosIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.vestibular.inscricao-cursos.index");

        if (!obj) {
            obj = new InscricaoCursosIndex();
            obj.run(params);
            $(window).data('universa.vestibular.inscricao-cursos.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);