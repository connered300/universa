(function ($, window, document) {
    'use strict';
    var defaults = {
        url: {
            buscaCEP: '',
            pesquisaPF: '',
            buscaFormacao: '',
            gerenciadorDeArquivosThumb: '',
            gerenciadorDeArquivosDownload: '',
        },
        data: {
            necessidadesEspeciais: [],
            pessoaPesquisa: null,
            cursosOpcoes: [],
            locaisOpcoes: [],
            numeroDeCursos: 1,
            linguaEstrangeira: [],
            tipoEdicao: [],
            dataRealizacaoOpcoes: [],
        },
        value: {
            arq: null,
            responsaveis: null,
            inscricaoCursos: null,
            inscricaoLinguaEstrangeira: null,
            seletipoedicao: null,
            pessoaNecessidades: null,
        },
        datatables: {
            responsaveis: null,
            formacoes: null,
        },
        wizardElement: '#selecao-inscricao-wizard',
        formElement: '#selecao-inscricao-form',
        validator: null
    };

    var SelecaoInscricaoAdd = {
        validations: [],
        options: defaults,
        createGUID: function () {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }

            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        },
        __getFunc: function (container, position) {
            position = position || false;

            if (position == false) {
                return;
            }

            if (container[position] && typeof container[position] == 'function') {
                return container[position];
            }

            return false;
        },
        setDefaults: function (opts) {
            opts = opts || [];

            SelecaoInscricaoAdd.options = $.extend(defaults, opts);

            $.validator.setDefaults({
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    }
                    else if (element.parent('label').length) {
                        error.insertAfter(element.parent());
                    }
                    else {
                        error.insertAfter(element);
                    }
                }
            });

            if ($.browser.webkit) {
                $("input[autocomplete=off]").prop("type", "search");
            }

            SelecaoInscricaoAdd.options.validator =
                $(SelecaoInscricaoAdd.options.formElement).validate({ignore: '.ignore'});
        },
        showNotificacao: function (param) {
            var options = $.extend(
                true,
                {content: '', title: '', type: 'info', icon: '', color: '', timeout: false},
                param
            );

            switch (options.type) {
                case 'danger':
                    options.icon = 'fa-times-circle';
                    options.title = options.title || 'Atenção:';
                    options.color = '#a90329';
                    break;
                case 'info':
                    options.icon = 'fa-info-circle';
                    options.title = options.title || 'Informação:';
                    options.color = '#57889c';
                    break;
                case 'success':
                    options.icon = 'fa-check-circle';
                    options.title = options.title || 'Sucesso:';
                    options.color = '#739e73';
                    break;
                case 'warning':
                    options.icon = 'fa-exclamation-circle';
                    options.title = options.title || 'Atenção:';
                    options.color = '#c79121';
                    break;
            }

            var optionsNotification = {
                title: options.title,
                content: "<i>" + options.content + "</i>",
                color: options.color,
                icon: "fa " + options.icon
            };

            if (options.timeout) {
                optionsNotification.timeout = options.timeout;
            }

            $.smallBox(optionsNotification);
        },
        showNotificacaoFN: function (content, title, timeout, type) {
            content = content || '';
            title = title || '';
            timeout = timeout || 20000;

            if (!content) {
                return;
            }

            SelecaoInscricaoAdd.showNotificacao({
                content: content, type: type, title: title, timeout: timeout
            });
        },
        showNotificacaoInfo: function (content, title, timeout) {
            SelecaoInscricaoAdd.showNotificacaoFN(content, title, timeout, 'info');
        },
        showNotificacaoSuccess: function (content, title, timeout) {
            SelecaoInscricaoAdd.showNotificacaoFN(content, title, timeout, 'success');
        },
        showNotificacaoDanger: function (content, title, timeout) {
            SelecaoInscricaoAdd.showNotificacaoFN(content, title, timeout, 'danger');
        },
        showNotificacaoWarning: function (content, title, timeout) {
            SelecaoInscricaoAdd.showNotificacaoFN(content, title, timeout, 'warning');
        },
        addOverlay: function (el, mensagem) {
            mensagem = mensagem || 'Aguarde...';
            el.append('<div class="app-overlay">' + mensagem + '</div>');
        },
        removeOverlay: function (el) {
            el.find('.app-overlay').remove();
        },
        removeErroComponente: function (elemento) {
            if (elemento.find('>.component-item').length > 0) {
                elemento.find('>.component-item').removeClass('bg-danger');
                elemento.find('>.component-item .form-group .help-block').remove();
            } else {
                elemento.removeClass('bg-danger');
            }
        },
        adicionaErroComponente: function (elemento) {
            var $card = null;

            if (elemento.find('>.component-item').length > 0) {
                $card = elemento.find('>.component-item');
                $card.addClass('bg-danger');
            } else {
                $card = elemento;
                $card.addClass('bg-danger');
            }
        },
        steps: {
            dadosPessoais: {
                init: function () {
                    $("[name=pesCpf],[name=paiCpf],[name=maeCpf]").inputmask({
                        showMaskOnHover: false, clearIncomplete: true, mask: ['999.999.999-99']
                    });
                    $("[name=pesNacionalidade],[name=inscricaoEntregaDoc],[name=inscricaoStatus]").select2({
                        language: 'pt-BR'
                    });
                    $("[name=pessoaNecessidades]").select2({
                        language: 'pt-BR',
                        multiple: true,
                        data: function () {
                            return {results: SelecaoInscricaoAdd.options.data.necessidadesEspeciais};
                        }
                    });

                    $("[name=pessoaNecessidades]").select2(
                        'data',
                        SelecaoInscricaoAdd.options.value.pessoaNecessidades || []);

                    $("[name=pesNacionalidade]").change(function () {
                        var pesNacionalidade = this.value;
                        $(this).closest('form')
                            .find(".campoPesCpf")
                            [pesNacionalidade == 'Estrangeiro' ? 'addClass' : 'removeClass']('hidden');
                        $(this).closest('form')
                            .find(".campoPesDocEstrangeiro")
                            [pesNacionalidade != 'Estrangeiro' ? 'addClass' : 'removeClass']('hidden');
                    }).trigger('change');

                    var pesNomeAutocomplete = '#responsavel-aluno-form [name=pesNome]';

                    if ($("#selecao-inscricao-form [name=pesId]").val() != "") {
                        $('#selecao-inscricao-form .buscarDocumentoPessoa').parent().addClass('hide');
                    } else {
                        pesNomeAutocomplete += ', #selecao-inscricao-form [name=pesNome]';
                    }

                    $(pesNomeAutocomplete)
                        .blur(function () {
                            $(this).removeClass('ui-autocomplete-loading');
                        })
                        .autocomplete({
                            source: function (request, response) {
                                var $this = $(this);
                                var $element = $(this.element);
                                var previous_request = $element.data("jqXHR");

                                if (previous_request) {
                                    previous_request.abort();
                                }

                                $element.data("jqXHR", $.ajax({
                                    url: SelecaoInscricaoAdd.options.url.pesquisaPF,
                                    data: {query: request.term},
                                    type: 'POST',
                                    dataType: "json",
                                    success: function (data) {
                                        var transformed = $.map(data || [], function (el) {
                                            el.label = el.pesNome;

                                            return el;
                                        });

                                        response(transformed);
                                    }
                                }));
                            },
                            minLength: 2,
                            select: function (event, ui) {
                                var $element = $(this);
                                SelecaoInscricaoAdd.options.data.pessoaPesquisa = ui.item;
                                SelecaoInscricaoAdd.steps.dadosPessoais.selecaoPessoa($element.closest('form'));
                            }
                        });

                    $(".buscarDocumentoPessoa").click(function () {
                        var $form = $(this).closest('form');
                        var pesNacionalidade = $form.find("[name=pesNacionalidade]").val();
                        var arrPesquisa = {};

                        if (pesNacionalidade != 'Estrangeiro') {
                            var pesCpf = $form.find("[name=pesCpf]").val();
                            var pesCpfNums = pesCpf.replace(/[^0-9]/g, '');

                            if (pesCpfNums.length < 11) {
                                SelecaoInscricaoAdd.notificacaoPessoa('CPF inválido!');

                                return;
                            }
                            arrPesquisa.pesCpf = pesCpf;
                        } else {
                            var pesDocEstrangeiro = $form.find("[name=pesDocEstrangeiro]").val();
                            arrPesquisa.pesDocEstrangeiro = pesDocEstrangeiro;
                        }

                        SelecaoInscricaoAdd.steps.dadosPessoais.pesquisarPesssoaPeloDocumento(arrPesquisa, $form);
                    });

                    $("[name=pesDataNascimento], [name=pesRgEmissao]")
                        .datepicker({
                            prevText: '<i class="fa fa-chevron-left"></i>',
                            nextText: '<i class="fa fa-chevron-right"></i>'
                        })
                        .inputmask({
                            showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']
                        });
                },
                selecaoPessoa: function ($form) {
                    var dados = SelecaoInscricaoAdd.options.data.pessoaPesquisa || {};

                    $.SmartMessageBox({
                        title: "Confirme a operação:",
                        content: '' +
                        'Deseja realmente carregar os dados no formulário?' +
                        '<br>Nome:<b> ' + dados['pesNome'] + '</b>' +
                        '<br>Sexo:<b> ' + dados['pesSexo'] + '</b>' +
                        '<br>CPF:<b> ' + dados['pesCpf'] + '</b>' +
                        '<br>Data de nascimento:<b> ' + dados['pesDataNascimento'] + '</b>' +
                        '<br>Cidade/Estado:<b> ' + dados['endCidade'] + ' - ' + dados['endEstado'] + '</b>',
                        buttons: "[Não][Sim]"
                    }, function (ButtonPress) {
                        if (ButtonPress == "Sim") {
                            var dados = SelecaoInscricaoAdd.options.data.pessoaPesquisa || {};
                            SelecaoInscricaoAdd.steps.dadosPessoais.buscarDadosAlunoPelaPessoa(
                                dados['pesId'], $form
                            );
                        }

                        SelecaoInscricaoAdd.options.data.pessoaPesquisa = null;
                    });

                },
                pesquisarPesssoaPeloDocumento: function (criterioBusca, $form) {
                    $form = $form || $(SelecaoInscricaoAdd.options.formElement);
                    criterioBusca = criterioBusca || {};
                    SelecaoInscricaoAdd.addOverlay($form, 'Aguarde. Pesquisando pessoa pelo documento...');

                    $.ajax({
                        url: SelecaoInscricaoAdd.options.url.pesquisaPF,
                        dataType: 'json',
                        type: 'post',
                        data: criterioBusca,
                        success: function (data) {
                            SelecaoInscricaoAdd.removeOverlay($form);

                            if (Object.keys(data).length == 0) {
                                SelecaoInscricaoAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                            } else {
                                SelecaoInscricaoAdd.options.data.pessoaPesquisa = data[0];
                                SelecaoInscricaoAdd.steps.dadosPessoais.selecaoPessoa($form);
                            }
                        },
                        erro: function () {
                            SelecaoInscricaoAdd.showNotificacaoDanger('Erro desconhecido!');
                            SelecaoInscricaoAdd.removeOverlay($form);
                        }
                    });
                },
                buscarDadosAlunoPelaPessoa: function (pesId, $form) {
                    $form = $form || $(SelecaoInscricaoAdd.options.formElement);

                    var formAluno = $form.attr('id').indexOf('responsavel') == -1;

                    SelecaoInscricaoAdd.addOverlay($form, 'Aguarde. Buscando informações adicionais...');

                    $.ajax({
                        url: SelecaoInscricaoAdd.options.url.pesquisaPF,
                        dataType: 'json',
                        type: 'post',
                        data: {pesId: pesId, dadosDeAluno: formAluno},
                        success: function (data) {
                            SelecaoInscricaoAdd.removeOverlay($form);

                            if (Object.keys(data).length == 0) {
                                SelecaoInscricaoAdd.showNotificacaoInfo('Dados não encontrados!');
                            } else {
                                var dados = data;
                                $form[0].reset();
                                $form.deserialize(dados);

                                if (formAluno) {
                                    SelecaoInscricaoAdd.options.value.inscricaoCursos = dados['inscricaoCursos'] || [];

                                    var camposVestibular = [
                                        'inscricaoResultado', 'inscricaoNota', 'inscricaoId', 'edicaoDescricao'
                                    ];

                                    $.each(camposVestibular, function (i, campo) {
                                        $('.' + campo).html(
                                            SelecaoInscricaoAdd.options.value.vestibular[campo] || '-'
                                        );
                                    });

                                    if (SelecaoInscricaoAdd.options.value.inscricaoCursos) {
                                        $('#inscricaoCursos').componente(['removeAll']);

                                        $.each(SelecaoInscricaoAdd.options.value.inscricaoCursos, function (i, item) {
                                            $('#inscricaoCursos').componente(['add', item]);
                                        });
                                    }
                                }

                                SelecaoInscricaoAdd.validate();
                            }
                        },
                        erro: function () {
                            SelecaoInscricaoAdd.showNotificacaoDanger('Erro desconhecido!');
                            SelecaoInscricaoAdd.removeOverlay($form);
                        }
                    });
                },
                validate: function () {
                    var $form = $(SelecaoInscricaoAdd.options.formElement);
                    SelecaoInscricaoAdd.options.validator.settings.rules = {
                        pesNacionalidade: {required: true},
                        pesCpf: {
                            cpf: true,
                            required: function () {
                                return $form.find('[name=pesNacionalidade]').val() != 'Estrangeiro';
                            }
                        },
                        pesDocEstrangeiro: {
                            required: function () {
                                return $form.find('[name=pesNacionalidade]').val() == 'Estrangeiro';
                            }
                        },
                        pesNome: {required: true, maxlength: 255},
                        pesNaturalidade: {required: false},
                        pesNascUf: {required: false},
                        pesDataNascimento: {required: true, dateBR: true},
                        pesSexo: {required: true},
                        pesRg: {required: true},
                        pesRgEmissao: {required: false, dateBR: true},
                        paiCpf: {cpf: true},
                        maeCpf: {cpf: true}
                    };
                    SelecaoInscricaoAdd.options.validator.settings.messages = {
                        pesNacionalidade: {required: 'Campo obrigatório!'},
                        pesCpf: {
                            required: 'Campo obrigatório!',
                            cpf: 'CPF inválido!'
                        },
                        pesDocEstrangeiro: {required: 'Campo obrigatório!'},
                        pesNome: {required: 'Campo obrigatório!', maxlength: 'Tamanho máximo: 255!'},
                        pesDataNascimento: {
                            required: 'Campo obrigatório!',
                            dateBR: 'Data inválida!'
                        },
                        pesSexo: {required: 'Campo obrigatório!'},
                        pesRg: {required: 'Campo obrigatório!'},
                        pesRgEmissao: {
                            required: 'Campo obrigatório!',
                            dateBR: 'Data inválida!'
                        },
                        paiCpf: {cpf: 'CPF inválido!'},
                        maeCpf: {cpf: 'CPF inválido!'},
                    };

                    return !$(SelecaoInscricaoAdd.options.formElement).valid();
                },
            },
            contatoEndereco: {
                init: function () {
                    $("[name=endCep]").inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99999-999']});
                    $('[name=endNumero]').inputmask({showMaskOnHover: false, mask: ['9{0,9}']});
                    $("[name=conContatoTelefone], [name=conContatoCelular]").inputmask({
                        showMaskOnHover: false, clearIncomplete: true, mask: ['(99) 9999-9999', '(99) 99999-9999']
                    });
                    $(".buscarCep").click(function () {
                        var $element = $(this);
                        var endCep = $(this).closest('form').find("[name=endCep]").val();
                        var endCepNums = endCep.replace(/[^0-9]/g, '');

                        if (endCepNums.length < 8) {
                            SelecaoInscricaoAdd.showNotificacaoWarning('CEP inválido ou não encontrado!');

                            return;
                        }

                        $element.prop('disabled', true);

                        $.ajax({
                            url: SelecaoInscricaoAdd.options.url.buscaCEP,
                            dataType: 'json',
                            type: 'post',
                            data: {cep: endCep},
                            success: function (data) {
                                $element.prop('disabled', false);

                                if (!data.dados) {
                                    SelecaoInscricaoAdd.showNotificacaoWarning('CEP inválido ou não encontrado!');
                                }

                                $element.closest('form').find("[name=endLogradouro]").val(
                                    ((data.dados.tip_logr_nome || '') + ' ' + (data.dados.logr_nome || '')).trim()
                                );
                                $element.closest('form').find("[name=endCidade]").val(data.dados.cid_nome || '');
                                $element.closest('form').find("[name=endEstado]").val(data.dados.est_uf || '').trigger('change');
                                $element.closest('form').find("[name=endBairro]").val(data.dados.bai_nome || '');
                            },
                            erro: function () {
                                SelecaoInscricaoAdd.showNotificacaoDanger('Falha ao buscar CEP!');
                                $element.prop('disabled', false);
                            }
                        });
                    });
                    $('[name=pesNaturalidade], [name=endCidade], [name=inscricaoCidadeFormacao]')
                        .blur(function () {
                            $(this).removeClass('ui-autocomplete-loading');
                        })
                        .autocomplete({
                            source: function (request, response) {
                                var $this = $(this);
                                var $element = $(this.element);
                                var previous_request = $element.data("jqXHR");

                                if (previous_request) {
                                    previous_request.abort();
                                }

                                $element.data("jqXHR", $.ajax({
                                    url: SelecaoInscricaoAdd.options.url.buscaCEP,
                                    data: {cidade: request.term},
                                    type: 'POST',
                                    dataType: "json",
                                    success: function (data) {
                                        var transformed = $.map(data.dados || [], function (el) {
                                            el.label = el.cid_nome;

                                            return el;
                                        });

                                        response(transformed);
                                    }
                                }));
                            },
                            minLength: 2,
                            focus: function (event, ui) {
                                var $element = $(this);
                                $element.val(ui.item.cid_nome);

                                return false;
                            },
                            select: function (event, ui) {
                                var $element = $(this);
                                $element.val(ui.item.cid_nome);

                                if ($element.attr('name') == 'endCidade') {
                                    $element.closest('form').find('[name=endEstado]').val(ui.item.est_uf).trigger('change');
                                }

                                if ($element.attr('name') == 'pesNaturalidade') {
                                    $element.closest('form').find('[name=pesNascUf]').val(ui.item.est_uf).trigger('change');
                                }

                                return false;
                            }
                        });

                    $('[name=pesNascUf], [name=endEstado]')
                        .blur(function () {
                            $(this).removeClass('ui-autocomplete-loading');
                        })
                        .autocomplete({
                            source: function (request, response) {
                                var $this = $(this);
                                var $element = $(this.element);
                                var previous_request = $element.data("jqXHR");

                                if (previous_request) {
                                    previous_request.abort();
                                }

                                $element.data("jqXHR", $.ajax({
                                    url: SelecaoInscricaoAdd.options.url.buscaCEP,
                                    data: {estado: request.term},
                                    type: 'POST',
                                    dataType: "json",
                                    success: function (data) {
                                        var transformed = $.map(data.dados || [], function (el) {
                                            return el.est_uf;
                                        });

                                        response(transformed);
                                    }
                                }));
                            },
                            minLength: 0
                        });
                },
                validate: function () {
                    var $form = $(SelecaoInscricaoAdd.options.formElement);
                    SelecaoInscricaoAdd.options.validator.settings.rules = {
                        endCep: {required: false},
                        endLogradouro: {required: true},
                        endNumero: {
                            required: function () {
                                return $form.find('[name=endComplemento]').val() == '';
                            }, number: true
                        },
                        endComplemento: {
                            required: function () {
                                return $form.find('[name=endNumero]').val() == '';
                            }
                        },
                        endBairro: {required: false},
                        endCidade: {required: true},
                        endEstado: {required: true},
                        conContatoTelefone: {telefoneValido: true, required: false},
                        conContatoCelular: {celular: true, required: true},
                        conContatoEmail: {required: true, email: true},
                    };
                    SelecaoInscricaoAdd.options.validator.settings.messages = {
                        endCep: {required: 'Campo obrigatório!'},
                        endLogradouro: {required: 'Campo obrigatório!'},
                        endNumero: {required: 'Campo obrigatório!', number: 'Número inválido!'},
                        endComplemento: {required: 'Campo obrigatório!'},
                        endBairro: {required: 'Campo obrigatório!'},
                        endCidade: {required: 'Campo obrigatória!'},
                        endEstado: {required: 'Campo obrigatório!'},
                        conContatoTelefone: {required: 'Campo obrigatório!'},
                        conContatoCelular: {required: 'Campo obrigatório!'},
                        conContatoEmail: {required: 'Campo obrigatório!', email: 'Email inválido!'},
                    };

                    return !$(SelecaoInscricaoAdd.options.formElement).valid();
                }
            },
            inscricaoCursos: {
                init: function () {
                    $("#inscricaoCursos").componente({
                        inputName: 'inscricaoCursos',
                        classCardExtra: '',
                        labelContainer: '<div class="form-group"/>',
                        tplItem: '\
                        <div>\
                            <div class="componente-content">\
                            </div>\
                        </div>',
                        fieldClass: 'form-control',
                        fields: [
                            {name: 'incricaoCursosId', label: 'incricaoCursosId', type: 'hidden'},
                            {name: 'inscricaoId', label: 'inscricaoId', type: 'hidden'},
                            {
                                name: 'inscricaoCursoOpcao', label: 'Opção', type: 'text',
                                addCallback: function (e) {
                                    var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                                    var $parent = field.parent();
                                    $parent.addClass('col-sm-2');
                                    $parent.find('.form-control').hide();
                                    $parent.find('label').html('&nbsp;');
                                    $parent.append('<p class="form-control-static inscricaoCursoOpcao">Opção</p>');
                                }
                            },
                            {
                                name: 'selcursosId', label: 'Curso', type: 'text',
                                addCallback: function (e) {
                                    var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                                    var $parent = field.parent();
                                    $parent.addClass('col-sm-6');
                                    var $item = field.closest('.componente-item');
                                    var id = itemConfig['id'] || '';

                                    itemConfig['selcursosId'] = itemConfig['selcursosId'] || null;

                                    field.select2({
                                        language: 'pt-BR',
                                        data: SelecaoInscricaoAdd.steps.inscricaoCursos.getCampusCurso
                                    }).change(function () {
                                        var inscricaoTurno = config.makeFieldName({id: id, name: 'inscricaoTurno'});
                                        $item.find('[name="' + inscricaoTurno + '"]').prop('disabled', false);
                                    });

                                    if (itemConfig['selcursosId']) {
                                        field.select2('data', itemConfig['selcursosId']);
                                    }

                                    field.trigger('change');
                                }
                            },
                            {
                                name: 'inscricaoTurno', label: 'Turno', type: 'text',
                                addCallback: function (e) {
                                    var field = e.data.field,
                                        itemConfig = e.data.itemSettings,
                                        config = e.data.settings;
                                    var $parent = field.parent();
                                    var $item = field.closest('.componente-item');
                                    $parent.addClass('col-sm-4');
                                    var id = itemConfig['id'] || '';
                                    var selcursosNome = config.makeFieldName({id: id, name: 'selcursosId'});

                                    itemConfig['inscricaoTurno'] = itemConfig['inscricaoTurno'] || null;

                                    field.select2({
                                        language: 'pt-BR',
                                        data: function () {
                                            var turnos = [];
                                            var arrSelcursosId = $item
                                                    .find('[name="' + selcursosNome + '"]')
                                                    .select2('data') || [];
                                            var arrPeriodos = arrSelcursosId['periodos'] || [];

                                            for (var i in arrPeriodos) {
                                                turnos.push({id: arrPeriodos[i], text: arrPeriodos[i]});
                                            }
                                            if (JSON.stringify(turnos) != "[]" && !field.attr("obrigatorio")) {
                                                field.attr("obrigatorio", true)
                                            }
                                            return {results: turnos};
                                        }
                                    });
                                    field.select2("val", itemConfig['inscricaoTurno']).trigger('change');
                                }
                            },
                        ],
                        addCallback: function (e) {
                            var item = e.data.item, itemSettings = e.data.itemSettings, settings = e.data.settings;
                            var id = itemSettings['id'] || '';
                            var inscricaoCursoOpcao = settings.makeFieldName({id: id, name: 'inscricaoCursoOpcao'});
                            item.find('.inscricaoCursoOpcao').html((itemSettings.index + 1) + " &ordf; opção de curso");
                            item.find('[name="' + inscricaoCursoOpcao + '"]').val((itemSettings.index + 1));

                        },
                        changeOrderCallback: function (e) {
                            var item = e.data.item, ordem = e.data.index, settings = e.data.settings;
                            var id = item.data('componente.id') || '';
                            var inscricaoCursoOpcao = settings.makeFieldName({id: id, name: 'inscricaoCursoOpcao'});
                            item.find('[name="' + inscricaoCursoOpcao + '"]').val((ordem + 1));
                            item.find('.inscricaoCursoOpcao').html((ordem + 1) + " &ordf; opção de curso");
                        }
                    });

                    var numCursos = SelecaoInscricaoAdd.options.data.numeroDeCursos,
                        quantidadeCursosPossiveis = 0,
                        i = 0;

                    if (SelecaoInscricaoAdd.options.value.inscricaoCursos) {
                        $.each(SelecaoInscricaoAdd.options.value.inscricaoCursos, function (i, item) {
                            $('#inscricaoCursos').componente(['add', item]);
                            numCursos--;
                        });
                    }
                    quantidadeCursosPossiveis = SelecaoInscricaoAdd.options.data.numeroDeCursos - (SelecaoInscricaoAdd.options.value.inscricaoCursos).length;

                    for (i; i < quantidadeCursosPossiveis; i++) {
                        $('#inscricaoCursos').componente(['add', {}]);
                    }

                    $("[name=inscricaoLinguaEstrangeira]").select2({
                        language: 'pt-BR',
                        data: function () {
                            var arrOpcoes = [];
                            var arrLinguas = SelecaoInscricaoAdd.options.data.linguaEstrangeira || [];

                            for (var i in arrLinguas) {
                                arrOpcoes.push({id: arrLinguas[i], text: arrLinguas[i]});
                            }

                            return {results: arrOpcoes};
                        }
                    });

                    $("[name=inscricaoDataHorarioProva]").select2({
                        language: 'pt-BR',
                        data: function () {
                            var arrOpcoes = [];
                            var arrDatas = SelecaoInscricaoAdd.options.data.dataRealizacaoOpcoes || [];

                            for (var i in arrDatas) {
                                arrOpcoes.push({id: arrDatas[i]['text'], text: arrDatas[i]['text']});
                            }

                            return {results: arrOpcoes};
                        }
                    });

                    $("[name=seletipoedicao]").select2({
                        language: 'pt-BR',
                        data: function () {
                            var arrOpcoes = [];
                            var arrTipoEdicao = SelecaoInscricaoAdd.options.data.tipoEdicao || [];

                            for (var i in arrTipoEdicao) {
                                arrOpcoes.push({id: i, text: arrTipoEdicao[i]});
                            }

                            return {results: arrOpcoes};
                        }
                    });

                    $("[name=seletipoedicao]").change(function () {
                        var data = $(this).select2('data') || [];
                        var tipoNome = (data['text'] || '').toLowerCase();

                        if (tipoNome.indexOf('enem') != -1) {
                            $('#opcoesEnem').removeClass('hidden');
                        } else {
                            $('#opcoesEnem').addClass('hidden');
                        }

                        if (tipoNome.indexOf('novo título') != -1) {
                            $('#opcoesNovoTitulo').removeClass('hidden');
                        } else {
                            $('#opcoesNovoTitulo').addClass('hidden');
                        }
                    });

                    $("[name=seletipoedicao]")
                        .select2('val', SelecaoInscricaoAdd.options.value.seletipoedicao || '')
                        .trigger('change');

                    $("[name=inscricaoLinguaEstrangeira]")
                        .select2('val', SelecaoInscricaoAdd.options.value.inscricaoLinguaEstrangeira);

                    var locais= SelecaoInscricaoAdd.options.data.locaisOpcoes || [];
                    $("[name=selelocais]").select2({
                        language: 'pt-BR',
                        data: locais
                    });

                    if (SelecaoInscricaoAdd.options.value.selelocais) {
                        $.each(locais, function (aux, aux1) {
                            if (aux1['id'] == SelecaoInscricaoAdd.options.value.selelocais) {
                                $("[name=selelocais]").select2('data', aux1);
                            }
                        });
                    }

                    $("[name=inscricaoDataFormacao]")
                        .datepicker({
                            prevText: '<i class="fa fa-chevron-left"></i>',
                            nextText: '<i class="fa fa-chevron-right"></i>'
                        })
                        .inputmask({
                            showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']
                        });
                    $(
                        '[name=inscricaoNotaEnemCiencHumanas],' +
                        '[name=inscricaoNotaEnemCiencNatureza],' +
                        '[name=inscricaoNotaEnemPortugues],' +
                        '[name=inscricaoNotaEnemRedacao],' +
                        '[name=inscricaoNotaEnemMatematica]'
                    ).inputmask({showMaskOnHover: false, alias: 'numeric'});
                },
                validate: function () {
                    var haErros = false;
                    var errors = [];
                    var componente = $("#inscricaoCursos").componente();
                    var inscricaoCursos = componente.items();
                    var primeiroCursoPreenchido = true;

                    SelecaoInscricaoAdd.removeErroComponente($("#inscricaoCursos"));

                    $.each(inscricaoCursos, function (i, item) {
                        var $item = $(item);
                        var id = $(item).data('componente.id');
                        SelecaoInscricaoAdd.removeErroComponente($item);

                        var camposVazios = [];

                        var $elSelcursosId = $item
                            .find('[name="' + componente.makeFieldName({id: id, name: 'selcursosId'}) + '"]');
                        var $elInscricaoTurno = $item
                            .find('[name="' + componente.makeFieldName({id: id, name: 'inscricaoTurno'}) + '"]');

                        var inscricaoTurnoVal = $elInscricaoTurno.val();
                        /*Somente turnos com dados são obrigatorios, caso hada dado*/
                        var turnoObrigatorio = $elInscricaoTurno.attr("obrigatorio") ? true : false;
                        var selcursosIdVal = $elSelcursosId.val();

                        if (turnoObrigatorio) {
                            if (inscricaoTurnoVal == "" && turnoObrigatorio) {
                                camposVazios.push("Turno");
                            }

                            if (inscricaoTurnoVal == "" && selcursosIdVal && !camposVazios.length) {
                                camposVazios.push("Turno");
                            }
                        }

                        if (selcursosIdVal == "" && i == 0) {
                            camposVazios.push("Curso");
                        }


                        if (selcursosIdVal == "" && inscricaoTurnoVal && !camposVazios.length) {
                            camposVazios.push("Curso");
                        }

                        if (camposVazios.length > 0) {
                            errors.push(
                                'A ' + (i + 1) + "&ordf; opção de curso " +
                                ' possui campos vazios. Por favor preencha os campos: <b>' +
                                camposVazios.join(', ') + '</b>.'
                            );
                            SelecaoInscricaoAdd.adicionaErroComponente($item, true);
                        }
                    });

                    if (Object.keys(errors).length > 0) {
                        if (errors) {
                            SelecaoInscricaoAdd.adicionaErroComponente($("#inscricaoCursos"));
                            SelecaoInscricaoAdd.showNotificacaoWarning(errors.join('<br>'));
                            haErros = true;
                        }

                        haErros = true;
                    }

                    SelecaoInscricaoAdd.options.validator.settings.rules = {
                        inscricaoEntregaDoc: {required: true},
                        inscricaoLinguaEstrangeira: {required: false},
                        inscricaoCursos: {required: true},
                        pesSexo: {required: true},
                        seletipoedicao: {required: true},
                        selelocais: {required: true},
                        inscricaoDataHorarioProva: {required: true},
                        inscricaoNotaEnemCiencHumanas: {
                            required: function () {
                                return !$('#opcoesEnem').hasClass('hidden')
                            }, number: true
                        },
                        inscricaoNotaEnemCiencNatureza: {
                            required: function () {
                                return !$('#opcoesEnem').hasClass('hidden')
                            }, number: true
                        },
                        inscricaoNotaEnemPortugues: {
                            required: function () {
                                return !$('#opcoesEnem').hasClass('hidden')
                            }, number: true
                        },
                        inscricaoNotaEnemRedacao: {
                            required: function () {
                                return !$('#opcoesEnem').hasClass('hidden')
                            }, number: true
                        },
                        inscricaoNotaEnemMatematica: {
                            required: function () {
                                return !$('#opcoesEnem').hasClass('hidden')
                            }, number: true
                        },
                        inscricaoUltimaFormacao: {
                            required: function () {
                                return !$('#opcoesNovoTitulo').hasClass('hidden')
                            }
                        },
                        inscricaoDataFormacao: {
                            required: function () {
                                return !$('#opcoesNovoTitulo').hasClass('hidden')
                            }, dateBR: true
                        },
                        inscricaoCidadeFormacao: {
                            required: function () {
                                return !$('#opcoesNovoTitulo').hasClass('hidden')
                            }
                        },
                        inscricaoFormacaoInstituicao: {
                            required: function () {
                                return !$('#opcoesNovoTitulo').hasClass('hidden')
                            }
                        }
                    };
                    SelecaoInscricaoAdd.options.validator.settings.messages = {
                        inscricaoEntregaDoc: {required: 'Campo obrigatório!'},
                        inscricaoLinguaEstrangeira: {required: 'Campo obrigatório!'},
                        inscricaoCursos: {required: 'Campo obrigatório!'},
                        pesSexo: {required: 'Campo obrigatório!'},
                        seletipoedicao: {required: 'Campo obrigatório!'},
                        selelocais: {required: 'Campo obrigatório!'},
                        inscricaoDataHorarioProva: {required: 'Campo obrigatório!', dateBR: 'Data inválido!'},
                        inscricaoNotaEnemCiencHumanas: {required: 'Campo obrigatório!', number: 'Número inválido!'},
                        inscricaoNotaEnemCiencNatureza: {required: 'Campo obrigatório!', number: 'Número inválido!'},
                        inscricaoNotaEnemPortugues: {required: 'Campo obrigatório!', number: 'Número inválido!'},
                        inscricaoNotaEnemRedacao: {required: 'Campo obrigatório!', number: 'Número inválido!'},
                        inscricaoNotaEnemMatematica: {required: 'Campo obrigatório!', number: 'Número inválido!'},
                        inscricaoUltimaFormacao: {required: 'Campo obrigatório!'},
                        inscricaoDataFormacao: {required: 'Campo obrigatório!'},
                        inscricaoCidadeFormacao: {required: 'Campo obrigatório!'},
                        inscricaoFormacaoInstituicao: {required: 'Campo obrigatório!'},
                    };

                    return !$(SelecaoInscricaoAdd.options.formElement).valid();
                },
                getCampusCurso: function () {
                    var inscricaoCursos = $('#inscricaoCursos').componente('exportJSON');
                    var arrCursosSelecionados = [];

                    $.each(inscricaoCursos, function (index, item) {
                        if (item['selcursosId']) {
                            arrCursosSelecionados.push(item['selcursosId']);
                        }
                    });

                    var data = [];

                    for (var i in SelecaoInscricaoAdd.options.data.cursosOpcoes || []) {
                        var item = SelecaoInscricaoAdd.options.data.cursosOpcoes[i];
                        var adiciona = true;

                        $.each(arrCursosSelecionados, function (index, cursocampusId) {
                            if (cursocampusId == item['id']) {
                                adiciona = false;
                                return true;
                            }
                        });

                        if (adiciona) {
                            data.push(item);
                        }
                    }

                    return {results: data};
                }
            },
        },
        loadSteps: function (begin, end) {
            var keys = Object.keys(SelecaoInscricaoAdd.steps);

            for (var i = 0; i < keys.length; i++) {
                var action = SelecaoInscricaoAdd.__getFunc(SelecaoInscricaoAdd.steps[keys[i]] || [], 'init');

                if (action) {
                    action();
                }
            }
        },
        setValidations: function () {
            $(document).on("change", ".form-control:hidden", function () {
                $(SelecaoInscricaoAdd.options.formElement).valid();
            });

            $(SelecaoInscricaoAdd.options.formElement).submit(function (e) {
                var ok = SelecaoInscricaoAdd.validate();

                if ($('[name=pesSexo]:checked').length == 0) {
                    SelecaoInscricaoAdd.showNotificacaoDanger('Selecione o sexo!');
                    ok = false;
                }

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var componente = $("#inscricaoCursos").componente();
                var inscricaoCursos = componente.items();

                $.each(inscricaoCursos, function (i, item) {
                    var $item = $(item);
                    var id = $(item).data('componente.id');
                    $item
                        .find('[name="' + componente.makeFieldName({id: id, name: 'cursocampusId'}) + '"]')
                        .prop('disabled', false);
                });

                if (Object.keys(inscricaoCursos).length == 0) {
                    SelecaoInscricaoAdd.showNotificacaoDanger('Selecione o curso!');
                    return false;
                }

                $('#responsaveis').val(JSON.stringify(SelecaoInscricaoAdd.options.value.responsaveis));
                $('#formacoes').val(JSON.stringify(SelecaoInscricaoAdd.options.value.formacoes));
            });
        },
        validate: function () {
            var navigation = $(SelecaoInscricaoAdd.options.wizardElement).find('ul:first');
            var nitens = navigation.find('li').length;
            var keys = Object.keys(SelecaoInscricaoAdd.steps);

            for (var i = 0; i < nitens; i++) {
                var action = SelecaoInscricaoAdd.__getFunc(SelecaoInscricaoAdd.steps[keys[i]] || [], 'validate');

                if (action) {
                    var err = action();

                    if (err) {
                        $(".form-wizard-nav li a").eq(i).click();

                        return false;
                    }
                }
            }

            return true;
        },
        wizard: function () {
            var handleTabShow = function (tab, navigation, index, wizard) {
                var total = navigation.find('li').length;
                var current = index + 0;
                var percent = (current / (total - 1)) * 100;
                var percentWidth = 100 - (100 / total) + '%';

                navigation.find('li').removeClass('done');
                navigation.find('li.active').prevAll().addClass('done');

                wizard.find('.progress-bar').css({width: percent + '%'});
                $(SelecaoInscricaoAdd.options.wizardElement)
                    .find('.form-wizard-horizontal')
                    .find('.progress')
                    .css({'width': percentWidth});
            };

            var itens = $(SelecaoInscricaoAdd.options.wizardElement).find('.form-wizard li');
            itens.css('width', (100 / itens.length) + '%');

            $(SelecaoInscricaoAdd.options.wizardElement).find('.progress').css({
                'width': ((100 / itens.length) * (itens.length - 1 ) * 0.99) + '%',
                'margin': '0 ' + ((100 / itens.length) / 2) + '%'
            });

            $(SelecaoInscricaoAdd.options.wizardElement).bootstrapWizard({
                tabClass: '',
                onTabShow: function (tab, navigation, index) {
                    handleTabShow(tab, navigation, index, $(SelecaoInscricaoAdd.options.wizardElement));
                },
                onTabClick: function (objAbaAtiva, objNavegacao, indexAbaAtiva, indexAbaDestino) {
                    var numeroAbas = objNavegacao.find('li').length;
                    if (indexAbaDestino >= numeroAbas) {
                        indexAbaDestino = numeroAbas - 1;
                    }

                    if (indexAbaAtiva < indexAbaDestino) {
                        var keys = Object.keys(SelecaoInscricaoAdd.steps);

                        for (var i = indexAbaAtiva; i < indexAbaDestino; i++) {
                            var action = SelecaoInscricaoAdd.__getFunc(
                                SelecaoInscricaoAdd.steps[keys[i]] || [],
                                'validate'
                            );

                            if (action) {
                                var err = action();

                                if (err) {
                                    return false;
                                }
                            }

                            $(SelecaoInscricaoAdd.options.wizardElement).bootstrapWizard('next');
                        }

                        return false;
                    }
                },
                onNext: function (objAbaAtiva) {
                    var indexAbaAtiva = objAbaAtiva.index();
                    var keys = Object.keys(SelecaoInscricaoAdd.steps);
                    var action = SelecaoInscricaoAdd.__getFunc(
                        SelecaoInscricaoAdd.steps[keys[indexAbaAtiva]] || [],
                        'validate'
                    );

                    if (action) {
                        var err = action();

                        if (err) {
                            return false;
                        }
                    }
                }
            });
        },
        run: function (opts) {
            SelecaoInscricaoAdd.setDefaults(opts);
            SelecaoInscricaoAdd.loadSteps();
            SelecaoInscricaoAdd.setValidations();
            SelecaoInscricaoAdd.wizard();
        }
    };

    $.selecaoInscricaoAdd = function (params) {
        params = params || [];
        var obj = $(window).data("universa.selecaoInscricao.add");

        if (!obj) {
            obj = SelecaoInscricaoAdd;
            obj.run(params);
            $(window).data('universa.selecaoInscricao.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);