(function ($, window, document) {
    'use strict';

    var ProfessorDiario = function () {
        var pub = this;
        var priv = {
            options: {
                url: {
                    informacaoPendencia: '',
                    alunoPendenciaNotas: '',
                    finalizarLancamentosParte1: '',
                    alunosFinal: '',
                    finalizarLancamentosParte2: ''
                },
                datatable: {
                    alunosPendenciaNotas: null,
                    alunosNotasFinais: null
                },
                data: {
                    docdiscId: '-',
                    alunosPendenciaNotas: [],
                    informacoesPendencias: [],
                    alunosNotasFinais: [],
                    configNota: []
                }
            }
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];

            priv.options = $.extend(priv.options, opts);
            priv.options.data.docdiscId = $('#docdiscId').val();
        };

        priv.atualizaInformacoesPendencias = function () {
            $('#diario-classe-mensagem').html('Atualizando informações. Aguarde...');
            $('#painelAlunosPendenciaNotas').hide();
            $('#fechamento-continuar-parte1').hide();
            $('#painelAlunosNotasFinais').hide();
            $('#fechamento-continuar-parte2-salvar, #fechamento-continuar-parte2-finalizar').hide();
            $('#entregaDiarios').hide();

            $.ajax({
                url: priv.options.url.informacaoPendencia,
                type: 'POST',
                data: {docdiscId: priv.options.data.docdiscId},
                dataType: 'json',
                success: function (data) {
                    priv.options.data.informacoesPendencias = data['informacoesPendencias'] || [];
                    var notasPendentes = (priv.options.data.informacoesPendencias['notas_pendentes'] || 0);
                    var docdiscDataFechamento = (
                        priv.options.data.informacoesPendencias['docdisc_data_fechamento'] || false
                    );
                    var docdiscDataFechamentoFinal = (
                        priv.options.data.informacoesPendencias['docdisc_data_fechamento_final'] || false
                    );

                    priv.atualizaEntregaDiario();

                    if (notasPendentes > 0) {
                        $('#diario-classe-mensagem').html(
                            'Para que se torne possível a finalização do período letivo,' +
                            'você precisa lançar as notas de todos os alunos matriculados na disciplina.<br>' +
                            '<b>Verifique as notas dos alunos:</b>'
                        );
                        priv.atualizaListagemAlunosPendenciaNotas();
                        $('#painelAlunosPendenciaNotas').show();
                    } else if (!docdiscDataFechamento) {
                        $('#diario-classe-mensagem').html(
                            '<p>' +
                            'Aparentemente está tudo certo com seus lançamentos de notas. ' +
                            'O fechamento do período ocorrerá da seguinte forma:' +
                            '</p>' +
                            '<ul>' +
                            '<li><b>Parte 1:</b> Fechamento de notas de alunos regulares;</li>' +
                            '<li><b>Parte 2:</b> Fechamento de notas de alunos que ficaram de exame final;</li>' +
                            '</ul>' +
                            '<p>Clique em "continuar" para prosseguir.</p>'
                        );
                        $('#fechamento-continuar-parte1').show();
                    } else if (!docdiscDataFechamentoFinal) {
                        priv.atualizaListagemAlunosNotasFinais();

                        $('#diario-classe-mensagem').html(
                            '<p>' +
                            'Aparentemente está tudo certo com seus lançamentos de notas. ' +
                            'O fechamento do período ocorrerá da seguinte forma:' +
                            '</p>' +
                            '<ul>' +
                            '<li style="text-decoration: line-through"><b>Parte 1:</b> Fechamento de notas de alunos regulares;</li>' +
                            '<li><b>Parte 2:</b> Fechamento de notas de alunos que ficaram de exame final;</li>' +
                            '</ul>' +
                            '<p>Clique em "Finalizar" para fechar as notas do período e ' +
                            'em "Salvar" para salvar as notas sem finalizar o lançamento.</p>'
                        );

                        $('#painelAlunosNotasFinais').show();
                        $('#fechamento-continuar-parte2-salvar, #fechamento-continuar-parte2-finalizar').show();
                    } else {
                        $('#diario-classe-mensagem').html(
                            '<p>Aparentemente está tudo certo com seus lançamentos.</p>' +
                            '<p>Você já efetuou o fechamento de notas de alunos regulares e de notas de alunos que ficaram de exame final.</p>' +
                            '<p>Caso precise efetuar alguma alteração, procure a coordenação.</p>'
                        );
                    }
                }
            });
        };

        priv.atualizaEntregaDiario = function () {
            var diarios = (priv.options.data.informacoesPendencias['diarios'] || false);

            if (diarios) {
                $('#entregaDiarios thead tr').html('');
                $('#entregaDiarios tbody tr').html('');

                $.each(diarios, function (i, item) {
                    var mes = item['mes'].substring(0, 3).toUpperCase();
                    $('#entregaDiarios thead tr').append('<td class="text-center">' + mes + '</td>');
                    $('#entregaDiarios tbody tr').append(
                        '<td>' +
                        '<i class="fa fa-book" title="Diário de anotações"></i> ' +
                        (item['dataAnotacao'] ? item['dataAnotacao'] : 'Pendente') + '<br>' +
                        '<i class="fa fa-check-square" title="Diário de frequência"></i> ' +
                        (item['dataFrequencia'] ? item['dataFrequencia'] : 'Pendente') +
                        '</td>'
                    );
                });

                $('#entregaDiarios').show();
            }
        };
        priv.atualizaListagemAlunosPendenciaNotas = function () {
            priv.options.datatable.alunosPendenciaNotas.api().clear().draw();
            priv.options.datatable.alunosPendenciaNotas.api().row.add(
                {pes_nome: 'Aguarde...', etapas_pendentes: ''}
            ).draw();

            $.ajax({
                url: priv.options.url.alunoPendenciaNotas,
                data: {docdiscId: priv.options.data.docdiscId},
                cache: false,
                type: "POST",
                dataType: "json",
                success: function (data) {
                    var d = data.alunosPendenciaNotas;
                    priv.options.datatable.alunosPendenciaNotas.api().clear().draw();

                    for (var i = 0; i < d.length; i++) {
                        priv.options.datatable.alunosPendenciaNotas.api().row.add(d[i]).draw(false);
                    }
                }
            });
        };

        priv.atualizaListagemAlunosNotasFinais = function () {
            priv.options.datatable.alunosNotasFinais.api().clear().draw();
            priv.options.datatable.alunosNotasFinais.api().row.add(
                {pes_nome: 'Aguarde...', alunofinal_nota_campo: ''}
            ).draw();

            $.ajax({
                url: priv.options.url.alunosFinal,
                data: {docdiscId: priv.options.data.docdiscId},
                cache: false,
                type: "POST",
                dataType: "json",
                success: function (data) {
                    if (!data.configNota) {
                        alert("Configuração de nota inválida!");
                        $(".sorting_1").text('Configuração de nota inválida!');
                        $(".text-center").addClass('hidden');
                        return false;
                    }

                    $(".text-center").removeClass('hidden');

                    priv.options.data.alunosNotasFinais = data.alunosNotasFinais;
                    priv.options.data.configNota = data.configNota;
                    priv.options.datatable.alunosNotasFinais.api().clear().draw();

                    if (priv.options.data.alunosNotasFinais.length == 0) {
                        $('#fechamento-continuar-parte2-salvar').hide();
                        return;
                    }

                    for (var i = 0; i < priv.options.data.alunosNotasFinais.length; i++) {
                        var alunodiscId = priv.options.data.alunosNotasFinais[i]['alunodisc_id'];
                        var alunofinalNota = priv.options.data.alunosNotasFinais[i]['alunofinal_nota'] || '';
                        priv.options.data.alunosNotasFinais[i]['alunofinal_nota_campo'] = '\
                    <input class="form-control input-notas" name="notasFinais[' + alunodiscId + ']"\
                        style="width: 100% !important" type="text" placeholder="Nota Final" maxlength="5"\
                        value="' + alunofinalNota + '" >';
                        priv.options.datatable.alunosNotasFinais.api().row.add(priv.options.data.alunosNotasFinais[i]).draw(false);
                    }

                    $('.input-notas').inputmask({
                        showMaskOnHover: false,
                        showMaskOnFocus: false,
                        clearMaskOnLostFocus: true,
                        alias: 'currency', groupSeparator: "", radixPoint: ".",
                        placeholder: "", prefix: ""
                    });
                    $('.input-notas').change(function () {
                        var $this = $(this);
                        var notaMax = priv.options.data.configNota['notaMax'] || 0;
                        var nota = parseFloat($this.val() || 0);

                        if (nota > notaMax) {
                            alert("O valor é maior que o permitido: " + notaMax + "!");
                            $this.val('');
                        }
                    });
                }
            });
        };

        priv.iniciaListagemAlunosPendenciaNotas = function () {
            priv.options.datatable.alunosPendenciaNotas = $('#alunosPendenciaNotas').dataTable({
                processing: true,
                columns: [
                    {name: "pes_nome", targets: 0, data: "pes_nome", width: '70%'},
                    {name: "etapas_pendentes", targets: 1, data: "etapas_pendentes", width: '30%'}
                ],
                autoWidth: false,
                "dom": "frt<'dt-toolbar-footer'<'col-sm-12'p>>",
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                "bLengthChange": false
            });
        };

        priv.iniciaListagemAlunosNotasFinais = function () {
            priv.options.datatable.alunosNotasFinais = $('#alunosNotasFinais').dataTable({
                processing: true,
                columns: [
                    {name: "pes_nome", targets: 0, data: "pes_nome", width: '80%'},
                    {name: "alunofinal_nota_campo", targets: 1, data: "alunofinal_nota_campo", width: '20%'}
                ],
                autoWidth: false,
                "dom": "frt<'dt-toolbar-footer'<'col-sm-12'p>>",
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                "bPaginate": false
            });

            $("#fechamento-continuar-parte2-salvar, #fechamento-continuar-parte2-finalizar").click(function () {
                var id = $(this).attr('id');
                $("input[type='search']").val('').trigger('keyup');
                $('#form-lancameto-notas-finais').find('input').prop('readonly', true);

                var dataArray = $('#form-lancameto-notas-finais').serializeJSON();

                dataArray['finalizar'] = (id.indexOf('finalizar') != -1 ? 1 : 0);
                dataArray['docdiscId'] = priv.options.data.docdiscId;

                $('#fechamento-continuar-parte2-salvar, #fechamento-continuar-parte2-finalizar')
                    .attr('disabled', 'disabled').find('.fa').remove();
                $(this).prepend('<i class="fa fa-refresh fa-spin fa-fw"></i>');

                $.ajax({
                    url: priv.options.url.finalizarLancamentosParte2,
                    dataType: 'json',
                    type: 'post',
                    data: dataArray,
                    success: function (data) {
                        $('#fechamento-continuar-parte2-salvar, #fechamento-continuar-parte2-finalizar')
                            .removeAttr('disabled').find('.fa').remove();

                        if (data['erro'] == 0) {
                            priv.atualizaInformacoesPendencias();
                        }

                        if (data['msg']) {
                            alert(data['msg']);
                        }
                    }
                });
            });
        };

        priv.finalizarLancamentosParte1 = function () {
            var docdisc_data_fechamento = (priv.options.data.informacoesPendencias['docdisc_data_fechamento'] || false);

            if (docdisc_data_fechamento) {
                alert('O fechamento da primeira parte já foi efetuado!');
                return;
            }

            if (confirm("Ao finalizar o período letivo as situações do alunos serão processadas e as respectivas notas não poderão ser editadas.\nDeseja Finalizar mesmo assim?")) {
                alert("As notas serão calculadas! Aguarde.");
            } else {
                alert('Fique atento a data de limite de fechamento do período letivo');
                return false;
            }

            $('#fechamento-continuar-parte1').attr('disabled', 'disabled').find('.fa').remove();
            $('#fechamento-continuar-parte1').prepend('<i class="fa fa-refresh fa-spin fa-fw"></i>');

            $.ajax({
                url: priv.options.url.finalizarLancamentosParte1,
                type: 'POST',
                data: {docdiscId: priv.options.data.docdiscId},
                dataType: 'json',
                success: function (data) {
                    $('#fechamento-continuar-parte1').removeAttr('disabled').find('.fa').remove();

                    if (data['erro'] == 0) {
                        priv.atualizaInformacoesPendencias();
                    }

                    if (data['msg']) {
                        alert(data['msg']);
                    }
                }
            });
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.iniciaListagemAlunosPendenciaNotas();
            priv.iniciaListagemAlunosNotasFinais();
            $('#diarioClasse').click(priv.atualizaInformacoesPendencias);
            $('#fechamento-continuar-parte1').click(priv.finalizarLancamentosParte1);
        };
    };

    $.professorDiario = function (params) {
        params = params || [];

        var obj = $(window).data("universa.professor.diario");

        if (!obj) {
            obj = new ProfessorDiario();
            obj.run(params);
            $(window).data('universa.professor.diario', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);