/**
 * Created by breno on 29/07/15.
 */

function daysWeekDisabled() {
    var diasAula = JSON.parse($("#aulas").html());
    var diasNao = diasQueNaoDaAula(diasAula);
    var numDias = numeroDias(diasNao);
    return numDias;
}

function getStartDate() {
    return new Date($("#startDate").html());
}

function getEndDate() {
    return new Date($("#endDate").html());
}

/*
 *
 * ================= SCRIPT PARA O INDEX ====================
 * */
function index() {
    jQuery(function () {
        $('#DataTables_Table_0').dataTable({
            "oLanguage": {
                "sInfo": "",
                "sZeroRecords": "Nenhum registro encontrado",
                "sInfoEmpty": "Exibindo 0 / 0 de _TOTAL_ registro(s)",
                "sLengthMenu": "Exibir _MENU_ registros por página",
                "sInfoFiltered": "(busca de _MAX_ registros )",
                "sSearch": "",
                "sPlaceholder": "Busca"
            },
            "sDom": "<'dt-toolbar'<'col-xs-6'><'col-xs-6'T>f>" +
            "t" +
            "<'dt-toolbar-footer'<'col-xs-6'i><'col-xs-6'>>",
            "bPaginate": false
        });
    });

    jQuery(function () {
        var alteracoes = false;

        jQuery("form[name='frequencia']").submit(function () {
            $("input[name='salvar']").val($("input[name='lancamentoBloqueado']").val() == 0);
            return true;
        });

        jQuery("input[name='alunoFaltaAula[]']").change(function () {
            alteracoes = true;
        });

        jQuery("[name='voltar']").click(function () {
            var weekNumber = parseInt(jQuery("input[name='numSemana']").val());
            jQuery("input[name='numSemana']").val(weekNumber - 1);

            if (alteracoes) {
                salvarAlteracoes();
            }

            jQuery("form[name='frequencia']").submit();
        });

        jQuery("[name='avancar']").click(function () {
            var weekNumber = parseInt(jQuery("input[name='numSemana']").val());
            jQuery("input[name='numSemana']").val(weekNumber + 1);

            if (alteracoes) {
                salvarAlteracoes();
            }

            jQuery("form[name='frequencia']").submit();
        });

        jQuery("input[name='datepicker']").datepicker({
            changeMonth: true,
            changeYear: true,
            showWeek: true,
            daysOfWeekDisabled: daysWeekDisabled(),
            format: "dd/mm/yyyy",
            startDate: getStartDate(),
            endDate: getEndDate()
        });
        jQuery("#dataAula").change(function () {
            $("input[name='datebusca']").val(true);

            if (alteracoes) {
                salvarAlteracoes();
            }

            $("form[name='frequencia']").submit();
        });

        var alunoFaltaAula = [];
        var faltas = window.faltasLancadas || [];

        jQuery("[name*=alunoFaltaAula]").each(function () {
            var string = $(this).val();
            var obj = string.split("$$");
            for (var i = 0; i < faltas.length; i++) {
                for (var j = 0; j < faltas[i].length; j++) {
                    if (obj[2] === faltas[i][j]['frequencia_data'] &&
                        parseInt(obj[0]) == parseInt(faltas[i][j]['alunodisc_id'])) {
                        $(this).prop('checked', false).trigger('change');
                        $(this).removeAttr('checked').trigger('change');
                    }
                }
            }
        });

        $("#anotations").click(function () {
            buscaAnotacoes();
        });
        $("#documentos").click(function () {
            exiberelatorios();
        });
    });
}

function salvarAlteracoes(alt) {
    if ($("input[name='lancamentoBloqueado']").val() == 0) {
        var salvar = confirm("Deseja salvar as alterações ?");
        $("input[name='salvar']").val(salvar);
    }
}

function diasQueNaoDaAula(diasAula) {
    var semana = ['Segunda', 'Terca', 'Quarta', 'Quinta', 'Sexta', 'Sabado', 'Domingo'];
    var aulas = new Array();
    for (var i = 0; i < diasAula.length; i++) {
        aulas.push(diasAula[i]['nome']);
    }
    var dif = arr_diff(semana, aulas);
    return dif;
}

function numeroDias(dias) {
    var num = new Array();
    var diasSemana = {
        'Segunda': 1,
        'Terca': 2,
        'Quarta': 3,
        'Quinta': 4,
        'Sexta': 5,
        'Sabado': 6,
        'Domingo': 0
    };
    for (var i = 0; i < dias.length; i++) {
        num.push(diasSemana[dias[i]]);
    }
    return num;
}

function arr_diff(a1, a2) {
    var a = [], diff = [];
    for (var i = 0; i < a1.length; i++) {
        a[a1[i]] = true;
    }
    for (var i = 0; i < a2.length; i++) {
        if (a[a2[i]]) {
            delete a[a2[i]];
        } else {
            a[a2[i]] = true;
        }
    }
    for (var k in a) {
        diff.push(k);
    }
    return diff;
}

function buscaAnotacoes() {
    var docdiscId = window.location.pathname;
    docdiscId = docdiscId.split("/");
    docdiscId = docdiscId[4];
    $.ajax({
        url: "/professor/disciplinas/anotacoes",
        dataType: 'html',
        type: 'post',
        data: {docdiscId: docdiscId},
        success: function (html) {
            $("#blocks-profile-1").empty();
            $("#blocks-profile-1").append(html);
        },
        error: function () {
            $("#blocks-profile-1").empty();
            $("#blocks-profile-1").append("<h3><i class='fa fa-exclamation-triangle'></i> Erro ao buscar anotações!</h3>"
                +
                "<p>Sua sessão pode ter expirado se ficou muito tempo sem interação com sistema</p>"
                + "<p>\
                    <a href='/acesso/autenticacao/logout'><b>Efetue login novamente</b></a> ou verifique sua conexão com a Internet.\
                </p>");
        }
    });
}

/*
 *
 * ================= SCRIPT PARA O ANOTACOES ====================
 * */
function anotacoes() {
    $(function () {
        $('#anotacoes').dataTable({
            "oLanguage": {
                "sInfo": "",
                "sZeroRecords": "Nenhum registro encontrado",
                "sInfoEmpty": "Exibindo 0 / 0 de _TOTAL_ registro(s)",
                "sLengthMenu": "Exibir _MENU_ registros por página",
                "sInfoFiltered": "(busca de _MAX_ registros )",
                "sSearch": "",
                "sPlaceholder": "Busca",
                "oPaginate": {
                    "sPrevious": "Anterior",
                    "sNext": "Próximo",
                    "sLast": "Último"
                }
            },
            "iDisplayLength": 10,
            "bLengthChange": false,
            //            "sDom":"<'dt-toolbar'<'col-xs-6'><'col-xs-6'T>f>" +
            //            "t" +
            //            "<'dt-toolbar-footer'<'col-xs-6'i><'col-xs-6'>>",
            "aoColumnDefs": [
                {'bSortable': false, 'aTargets': [0]}
            ]
        });

        jQuery("input[name='datepicker']").datepicker({
            changeMonth: true,
            changeYear: true,
            showWeek: true,
            daysOfWeekDisabled: daysWeekDisabled(),
            format: "dd/mm/yyyy",
            startDate: getStartDate(),
            endDate: getEndDate()
        });
        jQuery("input[name='anotData']").datepicker({
            changeMonth: true,
            changeYear: true,
            showWeek: true,
            daysOfWeekDisabled: daysWeekDisabled(),
            format: "dd/mm/yyyy",
            startDate: getStartDate(),
            endDate: getEndDate()
        });


        $("input[name='anotacaoArquivo']").change(function (event) {
            var data = new FormData();
            var files = event.target.files;
            for (var j = 0; j < files.length; j++) {
                data.append(j, files[j]);
            }

            $.ajax({
                url: '/professor/acadperiodo-anotacao-arquivo/insert-file', // Url do lado server que vai receber o arquivo
                data: data,
                processData: false, // Don't process the files
                contentType: false,
                dataType: 'json',
                type: 'POST',
                success: function (data) {
                    for (var i = 0; i < data.filesAdd.length; i++) {
                        $("#bodyTableArquivos").append(geraLinhaArquivo(data.filesAdd[i], true));
                        $("input[name='anotacaoArquivo']").val('');
                    }
                },
                erro: function () {
                    alert("Ocorreu um erro ao tentar enviar arquivo. Verifique sua conexão com à internet.");
                }
            });
        });

        $("#myModal").on('hidden.bs.modal', function () {
            if ($("input[name='anotId']").val().trim() == '') {
                $("input[name='arq[]']").each(function () {
                    excluirArquivo(this.value, null);

                });
            }
        });
    });
}

function geraLinhaArquivo(arquivo, newArq) {
    var html = '<tr>';
    if (newArq) {
        html += "<input type='hidden' name='arq[]' value='" + arquivo.arqId + "' >";
    }
    html += "<td>" + arquivo.arqNome + "</td><td><a href='/gerenciador-arquivos/arquivo/download/" + arquivo.arqChave +
        "'><i class='fa fa-cloud-download' ></i>Download</a><a onclick='removeArquivo(" + arquivo.arqId +
        ", this)'><i class='fa fa-trash-o'></i>Remover</a></td></tr>";
    return html
}

function excluirArquivo(id, button) {
    $.ajax({
        url: "/professor/acadperiodo-anotacao-arquivo/remove-file",
        dataType: 'json',
        type: 'post',
        data: {arquivoId: id},
        success: function (data) {
            if (data.deletado) {
                $(button).parent().parent().detach();
            } else {
                alert('Erro ao excluir arquivo! Tente novamente. Se o erro persistir contate o suporte.');
            }
        },
        erro: function () {
            alert('Erro ao excluir arquivo! Verifique sua conexão com a internet.');
        }
    });
}


function removeArquivo(id, button) {
    if (confirm("Deseja realmente excluir este arquivo")) {
        excluirArquivo(id, button);
    }
}

function buscaAnotacao(id) {
    var anotacoes = JSON.parse($("#anotacoesJson").html());
    for (var i = 0; i < anotacoes.length; i++) {
        if (anotacoes[i].anotId == id) {
            return anotacoes[i]
        }
    }
    return null;
}
function editaAnotacao(anotId) {
    limpaModal();
    var anotacao = buscaAnotacao(anotId);
    if (anotacao) {
        preencheModal(anotacao);
        $.ajax({
            url: "/professor/acadperiodo-anotacao-arquivo/busca-arquivos-anotacao",
            dataType: 'json',
            type: 'post',
            data: {anotacaoId: anotId},
            success: function (data) {
                for (var i = 0; i < data.arquivos.length; i++) {
                    $("#bodyTableArquivos").append(geraLinhaArquivo(data.arquivos[i], false));
                }
            },
            erro: function () {
                alert('Erro ao excluir arquivo! Verifique sua conexão com a internet.');
            }
        });
    }
}

function limpaModal() {
    if ($(".modal-backdrop").is(":visible")) {
        $(".modal-backdrop").addClass("hide");
    }

    if (CKEDITOR.instances.anotDescricao) {
        CKEDITOR.config.contentsCss = window.urlCssCKEDITOR;
        try {
            CKEDITOR.instances.anotDescricao.destroy();
        } catch (e) {
        }

    }

    jQuery("[name=anotDescricao]").ckeditor({
        height: '200px', linkShowAdvancedTab: false,
        autoStartup: true,
        contentsCss: window.urlCssCKEDITOR,
        extraAllowedContent: (
            'style;body;*{*};' +
            'img[src,alt,width,height,style,class];' +
            'td[class,colspan,width,height,style];' +
            'th[class,colspan,width,height,style];' +
            'tr[class,style];' +
            'h1[class,width,height,style];' +
            'h2[class,width,height,style];' +
            'h3[class,width,height,style];' +
            'div[class,width,height,style];' +
            'span[class,width,height,style];' +
            'p[class,width,height,style];' +
            'table[src,alt,width,height,style]'
        ),
        enterMode: Number(2),
        toolbar: [
            [
                'Styles', 'Bold', 'Italic', 'Underline', 'SpellChecker', 'Scayt', '-',
                'NumberedList', 'BulletedList'
            ],
            ['Table'],
            ['Link', 'Unlink'],
            ['Undo', 'Redo', '-', 'SelectAll'],
            ['Maximize']
        ]

    });
    $("input[name='anotId']").val('');
    $("input[name='anotData']").val('');
    $("[name='anotDescricao']").val('');
    $("input[name='anotTitulo']").val('');
    $("#bodyTableArquivos").empty();
    $("input[name='anotacaoArquivo']").val('');
}


function preencheModal(anot) {
    $("input[name='anotId']").val(anot.anotId);
    $("input[name='anotData']").val(anot.anotData);
    $("[name='anotDescricao']").val(anot.anotDescricao);
    $("input[name='anotTitulo']").val(anot.anotTitulo);
}

function enviaFormAnotacoes() {
    var docdiscId = window.location.pathname;
    docdiscId = docdiscId.split("/");
    docdiscId = docdiscId[4];

    var arquivos = new Array();
    $("input[name='arq[]']").each(function () {
        arquivos.push(this.value);
    });

    var dados = {
        anotId: $("input[name='anotId']").val(),
        anotData: $("input[name='anotData']").val(),
        anotDescricao: $("[name='anotDescricao']").val(),
        anotTitulo: $("input[name='anotTitulo']").val(),
        docdisc: docdiscId,
        arquivos: arquivos
    };


    if (validaForm()) {
        $.ajax({
            url: "/professor/disciplinas/save-anotacao",
            dataType: 'json',
            type: 'post',
            data: {dados: dados},
            success: function (data) {
                if (data.saved) {
                    $("#myModal").modal('hide');
                    buscaAnotacoes();
                } else {
                    alert("Erro ao salvar anotação!!");
                }
            },
            erro: function () {
                alert('Erro ao salvar anotações! Verifique sua conexão com a internet.');
            }
        });
        limpaModal();
    }
}

function validaForm() {

    var vs = new VersaShared();

    var error = false;
    var msgErro = '';

    if ($("input[name='anotData']").val().trim() == '') {
        $("input[name='anotData']").parent().addClass('has-error');
        msgErro += "É necessário informar a data!<br> ";
        error = true;
    } else {
        $("input[name='anotData']").parent().removeClass('has-error');
    }
    if ($("[name='anotDescricao']").val().trim() == '') {
        $("[name='anotDescricao']").parent().addClass('has-error');
        msgErro += "É necessário informar a descrição!<br> ";
        error = true;
    } else {
        $("[name='anotDescricao']").parent().removeClass('has-error');
    }
    if ($("input[name='anotTitulo']").val().trim() == '') {
        $("input[name='anotTitulo']").parent().addClass('has-error');
        msgErro += "É necessário informar o título!<br> ";
        error = true;
    } else {
        $("input[name='anotTitulo']").parent().removeClass('has-error');
    }

    vs.showNotificacaoWarning(msgErro);

    return !error;
}

function removeAnotacao(id) {
    if (CKEDITOR.instances.anotDescricao) {
        CKEDITOR.instances.anotDescricao.destroy();
    }

    if (confirm("Deseja realmente excluir este registro ?")) {
        var remover = new Array();
        remover.push(id);
        $.ajax({
            url: "/professor/disciplinas/remove",
            dataType: 'json',
            type: 'post',
            data: {id: remover},
            success: function (data) {
                if (data.excluido) {
                    buscaAnotacoes();
                } else {
                    alert("Erro ao deletar anotação!!");
                }
            },
            erro: function () {
                alert('Erro ao salvar anotações! Verifique sua conexão com a internet.');
            }
        });
    }
}

function selectAll(element) {
    var attr = $(element).prop('checked');
    $("input[name='anotacaoId[]']:visible").each(function () {
        if (!attr) {
            $(this).prop('checked', false);
        } else {
            $(this).prop('checked', true);
        }
    });
}

function removeSelecao() {
    if (confirm("Deseja realmente excluir anotações selecionadas ?")) {
        var remover = new Array();
        $("input[name='anotacaoId[]']:checked").each(function () {
            remover.push($(this).val());
        });

        $.ajax({
            url: "/professor/disciplinas/remove",
            dataType: 'json',
            type: 'post',
            data: {id: remover},
            success: function (data) {
                if (data.excluido) {
                    buscaAnotacoes();
                } else {
                    alert("Erro ao deletar anotação!!");
                }
            },
            erro: function () {
                alert('Erro ao salvar anotações! Verifique sua conexão com a internet.');
            }
        });

    }
}


/*
 *
 * ================= SCRIPT PARA O ANOTACOES ====================
 * */
/*
 *
 *=================Script para impressao de relatorios===========================
 */

function exiberelatorios() {
    var docdiscId = window.location.pathname;
    docdiscId = docdiscId.split("/");
    docdiscId = docdiscId[4];
    $.ajax({
        url: "/professor/disciplinas/documentos",
        dataType: 'html',
        type: 'post',
        data: {docdiscId: docdiscId},
        success: function (html) {
            $("#blocks-settings-1").empty();
            $("#blocks-settings-1").append(html);
        },
        erro: function () {
            alert('Erro ao buscar anotações! Verifique sua conexão com a internet.');
        }
    });
}

function documentos() {
    $(function () {
        $("#relFrequencia").click(function () {
            $("#relMeses").toggle();
            $("#relAnotacoes").toggle();
            $("#relAnotacoesSemestral").toggle();
            $("#relImprimir").toggle();
            $("#relNotas").toggle();
            var val = $(this).val();
            $("form[name='modeloRelatorio']").attr('action', val);
        });
        $("#relAnotacoes").click(function () {
            $("#relMeses").toggle();
            $("#relFrequencia").toggle();
            $("#relNotas").toggle();
            $("#relAnotacoesSemestral").toggle();
            $("#relImprimir").toggle();
            var val = $(this).val();
            $("form[name='modeloRelatorio']").attr('action', val);

        });
        $("#relAnotacoesSemestral").click(function () {
            $("#relNotas").toggle();
            $("#relFrequencia").toggle();
            $("#relAnotacoes").toggle();
            $("#relImprimir").toggle();
            var val = $(this).val();
            $("form[name='modeloRelatorio']").attr('action', val);

        });
        $("#relNotas").click(function () {
            $("#relAnotacoes").toggle();
            $("#relFrequencia").toggle();
            $("#relImprimir").toggle();
            $("#relMeses").hide();
            $("#relAnotacoesSemestral").toggle();
            var val = $(this).val();
            $("form[name='modeloRelatorio']").attr('action', val);
        });
    });
}