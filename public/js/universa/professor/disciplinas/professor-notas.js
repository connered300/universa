(function ($, window, document) {
    'use strict';

    var ProfessorNotas = function () {
        VersaShared.call(this)
        var pub = this;
        var priv = {
            options: {
                url: {
                    salvarNotas: '',
                    buscarNotas: ''
                },
                datatable: {
                    notas: null,
                    diarios: null,
                    diario: null
                },
                data: {
                    configCurso: [],
                    notasAlunos: [],
                    diarios: [],
                    notas: [],
                    dataAtual: '',
                    docdiscId: '',
                    alunosForaDaIntegradora: [],
                    lancamentoBloqueado: 0,
                    provaIntegradoraFoiFechada: 0,
                    turmaNaoFazIntegradora: 0,
                    colunasTabelaNotas: []
                }
            }
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];

            priv.options = $.extend(priv.options, opts);
        };

        priv.atualizaListagemEtapas = function () {
            priv.options.data.colunasTabelaNotas.push(
                {name: "pes_nome", targets: 0, data: "pes_nome_situacao", title: 'Aluno', 'width': '40%'}
            );

            var widthNotas = (60 / (priv.options.data.diarios.length + 1)) + '%';
            var sizeCol = Math.round(((100 / priv.options.data.diarios.length) * 12) / 100);
            sizeCol = 'col-xs-' + sizeCol;
            $('#painel-informacoes-diario').html('');

            $.each(priv.options.data.diarios, function (i, item) {
                var dataInicio = new Date(item['etapa_data_inicio']);
                var dataFim = new Date(item['etapa_data_fim']);

                item['data_inicio_yymmdd'] = $.formatDateTime("yymmdd", dataInicio);
                item['data_inicio_ddmmyy'] = $.formatDateTime("dd/mm", dataInicio);
                item['data_fim_yymmdd'] = $.formatDateTime("yymmdd", dataFim);
                item['data_fim_ddmmyy'] = $.formatDateTime("dd/mm", dataFim);

                if (item['etapadiario_extensao_data']) {
                    var dataExtensao = new Date(item['etapadiario_extensao_data']);
                    item['extensao_data_yymmdd'] = $.formatDateTime("yymmdd", dataExtensao);
                    item['extensao_data_ddmmyy'] = $.formatDateTime("dd/mm", dataExtensao);
                } else {
                    item['extensao_data_yymmdd'] = '-';
                    item['extensao_data_ddmmyy'] = '-';
                }

                priv.options.data.colunasTabelaNotas.push({
                    name: "nota_diario_" + item['diario_id'],
                    targets: priv.options.data.colunasTabelaNotas.length,
                    data: "nota_diario_" + item['diario_id'] + '_campo',
                    orderable: false,
                    title: item['etapa_descricao'],
                    width: widthNotas
                });

                $('#painel-informacoes-diario').append(
                    '<div class="form-group ' + sizeCol + '">' +
                    '<b>' + item['etapa_descricao'] + ':</b>' +
                    '<p>' +
                    item['etapa_nota_maxima'] + ' pts.<br>' +
                    item['data_inicio_ddmmyy'] + ' - ' +
                    (item['extensao_data_ddmmyy'] == '-' ? item['data_fim_ddmmyy'] : item['extensao_data_ddmmyy']) +
                    '</p>' +
                    '</div>'
                );

            });

            priv.options.data.colunasTabelaNotas.push({
                name: "nota_total",
                targets: priv.options.data.colunasTabelaNotas.length,
                data: "nota_total_formatado",
                orderable: false,
                width: widthNotas,
                title: 'Total'
            });

            $('#painel-informacoes-diario').show();
        };

        priv.associaNotasAlunoDiario = function () {
            $.each(priv.options.data.alunos, function (a, aluno) {
                $.each(priv.options.data.notas, function (d, diario) {
                    if (diario['alunodisc_id'] == aluno['alunodisc_id']) {
                        aluno['diarios'] = aluno['diarios'] || {};
                        aluno['diarios'][diario['diario_id']] = diario;
                    }
                });

                if (priv.options.data.alunosForaDaIntegradora.indexOf(aluno['alunodisc_id']) != -1) {
                    aluno['alunoFazIntegradora'] = true;
                } else {
                    aluno['alunoFazIntegradora'] = false;
                }

                aluno['pes_nome_situacao'] = aluno['pes_nome'];

                if (aluno['matsit_descricao'] != 'Matriculado') {
                    aluno['pes_nome_situacao'] += ' <b>' + aluno['matsit_descricao'] + '</b>';
                }

                priv.options.data.notasAlunosDiarios.push(aluno);
            });
        };

        priv.criaCamposNotas = function () {
            var ultimoDiario = priv.options.data.diarios[Object.keys(priv.options.data.diarios).length - 1];
            var notaMin = ultimoDiario['cursoconfig_nota_max'] || 70;

            $(priv.options.data.diarios).each(function (posDiario, diario) {
                var dataAtual = parseInt(priv.options.data.dataAtual);
                var diarioId = diario['diario_id'];
                var notaMaxima = diario['etapa_nota_maxima'];
                var dataInicio = parseInt(diario['data_inicio_yymmdd']);
                var dataFim = parseInt(diario['data_fim_yymmdd']);

                if (diario['extensao_data_yymmdd'] != '-') {
                    dataFim = parseInt(diario['extensao_data_yymmdd']);
                }

                var liberado = (dataAtual >= dataInicio && dataAtual <= dataFim);

                $(priv.options.data.notasAlunosDiarios).each(function (posAluno, aluno) {
                    var alunodiscId = aluno['alunodisc_id'];
                    var nota = (aluno['diarios'][diarioId] ? (aluno['diarios'][diarioId]['alunoetapa_nota'] || '') : '');
                    var notaTotal = parseFloat(aluno['nota_total'] || 0) + parseFloat(nota || 0);
                    var bloqueioAluno = !liberado;
                    notaTotal = priv.roundNumber(notaTotal, 2);

                    if (priv.options.data.lancamentoBloqueado) {
                        bloqueioAluno = false;
                    } else if (ultimoDiario['diario_id'] == diarioId) {
                        bloqueioAluno = true;

                        if (liberado) {
                            if (priv.options.data.turmaNaoFazIntegradora) {
                                bloqueioAluno = false;
                            }

                            if (aluno['alunoFazIntegradora'] && priv.options.data.provaIntegradoraFoiFechada == 1) {
                                bloqueioAluno = false;
                            }

                            if (aluno['matsit_descricao'] != 'Matriculado') {
                                bloqueioAluno = false;
                            }
                        }
                    }
                    var etapa = 1;
                    var arrCursoConfig = priv.options.data.configCurso;

                    if (arrCursoConfig['regraCalculoNota'] == arrCursoConfig['confCalcunoNotaInicialMedia']) {
                        etapa = arrCursoConfig['etapas'];
                    }

                    aluno['nota_total'] = notaTotal;

                    var classNotaTotal = (notaTotal >= notaMin ? 'text-info' : 'text-danger');

                    aluno['nota_total_formatado'] = (
                        '<div class="text-center"><p class="form-control-static ' + classNotaTotal + '"><b>' +
                        (pub.formatNumberUS((notaTotal / etapa), 1)) + '</b></p></div>'
                    );

                    aluno["nota_diario_" + diarioId] = nota;
                    aluno["nota_diario_" + diarioId + '_campo'] = '\
                    <div class="form-group">\
                        <input class="form-control input-notas input-sm" \
                        name="notas[' + diarioId + '][' + alunodiscId + ']"\
                        data-notaMaxima="' + notaMaxima + '" style="width: 100% !important"\
                        type="text" placeholder="Nota" maxlength="5"\
                        ' + (bloqueioAluno ? 'disabled' : '') + ' value="' + nota + '" >\
                    </div>';
                });
            });
        };

        priv.roundNumber = function (number, dec, dsep, tsep) {
            if (isNaN(number) || number == null) {
                return '';
            }

            dec = dec || 2;
            dsep = dsep || '.';
            tsep = tsep || '';

            var newNumber = parseFloat(number).toFixed(~~dec);

            var parts = newNumber.split('.');
            var fnums = parts[0];
            var decimals = parts[1] ? (dsep || '.') + parts[1] : '';

            return fnums.replace(/(\d)(?=(?:\d{3})+$)/g, '$1' + tsep) + decimals;
        };

        priv.atualizaListagemNotas = function () {
            if (priv.options.datatable.notas) {
                priv.options.datatable.notas.api().destroy();
            }

            $('#table-notas').empty();

            priv.options.datatable.notas = $('#table-notas').dataTable({
                processing: true,
                columns: priv.options.data.colunasTabelaNotas,
                data: priv.options.data.notasAlunosDiarios,
                "dom": "rf" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-12'p>>",
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                "iDisplayLength": 10,
                "bLengthChange": false,
                "bPaginate": false

            });
            $('#painel-notas').show();
        };

        priv.atualizaInformacoesNotas = function (mensagem) {
            mensagem = mensagem || '';
            $('#mensagem-notas').html('Atualizando informações de nota. Aguarde...');
            $('#btn-salvar-notas').prop('disabled', true);
            $('#painel-notas').hide();
            $('#painel-informacoes-diario').hide();

            $.ajax({
                url: priv.options.url.buscarNotas,
                type: 'POST',
                data: {docdiscId: priv.options.data.docdiscId},
                dataType: 'json',
                success: function (data) {
                    priv.options.data.diarios = data['diarios'] || [];
                    priv.options.data.alunos = data['alunos'] || [];
                    priv.options.data.notas = data['notas'] || [];
                    priv.options.data.notasAlunosDiarios = [];
                    priv.options.data.dataAtual = data['dataAtual'] || '';
                    priv.options.data.turmaNaoFazIntegradora = data['turmaNaoFazIntegradora'] || false;
                    priv.options.data.lancamentoBloqueado = data['lancamentoBloqueado'] || false;
                    priv.options.data.alunosForaDaIntegradora = data['alunosForaDaIntegradora'] || [];
                    priv.options.data.provaIntegradoraFoiFechada = data['provaIntegradoraFoiFechada'] || false;
                    priv.options.data.colunasTabelaNotas = [];

                    $('#mensagem-notas').html(mensagem);

                    if (priv.options.data.alunos.length == 0) {
                        $('#mensagem-notas').append('<p>A docência não possui alunos registrados.</p>');
                        return;
                    }

                    if (priv.options.data.lancamentoBloqueado) {
                        $('#mensagem-notas').append(
                            '<p>O lançamento de notas da docência está bloqueado.' +
                            'Caso seja necessário, entre em contato com a coordenação.</p>'
                        );
                        return;
                    }
                    priv.atualizaListagemEtapas();
                    priv.associaNotasAlunoDiario();
                    priv.criaCamposNotas();
                    priv.atualizaListagemNotas();
                    priv.criaAcaoNotas();
                }
            });
        };
        priv.criaAcaoNotas = function () {
            var arrCursoConfig = priv.options.data.configCurso;
            var notaFracionada = parseInt(arrCursoConfig['cursoconfigNotaFracionada']);

            $('.input-notas').inputmask({
                showMaskOnHover: false,
                showMaskOnFocus: false,
                clearMaskOnLostFocus: true,
                digits: (notaFracionada > 0 ? notaFracionada : 0),
                alias: 'currency', groupSeparator: "", radixPoint: ".",
                placeholder: "", prefix: ""
            });

            $('.input-notas').on('change paste', function () {
                var $this = $(this);
                var notaMaxima = $this.data('notamaxima');
                var nota = $this.val();

                if (nota > notaMaxima) {
                    alert('A nota "' + nota + '" é maior que a nota máxima permitida para a etapa!');
                    $this.val('').focus();
                    $this.parent().addClass('has-error');
                } else {
                    $this.parent().removeClass('has-error');
                }
            });

            $("#btn-salvar-notas").prop('disabled', priv.options.data.lancamentoBloqueado);
        };

        priv.iniciaAcaoSalvarNotas = function () {
            $("#btn-salvar-notas").click(function () {
                $('#preloader').fadeIn();
                $("input[type='search']").val('').trigger('keyup');
                $('.input-notas').prop('disabled', false);
                var dataArray = $('#form-notas').serializeJSON();
                dataArray.docdiscId = priv.options.data.docdiscId;

                $.ajax({
                    url: priv.options.url.salvarNotas,
                    dataType: 'json',
                    type: 'post',
                    data: dataArray,
                    success: function (data) {
                        var mensagem = '';
                        if (data.erro) {
                            mensagem = '<p>Ocorreu um erro ao salvar notas.<br>' + data.erro + '</p>';
                            alert('Ocorreu um erro ao salvar notas.\n' + data.erro);
                        } else {
                            mensagem = '<p>Notas salvas com sucesso!</p>';
                        }

                        priv.atualizaInformacoesNotas(mensagem);

                        $('#preloader').fadeOut();
                    },
                    erro: function () {
                        alert('Erro ao salvar notas! Verifique sua conexão com a internet.');
                    }
                });
            });
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.iniciaAcaoSalvarNotas();
            $('#buscaNotas').click(priv.atualizaInformacoesNotas);
        };
    };

    $.professorNotas = function (params) {
        params = params || [];

        var obj = $(window).data("universa.professor.notas");

        if (!obj) {
            obj = new ProfessorNotas();
            obj.run(params);
            $(window).data('universa.professor.notas', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);
