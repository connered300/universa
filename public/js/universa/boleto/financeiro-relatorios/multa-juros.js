$(document).ready(function(){
    $(document).on("focus","#data-inicio,#data-fim", function(){
        $('#data-inicio, #data-fim')
            .datepicker({prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'})
            .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});
    });

});