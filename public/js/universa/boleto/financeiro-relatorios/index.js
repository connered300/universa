/**
 * Created by versa on 17/09/15.
 */
$(document).ready(function () {
    var urlturma = '';
    var urlMultasJuros = '';

    $("#taxas").click(function () {
        formtaxas();
    });

    $("#titulos").click(function () {
        formmensalidades();
    });

    $("#descontos").click(function () {
        formtdescontos();
    });
    $("#cheques").click(function () {
        formcheques();
    });
    $("#comprovante-renda").click(function () {
        formcomprovanteRenda();
    });
    $("#dependencia-adaptacao").click(function() {
        formDependenciaAdaptacao();
    });
    $("#multas-juros").click(function(){
        formMultasJuros();
    });
    $(document).on("focus","#dataInicial,#dataFinal", function(){
        $('#dataInicial, #dataFinal')
            .datepicker({prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'})
            .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});
    });
 /*-------------------------------------------SELECTS E MASCARAS---------------------------------------------------------*/
    $(document).on("focus",".js-example-basic-multiple", function(){
        //$(".js-example-basic-multiple").select2();
    });
 /*-------------------------------CHAMADA DOS FORMULARIOS PARA OS VARIADOS TIPOS DE RELATÓRIOS---------------------------*/

    function formtaxas(){
        $.ajax({
            url: "/boleto-bancario/financeiro-relatorios/taxas",
            dataType: 'html',
            success: function (html) {
                $("#blocks-profile-taxas").empty();
                $("#blocks-profile-taxas").append(html);
            },
            erro: function () {
                alert('Erro ao buscar anotações! Verifique sua conexão com a internet.');
            }
        });
    }

    function formmensalidades(){
        $.ajax({
            url: "/boleto-bancario/financeiro-relatorios/mensalidades",
            dataType: 'html',
            success: function (html) {
                $("#blocks-profile-titulos").empty();
                $("#blocks-profile-titulos").append(html);
                buscaTurmas();
            },
            erro: function () {
                alert('Erro ao buscar anotações! Verifique sua conexão com a internet.');
            }
        });
    }

    function formtdescontos(){
        $.ajax({
            url: "/boleto-bancario/financeiro-relatorios/descontos",
            dataType: 'html',
            success: function (html) {
                $("#blocks-profile-descontos").empty();
                $("#blocks-profile-descontos").append(html);
            },
            erro: function () {
                alert('Erro ao buscar anotações! Verifique sua conexão com a internet.');
            }
        });
    }

    function formcheques(){
        $.ajax({
            url: "/boleto-bancario/financeiro-relatorios/cheques",
            dataType: 'html',
            success: function (html) {
                $("#blocks-profile-cheques").empty();
                $("#blocks-profile-cheques").append(html);
            },
            erro: function () {
                alert('Erro ao buscar anotações! Verifique sua conexão com a internet.');
            }
        });
    }

    function formcomprovanteRenda(){
        $.ajax({
            url: "/boleto-bancario/financeiro-relatorios/comprovante-renda",
            dataType: 'html',
            success: function (html) {
                $("#blocks-profile-cheques").empty();
                $("#blocks-profile-cheques").append(html);
            },
            erro: function () {
                alert('Erro ao buscar anotações! Verifique sua conexão com a internet.');
            }
        });
    }
    function formDependenciaAdaptacao(){
        $.ajax({
            url: "/boleto-bancario/financeiro-relatorios/dependencia-adaptacao",
            dataType: 'html',
            success: function (html) {
                //console.log(html);
                $("#blocks-profile-dependencia-adaptacoes").empty();
                $("#blocks-profile-dependencia-adaptacoes").append(html);
                buscaTurmas();
            },
            erro: function () {
                alert('Erro ao buscar anotações! Verifique sua conexão com a internet.');
            }
        });
    }
    function formMultasJuros(){
        $.ajax({
            url: "/boleto-bancario/financeiro-relatorios/multas-juros",
            dataType: 'html',
            success: function(html){
                $("#blocks-profile-multas-juros").empty();
                $("#blocks-profile-multas-juros").append(html);
                formaspagamento();
            },
            erro: function(){
                alert('Erro ao buscar view')
            }
        });
    }

    function buscaTurmas(){
        $.ajax({
            url: window.urlBuscaTurmas,
            dataType: 'json',
            type: 'POST',
            data:{turmasPeriodoLetivoAtual:true},
            success: function (parametros) {
                $("#turmas").select2({
                    data: function () {
                        var data = $.map(parametros['turmas'] || [], function (item) {
                            item['id'] = item['turma_id'];
                            item['text'] = item['per_nome']+" / "+item['turma_nome'];

                            return item
                        });
                        return {results: data};
                    },
                    tags:true
                });
            },
            erro: function () {
                alert('Erro ao buscar anotações! Verifique sua conexão com a internet.');
            }
        });
    }
/*-------------TRATAMENTO DOS FORMULARIOS DO CADA TIPO DE RELATÓRIO-------------------------*/

    $("input[id = 'titulos']").click(function() {
        $("#titulos-relatorio").toggle();
        $("#taxas-relatorio").hide();
        $("#relatorios").hide();
        $("#title").hide();


    });
    $("input[id = 'taxas']").click(function() {
        $("#taxas-relatorio").toggle();
        $("#titulos-relatorio").hide();
        $("#relatorios").hide();
        $("#cheques").hide();



    });

    $("input[id = 'alunos']").click(function() {
        $("#taxas-relatorio").hide();
        $("#titulos-relatorio").hide();
        $("#relatorios").hide();
        $("#cheques").hide();


    });

    $("input[id = 'descontos']").click(function() {
        $("#taxas-relatorio").hide();
        $("#titulos-relatorio").hide();
        $("#relatorios").hide();
        $("#cheques").hide();
    });

    $("input[id = 'financiamentos']").click(function() {
        $("#taxas-relatorio").hide();
        $("#titulos-relatorio").hide();
        $("#relatorios").hide();
        $("#cheques").hide();

    });

    $("input[id = 'cheques']").click(function() {
        $("#taxas-relatorio").hide();
        $("#titulos-relatorio").hide();
        $("#relatorios").hide();

    });

    $("input[id = 'comprovante-renda']").click(function() {
        $("#taxas-relatorio").hide();
        $("#titulos-relatorio").hide();
        $("#relatorios").hide();
        $("#cheques").hide();

    });

    $("input[id = 'dependencia-adaptacao']").click(function() {
        $("#taxas-relatorio").hide();
        $("#titulos-relatorio").hide();
        $("#relatorios").hide();
        $("#cheques").hide();

    });

    $("input[id = 'multas-juros']").click(function() {
        $("#dependencia-adaptacao").hide();
        $("#taxas-relatorio").hide();
        $("#titulos-relatorio").hide();
        $("#relatorios").hide();
        $("#cheques").hide();

    });
});
function hideFiltros(){
    $("input[id = 'pagamento-integral']").click(function() {
        val = $(this).is(':checked');
        if(val == true)
            $('.radios').addClass('hidden');
    });

    $("input[id = 'analitico']").click(function() {
        val = $(this).is(':checked');
        if(val == true)
            $('.radios').removeClass('hidden');
    });

    $("input[id = 'sintetico']").click(function() {
        val = $(this).is(':checked');
        if(val == true)
            $('.radios').removeClass('hidden');
    });
}

function formaspagamento(){
    hideFiltros();
    $("input[id = 'pagos']").click(function() {
        $("#forma-pagamentos").show();
        //$("form[name='modeloRelatorio']").attr('action', val);
    });

    $("input[id = 'abertos']").click(function() {
        $("#forma-pagamentos").hide();
        //$("form[name='modeloRelatorio']").attr('action', val);
    });

    $("input[id='todos']").click(function() {
        $("#forma-pagamentos").hide();
        //$("form[name='modeloRelatorio']").attr('action', val);
    });
}
/*------------------------------VALIDACOES DE DATA---------------------------------------------------------------------------------------------*/
function validaDatas(){
    $(document).on("blur","input[id='data-fim']", function(){
        var data_fim    = $("input[id = 'data-fim']").val();
        var data_now    = $("input[id = 'date-now']").val();

        var Compara01 = parseInt(data_fim.split("/")[2].toString() + data_fim.split("/")[1].toString() + data_fim.split("/")[0].toString());
        var Compara02 = parseInt( data_now.split("/")[2].toString() +  data_now.split("/")[1].toString() +  data_now.split("/")[0].toString());
        if(Compara01 > Compara02 ){
            $("input[id = 'data-fim']").val("");
            $("input[id = 'data-fim']").focus();
            alert("A data não pode ser maior que a atual")
        }
    })

    $("input[id = 'data-inicio']").blur( function(){
        var data_inicio    = $("input[id = 'data-inicio']").val();
        var data_now    = $("input[id = 'date-now']").val();

        var Compara01 = parseInt(data_inicio.split("/")[2].toString() + data_inicio.split("/")[1].toString() + data_inicio.split("/")[0].toString());
        var Compara02 = parseInt( data_now.split("/")[2].toString() +  data_now.split("/")[1].toString() +  data_now.split("/")[0].toString());

        if(Compara01 > Compara02 ){
            $("input[id = 'data-inicio']").val("");
            $("input[id = 'date-inicio']").focus();
            alert("A data não pode ser maior que a atual")
        }
    })

    $("input[id = 'data-fim']").blur( function(){
        var data_inicio    = $("input[id = 'data-inicio']").val();
        var data_fim = $("input[id = 'data-fim']").val();

        var Compara01 = parseInt(data_inicio.split("/")[2].toString() + data_inicio.split("/")[1].toString() + data_inicio.split("/")[0].toString());
        var Compara02 = parseInt( data_fim.split("/")[2].toString() +  data_fim.split("/")[1].toString() +  data_fim.split("/")[0].toString());

        if(Compara01 > Compara02 ){
            $("input[id = 'data-fim']").val("");
            $("input[id = 'data-fim']").focus();
            alert("Intervalo de Data Invalido!")
        }
    })
}

/*----------------------------FUNCOES GENERICAS--------------------------------------------------------------------------------------*/

function buscaTurmasTurno(){
    $("select[name = 'turnos']").change(function () {
        var options = '';
        jQuery.ajax({
            url: "/boleto-bancario/financeiro-relatorios/busca-turmas",
            dataType: 'json',
            type: 'post',
            async: true,
            data: {turno: jQuery("select[name = 'turnos']").val()},
            success: function (data) {
                if (data !== false) {
                    if (data !== false) {
                        var i;
                        options = '<option value="Todos">' + 'Todos' + '</option>';
                        for(i = 0 ;i < data.dados.length; i++){
                            options += '<option value="' + data.dados[i]['turma_nome'] + '">' + data.dados[i]['turma_nome'] + '</option>';                            $("select[name='turmas']").val(data.dados[i]['turma_nome']);
                        }
                        $("#turmas").html(options);
                        $("#coluna-turmas").show();
                        $("#linha-turmas").show();


                    }
                }
            },
            erro: function () {
                alert('Opa2');
            }
        });

    });
}

/*-----------------------------------FUNCOES DE VALIDACAO DA VIEW DE DESCONTO----------------------------------------*/

function validaCampos(){
    $("#modelo-relatorio-sintetico").click(function () {
            $("#modelo-analitico").hide();
            $("#filtros-sintetico").show();
            $("#filtro-descontos").hide();
    });
    $("#modelo-relatorio-analitico").click(function () {
        $("#modelo-analitico").show();
        $("#filtros-sintetico").hide();

    });
    $("#periodo-turmas-alunos").click(function () {
        $("#filtro-descontos").show();
    });
    $("#periodo-turmas-turma").click(function () {
        $("#filtro-descontos").hide();

    });

}
/*-----------------------------------FUNCOES DE VALIDACAO DA VIEW DE COMPROVANTE DE RENDA----------------------------------------*/


function modeloComprovante(){
    $("#renda-turma").click(function(){
        $("#table-crenda-turmas").show();
        $("#table-crenda-aluno").hide();
        $("#linha-renda-aluno").remove();
    });

    $("input[id = 'ano-base']").blur( function(){
        var ano_base    = $("input[id = 'ano-base']").val();
        var data_now    = $("input[id = 'date-now']").val();

        var ano_atual = parseInt(data_now.split("/")[2].toString());

        if(ano_base < 2015 ){
            $("input[id = 'data-fim']").val("");
            $("input[id = 'data-fim']").focus();
            alert("Digite um ano maior que 2014")
        }else if(ano_base > ano_atual){
            $("input[id = 'data-fim']").val("");
            $("input[id = 'data-fim']").focus();
            alert("O ano nao pode ser maior que o atual")
        }
    })

    $("input[id = 'renda-matricula']").blur( function(){
        var renda_matricula    = $("input[id = 'renda-matricula']").val();

        if(renda_matricula < 2176   ){
            $("input[id = 'data-fim']").val("");
            $("input[id = 'data-fim']").focus();
            alert("Digite um numero de Matricula valido")
        }
    })

}
