$(document).ready(function(){
    $("input, textarea, select").bind("keypress", function(e) {
        var tecla = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;
        if (tecla == 13) {
            var input = $(this).parent().parent().next().find("input:visible");
            if( input.html() != undefined ){
                input.focus();
            } else {
                count = 1;
                input = $(this).parent().parent();
                while(count < 10){
                    for(var i = 0; i < count; i++ ){
                        input = input.parent();
                    }
                    if(input.next().find("input:visible").html() != undefined ){
                        input = input.next().find("input:visible");
                        break;
                    } else if(input.next().find("select:visible").html() != undefined ){
                        input = input.next().find("select:visible");
                        break;
                    } else if(input.next().find("textarea:visible").html() != undefined ){
                        input = input.next().find("textarea:visible");
                        break;
                    }
                    count = count+1;

                }
                if(input.eq(0).html() != undefined){
                    input.eq(0).focus();
                } else {
                    return true;
                }
            }
            return false;
        }
    });

    var diaVencimento = $("#descontoDiaLimite");
    if( diaVencimento.val() == ""){
        diaVencimento.parent().parent().css('display','none');
    }
    var desctipoValormax = $("#desctipoValormax");
    if( desctipoValormax.val() == ""){
        desctipoValormax.parent().parent().css('display','none');
        $("#descontoValor").parent().parent().css('display','none');
    } else {
        $("#desctipoPercmax").parent().parent().css('display','block');
        $("#descontoPercentual").parent().parent().css('display','block');
    }
    //$("#desctipo").attr('disabled','disabled');
    //$("#infoEdit").append("<input name='desctipo' type='hidden' value='"+$("#desctipo").val()+"'>");

    var perMax = $("#desctipoPercmax");
    if( perMax.val() != ""){
        $("#descontoPercentual").attr({'max':perMax.val(), 'min':'1', 'type': 'number', 'step' : 0.01});
    }
    var valMax= $("#desctipoValormax");
    if( valMax.val() != "" ){
        $("#descontoValor").attr({'max':valMax.val(), 'min':'1', 'type': 'number'});
    }


    var alunoper = $("input[name='alunoper']").val();
    $.ajax({
        async: true,
        type: "POST",
        url: "/matricula/acadperiodo-aluno/resumo-aluno/"+alunoper,
        data: {
            matricula: alunoper,
        },
        success: function( data ){
            $("#dadosAluno").html(data);
        },
        error: function( a ){
            alert("Erro ao conectar ao servidor verifique sua conexão a internet!")
        }

    });

});

function buscaDadosDesconto(element){
    $.ajax({
        async: false,

        type: "POST",
        url: "/boleto-bancario/financeiro-desconto/busca-dados-desconto-tipo",
        data: {
            id : element.value
        },
        success: function( data ) {
            limpaCamposDesconto();
            if(data.result.desctipoDescricaoLimitaVencimento != "Não"){
                $("#descontoDiaLimite").parent().parent().css('display','block');
                $("#descontoDiaLimite").removeAttr('readonly').attr({'type':'number','max':'30', 'min':'1'});
            } else {
                $("#descontoDiaLimite").parent().parent().css('display','none');
            }
            if(data.result.desctipoPercmax != null){
                $("#desctipoPercmax").parent().parent().css('display','block');
                $("#descontoPercentual").parent().parent().css('display','block');

                $("#desctipoPercmax").val(data.result.desctipoPercmax);
                $("#descontoPercentual").attr({'max':data.result.desctipoPercmax, 'min':'1', 'type': 'number', 'step' : 0.01});
            } else {
                $("#desctipoPercmax").parent().parent().css('display','none');
                $("#descontoPercentual").parent().parent().css('display','none');
            }
            if(data.result.desctipoValormax != null){
                $("#desctipoValormax").parent().parent().css('display','block');
                $("#descontoValor").parent().parent().css('display','block');

                $("#desctipoValormax").val(data.result.desctipoValormax);
                $("#descontoValor").attr({'max':data.result.desctipoValormax, 'min':'1', 'type': 'number'});
            } else {
                $("#desctipoValormax").parent().parent().css('display','none');
                $("#descontoValor").parent().parent().css('display','none');
            }

        },
        error: function( a ){
            alert("Erro ao conectar ao servidor verifique sua conexão a internet!");
        }
    });

}

function limpaCamposDesconto() {
    $("#desctipoValormax:visible").val("");
    $("#desctipoPercmax:visible").val("");
    $("#descontoValor:visible").val("");
    $("#descontoPercentual:visible").val("");
    $("#descontoDiaLimite:visible").val("");
}

function disabledCheckBoxMensalidades(elemento){
    $(elemento).attr("checked", false);
    alert("Esta mensalidade ja foi paga e por isso não se pode vincular um desconto a mesma!");
}

function enableCheckBoxMensalidades(elemento){
    $(elemento).prop("checked", true);
    alert("Esta mensalidade ja foi paga e por isso não se pode desvincular o desconto!");
}


function validaFormFinanceiroDesconto(){
    if( $("#desctipo").val() == "NULL" ) {
        alert("Favor preencher as informações corretamente!");
        return false;
    }

    var flag = 0;
    $("input[name='descontoMensalidades[]']").each(function(){
        if( $(this).prop('checked') == true ){
            flag += 1;
        }
    });
    if( flag == 0 ){
        alert("Favor selecionar ao mesnos uma parcela em que o aluno tera desconto!");
        return false;
    }
    if( $("#descontoPercentual:visible").val() == "" ){
        alert("Favor inserir o percentual do desconto!");
        return false;
    }
    if( $("#descontoValor:visible").val() == "" ){
        alert("Favor inserir o valor do desconto!");
        return false;
    }

    var confirme = window.confirm("Desaja realmente realizar alterações neste desconto/financiamento para o aluno: "+$("input[name='pesNome']").val()+"?");
    if( confirme == false){
        return false;
    }
}

