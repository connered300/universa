/**
 * Created by lucasrafael on 16/09/15.
 */

var ctrlPrecionado = false;
var altPrecionado = false;
$(document).keydown(function(e) {
    switch(e.which) {
        case 37: // left
            if(ctrlPrecionado){
                var tabAtiv = $("#myTab1 li.active");
                var contentAtv = $($("#myTab1 li.active a").attr("href"));

                tabAtiv.removeClass("active");
                contentAtv.removeClass("active in");
                if(tabAtiv.prev().html() != undefined){
                    tabAtiv.prev().addClass("active");
                    contentAtv.prev().addClass("active in");

                } else {
                    $("#myTab1").find('li').eq(4).addClass("active");
                    $("myTabContent1").eq(4).addClass("active in");
                }
            }
            break;

        case 38: // up
            var trAtiva = $("table tr[data-ativo^='true']:visible");
            if(trAtiva.prev().html() != undefined){
                e.preventDefault();
                trAtiva.prev().attr("data-ativo","true");
                trAtiva.css("background-color","#fff");
                trAtiva.prev().css("background-color","#ECE5E5");
                trAtiva.attr("data-ativo","false");
            }

            break;

        case 39: // right
            if(ctrlPrecionado){
                var tabAtiv = $("#myTab1 li.active");
                var contentAtv = $($("#myTab1 li.active a").attr("href"));

                tabAtiv.removeClass("active");
                contentAtv.removeClass("active");
                if(tabAtiv.next().html() != undefined){
                    tabAtiv.next().addClass("active");
                    contentAtv.next().addClass("active in");
                } else {
                    $("#myTab1").find('li').eq(0).addClass("active");
                    $("myTabContent1").eq(0).addClass("active in");
                }
            }
            break;

        case 40: // down
            var trAtiva = $("table tr[data-ativo^='true']:visible");


            if( trAtiva.next().html() != undefined ){
                e.preventDefault();
                trAtiva.next().attr("data-ativo","true");
                trAtiva.prev().css("background-color","#fff");
                trAtiva.css("background-color","#ECE5E5");
                trAtiva.attr("data-ativo","false");
            } else if( trAtiva.prev().html() != undefined ) {
                e.preventDefault();
                trAtiva.prev().css("background-color","#fff");
                trAtiva.css("background-color","#ECE5E5");
            }

            break;

        case 13: // enter
            var trAtiva = $("table tr[data-ativo^='true']:visible");
            if( trAtiva.html() != undefined ){
                $($("#myTab1 li.active a").attr("href")).children().eq(0).hide();
                $($("#myTab1 li.active a").attr("href")).children().eq(1).show();
            }

            break;

        case 8: // backspace
            if(ctrlPrecionado) {
                $($("#myTab1 li.active a").attr("href")).children().eq(1).hide();
                $($("#myTab1 li.active a").attr("href")).children().eq(0).show();
            }
            break;

        case 80: // p
            if(altPrecionado) {
                $("#baixaModal").modal();
                $("#baixaModal").find("input[name^='aluno']").focus();

                $('.modal').on('shown.bs.modal', function() {
                    $(this).find('[autofocus]').focus();
                });

            }
            break;

        case 17: // ctrl
            ctrlPrecionado = true;

        case 18: // alt
            altPrecionado = true;

            break;

        default: return; // exit this handler for other keys
    }

    //e.preventDefault(); // prevent the default action (scroll / move caret)
});

$(document).keyup(function(e){
    if(e.which == 17) {
        ctrlPrecionado = false;
    }
    if(e.which == 18) {
        altPrecionado = false;
    }
});
