var pagamentoLote = (function ($, window, document) {
    "use strict";

    var tableAlunos;

    var today;

    var init = function () {
        tableAlunos = tableAlunosInit();

        today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        var today = dd + '/' + mm + '/' + yyyy;



        $('.date').mask('11/11/1111');
        $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
        $('.time').mask('00:00:00');
        $('.date_time').mask('00/00/0000 00:00:00');
        $('.cep').mask('00000-000');
        $('.phone').mask('0000-0000');
        $('.phone_with_ddd').mask('(00) 0000-0000');
        $('.phone_us').mask('(000) 000-0000');
        $('.mixed').mask('AAA 000-S0S');
        $('.cpf').mask('000.000.000-00', {reverse: true});
        $('.money').mask('000.000.000.000.000,00', {reverse: true});


        $(".moeda").maskMoney();


        $(document).on("click", "#tableTitulos tbody tr", function () {
            $("#titulosAlunoModal").modal();

        });


        //quebra galho

        var alunoper = 24024//$("input[name='alunoper']").val();
        $.ajax({
            async: true,
            type: "POST",
            url: "/matricula/acadperiodo-aluno/resumo-aluno/"+alunoper,
            data: {
                matricula: alunoper,
            },
            success: function( data ){
                $("#dadosAluno").html(data);
            },
            error: function( a ){
                alert("Erro ao conectar ao servidor verifique sua conexão a internet!")
            }

        });


        $(document).on("click", "#listaAlunos tbody tr", function () {
            var position = tableAlunos.fnGetPosition(this);
            var data = tableAlunos.fnGetData(position);
            $("#titulosAlunoModal").modal();
            //$("#turma").select2('val', data.turma_id);
            //buscaDisciplinasDocentesPorTurma();

        });

        //trata campo search
        $("input[type^='search']").focus();
        var aluno = $("input[name^='alunocursoId']").val();
        if(aluno != ""){
            $("input[type^='search']").val(aluno).keyup();
        }
        $("tr").on('click',  function(){
            var href = $(this).children().find("a").attr('href');
            if(href != undefined){
                window.location.replace(href);
            }
        });

        $(window).load(function(){
            $("input[type^='search']").keyup();
        });

        // para o correto funcionamento do select no modal
        $.fn.modal.Constructor.prototype.enforceFocus = function () {
            var that = this;
            $(document).on('focusin.modal', function (e) {
                if ($(e.target).hasClass('select2-input')) {
                    return true;
                }

                if (that.$element[0] !== e.target && !that.$element.has(e.target).length) {
                    that.$element.focus();
                }
            });
        };

        //tratar tamanho modal
        $(".modal-wide").on("show.bs.modal", function() {
            var height = $(window).height() - 200;
            $(this).find(".modal-body").css("max-height", height);
        });

        $("input[name='pagamentoParcelas']").on('change', function(){
            var row = "<div class='row'>"+
                "<section class='col-lg-3 col-sm-3 col-xs-3' style='padding-left:30px;'>"+
                "<label class='label'>n° cheque 1: </label>"+
                "<label class='input'>"+
                "<input type='text' name='chequeNumInput' value=''>"+
                "</label>"+

                "<div class='note'></div>"+
                "</section>"+
                "<section class='col-lg-3 col-sm-3 col-xs-3' style='padding-left:30px;'>"+
                "<label class='label'>Data vencimento</label>"+
                "<label class='input'>"+
                "<input type='text' name='chequeVencimentoInput' value=''class='placeholder' placeholder='__/__/____'>"+
                "</label>"+
                "</section>"+
                "<section class='col-lg-3 col-sm-3 col-xs-3' style='padding-left:30px;'>"+
                "<label class='label'>Valor R$</label>"+
                "<label class='input'>"+
                "<input data-thousands='.'data-decimal=',' name='valorFormaPagamentoInput' style='box-sizing:border-box;' type='text' class='form-control input input-sm moeda'/>"+
                "</label>"+
                "<div class='note'></div>"+
                "</section>"+
                "</div>";

            $("div[name='chequeAddParcela']").append(row);

        });

        function strip(str, c) {
            var tmp = str.split(c);
            return tmp.join("");
        }

        $(".cnpjCpf").keyup(function () {
            var tmp = strip($(this).val(), ".");
            tmp = strip(tmp, "/");
            tmp = strip(tmp, "-");
            if (12 < tmp.length) $(this).val(tmp.substr(0, 2) + '.' + tmp.substr(2, 3) + '.' + tmp.substr(5, 3) + '/' + tmp.substr(8, 4) + '-' + tmp.substr(12, 2));
            else if (9 < tmp.length) $(this).val(tmp.substr(0, 3) + '.' + tmp.substr(3, 3) + '.' + tmp.substr(6, 3) + '-' + tmp.substr(9, 3));
            else if (6 < tmp.length) $(this).val(tmp.substr(0, 3) + '.' + tmp.substr(3, 3) + '.' + tmp.substr(6, 3));
            else if (3 < tmp.length)  $(this).val(tmp.substr(0, 3) + '.' + tmp.substr(3, 3));
            else $(this).val(tmp);
        });
        $(".cnpjCpf").on("keydown", function (event) {
            var aKey;

            if (window.event)
                aKey = event.keyCode;
            else
                aKey = event.which;

            //if das teclas precionadas
            if (!(((aKey == 67) && (event.ctrlKey)) || ((aKey == 86) && (event.ctrlKey)) || (aKey == 8) || (aKey == 9) || (aKey == 13) || (aKey == 16) || (aKey == 17) || (aKey == 92)
                || ((aKey >= 96) && (aKey <= 105)) || ((aKey >= 48) && (aKey <= 57)) || ((aKey >= 37) && (aKey <= 40)) || ((aKey >= 112) && (aKey <= 123)))) {
                return false;
            }
        });


    };

    var tableAlunosInit = function () {
        return $("#listaAlunos").dataTable({
            "pageLength": 3,
            processing: true,
            serverSide: true,
            ajax: {
                url: "/boleto-bancario/financeiro/listagem-alunos",
                type: "POST",
                async: false,
                dataSrc: function (json) {
                    return json.data;
                }
            },
            columnDefs: [
                {name: "matricula", targets: 0, data: "matricula"},
                {name: "nome", targets: 1, data: "nome"},
                {name: "turma", targets: 2, data: "turma"}

            ],
            // Tabletools options:
            //   https://datatables.net/extensions/tabletools/button_options
            "sDom": "<'dt-toolbar'<'col-xs-6'f><'col-xs-6'T>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-xs-6'i><'col-xs-6'p>>",
            "oTableTools": {
                "aButtons": "",
                "sSwfPath": "/smart-admin/js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
            },
            oLanguage: {
                sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                sLengthMenu: "Mostrar _MENU_ registros por página",
                sZeroRecords: "Nenhum registro encontrado",
                sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                sInfoFiltered: "(filtrado de _MAX_ registros)",
                oPaginate: {
                    sFirst: "Início",
                    sPrevious: "Anterior",
                    sNext: "Próximo",
                    sLast: "Último"
                }
            }
        });
    };

    var cancelaQuitacao = function(){
        $("#quitacao").hide();
    };

    var abretitulosAlunoModal = function(){
        $("#quitacao").show();
        $('#titulosAlunoModal').modal('toggle');
    };

    var abreQuitacaotitulosModal = function(){
        $("#quitacaoTitulosModal").modal();
    };

    var validateForm = function(){
        if($("#turma").select2('val') == ''){
            alert("Favor selecionar uma turma");
        }
    };


    // functions do financeiro parar dar o minimo ede usabilidade
    var exibeFormaPagamento = function (element) {
        var form = $(element).parents('form');

        var tipoPagamento = $(element).parent().next().text().trim();

        // $("select[name^='tipoPagamentoInput']:visible option:selected").val(tipo);
        $(form).find("div[name='btAdicionarFormaPagamento']").hide();

        $(form).find("div[name='divFormaPagamento']").show();
        $(form).find("select[name^='tipoPagamentoInput']").val(tipoPagamento);
        $(form).find("input[name='chequePracaInput']").val('GOV. VALADARES');
        $(form).find("input[name='chequeEmitenteInput']").val($("input[name='pesNome']").val());
        exibeFormTipo(element);
    };

    var exibeFormTipo = function (element) {
        var form = $(element).parents("form");

        if ($("select[name^='tipoPagamentoInput']:visible option:selected").val() == 'Cheque') {
            $(form).find("div[name='tipoCheque']").show();
            $(form).find("div[name='divChequeValorRestante']").hide();
        } else {
            $(form).find("div[name='tipoCheque']").hide();
        }
    };

    var criaFormaPagamento = function (element) {
        var total = calculaTotalFormaPagamento();
        var form = $(element).parents('form');

        var totalMensalidade = parseFloat($(form).find("span[name='valorMensalideAPagarText']").text().replace('.', '').replace(',', '.'));

        if (total >= totalMensalidade) {
            alert("Impossível adicionar forma de pagamento pois o tilulo ja está com o valor cheio! Favor editar a forma de pagamento atual.");
            return false;
        } else {
            var form = $(element).parents('form');

            $(form).find("select[name='tipoPagamentoInput']").each(function () {
                // seta tipo de pagamento para dinheiro
                $(this).val('Dinheiro');
                $(this).trigger('change');
            });
            limpaFormFormaPagamento(form);

            exibeFormaPagamento(element);
        }
    };

    var calculaTotalFormaPagamento = function (tipoPagamento) {
        var total = 0;

        // busca por um tipo especifico de forma de pagamento
        if (tipoPagamento != undefined) {
            $("tr[data-edit='false']:visible").each(function () {
                if ($(this).find("input").eq(0).val() == tipoPagamento) {
                    if ($(this).find("input").eq(0).val() == "Cheque") {
                        if ($(this).find("input").eq(13).val() != "" && $(this).find("input").eq(13).val() != undefined) {
                            total = parseFloat(total) + parseFloat($(this).find("input").eq(13).val());
                        } else {
                            total = parseFloat(total) + parseFloat($(this).find("input").eq(1).val());
                        }
                    } else {
                        total = parseFloat(total) + parseFloat($(this).find("input").eq(1).val());
                    }
                }
            });
            return total;
        }

        $("tr[data-edit='false']:visible").each(function () {
            if ($(this).find("input").eq(0).val() == "Cheque") {
                if ($(this).find("input").eq(13).val() != "" && $(this).find("input").eq(13).val() != undefined) {
                    total = parseFloat(total) + parseFloat($(this).find("input").eq(13).val());
                } else {
                    total = parseFloat(total) + parseFloat($(this).find("input").eq(1).val());
                }
            } else {
                total = parseFloat(total) + parseFloat($(this).find("input").eq(1).val());
            }
        });

        return total;

    };

    var limpaFormFormaPagamento = function (form) {
        $("tr[data-edit^='true']").removeAttr('data-edit');

        $("div[name^='btAdicionarFormaPagamento']").show();

        if (form) {
            $(form).find("select[name^='tipoPagamentoInput']").each(function () {
                $(this).prop('selected', false);
            });

            $(form).find("select[name^='tipoPagamentoInput']").val("Dinheiro").trigger('change');

            $(form).find("input[name^='valorFormaPagamentoInput']").val("");
            $(form).find("input[name^='chequeValorRestanteInputDisabled']").val("");
            $(form).find("input[name^='chequeEmitenteInput']").val("");
            $(form).find("input[name^='chequeNumInput']").val("");
            $(form).find("select[name^='chequeBancoInput']").val("");
            $(form).find("input[name^='chequeAgenciaInput']").val("");
            $(form).find("input[name^='chequePracaInput']").val("");
            $(form).find("input[name^='chequeEmissaoInput']").val(today);
            $(form).find("input[name^='chequeVencimentoInput']").val(today);
            $(form).find("input[name^='chequeValorInput']").val("");
            $(form).find("input[name^='chequeContaInput']").val("");

            $(form).find("select[name^='tipoPagamentoInput']").trigger('click');

            $(form).find("div[name^='divFormaPagamento']").hide();
        } else {
            $("select[name^='tipoPagamentoInput']:visible").each(function () {
                $(this).prop('selected', false);
            });

            $("select[name^='tipoPagamentoInput']:visible").val("Dinheiro").trigger('change');

            $("input[name^='valorFormaPagamentoInput']:visible").val("");
            $("input[name^='chequeValorRestanteInputDisabled']:visible").val("");
            $("input[name^='chequeEmitenteInput']:visible").val("");
            $("input[name^='chequeNumInput']:visible").val("");
            $("select[name^='chequeBancoInput']:visible").val("");
            $("input[name^='chequeAgenciaInput']:visible").val("");
            $("input[name^='chequePracaInput']:visible").val("");
            $("input[name^='chequeEmissaoInput']:visible").val(today);
            $("input[name^='chequeVencimentoInput']:visible").val(today);
            $("input[name^='chequeValorInput']:visible").val("");
            $("input[name^='chequeContaInput']:visible").val("");

            $("select[name^='tipoPagamentoInput']:visible").trigger('click');

            $("div[name^='divFormaPagamento']:visible").hide();
        }

    };

    var salvarFormaPagamento = function () {
        //if (validaFormaPagamento()) {
            var form = $("select[name='tipoPagamentoInput']:visible").parents('form');
            var tipoPagamento = $("select[name='tipoPagamentoInput']:visible").val().trim();
            var valorFormaPagamento = $("input[name='valorFormaPagamentoInput']:visible").val();
            valorFormaPagamento = parseFloat(valorFormaPagamento.replace('.', '').replace(',', '.'));

            var count = 0;
            var cheque = "";
            var totalFormaPagamento = calculaTotalFormaPagamento(tipoPagamento);
            var totalPagamento = calculaTotalFormaPagamento();
            var status = confirm("Deseja salvar forma de pagamento");

            var cheque;

            if (status == true) {
                $("tr[data-edit='true']").remove();

                if (tipoPagamento == "Cheque") {
                    var restantCheque = $("input[name='chequeValorRestanteInputDisabled']").val();
                    valorFormaPagamento = parseFloat(restantCheque.replace('.', '').replace(',', '.')) || valorFormaPagamento;

                    if ($(form).find("input[name='tituloValor']").val() != undefined) {
                        var valorTitulo = parseFloat($(form).find("input[name='tituloValor']").val().replace('.', '').replace(',', '.'));
                    } else if ($(form).find("span[name='valorMensalideAPagarText']").text() != "") {
                        var valorTitulo = parseFloat($(form).find("span[name='valorMensalideAPagarText']").text().replace('.', '').replace(',', '.'));
                    } else {
                        var valorTitulo = parseFloat($(form).find("span[name='totalParcial']").text().replace('.', '').replace(',', '.'));
                    }
                    // valor do titulo menos o valor adicionado ou editado
                    var valorEmAberto = valorTitulo - valorFormaPagamento;



                    cheque =
                        "<input type='hidden' name='chequeBanco[]' value='" + $("select[name='chequeBancoInput']:visible option:selected").val() + "'>" +
                        "<input type='hidden' name='chequeNum[]' value='" + $("input[name='chequeNumInput']:visible").val() + "'>" +
                        "<input type='hidden' name='chequeConta[]' value='" + $("input[name='chequeContaInput']:visible").val() + "'>" +
                        "<input type='hidden' name='chequeEmitente[]' value='" + $("input[name='chequeEmitenteInput']:visible").val() + "'>" +
                        "<input type='hidden' name='chequeAgencia[]' value='" + $("input[name='chequeAgenciaInput']:visible").val() + "'>" +
                        "<input type='hidden' name='chequePraca[]' value='" + $("input[name='chequePracaInput']:visible").val() + "'>" +
                        "<input type='hidden' name='chequeEmissao[]' value='" + $("input[name='chequeEmissaoInput']:visible").val() + "'>" +
                        "<input type='hidden' name='chequeVencimento[]' value='" + $("input[name='chequeVencimentoInput']:visible").val() + "'>" +
                        "<input type='hidden' name='chequeValor[]' value='" + valorFormaPagamento + "'>" +
                        "<input type='hidden' name='chequeId[]' value='" + $("input[name^='chequeIdInput']").val() + "'>" +
                        "<input type='hidden' name='chequeValorUtilizado[]' value='" + $("input[name^='chequeValorUtilizadoInput']").val() + "'>" +
                        "<input type='hidden' name='chequeValorRestante[]' value='" + $("input[name^='chequeValorRestanteInput']").val() + "'>"
                    ;
                    // valores de cheque não podem ser somados, pois são cheques diferentes
                    totalFormaPagamento = 0;
                }

                totalFormaPagamento = totalFormaPagamento + valorFormaPagamento;
                $("tbody[name='tableFormaPagamento']:visible").prepend(
                    "<tr data-edit='false'>" +
                    "<td>" +
                    "<a onclick='financeiro.editaFormaPagameto(this)' >"+ tipoPagamento+" / "+ $("input[name='chequeCpfCnpj']").val()+"</a>"+
                    "</td>" +
                    "<td>" +
                    parseFloat(totalFormaPagamento).formatMoney(2, ',', '.') +
                    "</td>" +
                    "<input type='hidden' name='tituloTipoPagamento[]' value='" + tipoPagamento + "'>" +
                    "<input type='hidden' name='valorFormaPagamento[]' value='" + totalFormaPagamento + "'>" +
                    (cheque || '') +
                    "</tr>"
                );


                //atualizaTotalizador();

                limpaFormFormaPagamento(form);
            }
        //}
    };

    var validaFormaPagamento = function () {
        var tituloTipoPagamento = $("select[name='tipoPagamentoInput']:visible");
        var valorFormaPagamento = parseFloat($("input[name='valorFormaPagamentoInput']:visible").val().replace('.', '').replace(',', '.')).toFixed(2);

        if (tituloTipoPagamento.val() == "NULL") {
            alert("Selecione uma forma de !");
            tituloTipoPagamento.focus();
            return false
        }

        if (valorFormaPagamento <= 0) {
            alert("Favor inserir um valor valido!");
            $("input[name='valorFormaPagamentoInput']:visible").focus();
            return false;
        }

        var totalParcial = parseFloat($("span[name='valorMensalideAPagarText']:visible").text().replace('.', '').replace(',', '.'));
        var total = 0;

        total = calculaTotalFormaPagamento();

        if (tituloTipoPagamento.val() == "Cheque") {
            return validaFormaPagamentoCheque();
        }

        return true
    };

    var calculaTotalFormaPagamento = function (tipoPagamento) {
        var total = 0;

        // busca por um tipo especifico de forma de pagamento
        if (tipoPagamento != undefined) {
            $("tr[data-edit='false']:visible").each(function () {
                if ($(this).find("input").eq(0).val() == tipoPagamento) {
                    if ($(this).find("input").eq(0).val() == "Cheque") {
                        if ($(this).find("input").eq(13).val() != "" && $(this).find("input").eq(13).val() != undefined) {
                            total = parseFloat(total) + parseFloat($(this).find("input").eq(13).val());
                        } else {
                            total = parseFloat(total) + parseFloat($(this).find("input").eq(1).val());
                        }
                    } else {
                        total = parseFloat(total) + parseFloat($(this).find("input").eq(1).val());
                    }
                }
            });
            return total;
        }

        $("tr[data-edit='false']:visible").each(function () {
            if ($(this).find("input").eq(0).val() == "Cheque") {
                if ($(this).find("input").eq(13).val() != "" && $(this).find("input").eq(13).val() != undefined) {
                    total = parseFloat(total) + parseFloat($(this).find("input").eq(13).val());
                } else {
                    total = parseFloat(total) + parseFloat($(this).find("input").eq(1).val());
                }
            } else {
                total = parseFloat(total) + parseFloat($(this).find("input").eq(1).val());
            }
        });

        return total;

    };

    var validaFormaPagamentoCheque = function (tipoPagamento) {
        var total = 0;

        // busca por um tipo especifico de forma de pagamento
        if (tipoPagamento != undefined) {
            $("tr[data-edit='false']:visible").each(function () {
                if ($(this).find("input").eq(0).val() == tipoPagamento) {
                    if ($(this).find("input").eq(0).val() == "Cheque") {
                        if ($(this).find("input").eq(13).val() != "" && $(this).find("input").eq(13).val() != undefined) {
                            total = parseFloat(total) + parseFloat($(this).find("input").eq(13).val());
                        } else {
                            total = parseFloat(total) + parseFloat($(this).find("input").eq(1).val());
                        }
                    } else {
                        total = parseFloat(total) + parseFloat($(this).find("input").eq(1).val());
                    }
                }
            });
            return total;
        }

        $("tr[data-edit='false']:visible").each(function () {
            if ($(this).find("input").eq(0).val() == "Cheque") {
                if ($(this).find("input").eq(13).val() != "" && $(this).find("input").eq(13).val() != undefined) {
                    total = parseFloat(total) + parseFloat($(this).find("input").eq(13).val());
                } else {
                    total = parseFloat(total) + parseFloat($(this).find("input").eq(1).val());
                }
            } else {
                total = parseFloat(total) + parseFloat($(this).find("input").eq(1).val());
            }
        });

        return total;

    };

    Number.prototype.formatMoney = function(c, d, t){
        var j;
        var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };''

    var atualizaTotalizador = function () {
        var total = calculaTotalFormaPagamento();

        $("span[name='totalParcial']:visible").text(parseFloat(total).formatMoney(2, ',', '.'));
        var form = $("span[name='totalParcial']:visible").parents('form');

        calculaTroco(form);

    };

    var calculaTroco = function (form) {
        var total = parseFloat(calculaTotalFormaPagamento());
        var valorBase = clearValue($(form).find("td[name='totalBaseAPagar']").attr('data-valbase'));

        var troco = total - valorBase;

        if (troco > 0) {
            $("span[name='troco']").text(troco.formatMoney(2, ',', '.'));
        } else {
            $("span[name='troco']").text("0,00");
        }
    };

    var clearValue = function (value) {
        return parseFloat((value.replace('.', '').replace(',', '.')));
    };


    return {
        init                                : init,
        tableDocenciasInit                  : tableAlunosInit,
        abreQuitacaotitulosModal            : abreQuitacaotitulosModal,
        cancelaQuitacao                     : cancelaQuitacao,
        abretitulosAlunoModal               : abretitulosAlunoModal,
        exibeFormaPagamento                 : exibeFormaPagamento,
        exibeFormTipo                       : exibeFormTipo,
        criaFormaPagamento                  : criaFormaPagamento,
        calculaTotalFormaPagamento          : calculaTotalFormaPagamento,
        limpaFormFormaPagamento             : limpaFormFormaPagamento,
        salvarFormaPagamento                : salvarFormaPagamento,
        validaFormaPagamento                : validaFormaPagamento,
        validaFormaPagamentoCheque          : validaFormaPagamentoCheque,
        atualizaTotalizador                 : atualizaTotalizador,
        calculaTroco                        : calculaTroco


    };

})(jQuery, window, document);

$(function () {
    pagamentoLote.init();
});

