$(document).ready(function(){
    $("input, textarea, select ").bind("keypress", function(e) {
        var tecla = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;
        if (tecla == 13) {
            var input = $(this).parent().parent().next().find("input:visible");
            if( input.html() != undefined ){
                input.focus();
            } else {
                count = 1;
                input = $(this).parent().parent();
                while(count < 10){
                    for(var i = 0; i < count; i++ ){
                        input = input.parent();
                    }
                    if(input.next().find("input:visible").html() != undefined ){
                        input = input.next().find("input:visible");
                        break;
                    } else if(input.next().find("select:visible").html() != undefined ){
                        input = input.next().find("select:visible");
                        break;
                    } else if(input.next().find("textarea:visible").html() != undefined ){
                        input = input.next().find("textarea:visible");
                        break;
                    }
                    count = count+1;

                }
                if(input.eq(0).html() != undefined){
                    input.eq(0).focus();
                } else {
                    return true;
                }
            }
            return false;
        }
    });


    Number.prototype.formatMoney = function(c, d, t){
        var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };

    var matricula = $("input[name='matricula']").val();
    $.ajax({
        async: true,
        type: "POST",
        url: "/matricula/acadperiodo-aluno/resumo-aluno/"+matricula,
        data: {
            matricula: matricula,
        },
        success: function( data ){
            $("#dadosAluno").html(data);
        },
        error: function( a ){
            alert("Erro ao conectar ao servidor verifique sua conexão a internet!")
        }

    });

    // usando
    //(1000).formatMoney(2, ',', '.');
    //retorna 1.000,00

    $("input[name='tituloDataVencimento']").parent().parent().removeAttr('style').css('padding-left','60px');

    $('#valorFormaPagamento').maskMoney();
    $('#tituloDescontoManual').maskMoney();
    $('#valorPagoCaixa').maskMoney();
    $('#valorTroco').maskMoney();


    var tipoPagamento = jQuery("select[name='tipotitulo']").val();
    jQuery("input[name='per']").parent().append("<input type='hidden' name='tipotitulo' value='"+tipoPagamento+"'/>");
});

function baixaTitulo(){
    var totalParcial = $("#totalParcial").text();
    var valor = jQuery("#tituloValorAPagarText").text();
    var titulo = jQuery("input[name='tituloId']").val();

    if( totalParcial != valor ){
        alert("Atenção valor a pagar do titulo não corresponde com valor total a ser pago! Favor lançar os valores corretamente e tentar novamente.");
        return false;
    }
    var resposta = confirm("Confirma a baixa do titulo de n°"+titulo+" com valor "+valor+" !");
    if (resposta == false) {
        return false;
    }
    jQuery("input[data-masked='Percent']").each(function(){
        jQuery(this).val(parseFloat(jQuery(this).val().replace(",",".")));
    });
    //jQuery('#tituloDescontoManual').val(parseFloat($("#tituloDescontoManual").maskMoney('unmasked')[0]).toFixed(2));
    $("#formaPagamento").detach();

}

function tabenter(event,campo){
    var tecla = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
    if (tecla==13) {
        campo.focus();
    }
}

function exibeFormaPagamento(){
    $("#btAdicionarFormaPagamento").hide();
    $("#formaPagamento").show();
    $("#chequePraca").val('GOV. VALADARES');
    $("#chequeEmitente").val($("input[name='pesNome']").val());
}

function salvarFormaPagamento() {
    if( validaFormaPagamento() ){
        var tipoPagamento = $("#tituloTipoPagamento").val().trim();
        var valorFormaPagamento = $("#valorFormaPagamento").maskMoney('unmasked')[0].toFixed(2);
        var count = 0;
        var cheque = "";

        var status = confirm("Deseja salvar forma de pagamento");
        if(status == true){
            if( tipoPagamento == "Dinheiro" ){
                $("#tableFormaPagamento").find('tr').each(function(){
                    $(this).find('td').each(function(){
                        if($(this).text().trim() == "Dinheiro"){
                            $(this).parent().attr('data-edit',true);
                        }
                    });
                });
            }
            $("tr[data-edit='true']").remove();

            if(tipoPagamento == "Cheque"){
                var valorTitulo = parseFloat($("#tituloValorAPagarText").text().replace('.','').replace(',','.'));
                if(valorTitulo > valorFormaPagamento){
                    var valorEmAberto = valorTitulo - valorFormaPagamento;
                    $('#tableFormaPagamento').prepend(
                        "<tr data-edit='false'>" +
                        "<td>" +
                        "<a class='fa fa-edit' onclick='editaFormaPagameto(this)' > Editar</a> | " +
                        "<a class='fa fa-trash-o' onclick='removeFormaPagameto(this)' > Remover</a>"+
                        "</td>"+
                        "<td>" +
                        "Dinheiro"+
                        "</td>"+
                        "<td>" +
                        parseFloat(valorEmAberto).formatMoney(2, ',', '.')+
                        "</td>"+
                        "<input type='hidden' name='tituloTipoPagamento[]' value='Dinheiro'>"+
                        "<input type='hidden' name='valorFormaPagamento[]' value='"+valorEmAberto+"'>"+
                        "</tr>"
                    );
                }

                cheque =
                    "<input type='hidden' name='chequeEmitente[]' value='"+$("#chequeEmitente").val()+"'>"+
                    "<input type='hidden' name='chequeNum[]' value='"+$("#chequeNum").val()+"'>"+
                    "<input type='hidden' name='chequeBanco[]' value='"+$("#chequeBanco").val()+"'>"+
                    "<input type='hidden' name='chequeAgencia[]' value='"+$("#chequeAgencia").val()+"'>"+
                    "<input type='hidden' name='chequePraca[]' value='"+$("#chequePraca").val()+"'>"+
                    "<input type='hidden' name='chequeEmissao[]' value='"+$("#chequeEmissao").val()+"'>"+
                    "<input type='hidden' name='chequeVencimento[]' value='"+$("#chequeVencimento").val()+"'>"+
                    "<input type='hidden' name='chequeValor[]' value='"+valorFormaPagamento+"'>"
                ;
            }


            $('#tableFormaPagamento').prepend(
                "<tr data-edit='false'>" +
                "<td>" +
                "<a class='fa fa-edit' onclick='editaFormaPagameto(this)' > Editar</a> | " +
                "<a class='fa fa-trash-o' onclick='removeFormaPagameto(this)' > Remover</a>"+
                "</td>"+
                "<td>" +
                tipoPagamento+
                "</td>"+
                "<td>" +
                parseFloat(valorFormaPagamento).formatMoney(2, ',', '.')+
                "</td>"+
                "<input type='hidden' name='tituloTipoPagamento[]' value='"+tipoPagamento+"'>"+
                "<input type='hidden' name='valorFormaPagamento[]' value='"+valorFormaPagamento+"'>"+
                cheque+
                "</tr>"
            );

            limpaFormResponsavel();
            atualizaTotalizador();
        } else {
            limpaFormResponsavel();
        }
    }
}


function exibeFormTipo( element ){
    if( element.value == 'Cheque'){
        $("#tipoCheque").show();
    } else {
        $("#tipoCheque").hide();
    }
}

function limpaFormResponsavel () {
    $("tr[data-edit='true']").removeAttr('data-edit');

    $("#formaPagamento").hide();
    $("#btAdicionarFormaPagamento").show();
    $("#tituloTipoPagamento").each(function(){
        $(this).prop('selected', false);
    });
    $("#valorFormaPagamento").val("");
    $("#chequeEmitente").val("");
    $("#chequeNum").val("");
    $("#chequeBanco").val("");
    $("#chequeAgencia").val("");
    $("#chequePraca").val("");
    $("#chequeEmissao").val("");
    $("#chequeVencimento").val("");
    $("#chequeValor").val("");
}

function editaFormaPagameto( element ){
    var pain = $(element).parent().parent();
    var tituloTipoPagamento = $("#tituloTipoPagamento");

    pain.attr('data-edit',true);

    tituloTipoPagamento.filter(function() {
        return $(this).text() == pain.find('td').eq(1).text().trim();
    }).prop('selected', true);
    $("#valorFormaPagamento").val(pain.find('td').eq(2).text().trim());

    $("#chequeEmitente").val($("input[name='pesNome']").val());
    $("#chequePraca").val('GOV. VALADARES');

    if( tituloTipoPagamento.val().trim() == "Cheque" ){
        $("#chequeEmitente").val(pain.find('input').eq(2).val());
        $("#chequeNum").val(pain.find('input').eq(3).val());
        $("#chequeBanco").val(pain.find('input').eq(4).val());
        $("#chequeAgencia").val(pain.find('input').eq(5).val());
        $("#chequePraca").val(pain.find('input').eq(6).val());
        $("#chequeEmissao").val(pain.find('input').eq(7).val());
        $("#chequeVencimento").val(pain.find('input').eq(8).val());
        $("#chequeValor").val(pain.find('input').eq(9).val());
    }

    exibeFormaPagamento();

    //$(element).parent().parent().detach();
}

function removeFormaPagameto( element ){
    var count = 0;
    $("#tableFormaPagamento").find('tr').each(function(){
        count += 1;
    });
    if( count < 3){
        alert("Impossível excluir esse tipo de pagamento pois este é o tipo padrã, favor editar o memso caso necessite!");
        return false;
    }

    var confirme = window.confirm("Deseja realmente exlcuir essa forma de pagamento? Atenção as alteracoes só serão realizadas após salvar o titulo!");
    if( confirme ){
        $(element).parent().parent().detach();
    }
    atualizaTotalizador();
}

function validaFormaPagamento(){
    var tituloTipoPagamento = $("#tituloTipoPagamento");
    var valorFormaPagamento = $("#valorFormaPagamento").maskMoney('unmasked')[0].toFixed(2);
    var totalParcial = parseFloat($("#tituloValorAPagarText").text().replace('.','').replace(',','.'));
    var total = 0;

    if( tituloTipoPagamento.val() == "NULL" ){
        alert("Selecione um tipo!");
        tituloTipoPagamento.focus();
        return false
    }

    if( valorFormaPagamento <= 0  ){
        alert("Favor inserir um valor valido!");
        $("#valorFormaPagamento").focus();
        return false;
    }

    if( tituloTipoPagamento.val() != "Dinheiro" ){
        $("tr[data-edit='false']").each(function(){
            total = parseFloat(total) + parseFloat( $(this).find("input").eq(1).val());
        });
        if( (parseFloat(valorFormaPagamento) + parseFloat(total)) > totalParcial ){
            alert("Atenção, o valor que está tentando lançar é maior que o valor do titulo! Favor lançar os valores corretamente e tentar novamente.");
            $("#valorFormaPagamento").focus();
            return false;
        }
    }

    if( valorFormaPagamento > totalParcial   ){
        alert("Atenção, o valor que está tentando lançar é maior que o valor do titulo! Favor lançar os valores corretamente e tentar novamente.");
        $("#valorFormaPagamento").focus();
        return false;
    }

    if( tituloTipoPagamento.val() == "Cheque" ){
        return validaFormaPagamentoCheque();
    }
    return true
}

function validaFormaPagamentoCheque(){
    var chequeEmitente      = $("#chequeEmitente");
    var chequeNum           = $("#chequeNum");
    var chequeBanco         = $("#chequeBanco");
    var chequeAgencia       = $("#chequeAgencia");
    var chequePraca         = $("#chequePraca");
    var chequeEmissao       = $("#chequeEmissao");
    var chequeVencimento    = $("#chequeVencimento");
    var chequeValor         = $("#chequeValor");


    if( chequeEmitente.val().trim() == "" ){
        alert("Favor Preencher o campo Emitente corretamente!");
        chequeEmitente.focus();
        return false;
    }
    if( chequeNum.val().trim() == "" ){
        alert("Favor Preencher o campo N° cheque corretamente!");
        chequeNum.focus();
        return false;
    }
    if( chequeBanco.val().trim() == "" ){
        alert("Favor Preencher o campo Banco corretamente!");
        chequeNum.focus();
        return false;
    }
    if( chequeAgencia.val().trim() == "" ){
        alert("Favor Preencher o campo Agencia corretamente!");
        chequeAgencia.focus();
        return false;
    }
    if( chequePraca.val().trim() == "" ){
        alert("Favor Preencher o campo Praça corretamente!");
        chequePraca.focus();
        return false;
    }
    if( chequeEmissao.val().trim() == "" ){
        alert("Favor Preencher o campo Emissão corretamente!");
        chequeEmissao.focus();
        return false;
    }
    if( chequeVencimento.val().trim() == "" ){
        alert("Favor Preencher o campo Vencimento corretamente!");
        chequeVencimento.focus();
        return false;
    }

    return true;
}


function recalculaDescontos(element){
    $("input[name='salvar']").attr('disabled',true);
    var valor = $(element).maskMoney('unmasked')[0].toFixed(2);
    if( valor < 0){
        alert("Favor inserir um valor válido!");
        $(element).focus();
        return false;
    }
    var titulo = $("input[name='tituloId']").val();
    $.ajax({
        async: true,
        type: "POST",
        url: "/boleto-bancario/financeiro-titulo/insere-desconto-manual",
        data: {
            tituloDescontoManual: valor,
            tituloId: titulo
        },
        success: function( data ){
            location.reload();
        },
        error: function( a ){
            alert("Erro ao conectar ao servidor verifique sua conexão a internet!")
        }

    });

}

function atualizaTotalizador(){
    var total = 0;

    $("tr[data-edit='false']").each(function(){
        total = parseFloat(total) + parseFloat( $(this).find("input").eq(1).val());
    });

    $("#totalParcial").text(parseFloat(total).formatMoney(2, ',', '.'));

}

function calculaTroco( element ){
    var valorTitulo = parseFloat($("#tituloValorAPagarText").text().replace('.','').replace(',','.'));

    var pago = $(element).maskMoney('unmasked')[0];

    var troco = 0;
    if( pago < valorTitulo){
        alert("valor invalidado, favor inserir um valor maior ou igual ao do ttitulo!");
        return false;
    }

    troco = pago - valorTitulo;

    $("#valorTroco").val(troco);

    troco = parseFloat(pago) - parseFloat(valorTitulo);
    troco = troco.toFixed(2);
    $("#valorTroco").maskMoney('destroy');
    $("#valorTroco").val(parseFloat(troco).formatMoney(2, ',', '.'));
    $('#valorTroco').maskMoney();
}

function verificaFormaPagamento(){
    var ativo = $("#tituloTipoPagamento:visible");
    if(ativo.html() != undefined){
        salvarFormaPagamento();
    }
}
