$(document).ready(function(){
    $("input, textarea, select").bind("keypress", function(e) {
        var tecla = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;
        if (tecla == 13) {
            var input = $(this).parent().parent().next().find("input:visible");
            if( input.html() != undefined ){
                input.focus();
            } else {
                count = 1;
                input = $(this).parent().parent();
                while(count < 10){
                    for(var i = 0; i < count; i++ ){
                        input = input.parent();
                    }
                    if(input.next().find("input:visible").html() != undefined ){
                        input = input.next().find("input:visible");
                        break;
                    } else if(input.next().find("select:visible").html() != undefined ){
                        input = input.next().find("select:visible");
                        break;
                    } else if(input.next().find("textarea:visible").html() != undefined ){
                        input = input.next().find("textarea:visible");
                        break;
                    }
                    count = count+1;

                }
                if(input.eq(0).html() != undefined){
                    input.eq(0).focus();
                } else {
                    return true;
                }
            }
            return false;
        }
    });

    Number.prototype.formatMoney = function(c, d, t){
        var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };

    // usando
    //(1000).formatMoney(2, ',', '.');
    //retorna 1.000,00

    $('#valorFormaPagamento').maskMoney();
    $('#valor').maskMoney();


    var tipoPagamento = jQuery("select[name='tipotitulo']").val();
    jQuery("input[name='per']").parent().append("<input type='hidden' name='tipotitulo' value='"+tipoPagamento+"'/>");

    jQuery("form").submit(function(){
        if( $("#pessoaNome").val() == "" ){
            alert("Favor inserir um aluno valido");
            return false;
        }
        if( $("select[name='tipotitulo']").val() == "NULL" ){
            alert("Favor selecionar um tipo de taxa!");
            return false;
        }

        var totalParcial = $("#totalParcial").text();
        var valor = $("#valor").maskMoney('unmasked')[0].formatMoney(2, ',', '.');

        if( totalParcial != valor ){
            alert("Atenção valor a pagar do titulo não corresponde com valor total a ser pago! Favor lançar os valores corretamente e tentar novamente.");
            return false;
        }
        var resposta = confirm("Confirma a adicao de taxa com valor de: "+valor+" !");
        if (resposta == false) {
            return false
        }
        jQuery("input[data-masked='Percent']").each(function(){
            jQuery(this).val(parseFloat(jQuery(this).val().replace(",",".")));
        });

        $("#valor").val($("#valor").maskMoney('unmasked')[0].toFixed(2));

        $("#formaPagamento").detach();
    });

});

function baixaTitulo(){
}

function exibeFormaPagamento(){
    $("#btAdicionarFormaPagamento").hide();
    $("#formaPagamento").show();
    $("#chequePraca").val('GOV. VALADARES');
    $("#chequeEmitente").val($("input[name='pesNome']").val());
}

function salvarFormaPagamento() {
    if( validaFormaPagamento() ){
        var tipoPagamento = $("#tituloTipoPagamento").val().trim();
        var valorFormaPagamento = $("#valorFormaPagamento").maskMoney('unmasked')[0].toFixed(2);
        var count = 0;
        var cheque = "";

        if( tipoPagamento == "Dinheiro" ){
            $("#tableFormaPagamento").find('tr').each(function(){
                $(this).find('td').each(function(){
                    if($(this).text().trim() == "Dinheiro"){
                        $(this).parent().attr('data-edit',true);
                    }
                });
            });
        }

        $("tr[data-edit='true']").remove()

        if(tipoPagamento == "Cheque"){
            cheque =
                "<input type='hidden' name='chequeEmitente[]' value='"+$("#chequeEmitente").val()+"'>"+
                "<input type='hidden' name='chequeNum[]' value='"+$("#chequeNum").val()+"'>"+
                "<input type='hidden' name='chequeBanco[]' value='"+$("#chequeBanco").val()+"'>"+
                "<input type='hidden' name='chequeAgencia[]' value='"+$("#chequeAgencia").val()+"'>"+
                "<input type='hidden' name='chequePraca[]' value='"+$("#chequePraca").val()+"'>"+
                "<input type='hidden' name='chequeEmissao[]' value='"+$("#chequeEmissao").val()+"'>"+
                "<input type='hidden' name='chequeVencimento[]' value='"+$("#chequeVencimento").val()+"'>"+
                "<input type='hidden' name='chequeValor[]' value='"+valorFormaPagamento+"'>"
            ;
        }

        $('#tableFormaPagamento').prepend(
            "<tr data-edit='false'>" +
            "<td>" +
            "<a class='fa fa-edit' onclick='editaFormaPagameto(this)' > Editar</a> | " +
            "<a class='fa fa-trash-o' onclick='removeFormaPagameto(this)' > Remover</a>"+
            "</td>"+
            "<td>" +
            tipoPagamento+
            "</td>"+
            "<td>" +
            parseFloat(valorFormaPagamento).formatMoney(2, ',', '.')+
            "</td>"+
            "<input type='hidden' name='tituloTipoPagamento[]' value='"+tipoPagamento+"'>"+
            "<input type='hidden' name='valorFormaPagamento[]' value='"+valorFormaPagamento+"'>"+
            cheque+
            "</tr>"
        );
        limpaFormResponsavel();
        atualizaTotalizador();
    }
}


function exibeFormTipo( element ){
    if( element.value == 'Cheque'){
        $("#tipoCheque").show();
    } else {
        $("#tipoCheque").hide();
    }
}

function limpaFormResponsavel () {
    $("tr[data-edit='true']").removeAttr('data-edit');

    $("#formaPagamento").hide();
    $("#btAdicionarFormaPagamento").show();
    $("#tituloTipoPagamento").each(function(){
        $(this).prop('selected', false);
    });
    $("#valorFormaPagamento").val("");
    $("#chequeEmitente").val("");
    $("#chequeNum").val("");
    $("#chequeBanco").val("");
    $("#chequeAgencia").val("");
    $("#chequePraca").val("");
    $("#chequeEmissao").val("");
    $("#chequeVencimento").val("");
    $("#chequeValor").val("");
}

function editaFormaPagameto( element ){
    var pain = $(element).parent().parent();
    var tituloTipoPagamento = $("#tituloTipoPagamento");

    pain.attr('data-edit',true);

    tituloTipoPagamento.filter(function() {
        return $(this).text() == pain.find('td').eq(1).text().trim();
    }).prop('selected', true);
    $("#valorFormaPagamento").val(pain.find('td').eq(2).text().trim());

    $("#chequePraca").val('GOV. VALADARES');
    $("#chequeEmitente").val($("input[name='pesNome']").val());

    if( tituloTipoPagamento.val().trim() == "Cheque" ){
        $("#chequeEmitente").val(pain.find('input').eq(2).val());
        $("#chequeNum").val(pain.find('input').eq(3).val());
        $("#chequeBanco").val(pain.find('input').eq(4).val());
        $("#chequeAgencia").val(pain.find('input').eq(5).val());
        $("#chequePraca").val(pain.find('input').eq(6).val());
        $("#chequeEmissao").val(pain.find('input').eq(7).val());
        $("#chequeVencimento").val(pain.find('input').eq(8).val());
        $("#chequeValor").val(pain.find('input').eq(9).val());
    }

    exibeFormaPagamento();

    //$(element).parent().parent().detach();
}

function removeFormaPagameto( element ){
    var count = 0;
    $("#tableFormaPagamento").find('tr').each(function(){
        count += 1;
    });
    if( count < 3){
        alert("Impossível excluir esse tipo de pagamento pois este é o tipo padrã, favor editar o memso caso necessite!");
        return false;
    }

    var confirme = window.confirm("Deseja realmente exlcuir essa forma de pagamento? Atenção as alteracoes só serão realizadas após salvar o titulo!");
    if( confirme ){
        $(element).parent().parent().detach();
    }
    atualizaTotalizador();
}

function validaFormaPagamento(){
    var tituloTipoPagamento = $("#tituloTipoPagamento");
    var valorFormaPagamento = $("#valorFormaPagamento").maskMoney('unmasked')[0].toFixed(2);
    var totalParcial = $("#valor").maskMoney('unmasked')[0].toFixed(2);
    var total = 0;

    if( tituloTipoPagamento.val() == "NULL" ){
        alert("Selecione um tipo!");
        tituloTipoPagamento.focus();
        return false
    }

    if( valorFormaPagamento <= 0  ){
        alert("Favor inserir um valor valido!");
        valorFormaPagamento.focus();
        return false;
    }

    if( tituloTipoPagamento.val() != "Dinheiro" ){
        $("tr[data-edit='false']").each(function(){
            total = parseFloat(total) + parseFloat( $(this).find("input").eq(1).val());
        });
        if( (parseFloat(valorFormaPagamento) + parseFloat(total)) > totalParcial ){
            alert("Atenção, o valor que está tentando lançar é maior que o valor do titulo! Favor lançar os valores corretamente e tentar novamente.");
            return false;
        }
    }

    if( valorFormaPagamento > totalParcial   ){
        alert("Atenção, o valor que está tentando lançar é maior que o valor do titulo! Favor lançar os valores corretamente e tentar novamente.");
        return false;
    }

    if( tituloTipoPagamento.val() == "Cheque" ){
        return validaFormaPagamentoCheque();
    }
    return true
}

function validaFormaPagamentoCheque(){
    var chequeEmitente      = $("#chequeEmitente");
    var chequeNum           = $("#chequeNum");
    var chequeBanco         = $("#chequeBanco");
    var chequeAgencia       = $("#chequeAgencia");
    var chequePraca         = $("#chequePraca");
    var chequeEmissao       = $("#chequeEmissao");
    var chequeVencimento    = $("#chequeVencimento");
    var chequeValor         = $("#chequeValor");


    if( chequeEmitente.val().trim() == "" ){
        alert("Favor Preencher o campo Emitente corretamente!");
        chequeEmitente.focus();
        return false;
    }
    if( chequeNum.val().trim() == "" ){
        alert("Favor Preencher o campo N° cheque corretamente!");
        chequeNum.focus();
        return false;
    }
    if( chequeBanco.val().trim() == "" ){
        alert("Favor Preencher o campo Banco corretamente!");
        chequeNum.focus();
        return false;
    }
    if( chequeAgencia.val().trim() == "" ){
        alert("Favor Preencher o campo Agencia corretamente!");
        chequeAgencia.focus();
        return false;
    }
    if( chequePraca.val().trim() == "" ){
        alert("Favor Preencher o campo Praça corretamente!");
        chequePraca.focus();
        return false;
    }
    if( chequeEmissao.val().trim() == "" ){
        alert("Favor Preencher o campo Emissão corretamente!");
        chequeEmissao.focus();
        return false;
    }
    if( chequeVencimento.val().trim() == "" ){
        alert("Favor Preencher o campo Vencimento corretamente!");
        chequeVencimento.focus();
        return false;
    }

    return true;
}


function atualizaTotalizador(){
    var total = 0;

    $("tr[data-edit='false']").each(function(){
        total = parseFloat(total) + parseFloat( $(this).find("input").eq(1).val());
    });

    $("#totalParcial").text(parseFloat(total).formatMoney(2, ',', '.'));

}

function buscaAluno( element ){
    $.ajax({
        async: true,
        type: "POST",
        url: "/matricula/acadperiodo-aluno/busca-matricula",
        data: {
            matricula: $(element).val()
        },
        success: function( data ){
            $("#pessoaNome").parent().parent().detach();
            $("input[name='matricula']").parent().parent().detach();
            $("input[name='pes']").val(data.result.pes);

            $.ajax({
                async: true,
                type: "POST",
                url: "/matricula/acadperiodo-aluno/resumo-aluno/"+$(element).val(),
                data: {
                    matricula: $(element).val(),
                },
                success: function( data ){
                    $("#dadosAluno").html(data);
                    $("input[name='matricula']").removeAttr('readonly').attr('onchange','buscaAluno(this)');
                },
                error: function( a ){
                    alert("Erro ao conectar ao servidor verifique sua conexão a internet!")
                }

            });
        },
        error: function( a ){
            alert("Erro ao conectar ao servidor verifique sua conexão a internet!")
        }

    });
}

function alteraFormaPagamento( element ){
    var valor = parseFloat($(element).maskMoney('unmasked')[0].toFixed(2)).formatMoney(2, ',', '.');
    if(valor == ""){
        $(element).focus();
        alert("Favor inserir um valor valido");
    }
    $("#totalParcial").text(valor);

    if($('#tableFormaPagamento').children().length > 1){
        $('#tableFormaPagamento').children().eq(0).detach();
    }

    $('#tableFormaPagamento').prepend(
        "<tr data-edit='false'>" +
        "<td>" +
        "<a class='fa fa-edit' onclick='editaFormaPagameto(this)' > Editar</a> | " +
        "<a class='fa fa-trash-o' onclick='removeFormaPagameto(this)' > Remover</a>"+
        "</td>"+
        "<td>" +
        "Dinheiro"+
        "</td>"+
        "<td>" +
        valor+
        "</td>"+
        "<input type='hidden' name='tituloTipoPagamento[]' value='Dinheiro'>"+
        "<input type='hidden' name='valorFormaPagamento[]' value='"+$(element).maskMoney('unmasked')[0].toFixed(2)+"'>"+
        "</tr>"
    );
}
