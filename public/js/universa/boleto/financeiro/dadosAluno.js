var financeiro = (function ($, window, document) {
    var configuracoes = {
        alunoDesligado: 0,
        basePath: '',
        periodosLetivos: null,
        tiposMensalidade: null
    };
    var mesUtil = {
        nomes: {
            1: 'Janeiro',
            2: 'Fevereiro',
            3: 'Marco',
            4: 'Abril',
            5: 'Maio',
            6: 'Junho',
            7: 'Julho',
            8: 'Agosto',
            9: 'Setembro',
            10: 'Outubro',
            11: 'Novembro',
            12: 'Dezembro'
        },
        nomeExtenso: function (mes) {
            mes = mes || false;

            if (!mes) {
                return '';
            }

            return this.nomes[mes];
        }
    };

    var tableTaxas;
    var tableHistoricoFinanceiro;
    var tableHistoricoCancelamento;
    var tableCheques;

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    var today = dd + '/' + mm + '/' + yyyy;

    var elementEdit = {
        titulo: '',
        taxas: '',
        historicoFinanceiro: '',
        historicoCancelamento: '',
        descontos: '',
        cheques: '',
        Aluno: ''
    };


    var novoTitulo = {
        init: function () {
            $.validator.setDefaults({
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    }
                    else if (element.parent('label').length) {
                        error.insertAfter(element.parent());
                    }
                    else {
                        error.insertAfter(element);
                    }
                }
            });

            this.iniciaSelecaoPeriodoLetivo();
            this.iniciaSelecaoMesParcela();
            this.iniciaSelecaoVencimento();
            this.iniciaSelecaoTipo();
            this.iniciaSelecaoValor();
            this.validarCampos();

            $('#btn-novo-titulo').click(function () {
                novoTitulo.limparCampos();
                $('#modal-novo-titulo').modal('show');
            });
            $('#btn-nt-cancelar').click(function () {
                $('#modal-novo-titulo').modal('hide');
            });
            $('#btn-nt-salvar').click(function () {
                $('#form-novo-titulo').submit();
            });
            $('#btn-novo-titulo')[configuracoes.alunoDesligado ? 'hide' : 'show']();
        },
        iniciaSelecaoPeriodoLetivo: function () {
            $("#nt-periodoLetivo").select2({
                language: 'pt-BR',
                data: function () {
                    var transformed = $.map(configuracoes.periodosLetivos, function (el) {
                        el.text = el.per_nome;
                        el.id = el.per_id;

                        return el;
                    });

                    return {results: transformed};
                },
                allowClear: true
            });

            $("#nt-periodoLetivo").change(function () {
                $("#nt-mesParcela")
                    .val('')
                    .prop('disabled', this.value == "")
                    .trigger('change');
            })
        },
        iniciaSelecaoMesParcela: function () {
            $("#nt-mesParcela").select2({
                language: 'pt-BR',
                data: function () {
                    var periodoLetivo = $('#nt-periodoLetivo').select2('data') || false;
                    var data = [];

                    if (periodoLetivo) {
                        var perDataFim = new Date(periodoLetivo['per_data_fim']);
                        var perDataInicio = new Date(periodoLetivo['per_data_inicio']);
                        perDataInicio.setMonth(perDataInicio.getMonth() - 1);

                        while (perDataInicio <= perDataFim) {
                            var mes = (perDataInicio.getMonth() + 1);
                            var vencimento = new Date(perDataInicio);
                            vencimento.setDate(1);

                            data.push({id: mes.toString(), text: mesUtil.nomeExtenso(mes), vencimento: vencimento});
                            perDataInicio.setMonth(perDataInicio.getMonth() + 1);
                        }
                    }

                    return {results: data};
                },
                allowClear: true
            });

            $("#nt-mesParcela").change(function () {
                var data = $('#nt-mesParcela').select2('data') || [];
                var venc = data['vencimento'] ? $.datepicker.formatDate("dd/mm/yy", data['vencimento']) : '';
                $("#nt-vencimento")
                    .val(venc)
                    .trigger('change')
                    .trigger('focusout');
            });
        },
        iniciaSelecaoTipo: function () {
            $("#nt-tipo").select2({
                language: 'pt-BR',
                data: function () {
                    var transformed = $.map(configuracoes.tiposMensalidade, function (el) {
                        el.text = el.tipotitulo_nome;
                        el.id = el.tipotitulo_id;

                        return el;
                    });

                    return {results: transformed};
                },
                allowClear: true
            });

            $("#nt-tipo").change(function () {
                var tipo = $('#nt-tipo').select2('data') || [];
                $("#nt-valor")
                    .val(tipo['valores_preco'] || '')
                    .trigger('change')
                    .trigger('focusout');
            });
        },
        iniciaSelecaoVencimento: function () {
            $("#nt-vencimento").datepicker({
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>'
            }).inputmask({
                showMaskOnHover: false,
                clearIncomplete: true,
                mask: ['99/99/9999']
            });
        },
        iniciaSelecaoValor: function () {
            $("#nt-valor").inputmask({
                showMaskOnHover: false,
                clearIncomplete: true,
                alias: 'currency',
                groupSeparator: ".",
                radixPoint: ",",
                placeholder: "0",
                prefix: "",
                clearMaskOnLostFocus: true
            });
        },
        limparCampos: function () {
            $("#nt-periodoLetivo").val('').trigger('change');
            $("#nt-tipo").val('').trigger('change');
            $("#nt-mesParcela").val('').trigger('change');
            $("#nt-observacao").val('').trigger('change');
            $('#form-novo-titulo .has-error .help-block').remove();
            $('#form-novo-titulo .has-error').removeClass('has-error');
        },
        validarCampos: function () {
            $('#form-novo-titulo').validate({
                submitHandler: function (form) {
                    return novoTitulo.criarNovoTitulo();
                },
                rules: {
                    'nt-periodoLetivo': {required: true},
                    'nt-mesParcela': {required: true},
                    'nt-tipo': {required: true},
                    'nt-valor': {required: true},
                    'nt-vencimento': {required: true},
                    'nt-observacao': {required: true}
                },
                messages: {
                    'nt-periodoLetivo': {required: 'Campo obrigatório!'},
                    'nt-mesParcela': {required: 'Campo obrigatório!'},
                    'nt-tipo': {required: 'Campo obrigatório!'},
                    'nt-valor': {required: 'Campo obrigatório!'},
                    'nt-vencimento': {required: 'Campo obrigatório!'},
                    'nt-observacao': {required: 'Campo obrigatório!'}
                },
                ignore: '.ignore'
            });
            $('#form-novo-titulo').on('select2-close', function (e) {
                if (e.target) {
                    $(e.target).valid();
                }
            });
        },
        criarNovoTitulo: function () {
            $('#ajaxLoad').css({display: "block"});
            var periodoLetivo = $('#nt-periodoLetivo').select2('data') || [];
            var arrData = {
                alunoperId: periodoLetivo['alunoper_id'] || '',
                pesId: $('[name="pes"]:first').val(),
                periodoLetivo: periodoLetivo['per_id'] || '',
                mesParcela: $('#nt-mesParcela').val(),
                vencimento: $('#nt-vencimento').val(),
                tipo: $('#nt-tipo').val(),
                valor: $('#nt-valor').val(),
                observacao: $('#nt-observacao').val()
            };

            $.ajax({
                url: configuracoes.basePath + '/boleto-bancario/financeiro/novo-titulo',
                method: 'POST',
                dataType: 'json',
                data: arrData,
                success: function (data) {
                    var tipo = $('#nt-tipo').select2('data');
                    var tipoNome = tipo['tipotitulo_nome'] || '';

                    var abas = {
                        'Mensalidade': {aba: '#tab-s1', tabela: '#tableMensalidades'},
                        'Dependência': {aba: '#tab-s2', tabela: '#tableDependencias'},
                        'Adaptação': {aba: '#tab-s3', tabela: '#tableAdaptacoes'},
                        'Monografia': {aba: '#tab-s4', tabela: '#tableMonografias'}
                    };

                    var aba = abas[tipoNome] || null;

                    if (!aba) {
                        location.reload();
                        return;
                    }

                    $('#modal-novo-titulo').modal('hide');

                    var desconto = 0, acrescimo = 0;

                    if (data.titulo['tituloDesconto'] != undefined) {
                        desconto = data.titulo['tituloDesconto'];
                    }

                    if (data.titulo.acrescimos != undefined) {
                        acrescimo = data.titulo['acrescimos']['valorAcrescimos'];
                    }

                    var tr =
                        "<tr data-ativo='false' onclick='financeiro.buscaMensalidade(this)' data-tituloId='" +
                        data.titulo['tituloId'] + "'>" +
                        "<td><a href='#'>" + data.titulo['tituloMesVencimento'] + "</a></td>" +
                        "<td><a href='#'>" + data.titulo['tituloDataVencimento'] + "</a></td>" +
                        "<td><a href='#'>" +
                        parseFloat(data.titulo['tituloValor']).formatMoney(2, ',', '.') + "</a></td>" +
                        "<td><a href='#'>" + parseFloat(desconto).formatMoney(2, ',', '.') +
                        "</a></td>" +
                        "<td><a href='#'>" + parseFloat(acrescimo).formatMoney(2, ',', '.') +
                        "</a></td>" +
                        "<td><a href='#'>" +
                        parseFloat(data.titulo['valorReceber']).formatMoney(2, ',', '.') + "</a></td>" +
                        "</tr>";

                    $(aba['tabela']).prepend(tr);
                    $(aba['aba']).removeClass('hidden');
                },
                beforeSend: function () {
                    $('#ajaxLoad').css({display: "block"});
                },
                complete: function () {
                    $('#ajaxLoad').css({display: "none"});
                },
                error: function (e) {
                    $('#ajaxLoad').css({display: "none"});
                    alert("Erro ao conectar com o servidor favor verificar a conexão com a internet!");
                }
            });

            return false;
        }
    };

    var init = function (opts) {
        opts = opts || [];
        configuracoes = $.extend(configuracoes, opts);

        $('.date').mask('11/11/1111');
        $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
        $('.time').mask('00:00:00');
        $('.date_time').mask('00/00/0000 00:00:00');
        $('.cep').mask('00000-000');
        $('.phone').mask('0000-0000');
        $('.phone_with_ddd').mask('(00) 0000-0000');
        $('.phone_us').mask('(000) 000-0000');
        $('.mixed').mask('AAA 000-S0S');
        $('.cpf').mask('000.000.000-00', {reverse: true});
        $('.money').mask('000.000.000.000.000,00', {reverse: true});

        $("table tr").on('mouseover', function () {
            $(this).css("background-color", "#ECE5E5");
        });
        $("table tr").on('mouseout', function () {
            $(this).css("background-color", "#fff");
        });
        $(".moeda").maskMoney();

        $("input[name='pagarAgora']").click(function () {
            var val = $(this).val();
            if (val == "true") {
                $("#formTaxa").find("div[name='divTelaFormasDePagamento']").removeAttr("hidden");
            } else {
                $("#formTaxa").find("div[name='divTelaFormasDePagamento']").attr("hidden", "hidden");
            }
        });

        $("input[name='isentarJuros']").click(function () {
            var $form = $(this).parents('form');
            var tituloMulta = 0;
            var tituloJuros = 0;
            var exibirTabela = false;

            var $totalBase = $form.find("td[name='totalBaseAPagar']");
            var valorJuros = parseFloat($(this).attr('data-tvjuros'));
            var valorTotalBase = clearValue($totalBase.attr('data-valBase'));

            var nvTotal = 0;

            if (!$(this).is(":checked")) {
                tituloMulta = $(this).attr('data-tmulta');
                tituloJuros = $(this).attr('data-tjuros');
                exibirTabela = true;

                nvTotal = valorTotalBase + valorJuros;

            } else {
                nvTotal = valorTotalBase - valorJuros;
            }

            $form.find("input[name='tituloMulta']").val(tituloMulta);
            $form.find("input[name='tituloJuros']").val(tituloJuros);

            $totalBase.attr('data-valBase', nvTotal.formatMoney(2, ',', '.'));
            $(this).parent().parent().find('table')[exibirTabela ? 'show' : 'hide']();


            atualizaValorTotal($form);
        });


        tableTaxas = tableTaxasInit();
        tableHistoricoFinanceiro = tableHistoricoFinanceiroInit();
        tableHistoricoCancelamento = tableHistoricoCancelamentoInit();
        tableCheques = tableChequesInit();

        novoTitulo.init();
    };


    var tableTaxasInit = function () {
        TableTools.BUTTONS.add_taxa = $.extend(true, {}, TableTools.buttonBase, {
            "sAction": "div",
            "sTag": "default",
            "sToolTip": "Adicionar",
            "sNewLine": "<br>",
            "sButtonText": "Adicionar",
            //"sDiv": "",
            "fnClick": function (nButton, oConfig) {
                financeiro.adicionaTaxa("");
            }
        });

        var table = $('#taxas').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-6'f><'col-xs-6'T>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-xs-6'i><'col-xs-6'p>>",
            "oTableTools": {
                "sSwfPath": configuracoes.basePath + "/smart-admin/js/plugin/datatables/swf/copy_csv_xls_pdf.swf",
                "aButtons": [
                    {
                        "sExtends": "add_taxa",
                        "sButtonText": "Adicionar",
                        "sDiv": "addTaxa"
                    }
                ]
            },
            oLanguage: {
                sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                sLengthMenu: "Mostrar _MENU_ registros por página",
                sZeroRecords: "Nenhum registro encontrado",
                sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                sInfoFiltered: "(filtrado de _MAX_ registros)",
                oPaginate: {
                    sFirst: "Início",
                    sPrevious: "Anterior",
                    sNext: "Próximo",
                    sLast: "Último"
                }
            }
        });

        return table;
    };

    var tableHistoricoFinanceiroInit = function () {
        var table = $('#historicoFinanceiro').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-6'f><'col-xs-6'T>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-xs-6'i><'col-xs-6'p>>",
            "oTableTools": {
                "aButtons": [],
                "sSwfPath": configuracoes.basePath + "/smart-admin/js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
            },
            columnDefs: [
                {name: "tipo", targets: 0},
                {name: "parcelaMes", targets: 1},
                {name: "dapaPagto", targets: 2, type: "date-eu"},
                {name: "valor", targets: 3},
                {name: "valorPago", targets: 4},
                {name: "usuario", targets: 5},
                {name: "2via", targets: 6}
            ],
            oLanguage: {
                sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                sLengthMenu: "Mostrar _MENU_ registros por página",
                sZeroRecords: "Nenhum registro encontrado",
                sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                sInfoFiltered: "(filtrado de _MAX_ registros)",
                oPaginate: {
                    sFirst: "Início",
                    sPrevious: "Anterior",
                    sNext: "Próximo",
                    sLast: "Último"
                }
            },
            //order:[[2, 'desc']]
        });

        return table;
    };

    var tableHistoricoCancelamentoInit = function () {
        var table = $('#historicoCancelamento').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-6'f><'col-xs-6'T>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-xs-6'i><'col-xs-6'p>>",
            "oTableTools": {
                "aButtons": [],
                "sSwfPath": configuracoes.basePath + "/smart-admin/js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
            },
            columnDefs: [
                {name: "tipo", targets: 0},
                {name: "parcelaMes", targets: 1},
                {name: "dataCancelamento", targets: 2, type: "date-eu"},
                {name: "valor", targets: 3},
                {name: "estado", targets: 4},
                {name: "usuario", targets: 5}
            ],
            oLanguage: {
                sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                sLengthMenu: "Mostrar _MENU_ registros por página",
                sZeroRecords: "Nenhum registro encontrado",
                sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                sInfoFiltered: "(filtrado de _MAX_ registros)",
                oPaginate: {
                    sFirst: "Início",
                    sPrevious: "Anterior",
                    sNext: "Próximo",
                    sLast: "Último"
                }
            },
            //order:[[2, 'desc']]
        });

        return table;
    };

    var tableChequesInit = function () {
        var table = $('#cheques').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-6'f><'col-xs-6'T>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-xs-6'i><'col-xs-6'p>>",
            "oTableTools": {
                "aButtons": [],
                "sSwfPath": configuracoes.basePath + "/smart-admin/js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
            },
            oLanguage: {
                sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                sLengthMenu: "Mostrar _MENU_ registros por página",
                sZeroRecords: "Nenhum registro encontrado",
                sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                sInfoFiltered: "(filtrado de _MAX_ registros)",
                oPaginate: {
                    sFirst: "Início",
                    sPrevious: "Anterior",
                    sNext: "Próximo",
                    sLast: "Último"
                }
            }
        });
        return table;
    };


    var buscaMensalidade = function (element) {
        var tituloId = $(element).attr('data-tituloid');
        $.ajax({
            url: configuracoes.basePath + '/boleto-bancario/financeiro/dados-titulo',
            method: 'POST',
            data: {
                'tituloId': tituloId
            },
            success: function (data) {
                if (data.titulo != undefined) {
                    exibeSegundaView(element);

                    var form = $($("#myTab1 li.active a").attr("href")).children().eq(1).find('form');
                    limpaViewMensalidade(form);
                    insereDadosViewMensalidade(form, data.titulo);
                }
            },
            beforeSend: function () {
                $('#ajaxLoad').css({display: "block"});
            },
            complete: function () {
                $('#ajaxLoad').css({display: "none"});
            },
            error: function (e) {
                alert("Erro ao conectar com o servidor favor verificar a conexão com a internet!");
            }
        });
    };

    var buscaTaxa = function (element, id) {
        var tituloId = $(element).attr('data-tituloid');
        id = id || 'formTaxa';

        $.ajax({
            url: configuracoes.basePath + '/boleto-bancario/financeiro/dados-titulo',
            method: 'POST',
            data: {
                'tituloId': tituloId
            },
            success: function (data) {
                if (data.titulo != undefined) {
                    exibeSegundaView(element);
                    $("#cancelaTaxa").show();
                    var form = $("#" + id);
                    $(form).attr('action', 'javascript:financeiro.quitaTaxa("' + id + '")');
                    $(form).find("input[name^='pagarAgora']").eq(0).prop("checked", "checked");
                    $(form).find("div[name='divTelaFormasDePagamento']").removeAttr("hidden");
                    $(form).find("section[name^='sectionQuitarAgora']").attr('hidden', 'hidden');
                    jQuery("input[name='salvar']").addClass("btn btn-success");
                    jQuery("input[name='salvar']").val("Quitar");

                    limpaViewTaxas(form);
                    insereDadosViewTaxas(form, data.titulo);
                }
            },
            beforeSend: function () {
                $('#ajaxLoad').css({display: "block"});
            },
            complete: function () {
                $('#ajaxLoad').css({display: "none"});
            },
            error: function (e) {
                alert("Erro ao conectar com o servidor favor verificar a conexão com a internet!");
            }
        });

    };

    var buscaHistoricoTitulo = function (element, historicoCancelamento) {
        var tituloId = $(element).attr('data-tituloid');
        historicoCancelamento = historicoCancelamento || false;

        $.ajax({
            url: configuracoes.basePath + '/boleto-bancario/financeiro/dados-titulo',
            method: 'POST',
            data: {
                'tituloId': tituloId
            },
            success: function (data) {
                if (data.titulo != undefined) {
                    var viewHistorico = $(historicoCancelamento ? "#viewHistoricoCancelamento" : "#viewHistorico");
                    viewHistorico.children().remove();
                    var view = "";
                    var form = "";

                    var contem = [
                        "Mensalidade", "Dependência", "Adaptação", "Monografia"
                    ].indexOf(data.titulo.tipotituloNome);

                    if (contem >= 0) {
                        view = $("div[name='viewMensalidadeHistorico']").html();
                    } else {
                        view = $("div[name='viewTaxaHistorico']").html();
                    }

                    viewHistorico.append(view);
                    exibeSegundaView(element);

                    if (contem >= 0) {
                        form = $(viewHistorico).find("form[name='formMensalidadeHistorico']");
                        insereDadosViewMensalidadeHistorico(form, data.titulo);
                    } else {
                        form = $(viewHistorico).find("form[name='formTaxaHistorico']");
                        insereDadosViewTaxaHistorico(form, data.titulo);
                    }
                } else {
                    alert(data.error || 'Falha ao buscar dados do título!');
                }
            },
            beforeSend: function () {
                $('#ajaxLoad').css({display: "block"});
            },
            complete: function () {
                $('#ajaxLoad').css({display: "none"});
            },
            error: function (e) {
                alert("Erro ao conectar com o servidor favor verificar a conexão com a internet!");
            }
        });
    };

    var buscaCheque = function (element) {
        var chequeId = $(element).attr('data-chequeId');

        $.ajax({
            url: configuracoes.basePath + '/boleto-bancario/financeiro/dados-cheque',
            method: 'POST',
            data: {
                'chequeId': chequeId
            },
            success: function (data) {
                if (data.cheque != undefined) {
                    exibeSegundaView(element);
                    form = $("#formCheque");
                    //limpaViewMensalidade(form);
                    insereDadosViewCheque(form, data);
                }
            },
            beforeSend: function () {
                $('#ajaxLoad').css({display: "block"});
            },
            complete: function () {
                $('#ajaxLoad').css({display: "none"});
            },
            error: function (e) {
                alert("Erro ao conectar com o servidor favor verificar a conexão com a internet!");
            }
        });
    };

    var buscaChequeEuristica = function (saving) {
        var result;
        var chequeNum = $("input[name='chequeNumInput']:visible").val();
        var chequeConta = $("input[name='chequeContaInput']:visible").val();
        var chequeBanco = $("select[name='chequeBancoInput']:visible option:selected").val();

        $.ajax({
            url: configuracoes.basePath + '/boleto-bancario/financeiro/dados-cheque-euristica',
            method: 'POST',
            async: ((saving) ? false : true), // quando o cheque estiver sendo salvo, solução provisoria
            data: {
                'chequeNum': chequeNum,
                'chequeConta': chequeConta,
                'chequeBanco': chequeBanco
            },
            success: function (data) {
                result = true;

                if (data.cheque != undefined) {
                    $("input[name^='chequeIdInput']").val(data.cheque.chequeId);
                    $("input[name^='chequeValorUtilizadoInput']").val(data.cheque.chequeValorUtilizado);
                    $("input[name^='chequeValorRestanteInput']").val(data.cheque.chequeValorRestante);
                    $("input[name^='chequeValorRestanteInputDisabled']").val(parseFloat(data.cheque.chequeValorRestante).formatMoney(2,
                        ',',
                        '.'));
                    $("input[name^='valorFormaPagamentoInput']").val(data.cheque.chequeValorMoeda);
                    $("input[name='chequeEmitenteInput']:visible").val(data.cheque.chequeEmitente);
                    $("input[name='chequeAgenciaInput']:visible").val(data.cheque.chequeAgencia);
                    $("input[name='chequePracaInput']:visible").val(data.cheque.chequePraca);
                    $("input[name='chequeEmissaoInput']:visible").val(data.cheque.chequeEmissao);
                    $("input[name='chequeVencimentoInput']:visible").val(data.cheque.chequeVencimento);
                    $("input[name='chequeValorInput']:visible").val(data.cheque.chequeValor);
                    $("div[name^='divChequeValorRestante']").show();

                    result = true;
                } else {
                    if (saving == undefined) {
                        $("input[name^='chequeIdInput']").val("");
                        $("input[name^='chequeValorUtilizadoInput']").val("");
                        $("input[name^='chequeValorRestanteInput']").val("");
                        $("div[name^='divChequeValorRestante']").val("").hide();

                        result = true;
                    }
                }

                if (data.msg != undefined) {
                    $("input[name^='chequeAgenciaInput']").focus();
                    alert(data.msg);

                    result = false;
                }
            },
            beforeSend: function () {
                $('#ajaxLoad').css({display: "block"});
            },
            complete: function () {
                $('#ajaxLoad').css({display: "none"});
            },
            error: function (e) {
                alert("Erro ao conectar com o servidor favor verificar a conexão com a internet!");
            }
        });
        return result;
    };

    var criaTaxa = function () {
        var form = $("#formTaxa");
        var select = $("select[name='tipotitulo']").val();
        if (select == "NULL") {
            $(form).find("select[name='tipotitulo']").focus();
            alert("Favor selecionar uma taxa para continuar");
            return false;
        }

        var valor = $(form).find("input[name^='tituloValor']");
        if (valor.val() == "" || valor.val() == "0,00") {
            valor.focus();
            alert("Favor inserir um valor falido");
            return false
        }
        var pagarAgora = $(form).find("input[name='pagarAgora']:checked").val();
        var tituloTipo = $(form).find("select[name='tipotitulo']").find('option:selected').text();
        if (pagarAgora == "true") {
            var totalParcial = $(form).find("span[name='totalParcial']").text();

            if (totalParcial != valor.val()) {
                alert("Atenção valor a pagar do titulo não corresponde com valor total a ser pago! Favor lançar os valores corretamente e tentar novamente.");
                return false;
            }

            var resposta = confirm("Confirma a criaçao e baixa da taxa de " + tituloTipo + " com valor " + valor.val() +
            "!");
            if (resposta == false) {
                return false;
            }
        } else {
            var resposta = confirm("Confirma a criação da taxa de " + tituloTipo + " com valor " + valor.val() + "!");

            if (resposta == false) {
                return false;
            }
        }

        $(form).find("input[name='tituloValor']").val(valor.val().replace('.', '').replace(',', '.'));

        var post = $(form).serialize();

        $.ajax({
            url: configuracoes.basePath + '/boleto-bancario/financeiro/cria-taxa',
            method: 'POST',
            dataType: 'json',
            data: post,
            success: function (data) {
                $(form).find("input[name='tituloValor']").val(valor.val());
                if (data.result != undefined) {
                    if (data.result == true) {
                        alert(data.msg);
                        if (pagarAgora == "true") {
                            window.open(
                                configuracoes.basePath + '/boleto-bancario/financeiro/comprovante-pagamento/' +
                                $("input[name='matricula']").val() + '/' + $("input[name='alunoperId']").val() + '/' +
                                data.titulo.tituloId
                            );

                            if (data.cheques !== undefined) {
                                geraLinhaChequeRetornoAjax(data.cheques);
                            }

                            geraLinhaHistoricoRetornoAjax(data.titulo);
                        } else {
                            tableTaxas.fnDestroy();
                            var tr =
                                    "<tr onclick='financeiro.buscaTaxa(this)' data-tituloId='" + data.titulo.tituloId +
                                    "'>" +
                                    "<td><a href='#'>" + data.titulo.tipotituloNome + "</a></td>" +
                                    "<td><a href='#'>" + data.titulo.tituloDataVencimento + "</a></td>" +
                                    "<td><a href='#'>" + data.titulo.tituloValor + "</a></td>" +
                                    "<td><a href='#'>" + data.titulo.usuarioAutor + "</a></td>" +
                                    "</tr>"
                                ;
                            $('#taxas').append(tr);
                            tableTaxas = tableTaxasInit();
                        }

                        limpaViewTaxas(form);
                        voltaListagem();
                    }
                }

            },
            beforeSend: function () {
                $('#ajaxLoad').css({display: "block"});
            },
            complete: function () {
                $('#ajaxLoad').css({display: "none"});
            },
            error: function (e) {
                $("#formTaxa").find("input[name^='tituloValor']").val(parseFloat(valor.val()).formatMoney(2, ',', '.'));
                alert("Erro ao conectar com o servidor favor verificar a conexão com a internet!");
            }
        });

    };

    var quitaTaxa = function (id) {
        id = id || 'formTaxa';
        var form = $("#" + id);
        var totalParcial = $(form).find("span[name='totalParcial']").text();
        var valor = $(form).find("input[name='tituloValor']").val();
        var titulo = $(form).find("input[name='tituloId']").val();
        var tituloTipo = $(form).find("select[name='tipotitulo']").find('option:selected').text();

        if (totalParcial != valor) {
            alert("Atenção valor a pagar do titulo não corresponde com valor total a ser pago! Favor lançar os valores corretamente e tentar novamente.");
            return false;
        }

        var resposta = confirm("Confirma a baixa da taxa de " + tituloTipo + " com valor " + valor + "?");

        if (resposta == false) {
            return false;
        }

        var post = $(form).serialize();

        $.ajax({
            url: configuracoes.basePath + '/boleto-bancario/financeiro/quita-taxa',
            method: 'POST',
            dataType: 'json',
            data: post,
            success: function (data) {
                if (data.result != undefined) {
                    if (data.result == true) {
                        alert(data.msg);
                        window.open(
                            configuracoes.basePath + '/boleto-bancario/financeiro/comprovante-pagamento/' +
                            $("input[name='matricula']").val() + '/' + $("input[name='alunoperId']").val() +
                            '/' + titulo
                        );
                        tableTaxas.fnDestroy();
                        $(elementEdit.taxas).remove();
                        tableTaxas = tableTaxasInit();

                        if (data.cheques !== undefined) {
                            geraLinhaChequeRetornoAjax(data.cheques);
                        }

                        geraLinhaHistoricoRetornoAjax(data.titulo);
                        limpaViewTaxas();
                        voltaListagem();
                    }
                }
            },
            beforeSend: function () {
                $('#ajaxLoad').css({display: "block"});
            },
            complete: function () {
                $('#ajaxLoad').css({display: "none"});
            },
            error: function (e) {
                alert("Erro ao conectar com o servidor favor verificar a conexão com a internet!");
            }
        });
    };

    var quitaMensalidade = function () {
        var form = $($("#myTab1 li.active a").attr("href")).children().eq(1).find('form');
        var descontoManual = isNaN($(form).find("input[name='valorDescontoManual']").val()) ? clearValue($(form).find("input[name='valorDescontoManual']").val()) : 0;
        var acrescimoManual = isNaN($(form).find("input[name='valorAcrescimoManual']").val()) ? clearValue($(form).find("input[name='valorAcrescimoManual']").val()) : 0;
        var valorReceber = parseFloat($(form).find("input[name='valorReceber']").val());

        $(form).find("input[name='descontoManual']").val(descontoManual);
        $(form).find("input[name='acrescimoManual']").val(acrescimoManual);

        var totalFormaPagamento = calculaTotalFormaPagamento();
        var valor = clearValue($(form).find("span[name='valorMensalideAPagarText']").text());
        var titulo = $(form).find("input[name='tituloId']").val();

        if (totalFormaPagamento < valor) {
            alert("Atenção valor a pagar do titulo não corresponde com valor total a ser pago! Favor lançar os valores corretamente e tentar novamente.");
            return false;
        }
        $(form).find("input[name='valorReceber']").val(valor);

        var resposta = confirm("Confirma a baixa do titulo de n°" + titulo + " com valor " + valor + " !");

        if (resposta == false) {
            return false;
        }

        var post = $(form).serialize();

        $.ajax({
            url: configuracoes.basePath + '/boleto-bancario/financeiro/quita-mensalidade',
            method: 'POST',
            dataType: 'json',
            data: post,
            success: function (data) {
                if (data.result != undefined) {
                    if (data.result == true) {
                        alert(data.msg);
                        window.open(
                            configuracoes.basePath + '/boleto-bancario/financeiro/comprovante-pagamento/' +
                            $("input[name='matricula']").val() + '/' + $("input[name='alunoperId']").val() +
                            '/' + titulo
                        );
                        $(elementEdit.titulo).remove();

                        if (data.cheques != undefined) {
                            geraLinhaChequeRetornoAjax(data.cheques);
                        }

                        geraLinhaHistoricoRetornoAjax(data.titulo);
                        voltaListagem();
                        limpaViewMensalidade(form);
                    }
                }
            },
            beforeSend: function () {
                $('#ajaxLoad').css({display: "block"});
            },
            complete: function () {
                $('#ajaxLoad').css({display: "none"});
            },
            error: function (e) {
                //$(form).find("input[name='valorReceber']").val(valorReceber + descontoManual);
                alert("Erro ao conectar com o servidor favor verificar a conexão com a internet!");
            }
        });
    };

    var salvaObservacao = function () {
        var form = $("#formObservacoes");

        var resposta = confirm("Deseja realmente salvar estas observações?");

        if (resposta == false) {
            return false;
        }

        var post = $(form).serialize();

        $.ajax({
            url: configuracoes.basePath + '/boleto-bancario/financeiro/salva-observacoes',
            method: 'POST',
            dataType: 'json',
            data: post,
            success: function (data) {
                if (data.msg != undefined) {
                    alert(data.msg);
                }

            },
            beforeSend: function () {
                $('#ajaxLoad').css({display: "block"});
            },
            complete: function () {
                $('#ajaxLoad').css({display: "none"});
            },
            error: function (e) {
                alert("Erro ao conectar com o servidor favor verificar a conexão com a internet!");
            }
        });
    };

    var estornoTitulo = function () {
        if (configuracoes.alunoDesligado) {
            return false;
        }

        var form = $("#viewHistorico").find("form:visible");
        var resposta = confirm("Confirma o estorno do titulo?");

        if (resposta === false) {
            return false;
        }

        var post = $(form).serialize();

        $.ajax({
            url: configuracoes.basePath + '/boleto-bancario/financeiro/estorno-titulo',
            method: 'POST',
            dataType: 'json',
            data: post,
            success: function (data) {
                if (data.result !== undefined) {
                    if (data.result === true) {
                        alert(data.msg);
                        tableHistoricoFinanceiro.fnDestroy();
                        tableHistoricoCancelamento.fnDestroy();

                        var $tr = $(elementEdit.historicoFinanceiro).clone();

                        $tr.find('td:eq(2)').html('<a href="#">' + data.titulo.tituloDataProcessamento + '</a>');
                        $tr.find('td:eq(5)').html('<a href="#">' + data.titulo.usuarioAutor + '</a>');
                        $tr.find('td:eq(4)').html('<a href="#">Estorno</a>');
                        $tr.find('td:eq(6)').remove();
                        $tr.attr('onclick', "financeiro.buscaHistoricoTitulo(this,true)");

                        $('#historicoCancelamento').append($tr);
                        $(elementEdit.historicoFinanceiro).remove();
                        tableHistoricoFinanceiro = tableHistoricoFinanceiroInit();
                        tableHistoricoCancelamento = tableHistoricoCancelamentoInit();

                        if (data.titulo !== undefined) {
                            if (data.titulo.tipotituloNome == "Mensalidade") {
                                var desconto = 0;
                                var acrescimo = 0;
                                if (data.titulo.tituloDesconto !== undefined) {
                                    desconto = data.titulo.tituloDesconto;
                                }

                                if (data.titulo.acrescimos !== undefined) {
                                    acrescimo = data.titulo.acrescimos.valorAcrescimos;
                                }

                                var tr =
                                        "<tr data-ativo='false' onclick='financeiro.buscaMensalidade(this)' data-tituloId='" +
                                        data.titulo.tituloId + "'>" +
                                        "<td><a href='#'>" + data.titulo.tituloMesVencimento + "</a></td>" +
                                        "<td><a href='#'>" + data.titulo.tituloDataVencimento + "</a></td>" +
                                        "<td><a href='#'>" +
                                        parseFloat(data.titulo.tituloValor).formatMoney(2, ',', '.') + "</a></td>" +
                                        "<td><a href='#'>" + parseFloat(desconto).formatMoney(2, ',', '.') +
                                        "</a></td>" +
                                        "<td><a href='#'>" + parseFloat(acrescimo).formatMoney(2, ',', '.') +
                                        "</a></td>" +
                                        "<td><a href='#'>" +
                                        parseFloat(data.titulo.valorReceber).formatMoney(2, ',', '.') + "</a></td>" +
                                        "</tr>"
                                    ;
                                $('#tableMensalidades').prepend(tr);
                            } else {
                                tableTaxas.fnDestroy();
                                var tr =
                                        "<tr onclick='financeiro.buscaTaxa(this)' data-tituloId='" +
                                        data.titulo.tituloId + "'>" +
                                        "<td><a href='#'>" + data.titulo.tipotituloNome + "</a></td>" +
                                        "<td><a href='#'>" + data.titulo.tituloDataVencimento + "</a></td>" +
                                        "<td><a href='#'>" + data.titulo.tituloValor + "</a></td>" +
                                        "<td><a href='#'>" + data.titulo.usuarioAutor + "</a></td>" +
                                        "</tr>"
                                    ;
                                $('#taxas').append(tr);
                                tableTaxas = tableTaxasInit();
                            }
                        }

                        if (data.cheques !== undefined) {
                            tableCheques.fnDestroy();

                            for (var i = 0; i < data.cheques.length; i++) {
                                $("tr[data-chequeId='" + data.cheques[i].cheque_id + "']").remove();
                            }

                            tableCheques = tableChequesInit();
                        }

                        voltaListagem();
                    }
                }
            },
            beforeSend: function () {
                $('#ajaxLoad').css({display: "block"});
            },
            complete: function () {
                $('#ajaxLoad').css({display: "none"});
            },
            error: function (e) {
            }
        });
    };

    var cancelarPagamentoTitulo = function () {
        if (configuracoes.alunoDesligado) {
            return false;
        }

        var form = $("#viewHistorico").find("form:visible");
        var resposta = confirm("Confirma o cancelamento do titulo?");
        if (resposta === false) {
            return false;
        }

        var post = $(form).serialize();

        $.ajax({
            url: configuracoes.basePath + '/boleto-bancario/financeiro/cancela-pagamento-titulo',
            method: 'POST',
            dataType: 'json',
            data: post,
            success: function (data) {
                if (data.result !== undefined) {
                    if (data.result === true) {
                        alert(data.msg);
                        tableHistoricoFinanceiro.fnDestroy();
                        tableHistoricoCancelamento.fnDestroy();

                        var $tr = $(elementEdit.historicoFinanceiro).clone();

                        $tr.find('td:eq(2)').html('<a href="#">' + data.titulo.tituloDataProcessamento + '</a>');
                        $tr.find('td:eq(5)').html('<a href="#">' + data.titulo.usuarioAutor + '</a>');
                        $tr.find('td:eq(4)').html('<a href="#">Cancelado</a>');
                        $tr.find('td:eq(6)').remove();
                        $tr.attr('onclick', "financeiro.buscaHistoricoTitulo(this,true)");

                        $('#historicoCancelamento').append($tr);
                        $(elementEdit.historicoFinanceiro).remove();
                        tableHistoricoFinanceiro = tableHistoricoFinanceiroInit();
                        tableHistoricoCancelamento = tableHistoricoCancelamentoInit();

                        if (data.titulo !== undefined) {
                            if (data.titulo.tipotituloNome == "Mensalidade") {
                                var desconto = 0;
                                var acrescimo = 0;

                                if (data.titulo.tituloDesconto !== undefined) {
                                    desconto = data.titulo.tituloDesconto;
                                }

                                if (data.titulo.acrescimos !== undefined) {
                                    acrescimo = data.titulo.acrescimos.valorAcrescimos;
                                }

                                var tr =
                                        "<tr data-ativo='false' onclick='financeiro.buscaMensalidade(this)' data-tituloId='" +
                                        data.titulo.tituloId + "'>" +
                                        "<td><a href='#'>" + data.titulo.tituloMesVencimento + "</a></td>" +
                                        "<td><a href='#'>" + data.titulo.tituloDataVencimento + "</a></td>" +
                                        "<td><a href='#'>" +
                                        parseFloat(data.titulo.tituloValor).formatMoney(2, ',', '.') + "</a></td>" +
                                        "<td><a href='#'>" + parseFloat(desconto).formatMoney(2, ',', '.') +
                                        "</a></td>" +
                                        "<td><a href='#'>" + parseFloat(acrescimo).formatMoney(2, ',', '.') +
                                        "</a></td>" +
                                        "<td><a href='#'>" +
                                        parseFloat(data.titulo.valorReceber).formatMoney(2, ',', '.') + "</a></td>" +
                                        "</tr>"
                                    ;
                                $('#tableMensalidades').prepend(tr);
                            } else {
                                tableTaxas.fnDestroy();
                                var tr =
                                        "<tr onclick='financeiro.buscaTaxa(this)' data-tituloId='" +
                                        data.titulo.tituloId + "'>" +
                                        "<td><a href='#'>" + data.titulo.tipotituloNome + "</a></td>" +
                                        "<td><a href='#'>" + data.titulo.tituloDataVencimento + "</a></td>" +
                                        "<td><a href='#'>" + data.titulo.tituloValor + "</a></td>" +
                                        "<td><a href='#'>" + data.titulo.usuarioAutor + "</a></td>" +
                                        "</tr>"
                                    ;
                                $('#taxas').append(tr);
                                tableTaxas = tableTaxasInit();
                            }
                        }

                        if (data.cheques !== undefined) {
                            tableCheques.fnDestroy();
                            for (var i = 0; i < data.cheques.length; i++) {
                                $("tr[data-chequeId='" + data.cheques[i].cheque_id + "']").remove();
                            }
                            tableCheques = tableChequesInit();
                        }

                        voltaListagem();
                    }
                }
            },
            beforeSend: function () {
                $('#ajaxLoad').css({display: "block"});
            },
            complete: function () {
                $('#ajaxLoad').css({display: "none"});
            },
            error: function (e) {
            }
        });
    };

    var cancelarTitulo = function (element) {
        var form = $(element).parents('form');
        var resposta = confirm("Confirma o cancelamento do titulo?");

        if (resposta == false) {
            return false;
        }

        var post = $(form).serialize();

        $.ajax({
            url: configuracoes.basePath + '/boleto-bancario/financeiro/cancela-titulo',
            method: 'POST',
            dataType: 'json',
            data: post,
            success: function (data) {
                if (data.result != undefined) {
                    alert(data.msg);

                    if (data.result == true) {
                        $(elementEdit.titulo).remove();
                        tableTaxas.fnDestroy();
                        $(elementEdit.taxas).remove();
                        tableTaxas = tableTaxasInit();
                        tableHistoricoCancelamento.fnDestroy();

                        var tr =
                            "<tr data-ativo='false' onclick='financeiro.buscaHistoricoTitulo(this,true)' data-tituloId='" +
                            data.titulo.tituloId + "'>" +
                            "<td><a href='#'>" + data.titulo.tipotituloNome + "</a></td>";

                        if (data.titulo.tipotituloNome == "Mensalidade") {
                            tr = tr + "<td><a href='#'>" + data.titulo.tituloMesVencimento + "</a></td>";
                        } else {
                            tr = tr + "<td><a href='#'>" + data.titulo.tituloDataVencimento + "</a></td>";
                        }

                        tr = tr + "<td><a href='#'>" + data.titulo.tituloDataPagamento + "</a></td>" +
                        "<td><a href='#'>" + parseFloat(data.titulo.tituloValor).formatMoney(2, ',', '.') +
                        "</a></td>" +
                        "<td><a href='#'>" + data.titulo.tituloEstado + "</a></td>" +
                        "<td><a href='#'>" + data.titulo.usuarioBaixa + "</a></td>" +
                        "</tr>";

                        $('#historicoCancelamento').append(tr);

                        tableHistoricoCancelamento = tableHistoricoCancelamentoInit();

                        voltaListagem();
                    }
                }
            },
            beforeSend: function () {
                $('#ajaxLoad').css({display: "block"});
            },
            complete: function () {
                $('#ajaxLoad').css({display: "none"});
            },
            error: function (e) {
            }
        });

    };

    var insereDadosViewMensalidade = function (form, data) {
        limpaViewMensalidade(form);
        $(form).find("span[name='numParcelaText']").text(data.mensalidadeParcela + " / " + data.tituloMesVencimento);
        $(form).find("span[name='valorMensalideText']").text(parseFloat(data.tituloValor).formatMoney(2, ',', '.'));
        $(form).find("span[name='valorMensalideAPagarText']").text(
            parseFloat(data.valorReceber).formatMoney(2, ',', '.')
        );
        $(form).find("span[name='tituloDtVencimento']").text(data.tituloDataVencimento);
        $(form).find("input[name='valorReceber']").val(data.valorReceber);
        $(form).find("input[name='tituloDesconto']").val(parseFloat(data.tituloDesconto).formatMoney(2, ',', '.'));
        $(form).find("input[name='tituloId']").val(data.tituloId);
        $(form).find("textarea[name='tituloObservacoes']").val(data.tituloObservacoes);

        var trDescontos = "";
        var totalDescontos;

        if (data.descontos != undefined) {
            for (var i = 0; i < data.descontos.length; i++) {
                if (data.descontos[i].descontoDescontoDiaLimite == null) {
                    data.descontos[i].descontoDescontoDiaLimite = "Sem"
                }

                trDescontos +=
                    "<tr name='descontos'>" +
                    "<td>" +
                    "<span name='nomeDesconto'>" + data.descontos[i].descNome + "</span>" +
                    "</td>" +
                    "<td>" +
                    "<span name='diaVencimento'>" + data.descontos[i].descontoDescontoDiaLimite + "</span>" +
                    "</td>" +
                    "<td>" +
                    "<span name='descontoValor'>" +
                    parseFloat(data.descontos[i].descontoValor).formatMoney(2, ',', '.') + "</span>" +
                    "</td>" +
                    "</tr>";
            }

            $(trDescontos).insertAfter(form.find("tr[name='trDescontoManual']"));
        }

        if (data.acrescimos != undefined) {
            $(form).find("div[name='acrescimosTitulo']").removeAttr('hidden');
            $(form).find("span[name='diasVencidos']").text(data.acrescimos.dias);
            $(form).find("span[name='jurosMulta']").text(data.acrescimos.multa + "% / " + data.acrescimos.jurus + "%");
            $(form).find("span[name='valorAcrescimos']").text(
                parseFloat(data.acrescimos.valorAcrescimos).formatMoney(2, ',', '.')
            );
            $(form).find("input[name='tituloMulta']").val(data.acrescimos.multaCalculado);
            $(form).find("input[name='tituloJuros']").val(data.acrescimos.jurusCalculado);

            $(form).find("input[name='isentarJuros']").attr('data-tmulta', data.acrescimos.multaCalculado);
            $(form).find("input[name='isentarJuros']").attr('data-tjuros', data.acrescimos.jurusCalculado);

            var valorJuros = parseFloat(data.acrescimos.valorAcrescimos);
            $(form).find("input[name='isentarJuros']").attr('data-tvjuros', valorJuros);

        }

        formaPagamentoDinheiro(form, data.valorReceber);

        $(form).find("span[name='totalDescontos']")
            .text(parseFloat(data.tituloDesconto).formatMoney(2, ',', '.'));
        $(form).find("span[name='valorTipoPagamento']")
            .text(parseFloat(data.valorReceber).formatMoney(2, ',', '.'));
        $(form).find("span[name='totalParcial']")
            .text(parseFloat(data.valorReceber).formatMoney(2, ',', '.'));
        $(form).find("td[name='totalBaseAPagar']")
            .attr('data-valBase', parseFloat(data.valorReceber).formatMoney(2, ',', '.'));
        $(form).find("td[name='totalBaseDescontos']")
            .attr('data-valBase', parseFloat(data.tituloDesconto).formatMoney(2, ',', '.'));
    };

    var insereDadosViewTaxas = function (form, data) {
        $(form).find("select[name='tipotitulo']")
            .val(data.tipotitulo).attr('readonly', 'readonly');
        $(form).find("input[name='tituloDataVencimento']")
            .val(data.tituloDataVencimento).attr('readonly', 'readonly');
        $(form).find("input[name='tituloValor']")
            .val(parseFloat(data.tituloValor).formatMoney(2, ',', '.')).attr('readonly', 'readonly');
        $(form).find("input[name='tituloId']").val(data.tituloId);

        formaPagamentoDinheiro(form, data.tituloValor);
    };

    var insereDadosViewMensalidadeHistorico = function (form, data) {
        $(form).find("span[name='numParcelaText']")
            .text(data.mensalidadeParcela + " / " + data.tituloMesVencimento);
        $(form).find("span[name='valorMensalideText']")
            .text(parseFloat(data.tituloValor).formatMoney(2, ',', '.'));
        $(form).find("span[name='valorMensalidePagoText']")
            .text(parseFloat(data.tituloValorPago).formatMoney(2, ',', '.'));
        $(form).find("span[name='tituloDtPagamentoText']")
            .text(data.tituloDataPagamento);
        $(form).find("input[name='tituloId']")
            .val(data.tituloId);
        $(form).find("textarea[name='tituloObservacoes']")
            .val(data.tituloObservacoes);

        if (data.titulo != undefined) {
            var trDescontos = "";

            for (var i = 0; i < data.descontos.length; i++) {
                if (data.descontos[i].descontoDescontoDiaLimite == null) {
                    data.descontos[i].descontoDescontoDiaLimite = "Sem";
                }

                trDescontos +=
                    "<tr name='descontos'>" +
                    "<td>" +
                    "<span name='nomeDesconto'>" + data.descontos[i].descNome + "</span>" +
                    "</td>" +
                    "<td>" +
                    "<span name='diaVencimento'>" + data.descontos[i].descontoDescontoDiaLimite + "</span>" +
                    "</td>" +
                    "<td>" +
                    "<span name='descontoValor'>" +
                    parseFloat(data.descontos[i].descontoValor).formatMoney(2, ',', '.') + "</span>" +
                    "</td>" +
                    "</tr>";
            }

            $(trDescontos).insertAfter(form.find("tr[name='trDescontoManual']"));
        }

        var dataVencimento = data.tituloDataVencimento.split('/');
        var dataPagamento = data.tituloDataPagamento.split('/');
        dataVencimento = new Date(dataVencimento[2], dataVencimento[1] - 1, dataVencimento[0]);
        dataPagamento = new Date(dataPagamento[2], dataPagamento[1] - 1, dataPagamento[0]);

        if (data.acrescimos != undefined && dataPagamento > dataVencimento) {
            $(form).find("div[name='acrescimosTitulo']").removeAttr('hidden');
            $(form).find("span[name='diasVencidos']").text(data.acrescimos.dias);
            $(form).find("span[name='jurosMulta']")
                .text(data.acrescimos.multa + "% / " + data.acrescimos.jurus + "%");
            $(form).find("span[name='valorAcrescimos']")
                .text(parseFloat(data.acrescimos.valorAcrescimos).formatMoney(2, ',', '.'));
            $(form).find("input[name='tituloMulta']").val(data.acrescimos.multaCalculado);
            $(form).find("input[name='tituloJuros']").val(data.acrescimos.jurusCalculado);
        }

        if (data.tituloAcrescimoManual) {
            $(form).find("div[name='acrescimoManual']").removeAttr('hidden');
            $(form).find("span[name='valorAcrescimoManual']").text(
                parseFloat(data.tituloAcrescimoManual).formatMoney(2, ',', '.')
            );
        }

        if (data.tituloDescontoManual || data.tituloDesconto) {
            $(form).find("div[name='descontosTitulo']").removeAttr('hidden');

            data.tituloDescontoManual = data.tituloDescontoManual || 0;
            data.tituloDesconto = (data.tituloDesconto || 0) - data.tituloDescontoManual;

            data.tituloDesconto = (data.tituloDesconto < 0 ) ? 0 : data.tituloDesconto;

            $(form).find("span[name='valorDesconto']")
                .text(parseFloat(data.tituloDesconto).formatMoney(2, ',', '.'));
            $(form).find("span[name='valorDescontoManual']")
                .text(parseFloat(data.tituloDescontoManual).formatMoney(2, ',', '.'));
            $(form).find("span[name='totalDescontos']")
                .text(parseFloat(data.tituloDescontoManual + data.tituloDesconto).formatMoney(2, ',', '.'));
        }

        var formaPagamento =
            "<tr>" +
            "<td>" +
            "<span name='titpoPagamento'>" + data.tituloTipoPagamento + "</span>" +
            "</td>" +
            "<td>" +
            "<span name='valorTipoPagamento'>" + parseFloat(data.tituloValorPago).formatMoney(2, ',', '.') +
            "<span>" +
            "</td>" +
            "</tr>";
        $(form).find("tbody[name='tableFormaPagamento']").append(formaPagamento);

        if (data.tituloEstado == 'Cancelado' || data.tituloEstado == 'Estorno') {
            form.find('[name=estorno]').hide();
        } else {
            form.find('[name=estorno]').show();
        }

        if (configuracoes.alunoDesligado) {
            form.find('[name=estorno]').hide();
        }
    };

    var insereDadosViewTaxaHistorico = function (form, data) {
        $(form).find("select[name='tipotitulo']").val(data.tipotitulo).attr('disabled', 'disabledv');
        $(form).find("input[name='tituloDtPagamento']").val(data.tituloDataPagamento).attr('readonly', 'readonly');
        $(form).find("input[name='tituloValorPago']").val(
            parseFloat(data.tituloValorPago).formatMoney(2, ',', '.')
        ).attr('readonly', 'readonly');
        $(form).find("input[name='tituloId']").val(data.tituloId);

        var formaPagamento =
            "<tr>" +
            "<td>" +
            "<span name='titpoPagamento'>" + data.tituloTipoPagamento + "</span>" +
            "</td>" +
            "<td>" +
            "<span name='valorTipoPagamento'>" + parseFloat(data.tituloValorPago).formatMoney(2, ',', '.') +
            "<span>" +
            "</td>" +
            "</tr>";

        if (data.tituloEstado == 'Cancelado' || data.tituloEstado == 'Estorno') {
            form.find('[name=estorno]').hide();
        } else {
            form.find('[name=estorno]').show();
        }

        if (configuracoes.alunoDesligado) {
            form.find('[name=estorno]').hide();
        }

        $(form).find("tbody[name='tableFormaPagamento']").append(formaPagamento);
    };

    var insereDadosViewCheque = function (form, data) {
        $(form).find("input[name='chequeEmitenteInput']").val(data.cheque.chequeEmitente);
        $(form).find("input[name='chequeNumInput']").val(data.cheque.chequeNum);
        $(form).find("input[name='chequeBancoInput']").val(data.cheque.chequeBanco);
        $(form).find("input[name='chequeAgenciaInput']").val(data.cheque.chequeAgencia);
        $(form).find("input[name='chequePracaInput']").val(data.cheque.chequePraca);
        $(form).find("input[name='chequeEmissaoInput']").val(data.cheque.chequeEmissao);
        $(form).find("input[name='chequeVencimentoInput']").val(data.cheque.chequeVencimento);
        $(form).find("input[name='chequeValorInput']").val(data.cheque.chequeValorMoeda);

        var row = "";
        var totalUtilizado = 0;

        for (var i = 0; i < data.titulos.length; i++) {
            row = row +
            "<tr>" +
            "<td>" +
            "<a href='#'>" + data.titulos[i].matricula + "</a>" +
            "</td>" +
            "<td>" +
            "<a href='#'>" + data.titulos[i].tituloId + "</a>" +
            "</td>" +
            "<td>" +
            "<a href='#'>" + parseFloat(data.titulos[i].valorUtilizado).formatMoney(2, ',', '.') + "</a>" +
            "</td>" +
            "</tr>";
            totalUtilizado = totalUtilizado + data.titulos[i].valorUtilizado;
        }

        $(form).find("tbody[name='titulosPagosComEsteCheque']").prepend(row);
        $(form).find("span[name='totalUtilizado']").text(parseFloat(totalUtilizado).formatMoney(2, ',', '.'));

    };

    var alteraValorFomaPagamentoTaxa = function (element) {
        var form = $(element).parents("form");
        var select = $(form).find("select[name='tipotitulo']").val();

        if (select == "NULL") {
            $(form).find("select[name='tipotitulo']").focus();
            alert("Favor selecionar uma taxa para continuar");
            return false;
        }

        var valor = $(element).val().replace('.', '').replace(',', '.');

        if (valor == "0,00") {
            $(form).find("select[name='tipotitulo']:visible")
        }

        $(form).find("tbody[name='tableFormaPagamento']").children().detach();
        formaPagamentoDinheiro(form, valor);

    };

    var alteraValorParaTaxa = function (element) {
        var form = $(element).parents("form");
        var selected = element.options[element.selectedIndex];
        var valor = parseFloat($(selected).attr('data-valortaxa')).formatMoney(2, ',', '.');
        $(form).find("input[name='tituloValor']").val(valor);
        $(form).find("tbody[name='tableFormaPagamento']").children().detach();
        formaPagamentoDinheiro(form, valor);
    };

    var formaPagamentoDinheiro = function (form, value) {
        var formaPagamento =
            "<tr data-edit='false'>" +
            "<td>" +
            "<a class='fa fa-edit' onclick='financeiro.editaFormaPagameto(this)'> Editar</a> |" +
            "<a class='fa fa-trash-o' onclick='financeiro.removeFormaPagameto(this)'> Remover</a>" +
            "</td>" +
            "<td>" +
            "<span name='titpoPagamento'>Dinheiro</span>" +
            "</td>" +
            "<td>" +
            "<span name='valorTipoPagamento'>" + parseFloat(value).formatMoney(2, ',', '.') + "<span>" +
            "</td>" +
            "<input type='hidden' name='tituloTipoPagamento[]' value='Dinheiro'>" +
            "<input type='hidden' name='valorFormaPagamento[]' value='" + parseFloat(value).toFixed(2) + "'>" +
            "</tr>" +
            "<tr>" +
            "<td></td>" +
            "<td><strong>Total a Pagar R$: </strong></td>" +
            "<td>" + parseFloat(value).formatMoney(2, ',', '.') + "</td>" +
            "</tr>" +
            "<tr>" +
            "<td></td>" +
            "<td><strong>Total Pago R$:</strong></td>" +
            "<td  name='totalBaseAPagar' ><span name='totalParcial'>" + parseFloat(value).formatMoney(2, ',', '.') +
            "</span></td>" +
            "</tr>" +
            "<tr>" +
            "<td></td>" +
            "<td><strong>Troco R$:</strong></td>" +
            "<td  name='totalTroco'><span name='troco'>0,00</span></td>" +
            "</tr>";
        $(form).find("tbody[name='tableFormaPagamento']").append(formaPagamento);
    };

    var calculaTroco = function (form) {
        var total = parseFloat(calculaTotalFormaPagamento());
        var valorBase = clearValue($(form).find("td[name='totalBaseAPagar']").attr('data-valbase'));

        var troco = total - valorBase;

        if (troco > 0) {
            $("span[name='troco']").text(troco.formatMoney(2, ',', '.'));
        } else {
            $("span[name='troco']").text("0,00");
        }
    };

    var limpaViewMensalidade = function (form) {
        //detalhes
        $(form).find("span[name='numParcelaText']").text("");
        $(form).find("span[name='valorMensalideText']").text("");
        $(form).find("span[name='valorMensalideAPagarText']").text("");
        $(form).find("span[name='tituloDtVencimento']").text("");

        //descontos
        $(form).find("tr[name='descontos']").each(function () {
            $(this).remove();
        });
        $(form).find("span[name='totalDescontos']").text("0,00");
        $(form).find("input[name='valorDescontoManual']").val("0,00");
        $(form).find("span[name='totalBaseDescontos']").attr('data-valBase', "");

        //acrescimos
        $(form).find("div[name='acrescimosTitulo']").attr('hidden', 'hidden');
        $(form).find("input[name='valorAcrescimoManual']").val("0,00");
        $(form).find("span[name='diasVencidos']").text("");
        $(form).find("span[name='jurosMulta']").text("");
        $(form).find("span[name='valorAcrescimos']").text("");

        //juros
        $(form).find("input[name='tituloMulta']").val("");
        $(form).find("input[name='tituloJuros']").val("");

        //forma de pagamento
        $(form).find("tbody[name='tableFormaPagamento']").children().detach();

        //observacoes
        $(form).find("textarea[name='tituloObservacoes']").val("");
    };

    var limpaViewTaxas = function (form) {
        //detalhes
        $(form).find("select[name='tipotitulo']").val("").removeAttr('readonly');
        $(form).find("input[name='tituloDataVencimento']").val(today).removeAttr('readonly');
        $(form).find("input[name='tituloValor']").val("").removeAttr('readonly');
        $(form).find("input[name='tituloId']").val("");

        //forma de pagamento
        $(form).find("tbody[name='tableFormaPagamento']").children().detach();

        //observacoes
        $(form).find("textarea[name='tituloObservacoes']").val("");
    };

    var calculaDescontoManual = function (element) {
        var form = $(element).parents('form');
        var input = $(element);
        var val = clearValue(input.val());
        var totalApagar = clearValue($(form).find("td[name='totalBaseAPagar']").attr('data-valBase'));
        var totalDescontos = clearValue($(form).find("td[name='totalBaseDescontos']").attr('data-valBase'));

        if (val > totalApagar) {
            input.focus();
            alert("Valor de desconto maior que o valor a pagar. Favor corrigir e tentar novamente!");
            return false;
        }
        var novoTotalDescontos = totalDescontos + val;

        $(form).find("span[name='totalDescontos']").text(parseFloat(novoTotalDescontos).formatMoney(2, ',', '.'));

        atualizaValorTotal(form);
    };

    var atualizaValorTotal = function (element) {
        var form;

        // verifica se o elemento passado por parametro
        if ($(element).prop('tagName') != "FORM") {
            form = $(element).parents('form');
        } else {
            form = $(element);
        }

        var descontoManual = clearValue($(form).find("input[name='valorDescontoManual']").val()) || 0;
        var acrescimoManual = clearValue($(form).find("input[name='valorAcrescimoManual']").val()) || 0;

        var valorTotal = clearValue($(form).find("td[name='totalBaseAPagar']").attr('data-valBase'));
        var novoTotal = (valorTotal - descontoManual) + acrescimoManual;

        $(form).find("tbody[name='tableFormaPagamento']").children().detach();
        formaPagamentoDinheiro(form, novoTotal);

        novoTotal = parseFloat(novoTotal).formatMoney(2, ',', '.');

        $(form).find("span[name='valorTipoPagamento']").text(novoTotal);
        $(form).find("span[name='totalParcial']").text(novoTotal);
        $(form).find("span[name='valorMensalideAPagarText']").text(novoTotal);
        $(form).find("input[name='valorAPagarMensalidade']").val(novoTotal);

        $(form).find("td[name='totalBaseAPagar']").attr('data-valBase', valorTotal.formatMoney(2, ',', '.'));

        return novoTotal;

    };

    var clearValue = function (value) {
        return parseFloat((value.replace('.', '').replace(',', '.')));
    };

    var salvarFormaPagamento = function () {
        if (validaFormaPagamento()) {
            var form = $("select[name='tipoPagamentoInput']:visible").parents('form');
            var tipoPagamento = $("select[name='tipoPagamentoInput']:visible").val().trim();
            var valorFormaPagamento = $("input[name='valorFormaPagamentoInput']:visible").val();
            valorFormaPagamento = parseFloat(valorFormaPagamento.replace('.', '').replace(',', '.'));

            var count = 0;
            var cheque = "";
            var totalFormaPagamento = calculaTotalFormaPagamento(tipoPagamento);
            var totalPagamento = calculaTotalFormaPagamento();
            var status = confirm("Deseja salvar forma de pagamento");

            if (status == true) {
                if (tipoPagamento == "Dinheiro") {
                    $("tbody[name='tableFormaPagamento']:visible").find('tr').each(function () {
                        $(this).find('td').each(function () {
                            if ($(this).text().trim() == "Dinheiro") {
                                $(this).parent().attr('data-edit', true);
                            }
                        });
                    });
                }

                $("tr[data-edit='true']").remove();

                if (tipoPagamento == "Cheque") {
                    var restantCheque = $("input[name='chequeValorRestanteInputDisabled']").val();
                    valorFormaPagamento = (
                    parseFloat(restantCheque.replace('.', '').replace(',', '.')) || valorFormaPagamento
                    );

                    var valorTitulo = 0;

                    if ($(form).find("input[name='tituloValor']").val() != undefined) {
                        valorTitulo = parseFloat(
                            $(form).find("input[name='tituloValor']")
                                .val().replace('.', '').replace(',', '.')
                        );
                    } else if ($(form).find("span[name='valorMensalideAPagarText']").text() != "") {
                        valorTitulo = parseFloat(
                            $(form).find("span[name='valorMensalideAPagarText']")
                                .text().replace('.', '').replace(',', '.')
                        );
                    } else {
                        valorTitulo = parseFloat(
                            $(form).find("span[name='totalParcial']")
                                .text().replace('.', '').replace(',', '.')
                        );
                    }
                    // valor do titulo menos o valor adicionado ou editado
                    var valorEmAberto = valorTitulo - valorFormaPagamento;

                    // se valor do titulo é maior que o valor da forma de pagamento inserido
                    if (valorEmAberto > 0) {
                        // verifica se valor adicionado mais os valores inseridos anteriormente NÃO são suficientes para
                        // pagar o titulo
                        if ((totalPagamento + parseFloat(valorFormaPagamento)) < valorTitulo) {
                            $("tbody[name='tableFormaPagamento']:visible").find('tr').each(function () {
                                $(this).find('td').each(function (e) {
                                    if ($(this).text().trim() == "Dinheiro") {
                                        $(this).parent().attr('data-edit', true);
                                    }
                                });
                            });

                            $("tbody[name='tableFormaPagamento']:visible").prepend(
                                "<tr data-edit='false'>" +
                                "<td>" +
                                "<a class='fa fa-edit' onclick='financeiro.editaFormaPagameto(this)' > Editar</a> | " +
                                "<a class='fa fa-trash-o' onclick='financeiro.removeFormaPagameto(this)' > Remover</a>" +
                                "</td>" +
                                "<td>" +
                                "Dinheiro" +
                                "</td>" +
                                "<td>" +
                                parseFloat(valorEmAberto).formatMoney(2, ',', '.') +
                                "</td>" +
                                "<input type='hidden' name='tituloTipoPagamento[]' value='Dinheiro'>" +
                                "<input type='hidden' name='valorFormaPagamento[]' value='" + valorEmAberto + "'>" +
                                "</tr>"
                            );

                            $("tr[data-edit='true']").remove();
                        } else {
                            $("tbody[name='tableFormaPagamento']:visible").find('tr').each(function () {
                                if ($(this).find("td").eq(1).text().trim() == 'Dinheiro') {
                                    if (valorEmAberto < 0) {
                                        $(this).remove();
                                    } else {
                                        $(this).find("td").eq(2).text(parseFloat(valorEmAberto).formatMoney(2, ',',
                                            '.'));
                                        $(this).find("input[name='valorFormaPagamento[]']").val(valorEmAberto);
                                    }

                                }
                            });
                        }
                    } else {
                        $("tbody[name='tableFormaPagamento']:visible").find('tr').each(function () {
                            if ($(this).find("td").eq(1).text().trim() == 'Dinheiro') {
                                // if (valorEmAberto < 0) {
                                $(this).remove();
                                // } else {
                                //     de
                                //     $(this).find("td").eq(2).text(parseFloat(valorEmAberto).formatMoney(2, ',', '.'));
                                //     $(this).find("input[name='valorFormaPagamento[]']").val(valorEmAberto);
                                // }
                            }
                        });
                    }

                    cheque =
                        "<input type='hidden' name='chequeBanco[]' value='" +
                        $("select[name='chequeBancoInput']:visible option:selected").val() + "'>" +
                        "<input type='hidden' name='chequeNum[]' value='" +
                        $("input[name='chequeNumInput']:visible").val() + "'>" +
                        "<input type='hidden' name='chequeConta[]' value='" +
                        $("input[name='chequeContaInput']:visible").val() + "'>" +
                        "<input type='hidden' name='chequeEmitente[]' value='" +
                        $("input[name='chequeEmitenteInput']:visible").val() + "'>" +
                        "<input type='hidden' name='chequeAgencia[]' value='" +
                        $("input[name='chequeAgenciaInput']:visible").val() + "'>" +
                        "<input type='hidden' name='chequePraca[]' value='" +
                        $("input[name='chequePracaInput']:visible").val() + "'>" +
                        "<input type='hidden' name='chequeEmissao[]' value='" +
                        $("input[name='chequeEmissaoInput']:visible").val() + "'>" +
                        "<input type='hidden' name='chequeVencimento[]' value='" +
                        $("input[name='chequeVencimentoInput']:visible").val() + "'>" +
                        "<input type='hidden' name='chequeValor[]' value='" + valorFormaPagamento + "'>" +
                        "<input type='hidden' name='chequeId[]' value='" + $("input[name^='chequeIdInput']").val() +
                        "'>" +
                        "<input type='hidden' name='chequeValorUtilizado[]' value='" +
                        $("input[name^='chequeValorUtilizadoInput']").val() + "'>" +
                        "<input type='hidden' name='chequeValorRestante[]' value='" +
                        $("input[name^='chequeValorRestanteInput']").val() + "'>"
                    ;
                    // valores de cheque não podem ser somados, pois são cheques diferentes
                    totalFormaPagamento = 0;
                }

                totalFormaPagamento = totalFormaPagamento + valorFormaPagamento;
                $("tbody[name='tableFormaPagamento']:visible").prepend(
                    "<tr data-edit='false'>" +
                    "<td>" +
                    "<a class='fa fa-edit' onclick='financeiro.editaFormaPagameto(this)' > Editar</a> | " +
                    "<a class='fa fa-trash-o' onclick='financeiro.removeFormaPagameto(this)' > Remover</a>" +
                    "</td>" +
                    "<td>" +
                    tipoPagamento +
                    "</td>" +
                    "<td>" +
                    parseFloat(totalFormaPagamento).formatMoney(2, ',', '.') +
                    "</td>" +
                    "<input type='hidden' name='tituloTipoPagamento[]' value='" + tipoPagamento + "'>" +
                    "<input type='hidden' name='valorFormaPagamento[]' value='" + totalFormaPagamento + "'>" +
                    (cheque || '') +
                    "</tr>"
                );
                limpaFormFormaPagamento(form);
                atualizaTotalizador();
            } else {
                limpaFormFormaPagamento(form);
            }
        }
    };

    var calculaTotalFormaPagamento = function (tipoPagamento) {
        var total = 0;

        // busca por um tipo especifico de forma de pagamento
        if (tipoPagamento != undefined) {
            $("tr[data-edit='false']:visible").each(function () {
                if ($(this).find("input").eq(0).val() == tipoPagamento) {
                    if ($(this).find("input").eq(0).val() == "Cheque") {
                        if ($(this).find("input").eq(13).val() != "" &&
                            $(this).find("input").eq(13).val() != undefined) {
                            total = parseFloat(total) + parseFloat($(this).find("input").eq(13).val());
                        } else {
                            total = parseFloat(total) + parseFloat($(this).find("input").eq(1).val());
                        }
                    } else {
                        total = parseFloat(total) + parseFloat($(this).find("input").eq(1).val());
                    }
                }
            });
            return total;
        }

        $("tr[data-edit='false']:visible").each(function () {
            if ($(this).find("input").eq(0).val() == "Cheque") {
                if ($(this).find("input").eq(13).val() != "" && $(this).find("input").eq(13).val() != undefined) {
                    total = parseFloat(total) + parseFloat($(this).find("input").eq(13).val());
                } else {
                    total = parseFloat(total) + parseFloat($(this).find("input").eq(1).val());
                }
            } else {
                total = parseFloat(total) + parseFloat($(this).find("input").eq(1).val());
            }
        });

        return total;

    };

    var validaFormaPagamento = function () {
        var tituloTipoPagamento = $("select[name='tipoPagamentoInput']:visible");
        var valorFormaPagamento = parseFloat(
            $("input[name='valorFormaPagamentoInput']:visible").val().replace('.', '').replace(',', '.')
        ).toFixed(2);

        if (tituloTipoPagamento.val() == "NULL") {
            alert("Selecione uma forma de !");
            tituloTipoPagamento.focus();
            return false
        }

        if (valorFormaPagamento <= 0) {
            alert("Favor inserir um valor valido!");
            $("input[name='valorFormaPagamentoInput']:visible").focus();
            return false;
        }

        var totalParcial = parseFloat(
            $("span[name='valorMensalideAPagarText']:visible").text().replace('.', '').replace(',', '.')
        );
        var total = 0;

        total = calculaTotalFormaPagamento();

        if (tituloTipoPagamento.val() == "Cheque") {
            return validaFormaPagamentoCheque();
        }
        return true
    };

    var validaFormaPagamentoCheque = function () {
        var chequeEmitente = $("input[name='chequeEmitenteInput']:visible");
        var chequeNum = $("input[name='chequeNumInput']:visible");
        var chequeBanco = $("select[name='chequeBancoInput']:visible option:selected");
        var chequeAgencia = $("input[name='chequeAgenciaInput']:visible");
        var chequePraca = $("input[name='chequePracaInput']:visible");
        var chequeEmissao = $("input[name='chequeEmissaoInput']:visible");
        var chequeVencimento = $("input[name='chequeVencimentoInput']:visible");
        var chequeValor = $("input[name='valorFormaPagamentoInput']:visible");

        if (chequeValor.val().trim() == "") {
            alert("Favor Preencher o Valor do cheque corretamente!");
            chequeValor.focus();
            return false;
        }

        if (chequeEmitente.val().trim() == "") {
            alert("Favor Preencher o campo Emitente corretamente!");
            chequeEmitente.focus();
            return false;
        }

        if (chequeNum.val().trim() == "") {
            alert("Favor Preencher o campo N° cheque corretamente!");
            chequeNum.focus();
            return false;
        }
        if (chequeBanco.val() == "") {
            alert("Favor Preencher o campo Banco corretamente!");
            chequeBanco.focus();
            return false;
        }
        if (chequeAgencia.val().trim() == "") {
            alert("Favor Preencher o campo Agencia corretamente!");
            chequeAgencia.focus();
            return false;
        }
        if (chequePraca.val().trim() == "") {
            alert("Favor Preencher o campo Praça corretamente!");
            chequePraca.focus();
            return false;
        }
        if (chequeEmissao.val().trim() == "") {
            alert("Favor Preencher o campo Emissão corretamente!");
            chequeEmissao.focus();
            return false;
        }
        if (chequeVencimento.val().trim() == "") {
            alert("Favor Preencher o campo Vencimento corretamente!");
            chequeVencimento.focus();
            return false;
        }

        if (!buscaChequeEuristica('salvando')) {
            return false;
        }

        return true;
    };

    var verificaFormaPagamento = function () {
        var ativo = $("#tituloTipoPagamento:visible");
        if (ativo.html() != undefined) {
            salvarFormaPagamento();
        }
    };

    var criaFormaPagamento = function (element) {
        var total = calculaTotalFormaPagamento();
        var form = $(element).parents('form');

        var totalMensalidade = parseFloat(
            $(form).find("span[name='valorMensalideAPagarText']").text().replace('.', '').replace(',', '.')
        );

        if (total >= totalMensalidade) {
            alert("Impossível adicionar forma de pagamento pois o tilulo ja está com o valor cheio! Favor editar a forma de pagamento atual.");
            return false;
        } else {
            var form = $(element).parents('form');

            $(form).find("select[name='tipoPagamentoInput']").each(function () {
                // seta tipo de pagamento para dinheiro
                $(this).val('Dinheiro');
                $(this).trigger('change');
            });

            limpaFormFormaPagamento(form);
            exibeFormaPagamento(element);
        }
    };

    var exibeFormaPagamento = function (element) {
        var form = $(element).parents('form');

        var tipoPagamento = $(element).parent().next().text().trim();

        // $("select[name^='tipoPagamentoInput']:visible option:selected").val(tipo);
        $(form).find("div[name='btAdicionarFormaPagamento']").hide();

        $(form).find("div[name='divFormaPagamento']").show();
        $(form).find("select[name^='tipoPagamentoInput']").val(tipoPagamento);
        $(form).find("input[name='chequePracaInput']").val('GOV. VALADARES');
        $(form).find("input[name='chequeEmitenteInput']").val($("input[name='pesNome']").val());
        exibeFormTipo(element);
    };

    var editaFormaPagameto = function (element) {
        exibeFormaPagamento(element);

        var infoPagamento = $(element).parent().parent();
        var tituloTipoPagamento = $("select[name='tipoPagamentoInput']:visible");

        infoPagamento.attr('data-edit', true);

        var valor = infoPagamento.find('td').eq(2).text().trim();

        $("input[name='valorFormaPagamentoInput']:visible").val(valor);
        tituloTipoPagamento.val(infoPagamento.find('td').eq(1).text().trim());


        $("input[name='chequeEmitenteInput']:visible").val($("input[name='pesNome']").val());
        $("input[name='chequePracaInput']:visible").val('GOV. VALADARES');

        if (tituloTipoPagamento.val().trim() == "Cheque") {
            $("select[name^='chequeBancoInput']:visible").val(infoPagamento.find('input').eq(2).val());
            $("input[name^='chequeNumInput']:visible").val(infoPagamento.find('input').eq(3).val());
            $("input[name^='chequeContaInput']:visible").val(infoPagamento.find('input').eq(4).val());
            $("input[name^='chequeEmitenteInput']:visible").val(infoPagamento.find('input').eq(5).val());
            $("input[name^='chequeAgenciaInput']:visible").val(infoPagamento.find('input').eq(6).val());
            $("input[name^='chequePracaInput']:visible").val(infoPagamento.find('input').eq(7).val());
            $("input[name^='chequeEmissaoInput']:visible").val(infoPagamento.find('input').eq(8).val());
            $("input[name^='chequeVencimentoInput']:visible").val(infoPagamento.find('input').eq(9).val());
            $("input[name^='chequeValorInput']:visible").val(infoPagamento.find('input').eq(10).val());

            if (infoPagamento.find('input').eq(11) != undefined) {
                $("input[name^='chequeIdInput']").val(infoPagamento.find('input').eq(11).val());
                $("input[name^='chequeValorUtilizadoInput']").val(infoPagamento.find('input').eq(12).val());
                $("input[name^='chequeValorRestanteInput']").val(infoPagamento.find('input').eq(13).val());
                $("input[name^='chequeValorRestanteInputDisabled']")
                    .val(parseFloat(infoPagamento.find('input').eq(13).val()).formatMoney(2, ',', '.'));
                $("div[name^='divChequeValorRestante']").show()
            } else {
                $("div[name^='divChequeValorRestante']").hide()
            }
        } else {
            $("#tipoCheque").hide();
        }
    };

    var removeFormaPagameto = function (element) {
        var count = 0;

        $("tbody[name='tableFormaPagamento']:visible").find('tr').each(function () {
            count += 1;
        });

        if (count < 3) {
            alert("Impossível excluir esse tipo de pagamento pois este é o tipo padrão, favor editar o memso caso necessite!");
            return false;
        }

        var confirme = window.confirm("Deseja realmente exlcuir essa forma de pagamento? Atenção as alteracoes só serão realizadas após salvar o titulo!");
        if (confirme) {
            $(element).parent().parent().detach();
        }
        atualizaTotalizador();
    };

    var exibeFormTipo = function (element) {
        var form = $(element).parents("form");

        if ($("select[name^='tipoPagamentoInput']:visible option:selected").val() == 'Cheque') {
            $(form).find("div[name='tipoCheque']").show();
            $(form).find("div[name='divChequeValorRestante']").hide();
        } else {
            $(form).find("div[name='tipoCheque']").hide();
        }
    };

    var limpaFormFormaPagamento = function (form) {
        $("tr[data-edit^='true']").removeAttr('data-edit');
        $("div[name^='btAdicionarFormaPagamento']").show();

        if (form) {
            $(form).find("select[name^='tipoPagamentoInput']").each(function () {
                $(this).prop('selected', false);
            });

            $(form).find("select[name^='tipoPagamentoInput']").val("Dinheiro").trigger('change');
            $(form).find("input[name^='valorFormaPagamentoInput']").val("");
            $(form).find("input[name^='chequeValorRestanteInputDisabled']").val("");
            $(form).find("input[name^='chequeEmitenteInput']").val("");
            $(form).find("input[name^='chequeNumInput']").val("");
            $(form).find("select[name^='chequeBancoInput']").val("");
            $(form).find("input[name^='chequeAgenciaInput']").val("");
            $(form).find("input[name^='chequePracaInput']").val("");
            $(form).find("input[name^='chequeEmissaoInput']").val(today);
            $(form).find("input[name^='chequeVencimentoInput']").val(today);
            $(form).find("input[name^='chequeValorInput']").val("");
            $(form).find("input[name^='chequeContaInput']").val("");
            $(form).find("select[name^='tipoPagamentoInput']").trigger('click');
            $(form).find("div[name^='divFormaPagamento']").hide();
        } else {
            $("select[name^='tipoPagamentoInput']:visible").each(function () {
                $(this).prop('selected', false);
            });

            $("select[name^='tipoPagamentoInput']:visible").val("Dinheiro").trigger('change');

            $("input[name^='valorFormaPagamentoInput']:visible").val("");
            $("input[name^='chequeValorRestanteInputDisabled']:visible").val("");
            $("input[name^='chequeEmitenteInput']:visible").val("");
            $("input[name^='chequeNumInput']:visible").val("");
            $("select[name^='chequeBancoInput']:visible").val("");
            $("input[name^='chequeAgenciaInput']:visible").val("");
            $("input[name^='chequePracaInput']:visible").val("");
            $("input[name^='chequeEmissaoInput']:visible").val(today);
            $("input[name^='chequeVencimentoInput']:visible").val(today);
            $("input[name^='chequeValorInput']:visible").val("");
            $("input[name^='chequeContaInput']:visible").val("");
            $("select[name^='tipoPagamentoInput']:visible").trigger('click');
            $("div[name^='divFormaPagamento']:visible").hide();
        }

    };

    var atualizaTotalizador = function () {
        var total = calculaTotalFormaPagamento();
        $("span[name='totalParcial']:visible").text(parseFloat(total).formatMoney(2, ',', '.'));
        var form = $("span[name='totalParcial']:visible").parents('form');

        calculaTroco(form);
    };

    Number.prototype.formatMoney = function (c, d, t) {
        var n = this, c = isNaN(c = Math.abs(c)) ?
                2 :
                c, d = d == undefined ? "," :
                d, t = t == undefined ? "." :
                t, s = n < 0 ? "-" :
                "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s +
            (j ? i.substr(0, j) + t : "") +
            i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) +
            (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };

    var voltaListagem = function (element) {
        $($("#myTab1 li.active a").attr("href")).children().eq(1).hide();
        $($("#myTab1 li.active a").attr("href")).children().eq(0).show();
    };

    var exibeSegundaView = function (element) {
        var abaAtual = $("#myTab1 li.active a").text();

        if (abaAtual == 'Taxas') {
            elementEdit.taxas = element;
        } else if (abaAtual == 'Historico Financeiro') {
            elementEdit.historicoFinanceiro = element;
        } else if (abaAtual == 'Cancelamentos e estornos') {
            elementEdit.historicoCancelamento = element;
        } else if (abaAtual == 'Descontos') {
            elementEdit.descontos = element;
        } else if (abaAtual == 'Cheques') {
            elementEdit.cheques = element;
        } else {
            elementEdit.titulo = element;
        }

        $($("#myTab1 li.active a").attr("href")).children().eq(0).hide();
        $($("#myTab1 li.active a").attr("href")).children().eq(1).show();
    };

    var adicionaTaxa = function (element) {
        $("#formTaxa").attr('action', 'javascript:financeiro.criaTaxa()');
        $("#sectionQuitarAgora").removeAttr('hidden');
        limpaViewTaxas();
        exibeSegundaView(element);
        $("#cancelaTaxa").hide();
    };

    var procuraAluno = function () {
        var aluno = $("#formBusca").find("input[name='aluno']").val();
        if (aluno != "") {
            window.location.replace('/boleto-bancario/financeiro/index/' + aluno);
        }
    };

    var geraLinhaChequeRetornoAjax = function (cheques) {
        var tr = "";
        for (var i = 0; i < cheques.length; i++) {
            tr = tr +
            "<tr  onclick='financeiro.buscaCheque(this)' data-chequeId='" + cheques[i].cheque_id + "' >" +
            "<td>" +
            "<a href='#'>" + cheques[i].cheque_emitente + "</a>" +
            "</td>" +
            "<td>" +
            "<a href='#'>" + cheques[i].cheque_num + "</a>" +
            "</td>" +
            "<td>" +
            "<a href='#'>" + cheques[i].cheque_conta + "</a>" +
            "</td>" +
            "<td>" +
            "<a href='#'>" + cheques[i].cheque_banco + "</a>" +
            "</td>" +
            "<td>" +
            "<a href='#'>" + cheques[i].cheque_agencia + "</a>" +
            "</td>" +
            "<td>" +
            "<a href='#'>" + cheques[i].cheque_valor + "</a>" +
            "</td>" +
            "</tr>";
        }
        tableCheques.fnDestroy();
        $('#cheques').append(tr);
        tableCheques = tableChequesInit();
    };

    var geraLinhaHistoricoRetornoAjax = function (titulo) {
        var tr =
            "<tr onclick='financeiro.buscaHistoricoTitulo(this)' data-tituloId='" + titulo.tituloId + "'>" +
            "<td><a href='#'>" + titulo.tipotituloNome + "</a></td>" +
            "<td><a href='#'>" + titulo.tituloMesVencimento + "</a></td>" +
            "<td><a href='#'>" + titulo.tituloDataPagamento + "</a></td>" +
            "<td><a href='#'>" + parseFloat(titulo.tituloValor).formatMoney(2, ',', '.') + "</a></td>" +
            "<td><a href='#'>" + parseFloat(titulo.tituloValorPago).formatMoney(2, ',', '.') + "</a></td>" +
            "<td><a href='#'>" + titulo.usuarioBaixa + "</a></td>" +
            "<td><a href='" + configuracoes.basePath + "/boleto-bancario/financeiro/comprovante-pagamento/" + $("input[name='matricula']").val() +
            '/' + $("input[name='alunoperId']").val() + "/" + titulo.tituloId +
            "/1' target='_blank'><i class='fa fa-print fa-2'></i></a></td>" +
            "</tr>";

        tableHistoricoFinanceiro.fnDestroy();
        $('#historicoFinanceiro').append(tr);
        tableHistoricoFinanceiro = tableHistoricoFinanceiroInit();
    };

    var adicionarDesconto = function () {
        window.location.href = configuracoes.basePath + '/boleto-bancario/financeiro-desconto/add/' + $("input[name='alunoperId']").val() + '/financeiro';
    };

    return {
        init: init,
        buscaMensalidade: buscaMensalidade,
        voltaListagem: voltaListagem,
        exibeSegundaView: exibeSegundaView,
        exibeFormaPagamento: exibeFormaPagamento,
        editaFormaPagameto: editaFormaPagameto,
        removeFormaPagameto: removeFormaPagameto,
        exibeFormTipo: exibeFormTipo,
        salvarFormaPagamento: salvarFormaPagamento,
        calculaDescontoManual: calculaDescontoManual,
        atualizaValorTotal: atualizaValorTotal,
        limpaFormFormaPagamento: limpaFormFormaPagamento,
        quitaMensalidade: quitaMensalidade,
        verificaFormaPagamento: verificaFormaPagamento,
        buscaTaxa: buscaTaxa,
        alteraValorParaTaxa: alteraValorParaTaxa,
        alteraValorFomaPagamentoTaxa: alteraValorFomaPagamentoTaxa,
        criaTaxa: criaTaxa,
        adicionaTaxa: adicionaTaxa,
        quitaTaxa: quitaTaxa,
        buscaHistoricoTitulo: buscaHistoricoTitulo,
        estornoTitulo: estornoTitulo,
        buscaCheque: buscaCheque,
        buscaChequeEuristica: buscaChequeEuristica,
        criaFormaPagamento: criaFormaPagamento,
        salvaObservacao: salvaObservacao,
        procuraAluno: procuraAluno,
        cancelarPagamentoTitulo: cancelarPagamentoTitulo,
        cancelarTitulo: cancelarTitulo,
        adicionarDesconto: adicionarDesconto,
        abrirModalNovoTitulo: novoTitulo.abrirModal
    };
})(jQuery, window, document);