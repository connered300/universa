/**
 * Created by lucasrafael on 16/09/15.
 */
$(document).ready(function(){
    TableTools.BUTTONS.add_taxa = $.extend( true, {}, TableTools.buttonBase, {
        "sAction":"div",
        "sTag":"default",
        "sToolTip":"Adicionar",
        "sNewLine": "<br>",
        "sButtonText": "Fechamento de caixa",
        //"sDiv": "",
        "fnClick": function( nButton, oConfig ) {
            window.location.href = '/boleto-bancario/financeiro-titulo/relatorio';
        }
    });

    var tableList = $("#listAluno").dataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "/boleto-bancario/financeiro/listagem-alunos",
            type: "POST",
            async: false,
            dataSrc: function (json) {
                var data = json.data;
                var action = json.action;
                var basePath = "";
                for (var row in data) {
                    var alunoperId = data[row]['alunoper_id'];
                    for (var j in data[row]) {
                        data[row][j] = "<a href='/boleto-bancario/financeiro/dados-aluno/" + alunoperId + "'>" +
                            data[row][j] +
                            "</a>";
                    }
                }
                return data;
            }
        },
        columnDefs: [
            {name: "matricula", targets: 0, data: "matricula"},
            {name: "nome", targets: 1, data: "nome"},
            {name: "turma", targets: 2, data: "turma"},
            {name: "periodo", targets: 4, data: "periodo"},
            {name: "situacao", targets: 3, data: "situacao"}

        ],
        // Tabletools options:
        //   https://datatables.net/extensions/tabletools/button_options
        "sDom": "<'dt-toolbar'<'col-xs-6'f><'col-xs-6'T>r>" +
        "t" +
        "<'dt-toolbar-footer'<'col-xs-6'i><'col-xs-6'p>>",
        "oTableTools": {
            "aButtons": [
                {
                    "sExtends":    "add_taxa",
                    "sDiv":        "addTaxa"
                }
            ],
            "sSwfPath": "/smart-admin/js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
        },
        oLanguage: {
            sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
            sLengthMenu: "Mostrar _MENU_ registros por página",
            sZeroRecords: "Nenhum registro encontrado",
            sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
            sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
            sInfoFiltered: "(filtrado de _MAX_ registros)",
            oPaginate: {
                sFirst: "Início",
                sPrevious: "Anterior",
                sNext: "Próximo",
                sLast: "Último"
            }
        }
    });

    $("input[type^='search']").focus();
    var aluno = $("input[name^='alunocursoId']").val();
    if(aluno != ""){
        $("input[type^='search']").val(aluno).keyup();
    }
    $("tr").on('click',  function(){
        var href = $(this).children().find("a").attr('href');
        if(href != undefined){
            window.location.replace(href);
        }
    });
});

$(window).load(function(){
    $("input[type^='search']").keyup();
    $('#data-inicial, #data-final')
        .datepicker({prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'})
        .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});
});
