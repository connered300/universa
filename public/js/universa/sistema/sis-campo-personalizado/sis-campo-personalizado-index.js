(function ($, window, document) {
    'use strict';
    var SisCampoPersonalizadoIndex = function () {
        VersaShared.call(this);
        var __sisCampoPersonalizadoIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                sisCampoPersonalizado: null
            }
        };

        this.setSteps = function () {
            var $dataTableSisCampoPersonalizado = $('#dataTableSisCampoPersonalizado');
            var colNum = -1;
            __sisCampoPersonalizadoIndex.options.datatables.sisCampoPersonalizado = $dataTableSisCampoPersonalizado.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __sisCampoPersonalizadoIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [
                            {class: 'btn-primary item-edit', icon: 'fa-pencil'},
                            {class: 'btn-danger item-remove', icon: 'fa-times'}
                        ];

                        for (var row in data) {
                            data[row]['acao'] = __sisCampoPersonalizadoIndex.createBtnGroup(btns);
                        }

                        __sisCampoPersonalizadoIndex.removeOverlay($('#container-sis-campo-personalizado'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "campo_id", targets: ++colNum, data: "campo_id"},
                    {name: "campo_nome", targets: ++colNum, data: "campo_nome"},
                    {name: "campo_descricao", targets: ++colNum, data: "campo_descricao"},
                    {name: "campo_tamanho_min", targets: ++colNum, data: "campo_tamanho_min"},
                    {name: "campo_tamanho_max", targets: ++colNum, data: "campo_tamanho_max"},
                    {name: "campo_expressao_regular", targets: ++colNum, data: "campo_expressao_regular"},
                    {name: "campo_valor_padrao", targets: ++colNum, data: "campo_valor_padrao"},
                    {name: "campo_obrigatorio", targets: ++colNum, data: "campo_obrigatorio"},
                    {name: "campo_visivel_painel", targets: ++colNum, data: "campo_visivel_painel"},
                    {name: "campo_exibicao_externa", targets: ++colNum, data: "campo_exibicao_externa"},
                    {name: "campo_ativo", targets: ++colNum, data: "campo_ativo"},
                    {name: "campo_multiplo", targets: ++colNum, data: "campo_multiplo"},
                    {name: "campo_valores", targets: ++colNum, data: "campo_valores"},
                    {name: "campo_id", targets: ++colNum, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $dataTableSisCampoPersonalizado.on('click', '.item-edit', function () {
                var $pai = $(this).closest('tr');
                var data = __sisCampoPersonalizadoIndex.options.datatables.sisCampoPersonalizado.fnGetData($pai);

                if (!__sisCampoPersonalizadoIndex.options.ajax) {
                    location.href = __sisCampoPersonalizadoIndex.options.url.edit + '/' + data['campo_id'];
                } else {
                    __sisCampoPersonalizadoIndex.addOverlay(
                        $('#container-sis-campo-personalizado'), 'Aguarde, carregando dados para edição...'
                    );
                    $.sisCampoPersonalizadoAdd().pesquisaSisCampoPersonalizado(
                        data['campo_id'],
                        function (data) {
                            __sisCampoPersonalizadoIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-sis-campo-personalizado-add').click(function (e) {
                if (__sisCampoPersonalizadoIndex.options.ajax) {
                    __sisCampoPersonalizadoIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $dataTableSisCampoPersonalizado.on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __sisCampoPersonalizadoIndex.options.datatables.sisCampoPersonalizado.fnGetData($pai);
                var arrRemover = {
                    campoId: data['campo_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de campo personalizado "' + data['campo_descricao'] + '"?',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __sisCampoPersonalizadoIndex.addOverlay(
                                $('#container-sis-campo-personalizado'), 'Aguarde, solicitando remoção de registro de campo personalizado...'
                            );
                            $.ajax({
                                url: __sisCampoPersonalizadoIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __sisCampoPersonalizadoIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de campo personalizado:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __sisCampoPersonalizadoIndex.removeOverlay($('#container-sis-campo-personalizado'));
                                    } else {
                                        __sisCampoPersonalizadoIndex.reloadDataTableSisCampoPersonalizado();
                                        __sisCampoPersonalizadoIndex.showNotificacaoSuccess(
                                            "Registro de campo personalizado removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#sis-campo-personalizado-form'),
                $modal = $('#sis-campo-personalizado-modal');
            var actionForm = $form.attr('action');
            actionForm = actionForm.split('/');

            if (actionForm[actionForm.length - 2] == 'edit') {
                actionForm = actionForm.slice(0, actionForm.length - 1);
            }

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['campoId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['campoId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);

            var tipo = $("#campoTipo"),
                entidade = $("#campoEntidade");

            tipo.select2("val", '').trigger("chagen");
            entidade.select2("val", '').trigger("chagen");

            $form[0].reset();
            $form.deserialize(data);

            $modal.find('.modal-title').html(actionTitle + ' campo personalizado');
            $modal.modal();

            if (data['campoTipoId']) {
                entidade.select2("data",
                    {'id': data['campoEntidadeId'], 'text': data['campoEntidadeDescricao']}).trigger("change");
            }

            if (data['campoEntidadeId']) {
                tipo.select2("data",
                    {'id': data['campoTipoId'], 'text': data['campoTipoDescricao']}).trigger("change");
            }

            __sisCampoPersonalizadoIndex.removeOverlay($('#container-sis-campo-personalizado'));
        };

        this.reloadDataTableSisCampoPersonalizado = function () {
            this.getDataTableSisCampoPersonalizado().api().ajax.reload();
        };

        this.getDataTableSisCampoPersonalizado = function () {
            if (!__sisCampoPersonalizadoIndex.options.datatables.sisCampoPersonalizado) {
                if (!$.fn.dataTable.isDataTable('#dataTableSisCampoPersonalizado')) {
                    __sisCampoPersonalizadoIndex.options.datatables.sisCampoPersonalizado = $('#dataTableSisCampoPersonalizado').DataTable();
                } else {
                    __sisCampoPersonalizadoIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __sisCampoPersonalizadoIndex.options.datatables.sisCampoPersonalizado;
        };

        this.run = function (opts) {
            __sisCampoPersonalizadoIndex.setDefaults(opts);
            __sisCampoPersonalizadoIndex.setSteps();
        };
    };

    $.sisCampoPersonalizadoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.sistema.sis-campo-personalizado.index");

        if (!obj) {
            obj = new SisCampoPersonalizadoIndex();
            obj.run(params);
            $(window).data('universa.sistema.sis-campo-personalizado.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);