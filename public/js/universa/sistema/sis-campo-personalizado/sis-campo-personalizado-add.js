(function ($, window, document) {
    'use strict';

    var SisCampoPersonalizadoAdd = function () {
        VersaShared.call(this);
        var __sisCampoPersonalizadoAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                entidade: '',
                tipo: ''
            },
            data: {},
            value: {},
            datatables: {},
            wizardElement: '#sis-campo-personalizado-wizard',
            formElement: '#sis-campo-personalizado-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            var entidade = $("#campoEntidade"),
                tipo = $("#campoTipo");

            var $campoTamanhoMin = $("#campoTamanhoMin");
            __sisCampoPersonalizadoAdd.inciarElementoInteiro($campoTamanhoMin);

            var $campoTamanhoMax = $("#campoTamanhoMax");
            __sisCampoPersonalizadoAdd.inciarElementoInteiro($campoTamanhoMax);

            $("#campoObrigatorio, #campoVisivelPainel,#campoExibicaoExterna, #campoAtivo,#campoMultiplo").select2();

            entidade.select2({
                allowClear: true,
                language: 'pt-br',
                ajax: {
                    url: __sisCampoPersonalizadoAdd.options.url.entidade,
                    dataType: 'json',
                    method: 'POST',
                    data: function (query) {
                        return {
                            query: query
                        }
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.entidade_descricao;
                            el.id = el.entidade_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            tipo.select2({
                allowClear: true,
                language: 'pt-br',
                ajax: {
                    url: __sisCampoPersonalizadoAdd.options.url.tipo,
                    dataType: 'json',
                    method: 'POST',
                    data: function (query) {
                        return {
                            query: query
                        }
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.tipo_descricao;
                            el.id = el.tipo_campo;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });
        };

        this.setValidations = function () {
            __sisCampoPersonalizadoAdd.options.validator.settings.rules = {
                campoNome: {maxlength: 64, required: true},
                campoDescricao: {maxlength: 255},
                campoTamanhoMin: {maxlength: 2, number: true},
                campoTamanhoMax: {maxlength: 10, number: true},
                campoExpressaoRegular: {maxlength: 255},
                campoObrigatorio: {maxlength: 3},
                campoVisivelPainel: {maxlength: 3},
                campoExibicaoExterna: {maxlength: 3},
                campoAtivo: {maxlength: 3},
                campoMultiplo: {maxlength: 3},
                campoEntidade: {required: true},
                campoTipo: {required: true}
            };
            __sisCampoPersonalizadoAdd.options.validator.settings.messages = {
                campoNome: {maxlength: 'Tamanho máximo: 64!', required: 'Campo obrigatório!'},
                campoDescricao: {maxlength: 'Tamanho máximo: 255!'},
                campoTamanhoMin: {maxlength: 'Tamanho máximo: 2!', number: 'Número inválido!'},
                campoTamanhoMax: {maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!'},
                campoExpressaoRegular: {maxlength: 'Tamanho máximo: 255!'},
                campoObrigatorio: {maxlength: 'Tamanho máximo: 3!'},
                campoVisivelPainel: {maxlength: 'Tamanho máximo: 3!'},
                campoExibicaoExterna: {maxlength: 'Tamanho máximo: 3!'},
                campoAtivo: {maxlength: 'Tamanho máximo: 3!'},
                campoMultiplo: {maxlength: 'Tamanho máximo: 3!'},
                campoEntidade: {required: 'Campo obrigatório'},
                campoTipo: {required: 'Campo obrigatório'}
            };

            $(__sisCampoPersonalizadoAdd.options.formElement).submit(function (e) {
                var ok = $(__sisCampoPersonalizadoAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__sisCampoPersonalizadoAdd.options.formElement);

                if (__sisCampoPersonalizadoAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __sisCampoPersonalizadoAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __sisCampoPersonalizadoAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__sisCampoPersonalizadoAdd.options.listagem) {
                                    $.sisCampoPersonalizadoIndex().reloadDataTableSisCampoPersonalizado();
                                    __sisCampoPersonalizadoAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#sis-campo-personalizado-modal').modal('hide');
                                }

                                __sisCampoPersonalizadoAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaSisCampoPersonalizado = function (campo_id, callback) {
            var $form = $(__sisCampoPersonalizadoAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __sisCampoPersonalizadoAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + campo_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __sisCampoPersonalizadoAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __sisCampoPersonalizadoAdd.removeOverlay($form);
                },
                erro: function () {
                    __sisCampoPersonalizadoAdd.showNotificacaoDanger('Erro desconhecido!');
                    __sisCampoPersonalizadoAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.sisCampoPersonalizadoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.sistema.sis-campo-personalizado.add");

        if (!obj) {
            obj = new SisCampoPersonalizadoAdd();
            obj.run(params);
            $(window).data('universa.sistema.sis-campo-personalizado.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);