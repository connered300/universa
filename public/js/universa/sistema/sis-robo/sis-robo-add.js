(function ($, window, document) {
    'use strict';

    var SisRoboAdd = function () {
        VersaShared.call(this);
        var __sisRoboAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {},
            data: {},
            value: {},
            datatables: {},
            wizardElement: '#sis-robo-wizard',
            formElement: '#sis-robo-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#roboTempoExecucao").inputmask({showMaskOnHover: false, mask: ['9{0,9}'], rightAlign: true});
            $("#roboSituacao").select2({language: 'pt-BR'}).trigger("change");
        };

        this.setValidations = function () {
            __sisRoboAdd.options.validator.settings.rules = {
                roboNome: {maxlength: 45, required: true},
                roboUrl: {required: true},
                roboTempoExecucao: {maxlength: 11, number: true, required: true},
                roboSituacao: {maxlength: 11, required: true},
            };
            __sisRoboAdd.options.validator.settings.messages = {
                roboNome: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                roboUrl: {required: 'Campo obrigatório!'},
                roboTempoExecucao: {
                    maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!', required: 'Campo obrigatório!'
                },
                roboSituacao: {maxlength: 'Tamanho máximo: 11!', required: 'Campo obrigatório!'},
            };

            $(__sisRoboAdd.options.formElement).submit(function (e) {
                var ok = $(__sisRoboAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__sisRoboAdd.options.formElement);

                if (__sisRoboAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __sisRoboAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __sisRoboAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__sisRoboAdd.options.listagem) {
                                    $.sisRoboIndex().reloadDataTableSisRobo();
                                    __sisRoboAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#sis-robo-modal').modal('hide');
                                }

                                __sisRoboAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaSisRobo = function (robo_id, callback) {
            var $form = $(__sisRoboAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __sisRoboAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + robo_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    __sisRoboAdd.removeOverlay($form);

                    if (Object.keys(data).length == 0) {
                        __sisRoboAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }
                },
                erro: function () {
                    __sisRoboAdd.showNotificacaoDanger('Erro desconhecido!');
                    __sisRoboAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.sisRoboAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.sistema.sis-robo.add");

        if (!obj) {
            obj = new SisRoboAdd();
            obj.run(params);
            $(window).data('universa.sistema.sis-robo.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);