(function ($, window, document) {
    'use strict';
    var SisRoboIndex = function () {
        VersaShared.call(this);
        var __sisRoboIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                sisRobo: null
            },
        };

        this.setSteps = function () {
            var colNum = 0;
            __sisRoboIndex.options.datatables.sisRobo = $('#dataTableSisRobo').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __sisRoboIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary sisRobo-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger sisRobo-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                        }

                        __sisRoboIndex.removeOverlay($('#container-sis-robo'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "robo_id", targets: colNum++, data: "robo_id"},
                    {name: "robo_nome", targets: colNum++, data: "robo_nome"},
                    {name: "robo_url", targets: colNum++, data: "robo_url"},
                    {name: "robo_tempo_execucao", targets: colNum++, data: "robo_tempo_execucao"},
                    {name: "robo_ultima_execucao", targets: colNum++, data: "robo_ultima_execucao"},
                    {name: "robo_situacao", targets: colNum++, data: "robo_situacao"},
                    {name: "robo_id", targets: colNum++, data: "acao"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $('#dataTableSisRobo').on('click', '.sisRobo-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __sisRoboIndex.options.datatables.sisRobo.fnGetData($pai);

                if (!__sisRoboIndex.options.ajax) {
                    location.href = __sisRoboIndex.options.url.edit + '/' + data['robo_id'];
                } else {
                    $.sisRoboAdd().pesquisaSisRobo(
                        data['robo_id'],
                        function (data) { __sisRoboIndex.showModal(data); }
                    );
                }
            });


            $('#btn-sis-robo-add').click(function (e) {
                if (__sisRoboIndex.options.ajax) {
                    __sisRoboIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#dataTableSisRobo').on('click', '.sisRobo-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __sisRoboIndex.options.datatables.sisRobo.fnGetData($pai);
                var arrRemover = {
                    roboId: data['robo_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de robô "' + data['robo_nome'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __sisRoboIndex.addOverlay(
                                $('#container-sis-robo'), 'Aguarde, solicitando remoção de registro de robô...'
                            );
                            $.ajax({
                                url: __sisRoboIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __sisRoboIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de robô:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __sisRoboIndex.removeOverlay($('#container-sis-robo'));
                                    } else {
                                        __sisRoboIndex.reloadDataTableSisRobo();
                                        __sisRoboIndex.showNotificacaoSuccess(
                                            "Registro de robô removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#sis-robo-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['roboId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['roboId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);

            $("#roboSituacao").trigger("change");

            $('#sis-robo-modal .modal-title').html(actionTitle + ' robô');
            $('#sis-robo-modal').modal();
        };

        this.reloadDataTableSisRobo = function () {
            this.getDataTableSisRobo().api().ajax.reload(null, false);
        };

        this.getDataTableSisRobo = function () {
            if (!__sisRoboIndex.options.datatables.sisRobo) {
                if (!$.fn.dataTable.isDataTable('#dataTableSisRobo')) {
                    __sisRoboIndex.options.datatables.sisRobo = $('#dataTableSisRobo').DataTable();
                } else {
                    __sisRoboIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __sisRoboIndex.options.datatables.sisRobo;
        };

        this.run = function (opts) {
            __sisRoboIndex.setDefaults(opts);
            __sisRoboIndex.setSteps();
        };
    };

    $.sisRoboIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.sistema.sis-robo.index");

        if (!obj) {
            obj = new SisRoboIndex();
            obj.run(params);
            $(window).data('universa.sistema.sis-robo.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);