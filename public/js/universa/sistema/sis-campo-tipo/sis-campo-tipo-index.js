(function ($, window, document) {
    'use strict';
    var SisCampoTipoIndex = function () {
        VersaShared.call(this);
        var __sisCampoTipoIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                sisCampoTipo: null
            }
        };

        this.setSteps = function () {
            var $dataTableSisCampoTipo = $('#dataTableSisCampoTipo');
            var colNum = -1;
            __sisCampoTipoIndex.options.datatables.sisCampoTipo = $dataTableSisCampoTipo.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __sisCampoTipoIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [
                            {class: 'btn-primary item-edit', icon: 'fa-pencil'},
                            {class: 'btn-danger item-remove', icon: 'fa-times'}
                        ];

                        for (var row in data) {
                            data[row]['acao'] = __sisCampoTipoIndex.createBtnGroup(btns);
                        }

                        __sisCampoTipoIndex.removeOverlay($('#container-sis-campo-tipo'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "tipo_campo", targets: ++colNum, data: "tipo_campo"},
                    {name: "tipo_descricao", targets: ++colNum, data: "tipo_descricao"},
                    {name: "tipo_campo", targets: ++colNum, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $dataTableSisCampoTipo.on('click', '.item-edit', function () {
                var $pai = $(this).closest('tr');
                var data = __sisCampoTipoIndex.options.datatables.sisCampoTipo.fnGetData($pai);

                if (!__sisCampoTipoIndex.options.ajax) {
                    location.href = __sisCampoTipoIndex.options.url.edit + '/' + data['tipo_campo'];
                } else {
                    __sisCampoTipoIndex.addOverlay(
                        $('#container-sis-campo-tipo'), 'Aguarde, carregando dados para edição...'
                    );
                    $.sisCampoTipoAdd().pesquisaSisCampoTipo(
                        data['tipo_campo'],
                        function (data) {
                            __sisCampoTipoIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-sis-campo-tipo-add').click(function (e) {
                if (__sisCampoTipoIndex.options.ajax) {
                    __sisCampoTipoIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $dataTableSisCampoTipo.on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __sisCampoTipoIndex.options.datatables.sisCampoTipo.fnGetData($pai);
                var arrRemover = {
                    tipoCampo: data['tipo_campo']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de campo tipo "' + data['tipo_descricao'] + '"?',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __sisCampoTipoIndex.addOverlay(
                                $('#container-sis-campo-tipo'), 'Aguarde, solicitando remoção de registro de campo tipo...'
                            );
                            $.ajax({
                                url: __sisCampoTipoIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __sisCampoTipoIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de campo tipo:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __sisCampoTipoIndex.removeOverlay($('#container-sis-campo-tipo'));
                                    } else {
                                        __sisCampoTipoIndex.reloadDataTableSisCampoTipo();
                                        __sisCampoTipoIndex.showNotificacaoSuccess(
                                            "Registro de campo tipo removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#sis-campo-tipo-form'),
                $modal = $('#sis-campo-tipo-modal');
            var actionForm = $form.attr('action');
            actionForm = actionForm.split('/');

            if (actionForm[actionForm.length - 2] == 'edit') {
                actionForm = actionForm.slice(0, actionForm.length - 1);
            }

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['tipoCampo']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['tipoCampo'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);

            var tipoOrigin = $("#tipoCampoOrigin"),
                tipoDescOrigin = $("#tipoCampoDescOrigin");

            tipoDescOrigin.val('');
            tipoOrigin.val('');

            $form[0].reset();
            $form.deserialize(data);

            tipoDescOrigin.val(data['tipoDescricao']);
            tipoOrigin.val(data['tipoCampo']);

            $modal.find('.modal-title').html(actionTitle + ' campo tipo');
            $modal.modal();
            __sisCampoTipoIndex.removeOverlay($('#container-sis-campo-tipo'));
        };

        this.reloadDataTableSisCampoTipo = function () {
            this.getDataTableSisCampoTipo().api().ajax.reload();
        };

        this.getDataTableSisCampoTipo = function () {
            if (!__sisCampoTipoIndex.options.datatables.sisCampoTipo) {
                if (!$.fn.dataTable.isDataTable('#dataTableSisCampoTipo')) {
                    __sisCampoTipoIndex.options.datatables.sisCampoTipo = $('#dataTableSisCampoTipo').DataTable();
                } else {
                    __sisCampoTipoIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __sisCampoTipoIndex.options.datatables.sisCampoTipo;
        };

        this.run = function (opts) {
            __sisCampoTipoIndex.setDefaults(opts);
            __sisCampoTipoIndex.setSteps();
        };
    };

    $.sisCampoTipoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.sistema.sis-campo-tipo.index");

        if (!obj) {
            obj = new SisCampoTipoIndex();
            obj.run(params);
            $(window).data('universa.sistema.sis-campo-tipo.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);