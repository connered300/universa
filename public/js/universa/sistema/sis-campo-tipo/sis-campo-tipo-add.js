(function ($, window, document) {
    'use strict';

    var SisCampoTipoAdd = function () {
        VersaShared.call(this);
        var __sisCampoTipoAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {},
            data: {},
            value: {},
            datatables: {},
            wizardElement: '#sis-campo-tipo-wizard',
            formElement: '#sis-campo-tipo-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
        };

        this.setValidations = function () {
            __sisCampoTipoAdd.options.validator.settings.rules = {tipoDescricao: {maxlength: 255, required: true}};
            __sisCampoTipoAdd.options.validator.settings.messages = {
                tipoDescricao: {
                    maxlength: 'Tamanho máximo: 255!',
                    required: 'Campo obrigatório!'
                }
            };

            $(__sisCampoTipoAdd.options.formElement).submit(function (e) {
                var ok = $(__sisCampoTipoAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__sisCampoTipoAdd.options.formElement);

                if (__sisCampoTipoAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __sisCampoTipoAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __sisCampoTipoAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__sisCampoTipoAdd.options.listagem) {
                                    $.sisCampoTipoIndex().reloadDataTableSisCampoTipo();
                                    __sisCampoTipoAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#sis-campo-tipo-modal').modal('hide');
                                }

                                __sisCampoTipoAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaSisCampoTipo = function (tipo_campo, callback) {
            var $form = $(__sisCampoTipoAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __sisCampoTipoAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + tipo_campo,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __sisCampoTipoAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __sisCampoTipoAdd.removeOverlay($form);
                },
                erro: function () {
                    __sisCampoTipoAdd.showNotificacaoDanger('Erro desconhecido!');
                    __sisCampoTipoAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.sisCampoTipoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.sistema.sis-campo-tipo.add");

        if (!obj) {
            obj = new SisCampoTipoAdd();
            obj.run(params);
            $(window).data('universa.sistema.sis-campo-tipo.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);