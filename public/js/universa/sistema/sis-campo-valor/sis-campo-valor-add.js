(function ($, window, document) {
    'use strict';

    var SisCampoValorAdd = function () {
        VersaShared.call(this);
        var __sisCampoValorAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                campo: '',
                chave: ''
            },
            data: {},
            value: {},
            datatables: {},
            wizardElement: '#sis-campo-valor-wizard',
            formElement: '#sis-campo-valor-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            var campo = $("#campoId");

            campo.select2({
                language: 'pt-BR',
                ajax: {
                    url: __sisCampoValorAdd.options.url.campo,
                    dataType: 'json',
                    data: function (query) {
                        return {
                            query: query
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.campo_nome;
                            el.id = el.campo_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });
        };

        this.setValidations = function () {
            __sisCampoValorAdd.options.validator.settings.rules = {
                tipoDescricao: {maxlength: 255, required: true}
            };
            __sisCampoValorAdd.options.validator.settings.messages = {
                tipoDescricao: {
                    maxlength: 'Tamanho máximo: 255!',
                    required: 'Campo obrigatório!'
                }
            };

            $(__sisCampoValorAdd.options.formElement).submit(function (e) {
                var ok = $(__sisCampoValorAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__sisCampoValorAdd.options.formElement);

                if (__sisCampoValorAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    var action = $form.attr('action');
                    action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

                    if (!$form.data('ajax')) {

                        $form.data('ajax', true);
                        __sisCampoValorAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: action + '/edit',
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __sisCampoValorAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__sisCampoValorAdd.options.listagem) {
                                    $.sisCampoValorIndex().reloadDataTableSisCampoValor();
                                    __sisCampoValorAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#sis-campo-valor-modal').modal('hide');
                                }

                                __sisCampoValorAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaSisCampoValor = function (campo_id, callback) {
            var $form = $(__sisCampoValorAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __sisCampoValorAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit?" + campo_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __sisCampoValorAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __sisCampoValorAdd.removeOverlay($form);
                },
                erro: function () {
                    __sisCampoValorAdd.showNotificacaoDanger('Erro desconhecido!');
                    __sisCampoValorAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.sisCampoValorAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.sistema.sis-campo-valor.add");

        if (!obj) {
            obj = new SisCampoValorAdd();
            obj.run(params);
            $(window).data('universa.sistema.sis-campo-valor.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);