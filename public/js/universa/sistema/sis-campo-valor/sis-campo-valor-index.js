(function ($, window, document) {
    'use strict';
    var SisCampoValorIndex = function () {
        VersaShared.call(this);
        var __sisCampoValorIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                sisCampoValor: null
            }
        };

        this.setSteps = function () {
            var $dataTableSisCampoValor = $('#dataTableSisCampoValor');
            var colNum = -1;
            __sisCampoValorIndex.options.datatables.sisCampoValor = $dataTableSisCampoValor.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __sisCampoValorIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [
                            {class: 'btn-primary item-edit', icon: 'fa-pencil'},
                            {class: 'btn-danger item-remove', icon: 'fa-times'}
                        ];

                        for (var row in data) {
                            data[row]['acao'] = __sisCampoValorIndex.createBtnGroup(btns);
                        }

                        __sisCampoValorIndex.removeOverlay($('#container-sis-campo-valor'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "campo_id", targets: ++colNum, data: "campo_id"},
                    {name: "campo_valor", targets: ++colNum, data: "campo_valor"},
                    {name: "campo_chave", targets: ++colNum, data: "campo_chave"},
                    {name: "campo_chave", targets: ++colNum, data: "acao", orderable: false}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $dataTableSisCampoValor.on('click', '.item-edit', function () {
                var $pai = $(this).closest('tr');
                var data = __sisCampoValorIndex.options.datatables.sisCampoValor.fnGetData($pai);

                if (!__sisCampoValorIndex.options.ajax) {
                    location.href = __sisCampoValorIndex.options.url.edit + '?' + 'campo=' + data['campo_id'];
                } else {
                    __sisCampoValorIndex.addOverlay(
                        $('#container-sis-campo-valor'), 'Aguarde, carregando dados para edição...'
                    );
                    $.sisCampoValorAdd().pesquisaSisCampoValor(
                        'campo=' + data['campo_id'] + '&campoChave=' + data['campo_chave'],
                        function (data) {
                            __sisCampoValorIndex.showModal(data);
                        }
                    );
                }
            });

            $('#btn-sis-campo-valor-add').click(function (e) {
                if (__sisCampoValorIndex.options.ajax) {
                    __sisCampoValorIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $dataTableSisCampoValor.on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __sisCampoValorIndex.options.datatables.sisCampoValor.fnGetData($pai);
                var arrRemover = {
                    campo: data['campo_id'],
                    campoChave: data['campo_chave']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de campo valor "' + data['campo_id'] + '"?',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __sisCampoValorIndex.addOverlay(
                                $('#container-sis-campo-valor'), 'Aguarde, solicitando remoção de registro...'
                            );
                            $.ajax({
                                url: __sisCampoValorIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __sisCampoValorIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de campo valor:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __sisCampoValorIndex.removeOverlay($('#container-sis-campo-valor'));
                                    } else {
                                        __sisCampoValorIndex.reloadDataTableSisCampoValor();
                                        __sisCampoValorIndex.showNotificacaoSuccess(
                                            "Registro de campo valor removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });
                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#sis-campo-valor-form'),
                $modal = $('#sis-campo-valor-modal');
            var actionForm = $form.attr('action');
            actionForm = actionForm.split('/');
            var campo = $("#campoId"),
                chave = $("#campoChave");
            /*Campos que armazenam estado anterior*/
            var campoOrigin = $("#campoOrigin"),
                chaveOrigin = $("#chaveOrigin");

            if (actionForm[actionForm.length - 2] == 'edit') {
                actionForm = actionForm.slice(0, actionForm.length - 1);
            }

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['tipoCampo']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['tipoCampo'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);

            campo.select2("val", '').trigger("change");
            campoOrigin.val('');
            chaveOrigin.val('');

            $form[0].reset();
            $form.deserialize(data);

            if (data['campoId']) {
                campo.select2("data", {'id': data['campoId'], 'text': data['campoNome']}).trigger("change");
                campoOrigin.val(data['campoId']);
            }

            if (data['campoChave']) {
                chaveOrigin.val(data['campoChave']);
            }

            $modal.find('.modal-title').html(actionTitle + ' campo valor');
            $modal.modal();
            __sisCampoValorIndex.removeOverlay($('#container-sis-campo-valor'));
        };

        this.reloadDataTableSisCampoValor = function () {
            this.getDataTableSisCampoValor().api().ajax.reload();
        };

        this.getDataTableSisCampoValor = function () {
            if (!__sisCampoValorIndex.options.datatables.sisCampoValor) {
                if (!$.fn.dataTable.isDataTable('#dataTableSisCampoValor')) {
                    __sisCampoValorIndex.options.datatables.sisCampoValor = $('#dataTableSisCampoValor').DataTable();
                } else {
                    __sisCampoValorIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __sisCampoValorIndex.options.datatables.sisCampoValor;
        };

        this.run = function (opts) {
            __sisCampoValorIndex.setDefaults(opts);
            __sisCampoValorIndex.setSteps();
        };
    };

    $.sisCampoValorIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.sistema.sis-campo-valor.index");

        if (!obj) {
            obj = new SisCampoValorIndex();
            obj.run(params);
            $(window).data('universa.sistema.sis-campo-valor.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);