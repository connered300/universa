(function ($, window, document) {
    'use strict';
    var SisConfigPainelIndex = function () {
        VersaShared.call(this);
        var __sisConfigPainelIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            formElement: '#sis-config-painel-form'
        };

        this.setSteps = function () {
            $("[data-toggle^='tooltip']").tooltip();

            $('select[multiple]').each(function (i, item) {
                var $this = $(this);
                var val = $this.data("valor") || [];

                if (val && Object.keys(val).length == 1 && val[0] == "") {
                    val = [];
                }
                $this.select2({
                    allowClear: true

                });

                if (val) {
                    $this.select2('val', val).trigger("change");
                }
            });

            $("select:not(#financeiroGrupo, #financeiroGrupoSupervisao)").select2({
                allowClear: true,
                minimumResultsForSearch: -1
            });

            $("#smsAtivo").on('change', function () {
                var valor = parseInt($(this).val());
                var disabled = false;

                $("#opcoesSms").show();
                $("#agenteBoasVindasSMS").parent().show();
                $("#alunoBoasVindasSMS").parent().show();

                if (!valor) {
                    disabled = false;

                    $("#opcoesSms").hide();
                    $("#agenteBoasVindasSMS").parent().hide();
                    $("#agenteBoasVindasSMS").val(0);

                    $("#alunoBoasVindasSMS").parent().hide();
                    $("#alunoBoasVindasSMS").val(0);
                }

                $("#opcoesSms input").prop("disabled", disabled);
            }).trigger("change");

            $("#alunoGrupo").on("change", function () {
                var valor = $(this).val();

                $("#alunoCriarUsuario").parent().show();

                if (!valor) {
                    $("#alunoCriarUsuario").parent().hide();
                    $("#alunoCriarUsuario").val(0);

                }
            }).trigger("change");

            $("#agenteGrupo").on("change", function () {
                var valor = $(this).val();

                $("#alunoCriarUsuario").parent().show();

                if (!valor) {
                    $("#agenteCriarUsuario").parent().hide();
                    $("#agenteCriarUsuario").val(0);
                }

            }).trigger("change");

            $("#pagseguroAtivo").on("change", function () {
                var valor = parseInt($(this).val());
                var disabled = false;

                $("[id^='pagseguro']:not(#pagseguroAtivo)").parent().show();

                if (!valor) {
                    disabled = true;
                    $("[id^='pagseguro']:not(#pagseguroAtivo)").parent().hide();
                    $("#pagSeguroOpcoes").hide();
                }

                $("[id^='pagseguro']:not(#pagseguroAtivo)").prop("disabled", disabled);
            }).trigger("change");

            var configs = [
                {'id': '__DATA_PAGAMENTO__', 'descricao': 'Data de Vencimento do Título'},
                {'id': '__DATA_PAGAMENTO_ANO__', 'descricao': 'Ano de Vencimento do Título'},
                {'id': '__DATA_PAGAMENTO_DIA__', 'descricao': 'Dia de Vencimento do Título'},
                {'id': '__DATA_PAGAMENTO_MES__', 'descricao': 'Mês  de Vencimento do Título'},
                {'id': '__DATA_PAGAMENTO_MES_NOME__', 'descricao': 'Nome do Mês de Vencimento do Título'},
                {'id': '__PARCELA__', 'descricao': 'Número da Parcela'},
                {'id': '__TIPO__', 'descricao': 'Tipo de Título'},
                {'id': '__TOTAL_PARCELAS__', 'descricao': 'Número Total de Parcelas'},
            ];

            var atConfig = {
                at: "_",
                data: configs,
                headerTpl: '<div class="atwho-header">Variáveis disponíveis<small>↑&nbsp;↓&nbsp;</small></div>',
                insertTpl: '${id}',
                displayTpl: "<li>${descricao} - <span class='text-muted'>${id}</span></li>",
                limit: 200
            };

            $('#financeiroTituloDescricaoPadrao').atwho(atConfig);


        };

        this.setValidations = function () {
            var $form = $(__sisConfigPainelIndex.options.formElement);

            $form.submit(function (e) {
                var ok = $(__sisConfigPainelIndex.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                if (__sisConfigPainelIndex.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();

                    $("select[multiple]").each(function (i, item) {
                        var val = $(item).val() || [];
                        var name = item.name.replace('[]', '');
                        val = Object.keys(val).length > 0 ? val.join(",") : "";
                        dados[name] = val;
                    });

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __sisConfigPainelIndex.addOverlay($('#container-sis-config'), 'Salvando registro. Aguarde...');

                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro['mensagem'] && typeof data.erro['mensagem'] == "Array") {
                                    $.each(data.erro['mensagem'], function (index, msg) {
                                        __sisConfigPainelIndex.showNotificacaoDanger(
                                            msg || "Não foi possível salvar registro!"
                                        );
                                    });
                                } else if (data.erro['mensagem'] && typeof data.erro['mensagem'] == "string") {
                                    __sisConfigPainelIndex.showNotificacaoDanger(
                                        "Não foi possível salvar registro"
                                    );
                                }

                                if (data.success) {
                                    __sisConfigPainelIndex.showNotificacaoSuccess(
                                        "Registro(s) Salvo!"
                                    );
                                }

                                __sisConfigPainelIndex.removeOverlay($('#container-sis-config'));
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.sisConfigPainelIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.sistema.sis-config-painel.index");

        if (!obj) {
            obj = new SisConfigPainelIndex();
            obj.run(params);
            $(window).data('universa.sistema.sis-config-painel.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);