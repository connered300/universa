(function ($, window, document) {
    'use strict';

    var SisFilaIntegracaoAdd = function () {
        VersaShared.call(this);
        var __sisFilaIntegracaoAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {sisIntegracao: ''},
            data: {},
            value: {integracao: null},
            datatables: {},
            wizardElement: '#sis-fila-integracao-wizard',
            formElement: '#sis-fila-integracao-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#integracao").select2({
                language: 'pt-BR',
                ajax: {
                    url: __sisFilaIntegracaoAdd.options.url.sisIntegracao,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.integracao_descricao, id: el.integracao_id};
                        });

                        return {results: transformed};
                    }
                }
            });

            if (__sisFilaIntegracaoAdd.getIntegracao()) {
                $('#integracao').select2("data", __sisFilaIntegracaoAdd.getIntegracao());
            }

            $("#filaSituacao").select2({language: 'pt-BR'}).trigger("change");
        };

        this.setIntegracao = function (integracao) {
            this.options.value.integracao = integracao || null;
        };

        this.getIntegracao = function () {
            return this.options.value.integracao || null;
        };
        this.setValidations = function () {
            __sisFilaIntegracaoAdd.options.validator.settings.rules = {
                integracao: {maxlength: 10, number: true, required: true},
                filaConteudo: {required: true},
                filaSituacao: {maxlength: 11, required: true},
            };
            __sisFilaIntegracaoAdd.options.validator.settings.messages = {
                integracao: {
                    maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!', required: 'Campo obrigatório!'
                },
                filaConteudo: {required: 'Campo obrigatório!'},
                filaSituacao: {maxlength: 'Tamanho máximo: 11!', required: 'Campo obrigatório!'},
            };

            $(__sisFilaIntegracaoAdd.options.formElement).submit(function (e) {
                var ok = $(__sisFilaIntegracaoAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__sisFilaIntegracaoAdd.options.formElement);

                if (__sisFilaIntegracaoAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __sisFilaIntegracaoAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __sisFilaIntegracaoAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__sisFilaIntegracaoAdd.options.listagem) {
                                    $.sisFilaIntegracaoIndex().reloadDataTableSisFilaIntegracao();
                                    __sisFilaIntegracaoAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#sis-fila-integracao-modal').modal('hide');
                                }

                                __sisFilaIntegracaoAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaSisFilaIntegracao = function (fila_id, callback) {
            var $form = $(__sisFilaIntegracaoAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __sisFilaIntegracaoAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + fila_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    __sisFilaIntegracaoAdd.removeOverlay($form);

                    if (Object.keys(data).length == 0) {
                        __sisFilaIntegracaoAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }
                },
                erro: function () {
                    __sisFilaIntegracaoAdd.showNotificacaoDanger('Erro desconhecido!');
                    __sisFilaIntegracaoAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.sisFilaIntegracaoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.sistema.sis-fila-integracao.add");

        if (!obj) {
            obj = new SisFilaIntegracaoAdd();
            obj.run(params);
            $(window).data('universa.sistema.sis-fila-integracao.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);