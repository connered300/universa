(function ($, window, document) {
    'use strict';
    var SisFilaIntegracaoIndex = function () {
        VersaShared.call(this);
        var __sisFilaIntegracaoIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: '',
                executar: '',
                correcaoAutomatica: ''
            },
            datatables: {
                sisFilaIntegracao: null
            },
        };

        this.setSteps = function () {
            var colNum = 0;
            __sisFilaIntegracaoIndex.options.datatables.sisFilaIntegracao = $('#dataTableSisFilaIntegracao').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __sisFilaIntegracaoIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary sisFilaIntegracao-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger sisFilaIntegracao-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                            data[row]['fila_data_cadastro_formatado'] = __sisFilaIntegracaoIndex.formatDateTime(
                                data[row]['fila_data_cadastro']
                            );
                            data[row]['fila_data_processamento_formatado'] = __sisFilaIntegracaoIndex.formatDateTime(
                                data[row]['fila_data_processamento']
                            );
                            data[row]['fila_data_finalizacao_formatado'] = __sisFilaIntegracaoIndex.formatDateTime(
                                data[row]['fila_data_finalizacao']
                            );
                        }

                        __sisFilaIntegracaoIndex.removeOverlay($('#container-sis-fila-integracao'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "fila_id", targets: colNum++, data: "fila_id"},
                    {name: "integracao_descricao", targets: colNum++, data: "integracao_descricao"},
                    {name: "fila_chave", targets: colNum++, data: "fila_chave"},
                    {name: "fila_data_cadastro", targets: colNum++, data: "fila_data_cadastro_formatado"},
                    {name: "fila_data_processamento", targets: colNum++, data: "fila_data_processamento_formatado"},
                    {name: "fila_data_finalizacao", targets: colNum++, data: "fila_data_finalizacao_formatado"},
                    {name: "fila_situacao", targets: colNum++, data: "fila_situacao"},
                    {name: "fila_id", targets: colNum++, data: "acao"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[3, 'desc']]
            });

            $('#dataTableSisFilaIntegracao').on('click', '.sisFilaIntegracao-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __sisFilaIntegracaoIndex.options.datatables.sisFilaIntegracao.fnGetData($pai);

                if (!__sisFilaIntegracaoIndex.options.ajax) {
                    location.href = __sisFilaIntegracaoIndex.options.url.edit + '/' + data['fila_id'];
                } else {
                    __sisFilaIntegracaoIndex.addOverlay(
                        $('#container-sis-fila-integracao'), 'Aguarde, carregando dados para edição...'
                    );
                    $.sisFilaIntegracaoAdd().pesquisaSisFilaIntegracao(
                        data['fila_id'],
                        function (data) { __sisFilaIntegracaoIndex.showModal(data); }
                    );
                }
            });

            $('#btn-sis-fila-integracao-add').click(function (e) {
                if (__sisFilaIntegracaoIndex.options.ajax) {
                    __sisFilaIntegracaoIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#btn-sis-fila-integracao-executar').click(function (e) {
                __sisFilaIntegracaoIndex.addOverlay(
                    $('#container-sis-fila-integracao'), 'Aguarde, executando integração...'
                );
                $.ajax({
                    url: __sisFilaIntegracaoIndex.options.url.executar,
                    type: 'POST',
                    dataType: 'json',
                    success: function (data) {
                        __sisFilaIntegracaoIndex.showNotificacaoInfo(
                            "Itens integrados com sucesso: " + data['sucesso'] + "<br>" +
                            "Itens que deram problema ao integrar: " + data['erro']
                        );
                        __sisFilaIntegracaoIndex.reloadDataTableSisFilaIntegracao();
                    }
                });
                e.preventDefault();
                e.stopPropagation();
            });

            $('#btn-sis-fila-integracao-correcaoAutomatica').click(function (e) {
                __sisFilaIntegracaoIndex.addOverlay(
                    $('#container-sis-fila-integracao'), 'Aguarde, executando correção automática...'
                );
                $.ajax({
                    url: __sisFilaIntegracaoIndex.options.url.correcaoAutomatica,
                    type: 'POST',
                    dataType: 'json',
                    success: function (data) {
                        __sisFilaIntegracaoIndex.showNotificacaoSuccess(
                            "" + data['quantidade'] + " itens corrigidos!"
                        );
                        __sisFilaIntegracaoIndex.reloadDataTableSisFilaIntegracao();
                    }
                });
                e.preventDefault();
                e.stopPropagation();
            });

            $('#dataTableSisFilaIntegracao').on('click', '.sisFilaIntegracao-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __sisFilaIntegracaoIndex.options.datatables.sisFilaIntegracao.fnGetData($pai);
                var arrRemover = {
                    filaId: data['fila_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de fila de integração "' + data['integracao_id'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __sisFilaIntegracaoIndex.addOverlay(
                                $('#container-sis-fila-integracao'),
                                'Aguarde, solicitando remoção de registro de fila de integração...'
                            );
                            $.ajax({
                                url: __sisFilaIntegracaoIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __sisFilaIntegracaoIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de fila de integração:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __sisFilaIntegracaoIndex.removeOverlay($('#container-sis-fila-integracao'));
                                    } else {
                                        __sisFilaIntegracaoIndex.reloadDataTableSisFilaIntegracao();
                                        __sisFilaIntegracaoIndex.showNotificacaoSuccess(
                                            "Registro de fila de integração removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#sis-fila-integracao-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['filaId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['filaId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);

            $("#integracao").select2('data', data['integracao']).trigger("change");
            $("#filaSituacao").trigger("change");

            $('#sis-fila-integracao-modal .modal-title').html(actionTitle + ' fila de integração');
            $('#sis-fila-integracao-modal').modal();
            __sisFilaIntegracaoIndex.removeOverlay($('#container-sis-fila-integracao'));
        };

        this.reloadDataTableSisFilaIntegracao = function () {
            this.getDataTableSisFilaIntegracao().api().ajax.reload(null, false);
        };

        this.getDataTableSisFilaIntegracao = function () {
            if (!__sisFilaIntegracaoIndex.options.datatables.sisFilaIntegracao) {
                if (!$.fn.dataTable.isDataTable('#dataTableSisFilaIntegracao')) {
                    __sisFilaIntegracaoIndex.options.datatables.sisFilaIntegracao =
                        $('#dataTableSisFilaIntegracao').DataTable();
                } else {
                    __sisFilaIntegracaoIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __sisFilaIntegracaoIndex.options.datatables.sisFilaIntegracao;
        };

        this.run = function (opts) {
            __sisFilaIntegracaoIndex.setDefaults(opts);
            __sisFilaIntegracaoIndex.setSteps();
        };
    };

    $.sisFilaIntegracaoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.sistema.sis-fila-integracao.index");

        if (!obj) {
            obj = new SisFilaIntegracaoIndex();
            obj.run(params);
            $(window).data('universa.sistema.sis-fila-integracao.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);