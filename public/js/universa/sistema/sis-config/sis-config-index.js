(function ($, window, document) {
    'use strict';
    var SisConfigIndex = function () {
        VersaShared.call(this);
        var __sisConfigIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                sisConfig: null
            },
        };

        this.setSteps = function () {
            var colNum = 0;
            __sisConfigIndex.options.datatables.sisConfig = $('#dataTableSisConfig').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __sisConfigIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary sisConfig-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger sisConfig-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                        }

                        __sisConfigIndex.removeOverlay($('#container-sis-config'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "chave", targets: colNum++, data: "chave"},
                    {name: "valor", targets: colNum++, data: "valor"},
                    {name: "chave", targets: colNum++, data: "acao"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $('#dataTableSisConfig').on('click', '.sisConfig-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __sisConfigIndex.options.datatables.sisConfig.fnGetData($pai);

                if (!__sisConfigIndex.options.ajax) {
                    location.href = __sisConfigIndex.options.url.edit + '/' + data['chave'];
                } else {
                    $.sisConfigAdd().pesquisaSisConfig(
                        data['chave'],
                        function (data) { __sisConfigIndex.showModal(data); }
                    );
                }
            });


            $('#btn-sis-config-add').click(function (e) {
                if (__sisConfigIndex.options.ajax) {
                    __sisConfigIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#dataTableSisConfig').on('click', '.sisConfig-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __sisConfigIndex.options.datatables.sisConfig.fnGetData($pai);
                var arrRemover = {
                    chave: data['chave']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de configuração "' + data['valor'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __sisConfigIndex.addOverlay(
                                $('#container-sis-config'),
                                'Aguarde, solicitando remoção de registro de configuração...'
                            );
                            $.ajax({
                                url: __sisConfigIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __sisConfigIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de configuração:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __sisConfigIndex.removeOverlay($('#container-sis-config'));
                                    } else {
                                        __sisConfigIndex.reloadDataTableSisConfig();
                                        __sisConfigIndex.showNotificacaoSuccess(
                                            "Registro de configuração removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#sis-config-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.split('/');

            if (actionForm[actionForm.length - 2] == 'edit') {
                actionForm = actionForm.slice(0, actionForm.length - 1);
            }

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['chave']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['chave'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);

            $form[0].reset();
            $form.deserialize(data);

            $('#sis-config-modal').find('.modal-title').html(actionTitle + ' configuração');
            $('#sis-config-modal').modal();
        };

        this.reloadDataTableSisConfig = function () {
            this.getDataTableSisConfig().api().ajax.reload(null, false);
        };

        this.getDataTableSisConfig = function () {
            if (!__sisConfigIndex.options.datatables.sisConfig) {
                if (!$.fn.dataTable.isDataTable('#dataTableSisConfig')) {
                    __sisConfigIndex.options.datatables.sisConfig = $('#dataTableSisConfig').DataTable();
                } else {
                    __sisConfigIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __sisConfigIndex.options.datatables.sisConfig;
        };

        this.run = function (opts) {
            __sisConfigIndex.setDefaults(opts);
            __sisConfigIndex.setSteps();
        };
    };

    $.sisConfigIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.sistema.sis-config.index");

        if (!obj) {
            obj = new SisConfigIndex();
            obj.run(params);
            $(window).data('universa.sistema.sis-config.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);