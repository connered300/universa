(function ($, window, document) {
    'use strict';

    var SisConfigAdd = function () {
        VersaShared.call(this);
        var __sisConfigAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {},
            data: {},
            value: {},
            datatables: {},
            wizardElement: '#sis-config-wizard',
            formElement: '#sis-config-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
        };

        this.setValidations = function () {
            __sisConfigAdd.options.validator.settings.rules = {};
            __sisConfigAdd.options.validator.settings.messages = {};

            $(__sisConfigAdd.options.formElement).submit(function (e) {
                var ok = $(__sisConfigAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__sisConfigAdd.options.formElement);

                if (__sisConfigAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __sisConfigAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                                url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __sisConfigAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__sisConfigAdd.options.listagem) {
                                    $.sisConfigIndex().reloadDataTableSisConfig();
                                    __sisConfigAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#sis-config-modal').modal('hide');
                                }

                                __sisConfigAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaSisConfig = function (chave, callback) {
            var $form = $(__sisConfigAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __sisConfigAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + chave,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    __sisConfigAdd.removeOverlay($form);

                    if (Object.keys(data).length == 0) {
                        __sisConfigAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }
                },
                erro: function () {
                    __sisConfigAdd.showNotificacaoDanger('Erro desconhecido!');
                    __sisConfigAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.sisConfigAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.sistema.sis-config.add");

        if (!obj) {
            obj = new SisConfigAdd();
            obj.run(params);
            $(window).data('universa.sistema.sis-config.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);