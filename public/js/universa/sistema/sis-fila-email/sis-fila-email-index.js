(function ($, window, document) {
    'use strict';
    var SisFilaEmailIndex = function () {
        VersaShared.call(this);
        var __sisFilaEmailIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: '',
                executar: '',
                correcaoAutomatica: ''
            },
            datatables: {
                sisFilaEmail: null
            },
        };

        this.setSteps = function () {
            var colNum = 0;
            __sisFilaEmailIndex.options.datatables.sisFilaEmail = $('#dataTableSisFilaEmail').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __sisFilaEmailIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary sisFilaEmail-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger sisFilaEmail-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                            data[row]['fila_data_cadastro_formatado'] = __sisFilaEmailIndex.formatDateTime(
                                data[row]['fila_data_cadastro']
                            );
                            data[row]['fila_data_processamento_formatado'] = __sisFilaEmailIndex.formatDateTime(
                                data[row]['fila_data_processamento']
                            );
                            data[row]['fila_data_finalizacao_formatado'] = __sisFilaEmailIndex.formatDateTime(
                                data[row]['fila_data_finalizacao']
                            );
                        }

                        __sisFilaEmailIndex.removeOverlay($('#container-sis-fila-email'));

                        __sisFilaEmailIndex.removeOverlay($('#container-sis-fila-email'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "fila_id", targets: colNum++, data: "fila_id"},
                    {name: "conta_nome", targets: colNum++, data: "conta_nome"},
                    {name: "fila_assunto", targets: colNum++, data: "fila_assunto"},
                    {name: "fila_destinatario_nome", targets: colNum++, data: "fila_destinatario_nome"},
                    {name: "fila_destinatario_email", targets: colNum++, data: "fila_destinatario_email"},
                    {name: "fila_data_cadastro", targets: colNum++, data: "fila_data_cadastro_formatado"},
                    {name: "fila_data_processamento", targets: colNum++, data: "fila_data_processamento_formatado"},
                    {name: "fila_data_finalizacao", targets: colNum++, data: "fila_data_finalizacao_formatado"},
                    {name: "fila_situacao", targets: colNum++, data: "fila_situacao"},
                    {name: "fila_id", targets: colNum++, data: "acao"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[5, 'desc']]
            });

            $('#dataTableSisFilaEmail').on('click', '.sisFilaEmail-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __sisFilaEmailIndex.options.datatables.sisFilaEmail.fnGetData($pai);

                if (!__sisFilaEmailIndex.options.ajax) {
                    location.href = __sisFilaEmailIndex.options.url.edit + '/' + data['fila_id'];
                } else {
                    __sisFilaEmailIndex.addOverlay(
                        $('#container-sis-fila-email'), 'Aguarde, carregando dados para edição...'
                    );
                    $.sisFilaEmailAdd().pesquisaSisFilaEmail(
                        data['fila_id'],
                        function (data) { __sisFilaEmailIndex.showModal(data); }
                    );
                }
            });


            $('#btn-sis-fila-email-add').click(function (e) {
                if (__sisFilaEmailIndex.options.ajax) {
                    __sisFilaEmailIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#btn-sis-fila-email-executar').click(function (e) {
                __sisFilaEmailIndex.addOverlay(
                    $('#container-sis-fila-email'), 'Aguarde, executando envios...'
                );
                $.ajax({
                    url: __sisFilaEmailIndex.options.url.executar,
                    type: 'POST',
                    dataType: 'json',
                    success: function (data) {
                        __sisFilaEmailIndex.showNotificacaoInfo(
                            "Itens enviados com sucesso: " + data['sucesso'] + "<br>" +
                            "Itens que deram problema ao enviar: " + data['erro']
                        );
                        __sisFilaEmailIndex.reloadDataTableSisFilaEmail();
                    }
                });
                e.preventDefault();
                e.stopPropagation();
            });

            $('#btn-sis-fila-email-correcaoAutomatica').click(function (e) {
                __sisFilaEmailIndex.addOverlay(
                    $('#container-sis-fila-sms'), 'Aguarde, executando correção automática...'
                );

                $.ajax({
                    url: __sisFilaEmailIndex.options.url.correcaoAutomatica,
                    type: 'POST',
                    dataType: 'json',
                    success: function (data) {
                        __sisFilaEmailIndex.showNotificacaoSuccess(
                            "" + data['quantidade'] + " itens corrigidos!"
                        );
                        __sisFilaEmailIndex.reloadDataTableSisFilaEmail();
                    }
                });

                e.preventDefault();
                e.stopPropagation();
            });

            $('#dataTableSisFilaEmail').on('click', '.sisFilaEmail-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __sisFilaEmailIndex.options.datatables.sisFilaEmail.fnGetData($pai);
                var arrRemover = {
                    filaId: data['fila_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de fila de e-mail "' + data['fila_destinatario_nome'] +
                             '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __sisFilaEmailIndex.addOverlay(
                                $('#container-sis-fila-email'),
                                'Aguarde, solicitando remoção de registro de fila de e-mail...'
                            );
                            $.ajax({
                                url: __sisFilaEmailIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __sisFilaEmailIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de fila de e-mail:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __sisFilaEmailIndex.removeOverlay($('#container-sis-fila-email'));
                                    } else {
                                        __sisFilaEmailIndex.reloadDataTableSisFilaEmail();
                                        __sisFilaEmailIndex.showNotificacaoSuccess(
                                            "Registro de fila de e-mail removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#sis-fila-email-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['filaId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['filaId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);

            $("#conta").select2('data', data['conta']).trigger("change");
            $("#filaConteudo").val(data['filaConteudo']);
            $("#filaConteudo").trigger('change');
            $("#filaSituacao").trigger("change");

            $('#sis-fila-email-modal .modal-title').html(actionTitle + ' fila de e-mail');
            $('#sis-fila-email-modal').modal();
            __sisFilaEmailIndex.removeOverlay($('#container-sis-fila-email'));
        };

        this.reloadDataTableSisFilaEmail = function () {
            this.getDataTableSisFilaEmail().api().ajax.reload(null, false);
        };

        this.getDataTableSisFilaEmail = function () {
            if (!__sisFilaEmailIndex.options.datatables.sisFilaEmail) {
                if (!$.fn.dataTable.isDataTable('#dataTableSisFilaEmail')) {
                    __sisFilaEmailIndex.options.datatables.sisFilaEmail = $('#dataTableSisFilaEmail').DataTable();
                } else {
                    __sisFilaEmailIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __sisFilaEmailIndex.options.datatables.sisFilaEmail;
        };

        this.run = function (opts) {
            __sisFilaEmailIndex.setDefaults(opts);
            __sisFilaEmailIndex.setSteps();
        };
    };

    $.sisFilaEmailIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.sistema.sis-fila-email.index");

        if (!obj) {
            obj = new SisFilaEmailIndex();
            obj.run(params);
            $(window).data('universa.sistema.sis-fila-email.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);