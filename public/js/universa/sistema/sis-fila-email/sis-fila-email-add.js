(function ($, window, document) {
    'use strict';

    var SisFilaEmailAdd = function () {
        VersaShared.call(this);
        var __sisFilaEmailAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {orgEmailConta: ''},
            data: {},
            value: {conta: null},
            datatables: {},
            wizardElement: '#sis-fila-email-wizard',
            formElement: '#sis-fila-email-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#conta").select2({
                language: 'pt-BR',
                ajax: {
                    url: __sisFilaEmailAdd.options.url.orgEmailConta,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.conta_nome, id: el.conta_id};
                        });

                        return {results: transformed};
                    }
                }
            });

            if (__sisFilaEmailAdd.getConta()) {
                $('#conta').select2("data", __sisFilaEmailAdd.getConta());
            }


            $('#filaConteudo').ckeditor({
                height: '200px', linkShowAdvancedTab: false,
                autoStartup: true,
                extraAllowedContent: (
                    'style;body;*{*};' +
                    'img[src,alt,width,height,style,class,id];' +
                    'iframe[src,alt,width,height,style,class,id];' +
                    'td[class,colspan,width,height,style];' +
                    'th[class,colspan,width,height,style];' +
                    'tr[class,style];' +
                    'h1[class,width,height,style,id];' +
                    'h2[class,width,height,style,id];' +
                    'h3[class,width,height,style,id];' +
                    'div[class,width,height,style,id];' +
                    'span[class,width,height,style,id];' +
                    'p[class,width,height,style,id];' +
                    'table[src,alt,width,height,style,id,class]'
                ),
                enterMode: Number(2),
                toolbar: [
                    [
                        'Styles', 'Bold', 'Italic', 'Underline', 'SpellChecker', 'Scayt', '-',
                        'NumberedList', 'BulletedList'
                    ],
                    ['Table'],
                    ['Link', 'Unlink'],
                    ['Undo', 'Redo', '-', 'SelectAll'],
                    ['Maximize', 'Source']
                ]
            });

            $("#filaSituacao").select2({language: 'pt-BR'}).trigger("change");
        };

        this.setConta = function (conta) {
            this.options.value.conta = conta || null;
        };

        this.getConta = function () {
            return this.options.value.conta || null;
        };
        this.setValidations = function () {
            __sisFilaEmailAdd.options.validator.settings.rules = {
                conta: {maxlength: 10, number: true, required: true},
                filaAssunto: {maxlength: 250},
                filaDestinatarioNome: {maxlength: 100},
                filaDestinatarioEmail: {maxlength: 100, required: true},
                filaConteudo: {required: true},
                filaRetorno: {},
                filaSituacao: {maxlength: 11, required: true},
            };
            __sisFilaEmailAdd.options.validator.settings.messages = {
                conta: {maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!', required: 'Campo obrigatório!'},
                filaAssunto: {maxlength: 'Tamanho máximo: 250!'},
                filaDestinatarioNome: {maxlength: 'Tamanho máximo: 100!'},
                filaDestinatarioEmail: {maxlength: 'Tamanho máximo: 100!', required: 'Campo obrigatório!'},
                filaConteudo: {required: 'Campo obrigatório!'},
                filaRetorno: {},
                filaSituacao: {maxlength: 'Tamanho máximo: 11!', required: 'Campo obrigatório!'},
            };

            $(__sisFilaEmailAdd.options.formElement).submit(function (e) {
                var ok = $(__sisFilaEmailAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__sisFilaEmailAdd.options.formElement);

                if (__sisFilaEmailAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __sisFilaEmailAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __sisFilaEmailAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__sisFilaEmailAdd.options.listagem) {
                                    $.sisFilaEmailIndex().reloadDataTableSisFilaEmail();
                                    __sisFilaEmailAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#sis-fila-email-modal').modal('hide');
                                }

                                __sisFilaEmailAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaSisFilaEmail = function (fila_id, callback) {
            var $form = $(__sisFilaEmailAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __sisFilaEmailAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + fila_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    __sisFilaEmailAdd.removeOverlay($form);

                    if (Object.keys(data).length == 0) {
                        __sisFilaEmailAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }
                },
                erro: function () {
                    __sisFilaEmailAdd.showNotificacaoDanger('Erro desconhecido!');
                    __sisFilaEmailAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.sisFilaEmailAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.sistema.sis-fila-email.add");

        if (!obj) {
            obj = new SisFilaEmailAdd();
            obj.run(params);
            $(window).data('universa.sistema.sis-fila-email.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);