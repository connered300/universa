(function ($, window, document) {
    'use strict';
    var SisCampoEntidadeIndex = function () {
        VersaShared.call(this);
        var __sisCampoEntidadeIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                sisCampoEntidade: null
            }
        };

        this.setSteps = function () {
            var $dataTableSisCampoEntidade = $('#dataTableSisCampoEntidade');
            var colNum = -1;
            __sisCampoEntidadeIndex.options.datatables.sisCampoEntidade = $dataTableSisCampoEntidade.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __sisCampoEntidadeIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [
                            {class: 'btn-primary item-edit', icon: 'fa-pencil'},
                            {class: 'btn-danger item-remove', icon: 'fa-times'}
                        ];

                        for (var row in data) {
                            data[row]['acao'] = __sisCampoEntidadeIndex.createBtnGroup(btns);
                        }

                        __sisCampoEntidadeIndex.removeOverlay($('#container-sis-campo-entidade'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "entidade_id", targets: ++colNum, data: "entidade_id"},
                    {name: "entidade_descricao", targets: ++colNum, data: "entidade_descricao"},
                    {name: "entidade_tabela", targets: ++colNum, data: "entidade_tabela"},
                    {name: "entidade_chave", targets: ++colNum, data: "entidade_chave"},
                    {name: "entidade_id", targets: ++colNum, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $dataTableSisCampoEntidade.on('click', '.item-edit', function () {
                var $pai = $(this).closest('tr');
                var data = __sisCampoEntidadeIndex.options.datatables.sisCampoEntidade.fnGetData($pai);

                if (!__sisCampoEntidadeIndex.options.ajax) {
                    location.href = __sisCampoEntidadeIndex.options.url.edit + '/' + data['entidade_id'];
                } else {
                    __sisCampoEntidadeIndex.addOverlay(
                        $('#container-sis-campo-entidade'), 'Aguarde, carregando dados para edição...'
                    );
                    $.sisCampoEntidadeAdd().pesquisaSisCampoEntidade(
                        data['entidade_id'],
                        function (data) {
                            __sisCampoEntidadeIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-sis-campo-entidade-add').click(function (e) {
                if (__sisCampoEntidadeIndex.options.ajax) {
                    __sisCampoEntidadeIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $dataTableSisCampoEntidade.on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __sisCampoEntidadeIndex.options.datatables.sisCampoEntidade.fnGetData($pai);
                var arrRemover = {
                    entidadeId: data['entidade_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de campo entidade "' + data['entidade_descricao'] + '"?',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __sisCampoEntidadeIndex.addOverlay(
                                $('#container-sis-campo-entidade'), 'Aguarde, solicitando remoção de registro de campo entidade...'
                            );
                            $.ajax({
                                url: __sisCampoEntidadeIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __sisCampoEntidadeIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de campo entidade:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __sisCampoEntidadeIndex.removeOverlay($('#container-sis-campo-entidade'));
                                    } else {
                                        __sisCampoEntidadeIndex.reloadDataTableSisCampoEntidade();
                                        __sisCampoEntidadeIndex.showNotificacaoSuccess(
                                            "Registro de campo entidade removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#sis-campo-entidade-form'),
                $modal = $('#sis-campo-entidade-modal');
            var actionForm = $form.attr('action');
            actionForm = actionForm.split('/');

            if (actionForm[actionForm.length - 2] == 'edit') {
                actionForm = actionForm.slice(0, actionForm.length - 1);
            }

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['entidadeId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['entidadeId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);

            $("#entidadeTabela,#entidadeChave").select2("val", '');

            $form[0].reset();
            $form.deserialize(data);

            if (data['entidadeTabela']) {
                $("#entidadeTabela").select2("val", data['entidadeTabela']);
            }
            if (data['entidadeChave']) {
                $("#entidadeChave").select2("val", data['entidadeChave']);
            }

            $modal.find('.modal-title').html(actionTitle + ' campo entidade');
            $modal.modal();
            __sisCampoEntidadeIndex.removeOverlay($('#container-sis-campo-entidade'));
        };

        this.reloadDataTableSisCampoEntidade = function () {
            this.getDataTableSisCampoEntidade().api().ajax.reload();
        };

        this.getDataTableSisCampoEntidade = function () {
            if (!__sisCampoEntidadeIndex.options.datatables.sisCampoEntidade) {
                if (!$.fn.dataTable.isDataTable('#dataTableSisCampoEntidade')) {
                    __sisCampoEntidadeIndex.options.datatables.sisCampoEntidade = $('#dataTableSisCampoEntidade').DataTable();
                } else {
                    __sisCampoEntidadeIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __sisCampoEntidadeIndex.options.datatables.sisCampoEntidade;
        };

        this.run = function (opts) {
            __sisCampoEntidadeIndex.setDefaults(opts);
            __sisCampoEntidadeIndex.setSteps();
        };
    };

    $.sisCampoEntidadeIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.sistema.sis-campo-entidade.index");

        if (!obj) {
            obj = new SisCampoEntidadeIndex();
            obj.run(params);
            $(window).data('universa.sistema.sis-campo-entidade.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);