(function ($, window, document) {
    'use strict';

    var SisCampoEntidadeAdd = function () {
        VersaShared.call(this);
        var __sisCampoEntidadeAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                urlPesquisa: ''
            },
            data: {
                arrTabelas: [],
                arrEntidades: []
            },
            value: {},
            datatables: {},
            wizardElement: '#sis-campo-entidade-wizard',
            formElement: '#sis-campo-entidade-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            var entidadeChave = $("#entidadeChave"),
                entidadeTabela = $("#entidadeTabela");

            entidadeTabela.select2({
                'data': __sisCampoEntidadeAdd.options.data.arrTabelas
            }).on("change", function (e) {

                entidadeChave.select2({'data': []});
                entidadeChave.select2("disable");

                if (entidadeTabela.val()) {
                    entidadeChave.select2("enable");
                    __sisCampoEntidadeAdd.addOverlay($("#sis-campo-entidade-modal"));
                    $.ajax({
                        url: __sisCampoEntidadeAdd.options.url.urlPesquisa,
                        dataType: 'json',
                        type: 'POST',
                        data: {
                            filter: 'columns',
                            table: entidadeTabela.select2("data")['id']
                        },
                        success: function (data) {
                            __sisCampoEntidadeAdd.options.data.arrEntidades = data;
                            entidadeChave.select2({'data': data});
                            __sisCampoEntidadeAdd.removeOverlay($("#sis-campo-entidade-modal"));
                        }
                    });
                }
            });
        };

        this.setValidations = function () {
            __sisCampoEntidadeAdd.options.validator.settings.rules = {
                entidadeId: {maxlength: 255, required: true},
                entidadeDescricao: {maxlength: 255, required: true},
                entidadeTabela: {maxlength: 255, required: true},
                entidadeChave: {maxlength: 255, required: true}
            };
            __sisCampoEntidadeAdd.options.validator.settings.messages = {
                entidadeId: {maxlength: 'Tamanho máximo: 255!', required: 'Campo obrigatório!'},
                entidadeDescricao: {maxlength: 'Tamanho máximo: 255!', required: 'Campo obrigatório!'},
                entidadeTabela: {maxlength: 'Tamanho máximo: 255!', required: 'Campo obrigatório!'},
                entidadeChave: {maxlength: 'Tamanho máximo: 255!', required: 'Campo obrigatório!'}
            };

            $(__sisCampoEntidadeAdd.options.formElement).submit(function (e) {
                var ok = $(__sisCampoEntidadeAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__sisCampoEntidadeAdd.options.formElement);

                if (__sisCampoEntidadeAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __sisCampoEntidadeAdd.addOverlay($("#sis-campo-entidade-modal"), 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __sisCampoEntidadeAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__sisCampoEntidadeAdd.options.listagem) {
                                    $.sisCampoEntidadeIndex().reloadDataTableSisCampoEntidade();
                                    __sisCampoEntidadeAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#sis-campo-entidade-modal').modal('hide');
                                }

                                __sisCampoEntidadeAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    __sisCampoEntidadeAdd.removeOverlay($("#sis-campo-entidade-modal"))

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaSisCampoEntidade = function (entidade_id, callback) {
            var $form = $(__sisCampoEntidadeAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __sisCampoEntidadeAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + entidade_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __sisCampoEntidadeAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __sisCampoEntidadeAdd.removeOverlay($form);
                },
                erro: function () {
                    __sisCampoEntidadeAdd.showNotificacaoDanger('Erro desconhecido!');
                    __sisCampoEntidadeAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.sisCampoEntidadeAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.sistema.sis-campo-entidade.add");

        if (!obj) {
            obj = new SisCampoEntidadeAdd();
            obj.run(params);
            $(window).data('universa.sistema.sis-campo-entidade.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);