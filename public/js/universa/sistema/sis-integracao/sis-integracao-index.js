(function ($, window, document) {
    'use strict';
    var SisIntegracaoIndex = function () {
        VersaShared.call(this);
        var __sisIntegracaoIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                sisIntegracao: null
            }
        };

        this.setSteps = function () {
            var colNum = 0;

            __sisIntegracaoIndex.options.datatables.sisIntegracao = $('#dataTableSisIntegracao').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __sisIntegracaoIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary sisIntegracao-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger sisIntegracao-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                        }

                        __sisIntegracaoIndex.removeOverlay($('#container-sis-integracao'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "integracao_id", targets: colNum++, data: "integracao_id"},
                    {name: "integracao_descricao", targets: colNum++, data: "integracao_descricao"},
                    {name: "integracao_tipo", targets: colNum++, data: "integracao_tipo"},
                    {name: "integracao_token", targets: colNum++, data: "integracao_token"},
                    {name: "integracao_situacao", targets: colNum++, data: "integracao_situacao"},
                    {name: "integracao_id", targets: colNum++, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $('#dataTableSisIntegracao').on('click', '.sisIntegracao-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __sisIntegracaoIndex.options.datatables.sisIntegracao.fnGetData($pai);

                if (!__sisIntegracaoIndex.options.ajax) {
                    location.href = __sisIntegracaoIndex.options.url.edit + '/' + data['integracao_id'];
                } else {
                    $.sisIntegracaoAdd().pesquisaSisIntegracao(
                        data['integracao_id'],
                        function (data) {
                            __sisIntegracaoIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-sis-integracao-add').click(function (e) {
                if (__sisIntegracaoIndex.options.ajax) {
                    __sisIntegracaoIndex.showModal();
                    $('#integracaoId').val('');
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#dataTableSisIntegracao').on('click', '.sisIntegracao-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __sisIntegracaoIndex.options.datatables.sisIntegracao.fnGetData($pai);
                var arrRemover = {
                    integracaoId: data['integracao_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de integração "' + data['integracao_descricao'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __sisIntegracaoIndex.addOverlay(
                                $('#container-sis-integracao'),
                                'Aguarde, solicitando remoção de registro de integração...'
                            );
                            $.ajax({
                                url: __sisIntegracaoIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __sisIntegracaoIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de integração:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __sisIntegracaoIndex.removeOverlay($('#container-sis-integracao'));
                                    } else {
                                        __sisIntegracaoIndex.reloadDataTableSisIntegracao();
                                        __sisIntegracaoIndex.showNotificacaoSuccess(
                                            "Registro de integração removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#sis-integracao-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['integracaoId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['integracaoId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);

            $("#integracaoTipo").trigger("change");
            $("#integracaoSituacao").trigger("change");
            $("#integracaoToken").closest("div")[data['integracaoToken'] ? 'show' : 'hide']();

            $.sisIntegracaoAdd().setCampus(data['campus'] || null);

            $('#sis-integracao-modal .modal-title').html(actionTitle + ' integração');
            $('#sis-integracao-modal').modal();
        };

        this.reloadDataTableSisIntegracao = function () {
            this.getDataTableSisIntegracao().api().ajax.reload(null, false);
        };

        this.getDataTableSisIntegracao = function () {
            if (!__sisIntegracaoIndex.options.datatables.sisIntegracao) {
                if (!$.fn.dataTable.isDataTable('#dataTableSisIntegracao')) {
                    __sisIntegracaoIndex.options.datatables.sisIntegracao = $('#dataTableSisIntegracao').DataTable();
                } else {
                    __sisIntegracaoIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __sisIntegracaoIndex.options.datatables.sisIntegracao;
        };

        this.run = function (opts) {
            __sisIntegracaoIndex.setDefaults(opts);
            __sisIntegracaoIndex.setSteps();
        };
    };

    $.sisIntegracaoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.sistema.sis-integracao.index");

        if (!obj) {
            obj = new SisIntegracaoIndex();
            obj.run(params);
            $(window).data('universa.sistema.sis-integracao.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);