(function ($, window, document) {
    'use strict';

    var SisIntegracaoAdd = function () {
        VersaShared.call(this);
        var __sisIntegracaoAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                campus: ''
            },
            data: {},
            value: {
                campus: null
            },
            datatables: {},
            wizardElement: '#sis-integracao-wizard',
            formElement: '#sis-integracao-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#integracaoTipo, #integracaoSituacao").select2({language: 'pt-BR'}).trigger("change");

            $('#campus').select2({
                ajax: {
                    url: __sisIntegracaoAdd.options.url.campus,
                    dataType: 'json',
                    data: function (q) {
                        return {q: q};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.camp_nome, id: el.camp_id};
                        });

                        return {results: transformed};
                    }
                },
                multiple: true
            });

            $('#campus').select2('data', __sisIntegracaoAdd.getCampus());
        };

        this.setValidations = function () {
            __sisIntegracaoAdd.options.validator.settings.rules = {
                integracaoDescricao: {maxlength: 100, required: true},
                integracaoTipo: {maxlength: 9, required: true},
                integracaoUrl: {required: true},
                integracaoSituacao: {maxlength: 7, required: true},
                campus: {required: true}
            };
            __sisIntegracaoAdd.options.validator.settings.messages = {
                integracaoDescricao: {maxlength: 'Tamanho máximo: 100!', required: 'Campo obrigatório!'},
                integracaoTipo: {maxlength: 'Tamanho máximo: 9!', required: 'Campo obrigatório!'},
                integracaoUrl: {required: 'Campo obrigatório!'},
                integracaoSituacao: {maxlength: 'Tamanho máximo: 7!', required: 'Campo obrigatório!'},
                campus: {required: 'Esse campo é essencial!'}
            };

            $(__sisIntegracaoAdd.options.formElement).submit(function (e) {
                var ok = $(__sisIntegracaoAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__sisIntegracaoAdd.options.formElement);

                if (__sisIntegracaoAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __sisIntegracaoAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __sisIntegracaoAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__sisIntegracaoAdd.options.listagem) {
                                    $.sisIntegracaoIndex().reloadDataTableSisIntegracao();
                                    __sisIntegracaoAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#sis-integracao-modal').modal('hide');
                                }

                                __sisIntegracaoAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaSisIntegracao = function (integracao_id, callback) {
            var $form = $(__sisIntegracaoAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __sisIntegracaoAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + integracao_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    __sisIntegracaoAdd.removeOverlay($form);

                    if (Object.keys(data).length == 0) {
                        __sisIntegracaoAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }
                },
                erro: function () {
                    __sisIntegracaoAdd.showNotificacaoDanger('Erro desconhecido!');
                    __sisIntegracaoAdd.removeOverlay($form);
                }
            });
        };


        this.setCampus = function (campus) {
            this.options.value.campus = campus || null;

            $('#campus').select2('data', __sisIntegracaoAdd.getCampus());
        };

        this.getCampus = function () {
            return this.options.value.campus || null;
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.sisIntegracaoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.sistema.sis-integracao.add");

        if (!obj) {
            obj = new SisIntegracaoAdd();
            obj.run(params);
            $(window).data('universa.sistema.sis-integracao.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);