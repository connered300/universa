(function ($, window, document) {
    'use strict';
    var SisFilaSmsIndex = function () {
        VersaShared.call(this);
        var __sisFilaSmsIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: '',
                executar: '',
                correcaoAutomatica: ''
            },
            datatables: {
                sisFilaSms: null
            },
        };

        this.setSteps = function () {
            var colNum = 0;
            __sisFilaSmsIndex.options.datatables.sisFilaSms = $('#dataTableSisFilaSms').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __sisFilaSmsIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary sisFilaSms-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger sisFilaSms-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                            data[row]['fila_data_cadastro_formatado'] = __sisFilaSmsIndex.formatDateTime(
                                data[row]['fila_data_cadastro']
                            );
                            data[row]['fila_data_processamento_formatado'] = __sisFilaSmsIndex.formatDateTime(
                                data[row]['fila_data_processamento']
                            );
                            data[row]['fila_data_finalizacao_formatado'] = __sisFilaSmsIndex.formatDateTime(
                                data[row]['fila_data_finalizacao']
                            );
                        }

                        __sisFilaSmsIndex.removeOverlay($('#container-sis-fila-sms'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "fila_id", targets: colNum++, data: "fila_id"},
                    {name: "fila_destinatario", targets: colNum++, data: "fila_destinatario"},
                    {name: "fila_provedor", targets: colNum++, data: "fila_provedor"},
                    {name: "fila_conteudo", targets: colNum++, data: "fila_conteudo"},
                    {name: "fila_data_cadastro", targets: colNum++, data: "fila_data_cadastro_formatado"},
                    {name: "fila_data_processamento", targets: colNum++, data: "fila_data_processamento_formatado"},
                    {name: "fila_data_finalizacao", targets: colNum++, data: "fila_data_finalizacao_formatado"},
                    {name: "fila_situacao", targets: colNum++, data: "fila_situacao"},
                    {name: "fila_id", targets: colNum++, data: "acao"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[4, 'desc']]
            });

            $('#dataTableSisFilaSms').on('click', '.sisFilaSms-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __sisFilaSmsIndex.options.datatables.sisFilaSms.fnGetData($pai);

                if (!__sisFilaSmsIndex.options.ajax) {
                    location.href = __sisFilaSmsIndex.options.url.edit + '/' + data['fila_id'];
                } else {
                    __sisFilaSmsIndex.addOverlay(
                        $('#container-sis-fila-sms'), 'Aguarde, carregando dados para edição...'
                    );
                    $.sisFilaSmsAdd().pesquisaSisFilaSms(
                        data['fila_id'],
                        function (data) { __sisFilaSmsIndex.showModal(data); }
                    );
                }
            });


            $('#btn-sis-fila-sms-add').click(function (e) {
                if (__sisFilaSmsIndex.options.ajax) {
                    __sisFilaSmsIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#btn-sis-fila-sms-executar').click(function (e) {
                __sisFilaSmsIndex.addOverlay(
                    $('#container-sis-fila-sms'), 'Aguarde, executando envios...'
                );
                $.ajax({
                    url: __sisFilaSmsIndex.options.url.executar,
                    type: 'POST',
                    dataType: 'json',
                    success: function (data) {
                        __sisFilaSmsIndex.showNotificacaoInfo(
                            "Itens enviados com sucesso: " + data['sucesso'] + "<br>" +
                            "Itens que deram problema ao enviar: " + data['erro']
                        );
                        __sisFilaSmsIndex.reloadDataTableSisFilaSms();
                    }
                });

                e.preventDefault();
                e.stopPropagation();
            });

            $('#btn-sis-fila-sms-correcaoAutomatica').click(function (e) {
                __sisFilaSmsIndex.addOverlay(
                    $('#container-sis-fila-sms'), 'Aguarde, executando correção automática...'
                );

                $.ajax({
                    url: __sisFilaSmsIndex.options.url.correcaoAutomatica,
                    type: 'POST',
                    dataType: 'json',
                    success: function (data) {
                        __sisFilaSmsIndex.showNotificacaoSuccess(
                            "" + data['quantidade'] + " itens corrigidos!"
                        );
                        __sisFilaSmsIndex.reloadDataTableSisFilaSms();
                    }
                });

                e.preventDefault();
                e.stopPropagation();
            });

            $('#dataTableSisFilaSms').on('click', '.sisFilaSms-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __sisFilaSmsIndex.options.datatables.sisFilaSms.fnGetData($pai);
                var arrRemover = {
                    filaId: data['fila_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de fila de SMS "' + data['fila_destinatario'] + '"?',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __sisFilaSmsIndex.addOverlay(
                                $('#container-sis-fila-sms'),
                                'Aguarde, solicitando remoção de registro de fila de SMS...'
                            );
                            $.ajax({
                                url: __sisFilaSmsIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __sisFilaSmsIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de fila de SMS:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __sisFilaSmsIndex.removeOverlay($('#container-sis-fila-sms'));
                                    } else {
                                        __sisFilaSmsIndex.reloadDataTableSisFilaSms();
                                        __sisFilaSmsIndex.showNotificacaoSuccess(
                                            "Registro de fila de SMS removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#sis-fila-sms-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['filaId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['filaId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);

            $("#filaProvedor").trigger("change");
            $("#filaSituacao").trigger("change");

            $('#sis-fila-sms-modal .modal-title').html(actionTitle + ' fila de SMS');
            $('#sis-fila-sms-modal').modal();
            __sisFilaSmsIndex.removeOverlay($('#container-sis-fila-sms'));
        };

        this.reloadDataTableSisFilaSms = function () {
            this.getDataTableSisFilaSms().api().ajax.reload(null, false);
        };

        this.getDataTableSisFilaSms = function () {
            if (!__sisFilaSmsIndex.options.datatables.sisFilaSms) {
                if (!$.fn.dataTable.isDataTable('#dataTableSisFilaSms')) {
                    __sisFilaSmsIndex.options.datatables.sisFilaSms = $('#dataTableSisFilaSms').DataTable();
                } else {
                    __sisFilaSmsIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __sisFilaSmsIndex.options.datatables.sisFilaSms;
        };

        this.run = function (opts) {
            __sisFilaSmsIndex.setDefaults(opts);
            __sisFilaSmsIndex.setSteps();
        };
    };

    $.sisFilaSmsIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.sistema.sis-fila-sms.index");

        if (!obj) {
            obj = new SisFilaSmsIndex();
            obj.run(params);
            $(window).data('universa.sistema.sis-fila-sms.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);