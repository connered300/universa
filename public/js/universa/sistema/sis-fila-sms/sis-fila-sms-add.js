(function ($, window, document) {
    'use strict';

    var SisFilaSmsAdd = function () {
        VersaShared.call(this);
        var __sisFilaSmsAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {},
            data: {},
            value: {},
            datatables: {},
            wizardElement: '#sis-fila-sms-wizard',
            formElement: '#sis-fila-sms-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#filaDestinatario").inputmask({
                showMaskOnHover: false, clearIncomplete: true, mask: ['(99) 9999-9999', '(99) 99999-9999']
            });
            $("#filaProvedor").select2({language: 'pt-BR'}).trigger("change");
            $("#filaSituacao").select2({language: 'pt-BR'}).trigger("change");
        };

        this.setValidations = function () {
            __sisFilaSmsAdd.options.validator.settings.rules = {
                filaDestinatario: {celular: true, maxlength: 45, required: true},
                filaProvedor: {maxlength: 7},
                filaConteudo: {required: true, maxlength: 160},
                filaSituacao: {maxlength: 11, required: true}
            };
            __sisFilaSmsAdd.options.validator.settings.messages = {
                filaId: {maxlength: 'Tamanho máximo: 10!'},
                filaDestinatario: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                filaProvedor: {maxlength: 'Tamanho máximo: 7!'},
                filaConteudo: {required: 'Campo obrigatório!', maxlength: 'Tamanho máximo: 160!'},
                filaSituacao: {maxlength: 'Tamanho máximo: 11!', required: 'Campo obrigatório!'},
            };

            $(__sisFilaSmsAdd.options.formElement).submit(function (e) {
                var ok = $(__sisFilaSmsAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__sisFilaSmsAdd.options.formElement);

                if (__sisFilaSmsAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __sisFilaSmsAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __sisFilaSmsAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__sisFilaSmsAdd.options.listagem) {
                                    $.sisFilaSmsIndex().reloadDataTableSisFilaSms();
                                    __sisFilaSmsAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#sis-fila-sms-modal').modal('hide');
                                }

                                __sisFilaSmsAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaSisFilaSms = function (fila_id, callback) {
            var $form = $(__sisFilaSmsAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __sisFilaSmsAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + fila_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    __sisFilaSmsAdd.removeOverlay($form);

                    if (Object.keys(data).length == 0) {
                        __sisFilaSmsAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }
                },
                erro: function () {
                    __sisFilaSmsAdd.showNotificacaoDanger('Erro desconhecido!');
                    __sisFilaSmsAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.sisFilaSmsAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.sistema.sis-fila-sms.add");

        if (!obj) {
            obj = new SisFilaSmsAdd();
            obj.run(params);
            $(window).data('universa.sistema.sis-fila-sms.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);