(function ($, window, document) {
    'use strict';

    var AvaliacaoQuestionarioGrupoAdd = function () {
        VersaShared.call(this);
        var __avaliacaoQuestionarioGrupoAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                acessoGrupo: '',
                avaliacaoQuestionario: ''
            },
            data: {},
            value: {
                grupo: null,
                questionario: null
            },
            datatables: {},
            wizardElement: '#avaliacao-questionario-grupo-wizard',
            formElement: '#avaliacao-questionario-grupo-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            var $grupo = $("#grupo");
            $grupo.select2({
                language: 'pt-BR',
                allowClear: true,
                ajax: {
                    url: __avaliacaoQuestionarioGrupoAdd.options.url.acessoGrupo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.grup_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $grupo.select2("data", __avaliacaoQuestionarioGrupoAdd.getGrupo());

            var $questionario = $("#questionario");
            $questionario.select2({
                language: 'pt-BR',
                allowClear: true,
                ajax: {
                    url: __avaliacaoQuestionarioGrupoAdd.options.url.avaliacaoQuestionario,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.questionario_id;
                            el.text = el.questionario_titulo;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $questionario.select2("data", __avaliacaoQuestionarioGrupoAdd.getQuestionario());
        };

        this.setGrupo = function (grupo) {
            this.options.value.grupo = grupo || null;
        };

        this.getGrupo = function () {
            return this.options.value.grupo || null;
        };

        this.setQuestionario = function (questionario) {
            this.options.value.questionario = questionario || null;
        };

        this.getQuestionario = function () {
            return this.options.value.questionario || null;
        };
        this.setValidations = function () {
            __avaliacaoQuestionarioGrupoAdd.options.validator.settings.rules = {};
            __avaliacaoQuestionarioGrupoAdd.options.validator.settings.messages = {};

            $(__avaliacaoQuestionarioGrupoAdd.options.formElement).submit(function (e) {
                var ok = $(__avaliacaoQuestionarioGrupoAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__avaliacaoQuestionarioGrupoAdd.options.formElement);

                if (__avaliacaoQuestionarioGrupoAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __avaliacaoQuestionarioGrupoAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __avaliacaoQuestionarioGrupoAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__avaliacaoQuestionarioGrupoAdd.options.listagem) {
                                    $.avaliacaoQuestionarioGrupoIndex().reloadDataTableAvaliacaoQuestionarioGrupo();
                                    __avaliacaoQuestionarioGrupoAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#avaliacao-questionario-grupo-modal').modal('hide');
                                }

                                __avaliacaoQuestionarioGrupoAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaAvaliacaoQuestionarioGrupo = function (questionario_id, callback) {
            var $form = $(__avaliacaoQuestionarioGrupoAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __avaliacaoQuestionarioGrupoAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + questionario_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __avaliacaoQuestionarioGrupoAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __avaliacaoQuestionarioGrupoAdd.removeOverlay($form);
                },
                erro: function () {
                    __avaliacaoQuestionarioGrupoAdd.showNotificacaoDanger('Erro desconhecido!');
                    __avaliacaoQuestionarioGrupoAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.avaliacaoQuestionarioGrupoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.avaliacao.avaliacao-questionario-grupo.add");

        if (!obj) {
            obj = new AvaliacaoQuestionarioGrupoAdd();
            obj.run(params);
            $(window).data('universa.avaliacao.avaliacao-questionario-grupo.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);