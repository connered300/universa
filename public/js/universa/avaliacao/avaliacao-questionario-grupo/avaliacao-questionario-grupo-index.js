(function ($, window, document) {
    'use strict';
    var AvaliacaoQuestionarioGrupoIndex = function () {
        VersaShared.call(this);
        var __avaliacaoQuestionarioGrupoIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                avaliacaoQuestionarioGrupo: null
            }
        };

        this.setSteps = function () {
            var $dataTableAvaliacaoQuestionarioGrupo = $('#dataTableAvaliacaoQuestionarioGrupo');
            var colNum = -1;
            __avaliacaoQuestionarioGrupoIndex.options.datatables.avaliacaoQuestionarioGrupo = $dataTableAvaliacaoQuestionarioGrupo.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __avaliacaoQuestionarioGrupoIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [
                            {class: 'btn-primary item-edit', icon: 'fa-pencil'},
                            {class: 'btn-danger item-remove', icon: 'fa-times'}
                        ];

                        for (var row in data) {
                            data[row]['acao'] = __avaliacaoQuestionarioGrupoIndex.createBtnGroup(btns);
                        }

                        __avaliacaoQuestionarioGrupoIndex.removeOverlay($('#container-avaliacao-questionario-grupo'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "grupo_id", targets: ++colNum, data: "grupo_id"},
                    {name: "questionario_id", targets: ++colNum, data: "questionario_id"},
                    {name: "questionario_id", targets: ++colNum, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $dataTableAvaliacaoQuestionarioGrupo.on('click', '.item-edit', function () {
                var $pai = $(this).closest('tr');
                var data = __avaliacaoQuestionarioGrupoIndex.options.datatables.avaliacaoQuestionarioGrupo.fnGetData($pai);

                if (!__avaliacaoQuestionarioGrupoIndex.options.ajax) {
                    location.href = __avaliacaoQuestionarioGrupoIndex.options.url.edit + '/' + data['questionario_id'];
                } else {
                    __avaliacaoQuestionarioGrupoIndex.addOverlay(
                        $('#container-avaliacao-questionario-grupo'), 'Aguarde, carregando dados para edição...'
                    );
                    $.avaliacaoQuestionarioGrupoAdd().pesquisaAvaliacaoQuestionarioGrupo(
                        data['questionario_id'],
                        function (data) {
                            __avaliacaoQuestionarioGrupoIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-avaliacao-questionario-grupo-add').click(function (e) {
                if (__avaliacaoQuestionarioGrupoIndex.options.ajax) {
                    __avaliacaoQuestionarioGrupoIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $dataTableAvaliacaoQuestionarioGrupo.on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __avaliacaoQuestionarioGrupoIndex.options.datatables.avaliacaoQuestionarioGrupo.fnGetData($pai);
                var arrRemover = {
                    questionarioId: data['questionario_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de questionario grupo "' + data['questionario_id'] + '"?',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __avaliacaoQuestionarioGrupoIndex.addOverlay(
                                $('#container-avaliacao-questionario-grupo'), 'Aguarde, solicitando remoção de registro de questionario grupo...'
                            );
                            $.ajax({
                                url: __avaliacaoQuestionarioGrupoIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __avaliacaoQuestionarioGrupoIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de questionario grupo:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __avaliacaoQuestionarioGrupoIndex.removeOverlay($('#container-avaliacao-questionario-grupo'));
                                    } else {
                                        __avaliacaoQuestionarioGrupoIndex.reloadDataTableAvaliacaoQuestionarioGrupo();
                                        __avaliacaoQuestionarioGrupoIndex.showNotificacaoSuccess(
                                            "Registro de questionario grupo removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#avaliacao-questionario-grupo-form'),
                $modal = $('#avaliacao-questionario-grupo-modal');
            var actionForm = $form.attr('action');
            actionForm = actionForm.split('/');

            if (actionForm[actionForm.length - 2] == 'edit') {
                actionForm = actionForm.slice(0, actionForm.length - 1);
            }

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['questionarioId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['questionarioId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);


            var $grupo = $("#grupo");
            $grupo.select2('data', data['grupo']).trigger("change");

            var $questionario = $("#questionario");
            $questionario.select2('data', data['questionario']).trigger("change");

            $modal.find('.modal-title').html(actionTitle + ' questionario grupo');
            $modal.modal();
            __avaliacaoQuestionarioGrupoIndex.removeOverlay($('#container-avaliacao-questionario-grupo'));
        };

        this.reloadDataTableAvaliacaoQuestionarioGrupo = function () {
            this.getDataTableAvaliacaoQuestionarioGrupo().api().ajax.reload();
        };

        this.getDataTableAvaliacaoQuestionarioGrupo = function () {
            if (!__avaliacaoQuestionarioGrupoIndex.options.datatables.avaliacaoQuestionarioGrupo) {
                if (!$.fn.dataTable.isDataTable('#dataTableAvaliacaoQuestionarioGrupo')) {
                    __avaliacaoQuestionarioGrupoIndex.options.datatables.avaliacaoQuestionarioGrupo = $('#dataTableAvaliacaoQuestionarioGrupo').DataTable();
                } else {
                    __avaliacaoQuestionarioGrupoIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __avaliacaoQuestionarioGrupoIndex.options.datatables.avaliacaoQuestionarioGrupo;
        };

        this.run = function (opts) {
            __avaliacaoQuestionarioGrupoIndex.setDefaults(opts);
            __avaliacaoQuestionarioGrupoIndex.setSteps();
        };
    };

    $.avaliacaoQuestionarioGrupoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.avaliacao.avaliacao-questionario-grupo.index");

        if (!obj) {
            obj = new AvaliacaoQuestionarioGrupoIndex();
            obj.run(params);
            $(window).data('universa.avaliacao.avaliacao-questionario-grupo.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);