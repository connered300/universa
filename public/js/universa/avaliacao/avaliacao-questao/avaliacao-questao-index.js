(function ($, window, document) {
    'use strict';
    var AvaliacaoQuestaoIndex = function () {
        VersaShared.call(this);
        var __avaliacaoQuestaoIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                avaliacaoQuestao: null
            }
        };

        this.setSteps = function () {
            var $dataTableAvaliacaoQuestao = $('#dataTableAvaliacaoQuestao');
            var colNum = -1;
            __avaliacaoQuestaoIndex.options.datatables.avaliacaoQuestao = $dataTableAvaliacaoQuestao.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __avaliacaoQuestaoIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [
                            {class: 'btn-primary item-edit', icon: 'fa-pencil'},
                            {class: 'btn-danger item-remove', icon: 'fa-times'}
                        ];

                        for (var row in data) {
                            data[row]['acao'] = __avaliacaoQuestaoIndex.createBtnGroup(btns);
                        }

                        __avaliacaoQuestaoIndex.removeOverlay($('#container-avaliacao-questao'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "questao_id", targets: ++colNum, data: "questao_id"},
                    {name: "questaotipo_id", targets: ++colNum, data: "questaotipo_id"},
                    {name: "aplicacao_id", targets: ++colNum, data: "aplicacao_id"},
                    {name: "questao_descricao", targets: ++colNum, data: "questao_descricao"},
                    {name: "questao_id", targets: ++colNum, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $dataTableAvaliacaoQuestao.on('click', '.item-edit', function () {
                var $pai = $(this).closest('tr');
                var data = __avaliacaoQuestaoIndex.options.datatables.avaliacaoQuestao.fnGetData($pai);

                if (!__avaliacaoQuestaoIndex.options.ajax) {
                    location.href = __avaliacaoQuestaoIndex.options.url.edit + '/' + data['questao_id'];
                } else {
                    __avaliacaoQuestaoIndex.addOverlay(
                        $('#container-avaliacao-questao'), 'Aguarde, carregando dados para edição...'
                    );
                    $.avaliacaoQuestaoAdd().pesquisaAvaliacaoQuestao(
                        data['questao_id'],
                        function (data) {
                            __avaliacaoQuestaoIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-avaliacao-questao-add').click(function (e) {
                if (__avaliacaoQuestaoIndex.options.ajax) {
                    __avaliacaoQuestaoIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $dataTableAvaliacaoQuestao.on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __avaliacaoQuestaoIndex.options.datatables.avaliacaoQuestao.fnGetData($pai);
                var arrRemover = {
                    questaoId: data['questao_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de questão "' + data['questao_descricao'] + '"?',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __avaliacaoQuestaoIndex.addOverlay(
                                $('#container-avaliacao-questao'), 'Aguarde, solicitando remoção de registro de questão...'
                            );
                            $.ajax({
                                url: __avaliacaoQuestaoIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __avaliacaoQuestaoIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de questão:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __avaliacaoQuestaoIndex.removeOverlay($('#container-avaliacao-questao'));
                                    } else {
                                        __avaliacaoQuestaoIndex.reloadDataTableAvaliacaoQuestao();
                                        __avaliacaoQuestaoIndex.showNotificacaoSuccess(
                                            "Registro de questão removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#avaliacao-questao-form'),
                $modal = $('#avaliacao-questao-modal');
            var actionForm = $form.attr('action');
            actionForm = actionForm.split('/');

            if (actionForm[actionForm.length - 2] == 'edit') {
                actionForm = actionForm.slice(0, actionForm.length - 1);
            }

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['questaoId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['questaoId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);


            var $questaotipo = $("#questaotipo");
            $questaotipo.select2('data', data['questaotipo']).trigger("change");

            var $aplicacao = $("#aplicacao");
            $aplicacao.select2('data', data['aplicacao']).trigger("change");

            $modal.find('.modal-title').html(actionTitle + ' questão');
            $modal.modal();
            __avaliacaoQuestaoIndex.removeOverlay($('#container-avaliacao-questao'));
        };

        this.reloadDataTableAvaliacaoQuestao = function () {
            this.getDataTableAvaliacaoQuestao().api().ajax.reload();
        };

        this.getDataTableAvaliacaoQuestao = function () {
            if (!__avaliacaoQuestaoIndex.options.datatables.avaliacaoQuestao) {
                if (!$.fn.dataTable.isDataTable('#dataTableAvaliacaoQuestao')) {
                    __avaliacaoQuestaoIndex.options.datatables.avaliacaoQuestao = $('#dataTableAvaliacaoQuestao').DataTable();
                } else {
                    __avaliacaoQuestaoIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __avaliacaoQuestaoIndex.options.datatables.avaliacaoQuestao;
        };

        this.run = function (opts) {
            __avaliacaoQuestaoIndex.setDefaults(opts);
            __avaliacaoQuestaoIndex.setSteps();
        };
    };

    $.avaliacaoQuestaoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.avaliacao.avaliacao-questao.index");

        if (!obj) {
            obj = new AvaliacaoQuestaoIndex();
            obj.run(params);
            $(window).data('universa.avaliacao.avaliacao-questao.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);