(function ($, window, document) {
    'use strict';

    var AvaliacaoQuestaoAdd = function () {
        VersaShared.call(this);
        var __avaliacaoQuestaoAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                avaliacaoQuestaoTipo: '',
                avaliacaoQuestaoAplicacao: ''
            },
            data: {},
            value: {
                questaotipo: null,
                aplicacao: null
            },
            datatables: {},
            wizardElement: '#avaliacao-questao-wizard',
            formElement: '#avaliacao-questao-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            var $questaotipo = $("#questaotipo");
            $questaotipo.select2({
                language: 'pt-BR',
                allowClear: true,
                ajax: {
                    url: __avaliacaoQuestaoAdd.options.url.avaliacaoQuestaoTipo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.questaotipo_id;
                            el.text = el.questaotipo_descricao;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $questaotipo.select2("data", __avaliacaoQuestaoAdd.getQuestaotipo());

            var $aplicacao = $("#aplicacao");
            $aplicacao.select2({
                language: 'pt-BR',
                allowClear: true,
                ajax: {
                    url: __avaliacaoQuestaoAdd.options.url.avaliacaoQuestaoAplicacao,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.aplicacao_id;
                            el.text = el.aplicacao_descricao;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $aplicacao.select2("data", __avaliacaoQuestaoAdd.getAplicacao());
        };

        this.setQuestaotipo = function (questaotipo) {
            this.options.value.questaotipo = questaotipo || null;
        };

        this.getQuestaotipo = function () {
            return this.options.value.questaotipo || null;
        };

        this.setAplicacao = function (aplicacao) {
            this.options.value.aplicacao = aplicacao || null;
        };

        this.getAplicacao = function () {
            return this.options.value.aplicacao || null;
        };
        this.setValidations = function () {
            __avaliacaoQuestaoAdd.options.validator.settings.rules = {
                questaotipo: {required: true},
                aplicacao: {required: true},
                questaoDescricao: {maxlength: 255}
            };
            __avaliacaoQuestaoAdd.options.validator.settings.messages = {
                questaotipo: {required: 'Campo obrigatório!'},
                aplicacao: {required: 'Campo obrigatório!'},
                questaoDescricao: {maxlength: 'Tamanho máximo: 255!'}
            };

            $(__avaliacaoQuestaoAdd.options.formElement).submit(function (e) {
                var ok = $(__avaliacaoQuestaoAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__avaliacaoQuestaoAdd.options.formElement);

                if (__avaliacaoQuestaoAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __avaliacaoQuestaoAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __avaliacaoQuestaoAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__avaliacaoQuestaoAdd.options.listagem) {
                                    $.avaliacaoQuestaoIndex().reloadDataTableAvaliacaoQuestao();
                                    __avaliacaoQuestaoAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#avaliacao-questao-modal').modal('hide');
                                }

                                __avaliacaoQuestaoAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaAvaliacaoQuestao = function (questao_id, callback) {
            var $form = $(__avaliacaoQuestaoAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __avaliacaoQuestaoAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + questao_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __avaliacaoQuestaoAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __avaliacaoQuestaoAdd.removeOverlay($form);
                },
                erro: function () {
                    __avaliacaoQuestaoAdd.showNotificacaoDanger('Erro desconhecido!');
                    __avaliacaoQuestaoAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.avaliacaoQuestaoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.avaliacao.avaliacao-questao.add");

        if (!obj) {
            obj = new AvaliacaoQuestaoAdd();
            obj.run(params);
            $(window).data('universa.avaliacao.avaliacao-questao.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);