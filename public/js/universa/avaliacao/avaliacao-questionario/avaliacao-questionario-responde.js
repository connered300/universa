(function ($, window, document) {
    'use strict';

    var AvaliacaoQuestionarioResponde = function () {
        VersaShared.call(this);
        var __AvaliacaoQuestionarioResponde = this;
        this.defaults = {
            ajaxSubmit: 1,
            url: {
                salvar: '',
                telaInicial: '',
                retornarQuestionario: ''
            },
            data: {},
            value: {
                fim: null,
                pes: null,
                questionarioId: null,
                temQuestionario: false
            },
            datatables: {},
            formElement: '#avaliacao-questionario-responde-form',
            validator: null
        };


        this.validacaoDadosAvaliacao = function (dados) {
            var retorno = true;
            var arrAnterior = [];

            $.each(dados['questao'], function (index, value) {
                if (arrAnterior ['perguntaId'] == value['perguntaId']) {
                    if (!arrAnterior['resposta'] && value['resposta']) {
                        __AvaliacaoQuestionarioResponde.showNotificacaoWarning("Por favor responda a questão: " + '"' + arrAnterior['enunciado'] + '"', false, 50000);

                        retorno = false;
                        return false;

                    }

                    if (arrAnterior['resposta'] && !value['resposta']) {
                        __AvaliacaoQuestionarioResponde.showNotificacaoWarning("Por favor responda a questão: " + '"' + value['enunciado'] + '"', false, 50000);

                        retorno = false;
                        return false;
                    }

                } else {
                    arrAnterior = value;
                }
            });

            return retorno;
        };


        this.steps = {};

        this.setSteps = function () {
            if (!__AvaliacaoQuestionarioResponde.options.value.temQuestionario) {
                __AvaliacaoQuestionarioResponde.showNotificacaoInfo("Não há avaliação disponível. Obrigado!");

                setTimeout(function () {
                    location.href = __AvaliacaoQuestionarioResponde.options.url.telaInicial;
                }, 5000);

                return;
            }

            $("#btn-questinonario-enviar").removeClass("hidden");


            $("#pesId").val(__AvaliacaoQuestionarioResponde.options.value.pes);
            $("#questionarioId").val(__AvaliacaoQuestionarioResponde.options.value.questionarioId);
        };

        this.setValidations = function () {
            __AvaliacaoQuestionarioResponde.options.validator.settings.rules = {};
            __AvaliacaoQuestionarioResponde.options.validator.settings.messages = {};


            $("#btn-questinonario-enviar").on("click", function (e) {
                var ok = $(__AvaliacaoQuestionarioResponde.options.formElement).valid();
                var $form = $(__AvaliacaoQuestionarioResponde.options.formElement);
                var dados = $form.serializeJSON();

                var valida = __AvaliacaoQuestionarioResponde.validacaoDadosAvaliacao(dados);

                if (!ok || !valida) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                if (__AvaliacaoQuestionarioResponde.options.ajaxSubmit || $form.data('ajax')) {
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __AvaliacaoQuestionarioResponde.addOverlay($form, 'Enviando avaliação. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data['erro']) {
                                    __AvaliacaoQuestionarioResponde.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (data['retorno']) {
                                    $("#avaliacao-questionario-agradescimento-modal").modal("show");

                                    if (data['retorno'] == 'Parcial') {
                                        $("#agradescimento-continuar-btn").removeClass("hidden");
                                    }


                                    $("#agradecimento-tela-inicial-btn").on("click", function () {
                                        location.href = __AvaliacaoQuestionarioResponde.options.url.telaInicial;
                                    });

                                    $("#agradescimento-continuar-btn").on("click", function () {
                                        location.href = __AvaliacaoQuestionarioResponde.options.url.retornarQuestionario + '/' + data['id'];
                                    });

                                }
                                __AvaliacaoQuestionarioResponde.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };


        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.AvaliacaoQuestionarioResponde = function (params) {
        params = params || [];
        var obj = $(window).data("universa.avaliacao.avaliacao-questionario.questionario");

        if (!obj) {
            obj = new AvaliacaoQuestionarioResponde();
            obj.run(params);
            $(window).data('universa.avaliacao.avaliacao-questionario.questionario', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);