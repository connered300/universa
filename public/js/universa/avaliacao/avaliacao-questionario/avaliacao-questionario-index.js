(function ($, window, document) {
    'use strict';
    var AvaliacaoQuestionarioIndex = function () {
        VersaShared.call(this);
        var __avaliacaoQuestionarioIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                avaliacaoQuestionario: null
            }
        };

        this.setSteps = function () {
            var $dataTableAvaliacaoQuestionario = $('#dataTableAvaliacaoQuestionario');
            var colNum = -1;
            __avaliacaoQuestionarioIndex.options.datatables.avaliacaoQuestionario = $dataTableAvaliacaoQuestionario.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __avaliacaoQuestionarioIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [
                            {class: 'btn-primary item-edit', icon: 'fa-pencil'},
                            {class: 'btn-danger item-remove', icon: 'fa-times'}
                        ];

                        for (var row in data) {
                            data[row]['acao'] = __avaliacaoQuestionarioIndex.createBtnGroup(btns);
                        }

                        __avaliacaoQuestionarioIndex.removeOverlay($('#container-avaliacao-questionario'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "questionario_id", targets: ++colNum, data: "questionario_id"},
                    {name: "fim_id", targets: ++colNum, data: "fim_id"},
                    {name: "questionario_titulo", targets: ++colNum, data: "questionario_titulo"},
                    {name: "questionario_inicio", targets: ++colNum, data: "questionario_inicio"},
                    {name: "questionario_fim", targets: ++colNum, data: "questionario_fim"},
                    {name: "questionario_alteracao", targets: ++colNum, data: "questionario_alteracao"},
                    {name: "questionario_estado", targets: ++colNum, data: "questionario_estado"},
                    {name: "questionario_id", targets: ++colNum, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $dataTableAvaliacaoQuestionario.on('click', '.item-edit', function () {
                var $pai = $(this).closest('tr');
                var data = __avaliacaoQuestionarioIndex.options.datatables.avaliacaoQuestionario.fnGetData($pai);

                if (!__avaliacaoQuestionarioIndex.options.ajax) {
                    location.href = __avaliacaoQuestionarioIndex.options.url.edit + '/' + data['questionario_id'];
                } else {
                    __avaliacaoQuestionarioIndex.addOverlay(
                        $('#container-avaliacao-questionario'), 'Aguarde, carregando dados para edição...'
                    );
                    $.avaliacaoQuestionarioAdd().pesquisaAvaliacaoQuestionario(
                        data['questionario_id'],
                        function (data) {
                            __avaliacaoQuestionarioIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-avaliacao-questionario-add').click(function (e) {
                if (__avaliacaoQuestionarioIndex.options.ajax) {
                    __avaliacaoQuestionarioIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $dataTableAvaliacaoQuestionario.on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __avaliacaoQuestionarioIndex.options.datatables.avaliacaoQuestionario.fnGetData($pai);
                var arrRemover = {
                    questionarioId: data['questionario_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de questionario "' + data['questionario_descricao'] + '"?',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __avaliacaoQuestionarioIndex.addOverlay(
                                $('#container-avaliacao-questionario'), 'Aguarde, solicitando remoção de registro de questionario...'
                            );
                            $.ajax({
                                url: __avaliacaoQuestionarioIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __avaliacaoQuestionarioIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de questionario:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __avaliacaoQuestionarioIndex.removeOverlay($('#container-avaliacao-questionario'));
                                    } else {
                                        __avaliacaoQuestionarioIndex.reloadDataTableAvaliacaoQuestionario();
                                        __avaliacaoQuestionarioIndex.showNotificacaoSuccess(
                                            "Registro de questionario removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#avaliacao-questionario-form'),
                $modal = $('#avaliacao-questionario-modal');
            var actionForm = $form.attr('action');
            actionForm = actionForm.split('/');

            if (actionForm[actionForm.length - 2] == 'edit') {
                actionForm = actionForm.slice(0, actionForm.length - 1);
            }

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['questionarioId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['questionarioId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);


            var $fim = $("#fim");
            $fim.select2('data', data['fim']).trigger("change");

            var $questionarioEstado = $("#questionarioEstado");
            $questionarioEstado.select2('val', data['questionarioEstado']).trigger("change");

            var $questionarioObrigatoriedade = $("#questionarioObrigatoriedade");
            $questionarioObrigatoriedade.select2('val', data['questionarioObrigatoriedade']).trigger("change");

            var $questionarioAnonimo = $("#questionarioAnonimo");
            $questionarioAnonimo.select2('val', data['questionarioAnonimo']).trigger("change");

            $modal.find('.modal-title').html(actionTitle + ' questionario');
            $modal.modal();
            __avaliacaoQuestionarioIndex.removeOverlay($('#container-avaliacao-questionario'));
        };

        this.reloadDataTableAvaliacaoQuestionario = function () {
            this.getDataTableAvaliacaoQuestionario().api().ajax.reload();
        };

        this.getDataTableAvaliacaoQuestionario = function () {
            if (!__avaliacaoQuestionarioIndex.options.datatables.avaliacaoQuestionario) {
                if (!$.fn.dataTable.isDataTable('#dataTableAvaliacaoQuestionario')) {
                    __avaliacaoQuestionarioIndex.options.datatables.avaliacaoQuestionario = $('#dataTableAvaliacaoQuestionario').DataTable();
                } else {
                    __avaliacaoQuestionarioIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __avaliacaoQuestionarioIndex.options.datatables.avaliacaoQuestionario;
        };

        this.run = function (opts) {
            __avaliacaoQuestionarioIndex.setDefaults(opts);
            __avaliacaoQuestionarioIndex.setSteps();
        };
    };

    $.avaliacaoQuestionarioIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.avaliacao.avaliacao-questionario.index");

        if (!obj) {
            obj = new AvaliacaoQuestionarioIndex();
            obj.run(params);
            $(window).data('universa.avaliacao.avaliacao-questionario.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);