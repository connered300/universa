(function ($, window, document) {
    'use strict';

    var AvaliacaoQuestionarioAdd = function () {
        VersaShared.call(this);
        var __avaliacaoQuestionarioAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {avaliacaoFim: ''},
            data: {},
            value: {fim: null},
            datatables: {},
            wizardElement: '#avaliacao-questionario-wizard',
            formElement: '#avaliacao-questionario-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            var $fim = $("#fim");
            $fim.select2({
                language: 'pt-BR',
                allowClear: true,
                ajax: {
                    url: __avaliacaoQuestionarioAdd.options.url.avaliacaoFim,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.fim_id;
                            el.text = el.fim_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $fim.select2("data", __avaliacaoQuestionarioAdd.getFim());

            var $questionarioInicio = $("#questionarioInicio");
            __avaliacaoQuestionarioAdd.iniciarElementoDatePicker($questionarioInicio);

            var $questionarioFim = $("#questionarioFim");
            __avaliacaoQuestionarioAdd.iniciarElementoDatePicker($questionarioFim);

            var $questionarioEstado = $("#questionarioEstado");
            $questionarioEstado.select2({language: 'pt-BR'}).trigger("change");

            var $questionarioSecoes = $("#questionarioSecoes");
            __avaliacaoQuestionarioAdd.inciarElementoInteiro($questionarioSecoes);

            var $questionarioObrigatoriedade = $("#questionarioObrigatoriedade");
            $questionarioObrigatoriedade.select2({language: 'pt-BR'}).trigger("change");

            var $questionarioAnonimo = $("#questionarioAnonimo");
            $questionarioAnonimo.select2({language: 'pt-BR'}).trigger("change");
        };

        this.setFim = function (fim) {
            this.options.value.fim = fim || null;
        };

        this.getFim = function () {
            return this.options.value.fim || null;
        };
        this.setValidations = function () {
            __avaliacaoQuestionarioAdd.options.validator.settings.rules = {
                fim: {required: true},
                questionarioTitulo: {maxlength: 45, required: true},
                questionarioInicio: {required: true, dateBR: true},
                questionarioFim: {dateBR: true},
                questionarioEstado: {maxlength: 8},
                questionarioSecoes: {maxlength: 11, number: true},
                questionarioObrigatoriedade: {maxlength: 11},
                questionarioAnonimo: {maxlength: 3}
            };
            __avaliacaoQuestionarioAdd.options.validator.settings.messages = {
                fim: {required: 'Campo obrigatório!'},
                questionarioTitulo: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                questionarioInicio: {required: 'Campo obrigatório!', dateBR: 'Informe uma data válida!'},
                questionarioFim: {dateBR: 'Informe uma data válida!'},
                questionarioEstado: {maxlength: 'Tamanho máximo: 8!'},
                questionarioSecoes: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!'},
                questionarioObrigatoriedade: {maxlength: 'Tamanho máximo: 11!'},
                questionarioAnonimo: {maxlength: 'Tamanho máximo: 3!'}
            };

            $(__avaliacaoQuestionarioAdd.options.formElement).submit(function (e) {
                var ok = $(__avaliacaoQuestionarioAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__avaliacaoQuestionarioAdd.options.formElement);

                if (__avaliacaoQuestionarioAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __avaliacaoQuestionarioAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __avaliacaoQuestionarioAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__avaliacaoQuestionarioAdd.options.listagem) {
                                    $.avaliacaoQuestionarioIndex().reloadDataTableAvaliacaoQuestionario();
                                    __avaliacaoQuestionarioAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#avaliacao-questionario-modal').modal('hide');
                                }

                                __avaliacaoQuestionarioAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaAvaliacaoQuestionario = function (questionario_id, callback) {
            var $form = $(__avaliacaoQuestionarioAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __avaliacaoQuestionarioAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + questionario_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __avaliacaoQuestionarioAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __avaliacaoQuestionarioAdd.removeOverlay($form);
                },
                erro: function () {
                    __avaliacaoQuestionarioAdd.showNotificacaoDanger('Erro desconhecido!');
                    __avaliacaoQuestionarioAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.avaliacaoQuestionarioAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.avaliacao.avaliacao-questionario.add");

        if (!obj) {
            obj = new AvaliacaoQuestionarioAdd();
            obj.run(params);
            $(window).data('universa.avaliacao.avaliacao-questionario.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);