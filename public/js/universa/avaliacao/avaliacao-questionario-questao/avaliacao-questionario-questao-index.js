(function ($, window, document) {
    'use strict';
    var AvaliacaoQuestionarioQuestaoIndex = function () {
        VersaShared.call(this);
        var __avaliacaoQuestionarioQuestaoIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                avaliacaoQuestionarioQuestao: null
            }
        };

        this.setSteps = function () {
            var $dataTableAvaliacaoQuestionarioQuestao = $('#dataTableAvaliacaoQuestionarioQuestao');
            var colNum = -1;
            __avaliacaoQuestionarioQuestaoIndex.options.datatables.avaliacaoQuestionarioQuestao = $dataTableAvaliacaoQuestionarioQuestao.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __avaliacaoQuestionarioQuestaoIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [
                            {class: 'btn-primary item-edit', icon: 'fa-pencil'},
                            {class: 'btn-danger item-remove', icon: 'fa-times'}
                        ];

                        for (var row in data) {
                            data[row]['acao'] = __avaliacaoQuestionarioQuestaoIndex.createBtnGroup(btns);
                        }

                        __avaliacaoQuestionarioQuestaoIndex.removeOverlay($('#container-avaliacao-questionario-questao'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "questionarioquestao_id", targets: ++colNum, data: "questionarioquestao_id"},
                    {name: "questionario_id", targets: ++colNum, data: "questionario_id"},
                    {name: "secao_id", targets: ++colNum, data: "secao_id"},
                    {name: "questao_id", targets: ++colNum, data: "questao_id"},
                    {name: "questionarioquestao_id", targets: ++colNum, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $dataTableAvaliacaoQuestionarioQuestao.on('click', '.item-edit', function () {
                var $pai = $(this).closest('tr');
                var data = __avaliacaoQuestionarioQuestaoIndex.options.datatables.avaliacaoQuestionarioQuestao.fnGetData($pai);

                if (!__avaliacaoQuestionarioQuestaoIndex.options.ajax) {
                    location.href = __avaliacaoQuestionarioQuestaoIndex.options.url.edit + '/' + data['questionarioquestao_id'];
                } else {
                    __avaliacaoQuestionarioQuestaoIndex.addOverlay(
                        $('#container-avaliacao-questionario-questao'), 'Aguarde, carregando dados para edição...'
                    );
                    $.avaliacaoQuestionarioQuestaoAdd().pesquisaAvaliacaoQuestionarioQuestao(
                        data['questionarioquestao_id'],
                        function (data) {
                            __avaliacaoQuestionarioQuestaoIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-avaliacao-questionario-questao-add').click(function (e) {
                if (__avaliacaoQuestionarioQuestaoIndex.options.ajax) {
                    __avaliacaoQuestionarioQuestaoIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $dataTableAvaliacaoQuestionarioQuestao.on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __avaliacaoQuestionarioQuestaoIndex.options.datatables.avaliacaoQuestionarioQuestao.fnGetData($pai);
                var arrRemover = {
                    questionarioquestaoId: data['questionarioquestao_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de questionario questão "' + data['questionario_id'] + '"?',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __avaliacaoQuestionarioQuestaoIndex.addOverlay(
                                $('#container-avaliacao-questionario-questao'), 'Aguarde, solicitando remoção de registro de questionario questão...'
                            );
                            $.ajax({
                                url: __avaliacaoQuestionarioQuestaoIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __avaliacaoQuestionarioQuestaoIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de questionario questão:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __avaliacaoQuestionarioQuestaoIndex.removeOverlay($('#container-avaliacao-questionario-questao'));
                                    } else {
                                        __avaliacaoQuestionarioQuestaoIndex.reloadDataTableAvaliacaoQuestionarioQuestao();
                                        __avaliacaoQuestionarioQuestaoIndex.showNotificacaoSuccess(
                                            "Registro de questionario questão removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#avaliacao-questionario-questao-form'),
                $modal = $('#avaliacao-questionario-questao-modal');
            var actionForm = $form.attr('action');
            actionForm = actionForm.split('/');

            if (actionForm[actionForm.length - 2] == 'edit') {
                actionForm = actionForm.slice(0, actionForm.length - 1);
            }

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['questionarioquestaoId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['questionarioquestaoId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);


            var $questionario = $("#questionario");
            $questionario.select2('data', data['questionario']).trigger("change");

            var $secao = $("#secao");
            $secao.select2('data', data['secao']).trigger("change");

            var $questao = $("#questao");
            $questao.select2('data', data['questao']).trigger("change");

            var $questionarioquestaoAnulada = $("#questionarioquestaoAnulada");
            $questionarioquestaoAnulada.select2('val', data['questionarioquestaoAnulada']).trigger("change");

            var $questionarioquestaoObrigatoria = $("#questionarioquestaoObrigatoria");
            $questionarioquestaoObrigatoria.select2('val', data['questionarioquestaoObrigatoria']).trigger("change");

            $modal.find('.modal-title').html(actionTitle + ' questionario questão');
            $modal.modal();
            __avaliacaoQuestionarioQuestaoIndex.removeOverlay($('#container-avaliacao-questionario-questao'));
        };

        this.reloadDataTableAvaliacaoQuestionarioQuestao = function () {
            this.getDataTableAvaliacaoQuestionarioQuestao().api().ajax.reload();
        };

        this.getDataTableAvaliacaoQuestionarioQuestao = function () {
            if (!__avaliacaoQuestionarioQuestaoIndex.options.datatables.avaliacaoQuestionarioQuestao) {
                if (!$.fn.dataTable.isDataTable('#dataTableAvaliacaoQuestionarioQuestao')) {
                    __avaliacaoQuestionarioQuestaoIndex.options.datatables.avaliacaoQuestionarioQuestao = $('#dataTableAvaliacaoQuestionarioQuestao').DataTable();
                } else {
                    __avaliacaoQuestionarioQuestaoIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __avaliacaoQuestionarioQuestaoIndex.options.datatables.avaliacaoQuestionarioQuestao;
        };

        this.run = function (opts) {
            __avaliacaoQuestionarioQuestaoIndex.setDefaults(opts);
            __avaliacaoQuestionarioQuestaoIndex.setSteps();
        };
    };

    $.avaliacaoQuestionarioQuestaoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.avaliacao.avaliacao-questionario-questao.index");

        if (!obj) {
            obj = new AvaliacaoQuestionarioQuestaoIndex();
            obj.run(params);
            $(window).data('universa.avaliacao.avaliacao-questionario-questao.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);