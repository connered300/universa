(function ($, window, document) {
    'use strict';

    var AvaliacaoQuestionarioQuestaoAdd = function () {
        VersaShared.call(this);
        var __avaliacaoQuestionarioQuestaoAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                avaliacaoQuestionario: '',
                avaliacaoSecao: '',
                avaliacaoQuestao: ''
            },
            data: {},
            value: {
                questionario: null,
                secao: null,
                questao: null
            },
            datatables: {},
            wizardElement: '#avaliacao-questionario-questao-wizard',
            formElement: '#avaliacao-questionario-questao-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            var $questionario = $("#questionario");
            $questionario.select2({
                language: 'pt-BR',
                allowClear: true,
                ajax: {
                    url: __avaliacaoQuestionarioQuestaoAdd.options.url.avaliacaoQuestionario,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.questionario_id;
                            el.text = el.questionario_titulo;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $questionario.select2("data", __avaliacaoQuestionarioQuestaoAdd.getQuestionario());

            var $secao = $("#secao");
            $secao.select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __avaliacaoQuestionarioQuestaoAdd.options.url.avaliacaoSecao,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.secao_id;
                            el.text = el.secao_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $secao.select2("data", __avaliacaoQuestionarioQuestaoAdd.getSecao());

            var $questao = $("#questao");
            $questao.select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __avaliacaoQuestionarioQuestaoAdd.options.url.avaliacaoQuestao,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.questao_id;
                            el.text = el.questao_descricao;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $questao.select2("data", __avaliacaoQuestionarioQuestaoAdd.getQuestao());

            var $questionarioquestaoPosicao = $("#questionarioquestaoPosicao");
            __avaliacaoQuestionarioQuestaoAdd.inciarElementoInteiro($questionarioquestaoPosicao);

            var $questionarioquestaoAnulada = $("#questionarioquestaoAnulada");
            $questionarioquestaoAnulada.select2({language: 'pt-BR'}).trigger("change");

            var $questionarioquestaoObrigatoria = $("#questionarioquestaoObrigatoria");
            $questionarioquestaoObrigatoria.select2({language: 'pt-BR'}).trigger("change");
        };

        this.setQuestionario = function (questionario) {
            this.options.value.questionario = questionario || null;
        };

        this.getQuestionario = function () {
            return this.options.value.questionario || null;
        };

        this.setSecao = function (secao) {
            this.options.value.secao = secao || null;
        };

        this.getSecao = function () {
            return this.options.value.secao || null;
        };

        this.setQuestao = function (questao) {
            this.options.value.questao = questao || null;
        };

        this.getQuestao = function () {
            return this.options.value.questao || null;
        };
        this.setValidations = function () {
            __avaliacaoQuestionarioQuestaoAdd.options.validator.settings.rules = {
                questionario: {number: true, required: true},
                secao: {number: true, required: true},
                questao: {number: true, required: true},
                questionarioquestaoPosicao: {maxlength: 11, number: true, required: true},
                questionarioquestaoAnulada: {maxlength: 3},
                questionarioquestaoObrigatoria: {maxlength: 3},
                questionarioquestaoPeso: {maxlength: 45}
            };
            __avaliacaoQuestionarioQuestaoAdd.options.validator.settings.messages = {
                questionario: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                secao: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                questao: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                questionarioquestaoPosicao: {
                    maxlength: 'Tamanho máximo: 11!',
                    number: 'Número inválido!',
                    required: 'Campo obrigatório!'
                },
                questionarioquestaoAnulada: {maxlength: 'Tamanho máximo: 3!'},
                questionarioquestaoObrigatoria: {maxlength: 'Tamanho máximo: 3!'},
                questionarioquestaoPeso: {maxlength: 'Tamanho máximo: 45!'}
            };

            $(__avaliacaoQuestionarioQuestaoAdd.options.formElement).submit(function (e) {
                var ok = $(__avaliacaoQuestionarioQuestaoAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__avaliacaoQuestionarioQuestaoAdd.options.formElement);

                if (__avaliacaoQuestionarioQuestaoAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __avaliacaoQuestionarioQuestaoAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __avaliacaoQuestionarioQuestaoAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__avaliacaoQuestionarioQuestaoAdd.options.listagem) {
                                    $.avaliacaoQuestionarioQuestaoIndex().reloadDataTableAvaliacaoQuestionarioQuestao();
                                    __avaliacaoQuestionarioQuestaoAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#avaliacao-questionario-questao-modal').modal('hide');
                                }

                                __avaliacaoQuestionarioQuestaoAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaAvaliacaoQuestionarioQuestao = function (questionarioquestao_id, callback) {
            var $form = $(__avaliacaoQuestionarioQuestaoAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __avaliacaoQuestionarioQuestaoAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + questionarioquestao_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __avaliacaoQuestionarioQuestaoAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __avaliacaoQuestionarioQuestaoAdd.removeOverlay($form);
                },
                erro: function () {
                    __avaliacaoQuestionarioQuestaoAdd.showNotificacaoDanger('Erro desconhecido!');
                    __avaliacaoQuestionarioQuestaoAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.avaliacaoQuestionarioQuestaoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.avaliacao.avaliacao-questionario-questao.add");

        if (!obj) {
            obj = new AvaliacaoQuestionarioQuestaoAdd();
            obj.run(params);
            $(window).data('universa.avaliacao.avaliacao-questionario-questao.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);