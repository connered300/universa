(function ($, window, document) {
    'use strict';

    var AvaliacaoSecaoAdd = function () {
        VersaShared.call(this);
        var __avaliacaoSecaoAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {},
            data: {},
            value: {},
            datatables: {},
            wizardElement: '#avaliacao-secao-wizard',
            formElement: '#avaliacao-secao-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            var $secaoOrdem = $("#secaoOrdem");
            __avaliacaoSecaoAdd.inciarElementoInteiro($secaoOrdem);
        };

        this.setValidations = function () {
            __avaliacaoSecaoAdd.options.validator.settings.rules = {
                secaoNome: {maxlength: 45, required: true},
                secaoDescricao: {maxlength: 100},
                secaoOrdem: {maxlength: 11, number: true, required: true}
            };
            __avaliacaoSecaoAdd.options.validator.settings.messages = {
                secaoNome: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                secaoDescricao: {maxlength: 'Tamanho máximo: 100!'},
                secaoOrdem: {
                    maxlength: 'Tamanho máximo: 11!',
                    number: 'Número inválido!',
                    required: 'Campo obrigatório!'
                }
            };

            $(__avaliacaoSecaoAdd.options.formElement).submit(function (e) {
                var ok = $(__avaliacaoSecaoAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__avaliacaoSecaoAdd.options.formElement);

                if (__avaliacaoSecaoAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __avaliacaoSecaoAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __avaliacaoSecaoAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__avaliacaoSecaoAdd.options.listagem) {
                                    $.avaliacaoSecaoIndex().reloadDataTableAvaliacaoSecao();
                                    __avaliacaoSecaoAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#avaliacao-secao-modal').modal('hide');
                                }

                                __avaliacaoSecaoAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaAvaliacaoSecao = function (secao_id, callback) {
            var $form = $(__avaliacaoSecaoAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __avaliacaoSecaoAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + secao_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __avaliacaoSecaoAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __avaliacaoSecaoAdd.removeOverlay($form);
                },
                erro: function () {
                    __avaliacaoSecaoAdd.showNotificacaoDanger('Erro desconhecido!');
                    __avaliacaoSecaoAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.avaliacaoSecaoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.avaliacao.avaliacao-secao.add");

        if (!obj) {
            obj = new AvaliacaoSecaoAdd();
            obj.run(params);
            $(window).data('universa.avaliacao.avaliacao-secao.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);