(function ($, window, document) {
    'use strict';
    var AvaliacaoSecaoIndex = function () {
        VersaShared.call(this);
        var __avaliacaoSecaoIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                avaliacaoSecao: null
            }
        };

        this.setSteps = function () {
            var $dataTableAvaliacaoSecao = $('#dataTableAvaliacaoSecao');
            var colNum = -1;
            __avaliacaoSecaoIndex.options.datatables.avaliacaoSecao = $dataTableAvaliacaoSecao.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __avaliacaoSecaoIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [
                            {class: 'btn-primary item-edit', icon: 'fa-pencil'},
                            {class: 'btn-danger item-remove', icon: 'fa-times'}
                        ];

                        for (var row in data) {
                            data[row]['acao'] = __avaliacaoSecaoIndex.createBtnGroup(btns);
                        }

                        __avaliacaoSecaoIndex.removeOverlay($('#container-avaliacao-secao'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "secao_id", targets: ++colNum, data: "secao_id"},
                    {name: "secao_nome", targets: ++colNum, data: "secao_nome"},
                    {name: "secao_id", targets: ++colNum, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $dataTableAvaliacaoSecao.on('click', '.item-edit', function () {
                var $pai = $(this).closest('tr');
                var data = __avaliacaoSecaoIndex.options.datatables.avaliacaoSecao.fnGetData($pai);

                if (!__avaliacaoSecaoIndex.options.ajax) {
                    location.href = __avaliacaoSecaoIndex.options.url.edit + '/' + data['secao_id'];
                } else {
                    __avaliacaoSecaoIndex.addOverlay(
                        $('#container-avaliacao-secao'), 'Aguarde, carregando dados para edição...'
                    );
                    $.avaliacaoSecaoAdd().pesquisaAvaliacaoSecao(
                        data['secao_id'],
                        function (data) {
                            __avaliacaoSecaoIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-avaliacao-secao-add').click(function (e) {
                if (__avaliacaoSecaoIndex.options.ajax) {
                    __avaliacaoSecaoIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $dataTableAvaliacaoSecao.on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __avaliacaoSecaoIndex.options.datatables.avaliacaoSecao.fnGetData($pai);
                var arrRemover = {
                    secaoId: data['secao_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de seção "' + data['secao_descricao'] + '"?',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __avaliacaoSecaoIndex.addOverlay(
                                $('#container-avaliacao-secao'), 'Aguarde, solicitando remoção de registro de seção...'
                            );
                            $.ajax({
                                url: __avaliacaoSecaoIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __avaliacaoSecaoIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de seção:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __avaliacaoSecaoIndex.removeOverlay($('#container-avaliacao-secao'));
                                    } else {
                                        __avaliacaoSecaoIndex.reloadDataTableAvaliacaoSecao();
                                        __avaliacaoSecaoIndex.showNotificacaoSuccess(
                                            "Registro de seção removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#avaliacao-secao-form'),
                $modal = $('#avaliacao-secao-modal');
            var actionForm = $form.attr('action');
            actionForm = actionForm.split('/');

            if (actionForm[actionForm.length - 2] == 'edit') {
                actionForm = actionForm.slice(0, actionForm.length - 1);
            }

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['secaoId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['secaoId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);


            $modal.find('.modal-title').html(actionTitle + ' seção');
            $modal.modal();
            __avaliacaoSecaoIndex.removeOverlay($('#container-avaliacao-secao'));
        };

        this.reloadDataTableAvaliacaoSecao = function () {
            this.getDataTableAvaliacaoSecao().api().ajax.reload();
        };

        this.getDataTableAvaliacaoSecao = function () {
            if (!__avaliacaoSecaoIndex.options.datatables.avaliacaoSecao) {
                if (!$.fn.dataTable.isDataTable('#dataTableAvaliacaoSecao')) {
                    __avaliacaoSecaoIndex.options.datatables.avaliacaoSecao = $('#dataTableAvaliacaoSecao').DataTable();
                } else {
                    __avaliacaoSecaoIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __avaliacaoSecaoIndex.options.datatables.avaliacaoSecao;
        };

        this.run = function (opts) {
            __avaliacaoSecaoIndex.setDefaults(opts);
            __avaliacaoSecaoIndex.setSteps();
        };
    };

    $.avaliacaoSecaoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.avaliacao.avaliacao-secao.index");

        if (!obj) {
            obj = new AvaliacaoSecaoIndex();
            obj.run(params);
            $(window).data('universa.avaliacao.avaliacao-secao.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);