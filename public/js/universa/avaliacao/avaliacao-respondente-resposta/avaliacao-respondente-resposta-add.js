(function ($, window, document) {
    'use strict';

    var AvaliacaoRespondenteRespostaAdd = function () {
        VersaShared.call(this);
        var __avaliacaoRespondenteRespostaAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                avaliacaoQuestionarioQuestao: '',
                avaliacaoQuestaoResposta: '',
                avaliacaoQuestionarioRespondente: ''
            },
            data: {},
            value: {
                questionarioquestao: null,
                questaoresposta: null,
                respondente: null
            },
            datatables: {},
            wizardElement: '#avaliacao-respondente-resposta-wizard',
            formElement: '#avaliacao-respondente-resposta-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            var $questionarioquestao = $("#questionarioquestao");
            $questionarioquestao.select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __avaliacaoRespondenteRespostaAdd.options.url.avaliacaoQuestionarioQuestao,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.questionarioquestao_id;
                            el.text = el.questionarioquestao_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $questionarioquestao.select2("data", __avaliacaoRespondenteRespostaAdd.getQuestionarioquestao());

            var $questaoresposta = $("#questaoresposta");
            $questaoresposta.select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __avaliacaoRespondenteRespostaAdd.options.url.avaliacaoQuestaoResposta,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.questaoresposta_id;
                            el.text = el.questaoresposta_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $questaoresposta.select2("data", __avaliacaoRespondenteRespostaAdd.getQuestaoresposta());

            var $respondente = $("#respondente");
            $respondente.select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __avaliacaoRespondenteRespostaAdd.options.url.avaliacaoQuestionarioRespondente,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.respondente_id;
                            el.text = el.respondente_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $respondente.select2("data", __avaliacaoRespondenteRespostaAdd.getRespondente());
        };

        this.setQuestionarioquestao = function (questionarioquestao) {
            this.options.value.questionarioquestao = questionarioquestao || null;
        };

        this.getQuestionarioquestao = function () {
            return this.options.value.questionarioquestao || null;
        };

        this.setQuestaoresposta = function (questaoresposta) {
            this.options.value.questaoresposta = questaoresposta || null;
        };

        this.getQuestaoresposta = function () {
            return this.options.value.questaoresposta || null;
        };

        this.setRespondente = function (respondente) {
            this.options.value.respondente = respondente || null;
        };

        this.getRespondente = function () {
            return this.options.value.respondente || null;
        };
        this.setValidations = function () {
            __avaliacaoRespondenteRespostaAdd.options.validator.settings.rules = {
                questionarioquestao: {number: true, required: true},
                questaoresposta: {number: true},
                respondente: {number: true},
                respondenterespostaPeso: {maxlength: 45},
                respondenterespostaComplemento: {maxlength: 128}
            };
            __avaliacaoRespondenteRespostaAdd.options.validator.settings.messages = {
                questionarioquestao: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                questaoresposta: {number: 'Número inválido!'},
                respondente: {number: 'Número inválido!'},
                respondenterespostaPeso: {maxlength: 'Tamanho máximo: 45!'},
                respondenterespostaComplemento: {maxlength: 'Tamanho máximo: 128!'}
            };

            $(__avaliacaoRespondenteRespostaAdd.options.formElement).submit(function (e) {
                var ok = $(__avaliacaoRespondenteRespostaAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__avaliacaoRespondenteRespostaAdd.options.formElement);

                if (__avaliacaoRespondenteRespostaAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __avaliacaoRespondenteRespostaAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __avaliacaoRespondenteRespostaAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__avaliacaoRespondenteRespostaAdd.options.listagem) {
                                    $.avaliacaoRespondenteRespostaIndex().reloadDataTableAvaliacaoRespondenteResposta();
                                    __avaliacaoRespondenteRespostaAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#avaliacao-respondente-resposta-modal').modal('hide');
                                }

                                __avaliacaoRespondenteRespostaAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaAvaliacaoRespondenteResposta = function (respondenteresposta_id, callback) {
            var $form = $(__avaliacaoRespondenteRespostaAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __avaliacaoRespondenteRespostaAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + respondenteresposta_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __avaliacaoRespondenteRespostaAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __avaliacaoRespondenteRespostaAdd.removeOverlay($form);
                },
                erro: function () {
                    __avaliacaoRespondenteRespostaAdd.showNotificacaoDanger('Erro desconhecido!');
                    __avaliacaoRespondenteRespostaAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.avaliacaoRespondenteRespostaAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.avaliacao.avaliacao-respondente-resposta.add");

        if (!obj) {
            obj = new AvaliacaoRespondenteRespostaAdd();
            obj.run(params);
            $(window).data('universa.avaliacao.avaliacao-respondente-resposta.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);