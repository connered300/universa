(function ($, window, document) {
    'use strict';
    var AvaliacaoRespondenteRespostaIndex = function () {
        VersaShared.call(this);
        var __avaliacaoRespondenteRespostaIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                avaliacaoRespondenteResposta: null
            }
        };

        this.setSteps = function () {
            var $dataTableAvaliacaoRespondenteResposta = $('#dataTableAvaliacaoRespondenteResposta');
            var colNum = -1;
            __avaliacaoRespondenteRespostaIndex.options.datatables.avaliacaoRespondenteResposta = $dataTableAvaliacaoRespondenteResposta.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __avaliacaoRespondenteRespostaIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [
                            {class: 'btn-primary item-edit', icon: 'fa-pencil'},
                            {class: 'btn-danger item-remove', icon: 'fa-times'}
                        ];

                        for (var row in data) {
                            data[row]['acao'] = __avaliacaoRespondenteRespostaIndex.createBtnGroup(btns);
                        }

                        __avaliacaoRespondenteRespostaIndex.removeOverlay($('#container-avaliacao-respondente-resposta'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "respondenteresposta_id", targets: ++colNum, data: "respondenteresposta_id"},
                    {name: "questionarioquestao_id", targets: ++colNum, data: "questionarioquestao_id"},
                    {name: "questaoresposta_id", targets: ++colNum, data: "questaoresposta_id"},
                    {name: "respondente_id", targets: ++colNum, data: "respondente_id"},
                    {name: "respondenteresposta_peso", targets: ++colNum, data: "respondenteresposta_peso"},
                    {
                        name: "respondenteresposta_complemento",
                        targets: ++colNum,
                        data: "respondenteresposta_complemento"
                    },
                    {name: "respondenteresposta_id", targets: ++colNum, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $dataTableAvaliacaoRespondenteResposta.on('click', '.item-edit', function () {
                var $pai = $(this).closest('tr');
                var data = __avaliacaoRespondenteRespostaIndex.options.datatables.avaliacaoRespondenteResposta.fnGetData($pai);

                if (!__avaliacaoRespondenteRespostaIndex.options.ajax) {
                    location.href = __avaliacaoRespondenteRespostaIndex.options.url.edit + '/' + data['respondenteresposta_id'];
                } else {
                    __avaliacaoRespondenteRespostaIndex.addOverlay(
                        $('#container-avaliacao-respondente-resposta'), 'Aguarde, carregando dados para edição...'
                    );
                    $.avaliacaoRespondenteRespostaAdd().pesquisaAvaliacaoRespondenteResposta(
                        data['respondenteresposta_id'],
                        function (data) {
                            __avaliacaoRespondenteRespostaIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-avaliacao-respondente-resposta-add').click(function (e) {
                if (__avaliacaoRespondenteRespostaIndex.options.ajax) {
                    __avaliacaoRespondenteRespostaIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $dataTableAvaliacaoRespondenteResposta.on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __avaliacaoRespondenteRespostaIndex.options.datatables.avaliacaoRespondenteResposta.fnGetData($pai);
                var arrRemover = {
                    respondenterespostaId: data['respondenteresposta_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de respondente resposta "' + data['questionarioquestao_id'] + '"?',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __avaliacaoRespondenteRespostaIndex.addOverlay(
                                $('#container-avaliacao-respondente-resposta'), 'Aguarde, solicitando remoção de registro de respondente resposta...'
                            );
                            $.ajax({
                                url: __avaliacaoRespondenteRespostaIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __avaliacaoRespondenteRespostaIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de respondente resposta:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __avaliacaoRespondenteRespostaIndex.removeOverlay($('#container-avaliacao-respondente-resposta'));
                                    } else {
                                        __avaliacaoRespondenteRespostaIndex.reloadDataTableAvaliacaoRespondenteResposta();
                                        __avaliacaoRespondenteRespostaIndex.showNotificacaoSuccess(
                                            "Registro de respondente resposta removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#avaliacao-respondente-resposta-form'),
                $modal = $('#avaliacao-respondente-resposta-modal');
            var actionForm = $form.attr('action');
            actionForm = actionForm.split('/');

            if (actionForm[actionForm.length - 2] == 'edit') {
                actionForm = actionForm.slice(0, actionForm.length - 1);
            }

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['respondenterespostaId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['respondenterespostaId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);


            var $questionarioquestao = $("#questionarioquestao");
            $questionarioquestao.select2('data', data['questionarioquestao']).trigger("change");

            var $questaoresposta = $("#questaoresposta");
            $questaoresposta.select2('data', data['questaoresposta']).trigger("change");

            var $respondente = $("#respondente");
            $respondente.select2('data', data['respondente']).trigger("change");

            $modal.find('.modal-title').html(actionTitle + ' respondente resposta');
            $modal.modal();
            __avaliacaoRespondenteRespostaIndex.removeOverlay($('#container-avaliacao-respondente-resposta'));
        };

        this.reloadDataTableAvaliacaoRespondenteResposta = function () {
            this.getDataTableAvaliacaoRespondenteResposta().api().ajax.reload();
        };

        this.getDataTableAvaliacaoRespondenteResposta = function () {
            if (!__avaliacaoRespondenteRespostaIndex.options.datatables.avaliacaoRespondenteResposta) {
                if (!$.fn.dataTable.isDataTable('#dataTableAvaliacaoRespondenteResposta')) {
                    __avaliacaoRespondenteRespostaIndex.options.datatables.avaliacaoRespondenteResposta = $('#dataTableAvaliacaoRespondenteResposta').DataTable();
                } else {
                    __avaliacaoRespondenteRespostaIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __avaliacaoRespondenteRespostaIndex.options.datatables.avaliacaoRespondenteResposta;
        };

        this.run = function (opts) {
            __avaliacaoRespondenteRespostaIndex.setDefaults(opts);
            __avaliacaoRespondenteRespostaIndex.setSteps();
        };
    };

    $.avaliacaoRespondenteRespostaIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.avaliacao.avaliacao-respondente-resposta.index");

        if (!obj) {
            obj = new AvaliacaoRespondenteRespostaIndex();
            obj.run(params);
            $(window).data('universa.avaliacao.avaliacao-respondente-resposta.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);