(function ($, window, document) {
    'use strict';

    var AvaliacaoQuestaoTipoAdd = function () {
        VersaShared.call(this);
        var __avaliacaoQuestaoTipoAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {},
            data: {},
            value: {},
            datatables: {},
            wizardElement: '#avaliacao-questao-tipo-wizard',
            formElement: '#avaliacao-questao-tipo-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
        };

        this.setValidations = function () {
            __avaliacaoQuestaoTipoAdd.options.validator.settings.rules = {questaotipoDescricao: {maxlength: 45}};
            __avaliacaoQuestaoTipoAdd.options.validator.settings.messages = {questaotipoDescricao: {maxlength: 'Tamanho máximo: 45!'}};

            $(__avaliacaoQuestaoTipoAdd.options.formElement).submit(function (e) {
                var ok = $(__avaliacaoQuestaoTipoAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__avaliacaoQuestaoTipoAdd.options.formElement);

                if (__avaliacaoQuestaoTipoAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __avaliacaoQuestaoTipoAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __avaliacaoQuestaoTipoAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__avaliacaoQuestaoTipoAdd.options.listagem) {
                                    $.avaliacaoQuestaoTipoIndex().reloadDataTableAvaliacaoQuestaoTipo();
                                    __avaliacaoQuestaoTipoAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#avaliacao-questao-tipo-modal').modal('hide');
                                }

                                __avaliacaoQuestaoTipoAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaAvaliacaoQuestaoTipo = function (questaotipo_id, callback) {
            var $form = $(__avaliacaoQuestaoTipoAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __avaliacaoQuestaoTipoAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + questaotipo_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __avaliacaoQuestaoTipoAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __avaliacaoQuestaoTipoAdd.removeOverlay($form);
                },
                erro: function () {
                    __avaliacaoQuestaoTipoAdd.showNotificacaoDanger('Erro desconhecido!');
                    __avaliacaoQuestaoTipoAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.avaliacaoQuestaoTipoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.avaliacao.avaliacao-questao-tipo.add");

        if (!obj) {
            obj = new AvaliacaoQuestaoTipoAdd();
            obj.run(params);
            $(window).data('universa.avaliacao.avaliacao-questao-tipo.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);