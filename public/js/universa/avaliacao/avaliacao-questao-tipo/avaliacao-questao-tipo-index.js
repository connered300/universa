(function ($, window, document) {
    'use strict';
    var AvaliacaoQuestaoTipoIndex = function () {
        VersaShared.call(this);
        var __avaliacaoQuestaoTipoIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                avaliacaoQuestaoTipo: null
            }
        };

        this.setSteps = function () {
            var $dataTableAvaliacaoQuestaoTipo = $('#dataTableAvaliacaoQuestaoTipo');
            var colNum = -1;
            __avaliacaoQuestaoTipoIndex.options.datatables.avaliacaoQuestaoTipo = $dataTableAvaliacaoQuestaoTipo.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __avaliacaoQuestaoTipoIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [
                            {class: 'btn-primary item-edit', icon: 'fa-pencil'},
                            {class: 'btn-danger item-remove', icon: 'fa-times'}
                        ];

                        for (var row in data) {
                            data[row]['acao'] = __avaliacaoQuestaoTipoIndex.createBtnGroup(btns);
                        }

                        __avaliacaoQuestaoTipoIndex.removeOverlay($('#container-avaliacao-questao-tipo'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "questaotipo_id", targets: ++colNum, data: "questaotipo_id"},
                    {name: "questaotipo_descricao", targets: ++colNum, data: "questaotipo_descricao"},
                    {name: "questaotipo_id", targets: ++colNum, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $dataTableAvaliacaoQuestaoTipo.on('click', '.item-edit', function () {
                var $pai = $(this).closest('tr');
                var data = __avaliacaoQuestaoTipoIndex.options.datatables.avaliacaoQuestaoTipo.fnGetData($pai);

                if (!__avaliacaoQuestaoTipoIndex.options.ajax) {
                    location.href = __avaliacaoQuestaoTipoIndex.options.url.edit + '/' + data['questaotipo_id'];
                } else {
                    __avaliacaoQuestaoTipoIndex.addOverlay(
                        $('#container-avaliacao-questao-tipo'), 'Aguarde, carregando dados para edição...'
                    );
                    $.avaliacaoQuestaoTipoAdd().pesquisaAvaliacaoQuestaoTipo(
                        data['questaotipo_id'],
                        function (data) {
                            __avaliacaoQuestaoTipoIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-avaliacao-questao-tipo-add').click(function (e) {
                if (__avaliacaoQuestaoTipoIndex.options.ajax) {
                    __avaliacaoQuestaoTipoIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $dataTableAvaliacaoQuestaoTipo.on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __avaliacaoQuestaoTipoIndex.options.datatables.avaliacaoQuestaoTipo.fnGetData($pai);
                var arrRemover = {
                    questaotipoId: data['questaotipo_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de questão tipo "' + data['questaotipo_descricao'] + '"?',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __avaliacaoQuestaoTipoIndex.addOverlay(
                                $('#container-avaliacao-questao-tipo'), 'Aguarde, solicitando remoção de registro de questão tipo...'
                            );
                            $.ajax({
                                url: __avaliacaoQuestaoTipoIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __avaliacaoQuestaoTipoIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de questão tipo:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __avaliacaoQuestaoTipoIndex.removeOverlay($('#container-avaliacao-questao-tipo'));
                                    } else {
                                        __avaliacaoQuestaoTipoIndex.reloadDataTableAvaliacaoQuestaoTipo();
                                        __avaliacaoQuestaoTipoIndex.showNotificacaoSuccess(
                                            "Registro de questão tipo removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#avaliacao-questao-tipo-form'),
                $modal = $('#avaliacao-questao-tipo-modal');
            var actionForm = $form.attr('action');
            actionForm = actionForm.split('/');

            if (actionForm[actionForm.length - 2] == 'edit') {
                actionForm = actionForm.slice(0, actionForm.length - 1);
            }

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['questaotipoId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['questaotipoId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);


            $modal.find('.modal-title').html(actionTitle + ' questão tipo');
            $modal.modal();
            __avaliacaoQuestaoTipoIndex.removeOverlay($('#container-avaliacao-questao-tipo'));
        };

        this.reloadDataTableAvaliacaoQuestaoTipo = function () {
            this.getDataTableAvaliacaoQuestaoTipo().api().ajax.reload();
        };

        this.getDataTableAvaliacaoQuestaoTipo = function () {
            if (!__avaliacaoQuestaoTipoIndex.options.datatables.avaliacaoQuestaoTipo) {
                if (!$.fn.dataTable.isDataTable('#dataTableAvaliacaoQuestaoTipo')) {
                    __avaliacaoQuestaoTipoIndex.options.datatables.avaliacaoQuestaoTipo = $('#dataTableAvaliacaoQuestaoTipo').DataTable();
                } else {
                    __avaliacaoQuestaoTipoIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __avaliacaoQuestaoTipoIndex.options.datatables.avaliacaoQuestaoTipo;
        };

        this.run = function (opts) {
            __avaliacaoQuestaoTipoIndex.setDefaults(opts);
            __avaliacaoQuestaoTipoIndex.setSteps();
        };
    };

    $.avaliacaoQuestaoTipoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.avaliacao.avaliacao-questao-tipo.index");

        if (!obj) {
            obj = new AvaliacaoQuestaoTipoIndex();
            obj.run(params);
            $(window).data('universa.avaliacao.avaliacao-questao-tipo.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);