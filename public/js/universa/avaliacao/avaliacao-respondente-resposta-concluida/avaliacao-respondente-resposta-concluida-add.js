(function ($, window, document) {
    'use strict';

    var AvaliacaoRespondenteRespostaConcluidaAdd = function () {
        VersaShared.call(this);
        var __avaliacaoRespondenteRespostaConcluidaAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                avaliacaoQuestionarioQuestao: '',
                avaliacaoQuestaoResposta: '',
                avaliacaoQuestionarioRespondente: ''
            },
            data: {},
            value: {
                questionarioquestao: null,
                questaoresposta: null,
                respondente: null
            },
            datatables: {},
            wizardElement: '#avaliacao-respondente-resposta-concluida-wizard',
            formElement: '#avaliacao-respondente-resposta-concluida-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            var $questionarioquestao = $("#questionarioquestao");
            $questionarioquestao.select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __avaliacaoRespondenteRespostaConcluidaAdd.options.url.avaliacaoQuestionarioQuestao,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.questionarioquestao_id;
                            el.text = el.questionarioquestao_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $questionarioquestao.select2("data", __avaliacaoRespondenteRespostaConcluidaAdd.getQuestionarioquestao());

            var $questaoresposta = $("#questaoresposta");
            $questaoresposta.select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __avaliacaoRespondenteRespostaConcluidaAdd.options.url.avaliacaoQuestaoResposta,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.questaoresposta_id;
                            el.text = el.questaoresposta_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $questaoresposta.select2("data", __avaliacaoRespondenteRespostaConcluidaAdd.getQuestaoresposta());

            var $respondente = $("#respondente");
            $respondente.select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __avaliacaoRespondenteRespostaConcluidaAdd.options.url.avaliacaoQuestionarioRespondente,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.respondente_id;
                            el.text = el.respondente_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $respondente.select2("data", __avaliacaoRespondenteRespostaConcluidaAdd.getRespondente());

            var $respondenterespostaSubmissao = $("#respondenterespostaSubmissao");
            __avaliacaoRespondenteRespostaConcluidaAdd.iniciarElementoDatePicker($respondenterespostaSubmissao);
        };

        this.setQuestionarioquestao = function (questionarioquestao) {
            this.options.value.questionarioquestao = questionarioquestao || null;
        };

        this.getQuestionarioquestao = function () {
            return this.options.value.questionarioquestao || null;
        };

        this.setQuestaoresposta = function (questaoresposta) {
            this.options.value.questaoresposta = questaoresposta || null;
        };

        this.getQuestaoresposta = function () {
            return this.options.value.questaoresposta || null;
        };

        this.setRespondente = function (respondente) {
            this.options.value.respondente = respondente || null;
        };

        this.getRespondente = function () {
            return this.options.value.respondente || null;
        };
        this.setValidations = function () {
            __avaliacaoRespondenteRespostaConcluidaAdd.options.validator.settings.rules = {
                questionarioquestao: {number: true, required: true},
                questaoresposta: {number: true, required: true},
                respondente: {number: true, required: true},
                respondenterespostaSubmissao: {required: true, dateBR: true}
            };
            __avaliacaoRespondenteRespostaConcluidaAdd.options.validator.settings.messages = {
                questionarioquestao: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                questaoresposta: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                respondente: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                respondenterespostaSubmissao: {required: 'Campo obrigatório!', dateBR: 'Informe uma data válida!'}
            };

            $(__avaliacaoRespondenteRespostaConcluidaAdd.options.formElement).submit(function (e) {
                var ok = $(__avaliacaoRespondenteRespostaConcluidaAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__avaliacaoRespondenteRespostaConcluidaAdd.options.formElement);

                if (__avaliacaoRespondenteRespostaConcluidaAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __avaliacaoRespondenteRespostaConcluidaAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __avaliacaoRespondenteRespostaConcluidaAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__avaliacaoRespondenteRespostaConcluidaAdd.options.listagem) {
                                    $.avaliacaoRespondenteRespostaConcluidaIndex().reloadDataTableAvaliacaoRespondenteRespostaConcluida();
                                    __avaliacaoRespondenteRespostaConcluidaAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#avaliacao-respondente-resposta-concluida-modal').modal('hide');
                                }

                                __avaliacaoRespondenteRespostaConcluidaAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaAvaliacaoRespondenteRespostaConcluida = function (respondenteresposta_id, callback) {
            var $form = $(__avaliacaoRespondenteRespostaConcluidaAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __avaliacaoRespondenteRespostaConcluidaAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + respondenteresposta_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __avaliacaoRespondenteRespostaConcluidaAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __avaliacaoRespondenteRespostaConcluidaAdd.removeOverlay($form);
                },
                erro: function () {
                    __avaliacaoRespondenteRespostaConcluidaAdd.showNotificacaoDanger('Erro desconhecido!');
                    __avaliacaoRespondenteRespostaConcluidaAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.avaliacaoRespondenteRespostaConcluidaAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.avaliacao.avaliacao-respondente-resposta-concluida.add");

        if (!obj) {
            obj = new AvaliacaoRespondenteRespostaConcluidaAdd();
            obj.run(params);
            $(window).data('universa.avaliacao.avaliacao-respondente-resposta-concluida.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);