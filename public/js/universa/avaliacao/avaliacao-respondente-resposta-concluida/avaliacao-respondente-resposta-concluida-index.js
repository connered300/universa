(function ($, window, document) {
    'use strict';
    var AvaliacaoRespondenteRespostaConcluidaIndex = function () {
        VersaShared.call(this);
        var __avaliacaoRespondenteRespostaConcluidaIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                avaliacaoRespondenteRespostaConcluida: null
            }
        };

        this.setSteps = function () {
            var $dataTableAvaliacaoRespondenteRespostaConcluida = $('#dataTableAvaliacaoRespondenteRespostaConcluida');
            var colNum = -1;
            __avaliacaoRespondenteRespostaConcluidaIndex.options.datatables.avaliacaoRespondenteRespostaConcluida = $dataTableAvaliacaoRespondenteRespostaConcluida.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __avaliacaoRespondenteRespostaConcluidaIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [
                            {class: 'btn-primary item-edit', icon: 'fa-pencil'},
                            {class: 'btn-danger item-remove', icon: 'fa-times'}
                        ];

                        for (var row in data) {
                            data[row]['acao'] = __avaliacaoRespondenteRespostaConcluidaIndex.createBtnGroup(btns);
                        }

                        __avaliacaoRespondenteRespostaConcluidaIndex.removeOverlay($('#container-avaliacao-respondente-resposta-concluida'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "respondenteresposta_id", targets: ++colNum, data: "respondenteresposta_id"},
                    {name: "questionarioquestao_id", targets: ++colNum, data: "questionarioquestao_id"},
                    {name: "questaoresposta_id", targets: ++colNum, data: "questaoresposta_id"},
                    {name: "respondente_id", targets: ++colNum, data: "respondente_id"},
                    {name: "respondenteresposta_submissao", targets: ++colNum, data: "respondenteresposta_submissao"},
                    {name: "respondenteresposta_id", targets: ++colNum, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $dataTableAvaliacaoRespondenteRespostaConcluida.on('click', '.item-edit', function () {
                var $pai = $(this).closest('tr');
                var data = __avaliacaoRespondenteRespostaConcluidaIndex.options.datatables.avaliacaoRespondenteRespostaConcluida.fnGetData($pai);

                if (!__avaliacaoRespondenteRespostaConcluidaIndex.options.ajax) {
                    location.href = __avaliacaoRespondenteRespostaConcluidaIndex.options.url.edit + '/' + data['respondenteresposta_id'];
                } else {
                    __avaliacaoRespondenteRespostaConcluidaIndex.addOverlay(
                        $('#container-avaliacao-respondente-resposta-concluida'), 'Aguarde, carregando dados para edição...'
                    );
                    $.avaliacaoRespondenteRespostaConcluidaAdd().pesquisaAvaliacaoRespondenteRespostaConcluida(
                        data['respondenteresposta_id'],
                        function (data) {
                            __avaliacaoRespondenteRespostaConcluidaIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-avaliacao-respondente-resposta-concluida-add').click(function (e) {
                if (__avaliacaoRespondenteRespostaConcluidaIndex.options.ajax) {
                    __avaliacaoRespondenteRespostaConcluidaIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $dataTableAvaliacaoRespondenteRespostaConcluida.on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __avaliacaoRespondenteRespostaConcluidaIndex.options.datatables.avaliacaoRespondenteRespostaConcluida.fnGetData($pai);
                var arrRemover = {
                    respondenterespostaId: data['respondenteresposta_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de respondente resposta concluida "' + data['questionarioquestao_id'] + '"?',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __avaliacaoRespondenteRespostaConcluidaIndex.addOverlay(
                                $('#container-avaliacao-respondente-resposta-concluida'), 'Aguarde, solicitando remoção de registro de respondente resposta concluida...'
                            );
                            $.ajax({
                                url: __avaliacaoRespondenteRespostaConcluidaIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __avaliacaoRespondenteRespostaConcluidaIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de respondente resposta concluida:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __avaliacaoRespondenteRespostaConcluidaIndex.removeOverlay($('#container-avaliacao-respondente-resposta-concluida'));
                                    } else {
                                        __avaliacaoRespondenteRespostaConcluidaIndex.reloadDataTableAvaliacaoRespondenteRespostaConcluida();
                                        __avaliacaoRespondenteRespostaConcluidaIndex.showNotificacaoSuccess(
                                            "Registro de respondente resposta concluida removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#avaliacao-respondente-resposta-concluida-form'),
                $modal = $('#avaliacao-respondente-resposta-concluida-modal');
            var actionForm = $form.attr('action');
            actionForm = actionForm.split('/');

            if (actionForm[actionForm.length - 2] == 'edit') {
                actionForm = actionForm.slice(0, actionForm.length - 1);
            }

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['respondenterespostaId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['respondenterespostaId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);


            var $questionarioquestao = $("#questionarioquestao");
            $questionarioquestao.select2('data', data['questionarioquestao']).trigger("change");

            var $questaoresposta = $("#questaoresposta");
            $questaoresposta.select2('data', data['questaoresposta']).trigger("change");

            var $respondente = $("#respondente");
            $respondente.select2('data', data['respondente']).trigger("change");

            $modal.find('.modal-title').html(actionTitle + ' respondente resposta concluida');
            $modal.modal();
            __avaliacaoRespondenteRespostaConcluidaIndex.removeOverlay($('#container-avaliacao-respondente-resposta-concluida'));
        };

        this.reloadDataTableAvaliacaoRespondenteRespostaConcluida = function () {
            this.getDataTableAvaliacaoRespondenteRespostaConcluida().api().ajax.reload();
        };

        this.getDataTableAvaliacaoRespondenteRespostaConcluida = function () {
            if (!__avaliacaoRespondenteRespostaConcluidaIndex.options.datatables.avaliacaoRespondenteRespostaConcluida) {
                if (!$.fn.dataTable.isDataTable('#dataTableAvaliacaoRespondenteRespostaConcluida')) {
                    __avaliacaoRespondenteRespostaConcluidaIndex.options.datatables.avaliacaoRespondenteRespostaConcluida = $('#dataTableAvaliacaoRespondenteRespostaConcluida').DataTable();
                } else {
                    __avaliacaoRespondenteRespostaConcluidaIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __avaliacaoRespondenteRespostaConcluidaIndex.options.datatables.avaliacaoRespondenteRespostaConcluida;
        };

        this.run = function (opts) {
            __avaliacaoRespondenteRespostaConcluidaIndex.setDefaults(opts);
            __avaliacaoRespondenteRespostaConcluidaIndex.setSteps();
        };
    };

    $.avaliacaoRespondenteRespostaConcluidaIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.avaliacao.avaliacao-respondente-resposta-concluida.index");

        if (!obj) {
            obj = new AvaliacaoRespondenteRespostaConcluidaIndex();
            obj.run(params);
            $(window).data('universa.avaliacao.avaliacao-respondente-resposta-concluida.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);