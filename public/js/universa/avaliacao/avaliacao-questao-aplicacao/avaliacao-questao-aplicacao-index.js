(function ($, window, document) {
    'use strict';
    var AvaliacaoQuestaoAplicacaoIndex = function () {
        VersaShared.call(this);
        var __avaliacaoQuestaoAplicacaoIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                avaliacaoQuestaoAplicacao: null
            }
        };

        this.setSteps = function () {
            var $dataTableAvaliacaoQuestaoAplicacao = $('#dataTableAvaliacaoQuestaoAplicacao');
            var colNum = -1;
            __avaliacaoQuestaoAplicacaoIndex.options.datatables.avaliacaoQuestaoAplicacao = $dataTableAvaliacaoQuestaoAplicacao.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __avaliacaoQuestaoAplicacaoIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [
                            {class: 'btn-primary item-edit', icon: 'fa-pencil'},
                            {class: 'btn-danger item-remove', icon: 'fa-times'}
                        ];

                        for (var row in data) {
                            data[row]['acao'] = __avaliacaoQuestaoAplicacaoIndex.createBtnGroup(btns);
                        }

                        __avaliacaoQuestaoAplicacaoIndex.removeOverlay($('#container-avaliacao-questao-aplicacao'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "aplicacao_id", targets: ++colNum, data: "aplicacao_id"},
                    {name: "aplicacao_descricao", targets: ++colNum, data: "aplicacao_descricao"},
                    {name: "aplicacao_id", targets: ++colNum, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $dataTableAvaliacaoQuestaoAplicacao.on('click', '.item-edit', function () {
                var $pai = $(this).closest('tr');
                var data = __avaliacaoQuestaoAplicacaoIndex.options.datatables.avaliacaoQuestaoAplicacao.fnGetData($pai);

                if (!__avaliacaoQuestaoAplicacaoIndex.options.ajax) {
                    location.href = __avaliacaoQuestaoAplicacaoIndex.options.url.edit + '/' + data['aplicacao_id'];
                } else {
                    __avaliacaoQuestaoAplicacaoIndex.addOverlay(
                        $('#container-avaliacao-questao-aplicacao'), 'Aguarde, carregando dados para edição...'
                    );
                    $.avaliacaoQuestaoAplicacaoAdd().pesquisaAvaliacaoQuestaoAplicacao(
                        data['aplicacao_id'],
                        function (data) {
                            __avaliacaoQuestaoAplicacaoIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-avaliacao-questao-aplicacao-add').click(function (e) {
                if (__avaliacaoQuestaoAplicacaoIndex.options.ajax) {
                    __avaliacaoQuestaoAplicacaoIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $dataTableAvaliacaoQuestaoAplicacao.on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __avaliacaoQuestaoAplicacaoIndex.options.datatables.avaliacaoQuestaoAplicacao.fnGetData($pai);
                var arrRemover = {
                    aplicacaoId: data['aplicacao_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de questão aplicação "' + data['aplicacao_descricao'] + '"?',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __avaliacaoQuestaoAplicacaoIndex.addOverlay(
                                $('#container-avaliacao-questao-aplicacao'), 'Aguarde, solicitando remoção de registro de questão aplicação...'
                            );
                            $.ajax({
                                url: __avaliacaoQuestaoAplicacaoIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __avaliacaoQuestaoAplicacaoIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de questão aplicação:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __avaliacaoQuestaoAplicacaoIndex.removeOverlay($('#container-avaliacao-questao-aplicacao'));
                                    } else {
                                        __avaliacaoQuestaoAplicacaoIndex.reloadDataTableAvaliacaoQuestaoAplicacao();
                                        __avaliacaoQuestaoAplicacaoIndex.showNotificacaoSuccess(
                                            "Registro de questão aplicação removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#avaliacao-questao-aplicacao-form'),
                $modal = $('#avaliacao-questao-aplicacao-modal');
            var actionForm = $form.attr('action');
            actionForm = actionForm.split('/');

            if (actionForm[actionForm.length - 2] == 'edit') {
                actionForm = actionForm.slice(0, actionForm.length - 1);
            }

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['aplicacaoId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['aplicacaoId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);


            $modal.find('.modal-title').html(actionTitle + ' questão aplicação');
            $modal.modal();
            __avaliacaoQuestaoAplicacaoIndex.removeOverlay($('#container-avaliacao-questao-aplicacao'));
        };

        this.reloadDataTableAvaliacaoQuestaoAplicacao = function () {
            this.getDataTableAvaliacaoQuestaoAplicacao().api().ajax.reload();
        };

        this.getDataTableAvaliacaoQuestaoAplicacao = function () {
            if (!__avaliacaoQuestaoAplicacaoIndex.options.datatables.avaliacaoQuestaoAplicacao) {
                if (!$.fn.dataTable.isDataTable('#dataTableAvaliacaoQuestaoAplicacao')) {
                    __avaliacaoQuestaoAplicacaoIndex.options.datatables.avaliacaoQuestaoAplicacao = $('#dataTableAvaliacaoQuestaoAplicacao').DataTable();
                } else {
                    __avaliacaoQuestaoAplicacaoIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __avaliacaoQuestaoAplicacaoIndex.options.datatables.avaliacaoQuestaoAplicacao;
        };

        this.run = function (opts) {
            __avaliacaoQuestaoAplicacaoIndex.setDefaults(opts);
            __avaliacaoQuestaoAplicacaoIndex.setSteps();
        };
    };

    $.avaliacaoQuestaoAplicacaoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.avaliacao.avaliacao-questao-aplicacao.index");

        if (!obj) {
            obj = new AvaliacaoQuestaoAplicacaoIndex();
            obj.run(params);
            $(window).data('universa.avaliacao.avaliacao-questao-aplicacao.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);