(function ($, window, document) {
    'use strict';

    var AvaliacaoQuestaoAplicacaoAdd = function () {
        VersaShared.call(this);
        var __avaliacaoQuestaoAplicacaoAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {},
            data: {},
            value: {},
            datatables: {},
            wizardElement: '#avaliacao-questao-aplicacao-wizard',
            formElement: '#avaliacao-questao-aplicacao-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
        };

        this.setValidations = function () {
            __avaliacaoQuestaoAplicacaoAdd.options.validator.settings.rules = {aplicacaoDescricao: {maxlength: 45}};
            __avaliacaoQuestaoAplicacaoAdd.options.validator.settings.messages = {aplicacaoDescricao: {maxlength: 'Tamanho máximo: 45!'}};

            $(__avaliacaoQuestaoAplicacaoAdd.options.formElement).submit(function (e) {
                var ok = $(__avaliacaoQuestaoAplicacaoAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__avaliacaoQuestaoAplicacaoAdd.options.formElement);

                if (__avaliacaoQuestaoAplicacaoAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __avaliacaoQuestaoAplicacaoAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __avaliacaoQuestaoAplicacaoAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__avaliacaoQuestaoAplicacaoAdd.options.listagem) {
                                    $.avaliacaoQuestaoAplicacaoIndex().reloadDataTableAvaliacaoQuestaoAplicacao();
                                    __avaliacaoQuestaoAplicacaoAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#avaliacao-questao-aplicacao-modal').modal('hide');
                                }

                                __avaliacaoQuestaoAplicacaoAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaAvaliacaoQuestaoAplicacao = function (aplicacao_id, callback) {
            var $form = $(__avaliacaoQuestaoAplicacaoAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __avaliacaoQuestaoAplicacaoAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + aplicacao_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __avaliacaoQuestaoAplicacaoAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __avaliacaoQuestaoAplicacaoAdd.removeOverlay($form);
                },
                erro: function () {
                    __avaliacaoQuestaoAplicacaoAdd.showNotificacaoDanger('Erro desconhecido!');
                    __avaliacaoQuestaoAplicacaoAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.avaliacaoQuestaoAplicacaoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.avaliacao.avaliacao-questao-aplicacao.add");

        if (!obj) {
            obj = new AvaliacaoQuestaoAplicacaoAdd();
            obj.run(params);
            $(window).data('universa.avaliacao.avaliacao-questao-aplicacao.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);