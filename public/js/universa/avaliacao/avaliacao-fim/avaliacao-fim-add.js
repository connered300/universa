(function ($, window, document) {
    'use strict';

    var AvaliacaoFimAdd = function () {
        VersaShared.call(this);
        var __avaliacaoFimAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {},
            data: {},
            value: {},
            datatables: {},
            wizardElement: '#avaliacao-fim-wizard',
            formElement: '#avaliacao-fim-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
        };

        this.setValidations = function () {
            __avaliacaoFimAdd.options.validator.settings.rules = {
                fimNome: {maxlength: 45},
                fimDescricao: {maxlength: 250}
            };
            __avaliacaoFimAdd.options.validator.settings.messages = {
                fimNome: {maxlength: 'Tamanho máximo: 45!'},
                fimDescricao: {maxlength: 'Tamanho máximo: 250!'}
            };

            $(__avaliacaoFimAdd.options.formElement).submit(function (e) {
                var ok = $(__avaliacaoFimAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__avaliacaoFimAdd.options.formElement);

                if (__avaliacaoFimAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __avaliacaoFimAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __avaliacaoFimAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__avaliacaoFimAdd.options.listagem) {
                                    $.avaliacaoFimIndex().reloadDataTableAvaliacaoFim();
                                    __avaliacaoFimAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#avaliacao-fim-modal').modal('hide');
                                }

                                __avaliacaoFimAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaAvaliacaoFim = function (fim_id, callback) {
            var $form = $(__avaliacaoFimAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __avaliacaoFimAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + fim_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __avaliacaoFimAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __avaliacaoFimAdd.removeOverlay($form);
                },
                erro: function () {
                    __avaliacaoFimAdd.showNotificacaoDanger('Erro desconhecido!');
                    __avaliacaoFimAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.avaliacaoFimAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.avaliacao.avaliacao-fim.add");

        if (!obj) {
            obj = new AvaliacaoFimAdd();
            obj.run(params);
            $(window).data('universa.avaliacao.avaliacao-fim.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);