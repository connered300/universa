(function ($, window, document) {
    'use strict';
    var AvaliacaoFimIndex = function () {
        VersaShared.call(this);
        var __avaliacaoFimIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                avaliacaoFim: null
            }
        };

        this.setSteps = function () {
            var $dataTableAvaliacaoFim = $('#dataTableAvaliacaoFim');
            var colNum = -1;
            __avaliacaoFimIndex.options.datatables.avaliacaoFim = $dataTableAvaliacaoFim.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __avaliacaoFimIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [
                            {class: 'btn-primary item-edit', icon: 'fa-pencil'},
                            {class: 'btn-danger item-remove', icon: 'fa-times'}
                        ];

                        for (var row in data) {
                            data[row]['acao'] = __avaliacaoFimIndex.createBtnGroup(btns);
                        }

                        __avaliacaoFimIndex.removeOverlay($('#container-avaliacao-fim'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "fim_id", targets: ++colNum, data: "fim_id"},
                    {name: "fim_nome", targets: ++colNum, data: "fim_nome"},
                    {name: "fim_descricao", targets: ++colNum, data: "fim_descricao"},
                    {name: "fim_id", targets: ++colNum, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $dataTableAvaliacaoFim.on('click', '.item-edit', function () {
                var $pai = $(this).closest('tr');
                var data = __avaliacaoFimIndex.options.datatables.avaliacaoFim.fnGetData($pai);

                if (!__avaliacaoFimIndex.options.ajax) {
                    location.href = __avaliacaoFimIndex.options.url.edit + '/' + data['fim_id'];
                } else {
                    __avaliacaoFimIndex.addOverlay(
                        $('#container-avaliacao-fim'), 'Aguarde, carregando dados para edição...'
                    );
                    $.avaliacaoFimAdd().pesquisaAvaliacaoFim(
                        data['fim_id'],
                        function (data) {
                            __avaliacaoFimIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-avaliacao-fim-add').click(function (e) {
                if (__avaliacaoFimIndex.options.ajax) {
                    __avaliacaoFimIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $dataTableAvaliacaoFim.on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __avaliacaoFimIndex.options.datatables.avaliacaoFim.fnGetData($pai);
                var arrRemover = {
                    fimId: data['fim_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de fim "' + data['fim_descricao'] + '"?',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __avaliacaoFimIndex.addOverlay(
                                $('#container-avaliacao-fim'), 'Aguarde, solicitando remoção de registro de fim...'
                            );
                            $.ajax({
                                url: __avaliacaoFimIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __avaliacaoFimIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de fim:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __avaliacaoFimIndex.removeOverlay($('#container-avaliacao-fim'));
                                    } else {
                                        __avaliacaoFimIndex.reloadDataTableAvaliacaoFim();
                                        __avaliacaoFimIndex.showNotificacaoSuccess(
                                            "Registro de fim removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#avaliacao-fim-form'),
                $modal = $('#avaliacao-fim-modal');
            var actionForm = $form.attr('action');
            actionForm = actionForm.split('/');

            if (actionForm[actionForm.length - 2] == 'edit') {
                actionForm = actionForm.slice(0, actionForm.length - 1);
            }

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['fimId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['fimId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);


            $modal.find('.modal-title').html(actionTitle + ' fim');
            $modal.modal();
            __avaliacaoFimIndex.removeOverlay($('#container-avaliacao-fim'));
        };

        this.reloadDataTableAvaliacaoFim = function () {
            this.getDataTableAvaliacaoFim().api().ajax.reload();
        };

        this.getDataTableAvaliacaoFim = function () {
            if (!__avaliacaoFimIndex.options.datatables.avaliacaoFim) {
                if (!$.fn.dataTable.isDataTable('#dataTableAvaliacaoFim')) {
                    __avaliacaoFimIndex.options.datatables.avaliacaoFim = $('#dataTableAvaliacaoFim').DataTable();
                } else {
                    __avaliacaoFimIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __avaliacaoFimIndex.options.datatables.avaliacaoFim;
        };

        this.run = function (opts) {
            __avaliacaoFimIndex.setDefaults(opts);
            __avaliacaoFimIndex.setSteps();
        };
    };

    $.avaliacaoFimIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.avaliacao.avaliacao-fim.index");

        if (!obj) {
            obj = new AvaliacaoFimIndex();
            obj.run(params);
            $(window).data('universa.avaliacao.avaliacao-fim.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);