(function ($, window, document) {
    'use strict';
    var AvaliacaoQuestaoRespostaIndex = function () {
        VersaShared.call(this);
        var __avaliacaoQuestaoRespostaIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                avaliacaoQuestaoResposta: null
            }
        };

        this.setSteps = function () {
            var $dataTableAvaliacaoQuestaoResposta = $('#dataTableAvaliacaoQuestaoResposta');
            var colNum = -1;
            __avaliacaoQuestaoRespostaIndex.options.datatables.avaliacaoQuestaoResposta = $dataTableAvaliacaoQuestaoResposta.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __avaliacaoQuestaoRespostaIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [
                            {class: 'btn-primary item-edit', icon: 'fa-pencil'},
                            {class: 'btn-danger item-remove', icon: 'fa-times'}
                        ];

                        for (var row in data) {
                            data[row]['acao'] = __avaliacaoQuestaoRespostaIndex.createBtnGroup(btns);
                        }

                        __avaliacaoQuestaoRespostaIndex.removeOverlay($('#container-avaliacao-questao-resposta'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "questaoresposta_id", targets: ++colNum, data: "questaoresposta_id"},
                    {name: "questao_id", targets: ++colNum, data: "questao_id"},
                    {name: "questaoresposta_descricao", targets: ++colNum, data: "questaoresposta_descricao"},
                    {name: "questaoresposta_posicao", targets: ++colNum, data: "questaoresposta_posicao"},
                    {name: "questaoresposta_gabarito", targets: ++colNum, data: "questaoresposta_gabarito"},
                    {name: "questaoresposta_id", targets: ++colNum, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $dataTableAvaliacaoQuestaoResposta.on('click', '.item-edit', function () {
                var $pai = $(this).closest('tr');
                var data = __avaliacaoQuestaoRespostaIndex.options.datatables.avaliacaoQuestaoResposta.fnGetData($pai);

                if (!__avaliacaoQuestaoRespostaIndex.options.ajax) {
                    location.href = __avaliacaoQuestaoRespostaIndex.options.url.edit + '/' + data['questaoresposta_id'];
                } else {
                    __avaliacaoQuestaoRespostaIndex.addOverlay(
                        $('#container-avaliacao-questao-resposta'), 'Aguarde, carregando dados para edição...'
                    );
                    $.avaliacaoQuestaoRespostaAdd().pesquisaAvaliacaoQuestaoResposta(
                        data['questaoresposta_id'],
                        function (data) {
                            __avaliacaoQuestaoRespostaIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-avaliacao-questao-resposta-add').click(function (e) {
                if (__avaliacaoQuestaoRespostaIndex.options.ajax) {
                    __avaliacaoQuestaoRespostaIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $dataTableAvaliacaoQuestaoResposta.on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __avaliacaoQuestaoRespostaIndex.options.datatables.avaliacaoQuestaoResposta.fnGetData($pai);
                var arrRemover = {
                    questaorespostaId: data['questaoresposta_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de questão resposta "' + data['questaoresposta_descricao'] + '"?',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __avaliacaoQuestaoRespostaIndex.addOverlay(
                                $('#container-avaliacao-questao-resposta'), 'Aguarde, solicitando remoção de registro de questão resposta...'
                            );
                            $.ajax({
                                url: __avaliacaoQuestaoRespostaIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __avaliacaoQuestaoRespostaIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de questão resposta:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __avaliacaoQuestaoRespostaIndex.removeOverlay($('#container-avaliacao-questao-resposta'));
                                    } else {
                                        __avaliacaoQuestaoRespostaIndex.reloadDataTableAvaliacaoQuestaoResposta();
                                        __avaliacaoQuestaoRespostaIndex.showNotificacaoSuccess(
                                            "Registro de questão resposta removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#avaliacao-questao-resposta-form'),
                $modal = $('#avaliacao-questao-resposta-modal');
            var actionForm = $form.attr('action');
            actionForm = actionForm.split('/');

            if (actionForm[actionForm.length - 2] == 'edit') {
                actionForm = actionForm.slice(0, actionForm.length - 1);
            }

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['questaorespostaId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['questaorespostaId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);


            var $questao = $("#questao");
            $questao.select2('data', data['questao']).trigger("change");

            var $questaorespostaGabarito = $("#questaorespostaGabarito");
            $questaorespostaGabarito.select2('val', data['questaorespostaGabarito']).trigger("change");

            $modal.find('.modal-title').html(actionTitle + ' questão resposta');
            $modal.modal();
            __avaliacaoQuestaoRespostaIndex.removeOverlay($('#container-avaliacao-questao-resposta'));
        };

        this.reloadDataTableAvaliacaoQuestaoResposta = function () {
            this.getDataTableAvaliacaoQuestaoResposta().api().ajax.reload();
        };

        this.getDataTableAvaliacaoQuestaoResposta = function () {
            if (!__avaliacaoQuestaoRespostaIndex.options.datatables.avaliacaoQuestaoResposta) {
                if (!$.fn.dataTable.isDataTable('#dataTableAvaliacaoQuestaoResposta')) {
                    __avaliacaoQuestaoRespostaIndex.options.datatables.avaliacaoQuestaoResposta = $('#dataTableAvaliacaoQuestaoResposta').DataTable();
                } else {
                    __avaliacaoQuestaoRespostaIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __avaliacaoQuestaoRespostaIndex.options.datatables.avaliacaoQuestaoResposta;
        };

        this.run = function (opts) {
            __avaliacaoQuestaoRespostaIndex.setDefaults(opts);
            __avaliacaoQuestaoRespostaIndex.setSteps();
        };
    };

    $.avaliacaoQuestaoRespostaIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.avaliacao.avaliacao-questao-resposta.index");

        if (!obj) {
            obj = new AvaliacaoQuestaoRespostaIndex();
            obj.run(params);
            $(window).data('universa.avaliacao.avaliacao-questao-resposta.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);