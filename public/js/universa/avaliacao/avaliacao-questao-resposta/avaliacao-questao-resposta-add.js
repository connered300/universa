(function ($, window, document) {
    'use strict';

    var AvaliacaoQuestaoRespostaAdd = function () {
        VersaShared.call(this);
        var __avaliacaoQuestaoRespostaAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {avaliacaoQuestao: ''},
            data: {},
            value: {questao: null},
            datatables: {},
            wizardElement: '#avaliacao-questao-resposta-wizard',
            formElement: '#avaliacao-questao-resposta-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            var $questao = $("#questao");
            $questao.select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __avaliacaoQuestaoRespostaAdd.options.url.avaliacaoQuestao,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.questao_id;
                            el.text = el.questao_descricao;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $questao.select2("data", __avaliacaoQuestaoRespostaAdd.getQuestao());

            var $questaorespostaPosicao = $("#questaorespostaPosicao");
            __avaliacaoQuestaoRespostaAdd.inciarElementoInteiro($questaorespostaPosicao);

            var $questaorespostaGabarito = $("#questaorespostaGabarito");
            $questaorespostaGabarito.select2({
                minimumInputLength: 1,
                allowClear: true,
                language: 'pt-BR'
            }).trigger("change");
        };

        this.setQuestao = function (questao) {
            this.options.value.questao = questao || null;
        };

        this.getQuestao = function () {
            return this.options.value.questao || null;
        };
        this.setValidations = function () {
            __avaliacaoQuestaoRespostaAdd.options.validator.settings.rules = {
                questao: {number: true, required: true},
                questaorespostaDescricao: {maxlength: 255, required: true},
                questaorespostaPosicao: {maxlength: 11, number: true},
                questaorespostaGabarito: {maxlength: 3, required: true}
            };
            __avaliacaoQuestaoRespostaAdd.options.validator.settings.messages = {
                questao: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                questaorespostaDescricao: {maxlength: 'Tamanho máximo: 255!', required: 'Campo obrigatório!'},
                questaorespostaPosicao: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!'},
                questaorespostaGabarito: {maxlength: 'Tamanho máximo: 3!', required: 'Campo obrigatório!'}
            };

            $(__avaliacaoQuestaoRespostaAdd.options.formElement).submit(function (e) {
                var ok = $(__avaliacaoQuestaoRespostaAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__avaliacaoQuestaoRespostaAdd.options.formElement);

                if (__avaliacaoQuestaoRespostaAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __avaliacaoQuestaoRespostaAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __avaliacaoQuestaoRespostaAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__avaliacaoQuestaoRespostaAdd.options.listagem) {
                                    $.avaliacaoQuestaoRespostaIndex().reloadDataTableAvaliacaoQuestaoResposta();
                                    __avaliacaoQuestaoRespostaAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#avaliacao-questao-resposta-modal').modal('hide');
                                }

                                __avaliacaoQuestaoRespostaAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaAvaliacaoQuestaoResposta = function (questaoresposta_id, callback) {
            var $form = $(__avaliacaoQuestaoRespostaAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __avaliacaoQuestaoRespostaAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + questaoresposta_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __avaliacaoQuestaoRespostaAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __avaliacaoQuestaoRespostaAdd.removeOverlay($form);
                },
                erro: function () {
                    __avaliacaoQuestaoRespostaAdd.showNotificacaoDanger('Erro desconhecido!');
                    __avaliacaoQuestaoRespostaAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.avaliacaoQuestaoRespostaAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.avaliacao.avaliacao-questao-resposta.add");

        if (!obj) {
            obj = new AvaliacaoQuestaoRespostaAdd();
            obj.run(params);
            $(window).data('universa.avaliacao.avaliacao-questao-resposta.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);