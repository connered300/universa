(function ($, window, document) {
    'use strict';
    var AvaliacaoQuestionarioRespondenteIndex = function () {
        VersaShared.call(this);
        var __avaliacaoQuestionarioRespondenteIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                avaliacaoQuestionarioRespondente: null
            }
        };

        this.setSteps = function () {
            var $dataTableAvaliacaoQuestionarioRespondente = $('#dataTableAvaliacaoQuestionarioRespondente');
            var colNum = -1;
            __avaliacaoQuestionarioRespondenteIndex.options.datatables.avaliacaoQuestionarioRespondente = $dataTableAvaliacaoQuestionarioRespondente.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __avaliacaoQuestionarioRespondenteIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [
                            {class: 'btn-primary item-edit', icon: 'fa-pencil'},
                            {class: 'btn-danger item-remove', icon: 'fa-times'}
                        ];

                        for (var row in data) {
                            data[row]['acao'] = __avaliacaoQuestionarioRespondenteIndex.createBtnGroup(btns);
                        }

                        __avaliacaoQuestionarioRespondenteIndex.removeOverlay($('#container-avaliacao-questionario-respondente'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "respondente_id", targets: ++colNum, data: "respondente_id"},
                    {name: "secao_id", targets: ++colNum, data: "secao_id"},
                    {name: "respondente_status", targets: ++colNum, data: "respondente_status"},
                    {name: "respondente_data", targets: ++colNum, data: "respondente_data"},
                    {name: "pes_id", targets: ++colNum, data: "pes_id"},
                    {name: "respondente_id", targets: ++colNum, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $dataTableAvaliacaoQuestionarioRespondente.on('click', '.item-edit', function () {
                var $pai = $(this).closest('tr');
                var data = __avaliacaoQuestionarioRespondenteIndex.options.datatables.avaliacaoQuestionarioRespondente.fnGetData($pai);

                if (!__avaliacaoQuestionarioRespondenteIndex.options.ajax) {
                    location.href = __avaliacaoQuestionarioRespondenteIndex.options.url.edit + '/' + data['respondente_id'];
                } else {
                    __avaliacaoQuestionarioRespondenteIndex.addOverlay(
                        $('#container-avaliacao-questionario-respondente'), 'Aguarde, carregando dados para edição...'
                    );
                    $.avaliacaoQuestionarioRespondenteAdd().pesquisaAvaliacaoQuestionarioRespondente(
                        data['respondente_id'],
                        function (data) {
                            __avaliacaoQuestionarioRespondenteIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-avaliacao-questionario-respondente-add').click(function (e) {
                if (__avaliacaoQuestionarioRespondenteIndex.options.ajax) {
                    __avaliacaoQuestionarioRespondenteIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $dataTableAvaliacaoQuestionarioRespondente.on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __avaliacaoQuestionarioRespondenteIndex.options.datatables.avaliacaoQuestionarioRespondente.fnGetData($pai);
                var arrRemover = {
                    respondenteId: data['respondente_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de questionario respondente "' + data['secao_id'] + '"?',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __avaliacaoQuestionarioRespondenteIndex.addOverlay(
                                $('#container-avaliacao-questionario-respondente'), 'Aguarde, solicitando remoção de registro de questionario respondente...'
                            );
                            $.ajax({
                                url: __avaliacaoQuestionarioRespondenteIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __avaliacaoQuestionarioRespondenteIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de questionario respondente:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __avaliacaoQuestionarioRespondenteIndex.removeOverlay($('#container-avaliacao-questionario-respondente'));
                                    } else {
                                        __avaliacaoQuestionarioRespondenteIndex.reloadDataTableAvaliacaoQuestionarioRespondente();
                                        __avaliacaoQuestionarioRespondenteIndex.showNotificacaoSuccess(
                                            "Registro de questionario respondente removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#avaliacao-questionario-respondente-form'),
                $modal = $('#avaliacao-questionario-respondente-modal');
            var actionForm = $form.attr('action');
            actionForm = actionForm.split('/');

            if (actionForm[actionForm.length - 2] == 'edit') {
                actionForm = actionForm.slice(0, actionForm.length - 1);
            }

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['respondenteId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['respondenteId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);


            var $secao = $("#secao");
            $secao.select2('data', data['secao']).trigger("change");

            var $respondenteStatus = $("#respondenteStatus");
            $respondenteStatus.select2('val', data['respondenteStatus']).trigger("change");

            var $pes = $("#pes");
            $pes.select2('data', data['pes']).trigger("change");

            $modal.find('.modal-title').html(actionTitle + ' questionario respondente');
            $modal.modal();
            __avaliacaoQuestionarioRespondenteIndex.removeOverlay($('#container-avaliacao-questionario-respondente'));
        };

        this.reloadDataTableAvaliacaoQuestionarioRespondente = function () {
            this.getDataTableAvaliacaoQuestionarioRespondente().api().ajax.reload();
        };

        this.getDataTableAvaliacaoQuestionarioRespondente = function () {
            if (!__avaliacaoQuestionarioRespondenteIndex.options.datatables.avaliacaoQuestionarioRespondente) {
                if (!$.fn.dataTable.isDataTable('#dataTableAvaliacaoQuestionarioRespondente')) {
                    __avaliacaoQuestionarioRespondenteIndex.options.datatables.avaliacaoQuestionarioRespondente = $('#dataTableAvaliacaoQuestionarioRespondente').DataTable();
                } else {
                    __avaliacaoQuestionarioRespondenteIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __avaliacaoQuestionarioRespondenteIndex.options.datatables.avaliacaoQuestionarioRespondente;
        };

        this.run = function (opts) {
            __avaliacaoQuestionarioRespondenteIndex.setDefaults(opts);
            __avaliacaoQuestionarioRespondenteIndex.setSteps();
        };
    };

    $.avaliacaoQuestionarioRespondenteIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.avaliacao.avaliacao-questionario-respondente.index");

        if (!obj) {
            obj = new AvaliacaoQuestionarioRespondenteIndex();
            obj.run(params);
            $(window).data('universa.avaliacao.avaliacao-questionario-respondente.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);