(function ($, window, document) {
    'use strict';

    var AvaliacaoQuestionarioRespondenteAdd = function () {
        VersaShared.call(this);
        var __avaliacaoQuestionarioRespondenteAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                avaliacaoSecao: '',
                pessoa: ''
            },
            data: {},
            value: {
                secao: null,
                pes: null
            },
            datatables: {},
            wizardElement: '#avaliacao-questionario-respondente-wizard',
            formElement: '#avaliacao-questionario-respondente-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            var $secao = $("#secao");
            $secao.select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __avaliacaoQuestionarioRespondenteAdd.options.url.avaliacaoSecao,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.secao_id;
                            el.text = el.secao_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $secao.select2("data", __avaliacaoQuestionarioRespondenteAdd.getSecao());

            var $respondenteStatus = $("#respondenteStatus");
            $respondenteStatus.select2({minimumInputLength: 1, allowClear: true, language: 'pt-BR'}).trigger("change");

            var $respondenteData = $("#respondenteData");
            __avaliacaoQuestionarioRespondenteAdd.iniciarElementoDatePicker($respondenteData);

            var $pes = $("#pes");
            $pes.select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __avaliacaoQuestionarioRespondenteAdd.options.url.pessoa,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.pes_id;
                            el.text = el.pes_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $pes.select2("data", __avaliacaoQuestionarioRespondenteAdd.getPes());
        };

        this.setSecao = function (secao) {
            this.options.value.secao = secao || null;
        };

        this.getSecao = function () {
            return this.options.value.secao || null;
        };

        this.setPes = function (pes) {
            this.options.value.pes = pes || null;
        };

        this.getPes = function () {
            return this.options.value.pes || null;
        };
        this.setValidations = function () {
            __avaliacaoQuestionarioRespondenteAdd.options.validator.settings.rules = {
                secao: {number: true, required: true},
                respondenteStatus: {maxlength: 7, required: true},
                respondenteData: {dateBR: true},
                pes: {number: true, required: true}
            };
            __avaliacaoQuestionarioRespondenteAdd.options.validator.settings.messages = {
                secao: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                respondenteStatus: {maxlength: 'Tamanho máximo: 7!', required: 'Campo obrigatório!'},
                respondenteData: {dateBR: 'Informe uma data válida!'},
                pes: {number: 'Número inválido!', required: 'Campo obrigatório!'}
            };

            $(__avaliacaoQuestionarioRespondenteAdd.options.formElement).submit(function (e) {
                var ok = $(__avaliacaoQuestionarioRespondenteAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__avaliacaoQuestionarioRespondenteAdd.options.formElement);

                if (__avaliacaoQuestionarioRespondenteAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __avaliacaoQuestionarioRespondenteAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __avaliacaoQuestionarioRespondenteAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__avaliacaoQuestionarioRespondenteAdd.options.listagem) {
                                    $.avaliacaoQuestionarioRespondenteIndex().reloadDataTableAvaliacaoQuestionarioRespondente();
                                    __avaliacaoQuestionarioRespondenteAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#avaliacao-questionario-respondente-modal').modal('hide');
                                }

                                __avaliacaoQuestionarioRespondenteAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaAvaliacaoQuestionarioRespondente = function (respondente_id, callback) {
            var $form = $(__avaliacaoQuestionarioRespondenteAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __avaliacaoQuestionarioRespondenteAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + respondente_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __avaliacaoQuestionarioRespondenteAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __avaliacaoQuestionarioRespondenteAdd.removeOverlay($form);
                },
                erro: function () {
                    __avaliacaoQuestionarioRespondenteAdd.showNotificacaoDanger('Erro desconhecido!');
                    __avaliacaoQuestionarioRespondenteAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.avaliacaoQuestionarioRespondenteAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.avaliacao.avaliacao-questionario-respondente.add");

        if (!obj) {
            obj = new AvaliacaoQuestionarioRespondenteAdd();
            obj.run(params);
            $(window).data('universa.avaliacao.avaliacao-questionario-respondente.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);