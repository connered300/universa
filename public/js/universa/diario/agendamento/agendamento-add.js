(function ($, window, document) {
    'use strict';
    var AgendamentoAdd = function () {
        VersaShared.call(this);
        var __agendamentoAdd = this;
        this.defaults = {
            url: {
                urlPesquisaDiario: "",
                urlPesquisaCursos: "",
                urlDatatable: "",
                urlAgendamentoEdit: "",
                urlAgendamentoAdd: "",
                urlAgendamentoRemove: "",
                urlAgendamentoAlunoAdd: "",
            },
            data: {
                arrFilters: {}
            },
            datatables: {
                agendamentoFiles: null
            },
            value: {
                agendaNota: false,
            },
            readonly: false
        };

        this.setSteps = function () {
            $("#agendaAvaliativa").change(function(){
                $("#divNota").show();
                if($(this).val() == "Não"){
                    $("#divNota").hide();
                }
            }).trigger("change");


            $("#per").select2({
                allowClear: true,
                language: 'pt-BR',
            }).change(function () {
                $("#curso").select2("enable", true).trigger("change");

                $("#curso").select2("data", "").trigger("change");
                $("#turma").select2("data", "").trigger("change");
                $("#disc").select2("data", "").trigger("change");
                $("#etapa").select2("data", "").trigger("change");
                __agendamentoAdd.atualizaDataTable();
            });

            $("#curso").select2({
                language: 'pt-BR',
                ajax: {
                    url: __agendamentoAdd.options.url.urlPesquisaCursos,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {perId: $("#per").val(), query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.curso_nome, id: el.cursocampus_id};
                        });
                        return {results: transformed};
                    },
                },
            }).change(function(e){
                __agendamentoAdd.atualizaDataTable();

                //verificando interações com o select2 para que o usuário possa modificar
                //as demais informações de turma ou de disciplina, somente-se o curso for
                //selecionado
                $("#turma, #disc").select2("enable", !$.isEmptyObject(e.added)).trigger("change");
            });

            $("[id^='turma']").select2({
                language: 'pt-BR',
                ajax: {
                    url: __agendamentoAdd.options.url.urlPesquisaDiario,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {per_id: $("#per").val(), turma_nome: query, cursocampus_id: $("#curso").val()};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.turma_nome, id: el.turma_id};
                        });
                        return {results: transformed};
                    },
                },
            }).change(function(e){

                var isfiltro = $(this).data("filtro");

                if(e.added && isfiltro){
                    __agendamentoAdd.atualizaDataTable();
                }

            });

            $("#disc, #discModal").select2({
                language: 'pt-BR',
                ajax: {
                    url: __agendamentoAdd.options.url.urlPesquisaDiario,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var dados = {per_id: $("#per").val(), disc_nome: query};

                        if( $("#turma").val() ){
                            dados['turma_id'] = $("#turma").val();
                        } else{
                            dados['cursocampus_id'] = $("#curso").val();
                        }

                        return dados;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.disc_nome, id: el.disc_id, per_disc_avaliacao: el.per_disc_avaliacao};
                        });
                        return {results: transformed};
                    },
                },
            }).change(function(e){
                if(e.added ){
                    var rules  = {required: false, maxlength: 6, number: true};
                    var filtro = $(this).data("filtro");

                    $("#etapa").select2("enable", true).trigger("change");
                    $("#divNota").hide();
                    $("#agendaNota").rules("remove");

                    if(e.added.per_disc_avaliacao == "Valor Numérico"){

                        rules['required'] = true;

                        $("#divNota").show();
                    }

                    $("#agendaNota").rules("add", rules);

                    if(filtro){
                        __agendamentoAdd.atualizaDataTable();
                    }

                }
            });

            $("#etapa, #etapaModal").select2({
                language: 'pt-BR',
                ajax: {
                    url: __agendamentoAdd.options.url.urlPesquisaDiario,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {buscarEtapa: true, perId: $("#per").val(), q: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.etapa_descricao, id: el.etapa_id, etapa_data_fim: el.etapa_data_fim, etapa_data_inicio: el.etapa_data_inicio };
                        });
                        return {results: transformed};
                    },
                },
            }).change(function(e){

                if(e.added){

                    $('#agendaDataFim').datepicker('option', 'minDate', $.datepicker.parseDate("yy-mm-dd", e.added.etapa_data_inicio));
                    $('#agendaDataFim').datepicker('option', 'maxDate', $.datepicker.parseDate("yy-mm-dd", e.added.etapa_data_fim));


                    var filtro = $(this).data("filtro");
                    if(filtro){
                        __agendamentoAdd.atualizaDataTable();
                    }
                }
            });

            $("#turma, #disc, #etapa").select2("enable", false).trigger("change");


            $("#agendaNome").select2({
                allowClear: true,
                language: 'pt-BR',
                enable: false,
            });

            $("#agendaDataFim").datepicker({
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>',
                //maxDate: conforme etapa selecionada
            }).inputmask({
                showMaskOnHover: false,
                clearIncomplete: true,
                mask: ['99/99/9999']
            });

            $('#agendaNota').inputmask({
                showMaskOnHover: false,
                alias: 'currency',
                groupSeparator: "",
                radixPoint: ".",
                placeholder: "0",
                allowMinus: false,
                prefix: ""
            });

            $('form#agendamento-form').validate({
                submitHandler: function () {

                    var content = $("#agendamento-modal").find('.modal-content');
                    __agendamentoAdd.addOverlay(
                        content, 'Efetuando o agendamento de atividade. Aguarde.'
                    );

                    $.ajax({
                        url: __agendamentoAdd.options.url.urlAgendamentoAdd,
                        type: 'POST',
                        dataType: 'json',
                        data: $('#agendamento-form').serializeArray(),
                        success: function (data) {
                            if (data.success) {
                                __agendamentoAdd.showNotificacaoSuccess(
                                    "Agendamento efetuado com sucesso!"
                                );

                                __agendamentoAdd.atualizaDataTable();
                                $(".close").click();
                            }
                            else {
                                __agendamentoAdd.showNotificacaoDanger(
                                    data.error['mensagem'] || "Não foi possível efetuar o agendamento!"
                                );
                            }
                            __agendamentoAdd.removeOverlay(content);
                        }
                    });

                    return false;
                },
                ignore: '.ignore',
                rules: {
                    disc: {required: true},
                    etapa: {required: true},
                    agendaTitulo: {required: true, maxlength: 128},
                    agendaNome: {required: true},
                    agendaDataFim: {required: true, dateBR: true},
                    agendaAvaliativa: {required: true},
                    agendaEntregaTipo: {required: true},
                    agendaNota: {required: function(){
                        return $('#agendaNota').is(':visible');
                    }, maxlength: 6, number: true, max: 30},
                },
                messages: {
                    disc: {required: 'Campo obrigatório!'},
                    etapa: {required: 'Campo obrigatório!'},
                    agendaTitulo: {required: 'Campo obrigatório!', maxlength: "Tamanho máximo: 128"},
                    agendaNome: {required: 'Campo obrigatório!'},
                    agendaDataFim: {required: 'Campo obrigatório!', dateBR: 'Data inválida!'},
                    agendaAvaliativa: {required: 'Campo obrigatório!'},
                    agendaEntregaTipo: {required: 'Campo obrigatório!'},
                    agendaNota: {required: 'Campo obrigatório!', number: 'Número inválido!'},
                }
            });

            $("#btn-agenda-add").click(function(){
                __agendamentoAdd.openModal();
                return false;
            });

            __agendamentoAdd.dataTableAgendamento();

            $('#dataTableAgendamento').on('click', '.agenda-edit', function (e) {
                var $pai = $(this).closest('tr');
                var data = __agendamentoAdd.options.datatables.agendamentos.fnGetData($pai);

                $.ajax({
                    url: __agendamentoAdd.options.url.urlAgendamentoEdit+("?agenda_id="+ data['agenda_id']),
                    type: 'GET',
                    dataType: 'json',
                    async: false,
                    data: {"agenda_id": data['agenda_id']},
                    success: function (data) {

                        __agendamentoAdd.openModal(data['result']);
                    }
                });

                e.preventDefault();
                e.stopPropagation();
            });

            $('#dataTableAgendamento').on('click', '.agenda-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __agendamentoAdd.options.datatables.agendamentos.fnGetData($pai);

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja remover o agendamento "' + data['agenda_titulo'] + '"?',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {

                    if (ButtonPress == "Sim") {

                        var content = $("#content");
                        __agendamentoAdd.addOverlay(
                            content, 'Efetuando a remoção do agendamento. Aguarde.'
                        );

                        $.ajax({
                            url: __agendamentoAdd.options.url.urlAgendamentoRemove+"/"+data['agenda_id'],
                            type: 'POST',
                            dataType: 'json',
                            async: false,
                            success: function (data) {
                                if (data.success) {
                                    __agendamentoAdd.showNotificacaoSuccess(
                                        "Agendamento removido com sucesso!"
                                    );

                                    __agendamentoAdd.atualizaDataTable();
                                }
                                else {
                                    __agendamentoAdd.showNotificacaoDanger(
                                        data.error['mensagem'] || "Não foi possível remover o agendamento!"
                                    );
                                }
                                __agendamentoAdd.removeOverlay(content);
                            }

                        });
                    }

                });

                e.preventDefault();
                e.stopPropagation();
            });

            $('#dataTableAgendamento').on('click', '.agenda-nota', function (e) {
                var $pai = $(this).closest('tr');
                var data = __agendamentoAdd.options.datatables.agendamentos.fnGetData($pai);

                location.href = __agendamentoAdd.options.url.urlAgendamentoAlunoAdd+"/"+data['agendadiario_id'];
            });

        };

        this.atualizaDataTable = function(){
            __agendamentoAdd.options.datatables.agendamentos.api().ajax.reload();
        };

        this.dataTableAgendamento = function(){
            var colNum = 0;
            __agendamentoAdd.options.datatables.agendamentos = $('#dataTableAgendamento').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __agendamentoAdd.options.url.urlDatatable,
                    type: "POST",
                    data: function(d){

                        d['per_id'] = $("#per").val();
                        d['cursocampus_id'] = $("#curso").val();
                        d['turma_id'] = $("#turma").val();
                        d['disc_id'] = $("#disc").val();
                        d['etapa_id'] = $("#etapa").val();

                        return d;
                    },
                    dataSrc: function (json) {
                        var data = json.data;


                        for (var row in data) {

                            var btnGroup = '\
                            <div class="text-center">\
                                <div class="btn-group">\
                                    <button type="button" title="Editar Agendamento" data-target="#" data-toggle="#" class="btn btn-xs btn-primary agenda-edit">\
                                        <i class="fa fa-pencil fa-inverse"></i>\
                                    </button>\
                                    <button type="button" title="Remover Agendamento" data-target="#" data-toggle="#" class="btn btn-xs btn-danger agenda-remove">\
                                        <i class="fa fa-times fa-inverse"></i>\
                                    </button>';

                            if(data[row]['agenda_avaliativa'] == "Sim"){
                                btnGroup += '\
                                    <button type="button" title="Nota do Aluno" data-target="#" data-toggle="#" class="btn btn-xs btn-warning agenda-nota">\
                                        <i class="fa fas fa-copy fa-inverse"></i>\
                                    </button>';
                            }

                            btnGroup += '\
                                </div>\
                            </div>';

                            data[row]['acao'] = btnGroup;
                        }
                       return data;
                    }
                },
                columnDefs: [
                    {name: "agenda_titulo", targets: colNum++, data: "agenda_titulo"},
                    {name: "agenda_nome", targets: colNum++, data: "agenda_nome"},
                    {name: "agenda_nota", targets: colNum++, data: "agenda_nota"},
                    {name: "agenda_data_fim", targets: colNum++, data: "agenda_data_fim_br"},
                    {name: "curso_nome", targets: colNum++, data: "curso_nome"},
                    {name: "turma_nome", targets: colNum++, data: "turma_nome"},
                    {name: "disc_nome", targets: colNum++, data: "disc_nome"},
                    {name: "etapa_descricao", targets: colNum++, data: "etapa_descricao"},
                    {name: "agenda_id", targets: colNum++, data: "acao"},
                    {name: "agenda_data_fim", targets: colNum++, data: "agenda_data_fim",  visible:false, searchable:false},
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[3, 'asc'],[0, 'asc']]
            });
        },

        this.openModal = function(opts){

            $('#agendamento-form').validate().resetForm();

            var curso = $("#curso").select2("data");
            var turma = $("#turma").select2("data");
            var disc  = $("#disc").select2("data");
            var etapa = $("#etapa").select2("data");

            var options = $.extend({
                "cursocampus_id"     : curso ? curso.id : "" ,
                "curso_nome"         : curso ? curso.text : "" ,
                "turma_id"           : turma ? turma.id : "",
                "turma_nome"         : turma ? turma.text : "",
                "disc_id"            : disc  ? disc.id : "",
                "disc_nome"          : disc  ? disc.text : "",
                "etapa_id"           : etapa ? etapa.id : "",
                "etapa_descricao"    : etapa ? etapa.text : "",
                "agenda_id"          : "",
                "agenda_titulo"      : "",
                "agenda_nome"        : "",
                "agenda_data_fim"    : "",
                "agenda_avaliativa"  : "Sim",
                "agenda_nota"        : "",
                "agenda_entrega_tipo": "Presencial",
                "agenda_anexos"      : "",
                "agenda_descricao"   : "",
            } ,opts);

            $("[name='cursocampus']").select2("data", {id: options['cursocampus_id'], text: options['curso_nome']});
            $("[name='turma']").select2("data", {id: options['turma_id'], text: options['turma_nome']});
            $("[name='disc']").select2("data", {id: options['disc_id'], text: options['disc_nome']});
            $("[name='etapa']").select2("data", {id: options['etapa_id'], text: options['etapa_descricao']});

            $("[name='agendaId']").val(options['agenda_id']);
            $("[name='agendaTitulo']").val(options['agenda_titulo']);
            $("[name='agendaNome']").val(options['agenda_nome']);
            $("[name='agendaDataFim']").val("");

            if( options['agenda_data_fim'] ){
                $("[name='agendaDataFim']").datepicker("setDate", $.datepicker.parseDate("yy-mm-dd", options['agenda_data_fim']) );
            }

            $("[name='agendaAvaliativa']").val( options['agenda_avaliativa'] ).trigger("change");
            $("[name='agendaNota']").val( options['agenda_nota'] );
            $("[name='agendaEntregaTipo']").val( options['agenda_entrega_tipo'] ).trigger("change");
            $("[name='agendaAnexos']").val( options['agenda_anexos'] ).trigger("change");
            $("[name='agendaDescricao']").val( options['agenda_descricao'] );

            $("[name='turma']").select2("enable", true).trigger("change");

            $("#divTurmaModal").show();
            $("#divDiscModal").show();
            $("#divEtapaModal").show();

            if( $("#turma").val() ){
                $("#divTurmaModal").hide();
            }

            if( $("#disc").val() ){
                $("#divDiscModal").hide();
            }

            if( $("#etapa").val() ){
                $("#divEtapaModal").hide();
            }

            $("input:visible, textarea:visible").val("");
            $("#agendaId").val("");

            $('#agendamento-modal').modal();

            $(".close").click(function (e) {
                $('#agendamento-modal').modal('hide');
                e.stopImmediatePropagation();
                e.preventDefault();
            });

            $("#btn-enviar-agendamento").click(function (e) {
                $('#agendamento-form').submit()
                //evitando double event
                e.stopImmediatePropagation();
                e.preventDefault();
            });
        };

        this.run = function (opts) {
            __agendamentoAdd.setDefaults(opts);
            __agendamentoAdd.setSteps();
        };
    };

    $.agendamentoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.diario.agendamento.index");

        if (!obj) {
            obj = new AgendamentoAdd();
            obj.run(params);
            $(window).data('universa.diario.agendamento.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);