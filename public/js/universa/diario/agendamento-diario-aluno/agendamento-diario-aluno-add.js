(function ($, window, document) {
    'use strict';
    var AgendamentoAlunoAdd = function () {
        VersaShared.call(this);
        var __AgendamentoAlunoAdd = this;
        this.defaults = {
            url: {
                urlDatatable: "",
                urlAgendamentoAlunoAdd: "",
            },
            data: {
                arrFilters: {},
                arrNotas: {}
            },
            datatables: {
                agendamentoFiles: null,
                agendamentosAluno: null,
            },
            value: {
                agendaNota: false,
                agendadiarioId: null,
                dataInicio: null,
                dataFim: null,
            },
            readonly: false
        };

        this.setSteps = function () {
            __AgendamentoAlunoAdd.dataTableAgendamentoAluno();
        };

        this.atualizaDataTable = function(){
            __AgendamentoAlunoAdd.options.datatables.agendamentosAluno.api().ajax.reload();
        };

        this.dataTableAgendamentoAluno = function(){
            var colNum = 0;
            __AgendamentoAlunoAdd.options.datatables.agendamentosAluno = $('#dataTableAgendamentoAluno').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __AgendamentoAlunoAdd.options.url.urlDatatable,
                    data: function(d){
                        d['agendadiario_id'] = __AgendamentoAlunoAdd.options.value.agendadiarioId;

                        return d;
                    },
                    dataSrc: function (json) {
                        var data = json.data;

                        for (var row in data) {

                            var alunodisc = data[row]['alunodisc_id'];
                            var arrNotas   = __AgendamentoAlunoAdd.options.data.arrNotas[alunodisc];
                            var notaAluno  = data[row]['agendaaluno_conceito'];
                            var nomeCampo  = "agendaalunoConceito";
                            var dataAlunoEntrega = data[row]['agendaaluno_data_entrega'];

                            if( arrNotas && arrNotas['agendaalunoDataEntrega']){
                                dataAlunoEntrega = arrNotas['agendaalunoDataEntrega'];
                            } else if(dataAlunoEntrega){
                                dataAlunoEntrega = $.datepicker.formatDate("dd/mm/yy", $.datepicker.parseDate("yy-mm-dd", dataAlunoEntrega));
                            }

                            if( __AgendamentoAlunoAdd.options.value.avaliacao == "Valor Numérico" ){
                                nomeCampo = "agendaalunoNota";
                                notaAluno  = data[row]['agendaaluno_nota'];
                                if(notaAluno !== ""){
                                    notaAluno = __AgendamentoAlunoAdd.formatNumber(notaAluno);
                                }
                            }

                            //valor pode ser 0
                            if( arrNotas && arrNotas['agendaalunoNota'] != undefined){
                                notaAluno = arrNotas['agendaalunoNota'];
                            }
                            data[row]['nota'] = "<div class='form-group'><input class='nota form-control' type='text' name='alunodisc["+data[row]['alunodisc_id']+"]["+nomeCampo+"]' value='"+notaAluno+"'></div>";
                            data[row]['agendaaluno_data_entrega'] = "<div class='form-group'><input class='form-control' type='text' name='alunodisc["+data[row]['alunodisc_id']+"][agendaalunoDataEntrega]"+"' value='"+dataAlunoEntrega+"'></div>";
                        }

                        return data;
                    },
                    complete:function () {

                        $("[name*=agendaalunoNota]").inputmask('decimal',{
                            integerDigits: 7,
                            digits: 2,
                            radixPoint: '.',
                            rightAlign: true,
                            digitsOptional: true,
                            placeholder: '',
                            allowMinus: false,
                            allowPlus: false,
                            max: __AgendamentoAlunoAdd.options.value.agendaNota,
                        });

                        $("[name*=agendaalunoDataEntrega]").datepicker({
                            prevText: '<i class="fa fa-chevron-left"></i>',
                            nextText: '<i class="fa fa-chevron-right"></i>',
                            minDate: ($.datepicker.parseDate("yy-mm-dd", __AgendamentoAlunoAdd.options.value.dataInicio)),
                            maxDate: ($.datepicker.parseDate("yy-mm-dd", __AgendamentoAlunoAdd.options.value.dataFim)),
                            onSelect: function ( dateText, inst ) {
                                var $pai = $(this).closest('tr');
                                var data = __AgendamentoAlunoAdd.options.datatables.agendamentosAluno.fnGetData($pai);

                                __AgendamentoAlunoAdd.setArrNotas(data['alunodisc_id'], data['agendaaluno_id']);
                            }
                        }).inputmask({
                            showMaskOnHover: false,
                            clearIncomplete: true,
                            mask: ['99/99/9999']
                        }).prop("readonly", true);

                    },
                    type: "POST",
                },
                'createdRow': function(row, data, dataIndex){

                    if(data['situacao_aluno'] == "Desligado"){
                        $('td:eq(2)', row).css('display', 'none');
                        $('td:eq(3)', row).css('display', 'none');

                        var alunoDesligado = $("<td>").attr({
                            class: "text-center",
                            colspan: 2,
                        }).text("Aluno Desligado!");

                        $(row).append(alunoDesligado);
                    }

                },
                columnDefs: [
                    {name: "alunocurso_id", targets: colNum++, data: "alunocurso_id"},
                    {name: "pes_nome", targets: colNum++, data: "pes_nome"},
                    {name: "agendaaluno_data_entrega", targets: colNum++, data: "agendaaluno_data_entrega"},
                    {name: "agendaaluno_nota", targets: colNum++, data: "nota", orderable:false },
                    {name: "agendaaluno_data_entrega", targets: colNum++, data: "agendaaluno_data_entrega",  visible:false, searchable:false},
                ],
                "dom": "<'dt-toolbar-header'<'col-sm-4'l><'col-sm-4'r><'col-sm-4'f>>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[1, 'asc']]
            });

            $(document).on('change',"[name*=agendaalunoNota]", function(){
                var $pai = $(this).closest('tr');
                var data = __AgendamentoAlunoAdd.options.datatables.agendamentosAluno.fnGetData($pai);

                __AgendamentoAlunoAdd.setArrNotas(data['alunodisc_id'], data['agendaaluno_id']);
            });

            $("#btn-agenda-aluno-add").click(function (e) {
                $('#agendamento-aluno-form').submit()
                //evitando double event
                e.stopImmediatePropagation();
                e.preventDefault();
            });
        };

        this.aplicarRules = function(alunodisc){
            var validate = $('#agendamento-aluno-form').validate();

            validate.settings.rules["alunodisc["+alunodisc+"][agendaalunoDataEntrega]"] = {
                required: function () {
                    return $("[name='alunodisc["+alunodisc+"][agendaalunoNota]']").val() != '';
                },
                dateBR: true
            };
            validate.settings.messages["alunodisc["+alunodisc+"][agendaalunoDataEntrega]"] = {
                required: 'Campo obrigatório!',
                dateBR: 'Data inválida!'
            };

            validate.settings.rules["alunodisc["+alunodisc+"][agendaalunoNota]"] = {
                required: function () {
                    return $("[name='alunodisc["+alunodisc+"][agendaalunoDataEntrega]']").val() != '';
                },
                number: true,
            };
            validate.settings.messages["alunodisc["+alunodisc+"][agendaalunoNota]"] = {
                required: 'Campo obrigatório!',
                max: 'Valor maior que o agendamento!'
            };
        },

        this.setArrNotas = function(alunodiscId, agendaalunoId){
            __AgendamentoAlunoAdd.options.data.arrNotas[alunodiscId] = $.extend(__AgendamentoAlunoAdd.options.data.arrNotas[alunodiscId], {
                agendaalunoId: agendaalunoId,
                agendaalunoNota: $("[name='alunodisc["+alunodiscId+"][agendaalunoNota]']").val(),
                agendaalunoDataEntrega: $("[name='alunodisc["+alunodiscId+"][agendaalunoDataEntrega]']").val(),
            });

            __AgendamentoAlunoAdd.aplicarRules(alunodiscId);
        }

        this.validateAgendamentoAluno = function(){
            $('#agendamento-aluno-form').validate({
                submitHandler: function () {

                    var preenchidos = Object.keys(__AgendamentoAlunoAdd.options.data.arrNotas).length

                    if( !preenchidos ){
                        __AgendamentoAlunoAdd.showNotificacaoWarning('Não foi(ram) lançado(s) nota(s) para o(s) aluno(s), caso a nota seja zerada, por favor confirme o valor na nota do aluno?');
                        return false;
                    }

                    var content = $("#content");
                    __AgendamentoAlunoAdd.addOverlay(
                        content, 'Efetuando o lançamento de notas. Aguarde.'
                    );
                    $.ajax({
                        url: __AgendamentoAlunoAdd.options.url.urlAgendamentoAlunoAdd,
                        type: 'POST',
                        dataType: 'json',
                        data: {agendadiarioId: $("[name='agendadiarioId']").val(), alunodisc: __AgendamentoAlunoAdd.options.data.arrNotas},
                        success: function (data) {
                            if (data.success) {
                                __AgendamentoAlunoAdd.showNotificacaoSuccess(
                                    "Lançamento de nota do(s) aluno(s) efetuado com sucesso!"
                                );
                            }
                            else {
                                __AgendamentoAlunoAdd.showNotificacaoDanger(
                                    data.error['mensagem'] || "Não foi possível efetuar o lançamento de nota do(s) aluno(s) no agendamento!"
                                );
                            }

                            __AgendamentoAlunoAdd.options.datatables.agendamentosAluno.api().ajax.reload();
                            __AgendamentoAlunoAdd.removeOverlay(content);
                        }
                    });
                    return false;
                },
                ignore: '.ignore',
            });
        };

        this.run = function (opts) {
            __AgendamentoAlunoAdd.setDefaults(opts);
            __AgendamentoAlunoAdd.setSteps();
            __AgendamentoAlunoAdd.validateAgendamentoAluno();
        };
    };

    $.agendamentoAlunoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.diario.agendamento-aluno.index");

        if (!obj) {
            obj = new AgendamentoAlunoAdd();
            obj.run(params);
            $(window).data('universa.diario.agendamento-aluno.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);