(function ($, window, document) {
    'use strict';
    var ProtocoloSetorGrupoIndex = function () {
        VersaShared.call(this);
        var __protocoloSetorGrupoIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                protocoloSetorGrupo: null
            }
        };

        this.setSteps = function () {
            var $dataTableProtocoloSetorGrupo = $('#dataTableProtocoloSetorGrupo');
            var colNum = -1;
            __protocoloSetorGrupoIndex.options.datatables.protocoloSetorGrupo = $dataTableProtocoloSetorGrupo.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __protocoloSetorGrupoIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [
                            {class: 'btn-primary item-edit', icon: 'fa-pencil'},
                            {class: 'btn-danger item-remove', icon: 'fa-times'}
                        ];

                        for (var row in data) {
                            data[row]['acao'] = __protocoloSetorGrupoIndex.createBtnGroup(btns);
                        }

                        __protocoloSetorGrupoIndex.removeOverlay($('#container-protocolo-setor-grupo'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "setor_grupo_id", targets: ++colNum, data: "setor_grupo_id"},
                    {name: "setor_id", targets: ++colNum, data: "setor_id"},
                    {name: "grupo_id", targets: ++colNum, data: "grupo_id"},
                    {name: "setor_grupo_id", targets: ++colNum, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $dataTableProtocoloSetorGrupo.on('click', '.item-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __protocoloSetorGrupoIndex.options.datatables.protocoloSetorGrupo.fnGetData($pai);

                if (!__protocoloSetorGrupoIndex.options.ajax) {
                    location.href = __protocoloSetorGrupoIndex.options.url.edit + '/' + data['setor_grupo_id'];
                } else {
                    __protocoloSetorGrupoIndex.addOverlay(
                        $('#container-protocolo-setor-grupo'), 'Aguarde, carregando dados para edição...'
                    );
                    $.protocoloSetorGrupoAdd().pesquisaProtocoloSetorGrupo(
                        data['setor_grupo_id'],
                        function (data) {
                            __protocoloSetorGrupoIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-protocolo-setor-grupo-add').click(function (e) {
                if (__protocoloSetorGrupoIndex.options.ajax) {
                    __protocoloSetorGrupoIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $dataTableProtocoloSetorGrupo.on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __protocoloSetorGrupoIndex.options.datatables.protocoloSetorGrupo.fnGetData($pai);
                var arrRemover = {
                    setorGrupoId: data['setor_grupo_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de setor grupo "' + data['setor_id'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __protocoloSetorGrupoIndex.addOverlay(
                                $('#container-protocolo-setor-grupo'), 'Aguarde, solicitando remoção de registro de setor grupo...'
                            );
                            $.ajax({
                                url: __protocoloSetorGrupoIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __protocoloSetorGrupoIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de setor grupo:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __protocoloSetorGrupoIndex.removeOverlay($('#container-protocolo-setor-grupo'));
                                    } else {
                                        __protocoloSetorGrupoIndex.reloadDataTableProtocoloSetorGrupo();
                                        __protocoloSetorGrupoIndex.showNotificacaoSuccess(
                                            "Registro de setor grupo removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#protocolo-setor-grupo-form'),
                $modal = $('#protocolo-setor-grupo-modal');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['setorGrupoId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['setorGrupoId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);


            var $setor = $("#setor");
            $setor.select2('data', data['setor']).trigger("change");

            var $grupo = $("#grupo");
            $grupo.select2('data', data['grupo']).trigger("change");

            $modal.find('.modal-title').html(actionTitle + ' setor grupo');
            $modal.modal();
            __protocoloSetorGrupoIndex.removeOverlay($('#container-protocolo-setor-grupo'));
        };

        this.reloadDataTableProtocoloSetorGrupo = function () {
            this.getDataTableProtocoloSetorGrupo().api().ajax.reload(null, false);
        };

        this.getDataTableProtocoloSetorGrupo = function () {
            if (!__protocoloSetorGrupoIndex.options.datatables.protocoloSetorGrupo) {
                if (!$.fn.dataTable.isDataTable('#dataTableProtocoloSetorGrupo')) {
                    __protocoloSetorGrupoIndex.options.datatables.protocoloSetorGrupo = $('#dataTableProtocoloSetorGrupo').DataTable();
                } else {
                    __protocoloSetorGrupoIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __protocoloSetorGrupoIndex.options.datatables.protocoloSetorGrupo;
        };

        this.run = function (opts) {
            __protocoloSetorGrupoIndex.setDefaults(opts);
            __protocoloSetorGrupoIndex.setSteps();
        };
    };

    $.protocoloSetorGrupoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.protocolo.protocolo-setor-grupo.index");

        if (!obj) {
            obj = new ProtocoloSetorGrupoIndex();
            obj.run(params);
            $(window).data('universa.protocolo.protocolo-setor-grupo.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);