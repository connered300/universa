(function ($, window, document) {
    'use strict';

    var ProtocoloSetorGrupoAdd = function () {
        VersaShared.call(this);
        var __protocoloSetorGrupoAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                protocoloSetor: '',
                acessoGrupo: ''
            },
            data: {},
            value: {
                setor: null,
                grupo: null
            },
            datatables: {},
            wizardElement: '#protocolo-setor-grupo-wizard',
            formElement: '#protocolo-setor-grupo-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            var $setor = $("#setor");
            $setor.select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __protocoloSetorGrupoAdd.options.url.protocoloSetor,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.setor_id;
                            el.text = el.setor_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $setor.select2("data", __protocoloSetorGrupoAdd.getSetor());

            var $grupo = $("#grupo");
            $grupo.select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __protocoloSetorGrupoAdd.options.url.acessoGrupo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.id;
                            el.text = el.id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $grupo.select2("data", __protocoloSetorGrupoAdd.getGrupo());
        };

        this.setSetor = function (setor) {
            this.options.value.setor = setor || null;
        };

        this.getSetor = function () {
            return this.options.value.setor || null;
        };

        this.setGrupo = function (grupo) {
            this.options.value.grupo = grupo || null;
        };

        this.getGrupo = function () {
            return this.options.value.grupo || null;
        };
        this.setValidations = function () {
            __protocoloSetorGrupoAdd.options.validator.settings.rules = {
                setor: {number: true, required: true},
                grupo: {number: true, required: true}
            };
            __protocoloSetorGrupoAdd.options.validator.settings.messages = {
                setor: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                grupo: {number: 'Número inválido!', required: 'Campo obrigatório!'}
            };

            $(__protocoloSetorGrupoAdd.options.formElement).submit(function (e) {
                var ok = $(__protocoloSetorGrupoAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__protocoloSetorGrupoAdd.options.formElement);

                if (__protocoloSetorGrupoAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __protocoloSetorGrupoAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __protocoloSetorGrupoAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__protocoloSetorGrupoAdd.options.listagem) {
                                    $.protocoloSetorGrupoIndex().reloadDataTableProtocoloSetorGrupo();
                                    __protocoloSetorGrupoAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#protocolo-setor-grupo-modal').modal('hide');
                                }

                                __protocoloSetorGrupoAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaProtocoloSetorGrupo = function (setor_grupo_id, callback) {
            var $form = $(__protocoloSetorGrupoAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __protocoloSetorGrupoAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + setor_grupo_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __protocoloSetorGrupoAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __protocoloSetorGrupoAdd.removeOverlay($form);
                },
                erro: function () {
                    __protocoloSetorGrupoAdd.showNotificacaoDanger('Erro desconhecido!');
                    __protocoloSetorGrupoAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.protocoloSetorGrupoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.protocolo.protocolo-setor-grupo.add");

        if (!obj) {
            obj = new ProtocoloSetorGrupoAdd();
            obj.run(params);
            $(window).data('universa.protocolo.protocolo-setor-grupo.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);