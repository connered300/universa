(function ($, window, document) {
    'use strict';
    var ProtocoloMensagemPadraoIndex = function () {
        VersaShared.call(this);
        var __protocoloMensagemPadraoIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                protocoloMensagemPadrao: null
            }
        };

        this.setSteps = function () {
            var $dataTableProtocoloMensagemPadrao = $('#dataTableProtocoloMensagemPadrao');
            var colNum = -1;
            __protocoloMensagemPadraoIndex.options.datatables.protocoloMensagemPadrao = $dataTableProtocoloMensagemPadrao.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __protocoloMensagemPadraoIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [
                            {class: 'btn-primary item-edit', icon: 'fa-pencil'},
                            {class: 'btn-danger item-remove', icon: 'fa-times'}
                        ];

                        for (var row in data) {
                            data[row]['acao'] = __protocoloMensagemPadraoIndex.createBtnGroup(btns);
                        }

                        __protocoloMensagemPadraoIndex.removeOverlay($('#container-protocolo-mensagem-padrao'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "mensagem_id", targets: ++colNum, data: "mensagem_id"},
                    {name: "setor_descricao", targets: ++colNum, data: "setor_descricao"},
                    {name: "solicitacao_descricao", targets: ++colNum, data: "solicitacao_descricao"},
                    {name: "mensagem_descricao", targets: ++colNum, data: "mensagem_descricao"},
                    {name: "mensagem_id", targets: ++colNum, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $dataTableProtocoloMensagemPadrao.on('click', '.item-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __protocoloMensagemPadraoIndex.options.datatables.protocoloMensagemPadrao.fnGetData($pai);

                if (!__protocoloMensagemPadraoIndex.options.ajax) {
                    location.href = __protocoloMensagemPadraoIndex.options.url.edit + '/' + data['mensagem_id'];
                } else {
                    __protocoloMensagemPadraoIndex.addOverlay(
                        $('#container-protocolo-mensagem-padrao'), 'Aguarde, carregando dados para edição...'
                    );
                    $.protocoloMensagemPadraoAdd().pesquisaProtocoloMensagemPadrao(
                        data['mensagem_id'],
                        function (data) {
                            __protocoloMensagemPadraoIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-protocolo-mensagem-padrao-add').click(function (e) {
                if (__protocoloMensagemPadraoIndex.options.ajax) {
                    __protocoloMensagemPadraoIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $dataTableProtocoloMensagemPadrao.on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __protocoloMensagemPadraoIndex.options.datatables.protocoloMensagemPadrao.fnGetData($pai);
                var arrRemover = {
                    mensagemId: data['mensagem_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de mensagem padrão "' + data['mensagem_descricao'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __protocoloMensagemPadraoIndex.addOverlay(
                                $('#container-protocolo-mensagem-padrao'), 'Aguarde, solicitando remoção de registro de mensagem padrão...'
                            );
                            $.ajax({
                                url: __protocoloMensagemPadraoIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __protocoloMensagemPadraoIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de mensagem padrão:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __protocoloMensagemPadraoIndex.removeOverlay($('#container-protocolo-mensagem-padrao'));
                                    } else {
                                        __protocoloMensagemPadraoIndex.reloadDataTableProtocoloMensagemPadrao();
                                        __protocoloMensagemPadraoIndex.showNotificacaoSuccess(
                                            "Registro de mensagem padrão removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            $('.help-block').remove();
            $('.form-group').removeClass('has-error');
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#protocolo-mensagem-padrao-form'),
                $modal = $('#protocolo-mensagem-padrao-modal');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['mensagemId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['mensagemId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);


            var $setor = $("#setor");
            $setor.select2('data', data['setor']).trigger("change");

            var $solicitacao = $("#solicitacao");
            $solicitacao.select2('data', data['solicitacao']).trigger("change");

            var $mensagemConteudo = $("#mensagemConteudo");
            $mensagemConteudo.val(data['mensagemConteudo']);

            $modal.find('.modal-title').html(actionTitle + ' mensagem padrão');
            $modal.modal();

            __protocoloMensagemPadraoIndex.removeOverlay($('#container-protocolo-mensagem-padrao'));
        };

        this.reloadDataTableProtocoloMensagemPadrao = function () {
            this.getDataTableProtocoloMensagemPadrao().api().ajax.reload(null, false);
        };

        this.getDataTableProtocoloMensagemPadrao = function () {
            if (!__protocoloMensagemPadraoIndex.options.datatables.protocoloMensagemPadrao) {
                if (!$.fn.dataTable.isDataTable('#dataTableProtocoloMensagemPadrao')) {
                    __protocoloMensagemPadraoIndex.options.datatables.protocoloMensagemPadrao = $('#dataTableProtocoloMensagemPadrao').DataTable();
                } else {
                    __protocoloMensagemPadraoIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __protocoloMensagemPadraoIndex.options.datatables.protocoloMensagemPadrao;
        };

        this.run = function (opts) {
            __protocoloMensagemPadraoIndex.setDefaults(opts);
            __protocoloMensagemPadraoIndex.setSteps();
        };
    };

    $.protocoloMensagemPadraoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.protocolo.protocolo-mensagem-padrao.index");

        if (!obj) {
            obj = new ProtocoloMensagemPadraoIndex();
            obj.run(params);
            $(window).data('universa.protocolo.protocolo-mensagem-padrao.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);