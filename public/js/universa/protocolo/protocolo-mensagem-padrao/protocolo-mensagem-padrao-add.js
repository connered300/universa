(function ($, window, document) {
    'use strict';

    var ProtocoloMensagemPadraoAdd = function () {
        VersaShared.call(this);
        var __protocoloMensagemPadraoAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                protocoloSetor: '',
                protocoloSolicitacao: ''
            },
            data: {},
            value: {
                setor: null,
                solicitacao: null
            },
            datatables: {},
            wizardElement: '#protocolo-mensagem-padrao-wizard',
            formElement: '#protocolo-mensagem-padrao-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            var $setor = $("#setor");
            $setor.select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __protocoloMensagemPadraoAdd.options.url.protocoloSetor,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.setor_id;
                            el.text = el.setor_descricao;

                            return el;
                        });

                        return {results: transformed};
                    }
                }

            }).on('change', function () {
                $('#solicitacao').select2('val', '').trigger('change');
            });


            $setor.select2("data", __protocoloMensagemPadraoAdd.getSetor());

            var $solicitacao = $("#solicitacao");
            $solicitacao.select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __protocoloMensagemPadraoAdd.options.url.protocoloSolicitacao,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {
                            setorId: $setor.val(),
                            query: query
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.solicitacao_id;
                            el.text = el.solicitacao_descricao;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $("#mensagemConteudo").ckeditor({
                height: '200px', linkShowAdvancedTab: false,
                autoStartup: true,
                enterMode: Number(2),
                toolbar: [
                    [
                        'Styles', 'Bold', 'Italic', 'Underline', 'SpellChecker', 'Scayt', '-',
                        'NumberedList', 'BulletedList'
                    ],
                    ['Table'],
                    ['Link', 'Unlink'],
                    ['Undo', 'Redo', '-', 'SelectAll']
                ]

            });

            $solicitacao.select2("data", __protocoloMensagemPadraoAdd.getSolicitacao());
        };

        this.setSetor = function (setor) {
            this.options.value.setor = setor || null;
        };

        this.getSetor = function () {
            return this.options.value.setor || null;
        };

        this.setSolicitacao = function (solicitacao) {
            this.options.value.solicitacao = solicitacao || null;
        };

        this.getSolicitacao = function () {
            return this.options.value.solicitacao || null;
        };
        this.setValidations = function () {
            __protocoloMensagemPadraoAdd.options.validator.settings.rules = {
                setor: {number: true, required: true},
                mensagemDescricao: {maxlength: 150, required: true},
                mensagemConteudo: {required: true}
            };
            __protocoloMensagemPadraoAdd.options.validator.settings.messages = {
                setor: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                mensagemDescricao: {maxlength: 'Tamanho máximo: 150!', required: 'Campo obrigatório!'},
                mensagemConteudo: {required: 'Campo obrigatório!'}
            };

            $(__protocoloMensagemPadraoAdd.options.formElement).submit(function (e) {
                var ok = $(__protocoloMensagemPadraoAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__protocoloMensagemPadraoAdd.options.formElement);

                if (__protocoloMensagemPadraoAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __protocoloMensagemPadraoAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __protocoloMensagemPadraoAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__protocoloMensagemPadraoAdd.options.listagem) {
                                    $.protocoloMensagemPadraoIndex().reloadDataTableProtocoloMensagemPadrao();
                                    __protocoloMensagemPadraoAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#protocolo-mensagem-padrao-modal').modal('hide');
                                }

                                __protocoloMensagemPadraoAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaProtocoloMensagemPadrao = function (mensagem_id, callback) {
            var $form = $(__protocoloMensagemPadraoAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __protocoloMensagemPadraoAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + mensagem_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __protocoloMensagemPadraoAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __protocoloMensagemPadraoAdd.removeOverlay($form);
                },
                erro: function () {
                    __protocoloMensagemPadraoAdd.showNotificacaoDanger('Erro desconhecido!');
                    __protocoloMensagemPadraoAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.protocoloMensagemPadraoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.protocolo.protocolo-mensagem-padrao.add");

        if (!obj) {
            obj = new ProtocoloMensagemPadraoAdd();
            obj.run(params);
            $(window).data('universa.protocolo.protocolo-mensagem-padrao.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);