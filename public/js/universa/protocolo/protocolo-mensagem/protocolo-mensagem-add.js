(function ($, window, document) {
    'use strict';

    var ProtocoloMensagemAdd = function () {
        VersaShared.call(this);
        var __protocoloMensagemAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                protocolo: '',
                acessoPessoas: ''
            },
            data: {},
            value: {
                protocolo: null,
                mensagemUsuario: null
            },
            datatables: {},
            wizardElement: '#protocolo-mensagem-wizard',
            formElement: '#protocolo-mensagem-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            var $protocolo = $("#protocolo");
            $protocolo.select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __protocoloMensagemAdd.options.url.protocolo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.protocolo_id;
                            el.text = el.protocolo_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $protocolo.select2("data", __protocoloMensagemAdd.getProtocolo());

            var $mensagemUsuario = $("#mensagemUsuario");
            $mensagemUsuario.select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __protocoloMensagemAdd.options.url.acessoPessoas,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.id;
                            el.text = el.id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $mensagemUsuario.select2("data", __protocoloMensagemAdd.getMensagemUsuario());

            var $mensagemData = $("#mensagemData");
            __protocoloMensagemAdd.iniciarElementoDatePicker($mensagemData);

            var $mensagemOrigem = $("#mensagemOrigem");
            $mensagemOrigem.select2({minimumInputLength: 1, allowClear: true, language: 'pt-BR'}).trigger("change");

            var $mensagemSolicitanteVisivel = $("#mensagemSolicitanteVisivel");
            $mensagemSolicitanteVisivel.select2({
                minimumInputLength: 1,
                allowClear: true,
                language: 'pt-BR'
            }).trigger("change");
        };

        this.setProtocolo = function (protocolo) {
            this.options.value.protocolo = protocolo || null;
        };

        this.getProtocolo = function () {
            return this.options.value.protocolo || null;
        };

        this.setMensagemUsuario = function (mensagemUsuario) {
            this.options.value.mensagemUsuario = mensagemUsuario || null;
        };

        this.getMensagemUsuario = function () {
            return this.options.value.mensagemUsuario || null;
        };
        this.setValidations = function () {
            __protocoloMensagemAdd.options.validator.settings.rules = {
                protocolo: {number: true, required: true},
                mensagemUsuario: {number: true},
                mensagemData: {required: true, dateBR: true},
                mensagemOrigem: {maxlength: 11, required: true},
                mensagemSolicitanteVisivel: {maxlength: 3, required: true},
                mensagemConteudo: {required: true}
            };
            __protocoloMensagemAdd.options.validator.settings.messages = {
                protocolo: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                mensagemUsuario: {number: 'Número inválido!'},
                mensagemData: {required: 'Campo obrigatório!', dateBR: 'Informe uma data válida!'},
                mensagemOrigem: {maxlength: 'Tamanho máximo: 11!', required: 'Campo obrigatório!'},
                mensagemSolicitanteVisivel: {maxlength: 'Tamanho máximo: 3!', required: 'Campo obrigatório!'},
                mensagemConteudo: {required: 'Campo obrigatório!'}
            };

            $(__protocoloMensagemAdd.options.formElement).submit(function (e) {
                var ok = $(__protocoloMensagemAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__protocoloMensagemAdd.options.formElement);

                if (__protocoloMensagemAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __protocoloMensagemAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __protocoloMensagemAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__protocoloMensagemAdd.options.listagem) {
                                    $.protocoloMensagemIndex().reloadDataTableProtocoloMensagem();
                                    __protocoloMensagemAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#protocolo-mensagem-modal').modal('hide');
                                }

                                __protocoloMensagemAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaProtocoloMensagem = function (mensagem_id, callback) {
            var $form = $(__protocoloMensagemAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __protocoloMensagemAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + mensagem_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __protocoloMensagemAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __protocoloMensagemAdd.removeOverlay($form);
                },
                erro: function () {
                    __protocoloMensagemAdd.showNotificacaoDanger('Erro desconhecido!');
                    __protocoloMensagemAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.protocoloMensagemAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.protocolo.protocolo-mensagem.add");

        if (!obj) {
            obj = new ProtocoloMensagemAdd();
            obj.run(params);
            $(window).data('universa.protocolo.protocolo-mensagem.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);