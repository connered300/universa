(function ($, window, document) {
    'use strict';
    var ProtocoloMensagemIndex = function () {
        VersaShared.call(this);
        var __protocoloMensagemIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                protocoloMensagem: null
            }
        };

        this.setSteps = function () {
            var $dataTableProtocoloMensagem = $('#dataTableProtocoloMensagem');
            var colNum = -1;
            __protocoloMensagemIndex.options.datatables.protocoloMensagem = $dataTableProtocoloMensagem.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __protocoloMensagemIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [
                            {class: 'btn-primary item-edit', icon: 'fa-pencil'},
                            {class: 'btn-danger item-remove', icon: 'fa-times'}
                        ];

                        for (var row in data) {
                            data[row]['acao'] = __protocoloMensagemIndex.createBtnGroup(btns);
                        }

                        __protocoloMensagemIndex.removeOverlay($('#container-protocolo-mensagem'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "mensagem_id", targets: ++colNum, data: "mensagem_id"},
                    {name: "protocolo_id", targets: ++colNum, data: "protocolo_id"},
                    {name: "mensagem_usuario_id", targets: ++colNum, data: "mensagem_usuario_id"},
                    {name: "mensagem_data", targets: ++colNum, data: "mensagem_data"},
                    {name: "mensagem_origem", targets: ++colNum, data: "mensagem_origem"},
                    {name: "mensagem_solicitante_visivel", targets: ++colNum, data: "mensagem_solicitante_visivel"},
                    {name: "mensagem_conteudo", targets: ++colNum, data: "mensagem_conteudo"},
                    {name: "mensagem_id", targets: ++colNum, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $dataTableProtocoloMensagem.on('click', '.item-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __protocoloMensagemIndex.options.datatables.protocoloMensagem.fnGetData($pai);

                if (!__protocoloMensagemIndex.options.ajax) {
                    location.href = __protocoloMensagemIndex.options.url.edit + '/' + data['mensagem_id'];
                } else {
                    __protocoloMensagemIndex.addOverlay(
                        $('#container-protocolo-mensagem'), 'Aguarde, carregando dados para edição...'
                    );
                    $.protocoloMensagemAdd().pesquisaProtocoloMensagem(
                        data['mensagem_id'],
                        function (data) {
                            __protocoloMensagemIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-protocolo-mensagem-add').click(function (e) {
                if (__protocoloMensagemIndex.options.ajax) {
                    __protocoloMensagemIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $dataTableProtocoloMensagem.on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __protocoloMensagemIndex.options.datatables.protocoloMensagem.fnGetData($pai);
                var arrRemover = {
                    mensagemId: data['mensagem_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de mensagem "' + data['protocolo_id'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __protocoloMensagemIndex.addOverlay(
                                $('#container-protocolo-mensagem'), 'Aguarde, solicitando remoção de registro de mensagem...'
                            );
                            $.ajax({
                                url: __protocoloMensagemIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __protocoloMensagemIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de mensagem:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __protocoloMensagemIndex.removeOverlay($('#container-protocolo-mensagem'));
                                    } else {
                                        __protocoloMensagemIndex.reloadDataTableProtocoloMensagem();
                                        __protocoloMensagemIndex.showNotificacaoSuccess(
                                            "Registro de mensagem removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#protocolo-mensagem-form'),
                $modal = $('#protocolo-mensagem-modal');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['mensagemId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['mensagemId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);


            var $protocolo = $("#protocolo");
            $protocolo.select2('data', data['protocolo']).trigger("change");

            var $mensagemUsuario = $("#mensagemUsuario");
            $mensagemUsuario.select2('data', data['mensagemUsuario']).trigger("change");

            var $mensagemOrigem = $("#mensagemOrigem");
            $mensagemOrigem.select2('val', data['mensagemOrigem']).trigger("change");

            var $mensagemSolicitanteVisivel = $("#mensagemSolicitanteVisivel");
            $mensagemSolicitanteVisivel.select2('val', data['mensagemSolicitanteVisivel']).trigger("change");

            $modal.find('.modal-title').html(actionTitle + ' mensagem');
            $modal.modal();
            __protocoloMensagemIndex.removeOverlay($('#container-protocolo-mensagem'));
        };

        this.reloadDataTableProtocoloMensagem = function () {
            this.getDataTableProtocoloMensagem().api().ajax.reload(null, false);
        };

        this.getDataTableProtocoloMensagem = function () {
            if (!__protocoloMensagemIndex.options.datatables.protocoloMensagem) {
                if (!$.fn.dataTable.isDataTable('#dataTableProtocoloMensagem')) {
                    __protocoloMensagemIndex.options.datatables.protocoloMensagem = $('#dataTableProtocoloMensagem').DataTable();
                } else {
                    __protocoloMensagemIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __protocoloMensagemIndex.options.datatables.protocoloMensagem;
        };

        this.run = function (opts) {
            __protocoloMensagemIndex.setDefaults(opts);
            __protocoloMensagemIndex.setSteps();
        };
    };

    $.protocoloMensagemIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.protocolo.protocolo-mensagem.index");

        if (!obj) {
            obj = new ProtocoloMensagemIndex();
            obj.run(params);
            $(window).data('universa.protocolo.protocolo-mensagem.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);