(function ($, window, document) {
    'use strict';

    var ProtocoloAdd = function () {
        VersaShared.call(this);
        var __protocoloAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            filtrarSolicitante: false,
            url: {
                protocoloSolicitacao: '',
                pessoa: '',
                acadgeralAlunoCurso: '',
                protocoloSetor: ''
            },
            data: {
                arrUsuarioLogado: null
            },
            value: {
                solicitacao: null,
                usuarioIdCriacao: null,
                usuarioIdResponsavel: null,
                protocoloSolicitantePes: null,
                protocoloSolicitanteAlunocurso: null,
                setor: null,
                protocoloSolicitanteVisivel: null,
                protocoloSituacao: null
            },
            datatables: {},
            wizardElement: '#protocolo-wizard',
            formElement: '#protocolo-form',
            formResponsavel: '#protocolo-responsavel-form',
            validator: null
        };

        this.steps = {};

        this.iniciarCampos = function () {
            var $selecao = $('[name="protocolo-opcao"]');
            var $solicitacaoPessoa = $('.solicitacao-pessoa');
            var $protocoloMensagem = $('#protocoloMensagem');
            var $solicitacaoCorpo = $('.solicitacao-corpo');

            $('.solicitacao-outros, .solicitacao-corpo').hide();

            $selecao.on('change', function () {
                $('.has-error').removeClass('has-error');
                $('.help-block').remove();
                $('#solicitacao').select2('disable');

                $('#protocoloSolicitantePes, #protocoloSolicitanteAlunocurso').select2('val', '');
                $('.solicitacao-corpo').find('input').each(function (index, element) {
                    var id = $(element).attr('id').replace('s2id_', '');
                    if (id != 'protocoloSolicitanteVisivel' && id != 'protocoloSituacao') {
                        $('#' + id).select2('val', '').trigger('change');
                    }
                });

                switch ($(this).val()) {
                    case 'solicitacao-pessoa':
                        $solicitacaoPessoa.show();
                        $('.solicitacao-corpo, .solicitacao-outros').hide();
                        break;
                    case 'solicitacao-outros':
                        $('.solicitacao-pessoa').hide();
                        $('.solicitacao-outros, .solicitacao-corpo').show();
                        break;
                    default:
                        break;
                }
            });

            $('[name="protocoloSolicitantePes"]').on('change', function () {
                if ($(this).val() != '') {
                    $solicitacaoCorpo.show();
                } else {
                    $solicitacaoCorpo.hide();
                }
            });

            $protocoloMensagem.ckeditor({
                height: '200px', linkShowAdvancedTab: false,
                autoStartup: true,
                enterMode: Number(2),
                toolbar: [
                    [
                        'Styles', 'Bold', 'Italic', 'Underline', 'SpellChecker', 'Scayt', '-',
                        'NumberedList', 'BulletedList',
                    ],
                    ['Table'],
                    ['Link', 'Unlink'],
                    ['Undo', 'Redo', '-', 'SelectAll']
                ]

            });
        };

        this.setSteps = function () {
            var $protocoloSetor = $("#setorId");
            var $solicitacao = $("#solicitacao");

            $('#protocoloSolicitanteTelefone').inputmask({
                showMaskOnHover: false,
                clearIncomplete: true,
                mask: ['(99) 99999-9999', '(99) 9999-9999']
            });

            $protocoloSetor.select2({
                language: 'pt-BR',
                allowClear: true,
                ajax: {
                    url: __protocoloAdd.options.url.protocoloSetor,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.setor_id;
                            el.text = el.setor_descricao;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            }).on('change', function () {
                var input = $(this).val();
                $solicitacao.select2("val", "");
                $solicitacao.trigger('cahnge');

                if (input != '') {
                    $solicitacao.select2("enable");
                } else {
                    $solicitacao.select2("disable");
                }
            });

            $protocoloSetor.select2("data", __protocoloAdd.getProtocoloSetor());

            $solicitacao.select2({
                language: 'pt-BR',
                allowClear: true,
                ajax: {
                    url: __protocoloAdd.options.url.protocoloSolicitacao,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {
                            query: query,
                            setorId: $protocoloSetor.val()
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.solicitacao_id;
                            el.text = el.solicitacao_descricao;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $solicitacao.select2("data", __protocoloAdd.getSolicitacao());

            var $protocoloSolicitantePes = $("#protocoloSolicitantePes");
            $protocoloSolicitantePes.select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __protocoloAdd.options.url.pessoa,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var arrDados = {query: query, filtrarSolicitante: 0};

                        if (__protocoloAdd.options.filtrarSolicitante) {
                            arrDados['filtrarSolicitante'] = 1;
                        }

                        return arrDados;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            var text = el.pes_nome + (el.pes_cpf ? ' | CPF: ' + el.pes_cpf : '');
                            el.id = el.pes_id;
                            el.text = text;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $protocoloSolicitantePes.select2("data", __protocoloAdd.getProtocoloSolicitantePes());

            if (__protocoloAdd.options.filtrarSolicitante) {
                $protocoloSolicitantePes.select2("disable");
            }

            var $protocoloSolicitanteAlunocurso = $("#protocoloSolicitanteAlunocurso");
            $protocoloSolicitanteAlunocurso.select2({
                language: 'pt-BR',
                allowClear: true,
                ajax: {
                    url: __protocoloAdd.options.url.acadgeralAlunoCurso,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var arrDados = {query: query, filtrarSolicitante: 0};

                        if (__protocoloAdd.options.filtrarSolicitante) {
                            arrDados['filtrarSolicitante'] = 1;
                        }

                        return arrDados;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            var text = el.pesNome + ' | ' + el.cursoNome + ' | ' + parseInt(el.alunocurso_id);

                            el.id = el.alunocurso_id;
                            el.text = text;
                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            var $protocoloSolicitanteVisivel = $("#protocoloSolicitanteVisivel");
            $protocoloSolicitanteVisivel.select2({
                language: 'pt-BR',
                data: __protocoloAdd.options.value.protocoloSolicitanteVisivel
            });

            $protocoloSolicitanteVisivel.select2('val', 'Sim').trigger('change');

            var $protocoloDataCadastro = $("#protocoloDataCadastro");
            __protocoloAdd.iniciarElementoDatePicker($protocoloDataCadastro);

            var $protocoloDataAlteracao = $("#protocoloDataAlteracao");
            __protocoloAdd.iniciarElementoDatePicker($protocoloDataAlteracao);

            var $protocoloAvaliacao = $("#protocoloAvaliacao");
            __protocoloAdd.inciarElementoInteiro($protocoloAvaliacao);

            var $protocoloSituacao = $("#protocoloSituacao");
            $protocoloSituacao.select2(
                {
                    allowClear: true,
                    language: 'pt-BR',
                    data: __protocoloAdd.options.value.protocoloSituacao,
                    initSelection: function (element, callback) {
                        var dados = __protocoloAdd.options.value.protocoloSituacao;
                        return callback({text: dados[0]['text'], id: dados[0]['id']});
                    }
                }
            );
        };

        this.setSolicitacao = function (solicitacao) {
            this.options.value.solicitacao = solicitacao || null;
        };

        this.getSolicitacao = function () {
            return this.options.value.solicitacao || null;
        };

        this.getProtocoloSetor = function () {
            return this.options.value.setor || null;
        };

        this.setUsuarioIdCriacao = function (usuarioIdCriacao) {
            this.options.value.usuarioIdCriacao = usuarioIdCriacao || null;
        };

        this.getUsuarioIdCriacao = function () {
            return this.options.value.usuarioIdCriacao || null;
        };

        this.setUsuarioIdResponsavel = function (usuarioIdResponsavel) {
            this.options.value.usuarioIdResponsavel = usuarioIdResponsavel || null;
        };

        this.getUsuarioIdResponsavel = function () {
            return this.options.value.usuarioIdResponsavel || null;
        };

        this.setProtocoloSolicitantePes = function (protocoloSolicitantePes) {
            this.options.value.protocoloSolicitantePes = protocoloSolicitantePes || null;
        };

        this.getProtocoloSolicitantePes = function () {
            return this.options.value.protocoloSolicitantePes || null;
        };

        this.setProtocoloSolicitanteAlunocurso = function (protocoloSolicitanteAlunocurso) {
            this.options.value.protocoloSolicitanteAlunocurso = protocoloSolicitanteAlunocurso || null;
        };

        this.getProtocoloSolicitanteAlunocurso = function () {
            return this.options.value.protocoloSolicitanteAlunocurso || null;
        };

        this.validacaoDinamica = function (opcao) {
            if (opcao == 'solicitacao-pessoa') {
                __protocoloAdd.options.validator.settings.rules = {
                    protocoloSolicitantePes: {required: true},
                    solicitacao: {number: true, required: true},
                    protocoloAssunto: {maxlength: 200, required: true},
                    setorId: {required: true},
                    protocoloSituacao: {maxlength: 12},
                    protocoloMensagem: {required: true},
                    protocoloSolicitanteVisivel: {required: true}
                };
            } else {
                __protocoloAdd.options.validator.settings.rules = {
                    protocoloSolicitanteNome: {required: true},
                    solicitacao: {number: true, required: true},
                    protocoloSolicitanteEmail: {maxlength: 100, email: true},
                    protocoloSolicitanteTelefone: {telefoneValido:true, maxlength: 45},
                    protocoloAssunto: {maxlength: 200, required: true},
                    setorId: {required: true},
                    protocoloSituacao: {maxlength: 12},
                    protocoloMensagem: {required: true}
                };
            }
        };

        this.setValidations = function () {
            __protocoloAdd.options.validator.settings.messages = {
                solicitacao: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                protocoloSolicitantePes: {number: 'Número inválido!'},
                protocoloSolicitanteNome: {maxlength: 'Tamanho máximo: 100!', required: "Campo Obrigatório"},
                protocoloSolicitanteEmail: {maxlength: 'Tamanho máximo: 100!'},
                protocoloSolicitanteTelefone: {maxlength: 'Tamanho máximo: 45!'},
                protocoloAssunto: {maxlength: 'Tamanho máximo: 200!', required: 'Campo obrigatório!'},
                protocoloSituacao: {maxlength: 'Tamanho máximo: 12!'},
                protocoloMensagem: {required: "Campo Obrigatório"},
                setorId: {required: "Campo Obrigatório"},
                protocoloSolicitanteVisivel: {required: "Campo Obrigatório!"}
            };

            $(__protocoloAdd.options.formElement).submit(function (e) {
                __protocoloAdd.validacaoDinamica($('[name="protocolo-opcao"]:checked').val());
                var ok = $(__protocoloAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__protocoloAdd.options.formElement);
                $form.find('[name="ajax"]').val(true);

                if (__protocoloAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    dados['protocoloSolicitantePes'] = $("#protocoloSolicitantePes").val();

                    dados['protocoloSolicitanteVisivel'] = dados['protocoloSolicitanteVisivel'] || 'Sim';
                    dados['protocoloSituacao'] = dados['protocoloSituacao'] || 'Aberto';

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __protocoloAdd.addOverlay($form, 'Salvando registro. Aguarde...');

                        $form.ajaxSubmit({
                            dataType: 'json',
                            success: function (data) {
                                if (data.erro) {
                                    __protocoloAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__protocoloAdd.options.listagem) {
                                    $.protocoloIndex().reloadDataTableProtocolo();
                                    __protocoloAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#protocolo-modal').modal('hide');
                                    $('#solicitacao-pessoa').prop('checked', true).trigger('change');
                                    $.protocoloEdit().limparCacheAnexos();
                                }

                                __protocoloAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            },
                            error: function (data) {
                                __protocoloAdd.showNotificacaoDanger(
                                    "Não foi possível efetuar a operação. Atualize a tela e tente novamente."
                                );
                                __protocoloAdd.removeOverlay($form);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaProtocolo = function (protocolo_id, callback) {
            var $form = $(__protocoloAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __protocoloAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + protocolo_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __protocoloAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __protocoloAdd.removeOverlay($form);
                },
                erro: function () {
                    __protocoloAdd.showNotificacaoDanger('Erro desconhecido!');
                    __protocoloAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.iniciarCampos();
            this.setValidations();
        };
    };

    $.protocoloAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.protocolo.protocolo.add");

        if (!obj) {
            obj = new ProtocoloAdd();
            obj.run(params);
            $(window).data('universa.protocolo.protocolo.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);