(function ($, window, document) {
    'use strict';
    var ProtocoloEdit = function () {
        VersaShared.call(this);
        var __protocoloEdit = this;

        this.defaults = {
            filtrarSolicitante: false,
            url: {
                search: '',
                novo: '',
                editar: '',
                protocoloMensagem: '',
                atualizaSituacao: '',
                mensagemPadrao: '',
                searchArquivo: '',
                downloadArquivo: '',
                imprimir: ''
            },
            data: {
                arrUsuarioLogado: null
            },
            value: {
                editar: true,
                arrDados: null,
                arrProtocoloSituacao: null,
                qtdArquivosAnexados: 0,
                permissaoImprimir: false
            },
            formElement: '#formProtocoloMensagem',
            formInfo: '#form-info',
            datatables: {
                protocoloMensagemArquivo: null
            },
            validator: null,
            systemName: '',
            systemMessageText: ''
        };

        this.getSystemName = function () {
            return __protocoloEdit.options.systemName || 'MENSAGEM DO SISTEMA';
        };

        this.setSystemName = function (systemName) {
            __protocoloEdit.options.systemName = systemName;
        };

        this.getSystemMessageText = function () {
            return __protocoloEdit.options.systemMessageText || 'Vocẽ não tem permissão para enviar mensagens';
        };

        this.setSystemMessageText = function (systemMessageText) {
            __protocoloEdit.options.systemMessageText = systemMessageText;
        };

        this.retornaMensagemConteudo = function () {
            var $formMensagem = $(__protocoloEdit.options.formElement);
            var mensagem = $formMensagem.find('[name="mensagemConteudo"]').val();

            var ok = $('<div/>').append(mensagem).text();
            ok = ok || "";
            ok = ok.replace(/(&nbsp;|[\s\n])/g, '');
            ok = ok.trim();

            return ok;
        };

        this.chat = function () {
            __protocoloEdit.showMessages();
            var $formMensagem = $(__protocoloEdit.options.formElement),
                $formAcoes = $(__protocoloEdit.options.formInfo);

            $formMensagem.find('[name="ajax"]').val(true);

            $formMensagem.submit(function (e) {
                var $overlay = $('.chat');
                var $modal = $('#protocolo-edit-modal');
                var $corpo = $modal.find('.modal-dialog, .edit-body');
                var mensagem = __protocoloEdit.retornaMensagemConteudo();

                if (mensagem == "") {
                    __protocoloEdit.showNotificacaoDanger(
                        "Por favor, preencha o campo da mensagem!"
                    );
                    e.stopPropagation();
                    e.preventDefault();
                    return false;
                }
                $formMensagem.attr('action', __protocoloEdit.options.url.protocoloMensagem);

                __protocoloEdit.enviarMensagem();

                e.stopPropagation();
                e.preventDefault();

                return false;
            });

            var btns = [
                {class: 'btn-success item-transferencia', icon: 'fa-exchange'},
                {class: 'btn-danger item-responsavel', icon: 'fa-bell-o'}
            ];

            $('.btn-acoes').html(__protocoloEdit.createBtnGroup(btns));

            $formAcoes.on('click', '.item-responsavel', function (e) {
                $.protocoloIndex().showModalResponsavel(__protocoloEdit.getArrDados());
                e.preventDefault();
                e.stopPropagation();
            });

            $formAcoes.on('click', '.item-transferencia', function (e) {
                $.protocoloIndex().showModalTransferencia(__protocoloEdit.getArrDados());
                e.preventDefault();
                e.stopPropagation();
            });
        };

        this.enviarMensagem = function (mensagem) {
            var arrDados = __protocoloEdit.getArrDados(),
                protocoloId = arrDados['protocolo_id'];

            var $formMensagem = $(__protocoloEdit.options.formElement),
                $modal = $('#protocolo-edit-modal'),
                $corpo = $modal.find('.modal-dialog'),
                $overlay = $('.chat');

            __protocoloEdit.addOverlay($corpo, 'Enviando mensagem... ');

            $formMensagem.val(mensagem || __protocoloEdit.retornaMensagemConteudo());

            $formMensagem.ajaxSubmit({
                dataType: 'json',
                success: function (data) {
                    if (!arrDados['nome_responsavel'] && !__protocoloEdit.options.filtrarSolicitante) {
                        __protocoloEdit.alteraReponsavel(protocoloId);
                    }

                    __protocoloEdit.refreshMessages();
                    var removeOverlay = false;

                    var $fecharAposEnvio = $('#fecharAposEnvio');
                    var $alterarSituacaoAposEnvio = $('#alterarSituacaoAposEnvio');
                    if ($fecharAposEnvio.val() == '1') {
                        $("#protocolo-edit-modal").modal('hide');
                        removeOverlay = true;
                    }

                    $fecharAposEnvio.val('0');

                    if ($alterarSituacaoAposEnvio.val() == '1') {
                        var $formSituacao = $('#form-info');
                        $formSituacao.find("[name=protocoloSituacao]").trigger('change');
                        removeOverlay = true;
                    }

                    if (removeOverlay) {
                        __protocoloEdit.removeOverlay($corpo);
                    }

                    $alterarSituacaoAposEnvio.val('0');

                    if (!data['erro']) {
                        __protocoloEdit.showNotificacaoSuccess(
                            "Mensagem enviada com sucesso"
                        );
                        $formMensagem.data('ajax', false);
                        __protocoloEdit.limparCacheAnexos();
                    } else {
                        __protocoloEdit.showNotificacaoDanger(
                            data['mensagem'] || 'Não foi possível enviar a última mensagem!'
                        );
                    }

                    __protocoloEdit.removeOverlay($corpo);
                },
                error: function (data) {
                    __protocoloEdit.showNotificacaoDanger(
                        "Não foi possível efetuar a operação. Atualize a tela e tente novamente."
                    );
                    __protocoloEdit.removeOverlay($corpo);
                }

            });
        };

        this.anexos = function () {
            var colNum = -1;
            var $dataTableProtocoloMensagemArquivo = $('#dataTableProtocoloMensagemArquivo');

            __protocoloEdit.options.datatables.protocoloMensagemArquivo =
                $dataTableProtocoloMensagemArquivo.dataTable({
                    processing: false,
                    serverSide: false,
                    searching: false,
                    "order": [1, 'asc'],
                    data: [],
                    columns: [
                        {name: "mensagem_data", targets: ++colNum, data: "mensagem_data"},
                        {name: "mensagem_usuario", targets: ++colNum, data: "mensagem_usuario"},
                        {name: "arq_nome", targets: ++colNum, data: "arq_nome"},
                        {name: "btn-acoes", targets: ++colNum, data: "btn-acoes"}
                    ],
                    "dom": "fr" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                    "autoWidth": true,
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "Procurar: ",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    }
                });

            $('#btn-anexar').on('click', function (e) {
                var $element = $('#anexoInput').clone();
                var id = __protocoloEdit.createGUID();
                $element.attr('id', id);
                $element.click();
                $element.appendTo('#arrMensagemAnexos');
            });

            $(document).on('click', '.btn-excluir', function () {
                var el = '#' + $(this).attr('id');
                $('#formProtocoloMensagem').find(el).remove();
                __protocoloEdit.reloadDatatables();
            });

            $('.btn-ver-anexos').on('click', function () {
                __protocoloEdit.reloadDatatables();
            });

            $('.div-anexos, .div-anexos-add').change(function () {
                __protocoloEdit.reloadDatatables();
            });

            this.reloadDatatables = function () {
                var novosAnexosQtd = 0;
                var dados = $.ajax({
                    url: __protocoloEdit.options.url.searchArquivo,
                    method: "POST",
                    async: false,
                    type: 'json',
                    data: {protocoloId: __protocoloEdit.options.value.arrDados['protocoloId'] || ''}
                }).responseJSON['data'];

                dados = dados || [];

                $('.div-anexos').find('input').each(function (i, element) {
                    var arquivo = element.files[0];
                    var nomeArquivo = arquivo.name || '';
                    var GUID = element.id;
                    dados.push({
                        'arq_nome': '(NOVO) ' + nomeArquivo,
                        'mensagem_data': __protocoloEdit.formatDateTime((new Date())),
                        'mensagem_usuario': '-',
                        'mensagem_id': GUID,
                        'arq_chave': false,
                        'class': 'btn btn-danger btn-excluir'
                    });
                    novosAnexosQtd++;
                });

                if (novosAnexosQtd > 0) {
                    $('.btn-ver-anexos').text(
                        "Novos Anexos (" + novosAnexosQtd + ")"
                    );
                } else {
                    $('.btn-ver-anexos').html(
                        "Anexar na Mensagem <i class='fa fa-paperclip'></i>"
                    );
                }

                __protocoloEdit.options.value.qtdArquivosAnexados = novosAnexosQtd;

                for (var row in dados) {
                    var url = __protocoloEdit.options.url.downloadArquivo + '/' + dados[row]['arq_chave'];

                    if (!dados[row]['arq_chave']) {
                        url = "#"
                    }

                    var btns = [
                        {
                            icon: 'fa fa-times',
                            element: 'button',
                            id: dados[row]['mensagem_id'],
                            class: dados[row]['class'] || 'btn btn-danger disabled'
                        }
                    ];

                    dados[row]['arq_nome'] = "<a href='" + url + "'>" + dados[row]['arq_nome'] + "</a>";

                    dados[row]['btn-acoes'] = '';

                    dados[row]['mensagem_data'] = this.formatDateTime(dados[row]['mensagem_data']);

                    if (!dados[row]['arq_chave']) {
                        dados[row]['btn-acoes'] = __protocoloEdit.createBtnGroup(btns);
                    }
                }

                __protocoloEdit.options.datatables.protocoloMensagemArquivo.fnClearTable();

                if (dados.length > 0) {
                    __protocoloEdit.options.datatables.protocoloMensagemArquivo.fnAddData(dados);
                    __protocoloEdit.options.datatables.protocoloMensagemArquivo.fnDraw(dados);
                }
            };
        };

        this.limparCacheAnexos = function () {
            $('.div-anexos, .div-anexos-add').empty();
            __protocoloEdit.reloadDatatables();
        };

        this.cleanMessages = function () {
            $('.chat_corpo').empty();
        };

        this.showMessages = function (data) {
            __protocoloEdit.cleanMessages();
            var dados = data || __protocoloEdit.options.value.arrDados['arrMensagem'] || [];
            if (!dados) {
                $('.chat_corpo').append('<h1 style="color: rgb(107, 159, 83);font-weight: bolder;">Nenhuma Mensagem</h1>');
            }

            $.map(dados, function (elements) {
                __protocoloEdit.appendMensage(elements);
            });

            $('.app-overlay').remove();
            __protocoloEdit.downScroll();
        };

        this.refreshMessages = function (protocoloId, mensagemDoSistema) {
            var arrUsuarioLogado = __protocoloEdit.getUsuarioLogado() || [];
            __protocoloEdit.options.value.arrDados['protocoloId'] =
                protocoloId || __protocoloEdit.options.value.arrDados['protocoloId'];

            $(__protocoloEdit.options.formElement).find('[name="protocolo"]').val(
                __protocoloEdit.options.value.arrDados['protocoloId']
            );

            $.ajax({
                url: __protocoloEdit.options.url.search,
                type: 'POST',
                dataType: 'json',
                delay: 400,
                data: {
                    protocoloId: __protocoloEdit.options.value.arrDados['protocoloId'],
                    usuarioLogado: arrUsuarioLogado['id'] || ''
                },
                success: function (data) {
                    __protocoloEdit.showMessages(data['messages']);

                    if (mensagemDoSistema) {
                        __protocoloEdit.systemMessage(mensagemDoSistema);
                    }
                }
            });

            $('#mensagemConteudo').val('<p>&nbsp;</p>');
        };

        this.appendMensage = function (dados, destacar) {
            var $corpo = $('.chat_corpo');
            var classe = dados.origem == "Operador" ? 'msg_enviada text-right' : 'msg_recebida text-left';
            var anexosLink = '';

            if (dados['anexos']) {
                anexosLink = "<ul style='list-style: none'>";
                $.map(dados['anexos'], function (element) {
                    var nome = element['arq_nome'];
                    var url = __protocoloEdit.options.url.downloadArquivo + '/' + element['arq_chave'] + "?nd=1";
                    anexosLink += "<li><a href='" + url + "'target= '_blank'>" + nome + "</a></li>";
                });
                anexosLink += "</ul>";
            }

            var icone = "";

            if (dados['visivel'] == "Não" && dados['origem'] == "Operador") {
                icone = "<i class='fa fa-flag text-danger' title='Essa mensagem não é visível ao solicitante'></i>";
            }

            if (destacar) {
                destacar = "destacado";
            } else {
                destacar = "";
            }

            var div = '<div class="' + classe + '">' +
                '<div class="msg_header ' + destacar + '" id="' + dados['mensagemId'] + '"><strong>' +
                dados['autor'] + ' - ' + dados['data'] +
                '</strong></div>' +
                '<div class="msg_body">' +
                '<p>' +
                dados['mensagem'] +
                '</p>' +
                anexosLink +
                '<p style="text-align: left">' +
                icone +
                '</p>' +
                '</div>' +
                '</div>';

            $corpo.append(div);
        };

        this.downScroll = function () {
            var $element = $(".chat-fieldset");
            $element.scrollTop($element[0].scrollHeight);
        };

        this.systemMessage = function (message) {
            var arrDados = [];
            var data = new Date();

            message = __protocoloEdit.getSystemMessageText() || message;

            arrDados['autor'] = __protocoloEdit.getSystemName();
            arrDados['mensagem'] = message;
            arrDados['data'] = data.toLocaleDateString() + ' ' + data.toLocaleTimeString();
            arrDados['origem'] = '';

            __protocoloEdit.appendMensage(arrDados, true);

            __protocoloEdit.downScroll();
        };

        this.setSteps = function () {
            var editar = __protocoloEdit.options.value.editar;
            var $formMensagem = $('#formProtocoloMensagem');
            var $formSituacao = $('#form-info');
            var $protocoloSituacao = $formSituacao.find("[name=protocoloSituacao]"),
                $mensagemConteudo = $formMensagem.find("[name=mensagemConteudo]"),
                $mensagemPadrao = $formMensagem.find("[name=mensagemPadrao]");

            $mensagemConteudo.ckeditor({
                height: '90px', linkShowAdvancedTab: false,
                autoStartup: true,
                enterMode: Number(2),
                toolbar: [
                    [
                        'Styles', 'Bold', 'Italic', 'Underline', 'SpellChecker', 'Scayt', '-',
                        'NumberedList', 'BulletedList'
                    ],
                    ['Table'],
                    ['Link', 'Unlink'],
                    ['Undo', 'Redo', '-', 'SelectAll']
                ]
            });

            CKEDITOR.instances.mensagemConteudo.on('key', function (event) {
                var envioComEnter = $("#envioComEnter").prop("checked");

                if ($protocoloSituacao.val() == "Concluído") {
                    return false;
                }

                if (envioComEnter && event.data['keyCode'] === 13) {
                    $('#btn-enviar').click();
                }
            });

            $mensagemPadrao.select2({
                ajax: {
                    url: __protocoloEdit.options.url.mensagemPadrao,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {
                            query: query,
                            protocoloId: __protocoloEdit.options.value.arrDados['protocoloId'],
                            setorId: __protocoloEdit.options.value.arrDados['setor_id'],
                            solicitacaoId: __protocoloEdit.options.value.arrDados['solicitacao_id']
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.mensagem_conteudo;
                            el.text = el.mensagem_descricao;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            }).on('select2-highlight', function (element) {
                $('.select2-result-label').attr('title', element.val);
            }).on('change', function (e) {
                $mensagemConteudo.val(e.val || '<p>&nbsp;</p>');
            });

            $protocoloSituacao.select2({
                allowClear: false,
                language: 'pt-BR',
                data: __protocoloEdit.options.value.arrProtocoloSituacao
            }).on('change', function (e) {
                var $form = $(__protocoloEdit.options.formInfo);
                var $alterarSituacaoAposEnvio = $('#alterarSituacaoAposEnvio');
                var $modal = $('#protocolo-edit-modal');
                var $corpo = $modal.find('.modal-dialog, .edit-body');
                var dados = $form.serializeJSON();
                var ok = dados['protocoloSituacao'] || false;
                var mensagemConteudo = $mensagemConteudo.val();

                dados['protocoloId'] = dados['protocoloId'] || __protocoloEdit.options.value.arrDados['protocoloId'];
                dados['alterarSituacao'] = true;
                dados['ajax'] = true;

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    __protocoloEdit.showNotificacaoWarning('Defina uma situação');
                    return false;
                }

                $alterarSituacaoAposEnvio.val('0');

                if (__protocoloEdit.retornaMensagemConteudo() != "" && e.removed['id'] != "Concluído") {
                    $.SmartMessageBox({
                        title: (
                            "Atenção: Existe uma mensagem não enviada neste protocolo. " +
                            "Deseja envia-lá antes de prosseguir?"
                        ),
                        buttons: "[Não enviar][Enviar mensagem]"
                    }, function (ButtonPress) {
                        if (ButtonPress == "Enviar mensagem") {
                            $alterarSituacaoAposEnvio.val('1');
                            __protocoloEdit.addOverlay($corpo, 'Aguarde, enviando mensagem... ');
                            __protocoloEdit.enviarMensagem(mensagemConteudo);
                        } else {
                            $('#mensagemConteudo').val('<p>&nbsp;</p>');
                            __protocoloEdit.alterarSituacao(dados);
                        }
                    });
                } else {
                    __protocoloEdit.alterarSituacao(dados);
                }
            });

            if (__protocoloEdit.options.filtrarSolicitante) {
                $protocoloSituacao.select2('disable');
            }

            if (Object.keys(__protocoloEdit.getArrDados()).length > 1) {
                __protocoloEdit.setFormStaticValues(__protocoloEdit.options.value.arrDados);
            }

            $(document).on('click', '.msg-padrao', function () {
                var valor = $(this).val();
                var $campo = $('[name="mensagemConteudo"]');
                $campo.val(valor || '<p>&nbsp;</p>');
                $('#btn-enviar').click();
            });

            $("#protocolo-edit-modal").on("hide.bs.modal", function (e) {
                var exibeMensagem = false;

                $("#protocolo-edit-modal").data('closing', true);
                exibeMensagem = __protocoloEdit.exibeMensagemDescartarEnvio(
                    __protocoloEdit.retornaMensagemConteudo(),
                    "Atenção: Existe uma mensagem não enviada neste protocolo. " +
                    "Deseja envia-lá antes de prosseguir?"
                );

                if (!exibeMensagem) {
                    exibeMensagem = __protocoloEdit.exibeMensagemDescartarAnexos(__protocoloEdit.options.value.qtdArquivosAnexados);
                }

                if (exibeMensagem) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }
            });

            $("#btn-imprimir").on('click', function (e) {
                __protocoloEdit.addOverlay($(".imprimir-protocolo"));

                var arrDados = __protocoloEdit.getArrDados(),
                    protocoloId = arrDados['protocoloId'];
                var url = __protocoloEdit.options.url.imprimir + '?protocoloId=' + protocoloId;

                $(".modal-title").text('Informações do Protocolo: ' + protocoloId);
                $('#corpo-impressao-modal').attr('src', url);
                $(".imprimir-protocolo").modal('show');

                e.preventDefault();
                e.stopPropagation();
            });

            $(".close-Iframe").on("click", function (e) {
                $(".imprimir-protocolo").modal("hide");

                e.stopImmediatePropagation();
            });

            $('#corpo-impressao-modal').load(function () {
                __protocoloEdit.removeOverlay($(".imprimir-protocolo"));
            });

            $("#imprimirProtocolo").on("click", function (e) {
                $('#corpo-impressao-modal').get(0).contentWindow.print();

                e.stopImmediatePropagation();
            });

            $("#btn-imprimir").prop("disabled", false);
            $("#btn-imprimir").removeClass("hidden");

            if (!__protocoloEdit.options.value.permissaoImprimir) {
                $("#btn-imprimir").prop("disabled", true);
                $("#btn-imprimir").addClass("hidden");
            }
        };

        this.exibeMensagemDescartarAnexos = function (quantidadeArquivos) {
            if (quantidadeArquivos > 0) {
                var title = "Deseja descartar o arquivo anexado?";

                if (quantidadeArquivos > 1) {
                    title = "Deseja descartar os arquivos anexados?"
                }

                $.SmartMessageBox({
                    title: title,
                    buttons: "[Não descartar][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        __protocoloEdit.limparCacheAnexos();
                        $.protocoloIndex().limparCacheAnexos();
                        $('#mensagemConteudo').val('<p>&nbsp;</p>');
                        $("#protocolo-edit-modal").modal('hide');
                    }
                });

                return true;
            }

            return false;
        };

        this.exibeMensagemDescartarEnvio = function (conteudo, title) {
            var $fecharAposEnvio = $('#fecharAposEnvio');
            var $modal = $('#protocolo-edit-modal');
            var $corpo = $modal.find('.modal-dialog, .edit-body');

            $fecharAposEnvio.val('0');

            if (conteudo == "") {
                return false;
            }

            var data = $("[name=protocoloSituacao]").select2('data');

            if (data['text'] != "Concluído") {
                $.SmartMessageBox({
                    title: title,
                    buttons: "[Não enviar][Enviar mensagem]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Enviar mensagem") {
                        __protocoloEdit.addOverlay($corpo, 'Aguarde, enviando mensagem... ');
                        $fecharAposEnvio.val('1');
                        $('#btn-enviar').click();
                    } else {
                        $('#mensagemConteudo').val('<p>&nbsp;</p>');
                        $("#protocolo-edit-modal").modal('hide');
                    }
                });

                return true;
            }

            return false;
        };

        this.alterarSituacao = function (dados) {
            var $protocoloSituacaoFormGroup = $("#protocolo-edit-modal").find('[name=protocoloSituacao]').closest('.form-group');

            __protocoloEdit.addOverlay($protocoloSituacaoFormGroup, 'Aguarde, alterando situação... ');

            $.ajax({
                url: __protocoloEdit.options.url.atualizaSituacao,
                type: 'POST',
                dataType: 'json',
                method: 'POST',
                data: dados,
                success: function (data) {
                    if (data['erro'] == true) {
                        __protocoloEdit.showNotificacaoWarning(
                            data['mensagem'] || 'Não foi possível atualizar, por favor, tente novamente.',
                            'Erro'
                        );
                    } else {
                        __protocoloEdit.showNotificacaoSuccess(
                            data['mensagem'] || 'Situação atualizada com sucesso!'
                        );

                        $.protocoloIndex().reloadDataTableProtocolo();
                        __protocoloEdit.refreshMessages();
                    }
                    __protocoloEdit.removeOverlay($protocoloSituacaoFormGroup);
                    __protocoloEdit.bloqueiaEnvioMensagem($("[name=protocoloSituacao]").select2('data'));
                },
                error: function (data) {
                    __protocoloEdit.showNotificacaoDanger(
                        "Não foi possível efetuar a operação. Atualize a tela e tente novamente."
                    );
                    __protocoloEdit.removeOverlay($protocoloSituacaoFormGroup);
                }
            });
        };

        this.alteraReponsavel = function (protocoloId) {
            var $corpo = $('.modal-dialog');

            if (!$(".app-overlay").is(":visible")) {
                __protocoloEdit.addOverlay($corpo, 'Aguarde... ');
            }

            $.ajax({
                url: __protocoloEdit.options.url.editar,
                type: 'POST',
                dataType: 'json',
                data: {
                    'protocoloId': protocoloId,
                    'acao-origem ': "editar",
                    'local': "responsavel",
                    'usuarioIdResponsavel': "pessoaLogada"
                },
                success: function (data) {
                    if (data.erro) {
                        __protocoloEdit.showNotificacaoDanger(
                            "Não foi possivel alterar o responsavel:\n" +
                            "<i>" + data['message'] + "</i>"
                        );
                        __protocoloEdit.removeOverlay($corpo);
                    } else {
                        __protocoloEdit.showNotificacaoSuccess(data['message'] || "Responsavel alterado com sucesso!");
                        __protocoloEdit.removeOverlay($corpo);

                        var arrDados = __protocoloEdit.getArrDados();
                        $.protocoloIndex().reloadDataTableProtocolo();
                        $.protocoloEdit().carregarProtocolo(arrDados, true);

                    }
                },
                error: function (data) {
                    __protocoloEdit.showNotificacaoDanger(
                        "Não foi possível efetuar a operação. Atualize a tela e tente novamente."
                    );
                    __protocoloEdit.removeOverlay($corpo);
                }
            });
        };

        this.setFormStaticValues = function (dados) {
            var $form = $(__protocoloEdit.options.formInfo);
            var $formMensagem = $(__protocoloEdit.options.formElement);
            var data = dados || __protocoloEdit.options.value.arrDados || {};
            var $modal = $('#protocolo-edit-modal');
            var status = data.protocoloSituacao || '';

            if (data.result) {
                data['protocolo_assunto'] = data.result['protocolo_assunto'];
                data['nome_responsavel'] = data.result['nome_responsavel'];
                data['protocoloIdFormatado'] = data['protocolo_id'];
            }

            __protocoloEdit.options.value.arrDados['protocolo'] = data.protocoloId;

            $modal.find('.protocoloAssuntoTitulo').html(
                data['protocoloAssunto'] ? (data['protocoloAssunto']) : '-'
            );

            $modal.find('.protocoloIdTitulo').html(data['protocolo_id']);

            if (!data['usuario_alteracao']) {
                data['usuario_alteracao'] = '-';
            }

            if (!data['usuario_criacao']) {
                data['usuario_criacao'] = '-';
            }

            $modal.find('.dataProtocoloCriacao').html(data['protocolo_data_cadastro_formatada']);
            $modal.find('.usuarioAlteracao').html(data['usuario_alteracao']);
            $modal.find('.usuarioCriacao').html(data['usuario_criacao']);
            $modal.find('.dataProtocoloAtualizacao').html(data['protocolo_data_alteracao_formatada']);
            $modal.find('.protocoloAssunto').html(data['protocoloIdFormatado'] + ': ' + data['protocolo_assunto']);

            __protocoloEdit.bloqueiaEnvioMensagem(status);

            var $protocoloSituacao = $('#form-info').find("[name=protocoloSituacao]");
            $form.find('p,span').each(function (i, value) {
                var key = $(value).attr('data-info');
                $(value).html(data[key]);
            });

            $protocoloSituacao.select2('data', data['protocoloSituacao'] || 'Aberto');
        };

        this.bloqueiaEnvioMensagem = function (obj) {
            var $msgBox = $('.msg-box');
            if (obj.text == "Concluído") {
                $('#btn-enviar, .btn-ver-anexos').prop('disabled', true);
                $("#mensagemSolicitanteVisivel").prop("disabled", true);
                $(".camposMessagem").addClass("hidden");
                $msgBox.find('input').each(function (e, i) {
                    $(i).select2('disable')
                })
            } else {
                $('#btn-enviar, .btn-ver-anexos').prop('disabled', false);
                $("#mensagemSolicitanteVisivel").prop("disabled", false);
                $(".camposMessagem").removeClass("hidden");
                $msgBox.find('input').each(function (e, i) {
                    $(i).select2('enable')
                })
            }
        };

        this.carregarProtocolo = function (arrDadosProtocolo, naoAbrirModal) {
            arrDadosProtocolo = arrDadosProtocolo || [];
            naoAbrirModal = naoAbrirModal || false;
            var $modal = $('#protocolo-edit-modal');
            var $corpo = $modal.find('.modal-dialog');
            var protocoloId = (arrDadosProtocolo['protocolo_id'] || arrDadosProtocolo['protocoloId'] || '');
            var usuarioId = __protocoloEdit.getUsuarioLogado() || [];

            if (!protocoloId) {
                __protocoloEdit.showNotificacaoDanger('Código de protocolo inválido!');
            }

            __protocoloEdit.setArrDados(arrDadosProtocolo);

            if (!naoAbrirModal) {
                $modal.modal('show');
            }

            __protocoloEdit.addOverlay($corpo, 'Aguarde... ');

            $.ajax({
                url: __protocoloEdit.options.url.editar + '/' + protocoloId,
                method: 'POST',
                type: 'json',
                dataType: 'json',
                delay: 350,
                data: {
                    protocoloId: protocoloId,
                    local: 'index'
                },
                success: function (arrData) {
                    var arrDados = $.extend(true, $.extend(true, {}, arrDadosProtocolo), arrData);
                    __protocoloEdit.setArrDados(arrDados);

                    var responsavel = arrData['usuarioIdResponsavelLogin'] || arrDados['nome_responsavel'] || null;
                    var protocoloSituacao = arrData['protocoloSituacao'] || arrDados['protocolo_situacao'] || null;
                    protocoloSituacao = protocoloSituacao['id'] || protocoloSituacao;

                    if (!responsavel && arrDados['result']) {
                        responsavel = arrDados['result']['nome_responsavel'] || null;
                    }

                    if (!responsavel && !__protocoloEdit.options.filtrarSolicitante && protocoloSituacao != 'Concluído') {
                        $.SmartMessageBox({
                            title: "Confirme a operação:",
                            content: 'Deseja torna-se responsável por este protocolo' + '"?',
                            buttons: "[Não][Sim]"
                        }, function (ButtonPress) {
                            if (ButtonPress == "Sim") {
                                __protocoloEdit.alteraReponsavel(protocoloId);
                            }
                        });
                    }

                    __protocoloEdit.setFormStaticValues();
                },
                error: function (data) {
                    __protocoloEdit.showNotificacaoDanger(
                        "Não foi possível efetuar a operação. Atualize a tela e tente novamente."
                    );
                    __protocoloEdit.removeOverlay($corpo);
                }
            });

            usuarioId = usuarioId['id'] || false;

            if (
                !__protocoloEdit.options.filtrarSolicitante ||
                (usuarioId == arrDadosProtocolo['usuario_id_criacao'] || usuarioId == arrDadosProtocolo['usuario_id_responsavel'])
            ) {
                $(__protocoloEdit.options.formElement).show();
                __protocoloEdit.refreshMessages(protocoloId);
            } else {
                $(__protocoloEdit.options.formElement).hide();
                __protocoloEdit.refreshMessages(protocoloId, true);
            }
        };

        this.setArrDados = function (arrDados) {
            this.options.value.arrDados = arrDados || [];
        };

        this.getArrDados = function () {
            var arrDados = this.options.value.arrDados || [];

            arrDados['acao-origem'] = 'editar';

            return arrDados;
        };

        this.getUsuarioLogado = function () {
            return __protocoloEdit.options.data.arrUsuarioLogado;
        };

        this.run = function (opts) {
            __protocoloEdit.setDefaults(opts);
            __protocoloEdit.setSteps();
            __protocoloEdit.chat();
            __protocoloEdit.anexos();
        };
    };

    $.protocoloEdit = function (params) {
        params = params || [];

        var obj = $(window).data("universa.protocolo.protocolo.edit");

        if (!obj) {
            obj = new ProtocoloEdit();
            obj.run(params);
            $(window).data('universa.protocolo.protocolo.edit', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);