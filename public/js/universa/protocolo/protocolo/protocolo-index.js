(function ($, window, document) {
    'use strict';
    var ProtocoloIndex = function () {
        VersaShared.call(this);
        var __protocoloIndex = this;
        this.defaults = {
            ajax: true,
            filtrarSolicitante: false,
            url: {
                search: '',
                edit: '',
                transferencia: '',
                remove: '',
                solicitacao: '',
                setor: '',
                solicitanteExterno: '',
                solicitanteInterno: '',
                acessoPessoa: '',
                protocolo: '',
                pesquisaAluno: '',
                acessoPessoas: ''
            },
            data: {
                arrUsuarioLogado: null
            },
            datatables: {
                protocolo: null
            },
            dataTableProtocolo: null,
            values: {
                editar: true,
                quantidadeArquivo: 0,
                mensagemDataTable: 'Use os filtros para realizar a busca!',
                filtroObrigatorio: 1
            }
        };

        this.iniciarCamposResponsavel = function () {
            var $usuarioResponsavel = $("[name=usuarioIdResponsavel]");

            $usuarioResponsavel.select2({
                minimumInputLength: 1,
                ajax: {
                    url: __protocoloIndex.options.url.acessoPessoas,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {nome: query, agruparPessoas: true};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.id + '';
                            el.text = el.pes_nome;

                            return el;
                        });

                        transformed.unshift({id: 0, text: ' (Atribuir a mim) '});

                        return {results: transformed};
                    }
                }
            });

            $('#protocolo-responsavel-form').validate({
                submitHandler: function () {
                    var $form = $('#protocolo-responsavel-form');
                    var $modal = $('#protocolo-responsavel-modal');
                    var arrDados = $form.serializeJSON();
                    arrDados['local'] = 'responsavel';
                    arrDados['ajax'] = true;
                    arrDados['protocoloId'] = arrDados['protocoloId'] || parseInt($('.procotoloId').text());

                    var ok = arrDados['usuarioIdResponsavel'] || false;

                    if (!ok) {
                        __protocoloIndex.showNotificacaoDanger('Escolha um responsável!');
                        $modal.modal('hide');
                        return false;
                    }

                    __protocoloIndex.addOverlay($form, 'Aguarde... ');

                    $.ajax({
                        url: $form.attr('action'),
                        type: 'POST',
                        dataType: 'json',
                        data: arrDados,
                        success: function (data) {
                            if (!data.error) {
                                __protocoloIndex.showNotificacaoSuccess('Responsável Alterado com Sucesso!');

                                if (arrDados['acao-origem'] && arrDados['acao-origem'] == 'editar') {
                                    $.protocoloEdit().carregarProtocolo(data, true);
                                }
                            } else {
                                __protocoloIndex.showNotificacaoWarning(
                                    data['message'] || 'Não foi possível concluir a operação'
                                );
                            }

                            $.protocoloIndex().reloadDataTableProtocolo();
                            __protocoloIndex.removeOverlay($form);
                            $form.data('ajax', false);
                            $modal.modal('hide');
                        },
                        error: function (data) {
                            __protocoloIndex.showNotificacaoDanger(
                                "Não foi possível efetuar a operação. Atualize a tela e tente novamente."
                            );
                            __protocoloIndex.removeOverlay($form);
                        }
                    });
                },
                ignore: '.ignore',
                rules: {
                    usuarioIdResponsavel: {required: true}
                },
                messages: {
                    usuarioIdResponsavel: {required: 'Campo obrigatório!'}
                }
            });
        };

        this.iniciarFiltros = function () {
            var $solicitacao = $('#solicitacaoFiltro'),
                $situacao = $('#situacaoFiltro'),
                $setor = $('#setorFiltro'),
                $solicitanteInterno = $('#protocoloSolicitanteFiltroInterno'),
                $solicitanteExterno = $('#protocoloSolicitanteFiltroOutros'),
                $pesquisaAluno = $('#protocoloSolicitanteAluno');

            $situacao.select2({
                language: 'pt-BR',
                multiple: true,
                data: __protocoloIndex.options.data.arrProtocoloSituacao
            });

            $solicitacao.select2({
                language: 'pt-BR',
                allowClear: true,
                multiple: true,
                ajax: {
                    url: __protocoloIndex.options.url.pesquisaSolicitacao,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {
                            query: query,
                            setorId: $setor.val()
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            var text = el.setor_descricao + ' / ' + el.solicitacao_descricao;
                            return {text: text, id: el.solicitacao_id};
                        });
                        return {results: transformed};
                    }
                }
            });

            $setor.select2({
                language: 'pt-BR',
                allowClear: true,
                multiple: true,
                ajax: {
                    url: __protocoloIndex.options.url.pesquisaSetor,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {
                            query: query,
                            solicitante: true
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.setor_descricao, id: el.setor_id};
                        });

                        return {results: transformed};
                    }
                }
            }).change(function () {
                $solicitacao.select2('data', '').trigger('change');
            });

            $solicitanteInterno.select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __protocoloIndex.options.url.pessoa,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {
                            query: query, agruparAluno: true
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.pes_id;
                            el.text = el.pes_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $pesquisaAluno.select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __protocoloIndex.options.url.pesquisaAluno,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {
                            aluno: true,
                            query: query
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {id: el.aluno_id, text: el.pesNome};
                        });

                        return {results: transformed};
                    }
                }
            });

            $solicitanteExterno.select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __protocoloIndex.options.url.protocolo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query, solicitante: 'externo'};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.protocolo_solicitante, id: el.id};
                        });

                        return {results: transformed};
                    }
                }
            });

            $('#operadoresCadastroFiltro, #operadoresAlteracaoFiltro, #protocoloResponsavel').select2({
                language: 'pt-BR',
                allowClear: true,
                multiple: true,
                minimumInputLength: 1,
                ajax: {
                    url: __protocoloIndex.options.url.acessoPessoas,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {
                            query: query,
                            agruparPorUsuario: true
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.login + ' | ' + el.pes_nome, id: el.id};
                        });

                        return {results: transformed};
                    }
                }
            });

            $(document).on(
                'change select2-selected select2-clearing select2-removed', 'input, select',
                function () {
                    var $item = $(this);

                    if ($item.val() != '') {
                        $item.parent().find('.select2-container').addClass('select2-allowclear');
                    } else {
                        $item.parent().find('.select2-container').removeClass('select2-allowclear');
                    }
                }
            );

            __protocoloIndex.iniciarElementoDatePicker($("#dataCadastroInicial"));
            __protocoloIndex.iniciarElementoDatePicker($("#dataCadastroFinal"));
            __protocoloIndex.iniciarElementoDatePicker($("#dataAlteracaoInicial"));
            __protocoloIndex.iniciarElementoDatePicker($("#dataAlteracaoFinal"));

            $("#protocolosFiltroExecutar").click(function () {
                __protocoloIndex.options.values.filtroObrigatorio = 0;
                __protocoloIndex.reloadDataTableProtocolo();
            });

            $("#protocolosFiltroLimpar").click(function () {
                $('#solicitacaoFiltro').select2('val', '').trigger('change');
                $('#situacaoFiltro').select2('val', '').trigger('change');
                $('#setorFiltro').select2('val', '').trigger('change');
                $('#protocoloSolicitanteAluno').select2('val', '').trigger('change');
                $('#protocoloSolicitanteFiltroInterno').select2('val', '').trigger('change');
                $('#protocoloSolicitanteFiltroOutros').select2('val', '').trigger('change');
                $('#operadoresCadastroFiltro').select2('val', '').trigger('change');
                $('#operadoresAlteracaoFiltro').select2('val', '').trigger('change');
                $('#protocoloResponsavel').select2('val', '').trigger('change');
                $('#dataCadastroInicial').val('').trigger('change');
                $('#dataCadastroFinal').val('').trigger('change');
                $('#dataAlteracaoInicial').val('').trigger('change');
                $('#dataAlteracaoFinal').val('').trigger('change');
            });

            $("#acordion").click();
        };

        this.getQuantidadeArquivo = function () {
            return __protocoloIndex.options.values.quantidadeArquivo;
        };

        this.setQuantidadeArquivo = function (quantidadeArquivo) {
            __protocoloIndex.options.values.quantidadeArquivo = quantidadeArquivo;
        };

        this.limparCacheAnexos = function () {
            __protocoloIndex.setQuantidadeArquivo(0);
            $('#tabela-anexos-corpo').empty();
            $('#tabela-anexos').addClass('hidden');
        };

        this.setSteps = function () {
            var editar = __protocoloIndex.options.values.editar;
            var $dataTableProtocolo = $('#dataTableProtocolo');
            var colNum = -1;

            __protocoloIndex.options.datatables.protocolo = $dataTableProtocolo.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __protocoloIndex.options.url.search,
                    type: "POST",
                    data: function (data) {
                        __protocoloIndex.options.values.mensagemDataTable = 'Use os filtros para realizar a busca!';
                        __protocoloIndex.setarFiltrosDatatables(data);

                        data.filtroObrigatorio = __protocoloIndex.options.values.filtroObrigatorio;

                        if ((Object.keys(data.filter)).length > 0) {
                            __protocoloIndex.options.values.mensagemDataTable = 'Nenhum registro encontrado!';
                        }

                        return data;
                    },
                    dataSrc: function (json) {
                        var data = json.data;
                        for (var row in data) {
                            var protocoloAssinado = (parseInt(data[row]['usuario_id_responsavel']) ==
                            json['usuarioLogadoId']);

                            data[row]['colorir'] = json.usuarioLogadoId == data[row]['usuario_id_responsavel'];

                            var btns = [
                                {
                                    class: 'btn-primary item-manager',
                                    icon: 'fa-comment',
                                    title: 'Atendimento do protocolo'
                                }
                            ];

                            if (editar) {
                                btns.push(
                                    {
                                        class: 'btn-success item-transferencia',
                                        icon: 'fa-exchange',
                                        title: 'Transferir setor do protocolo'
                                    },
                                    {
                                        class: 'btn-warning item-responsavel',
                                        icon: (protocoloAssinado ? 'fa-bell' : 'fa-bell-o'),
                                        title: (protocoloAssinado ? 'Assinado' : 'Assinar')
                                    });
                            }

                            data[row]['acao'] = __protocoloIndex.createBtnGroup(btns);
                            data[row]['protocolo_data_cadastro_formatada'] = __protocoloIndex.formatDateTime(
                                data[row]['protocolo_data_cadastro']
                            );
                            data[row]['protocolo_data_alteracao_formatada'] = __protocoloIndex.formatDateTime(
                                data[row]['protocolo_data_alteracao']
                            );
                        }

                        __protocoloIndex.removeOverlay($('#container-protocolo'));
                        return data;
                    }
                },
                columns: [
                    {name: "protocolo_id", targets: ++colNum, data: "protocoloIdFormatado"},
                    {name: "setor_descricao", targets: ++colNum, data: "setor_descricao"},
                    {name: "solicitacao_descricao", targets: ++colNum, data: "solicitacao_descricao"},
                    {name: "protocolo_assunto", targets: ++colNum, data: "protocolo_assunto"},
                    {name: "nome_responsavel", targets: ++colNum, data: "nome_responsavel"},
                    {name: "protocolo_solicitante", targets: ++colNum, data: "protocolo_solicitante"},
                    {name: "protocolo_data_cadastro", targets: ++colNum, data: "protocolo_data_cadastro_formatada"},
                    {name: "protocolo_data_alteracao", targets: ++colNum, data: "protocolo_data_alteracao_formatada"},
                    {name: "protocolo_situacao", targets: ++colNum, data: "protocolo_situacao"},
                    {name: "protocolo_id", targets: ++colNum, data: "acao"}
                ],
                columnDefs: [
                    {
                        "targets": 0,
                        "createdCell": function (td, cellData, rowData) {
                        }
                    }
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: __protocoloIndex.options.values.mensagemDataTable,
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                "fnDrawCallback": function (oSettings) {
                    $(".dataTables_empty").text(__protocoloIndex.options.values.mensagemDataTable)
                },
                order: [[0, 'desc']]
            });

            $dataTableProtocolo.on('click', '.item-manager', function (e) {
                var $pai = $(this).closest('tr');
                var data = __protocoloIndex.options.datatables.protocolo.fnGetData($pai);

                $.protocoloEdit().setSystemMessageText(
                    "Somente o responsável ou o criador tem privilégios suficientes para enviar mensagens"
                );

                $.protocoloEdit().carregarProtocolo(data);
                e.preventDefault();
                e.stopPropagation();
            });

            $dataTableProtocolo.on('click', '.item-responsavel', function (e) {
                var $pai = $(this).closest('tr');
                var data = __protocoloIndex.options.datatables.protocolo.fnGetData($pai);

                __protocoloIndex.showModalResponsavel(data);
                e.preventDefault();
                e.stopPropagation();
            });

            $dataTableProtocolo.on('click', '.item-transferencia', function (e) {
                var $pai = $(this).closest('tr');
                var data = __protocoloIndex.options.datatables.protocolo.fnGetData($pai);

                __protocoloIndex.showModalTransferencia(data);
                e.preventDefault();
                e.stopPropagation();
            });

            $('#btn-protocolo-add').click(function (e) {
                __protocoloIndex.limparCacheAnexos();
                if (__protocoloIndex.options.ajax) {
                    __protocoloIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });


            $('#protocoloSolicitantePes').on('change', function (e) {
                if ($(this).val() == __protocoloIndex.getUsuarioLogado()) {
                    $('#protocoloSolicitanteVisivel').select2('disable');
                } else {
                    $('#protocoloSolicitanteVisivel').select2('enable');
                }
            });

            $('#btn-anexar-add').on('click', function (e) {
                var $element = $('.divAnexoInput').clone();
                var $input = $element.find('input');
                var $btn = $element.find('button');
                var $label = $element.find('.nomeArquivo');
                var id = __protocoloIndex.createGUID();

                $element.removeClass('divAnexoInput');
                $element.addClass(id);

                $label.attr('id', id);
                $input.attr('id', id);
                $btn.attr('id', id);

                $input.trigger('click');

                $input.change(function () {
                    $label.text(
                        this.files[0].name
                    );

                    $element.appendTo('#arrMensagemAnexos-add');

                    var row = "<tr id=" + id + ">";
                    row += "<th scope='row'>" + this.files[0].name + "</th>";
                    row += "<th scope='row'>" + $btn[0].outerHTML + "</th>";
                    row += "</tr>";

                    $('#tabela-anexos-corpo').append(row);
                    $('#tabela-anexos').removeClass('hidden');

                    __protocoloIndex.setQuantidadeArquivo(__protocoloIndex.getQuantidadeArquivo() + 1);
                });
            });

            $('.fechar-janela-protocolo-add').on('click', function () {
                $.protocoloEdit().exibeMensagemDescartarAnexos(__protocoloIndex.getQuantidadeArquivo());
            });

            $(document).on('click', '.btn-excluir-arquivo', function () {
                $('#' + $(this).attr('id')).remove();
                __protocoloIndex.setQuantidadeArquivo(__protocoloIndex.getQuantidadeArquivo() - 1);
            });

            $dataTableProtocolo.on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __protocoloIndex.options.datatables.protocolo.fnGetData($pai);
                var arrRemover = {protocoloId: data['protocolo_id']};

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de protocolo "' + data['protocolo_solicitante_nome'] +
                    '"?',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __protocoloIndex.addOverlay(
                                $('#container-protocolo'), 'Aguarde, solicitando remoção de registro de protocolo...'
                            );
                            $.ajax({
                                url: __protocoloIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __protocoloIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de protocolo:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __protocoloIndex.removeOverlay($('#container-protocolo'));
                                    } else {
                                        __protocoloIndex.reloadDataTableProtocolo();
                                        __protocoloIndex.showNotificacaoSuccess(
                                            "Registro de protocolo removido!"
                                        );
                                    }
                                },
                                error: function (data) {
                                    __protocoloIndex.showNotificacaoDanger(
                                        "Não foi possível efetuar a operação. Atualize a tela e tente novamente."
                                    );
                                    __protocoloIndex.removeOverlay($('#container-protocolo'));
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        $('#protocolo-transferencia-form').validate({
            submitHandler: function () {
                var $form = $('#protocolo-transferencia-form'),
                    $modal = $('#protocolo-transferencia-modal'),
                    $formInfo = $('#form-info');
                var arrDados = $form.serializeJSON();


                __protocoloIndex.addOverlay(
                    $modal, 'Aguarde, solicitando transferência de protocolo...'
                );

                $.ajax({
                    url: __protocoloIndex.options.url.transferencia,
                    type: 'POST',
                    dataType: 'json',
                    data: arrDados,
                    success: function (data) {
                        if (data.erro) {
                            __protocoloIndex.showNotificacaoDanger(
                                "Não foi possível transferir protocolo:\n" +
                                "<i>" + data['erroDescricao'] + "</i>"
                            );
                        } else {
                            var protocolo = data['protocolo'] || {};

                            if (protocolo) {
                                __protocoloIndex.options.values.protocolo = protocolo;

                                $formInfo.find('[data-info=protocoloSetor]').html(protocolo['setorDescricao']);
                                $formInfo.find('[data-info=solicitacao]').html(protocolo['solicitacaoDescricao']);
                            }

                            __protocoloIndex.reloadDataTableProtocolo();
                            __protocoloIndex.showNotificacaoSuccess(
                                "Protocolo transferido com sucesso! " +
                                "<br>" + "Protocolo: " + arrDados['protocoloId'] +
                                "<br>" + "Setor: " + (protocolo['setorDescricao'] || '-') +
                                "<br>" + "Solicitação: " + (protocolo['solicitacaoDescricao'] || '-')
                            );
                            $modal.modal('hide');
                        }

                        __protocoloIndex.removeOverlay($modal);
                    },
                    error: function (data) {
                        __protocoloIndex.showNotificacaoDanger(
                            "Não foi possível efetuar a operação. Atualize a tela e tente novamente."
                        );
                        __protocoloIndex.removeOverlay($modal);
                    }
                });

                return false;
            },
            ignore: '.ignore',
            rules: {
                setorId: {required: true},
                solicitacao: {required: true}
            },
            messages: {
                setorId: {required: 'Campo obrigatório!'},
                solicitacao: {required: 'Campo obrigatório!'}
            }

        });

        this.setarFiltrosDatatables = function (data) {
            var editar = __protocoloIndex.options.values.editar;

            data.filter = data.filter || {};
            data.index = true;

            if (editar) {
                var solicitacao = $('#solicitacaoFiltro').val() || "";
                var situacao = $('#situacaoFiltro').val() || "";
                var setor = $('#setorFiltro').val() || "";
                var protocoloSolicitanteAluno = $('#protocoloSolicitanteAluno').val() || "";
                var protocoloSolicitanteInterno = $('#protocoloSolicitanteFiltroInterno').val() || "";
                var protocoloSolicitanteOutros = $('#protocoloSolicitanteFiltroOutros').val() || "";
                var operadoresCadastro = $('#operadoresCadastroFiltro').val() || "";
                var operadoresAlteracao = $('#operadoresAlteracaoFiltro').val() || "";
                var responsavel = $('#protocoloResponsavel').val() || "";
                var dataAlteracaoFinal = $('#dataAlteracaoFinal').val() || "";
                var dataCadastroFinal = $('#dataCadastroFinal').val() || "";
                var dataAlteracaoInicial = $('#dataAlteracaoInicial').val() || "";
                var dataCadastroInicial = $('#dataCadastroInicial').val() || "";

                if (solicitacao) {
                    data.filter['solicitacao'] = solicitacao;
                }

                if (situacao) {
                    data.filter['situacao'] = situacao;
                }

                if (setor) {
                    data.filter['setor'] = setor;
                }

                if (protocoloSolicitanteAluno) {
                    data.filter['protocoloSolicitanteAluno'] = protocoloSolicitanteAluno;
                }

                if (protocoloSolicitanteInterno) {
                    data.filter['protocoloSolicitanteInterno'] = protocoloSolicitanteInterno;
                }
                if (protocoloSolicitanteOutros) {
                    data.filter['protocoloSolicitanteOutros'] = protocoloSolicitanteOutros;
                }

                if (operadoresCadastro) {
                    data.filter['operadoresCadastro'] = operadoresCadastro;
                }

                if (operadoresAlteracao) {
                    data.filter['operadoresAlteracao'] = operadoresAlteracao;
                }
                if (responsavel) {
                    data.filter['responsavel'] = responsavel;
                }

                if (dataAlteracaoFinal) {
                    data.filter['dataAlteracaoFinal'] = dataAlteracaoFinal;
                }

                if (dataCadastroFinal) {
                    data.filter['dataCadastroFinal'] = dataCadastroFinal;
                }

                if (dataAlteracaoInicial) {
                    data.filter['dataAlteracaoInicial'] = dataAlteracaoInicial;
                }

                if (dataCadastroInicial) {
                    data.filter['dataCadastroInicial'] = dataCadastroInicial;
                }

            } else {
                data.filter['filtrarPorUsuarioLogado'] = true;
            }
            return data;
        };

        this.showModalResponsavel = function (data) {
            data = data || {};
            __protocoloIndex.setarResponsavel(data);

            var $modal = $('#protocolo-responsavel-modal');
            $modal.modal('show');
        };

        this.setarResponsavel = function (data) {
            data = data || {};
            var objResponsavel = null,
                $modal = $('#protocolo-responsavel-modal'),
                $form = $('#protocolo-responsavel-form');

            data = $.extend(true, $.extend(true, {}, data), __protocoloIndex.keysToCamelCase(data));

            $form[0].reset();
            $form.deserialize(data);

            if (data['usuario_id_responsavel']) {
                objResponsavel = {id: data['usuarioIdResponsavel'], text: data['nomeResponsavel']};
            }

            $('#usuarioIdResponsavel').select2('data', objResponsavel).trigger('change');

            $modal.find('.protocoloIdTitulo').html(
                data['protocoloId'] ? (__protocoloIndex.numPad(data['protocoloId'], 10, '0')) : '-'
            );

            $form.find('[name="protocoloId"]').val(data['protocoloId']);
        };

        this.setarDadosTransferencia = function (data) {
            data = data || {};
            var $form = $('#protocolo-transferencia-form'),
                $modal = $('#protocolo-transferencia-modal'),
                actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            var protocoloAtualizado = __protocoloIndex.options.values.protocolo || false;

            data = $.extend(true, $.extend(true, {}, data), __protocoloIndex.keysToCamelCase(data));

            var actionTitle = 'Transferir';
            actionForm.push(data['protocoloId']);

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);

            $form[0].reset();
            $form.deserialize(data);

            var setor = null;
            var solicitacao = null;

            if (data['setorDescricao']) {
                setor = {id: data['setorId'], text: data['setorDescricao']};
            }

            if (data['solicitacaoDescricao']) {
                solicitacao = {id: data['solicitacaoId'], text: data['solicitacaoDescricao']};
            }

            if (protocoloAtualizado) {
                if (protocoloAtualizado['setorDescricao']) {
                    setor = {
                        id: protocoloAtualizado['setorId'], text: protocoloAtualizado['setorDescricao']
                    };
                }

                if (protocoloAtualizado['solicitacaoDescricao']) {
                    solicitacao = {
                        id: protocoloAtualizado['solicitacaoId'], text: protocoloAtualizado['solicitacaoDescricao']
                    };
                }
            }

            $modal.find('.protocoloIdTitulo').html(data['protocoloId']);

            var $setor = $form.find("[name=setorId]");
            var $solicitacao = $form.find("[name=solicitacao]");

            $setor.select2('data', setor).trigger("change");
            $solicitacao.select2('data', solicitacao).trigger("change");
            $form.find('[name=protocoloId]').val(data['protocoloId'] || '');
            $form.find('.protocoloId').html(data['protocoloId'] || '-');
            $form.valid();

            $modal.find('.modal-title').html(actionTitle + ' protocolo');
        };

        this.showModalTransferencia = function (data) {
            data = data || {};
            var $modal = $('#protocolo-transferencia-modal');

            __protocoloIndex.setarDadosTransferencia(data);

            $modal.modal('show');
            __protocoloIndex.removeOverlay($('#container-protocolo'));
        };

        this.iniciarCamposTransferencia = function () {
            var $form = $('#protocolo-transferencia-form');
            var $solicitacao = $form.find('[name=solicitacao]'),
                $setor = $form.find('[name=setorId]');

            $setor.select2({
                language: 'pt-BR',
                allowClear: true,
                ajax: {
                    url: __protocoloIndex.options.url.pesquisaSetor,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.setor_descricao, id: el.setor_id};
                        });

                        return {results: transformed};
                    }
                }
            }).on('change', function () {
                var input = $(this).val();
                if (input != '') {
                    $solicitacao.select2("enable");
                } else {
                    $solicitacao.select2("disable");
                }

                $solicitacao.select2("val", "");
            });

            $solicitacao.select2({
                language: 'pt-BR',
                allowClear: true,
                ajax: {
                    url: __protocoloIndex.options.url.pesquisaSolicitacao,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {
                            setorId: $setor.val(),
                            query: query
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.solicitacao_descricao, id: el.solicitacao_id};
                        });
                        return {results: transformed};
                    }
                }
            });
        };

        this.setSolicitacao = function (solicitacao) {
            this.options.value.solicitacao = solicitacao || null;
        };

        this.getSolicitacao = function () {
            return this.options.value.solicitacao || null;
        };

        this.getProtocoloSetor = function () {
            return this.options.value.setor || null;
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#protocolo-form'),
                $modal = $('#protocolo-modal'),
                $protocoloMensagem = $('#protocoloMensagem');

            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            $protocoloMensagem.val('');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['protocoloId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['protocoloId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);

            $form[0].reset();

            $form.deserialize(data);

            $('#solicitacao-pessoa').prop('checked', true).trigger('change');

            var $solicitacao = $("#solicitacao");
            $solicitacao.select2('data', data['solicitacao']).trigger("change");

            var $usuarioIdCriacao = $("#usuarioIdCriacao");
            $usuarioIdCriacao.select2('data', data['usuarioIdCriacao']).trigger("change");

            var $usuarioIdResponsavel = $("#usuarioIdResponsavel");
            $usuarioIdResponsavel.select2('data', data['usuarioIdResponsavel']).trigger("change");

            var $protocoloSolicitantePes = $("#protocoloSolicitantePes");
            $protocoloSolicitantePes.select2('data', data['protocoloSolicitantePes']).trigger("change");

            var $protocoloSolicitanteAlunocurso = $("#protocoloSolicitanteAlunocurso");
            $protocoloSolicitanteAlunocurso.select2('data', data['protocoloSolicitanteAlunocurso']).trigger("change");

            var $protocoloSolicitanteVisivel = $("#protocoloSolicitanteVisivel");
            $protocoloSolicitanteVisivel.select2('val', data['protocoloSolicitanteVisivel'] || "Sim").trigger("change");

            var $protocoloSituacao = $("#protocoloSituacao");
            $protocoloSituacao.select2('val', data['protocoloSituacao']);

            var objUsuarioLogado = __protocoloIndex.getUsuarioLogado();

            if (objUsuarioLogado && __protocoloIndex.filtrarSolicitante()) {
                $protocoloSolicitantePes.select2('data', objUsuarioLogado).trigger("change");
                $protocoloSolicitanteVisivel.select2('disable');
            }

            $modal.find('.modal-title').html(actionTitle + ' protocolo');
            $modal.modal('show');
            __protocoloIndex.removeOverlay($('#container-protocolo'));
        };

        __protocoloIndex.reloadDataTableProtocolo = function () {
            __protocoloIndex.getDataTableProtocolo().api().ajax.reload(null, false);
        };

        this.getDataTableProtocolo = function () {
            if (!__protocoloIndex.options.datatables.protocolo) {
                if (!$.fn.dataTable.isDataTable('#dataTableProtocolo')) {
                    __protocoloIndex.options.datatables.protocolo = $('#dataTableProtocolo').DataTable();
                } else {
                    __protocoloIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }
            return __protocoloIndex.options.datatables.protocolo;
        };

        this.getUsuarioLogado = function () {
            return __protocoloIndex.options.data.arrUsuarioLogado;
        };

        this.filtrarSolicitante = function () {
            return __protocoloIndex.options.filtrarSolicitante;
        };

        this.run = function (opts) {
            __protocoloIndex.setDefaults(opts);
            __protocoloIndex.iniciarCamposResponsavel();
            __protocoloIndex.iniciarFiltros();
            __protocoloIndex.iniciarCamposTransferencia();
            __protocoloIndex.setSteps();
        };
    };

    $.protocoloIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.protocolo.protocolo.index");

        if (!obj) {
            obj = new ProtocoloIndex();
            obj.run(params);
            $(window).data('universa.protocolo.protocolo.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);