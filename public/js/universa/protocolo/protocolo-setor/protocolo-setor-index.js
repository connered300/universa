(function ($, window, document) {
    'use strict';
    var ProtocoloSetorIndex = function () {
        VersaShared.call(this);
        var __protocoloSetorIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                protocoloSetor: null
            }
        };

        this.setSteps = function () {
            var $dataTableProtocoloSetor = $('#dataTableProtocoloSetor');
            var colNum = -1;
            __protocoloSetorIndex.options.datatables.protocoloSetor = $dataTableProtocoloSetor.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __protocoloSetorIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [
                            {class: 'btn-primary item-edit', icon: 'fa-pencil'},
                            {class: 'btn-danger item-remove', icon: 'fa-times'}
                        ];

                        for (var row in data) {
                            data[row]['acao'] = __protocoloSetorIndex.createBtnGroup(btns);
                        }

                        __protocoloSetorIndex.removeOverlay($('#container-protocolo-setor'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "setor_id", targets: ++colNum, data: "setor_id"},
                    {name: "setor_descricao", targets: ++colNum, data: "setor_descricao"},
                    {name: "setor_visibilidade", targets: ++colNum, data: "setor_visibilidade"},
                    {name: "setor_email", targets: ++colNum, data: "setor_email"},
                    {name: "pes_nome", targets: ++colNum, data: "pes_nome"},
                    {name: "setor_id", targets: ++colNum, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $dataTableProtocoloSetor.on('click', '.item-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __protocoloSetorIndex.options.datatables.protocoloSetor.fnGetData($pai);

                if (!__protocoloSetorIndex.options.ajax) {
                    location.href = __protocoloSetorIndex.options.url.edit + '/' + data['setor_id'];
                } else {
                    __protocoloSetorIndex.addOverlay(
                        $('#container-protocolo-setor'), 'Aguarde, carregando dados para edição...'
                    );
                    $.protocoloSetorAdd().pesquisaProtocoloSetor(
                        data['setor_id'],
                        function (data) {
                            __protocoloSetorIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-protocolo-setor-add').click(function (e) {
                $('#grupoId').select2('val', '');
                if (__protocoloSetorIndex.options.ajax) {
                    __protocoloSetorIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $dataTableProtocoloSetor.on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __protocoloSetorIndex.options.datatables.protocoloSetor.fnGetData($pai);
                var arrRemover = {
                    setorId: data['setor_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de setor "' + data['setor_descricao'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __protocoloSetorIndex.addOverlay(
                                $('#container-protocolo-setor'), 'Aguarde, solicitando remoção de registro de setor...'
                            );
                            $.ajax({
                                url: __protocoloSetorIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __protocoloSetorIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de setor:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __protocoloSetorIndex.removeOverlay($('#container-protocolo-setor'));
                                    } else {
                                        __protocoloSetorIndex.reloadDataTableProtocoloSetor();
                                        __protocoloSetorIndex.showNotificacaoSuccess(
                                            "Registro de setor removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#protocolo-setor-form'),
                $modal = $('#protocolo-setor-modal');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['setorId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['setorId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);


            var $setorVisibilidade = $("#setorVisibilidade");
            $setorVisibilidade.select2('data', data['setorVisibilidade']).trigger("change");

            var $pes = $("#pes");
            $pes.select2('data', data['pes']).trigger("change");

            var $grupo = $("#grupoId");
            $grupo.select2('data', data['grupo']).trigger("change");

            $modal.find('.modal-title').html(actionTitle + ' setor');
            $modal.modal();
            __protocoloSetorIndex.removeOverlay($('#container-protocolo-setor'));
        };

        this.reloadDataTableProtocoloSetor = function () {
            this.getDataTableProtocoloSetor().api().ajax.reload(null, false);
        };

        this.getDataTableProtocoloSetor = function () {
            if (!__protocoloSetorIndex.options.datatables.protocoloSetor) {
                if (!$.fn.dataTable.isDataTable('#dataTableProtocoloSetor')) {
                    __protocoloSetorIndex.options.datatables.protocoloSetor = $('#dataTableProtocoloSetor').DataTable();
                } else {
                    __protocoloSetorIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __protocoloSetorIndex.options.datatables.protocoloSetor;
        };

        this.run = function (opts) {
            __protocoloSetorIndex.setDefaults(opts);
            __protocoloSetorIndex.setSteps();
        };
    };

    $.protocoloSetorIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.protocolo.protocolo-setor.index");

        if (!obj) {
            obj = new ProtocoloSetorIndex();
            obj.run(params);
            $(window).data('universa.protocolo.protocolo-setor.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);