(function ($, window, document) {
    'use strict';

    var ProtocoloSetorAdd = function () {
        VersaShared.call(this);
        var __protocoloSetorAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                pessoa: '',
                grupo: ''
            },
            data: {},
            value: {
                pes: null,
                grupo: null,
                setorVisibilidade: null
            },
            datatables: {},
            wizardElement: '#protocolo-setor-wizard',
            formElement: '#protocolo-setor-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            var $setorVisibilidade = $("#setorVisibilidade");
            var $pes = $("#pes");
            var $grupo = $('#grupoId');

            $setorVisibilidade.select2({
                allowClear: true,
                multiple: true,
                language: 'pt-BR',
                data: __protocoloSetorAdd.options.value.setorVisibilidade
            });
            $setorVisibilidade.select2("data", __protocoloSetorAdd.getVisibilidade());

            $pes.select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __protocoloSetorAdd.options.url.pessoa,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.pes_id;
                            el.text = el.pes_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $pes.select2("data", __protocoloSetorAdd.getPes());

            $grupo.select2({
                ajax: {
                    url: __protocoloSetorAdd.options.url.grupo,
                    dataType: 'json',
                    data: function (q) {
                        return {query: q};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {
                                text: el.grup_nome,
                                id: el.id
                            };
                        });

                        return {results: transformed};
                    }
                },
                multiple: true
            });

            $grupo.select2('data', __protocoloSetorAdd.getGrupo());
        };

        this.setPes = function (pes) {
            this.options.value.pes = pes || null;
        };

        this.getPes = function () {
            return this.options.value.pes || null;
        };

        this.getVisibilidade = function () {
            return this.options.value.setorVisibilidade || null;
        };

        this.getGrupo = function () {
            return this.options.value.grupo || null;
        };

        this.setGrupo = function (grupo) {
            this.options.value.grupo = grupo || null;
        };

        this.setValidations = function () {
            __protocoloSetorAdd.options.validator.settings.rules = {
                setorDescricao: {maxlength: 200, required: true},
                setorVisibilidade: {required: true},
                setorEmail: {maxlength: 100},
                pes: {number: true, required: true}
            };
            __protocoloSetorAdd.options.validator.settings.messages = {
                setorDescricao: {maxlength: 'Tamanho máximo: 200!', required: 'Campo obrigatório!'},
                setorVisibilidade: {required: 'Campo obrigatório!'},
                setorEmail: {maxlength: 'Tamanho máximo: 100!'},
                pes: {number: 'Número inválido!', required: 'Campo obrigatório!'}
            };

            $(__protocoloSetorAdd.options.formElement).submit(function (e) {
                var ok = $(__protocoloSetorAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__protocoloSetorAdd.options.formElement);

                if (__protocoloSetorAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __protocoloSetorAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __protocoloSetorAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__protocoloSetorAdd.options.listagem) {
                                    $.protocoloSetorIndex().reloadDataTableProtocoloSetor();
                                    __protocoloSetorAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#protocolo-setor-modal').modal('hide');
                                }

                                __protocoloSetorAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaProtocoloSetor = function (setor_id, callback) {
            var $form = $(__protocoloSetorAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __protocoloSetorAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + setor_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __protocoloSetorAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __protocoloSetorAdd.removeOverlay($form);
                },
                erro: function () {
                    __protocoloSetorAdd.showNotificacaoDanger('Erro desconhecido!');
                    __protocoloSetorAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.protocoloSetorAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.protocolo.protocolo-setor.add");

        if (!obj) {
            obj = new ProtocoloSetorAdd();
            obj.run(params);
            $(window).data('universa.protocolo.protocolo-setor.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);