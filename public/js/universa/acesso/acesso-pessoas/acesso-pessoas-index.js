(function ($, window, document) {
    'use strict';
    var AcessoPessoasIndex = function () {
        VersaShared.call(this);
        var __acessoPessoasIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                acessoPessoas: null
            },
        };

        this.setSteps = function () {
            var colNum = 0;
            __acessoPessoasIndex.options.datatables.acessoPessoas = $('#dataTableAcessoPessoas').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __acessoPessoasIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;

                        for (var row in data) {
                            var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary acessoPessoas-edit" data-usuario="' +
                                           data[row]['id'] + '">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger acessoPessoas-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';
                            data[row]['acao'] = btnGroup;

                            if(data[row]['usr_status'] == "Inativo"){
                                data[row]['usr_status'] = "<span class='text-danger'  style='font-weight: bolder'>"+data[row]['usr_status']+"</span>"
                            }else{
                                data[row]['usr_status'] = "<span class='text-success' style='font-weight: bolder'>"+data[row]['usr_status']+"</span>"
                            }
                        }

                        __acessoPessoasIndex.removeOverlay($('#container-acesso-pessoas'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "id", targets: colNum++, data: "id"},
                    {name: "pes_nome", targets: colNum++, data: "pes_nome"},
                    {name: "login", targets: colNum++, data: "login"},
                    {name: "grup_nome", targets: colNum++, data: "grup_nome"},
                    {name: "email", targets: colNum++, data: "email"},
                    {name: "usr_status", targets: colNum++, data: "usr_status"},
                    {name: "id", targets: colNum++, data: "acao"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $('#dataTableAcessoPessoas').on('click', '.acessoPessoas-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __acessoPessoasIndex.options.datatables.acessoPessoas.fnGetData($pai);
                if (!__acessoPessoasIndex.options.ajax) {
                    location.href = __acessoPessoasIndex.options.url.edit + '/' + data['id'];
                } else {
                    var id = data['id'] | $('.acessoPessoas-edit').find('data-usuario').val();
                    $.acessoPessoasAdd().pesquisaAcessoPessoas(
                        id,
                        function (data) {
                            __acessoPessoasIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-acesso-pessoas-add').click(function (e) {
                if (__acessoPessoasIndex.options.ajax) {
                    __acessoPessoasIndex.showModal();
                    $('#loginMsg, .help-block').hide();
                    $('.has-error').removeClass('has-error');
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#dataTableAcessoPessoas').on('click', '.acessoPessoas-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __acessoPessoasIndex.options.datatables.acessoPessoas.fnGetData($pai);
                var arrRemover = {
                    id: data['id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de usuário "' + data['login'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __acessoPessoasIndex.addOverlay(
                                $('#container-acesso-pessoas'), 'Aguarde, solicitando remoção de registro de pessoas...'
                            );
                            $.ajax({
                                url: __acessoPessoasIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __acessoPessoasIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de pessoas:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __acessoPessoasIndex.removeOverlay($('#container-acesso-pessoas'));
                                    } else {
                                        __acessoPessoasIndex.reloadDataTableAcessoPessoas();
                                        __acessoPessoasIndex.showNotificacaoSuccess(
                                            "Registro de pessoas removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#acesso-pessoas-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['id']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['id'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);

            var pesFisica = $("#pesFisica");
            var pesJuridica = $("#pesJuridica");
            var campusCurso = $("#campusCurso");

            $form[0].reset();
            $form.deserialize(data);

            pesFisica.select2('data', data['pesFisica']).trigger("change");
            pesJuridica.select2('data', data['pesJuridica']).trigger("change");
            campusCurso.select2('data', data['campusCurso']).trigger("change");
            $("#acessoGrupo").select2('data', data['acessoGrupo']).trigger("change");
            $("#usrStatus").select2('val', data['usrStatus']).trigger("change");

            $('#acesso-pessoas-modal .modal-title').html(actionTitle + ' pessoas');
            $('#acesso-pessoas-modal').modal();
            __acessoPessoasIndex.removeOverlay($('#container-acesso-pessoas'));
        };

        this.reloadDataTableAcessoPessoas = function () {
            this.getDataTableAcessoPessoas().api().ajax.reload(null, false);
        };

        this.getDataTableAcessoPessoas = function () {
            if (!__acessoPessoasIndex.options.datatables.acessoPessoas) {
                if (!$.fn.dataTable.isDataTable('#dataTableAcessoPessoas')) {
                    __acessoPessoasIndex.options.datatables.acessoPessoas = $('#dataTableAcessoPessoas').DataTable();
                } else {
                    __acessoPessoasIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __acessoPessoasIndex.options.datatables.acessoPessoas;
        };

        this.run = function (opts) {
            __acessoPessoasIndex.setDefaults(opts);
            __acessoPessoasIndex.setSteps();
        };
    };

    $.acessoPessoasIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.acesso.acesso-pessoas.index");

        if (!obj) {
            obj = new AcessoPessoasIndex();
            obj.run(params);
            $(window).data('universa.acesso.acesso-pessoas.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);