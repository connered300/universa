(function ($, window, document) {
    'use strict';

    var AcessoPessoasAdd = function () {
        VersaShared.call(this);
        var __acessoPessoasAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                acessoUsuarios: '',
                pessoaFisica: '',
                pessoaJuridica: '',
                grupo: '',
                campusCurso: '',
                addPessoa: '',
                addPessoaJuridica: '',
                testeLogin: ''
            },
            data: {
                grupo: null
            },
            value: {
                usuario: null,
                pesFisica: null,
                pesJuridica: null,
                grupo: null
            },
            datatables: {},
            wizardElement: '#acesso-pessoas-wizard',
            formElement: '#acesso-pessoas-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            var acessoGrupo = $('#acessoGrupo');
            var $campusCurso = $('#campusCurso');
            var btnView = $('#viewPassword');
            var campos = $('#passwordCheck, #senha');

            $('#passwordCheck').change(function () {
                if ($(this).val() != '') {
                    btnView.prop('disabled', false);
                } else {
                    btnView.prop('disabled', true);
                }
            });

            btnView.hover(function () {
                campos.prop('type', 'text');
            }, function () {
                campos.prop('type', 'password');
            });

            $('#usrStatus').select2({
                data: [
                    {
                        id: 'Inativo',
                        text: 'Inativo'
                    },
                    {
                        id: 'Ativo',
                        text: 'Ativo'
                    }
                ]
            });

            acessoGrupo.select2({
                language: 'pt-BR',
                allowClear: true,
                multiple: true,
                ajax: {
                    url: __acessoPessoasAdd.options.url.grupo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query}
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.grup_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                },
                initSelection: function (element, callback) {
                    return callback(__acessoPessoasAdd.options.data.grupo);
                }
            });

            $campusCurso.select2({
                language: 'pt-BR',
                allowClear: true,
                multiple: true,
                ajax: {
                    url: __acessoPessoasAdd.options.url.campusCurso,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query, agruparPorCursocampus: true}
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.camp_nome + '/' + el.curso_nome;
                            el.id = el.cursocampus_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                },
                initSelection: function (element, callback) {
                    return callback(__acessoPessoasAdd.options.data.grupo);
                }
            });

            if (__acessoPessoasAdd.options.data.grupo) {
                acessoGrupo.select2('val', __acessoPessoasAdd.options.data.grupo);
            }

            $("#pesFisica").select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __acessoPessoasAdd.options.url.pessoaFisica,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.pes_id;
                            el.text = el.pes_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                },
                createSearchChoice: function (term) {
                    return {
                        id: term,
                        pesId: '',
                        pesNome: term,
                        text: term + ' (Criar novo registro)'
                    };
                }
            });

            $("#pesJuridica").select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __acessoPessoasAdd.options.url.pessoaJuridica,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.pes_id;
                            el.text = el.pes_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                },
                createSearchChoice: function (term) {
                    return {
                        id: term,
                        pesId: '',
                        pesNome: term,
                        text: term + ' (Criar novo registro)'
                    };
                }
            });

            $('#pesFisica').select2("data", __acessoPessoasAdd.getPesFisica());
            $('#usuario').select2("data", __acessoPessoasAdd.getUsuario());
            $('#pesJuridica').select2("data", __acessoPessoasAdd.getPesJuridica());

            //chama o evento de verificar login somente depois que o focus sai do campo de login
            $('#login').change(function () {
                var texto = $(this).val();
                var user = $("#id").val();
                var loginMsg = $('#loginMsg');
                loginMsg.hide();

                $('#enviarFormularioAddAcesso').attr('disabled', true);

                if (texto) {
                    $.ajax({
                        url: __acessoPessoasAdd.options.url.testeLogin,
                        method: 'POST',
                        data: {text: texto, id: user},
                        success: function (data) {
                            if (data.login) {
                                var login = data.login;
                                loginMsg.html("<span style='color: darkred'> " + login +
                                    " não está disponível !</span>");
                                $('#login').attr('data-check', false);
                                loginMsg.fadeIn();
                            } else {
                                loginMsg.html("<span style='color: darkgreen'> " + texto + " está disponível !</span>");
                                $('#login').attr('data-check', true);
                                $('#enviarFormularioAddAcesso').attr('disabled', false);
                                loginMsg.fadeIn();
                            }
                        }
                    })
                }
            });

            $('.editarPessoa').click(function (e) {
                var $elemento = $(this).parent().find('>input');
                var id = $elemento.attr('id');
                var data = $elemento.select2('data') || [];

                $('#pessoa-editar').modal('show');

                var pessoaAdd = $.pessoaAdd();
                pessoaAdd.setCallback('aposSalvar', function (dados, retorno) {
                    var text = [];

                    if (dados['pesCpf']) {
                        text.push(dados['pesCpf'])
                    }
                    if (dados['pesCnpj']) {
                        text.push(dados['pesCnpj'])
                    }
                    if (dados['pesNome']) {
                        text.push(dados['pesNome'])
                    }
                    if (dados['pesNomeFantasia']) {
                        text.push(dados['pesNomeFantasia'])
                    }

                    dados['id'] = dados['pesId'];
                    dados['text'] = text.join(' - ');

                    $elemento.select2('data', dados);

                    $('#pessoa-editar').modal('hide');
                });

                pessoaAdd.buscarDadosPessoa($elemento.val());

                $('#pessoa-editar [name=pesNome]').val(data['pesNome'] || '');

                e.preventDefault();
            });

            $('#pesJuridica, #pesFisica').change(function () {
                var data = $(this).select2('data') || [];

                if (Object.keys(data).length > 0) {
                    $(this).next('.editarPessoa').removeClass('hidden');

                    if (!$.isNumeric(data['id']) && data['pesNome']) {
                        $(this).next('.editarPessoa').click();
                    }
                } else {
                    $(this).next('.editarPessoa').addClass('hidden');
                }
            }).trigger('change');
        };

        this.setUsuario = function (usuario) {
            this.options.value.usuario = usuario || null;
        };

        this.getUsuario = function () {
            return this.options.value.usuario || null;
        };

        this.setPesFisica = function (pesFisica) {
            this.options.value.pesFisica = pesFisica || null;
        };

        this.getPesFisica = function () {
            return this.options.value.pesFisica || null;
        };

        this.setPesJuridica = function (pesJuridica) {
            this.options.value.pesJuridica = pesJuridica || null;
        };

        this.getPesJuridica = function () {
            return this.options.value.pesJuridica || null;
        };
        this.setValidations = function () {
            __acessoPessoasAdd.options.validator.settings.rules = {
                usuario: {number: true, required: true}, pesFisica: {number: true}, pesJuridica: {number: true},
                login: {maxlength: 45, required: true},
                senha: {
                    maxlength: 255, required: function () {
                        return ($('#id').val() == '');
                    }
                },
                usrStatus: {maxlength: 255, required: true},
                passwordCheck: {
                    maxlength: 255, required: function () {
                        return ($('#id').val() == '');
                    }, equalTo: '#senha'
                },
                email: {maxlength: 255}
            };
            __acessoPessoasAdd.options.validator.settings.messages = {
                usuario: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                pesFisica: {number: 'Número inválido!'}, pesJuridica: {number: 'Número inválido!'},
                login: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                senha: {maxlength: 'Tamanho máximo: 255!', required: 'Campo obrigatório!'},
                usrStatus: {required: 'Campo obrigatório!'},
                passwordCheck: {maxlength: 'Tamanho máximo: 255!', required: 'Campo obrigatório!'},
                email: {maxlength: 'Tamanho máximo: 255!'}
            };

            $(__acessoPessoasAdd.options.formElement).submit(function (e) {
                var ok = $(__acessoPessoasAdd.options.formElement).valid();
                var $form = $(__acessoPessoasAdd.options.formElement);

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                if (__acessoPessoasAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __acessoPessoasAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __acessoPessoasAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__acessoPessoasAdd.options.listagem) {
                                    $.acessoPessoasIndex().reloadDataTableAcessoPessoas();
                                    __acessoPessoasAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#acesso-pessoas-modal').modal('hide');
                                }

                                __acessoPessoasAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaAcessoPessoas = function (id, callback) {
            var $form = $(__acessoPessoasAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __acessoPessoasAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __acessoPessoasAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __acessoPessoasAdd.removeOverlay($form);
                },
                erro: function () {
                    __acessoPessoasAdd.showNotificacaoDanger('Erro desconhecido!');
                    __acessoPessoasAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.acessoPessoasAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.acesso.acesso-pessoas.add");

        if (!obj) {
            obj = new AcessoPessoasAdd();
            obj.run(params);
            $(window).data('universa.acesso.acesso-pessoas.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);