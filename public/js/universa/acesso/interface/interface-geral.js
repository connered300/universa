(function ($, window, document) {
    'use strict';

    var defaults = {
        url: {
            search: ''
        },
    };

    var acessoInterface = {
        options: defaults,
        setDefaults: function (opts) {
            opts = opts || [];

            acessoInterface.options = $.extend(defaults, opts);
        },
        dataTables: $('#dataTableAcesso').DataTable({
            "processing": true,
            "serverSide": true,
            "searching": false,
            "paging": false,
            "ajax": {
                "url": window.search,
                "type": "POST"
            },
            columnDefs: [
                {"name": "pes_nome", targets: 0},
                {"name": "nome", targets: 1, data: "nome"},
                {"name": "email", targets: 2, data: "email"},
                {"name": "login", targets: 3, data: "login"},
                {"name": "grupo", targets: 4, data: "grupo"},
                {"name": "status", targets: 5, data: "status"}
            ],
            order: [[0,'desc']],
            oLanguage: {
                sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                sLengthMenu: "Mostrar _MENU_ registros por página",

                sZeroRecords: "Nenhum registro encontrado",
                sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                sInfoFiltered: "(filtrado de _MAX_ registros)",
                sSearch: "",
                oPaginate: {
                    sFirst: "Início",
                    sPrevious: "Anterior",
                    sNext: "Próximo",
                    sLast: "Último"
                }
            }
        }),
        formatDateBrasileiro: function (element) {
            if (element == " - ") {
                return element;
            }
            var year = element.slice(0, 4);
            var month = element.slice(5, 7);
            var day = element.slice(8, 10);
            return (day + '/' + month + '/' + year);
        },
        showNotificacao: function (param) {
            var options = $.extend(
                true,
                {
                    content: '',
                    title: '',
                    type: 'info',
                    icon: '',
                    color: '',
                    timeout: false
                },
                param
            );

            switch (options.type) {
                case 'danger':
                    options.icon = 'fa-times-circle';
                    options.title = options.title || 'Atenção:';
                    options.color = '#a90329';
                    break;
                case 'info':
                    options.icon = 'fa-info-circle';
                    options.title = options.title || 'Informação:';
                    options.color = '#57889c';
                    break;
                case 'success':
                    options.icon = 'fa-check-circle';
                    options.title = options.title || 'Sucesso:';
                    options.color = '#739e73';
                    break;
                case 'warning':
                    options.icon = 'fa-exclamation-circle';
                    options.title = options.title || 'Atenção:';
                    options.color = '#c79121';
                    break;
            }

            var optionsNotification = {
                title: options.title,
                content: "<i>" + options.content + "</i>",
                color: options.color,
                icon: "fa " + options.icon
            };

            if (options.timeout) {
                optionsNotification.timeout = options.timeout;
            }

            $.smallBox(optionsNotification);
        },
        run: function (opts) {
            acessoInterface.setDefaults(opts);
        },
    };

    $.acessoInterface = function (params) {
        params = params || [];

        var obj = $(window).data("universa.acesso.interface");

        if (!obj) {
            obj = acessoInterface;
            obj.run(params);
            $(window).data('universa.acesso.interface', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);