(function ($, window, document) {
    'use strict';

    var EstagioperiodoAlunoPortfolioIndex = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlSearch: '',
                urlEdit: '',
                urlRemove: ''
            }
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);
        };

        priv.addOverlay = function (el, mensagem) {
            mensagem = mensagem || 'Aguarde...';
            el.append('<div class="app-overlay">' + mensagem + '</div>');
        };

        priv.removeOverlay = function (el) {
            el.find('>.app-overlay').remove();
        };

        priv.setSteps = function () {
            var colNum = 0;
            priv.dataTableEstagioperiodoAlunoPortfolio = $('#dataTableEstagioperiodoAlunoPortfolio').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: priv.options.urlSearch,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary estagioperiodoAlunoPortfolio-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger estagioperiodoAlunoPortfolio-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                        }

                        priv.removeOverlay($('.widget-body'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "portfolio_id", targets: colNum++, data: "portfolio_id"},
                    {name: "alunoper_id", targets: colNum++, data: "alunoper_id"},
                    {name: "portfolioaluno_data_entrega", targets: colNum++, data: "portfolioaluno_data_entrega"},
                    {name: "arq_id", targets: colNum++, data: "arq_id"},
                    {name: "portfolioaluno_pontuacao", targets: colNum++, data: "portfolioaluno_pontuacao"},
                    {name: "portfolioaluno_data_avaliacao", targets: colNum++, data: "portfolioaluno_data_avaliacao"},
                    {name: "portfolio_aluno_retorno", targets: colNum++, data: "portfolio_aluno_retorno"},
                    {name: "portfolioaluno_situacao", targets: colNum++, data: "portfolioaluno_situacao"},
                    {name: "acao", targets: colNum++, data: "portfolio_id"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $('#dataTableEstagioperiodoAlunoPortfolio').on('click', '.estagioperiodoAlunoPortfolio-edit',
                                                           function (event) {
                                                               var $pai = $(this).closest('tr');
                                                               var data = priv.dataTableEstagioperiodoAlunoPortfolio.fnGetData($pai);

                                                               location.href =
                                                                   priv.options.urlEdit + '/' + data['portfolio_id'];
                                                           });

            $('#dataTableEstagioperiodoAlunoPortfolio').on('click', '.estagioperiodoAlunoPortfolio-remove',
                                                           function (e) {
                                                               var $pai = $(this).closest('tr');
                                                               var data = priv.dataTableEstagioperiodoAlunoPortfolio.fnGetData($pai);
                                                               var arrRemover = {
                                                                   portfolioId: data['portfolio_id']
                                                               };

                                                               $.SmartMessageBox({
                                                                   title: "Confirme a operação:",
                                                                   content: 'Deseja realmente remover registro de estagioperiodo__aluno_portfolio "' +
                                                                            data['portfolioaluno_situacao'] + '"?',
                                                                   buttons: "[Não][Sim]",
                                                               }, function (ButtonPress) {
                                                                   if (ButtonPress == "Sim") {
                                                                       priv.addOverlay($('.widget-body'),
                                                                                       'Aguarde, solicitando remoção de registro de estagioperiodo__aluno_portfolio...');
                                                                       $.ajax({
                                                                           url: priv.options.urlRemove,
                                                                           type: 'POST',
                                                                           dataType: 'json',
                                                                           data: arrRemover,
                                                                           success: function (data) {
                                                                               if (data.erro) {
                                                                                   $.smallBox({
                                                                                       title: "Não foi possível remover registro de estagioperiodo__aluno_portfolio:",
                                                                                       content: "<i>" +
                                                                                                data['erroDescricao'] +
                                                                                                "</i>",
                                                                                       color: "#C46A69",
                                                                                       icon: "fa fa-times",
                                                                                       timeout: 10000
                                                                                   });

                                                                                   priv.removeOverlay($('.widget-body'));
                                                                               } else {
                                                                                   priv.dataTableEstagioperiodoAlunoPortfolio.api().ajax.reload(null, false);
                                                                                   $.smallBox({
                                                                                       title: "Sucesso!",
                                                                                       content: "<i>Registro de estagioperiodo__aluno_portfolio removido!</i>",
                                                                                       color: "#739e73",
                                                                                       icon: "fa fa-check-circle",
                                                                                       timeout: 10000
                                                                                   });
                                                                               }
                                                                           }
                                                                       });
                                                                   }
                                                               });

                                                               e.preventDefault();
                                                           });
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
        };
    };

    $.estagioperiodoAlunoPortfolioIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.atividade.estagioperiodo-aluno-portfolio.index");

        if (!obj) {
            obj = new EstagioperiodoAlunoPortfolioIndex();
            obj.run(params);
            $(window).data('universa.estagioperiodo-aluno-portfolio.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);