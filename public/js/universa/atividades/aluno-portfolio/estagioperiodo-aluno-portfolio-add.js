(function ($, window, document) {
    'use strict';

    var EstagioperiodoAlunoPortfolioAdd = function () {
        VersaShared.call(this);
        var __estagioperiodoAlunoPortfolioAdd = this;
        this.defaults = {
            url: {
                estagioperiodoAlunoNucleo: '',
                arquivo: ''
            },
            data: {},
            value: {
                alunoperId: null,
                arqId: null
            },
            datatables: {},
            wizardElement: '#estagioperiodo-aluno-portfolio-wizard',
            formElement: '#estagioperiodo-aluno-portfolio-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#alunoperId").select2({
                language: 'pt-BR',
                tags: true,
                ajax: {
                    url: __estagioperiodoAlunoPortfolioAdd.options.url.estagioperiodoAlunoNucleo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.alunonucleo_id, id: el.alunonucleo_id};
                        });

                        return {results: transformed};
                    }
                },
                allowClear: true,
                initSelection: function (element, callback) {
                    callback(__estagioperiodoAlunoPortfolioAdd.getAlunoperId());
                }
            });
            $('#alunoperId').select2("val", __estagioperiodoAlunoPortfolioAdd.getAlunoperId());
            $("#portfolioalunoDataEntrega")
                .datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                })
                .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});
            $("#arqId").select2({
                language: 'pt-BR',
                tags: true,
                ajax: {
                    url: __estagioperiodoAlunoPortfolioAdd.options.url.arquivo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.arq_id, id: el.arq_id};
                        });

                        return {results: transformed};
                    }
                },
                allowClear: true,
                initSelection: function (element, callback) {
                    callback(__estagioperiodoAlunoPortfolioAdd.getArqId());
                }
            });
            $('#arqId').select2("val", __estagioperiodoAlunoPortfolioAdd.getArqId());
            $("#portfolioalunoPontuacao").inputmask({showMaskOnHover: false, clearIncomplete: true, alias: 'integer'});
            $("#portfolioalunoDataAvaliacao")
                .datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                })
                .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});
            $("#portfolioalunoSituacao").select2({language: 'pt-BR'});
        };


        this.setAlunoperId = function (alunoperId) {
            this.options.value.AlunoperId = alunoperId || null;
        };

        this.getAlunoperId = function () {
            return priv.options.value.AlunoperId || null;
        };

        this.setArqId = function (arqId) {
            this.options.value.ArqId = arqId || null;
        };

        this.getArqId = function () {
            return priv.options.value.ArqId || null;
        };

        this.setValidations = function () {
            var submitHandler = function (form) {
                form.submit();
            };

            $(__estagioperiodoAlunoPortfolioAdd.options.formElement).validate({
                submitHandler: submitHandler,
                rules: {
                    portfolioId: {maxlength: 11, number: true, required: true},
                    alunoperId: {maxlength: 10, number: true, required: true},
                    portfolioalunoDataEntrega: {required: true, dateBR: true},
                    arqId: {maxlength: 10, number: true},
                    portfolioalunoPontuacao: {maxlength: 11, number: true},
                    portfolioalunoDataAvaliacao: {dateBR: true},
                    portfolioAlunoRetorno: {},
                    portfolioalunoSituacao: {maxlength: 9},
                },
                messages: {
                    portfolioId: {
                        maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    alunoperId: {
                        maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    portfolioalunoDataEntrega: {required: 'Campo obrigatório!', dateBR: 'Informe uma data válida!'},
                    arqId: {maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!'},
                    portfolioalunoPontuacao: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!'},
                    portfolioalunoDataAvaliacao: {dateBR: 'Informe uma data válida!'},
                    portfolioAlunoRetorno: {},
                    portfolioalunoSituacao: {maxlength: 'Tamanho máximo: 9!'},
                },
                ignore: '.ignore'
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.estagioperiodoAlunoPortfolioAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.atividade.estagioperiodo-aluno-portfolio.add");

        if (!obj) {
            obj = new EstagioperiodoAlunoPortfolioAdd();
            obj.run(params);
            $(window).data('universa.estagioperiodo-aluno-portfolio.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);