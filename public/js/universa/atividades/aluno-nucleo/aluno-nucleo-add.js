(function ($, window, document) {
    'use strict';

    var EstagioperiodoAlunoNucleoAdd = function () {
        VersaShared.call(this);
        var __estagioperiodoAlunoNucleoAdd = this;
        this.defaults = {
            url: {
                estagioPeriodoLetivo: '',
                estagiogeralNucleo: '',
                acadperiodoAluno: ''
            },
            data: {},
            value: {
                perId: null,
                nucleoId: null,
                alunoperId: null
            },
            datatables: {},
            wizardElement: '#estagioperiodo-aluno-nucleo-wizard',
            formElement: '#estagioperiodo-aluno-nucleo-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#perId").select2({
                language: 'pt-BR',
                tags: true,
                ajax: {
                    url: __estagioperiodoAlunoNucleoAdd.options.url.estagioPeriodoLetivo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.per_id, id: el.per_id};
                        });

                        return {results: transformed};
                    }
                },
                allowClear: true,
                initSelection: function (element, callback) {
                    callback(__estagioperiodoAlunoNucleoAdd.getPerId());
                }
            });
            $('#perId').select2("val", __estagioperiodoAlunoNucleoAdd.getPerId());
            $("#nucleoId").select2({
                language: 'pt-BR',
                tags: true,
                ajax: {
                    url: __estagioperiodoAlunoNucleoAdd.options.url.estagiogeralNucleo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.nucleo_id, id: el.nucleo_id};
                        });

                        return {results: transformed};
                    }
                },
                allowClear: true,
                initSelection: function (element, callback) {
                    callback(__estagioperiodoAlunoNucleoAdd.getNucleoId());
                }
            });
            $('#nucleoId').select2("val", __estagioperiodoAlunoNucleoAdd.getNucleoId());
            $("#alunoperId").select2({
                language: 'pt-BR',
                tags: true,
                ajax: {
                    url: __estagioperiodoAlunoNucleoAdd.options.url.acadperiodoAluno,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.alunoper_id, id: el.alunoper_id};
                        });

                        return {results: transformed};
                    }
                },
                allowClear: true,
                initSelection: function (element, callback) {
                    callback(__estagioperiodoAlunoNucleoAdd.getAlunoperId());
                }
            });
            $('#alunoperId').select2("val", __estagioperiodoAlunoNucleoAdd.getAlunoperId());
            $("#alunoperiodoDataInscricao")
                .datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                })
                .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});
            $("#alunoperiodoDataFechamento")
                .datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                })
                .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});
            $("#alunoperiodoTotalHoras").inputmask({showMaskOnHover: false, clearIncomplete: true, alias: 'integer'});
        };


        this.setPerId = function (perId) {
            this.options.value.PerId = perId || null;
        };

        this.getPerId = function () {
            return priv.options.value.PerId || null;
        };

        this.setNucleoId = function (nucleoId) {
            this.options.value.NucleoId = nucleoId || null;
        };

        this.getNucleoId = function () {
            return priv.options.value.NucleoId || null;
        };

        this.setAlunoperId = function (alunoperId) {
            this.options.value.AlunoperId = alunoperId || null;
        };

        this.getAlunoperId = function () {
            return priv.options.value.AlunoperId || null;
        };

        this.setValidations = function () {
            var submitHandler = function (form) {
                form.submit();
            };

            $(__estagioperiodoAlunoNucleoAdd.options.formElement).validate({
                submitHandler: submitHandler,
                rules: {
                    alunonucleoId: {maxlength: 10, number: true, required: true},
                    perId: {maxlength: 10, number: true, required: true},
                    nucleoId: {maxlength: 10, number: true, required: true},
                    alunoperId: {maxlength: 10, number: true, required: true},
                    alunoperiodoDataInscricao: {required: true, dateBR: true},
                    alunoperiodoDataFechamento: {dateBR: true},
                    alunoperiodoTotalHoras: {maxlength: 11, number: true},
                    alunoperiodoObservacoes: {maxlength: 45},
                },
                messages: {
                    alunonucleoId: {
                        maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    perId: {
                        maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    nucleoId: {
                        maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    alunoperId: {
                        maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    alunoperiodoDataInscricao: {required: 'Campo obrigatório!', dateBR: 'Informe uma data válida!'},
                    alunoperiodoDataFechamento: {dateBR: 'Informe uma data válida!'},
                    alunoperiodoTotalHoras: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!'},
                    alunoperiodoObservacoes: {maxlength: 'Tamanho máximo: 45!'},
                },
                ignore: '.ignore'
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.estagioperiodoAlunoNucleoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.atividade.estagioperiodo-aluno-nucleo.add");

        if (!obj) {
            obj = new EstagioperiodoAlunoNucleoAdd();
            obj.run(params);
            $(window).data('universa.estagioperiodo-aluno-nucleo.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);