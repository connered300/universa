(function ($, window, document) {
    'use strict';

    var atividadeAlunoNucleoIndex = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlSearch: '',
                urlEdit: '',
                urlRemove: '',
                urlAdd: '',
                urlNucleos: ''
            }
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);
        };

        priv.addAlunoNucleo = function () {
            var matricula = '';
            var periodo = '';

            $('#dataTableAlunoNucleo').on('click', '.inscricao', function () {
                priv.nucleoSelect2();
                var $pai = $(this).closest('tr');
                var data = priv.dataTableAlunoNucleo.fnGetData($pai);
                $("#pesquisaNucleo").select2("enable", true);

                $("#alunoNucleoId").val(data['alunonucleo_id']);
                $("#alunoperId").val(data['matricula']);
                $("#alunoNucleoEdita").val(true);

                if (data['alunoperiodo_status'] == "DESLIGADO") {
                    $("#alunoNucleoEdita").val('');
                }

                $('.form-control-static.' + 'alunoNucleoDesc').html(data['nucleo_descricao'] ? data['nucleo_descricao'] : '-');
                matricula = data['matricula'];
                periodo = data['PP'];
            });

            $('#salvarAlunoNucleo').click(function () {
                priv.addOverlay($(".modal-content"));

                $.ajax({
                    url: priv.options.urlAdd,
                    dataType: 'json',
                    method: 'POST',
                    data: {
                        nucleoId: $('#pesquisaNucleo').val() || false,
                        alunoperId: matricula,
                        perId: periodo,
                        alunonucleoId: $("#alunoNucleoEdita").val() ? false : $("#alunoNucleoId").val()
                    },
                    success: function (result) {
                        if (result['type'] == 'success') {
                            priv.showNotificacao({
                                type: 'success',
                                content: result.message,
                                timeout: 3000
                            });
                        } else {
                            priv.showNotificacao({
                                type: 'danger',
                                content: result.message,
                                timeout: 5000
                            });
                        }
                    }
                }).done(function () {
                    $('.fechaModal').click();
                    $('#dataTableAlunoNucleo').DataTable().ajax.reload(null, false);
                    priv.removeOverlay($(".modal-content"));
                });
            });
        };

        priv.addOverlay = function (el, mensagem) {
            mensagem = mensagem || 'Aguarde...';
            el.append('<div class="app-overlay">' + mensagem + '</div>');
        };

        priv.showNotificacao = function (param) {
            var options = $.extend(
                true,
                {
                    content: '',
                    title: '',
                    type: 'info',
                    icon: '',
                    color: '',
                    timeout: false
                },
                param
            );

            switch (options.type) {
                case 'danger':
                    options.icon = 'fa-times-circle';
                    options.title = options.title || 'Atenção:';
                    options.color = '#a90329';
                    break;
                case 'info':
                    options.icon = 'fa-info-circle';
                    options.title = options.title || 'Informação:';
                    options.color = '#57889c';
                    break;
                case 'success':
                    options.icon = 'fa-check-circle';
                    options.title = options.title || 'Sucesso:';
                    options.color = '#739e73';
                    break;
                case 'warning':
                    options.icon = 'fa-exclamation-circle';
                    options.title = options.title || 'Atenção:';
                    options.color = '#c79121';
                    break;
            }

            var optionsNotification = {
                title: options.title,
                content: "<i>" + options.content + "</i>",
                color: options.color,
                icon: "fa " + options.icon
            };

            if (options.timeout) {
                optionsNotification.timeout = options.timeout;
            }

            $.smallBox(optionsNotification);
        };

        priv.removeOverlay = function (el) {
            el.find('>.app-overlay').remove();
        };

        priv.nucleoSelect2 = function () {
            $('#pesquisaNucleo').select2({
                ajax: {
                    url: priv.options.urlNucleos,
                    dataType: 'json',
                    type: 'POST',
                    data: function (query) {
                        return {
                            query: query,
                            alunoperId: $("#alunoperId").val(),
                            nucleosValidos: $("#alunoNucleoEdita").val()
                        }
                    },
                    results: function (data) {
                        if (JSON.stringify(data) == "[]") {
                            $("#pesquisaNucleo").select2("disable", true);
                            priv.showNotificacao({
                                type: 'info',
                                content: "Não há núcleos disponíveis para este aluno!",
                                timeout: 3000
                            });
                        }

                        var transformed = $.map(data, function (el) {
                            el.text = el.nucleo_descricao;
                            el.id = el.nucleo_id;

                            return el;

                        });

                        return {results: transformed};
                    }
                }
            });
        };

        priv.setSteps = function () {
            var colNum = 0;
            priv.dataTableAlunoNucleo = $('#dataTableAlunoNucleo').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: priv.options.urlSearch,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary estagioperiodoAlunoNucleo-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger estagioperiodoAlunoNucleo-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;

                            if (data[row]['resultado'] == 'yes') {
                                data[row]["nucleos"] =
                                    "<button class='btn btn-success inscricao' data-toggle='modal' data-target='#modalAdd'>Inscrever</button>";
                            } else {
                                data[row]["nucleos"] =
                                    "<button class='btn btn-success inscricao' disabled>Inscrever</button>";
                            }
                        }

                        priv.removeOverlay($('.widget-body'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "periodo", data: "periodo", visible: false, orderable: false},
                    {name: "alunocurso_id", targets: colNum++, data: "alunocurso_id"},
                    {name: "matricula", targets: colNum++, data: "matricula"},
                    {name: "pes_nome", targets: colNum++, data: "pes_nome"},
                    {name: "curso_nome", targets: colNum++, data: "curso_nome"},
                    {name: "turma_nome", targets: colNum++, data: "turma_nome"},
                    {name: "status_entrega_portifolio", targets: colNum++, data: "status_entrega_portifolio"},
                    {name: "total_horas", targets: colNum++, data: "total_horas"},
                    {name: "status_disciplinaconclusao", targets: colNum++, data: "status_disciplinaconclusao"},
                    {name: "nucleo_descricao", targets: colNum++, data: "nucleo_descricao"},
                    {name: "nucleos", targets: colNum++, data: "nucleos"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[2, 'asc']]
            });

            $('#dataTableAlunoNucleo').on('click', '.estagioperiodoAlunoNucleo-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = priv.dataTableAlunoNucleo.fnGetData($pai);

                location.href = priv.options.urlEdit + '/' + data['alunonucleo_id'];
            });

            $('#dataTableAlunoNucleo').on('click', '.estagioperiodoAlunoNucleo-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = priv.dataTableAlunoNucleo.fnGetData($pai);
                var arrRemover = {
                    alunonucleoId: data['alunonucleo_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de estagioperiodo__aluno_nucleo "' +
                    data['alunoperiodo_observacoes'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        priv.addOverlay($('.widget-body'),
                            'Aguarde, solicitando remoção de registro de estagioperiodo__aluno_nucleo...');
                        $.ajax({
                            url: priv.options.urlRemove,
                            type: 'POST',
                            dataType: 'json',
                            data: arrRemover,
                            success: function (data) {
                                if (data.erro) {
                                    $.smallBox({
                                        title: "Não foi possível remover registro de estagioperiodo__aluno_nucleo:",
                                        content: "<i>" + data['erroDescricao'] + "</i>",
                                        color: "#C46A69",
                                        icon: "fa fa-times",
                                        timeout: 10000
                                    });

                                    priv.removeOverlay($('.widget-body'));
                                } else {
                                    priv.dataTableAlunoNucleo.api().ajax.reload(null, false);
                                    $.smallBox({
                                        title: "Sucesso!",
                                        content: "<i>Registro de estagioperiodo__aluno_nucleo removido!</i>",
                                        color: "#739e73",
                                        icon: "fa fa-check-circle",
                                        timeout: 10000
                                    });
                                }
                            }
                        });
                    }
                });

                e.preventDefault();
            });

            priv.addAlunoNucleo();
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
        };
    };

    $.atividadeAlunoNucleoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.atividade-aluno-nucleo.index");

        if (!obj) {
            obj = new atividadeAlunoNucleoIndex();
            obj.run(params);
            $(window).data('universa.atividade-aluno-nucleo.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);