(function ($, window, document) {
    'use strict';

    var EstagiogeralAtividadesDisciplinaAdd = function () {
        VersaShared.call(this);
        var __estagiogeralAtividadesDisciplinaAdd = this;
        this.defaults = {
            url: {
                estagiogeralConfiguracoesAtividades: '',
                acadgeralDisciplina: ''
            },
            data: {},
            value: {
                estagioconfatividadeId: null,
                discId: null
            },
            datatables: {},
            wizardElement: '#estagiogeral-atividades-disciplina-wizard',
            formElement: '#estagiogeral-atividades-disciplina-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#estagioconfatividadeId").select2({
                language: 'pt-BR',
                tags: true,
                ajax: {
                    url: __estagiogeralAtividadesDisciplinaAdd.options.url.estagiogeralConfiguracoesAtividades,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.estagioconfatividade_id, id: el.estagioconfatividade_id};
                        });

                        return {results: transformed};
                    }
                },
                allowClear: true,
                initSelection: function (element, callback) {
                    callback(__estagiogeralAtividadesDisciplinaAdd.getEstagioconfatividadeId());
                }
            });
            $('#estagioconfatividadeId').select2("val",
                                                 __estagiogeralAtividadesDisciplinaAdd.getEstagioconfatividadeId());
            $("#discId").select2({
                language: 'pt-BR',
                tags: true,
                ajax: {
                    url: __estagiogeralAtividadesDisciplinaAdd.options.url.acadgeralDisciplina,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.disc_id, id: el.disc_id};
                        });

                        return {results: transformed};
                    }
                },
                allowClear: true,
                initSelection: function (element, callback) {
                    callback(__estagiogeralAtividadesDisciplinaAdd.getDiscId());
                }
            });
            $('#discId').select2("val", __estagiogeralAtividadesDisciplinaAdd.getDiscId());
        };


        this.setEstagioconfatividadeId = function (estagioconfatividadeId) {
            this.options.value.EstagioconfatividadeId = estagioconfatividadeId || null;
        };

        this.getEstagioconfatividadeId = function () {
            return priv.options.value.EstagioconfatividadeId || null;
        };

        this.setDiscId = function (discId) {
            this.options.value.DiscId = discId || null;
        };

        this.getDiscId = function () {
            return priv.options.value.DiscId || null;
        };

        this.setValidations = function () {
            var submitHandler = function (form) {
                form.submit();
            };

            $(__estagiogeralAtividadesDisciplinaAdd.options.formElement).validate({
                submitHandler: submitHandler,
                rules: {
                    atividadediscId: {maxlength: 11, number: true, required: true},
                    estagioconfatividadeId: {maxlength: 11, number: true, required: true},
                    discId: {maxlength: 10, number: true, required: true},
                },
                messages: {
                    atividadediscId: {
                        maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    estagioconfatividadeId: {
                        maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    discId: {
                        maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                },
                ignore: '.ignore'
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.estagiogeralAtividadesDisciplinaAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.atividade.estagiogeral-atividades-disciplina.add");

        if (!obj) {
            obj = new EstagiogeralAtividadesDisciplinaAdd();
            obj.run(params);
            $(window).data('universa.estagiogeral-atividades-disciplina.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);