(function ($, window, document) {
    'use strict';

    var defaults = {
        url: {
            alunoPeriodo: '',
            alunoNucleo: '',
            alunoPortfolio: '',
            addPortfolio: '',
            atividadePeriodo: '',
            atividadeConfig: '',
            urlAtividades: '',
            urlPeriodo: '',
            urlNucleo: '',
            urlEdit: '',
            nucleo: '',
            urlRemove: ''
        },
        datatables: {
            nucleo: undefined,
            portfolio: undefined,
            atividades: undefined
        },
        portfolio: {
            PT_MAX: '',
            PT_MIN: '',
            alunonucleoId: '',
            matricula: ''
        },
        data: {
            aluno_informacoes: undefined,
            atividadePraticaId: undefined,
            portifolioId: undefined,
            resultadoAvaliacao: false
        },
        value: {
            possuiAcessoExclusao: null
        }
    };
    var versaShared = new VersaShared();

    var alunoAtividades = {
        options: defaults,
        setDefaults: function (opts) {
            opts = opts || [];

            alunoAtividades.options = $.extend(defaults, opts);
        },
        formatDateBrasileiro: function (element) {
            if (element == " - ") {
                return element;
            }
            var year = element.slice(0, 4);
            var month = element.slice(5, 7);
            var day = element.slice(8, 10);
            return (day + '/' + month + '/' + year);
        },
        pesquisaAluno: {
            setFields: function () {
                $('#alunoPesquisa').select2({
                    ajax: {
                        url: alunoAtividades.options.url.alunoPeriodo,
                        dataType: 'json',
                        data: function (query) {
                            return {query: query};
                        },
                        results: function (data) {
                            var resultado = $.map(data, function (elements) {
                                elements.text = elements.pesNome;
                                elements.id = elements.alunocursoId;

                                return elements;
                            });

                            return {
                                results: resultado
                            };
                        }
                    },
                    minimumInputLength: 1
                }).on("change", function () {
                    setTimeout(function () {
                        versaShared.addOverlay($("#widget-grid"), "Aguarde, carregando dados!")
                    }, 1000);

                    alunoAtividades.pesquisaAluno.buscaInformacoesAluno($(this).val());
                    alunoAtividades.pesquisaAluno.atualizaInformacoesAluno();
                    alunoAtividades.alunoNucleo.atualizaInformacoesAluno();
                    alunoAtividades.alunoPortfolio.atualizaInformacoesAluno();
                    alunoAtividades.alunoPortfolio.verificaSituacaoAluno();

                    $('#pesquisaPeriodo').select2("val", "");
                    $('#pesquisaAtividade').select2("val", "");
                    $('.ativRow').remove();
                    alunoAtividades.atividades.datatable();

                    $('.alunoAbasAviso').show();

                    if (this.value) {
                        $('.alunoAbasAviso').hide();
                    }

                    setTimeout(function () {
                        versaShared.removeOverlay($("#widget-grid"));
                    }, 2500);
                });
            },
            buscaInformacoesAluno: function (alunocursoId) {
                $.ajax({
                    async: false,
                    url: alunoAtividades.options.url.alunoPeriodo,
                    method: 'POST',
                    dataType: 'json',
                    data: {pesquisaAtividade: true, query: alunocursoId, alunocursoId: alunocursoId},
                    success: function (resultado) {
                        $.map(resultado, function (elements) {
                            alunoAtividades.options.data.aluno_informacoes = elements;
                            alunoAtividades.options.portfolio.matricula = elements.alunocursoId;
                        });
                    }
                });
            },
            atualizaInformacoesAluno: function () {
                var infoAluno = alunoAtividades.options.data.aluno_informacoes;

                // preenche os valores de informações de aluno
                for (var field in infoAluno) {
                    var atual = infoAluno[field] || '-';
                    $("p[data-info='" + field + "']").html(atual);
                }
                $('#alunoAbas').show();
            },
        },
        atividades: {
            datatable: function () {
                $('#tableAtividades').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "searching": false,
                    "destroy": true,
                    "order": [0, 'desc'],
                    "ajax": {
                        "url": alunoAtividades.options.url.urlAtividades,
                        "type": "POST",
                        "async": false,
                        "data": function (d) {
                            d.alunocursoId = alunoAtividades.options.data.aluno_informacoes.alunocursoId;

                            return d;
                        },
                        "dataSrc": function (e) {
                            var data = e.data;
                            var possuiAcessoExclusao = alunoAtividades.options.value.possuiAcessoExclusao;

                            for (var row in data) {
                                var atividadeId = data[row]['atividadealuno_atividade_id'];
                                var btn =
                                    "<td><button class='btn btn-xs btn-primary btn-modal-atividade-hora' data-toggle='modal' data-target='#atividadeEditar' title='" +
                                    atividadeId + "' " + status +
                                    "'><i  class='fa fa-pencil' ></i></button></td>";

                                if (possuiAcessoExclusao) {
                                    btn = btn +
                                        "<td><button class='btn btn-xs btn-danger btn-modal-atividade-excluir' data-toggle='modal'" +
                                        " title='Excluir      " +
                                        "registro'><i  class='fa fa-times' ></i></button></td>";
                                }

                                if (data[row]['status'] == 'ATIVO') {
                                    data[row]['btn'] = btn;
                                } else {
                                    data[row]['btn'] = '';
                                }
                            }
                            return data;
                        }
                    },
                    "sDom": "<'dt-toolbar'<'col-xs-6'f>r>" + "t" + "<'dt-toolbar-footer'<'col-xs-6'i><'col-xs-6'p>>",
                    "columns": [
                        {"name": "alunoatividade_data_lancamento", targets: 0, data: "alunoatividade_data_lancamento"},
                        {"name": "alunoatividade_horas", targets: 1, data: "alunoatividade_horas"},
                        {"name": "alunoatividade_descricao", targets: 2, data: "alunoatividade_descricao"},
                        {"name": "per_nome", targets: 3, data: "per_nome"},
                        {"name": "pes_nome", targets: 4, data: "pes_nome"},
                        {"data": "btn"}
                    ], oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    }
                });

                $(".btn-modal-atividade-excluir").on("click", function () {

                    var $tr = $(this).closest('tr');
                    var data = $('#tableAtividades').dataTable().fnGetData($tr);

                    $.SmartMessageBox({
                        title: "Confirme a operação:",
                        content: 'Deseja realmente remover este registro: ' + data.alunoatividade_descricao + '?',
                        buttons: "[Não][Sim]",
                    }, function (ButtonPress) {
                        if (ButtonPress == "Sim") {

                            versaShared.addOverlay($(".widget-body"), "Aguarde Excluindo Atividade!");

                            $.ajax({
                                url: alunoAtividades.options.url.urlRemove,
                                type: 'POST',
                                method: 'POST',
                                dataType: 'json',
                                data: {
                                    atividadealunoAtividadeId: data['atividadealuno_atividade_id']
                                },
                                success: function (data) {
                                    data = data || [];

                                    if (data && data.erro) {

                                        alunoAtividades.showNotificacao({
                                            type: 'warning',
                                            content: data['message'] || "Erro ao excluir registro!",
                                            timeout: 3000
                                        });

                                    } else {

                                        alunoAtividades.showNotificacao({
                                            type: 'success',
                                            content: "Registro excluido com sucesso!",
                                            timeout: 3000
                                        });
                                        $('#tableAtividades').DataTable().ajax.reload(null, false);

                                    }
                                }
                            }).done(function () {
                                versaShared.removeOverlay($(".widget-body"));
                            });
                        }
                    });
                });
            },
            setFields: function () {
                var perId = '';
                var nucleoId = '';

                $("#pesquisaPeriodo").select2({
                    ajax: {
                        url: alunoAtividades.options.url.urlPeriodo,
                        dataType: 'json',
                        data: function (query) {
                            return {query: query};
                        },
                        results: function (data) {
                            var resultado = $.map(data, function (elements) {
                                var text = elements.per_semestre + "/" + elements.per_ano;
                                elements.text = text;
                                elements.id = elements.per_id;

                                return elements;
                            });

                            return {
                                results: resultado
                            };
                        }
                    }
                }).on("change", function (periodo) {
                    perId = parseInt(periodo.val);

                    $("#pesquisaAtividade").select2('val', '');

                    $('.ativRow').remove();

                    $('.input-ativ').show();

                    $("#pesquisaAtividade").select2({
                        ajax: {
                            url: alunoAtividades.options.url.urlNucleo,
                            dataType: 'json',
                            data: function (query) {
                                return {
                                    query: query,
                                    perId: parseInt(periodo.val)
                                };
                            },
                            results: function (data) {
                                var resultado = $.map(data, function (elements) {
                                    elements.text = elements.nucleo_descricao;
                                    elements.id = elements.nucleo_id;

                                    return elements;
                                });

                                return {
                                    results: resultado
                                };
                            }
                        }
                    }).on("change", function (nucleo) {
                        nucleoId = parseInt(nucleo.val);
                    });

                });

                $('.salvarHoras').click(function () {
                    var horas = $('#atvHoras').val();

                    if (horas) {
                        alunoAtividades.atividades.salvarHoras(horas, nucleoId, perId);
                    }
                });
            },
            salvarHoras: function (horas) {
                $('#atvHoras').val('');
                versaShared.addOverlay($(".modal-content"), "Aguarde !");

                $.ajax({
                    url: alunoAtividades.options.url.urlEdit,
                    method: 'POST',
                    data: {
                        alunoatividade_horas: horas,
                        alunoatividadeId: $('.btn-modal-atividade-hora').attr('title')
                    },
                    success: function (data) {
                        if (data['type'] == 'success') {
                            alunoAtividades.showNotificacao({
                                type: 'success',
                                content: data['message'],
                                timeout: 3000
                            });
                        } else {
                            alunoAtividades.showNotificacao({
                                type: 'danger',
                                content: data['message'],
                                timeout: 6000
                            });
                        }
                    }
                }).done(function () {
                    versaShared.removeOverlay($(".modal-content"));
                    $('#tableAtividades').DataTable().ajax.reload(null, false);
                    $('.fechar').click();
                });
            }
        },

        alunoNucleo: {

            setFields: function () {
                var colNum = 0;
                alunoAtividades.options.datatables.nucleo = $("#tbNucleo").DataTable({
                    "processing": true,
                    "serverSide": true,
                    "searching": false,
                    "paging": false,
                    "ajax": {
                        "url": alunoAtividades.options.url.alunoNucleo,
                        "type": "POST",
                        "async": false,
                        data: function (d) {
                            d.matricula = alunoAtividades.options.data.aluno_informacoes.alunocursoId;

                            return d;
                        },
                        dataSrc: function (json) {
                            var data = json.data;

                            for (var row in data) {
                                data[row]['btn'] = "";

                                var btn1 = "\
                                        <button type='button' class='btn btn-primary btn-xs add-hours' data-toggle='modal' data-target='#modal-add-hours' >\
                                            <i class='fa fa-plus fa-inverse'></i>\
                                        </button>";

                                var btn2 = "\
                                        <button type='button' class='btn btn-danger btn-xs desliga' data-toggle='modal' data-target='#desligamentoModal' title='" +
                                    data[row]['alunonucleo_id'] + "' data-nucleo='" + data[row]['nucleo'] + "'>\
                                            <span class='glyphicon glyphicon-off' aria-hidden='true'></span>\
                                        </button>";

                                var btn3 = ' ' +
                                    '<button type="button" class="btn btn-xs btn-warning abrir" data-toggle="modal" title="' +
                                    data[row]['nucleo'] + '" id="' + data[row]['nucleo_id'] +
                                    '" data-target="#trocarNucleo" >' +
                                    '<span class="glyphicon glyphicon-random" aria-hidden="true"></span>' +
                                    '</button>';

                                if (data[row]['situacao_periodo'] == "ATIVO") {
                                    data[row]['btn'] = btn1 + btn2 + btn3;
                                }

                            }

                            return data;
                        }
                    },
                    columnDefs: [
                        {"name": "nucleo", targets: colNum++, data: "nucleo"},
                        {"name": "total_horas", targets: colNum++, data: "total_horas"},
                        {"name": "btn", targets: colNum++, data: "btn"},
                        {"name": "nucleo_id", data: "nucleo_id"},
                        {"name": "alunonucleo_id", data: "alunonucleo_id"},
                    ],
                    order: [[1, 'desc']],
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",

                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    }
                });

                $('#tbNucleo').on('click', '.add-hours', function (event) {
                    var $tr = $(this).closest('tr');
                    var data = $('#tbNucleo').dataTable().fnGetData($tr);
                    alunoAtividades.alunoNucleo.lancamentoAtividades(data);
                });

                $("#modal-add-hours").on('hidden.bs.modal', function () {
                    $("#tipoLancamento").val('diario');
                    $("#tipoLancamento").trigger('change');

                    $("#alunonucleo").val('');
                    $("#alunoAtividadeId").val('');
                    $("#atividadeId").val('');
                    $("#alunoatividadeDataInicial").val('');
                    $("#alunoatividadeDataFinal").val('');
                    $("#horasPontos").val('');
                    $("#alunoatividadeDescricao").val('');
                    $("#alunoatividadesObservacao").val('');
                });

                $("#saveAlunoAtividades").click(function () {
                    $("#aluno-atividades-form").submit();
                });

                $("#save").change(function () {
                    if (this.value) {
                        $("#modal-add-hours").modal('hide');
                        alunoAtividades.alunoNucleo.atualizaInformacoesAluno();
                    }
                });

                $(document).on('click', '.abrir', function () {
                    $('#nucleoPesquisa').select2({
                        ajax: {
                            url: alunoAtividades.options.url.urlNucleo,
                            dataType: 'json',
                            data: function (query) {
                                return {query: query};
                            },
                            results: function (data) {
                                var resultado = $.map(data, function (elements) {
                                    elements.id = elements.nucleo_id;
                                    elements.text = elements.nucleo_descricao;

                                    return elements;
                                });

                                return {
                                    results: resultado
                                };
                            }
                        }
                    });
                });

                $(document).on('click', '.trocar', function () {
                    alunoAtividades.alunoNucleo.trocar_de_nucleo($('#nucleoPesquisa').val());
                    $('#trocarNucleo').modal('hide');
                });

                $(document).on('click', '.desliga', function () {
                    var titulo = $(this).attr('data-nucleo');
                    $('.msgNucleo').text(titulo);
                    $('#msgAluno').text(alunoAtividades.options.data.aluno_informacoes.pesNome);
                });

                $(document).on('click', '.desligar', function () {
                    alunoAtividades.alunoNucleo.desliga_aluno_nucleo();
                    $('#desligamentoModal').modal('hide');
                });
            },
            desliga_aluno_nucleo: function () {
                $('#pesquisaAtividade').select2("val", "");
                $.ajax({
                    url: alunoAtividades.options.url.urlDesliga,
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        alunonucleoId: $('.desliga').attr('title')
                    },
                    success: function (data) {
                        if (data['type'] == 'error') {
                            alunoAtividades.showNotificacao({
                                type: 'danger',
                                content: data['message'],
                                timeout: 6000
                            });
                        } else {
                            alunoAtividades.showNotificacao({
                                type: 'success',
                                content: data['message'],
                                timeout: 6000
                            });
                        }
                    }
                }).done(function () {
                    alunoAtividades.options.datatables.nucleo.ajax.reload(null, false);
                });
            },
            trocar_de_nucleo: function (nucleoId) {
                alunoAtividades.alunoNucleo.desliga_aluno_nucleo();
                $('#pesquisaAtividade').select2("val", "");
                $.ajax({
                    url: alunoAtividades.options.url.trocaNucleo,
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        alunonucleoId: function () {
                            return alunoAtividades.options.data.aluno_informacoes.alunonucleo_id;
                        },
                        nucleoId: function () {
                            return nucleoId;
                        }
                    },
                    success: function (data) {
                        if (data['type'] == 'error') {
                            alunoAtividades.showNotificacao({
                                type: 'danger',
                                content: data['message'],
                                timeout: 6000
                            });
                        } else {
                            alunoAtividades.showNotificacao({
                                type: 'success',
                                content: data['message'],
                                timeout: 6000
                            });
                        }
                    }
                }).done(function () {
                    alunoAtividades.options.datatables.nucleo.ajax.reload(null, false);
                    alunoAtividades.pesquisaAluno.atualizaInformacoesAluno();
                });
            },
            atualizaInformacoesAluno: function () {
                if ($.isEmptyObject(alunoAtividades.options.datatables.nucleo)) {
                    alunoAtividades.alunoNucleo.setFields();

                    return;
                }

                // atualiza tabela
                alunoAtividades.options.datatables.nucleo.ajax.reload(null, false);
            },
            lancamentoAtividades: function (arrData) {
                $("#titleAtividade").html("<b>" + arrData['nucleo'] + "</b>");
                $("#alunonucleo").val(arrData['alunonucleo_id']);
                $("#atividadeId").val(alunoAtividades.options.data.atividadePraticaId);
            }
        },
        alunoPortfolio: {
            setFields: function () {
                var colNum = 0;

                alunoAtividades.options.datatables.portfolio = $("#tbPortfolio").DataTable({
                    "processing": true,
                    "serverSide": true,
                    "searching": false,
                    "ajax": {
                        "url": alunoAtividades.options.url.alunoPortfolio,
                        "type": "POST",
                        data: function (d) {
                            d.matricula = alunoAtividades.options.data.aluno_informacoes.alunocursoId;

                            return d;
                        },
                        dataSrc: function (json) {
                            var data = json.data;

                            for (var row in data) {
                                data[row]['data_entrega'] =
                                    alunoAtividades.formatDateBrasileiro(data[row]['data_entrega']);
                                data[row]['data_avaliacao'] =
                                    alunoAtividades.formatDateBrasileiro(data[row]['data_avaliacao']);
                                if (data[row]['retorno'] == "Aprovado") {
                                    defaults.data.resultadoAvaliacao = true;
                                }
                            }
                            return data;
                        }
                    },
                    "dom": "r" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-12'p>>",
                    "columns": [
                        {"name": "Data de Entrega", targets: colNum, data: "data_entrega"},
                        {"name": "Data de Avaliação", targets: colNum++, data: "data_avaliacao"},
                        {"name": "Pontuação Alcançada", targets: colNum++, data: "pontuacao"},
                        {"name": "Resultado da Avaliação", targets: colNum++, data: "retorno"},
                        {"name": "Observações", targets: colNum++, data: "situacao"}

                    ],
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    }
                });

                $("#salvarPortfolio").click(function () {
                    $('#salvarPortfolio').addClass('disabled');
                    versaShared.addOverlay($('.modal-content'), 'Aguarde!');
                    $.ajax({
                        url: alunoAtividades.options.url.addPortfolio,
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            alunocursoId: function () {
                                return alunoAtividades.options.data.aluno_informacoes.alunocursoId;
                            },
                            alunonucleoId: function () {
                                return alunoAtividades.options.data.aluno_informacoes.alunonucleo_id;
                            },
                            obs: $("#obs").val(),
                            pontos: $("#ptAlcancada").val(),
                            pt_max: alunoAtividades.options.portfolio.PT_MAX,
                            pt_min: alunoAtividades.options.portfolio.PT_MIN
                        },
                        success: function (data) {
                            if (data.result['type'] == 'error') {
                                alunoAtividades.showNotificacao({
                                    type: 'danger',
                                    content: data.result['message'] || 'Falha Ao lançar o portfólio.',
                                    timeout: 6000
                                });
                            } else if (data.result['type'] == 'warning') {
                                alunoAtividades.showNotificacao({
                                    type: 'warning',
                                    content: data.result['message'] || 'Falha Ao lançar o portfólio.',
                                    timeout: 6000
                                });
                            } else {
                                alunoAtividades.showNotificacao({
                                    type: 'success',
                                    content: data.result['message'] || 'Operação efetuada com sucesso.',
                                    timeout: 6000
                                });
                            }
                            versaShared.removeOverlay($('.modal-content'), 'Aguarde!');

                            $('#modalPortfolio').modal('hide');
                        }
                    }).done(function () {
                        alunoAtividades.options.datatables.portfolio.ajax.reload(null, false);
                        alunoAtividades.alunoPortfolio.verificaSituacaoAluno();

                    });
                });

                $("#btnLancamentoPort").click(function () {
                    var modalPortifolio = $("#modalPortfolio");

                    modalPortifolio.find('[name=ptAlcancada]').val("");
                    modalPortifolio.find('[name=obs]').val("");
                    $('#salvarPortfolio').removeClass('disabled');
                    modalPortifolio.modal('show');
                });
            },
            buscaConfPortfolio: function () {
                $.ajax({
                    async: false,
                    url: alunoAtividades.options.url.atividadeConfig,
                    method: 'POST',
                    dataType: 'json',
                    data: {atividadeatividadeId: alunoAtividades.options.data.portifolioId},
                    success: function (resultado) {
                        $.map(resultado, function (elements) {
                            alunoAtividades.options.portfolio.PT_MAX = elements.atividadeconf_pontuacao_max;
                            alunoAtividades.options.portfolio.PT_MIN = elements.atividadeconf_pontuacao_min;
                        });
                    }
                });
            },
            verificaSituacaoAluno: function () {
                $.ajax({
                    async: false,
                    url: alunoAtividades.options.url.alunoPortfolio,
                    method: 'POST',
                    dataType: 'json',
                    data: {matricula: alunoAtividades.options.portfolio.matricula},
                    success: function (resultado) {
                        var aprovado = false;
                        $.map(resultado, function (element) {
                            for (var row in element) {
                                if (element[row]['retorno'] == "Aprovado") {
                                    aprovado = true;
                                }
                            }
                        });
                        if (aprovado == true) {
                            $(':button#btnLancamentoPort').prop("disabled", true);
                        } else {
                            $(':button#btnLancamentoPort').prop("disabled", false);
                        }
                    }
                });
            },
            atualizaInformacoesAluno: function () {
                if ($.isEmptyObject(alunoAtividades.options.datatables.portfolio)) {
                    alunoAtividades.alunoPortfolio.setFields();

                    return;
                }

                alunoAtividades.options.datatables.portfolio.ajax.reload(null, false);
                alunoAtividades.alunoPortfolio.verificaSituacaoAluno();
            }
        },
        showNotificacao: function (param) {
            var options = $.extend(
                true,
                {
                    content: '',
                    title: '',
                    type: 'info',
                    icon: '',
                    color: '',
                    timeout: false
                },
                param
            );

            switch (options.type) {
                case 'danger':
                    options.icon = 'fa-times-circle';
                    options.title = options.title || 'Atenção:';
                    options.color = '#a90329';
                    break;
                case 'info':
                    options.icon = 'fa-info-circle';
                    options.title = options.title || 'Informação:';
                    options.color = '#57889c';
                    break;
                case 'success':
                    options.icon = 'fa-check-circle';
                    options.title = options.title || 'Sucesso:';
                    options.color = '#739e73';
                    break;
                case 'warning':
                    options.icon = 'fa-exclamation-circle';
                    options.title = options.title || 'Atenção:';
                    options.color = '#c79121';
                    break;
            }

            var optionsNotification = {
                title: options.title,
                content: "<i>" + options.content + "</i>",
                color: options.color,
                icon: "fa " + options.icon
            };

            if (options.timeout) {
                optionsNotification.timeout = options.timeout;
            }

            $.smallBox(optionsNotification);
        },
        run: function (opts) {
            alunoAtividades.setDefaults(opts);
            alunoAtividades.pesquisaAluno.setFields();
            alunoAtividades.alunoPortfolio.buscaConfPortfolio();
            alunoAtividades.atividades.setFields();
        },
    };

    $.alunoAtividades = function (params) {
        params = params || [];

        var obj = $(window).data("universa.aluno.atividades");

        if (!obj) {
            obj = alunoAtividades;
            obj.run(params);
            $(window).data('universa.aluno.atividades', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);