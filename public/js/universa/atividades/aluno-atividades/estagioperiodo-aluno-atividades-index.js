(function ($, window, document) {
    'use strict';

    var EstagioperiodoAlunoAtividadesIndex = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlSearch: '',
                urlEdit: '',
                urlRemove: ''
            }
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);
        };

        priv.addOverlay = function (el, mensagem) {
            mensagem = mensagem || 'Aguarde...';
            el.append('<div class="app-overlay">' + mensagem + '</div>');
        };

        priv.removeOverlay = function (el) {
            el.find('>.app-overlay').remove();
        };

        priv.setSteps = function () {
            var colNum = 0;
            priv.dataTableEstagioperiodoAlunoAtividades = $('#dataTableEstagioperiodoAlunoAtividades').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: priv.options.urlSearch,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary estagioperiodoAlunoAtividades-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger estagioperiodoAlunoAtividades-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                        }

                        priv.removeOverlay($('.widget-body'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "estagioaluno_atividade_id", targets: colNum++, data: "estagioaluno_atividade_id"},
                    {name: "alunoatividade_descricao", targets: colNum++, data: "alunoatividade_descricao"},
                    {name: "alunonucleo_id", targets: colNum++, data: "alunonucleo_id"},
                    {name: "estagioatividade_id", targets: colNum++, data: "estagioatividade_id"},
                    {name: "usuario_lancamento", targets: colNum++, data: "usuario_lancamento"},
                    {name: "alunoatividade_data", targets: colNum++, data: "alunoatividade_data"},
                    {name: "estagioprocesso_id", targets: colNum++, data: "estagioprocesso_id"},
                    {name: "alunoatividade_pontos", targets: colNum++, data: "alunoatividade_pontos"},
                    {name: "alunoatividade_horas", targets: colNum++, data: "alunoatividade_horas"},
                    {name: "alunoatividades_observacao", targets: colNum++, data: "alunoatividades_observacao"},
                    {name: "acao", targets: colNum++, data: "estagioaluno_atividade_id"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $('#dataTableEstagioperiodoAlunoAtividades').on('click', '.estagioperiodoAlunoAtividades-edit',
                                                            function (event) {
                                                                var $pai = $(this).closest('tr');
                                                                var data = priv.dataTableEstagioperiodoAlunoAtividades.fnGetData($pai);

                                                                location.href = priv.options.urlEdit + '/' +
                                                                                data['estagioaluno_atividade_id'];
                                                            });

            $('#dataTableEstagioperiodoAlunoAtividades').on('click', '.estagioperiodoAlunoAtividades-remove',
                                                            function (e) {
                                                                var $pai = $(this).closest('tr');
                                                                var data = priv.dataTableEstagioperiodoAlunoAtividades.fnGetData($pai);
                                                                var arrRemover = {
                                                                    estagioalunoAtividadeId: data['estagioaluno_atividade_id']
                                                                };

                                                                $.SmartMessageBox({
                                                                    title: "Confirme a operação:",
                                                                    content: 'Deseja realmente remover registro de estagioperiodo__aluno_atividades "' +
                                                                             data['alunoatividades_observacao'] + '"?',
                                                                    buttons: "[Não][Sim]",
                                                                }, function (ButtonPress) {
                                                                    if (ButtonPress == "Sim") {
                                                                        priv.addOverlay($('.widget-body'),
                                                                                        'Aguarde, solicitando remoção de registro de estagioperiodo__aluno_atividades...');
                                                                        $.ajax({
                                                                            url: priv.options.urlRemove,
                                                                            type: 'POST',
                                                                            dataType: 'json',
                                                                            data: arrRemover,
                                                                            success: function (data) {
                                                                                if (data.erro) {
                                                                                    $.smallBox({
                                                                                        title: "Não foi possível remover registro de estagioperiodo__aluno_atividades:",
                                                                                        content: "<i>" +
                                                                                                 data['erroDescricao'] +
                                                                                                 "</i>",
                                                                                        color: "#C46A69",
                                                                                        icon: "fa fa-times",
                                                                                        timeout: 10000
                                                                                    });

                                                                                    priv.removeOverlay($('.widget-body'));
                                                                                } else {
                                                                                    priv.dataTableEstagioperiodoAlunoAtividades.api().ajax.reload(null, false);
                                                                                    $.smallBox({
                                                                                        title: "Sucesso!",
                                                                                        content: "<i>Registro de estagioperiodo__aluno_atividades removido!</i>",
                                                                                        color: "#739e73",
                                                                                        icon: "fa fa-check-circle",
                                                                                        timeout: 10000
                                                                                    });
                                                                                }
                                                                            }
                                                                        });
                                                                    }
                                                                });

                                                                e.preventDefault();
                                                            });
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
        };
    };

    $.estagioperiodoAlunoAtividadesIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.atividade.aluno-atividades.index");

        if (!obj) {
            obj = new EstagioperiodoAlunoAtividadesIndex();
            obj.run(params);
            $(window).data('universa.aluno-atividades.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);