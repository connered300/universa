(function ($, window, document) {
    'use strict';

    var defaults = {
        partial: undefined,
        url: {
            alunoAtividadesAdd: '',
        },
    };

    var alunoAtividadesAdd = {
        options: defaults,
        setDefaults: function (opts) {
            opts = opts || [];

            $.validator.setDefaults({
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    }
                    else if (element.parent('label').length) {
                        error.insertAfter(element.parent());
                    }
                    else {
                        error.insertAfter(element);
                    }
                }
            });

            alunoAtividadesAdd.options = $.extend(defaults, opts);
        },
        setFields: function () {
            $("#tipoLancamento").select2({
                data: [
                    {id: 'diario', text: 'Diário'},
                    {id: 'mensal', text: 'Intervalo de Datas'}
                ]
            });

            $("#tipoLancamento").change(function () {
                var showDateFinal = (this.value == 'diario') ? 'none' : 'block';

                $("#blocoDataFinal").css('display', showDateFinal);
            });

            $("#alunoatividadeDataInicial").datepicker({
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>',
                maxDate: new Date(),
                onSelect: function (newDate, $datepicker) {
                    // atualizando o valor maximo da data final para o lancamento de atividades
                    // subtrai o mês por '-1' porque no javascript os meses começam em '0'
                    newDate = newDate.split('/');
                    $("#alunoatividadeDataFinal").datepicker(
                        'option', 'minDate', new Date(newDate[2], (newDate[1] - 1), newDate[0])
                    );
                },
            }).inputmask({
                showMaskOnHover: false,
                clearIncomplete: true,
                mask: ['99/99/9999']
            });

            $("#alunoatividadeDataFinal").datepicker({
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>',
                maxDate: new Date(),
                onSelect: function (newDate, $datepicker) {
                    // atualizando o valor máximo de data inicial para o lancamento de atividades
                    // subtrai o mês por '-1' porque no javascript os meses começam em '0'
                    newDate = newDate.split('/');
                    $("#alunoatividadeDataInicial").datepicker(
                        'option', 'maxDate', new Date(newDate[2], (newDate[1] - 1), newDate[0])
                    );
                },
            }).inputmask({
                showMaskOnHover: false,
                clearIncomplete: true,
                mask: ['99/99/9999']
            });

            $("#horasPontos").inputmask('decimal', {
                showMaskOnHover: false,
                clearIncomplete: true,
            });
        },
        setValidations: function () {
            $("#aluno-atividades-form").validate({
                submitHandler: function (form) {
                    var $form = $(form);
                    var confirmacao = $form.data('confirmacaoEnvio') || false;
                    if (!confirmacao) {
                        $.SmartMessageBox({
                            title: "Confirme a operação!",
                            content: 'Confirma o lançamento de atividades para este Aluno?',
                            buttons: "[Não][Sim]"
                        }, function (ButtonPress) {
                            if (ButtonPress == "Sim") {
                                $("#aluno-atividades-form").data('confirmacaoEnvio', true);
                                $("#aluno-atividades-form").submit();
                            }
                        });

                        return false;
                    }
                    if (alunoAtividadesAdd.options.partial) {
                        alunoAtividadesAdd.saveAlunoAtividades($form.serializeJSON());
                    } else {
                        form.submit();
                    }

                    $("#aluno-atividades-form").data('confirmacaoEnvio', false);
                },
                rules: {
                    alunoatividadeData: 'required',
                    horasPontos: 'required',
                },
                messages: {
                    alunoatividadeData: "Informe a data da atividade!",
                    horasPontos: "Informe a quantidade de pontos/horas à serem contabilizadas!",
                },
                ignore: '.ignore'
            });
        },
        saveAlunoAtividades: function (arrData) {
            arrData.ajax = true;
            $.ajax({
                url: alunoAtividadesAdd.options.url.alunoAtividadesAdd,
                method: 'POST',
                dataType: 'json',
                data: arrData,
                success: function (data) {
                    if (data.erro) {
                        $("#save").val(false);
                        $("#save").trigger('change');
                        $.smallBox({
                            title: "Não foi possível realizar o lançamento de atividades ao aluno:",
                            content: "<i>" + data.message + "</i>",
                            color: "#C46A69",
                            icon: "fa fa-times",
                            timeout: 10000
                        });
                    } else {
                        $("#save").val(true);
                        $("#save").trigger('change');
                        $.smallBox({
                            title: "Sucesso!",
                            content: "<i>" + data.message + "</i>",
                            color: "#739e73",
                            icon: "fa fa-check-circle",
                            timeout: 10000
                        });
                    }
                },
                beforeSend: function () {
                    alunoAtividadesAdd.addOverlay($('.modal-content'), "Aguarde...<br>Realizando lançamento");
                },
                complete: function () {
                    alunoAtividadesAdd.removeOverlay($('.modal-content'));
                }
            });
        },
        addOverlay: function (el, mensagem) {
            mensagem = mensagem || 'Aguarde...';
            el.append('<div class="loader-overlay">' + mensagem + '</div>');
        },
        removeOverlay: function (el) {
            el.find('>.loader-overlay').remove();
        },
        run: function (params) {
            alunoAtividadesAdd.setDefaults(params);
            alunoAtividadesAdd.setFields();
            alunoAtividadesAdd.setValidations();
        },
    };

    $.alunoAtividadesAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.aluno.atividades.add");

        if (!obj) {
            obj = alunoAtividadesAdd;
            obj.run(params);
            $(window).data("universa.aluno.atividades.add", obj);
        }

        return obj;
    };
})(window.jQuery, window, document);