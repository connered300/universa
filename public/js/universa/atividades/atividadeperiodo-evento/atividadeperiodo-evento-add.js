(function ($, window, document) {
    'use strict';

    var AtividadeperiodoEventoAdd = function () {
        VersaShared.call(this);

        var __atividadeperiodoEventoAdd = this;

        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                atividadegeralTipo: '',
                atividadeSecretaria: '',
                atividadePeriodoEvento: '',
                pessoaFisica: ''
            },
            data: {
                dadosEvento: {},
                atividadeProcessoTipo: {},
                dadosAtividadeTipo: {}
            },
            callback: {},
            value: {},
            datatables: {},
            wizardElement: '#atividadeperiodo-evento-wizard',
            formElement: '#atividadeperiodo-evento-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            var $form = $(__atividadeperiodoEventoAdd.options.formElement);
            var $eventoTipo = $form.find("[name=eventoTipo]"),
                $usuarioCadastro = $form.find("[name=usuarioCadastro]"),
                $eventoResponsavel = $form.find("[name=eventoResponsavel]"),
                $tipoAtividadeEvento = $form.find("[name=tipoAtividadeEvento]"),
                $atividadeSecretariaNome = $form.find("[name=atividadeSecretariaNome]"),
                $eventoNome = $form.find("[name=eventoNome]"),
                $eventoData = $form.find('[name=eventoData]'),
                $eventoHorasValidas = $form.find('[name=eventoHorasValidas]'),
                $eventoLotacao = $form.find("[name=eventoLotacao]");


            __atividadeperiodoEventoAdd.iniciarElementoDatePicker($eventoData);
            $eventoHorasValidas.inputmask('integer');
            $eventoLotacao.inputmask('integer');

            $atividadeSecretariaNome.select2({
                language: 'pt-BR',
                allowClear: true,
                ajax: {
                    url: __atividadeperiodoEventoAdd.options.url.atividadeSecretaria,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.secretaria_id;
                            el.text = el.secretaria_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            }).on("change", function () {
                $tipoAtividadeEvento.select2("data", "").trigger("change");

            });

            $tipoAtividadeEvento.select2({
                language: 'pt-BR',
                allowClear: true,
                ajax: {
                    url: __atividadeperiodoEventoAdd.options.url.atividadegeralTipo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {
                            query: query,
                            secretariaId: $atividadeSecretariaNome.val()
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.atividadeatividade_id;
                            el.text = el.atividadeatividade_descricao;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $eventoNome.blur(function () {
                $(this).removeClass('ui-autocomplete-loading');
            })
                .autocomplete({
                    source: function (request, response) {
                        var $element = $(this.element);
                        var previous_request = $element.data("jqXHR");

                        if (previous_request) {
                            previous_request.abort();
                        }

                        $element.data("jqXHR", $.ajax({
                            url: __atividadeperiodoEventoAdd.options.url.atividadePeriodoEvento,
                            data: {
                                query: $eventoNome.val(),
                                groupBy: true
                            },
                            type: 'POST',
                            dataType: "json",
                            success: function (data) {
                                var transformed = $.map(data || [], function (el) {
                                    el.label = el.evento_nome;

                                    return el;
                                });

                                response(transformed);
                            }
                        }));
                    },
                    minLength: 2,
                    focus: function (event, ui) {
                        var $element = $(this);
                        $element.val(ui.item.evento_nome);

                        return false;
                    },
                    select: function (event, ui) {
                        var $element = $(this);
                        $element.val(ui.item.evento_nome);
                        return false;
                    }
                });


            $eventoResponsavel.blur(function () {
                $(this).removeClass('ui-autocomplete-loading');
            })
                .autocomplete({
                    source: function (request, response) {
                        var $element = $(this.element),
                            previous_request = $element.data("jqXHR");

                        if (previous_request) {
                            previous_request.abort();
                        }

                        $element.data("jqXHR", $.ajax({
                            url: __atividadeperiodoEventoAdd.options.url.pessoaFisica,
                            data: {
                                query: $eventoResponsavel.val(),
                                pesquisaPFPJ: true,
                                groupBy: true
                            },
                            type: 'POST',
                            dataType: "json",
                            success: function (data) {
                                var transformed = $.map(data || [], function (el) {
                                    el.label = el.pes_nome;

                                    return el;
                                });

                                response(transformed);
                            }
                        }));
                    },
                    minLength: 2,
                    focus: function (event, ui) {
                        var $element = $(this);
                        $element.val(ui.item.pes_nome);

                        return false;
                    },
                    select: function (event, ui) {
                        var $element = $(this);
                        $element.val(ui.item.pes_nome);
                        return false;
                    }
                });

            $eventoTipo.select2({
                language: 'pt-BR',
                allowClear: true,
                data: [{'id': "Interno", "text": "Interno"}, {'id': "Externo", 'text': "Externo"}]
            });

        };

        this.setDadosAtividadeTipo = function (arrAtividadeTipo) {
            this.options.data.dadosAtividadeTipo = arrAtividadeTipo || null;
        };

        this.getDadosAtividadeTipo = function () {
            return this.options.data.dadosAtividadeTipo;
        };

        this.setAtividadeatividade = function (atividadeatividade) {
            this.options.value.atividadeatividade = atividadeatividade || null;
        };
        this.buscarDadosEvento = function (eventoId) {
            __atividadeperiodoEventoAdd.steps.dadosPessoais.buscarDadosPessoa(eventoId);
        };

        this.getAtividadeatividade = function () {
            return this.options.value.atividadeatividade || null;
        };

        this.setArq = function (arq) {
            this.options.value.arq = arq || null;
        };

        this.getArq = function () {
            return this.options.value.arq || null;
        };

        this.setUsuarioCadastro = function (usuarioCadastro) {
            this.options.value.usuarioCadastro = usuarioCadastro || null;
        };

        this.getUsuarioCadastro = function () {
            return this.options.value.usuarioCadastro || null;
        };
        this.setValidations = function () {
            __atividadeperiodoEventoAdd.options.validator.settings.rules = {
                atividadeSecretariaNome: {required: true},
                tipoAtividadeEvento: {required: true},
                atividadeatividade: {number: true, required: true},
                eventoTipo: {required: true},
                eventoNome: {maxlength: 45, required: true},
                eventoHorasValidas: {
                    maxlength: 5, required: true
                },
                eventoResponsavel: {maxlength: 45},
                eventoLotacao: {maxlength: 11, number: true}
            };
            __atividadeperiodoEventoAdd.options.validator.settings.messages = {
                atividadeSecretariaNome: {required: 'Campo obrigatório!'},
                tipoAtividadeEvento: {required: 'Campo obrigatório!'},
                atividadeatividade: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                eventoTipo: {required: 'Campo obrigatório!'},
                eventoNome: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                eventoHorasValidas: {
                    required: 'Campo obrigatório!', maxlenght: '5 digitos é o máximo permitido!'
                },
                eventoResponsavel: {maxlength: 'Tamanho máximo: 45!'},
                eventoLotacao: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!'}
            };

            $(__atividadeperiodoEventoAdd.options.formElement).submit(function (e) {
                var $form = $(__atividadeperiodoEventoAdd.options.formElement);
                var ok = $form.valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var arrTipoAtividadeEvento = $form.find("[name=tipoAtividadeEvento]").select2("data");

                if (arrTipoAtividadeEvento['atividadegeral__horas_praticas_max'] &&
                    ($form.find("[name=eventoHorasValidas]").val() > arrTipoAtividadeEvento['atividadegeral__horas_praticas_max'])
                ) {
                    __atividadeperiodoEventoAdd.showNotificacaoWarning(
                        "A quantidade máxima de horas (" + arrTipoAtividadeEvento['atividadegeral__horas_praticas_max'] + ") foi extrapolada !"
                    );
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }


                var $form = $(__atividadeperiodoEventoAdd.options.formElement);

                if (__atividadeperiodoEventoAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();

                    if (!dados['tipoAtividadeEvento']) {
                        dados['tipoAtividadeEvento'] =
                            dados['tipoAtividadeEvento'] ? dados['tipoAtividadeEvento'] : $form.find("[name=tipoAtividadeEvento]").val();
                    }

                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __atividadeperiodoEventoAdd.addOverlay($form, 'Salvando registro. Aguarde...');

                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                var retorno = data['arrDados'] || dados;

                                if (data.erro) {
                                    __atividadeperiodoEventoAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__atividadeperiodoEventoAdd.options.listagem) {

                                    var aposSalvarFN = __atividadeperiodoEventoAdd
                                        .getFunction(__atividadeperiodoEventoAdd.options.callback, 'aposSalvar');


                                    $('#atividadeperiodo-evento-modal').modal('hide');

                                    if (aposSalvarFN) {
                                        aposSalvarFN(retorno, data);
                                    } else {
                                        $.atividadeperiodoEventoIndex().reloadDataTableAtividadeperiodoEvento();
                                        __atividadeperiodoEventoAdd.showNotificacaoSuccess(
                                            data['mensagem'] || "Registro Salvo!"
                                        );

                                    }


                                }

                                __atividadeperiodoEventoAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.buscarDadosEvento = function (eventoId, $form) {
            $form = $form || $(__atividadeperiodoEventoAdd.options.formElement);
            eventoId = eventoId || '';

            if (!$.isNumeric(eventoId)) {
                $form[0].reset();
                $form.find('[name=eventoId]').val('');
                __atividadeperiodoEventoAdd.validate();

                return;
            }
        };

        this.pesquisaAtividadeperiodoEvento = function (eventoId, callback) {
            var $form = $(__atividadeperiodoEventoAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            eventoId = eventoId || '';

            if (!$.isNumeric(eventoId)) {
                $form[0].reset();
                $form.find('[name=eventoId]').val('');
                __atividadeperiodoEventoAdd.validate();

                return;
            }

            __atividadeperiodoEventoAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + eventoId,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __atividadeperiodoEventoAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }


                    __atividadeperiodoEventoAdd.removeOverlay($form);
                },
                erro: function () {
                    __atividadeperiodoEventoAdd.showNotificacaoDanger('Erro desconhecido!');
                    __atividadeperiodoEventoAdd.removeOverlay($form);
                }
            });
        };


        this.setCallback = function (callback, func) {
            __atividadeperiodoEventoAdd.options.callback[callback] = func;
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.atividadeperiodoEventoAdd = function (params) {
        params = $.extend([], arguments);

        if (params.length == 1) {
            params = params[0];
        }

        var method = false;
        var retval = false;

        if (typeof params === 'string' || typeof AtividadeperiodoEventoAdd[params] === 'function') {
            method = params;
            params = undefined;
        } else {
            method = (params || [false])[0];

            if (typeof method === 'string' || typeof AtividadeperiodoEventoAdd[method] === 'function') {
                params = params.slice(1)[0];
            }
        }

        var ret = [];

        var obj = $(window).data("universa.atividades.atividadeperiodo-evento.add");

        if (!obj) {
            obj = new AtividadeperiodoEventoAdd();
            obj.run(params);
            $(window).data('universa.atividades.atividadeperiodo-evento.add', obj);
        } else if (method) {
            retval = params ? obj[method](params) : obj[method]();
        }

        ret.push(retval === false ? obj : retval);

        if (ret.length == 1) {
            ret = ret.pop();
        }

        return ret;
    };
})(window.jQuery, window, document);