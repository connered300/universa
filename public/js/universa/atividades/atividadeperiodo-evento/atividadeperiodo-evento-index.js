(function ($, window, document) {
    'use strict';
    var AtividadeperiodoEventoIndex = function () {
        VersaShared.call(this);
        var __atividadeperiodoEventoIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            data: {},
            datatables: {
                atividadeperiodoEvento: null
            }
        };

        this.setSteps = function () {
            var $dataTableAtividadeperiodoEvento = $('#dataTableAtividadeperiodoEvento');
            var colNum = -1;
            __atividadeperiodoEventoIndex.options.datatables.atividadeperiodoEvento = $dataTableAtividadeperiodoEvento.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __atividadeperiodoEventoIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [
                            {class: 'btn-primary item-edit', icon: 'fa-pencil'},
                            {class: 'btn-danger item-remove', icon: 'fa-times'}
                        ];

                        for (var row in data) {
                            data[row]['acao'] = __atividadeperiodoEventoIndex.createBtnGroup(btns);
                            data[row]['evento_data'] = __atividadeperiodoEventoIndex.formatDate(data[row]['evento_data']);
                            data[row]['evento_lotacao'] = data[row]['evento_lotacao'] + (parseInt(data[row]['evento_lotacao']) > 1 ? ' Pessoas' : ' Pessoa');
                        }

                        __atividadeperiodoEventoIndex.removeOverlay($('#container-atividadeperiodo-evento'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "evento_id", targets: ++colNum, data: "evento_id"},
                    {name: "evento_nome", targets: ++colNum, data: "evento_nome"},
                    {name: "evento_responsavel", targets: ++colNum, data: "evento_responsavel"},
                    {name: "evento_tipo", targets: ++colNum, data: "evento_tipo"},
                    {name: "evento_lotacao", targets: ++colNum, data: "evento_lotacao"},
                    {name: "evento_data", targets: ++colNum, data: "evento_data"},
                    {name: "evento_horas_validas", targets: ++colNum, data: "evento_horas_validas"},

                    {name: "evento_id", targets: ++colNum, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $dataTableAtividadeperiodoEvento.on('click', '.item-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __atividadeperiodoEventoIndex.options.datatables.atividadeperiodoEvento.fnGetData($pai);

                if (!__atividadeperiodoEventoIndex.options.ajax) {
                    location.href = __atividadeperiodoEventoIndex.options.url.edit + '/' + data['evento_id'];
                } else {
                    __atividadeperiodoEventoIndex.addOverlay(
                        $('#container-atividadeperiodo-evento'), 'Aguarde, carregando dados para edição...'
                    );
                    $.atividadeperiodoEventoAdd().pesquisaAtividadeperiodoEvento(
                        data['evento_id'],
                        function (data) {
                            __atividadeperiodoEventoIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-atividadeperiodo-evento-add').click(function (e) {
                if (__atividadeperiodoEventoIndex.options.ajax) {
                    __atividadeperiodoEventoIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $dataTableAtividadeperiodoEvento.on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __atividadeperiodoEventoIndex.options.datatables.atividadeperiodoEvento.fnGetData($pai);
                var arrRemover = {
                    eventoId: data['evento_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de evento "' + data['evento_descricao'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __atividadeperiodoEventoIndex.addOverlay(
                                $('#container-atividadeperiodo-evento'), 'Aguarde, solicitando remoção de registro de evento...'
                            );
                            $.ajax({
                                url: __atividadeperiodoEventoIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __atividadeperiodoEventoIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de evento:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __atividadeperiodoEventoIndex.removeOverlay($('#container-atividadeperiodo-evento'));
                                    } else {
                                        __atividadeperiodoEventoIndex.reloadDataTableAtividadeperiodoEvento();
                                        __atividadeperiodoEventoIndex.showNotificacaoSuccess(
                                            "Registro de evento removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {

            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#atividadeperiodo-evento-form'),
                $modal = $('#atividadeperiodo-evento-modal');
            var actionForm = $form.attr('action');

            var atividadeSecretariaNome = $("#atividadeSecretariaNome"),
                eventoNome = $("#eventoNome"),
                eventoHorasValidas = $("#eventoHorasValidas"),
                eventoLotacao = $("#eventoLotacao"),
                eventoResponsavel = $("#eventoResponsavel"),
                tipoAtividadeEvento = $("#tipoAtividadeEvento"),
                eventoTipo = $("#eventoTipo"),
                eventoData = $("#eventoData"),
                eventoDescricao = $("#eventoDescricao"),
                usuarioCadastro = $("#usuarioCadastro"),
                eventoId = $("#eventoId"),
                periodoLetivo = $("#periodoLetivo");


            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['eventoId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['eventoId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);

            $form[0].reset();

            if (!data['eventoId']) {
                atividadeSecretariaNome.select2("val", "").trigger("change");
                eventoTipo.select2("val", "").trigger("change");
                tipoAtividadeEvento.select2("val", "").trigger("change");
                periodoLetivo.select2("val", "").trigger("change");
                usuarioCadastro.val("");
                eventoId.val("");
            } else {
                data['eventoData'] = data['eventoData'] ? __atividadeperiodoEventoIndex.formatDate(data['eventoData']) : "";
                atividadeSecretariaNome.select2('data', data['arrSecretariaSelect']).trigger("change");
                tipoAtividadeEvento.select2('data', data['arrAtividadeTipoSelect']).trigger("change");
                eventoTipo.select2('data', data['arrTipo']).trigger("change");
                periodoLetivo.select2('data', data['arrPeriodoLetivo']).trigger("change");

                eventoNome.val(data['eventoNome']);
                eventoHorasValidas.val(data['eventoHorasValidas']);
                eventoLotacao.val(data['eventoLotacao']);
                eventoResponsavel.val(data['eventoResponsavel']);
                eventoData.val(data['eventoData']);
                eventoDescricao.val(data['eventoDescricao']);
                usuarioCadastro.val(data['usuarioCadastro']['id']);
                eventoId.val(data['eventoId']);
            }

            $modal.find('.modal-title').html(actionTitle + ' evento');
            $modal.modal();
            __atividadeperiodoEventoIndex.removeOverlay($('#container-atividadeperiodo-evento'));
        };

        this.reloadDataTableAtividadeperiodoEvento = function () {
            this.getDataTableAtividadeperiodoEvento().api().ajax.reload();
        };

        this.getDataTableAtividadeperiodoEvento = function () {
            if (!__atividadeperiodoEventoIndex.options.datatables.atividadeperiodoEvento) {
                if (!$.fn.dataTable.isDataTable('#dataTableAtividadeperiodoEvento')) {
                    __atividadeperiodoEventoIndex.options.datatables.atividadeperiodoEvento = $('#dataTableAtividadeperiodoEvento').DataTable();
                } else {
                    __atividadeperiodoEventoIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __atividadeperiodoEventoIndex.options.datatables.atividadeperiodoEvento;
        };

        this.run = function (opts) {
            __atividadeperiodoEventoIndex.setDefaults(opts);
            __atividadeperiodoEventoIndex.setSteps();
        };
    };

    $.atividadeperiodoEventoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.atividades.atividadeperiodo-evento.index");

        if (!obj) {
            obj = new AtividadeperiodoEventoIndex();
            obj.run(params);
            $(window).data('universa.atividades.atividadeperiodo-evento.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);