(function ($, window, document) {
    'use strict';

    var EstagiogeralNucleoIndex = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlSearch: '',
                urlEdit: '',
                urlRemove: ''
            }
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);
        };

        priv.addOverlay = function (el, mensagem) {
            mensagem = mensagem || 'Aguarde...';
            el.append('<div class="app-overlay">' + mensagem + '</div>');
        };

        priv.removeOverlay = function (el) {
            el.find('>.app-overlay').remove();
        };

        priv.setSteps = function () {
            var colNum = 0;
            priv.dataTableEstagiogeralNucleo = $('#dataTableEstagiogeralNucleo').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: priv.options.urlSearch,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary estagiogeralNucleo-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger estagiogeralNucleo-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                        }

                        priv.removeOverlay($('.widget-body'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "nucleo_id", targets: colNum++, data: "nucleo_id"},
                    {name: "nucleo_descricao", targets: colNum++, data: "nucleo_descricao"},
                    {name: "nucleo_capacidade_total", targets: colNum++, data: "nucleo_capacidade_total"},
                    {name: "nucleo_tipo", targets: colNum++, data: "nucleo_tipo"},
                    {name: "nucleo_efetivacao_inicio", targets: colNum++, data: "nucleo_efetivacao_inicio"},
                    {name: "nucleo_efetivacao_fim", targets: colNum++, data: "nucleo_efetivacao_fim"},
                    {name: "nucleo_dias_funcionamento", targets: colNum++, data: "nucleo_dias_funcionamento"},
                    {name: "nucleo_funcionamento_abertura", targets: colNum++, data: "nucleo_funcionamento_abertura"},
                    {
                        name: "nucleo_funcionamento_fechamento", targets: colNum++,
                        data: "nucleo_funcionamento_fechamento"
                    },
                    {name: "nucleo_vagas", targets: colNum++, data: "nucleo_vagas"},
                    {name: "acao", targets: colNum++, data: "nucleo_id"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $('#dataTableEstagiogeralNucleo').on('click', '.estagiogeralNucleo-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = priv.dataTableEstagiogeralNucleo.fnGetData($pai);

                location.href = priv.options.urlEdit + '/' + data['nucleo_id'];
            });

            $('#dataTableEstagiogeralNucleo').on('click', '.estagiogeralNucleo-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = priv.dataTableEstagiogeralNucleo.fnGetData($pai);
                var arrRemover = {
                    nucleoId: data['nucleo_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de estagiogeral__nucleo "' + data['nucleo_vagas'] +
                             '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        priv.addOverlay($('.widget-body'),
                                        'Aguarde, solicitando remoção de registro de estagiogeral__nucleo...');
                        $.ajax({
                            url: priv.options.urlRemove,
                            type: 'POST',
                            dataType: 'json',
                            data: arrRemover,
                            success: function (data) {
                                if (data.erro) {
                                    $.smallBox({
                                        title: "Não foi possível remover registro de estagiogeral__nucleo:",
                                        content: "<i>" + data['erroDescricao'] + "</i>",
                                        color: "#C46A69",
                                        icon: "fa fa-times",
                                        timeout: 10000
                                    });

                                    priv.removeOverlay($('.widget-body'));
                                } else {
                                    priv.dataTableEstagiogeralNucleo.api().ajax.reload(null, false);
                                    $.smallBox({
                                        title: "Sucesso!",
                                        content: "<i>Registro de estagiogeral__nucleo removido!</i>",
                                        color: "#739e73",
                                        icon: "fa fa-check-circle",
                                        timeout: 10000
                                    });
                                }
                            }
                        });
                    }
                });

                e.preventDefault();
            });
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
        };
    };

    $.estagiogeralNucleoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.atividade.estagiogeral-nucleo.index");

        if (!obj) {
            obj = new EstagiogeralNucleoIndex();
            obj.run(params);
            $(window).data('universa.estagiogeral-nucleo.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);