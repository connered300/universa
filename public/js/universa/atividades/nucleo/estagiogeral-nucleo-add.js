(function ($, window, document) {
    'use strict';

    var EstagiogeralNucleoAdd = function () {
        VersaShared.call(this);
        var __estagiogeralNucleoAdd = this;
        this.defaults = {
            url: {},
            data: {},
            value: {},
            datatables: {},
            wizardElement: '#estagiogeral-nucleo-wizard',
            formElement: '#estagiogeral-nucleo-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#nucleoTipo").select2({language: 'pt-BR'});
            $("#nucleoEfetivacaoInicio")
                .datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                })
                .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});
            $("#nucleoEfetivacaoFim")
                .datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                })
                .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});
            $("#nucleoDiasFuncionamento").select2({language: 'pt-BR'});
            $("#nucleoVagas").inputmask({showMaskOnHover: false, clearIncomplete: true, alias: 'integer'});
        };


        this.setValidations = function () {
            var submitHandler = function (form) {
                form.submit();
            };

            $(__estagiogeralNucleoAdd.options.formElement).validate({
                submitHandler: submitHandler,
                rules: {
                    nucleoId: {maxlength: 10, number: true, required: true},
                    nucleoDescricao: {maxlength: 45},
                    nucleoCapacidadeTotal: {maxlength: 45},
                    nucleoTipo: {maxlength: 7},
                    nucleoEfetivacaoInicio: {dateBR: true},
                    nucleoEfetivacaoFim: {dateBR: true},
                    nucleoDiasFuncionamento: {maxlength: 5},
                    nucleoFuncionamentoAbertura: {},
                    nucleoFuncionamentoFechamento: {},
                    nucleoVagas: {maxlength: 11, number: true},
                },
                messages: {
                    nucleoId: {
                        maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    nucleoDescricao: {maxlength: 'Tamanho máximo: 45!'},
                    nucleoCapacidadeTotal: {maxlength: 'Tamanho máximo: 45!'},
                    nucleoTipo: {maxlength: 'Tamanho máximo: 7!'},
                    nucleoEfetivacaoInicio: {dateBR: 'Informe uma data válida!'},
                    nucleoEfetivacaoFim: {dateBR: 'Informe uma data válida!'},
                    nucleoDiasFuncionamento: {maxlength: 'Tamanho máximo: 5!'},
                    nucleoFuncionamentoAbertura: {},
                    nucleoFuncionamentoFechamento: {},
                    nucleoVagas: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!'},
                },
                ignore: '.ignore'
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.estagiogeralNucleoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.atividade.estagiogeral-nucleo.add");

        if (!obj) {
            obj = new EstagiogeralNucleoAdd();
            obj.run(params);
            $(window).data('universa.estagiogeral-nucleo.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);