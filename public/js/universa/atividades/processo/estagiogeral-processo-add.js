(function ($, window, document) {
    'use strict';

    var EstagiogeralProcessoAdd = function () {
        VersaShared.call(this);
        var __estagiogeralProcessoAdd = this;
        this.defaults = {
            url: {
                estagiogeralNucleoProcesso: ''
            },
            data: {},
            value: {
                nucleoProcessoId: null
            },
            datatables: {},
            wizardElement: '#estagiogeral-processo-wizard',
            formElement: '#estagiogeral-processo-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#nucleoProcessoId").select2({
                language: 'pt-BR',
                tags: true,
                ajax: {
                    url: __estagiogeralProcessoAdd.options.url.estagiogeralNucleoProcesso,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.nucleo_processo_id, id: el.nucleo_processo_id};
                        });

                        return {results: transformed};
                    }
                },
                allowClear: true,
                initSelection: function (element, callback) {
                    callback(__estagiogeralProcessoAdd.getNucleoProcessoId());
                }
            });
            $('#nucleoProcessoId').select2("val", __estagiogeralProcessoAdd.getNucleoProcessoId());
            $("#estagioProcessoDataInicio")
                .datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                })
                .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});
            $("#estagioProcessoDataFim")
                .datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                })
                .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});
        };


        this.setNucleoProcessoId = function (nucleoProcessoId) {
            this.options.value.NucleoProcessoId = nucleoProcessoId || null;
        };

        this.getNucleoProcessoId = function () {
            return priv.options.value.NucleoProcessoId || null;
        };

        this.setValidations = function () {
            var submitHandler = function (form) {
                form.submit();
            };

            $(__estagiogeralProcessoAdd.options.formElement).validate({
                submitHandler: submitHandler,
                rules: {
                    estagioProcessoId: {maxlength: 11, number: true, required: true},
                    nucleoProcessoId: {maxlength: 10, number: true, required: true},
                    estagioProcessoDescricao: {maxlength: 45, required: true},
                    estagioProcessoDataInicio: {required: true, dateBR: true},
                    estagioProcessoDataFim: {dateBR: true},
                },
                messages: {
                    estagioProcessoId: {
                        maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    nucleoProcessoId: {
                        maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    estagioProcessoDescricao: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                    estagioProcessoDataInicio: {required: 'Campo obrigatório!', dateBR: 'Informe uma data válida!'},
                    estagioProcessoDataFim: {dateBR: 'Informe uma data válida!'},
                },
                ignore: '.ignore'
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.estagiogeralProcessoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.atividade.estagiogeral-processo.add");

        if (!obj) {
            obj = new EstagiogeralProcessoAdd();
            obj.run(params);
            $(window).data('universa.estagiogeral-processo.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);