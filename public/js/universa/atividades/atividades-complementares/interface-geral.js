(function ($, window, document) {
    'use strict';
    var AlunoAtividades = function () {
        VersaShared.call(this);
        var __alunoAtividades = this;

        this.defaults = {
            url: {
                atividadesAlunoSearch: "",
                atividadesAlunoAdd: "",
                atividadesAlunoEdit: "",
                atividadesAlunoRemove: "",
                atividadesPesSearch: ""
            },
            datatables: {
                eventos: null,
                atividades: null
            },
            data: {
                alunoInformacoes: null,
                statusAtividades: null,
                dataDataTablesEvento: []
            }
        };

        this.pesquisaAluno = {
            setFields: function () {
                $('#alunoPesquisa').select2({
                    ajax: {
                        url: __alunoAtividades.options.url.atividadesAlunoSearch,
                        dataType: 'json',
                        data: function (query) {
                            return {
                                query: query,
                                buscarAlunoMatricula: true
                            };
                        },
                        results: function (data) {
                            var resultado = $.map(data, function (elements) {
                                elements.text = elements.pesNome;
                                elements.id = elements.alunocursoId;

                                return elements;
                            });

                            return {
                                results: resultado
                            };
                        }
                    },
                    minimumInputLength: 3
                }).on("change", function (e) {

                    __alunoAtividades.addOverlay($("#widget-grid"),"Aguarde, carregando as informações!");

                    //pegando o aluno selecionado
                    __alunoAtividades.options.data.alunoInformacoes = e.added;

                    __alunoAtividades.pesquisaAluno.atualizaInformacoesAluno(e.added);
                    __alunoAtividades.pesquisaAluno.buscaAtividadesComplementaresAluno($(this).val());
                    __alunoAtividades.datatables.atualizaDatatables();

                    $('#alunoAbasAviso').show();

                    if (this.value) {
                        $('#alunoAbasAviso').hide();
                    }
                });
            },
            buscaAtividadesComplementaresAluno: function (alunocursoId) {
                $.ajax({
                    url: __alunoAtividades.options.url.atividadesAlunoSearch,
                    method: 'POST',
                    dataType: 'json',
                    data: {buscarStatusAtividades: true, alunocursoId: alunocursoId},
                    success: function (resultado) {
                        __alunoAtividades.options.data.statusAtividades = resultado;
                        __alunoAtividades.pesquisaAluno.atualizaInformacoesAluno(resultado);
                    }
                });
            },
            atualizaInformacoesAluno: function (infoAluno) {
                // preenche os valores de informações de aluno
                for (var field in infoAluno) {
                    var atual = infoAluno[field] || '-';
                    $("p[data-info='" + field + "']").html(atual);
                }

                $('#alunoAbas').show();
            }
        };

        this.datatables = {
            setFields: function (tabelaDom, data, column, order, dataSource) {
                return tabelaDom.DataTable({
                    "processing": true,
                    "serverSide": true,
                    "searching": false,
                    "paging": false,
                    "ajax": {
                        "url": __alunoAtividades.options.url.atividadesAlunoSearch,
                        "type": "POST",
                        "data": $.extend({
                            alunocursoId: __alunoAtividades.options.data.alunoInformacoes.alunocursoId
                        }, data),
                        dataSrc: function (json) {
                            __alunoAtividades.options.data.dataDataTablesEvento = json['totalizacoes'];
                            if (dataSource !== undefined && typeof dataSource == "function") {
                                return dataSource(json);
                            }
                            return json.data || [];
                        }
                    },
                    columnDefs: column || [],
                    order: order || [],
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",

                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    }
                });
            },
            atualizaDatatables: function () {

                if (__alunoAtividades.options.datatables.eventos) {

                    __alunoAtividades.options.datatables.eventos.api().destroy();
                }

                var colNum = 0;
                __alunoAtividades.options.datatables.eventos = $('#tbEvento').dataTable({
                    processing: true,
                    serverSide: true,
                    searching: false,
                    ajax: {
                        url: __alunoAtividades.options.url.atividadesPesSearch,
                        type: "POST",
                        data: function (d) {
                            d.filter = d.filter || {};
                            d.filter['pes_id'] = __alunoAtividades.options.data.alunoInformacoes.pesId;
                            d.filter['totalHoras'] = true;
                            return d;
                        },
                        dataSrc: function (json) {

                            if (json.totalHoras) {
                                $('#totalHorasEvento').html("Total de Horas: <span>" + json.totalHoras + "</span>");
                            }
                            return json.data;
                        }
                    },
                    columnDefs: [
                        {name: "total_horas", targets: colNum++, data: "total_horas"},
                        {name: "atividadeatividade_descricao", targets: colNum++, data: "atividadeatividade_descricao"}
                    ],
                    "dom": "fr" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                    "autoWidth": true,
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "Procurar: ",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    },
                    order: [[1, 'desc']]
                });

                if (__alunoAtividades.options.datatables.atividades) {

                    __alunoAtividades.options.datatables.atividades.api().destroy();
                }

                var colNum = 0;
                __alunoAtividades.options.datatables.atividades = $("#tbAtividades").dataTable({
                    processing: true,
                    serverSide: true,
                    searching: false,
                    ajax: {
                        url: __alunoAtividades.options.url.atividadesPesSearch,
                        type: "POST",
                        data: function (d) {
                            d.filter = d.filter || {};
                            d.filter['pes_id'] = __alunoAtividades.options.data.alunoInformacoes.pesId;
                            d.filter['eventosAluno'] = true;
                            return d;
                        },
                        dataSrc: function (json) {

                            if (json.totalHoras) {
                                $('#totalHoras').html("Total de Horas: <span>" + json.totalHoras + "</span>");
                            }
                            return json.data;
                        }
                    },
                    columnDefs: [
                        {
                            name: "aluno_atividade_data_realizacao",
                            targets: colNum++,
                            data: "aluno_atividade_data_realizacao"
                        },
                        {name: "evento_nome", targets: colNum++, data: "evento_nome"},
                        {name: "atividadeatividade_descricao", targets: colNum++, data: "atividadeatividade_descricao"},
                        {name: "per_nome", targets: colNum++, data: "per_nome"},
                        {name: "evento_responsavel", targets: colNum++, data: "evento_responsavel"},
                        {
                            name: "aluno_atividade_horas_validas",
                            targets: colNum++,
                            data: "aluno_atividade_horas_validas"
                        }
                    ],
                    "dom": "fr" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                    "autoWidth": true,
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "Procurar: ",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    },
                    order: [[0, 'desc']]
                });

                setTimeout(function(){ __alunoAtividades.removeOverlay($("#widget-grid")); }, 3000)

            },
            sources: {
                dataSourceAtividade: function (json) {
                    /*DataTable Evento*/
                    var dataEvento = __alunoAtividades.options.data.dataDataTablesEvento || [],
                        totalHorasEvento = dataEvento['totalHorasEvento'];

                    if (totalHorasEvento) {
                        $('#totalHorasEvento').html("Total de Horas: <span>" + totalHorasEvento + "</span>");
                    }

                    /*DataTable Atividade*/
                    var statusAtividades = __alunoAtividades.options.data.statusAtividades || [],
                        totalHoras = statusAtividades['totalHoras'] || 0;

                    if (totalHoras) {
                        $('#totalHoras').html("Total de Horas: <span>" + totalHoras + "</span>");
                    }


                    return json.data || [];

                    /* comentário pois será add depois ao cliente, commitando pra fazer uma
                     entrega
                     for (var row in data) {

                     var alunoAtividadeId = data.aluno_atividade_id;

                     var iEdit = $("<i>").attr("class", "fa fa-pencil fa-inverse");
                     var iRemove = $("<i>").attr("class", "fa fa-times fa-inverse");

                     var btn1 = $("<a>").attr({
                     class: "btn btn-primary btn-xs",
                     id: 'edit_' + alunoAtividadeId,
                     'data-acao': "editar",
                     'data-toggle': "modal",
                     'data-target': "#atividadeAddEdit",
                     'data-atividadeId': alunoAtividadeId,
                     }).append(iEdit);

                     var btn2 = $("<a>").attr({
                     class: "btn btn-danger btn-xs",
                     'data-toggle': "modal",
                     'data-target': "#modal-remove-atividade",
                     id: 'remove_' + alunoAtividadeId,
                     'data-atividadeId': alunoAtividadeId,
                     }).append(iRemove);

                     data[row]['btn'] = btn1.get(0).outerHTML + btn2.get(0).outerHTML;
                     }*/

                    return data;
                }
            }
        };

        this.run = function (opts) {
            __alunoAtividades.setDefaults(opts);
            __alunoAtividades.pesquisaAluno.setFields();
            //__alunoAtividades.atividades.setFields();
            //__alunoAtividades.alunoPortfolio.buscaConfPortfolio();
        };
    };

    $.alunoAtividadesComplementares = function (params) {
        params = params || [];

        var obj = $(window).data("universa.aluno.atividades-complementares");

        if (!obj) {
            obj = new AlunoAtividades();
            obj.run(params);
            $(window).data('universa.aluno.atividades-complementares', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);