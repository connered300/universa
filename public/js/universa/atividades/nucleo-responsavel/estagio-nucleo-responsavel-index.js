(function ($, window, document) {
    'use strict';

    var EstagioNucleoResponsavelIndex = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlSearch: '',
                urlEdit: '',
                urlRemove: ''
            }
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);
        };

        priv.addOverlay = function (el, mensagem) {
            mensagem = mensagem || 'Aguarde...';
            el.append('<div class="app-overlay">' + mensagem + '</div>');
        };

        priv.removeOverlay = function (el) {
            el.find('>.app-overlay').remove();
        };

        priv.setSteps = function () {
            var colNum = 0;
            priv.dataTableEstagioNucleoResponsavel = $('#dataTableEstagioNucleoResponsavel').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: priv.options.urlSearch,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary estagioNucleoResponsavel-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger estagioNucleoResponsavel-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                        }

                        priv.removeOverlay($('.widget-body'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "nucleoresponsavel_id", targets: colNum++, data: "nucleoresponsavel_id"},
                    {name: "pes_id", targets: colNum++, data: "pes_id"},
                    {name: "nucleo_id", targets: colNum++, data: "nucleo_id"},
                    {name: "nucleoresponsavel_inicio", targets: colNum++, data: "nucleoresponsavel_inicio"},
                    {name: "nucleoresponsavel_fim", targets: colNum++, data: "nucleoresponsavel_fim"},
                    {name: "nucleoresponsavel_observacoes", targets: colNum++, data: "nucleoresponsavel_observacoes"},
                    {name: "nucleo_responsavel_nivel", targets: colNum++, data: "nucleo_responsavel_nivel"},
                    {name: "acao", targets: colNum++, data: "nucleoresponsavel_id"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $('#dataTableEstagioNucleoResponsavel').on('click', '.estagioNucleoResponsavel-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = priv.dataTableEstagioNucleoResponsavel.fnGetData($pai);

                location.href = priv.options.urlEdit + '/' + data['nucleoresponsavel_id'];
            });

            $('#dataTableEstagioNucleoResponsavel').on('click', '.estagioNucleoResponsavel-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = priv.dataTableEstagioNucleoResponsavel.fnGetData($pai);
                var arrRemover = {
                    nucleoresponsavelId: data['nucleoresponsavel_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de estagio__nucleo_responsavel "' +
                             data['nucleo_responsavel_nivel'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        priv.addOverlay($('.widget-body'),
                                        'Aguarde, solicitando remoção de registro de estagio__nucleo_responsavel...');
                        $.ajax({
                            url: priv.options.urlRemove,
                            type: 'POST',
                            dataType: 'json',
                            data: arrRemover,
                            success: function (data) {
                                if (data.erro) {
                                    $.smallBox({
                                        title: "Não foi possível remover registro de estagio__nucleo_responsavel:",
                                        content: "<i>" + data['erroDescricao'] + "</i>",
                                        color: "#C46A69",
                                        icon: "fa fa-times",
                                        timeout: 10000
                                    });

                                    priv.removeOverlay($('.widget-body'));
                                } else {
                                    priv.dataTableEstagioNucleoResponsavel.api().ajax.reload(null, false);
                                    $.smallBox({
                                        title: "Sucesso!",
                                        content: "<i>Registro de estagio__nucleo_responsavel removido!</i>",
                                        color: "#739e73",
                                        icon: "fa fa-check-circle",
                                        timeout: 10000
                                    });
                                }
                            }
                        });
                    }
                });

                e.preventDefault();
            });
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
        };
    };

    $.estagioNucleoResponsavelIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.atividade.estagio-nucleo-responsavel.index");

        if (!obj) {
            obj = new EstagioNucleoResponsavelIndex();
            obj.run(params);
            $(window).data('universa.estagio-nucleo-responsavel.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);