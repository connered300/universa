(function ($, window, document) {
    'use strict';

    var EstagioNucleoResponsavelAdd = function () {
        VersaShared.call(this);
        var __estagioNucleoResponsavelAdd = this;
        this.defaults = {
            url: {
                pessoa: '',
                estagiogeralNucleo: ''
            },
            data: {},
            value: {
                pesId: null,
                nucleoId: null
            },
            datatables: {},
            wizardElement: '#estagio-nucleo-responsavel-wizard',
            formElement: '#estagio-nucleo-responsavel-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#pesId").select2({
                language: 'pt-BR',
                tags: true,
                ajax: {
                    url: __estagioNucleoResponsavelAdd.options.url.pessoa,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.pes_id, id: el.pes_id};
                        });

                        return {results: transformed};
                    }
                },
                allowClear: true,
                initSelection: function (element, callback) {
                    callback(__estagioNucleoResponsavelAdd.getPesId());
                }
            });
            $('#pesId').select2("val", __estagioNucleoResponsavelAdd.getPesId());
            $("#nucleoId").select2({
                language: 'pt-BR',
                tags: true,
                ajax: {
                    url: __estagioNucleoResponsavelAdd.options.url.estagiogeralNucleo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.nucleo_id, id: el.nucleo_id};
                        });

                        return {results: transformed};
                    }
                },
                allowClear: true,
                initSelection: function (element, callback) {
                    callback(__estagioNucleoResponsavelAdd.getNucleoId());
                }
            });
            $('#nucleoId').select2("val", __estagioNucleoResponsavelAdd.getNucleoId());
            $("#nucleoresponsavelInicio")
                .datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                })
                .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});
            $("#nucleoresponsavelFim")
                .datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                })
                .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});
            $("#nucleoResponsavelNivel").select2({language: 'pt-BR'});
        };


        this.setPesId = function (pesId) {
            this.options.value.PesId = pesId || null;
        };

        this.getPesId = function () {
            return priv.options.value.PesId || null;
        };

        this.setNucleoId = function (nucleoId) {
            this.options.value.NucleoId = nucleoId || null;
        };

        this.getNucleoId = function () {
            return priv.options.value.NucleoId || null;
        };

        this.setValidations = function () {
            var submitHandler = function (form) {
                form.submit();
            };

            $(__estagioNucleoResponsavelAdd.options.formElement).validate({
                submitHandler: submitHandler,
                rules: {
                    nucleoresponsavelId: {maxlength: 11, number: true, required: true},
                    pesId: {maxlength: 11, number: true, required: true},
                    nucleoId: {maxlength: 10, number: true, required: true},
                    nucleoresponsavelInicio: {dateBR: true},
                    nucleoresponsavelFim: {dateBR: true},
                    nucleoresponsavelObservacoes: {maxlength: 45},
                    nucleoResponsavelNivel: {maxlength: 10},
                },
                messages: {
                    nucleoresponsavelId: {
                        maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    pesId: {
                        maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    nucleoId: {
                        maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    nucleoresponsavelInicio: {dateBR: 'Informe uma data válida!'},
                    nucleoresponsavelFim: {dateBR: 'Informe uma data válida!'},
                    nucleoresponsavelObservacoes: {maxlength: 'Tamanho máximo: 45!'},
                    nucleoResponsavelNivel: {maxlength: 'Tamanho máximo: 10!'},
                },
                ignore: '.ignore'
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.estagioNucleoResponsavelAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.atividade.estagio-nucleo-responsavel.add");

        if (!obj) {
            obj = new EstagioNucleoResponsavelAdd();
            obj.run(params);
            $(window).data('universa.estagio-nucleo-responsavel.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);