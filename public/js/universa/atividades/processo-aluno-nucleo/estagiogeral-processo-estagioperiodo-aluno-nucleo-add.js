(function ($, window, document) {
    'use strict';

    var EstagiogeralProcessoEstagioperiodoAlunoNucleoAdd = function () {
        VersaShared.call(this);
        var __estagiogeralProcessoEstagioperiodoAlunoNucleoAdd = this;
        this.defaults = {
            url: {
                estagiogeralProcesso: '',
                estagioperiodoAlunoNucleo: ''
            },
            data: {},
            value: {
                estagioprocessoId: null,
                alunonucleoId: null
            },
            datatables: {},
            wizardElement: '#estagiogeral-processo-estagioperiodo-aluno-nucleo-wizard',
            formElement: '#estagiogeral-processo-estagioperiodo-aluno-nucleo-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#estagioprocessoId").select2({
                language: 'pt-BR',
                tags: true,
                ajax: {
                    url: __estagiogeralProcessoEstagioperiodoAlunoNucleoAdd.options.url.estagiogeralProcesso,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.estagio_processo_id, id: el.estagio_processo_id};
                        });

                        return {results: transformed};
                    }
                },
                allowClear: true,
                initSelection: function (element, callback) {
                    callback(__estagiogeralProcessoEstagioperiodoAlunoNucleoAdd.getEstagioprocessoId());
                }
            });
            $('#estagioprocessoId').select2("val",
                                            __estagiogeralProcessoEstagioperiodoAlunoNucleoAdd.getEstagioprocessoId());
            $("#alunonucleoId").select2({
                language: 'pt-BR',
                tags: true,
                ajax: {
                    url: __estagiogeralProcessoEstagioperiodoAlunoNucleoAdd.options.url.estagioperiodoAlunoNucleo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.alunonucleo_id, id: el.alunonucleo_id};
                        });

                        return {results: transformed};
                    }
                },
                allowClear: true,
                initSelection: function (element, callback) {
                    callback(__estagiogeralProcessoEstagioperiodoAlunoNucleoAdd.getAlunonucleoId());
                }
            });
            $('#alunonucleoId').select2("val", __estagiogeralProcessoEstagioperiodoAlunoNucleoAdd.getAlunonucleoId());
            $("#alunoprocessoDataInicio")
                .datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                })
                .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});
            $("#alunoprocessoDataFim")
                .datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                })
                .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});
        };


        this.setEstagioprocessoId = function (estagioprocessoId) {
            this.options.value.EstagioprocessoId = estagioprocessoId || null;
        };

        this.getEstagioprocessoId = function () {
            return priv.options.value.EstagioprocessoId || null;
        };

        this.setAlunonucleoId = function (alunonucleoId) {
            this.options.value.AlunonucleoId = alunonucleoId || null;
        };

        this.getAlunonucleoId = function () {
            return priv.options.value.AlunonucleoId || null;
        };

        this.setValidations = function () {
            var submitHandler = function (form) {
                form.submit();
            };

            $(__estagiogeralProcessoEstagioperiodoAlunoNucleoAdd.options.formElement).validate({
                submitHandler: submitHandler,
                rules: {
                    alunoprocessoId: {maxlength: 11, number: true, required: true},
                    estagioprocessoId: {maxlength: 11, number: true, required: true},
                    alunonucleoId: {maxlength: 10, number: true, required: true},
                    alunoprocessoDataInicio: {required: true, dateBR: true},
                    alunoprocessoDataFim: {dateBR: true},
                    alunoprocessoObservacoes: {maxlength: 45},
                },
                messages: {
                    alunoprocessoId: {
                        maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    estagioprocessoId: {
                        maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    alunonucleoId: {
                        maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    alunoprocessoDataInicio: {required: 'Campo obrigatório!', dateBR: 'Informe uma data válida!'},
                    alunoprocessoDataFim: {dateBR: 'Informe uma data válida!'},
                    alunoprocessoObservacoes: {maxlength: 'Tamanho máximo: 45!'},
                },
                ignore: '.ignore'
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.estagiogeralProcessoEstagioperiodoAlunoNucleoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.atividade.estagiogeral-processo-estagioperiodo-aluno-nucleo.add");

        if (!obj) {
            obj = new EstagiogeralProcessoEstagioperiodoAlunoNucleoAdd();
            obj.run(params);
            $(window).data('universa.estagiogeral-processo-estagioperiodo-aluno-nucleo.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);