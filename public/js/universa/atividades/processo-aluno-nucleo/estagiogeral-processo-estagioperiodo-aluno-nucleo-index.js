(function ($, window, document) {
    'use strict';

    var EstagiogeralProcessoEstagioperiodoAlunoNucleoIndex = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlSearch: '',
                urlEdit: '',
                urlRemove: ''
            }
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);
        };

        priv.addOverlay = function (el, mensagem) {
            mensagem = mensagem || 'Aguarde...';
            el.append('<div class="app-overlay">' + mensagem + '</div>');
        };

        priv.removeOverlay = function (el) {
            el.find('>.app-overlay').remove();
        };

        priv.setSteps = function () {
            var colNum = 0;
            priv.dataTableEstagiogeralProcessoEstagioperiodoAlunoNucleo =
                $('#dataTableEstagiogeralProcessoEstagioperiodoAlunoNucleo').dataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: priv.options.urlSearch,
                        type: "POST",
                        dataSrc: function (json) {
                            var data = json.data;
                            var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary estagiogeralProcessoEstagioperiodoAlunoNucleo-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger estagiogeralProcessoEstagioperiodoAlunoNucleo-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                            for (var row in data) {
                                data[row]['acao'] = btnGroup;
                            }

                            priv.removeOverlay($('.widget-body'));

                            return data;
                        }
                    },
                    columnDefs: [
                        {name: "alunoprocesso_id", targets: colNum++, data: "alunoprocesso_id"},
                        {name: "estagioprocesso_id", targets: colNum++, data: "estagioprocesso_id"},
                        {name: "alunonucleo_id", targets: colNum++, data: "alunonucleo_id"},
                        {name: "alunoprocesso_data_inicio", targets: colNum++, data: "alunoprocesso_data_inicio"},
                        {name: "alunoprocesso_data_fim", targets: colNum++, data: "alunoprocesso_data_fim"},
                        {name: "alunoprocesso_observacoes", targets: colNum++, data: "alunoprocesso_observacoes"},
                        {name: "acao", targets: colNum++, data: "alunoprocesso_id"}
                    ],
                    "dom": "fr" +
                           "t" +
                           "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                    "autoWidth": true,
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "Procurar: ",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    },
                    order: [[0, 'asc']]
                });

            $('#dataTableEstagiogeralProcessoEstagioperiodoAlunoNucleo').on('click',
                                                                            '.estagiogeralProcessoEstagioperiodoAlunoNucleo-edit',
                                                                            function (event) {
                                                                                var $pai = $(this).closest('tr');
                                                                                var data = priv.dataTableEstagiogeralProcessoEstagioperiodoAlunoNucleo.fnGetData($pai);

                                                                                location.href =
                                                                                    priv.options.urlEdit + '/' +
                                                                                    data['alunoprocesso_id'];
                                                                            });

            $('#dataTableEstagiogeralProcessoEstagioperiodoAlunoNucleo').on('click',
                                                                            '.estagiogeralProcessoEstagioperiodoAlunoNucleo-remove',
                                                                            function (e) {
                                                                                var $pai = $(this).closest('tr');
                                                                                var data = priv.dataTableEstagiogeralProcessoEstagioperiodoAlunoNucleo.fnGetData($pai);
                                                                                var arrRemover = {
                                                                                    alunoprocessoId: data['alunoprocesso_id']
                                                                                };

                                                                                $.SmartMessageBox({
                                                                                    title: "Confirme a operação:",
                                                                                    content: 'Deseja realmente remover registro de estagiogeral__processo_estagioperiodo__aluno_nucleo "' +
                                                                                             data['alunoprocesso_observacoes'] +
                                                                                             '"?',
                                                                                    buttons: "[Não][Sim]",
                                                                                }, function (ButtonPress) {
                                                                                    if (ButtonPress == "Sim") {
                                                                                        priv.addOverlay($('.widget-body'),
                                                                                                        'Aguarde, solicitando remoção de registro de estagiogeral__processo_estagioperiodo__aluno_nucleo...');
                                                                                        $.ajax({
                                                                                            url: priv.options.urlRemove,
                                                                                            type: 'POST',
                                                                                            dataType: 'json',
                                                                                            data: arrRemover,
                                                                                            success: function (data) {
                                                                                                if (data.erro) {
                                                                                                    $.smallBox({
                                                                                                        title: "Não foi possível remover registro de estagiogeral__processo_estagioperiodo__aluno_nucleo:",
                                                                                                        content: "<i>" +
                                                                                                                 data['erroDescricao'] +
                                                                                                                 "</i>",
                                                                                                        color: "#C46A69",
                                                                                                        icon: "fa fa-times",
                                                                                                        timeout: 10000
                                                                                                    });

                                                                                                    priv.removeOverlay($('.widget-body'));
                                                                                                } else {
                                                                                                    priv.dataTableEstagiogeralProcessoEstagioperiodoAlunoNucleo.api().ajax.reload(null, false);
                                                                                                    $.smallBox({
                                                                                                        title: "Sucesso!",
                                                                                                        content: "<i>Registro de estagiogeral__processo_estagioperiodo__aluno_nucleo removido!</i>",
                                                                                                        color: "#739e73",
                                                                                                        icon: "fa fa-check-circle",
                                                                                                        timeout: 10000
                                                                                                    });
                                                                                                }
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                });

                                                                                e.preventDefault();
                                                                            });
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
        };
    };

    $.estagiogeralProcessoEstagioperiodoAlunoNucleoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.atividade.estagiogeral-processo-estagioperiodo-aluno-nucleo.index");

        if (!obj) {
            obj = new EstagiogeralProcessoEstagioperiodoAlunoNucleoIndex();
            obj.run(params);
            $(window).data('universa.estagiogeral-processo-estagioperiodo-aluno-nucleo.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);