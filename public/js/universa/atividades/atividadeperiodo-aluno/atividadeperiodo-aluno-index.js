(function ($, window, document) {
    'use strict';
    var AtividadeperiodoAlunoIndex = function () {
        VersaShared.call(this);
        var __atividadeperiodoAlunoIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                atividadeperiodoAluno: null
            }
        };

        this.setSteps = function () {
            var $dataTableAtividadeperiodoAluno = $('#dataTableAtividadeperiodoAluno');
            var colNum = -1;
            __atividadeperiodoAlunoIndex.options.datatables.atividadeperiodoAluno = $dataTableAtividadeperiodoAluno.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __atividadeperiodoAlunoIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [
                            {class: 'btn-primary item-edit', icon: 'fa-pencil'},
                            {class: 'btn-danger item-remove', icon: 'fa-times'}
                        ];

                        for (var row in data) {
                            data[row]['acao'] = __atividadeperiodoAlunoIndex.createBtnGroup(btns);
                            data[row]['aluno_atividade_data_lancamento'] = __atividadeperiodoAlunoIndex.formatDate(data[row]['aluno_atividade_data_lancamento']);
                            data[row]['aluno_atividade_data_realizacao'] = __atividadeperiodoAlunoIndex.formatDate(data[row]['aluno_atividade_data_realizacao']);
                        }
                        __atividadeperiodoAlunoIndex.removeOverlay($('#container-atividadeperiodo-aluno'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "alunocurso_id", targets: ++colNum, data: "alunocurso_id"},
                    {name: "pes_nome", targets: ++colNum, data: "pes_nome"},
                    {name: "evento_nome", targets: ++colNum, data: "evento_nome"},
                    {
                        name: "aluno_atividade_data_lancamento",
                        targets: ++colNum,
                        data: "aluno_atividade_data_lancamento"
                    },
                    {
                        name: "aluno_atividade_data_realizacao",
                        targets: ++colNum,
                        data: "aluno_atividade_data_realizacao"
                    },
                    {name: "aluno_atividade_horas_reais", targets: ++colNum, data: "aluno_atividade_horas_reais"},
                    {name: "aluno_atividade_horas_validas", targets: ++colNum, data: "aluno_atividade_horas_validas"},
                    {name: "aluno_atividade_data_realizacao", targets: ++colNum, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[3, 'desc']]
            });

            $dataTableAtividadeperiodoAluno.on('click', '.item-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __atividadeperiodoAlunoIndex.options.datatables.atividadeperiodoAluno.fnGetData($pai);

                if (!__atividadeperiodoAlunoIndex.options.ajax) {
                    location.href = __atividadeperiodoAlunoIndex.options.url.edit + '/' + data['aluno_atividade_id'];
                } else {
                    __atividadeperiodoAlunoIndex.addOverlay(
                        $('#container-atividadeperiodo-aluno'), 'Aguarde, carregando dados para edição...'
                    );
                    $.atividadeperiodoAlunoAdd().pesquisaAtividadeperiodoAluno(
                        data['aluno_atividade_id'],
                        function (data) {
                            __atividadeperiodoAlunoIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-atividadeperiodo-aluno-add').click(function (e) {
                if (__atividadeperiodoAlunoIndex.options.ajax) {
                    __atividadeperiodoAlunoIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $dataTableAtividadeperiodoAluno.on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __atividadeperiodoAlunoIndex.options.datatables.atividadeperiodoAluno.fnGetData($pai);
                var arrRemover =
                {atividade: data['aluno_atividade_id']};

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de atividade do aluno "' + data['alunocurso_id'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __atividadeperiodoAlunoIndex.addOverlay(
                                $('#container-atividadeperiodo-aluno'), 'Aguarde, removendo registro de atividade do aluno...'
                            );
                            $.ajax({
                                url: __atividadeperiodoAlunoIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __atividadeperiodoAlunoIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de atividade do aluno:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __atividadeperiodoAlunoIndex.removeOverlay($('#container-atividadeperiodo-aluno'));
                                    } else {
                                        __atividadeperiodoAlunoIndex.removeOverlay($('#container-atividadeperiodo-aluno'));
                                        __atividadeperiodoAlunoIndex.reloadDataTableAtividadeperiodoAluno();
                                        __atividadeperiodoAlunoIndex.showNotificacaoSuccess(
                                            "Registro de atividade do aluno removido!"
                                        );

                                    }
                                }
                            });
                        }, 400);
                    }
                });
                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};

            var usuarioCadastro = $("#usuarioCadastro"),
                alunoAtividadeDataLancamento = $("#alunoAtividadeDataLancamento"),
                alunoper = $("#alunoper"),
                evento = $("#evento"),
                alunoAtividadeData = $("#alunoAtividadeDataRealizacao"),
                alunoAtividadeHorasReais = $("#alunoAtividadeHorasReais"),
                alunoAtividadeHorasValidas = $("#alunoAtividadeHorasValidas"),
                alunoAtividadeObservacao = $("#alunoAtividadeObservacao"),
                periodoLetivo = $("#periodoLetivo"),
                actionTitle = 'Cadastrar',
                $form = $('#atividadeperiodo-aluno-form'),
                $modal = $('#atividadeperiodo-aluno-modal'),
                actionForm = $form.attr('action');

            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['alunoAtividadeId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['alunoAtividadeId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);

            $form[0].reset();
            evento.select2("enable", true).trigger("change");

            if (data['alunoAtividadeId']) {
                $("#alunoAtividadeId").val(data['alunoAtividadeId']);
                usuarioCadastro.val(data['usuarioCadastro']['id']);
                alunoAtividadeDataLancamento.val(data['alunoAtividadeDataLancamento']);
                alunoAtividadeHorasValidas.val(data['alunoAtividadeHorasValidas']);
                alunoAtividadeObservacao.val(data['alunoAtividadeObservacao']);
                alunoAtividadeData.val(__atividadeperiodoAlunoIndex.formatDate(data['alunoAtividadeDataRealizacao']));
                alunoper.select2("data", data['arrAcadPeriodoAluno']).trigger("change");
                evento.select2("data", data['arrEventoDados']).trigger("change");

                if (data['perId']) {
                    var arrdata = {'id': data['perId'], 'text': data['perNome']};
                    periodoLetivo.select2("data", arrdata).trigger("change");
                }

                periodoLetivo.select2("disable", true).trigger("change");

            } else {
                $("#alunoAtividadeId").val('');
                alunoAtividadeDataLancamento.val('');
                alunoAtividadeHorasValidas.val('');
                alunoAtividadeObservacao.val('');
                usuarioCadastro.val('');
                alunoper.select2("val", "").trigger("change");
                evento.select2("val", "").trigger("change");
                periodoLetivo.select2("val", "").trigger("change");
                periodoLetivo.select2("disable");
            }

            var arrAcadPeriodoAluno = data['arrAcadPeriodoAluno'] || [];

            $modal.find('.modal-title').html(actionTitle + ' Atividade');
            $modal.modal();

            __atividadeperiodoAlunoIndex.removeOverlay($('#container-atividadeperiodo-aluno'));
            alunoper.select2("disable", true).trigger("change");

            $('.form-control-static.' + 'alunoCurso').html(arrAcadPeriodoAluno['cursoNome'] ? arrAcadPeriodoAluno['cursoNome'] : '-');
            $('.form-control-static.' + 'alunoMatricula').html(arrAcadPeriodoAluno['alunoCursoId'] ? arrAcadPeriodoAluno['alunoCursoId'] : '-');
            $('.form-control-static.' + 'situacaoAluno').html(arrAcadPeriodoAluno['id'] ? arrAcadPeriodoAluno['alunoSituacao'] : '-');

            alunoAtividadeHorasReais.val(data ? data['alunoAtividadeHorasReais'] : "");
            alunoAtividadeData.val(data ? __atividadeperiodoAlunoIndex.formatDate(data['alunoAtividadeDataRealizacao']) : "");
        };

        this.reloadDataTableAtividadeperiodoAluno = function () {
            __atividadeperiodoAlunoIndex.addOverlay($('#container-atividadeperiodo-aluno'), "Aguarde carregando dados da listagem!");
            this.getDataTableAtividadeperiodoAluno().api().ajax.reload();
        };

        this.getDataTableAtividadeperiodoAluno = function () {
            if (!__atividadeperiodoAlunoIndex.options.datatables.atividadeperiodoAluno) {
                if (!$.fn.dataTable.isDataTable('#dataTableAtividadeperiodoAluno')) {
                    __atividadeperiodoAlunoIndex.options.datatables.atividadeperiodoAluno = $('#dataTableAtividadeperiodoAluno').DataTable();
                } else {
                    __atividadeperiodoAlunoIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __atividadeperiodoAlunoIndex.options.datatables.atividadeperiodoAluno;
        };

        this.run = function (opts) {
            __atividadeperiodoAlunoIndex.setDefaults(opts);
            __atividadeperiodoAlunoIndex.setSteps();
        };
    };

    $.atividadeperiodoAlunoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.atividades.atividadeperiodo-aluno.index");

        if (!obj) {
            obj = new AtividadeperiodoAlunoIndex();
            obj.run(params);
            $(window).data('universa.atividades.atividadeperiodo-aluno.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);