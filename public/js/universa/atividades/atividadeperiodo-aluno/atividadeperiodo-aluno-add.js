(function ($, window, document) {
    'use strict';

    var AtividadeperiodoAlunoAdd = function () {
        VersaShared.call(this);
        var __atividadeperiodoAlunoAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                acadperiodoAluno: '',
                atividadeAlunoEdit: '',
                atividadeperiodoEvento: '',
                acadgeralAlunocurso: '',
                periodoLetivo: ''
            },
            data: {
                arrEvento: {}
            },
            value: {
                alunoper: null,
                evento: null,
                usuarioCadastro: null
            },
            datatables: {},
            wizardElement: '#atividadeperiodo-aluno-wizard',
            formElement: '#atividadeperiodo-aluno-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            var $form = $(__atividadeperiodoAlunoAdd.options.formElement);
            var $periodoLetivo = $form.find("[name=periodoLetivo]"),
                $usuarioCadastro = $form.find("[name=usuarioCadastro]"),
                $alunoAtividadeDataLancamento = $form.find("[name=alunoAtividadeDataLancamento]"),
                $alunoper = $form.find("[name=alunoper]"),
                $evento = $form.find("[name=evento]"),
                $alunoAtividadeData = $form.find("[name=alunoAtividadeDataRealizacao]"),
                $alunoAtividadeHorasReais = $form.find("[name=alunoAtividadeHorasReais]"),
                $alunoAtividadeHorasValidas = $form.find("[name=alunoAtividadeHorasValidas]"),
                $alunoAtividadeObservacao = $form.find("[name=alunoAtividadeObservacao]");

            $('.form-control-static.' + 'alunoCurso').html('-');
            $('.form-control-static.' + 'alunoMatricula').html('-');

            __atividadeperiodoAlunoAdd.iniciarElementoDatePicker($alunoAtividadeData);

            $periodoLetivo.select2({
                language: 'pt-BR',
                allowClear: true,
                ajax: {
                    url: __atividadeperiodoAlunoAdd.options.url.periodoLetivo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {
                            query: query,
                            limite: 20
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.per_id;
                            el.text = el.per_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            }).on("change", function () {
                $alunoper.select2("val", '').trigger("change");
                $alunoper.select2($periodoLetivo.val() ? "enable" : 'disable');
            });

            $alunoper.select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __atividadeperiodoAlunoAdd.options.url.acadperiodoAluno,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {
                            query: query,
                            limite: 20,
                            somenteAlunosComMatricula: true,
                            perId: __atividadeperiodoAlunoAdd.getPeriodoLetivo()
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.alunoper_id;
                            el.text = el.alunocursoId + '-' + el.pesNome;
                            return el;
                        });

                        return {results: transformed};
                    }
                }
            }).on("change", function () {
                if ($alunoper.val()) {
                    var dados = $alunoper.select2("data");

                    $('.form-control-static.' + 'alunoCurso').html(dados['cursoNome'] ? dados['cursoNome'] : '-');
                    $('.form-control-static.' + 'alunoMatricula').html(dados['alunoper_id'] ? dados['alunocurso_id'] : '-');
                    $('.form-control-static.' + 'situacaoAluno').html(dados['situacaoPeriodo'] ? dados['situacaoPeriodo'] : '-');

                } else {
                    $('.form-control-static.' + 'alunoCurso').html('-');
                    $('.form-control-static.' + 'alunoMatricula').html('-');
                    $('.form-control-static.' + 'situacaoAluno').html('-');
                }
            });

            $evento.select2({
                language: 'pt-BR',
                ajax: {
                    url: __atividadeperiodoAlunoAdd.options.url.atividadeperiodoEvento,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {
                            query: query,
                            perId: __atividadeperiodoAlunoAdd.getPeriodoLetivo(),
                            ordenar: 'dataEvento'
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.evento_id;
                            el.text = el.evento_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                },
                createSearchChoice: function (term) {
                    return {
                        id: term,
                        evento_id: '',
                        evento_nome: term,
                        text: term + ' (Criar novo registro)'
                    };
                },
                allowClear: true,
                minimumInputLength: 1
            }).on("change", function () {
                $alunoAtividadeData.prop('disabled', $evento.val() ? false : true);
                $alunoAtividadeHorasReais.prop('disabled', $evento.val() ? false : true);
                $alunoAtividadeHorasValidas.prop('disabled', $evento.val() ? false : true);
                $alunoAtividadeObservacao.prop('disabled', $evento.val() ? false : true);
                $periodoLetivo.select2("val", '').trigger('change');
                $periodoLetivo.select2($evento.val() ? 'enable' : 'disable');

                var arrEvento = $evento.select2("data") || [];

                __atividadeperiodoAlunoAdd.setDadosEvento(arrEvento);

                if (Object.keys(arrEvento).length > 0) {
                    $alunoAtividadeData.val(
                        __atividadeperiodoAlunoAdd.formatDate(arrEvento['evento_data'] || arrEvento['eventoData'])
                    );
                    $alunoAtividadeHorasReais.val(
                        parseInt(arrEvento['hora_maxima'] || arrEvento['horaMaxima'] || 0)
                    );
                } else {
                    $alunoAtividadeData.val("");
                    $alunoAtividadeHorasReais.val("");
                }

                var data = $(this).select2('data') || [];

                if (Object.keys(arrEvento).length > 0) {
                    if (!$.isNumeric(data['id']) && (data['evento_nome'] || data['eventoNome'])) {
                        $(this).next('.editarEvento').click();
                    }
                }
            });

            $('.editarEvento').click(function (e) {
                var $elemento = $(this).parent().find('>input');
                var id = $elemento.attr('id');
                var data = $elemento.select2('data') || [];
                var arrDataPeriodo = $periodoLetivo.select2('data') || [];


                $('#atividadeperiodo-evento-modal').modal('show');
                var atividadeEvento = $.atividadeperiodoEventoAdd();

                atividadeEvento.setCallback('aposSalvar', function (dados, retorno) {
                    dados['id'] = dados['evento_id'] || dados['eventoId'];
                    dados['text'] = dados['evento_nome'] || dados['eventoNome'];

                    $elemento.select2('data', dados);
                    $elemento.trigger('change');

                    $('#atividadeperiodo-evento-modal').modal('hide');
                });

                if (!atividadeEvento.pesquisaAtividadeperiodoEvento($("#evento").val())) {
                    $('#atividadeperiodo-evento-modal [name=eventoNome]').val(
                        data['evento_nome'] || data['eventoNome'] || ''
                    );
                    $('#atividadeperiodo-evento-modal [name=periodoLetivo]').select2('data', arrDataPeriodo || '');
                }

                e.preventDefault();
            });

            $alunoAtividadeHorasReais.inputmask("integer");
            $alunoAtividadeHorasValidas.inputmask("integer");
        };

        this.getPeriodoLetivo = function () {
            var $form = $(__atividadeperiodoAlunoAdd.options.formElement);
            var $periodoLetivo = $form.find("[name=periodoLetivo]");

            return parseInt($periodoLetivo.val()) ? parseInt($periodoLetivo.val()) : null;
        };

        this.getDadosEvento = function () {
            return this.options.data.arrEvento || null;
        };

        this.setDadosEvento = function (arrEvento) {
            this.options.data.arrEvento = arrEvento || null;
        };

        this.setAlunoper = function (alunoper) {
            this.options.value.alunoper = alunoper || null;
        };

        this.getAlunoper = function () {
            return this.options.value.alunoper || null;
        };

        this.setEvento = function (evento) {
            this.options.value.evento = evento || null;
        };

        this.getEvento = function () {
            return this.options.value.evento || null;
        };

        this.setUsuarioCadastro = function (usuarioCadastro) {
            this.options.value.usuarioCadastro = usuarioCadastro || null;
        };

        this.getUsuarioCadastro = function () {
            return this.options.value.usuarioCadastro || null;
        };
        this.setValidations = function () {
            __atividadeperiodoAlunoAdd.options.validator.settings.rules = {
                periodoLetivo: {
                    required: function () {
                        return $("#alunoAtividadeId").val() ? false : true;
                    }
                },
                alunoper: {required: true, number: true},
                evento: {
                    number: true,
                    required: function () {
                        var arrEvento = __atividadeperiodoAlunoAdd.getDadosEvento();

                        if (arrEvento) {
                            if (arrEvento['eventoId']) {
                                return false
                            }
                        }
                        return true;
                    }
                },
                alunoAtividadeOrigemCadastro: {maxlength: 8, required: true},
                alunoAtividadeDataRealizacao: {
                    required: true, dateBR: true
                },
                alunoAtividadeHorasReais: {
                    maxlength: 5, number: true, required: true, min: 1, valorMaxInteiro: function () {
                        var arrEvento = __atividadeperiodoAlunoAdd.getDadosEvento();

                        if (arrEvento) {
                            return arrEvento['hora_maxima'] || arrEvento['horaMaxima'];
                        }
                    }
                }
                ,
                alunoAtividadeHorasValidas: {
                    maxlength: 10, number: true, required: true, valorMaxInteiro: function () {
                        var arrEvento = __atividadeperiodoAlunoAdd.getDadosEvento();

                        if (arrEvento) {
                            return arrEvento['hora_maxima'] || arrEvento['horaMaxima'];
                        }
                    }
                }
                ,
                alunoAtividadeObservacao: {
                    maxlength: 45
                }
            }
            ;
            __atividadeperiodoAlunoAdd.options.validator.settings.messages = {
                alunoper: {required: 'Campo obrigatório!', number: 'Número inválido!'},
                evento: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                alunoAtividadeOrigemCadastro: {maxlength: 'Tamanho máximo: 8!', required: 'Campo obrigatório!'},
                alunoAtividadeDataRealizacao: {required: 'Campo obrigatório!', dateBR: 'Informe uma data válida!'},
                alunoAtividadeHorasReais: {
                    maxlength: 'Tamanho máximo: 10!',
                    number: 'Número inválido!',
                    required: 'Campo obrigatório!',
                    min: 'Forneça uma quantidade de horas válida!'
                },
                alunoAtividadeHorasValidas: {
                    maxlength: 'Tamanho máximo: 10!',
                    number: 'Número inválido!',
                    required: 'Campo obrigatório!',
                    valorMaxInteiro: 'Este valor extrapola o máximo permitido!'
                },
                alunoAtividadeObservacao: {maxlength: 'Tamanho máximo: 45!'}
            };

            $(__atividadeperiodoAlunoAdd.options.formElement).submit(function (e) {
                var ok = $(__atividadeperiodoAlunoAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__atividadeperiodoAlunoAdd.options.formElement);

                if (__atividadeperiodoAlunoAdd.options.ajaxSubmit || $form.data('ajax')) {

                    var dados = $form.serializeJSON();

                    if (!dados['evento']) {
                        var arrEvento = $("#evento").select2("data");
                        dados['evento'] = arrEvento['eventoId'];
                    }
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __atividadeperiodoAlunoAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __atividadeperiodoAlunoAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__atividadeperiodoAlunoAdd.options.listagem) {
                                    $.atividadeperiodoAlunoIndex().reloadDataTableAtividadeperiodoAluno();
                                    __atividadeperiodoAlunoAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#atividadeperiodo-aluno-modal').modal('hide');
                                }

                                __atividadeperiodoAlunoAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaAtividadeperiodoAluno = function (alunoAtividadeId, callback) {
            var $form = $(__atividadeperiodoAlunoAdd.options.formElement);

            __atividadeperiodoAlunoAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');
            $.ajax({
                url: __atividadeperiodoAlunoAdd.options.url.atividadeAlunoEdit + '/' + alunoAtividadeId,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __atividadeperiodoAlunoAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }
                    __atividadeperiodoAlunoAdd.removeOverlay($form);
                },
                erro: function () {
                    __atividadeperiodoAlunoAdd.showNotificacaoDanger('Erro desconhecido!');
                    __atividadeperiodoAlunoAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.atividadeperiodoAlunoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.atividades.atividadeperiodo-aluno.add");

        if (!obj) {
            obj = new AtividadeperiodoAlunoAdd();
            obj.run(params);
            $(window).data('universa.atividades.atividadeperiodo-aluno.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);