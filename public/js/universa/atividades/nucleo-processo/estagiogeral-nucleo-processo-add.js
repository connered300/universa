(function ($, window, document) {
    'use strict';

    var EstagiogeralNucleoProcessoAdd = function () {
        VersaShared.call(this);
        var __estagiogeralNucleoProcessoAdd = this;
        this.defaults = {
            url: {
                estagiogeralNucleo: '',
                estagiogeralProcessoTipo: ''
            },
            data: {},
            value: {
                nucleoId: null,
                processoTipoId: null
            },
            datatables: {},
            wizardElement: '#estagiogeral-nucleo-processo-wizard',
            formElement: '#estagiogeral-nucleo-processo-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#nucleoId").select2({
                language: 'pt-BR',
                tags: true,
                ajax: {
                    url: __estagiogeralNucleoProcessoAdd.options.url.estagiogeralNucleo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.nucleo_id, id: el.nucleo_id};
                        });

                        return {results: transformed};
                    }
                },
                allowClear: true,
                initSelection: function (element, callback) {
                    callback(__estagiogeralNucleoProcessoAdd.getNucleoId());
                }
            });
            $('#nucleoId').select2("val", __estagiogeralNucleoProcessoAdd.getNucleoId());
            $("#processoTipoId").select2({
                language: 'pt-BR',
                tags: true,
                ajax: {
                    url: __estagiogeralNucleoProcessoAdd.options.url.estagiogeralProcessoTipo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.processo_tipo_id, id: el.processo_tipo_id};
                        });

                        return {results: transformed};
                    }
                },
                allowClear: true,
                initSelection: function (element, callback) {
                    callback(__estagiogeralNucleoProcessoAdd.getProcessoTipoId());
                }
            });
            $('#processoTipoId').select2("val", __estagiogeralNucleoProcessoAdd.getProcessoTipoId());
            $("#nucleoProcessoVinculacao").select2({language: 'pt-BR'});
        };


        this.setNucleoId = function (nucleoId) {
            this.options.value.NucleoId = nucleoId || null;
        };

        this.getNucleoId = function () {
            return priv.options.value.NucleoId || null;
        };

        this.setProcessoTipoId = function (processoTipoId) {
            this.options.value.ProcessoTipoId = processoTipoId || null;
        };

        this.getProcessoTipoId = function () {
            return priv.options.value.ProcessoTipoId || null;
        };

        this.setValidations = function () {
            var submitHandler = function (form) {
                form.submit();
            };

            $(__estagiogeralNucleoProcessoAdd.options.formElement).validate({
                submitHandler: submitHandler,
                rules: {
                    nucleoProcessoId: {maxlength: 10, number: true, required: true},
                    nucleoId: {maxlength: 10, number: true, required: true},
                    processoTipoId: {maxlength: 10, number: true, required: true},
                    nucleoProcessoVinculacao: {maxlength: 7, required: true},
                },
                messages: {
                    nucleoProcessoId: {
                        maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    nucleoId: {
                        maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    processoTipoId: {
                        maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    nucleoProcessoVinculacao: {maxlength: 'Tamanho máximo: 7!', required: 'Campo obrigatório!'},
                },
                ignore: '.ignore'
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.estagiogeralNucleoProcessoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.atividade.estagiogeral-nucleo-processo.add");

        if (!obj) {
            obj = new EstagiogeralNucleoProcessoAdd();
            obj.run(params);
            $(window).data('universa.estagiogeral-nucleo-processo.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);