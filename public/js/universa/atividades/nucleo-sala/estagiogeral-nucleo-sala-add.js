(function ($, window, document) {
    'use strict';

    var EstagiogeralNucleoSalaAdd = function () {
        VersaShared.call(this);
        var __estagiogeralNucleoSalaAdd = this;
        this.defaults = {
            url: {
                estagiogeralNucleo: '',
                infraSala: ''
            },
            data: {},
            value: {
                nucleoId: null,
                salaId: null
            },
            datatables: {},
            wizardElement: '#estagiogeral-nucleo-sala-wizard',
            formElement: '#estagiogeral-nucleo-sala-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#nucleoId").select2({
                language: 'pt-BR',
                tags: true,
                ajax: {
                    url: __estagiogeralNucleoSalaAdd.options.url.estagiogeralNucleo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.nucleo_id, id: el.nucleo_id};
                        });

                        return {results: transformed};
                    }
                },
                allowClear: true,
                initSelection: function (element, callback) {
                    callback(__estagiogeralNucleoSalaAdd.getNucleoId());
                }
            });
            $('#nucleoId').select2("val", __estagiogeralNucleoSalaAdd.getNucleoId());
            $("#salaId").select2({
                language: 'pt-BR',
                tags: true,
                ajax: {
                    url: __estagiogeralNucleoSalaAdd.options.url.infraSala,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.sala_id, id: el.sala_id};
                        });

                        return {results: transformed};
                    }
                },
                allowClear: true,
                initSelection: function (element, callback) {
                    callback(__estagiogeralNucleoSalaAdd.getSalaId());
                }
            });
            $('#salaId').select2("val", __estagiogeralNucleoSalaAdd.getSalaId());
        };


        this.setNucleoId = function (nucleoId) {
            this.options.value.NucleoId = nucleoId || null;
        };

        this.getNucleoId = function () {
            return priv.options.value.NucleoId || null;
        };

        this.setSalaId = function (salaId) {
            this.options.value.SalaId = salaId || null;
        };

        this.getSalaId = function () {
            return priv.options.value.SalaId || null;
        };

        this.setValidations = function () {
            var submitHandler = function (form) {
                form.submit();
            };

            $(__estagiogeralNucleoSalaAdd.options.formElement).validate({
                submitHandler: submitHandler,
                rules: {
                    estagiogeralSalaNucleoId: {maxlength: 11, number: true, required: true},
                    nucleoId: {maxlength: 10, number: true, required: true},
                    salaId: {maxlength: 11, number: true, required: true},
                },
                messages: {
                    estagiogeralSalaNucleoId: {
                        maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    nucleoId: {
                        maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    salaId: {
                        maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                },
                ignore: '.ignore'
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.estagiogeralNucleoSalaAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.atividade.estagiogeral-nucleo-sala.add");

        if (!obj) {
            obj = new EstagiogeralNucleoSalaAdd();
            obj.run(params);
            $(window).data('universa.estagiogeral-nucleo-sala.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);