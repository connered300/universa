(function ($, window, document) {
    'use strict';

    var EstagiogeralConfiguracoesAdd = function () {
        VersaShared.call(this);
        var __estagiogeralConfiguracoesAdd = this;
        this.defaults = {
            url: {},
            data: {},
            value: {},
            datatables: {},
            wizardElement: '#estagiogeral-configuracoes-wizard',
            formElement: '#estagiogeral-configuracoes-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#estagioconfMaximoDuracao").inputmask({showMaskOnHover: false, clearIncomplete: true, alias: 'integer'});
            $("#estagioconfDataDecreto")
                .datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                })
                .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});
            $("#estagiocargaTotalPontuacao").inputmask({
                showMaskOnHover: false, clearIncomplete: true, alias: 'integer'
            });
            $("#estagioconfCargaTotalHoras").inputmask({
                showMaskOnHover: false, clearIncomplete: true, alias: 'integer'
            });
            $("#estagioconfSerie").select2({language: 'pt-BR'});
        };


        this.setValidations = function () {
            var submitHandler = function (form) {
                form.submit();
            };

            $(__estagiogeralConfiguracoesAdd.options.formElement).validate({
                submitHandler: submitHandler,
                rules: {
                    estagioconfPortaria: {maxlength: 45, required: true},
                    estagioconfMaximoDuracao: {maxlength: 11, number: true, required: true},
                    estagioconfDataDecreto: {required: true, dateBR: true},
                    estagiocargaTotalPontuacao: {maxlength: 11, number: true, required: true},
                    estagioconfCargaTotalHoras: {maxlength: 11, number: true, required: true},
                    estagioconfSerie: {maxlength: 2, required: true},
                },
                messages: {
                    estagioconfPortaria: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                    estagioconfMaximoDuracao: {
                        maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    estagioconfDataDecreto: {required: 'Campo obrigatório!', dateBR: 'Informe uma data válida!'},
                    estagiocargaTotalPontuacao: {
                        maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    estagioconfCargaTotalHoras: {
                        maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    estagioconfSerie: {maxlength: 'Tamanho máximo: 2!', required: 'Campo obrigatório!'},
                },
                ignore: '.ignore'
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.estagiogeralConfiguracoesAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.atividade.estagiogeral-configuracoes.add");

        if (!obj) {
            obj = new EstagiogeralConfiguracoesAdd();
            obj.run(params);
            $(window).data('universa.estagiogeral-configuracoes.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);