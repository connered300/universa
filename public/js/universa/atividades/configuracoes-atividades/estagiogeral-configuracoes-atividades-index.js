(function ($, window, document) {
    'use strict';

    var EstagiogeralConfiguracoesAtividadesIndex = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlSearch: '',
                urlEdit: '',
                urlRemove: ''
            }
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);
        };

        priv.addOverlay = function (el, mensagem) {
            mensagem = mensagem || 'Aguarde...';
            el.append('<div class="app-overlay">' + mensagem + '</div>');
        };

        priv.removeOverlay = function (el) {
            el.find('>.app-overlay').remove();
        };

        priv.setSteps = function () {
            var colNum = 0;
            priv.dataTableEstagiogeralConfiguracoesAtividades =
                $('#dataTableEstagiogeralConfiguracoesAtividades').dataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: priv.options.urlSearch,
                        type: "POST",
                        dataSrc: function (json) {
                            var data = json.data;
                            var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary estagiogeralConfiguracoesAtividades-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger estagiogeralConfiguracoesAtividades-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                            for (var row in data) {
                                data[row]['acao'] = btnGroup;
                            }

                            priv.removeOverlay($('.widget-body'));

                            return data;
                        }
                    },
                    columnDefs: [
                        {name: "estagioconfatividade_id", targets: colNum++, data: "estagioconfatividade_id"},
                        {name: "estagioconf_portaria", targets: colNum++, data: "estagioconf_portaria"},
                        {name: "estagioatividade_id", targets: colNum++, data: "estagioatividade_id"},
                        {
                            name: "estagioconf_minimo_disciplina", targets: colNum++,
                            data: "estagioconf_minimo_disciplina"
                        },
                        {
                            name: "estagiogeral__horas_praticas_max", targets: colNum++,
                            data: "estagiogeral__horas_praticas_max"
                        },
                        {
                            name: "estagioconf_horas_praticas_min", targets: colNum++,
                            data: "estagioconf_horas_praticas_min"
                        },
                        {name: "estagioconf_pontuacao_max", targets: colNum++, data: "estagioconf_pontuacao_max"},
                        {name: "estagioconf_pontuacao_min", targets: colNum++, data: "estagioconf_pontuacao_min"},
                        {name: "acao", targets: colNum++, data: "estagioconfatividade_id"}
                    ],
                    "dom": "fr" +
                           "t" +
                           "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                    "autoWidth": true,
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "Procurar: ",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    },
                    order: [[0, 'asc']]
                });

            $('#dataTableEstagiogeralConfiguracoesAtividades').on('click', '.estagiogeralConfiguracoesAtividades-edit',
                                                                  function (event) {
                                                                      var $pai = $(this).closest('tr');
                                                                      var data = priv.dataTableEstagiogeralConfiguracoesAtividades.fnGetData($pai);

                                                                      location.href = priv.options.urlEdit + '/' +
                                                                                      data['estagioconfatividade_id'];
                                                                  });

            $('#dataTableEstagiogeralConfiguracoesAtividades').on('click',
                                                                  '.estagiogeralConfiguracoesAtividades-remove',
                                                                  function (e) {
                                                                      var $pai = $(this).closest('tr');
                                                                      var data = priv.dataTableEstagiogeralConfiguracoesAtividades.fnGetData($pai);
                                                                      var arrRemover = {
                                                                          estagioconfatividadeId: data['estagioconfatividade_id']
                                                                      };

                                                                      $.SmartMessageBox({
                                                                          title: "Confirme a operação:",
                                                                          content: 'Deseja realmente remover registro de estagiogeral__configuracoes_atividades "' +
                                                                                   data['estagioconf_pontuacao_min'] +
                                                                                   '"?',
                                                                          buttons: "[Não][Sim]",
                                                                      }, function (ButtonPress) {
                                                                          if (ButtonPress == "Sim") {
                                                                              priv.addOverlay($('.widget-body'),
                                                                                              'Aguarde, solicitando remoção de registro de estagiogeral__configuracoes_atividades...');
                                                                              $.ajax({
                                                                                  url: priv.options.urlRemove,
                                                                                  type: 'POST',
                                                                                  dataType: 'json',
                                                                                  data: arrRemover,
                                                                                  success: function (data) {
                                                                                      if (data.erro) {
                                                                                          $.smallBox({
                                                                                              title: "Não foi possível remover registro de estagiogeral__configuracoes_atividades:",
                                                                                              content: "<i>" +
                                                                                                       data['erroDescricao'] +
                                                                                                       "</i>",
                                                                                              color: "#C46A69",
                                                                                              icon: "fa fa-times",
                                                                                              timeout: 10000
                                                                                          });

                                                                                          priv.removeOverlay($('.widget-body'));
                                                                                      } else {
                                                                                          priv.dataTableEstagiogeralConfiguracoesAtividades.api().ajax.reload(null, false);
                                                                                          $.smallBox({
                                                                                              title: "Sucesso!",
                                                                                              content: "<i>Registro de estagiogeral__configuracoes_atividades removido!</i>",
                                                                                              color: "#739e73",
                                                                                              icon: "fa fa-check-circle",
                                                                                              timeout: 10000
                                                                                          });
                                                                                      }
                                                                                  }
                                                                              });
                                                                          }
                                                                      });

                                                                      e.preventDefault();
                                                                  });
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
        };
    };

    $.estagiogeralConfiguracoesAtividadesIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.atividade.estagiogeral-configuracoes-atividades.index");

        if (!obj) {
            obj = new EstagiogeralConfiguracoesAtividadesIndex();
            obj.run(params);
            $(window).data('universa.estagiogeral-configuracoes-atividades.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);