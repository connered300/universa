(function ($, window, document) {
    'use strict';

    var EstagiogeralConfiguracoesAtividadesAdd = function () {
        VersaShared.call(this);
        var __estagiogeralConfiguracoesAtividadesAdd = this;
        this.defaults = {
            url: {
                estagiogeralConfiguracoes: '',
                estagiogeralAtividades: ''
            },
            data: {},
            value: {
                estagioconfPortaria: null,
                estagioatividadeId: null
            },
            datatables: {},
            wizardElement: '#estagiogeral-configuracoes-atividades-wizard',
            formElement: '#estagiogeral-configuracoes-atividades-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#estagioconfPortaria").select2({
                language: 'pt-BR',
                tags: true,
                ajax: {
                    url: __estagiogeralConfiguracoesAtividadesAdd.options.url.estagiogeralConfiguracoes,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.estagioconf_portaria, id: el.estagioconf_portaria};
                        });

                        return {results: transformed};
                    }
                },
                allowClear: true,
                initSelection: function (element, callback) {
                    callback(__estagiogeralConfiguracoesAtividadesAdd.getEstagioconfPortaria());
                }
            });
            $('#estagioconfPortaria').select2("val", __estagiogeralConfiguracoesAtividadesAdd.getEstagioconfPortaria());
            $("#estagioatividadeId").select2({
                language: 'pt-BR',
                tags: true,
                ajax: {
                    url: __estagiogeralConfiguracoesAtividadesAdd.options.url.estagiogeralAtividades,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.estagioatividade_id, id: el.estagioatividade_id};
                        });

                        return {results: transformed};
                    }
                },
                allowClear: true,
                initSelection: function (element, callback) {
                    callback(__estagiogeralConfiguracoesAtividadesAdd.getEstagioatividadeId());
                }
            });
            $('#estagioatividadeId').select2("val", __estagiogeralConfiguracoesAtividadesAdd.getEstagioatividadeId());
            $("#estagioconfMinimoDisciplina").inputmask({
                showMaskOnHover: false, clearIncomplete: true, alias: 'integer'
            });
            $("#estagiogeralHorasPraticasMax").inputmask({
                showMaskOnHover: false, clearIncomplete: true, alias: 'integer'
            });
            $("#estagioconfHorasPraticasMin").inputmask({
                showMaskOnHover: false, clearIncomplete: true, alias: 'integer'
            });
            $("#estagioconfPontuacaoMax").inputmask({showMaskOnHover: false, clearIncomplete: true, alias: 'integer'});
            $("#estagioconfPontuacaoMin").inputmask({showMaskOnHover: false, clearIncomplete: true, alias: 'integer'});
        };


        this.setEstagioconfPortaria = function (estagioconfPortaria) {
            this.options.value.EstagioconfPortaria = estagioconfPortaria || null;
        };

        this.getEstagioconfPortaria = function () {
            return priv.options.value.EstagioconfPortaria || null;
        };

        this.setEstagioatividadeId = function (estagioatividadeId) {
            this.options.value.EstagioatividadeId = estagioatividadeId || null;
        };

        this.getEstagioatividadeId = function () {
            return priv.options.value.EstagioatividadeId || null;
        };

        this.setValidations = function () {
            var submitHandler = function (form) {
                form.submit();
            };

            $(__estagiogeralConfiguracoesAtividadesAdd.options.formElement).validate({
                submitHandler: submitHandler,
                rules: {
                    estagioconfatividadeId: {maxlength: 11, number: true, required: true},
                    estagioconfPortaria: {maxlength: 45, required: true},
                    estagioatividadeId: {maxlength: 11, number: true, required: true},
                    estagioconfMinimoDisciplina: {maxlength: 11, number: true},
                    estagiogeralHorasPraticasMax: {maxlength: 11, number: true},
                    estagioconfHorasPraticasMin: {maxlength: 11, number: true},
                    estagioconfPontuacaoMax: {maxlength: 11, number: true},
                    estagioconfPontuacaoMin: {maxlength: 11, number: true},
                },
                messages: {
                    estagioconfatividadeId: {
                        maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    estagioconfPortaria: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                    estagioatividadeId: {
                        maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    estagioconfMinimoDisciplina: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!'},
                    estagiogeralHorasPraticasMax: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!'},
                    estagioconfHorasPraticasMin: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!'},
                    estagioconfPontuacaoMax: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!'},
                    estagioconfPontuacaoMin: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!'},
                },
                ignore: '.ignore'
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.estagiogeralConfiguracoesAtividadesAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.atividade.estagiogeral-configuracoes-atividades.add");

        if (!obj) {
            obj = new EstagiogeralConfiguracoesAtividadesAdd();
            obj.run(params);
            $(window).data('universa.estagiogeral-configuracoes-atividades.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);