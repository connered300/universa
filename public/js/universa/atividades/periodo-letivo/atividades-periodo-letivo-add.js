(function ($, window, document) {
    'use strict';

    var EstagioPeriodoLetivoAdd = function () {
        VersaShared.call(this);
        var __estagioPeriodoLetivoAdd = this;
        this.defaults = {
            url: {
                acadperiodoLetivo: '',
                estagiogeralConfiguracoes: ''
            },
            data: {},
            value: {
                perId: null,
                estagioconfPortaria: null
            },
            datatables: {},
            wizardElement: '#atividade-periodo-letivo-wizard',
            formElement: '#atividade-periodo-letivo-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#perId").select2({
                language: 'pt-BR',
                tags: true,
                ajax: {
                    url: __estagioPeriodoLetivoAdd.options.url.acadperiodoLetivo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.per_id, id: el.per_id};
                        });

                        return {results: transformed};
                    }
                },
                allowClear: true,
                initSelection: function (element, callback) {
                    callback(__estagioPeriodoLetivoAdd.getPerId());
                }
            });
            $('#perId').select2("val", __estagioPeriodoLetivoAdd.getPerId());
            $("#estagioconfPortaria").select2({
                language: 'pt-BR',
                tags: true,
                ajax: {
                    url: __estagioPeriodoLetivoAdd.options.url.estagiogeralConfiguracoes,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.estagioconf_portaria, id: el.estagioconf_portaria};
                        });

                        return {results: transformed};
                    }
                },
                allowClear: true,
                initSelection: function (element, callback) {
                    callback(__estagioPeriodoLetivoAdd.getEstagioconfPortaria());
                }
            });
            $('#estagioconfPortaria').select2("val", __estagioPeriodoLetivoAdd.getEstagioconfPortaria());
            $("#estagioperiodoVagas").inputmask({showMaskOnHover: false, clearIncomplete: true, alias: 'integer'});
            $("#estagioperiodoDataInicio")
                .datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                })
                .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});
            $("#estagioperiodoDataFechamento")
                .datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                })
                .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});
        };


        this.setPerId = function (perId) {
            this.options.value.PerId = perId || null;
        };

        this.getPerId = function () {
            return priv.options.value.PerId || null;
        };

        this.setEstagioconfPortaria = function (estagioconfPortaria) {
            this.options.value.EstagioconfPortaria = estagioconfPortaria || null;
        };

        this.getEstagioconfPortaria = function () {
            return priv.options.value.EstagioconfPortaria || null;
        };

        this.setValidations = function () {
            var submitHandler = function (form) {
                form.submit();
            };

            $(__estagioPeriodoLetivoAdd.options.formElement).validate({
                submitHandler: submitHandler,
                rules: {
                    perId: {maxlength: 10, number: true, required: true},
                    estagioconfPortaria: {maxlength: 45, required: true},
                    estagioperiodoVagas: {maxlength: 11, number: true},
                    estagioperiodoDataInicio: {dateBR: true},
                    estagioperiodoDataFechamento: {dateBR: true},
                    estagioperiodoFuncionamentoInicio: {},
                    estagioperiodoFuncionamentoFim: {},
                },
                messages: {
                    perId: {
                        maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    estagioconfPortaria: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                    estagioperiodoVagas: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!'},
                    estagioperiodoDataInicio: {dateBR: 'Informe uma data válida!'},
                    estagioperiodoDataFechamento: {dateBR: 'Informe uma data válida!'},
                    estagioperiodoFuncionamentoInicio: {},
                    estagioperiodoFuncionamentoFim: {},
                },
                ignore: '.ignore'
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.estagioPeriodoLetivoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.atividade.atividade-periodo-letivo.add");

        if (!obj) {
            obj = new EstagioPeriodoLetivoAdd();
            obj.run(params);
            $(window).data('universa.atividade-periodo-letivo.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);