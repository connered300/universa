(function ($, window, document) {
    'use strict';

    var EstagioPeriodoLetivoIndex = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlSearch: '',
                urlEdit: '',
                urlRemove: '',
                urlCampus: '',
                urlCurso: '',
                urlPeriodo: '',
                urlListagem: '',
                urlPortaria: '',
                urlNucleos: '',
                urlSecretaria: '',
                urlSalvarData: '',
                urlSalvarRegistro: '',
                urlAtividadePeriodo: ''

            },
            data: {
                campus: '',
                periodo: '',
                config: '',
                secretaria: '',
                cursoId: '',
                dataInicial: ''
            }
        };

        priv.etapaDataTable = function () {
            $("#tbPeriodo").DataTable({
                "processing": true,
                "serverSide": true,
                "searching": false,
                "destroy": true,
                "ajax": {
                    "url": priv.options.urlListagem,
                    "type": "POST",
                    "async": false,
                    "data": function (d) {
                        d.cursocampusId = priv.data.campus.cursocampus_id;
                        d.perId = priv.data.periodo.per_id;

                        return d;
                    },
                    "dataSrc": function (e) {
                        var data = e.data;

                        for (var i in data) {
                            var id = data[i]['per_id'];
                            var title = data[i]['curso_id'];
                            data[i]['per_id'] = "<a href='#'>" + data[i]['per_id'] + "</a>";
                            data[i]['qtd'] =
                                "<button type='button' class='btn btn-sm btn-info' data-toggle='modal' data-target='#modalNucleos'>" +
                                data[i]['qtd'] + "</button>";
                            data[i]['atividadeperiodo_funcionamento_inicio'] =
                                data[i]['atividadeperiodo_funcionamento_inicio'] + " - " +
                                data[i]['atividadeperiodo_funcionamento_fim'];
                            data[i]['atividadeperiodo_data_inicio'] =
                                "<a href='#inicio'>" + data[i]['atividadeperiodo_data_inicio'] + "</a>";
                            data[i]['atividadeperiodo_data_fechamento'] =
                                "<a href='#fechamento'>" + data[i]['atividadeperiodo_data_fechamento'] + "</a>";
                            data[i]['btn'] =
                                "<button type='button' class='btn btn-sm btn-info editar' id='" + id + "' title='" +
                                title +
                                "' data-toggle='modal' data-target='#modalEditar'><span class='glyphicon glyphicon-pencil txt-color-white' aria-hidden='true'></span></button>";
                        }
                        return data;
                    }
                },
                "sDom": "<'dt-toolbar'<'col-xs-6'f>r>" + "t" + "<'dt-toolbar-footer'<'col-xs-6'i><'col-xs-6'p>>",
                "columns": [
                    {"name": "curso_nome", targets: 0, data: "curso_nome"},
                    {"name": "qtd", targets: 0, data: "qtd"},
                    {"name": "secretaria_nome", targets: 0, data: "secretaria_nome"},
                    {"name": "atividadeconf_portaria", targets: 0, data: "atividadeconf_portaria"},
                    {
                        "name": "atividadeperiodo_funcionamento_inicio", targets: 0,
                        data: "atividadeperiodo_funcionamento_inicio"
                    },
                    {"name": "atividadeperiodo_data_inicio", targets: 0, data: "atividadeperiodo_data_inicio"},
                    {"name": "atividadeperiodo_data_fechamento", targets: 0, data: "atividadeperiodo_data_fechamento"},
                    {"data": "btn"}
                ], oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                }
            });
        };

        priv.defaultValueSelect = function (url, id, text, ID) {
            $.ajax(url, {
                data: {query: ''},
                dataType: "json"
            }).done(function (data) {
                var transformed = $.map(data, function (el) {
                    el.text = el[text];
                    el.id = el[ID];

                    return el;
                });

                $('#' + id).select2('data', transformed[0] || null);
                $('#' + id).trigger('change');
            });
        };

        priv.buscaConfig = function (cursocampus_id, curso_id) {
            $.ajax({
                url: priv.options.urlPortaria,
                dataType: 'json',
                data: {cursocampusId: cursocampus_id, cursoId: curso_id}
            }).done(function (e) {
                var result = $.map(e, function (a) {
                    return a.secretaria_id;
                });

                priv.data.secretaria = $.extend({}, result);
            })
        };

        priv.inputChanged = function () {
            $('#pesquisaCampus').change(function () {
                var periodo = $('#periodo');
                periodo.hide(300);
                priv.data.campus = $('#pesquisaCampus').select2('data');
                periodo.show(300);
            });

            $('#pesquisaPeriodo').change(function () {
                priv.data.periodo = $('#pesquisaPeriodo').select2('data');
                priv.etapaDataTable();
                priv.setValues(priv.options.urlNucleos);
                priv.maskDate("dataInicio");
                priv.maskDate("dataInicioInserir");
                priv.editInfo();
            });

            $('#pesquisaCurso').change(function () {
                var curso = $('#pesquisaCurso').select2('data');
                priv.buscaConfig(priv.data.campus.cursocampus_id, curso.curso_id);
                priv.select2Secretaria(curso.curso_id);
                $('.secr').show();
            });

            $('#salvarRegistro').click(function () {
                var objSecretaria = $("#pesquisaSecretaria").select2('data') || '';
                var obj = $.extend({}, {
                    perId: priv.data.periodo.per_id,
                    atividadeperiodoVagas: $("#vagas").val(),
                    atividadeperiodoDataInicio: $("#dataInicioInserir").val(),
                    atividadeconfigcursoId: objSecretaria.atividadeconfigcurso_id
                });

                if (priv.modalValidate("Campo não pode ficar vazio", 500, 3000)) {
                    priv.add(obj);
                }

            });
        };

        priv.maskDate = function (id) {
            $("#" + id).datepicker({
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>',
                dateFormat: "dd-mm-yyyy"
            }).inputmask({
                showMaskOnHover: false,
                clearIncomplete: true,
                mask: ['99/99/9999']
            });
        };

        priv.editInfo = function () {
            $(".editar").click(function () {
                var periodo = $(this).attr("id");
                priv.data.cursoId = $(this).attr("title");
                $.ajax({
                    url: priv.options.urlAtividadePeriodo,
                    method: 'post',
                    dataType: 'json',
                    data: {perId: periodo, cursoId: priv.data.cursoId},
                    success: function (e) {
                        console.log(priv.formatDate(e[0].atividadeperiodo_data_inicio));
                        $('#dataInicio').val(priv.formatDate(e[0].atividadeperiodo_data_inicio));
                    }
                }).done(function () {
                    $('#dataFim').val($('a[href="#fechamento"]').text());
                });
            });
        };

        priv.warning = function (message) {
            $(".editar").click(function () {
                priv.showNotification({
                    title: "= (",
                    type: "custom",
                    content: message,
                    timeout: 5000
                });
            });
        };

        priv.add = function (obj) {
            $.ajax({
                url: priv.options.urlSalvarRegistro,
                method: 'post',
                dataType: 'json',
                data: {dados: obj},
                success: function (result) {
                    if (result['type'] == "success") {
                        priv.showNotification({
                            title: "= )",
                            type: "success",
                            content: result['message'],
                            timeout: 3000
                        });
                    } else {
                        priv.showNotification({
                            title: "= (",
                            type: "custom",
                            content: result['message'],
                            timeout: 5000
                        });
                    }
                }
            }).done(function () {
                $(".fechaModal").trigger("click");
            });
        };

        priv.modalValidate = function (message, fadein, fadeout) {
            var result = false;
            $('.modal-validate :input').each(function (e, a) {
                var valor = $(a).val();

                if (valor == null || valor == '') {
                    $(a).siblings('span').fadeIn(fadein);
                    $(a).siblings('span').html("&nbsp;" + message);
                    $(a).siblings('span').css("color", "red");
                    result = false;
                } else {
                    $(a).siblings('span').remove();
                    result = true;
                }

                $(a).siblings('span').fadeOut(fadeout);
            });
            return result;
        };

        priv.formatDate = function (element) {
            var date = new Date(element);
            var day = String(date.getDate());
            var month = String(date.getMonth() + 1);
            var year = date.getFullYear();

            if (day.length < 2) {day = "0" + day;}
            if (month.length < 2) {month = "0" + month;}

            return (day + '/' + month + '/' + year);
        };

        priv.clearTable = function (id) {
            $("#" + id + " tbody > tr").remove();
        };

        priv.setValues = function (url) {
            priv.clearTable("nucleoInfo");
            $.ajax({
                url: url,
                type: 'POST'
            }).done(function (e) {
                $.map(e, function (dados) {
                    $("#nucleoInfo tbody").append("<tr><td>" + dados.nucleo_descricao + "<td/><td>" +
                                                  dados.nucleo_tipo + "<td/></tr>")
                });
            });
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);
        };

        priv.addOverlay = function (el, mensagem) {
            mensagem = mensagem || 'Aguarde...';
            el.append('<div class="app-overlay">' + mensagem + '</div>');
        };

        priv.removeOverlay = function (el) {
            el.find('>.app-overlay').remove();
        };

        priv.showNotification = function (param) {
            var options = $.extend(
                true,
                {
                    content: '',
                    title: '',
                    type: 'info',
                    icon: '',
                    color: '',
                    timeout: false
                },
                param
            );

            switch (options.type) {
                case 'danger':
                    options.icon = 'fa-times-circle';
                    options.title = options.title || 'Atenção:';
                    options.color = '#a90329';
                    break;
                case 'info':
                    options.icon = 'fa-info-circle';
                    options.title = options.title || 'Informação:';
                    options.color = '#57889c';
                    break;
                case 'success':
                    options.icon = 'fa-check-circle';
                    options.title = options.title || 'Sucesso:';
                    options.color = '#739e73';
                    break;
                case 'warning':
                    options.icon = 'fa-exclamation-circle';
                    options.title = options.title || 'Atenção:';
                    options.color = '#c79121';
                    break;
                case 'custom':
                    options.icon = 'fa-exclamation-circle';
                    options.title = options.title || '';
                    options.color = '#2f2929';
                    break;
            }

            var optionsNotification = {
                title: options.title,
                content: "<i>" + options.content + "</i>",
                color: options.color
            };

            if (options.timeout) {
                optionsNotification.timeout = options.timeout;
            }

            $.smallBox(optionsNotification);
        };

        priv.select2Secretaria = function (curso) {
            $('#pesquisaSecretaria').select2({
                ajax: {
                    url: priv.options.urlSecretaria,
                    dataType: 'json',
                    type: 'POST',
                    data: function (query) {
                        return {query: query, cursoId: curso};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.secretaria_nome;
                            el.id = el.secretaria_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                },
            });
        };

        priv.setSteps = function () {
            $('#pesquisaCampus').select2({
                ajax: {
                    url: priv.options.urlCampus,
                    dataType: 'json',
                    data: function (query) { return {query: query};},
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.camp_nome;
                            el.id = el.camp_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                },
            });
            priv.defaultValueSelect(priv.options.urlCampus, "pesquisaCampus", "camp_nome", "camp_id");

            $('#pesquisaPeriodo').select2({
                ajax: {
                    url: priv.options.urlPeriodo,
                    dataType: 'json',
                    data: function (query) { return {query: query};},
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.per_semestre + '/' + el.per_ano;
                            el.id = el.per_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            }).change(function () {
                $('#dtPeriodo').fadeIn(800);
            });

            $('#pesquisaCurso').select2({
                ajax: {
                    url: priv.options.urlCurso,
                    dataType: 'json',
                    data: function (query) { return {query: query, campId: priv.data.campus.camp_id};},
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.curso_nome;
                            el.id = el.curso_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                },
            });
            priv.defaultValueSelect(priv.options.urlCurso, "pesquisaCurso", "curso_nome", "curso_id");

            $("#salvarData").click(function () {
                $.ajax({
                    url: priv.options.urlSalvarData,
                    dataType: 'json',
                    method: 'post',
                    data: {data: $("#dataInicio").val(), perId: priv.data.periodo.per_id, cursoId: priv.data.cursoId},
                    success: function (result) {
                        if (result['type'] == "success") {
                            priv.showNotification({
                                title: "= )",
                                type: "success",
                                content: result['message'],
                                timeout: 3000
                            });
                        } else {
                            priv.showNotification({
                                title: "= (",
                                type: "custom",
                                content: result['message'],
                                timeout: 5000
                            });
                        }
                    }
                }).done(function () {
                    $(".fechaModal").trigger("click");
                    $('#tbPeriodo').DataTable().ajax.reload(null, false);
                });
            });
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
            priv.inputChanged();
        };
    };

    $.estagioPeriodoLetivoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.atividade.atividade-periodo-letivo.index");

        if (!obj) {
            obj = new EstagioPeriodoLetivoIndex();
            obj.run(params);
            $(window).data('universa.atividade-periodo-letivo.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);