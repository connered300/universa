(function ($, window, document) {
    'use strict';

    var EstagiogeralProcessoTipoIndex = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlSearch: '',
                urlEdit: '',
                urlRemove: ''
            }
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);
        };

        priv.addOverlay = function (el, mensagem) {
            mensagem = mensagem || 'Aguarde...';
            el.append('<div class="app-overlay">' + mensagem + '</div>');
        };

        priv.removeOverlay = function (el) {
            el.find('>.app-overlay').remove();
        };

        priv.setSteps = function () {
            var colNum = 0;
            priv.dataTableEstagiogeralProcessoTipo = $('#dataTableEstagiogeralProcessoTipo').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: priv.options.urlSearch,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary estagiogeralProcessoTipo-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger estagiogeralProcessoTipo-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                        }

                        priv.removeOverlay($('.widget-body'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "processo_tipo_id", targets: colNum++, data: "processo_tipo_id"},
                    {name: "processo_tipo_descricao", targets: colNum++, data: "processo_tipo_descricao"},
                    {name: "processo_tipo_observacao", targets: colNum++, data: "processo_tipo_observacao"},
                    {name: "acao", targets: colNum++, data: "processo_tipo_id"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $('#dataTableEstagiogeralProcessoTipo').on('click', '.estagiogeralProcessoTipo-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = priv.dataTableEstagiogeralProcessoTipo.fnGetData($pai);

                location.href = priv.options.urlEdit + '/' + data['processo_tipo_id'];
            });

            $('#dataTableEstagiogeralProcessoTipo').on('click', '.estagiogeralProcessoTipo-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = priv.dataTableEstagiogeralProcessoTipo.fnGetData($pai);
                var arrRemover = {
                    processoTipoId: data['processo_tipo_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de estagiogeral__processo_tipo "' +
                             data['processo_tipo_observacao'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        priv.addOverlay($('.widget-body'),
                                        'Aguarde, solicitando remoção de registro de estagiogeral__processo_tipo...');
                        $.ajax({
                            url: priv.options.urlRemove,
                            type: 'POST',
                            dataType: 'json',
                            data: arrRemover,
                            success: function (data) {
                                if (data.erro) {
                                    $.smallBox({
                                        title: "Não foi possível remover registro de estagiogeral__processo_tipo:",
                                        content: "<i>" + data['erroDescricao'] + "</i>",
                                        color: "#C46A69",
                                        icon: "fa fa-times",
                                        timeout: 10000
                                    });

                                    priv.removeOverlay($('.widget-body'));
                                } else {
                                    priv.dataTableEstagiogeralProcessoTipo.api().ajax.reload(null, false);
                                    $.smallBox({
                                        title: "Sucesso!",
                                        content: "<i>Registro de estagiogeral__processo_tipo removido!</i>",
                                        color: "#739e73",
                                        icon: "fa fa-check-circle",
                                        timeout: 10000
                                    });
                                }
                            }
                        });
                    }
                });

                e.preventDefault();
            });
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
        };
    };

    $.estagiogeralProcessoTipoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.atividade.estagiogeral-processo-tipo.index");

        if (!obj) {
            obj = new EstagiogeralProcessoTipoIndex();
            obj.run(params);
            $(window).data('universa.estagiogeral-processo-tipo.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);