(function ($, window, document) {
    'use strict';

    var EstagiogeralProcessoTipoAdd = function () {
        VersaShared.call(this);
        var __estagiogeralProcessoTipoAdd = this;
        this.defaults = {
            url: {},
            data: {},
            value: {},
            datatables: {},
            wizardElement: '#estagiogeral-processo-tipo-wizard',
            formElement: '#estagiogeral-processo-tipo-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {

        };


        this.setValidations = function () {
            var submitHandler = function (form) {
                form.submit();
            };

            $(__estagiogeralProcessoTipoAdd.options.formElement).validate({
                submitHandler: submitHandler,
                rules: {
                    processoTipoId: {maxlength: 10, number: true, required: true},
                    processoTipoDescricao: {maxlength: 45, required: true},
                    processoTipoObservacao: {maxlength: 45},
                },
                messages: {
                    processoTipoId: {
                        maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    processoTipoDescricao: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                    processoTipoObservacao: {maxlength: 'Tamanho máximo: 45!'},
                },
                ignore: '.ignore'
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.estagiogeralProcessoTipoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.atividade.estagiogeral-processo-tipo.add");

        if (!obj) {
            obj = new EstagiogeralProcessoTipoAdd();
            obj.run(params);
            $(window).data('universa.estagiogeral-processo-tipo.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);