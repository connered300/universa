(function ($, window, document) {
    'use strict';

    var EstagiogeralAtividadesAdd = function () {
        VersaShared.call(this);
        var __estagiogeralAtividadesAdd = this;
        this.defaults = {
            url: {},
            data: {},
            value: {},
            datatables: {},
            wizardElement: '#estagiogeral-atividades-wizard',
            formElement: '#estagiogeral-atividades-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {

        };


        this.setValidations = function () {
            var submitHandler = function (form) {
                form.submit();
            };

            $(__estagiogeralAtividadesAdd.options.formElement).validate({
                submitHandler: submitHandler,
                rules: {
                    estagioatividadeId: {maxlength: 11, number: true, required: true},
                    estagioatividadeDescricao: {maxlength: 45, required: true},
                    estagioatividadeObservacoes: {required: true},
                },
                messages: {
                    estagioatividadeId: {
                        maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    estagioatividadeDescricao: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                    estagioatividadeObservacoes: {required: 'Campo obrigatório!'},
                },
                ignore: '.ignore'
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.estagiogeralAtividadesAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.atividade.estagiogeral-atividades.add");

        if (!obj) {
            obj = new EstagiogeralAtividadesAdd();
            obj.run(params);
            $(window).data('universa.estagiogeral-atividades.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);