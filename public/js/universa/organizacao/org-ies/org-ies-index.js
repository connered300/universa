(function ($, window, document) {
    'use strict';

    var OrgIesIndex = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlSearch: '',
                urlEdit: ''
            }
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);
        };

        priv.addOverlay = function (el, mensagem) {
            mensagem = mensagem || 'Aguarde...';
            el.append('<div class="app-overlay">' + mensagem + '</div>');
        };

        priv.removeOverlay = function (el) {
            el.find('>.app-overlay').remove();
        };

        priv.setSteps = function () {
            var colNum = 0;
            priv.dataTableOrgIes = $('#dataTableOrgIes').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: priv.options.urlSearch,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var columns = ["ies_nome", "mantenedora", "diretoria", "secretaria", "ies_id"];

                        for (var row in data) {
                            var urlEdit = priv.options.urlEdit + '/' + data[row]['ies_id'];

                            for (var colPos in columns) {
                                var col = columns[colPos];
                                data[row][col] = data[row][col] || '';
                                data[row][col + '_formatado'] = "<a href='" + urlEdit + "'>" + data[row][col] + "</a>";
                            }

                        }

                        priv.removeOverlay($('.widget-body'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "ies_nome", targets: colNum++, data: "ies_nome_formatado"},
                    {name: "mantenedora", targets: colNum++, data: "mantenedora_formatado"},
                    {name: "diretoria", targets: colNum++, data: "diretoria_formatado"},
                    {name: "secretaria", targets: colNum++, data: "secretaria_formatado"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $('#dataTableOrgIes').on('click', '.orgIes-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = priv.dataTableOrgIes.fnGetData($pai);
                var arrRemover = {
                    iesId: data['ies_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de ies "' + data['ies_id'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        priv.addOverlay($('.widget-body'), 'Aguarde, solicitando remoção de registro de ies...');
                        $.ajax({
                            url: priv.options.urlRemove,
                            type: 'POST',
                            dataType: 'json',
                            data: arrRemover,
                            success: function (data) {
                                if (data.erro) {
                                    $.smallBox({
                                        title: "Não foi possível remover registro de ies:",
                                        content: "<i>" + data['erroDescricao'] + "</i>",
                                        color: "#C46A69",
                                        icon: "fa fa-times",
                                        timeout: 10000
                                    });

                                    priv.removeOverlay($('.widget-body'));
                                } else {
                                    priv.dataTableOrgIes.api().ajax.reload(null, false);
                                    $.smallBox({
                                        title: "Sucesso!",
                                        content: "<i>Registro de ies removido!</i>",
                                        color: "#739e73",
                                        icon: "fa fa-check-circle",
                                        timeout: 10000
                                    });
                                }
                            }
                        });
                    }
                });

                e.preventDefault();
            });
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
        };
    };

    $.orgIesIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.organizacao.org-ies.index");

        if (!obj) {
            obj = new OrgIesIndex();
            obj.run(params);
            $(window).data('universa.organizacao.org-ies.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);