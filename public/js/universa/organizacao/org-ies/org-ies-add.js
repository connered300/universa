(function ($, window, document) {
    'use strict';

    var OrgIesAdd = function () {
        VersaShared.call(this);
        var __orgIesAdd = this;

        this.defaults = {
            steps: [],
            validations: [],
            options: {
                url: {
                    gerenciadorDeArquivosThumb: '',
                    gerenciadorDeArquivosDownload: '',
                    pessoaJuridica: '',
                    pessoaFisica: '',
                    buscaCEP: ''
                },
                data: {
                    arrEstados: [],
                },
                value: {
                    arq: null,
                    arqCarteirinha: null,
                    pesMantenedora: null,
                    diretoriaPes: null,
                    secretariaPes: null,
                },
            }
        };

        this.setSteps = function () {
            var vfile = $('#arq').vfile({
                thumbBaseUrl: __orgIesAdd.options.url.gerenciadorDeArquivosThumb,
                downloadBaseUrl: __orgIesAdd.options.url.gerenciadorDeArquivosDownload
            });

            var arqCarteirinha = $('#arqCarteirinha').vfile({
                thumbBaseUrl: __orgIesAdd.options.url.gerenciadorDeArquivosThumb,
                downloadBaseUrl: __orgIesAdd.options.url.gerenciadorDeArquivosDownload
            });

            if (__orgIesAdd.options.value.arq) {
                vfile.setSelection(__orgIesAdd.options.value.arq);
            }

            if (__orgIesAdd.options.value.arqCarteirinha) {
                arqCarteirinha.setSelection(__orgIesAdd.options.value.arqCarteirinha);
            }

            $("#pesMantenedora").select2({
                language: 'pt-BR',
                ajax: {
                    url: __orgIesAdd.options.url.pessoaJuridica,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            var text = [];

                            if (el['pesCnpj']) {text.push(el['pesCnpj'])}
                            if (el['pesNomeFantasia']) {text.push(el['pesNomeFantasia'])}
                            if (el['pesNome']) {text.push(el['pesNome'])}

                            return {id: el.pesId, text: text.join(' - ')};
                        });

                        return {results: transformed};
                    },
                },
                createSearchChoice: function (term) {
                    return {
                        id: term,
                        pesId: '',
                        pesNome: term,
                        text: term + ' (Criar novo registro)'
                    };
                },
                allowClear: true,
                minimumInputLength: 1
            });

            $("#diretoriaPes, #secretariaPes").select2({
                language: 'pt-BR',
                ajax: {
                    url: __orgIesAdd.options.url.pessoaFisica,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            var text = [];

                            if (el['pesCpf']) {text.push(el['pesCpf'])}
                            if (el['pesNome']) {text.push(el['pesNome'])}

                            return {id: el.pesId, text: text.join(' - ')};
                        });

                        return {results: transformed};
                    },
                },
                createSearchChoice: function (term) {
                    return {
                        id: term,
                        pesId: '',
                        pesNome: term,
                        text: term + ' (Criar novo registro)'
                    };
                },
                allowClear: true,
                minimumInputLength: 1
            });

            $('#secretariaPes').select2('data', __orgIesAdd.options.value.secretariaPes || null);
            $('#diretoriaPes').select2('data', __orgIesAdd.options.value.diretoriaPes || null);
            $('#pesMantenedora').select2('data', __orgIesAdd.options.value.pesMantenedora || null);

            $('.editarPessoa').click(function (e) {
                var $elemento = $(this).parent().find('>input');
                var id = $elemento.attr('id');
                var data = $elemento.select2('data') || [];

                if (id == 'pes' || id == 'pesMantenedora') {
                    $('#pessoa-juridica-editar').modal('show');

                    var pessoaJuridicaAdd = $.pessoaJuridicaAdd();
                    pessoaJuridicaAdd.setCallback('aposSalvar', function (dados, retorno) {
                        var text = [];

                        if (dados['pesCnpj']) {text.push(dados['pesCnpj'])}
                        if (dados['pesNomeFantasia']) {text.push(dados['pesNomeFantasia'])}
                        if (dados['pesNome']) {text.push(dados['pesNome'])}

                        dados['id'] = dados['pesId'];
                        dados['text']=text.join(' - ');

                        $elemento.select2('data', dados);

                        $('#pessoa-juridica-editar').modal('hide');
                    });
                    pessoaJuridicaAdd.buscarDadosPessoa($elemento.val());
                    $('#pessoa-juridica-editar [name=pesNome]').val(data['pesNome'] || '');
                } else if (id == 'diretoriaPes' || id == 'secretariaPes') {
                    $('#pessoa-fisica-editar').modal('show');
                    var pessoaFisicaAdd = $.pessoaFisicaAdd();
                    pessoaFisicaAdd.setCallback('aposSalvar', function (dados, retorno) {
                        var text = [];

                        if (dados['pesCpf']) {text.push(dados['pesCpf'])}
                        if (dados['pesNome']) {text.push(dados['pesNome'])}

                        $elemento.select2('data', {id: dados['pesId'], text: text.join(' - ')});

                        $('#pessoa-fisica-editar').modal('hide');
                    });
                    pessoaFisicaAdd.buscarDadosPessoa($elemento.val());

                    $('#pessoa-fisica-editar [name=pesNome]').val(data['pesNome'] || '');
                }

                e.preventDefault();
            });

            $('#pesMantenedora, #diretoriaPes, #secretariaPes').change(function () {
                var data = $(this).select2('data') || [];

                if (Object.keys(data).length > 0) {
                    $(this).next('.editarPessoa').removeClass('hidden');

                    if (!$.isNumeric(data['id']) && data['pesNome']) {
                        $(this).next('.editarPessoa').click();
                    }
                } else {
                    $(this).next('.editarPessoa').addClass('hidden');
                }
            }).trigger('change');


            $("#iesEndCep").inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99999-999']});
            $('#iesEndNumero').inputmask({showMaskOnHover: false, mask: ['9{0,9}']});
            $(".buscarCep").click(function () {
                var $element = $(this);
                var iesEndCep = $(this).closest('form').find("#iesEndCep").val();
                var iesEndCepNums = iesEndCep.replace(/[^0-9]/g, '');

                if (iesEndCepNums.length < 8) {
                    __orgIesAdd.showNotificacaoWarning('CEP inválido ou não encontrado!');

                    return;
                }

                $element.prop('disabled', true);

                $.ajax({
                    url: __orgIesAdd.options.url.buscaCEP,
                    dataType: 'json',
                    type: 'post',
                    data: {cep: iesEndCep},
                    success: function (data) {
                        $element.prop('disabled', false);

                        if (!data.dados) {
                            __orgIesAdd.showNotificacaoWarning('CEP inválido ou não encontrado!');
                        }

                        $element.closest('form').find("#iesEndLogradouro").val(
                            ((data.dados.tip_logr_nome || '') + ' ' + (data.dados.logr_nome || '')).trim()
                        );
                        $element.closest('form').find("#iesEndCidade").val(data.dados.cid_nome || '');
                        $element.closest('form').find("#iesEndEstado").val(data.dados.est_uf || '').trigger('change');
                        $element.closest('form').find("#iesEndBairro").val(data.dados.bai_nome || '');
                    },
                    erro: function () {
                        __orgIesAdd.showNotificacaoDanger('Falha ao buscar CEP!');
                        $element.prop('disabled', false);
                    }
                });
            });
            $('#iesEndCidade')
                .blur(function () {$(this).removeClass('ui-autocomplete-loading');})
                .autocomplete({
                    source: function (request, response) {
                        var $this = $(this);
                        var $element = $(this.element);
                        var previous_request = $element.data("jqXHR");

                        if (previous_request) {
                            previous_request.abort();
                        }

                        $element.data("jqXHR", $.ajax({
                            url: __orgIesAdd.options.url.buscaCEP,
                            data: {cidade: request.term},
                            type: 'POST',
                            dataType: "json",
                            success: function (data) {
                                var transformed = $.map(data.dados || [], function (el) {
                                    el.label = el.cid_nome;

                                    return el;
                                });

                                response(transformed);
                            }
                        }));
                    },
                    minLength: 2,
                    focus: function (event, ui) {
                        var $element = $(this);
                        $element.val(ui.item.cid_nome);

                        return false;
                    },
                    select: function (event, ui) {
                        var $element = $(this);
                        $element.val(ui.item.cid_nome);
                        $element.closest('form').find("#iesEndEstado").val(ui.item.est_uf).trigger('change');

                        return false;
                    }
                });

            $('#iesEndEstado').select2({
                language: 'pt-BR',
                allowClear: true,
                data: function () {
                    return {results: __orgIesAdd.options.data.arrEstados || []};
                }
            });

            $("#iesTelefone").inputmask({
                showMaskOnHover: false, clearIncomplete: true, mask: ['(99) 9999-9999', '(99) 99999-9999']
            });
        };

        __orgIesAdd.setValidations = function () {
            var submitHandler = function (form) {
                form.submit();
            };

            $("#org-ies-form").validate({
                submitHandler: submitHandler,
                rules: {
                    iesNome: {maxlength: 255, required: true},
                    iesSigla: {maxlength: 50, required: true},
                    iesEndNumero: {maxlength: 11, number: true},
                    iesEndCep: {maxlength: 45},
                    iesEndBairro: {maxlength: 45},
                    iesEndCidade: {maxlength: 45},
                    iesEndEstado: {maxlength: 45},
                    iesEndLogradouro: {maxlength: 45},
                    iesTelefone: {telefoneValido: true, maxlength: 15},
                    iesEmail: {maxlength: 100, email: true},
                    pesMantenedora: {number: true, required: true},
                    iesIdSede: {maxlength: 11, number: true},
                    iesNumeroMec: {maxlength: 45, required: true},
                    iesOrganizacaoAcademica: {maxlength: 45, required: true},
                    diretoriaPes: {number: true, required: true},
                    secretariaPes: {number: true, required: true},
                    arq: {required: true}
                },
                messages: {
                    iesNome: {maxlength: 'Tamanho máximo: 255!', required: 'Campo obrigatório!'},
                    iesSigla: {maxlength: 'Tamanho máximo: 50!', required: 'Campo obrigatório!'},
                    iesEndNumero: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!'},
                    iesEndCep: {maxlength: 'Tamanho máximo: 45!'},
                    iesEndBairro: {maxlength: 'Tamanho máximo: 45!'},
                    iesEndCidade: {maxlength: 'Tamanho máximo: 45!'},
                    iesEndEstado: {maxlength: 'Tamanho máximo: 45!'},
                    iesEndLogradouro: {maxlength: 'Tamanho máximo: 45!'},
                    iesTelefone: {maxlength: 'Tamanho máximo: 15!'},
                    iesEmail: {maxlength: 'Tamanho máximo: 100!', email: 'Email inválido!'},
                    pesMantenedora: {
                        maxlength: 'Tamanho máximo: 11!', number: 'Preencha os dados da instituição!',
                        required: 'Campo obrigatório!'
                    },
                    iesIdSede: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!'},
                    iesNumeroMec: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                    iesOrganizacaoAcademica: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                    diretoriaPes: {number: 'Preencha os dados da instituição!', required: 'Campo obrigatório!'},
                    secretariaPes: {number: 'Preencha os dados da instituição!', required: 'Campo obrigatório!'},
                    arq: {required: 'Campo obrigatório!'}
                },
                ignore: '.ignore'
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.orgIesAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.organizacao.org-ies.add");

        if (!obj) {
            obj = new OrgIesAdd();
            obj.run(params);
            $(window).data('universa.organizacao.org-ies.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);