(function ($, window, document) {
    'use strict';

    var OrgUnidadeEstudoAdd = function () {
        VersaShared.call(this);
        var __orgUnidadeEstudoAdd = this;
        this.defaults = {
            url: {
                orgAgenteEducacional: '',
                orgCampus: '',
                buscaCEP: ''
            },
            data: {
                arrEstados: [],
            },
            value: {
                agentes: null,
                camp: null
            },
            datatables: {},
            wizardElement: '#org-unidade-estudo-wizard',
            formElement: '#org-unidade-estudo-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            var $form = $(__orgUnidadeEstudoAdd.options.formElement);
            $("#agentes").select2({
                language: 'pt-BR',
                multiple: true,
                ajax: {
                    url: __orgUnidadeEstudoAdd.options.url.orgAgenteEducacional,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.pes_nome, id: el.pes_id};
                        });

                        return {results: transformed};
                    }
                }
            });

            $('#agentes').select2("data", __orgUnidadeEstudoAdd.getAgentes());

            $("#camp").select2({
                language: 'pt-BR',
                ajax: {
                    url: __orgUnidadeEstudoAdd.options.url.orgCampus,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.camp_nome, id: el.camp_id};
                        });

                        return {results: transformed};
                    }
                }
            });

            if (__orgUnidadeEstudoAdd.getCamp()) {
                $('#camp').select2("data", __orgUnidadeEstudoAdd.getCamp());
            }

            $("#unidadeEndCep").inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99999-999']});
            $('#unidadeEndNumero').inputmask({showMaskOnHover: false, mask: ['9{0,9}']});
            $(".buscarCep").click(function () {
                var $element = $(this);
                var unidadeEndCep = $(this).closest('form').find("#unidadeEndCep").val();
                var unidadeEndCepNums = unidadeEndCep.replace(/[^0-9]/g, '');

                if (unidadeEndCepNums.length < 8) {
                    __orgUnidadeEstudoAdd.showNotificacaoWarning('CEP inválido ou não encontrado!');

                    return;
                }

                $element.prop('disabled', true);

                $.ajax({
                    url: __orgUnidadeEstudoAdd.options.url.buscaCEP,
                    dataType: 'json',
                    type: 'post',
                    data: {cep: unidadeEndCep},
                    success: function (data) {
                        $element.prop('disabled', false);

                        if (!data.dados) {
                            __orgUnidadeEstudoAdd.showNotificacaoWarning('CEP inválido ou não encontrado!');
                        }

                        $element.closest('form').find("[name=unidadeEndLogradouro]").val(
                            ((data.dados.tip_logr_nome || '') + ' ' + (data.dados.logr_nome || '')).trim()
                        );
                        $element.closest('form').find("[name=unidadeEndCidade]").val(data.dados.cid_nome || '');
                        $element.closest('form').find("[name=unidadeEndEstado]").val(data.dados.est_uf || '').trigger('change');
                        $element.closest('form').find("[name=unidadeEndBairro]").val(data.dados.bai_nome || '');
                    },
                    erro: function () {
                        __orgUnidadeEstudoAdd.showNotificacaoDanger('Falha ao buscar CEP!');
                        $element.prop('disabled', false);
                    }
                });
            });
            $('#unidadeEndCidade')
                .blur(function () {
                    $(this).removeClass('ui-autocomplete-loading');
                })
                .autocomplete({
                    source: function (request, response) {
                        var $this = $(this);
                        var $element = $(this.element);
                        var previous_request = $element.data("jqXHR");

                        if (previous_request) {
                            previous_request.abort();
                        }

                        $element.data("jqXHR", $.ajax({
                            url: __orgUnidadeEstudoAdd.options.url.buscaCEP,
                            data: {cidade: request.term},
                            type: 'POST',
                            dataType: "json",
                            success: function (data) {
                                var transformed = $.map(data.dados || [], function (el) {
                                    el.label = el.cid_nome;

                                    return el;
                                });

                                response(transformed);
                            }
                        }));
                    },
                    minLength: 2,
                    focus: function (event, ui) {
                        var $element = $(this);
                        $element.val(ui.item.cid_nome);

                        return false;
                    },
                    select: function (event, ui) {
                        var $element = $(this);
                        $element.val(ui.item.cid_nome);
                        $element.closest('form').find("#unidadeEndEstado").val(ui.item.est_uf || '').trigger('change');

                        return false;
                    }
                });
            $form.find('[name=unidadeEndEstado]').select2({
                language: 'pt-BR',
                allowClear: true,
                data: function () {
                    return {results: __orgUnidadeEstudoAdd.options.data.arrEstados || []};
                }
            });

            $("#unidadeTelefone").inputmask({
                showMaskOnHover: false, clearIncomplete: true, mask: ['(99) 9999-9999', '(99) 99999-9999']
            });
        };

        this.setAgentes = function (agentes) {
            this.options.value.agentes = agentes || null;
        };

        this.getAgentes = function () {
            return this.options.value.agentes || null;
        };

        this.setCamp = function (camp) {
            this.options.value.camp = camp || null;
        };

        this.getCamp = function () {
            return this.options.value.camp || null;
        };
        this.setValidations = function () {
            var submitHandler = function (form) {
                form.submit();
            };

            $(__orgUnidadeEstudoAdd.options.formElement).validate({
                submitHandler: submitHandler,
                rules: {
                    unidadeId: {maxlength: 10, number: true, required: true},
                    unidadeNome: {maxlength: 45, required: true},
                    agentes: {maxlength: 11, number: true, required: false},
                    camp: {maxlength: 11, number: true, required: true},
                    unidadeEndNumero: {maxlength: 11, number: true},
                    unidadeEndCep: {maxlength: 45},
                    unidadeEndBairro: {maxlength: 45},
                    unidadeEndCidade: {maxlength: 45},
                    unidadeEndEstado: {maxlength: 45},
                    unidadeEndLogradouro: {maxlength: 45},
                    unidadeTelefone: {telefoneValido: true, maxlength: 15},
                    unidadeEmail: {maxlength: 100, email: true}
                },
                messages: {
                    unidadeId: {
                        maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    unidadeNome: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                    agentes: {
                        maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    camp: {
                        maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    unidadeEndNumero: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!'},
                    unidadeEndCep: {maxlength: 'Tamanho máximo: 45!'},
                    unidadeEndBairro: {maxlength: 'Tamanho máximo: 45!'},
                    unidadeEndCidade: {maxlength: 'Tamanho máximo: 45!'},
                    unidadeEndEstado: {maxlength: 'Tamanho máximo: 45!'},
                    unidadeEndLogradouro: {maxlength: 'Tamanho máximo: 45!'},
                    unidadeTelefone: {maxlength: 'Tamanho máximo: 15!'},
                    unidadeEmail: {maxlength: 'Tamanho máximo: 100!'},
                },
                ignore: '.ignore'
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.orgUnidadeEstudoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.organizacao.org-unidade-estudo.add");

        if (!obj) {
            obj = new OrgUnidadeEstudoAdd();
            obj.run(params);
            $(window).data('universa.organizacao.org-unidade-estudo.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);