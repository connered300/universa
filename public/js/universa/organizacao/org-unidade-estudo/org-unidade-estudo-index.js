(function ($, window, document) {
    'use strict';
    var OrgUnidadeEstudoIndex = function () {
        VersaShared.call(this);
        var __orgUnidadeEstudoIndex = this;
        this.defaults = {
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                orgUnidadeEstudo: null
            },
        };

        this.setSteps = function () {
            var colNum = 0;
            __orgUnidadeEstudoIndex.options.datatables.orgUnidadeEstudo = $('#dataTableOrgUnidadeEstudo').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __orgUnidadeEstudoIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary orgUnidadeEstudo-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger orgUnidadeEstudo-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                        }

                        __orgUnidadeEstudoIndex.removeOverlay($('.widget-body'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "unidade_id", targets: colNum++, data: "unidade_id"},
                    {name: "unidade_nome", targets: colNum++, data: "unidade_nome"},
                    {name: "pes_nome", targets: colNum++, data: "pes_nome"},
                    {name: "camp_nome", targets: colNum++, data: "camp_nome"},
                    {name: "unidade_end_cidade", targets: colNum++, data: "unidade_end_cidade"},
                    {name: "unidade_end_estado", targets: colNum++, data: "unidade_end_estado"},
                    {name: "unidade_telefone", targets: colNum++, data: "unidade_telefone"},
                    {name: "unidade_email", targets: colNum++, data: "unidade_email"},
                    {name: "unidade_id", targets: colNum++, data: "acao"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $('#dataTableOrgUnidadeEstudo').on('click', '.orgUnidadeEstudo-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __orgUnidadeEstudoIndex.options.datatables.orgUnidadeEstudo.fnGetData($pai);

                location.href = __orgUnidadeEstudoIndex.options.url.edit + '/' + data['unidade_id'];
            });

            $('#dataTableOrgUnidadeEstudo').on('click', '.orgUnidadeEstudo-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __orgUnidadeEstudoIndex.options.datatables.orgUnidadeEstudo.fnGetData($pai);
                var arrRemover = {
                    unidadeId: data['unidade_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de unidade estudo "' + data['unidade_nome'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        __orgUnidadeEstudoIndex.addOverlay($('.widget-body'),
                                                           'Aguarde, solicitando remoção de registro de unidade estudo...');
                        $.ajax({
                            url: __orgUnidadeEstudoIndex.options.url.remove,
                            type: 'POST',
                            dataType: 'json',
                            data: arrRemover,
                            success: function (data) {
                                if (data.erro) {
                                    __orgUnidadeEstudoIndex.showNotificacaoDanger(
                                        "Não foi possível remover registro de unidade estudo:\n" +
                                        "<i>" + data['erroDescricao'] + "</i>"
                                    );
                                    __orgUnidadeEstudoIndex.removeOverlay($('.widget-body'));
                                } else {
                                    __orgUnidadeEstudoIndex.options.datatables.orgUnidadeEstudo.api().ajax.reload(null, false);
                                    __orgUnidadeEstudoIndex.showNotificacaoSuccess(
                                        "Registro de unidade estudo removido!"
                                    );
                                }
                            }
                        });
                    }
                });

                e.preventDefault();
            });
        };

        this.run = function (opts) {
            __orgUnidadeEstudoIndex.setDefaults(opts);
            __orgUnidadeEstudoIndex.setSteps();
        };
    };

    $.orgUnidadeEstudoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.organizacao.org-unidade-estudo.index");

        if (!obj) {
            obj = new OrgUnidadeEstudoIndex();
            obj.run(params);
            $(window).data('universa.organizacao.org-unidade-estudo.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);