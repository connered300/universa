(function ($, window, document) {
    'use strict';

    var OrgCampusIndex = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlSearch: '',
                urlEdit: ''
            }
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);
        };

        priv.addOverlay = function (el, mensagem) {
            mensagem = mensagem || 'Aguarde...';
            el.append('<div class="app-overlay">' + mensagem + '</div>');
        };

        priv.removeOverlay = function (el) {
            el.find('>.app-overlay').remove();
        };

        priv.setSteps = function () {
            var colNum = 0;
            priv.dataTableOrgCampus = $('#dataTableOrgCampus').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: priv.options.urlSearch,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var columns = [
                            "camp_nome", "instituicao", "diretoria", "secretaria", "camp_telefone", "camp_email",
                            "camp_site"
                        ];

                        for (var row in data) {
                            var urlEdit = priv.options.urlEdit + '/' + data[row]['camp_id'];

                            for (var colPos in columns) {
                                var col = columns[colPos];
                                data[row][col] = data[row][col] || '';
                                data[row][col + '_formatado'] = "<a href='" + urlEdit + "'>" + data[row][col] + "</a>";
                            }

                        }

                        priv.removeOverlay($('.widget-body'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "camp_nome", targets: colNum++, data: "camp_nome_formatado"},
                    {name: "instituicao", targets: colNum++, data: "instituicao_formatado"},
                    {name: "diretoria", targets: colNum++, data: "diretoria_formatado"},
                    {name: "secretaria", targets: colNum++, data: "secretaria_formatado"},
                    {name: "camp_telefone", targets: colNum++, data: "camp_telefone_formatado"},
                    {name: "camp_email", targets: colNum++, data: "camp_email_formatado"},
                    {name: "camp_site", targets: colNum++, data: "camp_site_formatado"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $('#dataTableOrgCampus').on('click', '.orgCampus-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = priv.dataTableOrgCampus.fnGetData($pai);
                var arrRemover = {
                    campusId: data['camp_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de câmpus "' + data['camp_id'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        priv.addOverlay($('.widget-body'), 'Aguarde, solicitando remoção de registro de câmpus...');
                        $.ajax({
                            url: priv.options.urlRemove,
                            type: 'POST',
                            dataType: 'json',
                            data: arrRemover,
                            success: function (data) {
                                if (data.erro) {
                                    $.smallBox({
                                        title: "Não foi possível remover registro de câmpus:",
                                        content: "<i>" + data['erroDescricao'] + "</i>",
                                        color: "#C46A69",
                                        icon: "fa fa-times",
                                        timeout: 10000
                                    });

                                    priv.removeOverlay($('.widget-body'));
                                } else {
                                    priv.dataTableOrgCampus.api().ajax.reload(null, false);
                                    $.smallBox({
                                        title: "Sucesso!",
                                        content: "<i>Registro de câmpus removido!</i>",
                                        color: "#739e73",
                                        icon: "fa fa-check-circle",
                                        timeout: 10000
                                    });
                                }
                            }
                        });
                    }
                });

                e.preventDefault();
            });
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
        };
    };

    $.orgCampusIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.organizacao.org-campus.index");

        if (!obj) {
            obj = new OrgCampusIndex();
            obj.run(params);
            $(window).data('universa.organizacao.org-campus.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);