(function ($, window, document) {
    'use strict';
    var OrgCampusAdd = function () {
        VersaShared.call(this);

        var __OrgCampusAdd = this;

        this.defaults = {
            url: {
                gerenciadorDeArquivosThumb: '',
                gerenciadorDeArquivosDownload: '',
                pessoaFisica: '',
                ies: '',
                buscaCEP: '',
            },
            value: {
                arq: null,
                ies: null,
                arqCarteirinha: null,
                diretoriaPes: null,
                predios: null,
            },
            data: {
                arrEstados: [],
            },
            datatables: {
                predios: null,
                salas: null,
            },
            wizardElement: '#org-campus-wizard',
            formElement: '#org-campus-form',
            validator: null
        };

        this.steps = {};

        this.steps.dadosBasicos = {
            init: function () {
                var vfile = $('#arq').vfile({
                    thumbBaseUrl: __OrgCampusAdd.options.url.gerenciadorDeArquivosThumb,
                    downloadBaseUrl: __OrgCampusAdd.options.url.gerenciadorDeArquivosDownload
                });

                var arqCarteirinha = $('#arqCarteirinha').vfile({
                    thumbBaseUrl: __OrgCampusAdd.options.url.gerenciadorDeArquivosThumb,
                    downloadBaseUrl: __OrgCampusAdd.options.url.gerenciadorDeArquivosDownload
                });


                if (__OrgCampusAdd.options.value.arq) {
                    vfile.setSelection(__OrgCampusAdd.options.value.arq);
                }

                if (__OrgCampusAdd.options.value.arqCarteirinha) {
                    arqCarteirinha.setSelection(__OrgCampusAdd.options.value.arqCarteirinha);
                }

                $("#ies").select2({
                    language: 'pt-BR',
                    ajax: {
                        url: __OrgCampusAdd.options.url.ies,
                        dataType: 'json',
                        delay: 250,
                        data: function (query) {
                            return {query: query};
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                el['id'] = el['ies_id'] || el['iesId'];
                                el['text'] = el['ies_nome'] || el['iesNome'];

                                return el;
                            });

                            return {results: transformed};
                        },
                    },
                    allowClear: true,
                    minimumInputLength: 1
                });

                $("#diretoriaPes, #secretariaPes").select2({
                    language: 'pt-BR',
                    ajax: {
                        url: __OrgCampusAdd.options.url.pessoaFisica,
                        dataType: 'json',
                        delay: 250,
                        data: function (query) {
                            var data = {query: query};
                            return data;
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                var text = [];

                                if (el['pesCpf']) {
                                    text.push(el['pesCpf'])
                                }
                                if (el['pesNome']) {
                                    text.push(el['pesNome'])
                                }

                                return {id: el.pesId, text: text.join(' - ')};
                            });

                            return {results: transformed};
                        },
                    },
                    createSearchChoice: function (term) {
                        return {
                            id: term,
                            pesId: '',
                            pesNome: term,
                            text: term + ' (Criar novo registro)'
                        };
                    },
                    allowClear: true,
                    minimumInputLength: 1
                });

                $('#secretariaPes').select2('data', __OrgCampusAdd.options.value.secretariaPes || null);
                $('#diretoriaPes').select2('data', __OrgCampusAdd.options.value.diretoriaPes || null);
                $('#ies').select2('data', __OrgCampusAdd.options.value.ies || null);

                $('.editarPessoa').click(function (e) {
                    var $elemento = $(this).parent().find('>input');
                    var id = $elemento.attr('id');
                    var data = $elemento.select2('data') || [];

                    $('#pessoa-fisica-editar').modal('show');
                    var pessoaFisicaAdd = $.pessoaFisicaAdd();
                    pessoaFisicaAdd.setCallback('aposSalvar', function (dados, retorno) {
                        var text = [];

                        if (dados['pesCpf']) {
                            text.push(dados['pesCpf'])
                        }
                        if (dados['pesNome']) {
                            text.push(dados['pesNome'])
                        }

                        dados['id'] = dados['pesId'];
                        dados['text'] = text.join(' - ');

                        $elemento.select2('data', dados);

                        $('#pessoa-fisica-editar').modal('hide');
                    });
                    pessoaFisicaAdd.buscarDadosPessoa($elemento.val());

                    $('#pessoa-fisica-editar [name=pesNome]').val(data['pesNome'] || '');

                    e.preventDefault();
                });

                $('#diretoriaPes, #secretariaPes').change(function () {
                    var data = $(this).select2('data') || [];

                    if (Object.keys(data).length > 0) {
                        $(this).next('.editarPessoa').removeClass('hidden');

                        if (!$.isNumeric(data['id']) && data['pesNome']) {
                            $(this).next('.editarPessoa').click();
                        }
                    } else {
                        $(this).next('.editarPessoa').addClass('hidden');
                    }
                }).trigger('change');

                $("#campTelefone").inputmask({
                    showMaskOnHover: false, clearIncomplete: true, mask: ['(99) 9999-9999', '(99) 99999-9999']
                });
            },
            validate: function () {
                var $form = $(__OrgCampusAdd.options.formElement);
                var validacao = $form.validate({ignore: '.ignore'});
                validacao.settings.rules = {
                    ies: {number: true, required: true},
                    campusNumeroMec: {maxlength: 45, required: true},
                    campusOrganizacaoAcademica: {maxlength: 45, required: true},
                    diretoriaPes: {number: true, required: true},
                    secretariaPes: {number: true, required: true},
                    campTelefone: {telefoneValido: true,required: true},
                    campEmail: {required: true, email: true},
                    campSite: {url: true},
                    arq: {required: true}
                };

                validacao.settings.messages = {
                    ies: {
                        maxlength: 'Tamanho máximo: 11!',
                        number: 'Preencha a instituição!',
                        required: 'Campo obrigatório!'
                    },
                    diretoriaPes: {
                        required: 'Selecione a pessoa prédio pela diretoria do câmpus!'
                    },
                    secretariaPes: {
                        required: 'Selecione a pessoa prédio pela secretaria do câmpus!'
                    },
                    arq: {required: 'Campo obrigatório!'},
                    campTelefone: {required: 'Campo obrigatório!'},
                    campEmail: {required: 'Campo obrigatório!', email: 'Email inválido!'},
                    campSite: {url: 'Endereço inválido!'},
                };

                return !validacao.valid()
            }
        };
        this.steps.predios = {
            init: function () {
                __OrgCampusAdd.steps.predios.tratamentosModal();
                var colNum = 0;
                __OrgCampusAdd.options.datatables.predios = $('#dataTablePredios').dataTable({
                    processing: true,
                    columnDefs: [
                        {name: "predioTipo", targets: colNum++, data: "predioTipo"},
                        {name: "predioNome", targets: colNum++, data: "predioNome"},
                        {name: "predioAcoes", targets: colNum++, data: 'predioAcoes'}
                    ],
                    "dom": "<'dt-toolbar'<'col-xs-3 dt-toolbar-predio no-padding'B><'col-xs-6 predio-aviso'><'col-xs-3 no-padding'f>r>t",
                    "autoWidth": true,
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "Procurar: ",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    },
                    order: [[1, 'desc']]
                });

                $('.dt-toolbar-predio').append(
                    '<div class="btn-group">' +
                    '<button type="button" class="btn btn-primary" id="btn-add-predio">Adicionar</button>' +
                    '</div>'
                );

                $('#btn-add-predio').click(function () {
                    var data = {'GUID': __OrgCampusAdd.createGUID()};
                    __OrgCampusAdd.steps.predios.showModalResponsavel(data);
                });


                $('#dataTablePredios').on('click', '.btn-item-edit', function () {
                    var $pai = $(this).closest('tr');
                    var data = __OrgCampusAdd.options.datatables.predios.fnGetData($pai);
                    data['GUID'] = data['GUID'] || __OrgCampusAdd.createGUID();
                    __OrgCampusAdd.steps.predios.showModalResponsavel(data);
                });

                $('#btn-salvar-predio').click(function (e) {
                    var $form = $(__OrgCampusAdd.options.formElement);
                    var validacao = $form.validate({ignore: '.ignore'});
                    validacao.settings.rules = {
                        predioTipo: {required: true},
                        predioNome: {required: true},
                        predioCep: {required: false},
                        predioLogradouro: {required: true},
                        predioNumero: {required: false, number: true},
                        predioComplemento: {required: false},
                        predioBairro: {required: false},
                        predioCidade: {required: true},
                        predioEstado: {required: true},
                    };
                    validacao.settings.messages = {
                        predioTipo: {required: 'Campo obrigatório!'},
                        predioNome: {required: 'Campo obrigatório!'},
                        predioCep: {required: 'Campo obrigatório!'},
                        predioLogradouro: {required: 'Campo obrigatório!'},
                        predioNumero: {required: 'Campo obrigatório!', number: 'Número inválido!'},
                        predioComplemento: {required: 'Campo obrigatório!'},
                        predioBairro: {required: 'Campo obrigatório!'},
                        predioCidade: {required: 'Campo obrigatória!'},
                        predioEstado: {required: 'Campo obrigatório!'},
                    };

                    if (!$('#predio-form').valid()) {
                        var erros = [];

                        $.each(validacao.errorList, function (i, item) {
                            var erro = $(item.element)
                                .closest('.form-group')
                                .find('label')
                                .text();
                            erro += ' ' + item.message;
                            erros.push(erro);
                        });

                        __OrgCampusAdd.showNotificacaoWarning(
                            'Não foi possível salvar o prédio.<br>' +
                            erros.join('<br>')
                        );
                        return;
                    }

                    var arrDados = $('#predio-form').serializeJSON();
                    arrDados['GUID'] = arrDados['GUID'] || __OrgCampusAdd.createGUID();

                    $('#modal-predio').modal('hide');
                    $('#predio-form').trigger("reset");
                    __OrgCampusAdd.steps.predios.editarResponsavelItem(arrDados);
                });

                $('#dataTablePredios').on('click', '.item-remove', function (e) {
                    var $pai = $(this).closest('tr');
                    var predioData = __OrgCampusAdd.options.datatables.predios.fnGetData($pai);
                    predioData['GUID'] = predioData['GUID'] || __OrgCampusAdd.createGUID();

                    $.SmartMessageBox({
                        title: "Confirme a operação:",
                        content: 'Deseja realmente remover este prédio?',
                        buttons: "[Cancelar][Ok]"
                    }, function (ButtonPress) {
                        if (ButtonPress == "Ok") {
                            var arrPredioManter = $.grep(
                                __OrgCampusAdd.options.value.predios || [],
                                function (item) {
                                    item['GUID'] = item['GUID'] || __OrgCampusAdd.createGUID();
                                    return (predioData['GUID'] != item['GUID']);
                                }
                            );

                            __OrgCampusAdd.options.value.predios = arrPredioManter;
                            __OrgCampusAdd.steps.predios.redrawDatatablePredios();
                        }
                    });

                    e.preventDefault();
                });

                __OrgCampusAdd.steps.predios.redrawDatatablePredios();
            },
            validate: function () {
                var erros = [], principal = 0, prediosValidacao = {};

                $.each(__OrgCampusAdd.options.value.predios || [], function (i, predio) {
                    if (predio['predioTipo'] == 'principal') {
                        principal++;
                    }

                    var chave = predio['predioNome'].trim();

                    prediosValidacao[chave] = prediosValidacao[chave] || 0;
                    prediosValidacao[chave] += 1;
                });

                if (principal == 0) {
                    erros.push('Insira ao menos um prédio para o câmpus!');
                } else if (principal > 1) {
                    erros.push('Insira somente um prédio principal para o câmpus!');
                }

                for (var predio in prediosValidacao) {
                    if (prediosValidacao[predio] > 1) {
                        erros.push(
                            predio + '<br>' + 'Registrado ' + prediosValidacao[predio] +
                            ' vezes.<br> Remova os itens duplicados.'
                        );
                    }
                }

                if (erros.length > 0) {
                    $.each(erros, function (i, erro) {
                        __OrgCampusAdd.showNotificacaoWarning(erro);
                    });

                    return true;
                }

                return false;
            },
            tratamentosModal: function () {
                $("#predioTipo").select2({language: 'pt-BR'});
                $("#predioCep").inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99999-999']});
                $('#predioNumero').inputmask({showMaskOnHover: false, mask: ['9{0,9}']});
                $(".buscarCep").click(function () {
                    var $element = $(this);
                    var predioCep = $(this).closest('form').find("#predioCep").val();
                    var predioCepNums = predioCep.replace(/[^0-9]/g, '');

                    if (predioCepNums.length < 8) {
                        __OrgCampusAdd.showNotificacaoWarning('CEP inválido ou não encontrado!');

                        return;
                    }

                    $element.prop('disabled', true);

                    $.ajax({
                        url: __OrgCampusAdd.options.url.buscaCEP,
                        dataType: 'json',
                        type: 'post',
                        data: {cep: predioCep},
                        success: function (data) {
                            $element.prop('disabled', false);

                            if (!data.dados) {
                                __OrgCampusAdd.showNotificacaoWarning('CEP inválido ou não encontrado!');
                            }

                            $element.closest('form').find("#predioLogradouro").val(
                                ((data.dados.tip_logr_nome || '') + ' ' + (data.dados.logr_nome || '')).trim()
                            );
                            $element.closest('form').find("#predioCidade").val(data.dados.cid_nome || '');
                            $element.closest('form').find("#predioEstado").val(data.dados.est_uf ||
                                '').trigger('change');
                            $element.closest('form').find("#predioBairro").val(data.dados.bai_nome || '');
                        },
                        erro: function () {
                            __OrgCampusAdd.showNotificacaoDanger('Falha ao buscar CEP!');
                            $element.prop('disabled', false);
                        }
                    });
                });
                $('#predioCidade').blur(
                    function () {
                        $(this).removeClass('ui-autocomplete-loading');
                    }
                ).autocomplete({
                    source: function (request, response) {
                        var $this = $(this);
                        var $element = $(this.element);
                        var previous_request = $element.data("jqXHR");

                        if (previous_request) {
                            previous_request.abort();
                        }

                        $element.data("jqXHR", $.ajax({
                            url: __OrgCampusAdd.options.url.buscaCEP,
                            data: {cidade: request.term},
                            type: 'POST',
                            dataType: "json",
                            success: function (data) {
                                var transformed = $.map(data.dados || [], function (el) {
                                    el.label = el.cid_nome;

                                    return el;
                                });

                                response(transformed);
                            }
                        }));
                    },
                    minLength: 2,
                    focus: function (event, ui) {
                        var $element = $(this);
                        $element.val(ui.item.cid_nome);

                        return false;
                    },
                    select: function (event, ui) {
                        var $element = $(this);
                        $element.val(ui.item.cid_nome);
                        $element.closest('form').find("#predioEstado").val(ui.item.est_uf).trigger('change');

                        return false;
                    }
                });
                $('[name=predioEstado]').select2({
                    language: 'pt-BR',
                    allowClear: true,
                    data: function () {
                        return {results: __OrgCampusAdd.options.data.arrEstados || []};
                    }
                });
            },
            showModalResponsavel: function (data) {
                data = data || {};
                var actionTitle = 'Cadastrar';
                var $form = $('#predio-form');

                if (Object.keys(data).length > 1) {
                    actionTitle = 'Editar';
                }

                $('#modal-predio .modal-title').html(actionTitle + ' prédio');

                data['predioTipo'] = data['predioTipo'] || 'apoio';

                $form[0].reset();
                $form.deserialize(data);

                $('#predioTipo').trigger('change');

                $('#modal-predio').modal();
            },
            editarResponsavelItem: function (dados) {
                var novo = true;
                __OrgCampusAdd.options.value.predios = __OrgCampusAdd.options.value.predios || [];

                for (var i in __OrgCampusAdd.options.value.predios) {
                    var item = __OrgCampusAdd.options.value.predios[i];
                    item['GUID'] = item['GUID'] || __OrgCampusAdd.createGUID();

                    if (dados['GUID'] == item['GUID']) {
                        __OrgCampusAdd.options.value.predios[i] = dados;
                        novo = false;

                        break;
                    }
                }

                if (novo) {
                    __OrgCampusAdd.options.value.predios.push(dados);
                }

                __OrgCampusAdd.steps.predios.redrawDatatablePredios();
            },
            redrawDatatablePredios: function () {
                __OrgCampusAdd.options.datatables.predios.fnClearTable();
                var data = [];

                $.each(__OrgCampusAdd.options.value.predios || [], function (i, item) {
                    item['predioId'] = item['predioId'] || '';
                    item['GUID'] = item['GUID'] || __OrgCampusAdd.createGUID();
                    item['predioAcoes'] = '';
                    item['predioAcoes'] = '\
                <div class="btn-group btn-group-xs text-center">\
                    <a href="#" class="btn btn-primary btn-xs btn-item-edit" title="Editar">\
                        <i class="fa fa-pencil fa-inverse"></i>\
                    </a>\
                    <a href="#" class="btn btn-danger btn-xs item-remove" title="Remover predio" >\
                        <i class="fa fa-times fa-inverse"></i>\
                    </a>\
                </div>';

                    data.push(item);
                });

                if (data.length > 0) {
                    __OrgCampusAdd.options.datatables.predios.fnAddData(data);
                }

                __OrgCampusAdd.options.datatables.predios.fnDraw();
            }
        };

        this.setValidations = function () {
            $(document).on("change", ".form-control:hidden", function () {
                $(__OrgCampusAdd.options.formElement).valid();
            });

            $(this.options.formElement).submit(function (e) {
                var ok = __OrgCampusAdd.validate();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                $('#predios').val(JSON.stringify(__OrgCampusAdd.options.value.predios));
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.loadSteps();
            this.setValidations();
            this.wizard();
        };
    };

    $.orgCampusAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.organizacao.org-campus.add");

        if (!obj) {
            obj = new OrgCampusAdd();
            obj.run(params);
            $(window).data('universa.organizacao.org-campus.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);