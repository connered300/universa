(function ($, window, document) {
    'use strict';

    var OrgEmailContaAdd = function () {
        VersaShared.call(this);
        var __orgEmailContaAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                orgComunicacaoTipo: ''
            },
            data: {},
            value: {
                tipo: null
            },
            datatables: {},
            wizardElement: '#org-email-conta-wizard',
            formElement: '#org-email-conta-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#tipo").select2({
                language: 'pt-BR',
                multiple: true,
                ajax: {
                    url: __orgEmailContaAdd.options.url.orgComunicacaoTipo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query, tipoId: $("#tipo").val()};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.tipo_descricao, id: el.tipo_id};
                        });

                        return {results: transformed};
                    }
                }
            });

            $('#tipo').select2("data", __orgEmailContaAdd.getTipo());

            $("#contaPorta,#contaImapPorta").inputmask({showMaskOnHover: false, mask: ['9{0,9}'], rightAlign: true});
            $("#contaSsl,#contaImapSsl,#contaLead,#contaAtivada").select2({
                language: 'pt-BR', allowClear: true
            }).trigger("change");
        };

        this.setValidations = function () {
            __orgEmailContaAdd.options.validator.settings.rules = {
                contaNome: {maxlength: 100, required: true},
                contaHost: {required: true},
                contaUsuario: {maxlength: 100, required: true},
                contaSenha: {maxlength: 100, required: true},
                contaPorta: {maxlength: 11, number: true, required: true},
                contaSsl: {maxlength: 3},
                contaImapPorta: {maxlength: 11, number: true},
                contaImapSsl: {maxlength: 3},
                contaLead: {maxlength: 3, required: true},
                contaAtivada: {maxlength: 3, required: true}
            };
            __orgEmailContaAdd.options.validator.settings.messages = {
                contaNome: {maxlength: 'Tamanho máximo: 100!', required: 'Campo obrigatório!'},
                contaHost: {required: 'Campo obrigatório!'},
                contaUsuario: {maxlength: 'Tamanho máximo: 100!', required: 'Campo obrigatório!'},
                contaSenha: {maxlength: 'Tamanho máximo: 100!', required: 'Campo obrigatório!'},
                contaPorta: {
                    maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!', required: 'Campo obrigatório!'
                },
                contaSsl: {maxlength: 'Tamanho máximo: 3!'},
                contaImapPorta: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!'},
                contaImapSsl: {maxlength: 'Tamanho máximo: 3!'},
                contaLead: {maxlength: 'Tamanho máximo: 3!', required: 'Campo obrigatório!'},
                contaAtivada: {maxlength: 'Tamanho máximo: 3!', required: 'Campo obrigatório!'}
            };

            $(__orgEmailContaAdd.options.formElement).submit(function (e) {
                var ok = $(__orgEmailContaAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__orgEmailContaAdd.options.formElement);

                if (__orgEmailContaAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __orgEmailContaAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __orgEmailContaAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__orgEmailContaAdd.options.listagem) {
                                    $.orgEmailContaIndex().reloadDataTableOrgEmailConta();
                                    __orgEmailContaAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#org-email-conta-modal').modal('hide');
                                }
                                __orgEmailContaAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.setTipo = function (tipo) {
            this.options.value.tipo = tipo || null;
        };

        this.getTipo = function () {
            return this.options.value.tipo || null;
        };

        this.pesquisaOrgEmailConta = function (conta_id, callback) {
            var $form = $(__orgEmailContaAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __orgEmailContaAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + conta_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __orgEmailContaAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __orgEmailContaAdd.removeOverlay($form);
                },
                erro: function () {
                    __orgEmailContaAdd.showNotificacaoDanger('Erro desconhecido!');
                    __orgEmailContaAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.orgEmailContaAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.organizacao.org-email-conta.add");

        if (!obj) {
            obj = new OrgEmailContaAdd();
            obj.run(params);
            $(window).data('universa.organizacao.org-email-conta.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);