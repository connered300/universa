(function ($, window, document) {
    'use strict';
    var OrgEmailContaIndex = function () {
        VersaShared.call(this);
        var __orgEmailContaIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                orgEmailConta: null
            }
        };

        this.setSteps = function () {
            var colNum = 0;
            __orgEmailContaIndex.options.datatables.orgEmailConta = $('#dataTableOrgEmailConta').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __orgEmailContaIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary orgEmailConta-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger orgEmailConta-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                        }

                        __orgEmailContaIndex.removeOverlay($('#container-org-email-conta'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "conta_id", targets: colNum++, data: "conta_id"},
                    {name: "conta_nome", targets: colNum++, data: "conta_nome"},
                    {name: "conta_host", targets: colNum++, data: "conta_host"},
                    {name: "conta_usuario", targets: colNum++, data: "conta_usuario"},
                    {name: "conta_id", targets: colNum++, data: "acao"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $('#dataTableOrgEmailConta').on('click', '.orgEmailConta-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __orgEmailContaIndex.options.datatables.orgEmailConta.fnGetData($pai);

                if (!__orgEmailContaIndex.options.ajax) {
                    location.href = __orgEmailContaIndex.options.url.edit + '/' + data['conta_id'];
                } else {
                    $.orgEmailContaAdd().pesquisaOrgEmailConta(
                        data['conta_id'],
                        function (data) { __orgEmailContaIndex.showModal(data); }
                    );
                }
            });


            $('#btn-org-email-conta-add').click(function (e) {
                if (__orgEmailContaIndex.options.ajax) {
                    __orgEmailContaIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#dataTableOrgEmailConta').on('click', '.orgEmailConta-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __orgEmailContaIndex.options.datatables.orgEmailConta.fnGetData($pai);
                var arrRemover = {
                    contaId: data['conta_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de conta de e-mail "' + data['conta_nome'] + '"?',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __orgEmailContaIndex.addOverlay(
                                $('#container-org-email-conta'),
                                'Aguarde, solicitando remoção de registro de conta de e-mail...'
                            );
                            $.ajax({
                                url: __orgEmailContaIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __orgEmailContaIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de conta de e-mail:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __orgEmailContaIndex.removeOverlay($('#container-org-email-conta'));
                                    } else {
                                        __orgEmailContaIndex.reloadDataTableOrgEmailConta();
                                        __orgEmailContaIndex.showNotificacaoSuccess(
                                            "Registro de conta de e-mail removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#org-email-conta-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['contaId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['contaId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);

            $("#tipo").select2('data', data['tipo'] || '').trigger("change");
            $("#contaSsl").select2('val', data['contaSsl'] || '').trigger("change");
            $("#contaImapSsl").select2('val', data['contaImapSsl'] || '').trigger("change");
            $("#contaLead").select2('val', data['contaLead'] || '').trigger("change");
            $("#contaAtivada").select2('val', data['contaAtivada'] || '').trigger("change");

            $('#org-email-conta-modal').find('.modal-title').html(actionTitle + ' conta de e-mail');
            $('#org-email-conta-modal').modal();
        };

        this.reloadDataTableOrgEmailConta = function () {
            this.getDataTableOrgEmailConta().api().ajax.reload(null, false);
        };

        this.getDataTableOrgEmailConta = function () {
            if (!__orgEmailContaIndex.options.datatables.orgEmailConta) {
                if (!$.fn.dataTable.isDataTable('#dataTableOrgEmailConta')) {
                    __orgEmailContaIndex.options.datatables.orgEmailConta = $('#dataTableOrgEmailConta').DataTable();
                } else {
                    __orgEmailContaIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __orgEmailContaIndex.options.datatables.orgEmailConta;
        };

        this.run = function (opts) {
            __orgEmailContaIndex.setDefaults(opts);
            __orgEmailContaIndex.setSteps();
        };
    };

    $.orgEmailContaIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.organizacao.org-email-conta.index");

        if (!obj) {
            obj = new OrgEmailContaIndex();
            obj.run(params);
            $(window).data('universa.organizacao.org-email-conta.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);