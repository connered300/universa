(function ($, window, document) {
    'use strict';
    var OrgAgenteEducacionalIndex = function () {
        VersaShared.call(this);
        var __orgAgenteEducacionalIndex = this;
        this.defaults = {
            url: {
                search: '',
                edit: '',
                remove: '',
                cadastroOrigem: '',
                buscaCEP: '',
                comunicacaoModelo: '',
                pesquisaDadosPessoa: ''
            },
            value: {
                agenteInfo: null
            },
            datatables: {
                orgAgenteEducacional: null
            },
            dataTableOrgAgenteEducacional: null

        };

        this.iniciarFiltros = function () {
            var $cidadeAgente = $('#cidadeAgente'),
                $estadoAgente = $('#estadoAgente'),
                $origemCadastro = $('#origemCadastro');

            $estadoAgente.select2({
                language: 'pt-BR',
                minimumInputLength: 1,
                allowClear: true,
                ajax: {
                    url: __orgAgenteEducacionalIndex.options.url.buscaCEP,
                    type: 'POST',
                    dataType: "json",
                    data: function (query) {
                        return {
                            estado: query
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data.dados || [], function (el) {
                            return {text: el.est_nome, id: el.est_uf};
                        });

                        return {results: transformed};
                    }
                }
            }).on('change', function () {
                var estado = $(this).val();
                if (estado != '') {
                    $('#cidadeAgente').select2('enable');

                } else {
                    $('#cidadeAgente').select2('disable');
                    $('#cidadeAgente').select2('val', '').trigger('change');
                }
            });

            $cidadeAgente.select2({
                language: 'pt-BR',
                minimumInputLength: 1,
                allowClear: true,
                ajax: {
                    url: __orgAgenteEducacionalIndex.options.url.buscaCEP,
                    type: 'POST',
                    dataType: "json",
                    data: function (query) {

                        return {
                            estado: $estadoAgente.val(),
                            cidade: query
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data.dados || [], function (el) {
                            return {
                                text: el.cid_nome,
                                id: el.cid_nome,
                                estadoId: el.est_id,
                                estadoNome: el.est_nome
                            };
                        });

                        return {results: transformed};
                    }
                }
            });

            $('#cidadeAgente').select2('disable');

            $origemCadastro.select2({
                language: 'pt-BR',
                allowClear: true,
                multiple: true,
                ajax: {
                    url: __orgAgenteEducacionalIndex.options.url.cadastroOrigem,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.origem_nome, id: el.origem_id};
                        });

                        return {results: transformed};
                    }
                }
            });

            $(document).on('change select2-selected select2-clearing select2-removed', 'input, select', function () {
                    var $item = $(this);

                    if ($item.val() != '') {
                        $item.parent().find('.select2-container').addClass('select2-allowclear');
                    } else {
                        $item.parent().find('.select2-container').removeClass('select2-allowclear');
                    }
                }
            );

            __orgAgenteEducacionalIndex.iniciarElementoDatePicker($("#dataCadastroInicial"));
            __orgAgenteEducacionalIndex.iniciarElementoDatePicker($("#dataCadastroFinal"));
            __orgAgenteEducacionalIndex.iniciarElementoDatePicker($("#dataAlteracaoInicial"));
            __orgAgenteEducacionalIndex.iniciarElementoDatePicker($("#dataAlteracaoFinal"));

            $("#agentesFiltroExecutar").click(function () {
                __orgAgenteEducacionalIndex.atualizarListagem();
            });

            $("#agentesFiltroLimpar").click(function () {
                $('#estadoAgente').select2('val', '').trigger('change');
                $('#cidadeAgente').select2('val', '').trigger('change');
                $('#origemCadastro').select2('val', '').trigger('change');
                $('#dataCadastroInicial').val('').trigger('change');
                $('#dataCadastroFinal').val('').trigger('change');
                $('#dataAlteracaoInicial').val('').trigger('change');
                $('#dataAlteracaoFinal').val('').trigger('change');
            });

        };

        this.atualizarListagem = function () {
            __orgAgenteEducacionalIndex.options.datatables.orgAgenteEducacional.api().ajax.reload(null, false);
        };

        this.setSteps = function () {
            var $dataTables = $('#dataTableOrgAgenteEducacional');
            var colNum = 0;

            __orgAgenteEducacionalIndex.options.datatables.orgAgenteEducacional = $dataTables.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __orgAgenteEducacionalIndex.options.url.search,
                    data: function (x) {
                        x.filter = x.filter || {};
                        x.index = true;

                        var cidadeAgente = $('#cidadeAgente').val() || "";
                        var estadoAgente = $('#estadoAgente').val() || "";
                        var origemCadastro = $('#origemCadastro').val() || "";
                        var dataCadastroInicial = $('#dataCadastroInicial').val() || "";
                        var dataCadastroFinal = $('#dataCadastroFinal').val() || "";
                        var dataAlteracaoInicial = $('#dataAlteracaoInicial').val() || "";
                        var dataAlteracaoFinal = $('#dataAlteracaoFinal').val() || "";

                        if (cidadeAgente) {
                            x.filter['cidadeAgente'] = cidadeAgente;
                        }
                        if (estadoAgente) {
                            x.filter['estadoAgente'] = estadoAgente;
                        }
                        if (origemCadastro) {
                            x.filter['origemCadastro'] = origemCadastro;
                        }
                        if (dataCadastroInicial) {
                            x.filter['dataCadastroInicial'] = dataCadastroInicial;
                        }
                        if (dataCadastroFinal) {
                            x.filter['dataCadastroFinal'] = dataCadastroFinal;
                        }
                        if (dataAlteracaoInicial) {
                            x.filter['dataAlteracaoInicial'] = dataAlteracaoInicial;
                        }
                        if (dataAlteracaoFinal) {
                            x.filter['dataAlteracaoFinal'] = dataAlteracaoFinal;
                        }
                        return x;
                    },

                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;

                        var btnGroup = [
                            {
                                'class': 'btn btn-xs btn-primary orgAgenteEducacional-edit',
                                icon: 'fa fa-pencil fa-inverse'
                            },
                            {
                                'class': 'btn btn-xs btn-success orgAgenteEducacional-view',
                                icon: 'fa fa-eye fa-inverse'
                            },
                            {
                                'class': 'btn btn-xs btn-warning orgAgenteEducacional-contact',
                                icon: 'fa fa-send fa-inverse'
                            }
                        ];


                        for (var row in data) {
                            data[row]['agente_data_cadastro_formatado'] = __orgAgenteEducacionalIndex.formatDate(
                                data[row]['agente_data_cadastro']
                            );
                            data[row]['acao'] = __orgAgenteEducacionalIndex.createBtnGroup(btnGroup);
                        }

                        __orgAgenteEducacionalIndex.removeOverlay($('.widget-body'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "pes_id", targets: colNum++, data: "pes_id"},
                    {name: "pes_nome", targets: colNum++, data: "pes_nome"},
                    {name: "pes_cpf_cnpj", targets: colNum++, data: "pes_cpf_cnpj"},
                    {name: "con_contato_celular", targets: colNum++, data: "con_contato_celular"},
                    {name: "con_contato_email", targets: colNum++, data: "con_contato_email"},
                    {
                        name: "comissaoPrimeiroNivelFormatado", targets: colNum++,
                        data: "comissaoPrimeiroNivelFormatado"
                    },
                    {
                        name: "comissaoDemaisNiveisFormatada", targets: colNum++,
                        data: "comissaoDemaisNiveisFormatada"
                    },
                    {name: "agente_data_cadastro", targets: colNum++, data: "agente_data_cadastro_formatado"},
                    {name: "agente_situacao", targets: colNum++, data: "agente_situacao"},
                    {name: "pes_id", targets: colNum++, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'desc']]
            });

            $dataTables.on('click', '.orgAgenteEducacional-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __orgAgenteEducacionalIndex.options.datatables.orgAgenteEducacional.fnGetData($pai);
                location.href = __orgAgenteEducacionalIndex.options.url.edit + '/' + data['pes_id'];
            });

            $dataTables.on('click', '.orgAgenteEducacional-view', function (event) {
                var $pai = $(this).closest('tr');
                var data = __orgAgenteEducacionalIndex.options.datatables.orgAgenteEducacional.fnGetData($pai);
                __orgAgenteEducacionalIndex.showModal(data);
            });

            $dataTables.on('click', '.orgAgenteEducacional-contact', function (event) {
                var $pai = $(this).closest('tr');
                var data = __orgAgenteEducacionalIndex.options.datatables.orgAgenteEducacional.fnGetData($pai);
                __orgAgenteEducacionalIndex.agenteInfo(data);
                $('#enviar-comunicacao-modal').modal('show');
                $('#enviar-comunicacao-modal').find('[name="modeloId"]').select2('val', '');
            });

            $dataTables.on('click', '.orgAgenteEducacional-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __orgAgenteEducacionalIndex.options.datatables.orgAgenteEducacional.fnGetData($pai);
                var arrRemover = {
                    pesId: data['pes_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de agente educacional "' + data['pes_id_indicacao'] +
                    '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        __orgAgenteEducacionalIndex.addOverlay($('.widget-body'),
                            'Aguarde, solicitando remoção de registro de agente educacional...');
                        $.ajax({
                            url: __orgAgenteEducacionalIndex.options.url.remove,
                            type: 'POST',
                            dataType: 'json',
                            data: arrRemover,
                            success: function (data) {
                                if (data.erro) {
                                    __orgAgenteEducacionalIndex.showNotificacaoDanger(
                                        "Não foi possível remover registro de agente educacional:\n" +
                                        "<i>" + data['erroDescricao'] + "</i>"
                                    );
                                    __orgAgenteEducacionalIndex.removeOverlay($('.widget-body'));
                                } else {
                                    __orgAgenteEducacionalIndex.options.datatables.orgAgenteEducacional.api().ajax.reload(null, false);
                                    __orgAgenteEducacionalIndex.showNotificacaoSuccess(
                                        "Registro de agente educacional removido!"
                                    );
                                }
                            }
                        });
                    }
                });

                e.preventDefault();
            });

            this.comunicacao = {
                iniciarFuncionalidades: function () {
                    var $modal = $('.modal-overlay');
                    var $form = $('#form-enviar-comunicacao');
                    var $modeloComunicacao = $form.find('[name="modeloId"]');

                    $modeloComunicacao.select2({
                        language: 'pt-BR',
                        allowClear: true,
                        ajax: {
                            url: __orgAgenteEducacionalIndex.options.url.comunicacaoModelo,
                            dataType: 'json',
                            type: 'POST',
                            delay: 250,
                            data: function (query) {
                                return {
                                    tipoContexto: ['Pessoa', 'Agente Educacional'],
                                    query: query
                                };
                            },
                            results: function (data) {
                                var transformed = $.map(data, function (el) {
                                    return {text: el.modelo_nome, id: el.modelo_id};
                                });

                                return {results: transformed};
                            }
                        }
                    });

                    $form.on('submit', function (e) {
                        e.preventDefault();
                        e.stopPropagation();
                    }).validate({
                        ignore: '.ignore',
                        submitHandler: function () {
                            __orgAgenteEducacionalIndex.addOverlay($modal, 'Aguarde... ');

                            var arrDados = __orgAgenteEducacionalIndex.agenteInfo();
                            var dados = $form.serializeJSON();

                            dados['pesId'] = arrDados['pes_id'] || '';

                            $.ajax({
                                url: $form.attr('action'),
                                type: 'POST',
                                dataType: 'json',
                                data: dados,
                                success: function (data) {
                                    if (!data.erro) {
                                        __orgAgenteEducacionalIndex.showNotificacaoSuccess(
                                            data.message || 'Registrado na fila de envio com Sucesso!'
                                        );
                                    } else {
                                        __orgAgenteEducacionalIndex.showNotificacaoDanger(
                                            data.message || 'Não foi possível registrar na fila de envio'
                                        );
                                    }

                                    __orgAgenteEducacionalIndex.removeOverlay($modal);
                                    $('#enviar-comunicacao-modal').modal('hide');
                                }
                            });
                        },
                        rules: {
                            modeloId: {required: true}
                        },
                        messages: {
                            modeloId: {required: 'Campo Obrigatório'}
                        }
                    });
                }
            };

            this.showModal = function (data) {
                var $el = $('.modal-content');

                __orgAgenteEducacionalIndex.addOverlay($el, 'Aguarde...');

                $.ajax({
                    url: __orgAgenteEducacionalIndex.options.url.pesquisaDadosPessoa,
                    data: {pesId: data['pes_id']},
                    dataType: 'json',
                    type: 'POST',
                    success: function (dados) {
                        $('#agenteInfo').html(dados.result['pesId']);
                        __orgAgenteEducacionalIndex.setFormStaticValues(dados.result);
                        __orgAgenteEducacionalIndex.removeOverlay($el);
                    }
                });

                $('#modal-visualizar-agente').modal('show');
            }
        };

        this.setFormStaticValues = function (data) {
            $.map(data, function (el, index) {
                $('.form-static-agente').find('p').each(function () {
                    $('.' + index).html(el);
                });
            });
        };

        this.agenteInfo = function (data) {
            __orgAgenteEducacionalIndex.options.value.agenteInfo =
                data || __orgAgenteEducacionalIndex.options.value.agenteInfo;
            return __orgAgenteEducacionalIndex.options.value.agenteInfo;
        };

        this.run = function (opts) {
            __orgAgenteEducacionalIndex.setDefaults(opts);
            __orgAgenteEducacionalIndex.setSteps();
            __orgAgenteEducacionalIndex.iniciarFiltros();
            __orgAgenteEducacionalIndex.comunicacao.iniciarFuncionalidades();

        };
    };

    $.orgAgenteEducacionalIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.organizacao.org-agente-educacional.index");

        if (!obj) {
            obj = new OrgAgenteEducacionalIndex();
            obj.run(params);
            $(window).data('universa.organizacao.org-agente-educacional.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);