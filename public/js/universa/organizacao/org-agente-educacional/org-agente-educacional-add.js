(function ($, window, document) {
    'use strict';

    var OrgAgenteEducacionalAdd = function () {
        VersaShared.call(this);
        var __orgAgenteEducacionalAdd = this;
        this.defaults = {
            url: {
                pessoa: '',
                orgAgenteEducacional: '',
                acadgeralCadastroOrigem: '',
                boletoBanco: ''
            },
            data: {
                arrEstados: [],
            },
            value: {
                pesIdAgente: null,
                pesIdIndicacao: null,
                origem: null,
                banc: null
            },
            datatables: {},
            wizardElement: '#org-agente-educacional-wizard',
            formElement: '#org-agente-educacional-form',
            validator: null
        };

        __orgAgenteEducacionalAdd.iniciarCampos = function () {
            var $pessoaForm = $('#pessoa-form');
            var $form = $(__orgAgenteEducacionalAdd.options.formElement);

            $("#pesIdAgente").select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __orgAgenteEducacionalAdd.options.url.pessoa,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {
                            query: query,
                            pesquisaPFPJ: true
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.pes_id;
                            el.text = el.pes_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                },
                createSearchChoice: function (term) {
                    return {
                        id: term,
                        pesId: '',
                        pesNome: term,
                        text: term + ' (Criar novo registro)'
                    };
                }
            });

            $('#pesIdAgente').change(function (e) {
                var data = $(this).select2('data') || [];

                if (Object.keys(data).length > 0) {
                    $(this).next('.editarPessoa').removeClass('hidden');

                    if (!$.isNumeric(data['id']) && data['pesNome']) {
                        $(this).next('.editarPessoa').click();
                    }
                } else {
                    $(this).next('.editarPessoa').addClass('hidden');
                }
            }).trigger('change');

            $('.editarPessoa').click(function (e) {
                var $elemento = $(this).parent().find('>input');
                var data = $elemento.select2('data') || [];
                $('#pessoa-editar').modal('show');

                var pessoaAdd = $.pessoaAdd();
                pessoaAdd.setCallback('aposSalvar', function (dados, retorno) {
                    var text = [];

                    if (dados['pesCnpj']) {
                        text.push(dados['pesCnpj'])
                    }
                    if (dados['pesCpf']) {
                        text.push(dados['pesCpf'])
                    }
                    if (dados['pesNome']) {
                        text.push(dados['pesNome'])
                    }

                    dados['id'] = dados['pesId'];
                    dados['text'] = text.join(' - ');

                    $elemento.select2('data', dados);

                    $('#pessoa-editar').modal('hide');
                });

                pessoaAdd.buscarDadosPessoa($elemento.val());

                $pessoaForm.find('input[name=pesNome]').val(data['pesNome'] || '');

                e.preventDefault();
            });

            $("#pesIdIndicacao").select2({
                language: 'pt-BR',
                minimumInputLength: 1,
                ajax: {
                    url: __orgAgenteEducacionalAdd.options.url.orgAgenteEducacional,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query, ignorarPesId: $('#pesId').val()};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.pes_nome, id: el.pes_id};
                        });

                        return {results: transformed};
                    }
                }
            });

            $("#origem").select2({
                language: 'pt-BR',
                ajax: {
                    url: __orgAgenteEducacionalAdd.options.url.acadgeralCadastroOrigem,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.origem_nome, id: el.origem_id};
                        });

                        return {results: transformed};
                    }
                }
            });

            $("#banc").select2({
                language: 'pt-BR',
                minimumInputLength: 1,
                ajax: {
                    url: __orgAgenteEducacionalAdd.options.url.boletoBanco,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.banc_nome, id: el.banc_id};
                        });

                        return {results: transformed};
                    }
                }
            });

            $('#pesIdAgente').select2("data", __orgAgenteEducacionalAdd.getPesIdAgente()).trigger('change');
            $('#pesIdIndicacao').select2("data", __orgAgenteEducacionalAdd.getPesIdIndicacao());
            $('#origem').select2("data", __orgAgenteEducacionalAdd.getOrigem());
            $('#banc').select2("data", __orgAgenteEducacionalAdd.getBanc());

            $("#agenteContaTipo, #agenteSituacao").select2({
                allowClear: true, language: 'pt-BR'
            }).trigger("change");

            $('#agenteMaxNivel').inputmask({
                showMaskOnHover: false, mask: ['9{0,9}'], rightAlign: true
            });

            $(" #agenteComissaoPrimeiroNivel,#agenteComissaoDemaisNiveis").inputmask({
                    showMaskOnHover: false,
                    alias: 'currency',
                    groupSeparator: ".",
                    radixPoint: ",",
                    placeholder: "0",
                    prefix: ""
                }
            )
            ;

            $("#agenteContaAgencia, #agenteContaNumero").inputmask({
                showMaskOnHover: false, mask: ['9{3,8}[[-]*{0,3}]'], rightAlign: true
            });
        };


        __orgAgenteEducacionalAdd.setValidations = function () {
            var $form = $(__orgAgenteEducacionalAdd.options.formElement);
            __orgAgenteEducacionalAdd.options.validator.settings.rules = {
                pesIdAgente: {required: true, maxlength: 11, number: true},
                pesIdIndicacao: {maxlength: 11, number: true},
                origem: {maxlength: 10, number: true},
                agenteMaxNivel: {maxlength: 1, number: true, required: true},
                agenteComissaoPrimeiroNivel: {maxlength: 6},
                agenteComissaoDemaisNiveis: {maxlength: 6},
                banc: {number: true},
                agenteSituacao: {maxlength: 7, required: true}
            };
            __orgAgenteEducacionalAdd.options.validator.settings.messages = {
                pesIdAgente: {
                    required: 'Campo obrigatório!', maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!'
                },
                pesIdIndicacao: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!'},
                origem: {maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!'},
                agenteMaxNivel: {
                    maxlength: 'Tamanho máximo: 1!', number: 'Número inválido!', required: 'Campo obrigatório!'
                },
                agenteComissaoPrimeiroNivel: {maxlength: 'Porcentagem máxima 100 %!'},
                agenteComissaoDemaisNiveis: {maxlength: 'Porcentagem máxima 100 %!'},
                agenteSituacao: {maxlength: 'Tamanho máximo: 7!', required: 'Campo obrigatório!'}
            };
        };

        this.setPesIdAgente = function (pesIdAgente) {
            this.options.value.pesIdAgente = pesIdAgente || null;
        };

        this.getPesIdAgente = function () {
            return this.options.value.pesIdAgente || null;
        };

        this.setPesIdIndicacao = function (pesIdIndicacao) {
            this.options.value.pesIdIndicacao = pesIdIndicacao || null;
        };

        this.getPesIdIndicacao = function () {
            return this.options.value.pesIdIndicacao || null;
        };

        this.setOrigem = function (origem) {
            this.options.value.origem = origem || null;
        };

        this.getOrigem = function () {
            return this.options.value.origem || null;
        };

        this.setBanc = function (banc) {
            this.options.value.banc = banc || null;
        };

        this.getBanc = function () {
            return this.options.value.banc || null;
        };

        this.setPessoaNecessidades = function (pessoaNecessidades) {
            this.options.value.pessoaNecessidades = pessoaNecessidades || null;
        };

        this.getPessoaNecessidades = function () {
            return this.options.value.pessoaNecessidades || null;
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.iniciarCampos();
            this.setValidations();
            this.wizard();
        };
    };

    $.orgAgenteEducacionalAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.organizacao.org-agente-educacional.add");

        if (!obj) {
            obj = new OrgAgenteEducacionalAdd();
            obj.run(params);
            $(window).data('universa.organizacao.org-agente-educacional.add', obj);
        }

        return obj;
    };
})
(window.jQuery, window, document);