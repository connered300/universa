(function ($, window, document) {
    'use strict';
    var AcadperiodoMatrizCurricularIndex = function () {
        VersaShared.call(this);
        var __acadperiodoMatrizCurricularIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                acadperiodoMatrizCurricular: null
            }
        };

        this.setSteps = function () {
            var colNum = 0;
            __acadperiodoMatrizCurricularIndex.options.datatables.acadperiodoMatrizCurricular = $('#dataTableAcadperiodoMatrizCurricular').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __acadperiodoMatrizCurricularIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [];

                        for (var row in data) {
                            var linkClonar = '/organizacao/acadperiodo-matriz-curricular/clonar/' + data[row]['mat_cur_id'],
                                linkEditar = '/organizacao/acadperiodo-matriz-curricular/edit/' + data[row]['mat_cur_id'],
                                linkPdf = '/matricula/relatorios/matriz/' + data[row]['mat_cur_id'];

                            btns = [
                                {
                                    element: 'a',
                                    href: linkEditar,
                                    title: "Editar Matriz Curricular",
                                    class: 'btn-primary acadperiodoMatrizCurricular-edit',
                                    icon: 'fa-pencil'
                                },
                                {
                                    element: 'a',
                                    href: linkClonar,
                                    title: "Clonar Matriz Curricular",
                                    class: 'btn-warning acadperiodoMatrizCurricular-clonar',
                                    icon: 'fas fa-copy'

                                },
                                {
                                    element: 'a',
                                    href: linkPdf,
                                    title: "Emitir PDF da Matriz Curricular",
                                    class: 'btn-info acadperiodoMatrizCurricular-pdf',
                                    icon: 'fa fa-file',
                                    target: "_blank"
                                }
                            ];

                            data[row]['btns'] = __acadperiodoMatrizCurricularIndex.createBtnGroup(btns);
                        }

                        return data;
                    }
                },
                columnDefs: [
                    {name: "mat_cur_id", targets: colNum++, data: "mat_cur_id"},
                    {name: "mat_cur_descricao", targets: colNum++, data: "mat_cur_descricao"},
                    {name: "mat_cur_semestre", targets: colNum++, data: "mat_cur_semestre"},
                    {name: "mat_cur_ano", targets: colNum++, data: "mat_cur_ano"},
                    {name: "mat_cur_id", targets: colNum++, data: "btns", search: false, orderable: false}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[3, 'desc'], [2, 'asc']]
            });

            $("#btn-adicionar").on("click", function () {
                location.href = '/organizacao/acadperiodo-matriz-curricular/add'
            });

            $(".acadperiodoMatrizCurricular-clonar").on("click", function () {
                __acadperiodoMatrizCurricularIndex.addOverlay($("#container-acadperiodo-matriz-curricular"), "Aguarde! Carregando dados para clonar...");
            });

            $(document).on("click", ".acadperiodoMatrizCurricular-edit", function () {
                __acadperiodoMatrizCurricularIndex.addOverlay($("#container-acadperiodo-matriz-curricular"), "Aguarde! Carregando dados para edição...");
            });
        };

        this.reloadDataTableAcadperiodoMatrizCurricular = function () {
            this.getDataTableAcadperiodoMatrizCurricular().api().ajax.reload(null, false);
        };

        this.getDataTableAcadperiodoMatrizCurricular = function () {
            if (!__acadperiodoMatrizCurricularIndex.options.datatables.acadperiodoMatrizCurricular) {
                if (!$.fn.dataTable.isDataTable('#dataTableAcadperiodoMatrizCurricular')) {
                    __acadperiodoMatrizCurricularIndex.options.datatables.acadperiodoMatrizCurricular = $('#dataTableAcadperiodoMatrizCurricular').DataTable();
                } else {
                    __acadperiodoMatrizCurricularIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __acadperiodoMatrizCurricularIndex.options.datatables.acadperiodoMatrizCurricular;
        };

        this.run = function (opts) {
            __acadperiodoMatrizCurricularIndex.setDefaults(opts);
            __acadperiodoMatrizCurricularIndex.setSteps();
        };
    };

    $.acadperiodoMatrizCurricularIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.organizacao.acadperiodo-matriz-curricular.index");

        if (!obj) {
            obj = new AcadperiodoMatrizCurricularIndex();
            obj.run(params);
            $(window).data('universa.organizacao.acadperiodo-matriz-curricular.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);