(function ($, window, document) {
    'use strict';
    var OrgComunicacaoTipoIndex = function () {
        VersaShared.call(this);
        var __orgComunicacaoTipoIndex = this;
        this.defaults                 = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                orgComunicacaoTipo: null
            },
        };

        this.setSteps = function () {
            var colNum                                                      = 0;
            __orgComunicacaoTipoIndex.options.datatables.orgComunicacaoTipo =
                $('#dataTableOrgComunicacaoTipo').dataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: __orgComunicacaoTipoIndex.options.url.search,
                        type: "POST",
                        dataSrc: function (json) {
                            var data     = json.data;
                            var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary orgComunicacaoTipo-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger orgComunicacaoTipo-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                            for (var row in data) {
                                data[row]['acao'] = btnGroup;
                            }

                            __orgComunicacaoTipoIndex.removeOverlay($('#container-org-comunicacao-tipo'));

                            return data;
                        }
                    },
                    columnDefs: [
                        {name: "tipo_id", targets: colNum++, data: "tipo_id"},
                        {name: "tipo_descricao", targets: colNum++, data: "tipo_descricao"},
                        {name: "tipo_contexto", targets: colNum++, data: "tipo_contexto"},
                        {name: "tipo_id", targets: colNum++, data: "acao"}
                    ],
                    "dom": "fr" +
                           "t" +
                           "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                    "autoWidth": true,
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "Procurar: ",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    },
                    order: [[0, 'asc']]
                });

            $('#dataTableOrgComunicacaoTipo').on('click', '.orgComunicacaoTipo-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __orgComunicacaoTipoIndex.options.datatables.orgComunicacaoTipo.fnGetData($pai);

                if (!__orgComunicacaoTipoIndex.options.ajax) {
                    location.href = __orgComunicacaoTipoIndex.options.url.edit + '/' + data['tipo_id'];
                } else {
                    $.orgComunicacaoTipoAdd().pesquisaOrgComunicacaoTipo(
                        data['tipo_id'],
                        function (data) {
                            __orgComunicacaoTipoIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-org-comunicacao-tipo-add').click(function (e) {
                if (__orgComunicacaoTipoIndex.options.ajax) {
                    __orgComunicacaoTipoIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#dataTableOrgComunicacaoTipo').on('click', '.orgComunicacaoTipo-remove', function (e) {
                var $pai       = $(this).closest('tr');
                var data       = __orgComunicacaoTipoIndex.options.datatables.orgComunicacaoTipo.fnGetData($pai);
                var arrRemover = {
                    tipoId: data['tipo_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de tipo de comunicação "' + data['tipo_descricao'] +
                             '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __orgComunicacaoTipoIndex.addOverlay(
                                $('#container-org-comunicacao-tipo'),
                                'Aguarde, solicitando remoção de registro de tipo de comunicação...'
                            );
                            $.ajax({
                                url: __orgComunicacaoTipoIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __orgComunicacaoTipoIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de tipo de comunicação:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __orgComunicacaoTipoIndex.removeOverlay($('#container-org-comunicacao-tipo'));
                                    } else {
                                        __orgComunicacaoTipoIndex.reloadDataTableOrgComunicacaoTipo();
                                        __orgComunicacaoTipoIndex.showNotificacaoSuccess(
                                            "Registro de tipo de comunicação removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data            = data || {};
            var actionTitle = 'Cadastrar';
            var $form       = $('#org-comunicacao-tipo-form');
            var actionForm  = $form.attr('action');
            actionForm      = actionForm.replace(/\/[0-9]+/g, '');
            actionForm      = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle                       = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['tipoId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['tipoId']                    = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);

            $('#tipoContexto').select2('val', data['tipoContexto'] || null);


            $('#org-comunicacao-tipo-modal .modal-title').html(actionTitle + ' tipo de comunicação');
            $('#org-comunicacao-tipo-modal').modal();
        };

        this.reloadDataTableOrgComunicacaoTipo = function () {
            this.getDataTableOrgComunicacaoTipo().api().ajax.reload(null, false);
        };

        this.getDataTableOrgComunicacaoTipo = function () {
            if (!__orgComunicacaoTipoIndex.options.datatables.orgComunicacaoTipo) {
                if (!$.fn.dataTable.isDataTable('#dataTableOrgComunicacaoTipo')) {
                    __orgComunicacaoTipoIndex.options.datatables.orgComunicacaoTipo =
                        $('#dataTableOrgComunicacaoTipo').DataTable();
                } else {
                    __orgComunicacaoTipoIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __orgComunicacaoTipoIndex.options.datatables.orgComunicacaoTipo;
        };

        this.run = function (opts) {
            __orgComunicacaoTipoIndex.setDefaults(opts);
            __orgComunicacaoTipoIndex.setSteps();
        };
    };

    $.orgComunicacaoTipoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.organizacao.org-comunicacao-tipo.index");

        if (!obj) {
            obj = new OrgComunicacaoTipoIndex();
            obj.run(params);
            $(window).data('universa.organizacao.org-comunicacao-tipo.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);