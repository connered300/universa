(function ($, window, document) {
    'use strict';

    var OrgComunicacaoTipoAdd = function () {
        VersaShared.call(this);
        var __orgComunicacaoTipoAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {},
            data: {},
            value: {},
            datatables: {},
            wizardElement: '#org-comunicacao-tipo-wizard',
            formElement: '#org-comunicacao-tipo-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $('#tipoContexto').select2({language: 'pt-BR'});
        };

        this.setValidations = function () {
            __orgComunicacaoTipoAdd.options.validator.settings.rules = {
                tipoId: {maxlength: 10},
                tipoContexto: {required: true},
                tipoDescricao: {maxlength: 45},
            };
            __orgComunicacaoTipoAdd.options.validator.settings.messages = {
                tipoId: {maxlength: 'Tamanho máximo: 10!'},
                tipoContexto: {required: 'Campo Obrigatório!'},
                tipoDescricao: {maxlength: 'Tamanho máximo: 45!'},
            };

            $(__orgComunicacaoTipoAdd.options.formElement).submit(function (e) {
                var ok = $(__orgComunicacaoTipoAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__orgComunicacaoTipoAdd.options.formElement);

                if (__orgComunicacaoTipoAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __orgComunicacaoTipoAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __orgComunicacaoTipoAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__orgComunicacaoTipoAdd.options.listagem) {
                                    $.orgComunicacaoTipoIndex().reloadDataTableOrgComunicacaoTipo();
                                    __orgComunicacaoTipoAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#org-comunicacao-tipo-modal').modal('hide');
                                }

                                __orgComunicacaoTipoAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaOrgComunicacaoTipo = function (tipo_id, callback) {
            var $form = $(__orgComunicacaoTipoAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __orgComunicacaoTipoAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + tipo_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    __orgComunicacaoTipoAdd.removeOverlay($form);

                    if (Object.keys(data).length == 0) {
                        __orgComunicacaoTipoAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }
                },
                erro: function () {
                    __orgComunicacaoTipoAdd.showNotificacaoDanger('Erro desconhecido!');
                    __orgComunicacaoTipoAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.orgComunicacaoTipoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.organizacao.org-comunicacao-tipo.add");

        if (!obj) {
            obj = new OrgComunicacaoTipoAdd();
            obj.run(params);
            $(window).data('universa.organizacao.org-comunicacao-tipo.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);