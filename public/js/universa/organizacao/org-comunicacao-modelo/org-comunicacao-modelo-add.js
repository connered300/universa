(function ($, window, document) {
    'use strict';

    var OrgComunicacaoModeloAdd = function () {
        VersaShared.call(this);
        var __orgComunicacaoModeloAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                orgComunicacaoTipo: ''
            },
            data: {},
            value: {
                tipo: null,
                grupo: null
            },
            datatables: {},
            wizardElement: '#org-comunicacao-modelo-wizard',
            formElement: '#org-comunicacao-modelo-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#tipo").select2({
                language: 'pt-BR',
                ajax: {
                    url: __orgComunicacaoModeloAdd.options.url.orgComunicacaoTipo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.tipo_descricao, id: el.tipo_id};
                        });

                        return {results: transformed};
                    }
                }
            });

            if (__orgComunicacaoModeloAdd.getTipo()) {
                $('#tipo').select2("data", __orgComunicacaoModeloAdd.getTipo());
            }

            $("#modeloTipo")
                .select2({language: 'pt-BR'})
                .on('change',
                    function () {
                        var tipo = $(this).val();

                        if (tipo == 'SMS') {
                            if (CKEDITOR.instances['modeloConteudo']) {
                                CKEDITOR.instances['modeloConteudo'].destroy();
                            }
                        } else {
                            $('#modeloConteudo').ckeditor({
                                height: '200px', linkShowAdvancedTab: false,
                                autoStartup: true,
                                extraAllowedContent: (
                                    'style;body;*{*};' +
                                    'img[src,alt,width,height,style,class,id];' +
                                    'iframe[src,alt,width,height,style,class,id];' +
                                    'td[class,colspan,width,height,style];' +
                                    'th[class,colspan,width,height,style];' +
                                    'tr[class,style];' +
                                    'h1[class,width,height,style,id];' +
                                    'h2[class,width,height,style,id];' +
                                    'h3[class,width,height,style,id];' +
                                    'div[class,width,height,style,id];' +
                                    'span[class,width,height,style,id];' +
                                    'p[class,width,height,style,id];' +
                                    'table[src,alt,width,height,style,id,class]'
                                ),
                                enterMode: Number(2),
                                toolbar: [
                                    [
                                        'Styles', 'Bold', 'Italic', 'Underline', 'SpellChecker', 'Scayt', '-',
                                        'NumberedList', 'BulletedList'
                                    ],
                                    ['Table'],
                                    ['Link', 'Unlink'],
                                    ['Undo', 'Redo', '-', 'SelectAll'],
                                    ['Maximize', 'Source']
                                ]
                            });
                        }
                    })
                .trigger("change");
            $("#modeloDataInicio")
                .datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                })
                .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});
            $("#modeloDataFim")
                .datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                })
                .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});

            $("#grupoId").select2({
                language: 'pt-BR',
                allowClear: true,
                multiple: true,
                ajax: {
                    url: __orgComunicacaoModeloAdd.options.url.acessoGrupo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.id;
                            el.text = el.grup_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $('#grupoId').select2("data", __orgComunicacaoModeloAdd.getGrupo());
        };

        this.getGrupo = function () {
            return this.options.value.grupo || null;
        };

        this.setTipo = function (tipo) {
            this.options.value.tipo = tipo || null;
        };

        this.getTipo = function () {
            return this.options.value.tipo || null;
        };

        this.setValidations = function () {
            __orgComunicacaoModeloAdd.options.validator.settings.rules = {
                modeloId: {maxlength: 10},
                tipo: {maxlength: 10, number: true, required: true},
                usuario: {maxlength: 11, number: true, required: true},
                grupoId: {required: true},
                modeloNome: {maxlength: 45, required: true},
                modeloTipo: {maxlength: 3},
                modeloAssunto: {maxlength: 250, required: true},
                modeloConteudo: {required: true, maxlength: 160},
                modeloDataInicio: {required: true, dateBR: true},
                modeloDataFim: {required: true, dateBR: true}
            };

            if ($('#modeloTipo').val() != 'SMS') {
                delete  __orgComunicacaoModeloAdd
                    .options
                    .validator
                    .settings
                    .rules
                    .modeloConteudo['maxlength'];
            }

            __orgComunicacaoModeloAdd.options.validator.settings.messages = {
                modeloId: {maxlength: 'Tamanho máximo: 10!'},
                tipo: {maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!', required: 'Campo obrigatório!'},
                usuario: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!', required: 'Campo obrigatório!'},
                modeloNome: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                grupoId: {required: "Campo Obrigatório!"},
                modeloTipo: {maxlength: 'Tamanho máximo: 3!'},
                modeloAssunto: {maxlength: 'Tamanho máximo: 250!', required: 'Campo obrigatório!'},
                modeloConteudo: {required: 'Campo obrigatório!'},
                modeloDataInicio: {required: 'Campo obrigatório!', dateBR: 'Informe uma data válida!'},
                modeloDataFim: {required: 'Campo obrigatório!', dateBR: 'Informe uma data válida!'},
            };

            $(__orgComunicacaoModeloAdd.options.formElement).submit(function (e) {
                var ok = $(__orgComunicacaoModeloAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__orgComunicacaoModeloAdd.options.formElement);

                if (__orgComunicacaoModeloAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __orgComunicacaoModeloAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __orgComunicacaoModeloAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__orgComunicacaoModeloAdd.options.listagem) {
                                    $.orgComunicacaoModeloIndex().reloadDataTableOrgComunicacaoModelo();
                                    __orgComunicacaoModeloAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#org-comunicacao-modelo-modal').modal('hide');
                                }

                                __orgComunicacaoModeloAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaOrgComunicacaoModelo = function (modelo_id, callback) {
            var $form = $(__orgComunicacaoModeloAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __orgComunicacaoModeloAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + modelo_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    __orgComunicacaoModeloAdd.removeOverlay($form);

                    if (Object.keys(data).length == 0) {
                        __orgComunicacaoModeloAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }
                },
                erro: function () {
                    __orgComunicacaoModeloAdd.showNotificacaoDanger('Erro desconhecido!');
                    __orgComunicacaoModeloAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.orgComunicacaoModeloAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.organizacao.org-comunicacao-modelo.add");

        if (!obj) {
            obj = new OrgComunicacaoModeloAdd();
            obj.run(params);
            $(window).data('universa.organizacao.org-comunicacao-modelo.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);