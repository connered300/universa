(function ($, window, document) {
    'use strict';
    var OrgComunicacaoModeloIndex = function () {
        VersaShared.call(this);
        var __orgComunicacaoModeloIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            value: {
                arrDados: null
            },
            datatables: {
                orgComunicacaoModelo: null
            },
        };

        this.setSteps = function () {
            var colNum = 0;
            __orgComunicacaoModeloIndex.options.datatables.orgComunicacaoModelo =
                $('#dataTableOrgComunicacaoModelo').dataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: __orgComunicacaoModeloIndex.options.url.search,
                        type: "POST",
                        dataSrc: function (json) {
                            var data = json.data;
                            var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary orgComunicacaoModelo-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger orgComunicacaoModelo-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                            for (var row in data) {
                                data[row]['acao'] = btnGroup;
                            }

                            __orgComunicacaoModeloIndex.removeOverlay($('#container-org-comunicacao-modelo'));

                            return data;
                        }
                    },
                    columnDefs: [
                        {name: "modelo_id", targets: colNum++, data: "modelo_id"},
                        {name: "tipo_descricao", targets: colNum++, data: "tipo_descricao"},
                        {name: "modelo_nome", targets: colNum++, data: "modelo_nome"},
                        {name: "modelo_tipo", targets: colNum++, data: "modelo_tipo"},
                        {name: "modelo_assunto", targets: colNum++, data: "modelo_assunto"},
                        {name: "modelo_data_inicio", targets: colNum++, data: "modelo_data_inicio"},
                        {name: "modelo_data_fim", targets: colNum++, data: "modelo_data_fim"},
                        {name: "modelo_id", targets: colNum++, data: "acao"}
                    ],
                    "dom": "fr" +
                           "t" +
                           "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                    "autoWidth": true,
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "Procurar: ",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    },
                    order: [[0, 'asc']]
                });

            $('#dataTableOrgComunicacaoModelo').on('click', '.orgComunicacaoModelo-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __orgComunicacaoModeloIndex.options.datatables.orgComunicacaoModelo.fnGetData($pai);

                if (!__orgComunicacaoModeloIndex.options.ajax) {
                    location.href = __orgComunicacaoModeloIndex.options.url.edit + '/' + data['modelo_id'];
                } else {
                    $.orgComunicacaoModeloAdd().pesquisaOrgComunicacaoModelo(
                        data['modelo_id'],
                        function (data) { __orgComunicacaoModeloIndex.showModal(data); }
                    );
                }
            });


            $('#btn-org-comunicacao-modelo-add').click(function (e) {
                if (__orgComunicacaoModeloIndex.options.ajax) {
                    __orgComunicacaoModeloIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#dataTableOrgComunicacaoModelo').on('click', '.orgComunicacaoModelo-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __orgComunicacaoModeloIndex.options.datatables.orgComunicacaoModelo.fnGetData($pai);
                var arrRemover = {
                    modeloId: data['modelo_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de comunicação modelo "' + data['modelo_nome'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __orgComunicacaoModeloIndex.addOverlay(
                                $('#container-org-comunicacao-modelo'),
                                'Aguarde, solicitando remoção de registro de comunicação modelo...'
                            );
                            $.ajax({
                                url: __orgComunicacaoModeloIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __orgComunicacaoModeloIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de comunicação modelo:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __orgComunicacaoModeloIndex.removeOverlay($('#container-org-comunicacao-modelo'));
                                    } else {
                                        __orgComunicacaoModeloIndex.reloadDataTableOrgComunicacaoModelo();
                                        __orgComunicacaoModeloIndex.showNotificacaoSuccess(
                                            "Registro de comunicação modelo removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#org-comunicacao-modelo-form');
            var actionForm = $form.attr('action');
            var $modal = $('#org-comunicacao-modelo-modal');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['modeloId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['modeloId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);
            
            $form[0].reset();
            $form.deserialize(data);

            $("#tipo").select2('data', data['tipo']).trigger("change");
            $("#usuario").trigger("change");
            $("#modeloTipo").trigger("change");
            $("#grupoId").select2('data', data['arrGrupo']).trigger("change");
            $("#modeloConteudo").val(data['modeloConteudo']);
            $("#modeloConteudo").trigger("change");

            $('#org-comunicacao-modelo-modal .modal-title').html(actionTitle + ' comunicação modelo');
            $modal.modal();
        };

        this.reloadDataTableOrgComunicacaoModelo = function () {
            this.getDataTableOrgComunicacaoModelo().api().ajax.reload(null, false);
        };

        this.getDataTableOrgComunicacaoModelo = function () {
            if (!__orgComunicacaoModeloIndex.options.datatables.orgComunicacaoModelo) {
                if (!$.fn.dataTable.isDataTable('#dataTableOrgComunicacaoModelo')) {
                    __orgComunicacaoModeloIndex.options.datatables.orgComunicacaoModelo =
                        $('#dataTableOrgComunicacaoModelo').DataTable();
                } else {
                    __orgComunicacaoModeloIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __orgComunicacaoModeloIndex.options.datatables.orgComunicacaoModelo;
        };

        this.run = function (opts) {
            __orgComunicacaoModeloIndex.setDefaults(opts);
            __orgComunicacaoModeloIndex.setSteps();
        };
    };

    $.orgComunicacaoModeloIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.organizacao.org-comunicacao-modelo.index");

        if (!obj) {
            obj = new OrgComunicacaoModeloIndex();
            obj.run(params);
            $(window).data('universa.organizacao.org-comunicacao-modelo.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);