(function ($, window, document) {
    'use strict';

    var FinanceiroDescontoAdd = function () {
            VersaShared.call(this);
            var __financeiroDescontoAdd = this;
            this.defaults = {
                ajaxSubmit: 0,
                listagem: 0,
                url: {
                    financeiroDescontoTipo: '',
                    financeiroTitulo: '',
                    acessoPessoas: '',
                    acadgeralAluno: ''
                },
                data: {},
                value: {
                    desctipo: null,
                    aluno: null,
                    titulosVinculados: []
                },
                datatables: {
                    titulosVinculados: null,
                    titulosAbertosPessoa: null,
                },
                wizardElement: '#financeiro-desconto-wizard',
                formElement: '#financeiro-desconto-form',
                validator: null
            };

            this.steps.dadosBasicos = {
                init: function () {
                    $("#descontoStatus").on('change', function () {
                        if ($("#descontoStatus").val() == 'Inativo') {
                            $("#btn-financeiro-desconto-deferir").addClass("hidden");
                            __financeiroDescontoAdd.steps.titulosVinculados.validaCamposDescontos(true, true);

                        } else {
                            var titulosVinculados = __financeiroDescontoAdd.getTitulosVinculados();
                            $("#btn-add-descontoTitulo").removeClass("hidden");

                            if (titulosVinculados && titulosVinculados.length > 0) {

                                var len = titulosVinculados.length,
                                    i = 0;

                                for (i = 0; i < len; i++) {
                                    if (titulosVinculados[0].descontoTituloSituacao == 'Pendente' || titulosVinculados[0].descontoTituloSituacao == 'Indeferido') {
                                        $("#btn-financeiro-desconto-deferir").removeClass("hidden");
                                        break;
                                    }
                                }
                                __financeiroDescontoAdd.steps.titulosVinculados.validaCamposDescontos(false, true);
                            }
                        }
                        $("#btn-salvarDesconto").removeClass('hidden');
                    });

                    $('#btn-financeiro-desconto-deferir').click(function () {

                        var titulosVinculados = __financeiroDescontoAdd.getTitulosVinculados();
                        $.each(titulosVinculados, function (index, value) {

                            value.descontoTituloSituacao = 'Deferido';
                        });

                        __financeiroDescontoAdd.setTitulosVinculados(titulosVinculados);
                        $(__financeiroDescontoAdd.options.formElement).submit();

                    });

                    $("#desctipo").select2({
                        language: 'pt-BR',
                        ajax: {
                            url: __financeiroDescontoAdd.options.url.financeiroDescontoTipo,
                            dataType: 'json',
                            delay: 250,
                            data: function (query) {
                                return {query: query};
                            },
                            results: function (data) {
                                var transformed = $.map(data, function (el) {
                                    el.text = el.desctipoDescricao;
                                    el.id = el.desctipoId;

                                    return el;
                                });

                                return {results: transformed};
                            }
                        }
                    }).change(function () {
                        __financeiroDescontoAdd.steps.dadosBasicos.validaCampos();
                    });

                    if (__financeiroDescontoAdd.getDesctipo()) {
                        $('#desctipo').select2("data", __financeiroDescontoAdd.getDesctipo()).trigger('change');
                    }

                    $('#descontoValor,#descontoPercentual').inputmask({
                        showMaskOnHover: false,
                        alias: 'currency',
                        groupSeparator: "",
                        radixPoint: ",",
                        placeholder: "0",
                        prefix: "",
                        allowMinus: 0
                    });

                    $("#descontoDiaLimite").inputmask({showMaskOnHover: false, mask: ['9{0,2}'], rightAlign: true});

                    $("#descontoStatus").select2({language: 'pt-BR', minimumInputLength: -1}).trigger("change");

                    $("#aluno").select2({
                        language: 'pt-BR',
                        minimumInputLength: 1,
                        ajax: {
                            url: __financeiroDescontoAdd.options.url.acadgeralAluno,
                            dataType: 'json',
                            delay: 250,
                            data: function (query) {
                                return {query: query, somenteAlunos: true, agruparAluno: true};
                            },
                            results: function (data) {
                                var transformed = $.map(data, function (el) {
                                    el.text = el.pesNome;
                                    el.id = el.alunoId;

                                    return el;
                                });

                                return {results: transformed};
                            }
                        }
                    });

                    if (__financeiroDescontoAdd.getAluno()) {
                        $('#aluno').select2("data", __financeiroDescontoAdd.getAluno()).trigger('change');
                    }

                    $("#descontoPercentual, #descontoValor").change(function () {
                        __financeiroDescontoAdd.steps.titulosVinculados.redrawdDataTableTitulosVinculados();
                    });
                },
                validaCampos: function () {
                    var arrData = $('#desctipo').select2("data") || {};

                    $('#desctipoPercmax').html(
                        __financeiroDescontoAdd.formatarMoeda(arrData['desctipoPercmax']) || '-');
                    $('#desctipoValormax').html(
                        __financeiroDescontoAdd.formatarMoeda(arrData['desctipoValormax']) || '-');
                    $('#desctipoModalidade').html(arrData['desctipoModalidade'] || '-');
                    $('#desctipoLimitaVencimento').html(arrData['desctipoLimitaVencimento'] || '-');
                    $('#tipotituloNome').html(arrData['tipotituloNome'] || 'Todos os títulos');

                    var desctipoPercmax = parseFloat(arrData['desctipoPercmax'] || '0');
                    var desctipoValormax = parseFloat(arrData['desctipoValormax'] || '0');

                    $("#descontoPercentual, #descontoValor").prop('disabled', true);

                    if (desctipoPercmax != 0) {
                        $("#descontoPercentual").prop('disabled', false);
                        $("#descontoValor").val('');
                        desctipoValormax = 0;
                    }

                    if (desctipoValormax != 0) {
                        $("#descontoValor").prop('disabled', false);
                        $("#descontoPercentual").val('');
                        desctipoPercmax = 0;
                    }

                },
                validate: function () {
                    var arrData = $('#desctipo').select2("data") || {};

                    __financeiroDescontoAdd.options.validator.settings.rules = {
                        desctipo: {number: true, required: true},
                        descontoValor: {
                            maxlength: 10,
                            required: function () {
                                var desctipoValormax = parseFloat(arrData['desctipoValormax'] || '0');
                                return desctipoValormax != 0;
                            },
                            valorMaxBR: function () {
                                var valorMax = arrData['desctipoValormax'] || 0;

                                if (arrData['desctipoValormax']) {
                                    return parseFloat(valorMax);
                                } else {
                                    return false
                                }
                            }
                        },
                        descontoPercentual: {
                            maxlength: 6,
                            required: function () {
                                var desctipoPercmax = parseFloat(arrData['desctipoPercmax'] || '0');

                                return desctipoPercmax != 0;
                            },
                            valorMaxBR: function () {
                                var desctipoPercmax = __financeiroDescontoAdd.formatarMoeda(arrData['desctipoPercmax']);

                                if (arrData['desctipoPercmax']) {
                                    return parseFloat(desctipoPercmax);
                                } else {
                                    return false;
                                }
                            }
                        },
                        descontoDiaLimite: {
                            maxlength: 2, number: true, min: 1, max: 31
                        },
                        descontoStatus: {required: true},
                        aluno: {required: true, number: true}
                    };
                    __financeiroDescontoAdd.options.validator.settings.messages = {
                        desctipo: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                        descontoValor: {
                            maxlength: 'Tamanho máximo: 10!',
                            required: 'Campo obrigatório !',
                            valorMaxBR: 'Desconto máximo: ' + $('#desctipoValormax').html() + '!'
                        },
                        descontoPercentual: {
                            maxlength: 'Tamanho máximo: 6!',
                            required: 'Campo obrigatório!',
                            valorMaxBR: "Porcentagem máxima:" + $('#desctipoPercmax').html() + "%!"
                        }
                        ,
                        descontoDiaLimite: {
                            maxlength: 'Tamanho máximo: 2!',
                            number: 'Número inválido!',
                            required: 'Campo obrigatório!',
                            min: 'Valor mínimo: {1}!',
                            max: 'Valor máximo: {1}!'
                        }
                        ,
                        descontoStatus: {
                            required: 'Campo obrigatório!'
                        }
                        ,
                        aluno: {
                            number: 'Número inválido!', required: 'Campo obrigatório!'
                        }
                    };

                    return !$(__financeiroDescontoAdd.options.formElement).valid();
                }
            };

            this.steps.titulosVinculados = {
                init: function () {
                    __financeiroDescontoAdd.steps.titulosVinculados.iniciadataTableTitulosVinculados();
                    __financeiroDescontoAdd.steps.titulosVinculados.iniciadataTableTitulosAbertosPessoa();
                },
                validate: function () {
                },
                validaCamposDescontos: function (inativar, redrawdDataTableTitulos) {
                    if (inativar) {
                        $("#aluno,#desctipo").select2("enable", false);
                        $("#btn-salvarDesconto,#btn-add-descontoTitulo,.btn-descontoTitulo-indeferir,.btn-financeiro-desconto-deferir").addClass('hidden');
                        $("#descontoPercentual,#descontoValor,#descontoObservacao,#descontoDiaLimite").prop('disabled', true);

                    } else {

                        var desctipo = $("#desctipo").select2("data");

                        if (desctipo) {
                            if (desctipo['desctipoValormax']) {
                                $("#descontoValor").prop("disabled", false);
                            } else {
                                $("#descontoPercentual").prop("disabled", false);
                            }
                        }

                        $("#descontoStatus,#aluno,#desctipo").select2("enable", true);
                        $("#btn-salvarDesconto,#btn-add-descontoTitulo,.btn-descontoTitulo-indeferir,.btn-descontoTitulo-deferir").removeClass('hidden');
                        $("#descontoObservacao,#descontoDiaLimite").prop('disabled', false);

                    }

                    if (redrawdDataTableTitulos) {
                        var valida = {};

                        valida['validando'] = true;
                        __financeiroDescontoAdd.steps.titulosVinculados.redrawdDataTableTitulosVinculados(valida);

                    }

                },
                iniciadataTableTitulosVinculados: function () {
                    var colNum = 0;
                    var dtTitulosVinculados = $('#dataTableTitulosVinculados').dataTable({
                        processing: true,
                        columnDefs: [
                            {name: "tituloId", targets: colNum++, data: "tituloId", className: "text-right"},
                            {name: "pesNome", targets: colNum++, data: "pesNome"},
                            {name: "cursoSigla", targets: colNum++, data: "cursoSigla"},
                            {name: "tituloDescricao", targets: colNum++, data: "tituloDescricao"},
                            {
                                name: "tituloValorFormatado",
                                targets: colNum++,
                                data: "tituloValorFormatado",
                                className: "text-right"
                            },
                            {
                                name: "descontoTituloValorCalculadoFormatado", targets: colNum++,
                                data: "descontoTituloValorCalculadoFormatado", className: "text-right"
                            },
                            {name: "descontoTituloAplicado", targets: colNum++, data: "descontoTituloAplicado"},
                            {name: "descontoTituloSituacao", targets: colNum++, data: "descontoTituloSituacao"},
                            {name: "descontoTituloAcoes", targets: colNum++, data: 'descontoTituloAcoes'},
                            {name: "descontoTituloId", targets: colNum++, data: "descontoTituloId", visible: false}
                        ],
                        "dom": "<'dt-toolbar'<'col-xs-3 dt-toolbar-descontoTitulo no-padding'B><'col-xs-6 descontoTitulo-aviso'><'col-xs-3 no-padding'f>r>t" +
                        "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                        "autoWidth": true,
                        oLanguage: {
                            sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                            sLengthMenu: "Mostrar _MENU_ registros por página",
                            sZeroRecords: "Nenhum registro encontrado",
                            sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                            sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                            sInfoFiltered: "(filtrado de _MAX_ registros)",
                            sSearch: "Procurar: ",
                            oPaginate: {
                                sFirst: "Início",
                                sPrevious: "Anterior",
                                sNext: "Próximo",
                                sLast: "Último"
                            }
                        },
                        order: [[1, 'desc']]
                    });

                    __financeiroDescontoAdd.setDatatable('titulosVinculados', dtTitulosVinculados);

                    $('.dt-toolbar-descontoTitulo').append(
                        '<div class="btn-group">' +
                        '<button type="button" class="btn btn-primary" id="btn-add-descontoTitulo">Adicionar</button>' +
                        '</div>'
                    );

                    $(document).on('click', '#btn-add-descontoTitulo', function () {
                        __financeiroDescontoAdd.steps.titulosVinculados.showModalTituloDesconto();
                        var data = {'GUID': __financeiroDescontoAdd.createGUID()};
                        __financeiroDescontoAdd.steps.titulosVinculados.showModalTituloDesconto(data);
                    });

                    $('#dataTableTitulosVinculados').on('click', '.btn-descontoTitulo-remove', function () {
                        var $pai = $(this).closest('tr');
                        var data = __financeiroDescontoAdd.getDatatable('titulosVinculados').fnGetData($pai);

                        $.SmartMessageBox({
                            title: "Confirme a operação:",
                            content: 'Deseja remover o título "' + data['tituloId'] + '" da relação de descontos?',
                            buttons: "[Não][Sim]"
                        }, function (ButtonPress) {
                            if (ButtonPress == "Sim") {
                                __financeiroDescontoAdd.steps.titulosVinculados.removeTituloDesconto(data['tituloId']);
                            }
                        });
                    });

                    var descontoStatus = $("#descontoStatus");

                    $('#dataTableTitulosVinculados').on(
                        'click',
                        '.btn-descontoTitulo-deferir, .btn-descontoTitulo-indeferir',
                        function () {
                            var $pai = $(this).closest('tr');
                            var data = __financeiroDescontoAdd.getDatatable('titulosVinculados').fnGetData($pai);
                            var sit = $(this).hasClass('btn-descontoTitulo-indeferir') ? 'Indeferi' : 'Deferi';
                            var situacao = sit + 'do';
                            var limitadoVencimento = data.desctipoLimitaVencimento;

                            var dataHj = new Date();
                            dataHj.setHours(0);
                            dataHj.setMinutes(0);
                            dataHj.setSeconds(0);
                            dataHj.setMilliseconds(0);
                            var dataVencimento = $.datepicker.parseDate("dd/mm/yy", data.tituloDataVencimento);

                            if(limitadoVencimento && limitadoVencimento == 'Sim' && situacao == "Deferido" && dataHj.getTime() > dataVencimento.getTime() ){
                                __financeiroDescontoAdd.showNotificacaoDanger("Título já vencido, não é possível deferir o desconto neste título.");
                                return false;
                            }
                            else if (data['desconto_dados'] > 100) {
                                __financeiroDescontoAdd.showNotificacaoWarning('Valor do percentual não pode extrapolar 100%!');
                                return false;
                            }
                            if ($('#descontoPercentual').val() == 100) {
                                __financeiroDescontoAdd.showNotificacaoInfo('Atenção você esta deferindo um desconto de 100% para este título! ');
                                $.SmartMessageBox({
                                    title: "Confirme a operação:",
                                    content: 'Atenção o desconto vinculado a este título é de 100%, tem certeza que deseja ' + sit + 'r o desconto no título "' + data['tituloId'] + '"?',
                                    buttons: "[Não][Sim]"
                                }, function (ButtonPress) {
                                    if (ButtonPress == "Sim") {
                                        __financeiroDescontoAdd.steps.titulosVinculados.alteraDeferimentoTituloDesconto(
                                            data['tituloId'],
                                            situacao
                                        );
                                    }
                                });
                            }
                            else {
                                $.SmartMessageBox({
                                    title: "Confirme a operação:",
                                    content: 'Deseja ' + sit + 'r o desconto no título "' + data['tituloId'] + '"?',
                                    buttons: "[Não][Sim]"
                                }, function (ButtonPress) {
                                    if (ButtonPress == "Sim") {
                                        __financeiroDescontoAdd.steps.titulosVinculados.alteraDeferimentoTituloDesconto(
                                            data['tituloId'],
                                            situacao
                                        );
                                    }
                                });
                            }
                        }
                    );

                    __financeiroDescontoAdd.steps.titulosVinculados.redrawdDataTableTitulosVinculados();
                },
                redrawdDataTableTitulosVinculados: function (arrDados) {
                    __financeiroDescontoAdd.getDatatable('titulosVinculados').fnClearTable();
                    var btnDeferirSalvar = $("#btn-financeiro-desconto-deferir");
                    var data = [];
                    var titulosVinculados = __financeiroDescontoAdd.getTitulosVinculados();
                    var descontoStatus = $("#descontoStatus").val() == 'Inativo' ? true : false;
                    var btnAtivadoDeferir = false;


                    if (titulosVinculados && arrDados) {
                        if (arrDados['validando']) {
                            __financeiroDescontoAdd.steps.titulosVinculados.validaCamposDescontos(descontoStatus, false);
                        }

                    }


                    btnDeferirSalvar.addClass("hidden");

                    for (var i in titulosVinculados) {

                        var item = titulosVinculados[i];
                        item['descontoTituloId'] = item['descontoTituloId'] || '';
                        item['descontoTituloValorCalculadoFormatado'] = item['descontoTituloValor'] || '';
                        item['descontoTituloAplicado'] = (item['descontoTituloAplicado'] || 'Não');
                        item['descontoTituloSituacao'] =
                            (item['descontoTituloSituacao'] || 'Pendente');

                        if (arrDados) {
                            if (arrDados['erro']) {
                                item['descontoTituloAplicado'] = true;
                                item['descontoTituloSituacao'] = 'Pendente';
                            }
                        }
                        item['cursoSigla'] = (item['cursoSigla'] || (item['cursoNome'] || '').substr(0, 3).toUpperCase());
                        item['descontoTituloAcoes'] = '';

                        if (item['descontoTituloAplicado'] != 'Não' && item['tituloEstado'] != 'Aberto') {
                            item['descontoTituloAcoes'] = __financeiroDescontoAdd.createBtnGroup([]);
                        } else {
                            var btns = [];

                            if (item['descontoTituloSituacao'] != 'Deferido' && !descontoStatus) {
                                btns.push(
                                    {
                                        class: 'btn-primary btn-descontoTitulo-deferir', icon: 'fa-check',
                                        title: 'Deferir título'
                                    }
                                );
                            }

                            if (item['descontoTituloSituacao'] != 'Indeferido' && !descontoStatus) {
                                btns.push(
                                    {
                                        class: 'btn-warning btn-descontoTitulo-indeferir', icon: 'fa-ban',
                                        title: 'Indeferir título'
                                    }
                                );
                            }

                            if (item['descontoTituloSituacao'] == 'Pendente' && !descontoStatus) {
                                btns.push(
                                    {
                                        class: 'btn-danger btn-descontoTitulo-remove', icon: 'fa-times',
                                        title: 'Remover desconto do título.'
                                    }
                                );
                            }

                            item['descontoTituloAcoes'] = __financeiroDescontoAdd.createBtnGroup(btns);
                        }

                        if (!item['descontoTituloValorCalculadoFormatado']) {

                            var tituloValor = parseFloat(item['tituloValor']);
                            var descontoPercentual = parseFloat($('#descontoPercentual').val() || '0');
                            var descontoValor = parseFloat($('#descontoValor').val() || '0');


                            if (item['tituloValor'] == item['tituloValorFormatado']) {

                                tituloValor = item['tituloValor'].toString().replace(/\./, '').replace(/,/, '.');
                            }

                            if (descontoPercentual != 0) {
                                descontoValor = tituloValor * (descontoPercentual / 100);
                            }

                            item['descontoTituloValorCalculadoFormatado'] = descontoValor ? descontoValor.toFixed(2) : '0.00';
                        }

                        item['descontoTituloValorCalculadoFormatado'] =
                            __financeiroDescontoAdd.formatarMoeda(item['descontoTituloValorCalculadoFormatado']);

                        if (item['descontoTituloSituacao'] == 'Pendente') {
                            btnDeferirSalvar.removeClass("hidden");
                        }


                        data.push(item);

                        if (!descontoStatus && !btnAtivadoDeferir) {
                            if (item['descontoTituloSituacao'] == 'Pendente' || item['descontoTituloSituacao'] == 'Indeferido') {
                                $("#btn-financeiro-desconto-deferir").removeClass("hidden");
                                btnAtivadoDeferir = true;

                            }
                        }
                    }

                    if (data.length > 0) {
                        __financeiroDescontoAdd.getDatatable('titulosVinculados').fnAddData(data);
                    }

                    __financeiroDescontoAdd.getDatatable('titulosVinculados').fnDraw();
                },
                iniciadataTableTitulosAbertosPessoa: function () {
                    var colNum = 0;
                    var dtTitulosAbertosPessoa = $('#dataTableTitulosAbertosPessoa').dataTable({
                        processing: true,
                        serverSide: true,
                        ajax: {
                            url: __financeiroDescontoAdd.options.url.financeiroTitulo,
                            type: "POST",
                            data: function (d) {
                                d.filter = d.filter || {};
                                var arrAluno = $("#aluno").select2('data') || {};
                                var arrDescTipo = $("#desctipo").select2('data') || {};

                                var pesId = arrAluno['pesId'] || "-1";
                                var tipotituloId = arrDescTipo['tipotituloId'] || "";

                                if (tipotituloId) {
                                    d.filter['tipotitulo'] = tipotituloId;
                                }

                                d.filter['tituloEstado'] = ['Aberto'];
                                d.filter['pesId'] = pesId;

                                __financeiroDescontoAdd.addOverlay(
                                    $('#modal-descontoTitulo'),
                                    'Aguarde, carregando títulos em aberto do aluno.'
                                );

                                return d;
                            },
                            dataSrc: function (json) {
                                var data = json.data;
                                var titulosVinculados = __financeiroDescontoAdd.getTitulosVinculados();

                                var perctDesc = $('#descontoPercentual').val(),
                                    valDesc = $('#descontoValor').val();

                                for (var row in data) {
                                    var dataProcessamento = __financeiroDescontoAdd.formatDate(data[row]['titulo_data_processamento']);
                                    var dataVencimento = __financeiroDescontoAdd.formatDate(data[row]['titulo_data_vencimento']);
                                    var dataPagamento = __financeiroDescontoAdd.formatDate(data[row]['titulo_data_pagamento']);
                                    var vencido = false,
                                        descontar = 0;

                                    if (perctDesc) {
                                        descontar = parseFloat(data[row]['titulo_multa_calc']) + parseFloat(data[row]['titulo_juros_calc']) + ((perctDesc / 100) * data[row]['titulo_valor']);

                                    } else if (valDesc) {
                                        descontar = data[row]['titulo_multa_calc'] + data[row]['titulo_juros_calc'] + valDesc;
                                    }

                                    var encontrado = false;

                                    for (var i in titulosVinculados) {
                                        if (parseInt(titulosVinculados[i]['tituloId']) ==
                                            parseInt(data[row]['titulo_id'])) {
                                            encontrado = true;
                                        }
                                    }

                                    var arrData = $('#desctipo').select2("data") || {};

                                    if (parseInt(data[row]['dias_atraso']) > 0 && arrData['desctipoLimitaVencimento'] == 'Sim') {
                                        vencido = true;
                                    }

                                    var btns;

                                    if (descontar > data[row]['titulo_valor_calc']) {
                                        btns = [
                                            {
                                                class: 'btn-warning -', icon: 'fa fa-exclamation',
                                                title: 'O desconto excede o valor liquido do título'
                                            }
                                        ];

                                    } else if (vencido) {
                                        btns = [
                                            {
                                                class: 'btn-warning -', icon: 'fa-warning',
                                                title: 'Desconto limitado ao vencimento'
                                            }
                                        ];

                                    } else {
                                        btns = [
                                            {
                                                class: 'btn-primary btn-descontoTitulo-add', icon: 'fa-download',
                                                title: 'Adicionar desconto ao título'
                                            }
                                        ];
                                    }

                                    data[row]['curso_sigla'] = (
                                        data[row]['curso_sigla'] ||
                                        (data[row]['curso_nome'] || '').substr(0, 3).toUpperCase()
                                    );

                                    data[row]['titulo_valor_formatado'] = (
                                        '<span ' + (vencido ? ' class="text-danger"' : '') + '>' +
                                        __financeiroDescontoAdd.formatarMoeda(data[row]['titulo_valor']) +
                                        '</span>'
                                    );

                                    data[row]['acao'] = __financeiroDescontoAdd.createBtnGroup(!encontrado ? btns : []);
                                    data[row]['titulo_data_processamento_formatado'] = dataProcessamento;
                                    data[row]['titulo_data_vencimento_formatado'] = dataVencimento;
                                    data[row]['titulo_data_pagamento_formatado'] = dataPagamento;

                                }

                                __financeiroDescontoAdd.removeOverlay($('#modal-descontoTitulo'));

                                return data;

                            }
                        },
                        error: function (xhr, error, thrown) {
                            __financeiroDescontoAdd.showNotificacaoWarning(
                                "Não foi possível efetuar a operação. Tente efetuar novamente a operação ou recarregue a página."
                            );
                            __financeiroDescontoAdd.removeOverlay($('#modal-descontoTitulo'));
                        },
                        columnDefs: [
                            {name: "titulo_id", targets: colNum++, data: "titulo_id", className: "text-right"},
                            {name: "titulo_descricao", targets: colNum++, data: "titulo_descricao"},
                            {name: "pes_nome", targets: colNum++, data: "pes_nome"},
                            {name: "curso_sigla", targets: colNum++, data: "curso_sigla"},
                            {
                                name: "titulo_valor_Format",
                                targets: colNum++,
                                data: "titulo_valor_Format",
                                className: "text-right"
                            },
                            {
                                name: "titulo_desconto_calc_formatado",
                                targets: colNum++,
                                data: "titulo_desconto_calc_formatado",
                                className: "text-right"
                            },
                            {
                                name: "titulo_valor_calc_formatado",
                                targets: colNum++,
                                data: "titulo_valor_calc_formatado",
                                className: "text-right"
                            },
                            {
                                name: "titulo_data_processamento", targets: colNum++,
                                data: "titulo_data_processamento_formatado",
                                className: "text-right"
                            },
                            {
                                name: "titulo_data_vencimento", targets: colNum++,
                                data: "titulo_data_vencimento_formatado",
                                className: "text-right"
                            },
                            {name: "titulo_id", targets: colNum++, data: "acao", orderable: false}
                        ],
                        "dom": "fr" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                        "autoWidth": true,
                        oLanguage: {
                            sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                            sLengthMenu: "Mostrar _MENU_ registros por página",
                            sZeroRecords: "Nenhum registro encontrado",
                            sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                            sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                            sInfoFiltered: "(filtrado de _MAX_ registros)",
                            sSearch: "Procurar: ",
                            oPaginate: {
                                sFirst: "Início",
                                sPrevious: "Anterior",
                                sNext: "Próximo",
                                sLast: "Último"
                            }
                        },
                        order: [[0, 'asc']]
                    });

                    __financeiroDescontoAdd.setDatatable('titulosAbertosPessoa', dtTitulosAbertosPessoa);

                    $('#dataTableTitulosAbertosPessoa').on('click', '.btn-descontoTitulo-add', function () {

                        var $pai = $(this).closest('tr');
                        var data = __financeiroDescontoAdd.getDatatable('titulosAbertosPessoa').fnGetData($pai);

                        __financeiroDescontoAdd.steps.titulosVinculados.adicionaTituloDesconto(
                            __financeiroDescontoAdd.keysToCamelCase(data)
                        );

                        $(this).prop('disabled', true);
                    });
                },
                adicionaTituloDesconto: function (arrTitulo) {
                    var encontrado = false;
                    var titulosVinculados = __financeiroDescontoAdd.getTitulosVinculados();

                    for (var i in titulosVinculados) {
                        if (parseInt(titulosVinculados[i]['tituloId']) == parseInt(arrTitulo['tituloId'])) {
                            encontrado = true;
                        }
                    }

                    if (encontrado) {
                        __financeiroDescontoAdd.showNotificacaoWarning('Título já incluído na relação de desconto!');
                        return false;
                    }

                    titulosVinculados.push(arrTitulo);

                    __financeiroDescontoAdd.setTitulosVinculados(titulosVinculados);

                },
                removeTituloDesconto: function (tituloId) {
                    var encontrado = false;
                    var titulosVinculados = __financeiroDescontoAdd.getTitulosVinculados();
                    var novosTitulosVinculados = [];

                    for (var i in titulosVinculados) {
                        if (parseInt(titulosVinculados[i]['tituloId']) == parseInt(tituloId)) {
                            encontrado = true;
                        } else {
                            novosTitulosVinculados.push(titulosVinculados[i]);
                        }
                    }

                    if (!encontrado) {
                        __financeiroDescontoAdd.showNotificacaoWarning('Título não encontrado na relação de desconto!');
                        return false;
                    }

                    __financeiroDescontoAdd.setTitulosVinculados(novosTitulosVinculados);
                },
                alteraDeferimentoTituloDesconto: function (tituloId, situacao) {
                    var encontrado = false;
                    var titulosVinculados = __financeiroDescontoAdd.getTitulosVinculados();

                    for (var i in titulosVinculados) {
                        if (parseInt(titulosVinculados[i]['tituloId']) == parseInt(tituloId)) {
                            encontrado = true;
                            titulosVinculados[i]['descontoTituloSituacao'] = situacao;
                        }
                    }

                    if (!encontrado) {
                        __financeiroDescontoAdd.showNotificacaoWarning('Título não encontrado na relação de desconto!');
                        return;
                    }

                    __financeiroDescontoAdd.setTitulosVinculados(titulosVinculados);
                },
                showModalTituloDesconto: function () {
                    __financeiroDescontoAdd.getDatatable('titulosAbertosPessoa').api().ajax.reload(null, false);
                    $('#modal-descontoTitulo').modal('show');
                }
            };

            this.setDesctipo = function (desctipo) {
                this.options.value.desctipo = desctipo || null;
            };

            this.getDesctipo = function () {
                return this.options.value.desctipo || null;
            };

            this.setAluno = function (aluno) {
                this.options.value.aluno = aluno || null;
            };

            this.getAluno = function () {
                return this.options.value.aluno || null;
            };

            this.setTitulosVinculados = function (titulosVinculados) {
                this.options.value.titulosVinculados = titulosVinculados || [];
                __financeiroDescontoAdd.steps.titulosVinculados.redrawdDataTableTitulosVinculados();
            };

            this.getTitulosVinculados = function () {
                return this.options.value.titulosVinculados || [];
            };

            this.pesquisaFinanceiroDesconto = function (desconto_id, callback) {
                var $form = $(__financeiroDescontoAdd.options.formElement);
                var action = $form.attr('action');
                action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

                __financeiroDescontoAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

                $.ajax({
                    url: action + "/edit/" + desconto_id,
                    dataType: 'json',
                    type: 'get',
                    data: {ajax: true},
                    success: function (data) {
                        if (Object.keys(data).length == 0) {
                            __financeiroDescontoAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                        } else {
                            callback(data['arrDados']);
                        }

                        __financeiroDescontoAdd.removeOverlay($form);
                    },
                    error: function () {
                        __financeiroDescontoAdd.showNotificacaoDanger('Erro desconhecido!');
                        __financeiroDescontoAdd.removeOverlay($form);
                    }
                });
            };

            this.setValidations = function () {
                $(__financeiroDescontoAdd.options.formElement).submit(function (e) {
                    var $form = $(__financeiroDescontoAdd.options.formElement);
                    var ok = __financeiroDescontoAdd.validate();
                    var titulosVinculados = __financeiroDescontoAdd.getTitulosVinculados();
                    var arrTitulosVinculados = __financeiroDescontoAdd.getTitulosVinculados();

                    __financeiroDescontoAdd.addOverlay($form, 'Salvando registro. Aguarde...');


                    if (!ok) {
                        __financeiroDescontoAdd.removeOverlay($form);
                        e.preventDefault();
                        e.stopPropagation();
                        return false;
                    }

                    if (arrTitulosVinculados.length <= 0) {
                        __financeiroDescontoAdd.removeOverlay($form);
                        __financeiroDescontoAdd.showNotificacaoWarning(
                            "Nenhum título foi vinculado a este desconto!");

                        __financeiroDescontoAdd.removeOverlay($form);
                        e.preventDefault();
                        e.stopPropagation();
                        return false;
                    }

                    $('#titulosVinculados').val(JSON.stringify($.makeArray(titulosVinculados)));

                    if (__financeiroDescontoAdd.options.ajaxSubmit || $form.data('ajax')) {
                        $("#aluno").select2('enable');
                        $("#desctipo").select2('enable');

                        var dados = $form.serializeJSON();
                        dados['ajax'] = true;
                        dados['tituloId'] = titulosVinculados[0]['tituloId'];
                        dados['titulosvinculados'] = titulosVinculados;
                        dados['descontoStatus'] = $("#descontoStatus").val();

                        if (Object.keys(titulosVinculados).length > 0) {
                            $("#aluno").select2('disable');
                            $("#desctipo").select2('disable');
                        }

                        if (!$form.data('ajax')) {
                            $form.data('ajax', true);
                            $.ajax({
                                url: $form.attr('action'),
                                type: 'POST',
                                dataType: 'json',
                                data: dados,
                                success: function (data) {
                                    if (data.erro) {
                                        __financeiroDescontoAdd.steps.titulosVinculados.redrawdDataTableTitulosVinculados([{'erro': true}]);
                                        __financeiroDescontoAdd.showNotificacaoDanger(
                                            data['mensagem'] || "Não foi possível salvar registro"
                                        );
                                        $('#financeiro-desconto-modal').modal('hide');
                                        __financeiroDescontoAdd.removeOverlay($form);

                                        if ($("#descontoStatus").val() == 'Inativo') {
                                            $("#descontoStatus").prop("disabled", false);

                                            var temp = $("#desctipo").select2("data");
                                            $("#btn-salvarDesconto").removeClass("hidden");
                                            if (temp) {
                                                if (temp['desctipoPercmax']) {
                                                    $("#descontoPercentual").prop("disabled", false);
                                                    $("#descontoValor").prop("disabled", true);
                                                } else {
                                                    $("#descontoValor").prop("disabled", false);
                                                    $("#descontoPercentual").prop("disabled", true);
                                                }
                                            }
                                        }
                                    } else if (__financeiroDescontoAdd.options.listagem) {
                                        $.financeiroDescontoIndex().reloadDataTableFinanceiroDesconto();
                                        __financeiroDescontoAdd.showNotificacaoSuccess(
                                            data['mensagem'] || "Registro Salvo!"
                                        );
                                        $('#financeiro-desconto-modal').modal('hide');
                                    }

                                    __financeiroDescontoAdd.removeOverlay($form);
                                    $form.data('ajax', false);
                                },
                                error: function (xhr, error, thrown) {
                                    __financeiroDescontoAdd.showNotificacaoWarning(
                                        "Não foi possível efetuar a operação. Tente efetuar novamente a operação ou recarregue a página."
                                    );
                                    __financeiroDescontoAdd.removeOverlay($form);
                                }
                            });
                        } else {
                            $form.data('ajax', false);
                        }

                        e.preventDefault();
                        e.stopPropagation();
                        return false;
                    }

                    this.submit();
                    return true;
                });
            };

            this.run = function (opts) {
                this.setDefaults(opts);
                this.loadSteps();
                this.setValidations();
                this.wizard();
            };
        }
        ;

    $.financeiroDescontoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.financeiro-desconto.add");

        if (!obj) {
            obj = new FinanceiroDescontoAdd();
            obj.run(params);
            $(window).data('universa.financeiro.financeiro-desconto.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);