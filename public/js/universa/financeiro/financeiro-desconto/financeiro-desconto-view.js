(function ($, window, document) {
    'use strict';

    var FinanceiroDescontoView = function () {
        VersaShared.call(this);
        var __financeiroDescontoView = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {},
            data: {},
            value: {arrDados: []},
            datatables: {titulosVinculados: null},
            wizardElement: '#financeiro-desconto-wizard-view',
            formElement: '#financeiro-desconto-form-view',
            validator: null
        };

        this.iniciadataTableTitulosVinculados = function () {
            var colNum = 0;

            var dtTitulosVinculados = $('#dataTableTitulosVinculadosDescontos-view').dataTable({
                processing: true,
                columnDefs: [
                    {name: "tituloId", targets: colNum++, data: "tituloId"},
                    {name: "tituloDescricao", targets: colNum++, data: "tituloDescricao"},
                    {name: "tituloValor", targets: colNum++, data: "tituloValor"},
                    {
                        name: "descontoTituloValorCalculadoFormatado", targets: colNum++,
                        data: "descontoTituloValorCalculadoFormatado"
                    },
                    {name: "descontoTituloAplicado", targets: colNum++, data: "descontoTituloAplicado"},
                    {name: "descontoTituloSituacao", targets: colNum++, data: "descontoTituloSituacao"}
                ],
                "dom": "<'dt-toolbar'<'col-xs-3 dt-toolbar-descontoTitulo-view no-padding'B><'col-xs-6 descontoTitulo-aviso'><'col-xs-3 no-padding'f>r>t",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[1, 'desc']]
            });

            __financeiroDescontoView.setDatatable('titulosVinculados', dtTitulosVinculados);

            __financeiroDescontoView.redrawdataTableTitulosVinculados();
        };
        this.redrawdataTableTitulosVinculados = function () {
            __financeiroDescontoView.getDatatable('titulosVinculados').fnClearTable();
            var data = [];
            var titulosVinculados = __financeiroDescontoView.getTitulosVinculados();

            for (var i in titulosVinculados) {
                var item = titulosVinculados[i];
                item['descontoTituloId'] = item['descontoTituloId'] || '';
                item['descontoTituloValorCalculadoFormatado'] = item['descontoTituloValor'] || '';
                item['descontoTituloAplicado'] = item['descontoTituloAplicado'] || 'Não';
                item['descontoTituloSituacao'] = item['descontoTituloSituacao'] || 'Pendente';

                if (!item['descontoTituloValorCalculadoFormatado']) {
                    var tituloValor = parseFloat(item['tituloValor']);
                    var descontoPercentual = parseFloat(
                        __financeiroDescontoView.getArrDados()['descontoPercentual'] || '0'
                    );
                    var descontoValor = parseFloat(
                        __financeiroDescontoView.getArrDados()['descontoValor'] || '0'
                    );

                    if (descontoPercentual != 0) {
                        descontoValor = tituloValor * (descontoPercentual / 100);
                    }

                    item['descontoTituloValorCalculadoFormatado'] = descontoValor;
                }

                data.push(item);
            }

            if (data.length > 0) {
                __financeiroDescontoView.getDatatable('titulosVinculados').fnAddData(data);
            }

            __financeiroDescontoView.getDatatable('titulosVinculados').fnDraw();
        };

        this.pesquisaFinanceiroDesconto = function (desconto_id, callback) {
            var $form = $(__financeiroDescontoView.options.formElement);
            var action = $form.attr('action');
            __financeiroDescontoView.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/" + desconto_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __financeiroDescontoView.showNotificacaoInfo('Nenhum registro encontrado!');
                    }

                    __financeiroDescontoView.setArrDados(data['arrDados'] || {});

                    if (callback) {
                        callback(__financeiroDescontoView.getArrDados());
                    }

                    __financeiroDescontoView.removeOverlay($form);
                },
                error: function () {
                    __financeiroDescontoView.showNotificacaoDanger('Erro desconhecido!');
                    __financeiroDescontoView.removeOverlay($form);
                }
            });
        };

        this.setArrDados = function (arrDados) {
            this.options.value.arrDados = arrDados || [];
            $('#alunoDesconto-view').html(arrDados['aluno']['text'] || '-');
            $('#desctipoDescricao-view').html(arrDados['desctipo']['desctipoDescricao'] || '-');
            $('#desctipoPercmax-view').html(arrDados['desctipo']['desctipoPercmax'] || '-');
            $('#desctipoValormax-view').html(arrDados['desctipo']['desctipoValormax'] || '-');
            $('#desctipoModalidade-view').html(arrDados['desctipo']['desctipoModalidade'] || '-');
            $('#desctipoLimitaVencimento-view').html(arrDados['desctipo']['desctipoLimitaVencimento'] || '-');
            $('#tipotituloNome-view').html(arrDados['tipotituloNome'] || 'TODOS');
            $('#descontoPercentual-view').html(arrDados['descontoPercentual'] || '-');
            $('#descontoValor-view').html(arrDados['descontoValor'] || '-');
            $('#descontoDiaLimite-view').html(arrDados['descontoDiaLimite'] || '-');
            $('#descontoStatus-view').html(arrDados['descontoStatus'] || '-');
            $('#descontoObservacao-view').html(arrDados['descontoObservacao'] || '-');
            __financeiroDescontoView.redrawdataTableTitulosVinculados();
        };

        this.getArrDados = function () {
            return __financeiroDescontoView.options.value.arrDados || [];
        };

        this.setTitulosVinculados = function (titulosVinculados) {
            this.options.value.arrDados.titulosVinculados = titulosVinculados || [];
            __financeiroDescontoView.redrawdataTableTitulosVinculados();
        };

        this.getTitulosVinculados = function () {
            return __financeiroDescontoView.getArrDados()['titulosVinculados'] || [];
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            __financeiroDescontoView.iniciadataTableTitulosVinculados();
            this.wizard();
        };
    };

    $.financeiroDescontoView = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.financeiro-desconto.view");

        if (!obj) {
            obj = new FinanceiroDescontoView();
            obj.run(params);
            $(window).data('universa.financeiro.financeiro-desconto.view', obj);
        }

        return obj;
    };
})
(window.jQuery, window, document);