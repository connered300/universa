(function ($, window, document) {
    'use strict';
    var FinanceiroDescontoIndex = function () {
        VersaShared.call(this);
        var __financeiroDescontoIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: '',
                view: ''
            },
            data: {
                arrFilters: {}
            },
            datatables: {
                financeiroDesconto: null
            },
            value: {
                arrPermissoes: {},
                aluno: {},
                painel: false,
                tituloAberto: null,
                bloqueaBtnNovoDesconto: false,
                alunoPerId: null
            },
            readonly: false
        };

        this.setSteps = function () {
            var colNum = 0;
            __financeiroDescontoIndex.options.datatables.financeiroDesconto =
                $('#dataTableFinanceiroDesconto').dataTable(
                    {
                        processing: true,
                        serverSide: true,
                        ajax: {
                            url: __financeiroDescontoIndex.options.url.search,
                            type: "POST",
                            data: function (d) {
                                d['filter'] = d['filter'] || {};

                                d['filter'] = $.extend({}, d['filter'], __financeiroDescontoIndex.getFilterDatatables());
                                return d;
                            },
                            dataSrc: function (json) {
                                var data = json.data,
                                    permissoes = __financeiroDescontoIndex.options.value.arrPermissoes;
                                __financeiroDescontoIndex.bloqueiaBtnNovoDesconto();

                                if (Object.keys(permissoes).length > 0) {
                                    if (permissoes['alterarDesconto']) {
                                        var btnArr = [
                                            {
                                                class: ' btn-primary financeiroDesconto-edit',
                                                title: 'Editar',
                                                icon: 'fa-pencil'
                                            }
                                        ];
                                    }

                                    if (!permissoes['alterarDesconto']) {
                                        var btnArr = [];
                                    }

                                } else {
                                    var btnArr = [
                                        {
                                            class: ' btn-primary financeiroDesconto-edit',
                                            title: 'Editar',
                                            icon: 'fa-pencil'
                                        }
                                    ];
                                }

                                $('.descontoPercentual').html(json.descontoPercentual);

                                if (__financeiroDescontoIndex.options.readonly) {
                                    btnArr = [
                                        {
                                            class: ' btn-primary financeiroDesconto-view',
                                            title: 'Visualizar',
                                            icon: 'fa-eye'
                                        }
                                    ];
                                }

                                var btnGroup = __financeiroDescontoIndex.createBtnGroup(btnArr);

                                for (var row in data) {
                                    data[row]['acao'] = btnGroup;

                                    data[row]['desconto_valor_formatado'] = data[row]['desconto_valor_formatado'] ? data[row]['desconto_valor_formatado'] : '0,00';
                                    data[row]['desconto_percentual_formatado'] = data[row]['desconto_percentual_formatado'] ? data[row]['desconto_percentual_formatado'] : '0,00';

                                }

                                __financeiroDescontoIndex.removeOverlay($('#container-financeiro-desconto'));

                                return data;
                            }
                        },
                        error: function (xhr, error, thrown) {
                            __financeiroDescontoIndex.showNotificacaoWarning(
                                "Não foi possível efetuar a operação. Tente efetuar novamente a operação ou recarregue a página."
                            );
                            __financeiroDescontoIndex.removeOverlay($('#container-financeiro-desconto'));
                        },
                        columnDefs: [
                            {name: "desconto_id", targets: colNum++, data: "desconto_id", className: "text-right"},
                            {name: "desctipo_descricao", targets: colNum++, data: "desctipo_descricao"},
                            {name: "pes_nome", targets: colNum++, data: "pes_nome"},
                            {name: "usuario_login", targets: colNum++, data: "usuario_login"},
                            {
                                name: "desconto_valor",
                                targets: colNum++,
                                data: "desconto_valor_formatado",
                                className: "text-right"
                            },
                            {
                                name: "desconto_percentual",
                                targets: colNum++,
                                data: "desconto_percentual_formatado",
                                className: "text-right"
                            },
                            {
                                name: "quantidade_titulos_vinculados", targets: colNum++,
                                data: "quantidade_titulos_vinculados", className: "text-right"
                            },
                            {name: "desconto_status", targets: colNum++, data: "desconto_status"},
                            {name: "desconto_id", targets: colNum++, data: "acao"}
                        ],
                        "dom": "fr" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                        "autoWidth": true,
                        oLanguage: {
                            sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                            sLengthMenu: "Mostrar _MENU_ registros por página",
                            sZeroRecords: "Nenhum registro encontrado",
                            sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                            sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                            sInfoFiltered: "(filtrado de _MAX_ registros)",
                            sSearch: "Procurar: ",
                            oPaginate: {
                                sFirst: "Início",
                                sPrevious: "Anterior",
                                sNext: "Próximo",
                                sLast: "Último"
                            }
                        },
                        order: [[0, 'desc']]
                    });

            $('#dataTableFinanceiroDesconto').on('click', '.financeiroDesconto-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __financeiroDescontoIndex.options.datatables.financeiroDesconto.fnGetData($pai);

                if (!__financeiroDescontoIndex.options.ajax) {
                    location.href = __financeiroDescontoIndex.options.url.edit + '/' + data['desconto_id'];
                } else {
                    __financeiroDescontoIndex.addOverlay(
                        $('#container-financeiro-desconto'), 'Aguarde, carregando dados para edição...'
                    );
                    $.financeiroDescontoAdd().pesquisaFinanceiroDesconto(
                        data['desconto_id'],
                        function (data) {
                            __financeiroDescontoIndex.showModal(data);
                            if (data['titulosVinculados']) {
                                for (var row in data['titulosVinculados']) {
                                    if (data['titulosVinculados'][row]['descontoTituloSituacao'] == 'Pendente') {
                                        $("#btn-financeiro-desconto-deferir").removeClass("hidden");
                                    }
                                }
                            }
                        }
                    );
                }
            });

            $('#dataTableFinanceiroDesconto').on('click', '.financeiroDesconto-view', function (event) {
                var $pai = $(this).closest('tr');
                var data = __financeiroDescontoIndex.options.datatables.financeiroDesconto.fnGetData($pai);
                var $container = $('#container-financeiro-desconto');

                if ($['financeiroDescontoView']) {
                    __financeiroDescontoIndex.addOverlay(
                        $container, 'Aguarde, carregando dados para visualizaçao...'
                    );

                    $.financeiroDescontoView().pesquisaFinanceiroDesconto(
                        data['desconto_id'],
                        function (arrDados) {
                            if (arrDados) {
                                $('#financeiro-desconto-view-modal').modal('show');
                            }

                            __financeiroDescontoIndex.removeOverlay($container);
                        }
                    );
                }
            });

            $('#btn-financeiro-desconto-add').click(function (e) {
                if(__financeiroDescontoIndex.getBloqueaBtnNovoDesconto()){
                    this.bloqueiaBtnNovoDesconto([{'mensagem': "Essa pessoa não é um aluno!"}]);
                    return false;
                }

                $("#dataTableTitulosVinculados_wrapper").find('input[type=search]').val('').trigger($.Event('keypress', {
                    keyCode: $.ui.keyCode.ENTER,
                    which: $.ui.keyCode.ENTER
                }));

                if (__financeiroDescontoIndex.options.ajax) {
                    __financeiroDescontoIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });
        };

        this.bloqueiaBtnNovoDesconto = function (param) {
            if (__financeiroDescontoIndex.getBloqueaBtnNovoDesconto()) {
                $("#btn-financeiro-desconto-add").addClass("hidden");

                if (param) {
                    if (param['mensagem']) {
                        __financeiroDescontoIndex.showNotificacaoWarning(param['mensagem']);
                    }
                }

                return true

            } else {
                $("#btn-financeiro-desconto-add").removeClass("hidden");
            }
        };

        this.showModal = function (data) {
            var mensagem = {};
            mensagem['mensagem'] = "Essa pessoa não é um aluno!";

            if (__financeiroDescontoIndex.bloqueiaBtnNovoDesconto(mensagem)) {
                return false;
            }

            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#financeiro-desconto-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['descontoId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['descontoId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);
            var validator = $form.validate();
            if (data.length > 0) {
                var desctipoPercmax = parseFloat(data['desctipo']['desctipoPercmax'] || '0'),
                    desctipoValormax = parseFloat(data['desctipo']['desctipoValormax'] || '0');
            }

            validator.resetForm();
            $form.find(".has-error").removeClass("has-error");
            $form[0].reset();
            $form.deserialize(data);

            $("#desctipo").select2('data', data['desctipo']).trigger("change");
            $("#descontoPercentual").val(data['descontoPercentual'] || '').trigger("change");
            $("#descontoValor").val(data['descontoValor'] || '').trigger("change");
            $("#descontoStatus").select2('val', data['descontoStatus'] || 'Ativo').trigger("change");
            $("#aluno").select2('data', data['aluno'] || __financeiroDescontoIndex.getAluno()).trigger("change");
            $("#aluno").select2('enable');
            $("#desctipo").select2('enable');
            $.financeiroDescontoAdd().setTitulosVinculados(data['titulosVinculados'] || []);

            $('#financeiro-desconto-modal .modal-title:first').html(actionTitle + ' desconto');
            $('#financeiro-desconto-modal').modal('show');
            __financeiroDescontoIndex.removeOverlay($('#container-financeiro-desconto'));
            $("#financeiro-desconto-wizard a[href=#abaDadosBasicos]").trigger("click");

            if (Object.keys(data['titulosVinculados'] || []).length > 0) {
                $("#aluno").select2('disable');
                $("#desctipo").select2('disable');
            }

            if ($("#aluno").val()) {
                $("#aluno").select2('disable');
            }

            $("#btn-financeiro-desconto-deferir").addClass("hidden");

            if (data['descontoStatus'] == 'Inativo') {
                $("#descontoStatus").select2('disable');
            }

        };

        this.reloadDataTableFinanceiroDesconto = function () {
            this.getDataTableFinanceiroDesconto().api().ajax.reload(null, false);
        };

        this.setFilterDatatables = function (arrFilters) {
            __financeiroDescontoIndex.options.data.arrFilters = arrFilters || {};
            __financeiroDescontoIndex.reloadDataTableFinanceiroDesconto();
        };

        this.getFilterDatatables = function () {
            var arrFilters = __financeiroDescontoIndex.options.data.arrFilters || {};
            arrFilters['painel'] = __financeiroDescontoIndex.options.value.painel || false;
            arrFilters['alunoPer'] = __financeiroDescontoIndex.getAlunoPerId();

            return arrFilters;
        };

        this.getDataTableFinanceiroDesconto = function () {
            if (!__financeiroDescontoIndex.options.datatables.financeiroDesconto) {
                if (!$.fn.dataTable.isDataTable('#dataTableFinanceiroDesconto')) {
                    __financeiroDescontoIndex.options.datatables.financeiroDesconto =
                        $('#dataTableFinanceiroDesconto').DataTable();
                } else {
                    __financeiroDescontoIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __financeiroDescontoIndex.options.datatables.financeiroDesconto;
        };

        this.setAlunoPerId = function (alunoPerId) {
            this.options.value.alunoPerId = alunoPerId || null;
        };

        this.getAlunoPerId = function () {
            return this.options.value.alunoPerId || null;
        };

        this.setAluno = function (aluno) {
            this.options.value.aluno = aluno || null;
        };

        this.getAluno = function () {
            var aluno = this.options.value.aluno || " ";

            if (aluno['alunoId'] || aluno['aluno_id']) {
                aluno['id'] = aluno['alunoId'] || aluno['aluno_id'];
                aluno['text'] = aluno['pesNome'];

                return aluno
            }

        };

        this.setBloqueaBtnNovoDesconto = function (bloqueaBtnNovoDesconto) {
            this.options.value.bloqueaBtnNovoDesconto = bloqueaBtnNovoDesconto || false;
        };

        this.getBloqueaBtnNovoDesconto = function () {
            return this.options.value.bloqueaBtnNovoDesconto;

        };

        this.run = function (opts) {
            __financeiroDescontoIndex.setDefaults(opts);
            __financeiroDescontoIndex.setSteps();
        };
    };

    $.financeiroDescontoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.financeiro-desconto.index");

        if (!obj) {
            obj = new FinanceiroDescontoIndex();
            obj.run(params);
            $(window).data('universa.financeiro.financeiro-desconto.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);