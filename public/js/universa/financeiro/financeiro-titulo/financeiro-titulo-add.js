(function ($, window, document) {
    'use strict';

    var FinanceiroTituloAdd = function () {
        VersaShared.call(this);
        var __financeiroTituloAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                pessoa: '',
                financeiroTituloTipo: '',
                acessoPessoas: '',
                financeiroTitulo: ''
            },
            data: {},
            value: {
                pes: null,
                tipotitulo: null,
                usuarioBaixa: null,
                usuarioAutor: null,
                tituloNovo: null
            },
            datatables: {},
            wizardElement: '#financeiro-titulo-wizard',
            formElement: '#financeiro-titulo-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#pes").select2({
                language: 'pt-BR',
                ajax: {
                    url: __financeiroTituloAdd.options.url.pessoa,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.pes_id, id: el.pes_id};
                        });

                        return {results: transformed};
                    }
                }
            });

            if (__financeiroTituloAdd.getPes()) {
                $('#pes').select2("data", __financeiroTituloAdd.getPes());
            }
            $("#tipotitulo").select2({
                language: 'pt-BR',
                ajax: {
                    url: __financeiroTituloAdd.options.url.financeiroTituloTipo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.tipotitulo_id, id: el.tipotitulo_id};
                        });

                        return {results: transformed};
                    }
                }
            });

            if (__financeiroTituloAdd.getTipotitulo()) {
                $('#tipotitulo').select2("data", __financeiroTituloAdd.getTipotitulo());
            }
            $("#usuarioBaixa").select2({
                language: 'pt-BR',
                ajax: {
                    url: __financeiroTituloAdd.options.url.acessoPessoas,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.id, id: el.id};
                        });

                        return {results: transformed};
                    }
                }
            });

            if (__financeiroTituloAdd.getUsuarioBaixa()) {
                $('#usuarioBaixa').select2("data", __financeiroTituloAdd.getUsuarioBaixa());
            }
            $("#usuarioAutor").select2({
                language: 'pt-BR',
                ajax: {
                    url: __financeiroTituloAdd.options.url.acessoPessoas,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.id, id: el.id};
                        });

                        return {results: transformed};
                    }
                }
            });

            if (__financeiroTituloAdd.getUsuarioAutor()) {
                $('#usuarioAutor').select2("data", __financeiroTituloAdd.getUsuarioAutor());
            }
            $("#tituloNovo").select2({
                language: 'pt-BR',
                ajax: {
                    url: __financeiroTituloAdd.options.url.financeiroTitulo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.titulo_id, id: el.titulo_id};
                        });

                        return {results: transformed};
                    }
                }
            });

            if (__financeiroTituloAdd.getTituloNovo()) {
                $('#tituloNovo').select2("data", __financeiroTituloAdd.getTituloNovo());
            }
            $("#tituloParcela").inputmask({showMaskOnHover: false, mask: ['9{0,9}'], rightAlign: true});
            $("#tituloDataProcessamento")
                .datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                })
                .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});
            $("#tituloDataVencimento")
                .datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                })
                .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});
            $("#tituloTipoPagamento").select2({language: 'pt-BR'}).trigger("change");
            $("#tituloValor").inputmask({showMaskOnHover: false, mask: ['9{0,9}'], rightAlign: true});
            $("#tituloDataPagamento")
                .datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                })
                .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});
            $("#tituloMulta").inputmask({showMaskOnHover: false, mask: ['9{0,9}'], rightAlign: true});
            $("#tituloJuros").inputmask({showMaskOnHover: false, mask: ['9{0,9}'], rightAlign: true});
            $("#tituloEstado").select2({language: 'pt-BR'}).trigger("change");
            $("#tituloValorPago").inputmask({showMaskOnHover: false, mask: ['9{0,9}'], rightAlign: true});
            $("#tituloValorPagoDinheiro").inputmask({showMaskOnHover: false, mask: ['9{0,9}'], rightAlign: true});
            $("#tituloDesconto").inputmask({showMaskOnHover: false, mask: ['9{0,9}'], rightAlign: true});
            $("#tituloDescontoManual").inputmask({showMaskOnHover: false, mask: ['9{0,9}'], rightAlign: true});
            $("#tituloAcrescimoManual").inputmask({showMaskOnHover: false, mask: ['9{0,9}'], rightAlign: true});
        };

        this.setPes = function (pes) {
            this.options.value.pes = pes || null;
        };

        this.getPes = function () {
            return this.options.value.pes || null;
        };

        this.setTipotitulo = function (tipotitulo) {
            this.options.value.tipotitulo = tipotitulo || null;
        };

        this.getTipotitulo = function () {
            return this.options.value.tipotitulo || null;
        };

        this.setUsuarioBaixa = function (usuarioBaixa) {
            this.options.value.usuarioBaixa = usuarioBaixa || null;
        };

        this.getUsuarioBaixa = function () {
            return this.options.value.usuarioBaixa || null;
        };

        this.setUsuarioAutor = function (usuarioAutor) {
            this.options.value.usuarioAutor = usuarioAutor || null;
        };

        this.getUsuarioAutor = function () {
            return this.options.value.usuarioAutor || null;
        };

        this.setTituloNovo = function (tituloNovo) {
            this.options.value.tituloNovo = tituloNovo || null;
        };

        this.getTituloNovo = function () {
            return this.options.value.tituloNovo || null;
        };
        this.setValidations = function () {
            __financeiroTituloAdd.options.validator.settings.rules = {
                pes: {maxlength: 11, number: true, required: true},
                tipotitulo: {maxlength: 11, number: true, required: true}, usuarioBaixa: {maxlength: 11, number: true},
                usuarioAutor: {maxlength: 11, number: true}, tituloNovo: {maxlength: 11, number: true},
                tituloDescricao: {maxlength: 255, required: true},
                tituloParcela: {maxlength: 4, number: true, required: true},
                tituloDataProcessamento: {required: true, dateBR: true},
                tituloDataVencimento: {required: true, dateBR: true}, tituloTipoPagamento: {maxlength: 6},
                tituloValor: {maxlength: 10, number: true, required: true}, tituloDataPagamento: {dateBR: true},
                tituloMulta: {maxlength: 10, number: true}, tituloJuros: {maxlength: 10, number: true},
                tituloEstado: {maxlength: 4}, tituloValorPago: {maxlength: 10, number: true},
                tituloValorPagoDinheiro: {maxlength: 10, number: true},
                tituloDesconto: {maxlength: 10, number: true},
                tituloDescontoManual: {maxlength: 10, number: true},
                tituloAcrescimoManual: {maxlength: 10, number: true},
            };
            __financeiroTituloAdd.options.validator.settings.messages = {
                pes: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!', required: 'Campo obrigatório!'},
                tipotitulo: {
                    maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!', required: 'Campo obrigatório!'
                }, usuarioBaixa: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!'},
                usuarioAutor: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!'},
                tituloNovo: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!'},
                tituloDescricao: {maxlength: 'Tamanho máximo: 255!', required: 'Campo obrigatório!'}, tituloParcela: {
                    maxlength: 'Tamanho máximo: 4!', number: 'Número inválido!', required: 'Campo obrigatório!'
                }, tituloDataProcessamento: {required: 'Campo obrigatório!', dateBR: 'Informe uma data válida!'},
                tituloDataVencimento: {required: 'Campo obrigatório!', dateBR: 'Informe uma data válida!'},
                tituloTipoPagamento: {maxlength: 'Tamanho máximo: 6!'}, tituloValor: {
                    maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!', required: 'Campo obrigatório!'
                }, tituloDataPagamento: {dateBR: 'Informe uma data válida!'},
                tituloMulta: {maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!'},
                tituloJuros: {maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!'},
                tituloEstado: {maxlength: 'Tamanho máximo: 4!'},
                tituloValorPago: {maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!'},
                tituloValorPagoDinheiro: {maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!'},
                tituloDesconto: {maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!'},
                tituloDescontoManual: {maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!'},
                tituloAcrescimoManual: {maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!'},
            };

            $(__financeiroTituloAdd.options.formElement).submit(function (e) {
                var ok = $(__financeiroTituloAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__financeiroTituloAdd.options.formElement);

                if (__financeiroTituloAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __financeiroTituloAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __financeiroTituloAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__financeiroTituloAdd.options.listagem) {
                                    $.financeiroTituloIndex().reloadDataTableFinanceiroTitulo();
                                    __financeiroTituloAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#financeiro-titulo-modal').modal('hide');
                                }

                                __financeiroTituloAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaFinanceiroTitulo = function (titulo_id, callback) {
            var $form = $(__financeiroTituloAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __financeiroTituloAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + titulo_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __financeiroTituloAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __financeiroTituloAdd.removeOverlay($form);
                },
                erro: function () {
                    __financeiroTituloAdd.showNotificacaoDanger('Erro desconhecido!');
                    __financeiroTituloAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.financeiroTituloAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.financeiro-titulo.add");

        if (!obj) {
            obj = new FinanceiroTituloAdd();
            obj.run(params);
            $(window).data('universa.financeiro.financeiro-titulo.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);