(function ($, window, document) {
    'use strict';
    var FinanceiroTituloIndex = function () {
        VersaShared.call(this);
        var __financeiroTituloIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                financeiroTitulo: null
            },
        };

        this.setSteps = function () {
            var colNum = 0;
            __financeiroTituloIndex.options.datatables.financeiroTitulo = $('#dataTableFinanceiroTitulo').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __financeiroTituloIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary financeiroTitulo-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger financeiroTitulo-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                        }

                        __financeiroTituloIndex.removeOverlay($('#container-financeiro-titulo'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "titulo_id", targets: colNum++, data: "titulo_id"},
                    {name: "pes_nome", targets: colNum++, data: "pes_nome"},
                    {name: "tipotitulo_nome", targets: colNum++, data: "tipotitulo_nome"},
                    {name: "usuario_baixa_login", targets: colNum++, data: "usuario_baixa_login"},
                    {name: "usuario_autor_login", targets: colNum++, data: "usuario_autor_login"},
                    {name: "titulo_descricao", targets: colNum++, data: "titulo_descricao"},
                    {name: "titulo_parcela", targets: colNum++, data: "titulo_parcela"},
                    {name: "titulo_data_processamento", targets: colNum++, data: "titulo_data_processamento"},
                    {name: "titulo_data_vencimento", targets: colNum++, data: "titulo_data_vencimento"},
                    {name: "titulo_valor", targets: colNum++, data: "titulo_valor"},
                    {name: "titulo_estado", targets: colNum++, data: "titulo_estado"},
                    {name: "titulo_id", targets: colNum++, data: "acao"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $('#dataTableFinanceiroTitulo').on('click', '.financeiroTitulo-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __financeiroTituloIndex.options.datatables.financeiroTitulo.fnGetData($pai);

                if (!__financeiroTituloIndex.options.ajax) {
                    location.href = __financeiroTituloIndex.options.url.edit + '/' + data['titulo_id'];
                } else {
                    $.financeiroTituloAdd().pesquisaFinanceiroTitulo(
                        data['titulo_id'],
                        function (data) { __financeiroTituloIndex.showModal(data); }
                    );
                }
            });


            $('#btn-financeiro-titulo-add').click(function (e) {
                if (__financeiroTituloIndex.options.ajax) {
                    __financeiroTituloIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#dataTableFinanceiroTitulo').on('click', '.financeiroTitulo-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __financeiroTituloIndex.options.datatables.financeiroTitulo.fnGetData($pai);
                var arrRemover = {
                    tituloId: data['titulo_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de título "' + data['titulo_desconto_manual'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __financeiroTituloIndex.addOverlay(
                                $('#container-financeiro-titulo'),
                                'Aguarde, solicitando remoção de registro de título...'
                            );
                            $.ajax({
                                url: __financeiroTituloIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __financeiroTituloIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de título:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __financeiroTituloIndex.removeOverlay($('#container-financeiro-titulo'));
                                    } else {
                                        __financeiroTituloIndex.reloadDataTableFinanceiroTitulo();
                                        __financeiroTituloIndex.showNotificacaoSuccess(
                                            "Registro de título removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#financeiro-titulo-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['tituloId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['tituloId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);

            $("#pes").select2('data', data['pes']).trigger("change");
            $("#tipotitulo").select2('data', data['tipotitulo']).trigger("change");
            $("#usuarioBaixa").select2('data', data['usuarioBaixa']).trigger("change");
            $("#usuarioAutor").select2('data', data['usuarioAutor']).trigger("change");
            $("#tituloNovo").select2('data', data['tituloNovo']).trigger("change");
            $("#tituloTipoPagamento").select2('data', data['tituloTipoPagamento']).trigger("change");
            $("#tituloEstado").select2('data', data['tituloEstado']).trigger("change");

            $('#financeiro-titulo-modal .modal-title').html(actionTitle + ' título');
            $('#financeiro-titulo-modal').modal();
        };

        this.reloadDataTableFinanceiroTitulo = function () {
            this.getDataTableFinanceiroTitulo().api().ajax.reload(null, false);
        };

        this.getDataTableFinanceiroTitulo = function () {
            if (!__financeiroTituloIndex.options.datatables.financeiroTitulo) {
                if (!$.fn.dataTable.isDataTable('#dataTableFinanceiroTitulo')) {
                    __financeiroTituloIndex.options.datatables.financeiroTitulo =
                        $('#dataTableFinanceiroTitulo').DataTable();
                } else {
                    __financeiroTituloIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __financeiroTituloIndex.options.datatables.financeiroTitulo;
        };

        this.run = function (opts) {
            __financeiroTituloIndex.setDefaults(opts);
            __financeiroTituloIndex.setSteps();
        };
    };

    $.financeiroTituloIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.financeiro-titulo.index");

        if (!obj) {
            obj = new FinanceiroTituloIndex();
            obj.run(params);
            $(window).data('universa.financeiro.financeiro-titulo.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);