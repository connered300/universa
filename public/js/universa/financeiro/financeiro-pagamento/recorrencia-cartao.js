(function ($, window, document) {
    'use strict';

    var recorrenciaCartao = function () {
        VersaShared.call(this);
        var __recorrenciaCartao = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                pagamento: '',
                validaCartao: ''
            },
            data: {
                arrDataTable: []
            },
            value: {},
            dataTables: {
                tableRecorrencia: null
            },
            formElement: '#recorrencia-cartao-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            var btnPagamentoForma = $("#pagamentoForma"),
                pagamentoCartao = $(".pagamentoCartao"),
                colNum = 0;

            __recorrenciaCartao.options.dataTables.tableRecorrencia = $("#tableRecorrencia").DataTable({
                "sDom": "<'dt-toolbar-header'<'col-sm-4'l><'col-sm-4'r><'col-sm-4'f>>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                autoWidth: true,
                data: JSON.parse('{"draw":"1", "recordsTotal": 0, "recordsFiltered": 0, "data": []}'),
                columnDefs: [
                    {name: 'action', targets: colNum++, data: 'action', orderable: false, searchable: false},
                    {name: 'matricula', targets: colNum++, data: 'matricula'},
                    {name: 'tituloId', targets: colNum++, data: 'tituloId'},
                    {name: 'tituloDescricao', targets: colNum++, data: 'tituloDescricao'},
                    {name: 'tituloValor', targets: colNum++, data: 'tituloValor'}
                ],
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[3, 'asc']]
            });

            var arrDataTable = __recorrenciaCartao.options.data.arrDataTable["data"];

            if (JSON.stringify(arrDataTable) != '[]') {
                var checked = 'checked',
                    btnChecked = "\
                    <label class='checkbox'>\
                    <input type='checkbox' class='checkbox-inline' " + checked + " name='checkboxBoleto[]'>\
                    <i></i></label>";

                $.each(arrDataTable, function (position, data) {
                    var dados = data;
                    dados['action'] = btnChecked;

                    __recorrenciaCartao.options.dataTables.tableRecorrencia.row.add(dados);

                });

                __recorrenciaCartao.options.dataTables.tableRecorrencia.draw();
            }

            var arrPagamentoForma = [{'id': 'Boleto', 'text': 'Boleto'}, {'id': 'Cartão', 'text': 'Cartão'}];
            btnPagamentoForma.select2({data: arrPagamentoForma});
            btnPagamentoForma.select2('val', 'Boleto').trigger("change");
            $("input[name='checkboxBoleto[]']").addClass("hidden");

            btnPagamentoForma.on("change", function (e) {

                pagamentoCartao.addClass('hidden');
                $("input[name='checkboxBoleto[]']").addClass('hidden');

                if (btnPagamentoForma.val() == 'Cartão') {

                    pagamentoCartao.removeClass('hidden');
                    $("input[name='checkboxBoleto[]']").removeClass('hidden');

                    new Card({
                        // a selector or DOM element for the form where users will
                        // be entering their information
                        form: '#recorrencia-cartao-form', // *required*
                        // a selector or DOM element for the container
                        // where you want the card to appear
                        container: '.exibeCartao', // *required*

                        formSelectors: {
                            numberInput: '#cartaoNumero', // optional — default input[name="number"]
                            expiryInput: '#cartaoVencimento', // optional — default input[name="expiry"]
                            cvcInput: '#cartaoCvv', // optional — default input[name="cvc"]
                            nameInput: '#cartaoNomeTitular' // optional - defaults input[name="name"]
                        },

                        width: 300, // optional — default 350px
                        formatting: true, // optional - default true

                        // Strings for translation - optional
                        messages: {
                            validDate: 'valid\ndate', // optional - default 'valid\nthru'
                            monthYear: 'mm/yyyy' // optional - default 'month/year'
                        },

                        // Default placeholders for rendered fields - optional
                        placeholders: {
                            number: '•••• •••• •••• ••••',
                            name: 'Full Name',
                            expiry: '••/••',
                            cvc: '•••'
                        },

                        masks: {
                            cardNumber: '•' // optional - mask card number
                        },

                        // if true, will log helpful messages for setting up Card
                        debug: false // optional - default false
                    });

                }
            });
        };

        this.setValidations = function () {

            __recorrenciaCartao.options.validator.settings.rules = {
                pagamentoForma: {required: true}
            };
            __recorrenciaCartao.options.validator.settings.messages = {
                pagamentoForma: {required: 'Campo obrigatório!'}
            };

            $(__recorrenciaCartao.options.formElement).submit(function (e) {
                var ok = $(__recorrenciaCartao.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__recorrenciaCartao.options.formElement),
                    dados = $form.serializeJSON(),
                    arrTitulos = [];
                dados['ajax'] = true;

                __recorrenciaCartao.addOverlay($(".widget-body"));

                $("input[name='checkboxBoleto[]']:checked").each(function () {
                    var row = $(this).parents('tr');
                    arrTitulos.push(__recorrenciaCartao.options.dataTables.tableRecorrencia.row(row).data().tituloId);
                });

                dados['titulos'] = arrTitulos;

                setTimeout(function () {
                    $.ajax({
                        url: __recorrenciaCartao.options.url.pagamento,
                        type: 'POST',
                        dataType: 'json',
                        data: dados,
                        success: function (data) {
                            if (data['erro']) {
                                __recorrenciaCartao.showNotificacaoInfo((data['erro']['erro']));
                            } else if (data['rotaSaida']) {
                                window.location.href = data['rotaSaida'];
                            }
                            __recorrenciaCartao.removeOverlay($(".widget-body"));
                        }
                    })

                }, 500);

                e.preventDefault();
                e.stopImmediatePropagation(true);
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.recorrenciaCartao = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.financeiro-pagamento.recorrencia-cartao");

        if (!obj) {
            obj = new recorrenciaCartao();
            obj.run(params);
            $(window).data("universa.financeiro.financeiro-pagamento.recorrencia-cartao");
        }

        return obj;
    };
})(window.jQuery, window, document);