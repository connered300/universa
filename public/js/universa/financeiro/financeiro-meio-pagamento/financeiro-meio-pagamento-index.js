(function ($, window, document) {
    'use strict';
    var FinanceiroMeioPagamentoIndex = function () {
        VersaShared.call(this);
        var __financeiroMeioPagamentoIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                financeiroMeioPagamento: null
            },
        };

        this.setSteps = function () {
            var colNum = 0;
            __financeiroMeioPagamentoIndex.options.datatables.financeiroMeioPagamento =
                $('#dataTableFinanceiroMeioPagamento').dataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: __financeiroMeioPagamentoIndex.options.url.search,
                        type: "POST",
                        dataSrc: function (json) {
                            var data = json.data;
                            var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary financeiroMeioPagamento-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger financeiroMeioPagamento-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                            for (var row in data) {
                                data[row]['acao'] = btnGroup;
                            }

                            __financeiroMeioPagamentoIndex.removeOverlay($('#container-financeiro-meio-pagamento'));

                            return data;
                        }
                    },
                    columnDefs: [
                        {name: "meio_pagamento_id", targets: colNum++, data: "meio_pagamento_id"},
                        {name: "meio_pagamento_descricao", targets: colNum++, data: "meio_pagamento_descricao"},
                        {name: "meio_pagamento_tipo", targets: colNum++, data: "meio_pagamento_tipo"},
                        {name: "meio_pagamento_uso_externo", targets: colNum++, data: "meio_pagamento_uso_externo"},
                        {name: "meio_pagamento_uso_multiplo", targets: colNum++, data: "meio_pagamento_uso_multiplo"},
                        {name: "meio_pagamento_status", targets: colNum++, data: "meio_pagamento_status"},
                        {name: "meio_pagamento_id", targets: colNum++, data: "acao"}
                    ],
                    "dom": "fr" +
                           "t" +
                           "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                    "autoWidth": true,
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "Procurar: ",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    },
                    order: [[0, 'asc']]
                });

            $('#dataTableFinanceiroMeioPagamento').on('click', '.financeiroMeioPagamento-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __financeiroMeioPagamentoIndex.options.datatables.financeiroMeioPagamento.fnGetData($pai);

                if (!__financeiroMeioPagamentoIndex.options.ajax) {
                    location.href = __financeiroMeioPagamentoIndex.options.url.edit + '/' + data['meio_pagamento_id'];
                } else {
                    $.financeiroMeioPagamentoAdd().pesquisaFinanceiroMeioPagamento(
                        data['meio_pagamento_id'],
                        function (data) { __financeiroMeioPagamentoIndex.showModal(data); }
                    );
                }
            });


            $('#btn-financeiro-meio-pagamento-add').click(function (e) {
                if (__financeiroMeioPagamentoIndex.options.ajax) {
                    __financeiroMeioPagamentoIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#dataTableFinanceiroMeioPagamento').on('click', '.financeiroMeioPagamento-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __financeiroMeioPagamentoIndex.options.datatables.financeiroMeioPagamento.fnGetData($pai);
                var arrRemover = {
                    meioPagamentoId: data['meio_pagamento_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de meio de pagamento "' +
                             data['meio_pagamento_descricao'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __financeiroMeioPagamentoIndex.addOverlay(
                                $('#container-financeiro-meio-pagamento'),
                                'Aguarde, solicitando remoção de registro de meio de pagamento...'
                            );
                            $.ajax({
                                url: __financeiroMeioPagamentoIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __financeiroMeioPagamentoIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de meio de pagamento:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __financeiroMeioPagamentoIndex.removeOverlay($('#container-financeiro-meio-pagamento'));
                                    } else {
                                        __financeiroMeioPagamentoIndex.reloadDataTableFinanceiroMeioPagamento();
                                        __financeiroMeioPagamentoIndex.showNotificacaoSuccess(
                                            "Registro de meio de pagamento removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#financeiro-meio-pagamento-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['meioPagamentoId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['meioPagamentoId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);

            $("#meioPagamentoTipo").select2('val', data['meioPagamentoTipo']).trigger("change");
            $("#meioPagamentoUsoExterno").select2('val', data['meioPagamentoUsoExterno']).trigger("change");
            $("#meioPagamentoUsoMultiplo").select2('val', data['meioPagamentoUsoMultiplo']).trigger("change");
            $("#meioPagamentoCamposCheque").select2('val', data['meioPagamentoCamposCheque']).trigger("change");
            $("#meioPagamentoStatus").select2('val', data['meioPagamentoStatus']).trigger("change");

            $('#financeiro-meio-pagamento-modal .modal-title').html(actionTitle + ' meio de pagamento');
            $('#financeiro-meio-pagamento-modal').modal();
        };

        this.reloadDataTableFinanceiroMeioPagamento = function () {
            this.getDataTableFinanceiroMeioPagamento().api().ajax.reload(null, false);
        };

        this.getDataTableFinanceiroMeioPagamento = function () {
            if (!__financeiroMeioPagamentoIndex.options.datatables.financeiroMeioPagamento) {
                if (!$.fn.dataTable.isDataTable('#dataTableFinanceiroMeioPagamento')) {
                    __financeiroMeioPagamentoIndex.options.datatables.financeiroMeioPagamento =
                        $('#dataTableFinanceiroMeioPagamento').DataTable();
                } else {
                    __financeiroMeioPagamentoIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __financeiroMeioPagamentoIndex.options.datatables.financeiroMeioPagamento;
        };

        this.run = function (opts) {
            __financeiroMeioPagamentoIndex.setDefaults(opts);
            __financeiroMeioPagamentoIndex.setSteps();
        };
    };

    $.financeiroMeioPagamentoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.financeiro-meio-pagamento.index");

        if (!obj) {
            obj = new FinanceiroMeioPagamentoIndex();
            obj.run(params);
            $(window).data('universa.financeiro.financeiro-meio-pagamento.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);