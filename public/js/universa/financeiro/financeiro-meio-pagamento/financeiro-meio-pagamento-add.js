(function ($, window, document) {
    'use strict';

    var FinanceiroMeioPagamentoAdd = function () {
        VersaShared.call(this);
        var __financeiroMeioPagamentoAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {},
            data: {},
            value: {},
            datatables: {},
            wizardElement: '#financeiro-meio-pagamento-wizard',
            formElement: '#financeiro-meio-pagamento-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#meioPagamentoTipo,#meioPagamentoUsoExterno,#meioPagamentoStatus,#meioPagamentoUsoMultiplo,#meioPagamentoCamposCheque")
                .select2({allowClear: true, language: 'pt-BR'})
                .trigger("change");
        };

        this.setValidations = function () {
            __financeiroMeioPagamentoAdd.options.validator.settings.rules = {
                meioPagamentoDescricao: {maxlength: 45, required: true},
                meioPagamentoTipo: {maxlength: 6, required: true},
                meioPagamentoUsoExterno: {maxlength: 3, required: true},
                meioPagamentoStatus: {maxlength: 7, required: true},
                meioPagamentoUsoMultiplo: {maxlength: 3, required: true},
                meioPagamentoCamposCheque: {maxlength: 3, required: true}
            };
            __financeiroMeioPagamentoAdd.options.validator.settings.messages = {
                meioPagamentoDescricao: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                meioPagamentoTipo: {maxlength: 'Tamanho máximo: 6!', required: 'Campo obrigatório!'},
                meioPagamentoUsoExterno: {maxlength: 'Tamanho máximo: 3!', required: 'Campo obrigatório!'},
                meioPagamentoStatus: {maxlength: 'Tamanho máximo: 7!', required: 'Campo obrigatório!'},
                meioPagamentoUsoMultiplo: {maxlength: 'Tamanho máximo: 3!', required: 'Campo obrigatório!'},
                meioPagamentoCamposCheque: {maxlength: 'Tamanho máximo: 3!', required: 'Campo obrigatório!'}
            };

            $(__financeiroMeioPagamentoAdd.options.formElement).submit(function (e) {
                var ok = $(__financeiroMeioPagamentoAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__financeiroMeioPagamentoAdd.options.formElement);

                if (__financeiroMeioPagamentoAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __financeiroMeioPagamentoAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __financeiroMeioPagamentoAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__financeiroMeioPagamentoAdd.options.listagem) {
                                    $.financeiroMeioPagamentoIndex().reloadDataTableFinanceiroMeioPagamento();
                                    __financeiroMeioPagamentoAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#financeiro-meio-pagamento-modal').modal('hide');
                                }

                                __financeiroMeioPagamentoAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaFinanceiroMeioPagamento = function (meio_pagamento_id, callback) {
            var $form = $(__financeiroMeioPagamentoAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __financeiroMeioPagamentoAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + meio_pagamento_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __financeiroMeioPagamentoAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __financeiroMeioPagamentoAdd.removeOverlay($form);
                },
                erro: function () {
                    __financeiroMeioPagamentoAdd.showNotificacaoDanger('Erro desconhecido!');
                    __financeiroMeioPagamentoAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.financeiroMeioPagamentoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.financeiro-meio-pagamento.add");

        if (!obj) {
            obj = new FinanceiroMeioPagamentoAdd();
            obj.run(params);
            $(window).data('universa.financeiro.financeiro-meio-pagamento.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);