(function ($, window, document) {
    'use strict';

    var FinanceiroTituloConfigAdd = function () {
        VersaShared.call(this);
        var __financeiroTituloConfigAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                campusCurso: '',
                acadperiodoLetivo: '',
                financeiroTituloTipo: '',
                boletoConfConta: ''
            },
            data: {
                titutloconfInstrucaoAutomaticaSelect2: {},
                constDescontoIncentivoAcumulativoSim: null,
                arrTituloconfDescontoIncentivoAcumulativo: []
            },
            value: {
                cursocampus: null,
                per: null,
                tipotitulo: null,
                confcont: null,
                tituloconfDescontoIncentivoAcumulativo: null,
                vencimentoFixo: null,
                vencimentoAlteravel: null
            },
            datatables: {},
            wizardElement: '#financeiro-titulo-config-wizard',
            formElement: '#financeiro-titulo-config-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {

            var $tituloconfDataFim = $("#tituloconfDataFim"),
                $tituloconfDataInicio = $("#tituloconfDataInicio"),
                $tituloconfDatas = $("#tituloconfDataInicio, #tituloconfDataFim"),
                $cursocampus = $("#cursocampus"),
                $confcont = $("#confcont"),
                $tipotitulo = $("#tipotitulo"),
                $per = $("#per"),
                $tituloconfigIntrucaoBoletoAutomatica = $('#tituloconfigInstrucaoAutomatico'),
                $tituloconfDescontoIncentivoAcumulativo = $('#tituloconfDescontoIncentivoAcumulativo'),
                $tituloconfVencimentoFixo = $('#tituloconfVencimentoFixo'),
                $tituloconfVencimentoAlteravel = $('#tituloconfVencimentoAlteravel');

            $tituloconfVencimentoFixo.select2();

            if (__financeiroTituloConfigAdd.options.value.vencimentoFixo) {
                $tituloconfVencimentoFixo.select2('val', __financeiroTituloConfigAdd.options.value.vencimentoFixo);
            }

            $tituloconfVencimentoAlteravel.select2();

            if (__financeiroTituloConfigAdd.options.value.vencimentoAlteravel) {
                $tituloconfVencimentoAlteravel.select2('val', __financeiroTituloConfigAdd.options.value.vencimentoAlteravel);
            }

            $tituloconfDescontoIncentivoAcumulativo.select2({
                data: __financeiroTituloConfigAdd.options.data.arrTituloconfDescontoIncentivoAcumulativo || []
            });

            $tituloconfDescontoIncentivoAcumulativo.select2('val', (
                __financeiroTituloConfigAdd.options.data.constDescontoIncentivoAcumulativoSim ||
                __financeiroTituloConfigAdd.options.value.tituloconfDescontoIncentivoAcumulativo ||
                null
            ));

            $cursocampus.select2({
                language: 'pt-BR',
                allowClear: true,
                ajax: {
                    url: __financeiroTituloConfigAdd.options.url.campusCurso,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.camp_nome + ' / ' + el.curso_nome, id: el.cursocampus_id};
                        });

                        return {results: transformed};
                    }
                }
            });

            if (__financeiroTituloConfigAdd.getCursocampus()) {
                $cursocampus.select2("data", __financeiroTituloConfigAdd.getCursocampus());
            }

            $tituloconfigIntrucaoBoletoAutomatica.select2({data: __financeiroTituloConfigAdd.options.data.titutloconfInstrucaoAutomaticaSelect2});

            $per.select2({
                language: 'pt-BR',
                allowClear: true,
                ajax: {
                    url: __financeiroTituloConfigAdd.options.url.acadperiodoLetivo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.per_nome, id: el.per_id};
                        });

                        return {results: transformed};
                    }
                }
            }).change(function () {
                if (this.value != '') {
                    $tituloconfDatas.prop('disabled', true).val('');
                } else {
                    $tituloconfDatas.prop('disabled', false);
                }
            });

            if (__financeiroTituloConfigAdd.getPer()) {
                $per.select2("data", __financeiroTituloConfigAdd.getPer());
            }

            $tipotitulo.select2({
                language: 'pt-BR',
                allowClear: true,
                ajax: {
                    url: __financeiroTituloConfigAdd.options.url.financeiroTituloTipo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.tipotitulo_id;
                            el.text = el.tipotitulo_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            if (__financeiroTituloConfigAdd.getTipotitulo()) {
                $tipotitulo.select2("data", __financeiroTituloConfigAdd.getTipotitulo());
            }

            $confcont.select2({
                language: 'pt-BR',
                allowClear: true,
                ajax: {
                    url: __financeiroTituloConfigAdd.options.url.boletoConfConta,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.confcont_id;
                            el.text = el.conta;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            if (__financeiroTituloConfigAdd.getConfcont()) {
                $confcont.select2("data", __financeiroTituloConfigAdd.getConfcont());
            }

            $("#tituloconfDiaVenc, #tituloconfDiaJuros").inputmask({
                showMaskOnHover: false, mask: ['9{0,9}'], rightAlign: true
            });

            $("#tituloconfDiaDesc, #tituloconfDiaDesc2").inputmask({
                showMaskOnHover: false, alias: 'numeric', rightAlign: true,
                allowPlus: true, decimalProtect: true, min: -31, max: 31
            });

            $("#tituloconfValorDesc, #tituloconfValordesc2").inputmask({
                showMaskOnHover: false, alias: 'currency', prefix: '', groupSeparator: ''
            });
            $("#tituloconfPercentDesc, #tituloconfPercentDesc2").inputmask({
                showMaskOnHover: false, alias: 'currency', prefix: '', groupSeparator: '', digits: 4
            });
            $("#tituloconfMulta").inputmask({
                showMaskOnHover: false, alias: 'currency', prefix: '', groupSeparator: '', digits: 4
            });
            $("#tituloconfJuros").inputmask({
                showMaskOnHover: false, alias: 'currency', prefix: '', groupSeparator: '', digits: 4
            });
            $tituloconfDataInicio
                .datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                })
                .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});
            $tituloconfDataFim
                .datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                })
                .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});

            $tituloconfDatas.change(function () {
                if ($tituloconfDataInicio.val() != '' || $tituloconfDataFim.val() != '') {
                    $per.select2('disable');
                    $per.val('');
                } else {
                    $per.select2('enable');
                }

                $per.trigger('change');
            });
        };

        this.setCursocampus = function (cursocampus) {
            this.options.value.cursocampus = cursocampus || null;
        };

        this.getCursocampus = function () {
            return this.options.value.cursocampus || null;
        };

        this.setPer = function (per) {
            this.options.value.per = per || null;
        };

        this.getPer = function () {
            return this.options.value.per || null;
        };

        this.setTipotitulo = function (tipotitulo) {
            this.options.value.tipotitulo = tipotitulo || null;
        };

        this.getTipotitulo = function () {
            return this.options.value.tipotitulo || null;
        };

        this.setConfcont = function (confcont) {
            this.options.value.confcont = confcont || null;
        };

        this.getConfcont = function () {
            return this.options.value.confcont || null;
        };

        this.setValidations = function () {
            __financeiroTituloConfigAdd.options.validator.settings.rules = {
                cursocampus: {maxlength: 10, number: true}, per: {maxlength: 10, number: true},
                tipotitulo: {number: true}, confcont: {number: true, required: true},
                tituloconfDiaVenc: {maxlength: 2, number: true, required: true, min: 1, max: 31},
                tituloconfDiaDesc: {maxlength: 3, number: true, min: -31, max: 31},
                tituloconfDiaDesc2: {maxlength: 3, number: true, min: -31, max: 31},
                tituloconfValorDesc: {number: true},
                tituloconfValordesc2: {number: true},
                tituloconfPercentDesc: {number: true, min: 0, max: 100},
                tituloconfPercentDesc2: {number: true, min: 0, max: 100},
                tituloconfMulta: {number: true, min: 0, max: 100},
                tituloconfJuros: {number: true, min: 0, max: 100},
                tituloconfDataInicio: {dateBR: true},
                tituloconfDataFim: {dateBR: true},
                tituloconfDescontoIncentivoAcumulativo: {maxlength: 3, required: true},
                tituloconfNome: {required: true},
            };
            __financeiroTituloConfigAdd.options.validator.settings.messages = {
                cursocampus: {maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!'},
                per: {maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!'},
                tipotitulo: {number: 'Número inválido!'},
                confcont: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                tituloconfDiaVenc: {
                    maxlength: 'Tamanho máximo: 2 Dígitos!', number: 'Número inválido!', required: 'Campo obrigatório!'
                },
                tituloconfDiaDesc: {
                    maxlength: 'Tamanho máximo: 2 Dígitos!', number: 'Número inválido!', required: 'Campo obrigatório!'
                },
                tituloconfDiaDesc2: {
                    maxlength: 'Tamanho máximo: 2 Dígitos!', number: 'Número inválido!', required: 'Campo obrigatório!'
                },
                tituloconfValorDesc: {number: 'Número inválido!'},
                tituloconfValordesc2: {number: 'Número inválido!'},
                tituloconfPercentDesc: {number: 'Número inválido! Ou excede o valor máximo!'},
                tituloconfPercentDesc2: {number: 'Número inválido! Ou excede o valor máximo!'},
                tituloconfMulta: {number: 'Número inválido! Ou excede o valor máximo!'},
                tituloconfJuros: {number: 'Número inválido! Ou excede o valor máximo!'},
                tituloconfDataInicio: {dateBR: 'Informe uma data válida!'},
                tituloconfDataFim: {dateBR: 'Informe uma data válida!'},
                tituloconfDescontoIncentivoAcumulativo: {
                    maxlength: 'Tamanho máximo: 3!',
                    required: 'Campo obrigatório!'
                },
                tituloconfNome: {required: 'Campo obrigatório!'}
            };

            $(__financeiroTituloConfigAdd.options.formElement).submit(function (e) {
                var ok = $(__financeiroTituloConfigAdd.options.formElement).valid();

                if (
                    $('#tituloconfPercentDesc').val() > 100 ||
                    $('#tituloconfMulta').val() > 100 ||
                    $('#tituloconfJuros').val() > 100 ||
                    $('#tituloconfPercentDesc2').val() > 100
                ) {
                    __financeiroTituloConfigAdd.showNotificacaoWarning(
                        'A porcetagem não pode ser maior que 100%' + '&nbsp;' + '!'
                    )
                    ok = false;
                }

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                $('#tituloconfDataInicio, #tituloconfDataFim, #per').prop('disabled', false);
                var $form = $(__financeiroTituloConfigAdd.options.formElement);

                if (parseFloat($("#tituloconfValorDesc").val()) > 0 && parseFloat($("#tituloconfPercentDesc").val()) > 0) {
                    __financeiroTituloConfigAdd.showNotificacaoWarning('Adicione Valor ou Percentual para desconto de incentivo!');
                    return false;
                }

                if (parseFloat($("#tituloconfValordesc2").val()) > 0 && parseFloat($("#tituloconfPercentDesc2").val()) > 0) {
                    __financeiroTituloConfigAdd.showNotificacaoWarning('Adicione Valor ou Percentual para o segundo desconto de incentivo!');
                    return false;
                }


                if (parseInt($("#tituloconfDiaDesc").val()) <= parseInt($("#tituloconfDiaDesc2").val()) &&
                    ( $("#tituloconfValordesc2").val() > 0 || $("#tituloconfPercentDesc2").val() > 0)
                ) {
                    __financeiroTituloConfigAdd.showNotificacaoWarning(
                        'O número de dias de antecipação da segunda configuração deve ser menor a da primeira!'
                    );
                    return false;
                }

                if (__financeiroTituloConfigAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __financeiroTituloConfigAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __financeiroTituloConfigAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__financeiroTituloConfigAdd.options.listagem) {
                                    $.financeiroTituloConfigIndex().reloadDataTableFinanceiroTituloConfig();
                                    __financeiroTituloConfigAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#financeiro-titulo-config-modal').modal('hide');
                                }

                                __financeiroTituloConfigAdd.removeOverlay($form);
                                $('#tituloconfDataInicio, #tituloconfDataFim, #per').trigger('change');
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaFinanceiroTituloConfig = function (tituloconf_id, callback) {
            var $form = $(__financeiroTituloConfigAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __financeiroTituloConfigAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + tituloconf_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __financeiroTituloConfigAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __financeiroTituloConfigAdd.removeOverlay($form);
                },
                erro: function () {
                    __financeiroTituloConfigAdd.showNotificacaoDanger('Erro desconhecido!');
                    __financeiroTituloConfigAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.financeiroTituloConfigAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.financeiro-titulo-config.add");

        if (!obj) {
            obj = new FinanceiroTituloConfigAdd();
            obj.run(params);
            $(window).data('universa.financeiro.financeiro-titulo-config.add', obj);
        }

        return obj;
    };
})
(window.jQuery, window, document);