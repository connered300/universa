(function ($, window, document) {
    'use strict';
    var FinanceiroTituloConfigIndex = function () {
        VersaShared.call(this);
        var __financeiroTituloConfigIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            data: {},
            datatables: {
                financeiroTituloConfig: null
            }
        };

        this.setSteps = function () {
            var colNum = 0;
            __financeiroTituloConfigIndex.options.datatables.financeiroTituloConfig =
                $('#dataTableFinanceiroTituloConfig').dataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: __financeiroTituloConfigIndex.options.url.search,
                        type: "POST",
                        dataSrc: function (json) {
                            var data = json.data;
                            var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary financeiroTituloConfig-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger financeiroTituloConfig-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                            for (var row in data) {
                                data[row]['acao'] = btnGroup;
                            }

                            __financeiroTituloConfigIndex.removeOverlay($('#container-financeiro-titulo-config'));
                            return data;
                        }
                    },
                    columnDefs: [
                        {name: "tituloconf_id", targets: colNum++, data: "tituloconf_id"},

                        {name: "tituloconf_nome", targets: colNum++, data: "tituloconf_nome"},

                        {name: "campus", targets: colNum++, data: "campus"},
                        {name: "tipotitulo_nome", targets: colNum++, data: "tipotitulo_nome"},
                        {name: "per_nome", targets: colNum++, data: "per_nome"},
                        {name: "tituloconf_data_inicio", targets: colNum++, data: "tituloconf_data_inicio"},
                        {name: "tituloconf_data_fim", targets: colNum++, data: "tituloconf_data_fim"},
                        {name: "tituloconf_id", targets: colNum++, data: "acao"}
                    ],
                    "dom": "fr" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                    "autoWidth": true,
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "Procurar: ",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    },
                    order: [[0, 'asc']]
                });

            $('#dataTableFinanceiroTituloConfig').on('click', '.financeiroTituloConfig-edit', function (event) {

                var $pai = $(this).closest('tr');
                var data = __financeiroTituloConfigIndex.options.datatables.financeiroTituloConfig.fnGetData($pai);

                if (!__financeiroTituloConfigIndex.options.ajax) {
                    location.href = __financeiroTituloConfigIndex.options.url.edit + '/' + data['tituloconf_id'];
                } else {
                    __financeiroTituloConfigIndex.addOverlay(
                        $('#container-financeiro-titulo-config'), 'Aguarde, carregando dados para edição...'
                    );
                    $.financeiroTituloConfigAdd().pesquisaFinanceiroTituloConfig(
                        data['tituloconf_id'],
                        function (data) {
                            __financeiroTituloConfigIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-financeiro-titulo-config-add').click(function (e) {
                if (__financeiroTituloConfigIndex.options.ajax) {
                    __financeiroTituloConfigIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#dataTableFinanceiroTituloConfig').on('click', '.financeiroTituloConfig-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __financeiroTituloConfigIndex.options.datatables.financeiroTituloConfig.fnGetData($pai);
                var arrRemover = {
                    tituloconfId: data['tituloconf_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de configuração de título "' +
                    data['tituloconf_percent_desc'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __financeiroTituloConfigIndex.addOverlay(
                                $('#container-financeiro-titulo-config'),
                                'Aguarde, solicitando remoção de registro de configuração de título...'
                            );
                            $.ajax({
                                url: __financeiroTituloConfigIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __financeiroTituloConfigIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de configuração de título:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __financeiroTituloConfigIndex.removeOverlay($('#container-financeiro-titulo-config'));
                                    } else {
                                        __financeiroTituloConfigIndex.reloadDataTableFinanceiroTituloConfig();
                                        __financeiroTituloConfigIndex.showNotificacaoSuccess(
                                            "Registro de configuração de título removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#financeiro-titulo-config-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['tituloconfId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['tituloconfId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);

            /*deserialize : Não aceita paremetros null*/
            data['tituloconfDiaDesc'] = !data['tituloconfDiaDesc'] ? 0 : data['tituloconfDiaDesc'];
            data['tituloconfPercentDesc'] = !data['tituloconfPercentDesc'] ? 0 : data['tituloconfPercentDesc'];
            data['tituloconfValordesc'] = !data['tituloconfValordesc'] ? 0 : data['tituloconfValordesc'];

            data['tituloconfDiaDesc2'] = !data['tituloconfDiaDesc2'] ? 0 : data['tituloconfDiaDesc2'];
            data['tituloconfPercentDesc2'] = !data['tituloconfPercentDesc2'] ? 0 : data['tituloconfPercentDesc2'];
            data['tituloconfValordesc2'] = !data['tituloconfValordesc2'] ? 0 : data['tituloconfValordesc2'];

            $form[0].reset();
            $form.deserialize(data);

            $("#tituloconfigInstrucaoAutomatico").val(data['titutloconfInstrucaoAutomatica']).trigger("change");
            $("#tituloconfDescontoIncentivoAcumulativo").val(data['tituloconfDescontoIncentivoAcumulativo']).trigger("change");
            $("#tituloconfVencimentoAlteravel").val(data['tituloconfVencimentoAlteravel'] || 'Sim').trigger("change");
            $("#tituloconfVencimentoFixo").val(data['tituloconfVencimentoFixo'] || 'Sim').trigger("change");
            $("#cursocampus").select2('data', data['cursocampus']).trigger("change");
            $("#per").select2('data', data['per']);
            $("#tipotitulo").select2('data', data['tipotitulo']).trigger("change");
            $("#confcont").select2('data', data['confcont']).trigger("change");
            $('#tituloconfDataInicio, #tituloconfDataFim, #per').trigger('change');

            $('#financeiro-titulo-config-modal').find('.modal-title').html(actionTitle + ' configuração de título');
            $('#financeiro-titulo-config-modal').modal();
            __financeiroTituloConfigIndex.removeOverlay($('#container-financeiro-titulo-config'));
        };

        this.reloadDataTableFinanceiroTituloConfig = function () {
            this.getDataTableFinanceiroTituloConfig().api().ajax.reload(null, false);
        };

        this.getDataTableFinanceiroTituloConfig = function () {
            if (!__financeiroTituloConfigIndex.options.datatables.financeiroTituloConfig) {
                if (!$.fn.dataTable.isDataTable('#dataTableFinanceiroTituloConfig')) {
                    __financeiroTituloConfigIndex.options.datatables.financeiroTituloConfig =
                        $('#dataTableFinanceiroTituloConfig').DataTable();
                } else {
                    __financeiroTituloConfigIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __financeiroTituloConfigIndex.options.datatables.financeiroTituloConfig;
        };

        this.run = function (opts) {
            __financeiroTituloConfigIndex.setDefaults(opts);
            __financeiroTituloConfigIndex.setSteps();
        };
    };

    $.financeiroTituloConfigIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.financeiro-titulo-config.index");

        if (!obj) {
            obj = new FinanceiroTituloConfigIndex();
            obj.run(params);
            $(window).data('universa.financeiro.financeiro-titulo-config.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);