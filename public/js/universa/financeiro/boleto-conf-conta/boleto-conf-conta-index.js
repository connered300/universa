(function ($, window, document) {
    'use strict';
    var BoletoConfContaIndex = function () {
        VersaShared.call(this);
        var __boletoConfContaIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                boletoConfConta: null
            }
        };

        this.setSteps = function () {
            var colNum = -1;
            __boletoConfContaIndex.options.datatables.boletoConfConta = $('#dataTableBoletoConfConta').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __boletoConfContaIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary boletoConfConta-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger boletoConfConta-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                        }

                        __boletoConfContaIndex.removeOverlay($('#container-boleto-conf-conta'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "confcont_id", targets: ++colNum, data: "confcont_id"},
                    {name: "pes_nome", targets: ++colNum, data: "pes_nome"},
                    {name: "banc_id", targets: ++colNum, data: "banc_nome"},
                    {name: "confcont_agencia", targets: ++colNum, data: "confcont_agencia"},
                    {name: "confcont_conta", targets: ++colNum, data: "confcont_conta"},
                    {name: "confcont_convenio", targets: ++colNum, data: "confcont_convenio"},
                    {name: "confcont_id", targets: ++colNum, data: "acao"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $('#dataTableBoletoConfConta').on('click', '.boletoConfConta-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __boletoConfContaIndex.options.datatables.boletoConfConta.fnGetData($pai);

                if (!__boletoConfContaIndex.options.ajax) {
                    location.href = __boletoConfContaIndex.options.url.edit + '/' + data['confcont_id'];
                } else {
                    $.boletoConfContaAdd().pesquisaBoletoConfConta(
                        data['confcont_id'],
                        function (data) { __boletoConfContaIndex.showModal(data); }
                    );
                }
            });


            $('#btn-boleto-conf-conta-add').click(function (e) {
                if (__boletoConfContaIndex.options.ajax) {
                    __boletoConfContaIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#dataTableBoletoConfConta').on('click', '.boletoConfConta-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __boletoConfContaIndex.options.datatables.boletoConfConta.fnGetData($pai);
                var arrRemover = {
                    confcontId: data['confcont_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de configuração de conta "' + data['pes_id'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __boletoConfContaIndex.addOverlay(
                                $('#container-boleto-conf-conta'),
                                'Aguarde, solicitando remoção de registro de configuração de conta...'
                            );
                            $.ajax({
                                url: __boletoConfContaIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __boletoConfContaIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de configuração de conta:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __boletoConfContaIndex.removeOverlay($('#container-boleto-conf-conta'));
                                    } else {
                                        __boletoConfContaIndex.reloadDataTableBoletoConfConta();
                                        __boletoConfContaIndex.showNotificacaoSuccess(
                                            "Registro de configuração de conta removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#boleto-conf-conta-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['confcontId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['confcontId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);

            $("#pes").select2('data', data['pes']).trigger("change");
            $("#banc").select2('data', data['banc']).trigger("change");

            $('#boleto-conf-conta-modal .modal-title').html(actionTitle + ' configuração de conta');
            $('#boleto-conf-conta-modal').modal();
        };

        this.reloadDataTableBoletoConfConta = function () {
            this.getDataTableBoletoConfConta().api().ajax.reload(null, false);
        };

        this.getDataTableBoletoConfConta = function () {
            if (!__boletoConfContaIndex.options.datatables.boletoConfConta) {
                if (!$.fn.dataTable.isDataTable('#dataTableBoletoConfConta')) {
                    __boletoConfContaIndex.options.datatables.boletoConfConta =
                        $('#dataTableBoletoConfConta').DataTable();
                } else {
                    __boletoConfContaIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __boletoConfContaIndex.options.datatables.boletoConfConta;
        };

        this.run = function (opts) {
            __boletoConfContaIndex.setDefaults(opts);
            __boletoConfContaIndex.setSteps();
        };
    };

    $.boletoConfContaIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.boleto-conf-conta.index");

        if (!obj) {
            obj = new BoletoConfContaIndex();
            obj.run(params);
            $(window).data('universa.financeiro.boleto-conf-conta.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);