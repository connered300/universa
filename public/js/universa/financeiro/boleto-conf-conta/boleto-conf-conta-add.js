(function ($, window, document) {
    'use strict';

    var BoletoConfContaAdd = function () {
        VersaShared.call(this);
        var __boletoConfContaAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                pessoa: '',
                boletoBanco: '',
                arquivo: ''
            },
            data: {},
            value: {
                pes: null,
                banc: null,
                arq: null
            },
            datatables: {},
            wizardElement: '#boleto-conf-conta-wizard',
            formElement: '#boleto-conf-conta-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#pes").select2({
                language: 'pt-BR',
                minimumInputLength: 1,
                ajax: {
                    url: __boletoConfContaAdd.options.url.pessoa,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query, pesquisaPFPJ: true};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.pes_nome, id: el.pes_id};
                        });

                        return {results: transformed};
                    }
                }
            });

            if (__boletoConfContaAdd.getPes()) {
                $('#pes').select2("data", __boletoConfContaAdd.getPes());
            }

            $("#banc").select2({
                language: 'pt-BR',
                ajax: {
                    url: __boletoConfContaAdd.options.url.boletoBanco,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.banc_nome, id: el.banc_id};
                        });

                        return {results: transformed};
                    }
                }
            });

            if (__boletoConfContaAdd.getBanc()) {
                $('#banc').select2("data", __boletoConfContaAdd.getBanc());
            }
            $("#confcontNossoNumeroInicial,#confcontRemessaSequencial").inputmask({showMaskOnHover: false, mask: ['9{0,9}'], rightAlign: true});
        };

        this.setPes = function (pes) {
            this.options.value.pes = pes || null;
        };

        this.getPes = function () {
            return this.options.value.pes || null;
        };

        this.setBanc = function (banc) {
            this.options.value.banc = banc || null;
        };

        this.getBanc = function () {
            return this.options.value.banc || null;
        };

        this.setValidations = function () {
            __boletoConfContaAdd.options.validator.settings.rules = {
                pes: {maxlength: 11, number: true, required: true}, banc: {maxlength: 11, number: true, required: true},
                arq: {maxlength: 10, number: true}, confcontAgencia: {maxlength: 45, required: true},
                confcontAgenciaDigito: {maxlength: 45, required: true}, confcontConta: {maxlength: 45, required: true},
                confcontContaDigito: {maxlength: 45}, confcontCarteira: {maxlength: 45},
                confcontVariacao: {maxlength: 45}, confcontConvenio: {maxlength: 45}, confcontContrato: {maxlength: 45},
                confcontNossoNumeroInicial: {maxlength: 11, number: true, required: true},
            };
            __boletoConfContaAdd.options.validator.settings.messages = {
                pes: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!', required: 'Campo obrigatório!'},
                banc: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!', required: 'Campo obrigatório!'},
                arq: {maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!'},
                confcontAgencia: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                confcontAgenciaDigito: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                confcontConta: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                confcontContaDigito: {maxlength: 'Tamanho máximo: 45!'},
                confcontCarteira: {maxlength: 'Tamanho máximo: 45!'},
                confcontVariacao: {maxlength: 'Tamanho máximo: 45!'},
                confcontConvenio: {maxlength: 'Tamanho máximo: 45!'},
                confcontContrato: {maxlength: 'Tamanho máximo: 45!'}, confcontNossoNumeroInicial: {
                    maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!', required: 'Campo obrigatório!'
                },
            };

            $(__boletoConfContaAdd.options.formElement).submit(function (e) {
                var ok = $(__boletoConfContaAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__boletoConfContaAdd.options.formElement);

                if (__boletoConfContaAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __boletoConfContaAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __boletoConfContaAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__boletoConfContaAdd.options.listagem) {
                                    $.boletoConfContaIndex().reloadDataTableBoletoConfConta();
                                    __boletoConfContaAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#boleto-conf-conta-modal').modal('hide');
                                }

                                __boletoConfContaAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaBoletoConfConta = function (confcont_id, callback) {
            var $form = $(__boletoConfContaAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __boletoConfContaAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + confcont_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __boletoConfContaAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __boletoConfContaAdd.removeOverlay($form);
                },
                erro: function () {
                    __boletoConfContaAdd.showNotificacaoDanger('Erro desconhecido!');
                    __boletoConfContaAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.boletoConfContaAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.boleto-conf-conta.add");

        if (!obj) {
            obj = new BoletoConfContaAdd();
            obj.run(params);
            $(window).data('universa.financeiro.boleto-conf-conta.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);