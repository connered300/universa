(function ($, window, document) {
    'use strict';
    var vs = new VersaShared();

    var defaults = {
        partial: false,
        url: {
            urlSearchBoleto: '',
            urlGeraRemessa: '',
            urlDownload: '',
            urlBuscaBoletos: ''
        },
        dataTables: {
            tableBoletos: '',
            tableBoletosSelected: ''
        },
        data: {
            boletosNotInclused: []
        },
        value: {
            dataAtual: ''
        }
    };

    var financeiroRemessaAdd = {
        options: defaults,
        setDefaults: function (opts) {
            opts = opts || [];


            $("#adiciona-todos-boletos").on('click', function () {


                var arrFiltros = {};
                arrFiltros.todosBoletosValidos = true;
                arrFiltros.boletoCarteira = $("#carteiraBoleto").val();
                arrFiltros.titulosValidosRemessa = true;
                arrFiltros.confContaId = $("#confConta").select2('val');
                arrFiltros.tituloEstado = $("#tituloEstado").select2('val');
                arrFiltros.reinclusao = $("#reinclusao").select2('val');
                arrFiltros.dataPross = {
                    dataInicial: $("#dataProcessInicial").val(),
                    dataFinal: $("#dataProcessFinal").val()
                };
                arrFiltros.dataVenc = {
                    dataInicial: $("#dataVencInicial").val(),
                    dataFinal: $("#dataVencFinal").val()
                };
                arrFiltros.gerarRemessaTodosTitulos = true;

                $.SmartMessageBox({
                    title: "Deseja gerar o arquivo para todos boletos disponíveis?",
                    content: 'Atenção: Estes boletos não poderão ser adicionados a outro arquivo de remessa!',
                    buttons: "[Cancelar][Ok]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Ok") {
                        $.ajax({
                            type: 'POST',
                            url: financeiroRemessaAdd.options.url.urlGeraRemessa,
                            data: arrFiltros,
                            beforeSend: function () {
                                financeiroRemessaAdd.addOverlay(
                                    $("#remessa-add-wizard"), "Aguarde, gerando o arquivo de remessa..."
                                );
                            },
                            success: function (r) {
                                $.financeiroRemessaIndex().refreshDataTables();
                                if (r.chaveDownload) {
                                    var url = '';
                                    var msg = [];

                                    if (Object.keys(r['chaveDownload']).length > 0) {
                                        $.each(r['chaveDownload'], function (i, item) {
                                            url = financeiroRemessaAdd.options.url.urlDownload + "/" + item['chave'];
                                            msg.push('Para baixar o arquivo ' + item['nome'] + ' clique <a target="_blank" href="' + url + '">aqui</a>.');
                                        });
                                    } else {
                                        url = financeiroRemessaAdd.options.url.urlDownload + "/" + r.chaveDownload;
                                        msg.push('Para baixar o arquivo clique <a target="_blank" href="' + url + '">aqui</a>.');
                                    }

                                    financeiroRemessaAdd.showNotificacao({
                                        title: "Arquivo gerado com sucesso: ",
                                        content: (
                                            'Um total de ' + r.inseridos.length + ' Boletos foram incluidos no arquivo.<br>' +
                                            msg.join('<br>')
                                        ),
                                        type: 'success'
                                    });

                                    financeiroRemessaAdd.options.data.boletosNotInclused = [];
                                    financeiroRemessaAdd.options.dataTables.tableBoletosSelected.clear().draw();
                                } else {
                                    var content = '<br>';
                                    for (var row in r.valid) {
                                        content += r.valid[row] + '<br>';
                                    }
                                    financeiroRemessaAdd.showNotificacao({
                                        title: "O arquivo não foi gerado!",
                                        content: 'Ocorreram erros na geração de arquivo.' + content,
                                        type: 'warning'
                                    });
                                }

                                if (r.errors.length > 0) {
                                    var content = "";

                                    for (var bol in r.errors) {
                                        content += "<ul>";
                                        content += "<li> Nosso Número:" + bol;
                                        content += "<ul>";
                                        for (var message in r.errors[bol]) {
                                            content += "<li>" + r.errors[bol][message] + "</li>";
                                        }
                                        content += "</ul>";
                                        content += "</li>";
                                        content += "</ul>";
                                    }

                                    financeiroRemessaAdd.showNotificacao({
                                        title: "Boletos não inclusos: ",
                                        content: content,
                                        type: 'danger',
                                    });
                                }
                            },
                            complete: function () {
                                $("#remessa-add-wizard").bootstrapWizard('show', 0);
                                financeiroRemessaAdd.removeOverlay($("#remessa-add-wizard"));
                            }
                        });
                    }
                });
            });

            var arrCarteiraBoleto = [
                {'id': '1', 'text': 'Registrado'},
                {'id': '2', 'text': 'Não Registrado'}
            ];
            $("#carteiraBoleto").select2(
                {
                    multiple: true,
                    allowClear: true,
                    data: arrCarteiraBoleto
                });

            $("#carteiraBoleto").select2("val", 1);


            $.validator.setDefaults({
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    }
                    else if (element.parent('label').length) {
                        error.insertAfter(element.parent());
                    }
                    else {
                        error.insertAfter(element);
                    }
                }
            });

            financeiroRemessaAdd.options = $.extend(defaults, opts);
        },
        validator: $("#remessa-form").validate({
            submitHandler: function (form) {
                var $form = $(form);
                var cofirm = $form.data('confirmSend') || false;

                if (!confirm) {
                    $.SmartMessageBox({
                        title: "Confirme a operação!",
                        content: "Confirma a geração de um arquivo de remessa com os boletos selecionados?",
                        buttons: "[Não]['Sim']",
                    }, function (buttonPress) {
                        if (buttonPress == "Sim") {
                            $("#remessa-form").data('confirmSend', true);
                            $("#remessa-form").submit();
                        }
                    });

                    return false;
                }

                if (financeiroRemessaAdd.options.partial) {
                    financeiroRemessaAdd.generateRemessaFile($form.serializeJSON());
                } else {
                    $form.submit();
                }

                $("#remessa-form").data('confirmSend', false);
            },
            rules: {
                confConta: {required: true},
                tituloEstado: {required: true}
            }, messages: {
                confConta: {required: "Selecione a conta"},
                tituloEstado: {required: "Selecione a estado"}
            },
            ignore: '.ignore'
        }),
        wizard: {
            init: function () {
                $("#remessa-add-wizard").bootstrapWizard({
                    'tabClass': 'form-wizard',
                    'onNext': function (tab, navigation, nextTab) {
                        var currentTab = nextTab - 1;
                        var nextStep = financeiroRemessaAdd.steps["step" + nextTab];
                        var currentStep = financeiroRemessaAdd.steps["step" + currentTab];

                        if (nextTab) {
                            if (currentStep.hasOwnProperty('validate')) {
                                var valid = currentStep.validate();

                                if (valid) {
                                    financeiroRemessaAdd.steps["step" + nextTab].update();
                                }

                                return valid;
                            }
                        }
                    },
                    'onPrevious': function (tab, navigation, previousTab) {
                        var currentTab = previousTab + 1;
                        var previousStep = financeiroRemessaAdd.steps["step" + previousTab];
                        var currentStep = financeiroRemessaAdd.steps["step" + currentTab];

                        if (previousTab !== undefined) {
                            $.SmartMessageBox({
                                title: "Atenção!",
                                content: 'Todas as alterações serão perdidas.<br>\
                                          Deseja Continuar?',
                                buttons: "[Cancelar][Ok]"
                            }, function (ButtonPress) {
                                if (ButtonPress == "Ok") {
                                    $("#remessa-add-wizard").bootstrapWizard('show', previousTab);
                                }
                            });
                        }

                        return false;
                    },
                    'onTabClick': function (tab, navigation, nextTab) {
                        return false;
                    },
                });
            },
        },
        steps: {
            step0: {
                init: function () {
                    $("#confConta").select2({
                        language: 'pt-BR',
                        allowClear: true,
                        placeholder: 'Selecione a Conta',
                    });

                    $("#tituloEstado").select2({
                        language: 'pt-BR',
                        allowClear: true,
                        placeholder: 'Selecione o estado dos títulos',
                    });

                    $("#reinclusao").select2({
                        language: 'pt-BR',
                        allowClear: true,
                    });

                    $("#tituloEstado").select2('val', 'Aberto');
                    $("#reinclusao").select2('val', 'Não');
                    $("#reinclusao, #tituloEstado").select2('disable');

                    $("#dataProcessInicial").datepicker({
                        prevText: '<i class="fa fa-chevron-left"></i>',
                        nextText: '<i class="fa fa-chevron-right"></i>',
                        maxDate: new Date(),
                        onSelect: function (newDate, $datepicker) {
                            // atualizando o valor minimo da data final para o lancamento de atividades
                            // subtrai o mês por '-1' porque no javascript os meses começam em '0'
                            newDate = newDate.split('/');
                            $("#dataProcessFinal").datepicker(
                                'option', 'minDate', new Date(newDate[2], (newDate[1] - 1), newDate[0])
                            );
                        },
                    }).inputmask({
                        showMaskOnHover: false,
                        clearIncomplete: true,
                        mask: ['99/99/9999']
                    });

                    $("#dataProcessFinal").datepicker({
                        prevText: '<i class="fa fa-chevron-left"></i>',
                        nextText: '<i class="fa fa-chevron-right"></i>',
                        maxDate: new Date(),
                        onSelect: function (newDate, $datepicker) {
                            // atualizando o valor maximo da data inicial para o lancamento de atividades
                            // subtrai o mês por '-1' porque no javascript os meses começam em '0'
                            newDate = newDate.split('/');
                            $("#dataProcessInicial").datepicker(
                                'option', 'maxDate', new Date(newDate[2], (newDate[1] - 1), newDate[0])
                            );
                        },
                    }).inputmask({
                        showMaskOnHover: false,
                        clearIncomplete: true,
                        mask: ['99/99/9999']
                    });

                    $("#dataVencInicial").datepicker({
                        prevText: '<i class="fa fa-chevron-left"></i>',
                        nextText: '<i class="fa fa-chevron-right"></i>',
                        minDate: new Date(),
                        onSelect: function (newDate, $datepicker) {
                            // atualizando o valor minimo da data final para o lancamento de atividades
                            // subtrai o mês por '-1' porque no javascript os meses começam em '0'
                            newDate = newDate.split('/');
                            $("#dataVencFinal").datepicker(
                                'option', 'minDate', new Date(newDate[2], (newDate[1] - 1), newDate[0])
                            );
                        },
                    }).inputmask({
                        showMaskOnHover: false,
                        clearIncomplete: true,
                        mask: ['99/99/9999']
                    });

                    $("#dataVencFinal").datepicker({
                        prevText: '<i class="fa fa-chevron-left"></i>',
                        nextText: '<i class="fa fa-chevron-right"></i>',
                        minDate: new Date(),
                        onSelect: function (newDate, $datepicker) {
                            // atualizando o valor maximo da data inicial para o lancamento de atividades
                            // subtrai o mês por '-1' porque no javascript os meses começam em '0'
                            newDate = newDate.split('/');
                            $("#dataVencInicial").datepicker(
                                'option', 'maxDate', new Date(newDate[2], (newDate[1] - 1), newDate[0])
                            );
                        },
                    }).inputmask({
                        showMaskOnHover: false,
                        clearIncomplete: true,
                        mask: ['99/99/9999']
                    });
                },
                update: function () {

                },
                validate: function () {
                    var valid = true,
                        erroData = false;

                    if (!$("#confConta").val()) {
                        financeiroRemessaAdd.showNotificacao({
                            content: "É necessário selecionar uma configuração de conta antes de selecionar os boletos",
                            type: 'danger', timeout: 5000
                        });

                        valid = false;
                    }

                    if ($("#dataVencInicial").val()) {
                        var data = $("#dataVencInicial").val().split('/'),
                            dataAtual = financeiroRemessaAdd.options.value.dataAtual.split("/");

                        data = new Date(data[2], data[1] - 1, data[0]);
                        dataAtual = new Date(dataAtual[2], dataAtual[1] - 1, dataAtual[0]);

                        if (data < dataAtual) {
                            financeiroRemessaAdd.showNotificacao({
                                content: "Não é permitido adicionar boletos vencidos!",
                                type: 'warning', timeout: 5000
                            });

                            erroData = true;
                            valid = false;
                        }
                    }
                    if ($("#dataVencFinal").val() && !erroData) {
                        var data = $("#dataVencFinal").val().split('/'),
                            dataAtual = financeiroRemessaAdd.options.value.dataAtual.split("/");

                        data = new Date(data[2], data[1] - 1, data[0]);
                        dataAtual = new Date(dataAtual[2], dataAtual[1] - 1, dataAtual[0]);

                        if (data < dataAtual) {
                            financeiroRemessaAdd.showNotificacao({
                                content: "Não é permitido adicionar boletos vencidos!",
                                type: 'warning', timeout: 5000
                            });

                            valid = false;
                        }
                    }

                    return valid;
                }
            },
            step1: {
                init: function () {

                    var vs = new VersaShared();
                    var colNum = 0;
                    var updateOptionsBoletos = false;

                    vs.addOverlay($(".row"));

                    financeiroRemessaAdd.options.dataTables.tableBoletos = $("#tableBoletos").DataTable({
                        processing: true,
                        serverSide: true,
                        searching: true,
                        "sDom": "<'dt-toolbar'<'col-xs-6'f>lr>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-xs-6'i><'col-xs-6'p>>",
                        autoWidth: true,
                        ajax: {
                            "url": financeiroRemessaAdd.options.url.urlSearchBoleto,
                            "type": "POST",
                            data: function (d) {
                                d.boletoCarteira = $("#carteiraBoleto").val();
                                d.titulosValidosRemessa = true;
                                d.confContaId = $("#confConta").select2('val');
                                d.tituloEstado = $("#tituloEstado").select2('val');
                                d.reinclusao = $("#reinclusao").select2('val');
                                d.dataPross = {
                                    dataInicial: $("#dataProcessInicial").val(),
                                    dataFinal: $("#dataProcessFinal").val(),
                                };
                                d.dataVenc = {
                                    dataInicial: $("#dataVencInicial").val(),
                                    dataFinal: $("#dataVencFinal").val(),
                                };
                                d.boletosNotInclused = financeiroRemessaAdd.options.data.boletosNotInclused;
                                d.boletosNotInclused = d.boletosNotInclused.filter(function (value) {
                                    if (value) {
                                        return true;
                                    }
                                    return false;
                                });

                                return d;
                            },
                            dataSrc: function (r) {
                                var data = r.data;
                                var boletosNotInclused = financeiroRemessaAdd.options.data.boletosNotInclused;

                                for (var row in data) {
                                    var checked = "checked";

                                    if (boletosNotInclused[data[row]['bol_id']]) {
                                        checked = "";
                                    }

                                    data[row]['action'] = "\
                                        <label class='checkbox'>\
                                            <input type='checkbox' class='checkbox-inline' " + checked + " name='addBoleto[]'>\
                                            <i></i>\
                                        </label>";
                                }

                                return r.data;
                            },
                        },
                        columnDefs: [
                            {name: 'sacado', targets: colNum++, data: 'action', searchable: false, width: '10%'},
                            {name: 'nosso_numero', targets: colNum++, data: 'nosso_numero'},
                            {name: 'tipo_titulo', targets: colNum++, data: 'tipo_titulo'},
                            {name: 'sacado', targets: colNum++, data: 'sacado'},
                            {name: 'valor', targets: colNum++, data: 'valor'},
                            {name: 'data_processamento', targets: colNum++, data: 'data_processamento'},
                            {name: 'data_vencimento', targets: colNum++, data: 'data_vencimento'},
                            {name: 'titulo_estado', targets: colNum++, data: 'titulo_estado'},
                            {name: 'bol_id', targets: colNum++, data: 'bol_id', visible: false, searchable: false},
                        ],
                        oLanguage: {
                            sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                            sLengthMenu: "Mostrar _MENU_ registros por página",
                            sZeroRecords: "Nenhum registro encontrado",
                            sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                            sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                            sInfoFiltered: "(filtrado de _MAX_ registros)",
                            sSearch: "",
                            oPaginate: {
                                sFirst: "Início",
                                sPrevious: "Anterior",
                                sNext: "Próximo",
                                sLast: "Último"
                            }
                        }
                    });

                    colNum = 0;
                    financeiroRemessaAdd.options.dataTables.tableBoletosSelected = $("#tableBoletosSelected").DataTable({
                        processing: true,
                        autoWidth: true,
                        sDom: "<'dt-toolbar'<'col-xs-6'f>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-xs-6'i><'col-xs-6'p>>",
                        columnDefs: [
                            {name: 'nosso_numero', targets: colNum++, data: 'nosso_numero'},
                            {name: 'tipo_titulo', targets: colNum++, data: 'tipo_titulo'},
                            {name: 'sacado', targets: colNum++, data: 'sacado'},
                            {name: 'valor', targets: colNum++, data: 'valor'},
                            {name: 'data_processamento', targets: colNum++, data: 'data_processamento'},
                            {name: 'data_vencimento', targets: colNum++, data: 'data_vencimento'},
                            {name: 'action', targets: colNum++, data: 'action', searchable: false},
                            {name: 'bol_id', targets: colNum++, data: 'bol_id', visible: false, searchable: false},
                        ],
                        oLanguage: {
                            sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                            sLengthMenu: "Mostrar _MENU_ registros por página",
                            sZeroRecords: "Nenhum Boleto Selecionado",
                            sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                            sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                            sInfoFiltered: "(filtrado de _MAX_ registros)",
                            sSearch: "",
                            oPaginate: {
                                sFirst: "Início",
                                sPrevious: "Anterior",
                                sNext: "Próximo",
                                sLast: "Último"
                            }
                        }
                    });

                    $(document).on('click', '#btn-remessa-add', function () {

                        var erroCpfCnpj = '';
                        var erroEndereco = '';
                        var boletosVencidos = '';

                        /**************************************************************/
                        // percorre os boletos selecionados na listagem de boletos
                        $("input[name='addBoleto[]']:checked").each(function () {

                            var row = $(this).parents('tr');
                            var data = financeiroRemessaAdd.options.dataTables.tableBoletos.row(row).data();

                            if (parseInt(data['cpf_cnpj_invalido']) > 0) {
                                erroCpfCnpj = erroCpfCnpj.length > 0 ? erroCpfCnpj + ',' + data.nosso_numero : data.nosso_numero;
                            }

                            if (parseInt(data['endereco_invalido']) > 0) {
                                erroEndereco = erroEndereco.length > 0 ? erroEndereco + "," + data.nosso_numero : data.nosso_numero;
                            }
                            if (parseInt(data['boleto_vencido']) > 0) {
                                boletosVencidos = boletosVencidos.length > 0 ? boletosVencidos + "," + data.nosso_numero : data.nosso_numero;
                            }

                            if (parseInt(data['endereco_invalido']) == 0 && parseInt(data['cpf_cnpj_invalido']) == 0 &&
                                parseInt(data['boleto_vencido']) == 0) {
                                data.action = "\
                                <div class='text-center'>\
                                    <button type='button'\ class='btn btn-xs btn-danger boleto-remove'>\
                                        <i class='fa fa-times fa-inverse'></i>\
                                    </button>\
                                </div>";

                                // adiciona o boleto na tabela de boletos selecionados para gerar arquivo de remessa
                                financeiroRemessaAdd.options.dataTables.tableBoletosSelected.row.add(data).draw();
                                // adiciona o boleto para não buscar novamente
                                financeiroRemessaAdd.options.data.boletosNotInclused[data.bol_id] = data.bol_id;
                            }
                        });

                        if (erroCpfCnpj) {
                            vs.showNotificacaoWarning("Os boletos com Nosso Número: " + erroCpfCnpj + " possui CPF ou CNPJ inválido!\nEstes não serão adicionados na remessa!");
                        }
                        if (erroEndereco) {
                            vs.showNotificacaoWarning("Os boletos com Nosso Número: " + erroEndereco + " possui endereço inválido ou incompleto!\nEstes não serão adicionados na remessa!");
                        }
                        if (boletosVencidos) {
                            vs.showNotificacaoWarning("Os boletos com Nosso Número: " + boletosVencidos + " estão vencidos!\nEstes não serão adicionados na remessa!");
                        }

                        // se houver algum boleto selecinado atualiza a tabela de opções a selecionar
                        // e habilita botão de gerar arquivo de remessa e limpar boletos selecionados
                        if ($("input[name='addBoleto[]']:checked").length > 0) {
                            financeiroRemessaAdd.options.dataTables.tableBoletos.ajax.reload(null, false);
                            $("#gerar-remessa").prop('disabled', false);
                            $("#clear-remessa").prop('disabled', false);
                        }
                    });

                    // lógica para remover retirar boleto como selecionado para incluir no arquivo de remessa
                    $(document).on('click', '.boleto-remove', function () {
                        // seta para atualizar as opções de boletos
                        updateOptionsBoletos = true;

                        var row = $(this).parents('tr');
                        var boleto = financeiroRemessaAdd.options.dataTables.tableBoletosSelected.row(row).data();

                        delete financeiroRemessaAdd.options.data.boletosNotInclused[boleto.bol_id];
                        financeiroRemessaAdd.options.dataTables.tableBoletosSelected.row(row).remove().draw();
                    });

                    // ao sair do modal de boletos selecionados, atualiza as opções de boleto selecionado
                    $(document).on('hide.bs.modal', '#modal-add-remessa', function () {
                        // verifica se é necessário atualizar as opções de boleto
                        if (updateOptionsBoletos) {
                            updateOptionsBoletos = false;
                            financeiroRemessaAdd.options.dataTables.tableBoletos.ajax.reload(null, false);
                        }
                    });

                    $("#gerar-remessa").click(function () {
                        $.SmartMessageBox({
                            title: "Deseja gerar o arquivo para os boletos selecionados?",
                            content: 'Atenção: Os boletos selecionados não poderão ser adicionados a outro arquivo de remessa.',
                            buttons: "[Cancelar][Ok]"
                        }, function (ButtonPress) {
                            if (ButtonPress == "Ok") {
                                $.ajax({
                                    type: 'POST',
                                    url: financeiroRemessaAdd.options.url.urlGeraRemessa,
                                    data: {
                                        boletos: function () {
                                            var data = financeiroRemessaAdd.options.dataTables.tableBoletosSelected.data();

                                            var boletos = [];
                                            for (var boleto in data) {
                                                if (data[boleto].bol_id) {
                                                    boletos.push(data[boleto].bol_id);
                                                }
                                            }

                                            return boletos;
                                        },
                                        confContaId: $("#confConta").select2('val')
                                    },
                                    beforeSend: function () {
                                        financeiroRemessaAdd.addOverlay(
                                            $("#modal-add-remessa"), "Aguarde, gerando o arquivo de remessa..."
                                        );
                                    },
                                    success: function (r) {
                                        $.financeiroRemessaIndex().refreshDataTables();

                                        if (r.chaveDownload != "") {
                                            $.SmartMessageBox({
                                                title: "Deseja Baixar o arquivo Gerado?",
                                                buttons: "[Cancelar][OK]"
                                            }, function (BtnPress) {
                                                if (BtnPress == 'OK') {
                                                    if (r.chaveDownload.length > 0)
                                                        $.map(r.chaveDownload, function (a) {
                                                            var url = financeiroRemessaAdd.options.url.urlDownload + "/" + a.chave;
                                                            window.open(url, "_blank");
                                                        });
                                                } else {
                                                    var url = financeiroRemessaAdd.options.url.urlDownload + "/" + r.chaveDownload;
                                                    window.open(url, "_blank");
                                                }
                                            });

                                            financeiroRemessaAdd.showNotificacao({
                                                title: "Arquivo gerado com sucesso: ",
                                                content: 'Um total de ' + r.inseridos.length + ' Boletos foram incluidos no arquivo.',
                                                type: 'success'
                                            });

                                            financeiroRemessaAdd.options.data.boletosNotInclused = [];
                                            financeiroRemessaAdd.options.dataTables.tableBoletosSelected.clear().draw();
                                        }

                                        if (r.errors.length > 0) {
                                            var content = "";

                                            for (var bol in r.errors) {
                                                content += "<ul>";
                                                content += "<li> Nosso Número: " + bol;
                                                content += "<ul>";
                                                for (var message in r.errors[bol]) {
                                                    content += "<li>" + r.errors[bol][message] + "</li>";
                                                }
                                                content += "</ul>";
                                                content += "</li>";
                                                content += "</ul>";
                                            }

                                            financeiroRemessaAdd.showNotificacao({
                                                title: "Boletos não inclusos: ",
                                                content: content,
                                                type: 'danger'
                                            });
                                        }

                                        if (r.message.length > 0) {
                                            $.map(r.message, function (a) {
                                                financeiroRemessaAdd.showNotificacao({
                                                    title: "Atenção!",
                                                    content: a.message,
                                                    type: a.type
                                                });
                                            });
                                        }
                                    },
                                    complete: function () {
                                        financeiroRemessaAdd.removeOverlay($("#modal-add-remessa"));
                                    }
                                });
                            }
                        });
                    });

                    $("#clear-remessa").click(function () {
                        financeiroRemessaAdd.options.data.boletosNotInclused = [];
                        financeiroRemessaAdd.options.dataTables.tableBoletosSelected.clear().draw();

                        $("#gerar-remessa").prop('disabled', true);
                    });
                    vs.removeOverlay($(".row"));
                },
                update: function () {
                    if ($.isEmptyObject(financeiroRemessaAdd.options.dataTables.tableBoletos)) {
                        financeiroRemessaAdd.steps.step1.init();

                        return;
                    }

                    financeiroRemessaAdd.options.data.boletosNotInclused = [];
                    financeiroRemessaAdd.options.dataTables.tableBoletosSelected.clear().draw();
                    $("#gerar-remessa").prop('disabled', true);

                    //atualiza opções de boletos a selecionar
                    financeiroRemessaAdd.options.dataTables.tableBoletos.ajax.reload(null, false);
                },
                validate: function () {

                    return $("#remessa-form").valid();
                },
            },
        },
        showNotificacao: function (param) {
            var options = $.extend(true, {
                content: '', title: '', type: 'info', icon: '', color: '', timeout: false
            }, param);

            switch (options.type) {
                case 'danger':
                    options.icon = 'fa fa-times-circle';
                    options.title = options.title || 'Atenção:';
                    options.color = '#a90329';
                    break;
                case 'info':
                    options.icon = 'fa fa-info-circle';
                    options.title = options.title || 'Informação:';
                    options.color = '#57889c';
                    break;
                case 'success':
                    options.icon = 'fa fa-check-circle';
                    options.title = options.title || 'Sucesso:';
                    options.color = '#739e73';
                    break;
                case 'warning':
                    options.icon = 'fa fa-exclamation-circle';
                    options.title = options.title || 'Atenção:';
                    options.color = '#c79121';
                    break;
            }

            var optionsNotification = {
                title: options.title,
                content: "<i>" + options.content + "</i>",
                color: options.color,
                icon: options.icon
            };

            if (options.timeout) {
                optionsNotification.timeout = options.timeout;
            }

            $.smallBox(optionsNotification);
        },
        addOverlay: function (el, mensagem) {
            mensagem = mensagem || 'Aguarde...';
            el.append('<div class="loader-overlay">' + mensagem + '</div>');
        },
        removeOverlay: function (el) {
            el.find('.loader-overlay').remove();
        },
        run: function (opts) {
            financeiroRemessaAdd.setDefaults(opts);
            financeiroRemessaAdd.wizard.init();
            financeiroRemessaAdd.steps.step0.init();
        },
    };

    $.financeiroRemessaAdd = function (params) {
        params = params || [];

        var obj = $(window).data('universa.financeiro.remessa.add');

        if (!obj) {
            obj = financeiroRemessaAdd;
            obj.run(params);
            $(window).data('universa.financeiro.remessa.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);