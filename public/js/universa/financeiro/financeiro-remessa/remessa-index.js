(function ($, window, document) {
    'use strict';
    var FinanceiroRemessaIndex = function () {
        VersaShared.call(this);
        var __financeiroRemessaIndex = this;
        this.defaults = {
            url: {
                urlListRemessa: '',
                urlNewRemessa: '',
                urlDownFile: '',
                urlSearchBoleto: '',
                urlDownload: ''
            },
            dataTables: {
                tableRemessa: null,
                boletosTab: '',
                boletosRemovidosTab: '',
                boletosImpedimentosTab: ''

            },
            data: {
                boletosComImpedimento: [],
                boletosRemovidos: [],
                optsConfConta: null
            },
            value: {
                dataAtual: '',
                quantidadeMaximaBoletos: '',
                conta: null
            }
        };

        this.setSteps = function () {
            var $confConta = $("#confConta"),
                $tituloEstado = $("#tituloEstado"),
                $reinclusao = $("#reinclusao"),
                $dataProcessInicial = $("#dataProcessInicial"),
                $dataProcessFinal = $("#dataProcessFinal"),
                $dataVencInicial = $("#dataVencInicial"),
                $dataVencFinal = $("#dataVencFinal");

            $confConta.select2({
                language: 'pt-BR',
                allowClear: true,
                placeholder: 'Selecione a Conta'
            });

            $tituloEstado.select2({
                language: 'pt-BR',
                allowClear: true,
                placeholder: 'Selecione o estado dos títulos'
            });

            $reinclusao.select2({
                language: 'pt-BR',
                allowClear: true
            });

            $tituloEstado.select2('val', 'Aberto');
            $reinclusao.select2('val', 'Não');
            $("#reinclusao, #tituloEstado").select2('disable');

            __financeiroRemessaIndex.iniciarElementoDatePicker(
                $dataProcessInicial, {
                    maxDate: new Date(),
                    onSelect: function (newDate, $datepicker) {
                        // atualizando o valor minimo da data final para o lancamento de atividades
                        // subtrai o mês por '-1' porque no javascript os meses começam em '0'
                        newDate = newDate.split('/');
                        $dataProcessFinal.datepicker(
                            'option', 'minDate', new Date(newDate[2], (newDate[1] - 1), newDate[0])
                        );
                    }
                }
            );

            __financeiroRemessaIndex.iniciarElementoDatePicker(
                $dataProcessFinal,
                {
                    maxDate: new Date(),
                    onSelect: function (newDate, $datepicker) {
                        // atualizando o valor maximo da data inicial para o lancamento de atividades
                        // subtrai o mês por '-1' porque no javascript os meses começam em '0'
                        newDate = newDate.split('/');
                        $dataProcessInicial.datepicker(
                            'option', 'maxDate', new Date(newDate[2], (newDate[1] - 1), newDate[0])
                        );
                    }
                }
            );

            __financeiroRemessaIndex.iniciarElementoDatePicker(
                $dataVencInicial,
                {
                    minDate: new Date(),
                    onSelect: function (newDate, $datepicker) {
                        // atualizando o valor minimo da data final para o lancamento de atividades
                        // subtrai o mês por '-1' porque no javascript os meses começam em '0'
                        newDate = newDate.split('/');
                        $dataVencFinal.datepicker(
                            'option', 'minDate', new Date(newDate[2], (newDate[1] - 1), newDate[0])
                        );
                    }
                }
            );

            __financeiroRemessaIndex.iniciarElementoDatePicker(
                $dataVencFinal,
                {
                    minDate: new Date(),
                    onSelect: function (newDate, $datepicker) {
                        // atualizando o valor maximo da data inicial para o lancamento de atividades
                        // subtrai o mês por '-1' porque no javascript os meses começam em '0'
                        newDate = newDate.split('/');
                        $dataVencInicial.datepicker(
                            'option', 'maxDate', new Date(newDate[2], (newDate[1] - 1), newDate[0])
                        );
                    }
                }
            );

            var arrCarteiraBoleto = [
                {'id': '1', 'text': 'Registrado'},
                {'id': '2', 'text': 'Não Registrado'}
            ];
            $("#carteiraBoleto").select2(
                {
                    data: arrCarteiraBoleto
                });

            $("#carteiraBoleto").select2("val", 1);


            $(".panel-title a").click(function () {
                $(".panel-title .fa").toggleClass('fa-chevron-right').toggleClass('fa-chevron-down');
            });

            $("#limpar-datas").click(function () {
                $("#dataVencFinal, #dataVencInicial,#dataProcessFinal,#dataProcessInicial").val('');
            });

            $("#btn-visualiza-remessa").click(function () {
                $("#financeiro-remessas-modal").modal("show");
            });

            $("#btn-fechar-remessa").click(function () {
                $("#financeiro-remessas-modal").modal("hide");
            });

            $("#filtrar-resultados").click(function () {
                !$("#painelAbas").is(":visible") ? $("#painelAbas,#gerar-remessa").removeClass('hidden') : "";

                var colNum = 0;
                __financeiroRemessaIndex.addOverlay($("#main"));

                if ($.fn.DataTable.isDataTable("#tableBoletos")) {

                    try {
                        $("#tableBoletos").DataTable().destroy();
                    } catch (err) {

                    }

                    $("#tableBoletos").DataTable().destroy();
                }

                __financeiroRemessaIndex.options.dataTables.boletosTab = $("#tableBoletos").DataTable({
                    processing: true,
                    ordering: false,
                    serverSide: true,
                    searching: true,
                    "sDom": "<'dt-toolbar-header'<'col-sm-4'l><'col-sm-4'r><'col-sm-4'f>>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                    autoWidth: true,
                    ajax: {
                        url: __financeiroRemessaIndex.options.url.urlSearchBoleto,
                        type: "POST",
                        data: function (d) {
                            d.boletoCarteira = $("#carteiraBoleto").val();
                            d.titulosValidosRemessa = true;
                            d.boletosValidosRemessa = true;
                            d.confContaId = $confConta.select2('val');
                            d.tituloEstado = $tituloEstado.select2('val');
                            d.reinclusao = $reinclusao.select2('val');
                            d.dataPross = {
                                dataInicial: $dataProcessInicial.val(),
                                dataFinal: $dataProcessFinal.val()
                            };
                            d.dataVenc = {
                                dataInicial: $dataVencInicial.val(),
                                dataFinal: $dataVencFinal.val()
                            };
                            d.boletosNotInclused = __financeiroRemessaIndex.options.data.boletosRemovidos || [];

                            return d;
                        },
                        dataSrc: function (r) {
                            var data = r.data;
                            $("#gerar-remessa").removeClass("hidden");

                            if (JSON.stringify(data) === "[]") {
                                $("#gerar-remessa").addClass("hidden");

                                __financeiroRemessaIndex.showNotificacaoInfo("Nenhum título localizado para estes filtros!");
                                $("#gerar-remessa").addClass("hidden");
                            }

                            for (var row in data) {
                                var btn = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button"  title="Adicionar a Exceção" class="btn btn-xs btn-warning financeiroTituloConfig-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                                data[row]['action'] = btn;
                            }

                            return r.data;
                        }
                    },
                    columnDefs: [
                        {name: 'sacado', targets: colNum++, data: 'action', searchable: false, width: '10%'},
                        {name: 'titulo_id', targets: colNum++, data: 'titulo_id'},
                        {name: 'nosso_numero', targets: colNum++, data: 'nosso_numero'},
                        {name: 'tipo_titulo', targets: colNum++, data: 'tipo_titulo'},
                        {name: 'alunocurso_id', targets: colNum++, data: 'alunocurso_id'},
                        {name: 'sacado', targets: colNum++, data: 'sacado'},
                        {name: 'valor', targets: colNum++, data: 'valor'},
                        {name: 'data_processamento', targets: colNum++, data: 'data_processamento'},
                        {name: 'data_vencimento', targets: colNum++, data: 'data_vencimento'},
                        {name: 'titulo_estado', targets: colNum++, data: 'titulo_estado'},
                        {name: 'bol_id', targets: colNum++, data: 'bol_id', visible: false, searchable: false}
                    ],
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    }
                });

                __financeiroRemessaIndex.iniciaBotoesDataTables();

                if ($.fn.DataTable.isDataTable("#tableBoletosImpedimento")) {
                    try {
                        $("#tableBoletosImpedimento").DataTable().destroy();
                    } catch (err) {

                    }

                    $("#tableBoletosImpedimento").DataTable().destroy();
                }

                colNum = 0;

                __financeiroRemessaIndex.options.dataTables.boletosImpedimentosTab = $("#tableBoletosImpedimento").DataTable({
                    processing: true,
                    serverSide: true,
                    ordering: false,
                    searching: true,
                    "sDom": "<'dt-toolbar-header'<'col-sm-4'l><'col-sm-4'r><'col-sm-4'f>>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                    autoWidth: true,
                    ajax: {
                        url: __financeiroRemessaIndex.options.url.urlSearchBoleto,
                        type: "POST",
                        data: function (d) {
                            d.boletoCarteira = $("#carteiraBoleto").val();
                            d.titulosValidosRemessa = true;
                            d.boletosInValidosRemessa = true;
                            d.confContaId = $confConta.select2('val');
                            d.tituloEstado = $tituloEstado.select2('val');
                            d.reinclusao = $reinclusao.select2('val');
                            d.dataPross = {
                                dataInicial: $dataProcessInicial.val(),
                                dataFinal: $dataProcessFinal.val()
                            };
                            d.dataVenc = {
                                dataInicial: $dataVencInicial.val(),
                                dataFinal: $dataVencFinal.val()
                            };
                            return d;
                        },
                        dataSrc: function (r) {

                            var data = r.data;

                            for (var row in data) {
                                if (data[row]['cpf_cnpj_invalido']) {

                                    data[row]['observacao'] = "CPF ou CNPJ do títular inválidos!";
                                }

                                if (data[row]['endereco_invalido']) {

                                    data[row]['observacao'] =
                                        data[row]['observacao'] ? data[row]['observacao'] + " - Endereço inválido ou incompleto!" :
                                            "Endereço inválido ou incompleto!";
                                }

                                if (data[row]['boleto_vencido']) {

                                    data[row]['observacao'] =
                                        data[row]['observacao'] = data[row]['observacao'] ? data[row]['observacao'] + " - Boleto Vencido!" :
                                            "Boleto Vencido!";
                                }
                            }

                            return r.data;
                        }
                    },
                    columnDefs: [
                        {name: 'titulo_id', targets: colNum++, data: 'titulo_id'},
                        {name: 'nosso_numero', targets: colNum++, data: 'nosso_numero'},
                        {name: 'tipo_titulo', targets: colNum++, data: 'tipo_titulo'},
                        {name: 'alunocurso_id', targets: colNum++, data: 'alunocurso_id'},
                        {name: 'sacado', targets: colNum++, data: 'sacado'},
                        {name: 'valor', targets: colNum++, data: 'valor'},
                        {name: 'data_processamento', targets: colNum++, data: 'data_processamento'},
                        {name: 'data_vencimento', targets: colNum++, data: 'data_vencimento'},
                        {name: 'titulo_estado', targets: colNum++, data: 'titulo_estado'},
                        {name: 'observacao', targets: colNum++, data: 'observacao'},
                        {name: 'bol_id', targets: colNum++, data: 'bol_id', visible: false, searchable: false}
                    ],
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    }
                });

                if ($.fn.DataTable.isDataTable("#tableBoletosRemovidos")) {
                    try {
                        $("#tableBoletosRemovidos").DataTable().destroy();
                    } catch (err) {
                    }

                    $("#tableBoletosRemovidos").DataTable().destroy();
                }

                colNum = 0;

                __financeiroRemessaIndex.options.dataTables.boletosRemovidosTab = $("#tableBoletosRemovidos").DataTable({
                    "sDom": "<'dt-toolbar-header'<'col-sm-4'l><'col-sm-4'r><'col-sm-4'f>>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                    autoWidth: true,
                    data: JSON.parse('{"draw":"1", "recordsTotal": 0, "recordsFiltered": 0, "data": []}'),
                    columnDefs: [
                        {name: 'sacado', targets: colNum++, data: 'action'},
                        {name: 'titulo_id', targets: colNum++, data: 'titulo_id'},
                        {name: 'nosso_numero', targets: colNum++, data: 'nosso_numero'},
                        {name: 'tipo_titulo', targets: colNum++, data: 'tipo_titulo'},
                        {name: 'alunocurso_id', targets: colNum++, data: 'alunocurso_id'},
                        {name: 'sacado', targets: colNum++, data: 'sacado'},
                        {name: 'valor', targets: colNum++, data: 'valor'},
                        {name: 'data_processamento', targets: colNum++, data: 'data_processamento'},
                        {name: 'data_vencimento', targets: colNum++, data: 'data_vencimento'},
                        {name: 'titulo_estado', targets: colNum++, data: 'titulo_estado'},
                        {name: 'bol_id', targets: colNum++, data: 'bol_id', visible: false, searchable: false},
                    ],
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    }
                });

                __financeiroRemessaIndex.removeOverlay($("#main"));
            });

            $("#gerar-remessa, #gerar-remessa-excecao").on('click', function () {
                var qtdMaximaBoletos = 950, arrFiltros = {};

                if (__financeiroRemessaIndex.options.value.quantidadeMaximaBoletos) {
                    qtdMaximaBoletos = __financeiroRemessaIndex.options.value.quantidadeMaximaBoletos;
                }

                arrFiltros.todosBoletosValidos = true;
                arrFiltros.boletoCarteira = $("#carteiraBoleto").val();
                arrFiltros.titulosValidosRemessa = true;
                arrFiltros.confContaId = $confConta.select2('val');
                arrFiltros.tituloEstado = $tituloEstado.select2('val');
                arrFiltros.reinclusao = $reinclusao.select2('val');
                arrFiltros.dataPross = {
                    dataInicial: $dataProcessInicial.val(),
                    dataFinal: $dataProcessFinal.val()
                };
                arrFiltros.dataVenc = {
                    dataInicial: $dataVencInicial.val(),
                    dataFinal: $dataVencFinal.val()
                };
                arrFiltros.boletosNotInclused = __financeiroRemessaIndex.options.data.boletosRemovidos || [];
                arrFiltros.gerarRemessaTodosTitulos = true;

                var $this = $(this);

                if ($this.attr('id') == 'gerar-remessa-excecao') {
                    arrFiltros.todosBoletosValidos = false;
                    arrFiltros.gerarRemessaTodosTitulos = false;
                    arrFiltros.boletosNotInclused = [];
                    arrFiltros.boletos = __financeiroRemessaIndex.options.data.boletosRemovidos || [];

                    var arrBoletosRemovidos = __financeiroRemessaIndex.options.data.boletosRemovidos || [];

                    if (!arrBoletosRemovidos || arrBoletosRemovidos.length == 0) {
                        __financeiroRemessaIndex.showNotificacao({
                            content: "Não há boletos selecionados para gerar o arquivo de remessa!",
                            type: 'info'
                        });
                        return false;

                    }
                }

                arrFiltros.todosBoletosValidos = arrFiltros.todosBoletosValidos ? 1 : 0;
                arrFiltros.gerarRemessaTodosTitulos = arrFiltros.gerarRemessaTodosTitulos ? 1 : 0;
                arrFiltros.titulosValidosRemessa = arrFiltros.titulosValidosRemessa ? 1 : 0;


                $.SmartMessageBox({
                    title: "Deseja gerar o arquivo para estes boletos?",
                    content: 'Atenção: Estes boletos não poderão ser adicionados a outro arquivo de remessa!',
                    buttons: "[Cancelar][Ok]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Ok") {
                        __financeiroRemessaIndex.showNotificacao({
                            title: "Atenção!",
                            content: "Será adicionado no máximo: " + qtdMaximaBoletos + " boletos por arquivo!",
                            type: 'info'
                        });

                        $.ajax({
                            type: 'POST',
                            url: __financeiroRemessaIndex.options.url.urlGeraRemessa,
                            data: arrFiltros,
                            beforeSend: function () {
                                __financeiroRemessaIndex.addOverlay(
                                    $("#main"), "Aguarde, gerando o arquivo de remessa..."
                                );
                            },
                            success: function (r) {
                                $("#limpar-datas").click();
                                $("#painelAbas, #gerar-remessa").addClass("hidden");

                                if (r['message'][0] && r['message'][0]['type'] == 'error') {
                                    var posicao = null;

                                    for (posicao in r['message']) {

                                        var teste = r['message'][posicao]['message'];
                                        __financeiroRemessaIndex.showNotificacaoWarning(r['message'][posicao]['message']);
                                    }

                                    return false;
                                }

                                if (r['error']) {
                                    __financeiroRemessaIndex.showNotificacaoDanger(
                                        r['message'] || "Não foi possível gerar remessa(s)!"
                                    );

                                    return false;
                                }

                                $.financeiroRemessaIndex().refreshDataTables();

                                if (r['chaveDownload']) {
                                    var url = '';
                                    var msg = [];

                                    if (Object.keys(r['chaveDownload']).length > 0) {
                                        $.each(r['chaveDownload'], function (i, item) {
                                            url = __financeiroRemessaIndex.options.url.urlDownload + "/" + item['chave'];
                                            msg.push('Para baixar o arquivo ' + item['nome'] + ' clique <a target="_blank" href="' + url + '">aqui</a>.');
                                        });
                                    } else {
                                        url = __financeiroRemessaIndex.options.url.urlDownload + "/" + r.chaveDownload;
                                        msg.push('Para baixar o arquivo clique <a target="_blank" href="' + url + '">aqui</a>.');
                                    }

                                    __financeiroRemessaIndex.showNotificacao({
                                        title: "Arquivo gerado com sucesso: ",
                                        content: (
                                            'Um total de ' + r.inseridos.length + ' Boletos foram incluidos no arquivo.<br>' +
                                            msg.join('<br>')
                                        ),
                                        type: 'success'
                                    });

                                    __financeiroRemessaIndex.options.data.boletosNotInclused = [];
                                } else {
                                    var content = '<br>';
                                    for (var row in r.valid) {
                                        content += r.valid[row] + '<br>';
                                    }
                                    __financeiroRemessaIndex.showNotificacaoWarning('Ocorreram erros na geração de arquivo!');
                                }

                                if (r.errors.length > 0) {
                                    var content = "";

                                    for (var bol in r.errors) {
                                        content += "<ul>";
                                        content += "<li> Nosso Número:" + bol;
                                        content += "<ul>";
                                        for (var message in r.errors[bol]) {
                                            content += "<li>" + r.errors[bol][message] + "</li>";
                                        }
                                        content += "</ul>";
                                        content += "</li>";
                                        content += "</ul>";
                                    }

                                    __financeiroRemessaIndex.showNotificacao({
                                        title: "Boletos não inclusos: ",
                                        content: content,
                                        type: 'danger'
                                    });
                                }
                            },
                            complete: function () {
                                __financeiroRemessaIndex.removeOverlay($("#main"));
                            }
                        });
                    }
                });
            });

        };

        this.setFields = function () {
            var colNum = 0;
            __financeiroRemessaIndex.options.dataTables.tableRemessa = $("#tableRemessa").dataTable({
                processing: true,
                ordering: false,
                serverSide: true,
                destroy: true,
                "sDom": "<'dt-toolbar-header'<'col-sm-4'l><'col-sm-4'r><'col-sm-4'f>>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                ajax: {
                    "url": __financeiroRemessaIndex.options.url.urlListRemessa,
                    "type": "POST",
                    data: function (d) {
                        return d;
                    },
                    dataSrc: function (result) {
                        result = result.data;
                        for (var row in result) {

                            result[row]['dadosBancarios'] = 'Banco: ' + result[row]['banc_nome'] + ' | Agência: ' + result[row]['agencia'] +
                                '| Conta: ' + result[row]['conta'] + ' | Convêncio: ' + result[row]['convenio'];

                            var date = result[row]['dataCriacao'];
                            result[row]['arquivo'] = "\
                                <a class='btn btn-default btn-xs'\
                                    title= '" + result[row]['arquivoNome'] + "'\
                                    data-codigo = '" + result[row]["codigo"] + "'\
                                    href = '" + __financeiroRemessaIndex.options.url.urlDownFile + "/" + result[row]['arquivo'] + "' >\
                                    <i class='fa fa-cog'></i>\
                                    Baixar Arquivo\
                                </a>\
                            ";
                        }
                        return result;
                    }
                },
                columnDefs: [
                    {name: 'codigo', targets: colNum++, data: 'codigo'},
                    {name: 'codigoRemessa', targets: colNum++, data: 'codigoRemessa'},
                    {name: 'conta', targets: colNum++, data: 'dadosBancarios'},
                    {name: 'quantidadeBoletos', targets: colNum++, data: 'quantidadeBoletos'},
                    {name: 'dataCriacao', targets: colNum++, data: 'dataCriacaoFormatada'},
                    {name: 'codigo', targets: colNum++, data: 'arquivo', searchable: false},
                    {
                        name: 'dataCriacaoFormatada',
                        targets: colNum++,
                        data: 'dataCriacaoFormatada',
                        visible: false,
                        searchable: true
                    }
                ],
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                }
            });
        };

        this.refreshDataTables = function () {
            __financeiroRemessaIndex.options.dataTables.tableRemessa.api().ajax.reload(null, false);
        };

        this.run = function (opts) {
            __financeiroRemessaIndex.setDefaults(opts);
            __financeiroRemessaIndex.setSteps();
            __financeiroRemessaIndex.setFields();
        };

        this.iniciaBotoesDataTables = function () {
            $(document).on('click', ".financeiroTituloConfig-remove", function () {
                var row = $(this).parents('tr');

                row.find('.financeiroTituloConfig-remove').remove();
                var data = __financeiroRemessaIndex.options.dataTables.boletosTab.row(row).data();

                if (data) {
                    var tituloId = data.titulo_id;

                    __financeiroRemessaIndex.showNotificacaoInfo("O título: " + tituloId + " foi adicionado a tabela de exceções!");

                    data['action'] = '\
                <div class="text-center">\
                    <div class="btn-group">\
                        <button type="button" class="btn btn-xs btn-primary financeiroTituloConfig-add">\
                            <i class="fa fa-plus-circle"></i>\
                        </button>\
                    </div>\
                </div>';

                    __financeiroRemessaIndex.options.data.boletosRemovidos.push(data['bol_id']);
                    __financeiroRemessaIndex.options.dataTables.boletosRemovidosTab.row.add(data).draw();
                    __financeiroRemessaIndex.options.dataTables.boletosTab.ajax.reload();
                    __financeiroRemessaIndex.iniciaBotoesDataTables();
                }
            });

            $(".financeiroTituloConfig-add").on('click', function () {

                var row = $(this).parents('tr');
                var data = __financeiroRemessaIndex.options.dataTables.boletosRemovidosTab.row(row).data();

                data['action'] = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-danger financeiroTituloConfig-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                for (var i in __financeiroRemessaIndex.options.data.boletosRemovidos) {
                    if (__financeiroRemessaIndex.options.data.boletosRemovidos[i] == data['bol_id']) {
                        delete __financeiroRemessaIndex.options.data.boletosRemovidos[i];
                        break;
                    }
                }

                __financeiroRemessaIndex.options.dataTables.boletosRemovidosTab.row(row).remove();
                __financeiroRemessaIndex.options.dataTables.boletosRemovidosTab.draw();
                __financeiroRemessaIndex.options.dataTables.boletosTab.ajax.reload();
                __financeiroRemessaIndex.iniciaBotoesDataTables();
            });
        }
    };

    $.financeiroRemessaIndex = function (params) {
        params = params || [];

        var obj = $(window).data('universa.financeiro.remessa.index');

        if (!obj) {
            obj = new FinanceiroRemessaIndex();
            obj.run(params);
            $(window).data('universa.financeiro.remessa.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);