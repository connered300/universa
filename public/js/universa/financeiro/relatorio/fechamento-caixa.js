(function ($, window) {
    'use strict';
    var FechamentoCaixa = function () {
        VersaShared.call(this);
        var __fechamentoCaixa = this;
        this.defaults = {
            ajax: true,
            url: {
                campusCurso: '',
                responsavelPagamento: '',
                meioPagamento: '',
                tipoTitulo: ''
            },
            value: {
                gruposNaListagemUsarios: 0
            },
            formElement: '#fechamento-caixa-form'
        };

        this.setSteps = function () {
            var $campusCurso          = $('[name="campusCurso"]'),
                $responsavelPagamento = $('[name="pagamentoUsuario"]'),
                $tipoTitulo           = $('[name="tipoTitulo"]'),
                $tipoRelatorio        = $('[name="tipoRelatorio"]'),
                $meioPagamento        = $('[name="meioPagamento"]');

            $campusCurso.select2({
                language: 'pt-BR',
                multiple: true,
                minimumInputLength: 1,
                ajax: {
                    url: __fechamentoCaixa.options.url.campusCurso,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            var text = el.camp_nome + ' / ' + el.curso_nome;
                            return {text: text, id: el.cursocampus_id};
                        });

                        return {results: transformed};
                    }
                }
            });

            $responsavelPagamento.select2({
                language: 'pt-BR',
                multiple: true,
                minimumInputLength: 1,
                ajax: {
                    url: __fechamentoCaixa.options.url.responsavelPagamento,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {
                            query: query,
                            agruparPorUsuario: true,
                            grupo: __fechamentoCaixa.options.value.gruposNaListagemUsarios || null
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: '('+el.login +') '+ (el.pes_nome || ''), id: el.id};
                        });

                        return {results: transformed};
                    }
                }
            });

            $tipoTitulo.select2({
                language: 'pt-BR',
                multiple: true,
                ajax: {
                    url: __fechamentoCaixa.options.url.tipoTitulo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.tipotitulo_nome, id: el.tipotitulo_id};
                        });

                        return {results: transformed};
                    }
                }
            });

            $meioPagamento.select2({
                language: 'pt-BR',
                multiple: true,
                ajax: {
                    url: __fechamentoCaixa.options.url.meioPagamento,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.meioPagamentoDescricao, id: el.meioPagamentoId};
                        });

                        return {results: transformed};
                    }
                }
            });

            $tipoRelatorio.select2({
                language: 'pt-BR',
                data: [
                    {id: 'analitico', text: 'Analítico'},
                    {id: 'sintetico', text: 'Sintético'}
                ]
            });

            __fechamentoCaixa.iniciarElementoDatePicker($("#dataInicial, #dataFinal"));

            __fechamentoCaixa.options.validator.settings.messages = {
                dataInicial: {required: 'Campo obrigatório!'},
                dataFinal: {required: 'Campo obrigatório!'},
                tipoRelatorio: {required: 'Campo obrigatório!'}
            };

            __fechamentoCaixa.options.validator.settings.rules = {
                dataInicial: {required: true},
                dataFinal: {required: true},
                tipoRelatorio: {required: true}
            };

            $('#fechamento-caixa-form').on('submit', function (e) {
                var ok = $(__fechamentoCaixa.options.formElement).valid();

                if (!ok) {
                    e.stopPropagation();
                    e.preventDefault();
                }
            });

            __fechamentoCaixa.limparCampos();
        };

        this.limparCampos = function () {
            var $campusCurso          = $('[name="campusCurso"]'),
                $responsavelPagamento = $('[name="pagamentoUsuario"]'),
                $tipoTitulo           = $('[name="tipoTitulo"]'),
                $tipoPagamento        = $('[name="tipoPagamento"]'),
                $tipoRelatorio        = $('[name="tipoRelatorio"]'),
                $meioPagamento        = $('[name="meioPagamento"]');

            $tipoTitulo.select2('val', '').trigger('change');
            $campusCurso.select2('val', '').trigger('change');
            $tipoPagamento.select2('val', '').trigger('change');
            $meioPagamento.select2('val', '').trigger('change');
            $responsavelPagamento.select2('val', '').trigger('change');
            $tipoRelatorio.select2('val', 'analitico').trigger('change');

            var hoje = __fechamentoCaixa.formatDate(new Date());

            $('[name="dataInicial"]').val(hoje);
            $('[name="dataFinal"]').val(hoje);
        };

        this.run = function (opts) {
            __fechamentoCaixa.setDefaults(opts);
            __fechamentoCaixa.setSteps();
        };
    };

    $.fechamentoCaixa = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.relatorio.fechamento-caixa");

        if (!obj) {
            obj = new FechamentoCaixa();
            obj.run(params);
            $(window).data('universa.financeiro.relatorio.fechamento-caixa', obj);
        }

        return obj;
    };
})(window.jQuery, window);