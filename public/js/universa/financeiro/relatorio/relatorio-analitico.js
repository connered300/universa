(function ($, window, document) {
    'use strict';

    var RelatorioAnalitico = function () {
        VersaShared.call(this);
        var __relatorioAnalitico = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                financeiroTituloTipo: '',
                acadgeralAreaConhecimento: '',
                acadCurso: '',
                orgCampus: '',
                orgAgenteEducacional: '',
                meioPagamento: '',
                acessoPessoas: '',
                periodoLetivo: '',
                turmaAlunos: ''
            },
            data: {
                arrSituacaoAlunoPeriodo: [],
                arrRelatorios: []
            },
            value: {
                arrTituloEstado: null
            },
            datatables: {},
            wizardElement: '#relatorio-analitico-wizard',
            formElement: '#relatorio-analitico-form',
            validator: null
        };

        this.filtrosRelatorio = {
            areaId: true,
            campId: true,
            cursoId: true,
            usuarioCadastroAluno: true,
            pesIdAgente: true,
            tipotituloId: true,
            tituloEstado: false,
            usuarioAutor: true,
            usuarioBaixa: true,
            tituloDataProcessamentoInicio: false,
            tituloDataProcessamentoFim: false,
            tituloDataVencimentoInicio: false,
            tituloDataVencimentoFim: false,
            tituloDataPagamentoInicio: false,
            tituloDataPagamentoFim: false,
            tituloDataBaixaInicio: false,
            tituloDataBaixaFim: false,
            agenteOpcao: false,
            meioDePagamento: true,
            meioPag: false
        };

        this.steps = {};

        this.setSteps = function () {
            $('#tipoRelatorio')
                .select2({
                    allowClear: true,
                    data: __relatorioAnalitico.options.data.arrRelatorios,
                    minimumResultsForSearch: -1,
                    language: 'pt-BR'
                })
                .change(function () {
                    var data = $(this).select2('data') || {};
                    var filtros = data['filtros'] || {};
                    var ext = data['ext'] || [];

                    for (var chave in __relatorioAnalitico.filtrosRelatorio) {
                        var isSelect2 = __relatorioAnalitico.filtrosRelatorio[chave];

                        $('[name=' + chave + ']').prop('disabled', false).trigger('change');

                        if (typeof filtros[chave] != "undefined") {
                            $('[name=' + chave + ']').val(filtros[chave]).prop('disabled', true).trigger('change');
                        }
                    }

                    $('#gerar-xlsx, #gerar-pdf').hide();

                    if (ext.indexOf('pdf') != -1) {
                        $('#gerar-pdf').show();
                    }

                    if (ext.indexOf('xlsx') != -1) {
                        $('#gerar-xlsx').show();
                    }
                })
                .select2('val', 'financeiro_titulos')
                .trigger('change');


            $("#areaId").select2({
                language: 'pt-BR',
                allowClear: true,
                multiple: true,
                ajax: {
                    url: __relatorioAnalitico.options.url.acadgeralAreaConhecimento,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.area_descricao, id: el.area_id};
                        });

                        return {results: transformed};
                    }
                }
            });

            $('#tituloEstado').select2({
                allowClear: true,
                data: __relatorioAnalitico.options.data.arrTituloEstado,
                minimumInputLength: -1,
                multiple: true,
                language: 'pt-BR'
            });

            $("#tipotituloId").select2({
                language: 'pt-BR',
                multiple: true,
                minimumInputLength: -1,
                ajax: {
                    url: __relatorioAnalitico.options.url.financeiroTituloTipo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.tipotitulo_nome;
                            el.id = el.tipotitulo_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $('#campId').select2({
                language: 'pt-BR',
                multiple: true,
                ajax: {
                    url: __relatorioAnalitico.options.url.orgCampus,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.camp_nome;
                            el.id = el.camp_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $('#cursoId').select2({
                language: 'pt-BR',
                multiple: true,
                ajax: {
                    url: __relatorioAnalitico.options.url.acadCurso,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.curso_nome;
                            el.id = el.curso_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $('#usuarioAutor, #usuarioBaixa, #usuarioCadastroAluno').select2({
                language: 'pt-BR',
                multiple: true,
                minimumInputLength: 1,
                ajax: {
                    url: __relatorioAnalitico.options.url.acessoPessoas,
                    dataType: 'json',
                    delay: 250,
                    data: function (pesquisa) {
                        return {query: pesquisa};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.pes_nome || el.login;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $('#pesIdAgente').select2({
                language: 'pt-BR',
                multiple: true,
                minimumInputLength: 1,
                ajax: {
                    url: __relatorioAnalitico.options.url.orgAgenteEducacional,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                            var transformed = $.map(data, function (el) {
                            el.id = el.pes_id;
                            el.text = el.pes_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $('#periodoLetivo').select2({
                language: 'pt-BR',
                multiple: true,
                ajax: {
                    url: __relatorioAnalitico.options.url.periodoLetivo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.per_id;
                            el.text = el.per_nome;
                            return el;
                        });
                        return {results: transformed};
                    }
                }

            }).on('change', function (e) {
                if (!$('#periodoLetivo').val()) {
                    $('#situacaoalunoPeriodoLetivo').select2('data', "").trigger('change');
                    $('#alunosTurma').select2('data', "").trigger('change');
                    $('#situacaoalunoPeriodoLetivo').select2('disable', true);
                    $('#alunosTurma').select2('disable', true);
                } else {
                    $('#situacaoalunoPeriodoLetivo').select2('enable', true);
                    $('#alunosTurma').select2('enable', true);
                }
            });

            $("#situacaoalunoPeriodoLetivo").select2({
                language: 'pt-BR',
                multiple: false,
                allowClear:true,
                data: __relatorioAnalitico.options.data.arrSituacaoAlunoPeriodo
            });

            $('#alunosTurma').select2({
                language: 'pt-BR',
                multiple: true,
                ajax: {
                    url: __relatorioAnalitico.options.url.turmaAlunos,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {
                            query: query,
                            perId: $("#periodoLetivo").val()
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.turma_id;
                            el.text = el.turma_nome;
                            return el;
                        });
                        return {results: transformed};
                    }
                }
            });

            $('#meioDePagamento').select2({
                language: 'pt-BR',
                allowClear: true,
                multiple: true,
                ajax: {
                    url: __relatorioAnalitico.options.url.meioPagamento,
                    dataType: 'json',
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.meioPagamentoDescricao, id: el.meioPagamentoId};
                        });

                        return {results: transformed};
                    }
                }
            });

            __relatorioAnalitico.iniciarElementoDatePicker($(
                '#tituloDataVencimentoInicio, #tituloDataVencimentoFim,' +
                '#tituloDataPagamentoInicio, #tituloDataPagamentoFim,' +
                '#tituloDataBaixaInicio, #tituloDataBaixaFim,' +
                '#tituloDataProcessamentoInicio, #tituloDataProcessamentoFim'
            ));

            if (!$('#periodoLetivo').val()) {
                $('#situacaoalunoPeriodoLetivo').select2('disable', true);
                $('#alunosTurma').select2('disable', true);
            }
            $("#tituloParcela").inputmask({
                showMaskOnHover: false,
                mask: ['9{0,9}'],
                rightAlign: true,
                allowMinus: false
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
        };
    };
    $("#limpar").click(function (event) {
        $('#areaId').select2('val', '').trigger('change');
        $('#campId').select2('val', '').trigger('change');
        $('#cursoId').select2('val', '').trigger('change');
        $('#tipotituloId').select2('val', '').trigger('change');
        $('#tituloEstado').select2('val', '').trigger('change');
        $('#usuarioAutor').select2('val', '').trigger('change');
        $('#usuarioBaixa').select2('val', '').trigger('change');
        $('#usuarioCadastroAluno').select2('val', '').trigger('change');
        $('#tituloDataVencimentoInicio').val('').trigger('change');
        $('#tituloDataVencimentoFim').val('').trigger('change');
        $('#tituloDataPagamentoInicio').val('').trigger('change');
        $('#tituloDataPagamentoFim').val('').trigger('change');
        $('#tituloDataBaixaInicio').val('').trigger('change');
        $('#tituloDataBaixaFim').val('').trigger('change');
        $('#tituloDataProcessamentoInicio').val('').trigger('change');
        $('#tituloDataProcessamentoFim').val('').trigger('change');
        $("#tituloParcela").val('');
        $('#periodoLetivo').select2('val', '').trigger('change');
        $('#situacaoalunoPeriodoLetivo').select2('val', '').trigger('change');
        $('#alunosTurma').select2('val', '').trigger('change');


    });

    $.relatorioAnalitico = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.relatorio.analitico");

        if (!obj) {
            obj = new RelatorioAnalitico();
            obj.run(params);
            $(window).data('universa.financeiro.relatorio.analitico', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);