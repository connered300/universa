(function ($, window) {
    'use strict';
    var ComprovanteRenda = function () {
        VersaShared.call(this);
        var __comprovanteRenda = this;
        this.defaults = {
            ajax: true,
            url: {
                pessoa: ''
            },
            value: {},
            formElement: '#comprovante-renda-form'
        };

        this.setSteps = function () {
            $('#pesId').select2({
                language: 'pt-BR',
                minimumInputLength: 1,
                ajax: {
                    url: __comprovanteRenda.options.url.pessoa,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {
                            query: query,
                            agruparAluno: 1
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.pes_nome, id: el.pes_id};
                        });

                        return {results: transformed};
                    }
                }
            });

            __comprovanteRenda.options.validator.settings.messages = {
                anoBase: {required: 'Campo obrigatório!'},
                pesId: {required: 'Campo obrigatório!'}
            };

            __comprovanteRenda.options.validator.settings.rules = {
                anoBase: {required: true},
                pesId: {required: true}
            };

        };


        this.run = function (opts) {
            __comprovanteRenda.setDefaults(opts);
            __comprovanteRenda.setSteps();
        };
    };

    $.comprovanteRenda = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.relatorio.comprovante-renda");

        if (!obj) {
            obj = new ComprovanteRenda();
            obj.run(params);
            $(window).data('universa.financeiro.relatorio.comprovante-renda', obj);
        }

        return obj;
    };
})(window.jQuery, window);